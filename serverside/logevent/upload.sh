#!/usr/bin/env bash
aws lambda create-function \
   --region eu-central-1 \
   --function-name new-event \
   --zip-file fileb://target/logevent-1.0-SNAPSHOT.jar --role arn:aws:iam::825403711740:role/service-role/logevent-role \
   --handler greybeard.NewEventLambda \
   --runtime java8 \
   --timeout 15 \
   --memory-size 512

aws lambda create-function \
   --region eu-central-1 \
   --function-name add-behaviour \
   --zip-file fileb://target/logevent-1.0-SNAPSHOT.jar --role arn:aws:iam::825403711740:role/service-role/logevent-role \
   --handler greybeard.AddBehaviourLambda \
   --runtime java8 \
   --timeout 15 \
   --memory-size 512

aws lambda create-function \
   --region eu-central-1 \
   --function-name add-remedy \
   --zip-file fileb://target/logevent-1.0-SNAPSHOT.jar --role arn:aws:iam::825403711740:role/service-role/logevent-role \
   --handler greybeard.AddRemedyLambda \
   --runtime java8 \
   --timeout 15 \
   --memory-size 512

aws lambda create-function \
   --region eu-central-1 \
   --function-name event-end \
   --zip-file fileb://target/logevent-1.0-SNAPSHOT.jar --role arn:aws:iam::825403711740:role/service-role/logevent-role \
   --handler greybeard.EventEndLambda \
   --runtime java8 \
   --timeout 15 \
   --memory-size 512
