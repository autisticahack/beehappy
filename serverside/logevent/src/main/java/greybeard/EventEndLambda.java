package greybeard;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import greybeard.dynamo.EventDao;

public class EventEndLambda implements RequestHandler<EventEndRequest, EventResponse> {
    private EventDao dao = new EventDao();
    public EventResponse handleRequest(EventEndRequest input, Context context) {
        System.out.println(input);

        if (dao.addResolution(input))
            return new EventResponse("ok");
        else
            return new EventResponse("failed");

    }
}

