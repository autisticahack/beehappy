package greybeard;

public class AddRemedyRequest {

    private String uuid;

    private String remedy;

    private String locationGps;

    private String isoDateTime;

    private String notes;

    public String getUuid() {
        return uuid;
    }

    public String getRemedy() {
        return remedy;
    }

    public String getLocationGps() {
        return locationGps;
    }

    public String getIsoDateTime() {
        return isoDateTime;
    }

    public String getNotes() {
        return notes;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public void setRemedy(String remedy) {
        this.remedy = remedy;
    }

    public void setLocationGps(String locationGps) {
        this.locationGps = locationGps;
    }

    public void setIsoDateTime(String isoDateTime) {
        this.isoDateTime = isoDateTime;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    @Override
    public String toString() {
        return "AddRemedyRequest{" +
                "uuid='" + uuid + '\'' +
                ", remedy='" + remedy + '\'' +
                ", locationGps='" + locationGps + '\'' +
                ", isoDateTime='" + isoDateTime + '\'' +
                ", notes='" + notes + '\'' +
                '}';
    }
}
