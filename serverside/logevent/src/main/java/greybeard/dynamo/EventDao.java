package greybeard.dynamo;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.document.*;
import com.amazonaws.services.dynamodbv2.document.spec.UpdateItemSpec;
import com.amazonaws.services.dynamodbv2.document.utils.NameMap;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.amazonaws.services.dynamodbv2.model.ReturnValue;
import greybeard.AddBehaviourRequest;
import greybeard.AddRemedyRequest;
import greybeard.EventEndRequest;
import greybeard.NewEventRequest;

public class EventDao {

    public boolean insert(final NewEventRequest newEvent) {

        final Table table = openTable("Event");

        final ValueMap feeling = new ValueMap().
                with("what", newEvent.getFeeling()).
                with("when", newEvent.getIsoDateTime()).
                with("where", newEvent.getLocationGps());

        try {
            final PutItemOutcome outcome =
                    table.putItem(new Item().withPrimaryKey("uuid", newEvent.getUuid()).
                            withString("anxious-person", newEvent.getAnxiousPerson()).
                            withMap("behaviour", new ValueMap()).
                            withMap("feeling", feeling).
                            withMap("resolution", new ValueMap()));

            System.out.println(outcome.getPutItemResult());

        } catch (Exception e) {
            System.err.println("Unable to add item: " + newEvent.getUuid() + " " + newEvent.getAnxiousPerson());
            System.err.println(e.getMessage());
            return false;
        }
        return true;
    }

    public boolean addBehaviour(final AddBehaviourRequest addBehaviour) {
        final Table table = openTable("Event");

        UpdateItemSpec updateItemSpec = new UpdateItemSpec().withPrimaryKey("uuid", addBehaviour.getUuid()).
                withUpdateExpression("SET #b.#what=:w1, #b.#when=:w2, #b.#where=:w3, #b.#notes=:notes").
                withNameMap(new NameMap().
                        with("#b", "behaviour").
                        with("#what", "what").
                        with("#when", "when").
                        with("#where", "where").
                        with("#notes", "notes")).
                withValueMap(new ValueMap().
                        with(":w1", addBehaviour.getBehaviour()).
                        with(":w2", addBehaviour.getIsoDateTime()).
                        with(":w3", addBehaviour.getLocationGps()).
                        with(":notes", addBehaviour.getNotes())).
        withReturnValues(ReturnValue.UPDATED_NEW);

        try {
            System.out.println("Updating the item...");
            UpdateItemOutcome outcome = table.updateItem(updateItemSpec);
            System.out.println("UpdateItem succeeded:\n" + outcome.getItem().toJSONPretty());
            return true;

        } catch (Exception e) {
            System.err.println("Unable to update item: " + addBehaviour.getUuid());
            System.err.println(e.getMessage());
            return false;
        }
    }

    public boolean addRemedy(final AddRemedyRequest addRemedy) {
        final Table table = openTable("Remedy");

        try {
            final PutItemOutcome outcome =
                    table.putItem(new Item().withPrimaryKey("uuid", addRemedy.getUuid(), "when", addRemedy.getIsoDateTime()).
                            withString("what", addRemedy.getRemedy()).
                            withString("where", addRemedy.getLocationGps()).
                            withString("notes", addRemedy.getNotes()));

            System.out.println(outcome.getPutItemResult());

        } catch (Exception e) {
            System.err.println("Unable to remedy item: " + addRemedy.getUuid() + " " + addRemedy.getUuid());
            System.err.println(e.getMessage());
            return false;
        }
        return true;

    }

    public boolean addResolution(final EventEndRequest eventEnd) {
        final Table table = openTable("Event");

        UpdateItemSpec updateItemSpec = new UpdateItemSpec().withPrimaryKey("uuid", eventEnd.getUuid()).
                withUpdateExpression("SET #b.#outcome=:outcome, #b.#when=:when, #b.#where=:where, #b.#details=:details").
                withNameMap(new NameMap().
                        with("#b", "resolution").
                        with("#outcome", "outcome-successful").
                        with("#when", "when").
                        with("#where", "where").
                        with("#details", "trigger-details")).
                withValueMap(new ValueMap().
                        with(":outcome", eventEnd.getSuccessfulOutcome()).
                        with(":when", eventEnd.getIsoDateTime()).
                        with(":where", eventEnd.getLocationGps()).
                        with(":details", eventEnd.getTriggerDetails())).
                withReturnValues(ReturnValue.UPDATED_NEW);

        try {
            System.out.println("Updating the item...");
            UpdateItemOutcome outcome = table.updateItem(updateItemSpec);
            System.out.println("UpdateItem succeeded:\n" + outcome.getItem().toJSONPretty());
            return true;

        } catch (Exception e) {
            System.err.println("Unable to update item: " + eventEnd.getUuid());
            System.err.println(e.getMessage());
            return false;
        }
    }

    private Table openTable(final String tableName) {
        final AmazonDynamoDBClient client = new AmazonDynamoDBClient().withEndpoint(DynamoConfig.instance.getAWSEndpoint());

        final DynamoDB dynamoDB = new DynamoDB(client);

        return dynamoDB.getTable(tableName);
    }
}
