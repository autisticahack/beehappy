package greybeard.dynamo;

public class DynamoConfig {
    public static DynamoConfig instance = new DynamoConfig();

    public String getAWSEndpoint() {
        return ("https://dynamodb.eu-central-1.amazonaws.com");
    }
}

