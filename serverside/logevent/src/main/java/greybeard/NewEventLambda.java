package greybeard;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import greybeard.dynamo.EventDao;

public class NewEventLambda implements RequestHandler<NewEventRequest, EventResponse> {

    private EventDao dao = new EventDao();

    public EventResponse handleRequest(NewEventRequest input, Context context) {

        System.out.println(input);

        if (dao.insert(input))
            return new EventResponse("ok");
        else
            return new EventResponse("failed");
    }
}
