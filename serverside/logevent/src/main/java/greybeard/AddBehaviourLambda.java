package greybeard;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import greybeard.dynamo.EventDao;

public class AddBehaviourLambda implements RequestHandler<AddBehaviourRequest, EventResponse> {

    private EventDao dao = new EventDao();
    public EventResponse handleRequest(AddBehaviourRequest input, Context context) {
        System.out.println(input);

        if (dao.addBehaviour(input))
            return new EventResponse("ok");
        else
            return new EventResponse("failed");
    }
}
