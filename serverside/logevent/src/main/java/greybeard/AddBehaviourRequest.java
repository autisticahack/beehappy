package greybeard;

public class AddBehaviourRequest {

    private String uuid;
    private String behaviour;
    private String locationGps;
    private String isoDateTime;
    private String notes;

    public String getUuid() {
        return uuid;
    }

    public String getBehaviour() {
        return behaviour;
    }

    public String getLocationGps() {
        return locationGps;
    }

    public String getIsoDateTime() {
        return isoDateTime;
    }

    public String getNotes() {
        return notes;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public void setBehaviour(String behaviour) {
        this.behaviour = behaviour;
    }

    public void setLocationGps(String locationGps) {
        this.locationGps = locationGps;
    }

    public void setIsoDateTime(String isoDateTime) {
        this.isoDateTime = isoDateTime;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    @Override
    public String toString() {
        return "AddBehaviourRequest{" +
                "uuid='" + uuid + '\'' +
                ", behaviour='" + behaviour + '\'' +
                ", locationGps='" + locationGps + '\'' +
                ", isoDateTime='" + isoDateTime + '\'' +
                ", notes='" + notes + '\'' +
                '}';
    }
}
