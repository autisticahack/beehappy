package greybeard;

public class EventEndRequest {

    private String uuid;

    private String successfulOutcome;

    private String locationGps;

    private String isoDateTime;

    private String triggerDetails;

    public String getUuid() {
        return uuid;
    }

    public String getSuccessfulOutcome() {
        return successfulOutcome;
    }

    public String getLocationGps() {
        return locationGps;
    }

    public String getIsoDateTime() {
        return isoDateTime;
    }

    public String getTriggerDetails() {
        return triggerDetails;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public void setSuccessfulOutcome(String successfulOutcome) {
        this.successfulOutcome = successfulOutcome;
    }

    public void setLocationGps(String locationGps) {
        this.locationGps = locationGps;
    }

    public void setIsoDateTime(String isoDateTime) {
        this.isoDateTime = isoDateTime;
    }

    public void setTriggerDetails(String triggerDetails) {
        this.triggerDetails = triggerDetails;
    }

    @Override
    public String toString() {
        return "EventEndRequest{" +
                "uuid='" + uuid + '\'' +
                ", successfulOutcome='" + successfulOutcome + '\'' +
                ", locationGps='" + locationGps + '\'' +
                ", isoDateTime='" + isoDateTime + '\'' +
                ", triggerDetails='" + triggerDetails + '\'' +
                '}';
    }
}
