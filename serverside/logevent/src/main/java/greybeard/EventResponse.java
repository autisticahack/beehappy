package greybeard;

public class EventResponse {
    private String status;

    public EventResponse(String status) {
        this.status = status;
    }

    public EventResponse() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "EventResponse{" +
                "status='" + status + '\'' +
                '}';
    }
}
