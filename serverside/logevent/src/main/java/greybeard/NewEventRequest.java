package greybeard;

public class NewEventRequest {

    private String uuid;
    private String anxiousPerson;
    private String feeling;
    private String locationGps;
    private String isoDateTime;

    public String getUuid() {
        return uuid;
    }

    public String getAnxiousPerson() {
        return anxiousPerson;
    }

    public String getFeeling() {
        return feeling;
    }

    public String getLocationGps() {
        return locationGps;
    }

    public String getIsoDateTime() {
        return isoDateTime;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public void setAnxiousPerson(String anxiousPerson) {
        this.anxiousPerson = anxiousPerson;
    }

    public void setFeeling(String feeling) {
        this.feeling = feeling;
    }

    public void setLocationGps(String locationGps) {
        this.locationGps = locationGps;
    }

    public void setIsoDateTime(String isoDateTime) {
        this.isoDateTime = isoDateTime;
    }

    @Override
    public String toString() {
        return "NewEventRequest{" +
                "uuid='" + uuid + '\'' +
                ", anxiousPerson='" + anxiousPerson + '\'' +
                ", feeling='" + feeling + '\'' +
                ", locationGps='" + locationGps + '\'' +
                ", isoDateTime='" + isoDateTime + '\'' +
                '}';
    }
}

