package greybeard;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import greybeard.dynamo.EventDao;

public class AddRemedyLambda implements RequestHandler<AddRemedyRequest, EventResponse> {

    private EventDao dao = new EventDao();

    public EventResponse handleRequest(AddRemedyRequest input, Context context) {

        System.out.println(input);

        if (dao.addRemedy(input))
            return new EventResponse("ok");
        else
            return new EventResponse("failed");

    }
}
