package greybeard;

import com.amazonaws.services.lambda.invoke.LambdaFunction;

public interface NewEventService {
    @LambdaFunction(functionName="new-event")
    EventResponse newEvent(NewEventRequest newEvent);
}
