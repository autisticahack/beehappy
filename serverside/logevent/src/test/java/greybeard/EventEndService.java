package greybeard;

import com.amazonaws.services.lambda.invoke.LambdaFunction;

public interface EventEndService {
    @LambdaFunction(functionName="event-end")
    EventResponse endEvent(EventEndRequest e);
}
