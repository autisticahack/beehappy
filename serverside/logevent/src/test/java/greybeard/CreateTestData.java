package greybeard;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.lambda.AWSLambdaClient;
import com.amazonaws.services.lambda.invoke.LambdaInvokerFactory;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Random;
import java.util.UUID;

public class CreateTestData {
    public static void main(String[] args) {
        AWSLambdaClient lambda = new AWSLambdaClient();
        lambda.configureRegion(Regions.EU_CENTRAL_1);

        NewEventService svcNewEvent = LambdaInvokerFactory.build(NewEventService.class, lambda);
        AddBehaviourService svcAddBehaviour = LambdaInvokerFactory.build(AddBehaviourService.class, lambda);
        EventEndService svcEventEnd = LambdaInvokerFactory.build(EventEndService.class, lambda);
        RemedyService svcRemedy = LambdaInvokerFactory.build(RemedyService.class, lambda);

        for (int i = 0; i < 100; i++) {
            insertRandomStuff(svcNewEvent, svcAddBehaviour, svcEventEnd, svcRemedy);
        }
    }

    static private void insertRandomStuff(NewEventService svcNewEvent,
                                          AddBehaviourService svcAddBehaviour,
                                          EventEndService svcEventEnd,
                                          RemedyService svcRemedy) {
        String uuid = UUID.randomUUID().toString();

        try {
            NewEventRequest newEventRequest = new NewEventRequest();
            newEventRequest.setAnxiousPerson(randomPerson());
            newEventRequest.setFeeling(randomFeeling());
            newEventRequest.setIsoDateTime(randomUTC());
            newEventRequest.setLocationGps(randomGps());
            newEventRequest.setUuid(uuid);
            System.out.println(svcNewEvent.newEvent(newEventRequest));

            if (new Random().nextBoolean()) {
                AddBehaviourRequest addBehaviourRequest = new AddBehaviourRequest();
                addBehaviourRequest.setUuid(uuid);
                addBehaviourRequest.setLocationGps(randomGps());
                addBehaviourRequest.setIsoDateTime(randomUTC());
                addBehaviourRequest.setBehaviour(randomBehaviour());
                addBehaviourRequest.setNotes(randomNotes());
                System.out.println(svcAddBehaviour.addBehaviour(addBehaviourRequest));
            }

            int count = new Random().nextInt(2);
            for (int i = 0; i < count; i++) {
                AddRemedyRequest remedyRequest = new AddRemedyRequest();
                remedyRequest.setUuid(uuid);
                remedyRequest.setLocationGps(randomGps());
                remedyRequest.setIsoDateTime(randomUTC());
                remedyRequest.setNotes(randomNotes());
                remedyRequest.setRemedy(randomRemedy());
                svcRemedy.addRemedy(remedyRequest);
            }

            if (new Random().nextBoolean()) {
                EventEndRequest eventEndRequest = new EventEndRequest();
                eventEndRequest.setIsoDateTime(randomUTC());
                eventEndRequest.setLocationGps(randomGps());
                eventEndRequest.setUuid(uuid);
                eventEndRequest.setTriggerDetails(randomTrigger());
                eventEndRequest.setSuccessfulOutcome(randomOutcome());
                System.out.println(svcEventEnd.endEvent(eventEndRequest));
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    static private String randomFeeling() {
        String data[] = {
                "Happy", "Sad", "Cry", "Scared", "Irritated"
        };

        return data[new Random().nextInt(data.length)];
    }

    static private String randomBehaviour() {
        String data[] = {
                "Run away", "Talk with someone", "Do some thinking", "Go to sleep", "Laugh", "Scream", "Hurt myself"
        };

        return data[new Random().nextInt(data.length)];
    }

    static private String randomUTC() {
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.UK);
        Date date = new Date(new Date().getTime() + new Random().nextInt(1000000) - 1000000);
        return fmt.format(date);
    }

    static private String randomGps() {
        Random rand = new Random();
        float lat = (rand.nextFloat() * 0.40f) + 51.5074f - 0.2f;
        float lng = (rand.nextFloat() * 0.40f) + -0.1f - 0.2f;

        return String.format("%f,%f", lat, lng);
    }

    static private String randomPerson() {
        String data[] = {
                "Tom W", "Nick", "Tom G", "Harry", "Paul", "Pat", "Jim"
        };
        return data[new Random().nextInt(data.length)];
    }

    static private String randomNotes() {
        String data[] = {
                "Blah blah blah", "Hmmm hmmm hmmm", "What what what???"
        };
        return data[new Random().nextInt(data.length)];
    }

    static private String randomTrigger() {
        String data[] = {
                "Too many people around", "It was noisy", "Somebody shouted at me"
        };
        return data[new Random().nextInt(data.length)];
    }

    static private String randomOutcome() {
        return new Random().nextBoolean() ? "Yes" : "No";
    }

    static private String randomRemedy() {
        String data[] = {"Hold your breath", "Count to ten", "Go to happy place"};
        return data[new Random().nextInt(data.length)];
    }
}