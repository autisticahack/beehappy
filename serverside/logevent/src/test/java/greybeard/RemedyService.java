package greybeard;

import com.amazonaws.services.lambda.invoke.LambdaFunction;

public interface RemedyService {
    @LambdaFunction(functionName="add-remedy")
    EventResponse addRemedy(AddRemedyRequest remedyRequest);
}
