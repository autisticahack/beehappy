package greybeard;

import com.amazonaws.services.lambda.invoke.LambdaFunction;

public interface AddBehaviourService {
    @LambdaFunction(functionName="add-behaviour")
    EventResponse addBehaviour(AddBehaviourRequest r);
}
