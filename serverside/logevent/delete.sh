#!/usr/bin/env bash
aws lambda delete-function --function-name new-event
aws lambda delete-function --function-name add-remedy
aws lambda delete-function --function-name add-behaviour
aws lambda delete-function --function-name event-end
