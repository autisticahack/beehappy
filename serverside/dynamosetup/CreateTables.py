from __future__ import print_function  # Python 2/3 compatibility
import boto3

# dynamodb = boto3.resource('dynamodb', region_name='eu-central-1', endpoint_url="http://localhost:8000")
dynamodb = boto3.resource('dynamodb', region_name='eu-central-1', endpoint_url="https://dynamodb.eu-central-1.amazonaws.com" )

# table = dynamodb.create_table(
#     TableName='Event',
#     KeySchema=[
#         {
#             'AttributeName': 'uuid',
#             'KeyType': 'HASH'
#         }
#     ],
#     AttributeDefinitions=[
#         {
#             'AttributeName': 'uuid',
#             'AttributeType': 'S'
#         }
#     ],
#     ProvisionedThroughput={
#         'ReadCapacityUnits': 10,
#         'WriteCapacityUnits': 2
#     }
# )
#
# print("Table status:", table.table_status)

table = dynamodb.create_table(
    TableName='Remedy',
    KeySchema=[
        {
            'AttributeName': 'uuid',
            'KeyType': 'HASH'
        },
        {
            'AttributeName': 'when',
            'KeyType': 'RANGE'
        }

    ],
    AttributeDefinitions=[
        {
            'AttributeName': 'uuid',
            'AttributeType': 'S'
        },
        {
            'AttributeName': 'when',
            'AttributeType': 'S'
        }
    ],
    ProvisionedThroughput={
        'ReadCapacityUnits': 10,
        'WriteCapacityUnits': 2
    }
)

print("Table status:", table.table_status)


