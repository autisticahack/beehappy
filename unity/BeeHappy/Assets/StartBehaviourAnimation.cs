﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartBehaviourAnimation : MonoBehaviour {

	public Text BehaviourText;

	private Animator _anim;
	private string[] _triggers = new string[]{"DoRun", "DoTalk", "DoThink", "DoSleep", "DoLaugh", "DoScream", "DoHurt"};
	private string[] _triggerLabels = new string[]{"Run away", "Talk with someone", "Do some thinking", "Go to sleep", "Laugh", "Scream", "Hurt myself"};
	private int _triggerIndex = 0;


	void Start () {

		_anim = GetComponent<Animator>();
		//int standhappyhash = Animator.StringToHash("stand-happy");
		//anim = GetComponent<Animator>();
		//anim.SetTrigger(standhappyhash);
		MoveToState(_triggerIndex);

	}

	public void NextState()
	{

		_triggerIndex++;

		if (_triggerIndex == _triggers.Length)
		{
			_triggerIndex = 0;
		}
		MoveToState(_triggerIndex);
	}

	void MoveToState(int triggerIndex)
	{
		string triggerName = _triggers[triggerIndex];
		_anim.SetTrigger(triggerName);
		BehaviourText.text = _triggerLabels[triggerIndex];
	}
}
