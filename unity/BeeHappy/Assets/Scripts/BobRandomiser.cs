﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BobRandomiser : MonoBehaviour {



	// Use this for initialization
	void Start () {
	
		Animator a = this.GetComponent<Animator>();
		a.StartPlayback();
		a.playbackTime = Random.value;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
