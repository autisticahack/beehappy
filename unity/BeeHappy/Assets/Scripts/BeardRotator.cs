﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class BeardRotator : MonoBehaviour {

	public float RotationSpeed = 0.05f;
	public Text Title;

	private enum State
	{
		RotatingBeard,
		FadingInText,
		Done
	}

	private State m_BeardState;

	// Use this for initialization
	void Start () {
	
		m_BeardState = State.RotatingBeard;

		Title.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
	
		if (m_BeardState == State.RotatingBeard)
		{
			
			if (this.transform.eulerAngles.y <= 90.0f)
			{
				//Debug.Log("Angle " + this.transform.eulerAngles.y.ToString());
				transform.Rotate(transform.rotation.eulerAngles.x, Time.deltaTime * -RotationSpeed, transform.rotation.eulerAngles.z);
			}
			else
			{
				m_BeardState = State.FadingInText;
				Rigidbody rb = this.GetComponent<Rigidbody>();
				rb.useGravity = true;
			}
		}

		if (m_BeardState == State.FadingInText)
		{
			Title.enabled = true;
			m_BeardState = State.Done;
		}
	}

	public void DropBeard()
	{
		Rigidbody rb = this.GetComponent<Rigidbody>();
		rb.useGravity = true;
	}

	public void NewScene()
	{
		SceneManager.LoadScene("Main",LoadSceneMode.Single);
	}


}
