﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartAnimation : MonoBehaviour {

	public Text EmotionText;

	private Animator _anim;
	private string[] _triggers = new string[]{"DoHappy", "DoSad", "DoCry", "DoScared", "DoIrritated"};
	private int _triggerIndex = 0;


	void Start () {

		_anim = GetComponent<Animator>();
		//int standhappyhash = Animator.StringToHash("stand-happy");
		//anim = GetComponent<Animator>();
		//anim.SetTrigger(standhappyhash);
		MoveToState(_triggerIndex);

	}

	public void NextState()
	{

		_triggerIndex++;

		if (_triggerIndex == _triggers.Length)
		{
			_triggerIndex = 0;
		}
		MoveToState(_triggerIndex);
	}

	void MoveToState(int triggerIndex)
	{
		string triggerName = _triggers[triggerIndex];
		_anim.SetTrigger(triggerName);
		EmotionText.text = triggerName.Substring(2, triggerName.Length-2);
	}
}
