﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.CognitoSync.Model.ResourceConflictException
struct ResourceConflictException_t3114547969;
// System.String
struct String_t;
// System.Exception
struct Exception_t1927440687;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Exception1927440687.h"
#include "AWSSDK_Core_Amazon_Runtime_ErrorType1448377524.h"
#include "System_System_Net_HttpStatusCode1898409641.h"
#include "mscorlib_System_Runtime_Serialization_Serialization228987430.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"

// System.Void Amazon.CognitoSync.Model.ResourceConflictException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
extern "C"  void ResourceConflictException__ctor_m3609650239 (ResourceConflictException_t3114547969 * __this, String_t* ___message0, Exception_t1927440687 * ___innerException1, int32_t ___errorType2, String_t* ___errorCode3, String_t* ___requestId4, int32_t ___statusCode5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Model.ResourceConflictException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void ResourceConflictException__ctor_m3107773559 (ResourceConflictException_t3114547969 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
