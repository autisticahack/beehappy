﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.UnityWwwRequest
struct UnityWwwRequest_t118609659;
// System.Uri
struct Uri_t19570940;
// System.IDisposable
struct IDisposable_t2427283555;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.Threading.ManualResetEvent
struct ManualResetEvent_t926074657;
// Amazon.Runtime.Internal.Transform.IWebResponseData
struct IWebResponseData_t3815728244;
// System.Exception
struct Exception_t1927440687;
// System.String
struct String_t;
// Amazon.Runtime.IRequestContext
struct IRequestContext_t1620878341;
// System.Collections.Generic.IDictionary`2<System.String,System.String>
struct IDictionary_2_t1943082916;
// System.IO.Stream
struct Stream_t3255436806;
// System.Object
struct Il2CppObject;
// Amazon.Runtime.Internal.StreamReadTracker
struct StreamReadTracker_t1958363340;
// System.EventHandler`1<Amazon.Runtime.StreamTransferProgressArgs>
struct EventHandler_1_t1230945235;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Uri19570940.h"
#include "mscorlib_System_AsyncCallback163412349.h"
#include "mscorlib_System_Threading_ManualResetEvent926074657.h"
#include "mscorlib_System_Exception1927440687.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_IO_Stream3255436806.h"
#include "mscorlib_System_Object2689449295.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_StreamReadTrac1958363340.h"

// System.Uri Amazon.Runtime.Internal.UnityWwwRequest::get_RequestUri()
extern "C"  Uri_t19570940 * UnityWwwRequest_get_RequestUri_m3878602773 (UnityWwwRequest_t118609659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.UnityWwwRequest::set_RequestUri(System.Uri)
extern "C"  void UnityWwwRequest_set_RequestUri_m1646521722 (UnityWwwRequest_t118609659 * __this, Uri_t19570940 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.UnityWwwRequest::set_WwwRequest(System.IDisposable)
extern "C"  void UnityWwwRequest_set_WwwRequest_m695010062 (UnityWwwRequest_t118609659 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Amazon.Runtime.Internal.UnityWwwRequest::get_RequestContent()
extern "C"  ByteU5BU5D_t3397334013* UnityWwwRequest_get_RequestContent_m875578554 (UnityWwwRequest_t118609659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.UnityWwwRequest::set_RequestContent(System.Byte[])
extern "C"  void UnityWwwRequest_set_RequestContent_m2754054299 (UnityWwwRequest_t118609659 * __this, ByteU5BU5D_t3397334013* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> Amazon.Runtime.Internal.UnityWwwRequest::get_Headers()
extern "C"  Dictionary_2_t3943999495 * UnityWwwRequest_get_Headers_m2969623874 (UnityWwwRequest_t118609659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.UnityWwwRequest::set_Headers(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void UnityWwwRequest_set_Headers_m1462302275 (UnityWwwRequest_t118609659 * __this, Dictionary_2_t3943999495 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.AsyncCallback Amazon.Runtime.Internal.UnityWwwRequest::get_Callback()
extern "C"  AsyncCallback_t163412349 * UnityWwwRequest_get_Callback_m1187439854 (UnityWwwRequest_t118609659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.UnityWwwRequest::set_Callback(System.AsyncCallback)
extern "C"  void UnityWwwRequest_set_Callback_m3002196425 (UnityWwwRequest_t118609659 * __this, AsyncCallback_t163412349 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Amazon.Runtime.Internal.UnityWwwRequest::get_AsyncResult()
extern "C"  Il2CppObject * UnityWwwRequest_get_AsyncResult_m2113714793 (UnityWwwRequest_t118609659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.UnityWwwRequest::set_AsyncResult(System.IAsyncResult)
extern "C"  void UnityWwwRequest_set_AsyncResult_m4272204888 (UnityWwwRequest_t118609659 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.ManualResetEvent Amazon.Runtime.Internal.UnityWwwRequest::get_WaitHandle()
extern "C"  ManualResetEvent_t926074657 * UnityWwwRequest_get_WaitHandle_m3796419424 (UnityWwwRequest_t118609659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.UnityWwwRequest::set_WaitHandle(System.Threading.ManualResetEvent)
extern "C"  void UnityWwwRequest_set_WaitHandle_m3446975985 (UnityWwwRequest_t118609659 * __this, ManualResetEvent_t926074657 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.UnityWwwRequest::get_IsSync()
extern "C"  bool UnityWwwRequest_get_IsSync_m1136735365 (UnityWwwRequest_t118609659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.UnityWwwRequest::set_IsSync(System.Boolean)
extern "C"  void UnityWwwRequest_set_IsSync_m1958192226 (UnityWwwRequest_t118609659 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.Internal.Transform.IWebResponseData Amazon.Runtime.Internal.UnityWwwRequest::get_Response()
extern "C"  Il2CppObject * UnityWwwRequest_get_Response_m1743981495 (UnityWwwRequest_t118609659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.UnityWwwRequest::set_Response(Amazon.Runtime.Internal.Transform.IWebResponseData)
extern "C"  void UnityWwwRequest_set_Response_m1004816742 (UnityWwwRequest_t118609659 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception Amazon.Runtime.Internal.UnityWwwRequest::get_Exception()
extern "C"  Exception_t1927440687 * UnityWwwRequest_get_Exception_m2738944626 (UnityWwwRequest_t118609659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.UnityWwwRequest::set_Exception(System.Exception)
extern "C"  void UnityWwwRequest_set_Exception_m2823187919 (UnityWwwRequest_t118609659 * __this, Exception_t1927440687 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.UnityWwwRequest::set_Method(System.String)
extern "C"  void UnityWwwRequest_set_Method_m90795337 (UnityWwwRequest_t118609659 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.UnityWwwRequest::.ctor(System.Uri)
extern "C"  void UnityWwwRequest__ctor_m554514830 (UnityWwwRequest_t118609659 * __this, Uri_t19570940 * ___requestUri0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.UnityWwwRequest::ConfigureRequest(Amazon.Runtime.IRequestContext)
extern "C"  void UnityWwwRequest_ConfigureRequest_m1753877721 (UnityWwwRequest_t118609659 * __this, Il2CppObject * ___requestContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.UnityWwwRequest::SetRequestHeaders(System.Collections.Generic.IDictionary`2<System.String,System.String>)
extern "C"  void UnityWwwRequest_SetRequestHeaders_m753659638 (UnityWwwRequest_t118609659 * __this, Il2CppObject* ___headers0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Runtime.Internal.UnityWwwRequest::GetRequestContent()
extern "C"  String_t* UnityWwwRequest_GetRequestContent_m947811794 (UnityWwwRequest_t118609659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.Internal.Transform.IWebResponseData Amazon.Runtime.Internal.UnityWwwRequest::GetResponse()
extern "C"  Il2CppObject * UnityWwwRequest_GetResponse_m2158265192 (UnityWwwRequest_t118609659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.UnityWwwRequest::WriteToRequestBody(System.String,System.IO.Stream,System.Collections.Generic.IDictionary`2<System.String,System.String>,Amazon.Runtime.IRequestContext)
extern "C"  void UnityWwwRequest_WriteToRequestBody_m659975560 (UnityWwwRequest_t118609659 * __this, String_t* ___requestContent0, Stream_t3255436806 * ___contentStream1, Il2CppObject* ___contentHeaders2, Il2CppObject * ___requestContext3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.UnityWwwRequest::WriteToRequestBody(System.String,System.Byte[],System.Collections.Generic.IDictionary`2<System.String,System.String>)
extern "C"  void UnityWwwRequest_WriteToRequestBody_m1235365709 (UnityWwwRequest_t118609659 * __this, String_t* ___requestContent0, ByteU5BU5D_t3397334013* ___content1, Il2CppObject* ___contentHeaders2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Amazon.Runtime.Internal.UnityWwwRequest::BeginGetRequestContent(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnityWwwRequest_BeginGetRequestContent_m3033961552 (UnityWwwRequest_t118609659 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___state1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Runtime.Internal.UnityWwwRequest::EndGetRequestContent(System.IAsyncResult)
extern "C"  String_t* UnityWwwRequest_EndGetRequestContent_m1613600672 (UnityWwwRequest_t118609659 * __this, Il2CppObject * ___asyncResult0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Amazon.Runtime.Internal.UnityWwwRequest::BeginGetResponse(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnityWwwRequest_BeginGetResponse_m2545831987 (UnityWwwRequest_t118609659 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___state1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.Internal.Transform.IWebResponseData Amazon.Runtime.Internal.UnityWwwRequest::EndGetResponse(System.IAsyncResult)
extern "C"  Il2CppObject * UnityWwwRequest_EndGetResponse_m1670004084 (UnityWwwRequest_t118609659 * __this, Il2CppObject * ___asyncResult0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.Internal.StreamReadTracker Amazon.Runtime.Internal.UnityWwwRequest::get_Tracker()
extern "C"  StreamReadTracker_t1958363340 * UnityWwwRequest_get_Tracker_m4206324222 (UnityWwwRequest_t118609659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.UnityWwwRequest::set_Tracker(Amazon.Runtime.Internal.StreamReadTracker)
extern "C"  void UnityWwwRequest_set_Tracker_m1879836257 (UnityWwwRequest_t118609659 * __this, StreamReadTracker_t1958363340 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream Amazon.Runtime.Internal.UnityWwwRequest::SetupProgressListeners(System.IO.Stream,System.Int64,System.Object,System.EventHandler`1<Amazon.Runtime.StreamTransferProgressArgs>)
extern "C"  Stream_t3255436806 * UnityWwwRequest_SetupProgressListeners_m850553078 (UnityWwwRequest_t118609659 * __this, Stream_t3255436806 * ___originalStream0, int64_t ___progressUpdateInterval1, Il2CppObject * ___sender2, EventHandler_1_t1230945235 * ___callback3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.UnityWwwRequest::OnUploadProgressChanged(System.Single)
extern "C"  void UnityWwwRequest_OnUploadProgressChanged_m4196248977 (UnityWwwRequest_t118609659 * __this, float ___progress0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.UnityWwwRequest::Dispose()
extern "C"  void UnityWwwRequest_Dispose_m1926145872 (UnityWwwRequest_t118609659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.UnityWwwRequest::Dispose(System.Boolean)
extern "C"  void UnityWwwRequest_Dispose_m4000176337 (UnityWwwRequest_t118609659 * __this, bool ___disposing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.UnityWwwRequest::<Dispose>b__64_0()
extern "C"  void UnityWwwRequest_U3CDisposeU3Eb__64_0_m324096891 (UnityWwwRequest_t118609659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
