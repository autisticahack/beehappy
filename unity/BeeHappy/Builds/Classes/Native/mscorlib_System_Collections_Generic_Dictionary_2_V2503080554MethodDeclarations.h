﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va984569266MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m301352771(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t2503080554 *, Dictionary_2_t3800020711 *, const MethodInfo*))ValueCollection__ctor_m1801851342_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3748354297(__this, ___item0, method) ((  void (*) (ValueCollection_t2503080554 *, RegionEndpoint_t1885241449 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1477647540_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1108818402(__this, method) ((  void (*) (ValueCollection_t2503080554 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m573646175_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2143125383(__this, ___item0, method) ((  bool (*) (ValueCollection_t2503080554 *, RegionEndpoint_t1885241449 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1598273024_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2528937408(__this, ___item0, method) ((  bool (*) (ValueCollection_t2503080554 *, RegionEndpoint_t1885241449 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3764375695_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3435328482(__this, method) ((  Il2CppObject* (*) (ValueCollection_t2503080554 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3036711881_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m65615986(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2503080554 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m3792551117_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1943478053(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2503080554 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1773104428_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m949922284(__this, method) ((  bool (*) (ValueCollection_t2503080554 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1530798787_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1990596146(__this, method) ((  bool (*) (ValueCollection_t2503080554 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3044620153_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m223843394(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2503080554 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m919209341_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m1966845130(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2503080554 *, RegionEndpointU5BU5D_t3500428244*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m927881183_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint>::GetEnumerator()
#define ValueCollection_GetEnumerator_m3625776847(__this, method) ((  Enumerator_t1191586179  (*) (ValueCollection_t2503080554 *, const MethodInfo*))ValueCollection_GetEnumerator_m401908452_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint>::get_Count()
#define ValueCollection_get_Count_m4141918834(__this, method) ((  int32_t (*) (ValueCollection_t2503080554 *, const MethodInfo*))ValueCollection_get_Count_m3718352161_gshared)(__this, method)
