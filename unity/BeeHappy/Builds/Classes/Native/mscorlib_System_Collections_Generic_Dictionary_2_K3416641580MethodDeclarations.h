﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>
struct Dictionary_2_t727138142;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3416641580.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m860887073_gshared (Enumerator_t3416641580 * __this, Dictionary_2_t727138142 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m860887073(__this, ___host0, method) ((  void (*) (Enumerator_t3416641580 *, Dictionary_2_t727138142 *, const MethodInfo*))Enumerator__ctor_m860887073_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m945475828_gshared (Enumerator_t3416641580 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m945475828(__this, method) ((  Il2CppObject * (*) (Enumerator_t3416641580 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m945475828_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m436067500_gshared (Enumerator_t3416641580 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m436067500(__this, method) ((  void (*) (Enumerator_t3416641580 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m436067500_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::Dispose()
extern "C"  void Enumerator_Dispose_m1760329057_gshared (Enumerator_t3416641580 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1760329057(__this, method) ((  void (*) (Enumerator_t3416641580 *, const MethodInfo*))Enumerator_Dispose_m1760329057_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m659395664_gshared (Enumerator_t3416641580 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m659395664(__this, method) ((  bool (*) (Enumerator_t3416641580 *, const MethodInfo*))Enumerator_MoveNext_m659395664_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m927196420_gshared (Enumerator_t3416641580 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m927196420(__this, method) ((  Il2CppObject * (*) (Enumerator_t3416641580 *, const MethodInfo*))Enumerator_get_Current_m927196420_gshared)(__this, method)
