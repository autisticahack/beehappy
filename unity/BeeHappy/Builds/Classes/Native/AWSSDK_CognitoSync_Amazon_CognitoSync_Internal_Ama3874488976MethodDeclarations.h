﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.CognitoSync.Internal.AmazonCognitoSyncPostMarshallHandler
struct AmazonCognitoSyncPostMarshallHandler_t3874488976;
// Amazon.Runtime.IExecutionContext
struct IExecutionContext_t2477130752;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// Amazon.Runtime.IAsyncExecutionContext
struct IAsyncExecutionContext_t3792344986;

#include "codegen/il2cpp-codegen.h"

// System.Void Amazon.CognitoSync.Internal.AmazonCognitoSyncPostMarshallHandler::InvokeSync(Amazon.Runtime.IExecutionContext)
extern "C"  void AmazonCognitoSyncPostMarshallHandler_InvokeSync_m2514101944 (AmazonCognitoSyncPostMarshallHandler_t3874488976 * __this, Il2CppObject * ___executionContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Amazon.CognitoSync.Internal.AmazonCognitoSyncPostMarshallHandler::InvokeAsync(Amazon.Runtime.IAsyncExecutionContext)
extern "C"  Il2CppObject * AmazonCognitoSyncPostMarshallHandler_InvokeAsync_m2858079801 (AmazonCognitoSyncPostMarshallHandler_t3874488976 * __this, Il2CppObject * ___executionContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Internal.AmazonCognitoSyncPostMarshallHandler::PreInvoke(Amazon.Runtime.IExecutionContext)
extern "C"  void AmazonCognitoSyncPostMarshallHandler_PreInvoke_m3931594658 (AmazonCognitoSyncPostMarshallHandler_t3874488976 * __this, Il2CppObject * ___executionContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Internal.AmazonCognitoSyncPostMarshallHandler::ProcessRequestHandlers(Amazon.Runtime.IExecutionContext)
extern "C"  void AmazonCognitoSyncPostMarshallHandler_ProcessRequestHandlers_m1425016620 (AmazonCognitoSyncPostMarshallHandler_t3874488976 * __this, Il2CppObject * ___executionContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Internal.AmazonCognitoSyncPostMarshallHandler::.ctor()
extern "C"  void AmazonCognitoSyncPostMarshallHandler__ctor_m2736500229 (AmazonCognitoSyncPostMarshallHandler_t3874488976 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
