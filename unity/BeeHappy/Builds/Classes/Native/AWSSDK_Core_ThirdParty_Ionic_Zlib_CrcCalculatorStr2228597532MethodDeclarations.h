﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ThirdParty.Ionic.Zlib.CrcCalculatorStream
struct CrcCalculatorStream_t2228597532;
// System.IO.Stream
struct Stream_t3255436806;
// System.Byte[]
struct ByteU5BU5D_t3397334013;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_Stream3255436806.h"
#include "mscorlib_System_IO_SeekOrigin2475945306.h"

// System.Void ThirdParty.Ionic.Zlib.CrcCalculatorStream::.ctor(System.IO.Stream,System.Int64)
extern "C"  void CrcCalculatorStream__ctor_m2231557300 (CrcCalculatorStream_t2228597532 * __this, Stream_t3255436806 * ___stream0, int64_t ___length1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ThirdParty.Ionic.Zlib.CrcCalculatorStream::get_Crc32()
extern "C"  int32_t CrcCalculatorStream_get_Crc32_m1702054645 (CrcCalculatorStream_t2228597532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ThirdParty.Ionic.Zlib.CrcCalculatorStream::Read(System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t CrcCalculatorStream_Read_m1317846680 (CrcCalculatorStream_t2228597532 * __this, ByteU5BU5D_t3397334013* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Ionic.Zlib.CrcCalculatorStream::Write(System.Byte[],System.Int32,System.Int32)
extern "C"  void CrcCalculatorStream_Write_m2357956007 (CrcCalculatorStream_t2228597532 * __this, ByteU5BU5D_t3397334013* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ThirdParty.Ionic.Zlib.CrcCalculatorStream::get_CanRead()
extern "C"  bool CrcCalculatorStream_get_CanRead_m592182550 (CrcCalculatorStream_t2228597532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ThirdParty.Ionic.Zlib.CrcCalculatorStream::get_CanSeek()
extern "C"  bool CrcCalculatorStream_get_CanSeek_m1520227510 (CrcCalculatorStream_t2228597532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ThirdParty.Ionic.Zlib.CrcCalculatorStream::get_CanWrite()
extern "C"  bool CrcCalculatorStream_get_CanWrite_m1998127089 (CrcCalculatorStream_t2228597532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Ionic.Zlib.CrcCalculatorStream::Flush()
extern "C"  void CrcCalculatorStream_Flush_m4227425037 (CrcCalculatorStream_t2228597532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ThirdParty.Ionic.Zlib.CrcCalculatorStream::get_Length()
extern "C"  int64_t CrcCalculatorStream_get_Length_m3816813393 (CrcCalculatorStream_t2228597532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ThirdParty.Ionic.Zlib.CrcCalculatorStream::get_Position()
extern "C"  int64_t CrcCalculatorStream_get_Position_m3084305182 (CrcCalculatorStream_t2228597532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Ionic.Zlib.CrcCalculatorStream::set_Position(System.Int64)
extern "C"  void CrcCalculatorStream_set_Position_m3370163459 (CrcCalculatorStream_t2228597532 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ThirdParty.Ionic.Zlib.CrcCalculatorStream::Seek(System.Int64,System.IO.SeekOrigin)
extern "C"  int64_t CrcCalculatorStream_Seek_m3609773193 (CrcCalculatorStream_t2228597532 * __this, int64_t ___offset0, int32_t ___origin1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Ionic.Zlib.CrcCalculatorStream::SetLength(System.Int64)
extern "C"  void CrcCalculatorStream_SetLength_m3471967417 (CrcCalculatorStream_t2228597532 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
