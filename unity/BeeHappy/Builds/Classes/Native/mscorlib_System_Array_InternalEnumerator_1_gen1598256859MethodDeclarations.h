﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1598256859.h"
#include "mscorlib_System_Array3829468939.h"
#include "System_Xml_System_Xml_XmlNodeType739504597.h"

// System.Void System.Array/InternalEnumerator`1<System.Xml.XmlNodeType>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1972732386_gshared (InternalEnumerator_1_t1598256859 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1972732386(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1598256859 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1972732386_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Xml.XmlNodeType>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m439242922_gshared (InternalEnumerator_1_t1598256859 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m439242922(__this, method) ((  void (*) (InternalEnumerator_1_t1598256859 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m439242922_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Xml.XmlNodeType>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m128604886_gshared (InternalEnumerator_1_t1598256859 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m128604886(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1598256859 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m128604886_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Xml.XmlNodeType>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m836947227_gshared (InternalEnumerator_1_t1598256859 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m836947227(__this, method) ((  void (*) (InternalEnumerator_1_t1598256859 *, const MethodInfo*))InternalEnumerator_1_Dispose_m836947227_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Xml.XmlNodeType>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m131387450_gshared (InternalEnumerator_1_t1598256859 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m131387450(__this, method) ((  bool (*) (InternalEnumerator_1_t1598256859 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m131387450_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Xml.XmlNodeType>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m160651067_gshared (InternalEnumerator_1_t1598256859 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m160651067(__this, method) ((  int32_t (*) (InternalEnumerator_1_t1598256859 *, const MethodInfo*))InternalEnumerator_1_get_Current_m160651067_gshared)(__this, method)
