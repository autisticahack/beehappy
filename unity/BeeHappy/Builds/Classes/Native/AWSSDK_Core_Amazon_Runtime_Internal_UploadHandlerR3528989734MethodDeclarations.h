﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.UploadHandlerRawWrapper
struct UploadHandlerRawWrapper_t3528989734;
// System.Object
struct Il2CppObject;
// System.Byte[]
struct ByteU5BU5D_t3397334013;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Object Amazon.Runtime.Internal.UploadHandlerRawWrapper::get_Instance()
extern "C"  Il2CppObject * UploadHandlerRawWrapper_get_Instance_m3086720269 (UploadHandlerRawWrapper_t3528989734 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.UploadHandlerRawWrapper::set_Instance(System.Object)
extern "C"  void UploadHandlerRawWrapper_set_Instance_m3474588736 (UploadHandlerRawWrapper_t3528989734 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.UploadHandlerRawWrapper::.cctor()
extern "C"  void UploadHandlerRawWrapper__cctor_m3410822739 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.UploadHandlerRawWrapper::.ctor(System.Byte[])
extern "C"  void UploadHandlerRawWrapper__ctor_m2623689391 (UploadHandlerRawWrapper_t3528989734 * __this, ByteU5BU5D_t3397334013* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.UploadHandlerRawWrapper::Dispose(System.Boolean)
extern "C"  void UploadHandlerRawWrapper_Dispose_m1509789060 (UploadHandlerRawWrapper_t3528989734 * __this, bool ___disposing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.UploadHandlerRawWrapper::Dispose()
extern "C"  void UploadHandlerRawWrapper_Dispose_m3901901339 (UploadHandlerRawWrapper_t3528989734 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
