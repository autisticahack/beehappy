﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AccelBrain.MonauralBeatController
struct MonauralBeatController_t1412419075;
// AccelBrain.BrainBeat
struct BrainBeat_t95439410;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AccelBrain_BrainBeat95439410.h"

// System.Void AccelBrain.MonauralBeatController::.ctor()
extern "C"  void MonauralBeatController__ctor_m1626198532 (MonauralBeatController_t1412419075 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AccelBrain.MonauralBeatController::set__brainBeat(AccelBrain.BrainBeat)
extern "C"  void MonauralBeatController_set__brainBeat_m2187640370 (MonauralBeatController_t1412419075 * __this, BrainBeat_t95439410 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AccelBrain.BrainBeat AccelBrain.MonauralBeatController::get__brainBeat()
extern "C"  BrainBeat_t95439410 * MonauralBeatController_get__brainBeat_m3950529715 (MonauralBeatController_t1412419075 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AccelBrain.MonauralBeatController::Start()
extern "C"  void MonauralBeatController_Start_m818450336 (MonauralBeatController_t1412419075 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
