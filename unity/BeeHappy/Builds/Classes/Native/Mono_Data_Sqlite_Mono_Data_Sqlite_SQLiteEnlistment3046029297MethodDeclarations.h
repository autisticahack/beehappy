﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Data.Sqlite.SQLiteEnlistment
struct SQLiteEnlistment_t3046029297;
// Mono.Data.Sqlite.SqliteConnection
struct SqliteConnection_t1726730022;
// System.Transactions.Transaction
struct Transaction_t869361102;
// System.Transactions.Enlistment
struct Enlistment_t1750074243;
// System.Transactions.PreparingEnlistment
struct PreparingEnlistment_t2112014291;

#include "codegen/il2cpp-codegen.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteConnection1726730022.h"
#include "System_Transactions_System_Transactions_Transaction869361102.h"
#include "System_Transactions_System_Transactions_Enlistment1750074243.h"
#include "System_Transactions_System_Transactions_PreparingE2112014291.h"

// System.Void Mono.Data.Sqlite.SQLiteEnlistment::.ctor(Mono.Data.Sqlite.SqliteConnection,System.Transactions.Transaction)
extern "C"  void SQLiteEnlistment__ctor_m729024045 (SQLiteEnlistment_t3046029297 * __this, SqliteConnection_t1726730022 * ___cnn0, Transaction_t869361102 * ___scope1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SQLiteEnlistment::Cleanup(Mono.Data.Sqlite.SqliteConnection)
extern "C"  void SQLiteEnlistment_Cleanup_m3944030215 (SQLiteEnlistment_t3046029297 * __this, SqliteConnection_t1726730022 * ___cnn0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SQLiteEnlistment::Commit(System.Transactions.Enlistment)
extern "C"  void SQLiteEnlistment_Commit_m503740594 (SQLiteEnlistment_t3046029297 * __this, Enlistment_t1750074243 * ___enlistment0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SQLiteEnlistment::Prepare(System.Transactions.PreparingEnlistment)
extern "C"  void SQLiteEnlistment_Prepare_m2736668954 (SQLiteEnlistment_t3046029297 * __this, PreparingEnlistment_t2112014291 * ___preparingEnlistment0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SQLiteEnlistment::Rollback(System.Transactions.Enlistment)
extern "C"  void SQLiteEnlistment_Rollback_m762837319 (SQLiteEnlistment_t3046029297 * __this, Enlistment_t1750074243 * ___enlistment0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
