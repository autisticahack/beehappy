﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_637145336.h"
#include "mscorlib_System_Object2689449295.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_PropertyMetada3287739986.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,ThirdParty.Json.LitJson.PropertyMetadata>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3823779791_gshared (KeyValuePair_2_t637145336 * __this, Il2CppObject * ___key0, PropertyMetadata_t3287739986  ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m3823779791(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t637145336 *, Il2CppObject *, PropertyMetadata_t3287739986 , const MethodInfo*))KeyValuePair_2__ctor_m3823779791_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,ThirdParty.Json.LitJson.PropertyMetadata>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m4231504425_gshared (KeyValuePair_2_t637145336 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m4231504425(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t637145336 *, const MethodInfo*))KeyValuePair_2_get_Key_m4231504425_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,ThirdParty.Json.LitJson.PropertyMetadata>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m298860204_gshared (KeyValuePair_2_t637145336 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m298860204(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t637145336 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Key_m298860204_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,ThirdParty.Json.LitJson.PropertyMetadata>::get_Value()
extern "C"  PropertyMetadata_t3287739986  KeyValuePair_2_get_Value_m4200112345_gshared (KeyValuePair_2_t637145336 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m4200112345(__this, method) ((  PropertyMetadata_t3287739986  (*) (KeyValuePair_2_t637145336 *, const MethodInfo*))KeyValuePair_2_get_Value_m4200112345_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,ThirdParty.Json.LitJson.PropertyMetadata>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m875108684_gshared (KeyValuePair_2_t637145336 * __this, PropertyMetadata_t3287739986  ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m875108684(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t637145336 *, PropertyMetadata_t3287739986 , const MethodInfo*))KeyValuePair_2_set_Value_m875108684_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,ThirdParty.Json.LitJson.PropertyMetadata>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m1871063070_gshared (KeyValuePair_2_t637145336 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m1871063070(__this, method) ((  String_t* (*) (KeyValuePair_2_t637145336 *, const MethodInfo*))KeyValuePair_2_ToString_m1871063070_gshared)(__this, method)
