﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.Util.ThreadPoolThrottler`1<System.Object>
struct ThreadPoolThrottler_1_t3928650719;
// System.Object
struct Il2CppObject;
// System.Action`1<System.Object>
struct Action_1_t2491248677;
// System.Action`2<System.Exception,System.Object>
struct Action_2_t738274861;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Int32 Amazon.Runtime.Internal.Util.ThreadPoolThrottler`1<System.Object>::get_MaxConcurentRequest()
extern "C"  int32_t ThreadPoolThrottler_1_get_MaxConcurentRequest_m2980732324_gshared (ThreadPoolThrottler_1_t3928650719 * __this, const MethodInfo* method);
#define ThreadPoolThrottler_1_get_MaxConcurentRequest_m2980732324(__this, method) ((  int32_t (*) (ThreadPoolThrottler_1_t3928650719 *, const MethodInfo*))ThreadPoolThrottler_1_get_MaxConcurentRequest_m2980732324_gshared)(__this, method)
// System.Void Amazon.Runtime.Internal.Util.ThreadPoolThrottler`1<System.Object>::set_MaxConcurentRequest(System.Int32)
extern "C"  void ThreadPoolThrottler_1_set_MaxConcurentRequest_m267262387_gshared (ThreadPoolThrottler_1_t3928650719 * __this, int32_t ___value0, const MethodInfo* method);
#define ThreadPoolThrottler_1_set_MaxConcurentRequest_m267262387(__this, ___value0, method) ((  void (*) (ThreadPoolThrottler_1_t3928650719 *, int32_t, const MethodInfo*))ThreadPoolThrottler_1_set_MaxConcurentRequest_m267262387_gshared)(__this, ___value0, method)
// System.Void Amazon.Runtime.Internal.Util.ThreadPoolThrottler`1<System.Object>::.ctor(System.Int32)
extern "C"  void ThreadPoolThrottler_1__ctor_m378429414_gshared (ThreadPoolThrottler_1_t3928650719 * __this, int32_t ___maxConcurrentRequests0, const MethodInfo* method);
#define ThreadPoolThrottler_1__ctor_m378429414(__this, ___maxConcurrentRequests0, method) ((  void (*) (ThreadPoolThrottler_1_t3928650719 *, int32_t, const MethodInfo*))ThreadPoolThrottler_1__ctor_m378429414_gshared)(__this, ___maxConcurrentRequests0, method)
// System.Void Amazon.Runtime.Internal.Util.ThreadPoolThrottler`1<System.Object>::Enqueue(T,System.Action`1<T>,System.Action`2<System.Exception,T>)
extern "C"  void ThreadPoolThrottler_1_Enqueue_m4160389796_gshared (ThreadPoolThrottler_1_t3928650719 * __this, Il2CppObject * ___executionContext0, Action_1_t2491248677 * ___callback1, Action_2_t738274861 * ___errorCallback2, const MethodInfo* method);
#define ThreadPoolThrottler_1_Enqueue_m4160389796(__this, ___executionContext0, ___callback1, ___errorCallback2, method) ((  void (*) (ThreadPoolThrottler_1_t3928650719 *, Il2CppObject *, Action_1_t2491248677 *, Action_2_t738274861 *, const MethodInfo*))ThreadPoolThrottler_1_Enqueue_m4160389796_gshared)(__this, ___executionContext0, ___callback1, ___errorCallback2, method)
// System.Void Amazon.Runtime.Internal.Util.ThreadPoolThrottler`1<System.Object>::Callback(System.Object)
extern "C"  void ThreadPoolThrottler_1_Callback_m3696307736_gshared (ThreadPoolThrottler_1_t3928650719 * __this, Il2CppObject * ___state0, const MethodInfo* method);
#define ThreadPoolThrottler_1_Callback_m3696307736(__this, ___state0, method) ((  void (*) (ThreadPoolThrottler_1_t3928650719 *, Il2CppObject *, const MethodInfo*))ThreadPoolThrottler_1_Callback_m3696307736_gshared)(__this, ___state0, method)
// System.Void Amazon.Runtime.Internal.Util.ThreadPoolThrottler`1<System.Object>::SignalCompletion()
extern "C"  void ThreadPoolThrottler_1_SignalCompletion_m3971342417_gshared (ThreadPoolThrottler_1_t3928650719 * __this, const MethodInfo* method);
#define ThreadPoolThrottler_1_SignalCompletion_m3971342417(__this, method) ((  void (*) (ThreadPoolThrottler_1_t3928650719 *, const MethodInfo*))ThreadPoolThrottler_1_SignalCompletion_m3971342417_gshared)(__this, method)
// System.Void Amazon.Runtime.Internal.Util.ThreadPoolThrottler`1<System.Object>::.cctor()
extern "C"  void ThreadPoolThrottler_1__cctor_m1038399746_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define ThreadPoolThrottler_1__cctor_m1038399746(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ThreadPoolThrottler_1__cctor_m1038399746_gshared)(__this /* static, unused */, method)
