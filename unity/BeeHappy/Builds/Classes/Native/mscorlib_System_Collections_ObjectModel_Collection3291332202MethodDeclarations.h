﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>
struct Collection_1_t3291332202;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>[]
struct KeyValuePair_2U5BU5D_t598529833;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>
struct IEnumerator_1_t1225111275;
// System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>
struct IList_1_t4290528049;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23749587448.h"

// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::.ctor()
extern "C"  void Collection_1__ctor_m2124686903_gshared (Collection_1_t3291332202 * __this, const MethodInfo* method);
#define Collection_1__ctor_m2124686903(__this, method) ((  void (*) (Collection_1_t3291332202 *, const MethodInfo*))Collection_1__ctor_m2124686903_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m926915458_gshared (Collection_1_t3291332202 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m926915458(__this, method) ((  bool (*) (Collection_1_t3291332202 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m926915458_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m2089394683_gshared (Collection_1_t3291332202 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m2089394683(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t3291332202 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m2089394683_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m3977142854_gshared (Collection_1_t3291332202 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m3977142854(__this, method) ((  Il2CppObject * (*) (Collection_1_t3291332202 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m3977142854_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m2667620235_gshared (Collection_1_t3291332202 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m2667620235(__this, ___value0, method) ((  int32_t (*) (Collection_1_t3291332202 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m2667620235_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m723093395_gshared (Collection_1_t3291332202 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m723093395(__this, ___value0, method) ((  bool (*) (Collection_1_t3291332202 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m723093395_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m4222642065_gshared (Collection_1_t3291332202 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m4222642065(__this, ___value0, method) ((  int32_t (*) (Collection_1_t3291332202 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m4222642065_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m559709476_gshared (Collection_1_t3291332202 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m559709476(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3291332202 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m559709476_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m3952501250_gshared (Collection_1_t3291332202 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m3952501250(__this, ___value0, method) ((  void (*) (Collection_1_t3291332202 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m3952501250_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m2564403843_gshared (Collection_1_t3291332202 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m2564403843(__this, method) ((  bool (*) (Collection_1_t3291332202 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m2564403843_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m2731032187_gshared (Collection_1_t3291332202 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m2731032187(__this, method) ((  Il2CppObject * (*) (Collection_1_t3291332202 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m2731032187_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m1569309512_gshared (Collection_1_t3291332202 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m1569309512(__this, method) ((  bool (*) (Collection_1_t3291332202 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m1569309512_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m1472040703_gshared (Collection_1_t3291332202 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m1472040703(__this, method) ((  bool (*) (Collection_1_t3291332202 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m1472040703_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m2179250244_gshared (Collection_1_t3291332202 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m2179250244(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t3291332202 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m2179250244_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m3647769745_gshared (Collection_1_t3291332202 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m3647769745(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3291332202 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m3647769745_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::Add(T)
extern "C"  void Collection_1_Add_m1506728360_gshared (Collection_1_t3291332202 * __this, KeyValuePair_2_t3749587448  ___item0, const MethodInfo* method);
#define Collection_1_Add_m1506728360(__this, ___item0, method) ((  void (*) (Collection_1_t3291332202 *, KeyValuePair_2_t3749587448 , const MethodInfo*))Collection_1_Add_m1506728360_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::Clear()
extern "C"  void Collection_1_Clear_m2398625620_gshared (Collection_1_t3291332202 * __this, const MethodInfo* method);
#define Collection_1_Clear_m2398625620(__this, method) ((  void (*) (Collection_1_t3291332202 *, const MethodInfo*))Collection_1_Clear_m2398625620_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::ClearItems()
extern "C"  void Collection_1_ClearItems_m361306314_gshared (Collection_1_t3291332202 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m361306314(__this, method) ((  void (*) (Collection_1_t3291332202 *, const MethodInfo*))Collection_1_ClearItems_m361306314_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::Contains(T)
extern "C"  bool Collection_1_Contains_m3662151142_gshared (Collection_1_t3291332202 * __this, KeyValuePair_2_t3749587448  ___item0, const MethodInfo* method);
#define Collection_1_Contains_m3662151142(__this, ___item0, method) ((  bool (*) (Collection_1_t3291332202 *, KeyValuePair_2_t3749587448 , const MethodInfo*))Collection_1_Contains_m3662151142_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m2353712956_gshared (Collection_1_t3291332202 * __this, KeyValuePair_2U5BU5D_t598529833* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m2353712956(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t3291332202 *, KeyValuePair_2U5BU5D_t598529833*, int32_t, const MethodInfo*))Collection_1_CopyTo_m2353712956_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m242139379_gshared (Collection_1_t3291332202 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m242139379(__this, method) ((  Il2CppObject* (*) (Collection_1_t3291332202 *, const MethodInfo*))Collection_1_GetEnumerator_m242139379_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m313231410_gshared (Collection_1_t3291332202 * __this, KeyValuePair_2_t3749587448  ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m313231410(__this, ___item0, method) ((  int32_t (*) (Collection_1_t3291332202 *, KeyValuePair_2_t3749587448 , const MethodInfo*))Collection_1_IndexOf_m313231410_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m730918621_gshared (Collection_1_t3291332202 * __this, int32_t ___index0, KeyValuePair_2_t3749587448  ___item1, const MethodInfo* method);
#define Collection_1_Insert_m730918621(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3291332202 *, int32_t, KeyValuePair_2_t3749587448 , const MethodInfo*))Collection_1_Insert_m730918621_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m2603963714_gshared (Collection_1_t3291332202 * __this, int32_t ___index0, KeyValuePair_2_t3749587448  ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m2603963714(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3291332202 *, int32_t, KeyValuePair_2_t3749587448 , const MethodInfo*))Collection_1_InsertItem_m2603963714_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::Remove(T)
extern "C"  bool Collection_1_Remove_m2887213885_gshared (Collection_1_t3291332202 * __this, KeyValuePair_2_t3749587448  ___item0, const MethodInfo* method);
#define Collection_1_Remove_m2887213885(__this, ___item0, method) ((  bool (*) (Collection_1_t3291332202 *, KeyValuePair_2_t3749587448 , const MethodInfo*))Collection_1_Remove_m2887213885_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m1330164409_gshared (Collection_1_t3291332202 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m1330164409(__this, ___index0, method) ((  void (*) (Collection_1_t3291332202 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m1330164409_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m2545982485_gshared (Collection_1_t3291332202 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m2545982485(__this, ___index0, method) ((  void (*) (Collection_1_t3291332202 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m2545982485_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m622560779_gshared (Collection_1_t3291332202 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m622560779(__this, method) ((  int32_t (*) (Collection_1_t3291332202 *, const MethodInfo*))Collection_1_get_Count_m622560779_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::get_Item(System.Int32)
extern "C"  KeyValuePair_2_t3749587448  Collection_1_get_Item_m1331512421_gshared (Collection_1_t3291332202 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m1331512421(__this, ___index0, method) ((  KeyValuePair_2_t3749587448  (*) (Collection_1_t3291332202 *, int32_t, const MethodInfo*))Collection_1_get_Item_m1331512421_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m3122788206_gshared (Collection_1_t3291332202 * __this, int32_t ___index0, KeyValuePair_2_t3749587448  ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m3122788206(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3291332202 *, int32_t, KeyValuePair_2_t3749587448 , const MethodInfo*))Collection_1_set_Item_m3122788206_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m3814643073_gshared (Collection_1_t3291332202 * __this, int32_t ___index0, KeyValuePair_2_t3749587448  ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m3814643073(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3291332202 *, int32_t, KeyValuePair_2_t3749587448 , const MethodInfo*))Collection_1_SetItem_m3814643073_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m2960448888_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m2960448888(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m2960448888_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::ConvertItem(System.Object)
extern "C"  KeyValuePair_2_t3749587448  Collection_1_ConvertItem_m2034895050_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m2034895050(__this /* static, unused */, ___item0, method) ((  KeyValuePair_2_t3749587448  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m2034895050_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m1135968780_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m1135968780(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m1135968780_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m2138234282_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m2138234282(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m2138234282_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m526287301_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m526287301(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m526287301_gshared)(__this /* static, unused */, ___list0, method)
