﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_Data_System_Data_DataTableNewRowEventArgs743281225.h"
#include "System_Data_System_Data_DataViewRowState2419600614.h"
#include "System_Data_System_Data_DbType3924915636.h"
#include "System_Data_System_Data_DefaultValueTypeConverter2120401526.h"
#include "System_Data_System_Data_DeletedRowInaccessibleExce2346770439.h"
#include "System_Data_System_Data_DuplicateNameException3552620837.h"
#include "System_Data_System_Data_EvaluateException2485944854.h"
#include "System_Data_System_Data_FillErrorEventArgs1074279162.h"
#include "System_Data_System_Data_ForeignKeyConstraint173811822.h"
#include "System_Data_System_Data_InRowChangingEventExceptio3570492159.h"
#include "System_Data_System_Data_InternalDataCollectionBase623571698.h"
#include "System_Data_System_Data_InvalidConstraintException2718272443.h"
#include "System_Data_System_Data_InvalidExpressionException539357262.h"
#include "System_Data_System_Data_IsolationLevel59320808.h"
#include "System_Data_System_Data_LoadOption3079508071.h"
#include "System_Data_System_Data_MappingType2630770062.h"
#include "System_Data_System_Data_MergeFailedEventArgs1148725902.h"
#include "System_Data_System_Data_MergeManager612289193.h"
#include "System_Data_System_Data_MissingMappingAction3119158880.h"
#include "System_Data_System_Data_MissingPrimaryKeyException235109558.h"
#include "System_Data_System_Data_MissingSchemaAction1287182105.h"
#include "System_Data_System_Data_NoNullAllowedException1424657961.h"
#include "System_Data_System_Data_ParameterDirection744727978.h"
#include "System_Data_System_Data_PropertyCollection2281048903.h"
#include "System_Data_System_Data_ReadOnlyException3369519507.h"
#include "System_Data_System_Data_RelationshipConverter1979275730.h"
#include "System_Data_System_Data_ResDescriptionAttribute123323792.h"
#include "System_Data_System_Data_RowNotInTableException2346777297.h"
#include "System_Data_System_Data_Rule1417458836.h"
#include "System_Data_System_Data_SchemaSerializationMode367309070.h"
#include "System_Data_System_Data_SchemaType1007105897.h"
#include "System_Data_System_Data_SerializationFormat2880076079.h"
#include "System_Data_System_Data_StateChangeEventArgs215369544.h"
#include "System_Data_System_Data_StatementType1081125549.h"
#include "System_Data_System_Data_SyntaxErrorException3530425704.h"
#include "System_Data_System_Data_GenerateMethodsType120596799.h"
#include "System_Data_System_Data_DbSourceMethodInfo4064904528.h"
#include "System_Data_System_Data_DbCommandInfo413992431.h"
#include "System_Data_System_Data_TableAdapterSchemaInfo2137065232.h"
#include "System_Data_System_Data_UniqueConstraint3624508568.h"
#include "System_Data_System_Data_UpdateRowSource4286771644.h"
#include "System_Data_System_Data_UpdateStatus3470546469.h"
#include "System_Data_System_Data_VersionNotFoundException966570184.h"
#include "System_Data_System_Data_XmlConstants2546877498.h"
#include "System_Data_System_Data_ElementMappingType2128901752.h"
#include "System_Data_System_Data_TableMappingCollection837861354.h"
#include "System_Data_System_Data_TableMapping1812080488.h"
#include "System_Data_System_Data_XmlDataInferenceLoader3296888569.h"
#include "System_Data_System_Data_XmlDataLoader525036246.h"
#include "System_Data_System_Data_XmlDataReader1109197734.h"
#include "System_Data_System_Data_XmlDiffLoader1575285317.h"
#include "System_Data_XmlHelper925396301.h"
#include "System_Data_System_Data_XmlReadMode2037225274.h"
#include "System_Data_System_Data_RelationStructureCollection827821361.h"
#include "System_Data_System_Data_TableStructure1849363163.h"
#include "System_Data_System_Data_RelationStructure190084897.h"
#include "System_Data_System_Data_ConstraintStructure1728327650.h"
#include "System_Data_System_Data_XmlSchemaDataImporter3046229468.h"
#include "System_Data_System_Data_XmlSchemaWriter3154544541.h"
#include "System_Data_XmlTableWriter2994005340.h"
#include "System_Data_System_Data_XmlWriteMode996295013.h"
#include "System_Data_System_Data_DelegateConstraintNameChan2880054281.h"
#include "System_Data_System_Data_DataColumnChangeEventHandl3436675734.h"
#include "System_Data_System_Data_DataRowChangeEventHandler167053918.h"
#include "System_Data_System_Data_DataTableNewRowEventHandle2361045790.h"
#include "System_Data_System_Data_FillErrorEventHandler767070689.h"
#include "System_Data_System_Data_MergeFailedEventHandler790137133.h"
#include "System_Data_System_Data_StateChangeEventHandler979139449.h"
#include "System_Data_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "System_Data_U3CPrivateImplementationDetailsU3E_U242441637382.h"
#include "System_Data_U3CPrivateImplementationDetailsU3E_U24A116038620.h"
#include "System_Data_U3CPrivateImplementationDetailsU3E_U242866209745.h"
#include "System_Data_U3CPrivateImplementationDetailsU3E_U242672183884.h"
#include "System_Data_U3CPrivateImplementationDetailsU3E_U24A540610915.h"
#include "AWSSDK_CognitoIdentity_U3CModuleU3E3783534214.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Cogn2370264792.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Cogn3661410707.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Cogn1657401211.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Cogn2011390993.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Cogni795329086.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Amaz1124767059.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Amazo158465174.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Amazo492659996.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Amaz3069350888.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2600 = { sizeof (DataTableNewRowEventArgs_t743281225), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2600[1] = 
{
	DataTableNewRowEventArgs_t743281225::get_offset_of__row_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2601 = { sizeof (DataViewRowState_t2419600614)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2601[9] = 
{
	DataViewRowState_t2419600614::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2602 = { sizeof (DbType_t3924915636)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2602[28] = 
{
	DbType_t3924915636::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2603 = { sizeof (DefaultValueTypeConverter_t2120401526), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2604 = { sizeof (DeletedRowInaccessibleException_t2346770439), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2605 = { sizeof (DuplicateNameException_t3552620837), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2606 = { sizeof (EvaluateException_t2485944854), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2607 = { sizeof (FillErrorEventArgs_t1074279162), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2607[4] = 
{
	FillErrorEventArgs_t1074279162::get_offset_of_data_table_1(),
	FillErrorEventArgs_t1074279162::get_offset_of_values_2(),
	FillErrorEventArgs_t1074279162::get_offset_of_errors_3(),
	FillErrorEventArgs_t1074279162::get_offset_of_f_continue_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2608 = { sizeof (ForeignKeyConstraint_t173811822), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2608[6] = 
{
	ForeignKeyConstraint_t173811822::get_offset_of__parentUniqueConstraint_8(),
	ForeignKeyConstraint_t173811822::get_offset_of__parentColumns_9(),
	ForeignKeyConstraint_t173811822::get_offset_of__childColumns_10(),
	ForeignKeyConstraint_t173811822::get_offset_of__deleteRule_11(),
	ForeignKeyConstraint_t173811822::get_offset_of__updateRule_12(),
	ForeignKeyConstraint_t173811822::get_offset_of__acceptRejectRule_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2609 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2610 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2611 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2612 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2613 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2614 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2615 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2616 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2617 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2618 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2619 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2620 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2621 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2622 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2623 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2624 = { sizeof (InRowChangingEventException_t3570492159), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2625 = { sizeof (InternalDataCollectionBase_t623571698), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2625[2] = 
{
	InternalDataCollectionBase_t623571698::get_offset_of_list_0(),
	InternalDataCollectionBase_t623571698::get_offset_of_synchronized_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2626 = { sizeof (InvalidConstraintException_t2718272443), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2627 = { sizeof (InvalidExpressionException_t539357262), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2628 = { sizeof (IsolationLevel_t59320808)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2628[8] = 
{
	IsolationLevel_t59320808::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2629 = { sizeof (LoadOption_t3079508071)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2629[4] = 
{
	LoadOption_t3079508071::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2630 = { sizeof (MappingType_t2630770062)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2630[5] = 
{
	MappingType_t2630770062::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2631 = { sizeof (MergeFailedEventArgs_t1148725902), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2631[2] = 
{
	MergeFailedEventArgs_t1148725902::get_offset_of_data_table_1(),
	MergeFailedEventArgs_t1148725902::get_offset_of_conflict_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2632 = { sizeof (MergeManager_t612289193), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2633 = { sizeof (MissingMappingAction_t3119158880)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2633[4] = 
{
	MissingMappingAction_t3119158880::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2634 = { sizeof (MissingPrimaryKeyException_t235109558), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2635 = { sizeof (MissingSchemaAction_t1287182105)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2635[5] = 
{
	MissingSchemaAction_t1287182105::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2636 = { sizeof (NoNullAllowedException_t1424657961), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2637 = { sizeof (ParameterDirection_t744727978)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2637[5] = 
{
	ParameterDirection_t744727978::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2638 = { sizeof (PropertyCollection_t2281048903), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2639 = { sizeof (ReadOnlyException_t3369519507), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2640 = { sizeof (RelationshipConverter_t1979275730), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2641 = { sizeof (ResDescriptionAttribute_t123323792), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2641[1] = 
{
	ResDescriptionAttribute_t123323792::get_offset_of_description_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2642 = { sizeof (RowNotInTableException_t2346777297), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2643 = { sizeof (Rule_t1417458836)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2643[5] = 
{
	Rule_t1417458836::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2644 = { sizeof (SchemaSerializationMode_t367309070)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2644[3] = 
{
	SchemaSerializationMode_t367309070::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2645 = { sizeof (SchemaType_t1007105897)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2645[3] = 
{
	SchemaType_t1007105897::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2646 = { sizeof (SerializationFormat_t2880076079)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2646[3] = 
{
	SerializationFormat_t2880076079::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2647 = { sizeof (StateChangeEventArgs_t215369544), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2647[2] = 
{
	StateChangeEventArgs_t215369544::get_offset_of_originalState_1(),
	StateChangeEventArgs_t215369544::get_offset_of_currentState_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2648 = { sizeof (StatementType_t1081125549)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2648[6] = 
{
	StatementType_t1081125549::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2649 = { sizeof (SyntaxErrorException_t3530425704), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2650 = { sizeof (GenerateMethodsType_t120596799)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2650[5] = 
{
	GenerateMethodsType_t120596799::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2651 = { sizeof (DbSourceMethodInfo_t4064904528), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2651[5] = 
{
	DbSourceMethodInfo_t4064904528::get_offset_of_MethodType_0(),
	DbSourceMethodInfo_t4064904528::get_offset_of_Name_1(),
	DbSourceMethodInfo_t4064904528::get_offset_of_Modifier_2(),
	DbSourceMethodInfo_t4064904528::get_offset_of_QueryType_3(),
	DbSourceMethodInfo_t4064904528::get_offset_of_ScalarCallRetval_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2652 = { sizeof (DbCommandInfo_t413992431), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2652[2] = 
{
	DbCommandInfo_t413992431::get_offset_of_Command_0(),
	DbCommandInfo_t413992431::get_offset_of_Methods_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2653 = { sizeof (TableAdapterSchemaInfo_t2137065232), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2653[8] = 
{
	TableAdapterSchemaInfo_t2137065232::get_offset_of_Provider_0(),
	TableAdapterSchemaInfo_t2137065232::get_offset_of_Adapter_1(),
	TableAdapterSchemaInfo_t2137065232::get_offset_of_Connection_2(),
	TableAdapterSchemaInfo_t2137065232::get_offset_of_ConnectionString_3(),
	TableAdapterSchemaInfo_t2137065232::get_offset_of_BaseClass_4(),
	TableAdapterSchemaInfo_t2137065232::get_offset_of_Name_5(),
	TableAdapterSchemaInfo_t2137065232::get_offset_of_ShortCommands_6(),
	TableAdapterSchemaInfo_t2137065232::get_offset_of_Commands_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2654 = { sizeof (UniqueConstraint_t3624508568), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2654[5] = 
{
	UniqueConstraint_t3624508568::get_offset_of__isPrimaryKey_8(),
	UniqueConstraint_t3624508568::get_offset_of__belongsToCollection_9(),
	UniqueConstraint_t3624508568::get_offset_of__dataTable_10(),
	UniqueConstraint_t3624508568::get_offset_of__dataColumns_11(),
	UniqueConstraint_t3624508568::get_offset_of__childConstraint_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2655 = { sizeof (UpdateRowSource_t4286771644)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2655[5] = 
{
	UpdateRowSource_t4286771644::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2656 = { sizeof (UpdateStatus_t3470546469)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2656[5] = 
{
	UpdateStatus_t3470546469::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2657 = { sizeof (VersionNotFoundException_t966570184), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2658 = { sizeof (XmlConstants_t2546877498), -1, sizeof(XmlConstants_t2546877498_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2658[19] = 
{
	XmlConstants_t2546877498_StaticFields::get_offset_of_QnString_0(),
	XmlConstants_t2546877498_StaticFields::get_offset_of_QnShort_1(),
	XmlConstants_t2546877498_StaticFields::get_offset_of_QnInt_2(),
	XmlConstants_t2546877498_StaticFields::get_offset_of_QnLong_3(),
	XmlConstants_t2546877498_StaticFields::get_offset_of_QnBoolean_4(),
	XmlConstants_t2546877498_StaticFields::get_offset_of_QnUnsignedByte_5(),
	XmlConstants_t2546877498_StaticFields::get_offset_of_QnChar_6(),
	XmlConstants_t2546877498_StaticFields::get_offset_of_QnDateTime_7(),
	XmlConstants_t2546877498_StaticFields::get_offset_of_QnDecimal_8(),
	XmlConstants_t2546877498_StaticFields::get_offset_of_QnDouble_9(),
	XmlConstants_t2546877498_StaticFields::get_offset_of_QnSbyte_10(),
	XmlConstants_t2546877498_StaticFields::get_offset_of_QnFloat_11(),
	XmlConstants_t2546877498_StaticFields::get_offset_of_QnDuration_12(),
	XmlConstants_t2546877498_StaticFields::get_offset_of_QnUnsignedShort_13(),
	XmlConstants_t2546877498_StaticFields::get_offset_of_QnUnsignedInt_14(),
	XmlConstants_t2546877498_StaticFields::get_offset_of_QnUnsignedLong_15(),
	XmlConstants_t2546877498_StaticFields::get_offset_of_QnUri_16(),
	XmlConstants_t2546877498_StaticFields::get_offset_of_QnBase64Binary_17(),
	XmlConstants_t2546877498_StaticFields::get_offset_of_QnXmlQualifiedName_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2659 = { sizeof (ElementMappingType_t2128901752)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2659[4] = 
{
	ElementMappingType_t2128901752::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2660 = { sizeof (TableMappingCollection_t837861354), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2661 = { sizeof (TableMapping_t1812080488), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2661[10] = 
{
	TableMapping_t1812080488::get_offset_of_existsInDataSet_0(),
	TableMapping_t1812080488::get_offset_of_Table_1(),
	TableMapping_t1812080488::get_offset_of_Elements_2(),
	TableMapping_t1812080488::get_offset_of_Attributes_3(),
	TableMapping_t1812080488::get_offset_of_SimpleContent_4(),
	TableMapping_t1812080488::get_offset_of_PrimaryKey_5(),
	TableMapping_t1812080488::get_offset_of_ReferenceKey_6(),
	TableMapping_t1812080488::get_offset_of_lastElementIndex_7(),
	TableMapping_t1812080488::get_offset_of_ParentTable_8(),
	TableMapping_t1812080488::get_offset_of_ChildTables_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2662 = { sizeof (XmlDataInferenceLoader_t3296888569), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2662[6] = 
{
	XmlDataInferenceLoader_t3296888569::get_offset_of_dataset_0(),
	XmlDataInferenceLoader_t3296888569::get_offset_of_document_1(),
	XmlDataInferenceLoader_t3296888569::get_offset_of_mode_2(),
	XmlDataInferenceLoader_t3296888569::get_offset_of_ignoredNamespaces_3(),
	XmlDataInferenceLoader_t3296888569::get_offset_of_tables_4(),
	XmlDataInferenceLoader_t3296888569::get_offset_of_relations_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2663 = { sizeof (XmlDataLoader_t525036246), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2664 = { sizeof (XmlDataReader_t1109197734), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2664[3] = 
{
	XmlDataReader_t1109197734::get_offset_of_dataset_0(),
	XmlDataReader_t1109197734::get_offset_of_reader_1(),
	XmlDataReader_t1109197734::get_offset_of_mode_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2665 = { sizeof (XmlDiffLoader_t1575285317), -1, sizeof(XmlDiffLoader_t1575285317_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2665[5] = 
{
	XmlDiffLoader_t1575285317::get_offset_of_DSet_0(),
	XmlDiffLoader_t1575285317::get_offset_of_table_1(),
	XmlDiffLoader_t1575285317::get_offset_of_DiffGrRows_2(),
	XmlDiffLoader_t1575285317::get_offset_of_ErrorRows_3(),
	XmlDiffLoader_t1575285317_StaticFields::get_offset_of_U3CU3Ef__switchU24map8_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2666 = { sizeof (XmlHelper_t925396301), -1, sizeof(XmlHelper_t925396301_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2666[2] = 
{
	XmlHelper_t925396301_StaticFields::get_offset_of_localSchemaNameCache_0(),
	XmlHelper_t925396301_StaticFields::get_offset_of_localXmlNameCache_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2667 = { sizeof (XmlReadMode_t2037225274)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2667[8] = 
{
	XmlReadMode_t2037225274::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2668 = { sizeof (RelationStructureCollection_t827821361), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2669 = { sizeof (TableStructure_t1849363163), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2669[4] = 
{
	TableStructure_t1849363163::get_offset_of_Table_0(),
	TableStructure_t1849363163::get_offset_of_OrdinalColumns_1(),
	TableStructure_t1849363163::get_offset_of_NonOrdinalColumns_2(),
	TableStructure_t1849363163::get_offset_of_PrimaryKey_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2670 = { sizeof (RelationStructure_t190084897), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2670[7] = 
{
	RelationStructure_t190084897::get_offset_of_ExplicitName_0(),
	RelationStructure_t190084897::get_offset_of_ParentTableName_1(),
	RelationStructure_t190084897::get_offset_of_ChildTableName_2(),
	RelationStructure_t190084897::get_offset_of_ParentColumnName_3(),
	RelationStructure_t190084897::get_offset_of_ChildColumnName_4(),
	RelationStructure_t190084897::get_offset_of_IsNested_5(),
	RelationStructure_t190084897::get_offset_of_CreateConstraint_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2671 = { sizeof (ConstraintStructure_t1728327650), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2671[8] = 
{
	ConstraintStructure_t1728327650::get_offset_of_TableName_0(),
	ConstraintStructure_t1728327650::get_offset_of_Columns_1(),
	ConstraintStructure_t1728327650::get_offset_of_IsAttribute_2(),
	ConstraintStructure_t1728327650::get_offset_of_ConstraintName_3(),
	ConstraintStructure_t1728327650::get_offset_of_IsPrimaryKey_4(),
	ConstraintStructure_t1728327650::get_offset_of_ReferName_5(),
	ConstraintStructure_t1728327650::get_offset_of_IsNested_6(),
	ConstraintStructure_t1728327650::get_offset_of_IsConstraintOnly_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2672 = { sizeof (XmlSchemaDataImporter_t3046229468), -1, sizeof(XmlSchemaDataImporter_t3046229468_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2672[19] = 
{
	XmlSchemaDataImporter_t3046229468_StaticFields::get_offset_of_schemaIntegerType_0(),
	XmlSchemaDataImporter_t3046229468_StaticFields::get_offset_of_schemaDecimalType_1(),
	XmlSchemaDataImporter_t3046229468_StaticFields::get_offset_of_schemaAnyType_2(),
	XmlSchemaDataImporter_t3046229468::get_offset_of_dataset_3(),
	XmlSchemaDataImporter_t3046229468::get_offset_of_forDataSet_4(),
	XmlSchemaDataImporter_t3046229468::get_offset_of_schema_5(),
	XmlSchemaDataImporter_t3046229468::get_offset_of_relations_6(),
	XmlSchemaDataImporter_t3046229468::get_offset_of_reservedConstraints_7(),
	XmlSchemaDataImporter_t3046229468::get_offset_of_datasetElement_8(),
	XmlSchemaDataImporter_t3046229468::get_offset_of_topLevelElements_9(),
	XmlSchemaDataImporter_t3046229468::get_offset_of_targetElements_10(),
	XmlSchemaDataImporter_t3046229468::get_offset_of_currentTable_11(),
	XmlSchemaDataImporter_t3046229468::get_offset_of_currentAdapter_12(),
	XmlSchemaDataImporter_t3046229468_StaticFields::get_offset_of_U3CU3Ef__switchU24map9_13(),
	XmlSchemaDataImporter_t3046229468_StaticFields::get_offset_of_U3CU3Ef__switchU24mapA_14(),
	XmlSchemaDataImporter_t3046229468_StaticFields::get_offset_of_U3CU3Ef__switchU24mapB_15(),
	XmlSchemaDataImporter_t3046229468_StaticFields::get_offset_of_U3CU3Ef__switchU24mapC_16(),
	XmlSchemaDataImporter_t3046229468_StaticFields::get_offset_of_U3CU3Ef__switchU24mapD_17(),
	XmlSchemaDataImporter_t3046229468_StaticFields::get_offset_of_U3CU3Ef__switchU24mapE_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2673 = { sizeof (XmlSchemaWriter_t3154544541), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2673[11] = 
{
	XmlSchemaWriter_t3154544541::get_offset_of_w_0(),
	XmlSchemaWriter_t3154544541::get_offset_of_tables_1(),
	XmlSchemaWriter_t3154544541::get_offset_of_relations_2(),
	XmlSchemaWriter_t3154544541::get_offset_of_mainDataTable_3(),
	XmlSchemaWriter_t3154544541::get_offset_of_dataSetName_4(),
	XmlSchemaWriter_t3154544541::get_offset_of_dataSetNamespace_5(),
	XmlSchemaWriter_t3154544541::get_offset_of_dataSetProperties_6(),
	XmlSchemaWriter_t3154544541::get_offset_of_dataSetLocale_7(),
	XmlSchemaWriter_t3154544541::get_offset_of_globalTypeTables_8(),
	XmlSchemaWriter_t3154544541::get_offset_of_additionalNamespaces_9(),
	XmlSchemaWriter_t3154544541::get_offset_of_annotation_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2674 = { sizeof (XmlTableWriter_t2994005340), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2675 = { sizeof (XmlWriteMode_t996295013)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2675[4] = 
{
	XmlWriteMode_t996295013::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2676 = { sizeof (DelegateConstraintNameChange_t2880054281), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2677 = { sizeof (DataColumnChangeEventHandler_t3436675734), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2678 = { sizeof (DataRowChangeEventHandler_t167053918), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2679 = { sizeof (DataTableNewRowEventHandler_t2361045790), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2680 = { sizeof (FillErrorEventHandler_t767070689), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2681 = { sizeof (MergeFailedEventHandler_t790137133), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2682 = { sizeof (StateChangeEventHandler_t979139449), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2683 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305144), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305144_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2683[9] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305144_StaticFields::get_offset_of_U24U24fieldU2D0_0(),
	U3CPrivateImplementationDetailsU3E_t1486305144_StaticFields::get_offset_of_U24U24fieldU2D1_1(),
	U3CPrivateImplementationDetailsU3E_t1486305144_StaticFields::get_offset_of_U24U24fieldU2D2_2(),
	U3CPrivateImplementationDetailsU3E_t1486305144_StaticFields::get_offset_of_U24U24fieldU2D3_3(),
	U3CPrivateImplementationDetailsU3E_t1486305144_StaticFields::get_offset_of_U24U24fieldU2D4_4(),
	U3CPrivateImplementationDetailsU3E_t1486305144_StaticFields::get_offset_of_U24U24fieldU2D5_5(),
	U3CPrivateImplementationDetailsU3E_t1486305144_StaticFields::get_offset_of_U24U24fieldU2D6_6(),
	U3CPrivateImplementationDetailsU3E_t1486305144_StaticFields::get_offset_of_U24U24fieldU2D7_7(),
	U3CPrivateImplementationDetailsU3E_t1486305144_StaticFields::get_offset_of_U24U24fieldU2D8_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2684 = { sizeof (U24ArrayTypeU24168_t2441637382)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24168_t2441637382 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2685 = { sizeof (U24ArrayTypeU24328_t116038620)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24328_t116038620 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2686 = { sizeof (U24ArrayTypeU2464_t2866209747)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2464_t2866209747 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2687 = { sizeof (U24ArrayTypeU241084_t2672183884)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU241084_t2672183884 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2688 = { sizeof (U24ArrayTypeU2440_t540610915)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2440_t540610915 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2689 = { sizeof (U3CModuleU3E_t3783534226), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2690 = { sizeof (CognitoAWSCredentials_t2370264792), -1, sizeof(CognitoAWSCredentials_t2370264792_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2690[14] = 
{
	CognitoAWSCredentials_t2370264792_StaticFields::get_offset_of_refreshIdLock_3(),
	CognitoAWSCredentials_t2370264792::get_offset_of_identityId_4(),
	CognitoAWSCredentials_t2370264792_StaticFields::get_offset_of_DefaultDurationSeconds_5(),
	CognitoAWSCredentials_t2370264792::get_offset_of_cib_6(),
	CognitoAWSCredentials_t2370264792::get_offset_of_sts_7(),
	CognitoAWSCredentials_t2370264792::get_offset_of_U3CAccountIdU3Ek__BackingField_8(),
	CognitoAWSCredentials_t2370264792::get_offset_of_U3CIdentityPoolIdU3Ek__BackingField_9(),
	CognitoAWSCredentials_t2370264792::get_offset_of_U3CUnAuthRoleArnU3Ek__BackingField_10(),
	CognitoAWSCredentials_t2370264792::get_offset_of_U3CAuthRoleArnU3Ek__BackingField_11(),
	CognitoAWSCredentials_t2370264792::get_offset_of_U3CLoginsU3Ek__BackingField_12(),
	CognitoAWSCredentials_t2370264792::get_offset_of__identityState_13(),
	CognitoAWSCredentials_t2370264792::get_offset_of_mIdentityChangedEvent_14(),
	CognitoAWSCredentials_t2370264792_StaticFields::get_offset_of_IDENTITY_ID_CACHE_KEY_15(),
	CognitoAWSCredentials_t2370264792_StaticFields::get_offset_of__lock_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2691 = { sizeof (RefreshIdentityOptions_t3661410707)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2691[3] = 
{
	RefreshIdentityOptions_t3661410707::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2692 = { sizeof (IdentityChangedArgs_t1657401211), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2692[2] = 
{
	IdentityChangedArgs_t1657401211::get_offset_of_U3COldIdentityIdU3Ek__BackingField_1(),
	IdentityChangedArgs_t1657401211::get_offset_of_U3CNewIdentityIdU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2693 = { sizeof (IdentityState_t2011390993), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2693[4] = 
{
	IdentityState_t2011390993::get_offset_of_U3CIdentityIdU3Ek__BackingField_0(),
	IdentityState_t2011390993::get_offset_of_U3CLoginProviderU3Ek__BackingField_1(),
	IdentityState_t2011390993::get_offset_of_U3CLoginTokenU3Ek__BackingField_2(),
	IdentityState_t2011390993::get_offset_of_U3CFromCacheU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2694 = { sizeof (U3CU3Ec__DisplayClass59_0_t795329086), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2694[3] = 
{
	U3CU3Ec__DisplayClass59_0_t795329086::get_offset_of_exception_0(),
	U3CU3Ec__DisplayClass59_0_t795329086::get_offset_of_credentials_1(),
	U3CU3Ec__DisplayClass59_0_t795329086::get_offset_of_ars_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2695 = { sizeof (AmazonCognitoIdentityConfig_t1124767059), -1, sizeof(AmazonCognitoIdentityConfig_t1124767059_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2695[2] = 
{
	AmazonCognitoIdentityConfig_t1124767059_StaticFields::get_offset_of_UserAgentString_21(),
	AmazonCognitoIdentityConfig_t1124767059::get_offset_of__userAgent_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2696 = { sizeof (AmazonCognitoIdentityException_t158465174), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2697 = { sizeof (AmazonCognitoIdentityRequest_t492659996), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2698 = { sizeof (AmazonCognitoIdentityClient_t3069350888), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2699 = { 0, -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
