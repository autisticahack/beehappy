﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.PipelineHandler
struct PipelineHandler_t1486422324;
// Amazon.Runtime.Internal.Util.ILogger
struct ILogger_t676853571;
// Amazon.Runtime.IPipelineHandler
struct IPipelineHandler_t2320756001;
// Amazon.Runtime.IExecutionContext
struct IExecutionContext_t2477130752;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// Amazon.Runtime.IAsyncExecutionContext
struct IAsyncExecutionContext_t3792344986;

#include "codegen/il2cpp-codegen.h"

// Amazon.Runtime.Internal.Util.ILogger Amazon.Runtime.Internal.PipelineHandler::get_Logger()
extern "C"  Il2CppObject * PipelineHandler_get_Logger_m4193200210 (PipelineHandler_t1486422324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.PipelineHandler::set_Logger(Amazon.Runtime.Internal.Util.ILogger)
extern "C"  void PipelineHandler_set_Logger_m782444919 (PipelineHandler_t1486422324 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.IPipelineHandler Amazon.Runtime.Internal.PipelineHandler::get_InnerHandler()
extern "C"  Il2CppObject * PipelineHandler_get_InnerHandler_m2623880691 (PipelineHandler_t1486422324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.PipelineHandler::set_InnerHandler(Amazon.Runtime.IPipelineHandler)
extern "C"  void PipelineHandler_set_InnerHandler_m1167490210 (PipelineHandler_t1486422324 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.IPipelineHandler Amazon.Runtime.Internal.PipelineHandler::get_OuterHandler()
extern "C"  Il2CppObject * PipelineHandler_get_OuterHandler_m2438372122 (PipelineHandler_t1486422324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.PipelineHandler::set_OuterHandler(Amazon.Runtime.IPipelineHandler)
extern "C"  void PipelineHandler_set_OuterHandler_m1529770487 (PipelineHandler_t1486422324 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.PipelineHandler::InvokeSync(Amazon.Runtime.IExecutionContext)
extern "C"  void PipelineHandler_InvokeSync_m1176515843 (PipelineHandler_t1486422324 * __this, Il2CppObject * ___executionContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Amazon.Runtime.Internal.PipelineHandler::InvokeAsync(Amazon.Runtime.IAsyncExecutionContext)
extern "C"  Il2CppObject * PipelineHandler_InvokeAsync_m876787224 (PipelineHandler_t1486422324 * __this, Il2CppObject * ___executionContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.PipelineHandler::AsyncCallback(Amazon.Runtime.IAsyncExecutionContext)
extern "C"  void PipelineHandler_AsyncCallback_m4078809381 (PipelineHandler_t1486422324 * __this, Il2CppObject * ___executionContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.PipelineHandler::InvokeAsyncCallback(Amazon.Runtime.IAsyncExecutionContext)
extern "C"  void PipelineHandler_InvokeAsyncCallback_m4116121165 (PipelineHandler_t1486422324 * __this, Il2CppObject * ___executionContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.PipelineHandler::LogMetrics(Amazon.Runtime.IExecutionContext)
extern "C"  void PipelineHandler_LogMetrics_m4056973959 (PipelineHandler_t1486422324 * __this, Il2CppObject * ___executionContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.PipelineHandler::.ctor()
extern "C"  void PipelineHandler__ctor_m3341977352 (PipelineHandler_t1486422324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
