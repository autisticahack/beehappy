﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ThirdParty.Json.LitJson.Lexer/StateHandler
struct StateHandler_t3489987002;
// System.Object
struct Il2CppObject;
// ThirdParty.Json.LitJson.FsmContext
struct FsmContext_t4275593467;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_FsmContext4275593467.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void ThirdParty.Json.LitJson.Lexer/StateHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void StateHandler__ctor_m1057417747 (StateHandler_t3489987002 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ThirdParty.Json.LitJson.Lexer/StateHandler::Invoke(ThirdParty.Json.LitJson.FsmContext)
extern "C"  bool StateHandler_Invoke_m4291935966 (StateHandler_t3489987002 * __this, FsmContext_t4275593467 * ___ctx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult ThirdParty.Json.LitJson.Lexer/StateHandler::BeginInvoke(ThirdParty.Json.LitJson.FsmContext,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * StateHandler_BeginInvoke_m2133288829 (StateHandler_t3489987002 * __this, FsmContext_t4275593467 * ___ctx0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ThirdParty.Json.LitJson.Lexer/StateHandler::EndInvoke(System.IAsyncResult)
extern "C"  bool StateHandler_EndInvoke_m2698481297 (StateHandler_t3489987002 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
