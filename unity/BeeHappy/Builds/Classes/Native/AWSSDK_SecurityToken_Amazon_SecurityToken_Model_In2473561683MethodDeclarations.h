﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.SecurityToken.Model.Internal.MarshallTransformations.AssumedRoleUserUnmarshaller
struct AssumedRoleUserUnmarshaller_t2473561683;
// Amazon.SecurityToken.Model.AssumedRoleUser
struct AssumedRoleUser_t150458319;
// Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext
struct XmlUnmarshallerContext_t1179575220;
// Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext
struct JsonUnmarshallerContext_t456235889;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_XmlU1179575220.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_JsonU456235889.h"

// Amazon.SecurityToken.Model.AssumedRoleUser Amazon.SecurityToken.Model.Internal.MarshallTransformations.AssumedRoleUserUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext)
extern "C"  AssumedRoleUser_t150458319 * AssumedRoleUserUnmarshaller_Unmarshall_m1449889951 (AssumedRoleUserUnmarshaller_t2473561683 * __this, XmlUnmarshallerContext_t1179575220 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.SecurityToken.Model.AssumedRoleUser Amazon.SecurityToken.Model.Internal.MarshallTransformations.AssumedRoleUserUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern "C"  AssumedRoleUser_t150458319 * AssumedRoleUserUnmarshaller_Unmarshall_m3565771478 (AssumedRoleUserUnmarshaller_t2473561683 * __this, JsonUnmarshallerContext_t456235889 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.SecurityToken.Model.Internal.MarshallTransformations.AssumedRoleUserUnmarshaller Amazon.SecurityToken.Model.Internal.MarshallTransformations.AssumedRoleUserUnmarshaller::get_Instance()
extern "C"  AssumedRoleUserUnmarshaller_t2473561683 * AssumedRoleUserUnmarshaller_get_Instance_m2346020868 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.SecurityToken.Model.Internal.MarshallTransformations.AssumedRoleUserUnmarshaller::.ctor()
extern "C"  void AssumedRoleUserUnmarshaller__ctor_m2457816639 (AssumedRoleUserUnmarshaller_t2473561683 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.SecurityToken.Model.Internal.MarshallTransformations.AssumedRoleUserUnmarshaller::.cctor()
extern "C"  void AssumedRoleUserUnmarshaller__cctor_m2822142530 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
