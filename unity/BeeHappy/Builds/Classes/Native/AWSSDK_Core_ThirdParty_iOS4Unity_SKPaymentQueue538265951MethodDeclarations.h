﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ThirdParty.iOS4Unity.SKPaymentQueue
struct SKPaymentQueue_t538265951;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr2504060609.h"

// System.Void ThirdParty.iOS4Unity.SKPaymentQueue::.cctor()
extern "C"  void SKPaymentQueue__cctor_m2759127688 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.iOS4Unity.SKPaymentQueue::.ctor(System.IntPtr)
extern "C"  void SKPaymentQueue__ctor_m766051409 (SKPaymentQueue_t538265951 * __this, IntPtr_t ___handle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.iOS4Unity.SKPaymentQueue::Dispose()
extern "C"  void SKPaymentQueue_Dispose_m2857690684 (SKPaymentQueue_t538265951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
