﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ThirdParty.iOS4Unity.NSLocale
struct NSLocale_t2224424797;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ThirdParty.iOS4Unity.NSLocale::.cctor()
extern "C"  void NSLocale__cctor_m3953903070 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.iOS4Unity.NSLocale::.ctor(System.IntPtr)
extern "C"  void NSLocale__ctor_m4025262799 (NSLocale_t2224424797 * __this, IntPtr_t ___handle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ThirdParty.iOS4Unity.NSLocale ThirdParty.iOS4Unity.NSLocale::get_AutoUpdatingCurrentLocale()
extern "C"  NSLocale_t2224424797 * NSLocale_get_AutoUpdatingCurrentLocale_m3105436119 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ThirdParty.iOS4Unity.NSLocale::get_Identifier()
extern "C"  String_t* NSLocale_get_Identifier_m2215215338 (NSLocale_t2224424797 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ThirdParty.iOS4Unity.NSLocale::get_LocaleIdentifier()
extern "C"  String_t* NSLocale_get_LocaleIdentifier_m3949637744 (NSLocale_t2224424797 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ThirdParty.iOS4Unity.NSLocale::ObjectForKey(System.String)
extern "C"  String_t* NSLocale_ObjectForKey_m3214359899 (NSLocale_t2224424797 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
