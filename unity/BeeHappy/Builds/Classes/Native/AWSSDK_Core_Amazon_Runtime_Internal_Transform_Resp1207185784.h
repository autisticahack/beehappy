﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.Runtime.Internal.Transform.ResponseMetadataUnmarshaller
struct ResponseMetadataUnmarshaller_t1207185784;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Transform.ResponseMetadataUnmarshaller
struct  ResponseMetadataUnmarshaller_t1207185784  : public Il2CppObject
{
public:

public:
};

struct ResponseMetadataUnmarshaller_t1207185784_StaticFields
{
public:
	// Amazon.Runtime.Internal.Transform.ResponseMetadataUnmarshaller Amazon.Runtime.Internal.Transform.ResponseMetadataUnmarshaller::_instance
	ResponseMetadataUnmarshaller_t1207185784 * ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(ResponseMetadataUnmarshaller_t1207185784_StaticFields, ____instance_0)); }
	inline ResponseMetadataUnmarshaller_t1207185784 * get__instance_0() const { return ____instance_0; }
	inline ResponseMetadataUnmarshaller_t1207185784 ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(ResponseMetadataUnmarshaller_t1207185784 * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier(&____instance_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
