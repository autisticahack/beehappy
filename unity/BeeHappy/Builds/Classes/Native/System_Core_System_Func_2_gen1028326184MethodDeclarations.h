﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen1688555246MethodDeclarations.h"

// System.Void System.Func`2<Amazon.Runtime.Metric,System.String>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m624147916(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t1028326184 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m3128215240_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<Amazon.Runtime.Metric,System.String>::Invoke(T)
#define Func_2_Invoke_m1167410128(__this, ___arg10, method) ((  String_t* (*) (Func_2_t1028326184 *, int32_t, const MethodInfo*))Func_2_Invoke_m1492874182_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<Amazon.Runtime.Metric,System.String>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m2524959029(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t1028326184 *, int32_t, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m4267057529_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<Amazon.Runtime.Metric,System.String>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m3441383578(__this, ___result0, method) ((  String_t* (*) (Func_2_t1028326184 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m4092508720_gshared)(__this, ___result0, method)
