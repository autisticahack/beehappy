﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.Util.ThreadPoolThrottler`1/ThreadPoolOptions`1<System.Object,System.Object>
struct ThreadPoolOptions_1_t1583506835;
// System.Action`1<System.Object>
struct Action_1_t2491248677;
// System.Action`2<System.Exception,System.Object>
struct Action_2_t738274861;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Action`1<Q> Amazon.Runtime.Internal.Util.ThreadPoolThrottler`1/ThreadPoolOptions`1<System.Object,System.Object>::get_Callback()
extern "C"  Action_1_t2491248677 * ThreadPoolOptions_1_get_Callback_m4086324877_gshared (ThreadPoolOptions_1_t1583506835 * __this, const MethodInfo* method);
#define ThreadPoolOptions_1_get_Callback_m4086324877(__this, method) ((  Action_1_t2491248677 * (*) (ThreadPoolOptions_1_t1583506835 *, const MethodInfo*))ThreadPoolOptions_1_get_Callback_m4086324877_gshared)(__this, method)
// System.Void Amazon.Runtime.Internal.Util.ThreadPoolThrottler`1/ThreadPoolOptions`1<System.Object,System.Object>::set_Callback(System.Action`1<Q>)
extern "C"  void ThreadPoolOptions_1_set_Callback_m57615590_gshared (ThreadPoolOptions_1_t1583506835 * __this, Action_1_t2491248677 * ___value0, const MethodInfo* method);
#define ThreadPoolOptions_1_set_Callback_m57615590(__this, ___value0, method) ((  void (*) (ThreadPoolOptions_1_t1583506835 *, Action_1_t2491248677 *, const MethodInfo*))ThreadPoolOptions_1_set_Callback_m57615590_gshared)(__this, ___value0, method)
// System.Action`2<System.Exception,Q> Amazon.Runtime.Internal.Util.ThreadPoolThrottler`1/ThreadPoolOptions`1<System.Object,System.Object>::get_ErrorCallback()
extern "C"  Action_2_t738274861 * ThreadPoolOptions_1_get_ErrorCallback_m689805622_gshared (ThreadPoolOptions_1_t1583506835 * __this, const MethodInfo* method);
#define ThreadPoolOptions_1_get_ErrorCallback_m689805622(__this, method) ((  Action_2_t738274861 * (*) (ThreadPoolOptions_1_t1583506835 *, const MethodInfo*))ThreadPoolOptions_1_get_ErrorCallback_m689805622_gshared)(__this, method)
// System.Void Amazon.Runtime.Internal.Util.ThreadPoolThrottler`1/ThreadPoolOptions`1<System.Object,System.Object>::set_ErrorCallback(System.Action`2<System.Exception,Q>)
extern "C"  void ThreadPoolOptions_1_set_ErrorCallback_m4058543465_gshared (ThreadPoolOptions_1_t1583506835 * __this, Action_2_t738274861 * ___value0, const MethodInfo* method);
#define ThreadPoolOptions_1_set_ErrorCallback_m4058543465(__this, ___value0, method) ((  void (*) (ThreadPoolOptions_1_t1583506835 *, Action_2_t738274861 *, const MethodInfo*))ThreadPoolOptions_1_set_ErrorCallback_m4058543465_gshared)(__this, ___value0, method)
// Q Amazon.Runtime.Internal.Util.ThreadPoolThrottler`1/ThreadPoolOptions`1<System.Object,System.Object>::get_State()
extern "C"  Il2CppObject * ThreadPoolOptions_1_get_State_m392834197_gshared (ThreadPoolOptions_1_t1583506835 * __this, const MethodInfo* method);
#define ThreadPoolOptions_1_get_State_m392834197(__this, method) ((  Il2CppObject * (*) (ThreadPoolOptions_1_t1583506835 *, const MethodInfo*))ThreadPoolOptions_1_get_State_m392834197_gshared)(__this, method)
// System.Void Amazon.Runtime.Internal.Util.ThreadPoolThrottler`1/ThreadPoolOptions`1<System.Object,System.Object>::set_State(Q)
extern "C"  void ThreadPoolOptions_1_set_State_m4200571582_gshared (ThreadPoolOptions_1_t1583506835 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ThreadPoolOptions_1_set_State_m4200571582(__this, ___value0, method) ((  void (*) (ThreadPoolOptions_1_t1583506835 *, Il2CppObject *, const MethodInfo*))ThreadPoolOptions_1_set_State_m4200571582_gshared)(__this, ___value0, method)
// System.Void Amazon.Runtime.Internal.Util.ThreadPoolThrottler`1/ThreadPoolOptions`1<System.Object,System.Object>::.ctor()
extern "C"  void ThreadPoolOptions_1__ctor_m2042521873_gshared (ThreadPoolOptions_1_t1583506835 * __this, const MethodInfo* method);
#define ThreadPoolOptions_1__ctor_m2042521873(__this, method) ((  void (*) (ThreadPoolOptions_1_t1583506835 *, const MethodInfo*))ThreadPoolOptions_1__ctor_m2042521873_gshared)(__this, method)
