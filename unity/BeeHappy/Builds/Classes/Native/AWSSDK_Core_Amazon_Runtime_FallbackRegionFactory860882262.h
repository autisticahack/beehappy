﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// System.Collections.Generic.List`1<Amazon.Runtime.FallbackRegionFactory/RegionGenerator>
struct List_1_t2492863754;
// Amazon.Runtime.AWSRegion
struct AWSRegion_t969138115;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.FallbackRegionFactory
struct  FallbackRegionFactory_t860882262  : public Il2CppObject
{
public:

public:
};

struct FallbackRegionFactory_t860882262_StaticFields
{
public:
	// System.Object Amazon.Runtime.FallbackRegionFactory::_lock
	Il2CppObject * ____lock_0;
	// System.Collections.Generic.List`1<Amazon.Runtime.FallbackRegionFactory/RegionGenerator> Amazon.Runtime.FallbackRegionFactory::<AllGenerators>k__BackingField
	List_1_t2492863754 * ___U3CAllGeneratorsU3Ek__BackingField_1;
	// System.Collections.Generic.List`1<Amazon.Runtime.FallbackRegionFactory/RegionGenerator> Amazon.Runtime.FallbackRegionFactory::<NonMetadataGenerators>k__BackingField
	List_1_t2492863754 * ___U3CNonMetadataGeneratorsU3Ek__BackingField_2;
	// Amazon.Runtime.AWSRegion Amazon.Runtime.FallbackRegionFactory::cachedRegion
	AWSRegion_t969138115 * ___cachedRegion_3;

public:
	inline static int32_t get_offset_of__lock_0() { return static_cast<int32_t>(offsetof(FallbackRegionFactory_t860882262_StaticFields, ____lock_0)); }
	inline Il2CppObject * get__lock_0() const { return ____lock_0; }
	inline Il2CppObject ** get_address_of__lock_0() { return &____lock_0; }
	inline void set__lock_0(Il2CppObject * value)
	{
		____lock_0 = value;
		Il2CppCodeGenWriteBarrier(&____lock_0, value);
	}

	inline static int32_t get_offset_of_U3CAllGeneratorsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(FallbackRegionFactory_t860882262_StaticFields, ___U3CAllGeneratorsU3Ek__BackingField_1)); }
	inline List_1_t2492863754 * get_U3CAllGeneratorsU3Ek__BackingField_1() const { return ___U3CAllGeneratorsU3Ek__BackingField_1; }
	inline List_1_t2492863754 ** get_address_of_U3CAllGeneratorsU3Ek__BackingField_1() { return &___U3CAllGeneratorsU3Ek__BackingField_1; }
	inline void set_U3CAllGeneratorsU3Ek__BackingField_1(List_1_t2492863754 * value)
	{
		___U3CAllGeneratorsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CAllGeneratorsU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CNonMetadataGeneratorsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(FallbackRegionFactory_t860882262_StaticFields, ___U3CNonMetadataGeneratorsU3Ek__BackingField_2)); }
	inline List_1_t2492863754 * get_U3CNonMetadataGeneratorsU3Ek__BackingField_2() const { return ___U3CNonMetadataGeneratorsU3Ek__BackingField_2; }
	inline List_1_t2492863754 ** get_address_of_U3CNonMetadataGeneratorsU3Ek__BackingField_2() { return &___U3CNonMetadataGeneratorsU3Ek__BackingField_2; }
	inline void set_U3CNonMetadataGeneratorsU3Ek__BackingField_2(List_1_t2492863754 * value)
	{
		___U3CNonMetadataGeneratorsU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CNonMetadataGeneratorsU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_cachedRegion_3() { return static_cast<int32_t>(offsetof(FallbackRegionFactory_t860882262_StaticFields, ___cachedRegion_3)); }
	inline AWSRegion_t969138115 * get_cachedRegion_3() const { return ___cachedRegion_3; }
	inline AWSRegion_t969138115 ** get_address_of_cachedRegion_3() { return &___cachedRegion_3; }
	inline void set_cachedRegion_3(AWSRegion_t969138115 * value)
	{
		___cachedRegion_3 = value;
		Il2CppCodeGenWriteBarrier(&___cachedRegion_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
