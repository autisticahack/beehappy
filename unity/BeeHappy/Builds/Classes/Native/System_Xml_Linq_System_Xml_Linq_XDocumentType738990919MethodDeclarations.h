﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.Linq.XDocumentType
struct XDocumentType_t738990919;
// System.String
struct String_t;
// System.Xml.XmlWriter
struct XmlWriter_t1048088568;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "System_Xml_Linq_System_Xml_Linq_XDocumentType738990919.h"
#include "System_Xml_System_Xml_XmlNodeType739504597.h"
#include "System_Xml_System_Xml_XmlWriter1048088568.h"

// System.Void System.Xml.Linq.XDocumentType::.ctor(System.String,System.String,System.String,System.String)
extern "C"  void XDocumentType__ctor_m2779569696 (XDocumentType_t738990919 * __this, String_t* ___name0, String_t* ___publicId1, String_t* ___systemId2, String_t* ___internalSubset3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Linq.XDocumentType::.ctor(System.Xml.Linq.XDocumentType)
extern "C"  void XDocumentType__ctor_m4083239521 (XDocumentType_t738990919 * __this, XDocumentType_t738990919 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.Linq.XDocumentType::get_Name()
extern "C"  String_t* XDocumentType_get_Name_m364384709 (XDocumentType_t738990919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.Linq.XDocumentType::get_PublicId()
extern "C"  String_t* XDocumentType_get_PublicId_m877542134 (XDocumentType_t738990919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.Linq.XDocumentType::get_SystemId()
extern "C"  String_t* XDocumentType_get_SystemId_m594286306 (XDocumentType_t738990919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.Linq.XDocumentType::get_InternalSubset()
extern "C"  String_t* XDocumentType_get_InternalSubset_m1487579211 (XDocumentType_t738990919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlNodeType System.Xml.Linq.XDocumentType::get_NodeType()
extern "C"  int32_t XDocumentType_get_NodeType_m2000115737 (XDocumentType_t738990919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Linq.XDocumentType::WriteTo(System.Xml.XmlWriter)
extern "C"  void XDocumentType_WriteTo_m2391677798 (XDocumentType_t738990919 * __this, XmlWriter_t1048088568 * ___w0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
