﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.CognitoIdentity.Model.GetIdResponse
struct GetIdResponse_t2091118072;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.String Amazon.CognitoIdentity.Model.GetIdResponse::get_IdentityId()
extern "C"  String_t* GetIdResponse_get_IdentityId_m160087976 (GetIdResponse_t2091118072 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoIdentity.Model.GetIdResponse::set_IdentityId(System.String)
extern "C"  void GetIdResponse_set_IdentityId_m4089638831 (GetIdResponse_t2091118072 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoIdentity.Model.GetIdResponse::.ctor()
extern "C"  void GetIdResponse__ctor_m1965179359 (GetIdResponse_t2091118072 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
