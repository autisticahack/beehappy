﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.CognitoSync.Model.RecordPatch
struct RecordPatch_t97905615;
// System.String
struct String_t;
// Amazon.CognitoSync.Operation
struct Operation_t3836154307;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime693205669.h"
#include "mscorlib_System_String2029220233.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_Operation3836154307.h"

// System.DateTime Amazon.CognitoSync.Model.RecordPatch::get_DeviceLastModifiedDate()
extern "C"  DateTime_t693205669  RecordPatch_get_DeviceLastModifiedDate_m3682150623 (RecordPatch_t97905615 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Model.RecordPatch::set_DeviceLastModifiedDate(System.DateTime)
extern "C"  void RecordPatch_set_DeviceLastModifiedDate_m3084918640 (RecordPatch_t97905615 * __this, DateTime_t693205669  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.CognitoSync.Model.RecordPatch::IsSetDeviceLastModifiedDate()
extern "C"  bool RecordPatch_IsSetDeviceLastModifiedDate_m12235479 (RecordPatch_t97905615 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.CognitoSync.Model.RecordPatch::get_Key()
extern "C"  String_t* RecordPatch_get_Key_m1098023535 (RecordPatch_t97905615 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Model.RecordPatch::set_Key(System.String)
extern "C"  void RecordPatch_set_Key_m4168825582 (RecordPatch_t97905615 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.CognitoSync.Model.RecordPatch::IsSetKey()
extern "C"  bool RecordPatch_IsSetKey_m1869535299 (RecordPatch_t97905615 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.CognitoSync.Operation Amazon.CognitoSync.Model.RecordPatch::get_Op()
extern "C"  Operation_t3836154307 * RecordPatch_get_Op_m2888682898 (RecordPatch_t97905615 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Model.RecordPatch::set_Op(Amazon.CognitoSync.Operation)
extern "C"  void RecordPatch_set_Op_m2043014053 (RecordPatch_t97905615 * __this, Operation_t3836154307 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.CognitoSync.Model.RecordPatch::IsSetOp()
extern "C"  bool RecordPatch_IsSetOp_m4245313611 (RecordPatch_t97905615 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Amazon.CognitoSync.Model.RecordPatch::get_SyncCount()
extern "C"  int64_t RecordPatch_get_SyncCount_m2520034310 (RecordPatch_t97905615 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Model.RecordPatch::set_SyncCount(System.Int64)
extern "C"  void RecordPatch_set_SyncCount_m2765090663 (RecordPatch_t97905615 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.CognitoSync.Model.RecordPatch::IsSetSyncCount()
extern "C"  bool RecordPatch_IsSetSyncCount_m708684784 (RecordPatch_t97905615 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.CognitoSync.Model.RecordPatch::get_Value()
extern "C"  String_t* RecordPatch_get_Value_m2608180695 (RecordPatch_t97905615 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Model.RecordPatch::set_Value(System.String)
extern "C"  void RecordPatch_set_Value_m406070084 (RecordPatch_t97905615 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.CognitoSync.Model.RecordPatch::IsSetValue()
extern "C"  bool RecordPatch_IsSetValue_m591623923 (RecordPatch_t97905615 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Model.RecordPatch::.ctor()
extern "C"  void RecordPatch__ctor_m4274095496 (RecordPatch_t97905615 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
