﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.CognitoIdentity.CognitoAWSCredentials
struct CognitoAWSCredentials_t2370264792;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;
// Amazon.CognitoIdentity.CognitoAWSCredentials/IdentityState
struct IdentityState_t2011390993;
// Amazon.CognitoIdentity.AmazonCognitoIdentityException
struct AmazonCognitoIdentityException_t158465174;
// System.EventHandler`1<Amazon.CognitoIdentity.CognitoAWSCredentials/IdentityChangedArgs>
struct EventHandler_1_t248708383;
// Amazon.RegionEndpoint
struct RegionEndpoint_t661522805;
// Amazon.CognitoIdentity.IAmazonCognitoIdentity
struct IAmazonCognitoIdentity_t1261655088;
// Amazon.SecurityToken.IAmazonSecurityTokenService
struct IAmazonSecurityTokenService_t4042936971;
// Amazon.Runtime.RefreshingAWSCredentials/CredentialsRefreshState
struct CredentialsRefreshState_t3294867821;
// Amazon.SecurityToken.Model.Credentials
struct Credentials_t3554032640;
// Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest
struct AssumeRoleWithWebIdentityRequest_t193094215;
// Amazon.CognitoIdentity.Model.GetCredentialsForIdentityResponse
struct GetCredentialsForIdentityResponse_t2302678766;
// Amazon.CognitoIdentity.Model.GetCredentialsForIdentityRequest
struct GetCredentialsForIdentityRequest_t932830458;
// Amazon.CognitoIdentity.Model.GetOpenIdTokenResponse
struct GetOpenIdTokenResponse_t955597635;
// Amazon.CognitoIdentity.Model.GetOpenIdTokenRequest
struct GetOpenIdTokenRequest_t2698079901;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Cogn3661410707.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Amazo158465174.h"
#include "AWSSDK_Core_Amazon_RegionEndpoint661522805.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_Model_Ass193094215.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Model932830458.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Mode2698079901.h"
#include "AWSSDK_Core_Amazon_Runtime_RefreshingAWSCredential3294867821.h"

// System.Boolean Amazon.CognitoIdentity.CognitoAWSCredentials::get_IsIdentitySet()
extern "C"  bool CognitoAWSCredentials_get_IsIdentitySet_m3850620751 (CognitoAWSCredentials_t2370264792 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoIdentity.CognitoAWSCredentials::UpdateIdentity(System.String)
extern "C"  void CognitoAWSCredentials_UpdateIdentity_m833388265 (CognitoAWSCredentials_t2370264792 * __this, String_t* ___newIdentityId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.CognitoIdentity.CognitoAWSCredentials::GetNamespacedKey(System.String)
extern "C"  String_t* CognitoAWSCredentials_GetNamespacedKey_m3390205149 (CognitoAWSCredentials_t2370264792 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.CognitoIdentity.CognitoAWSCredentials::get_AccountId()
extern "C"  String_t* CognitoAWSCredentials_get_AccountId_m1435683876 (CognitoAWSCredentials_t2370264792 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoIdentity.CognitoAWSCredentials::set_AccountId(System.String)
extern "C"  void CognitoAWSCredentials_set_AccountId_m309865923 (CognitoAWSCredentials_t2370264792 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.CognitoIdentity.CognitoAWSCredentials::get_IdentityPoolId()
extern "C"  String_t* CognitoAWSCredentials_get_IdentityPoolId_m2723479775 (CognitoAWSCredentials_t2370264792 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoIdentity.CognitoAWSCredentials::set_IdentityPoolId(System.String)
extern "C"  void CognitoAWSCredentials_set_IdentityPoolId_m2126935958 (CognitoAWSCredentials_t2370264792 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.CognitoIdentity.CognitoAWSCredentials::get_UnAuthRoleArn()
extern "C"  String_t* CognitoAWSCredentials_get_UnAuthRoleArn_m4182746162 (CognitoAWSCredentials_t2370264792 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoIdentity.CognitoAWSCredentials::set_UnAuthRoleArn(System.String)
extern "C"  void CognitoAWSCredentials_set_UnAuthRoleArn_m338067849 (CognitoAWSCredentials_t2370264792 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.CognitoIdentity.CognitoAWSCredentials::get_AuthRoleArn()
extern "C"  String_t* CognitoAWSCredentials_get_AuthRoleArn_m2832189913 (CognitoAWSCredentials_t2370264792 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoIdentity.CognitoAWSCredentials::set_AuthRoleArn(System.String)
extern "C"  void CognitoAWSCredentials_set_AuthRoleArn_m1654546 (CognitoAWSCredentials_t2370264792 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> Amazon.CognitoIdentity.CognitoAWSCredentials::get_Logins()
extern "C"  Dictionary_2_t3943999495 * CognitoAWSCredentials_get_Logins_m1002484841 (CognitoAWSCredentials_t2370264792 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoIdentity.CognitoAWSCredentials::set_Logins(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void CognitoAWSCredentials_set_Logins_m932349206 (CognitoAWSCredentials_t2370264792 * __this, Dictionary_2_t3943999495 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoIdentity.CognitoAWSCredentials::Clear()
extern "C"  void CognitoAWSCredentials_Clear_m3749298493 (CognitoAWSCredentials_t2370264792 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.CognitoIdentity.CognitoAWSCredentials::GetIdentityId()
extern "C"  String_t* CognitoAWSCredentials_GetIdentityId_m1822078866 (CognitoAWSCredentials_t2370264792 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.CognitoIdentity.CognitoAWSCredentials::GetIdentityId(Amazon.CognitoIdentity.CognitoAWSCredentials/RefreshIdentityOptions)
extern "C"  String_t* CognitoAWSCredentials_GetIdentityId_m1939085957 (CognitoAWSCredentials_t2370264792 * __this, int32_t ___options0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.CognitoIdentity.CognitoAWSCredentials/IdentityState Amazon.CognitoIdentity.CognitoAWSCredentials::RefreshIdentity()
extern "C"  IdentityState_t2011390993 * CognitoAWSCredentials_RefreshIdentity_m2495389495 (CognitoAWSCredentials_t2370264792 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.CognitoIdentity.CognitoAWSCredentials::ShouldRetry(Amazon.CognitoIdentity.AmazonCognitoIdentityException)
extern "C"  bool CognitoAWSCredentials_ShouldRetry_m1614465732 (CognitoAWSCredentials_t2370264792 * __this, AmazonCognitoIdentityException_t158465174 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoIdentity.CognitoAWSCredentials::add_IdentityChangedEvent(System.EventHandler`1<Amazon.CognitoIdentity.CognitoAWSCredentials/IdentityChangedArgs>)
extern "C"  void CognitoAWSCredentials_add_IdentityChangedEvent_m542934635 (CognitoAWSCredentials_t2370264792 * __this, EventHandler_1_t248708383 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoIdentity.CognitoAWSCredentials::remove_IdentityChangedEvent(System.EventHandler`1<Amazon.CognitoIdentity.CognitoAWSCredentials/IdentityChangedArgs>)
extern "C"  void CognitoAWSCredentials_remove_IdentityChangedEvent_m1774745486 (CognitoAWSCredentials_t2370264792 * __this, EventHandler_1_t248708383 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoIdentity.CognitoAWSCredentials::.ctor(System.String,Amazon.RegionEndpoint)
extern "C"  void CognitoAWSCredentials__ctor_m2007748265 (CognitoAWSCredentials_t2370264792 * __this, String_t* ___identityPoolId0, RegionEndpoint_t661522805 * ___region1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoIdentity.CognitoAWSCredentials::.ctor(System.String,System.String,System.String,System.String,Amazon.RegionEndpoint)
extern "C"  void CognitoAWSCredentials__ctor_m3149997047 (CognitoAWSCredentials_t2370264792 * __this, String_t* ___accountId0, String_t* ___identityPoolId1, String_t* ___unAuthRoleArn2, String_t* ___authRoleArn3, RegionEndpoint_t661522805 * ___region4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoIdentity.CognitoAWSCredentials::.ctor(System.String,System.String,System.String,System.String,Amazon.CognitoIdentity.IAmazonCognitoIdentity,Amazon.SecurityToken.IAmazonSecurityTokenService)
extern "C"  void CognitoAWSCredentials__ctor_m2238162873 (CognitoAWSCredentials_t2370264792 * __this, String_t* ___accountId0, String_t* ___identityPoolId1, String_t* ___unAuthRoleArn2, String_t* ___authRoleArn3, Il2CppObject * ___cibClient4, Il2CppObject * ___stsClient5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.RefreshingAWSCredentials/CredentialsRefreshState Amazon.CognitoIdentity.CognitoAWSCredentials::GenerateNewCredentials()
extern "C"  CredentialsRefreshState_t3294867821 * CognitoAWSCredentials_GenerateNewCredentials_m1385113071 (CognitoAWSCredentials_t2370264792 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.RefreshingAWSCredentials/CredentialsRefreshState Amazon.CognitoIdentity.CognitoAWSCredentials::GetPoolCredentials()
extern "C"  CredentialsRefreshState_t3294867821 * CognitoAWSCredentials_GetPoolCredentials_m3438202962 (CognitoAWSCredentials_t2370264792 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.RefreshingAWSCredentials/CredentialsRefreshState Amazon.CognitoIdentity.CognitoAWSCredentials::GetCredentialsForRole(System.String)
extern "C"  CredentialsRefreshState_t3294867821 * CognitoAWSCredentials_GetCredentialsForRole_m3530527127 (CognitoAWSCredentials_t2370264792 * __this, String_t* ___roleArn0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.SecurityToken.Model.Credentials Amazon.CognitoIdentity.CognitoAWSCredentials::GetStsCredentials(Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest)
extern "C"  Credentials_t3554032640 * CognitoAWSCredentials_GetStsCredentials_m3127476678 (CognitoAWSCredentials_t2370264792 * __this, AssumeRoleWithWebIdentityRequest_t193094215 * ___assumeRequest0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.CognitoIdentity.Model.GetCredentialsForIdentityResponse Amazon.CognitoIdentity.CognitoAWSCredentials::GetCredentialsForIdentity(Amazon.CognitoIdentity.Model.GetCredentialsForIdentityRequest)
extern "C"  GetCredentialsForIdentityResponse_t2302678766 * CognitoAWSCredentials_GetCredentialsForIdentity_m3961787566 (CognitoAWSCredentials_t2370264792 * __this, GetCredentialsForIdentityRequest_t932830458 * ___getCredentialsRequest0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.CognitoIdentity.Model.GetOpenIdTokenResponse Amazon.CognitoIdentity.CognitoAWSCredentials::GetOpenId(Amazon.CognitoIdentity.Model.GetOpenIdTokenRequest)
extern "C"  GetOpenIdTokenResponse_t955597635 * CognitoAWSCredentials_GetOpenId_m3568919528 (CognitoAWSCredentials_t2370264792 * __this, GetOpenIdTokenRequest_t2698079901 * ___getTokenRequest0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.CognitoIdentity.CognitoAWSCredentials::GetCachedIdentityId()
extern "C"  String_t* CognitoAWSCredentials_GetCachedIdentityId_m4263931988 (CognitoAWSCredentials_t2370264792 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoIdentity.CognitoAWSCredentials::CacheIdentityId(System.String)
extern "C"  void CognitoAWSCredentials_CacheIdentityId_m1798499981 (CognitoAWSCredentials_t2370264792 * __this, String_t* ___identityId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoIdentity.CognitoAWSCredentials::ClearIdentityCache()
extern "C"  void CognitoAWSCredentials_ClearIdentityCache_m2215438351 (CognitoAWSCredentials_t2370264792 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoIdentity.CognitoAWSCredentials::CacheCredentials(Amazon.Runtime.RefreshingAWSCredentials/CredentialsRefreshState)
extern "C"  void CognitoAWSCredentials_CacheCredentials_m2721308407 (CognitoAWSCredentials_t2370264792 * __this, CredentialsRefreshState_t3294867821 * ___credentialsState0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.RefreshingAWSCredentials/CredentialsRefreshState Amazon.CognitoIdentity.CognitoAWSCredentials::GetCachedCredentials()
extern "C"  CredentialsRefreshState_t3294867821 * CognitoAWSCredentials_GetCachedCredentials_m4289102006 (CognitoAWSCredentials_t2370264792 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoIdentity.CognitoAWSCredentials::.cctor()
extern "C"  void CognitoAWSCredentials__cctor_m1379474879 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
