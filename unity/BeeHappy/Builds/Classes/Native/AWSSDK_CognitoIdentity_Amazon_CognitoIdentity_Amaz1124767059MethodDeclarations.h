﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.CognitoIdentity.AmazonCognitoIdentityConfig
struct AmazonCognitoIdentityConfig_t1124767059;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void Amazon.CognitoIdentity.AmazonCognitoIdentityConfig::.ctor()
extern "C"  void AmazonCognitoIdentityConfig__ctor_m680991391 (AmazonCognitoIdentityConfig_t1124767059 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.CognitoIdentity.AmazonCognitoIdentityConfig::get_RegionEndpointServiceName()
extern "C"  String_t* AmazonCognitoIdentityConfig_get_RegionEndpointServiceName_m3891219750 (AmazonCognitoIdentityConfig_t1124767059 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.CognitoIdentity.AmazonCognitoIdentityConfig::get_UserAgent()
extern "C"  String_t* AmazonCognitoIdentityConfig_get_UserAgent_m3071703051 (AmazonCognitoIdentityConfig_t1124767059 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoIdentity.AmazonCognitoIdentityConfig::.cctor()
extern "C"  void AmazonCognitoIdentityConfig__cctor_m647399328 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
