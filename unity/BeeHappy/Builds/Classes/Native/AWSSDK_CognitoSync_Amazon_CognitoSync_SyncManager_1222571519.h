﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.Runtime.Internal.Util.Logger
struct Logger_t2262497814;
// Amazon.CognitoSync.SyncManager.ILocalStorage
struct ILocalStorage_t1053993977;
// Amazon.CognitoSync.SyncManager.Internal.CognitoSyncStorage
struct CognitoSyncStorage_t101590051;
// Amazon.CognitoIdentity.CognitoAWSCredentials
struct CognitoAWSCredentials_t2370264792;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.CognitoSync.SyncManager.CognitoSyncManager
struct  CognitoSyncManager_t1222571519  : public Il2CppObject
{
public:
	// Amazon.Runtime.Internal.Util.Logger Amazon.CognitoSync.SyncManager.CognitoSyncManager::_logger
	Logger_t2262497814 * ____logger_0;
	// System.Boolean Amazon.CognitoSync.SyncManager.CognitoSyncManager::_disposed
	bool ____disposed_1;
	// Amazon.CognitoSync.SyncManager.ILocalStorage Amazon.CognitoSync.SyncManager.CognitoSyncManager::Local
	Il2CppObject * ___Local_2;
	// Amazon.CognitoSync.SyncManager.Internal.CognitoSyncStorage Amazon.CognitoSync.SyncManager.CognitoSyncManager::Remote
	CognitoSyncStorage_t101590051 * ___Remote_3;
	// Amazon.CognitoIdentity.CognitoAWSCredentials Amazon.CognitoSync.SyncManager.CognitoSyncManager::CognitoCredentials
	CognitoAWSCredentials_t2370264792 * ___CognitoCredentials_4;

public:
	inline static int32_t get_offset_of__logger_0() { return static_cast<int32_t>(offsetof(CognitoSyncManager_t1222571519, ____logger_0)); }
	inline Logger_t2262497814 * get__logger_0() const { return ____logger_0; }
	inline Logger_t2262497814 ** get_address_of__logger_0() { return &____logger_0; }
	inline void set__logger_0(Logger_t2262497814 * value)
	{
		____logger_0 = value;
		Il2CppCodeGenWriteBarrier(&____logger_0, value);
	}

	inline static int32_t get_offset_of__disposed_1() { return static_cast<int32_t>(offsetof(CognitoSyncManager_t1222571519, ____disposed_1)); }
	inline bool get__disposed_1() const { return ____disposed_1; }
	inline bool* get_address_of__disposed_1() { return &____disposed_1; }
	inline void set__disposed_1(bool value)
	{
		____disposed_1 = value;
	}

	inline static int32_t get_offset_of_Local_2() { return static_cast<int32_t>(offsetof(CognitoSyncManager_t1222571519, ___Local_2)); }
	inline Il2CppObject * get_Local_2() const { return ___Local_2; }
	inline Il2CppObject ** get_address_of_Local_2() { return &___Local_2; }
	inline void set_Local_2(Il2CppObject * value)
	{
		___Local_2 = value;
		Il2CppCodeGenWriteBarrier(&___Local_2, value);
	}

	inline static int32_t get_offset_of_Remote_3() { return static_cast<int32_t>(offsetof(CognitoSyncManager_t1222571519, ___Remote_3)); }
	inline CognitoSyncStorage_t101590051 * get_Remote_3() const { return ___Remote_3; }
	inline CognitoSyncStorage_t101590051 ** get_address_of_Remote_3() { return &___Remote_3; }
	inline void set_Remote_3(CognitoSyncStorage_t101590051 * value)
	{
		___Remote_3 = value;
		Il2CppCodeGenWriteBarrier(&___Remote_3, value);
	}

	inline static int32_t get_offset_of_CognitoCredentials_4() { return static_cast<int32_t>(offsetof(CognitoSyncManager_t1222571519, ___CognitoCredentials_4)); }
	inline CognitoAWSCredentials_t2370264792 * get_CognitoCredentials_4() const { return ___CognitoCredentials_4; }
	inline CognitoAWSCredentials_t2370264792 ** get_address_of_CognitoCredentials_4() { return &___CognitoCredentials_4; }
	inline void set_CognitoCredentials_4(CognitoAWSCredentials_t2370264792 * value)
	{
		___CognitoCredentials_4 = value;
		Il2CppCodeGenWriteBarrier(&___CognitoCredentials_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
