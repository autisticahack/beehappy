﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Amazon.Runtime.Internal.Util.InternalLogger>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m208994533(__this, ___l0, method) ((  void (*) (Enumerator_t1876224149 *, List_1_t2341494475 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Amazon.Runtime.Internal.Util.InternalLogger>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2286725813(__this, method) ((  void (*) (Enumerator_t1876224149 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Amazon.Runtime.Internal.Util.InternalLogger>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3105440429(__this, method) ((  Il2CppObject * (*) (Enumerator_t1876224149 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Amazon.Runtime.Internal.Util.InternalLogger>::Dispose()
#define Enumerator_Dispose_m1495939816(__this, method) ((  void (*) (Enumerator_t1876224149 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Amazon.Runtime.Internal.Util.InternalLogger>::VerifyState()
#define Enumerator_VerifyState_m2962620159(__this, method) ((  void (*) (Enumerator_t1876224149 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Amazon.Runtime.Internal.Util.InternalLogger>::MoveNext()
#define Enumerator_MoveNext_m3111811052(__this, method) ((  bool (*) (Enumerator_t1876224149 *, const MethodInfo*))Enumerator_MoveNext_m984484162_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Amazon.Runtime.Internal.Util.InternalLogger>::get_Current()
#define Enumerator_get_Current_m1292891094(__this, method) ((  InternalLogger_t2972373343 * (*) (Enumerator_t1876224149 *, const MethodInfo*))Enumerator_get_Current_m2193722336_gshared)(__this, method)
