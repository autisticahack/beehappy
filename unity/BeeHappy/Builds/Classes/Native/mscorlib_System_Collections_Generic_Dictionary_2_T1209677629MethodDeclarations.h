﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ThirdParty.Json.LitJson.ObjectMetadata,ThirdParty.Json.LitJson.ObjectMetadata>
struct Transform_1_t1209677629;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_ObjectMetadata4058137740.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ThirdParty.Json.LitJson.ObjectMetadata,ThirdParty.Json.LitJson.ObjectMetadata>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2356499999_gshared (Transform_1_t1209677629 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Transform_1__ctor_m2356499999(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t1209677629 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m2356499999_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ThirdParty.Json.LitJson.ObjectMetadata,ThirdParty.Json.LitJson.ObjectMetadata>::Invoke(TKey,TValue)
extern "C"  ObjectMetadata_t4058137740  Transform_1_Invoke_m3678771547_gshared (Transform_1_t1209677629 * __this, Il2CppObject * ___key0, ObjectMetadata_t4058137740  ___value1, const MethodInfo* method);
#define Transform_1_Invoke_m3678771547(__this, ___key0, ___value1, method) ((  ObjectMetadata_t4058137740  (*) (Transform_1_t1209677629 *, Il2CppObject *, ObjectMetadata_t4058137740 , const MethodInfo*))Transform_1_Invoke_m3678771547_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ThirdParty.Json.LitJson.ObjectMetadata,ThirdParty.Json.LitJson.ObjectMetadata>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1969615422_gshared (Transform_1_t1209677629 * __this, Il2CppObject * ___key0, ObjectMetadata_t4058137740  ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define Transform_1_BeginInvoke_m1969615422(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t1209677629 *, Il2CppObject *, ObjectMetadata_t4058137740 , AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m1969615422_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ThirdParty.Json.LitJson.ObjectMetadata,ThirdParty.Json.LitJson.ObjectMetadata>::EndInvoke(System.IAsyncResult)
extern "C"  ObjectMetadata_t4058137740  Transform_1_EndInvoke_m3217343613_gshared (Transform_1_t1209677629 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Transform_1_EndInvoke_m3217343613(__this, ___result0, method) ((  ObjectMetadata_t4058137740  (*) (Transform_1_t1209677629 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m3217343613_gshared)(__this, ___result0, method)
