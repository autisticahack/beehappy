﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Data.FillErrorEventHandler
struct FillErrorEventHandler_t767070689;
// System.Object
struct Il2CppObject;
// System.Data.FillErrorEventArgs
struct FillErrorEventArgs_t1074279162;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "System_Data_System_Data_FillErrorEventArgs1074279162.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void System.Data.FillErrorEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void FillErrorEventHandler__ctor_m2336423646 (FillErrorEventHandler_t767070689 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.FillErrorEventHandler::Invoke(System.Object,System.Data.FillErrorEventArgs)
extern "C"  void FillErrorEventHandler_Invoke_m3470872185 (FillErrorEventHandler_t767070689 * __this, Il2CppObject * ___sender0, FillErrorEventArgs_t1074279162 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult System.Data.FillErrorEventHandler::BeginInvoke(System.Object,System.Data.FillErrorEventArgs,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * FillErrorEventHandler_BeginInvoke_m2003176970 (FillErrorEventHandler_t767070689 * __this, Il2CppObject * ___sender0, FillErrorEventArgs_t1074279162 * ___e1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.FillErrorEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void FillErrorEventHandler_EndInvoke_m2837513312 (FillErrorEventHandler_t767070689 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
