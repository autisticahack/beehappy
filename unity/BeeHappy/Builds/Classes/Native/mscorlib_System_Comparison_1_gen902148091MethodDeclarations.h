﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Comparison_1_gen1300593496MethodDeclarations.h"

// System.Void System.Comparison`1<System.Collections.Generic.KeyValuePair`2<System.String,ThirdParty.Json.LitJson.JsonData>>::.ctor(System.Object,System.IntPtr)
#define Comparison_1__ctor_m3238264000(__this, ___object0, ___method1, method) ((  void (*) (Comparison_1_t902148091 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m258655046_gshared)(__this, ___object0, ___method1, method)
// System.Int32 System.Comparison`1<System.Collections.Generic.KeyValuePair`2<System.String,ThirdParty.Json.LitJson.JsonData>>::Invoke(T,T)
#define Comparison_1_Invoke_m1722783380(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparison_1_t902148091 *, KeyValuePair_2_t3935376536 , KeyValuePair_2_t3935376536 , const MethodInfo*))Comparison_1_Invoke_m2407609722_gshared)(__this, ___x0, ___y1, method)
// System.IAsyncResult System.Comparison`1<System.Collections.Generic.KeyValuePair`2<System.String,ThirdParty.Json.LitJson.JsonData>>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
#define Comparison_1_BeginInvoke_m2821084595(__this, ___x0, ___y1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Comparison_1_t902148091 *, KeyValuePair_2_t3935376536 , KeyValuePair_2_t3935376536 , AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Comparison_1_BeginInvoke_m1988197935_gshared)(__this, ___x0, ___y1, ___callback2, ___object3, method)
// System.Int32 System.Comparison`1<System.Collections.Generic.KeyValuePair`2<System.String,ThirdParty.Json.LitJson.JsonData>>::EndInvoke(System.IAsyncResult)
#define Comparison_1_EndInvoke_m2021457318(__this, ___result0, method) ((  int32_t (*) (Comparison_1_t902148091 *, Il2CppObject *, const MethodInfo*))Comparison_1_EndInvoke_m2681344252_gshared)(__this, ___result0, method)
