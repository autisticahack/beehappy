﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// System.Collections.Generic.IDictionary`2<System.String,System.String>
struct IDictionary_2_t1943082916;
// System.Uri
struct Uri_t19570940;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// Amazon.Runtime.Internal.Util.BackgroundInvoker
struct BackgroundInvoker_t1722929158;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Uri19570940.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_DateTime693205669.h"
#include "mscorlib_System_Object2689449295.h"

// System.String Amazon.Util.AWSSDKUtils::DetermineValidPathCharacters()
extern "C"  String_t* AWSSDKUtils_DetermineValidPathCharacters_m1449869222 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Util.AWSSDKUtils::GetParametersAsString(System.Collections.Generic.IDictionary`2<System.String,System.String>)
extern "C"  String_t* AWSSDKUtils_GetParametersAsString_m3230029975 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___parameters0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Util.AWSSDKUtils::CanonicalizeResourcePath(System.Uri,System.String)
extern "C"  String_t* AWSSDKUtils_CanonicalizeResourcePath_m1550332836 (Il2CppObject * __this /* static, unused */, Uri_t19570940 * ___endpoint0, String_t* ___resourcePath1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Util.AWSSDKUtils::DetermineRegion(System.String)
extern "C"  String_t* AWSSDKUtils_DetermineRegion_m2367080605 (Il2CppObject * __this /* static, unused */, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Util.AWSSDKUtils::DetermineService(System.String)
extern "C"  String_t* AWSSDKUtils_DetermineService_m2995691942 (Il2CppObject * __this /* static, unused */, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double Amazon.Util.AWSSDKUtils::ConvertToUnixEpochMilliSeconds(System.DateTime)
extern "C"  double AWSSDKUtils_ConvertToUnixEpochMilliSeconds_m936372869 (Il2CppObject * __this /* static, unused */, DateTime_t693205669  ___dateTime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Util.AWSSDKUtils::ToHex(System.Byte[],System.Boolean)
extern "C"  String_t* AWSSDKUtils_ToHex_m2545940182 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___data0, bool ___lowercase1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.Internal.Util.BackgroundInvoker Amazon.Util.AWSSDKUtils::get_Dispatcher()
extern "C"  BackgroundInvoker_t1722929158 * AWSSDKUtils_get_Dispatcher_m3784221935 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Util.AWSSDKUtils::AreEqual(System.Object[],System.Object[])
extern "C"  bool AWSSDKUtils_AreEqual_m3433181997 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t3614634134* ___itemsA0, ObjectU5BU5D_t3614634134* ___itemsB1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Util.AWSSDKUtils::AreEqual(System.Object,System.Object)
extern "C"  bool AWSSDKUtils_AreEqual_m3682793581 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___a0, Il2CppObject * ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Util.AWSSDKUtils::UrlEncode(System.String,System.Boolean)
extern "C"  String_t* AWSSDKUtils_UrlEncode_m2320844696 (Il2CppObject * __this /* static, unused */, String_t* ___data0, bool ___path1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Util.AWSSDKUtils::UrlEncode(System.Int32,System.String,System.Boolean)
extern "C"  String_t* AWSSDKUtils_UrlEncode_m2674052371 (Il2CppObject * __this /* static, unused */, int32_t ___rfcNumber0, String_t* ___data1, bool ___path2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime Amazon.Util.AWSSDKUtils::get_CorrectedUtcNow()
extern "C"  DateTime_t693205669  AWSSDKUtils_get_CorrectedUtcNow_m2838950296 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Util.AWSSDKUtils::HasBidiControlCharacters(System.String)
extern "C"  bool AWSSDKUtils_HasBidiControlCharacters_m1397687428 (Il2CppObject * __this /* static, unused */, String_t* ___input0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Util.AWSSDKUtils::IsBidiControlChar(System.Char)
extern "C"  bool AWSSDKUtils_IsBidiControlChar_m1392594707 (Il2CppObject * __this /* static, unused */, Il2CppChar ___c0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Util.AWSSDKUtils::Sleep(System.Int32)
extern "C"  void AWSSDKUtils_Sleep_m332852201 (Il2CppObject * __this /* static, unused */, int32_t ___ms0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Util.AWSSDKUtils::get_IsIL2CPP()
extern "C"  bool AWSSDKUtils_get_IsIL2CPP_m3178093986 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Util.AWSSDKUtils::.cctor()
extern "C"  void AWSSDKUtils__cctor_m2627013042 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
