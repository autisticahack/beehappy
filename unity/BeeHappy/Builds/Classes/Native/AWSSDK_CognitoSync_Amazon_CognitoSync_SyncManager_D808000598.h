﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.Record>
struct List_1_t237920701;
// Amazon.CognitoSync.SyncManager.Dataset
struct Dataset_t3040902086;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.CognitoSync.SyncManager.Dataset/<>c__DisplayClass72_0
struct  U3CU3Ec__DisplayClass72_0_t808000598  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.Record> Amazon.CognitoSync.SyncManager.Dataset/<>c__DisplayClass72_0::records
	List_1_t237920701 * ___records_0;
	// Amazon.CognitoSync.SyncManager.Dataset Amazon.CognitoSync.SyncManager.Dataset/<>c__DisplayClass72_0::<>4__this
	Dataset_t3040902086 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_records_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass72_0_t808000598, ___records_0)); }
	inline List_1_t237920701 * get_records_0() const { return ___records_0; }
	inline List_1_t237920701 ** get_address_of_records_0() { return &___records_0; }
	inline void set_records_0(List_1_t237920701 * value)
	{
		___records_0 = value;
		Il2CppCodeGenWriteBarrier(&___records_0, value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass72_0_t808000598, ___U3CU3E4__this_1)); }
	inline Dataset_t3040902086 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline Dataset_t3040902086 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(Dataset_t3040902086 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E4__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
