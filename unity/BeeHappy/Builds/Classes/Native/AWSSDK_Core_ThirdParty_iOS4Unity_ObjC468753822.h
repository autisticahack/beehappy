﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ThirdParty.iOS4Unity.Selector
struct Selector_t1939762325;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.iOS4Unity.ObjC
struct  ObjC_t468753822  : public Il2CppObject
{
public:

public:
};

struct ObjC_t468753822_StaticFields
{
public:
	// ThirdParty.iOS4Unity.Selector ThirdParty.iOS4Unity.ObjC::TimeIntervalSinceReferenceDateSelector
	Selector_t1939762325 * ___TimeIntervalSinceReferenceDateSelector_0;
	// ThirdParty.iOS4Unity.Selector ThirdParty.iOS4Unity.ObjC::AbsoluteStringSelector
	Selector_t1939762325 * ___AbsoluteStringSelector_1;
	// ThirdParty.iOS4Unity.Selector ThirdParty.iOS4Unity.ObjC::DoubleValueSelector
	Selector_t1939762325 * ___DoubleValueSelector_2;
	// ThirdParty.iOS4Unity.Selector ThirdParty.iOS4Unity.ObjC::CountSelector
	Selector_t1939762325 * ___CountSelector_3;
	// ThirdParty.iOS4Unity.Selector ThirdParty.iOS4Unity.ObjC::ObjectAtIndexSelector
	Selector_t1939762325 * ___ObjectAtIndexSelector_4;
	// ThirdParty.iOS4Unity.Selector ThirdParty.iOS4Unity.ObjC::AllObjectsSelector
	Selector_t1939762325 * ___AllObjectsSelector_5;
	// ThirdParty.iOS4Unity.Selector ThirdParty.iOS4Unity.ObjC::ArrayWithObjects_CountSelector
	Selector_t1939762325 * ___ArrayWithObjects_CountSelector_6;
	// ThirdParty.iOS4Unity.Selector ThirdParty.iOS4Unity.ObjC::SetWithArraySelector
	Selector_t1939762325 * ___SetWithArraySelector_7;
	// ThirdParty.iOS4Unity.Selector ThirdParty.iOS4Unity.ObjC::InitWithCharacters_lengthSelector
	Selector_t1939762325 * ___InitWithCharacters_lengthSelector_8;
	// ThirdParty.iOS4Unity.Selector ThirdParty.iOS4Unity.ObjC::URLWithStringSelector
	Selector_t1939762325 * ___URLWithStringSelector_9;
	// ThirdParty.iOS4Unity.Selector ThirdParty.iOS4Unity.ObjC::DateWithTimeIntervalSinceReferenceDateSelector
	Selector_t1939762325 * ___DateWithTimeIntervalSinceReferenceDateSelector_10;
	// ThirdParty.iOS4Unity.Selector ThirdParty.iOS4Unity.ObjC::InitWithDoubleSelector
	Selector_t1939762325 * ___InitWithDoubleSelector_11;

public:
	inline static int32_t get_offset_of_TimeIntervalSinceReferenceDateSelector_0() { return static_cast<int32_t>(offsetof(ObjC_t468753822_StaticFields, ___TimeIntervalSinceReferenceDateSelector_0)); }
	inline Selector_t1939762325 * get_TimeIntervalSinceReferenceDateSelector_0() const { return ___TimeIntervalSinceReferenceDateSelector_0; }
	inline Selector_t1939762325 ** get_address_of_TimeIntervalSinceReferenceDateSelector_0() { return &___TimeIntervalSinceReferenceDateSelector_0; }
	inline void set_TimeIntervalSinceReferenceDateSelector_0(Selector_t1939762325 * value)
	{
		___TimeIntervalSinceReferenceDateSelector_0 = value;
		Il2CppCodeGenWriteBarrier(&___TimeIntervalSinceReferenceDateSelector_0, value);
	}

	inline static int32_t get_offset_of_AbsoluteStringSelector_1() { return static_cast<int32_t>(offsetof(ObjC_t468753822_StaticFields, ___AbsoluteStringSelector_1)); }
	inline Selector_t1939762325 * get_AbsoluteStringSelector_1() const { return ___AbsoluteStringSelector_1; }
	inline Selector_t1939762325 ** get_address_of_AbsoluteStringSelector_1() { return &___AbsoluteStringSelector_1; }
	inline void set_AbsoluteStringSelector_1(Selector_t1939762325 * value)
	{
		___AbsoluteStringSelector_1 = value;
		Il2CppCodeGenWriteBarrier(&___AbsoluteStringSelector_1, value);
	}

	inline static int32_t get_offset_of_DoubleValueSelector_2() { return static_cast<int32_t>(offsetof(ObjC_t468753822_StaticFields, ___DoubleValueSelector_2)); }
	inline Selector_t1939762325 * get_DoubleValueSelector_2() const { return ___DoubleValueSelector_2; }
	inline Selector_t1939762325 ** get_address_of_DoubleValueSelector_2() { return &___DoubleValueSelector_2; }
	inline void set_DoubleValueSelector_2(Selector_t1939762325 * value)
	{
		___DoubleValueSelector_2 = value;
		Il2CppCodeGenWriteBarrier(&___DoubleValueSelector_2, value);
	}

	inline static int32_t get_offset_of_CountSelector_3() { return static_cast<int32_t>(offsetof(ObjC_t468753822_StaticFields, ___CountSelector_3)); }
	inline Selector_t1939762325 * get_CountSelector_3() const { return ___CountSelector_3; }
	inline Selector_t1939762325 ** get_address_of_CountSelector_3() { return &___CountSelector_3; }
	inline void set_CountSelector_3(Selector_t1939762325 * value)
	{
		___CountSelector_3 = value;
		Il2CppCodeGenWriteBarrier(&___CountSelector_3, value);
	}

	inline static int32_t get_offset_of_ObjectAtIndexSelector_4() { return static_cast<int32_t>(offsetof(ObjC_t468753822_StaticFields, ___ObjectAtIndexSelector_4)); }
	inline Selector_t1939762325 * get_ObjectAtIndexSelector_4() const { return ___ObjectAtIndexSelector_4; }
	inline Selector_t1939762325 ** get_address_of_ObjectAtIndexSelector_4() { return &___ObjectAtIndexSelector_4; }
	inline void set_ObjectAtIndexSelector_4(Selector_t1939762325 * value)
	{
		___ObjectAtIndexSelector_4 = value;
		Il2CppCodeGenWriteBarrier(&___ObjectAtIndexSelector_4, value);
	}

	inline static int32_t get_offset_of_AllObjectsSelector_5() { return static_cast<int32_t>(offsetof(ObjC_t468753822_StaticFields, ___AllObjectsSelector_5)); }
	inline Selector_t1939762325 * get_AllObjectsSelector_5() const { return ___AllObjectsSelector_5; }
	inline Selector_t1939762325 ** get_address_of_AllObjectsSelector_5() { return &___AllObjectsSelector_5; }
	inline void set_AllObjectsSelector_5(Selector_t1939762325 * value)
	{
		___AllObjectsSelector_5 = value;
		Il2CppCodeGenWriteBarrier(&___AllObjectsSelector_5, value);
	}

	inline static int32_t get_offset_of_ArrayWithObjects_CountSelector_6() { return static_cast<int32_t>(offsetof(ObjC_t468753822_StaticFields, ___ArrayWithObjects_CountSelector_6)); }
	inline Selector_t1939762325 * get_ArrayWithObjects_CountSelector_6() const { return ___ArrayWithObjects_CountSelector_6; }
	inline Selector_t1939762325 ** get_address_of_ArrayWithObjects_CountSelector_6() { return &___ArrayWithObjects_CountSelector_6; }
	inline void set_ArrayWithObjects_CountSelector_6(Selector_t1939762325 * value)
	{
		___ArrayWithObjects_CountSelector_6 = value;
		Il2CppCodeGenWriteBarrier(&___ArrayWithObjects_CountSelector_6, value);
	}

	inline static int32_t get_offset_of_SetWithArraySelector_7() { return static_cast<int32_t>(offsetof(ObjC_t468753822_StaticFields, ___SetWithArraySelector_7)); }
	inline Selector_t1939762325 * get_SetWithArraySelector_7() const { return ___SetWithArraySelector_7; }
	inline Selector_t1939762325 ** get_address_of_SetWithArraySelector_7() { return &___SetWithArraySelector_7; }
	inline void set_SetWithArraySelector_7(Selector_t1939762325 * value)
	{
		___SetWithArraySelector_7 = value;
		Il2CppCodeGenWriteBarrier(&___SetWithArraySelector_7, value);
	}

	inline static int32_t get_offset_of_InitWithCharacters_lengthSelector_8() { return static_cast<int32_t>(offsetof(ObjC_t468753822_StaticFields, ___InitWithCharacters_lengthSelector_8)); }
	inline Selector_t1939762325 * get_InitWithCharacters_lengthSelector_8() const { return ___InitWithCharacters_lengthSelector_8; }
	inline Selector_t1939762325 ** get_address_of_InitWithCharacters_lengthSelector_8() { return &___InitWithCharacters_lengthSelector_8; }
	inline void set_InitWithCharacters_lengthSelector_8(Selector_t1939762325 * value)
	{
		___InitWithCharacters_lengthSelector_8 = value;
		Il2CppCodeGenWriteBarrier(&___InitWithCharacters_lengthSelector_8, value);
	}

	inline static int32_t get_offset_of_URLWithStringSelector_9() { return static_cast<int32_t>(offsetof(ObjC_t468753822_StaticFields, ___URLWithStringSelector_9)); }
	inline Selector_t1939762325 * get_URLWithStringSelector_9() const { return ___URLWithStringSelector_9; }
	inline Selector_t1939762325 ** get_address_of_URLWithStringSelector_9() { return &___URLWithStringSelector_9; }
	inline void set_URLWithStringSelector_9(Selector_t1939762325 * value)
	{
		___URLWithStringSelector_9 = value;
		Il2CppCodeGenWriteBarrier(&___URLWithStringSelector_9, value);
	}

	inline static int32_t get_offset_of_DateWithTimeIntervalSinceReferenceDateSelector_10() { return static_cast<int32_t>(offsetof(ObjC_t468753822_StaticFields, ___DateWithTimeIntervalSinceReferenceDateSelector_10)); }
	inline Selector_t1939762325 * get_DateWithTimeIntervalSinceReferenceDateSelector_10() const { return ___DateWithTimeIntervalSinceReferenceDateSelector_10; }
	inline Selector_t1939762325 ** get_address_of_DateWithTimeIntervalSinceReferenceDateSelector_10() { return &___DateWithTimeIntervalSinceReferenceDateSelector_10; }
	inline void set_DateWithTimeIntervalSinceReferenceDateSelector_10(Selector_t1939762325 * value)
	{
		___DateWithTimeIntervalSinceReferenceDateSelector_10 = value;
		Il2CppCodeGenWriteBarrier(&___DateWithTimeIntervalSinceReferenceDateSelector_10, value);
	}

	inline static int32_t get_offset_of_InitWithDoubleSelector_11() { return static_cast<int32_t>(offsetof(ObjC_t468753822_StaticFields, ___InitWithDoubleSelector_11)); }
	inline Selector_t1939762325 * get_InitWithDoubleSelector_11() const { return ___InitWithDoubleSelector_11; }
	inline Selector_t1939762325 ** get_address_of_InitWithDoubleSelector_11() { return &___InitWithDoubleSelector_11; }
	inline void set_InitWithDoubleSelector_11(Selector_t1939762325 * value)
	{
		___InitWithDoubleSelector_11 = value;
		Il2CppCodeGenWriteBarrier(&___InitWithDoubleSelector_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
