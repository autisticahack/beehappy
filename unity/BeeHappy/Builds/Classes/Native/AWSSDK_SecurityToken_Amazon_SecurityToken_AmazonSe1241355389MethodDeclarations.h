﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.SecurityToken.AmazonSecurityTokenServiceClient
struct AmazonSecurityTokenServiceClient_t1241355389;
// Amazon.Runtime.AWSCredentials
struct AWSCredentials_t3583921007;
// Amazon.RegionEndpoint
struct RegionEndpoint_t661522805;
// Amazon.SecurityToken.AmazonSecurityTokenServiceConfig
struct AmazonSecurityTokenServiceConfig_t1261559370;
// Amazon.Runtime.Internal.Auth.AbstractAWSSigner
struct AbstractAWSSigner_t2114314031;
// Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest
struct AssumeRoleWithWebIdentityRequest_t193094215;
// Amazon.Runtime.AmazonServiceCallback`2<Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest,Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityResponse>
struct AmazonServiceCallback_2_t142122009;
// Amazon.Runtime.AsyncOptions
struct AsyncOptions_t558351272;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_Runtime_AWSCredentials3583921007.h"
#include "AWSSDK_Core_Amazon_RegionEndpoint661522805.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_AmazonSe1261559370.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_Model_Ass193094215.h"
#include "AWSSDK_Core_Amazon_Runtime_AsyncOptions558351272.h"

// System.Void Amazon.SecurityToken.AmazonSecurityTokenServiceClient::.ctor(Amazon.Runtime.AWSCredentials,Amazon.RegionEndpoint)
extern "C"  void AmazonSecurityTokenServiceClient__ctor_m77079877 (AmazonSecurityTokenServiceClient_t1241355389 * __this, AWSCredentials_t3583921007 * ___credentials0, RegionEndpoint_t661522805 * ___region1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.SecurityToken.AmazonSecurityTokenServiceClient::.ctor(Amazon.Runtime.AWSCredentials,Amazon.SecurityToken.AmazonSecurityTokenServiceConfig)
extern "C"  void AmazonSecurityTokenServiceClient__ctor_m3543846829 (AmazonSecurityTokenServiceClient_t1241355389 * __this, AWSCredentials_t3583921007 * ___credentials0, AmazonSecurityTokenServiceConfig_t1261559370 * ___clientConfig1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.Internal.Auth.AbstractAWSSigner Amazon.SecurityToken.AmazonSecurityTokenServiceClient::CreateSigner()
extern "C"  AbstractAWSSigner_t2114314031 * AmazonSecurityTokenServiceClient_CreateSigner_m2338458870 (AmazonSecurityTokenServiceClient_t1241355389 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.SecurityToken.AmazonSecurityTokenServiceClient::Dispose(System.Boolean)
extern "C"  void AmazonSecurityTokenServiceClient_Dispose_m824202305 (AmazonSecurityTokenServiceClient_t1241355389 * __this, bool ___disposing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.SecurityToken.AmazonSecurityTokenServiceClient::AssumeRoleWithWebIdentityAsync(Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest,Amazon.Runtime.AmazonServiceCallback`2<Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest,Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityResponse>,Amazon.Runtime.AsyncOptions)
extern "C"  void AmazonSecurityTokenServiceClient_AssumeRoleWithWebIdentityAsync_m255795346 (AmazonSecurityTokenServiceClient_t1241355389 * __this, AssumeRoleWithWebIdentityRequest_t193094215 * ___request0, AmazonServiceCallback_2_t142122009 * ___callback1, AsyncOptions_t558351272 * ___options2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
