﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.RegionEndpoint
struct RegionEndpoint_t661522805;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.AWSRegion
struct  AWSRegion_t969138115  : public Il2CppObject
{
public:
	// Amazon.RegionEndpoint Amazon.Runtime.AWSRegion::<Region>k__BackingField
	RegionEndpoint_t661522805 * ___U3CRegionU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CRegionU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AWSRegion_t969138115, ___U3CRegionU3Ek__BackingField_0)); }
	inline RegionEndpoint_t661522805 * get_U3CRegionU3Ek__BackingField_0() const { return ___U3CRegionU3Ek__BackingField_0; }
	inline RegionEndpoint_t661522805 ** get_address_of_U3CRegionU3Ek__BackingField_0() { return &___U3CRegionU3Ek__BackingField_0; }
	inline void set_U3CRegionU3Ek__BackingField_0(RegionEndpoint_t661522805 * value)
	{
		___U3CRegionU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CRegionU3Ek__BackingField_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
