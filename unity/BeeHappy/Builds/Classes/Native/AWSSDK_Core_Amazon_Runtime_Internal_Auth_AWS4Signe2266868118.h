﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.Runtime.Internal.Auth.AWS4Signer/<>c
struct U3CU3Ec_t2266868118;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,System.String>,System.String>
struct Func_2_t4206299577;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,System.String>,System.Boolean>
struct Func_2_t1707686766;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Auth.AWS4Signer/<>c
struct  U3CU3Ec_t2266868118  : public Il2CppObject
{
public:

public:
};

struct U3CU3Ec_t2266868118_StaticFields
{
public:
	// Amazon.Runtime.Internal.Auth.AWS4Signer/<>c Amazon.Runtime.Internal.Auth.AWS4Signer/<>c::<>9
	U3CU3Ec_t2266868118 * ___U3CU3E9_0;
	// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,System.String>,System.String> Amazon.Runtime.Internal.Auth.AWS4Signer/<>c::<>9__36_0
	Func_2_t4206299577 * ___U3CU3E9__36_0_1;
	// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,System.String>,System.String> Amazon.Runtime.Internal.Auth.AWS4Signer/<>c::<>9__37_0
	Func_2_t4206299577 * ___U3CU3E9__37_0_2;
	// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,System.String>,System.Boolean> Amazon.Runtime.Internal.Auth.AWS4Signer/<>c::<>9__38_0
	Func_2_t1707686766 * ___U3CU3E9__38_0_3;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2266868118_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t2266868118 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t2266868118 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t2266868118 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9_0, value);
	}

	inline static int32_t get_offset_of_U3CU3E9__36_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2266868118_StaticFields, ___U3CU3E9__36_0_1)); }
	inline Func_2_t4206299577 * get_U3CU3E9__36_0_1() const { return ___U3CU3E9__36_0_1; }
	inline Func_2_t4206299577 ** get_address_of_U3CU3E9__36_0_1() { return &___U3CU3E9__36_0_1; }
	inline void set_U3CU3E9__36_0_1(Func_2_t4206299577 * value)
	{
		___U3CU3E9__36_0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9__36_0_1, value);
	}

	inline static int32_t get_offset_of_U3CU3E9__37_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2266868118_StaticFields, ___U3CU3E9__37_0_2)); }
	inline Func_2_t4206299577 * get_U3CU3E9__37_0_2() const { return ___U3CU3E9__37_0_2; }
	inline Func_2_t4206299577 ** get_address_of_U3CU3E9__37_0_2() { return &___U3CU3E9__37_0_2; }
	inline void set_U3CU3E9__37_0_2(Func_2_t4206299577 * value)
	{
		___U3CU3E9__37_0_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9__37_0_2, value);
	}

	inline static int32_t get_offset_of_U3CU3E9__38_0_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2266868118_StaticFields, ___U3CU3E9__38_0_3)); }
	inline Func_2_t1707686766 * get_U3CU3E9__38_0_3() const { return ___U3CU3E9__38_0_3; }
	inline Func_2_t1707686766 ** get_address_of_U3CU3E9__38_0_3() { return &___U3CU3E9__38_0_3; }
	inline void set_U3CU3E9__38_0_3(Func_2_t1707686766 * value)
	{
		___U3CU3E9__38_0_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9__38_0_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
