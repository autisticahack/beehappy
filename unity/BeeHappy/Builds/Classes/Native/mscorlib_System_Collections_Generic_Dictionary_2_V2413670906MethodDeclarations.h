﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>
struct Dictionary_2_t727138142;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2413670906.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_ArrayMetadata1135078014.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3721515377_gshared (Enumerator_t2413670906 * __this, Dictionary_2_t727138142 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m3721515377(__this, ___host0, method) ((  void (*) (Enumerator_t2413670906 *, Dictionary_2_t727138142 *, const MethodInfo*))Enumerator__ctor_m3721515377_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2245862918_gshared (Enumerator_t2413670906 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2245862918(__this, method) ((  Il2CppObject * (*) (Enumerator_t2413670906 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2245862918_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4229587238_gshared (Enumerator_t2413670906 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m4229587238(__this, method) ((  void (*) (Enumerator_t2413670906 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m4229587238_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::Dispose()
extern "C"  void Enumerator_Dispose_m1982504185_gshared (Enumerator_t2413670906 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1982504185(__this, method) ((  void (*) (Enumerator_t2413670906 *, const MethodInfo*))Enumerator_Dispose_m1982504185_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3949836258_gshared (Enumerator_t2413670906 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3949836258(__this, method) ((  bool (*) (Enumerator_t2413670906 *, const MethodInfo*))Enumerator_MoveNext_m3949836258_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::get_Current()
extern "C"  ArrayMetadata_t1135078014  Enumerator_get_Current_m148336996_gshared (Enumerator_t2413670906 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m148336996(__this, method) ((  ArrayMetadata_t1135078014  (*) (Enumerator_t2413670906 *, const MethodInfo*))Enumerator_get_Current_m148336996_gshared)(__this, method)
