﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ThirdParty.iOS4Unity.SKPaymentTransaction
struct SKPaymentTransaction_t2918057744;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr2504060609.h"

// System.Void ThirdParty.iOS4Unity.SKPaymentTransaction::.cctor()
extern "C"  void SKPaymentTransaction__cctor_m3267372721 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.iOS4Unity.SKPaymentTransaction::.ctor(System.IntPtr)
extern "C"  void SKPaymentTransaction__ctor_m1324688674 (SKPaymentTransaction_t2918057744 * __this, IntPtr_t ___handle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
