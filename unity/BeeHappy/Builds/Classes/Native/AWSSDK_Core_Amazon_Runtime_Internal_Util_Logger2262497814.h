﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.IDictionary`2<System.Type,Amazon.Runtime.Internal.Util.Logger>
struct IDictionary_2_t2198939132;
// System.Collections.Generic.List`1<Amazon.Runtime.Internal.Util.InternalLogger>
struct List_1_t2341494475;
// Amazon.Runtime.Internal.Util.Logger
struct Logger_t2262497814;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Util.Logger
struct  Logger_t2262497814  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<Amazon.Runtime.Internal.Util.InternalLogger> Amazon.Runtime.Internal.Util.Logger::loggers
	List_1_t2341494475 * ___loggers_1;

public:
	inline static int32_t get_offset_of_loggers_1() { return static_cast<int32_t>(offsetof(Logger_t2262497814, ___loggers_1)); }
	inline List_1_t2341494475 * get_loggers_1() const { return ___loggers_1; }
	inline List_1_t2341494475 ** get_address_of_loggers_1() { return &___loggers_1; }
	inline void set_loggers_1(List_1_t2341494475 * value)
	{
		___loggers_1 = value;
		Il2CppCodeGenWriteBarrier(&___loggers_1, value);
	}
};

struct Logger_t2262497814_StaticFields
{
public:
	// System.Collections.Generic.IDictionary`2<System.Type,Amazon.Runtime.Internal.Util.Logger> Amazon.Runtime.Internal.Util.Logger::cachedLoggers
	Il2CppObject* ___cachedLoggers_0;
	// Amazon.Runtime.Internal.Util.Logger Amazon.Runtime.Internal.Util.Logger::emptyLogger
	Logger_t2262497814 * ___emptyLogger_2;

public:
	inline static int32_t get_offset_of_cachedLoggers_0() { return static_cast<int32_t>(offsetof(Logger_t2262497814_StaticFields, ___cachedLoggers_0)); }
	inline Il2CppObject* get_cachedLoggers_0() const { return ___cachedLoggers_0; }
	inline Il2CppObject** get_address_of_cachedLoggers_0() { return &___cachedLoggers_0; }
	inline void set_cachedLoggers_0(Il2CppObject* value)
	{
		___cachedLoggers_0 = value;
		Il2CppCodeGenWriteBarrier(&___cachedLoggers_0, value);
	}

	inline static int32_t get_offset_of_emptyLogger_2() { return static_cast<int32_t>(offsetof(Logger_t2262497814_StaticFields, ___emptyLogger_2)); }
	inline Logger_t2262497814 * get_emptyLogger_2() const { return ___emptyLogger_2; }
	inline Logger_t2262497814 ** get_address_of_emptyLogger_2() { return &___emptyLogger_2; }
	inline void set_emptyLogger_2(Logger_t2262497814 * value)
	{
		___emptyLogger_2 = value;
		Il2CppCodeGenWriteBarrier(&___emptyLogger_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
