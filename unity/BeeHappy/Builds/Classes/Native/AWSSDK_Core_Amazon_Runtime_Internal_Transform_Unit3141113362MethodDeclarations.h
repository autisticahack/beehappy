﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.Transform.UnityWebResponseData
struct UnityWebResponseData_t3141113362;
// Amazon.Runtime.Internal.UnityWebRequestWrapper
struct UnityWebRequestWrapper_t1542496577;
// UnityEngine.WWW
struct WWW_t2919945039;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;
// Amazon.Runtime.Internal.Transform.IHttpResponseBody
struct IHttpResponseBody_t2556229876;
// System.IO.Stream
struct Stream_t3255436806;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_UnityWebReques1542496577.h"
#include "UnityEngine_UnityEngine_WWW2919945039.h"
#include "mscorlib_System_String2029220233.h"
#include "System_System_Net_HttpStatusCode1898409641.h"

// System.Void Amazon.Runtime.Internal.Transform.UnityWebResponseData::.ctor(Amazon.Runtime.Internal.UnityWebRequestWrapper)
extern "C"  void UnityWebResponseData__ctor_m2233870336 (UnityWebResponseData_t3141113362 * __this, UnityWebRequestWrapper_t1542496577 * ___unityWebRequest0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Transform.UnityWebResponseData::.ctor(UnityEngine.WWW)
extern "C"  void UnityWebResponseData__ctor_m3144241540 (UnityWebResponseData_t3141113362 * __this, WWW_t2919945039 * ___wwwRequest0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Amazon.Runtime.Internal.Transform.UnityWebResponseData::get_ContentLength()
extern "C"  int64_t UnityWebResponseData_get_ContentLength_m757597115 (UnityWebResponseData_t3141113362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Transform.UnityWebResponseData::set_ContentLength(System.Int64)
extern "C"  void UnityWebResponseData_set_ContentLength_m1281717766 (UnityWebResponseData_t3141113362 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Runtime.Internal.Transform.UnityWebResponseData::get_ContentType()
extern "C"  String_t* UnityWebResponseData_get_ContentType_m3665284723 (UnityWebResponseData_t3141113362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Transform.UnityWebResponseData::set_ContentType(System.String)
extern "C"  void UnityWebResponseData_set_ContentType_m1061111432 (UnityWebResponseData_t3141113362 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.HttpStatusCode Amazon.Runtime.Internal.Transform.UnityWebResponseData::get_StatusCode()
extern "C"  int32_t UnityWebResponseData_get_StatusCode_m2131713222 (UnityWebResponseData_t3141113362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Transform.UnityWebResponseData::set_StatusCode(System.Net.HttpStatusCode)
extern "C"  void UnityWebResponseData_set_StatusCode_m1117649685 (UnityWebResponseData_t3141113362 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.Transform.UnityWebResponseData::get_IsSuccessStatusCode()
extern "C"  bool UnityWebResponseData_get_IsSuccessStatusCode_m3609460799 (UnityWebResponseData_t3141113362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Transform.UnityWebResponseData::set_IsSuccessStatusCode(System.Boolean)
extern "C"  void UnityWebResponseData_set_IsSuccessStatusCode_m1476019058 (UnityWebResponseData_t3141113362 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.Transform.UnityWebResponseData::IsHeaderPresent(System.String)
extern "C"  bool UnityWebResponseData_IsHeaderPresent_m3228593504 (UnityWebResponseData_t3141113362 * __this, String_t* ___headerName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] Amazon.Runtime.Internal.Transform.UnityWebResponseData::GetHeaderNames()
extern "C"  StringU5BU5D_t1642385972* UnityWebResponseData_GetHeaderNames_m2101115876 (UnityWebResponseData_t3141113362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Runtime.Internal.Transform.UnityWebResponseData::GetHeaderValue(System.String)
extern "C"  String_t* UnityWebResponseData_GetHeaderValue_m543284895 (UnityWebResponseData_t3141113362 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Transform.UnityWebResponseData::CopyHeaderValues(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void UnityWebResponseData_CopyHeaderValues_m810956607 (UnityWebResponseData_t3141113362 * __this, Dictionary_2_t3943999495 * ___headers0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.Internal.Transform.IHttpResponseBody Amazon.Runtime.Internal.Transform.UnityWebResponseData::get_ResponseBody()
extern "C"  Il2CppObject * UnityWebResponseData_get_ResponseBody_m1586997346 (UnityWebResponseData_t3141113362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream Amazon.Runtime.Internal.Transform.UnityWebResponseData::OpenResponse()
extern "C"  Stream_t3255436806 * UnityWebResponseData_OpenResponse_m996655933 (UnityWebResponseData_t3141113362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Transform.UnityWebResponseData::Dispose()
extern "C"  void UnityWebResponseData_Dispose_m3628892973 (UnityWebResponseData_t3141113362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
