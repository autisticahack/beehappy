﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityStandardAssets.ImageEffects.Quads
struct Quads_t761286994;
// UnityEngine.Mesh[]
struct MeshU5BU5D_t894826206;
// UnityEngine.Mesh
struct Mesh_t1356156583;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityStandardAssets.ImageEffects.Quads::.ctor()
extern "C"  void Quads__ctor_m4198180917 (Quads_t761286994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityStandardAssets.ImageEffects.Quads::HasMeshes()
extern "C"  bool Quads_HasMeshes_m3049444858 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.ImageEffects.Quads::Cleanup()
extern "C"  void Quads_Cleanup_m4285769929 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Mesh[] UnityStandardAssets.ImageEffects.Quads::GetMeshes(System.Int32,System.Int32)
extern "C"  MeshU5BU5D_t894826206* Quads_GetMeshes_m19743407 (Il2CppObject * __this /* static, unused */, int32_t ___totalWidth0, int32_t ___totalHeight1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Mesh UnityStandardAssets.ImageEffects.Quads::GetMesh(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  Mesh_t1356156583 * Quads_GetMesh_m1651472875 (Il2CppObject * __this /* static, unused */, int32_t ___triCount0, int32_t ___triOffset1, int32_t ___totalWidth2, int32_t ___totalHeight3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.ImageEffects.Quads::.cctor()
extern "C"  void Quads__cctor_m2388849992 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
