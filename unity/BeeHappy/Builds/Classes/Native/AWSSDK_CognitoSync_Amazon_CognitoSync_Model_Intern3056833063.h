﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.CognitoSync.Model.Internal.MarshallTransformations.RecordUnmarshaller
struct RecordUnmarshaller_t3056833063;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.CognitoSync.Model.Internal.MarshallTransformations.RecordUnmarshaller
struct  RecordUnmarshaller_t3056833063  : public Il2CppObject
{
public:

public:
};

struct RecordUnmarshaller_t3056833063_StaticFields
{
public:
	// Amazon.CognitoSync.Model.Internal.MarshallTransformations.RecordUnmarshaller Amazon.CognitoSync.Model.Internal.MarshallTransformations.RecordUnmarshaller::_instance
	RecordUnmarshaller_t3056833063 * ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(RecordUnmarshaller_t3056833063_StaticFields, ____instance_0)); }
	inline RecordUnmarshaller_t3056833063 * get__instance_0() const { return ____instance_0; }
	inline RecordUnmarshaller_t3056833063 ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(RecordUnmarshaller_t3056833063 * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier(&____instance_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
