﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Util.Timing
struct  Timing_t847194262  : public Il2CppObject
{
public:
	// System.Int64 Amazon.Runtime.Internal.Util.Timing::startTime
	int64_t ___startTime_0;
	// System.Int64 Amazon.Runtime.Internal.Util.Timing::endTime
	int64_t ___endTime_1;
	// System.Boolean Amazon.Runtime.Internal.Util.Timing::<IsFinished>k__BackingField
	bool ___U3CIsFinishedU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_startTime_0() { return static_cast<int32_t>(offsetof(Timing_t847194262, ___startTime_0)); }
	inline int64_t get_startTime_0() const { return ___startTime_0; }
	inline int64_t* get_address_of_startTime_0() { return &___startTime_0; }
	inline void set_startTime_0(int64_t value)
	{
		___startTime_0 = value;
	}

	inline static int32_t get_offset_of_endTime_1() { return static_cast<int32_t>(offsetof(Timing_t847194262, ___endTime_1)); }
	inline int64_t get_endTime_1() const { return ___endTime_1; }
	inline int64_t* get_address_of_endTime_1() { return &___endTime_1; }
	inline void set_endTime_1(int64_t value)
	{
		___endTime_1 = value;
	}

	inline static int32_t get_offset_of_U3CIsFinishedU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Timing_t847194262, ___U3CIsFinishedU3Ek__BackingField_2)); }
	inline bool get_U3CIsFinishedU3Ek__BackingField_2() const { return ___U3CIsFinishedU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CIsFinishedU3Ek__BackingField_2() { return &___U3CIsFinishedU3Ek__BackingField_2; }
	inline void set_U3CIsFinishedU3Ek__BackingField_2(bool value)
	{
		___U3CIsFinishedU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
