﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Util.ProxyConfig
struct ProxyConfig_t2693849256;
// Amazon.ProxySection
struct ProxySection_t4280332351;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_ProxySection4280332351.h"

// System.Void Amazon.Util.ProxyConfig::.ctor()
extern "C"  void ProxyConfig__ctor_m721915347 (ProxyConfig_t2693849256 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Util.ProxyConfig::Configure(Amazon.ProxySection)
extern "C"  void ProxyConfig_Configure_m1875271790 (ProxyConfig_t2693849256 * __this, ProxySection_t4280332351 * ___section0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
