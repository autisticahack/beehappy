﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// Amazon.Runtime.Internal.Util.Logger
struct Logger_t2262497814;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Util.Internal.InternalSDKUtils
struct  InternalSDKUtils_t3074264706  : public Il2CppObject
{
public:

public:
};

struct InternalSDKUtils_t3074264706_StaticFields
{
public:
	// System.String Amazon.Util.Internal.InternalSDKUtils::_customSdkUserAgent
	String_t* ____customSdkUserAgent_0;
	// System.String Amazon.Util.Internal.InternalSDKUtils::_customData
	String_t* ____customData_1;
	// System.String Amazon.Util.Internal.InternalSDKUtils::_userAgentBaseName
	String_t* ____userAgentBaseName_2;
	// Amazon.Runtime.Internal.Util.Logger Amazon.Util.Internal.InternalSDKUtils::_logger
	Logger_t2262497814 * ____logger_3;

public:
	inline static int32_t get_offset_of__customSdkUserAgent_0() { return static_cast<int32_t>(offsetof(InternalSDKUtils_t3074264706_StaticFields, ____customSdkUserAgent_0)); }
	inline String_t* get__customSdkUserAgent_0() const { return ____customSdkUserAgent_0; }
	inline String_t** get_address_of__customSdkUserAgent_0() { return &____customSdkUserAgent_0; }
	inline void set__customSdkUserAgent_0(String_t* value)
	{
		____customSdkUserAgent_0 = value;
		Il2CppCodeGenWriteBarrier(&____customSdkUserAgent_0, value);
	}

	inline static int32_t get_offset_of__customData_1() { return static_cast<int32_t>(offsetof(InternalSDKUtils_t3074264706_StaticFields, ____customData_1)); }
	inline String_t* get__customData_1() const { return ____customData_1; }
	inline String_t** get_address_of__customData_1() { return &____customData_1; }
	inline void set__customData_1(String_t* value)
	{
		____customData_1 = value;
		Il2CppCodeGenWriteBarrier(&____customData_1, value);
	}

	inline static int32_t get_offset_of__userAgentBaseName_2() { return static_cast<int32_t>(offsetof(InternalSDKUtils_t3074264706_StaticFields, ____userAgentBaseName_2)); }
	inline String_t* get__userAgentBaseName_2() const { return ____userAgentBaseName_2; }
	inline String_t** get_address_of__userAgentBaseName_2() { return &____userAgentBaseName_2; }
	inline void set__userAgentBaseName_2(String_t* value)
	{
		____userAgentBaseName_2 = value;
		Il2CppCodeGenWriteBarrier(&____userAgentBaseName_2, value);
	}

	inline static int32_t get_offset_of__logger_3() { return static_cast<int32_t>(offsetof(InternalSDKUtils_t3074264706_StaticFields, ____logger_3)); }
	inline Logger_t2262497814 * get__logger_3() const { return ____logger_3; }
	inline Logger_t2262497814 ** get_address_of__logger_3() { return &____logger_3; }
	inline void set__logger_3(Logger_t2262497814 * value)
	{
		____logger_3 = value;
		Il2CppCodeGenWriteBarrier(&____logger_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
