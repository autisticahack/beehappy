﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Util.Internal.RootConfig
struct RootConfig_t4046630774;
// Amazon.Util.LoggingConfig
struct LoggingConfig_t4162907495;
// Amazon.Util.ProxyConfig
struct ProxyConfig_t2693849256;
// System.String
struct String_t;
// Amazon.RegionEndpoint
struct RegionEndpoint_t661522805;
// System.Collections.Generic.IDictionary`2<System.String,System.Xml.Linq.XElement>
struct IDictionary_2_t467683733;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_Util_LoggingConfig4162907495.h"
#include "AWSSDK_Core_Amazon_Util_ProxyConfig2693849256.h"
#include "mscorlib_System_String2029220233.h"

// Amazon.Util.LoggingConfig Amazon.Util.Internal.RootConfig::get_Logging()
extern "C"  LoggingConfig_t4162907495 * RootConfig_get_Logging_m2205740708 (RootConfig_t4046630774 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Util.Internal.RootConfig::set_Logging(Amazon.Util.LoggingConfig)
extern "C"  void RootConfig_set_Logging_m2205248709 (RootConfig_t4046630774 * __this, LoggingConfig_t4162907495 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Util.ProxyConfig Amazon.Util.Internal.RootConfig::get_Proxy()
extern "C"  ProxyConfig_t2693849256 * RootConfig_get_Proxy_m1621447446 (RootConfig_t4046630774 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Util.Internal.RootConfig::set_Proxy(Amazon.Util.ProxyConfig)
extern "C"  void RootConfig_set_Proxy_m1308251653 (RootConfig_t4046630774 * __this, ProxyConfig_t2693849256 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Util.Internal.RootConfig::get_EndpointDefinition()
extern "C"  String_t* RootConfig_get_EndpointDefinition_m2234044382 (RootConfig_t4046630774 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Util.Internal.RootConfig::set_EndpointDefinition(System.String)
extern "C"  void RootConfig_set_EndpointDefinition_m1670931329 (RootConfig_t4046630774 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Util.Internal.RootConfig::get_Region()
extern "C"  String_t* RootConfig_get_Region_m348758198 (RootConfig_t4046630774 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Util.Internal.RootConfig::set_Region(System.String)
extern "C"  void RootConfig_set_Region_m3236725431 (RootConfig_t4046630774 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Util.Internal.RootConfig::get_ProfileName()
extern "C"  String_t* RootConfig_get_ProfileName_m972180938 (RootConfig_t4046630774 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Util.Internal.RootConfig::set_ProfileName(System.String)
extern "C"  void RootConfig_set_ProfileName_m4087060227 (RootConfig_t4046630774 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Util.Internal.RootConfig::get_ProfilesLocation()
extern "C"  String_t* RootConfig_get_ProfilesLocation_m2857356621 (RootConfig_t4046630774 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Util.Internal.RootConfig::set_ProfilesLocation(System.String)
extern "C"  void RootConfig_set_ProfilesLocation_m1489384590 (RootConfig_t4046630774 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.RegionEndpoint Amazon.Util.Internal.RootConfig::get_RegionEndpoint()
extern "C"  RegionEndpoint_t661522805 * RootConfig_get_RegionEndpoint_m352985140 (RootConfig_t4046630774 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Util.Internal.RootConfig::set_UseSdkCache(System.Boolean)
extern "C"  void RootConfig_set_UseSdkCache_m1186395781 (RootConfig_t4046630774 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Util.Internal.RootConfig::get_CorrectForClockSkew()
extern "C"  bool RootConfig_get_CorrectForClockSkew_m4014959980 (RootConfig_t4046630774 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Util.Internal.RootConfig::set_CorrectForClockSkew(System.Boolean)
extern "C"  void RootConfig_set_CorrectForClockSkew_m1765031205 (RootConfig_t4046630774 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Util.Internal.RootConfig::get_ApplicationName()
extern "C"  String_t* RootConfig_get_ApplicationName_m2227368661 (RootConfig_t4046630774 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Util.Internal.RootConfig::set_ApplicationName(System.String)
extern "C"  void RootConfig_set_ApplicationName_m59340094 (RootConfig_t4046630774 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Util.Internal.RootConfig::.ctor()
extern "C"  void RootConfig__ctor_m3002128842 (RootConfig_t4046630774 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Util.Internal.RootConfig::Choose(System.String,System.String)
extern "C"  String_t* RootConfig_Choose_m2527941340 (Il2CppObject * __this /* static, unused */, String_t* ___a0, String_t* ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Util.Internal.RootConfig::set_ServiceSections(System.Collections.Generic.IDictionary`2<System.String,System.Xml.Linq.XElement>)
extern "C"  void RootConfig_set_ServiceSections_m630693458 (RootConfig_t4046630774 * __this, Il2CppObject* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
