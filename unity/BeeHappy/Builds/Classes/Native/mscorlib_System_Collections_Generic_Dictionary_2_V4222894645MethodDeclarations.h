﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>
struct ValueCollection_t4222894645;
// System.Collections.Generic.Dictionary`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>
struct Dictionary_2_t1224867506;
// System.Collections.Generic.IEnumerator`1<Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>
struct IEnumerator_1_t3403298501;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel[]
struct InstantiationModelU5BU5D_t1614949031;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_Util_Internal_PlatformServices_1632807378.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2911400270.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m1534037786_gshared (ValueCollection_t4222894645 * __this, Dictionary_2_t1224867506 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m1534037786(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t4222894645 *, Dictionary_2_t1224867506 *, const MethodInfo*))ValueCollection__ctor_m1534037786_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m4084879748_gshared (ValueCollection_t4222894645 * __this, int32_t ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m4084879748(__this, ___item0, method) ((  void (*) (ValueCollection_t4222894645 *, int32_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m4084879748_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1695851195_gshared (ValueCollection_t4222894645 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1695851195(__this, method) ((  void (*) (ValueCollection_t4222894645 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1695851195_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3443378028_gshared (ValueCollection_t4222894645 * __this, int32_t ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3443378028(__this, ___item0, method) ((  bool (*) (ValueCollection_t4222894645 *, int32_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3443378028_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m459294531_gshared (ValueCollection_t4222894645 * __this, int32_t ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m459294531(__this, ___item0, method) ((  bool (*) (ValueCollection_t4222894645 *, int32_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m459294531_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2422542625_gshared (ValueCollection_t4222894645 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2422542625(__this, method) ((  Il2CppObject* (*) (ValueCollection_t4222894645 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2422542625_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m3476840469_gshared (ValueCollection_t4222894645 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m3476840469(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t4222894645 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m3476840469_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2846809116_gshared (ValueCollection_t4222894645 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2846809116(__this, method) ((  Il2CppObject * (*) (ValueCollection_t4222894645 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2846809116_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3733867415_gshared (ValueCollection_t4222894645 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3733867415(__this, method) ((  bool (*) (ValueCollection_t4222894645 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3733867415_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1007804449_gshared (ValueCollection_t4222894645 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1007804449(__this, method) ((  bool (*) (ValueCollection_t4222894645 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1007804449_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m669103221_gshared (ValueCollection_t4222894645 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m669103221(__this, method) ((  Il2CppObject * (*) (ValueCollection_t4222894645 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m669103221_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m1635440867_gshared (ValueCollection_t4222894645 * __this, InstantiationModelU5BU5D_t1614949031* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m1635440867(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t4222894645 *, InstantiationModelU5BU5D_t1614949031*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m1635440867_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::GetEnumerator()
extern "C"  Enumerator_t2911400270  ValueCollection_GetEnumerator_m11874664_gshared (ValueCollection_t4222894645 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m11874664(__this, method) ((  Enumerator_t2911400270  (*) (ValueCollection_t4222894645 *, const MethodInfo*))ValueCollection_GetEnumerator_m11874664_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m316804641_gshared (ValueCollection_t4222894645 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m316804641(__this, method) ((  int32_t (*) (ValueCollection_t4222894645 *, const MethodInfo*))ValueCollection_get_Count_m316804641_gshared)(__this, method)
