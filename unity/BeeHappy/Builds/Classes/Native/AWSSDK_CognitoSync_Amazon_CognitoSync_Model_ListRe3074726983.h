﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AWSSDK_CognitoSync_Amazon_CognitoSync_AmazonCognit2140463921.h"
#include "mscorlib_System_Nullable_1_gen3467111648.h"
#include "mscorlib_System_Nullable_1_gen334943763.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.CognitoSync.Model.ListRecordsRequest
struct  ListRecordsRequest_t3074726983  : public AmazonCognitoSyncRequest_t2140463921
{
public:
	// System.String Amazon.CognitoSync.Model.ListRecordsRequest::_datasetName
	String_t* ____datasetName_3;
	// System.String Amazon.CognitoSync.Model.ListRecordsRequest::_identityId
	String_t* ____identityId_4;
	// System.String Amazon.CognitoSync.Model.ListRecordsRequest::_identityPoolId
	String_t* ____identityPoolId_5;
	// System.Nullable`1<System.Int64> Amazon.CognitoSync.Model.ListRecordsRequest::_lastSyncCount
	Nullable_1_t3467111648  ____lastSyncCount_6;
	// System.Nullable`1<System.Int32> Amazon.CognitoSync.Model.ListRecordsRequest::_maxResults
	Nullable_1_t334943763  ____maxResults_7;
	// System.String Amazon.CognitoSync.Model.ListRecordsRequest::_nextToken
	String_t* ____nextToken_8;
	// System.String Amazon.CognitoSync.Model.ListRecordsRequest::_syncSessionToken
	String_t* ____syncSessionToken_9;

public:
	inline static int32_t get_offset_of__datasetName_3() { return static_cast<int32_t>(offsetof(ListRecordsRequest_t3074726983, ____datasetName_3)); }
	inline String_t* get__datasetName_3() const { return ____datasetName_3; }
	inline String_t** get_address_of__datasetName_3() { return &____datasetName_3; }
	inline void set__datasetName_3(String_t* value)
	{
		____datasetName_3 = value;
		Il2CppCodeGenWriteBarrier(&____datasetName_3, value);
	}

	inline static int32_t get_offset_of__identityId_4() { return static_cast<int32_t>(offsetof(ListRecordsRequest_t3074726983, ____identityId_4)); }
	inline String_t* get__identityId_4() const { return ____identityId_4; }
	inline String_t** get_address_of__identityId_4() { return &____identityId_4; }
	inline void set__identityId_4(String_t* value)
	{
		____identityId_4 = value;
		Il2CppCodeGenWriteBarrier(&____identityId_4, value);
	}

	inline static int32_t get_offset_of__identityPoolId_5() { return static_cast<int32_t>(offsetof(ListRecordsRequest_t3074726983, ____identityPoolId_5)); }
	inline String_t* get__identityPoolId_5() const { return ____identityPoolId_5; }
	inline String_t** get_address_of__identityPoolId_5() { return &____identityPoolId_5; }
	inline void set__identityPoolId_5(String_t* value)
	{
		____identityPoolId_5 = value;
		Il2CppCodeGenWriteBarrier(&____identityPoolId_5, value);
	}

	inline static int32_t get_offset_of__lastSyncCount_6() { return static_cast<int32_t>(offsetof(ListRecordsRequest_t3074726983, ____lastSyncCount_6)); }
	inline Nullable_1_t3467111648  get__lastSyncCount_6() const { return ____lastSyncCount_6; }
	inline Nullable_1_t3467111648 * get_address_of__lastSyncCount_6() { return &____lastSyncCount_6; }
	inline void set__lastSyncCount_6(Nullable_1_t3467111648  value)
	{
		____lastSyncCount_6 = value;
	}

	inline static int32_t get_offset_of__maxResults_7() { return static_cast<int32_t>(offsetof(ListRecordsRequest_t3074726983, ____maxResults_7)); }
	inline Nullable_1_t334943763  get__maxResults_7() const { return ____maxResults_7; }
	inline Nullable_1_t334943763 * get_address_of__maxResults_7() { return &____maxResults_7; }
	inline void set__maxResults_7(Nullable_1_t334943763  value)
	{
		____maxResults_7 = value;
	}

	inline static int32_t get_offset_of__nextToken_8() { return static_cast<int32_t>(offsetof(ListRecordsRequest_t3074726983, ____nextToken_8)); }
	inline String_t* get__nextToken_8() const { return ____nextToken_8; }
	inline String_t** get_address_of__nextToken_8() { return &____nextToken_8; }
	inline void set__nextToken_8(String_t* value)
	{
		____nextToken_8 = value;
		Il2CppCodeGenWriteBarrier(&____nextToken_8, value);
	}

	inline static int32_t get_offset_of__syncSessionToken_9() { return static_cast<int32_t>(offsetof(ListRecordsRequest_t3074726983, ____syncSessionToken_9)); }
	inline String_t* get__syncSessionToken_9() const { return ____syncSessionToken_9; }
	inline String_t** get_address_of__syncSessionToken_9() { return &____syncSessionToken_9; }
	inline void set__syncSessionToken_9(String_t* value)
	{
		____syncSessionToken_9 = value;
		Il2CppCodeGenWriteBarrier(&____syncSessionToken_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
