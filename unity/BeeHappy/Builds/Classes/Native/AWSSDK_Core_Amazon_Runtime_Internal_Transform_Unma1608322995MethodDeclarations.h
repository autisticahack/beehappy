﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.Transform.UnmarshallerContext
struct UnmarshallerContext_t1608322995;
// ThirdParty.Ionic.Zlib.CrcCalculatorStream
struct CrcCalculatorStream_t2228597532;
// Amazon.Runtime.Internal.Transform.IWebResponseData
struct IWebResponseData_t3815728244;
// Amazon.Runtime.Internal.Util.CachingWrapperStream
struct CachingWrapperStream_t380166576;
// System.String
struct String_t;
// System.IO.Stream
struct Stream_t3255436806;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_ThirdParty_Ionic_Zlib_CrcCalculatorStr2228597532.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Util_CachingWra380166576.h"
#include "mscorlib_System_IO_Stream3255436806.h"
#include "mscorlib_System_String2029220233.h"

// System.Boolean Amazon.Runtime.Internal.Transform.UnmarshallerContext::get_MaintainResponseBody()
extern "C"  bool UnmarshallerContext_get_MaintainResponseBody_m2748576978 (UnmarshallerContext_t1608322995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Transform.UnmarshallerContext::set_MaintainResponseBody(System.Boolean)
extern "C"  void UnmarshallerContext_set_MaintainResponseBody_m4279670961 (UnmarshallerContext_t1608322995 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ThirdParty.Ionic.Zlib.CrcCalculatorStream Amazon.Runtime.Internal.Transform.UnmarshallerContext::get_CrcStream()
extern "C"  CrcCalculatorStream_t2228597532 * UnmarshallerContext_get_CrcStream_m2316295235 (UnmarshallerContext_t1608322995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Transform.UnmarshallerContext::set_CrcStream(ThirdParty.Ionic.Zlib.CrcCalculatorStream)
extern "C"  void UnmarshallerContext_set_CrcStream_m1834563342 (UnmarshallerContext_t1608322995 * __this, CrcCalculatorStream_t2228597532 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Amazon.Runtime.Internal.Transform.UnmarshallerContext::get_Crc32Result()
extern "C"  int32_t UnmarshallerContext_get_Crc32Result_m80445780 (UnmarshallerContext_t1608322995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Transform.UnmarshallerContext::set_Crc32Result(System.Int32)
extern "C"  void UnmarshallerContext_set_Crc32Result_m1398725431 (UnmarshallerContext_t1608322995 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.Internal.Transform.IWebResponseData Amazon.Runtime.Internal.Transform.UnmarshallerContext::get_WebResponseData()
extern "C"  Il2CppObject * UnmarshallerContext_get_WebResponseData_m2769699579 (UnmarshallerContext_t1608322995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Transform.UnmarshallerContext::set_WebResponseData(Amazon.Runtime.Internal.Transform.IWebResponseData)
extern "C"  void UnmarshallerContext_set_WebResponseData_m2048563200 (UnmarshallerContext_t1608322995 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.Internal.Util.CachingWrapperStream Amazon.Runtime.Internal.Transform.UnmarshallerContext::get_WrappingStream()
extern "C"  CachingWrapperStream_t380166576 * UnmarshallerContext_get_WrappingStream_m4107257696 (UnmarshallerContext_t1608322995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Transform.UnmarshallerContext::set_WrappingStream(Amazon.Runtime.Internal.Util.CachingWrapperStream)
extern "C"  void UnmarshallerContext_set_WrappingStream_m1553893283 (UnmarshallerContext_t1608322995 * __this, CachingWrapperStream_t380166576 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Runtime.Internal.Transform.UnmarshallerContext::get_ResponseBody()
extern "C"  String_t* UnmarshallerContext_get_ResponseBody_m41996546 (UnmarshallerContext_t1608322995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.Internal.Transform.IWebResponseData Amazon.Runtime.Internal.Transform.UnmarshallerContext::get_ResponseData()
extern "C"  Il2CppObject * UnmarshallerContext_get_ResponseData_m2106488207 (UnmarshallerContext_t1608322995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Transform.UnmarshallerContext::ValidateCRC32IfAvailable()
extern "C"  void UnmarshallerContext_ValidateCRC32IfAvailable_m3241116450 (UnmarshallerContext_t1608322995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Transform.UnmarshallerContext::SetupCRCStream(Amazon.Runtime.Internal.Transform.IWebResponseData,System.IO.Stream,System.Int64)
extern "C"  void UnmarshallerContext_SetupCRCStream_m1997963942 (UnmarshallerContext_t1608322995 * __this, Il2CppObject * ___responseData0, Stream_t3255436806 * ___responseStream1, int64_t ___contentLength2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.Transform.UnmarshallerContext::TestExpression(System.String)
extern "C"  bool UnmarshallerContext_TestExpression_m3553682743 (UnmarshallerContext_t1608322995 * __this, String_t* ___expression0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.Transform.UnmarshallerContext::TestExpression(System.String,System.Int32)
extern "C"  bool UnmarshallerContext_TestExpression_m3665247446 (UnmarshallerContext_t1608322995 * __this, String_t* ___expression0, int32_t ___startingStackDepth1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.Transform.UnmarshallerContext::ReadAtDepth(System.Int32)
extern "C"  bool UnmarshallerContext_ReadAtDepth_m2943457426 (UnmarshallerContext_t1608322995 * __this, int32_t ___targetDepth0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.Transform.UnmarshallerContext::TestExpression(System.String,System.String)
extern "C"  bool UnmarshallerContext_TestExpression_m4088787361 (Il2CppObject * __this /* static, unused */, String_t* ___expression0, String_t* ___currentPath1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.Transform.UnmarshallerContext::TestExpression(System.String,System.Int32,System.String,System.Int32)
extern "C"  bool UnmarshallerContext_TestExpression_m1189246809 (Il2CppObject * __this /* static, unused */, String_t* ___expression0, int32_t ___startingStackDepth1, String_t* ___currentPath2, int32_t ___currentDepth3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Transform.UnmarshallerContext::Dispose(System.Boolean)
extern "C"  void UnmarshallerContext_Dispose_m4036185933 (UnmarshallerContext_t1608322995 * __this, bool ___disposing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Transform.UnmarshallerContext::Dispose()
extern "C"  void UnmarshallerContext_Dispose_m2440460694 (UnmarshallerContext_t1608322995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Transform.UnmarshallerContext::.ctor()
extern "C"  void UnmarshallerContext__ctor_m1553482233 (UnmarshallerContext_t1608322995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
