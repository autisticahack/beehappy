﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.IFormatProvider
struct IFormatProvider_t2849799027;
// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Util.LogMessage
struct  LogMessage_t4004523899  : public Il2CppObject
{
public:
	// System.Object[] Amazon.Runtime.Internal.Util.LogMessage::<Args>k__BackingField
	ObjectU5BU5D_t3614634134* ___U3CArgsU3Ek__BackingField_0;
	// System.IFormatProvider Amazon.Runtime.Internal.Util.LogMessage::<Provider>k__BackingField
	Il2CppObject * ___U3CProviderU3Ek__BackingField_1;
	// System.String Amazon.Runtime.Internal.Util.LogMessage::<Format>k__BackingField
	String_t* ___U3CFormatU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CArgsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(LogMessage_t4004523899, ___U3CArgsU3Ek__BackingField_0)); }
	inline ObjectU5BU5D_t3614634134* get_U3CArgsU3Ek__BackingField_0() const { return ___U3CArgsU3Ek__BackingField_0; }
	inline ObjectU5BU5D_t3614634134** get_address_of_U3CArgsU3Ek__BackingField_0() { return &___U3CArgsU3Ek__BackingField_0; }
	inline void set_U3CArgsU3Ek__BackingField_0(ObjectU5BU5D_t3614634134* value)
	{
		___U3CArgsU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CArgsU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CProviderU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(LogMessage_t4004523899, ___U3CProviderU3Ek__BackingField_1)); }
	inline Il2CppObject * get_U3CProviderU3Ek__BackingField_1() const { return ___U3CProviderU3Ek__BackingField_1; }
	inline Il2CppObject ** get_address_of_U3CProviderU3Ek__BackingField_1() { return &___U3CProviderU3Ek__BackingField_1; }
	inline void set_U3CProviderU3Ek__BackingField_1(Il2CppObject * value)
	{
		___U3CProviderU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CProviderU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CFormatU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(LogMessage_t4004523899, ___U3CFormatU3Ek__BackingField_2)); }
	inline String_t* get_U3CFormatU3Ek__BackingField_2() const { return ___U3CFormatU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CFormatU3Ek__BackingField_2() { return &___U3CFormatU3Ek__BackingField_2; }
	inline void set_U3CFormatU3Ek__BackingField_2(String_t* value)
	{
		___U3CFormatU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CFormatU3Ek__BackingField_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
