﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.UnityRequestQueue
struct UnityRequestQueue_t3684366031;
// Amazon.Runtime.IUnityHttpRequest
struct IUnityHttpRequest_t1859903397;
// Amazon.Runtime.Internal.RuntimeAsyncResult
struct RuntimeAsyncResult_t4279356013;
// System.Action
struct Action_t3226471752;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_RuntimeAsyncRe4279356013.h"
#include "System_Core_System_Action3226471752.h"

// System.Void Amazon.Runtime.Internal.UnityRequestQueue::.ctor()
extern "C"  void UnityRequestQueue__ctor_m1088834539 (UnityRequestQueue_t3684366031 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.Internal.UnityRequestQueue Amazon.Runtime.Internal.UnityRequestQueue::get_Instance()
extern "C"  UnityRequestQueue_t3684366031 * UnityRequestQueue_get_Instance_m715080226 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.UnityRequestQueue::EnqueueRequest(Amazon.Runtime.IUnityHttpRequest)
extern "C"  void UnityRequestQueue_EnqueueRequest_m789718105 (UnityRequestQueue_t3684366031 * __this, Il2CppObject * ___request0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.IUnityHttpRequest Amazon.Runtime.Internal.UnityRequestQueue::DequeueRequest()
extern "C"  Il2CppObject * UnityRequestQueue_DequeueRequest_m885167954 (UnityRequestQueue_t3684366031 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.UnityRequestQueue::EnqueueCallback(Amazon.Runtime.Internal.RuntimeAsyncResult)
extern "C"  void UnityRequestQueue_EnqueueCallback_m1641044228 (UnityRequestQueue_t3684366031 * __this, RuntimeAsyncResult_t4279356013 * ___asyncResult0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.Internal.RuntimeAsyncResult Amazon.Runtime.Internal.UnityRequestQueue::DequeueCallback()
extern "C"  RuntimeAsyncResult_t4279356013 * UnityRequestQueue_DequeueCallback_m859144269 (UnityRequestQueue_t3684366031 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.UnityRequestQueue::ExecuteOnMainThread(System.Action)
extern "C"  void UnityRequestQueue_ExecuteOnMainThread_m269498729 (UnityRequestQueue_t3684366031 * __this, Action_t3226471752 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action Amazon.Runtime.Internal.UnityRequestQueue::DequeueMainThreadOperation()
extern "C"  Action_t3226471752 * UnityRequestQueue_DequeueMainThreadOperation_m462511085 (UnityRequestQueue_t3684366031 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.UnityRequestQueue::.cctor()
extern "C"  void UnityRequestQueue__cctor_m2497062492 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
