﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Stack`1<ThirdParty.Json.LitJson.JsonToken>
struct Stack_1_t3533309409;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Stack_1_Enumerat4183307769.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_JsonToken2445581255.h"

// System.Void System.Collections.Generic.Stack`1/Enumerator<ThirdParty.Json.LitJson.JsonToken>::.ctor(System.Collections.Generic.Stack`1<T>)
extern "C"  void Enumerator__ctor_m2678072094_gshared (Enumerator_t4183307769 * __this, Stack_1_t3533309409 * ___t0, const MethodInfo* method);
#define Enumerator__ctor_m2678072094(__this, ___t0, method) ((  void (*) (Enumerator_t4183307769 *, Stack_1_t3533309409 *, const MethodInfo*))Enumerator__ctor_m2678072094_gshared)(__this, ___t0, method)
// System.Void System.Collections.Generic.Stack`1/Enumerator<ThirdParty.Json.LitJson.JsonToken>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1139293256_gshared (Enumerator_t4183307769 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1139293256(__this, method) ((  void (*) (Enumerator_t4183307769 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1139293256_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1/Enumerator<ThirdParty.Json.LitJson.JsonToken>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2915944246_gshared (Enumerator_t4183307769 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2915944246(__this, method) ((  Il2CppObject * (*) (Enumerator_t4183307769 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2915944246_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1/Enumerator<ThirdParty.Json.LitJson.JsonToken>::Dispose()
extern "C"  void Enumerator_Dispose_m2371263537_gshared (Enumerator_t4183307769 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2371263537(__this, method) ((  void (*) (Enumerator_t4183307769 *, const MethodInfo*))Enumerator_Dispose_m2371263537_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1/Enumerator<ThirdParty.Json.LitJson.JsonToken>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m756200344_gshared (Enumerator_t4183307769 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m756200344(__this, method) ((  bool (*) (Enumerator_t4183307769 *, const MethodInfo*))Enumerator_MoveNext_m756200344_gshared)(__this, method)
// T System.Collections.Generic.Stack`1/Enumerator<ThirdParty.Json.LitJson.JsonToken>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m3389530809_gshared (Enumerator_t4183307769 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3389530809(__this, method) ((  int32_t (*) (Enumerator_t4183307769 *, const MethodInfo*))Enumerator_get_Current_m3389530809_gshared)(__this, method)
