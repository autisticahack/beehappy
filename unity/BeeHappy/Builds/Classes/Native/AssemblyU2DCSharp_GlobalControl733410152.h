﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// GlobalControl
struct GlobalControl_t733410152;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GlobalControl
struct  GlobalControl_t733410152  : public MonoBehaviour_t1158329972
{
public:
	// System.String GlobalControl::TestString
	String_t* ___TestString_3;

public:
	inline static int32_t get_offset_of_TestString_3() { return static_cast<int32_t>(offsetof(GlobalControl_t733410152, ___TestString_3)); }
	inline String_t* get_TestString_3() const { return ___TestString_3; }
	inline String_t** get_address_of_TestString_3() { return &___TestString_3; }
	inline void set_TestString_3(String_t* value)
	{
		___TestString_3 = value;
		Il2CppCodeGenWriteBarrier(&___TestString_3, value);
	}
};

struct GlobalControl_t733410152_StaticFields
{
public:
	// GlobalControl GlobalControl::Instance
	GlobalControl_t733410152 * ___Instance_2;

public:
	inline static int32_t get_offset_of_Instance_2() { return static_cast<int32_t>(offsetof(GlobalControl_t733410152_StaticFields, ___Instance_2)); }
	inline GlobalControl_t733410152 * get_Instance_2() const { return ___Instance_2; }
	inline GlobalControl_t733410152 ** get_address_of_Instance_2() { return &___Instance_2; }
	inline void set_Instance_2(GlobalControl_t733410152 * value)
	{
		___Instance_2 = value;
		Il2CppCodeGenWriteBarrier(&___Instance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
