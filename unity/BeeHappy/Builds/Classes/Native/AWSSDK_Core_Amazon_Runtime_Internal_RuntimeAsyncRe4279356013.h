﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// System.Threading.ManualResetEvent
struct ManualResetEvent_t926074657;
// Amazon.Runtime.Internal.Util.ILogger
struct ILogger_t676853571;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// System.Exception
struct Exception_t1927440687;
// Amazon.Runtime.AmazonWebServiceResponse
struct AmazonWebServiceResponse_t529043356;
// Amazon.Runtime.AmazonWebServiceRequest
struct AmazonWebServiceRequest_t3384026212;
// System.Action`4<Amazon.Runtime.AmazonWebServiceRequest,Amazon.Runtime.AmazonWebServiceResponse,System.Exception,Amazon.Runtime.AsyncOptions>
struct Action_4_t897279368;
// Amazon.Runtime.AsyncOptions
struct AsyncOptions_t558351272;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.RuntimeAsyncResult
struct  RuntimeAsyncResult_t4279356013  : public Il2CppObject
{
public:
	// System.Object Amazon.Runtime.Internal.RuntimeAsyncResult::_lockObj
	Il2CppObject * ____lockObj_0;
	// System.Threading.ManualResetEvent Amazon.Runtime.Internal.RuntimeAsyncResult::_waitHandle
	ManualResetEvent_t926074657 * ____waitHandle_1;
	// System.Boolean Amazon.Runtime.Internal.RuntimeAsyncResult::_disposed
	bool ____disposed_2;
	// System.Boolean Amazon.Runtime.Internal.RuntimeAsyncResult::_callbackInvoked
	bool ____callbackInvoked_3;
	// Amazon.Runtime.Internal.Util.ILogger Amazon.Runtime.Internal.RuntimeAsyncResult::_logger
	Il2CppObject * ____logger_4;
	// System.AsyncCallback Amazon.Runtime.Internal.RuntimeAsyncResult::<AsyncCallback>k__BackingField
	AsyncCallback_t163412349 * ___U3CAsyncCallbackU3Ek__BackingField_5;
	// System.Object Amazon.Runtime.Internal.RuntimeAsyncResult::<AsyncState>k__BackingField
	Il2CppObject * ___U3CAsyncStateU3Ek__BackingField_6;
	// System.Boolean Amazon.Runtime.Internal.RuntimeAsyncResult::<CompletedSynchronously>k__BackingField
	bool ___U3CCompletedSynchronouslyU3Ek__BackingField_7;
	// System.Boolean Amazon.Runtime.Internal.RuntimeAsyncResult::<IsCompleted>k__BackingField
	bool ___U3CIsCompletedU3Ek__BackingField_8;
	// System.Exception Amazon.Runtime.Internal.RuntimeAsyncResult::<Exception>k__BackingField
	Exception_t1927440687 * ___U3CExceptionU3Ek__BackingField_9;
	// Amazon.Runtime.AmazonWebServiceResponse Amazon.Runtime.Internal.RuntimeAsyncResult::<Response>k__BackingField
	AmazonWebServiceResponse_t529043356 * ___U3CResponseU3Ek__BackingField_10;
	// Amazon.Runtime.AmazonWebServiceRequest Amazon.Runtime.Internal.RuntimeAsyncResult::<Request>k__BackingField
	AmazonWebServiceRequest_t3384026212 * ___U3CRequestU3Ek__BackingField_11;
	// System.Action`4<Amazon.Runtime.AmazonWebServiceRequest,Amazon.Runtime.AmazonWebServiceResponse,System.Exception,Amazon.Runtime.AsyncOptions> Amazon.Runtime.Internal.RuntimeAsyncResult::<Action>k__BackingField
	Action_4_t897279368 * ___U3CActionU3Ek__BackingField_12;
	// Amazon.Runtime.AsyncOptions Amazon.Runtime.Internal.RuntimeAsyncResult::<AsyncOptions>k__BackingField
	AsyncOptions_t558351272 * ___U3CAsyncOptionsU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of__lockObj_0() { return static_cast<int32_t>(offsetof(RuntimeAsyncResult_t4279356013, ____lockObj_0)); }
	inline Il2CppObject * get__lockObj_0() const { return ____lockObj_0; }
	inline Il2CppObject ** get_address_of__lockObj_0() { return &____lockObj_0; }
	inline void set__lockObj_0(Il2CppObject * value)
	{
		____lockObj_0 = value;
		Il2CppCodeGenWriteBarrier(&____lockObj_0, value);
	}

	inline static int32_t get_offset_of__waitHandle_1() { return static_cast<int32_t>(offsetof(RuntimeAsyncResult_t4279356013, ____waitHandle_1)); }
	inline ManualResetEvent_t926074657 * get__waitHandle_1() const { return ____waitHandle_1; }
	inline ManualResetEvent_t926074657 ** get_address_of__waitHandle_1() { return &____waitHandle_1; }
	inline void set__waitHandle_1(ManualResetEvent_t926074657 * value)
	{
		____waitHandle_1 = value;
		Il2CppCodeGenWriteBarrier(&____waitHandle_1, value);
	}

	inline static int32_t get_offset_of__disposed_2() { return static_cast<int32_t>(offsetof(RuntimeAsyncResult_t4279356013, ____disposed_2)); }
	inline bool get__disposed_2() const { return ____disposed_2; }
	inline bool* get_address_of__disposed_2() { return &____disposed_2; }
	inline void set__disposed_2(bool value)
	{
		____disposed_2 = value;
	}

	inline static int32_t get_offset_of__callbackInvoked_3() { return static_cast<int32_t>(offsetof(RuntimeAsyncResult_t4279356013, ____callbackInvoked_3)); }
	inline bool get__callbackInvoked_3() const { return ____callbackInvoked_3; }
	inline bool* get_address_of__callbackInvoked_3() { return &____callbackInvoked_3; }
	inline void set__callbackInvoked_3(bool value)
	{
		____callbackInvoked_3 = value;
	}

	inline static int32_t get_offset_of__logger_4() { return static_cast<int32_t>(offsetof(RuntimeAsyncResult_t4279356013, ____logger_4)); }
	inline Il2CppObject * get__logger_4() const { return ____logger_4; }
	inline Il2CppObject ** get_address_of__logger_4() { return &____logger_4; }
	inline void set__logger_4(Il2CppObject * value)
	{
		____logger_4 = value;
		Il2CppCodeGenWriteBarrier(&____logger_4, value);
	}

	inline static int32_t get_offset_of_U3CAsyncCallbackU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(RuntimeAsyncResult_t4279356013, ___U3CAsyncCallbackU3Ek__BackingField_5)); }
	inline AsyncCallback_t163412349 * get_U3CAsyncCallbackU3Ek__BackingField_5() const { return ___U3CAsyncCallbackU3Ek__BackingField_5; }
	inline AsyncCallback_t163412349 ** get_address_of_U3CAsyncCallbackU3Ek__BackingField_5() { return &___U3CAsyncCallbackU3Ek__BackingField_5; }
	inline void set_U3CAsyncCallbackU3Ek__BackingField_5(AsyncCallback_t163412349 * value)
	{
		___U3CAsyncCallbackU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CAsyncCallbackU3Ek__BackingField_5, value);
	}

	inline static int32_t get_offset_of_U3CAsyncStateU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(RuntimeAsyncResult_t4279356013, ___U3CAsyncStateU3Ek__BackingField_6)); }
	inline Il2CppObject * get_U3CAsyncStateU3Ek__BackingField_6() const { return ___U3CAsyncStateU3Ek__BackingField_6; }
	inline Il2CppObject ** get_address_of_U3CAsyncStateU3Ek__BackingField_6() { return &___U3CAsyncStateU3Ek__BackingField_6; }
	inline void set_U3CAsyncStateU3Ek__BackingField_6(Il2CppObject * value)
	{
		___U3CAsyncStateU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CAsyncStateU3Ek__BackingField_6, value);
	}

	inline static int32_t get_offset_of_U3CCompletedSynchronouslyU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(RuntimeAsyncResult_t4279356013, ___U3CCompletedSynchronouslyU3Ek__BackingField_7)); }
	inline bool get_U3CCompletedSynchronouslyU3Ek__BackingField_7() const { return ___U3CCompletedSynchronouslyU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CCompletedSynchronouslyU3Ek__BackingField_7() { return &___U3CCompletedSynchronouslyU3Ek__BackingField_7; }
	inline void set_U3CCompletedSynchronouslyU3Ek__BackingField_7(bool value)
	{
		___U3CCompletedSynchronouslyU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CIsCompletedU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(RuntimeAsyncResult_t4279356013, ___U3CIsCompletedU3Ek__BackingField_8)); }
	inline bool get_U3CIsCompletedU3Ek__BackingField_8() const { return ___U3CIsCompletedU3Ek__BackingField_8; }
	inline bool* get_address_of_U3CIsCompletedU3Ek__BackingField_8() { return &___U3CIsCompletedU3Ek__BackingField_8; }
	inline void set_U3CIsCompletedU3Ek__BackingField_8(bool value)
	{
		___U3CIsCompletedU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CExceptionU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(RuntimeAsyncResult_t4279356013, ___U3CExceptionU3Ek__BackingField_9)); }
	inline Exception_t1927440687 * get_U3CExceptionU3Ek__BackingField_9() const { return ___U3CExceptionU3Ek__BackingField_9; }
	inline Exception_t1927440687 ** get_address_of_U3CExceptionU3Ek__BackingField_9() { return &___U3CExceptionU3Ek__BackingField_9; }
	inline void set_U3CExceptionU3Ek__BackingField_9(Exception_t1927440687 * value)
	{
		___U3CExceptionU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CExceptionU3Ek__BackingField_9, value);
	}

	inline static int32_t get_offset_of_U3CResponseU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(RuntimeAsyncResult_t4279356013, ___U3CResponseU3Ek__BackingField_10)); }
	inline AmazonWebServiceResponse_t529043356 * get_U3CResponseU3Ek__BackingField_10() const { return ___U3CResponseU3Ek__BackingField_10; }
	inline AmazonWebServiceResponse_t529043356 ** get_address_of_U3CResponseU3Ek__BackingField_10() { return &___U3CResponseU3Ek__BackingField_10; }
	inline void set_U3CResponseU3Ek__BackingField_10(AmazonWebServiceResponse_t529043356 * value)
	{
		___U3CResponseU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CResponseU3Ek__BackingField_10, value);
	}

	inline static int32_t get_offset_of_U3CRequestU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(RuntimeAsyncResult_t4279356013, ___U3CRequestU3Ek__BackingField_11)); }
	inline AmazonWebServiceRequest_t3384026212 * get_U3CRequestU3Ek__BackingField_11() const { return ___U3CRequestU3Ek__BackingField_11; }
	inline AmazonWebServiceRequest_t3384026212 ** get_address_of_U3CRequestU3Ek__BackingField_11() { return &___U3CRequestU3Ek__BackingField_11; }
	inline void set_U3CRequestU3Ek__BackingField_11(AmazonWebServiceRequest_t3384026212 * value)
	{
		___U3CRequestU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CRequestU3Ek__BackingField_11, value);
	}

	inline static int32_t get_offset_of_U3CActionU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(RuntimeAsyncResult_t4279356013, ___U3CActionU3Ek__BackingField_12)); }
	inline Action_4_t897279368 * get_U3CActionU3Ek__BackingField_12() const { return ___U3CActionU3Ek__BackingField_12; }
	inline Action_4_t897279368 ** get_address_of_U3CActionU3Ek__BackingField_12() { return &___U3CActionU3Ek__BackingField_12; }
	inline void set_U3CActionU3Ek__BackingField_12(Action_4_t897279368 * value)
	{
		___U3CActionU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CActionU3Ek__BackingField_12, value);
	}

	inline static int32_t get_offset_of_U3CAsyncOptionsU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(RuntimeAsyncResult_t4279356013, ___U3CAsyncOptionsU3Ek__BackingField_13)); }
	inline AsyncOptions_t558351272 * get_U3CAsyncOptionsU3Ek__BackingField_13() const { return ___U3CAsyncOptionsU3Ek__BackingField_13; }
	inline AsyncOptions_t558351272 ** get_address_of_U3CAsyncOptionsU3Ek__BackingField_13() { return &___U3CAsyncOptionsU3Ek__BackingField_13; }
	inline void set_U3CAsyncOptionsU3Ek__BackingField_13(AsyncOptions_t558351272 * value)
	{
		___U3CAsyncOptionsU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CAsyncOptionsU3Ek__BackingField_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
