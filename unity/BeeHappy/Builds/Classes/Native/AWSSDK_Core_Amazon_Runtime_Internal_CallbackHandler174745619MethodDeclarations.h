﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.CallbackHandler
struct CallbackHandler_t174745619;
// System.Action`1<Amazon.Runtime.IExecutionContext>
struct Action_1_t2278930134;
// Amazon.Runtime.IExecutionContext
struct IExecutionContext_t2477130752;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// Amazon.Runtime.IAsyncExecutionContext
struct IAsyncExecutionContext_t3792344986;

#include "codegen/il2cpp-codegen.h"

// System.Action`1<Amazon.Runtime.IExecutionContext> Amazon.Runtime.Internal.CallbackHandler::get_OnPreInvoke()
extern "C"  Action_1_t2278930134 * CallbackHandler_get_OnPreInvoke_m3651990765 (CallbackHandler_t174745619 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.CallbackHandler::set_OnPreInvoke(System.Action`1<Amazon.Runtime.IExecutionContext>)
extern "C"  void CallbackHandler_set_OnPreInvoke_m2430486094 (CallbackHandler_t174745619 * __this, Action_1_t2278930134 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action`1<Amazon.Runtime.IExecutionContext> Amazon.Runtime.Internal.CallbackHandler::get_OnPostInvoke()
extern "C"  Action_1_t2278930134 * CallbackHandler_get_OnPostInvoke_m543889568 (CallbackHandler_t174745619 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.CallbackHandler::set_OnPostInvoke(System.Action`1<Amazon.Runtime.IExecutionContext>)
extern "C"  void CallbackHandler_set_OnPostInvoke_m1766846829 (CallbackHandler_t174745619 * __this, Action_1_t2278930134 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.CallbackHandler::InvokeSync(Amazon.Runtime.IExecutionContext)
extern "C"  void CallbackHandler_InvokeSync_m3763213228 (CallbackHandler_t174745619 * __this, Il2CppObject * ___executionContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Amazon.Runtime.Internal.CallbackHandler::InvokeAsync(Amazon.Runtime.IAsyncExecutionContext)
extern "C"  Il2CppObject * CallbackHandler_InvokeAsync_m3274873881 (CallbackHandler_t174745619 * __this, Il2CppObject * ___executionContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.CallbackHandler::InvokeAsyncCallback(Amazon.Runtime.IAsyncExecutionContext)
extern "C"  void CallbackHandler_InvokeAsyncCallback_m895847560 (CallbackHandler_t174745619 * __this, Il2CppObject * ___executionContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.CallbackHandler::PreInvoke(Amazon.Runtime.IExecutionContext)
extern "C"  void CallbackHandler_PreInvoke_m2652037870 (CallbackHandler_t174745619 * __this, Il2CppObject * ___executionContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.CallbackHandler::PostInvoke(Amazon.Runtime.IExecutionContext)
extern "C"  void CallbackHandler_PostInvoke_m3099704867 (CallbackHandler_t174745619 * __this, Il2CppObject * ___executionContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.CallbackHandler::RaiseOnPreInvoke(Amazon.Runtime.IExecutionContext)
extern "C"  void CallbackHandler_RaiseOnPreInvoke_m1355715289 (CallbackHandler_t174745619 * __this, Il2CppObject * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.CallbackHandler::RaiseOnPostInvoke(Amazon.Runtime.IExecutionContext)
extern "C"  void CallbackHandler_RaiseOnPostInvoke_m3919729444 (CallbackHandler_t174745619 * __this, Il2CppObject * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.CallbackHandler::.ctor()
extern "C"  void CallbackHandler__ctor_m767000601 (CallbackHandler_t174745619 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
