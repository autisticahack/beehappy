﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Data.Common.DbCommandBuilder
struct DbCommandBuilder_t1858692636;
// System.String
struct String_t;
// System.Data.DataTable
struct DataTable_t3267612424;
// System.Data.DataRow
struct DataRow_t321465356;
// System.Data.Common.DbCommand
struct DbCommand_t673053565;
// System.Data.Common.DbParameter
struct DbParameter_t939375515;
// System.Data.Common.DbDataAdapter
struct DbDataAdapter_t1474708225;
// System.Data.Common.RowUpdatingEventArgs
struct RowUpdatingEventArgs_t1243529829;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "System_Data_System_Data_DataTable3267612424.h"
#include "System_Data_System_Data_DataRow321465356.h"
#include "System_Data_System_Data_Common_DbCommand673053565.h"
#include "System_Data_System_Data_Common_DbDataAdapter1474708225.h"
#include "System_Data_System_Data_Common_RowUpdatingEventArg1243529829.h"

// System.Void System.Data.Common.DbCommandBuilder::.ctor()
extern "C"  void DbCommandBuilder__ctor_m3909742192 (DbCommandBuilder_t1858692636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.DbCommandBuilder::.cctor()
extern "C"  void DbCommandBuilder__cctor_m4099758853 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.DbCommandBuilder::BuildCache(System.Boolean)
extern "C"  void DbCommandBuilder_BuildCache_m410597803 (DbCommandBuilder_t1858692636 * __this, bool ___closeConnection0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Data.Common.DbCommandBuilder::get_QuotedTableName()
extern "C"  String_t* DbCommandBuilder_get_QuotedTableName_m3882250549 (DbCommandBuilder_t1858692636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Data.Common.DbCommandBuilder::get_IsCommandGenerated()
extern "C"  bool DbCommandBuilder_get_IsCommandGenerated_m2615688605 (DbCommandBuilder_t1858692636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Data.Common.DbCommandBuilder::GetQuotedString(System.String)
extern "C"  String_t* DbCommandBuilder_GetQuotedString_m629306474 (DbCommandBuilder_t1858692636 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.DbCommandBuilder::BuildInformation(System.Data.DataTable)
extern "C"  void DbCommandBuilder_BuildInformation_m2158051743 (DbCommandBuilder_t1858692636 * __this, DataTable_t3267612424 * ___schemaTable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Data.Common.DbCommandBuilder::IncludedInInsert(System.Data.DataRow)
extern "C"  bool DbCommandBuilder_IncludedInInsert_m258290021 (DbCommandBuilder_t1858692636 * __this, DataRow_t321465356 * ___schemaRow0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Data.Common.DbCommandBuilder::IncludedInUpdate(System.Data.DataRow)
extern "C"  bool DbCommandBuilder_IncludedInUpdate_m4059218571 (DbCommandBuilder_t1858692636 * __this, DataRow_t321465356 * ___schemaRow0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Data.Common.DbCommandBuilder::IncludedInWhereClause(System.Data.DataRow)
extern "C"  bool DbCommandBuilder_IncludedInWhereClause_m2616127670 (DbCommandBuilder_t1858692636 * __this, DataRow_t321465356 * ___schemaRow0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DbCommand System.Data.Common.DbCommandBuilder::CreateDeleteCommand(System.Boolean)
extern "C"  DbCommand_t673053565 * DbCommandBuilder_CreateDeleteCommand_m3714059135 (DbCommandBuilder_t1858692636 * __this, bool ___option0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DbCommand System.Data.Common.DbCommandBuilder::CreateInsertCommand(System.Boolean,System.Data.DataRow)
extern "C"  DbCommand_t673053565 * DbCommandBuilder_CreateInsertCommand_m2439625500 (DbCommandBuilder_t1858692636 * __this, bool ___option0, DataRow_t321465356 * ___row1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.DbCommandBuilder::CreateNewCommand(System.Data.Common.DbCommand&)
extern "C"  void DbCommandBuilder_CreateNewCommand_m3666404498 (DbCommandBuilder_t1858692636 * __this, DbCommand_t673053565 ** ___command0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DbCommand System.Data.Common.DbCommandBuilder::CreateUpdateCommand(System.Boolean)
extern "C"  DbCommand_t673053565 * DbCommandBuilder_CreateUpdateCommand_m3023048899 (DbCommandBuilder_t1858692636 * __this, bool ___option0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DbParameter System.Data.Common.DbCommandBuilder::CreateParameter(System.Data.Common.DbCommand,System.Data.DataRow,System.Boolean)
extern "C"  DbParameter_t939375515 * DbCommandBuilder_CreateParameter_m2101399424 (DbCommandBuilder_t1858692636 * __this, DbCommand_t673053565 * ____dbCommand0, DataRow_t321465356 * ___schemaRow1, bool ___whereClause2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DbParameter System.Data.Common.DbCommandBuilder::CreateParameter(System.Data.Common.DbCommand,System.Int32,System.Data.DataRow)
extern "C"  DbParameter_t939375515 * DbCommandBuilder_CreateParameter_m63553406 (DbCommandBuilder_t1858692636 * __this, DbCommand_t673053565 * ____dbCommand0, int32_t ___paramIndex1, DataRow_t321465356 * ___schemaRow2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DbDataAdapter System.Data.Common.DbCommandBuilder::get_DataAdapter()
extern "C"  DbDataAdapter_t1474708225 * DbCommandBuilder_get_DataAdapter_m1541414788 (DbCommandBuilder_t1858692636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.DbCommandBuilder::set_DataAdapter(System.Data.Common.DbDataAdapter)
extern "C"  void DbCommandBuilder_set_DataAdapter_m3495427511 (DbCommandBuilder_t1858692636 * __this, DbDataAdapter_t1474708225 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Data.Common.DbCommandBuilder::get_QuotePrefix()
extern "C"  String_t* DbCommandBuilder_get_QuotePrefix_m1808550442 (DbCommandBuilder_t1858692636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.DbCommandBuilder::set_QuotePrefix(System.String)
extern "C"  void DbCommandBuilder_set_QuotePrefix_m1749772803 (DbCommandBuilder_t1858692636 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Data.Common.DbCommandBuilder::get_QuoteSuffix()
extern "C"  String_t* DbCommandBuilder_get_QuoteSuffix_m2139306787 (DbCommandBuilder_t1858692636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.DbCommandBuilder::set_QuoteSuffix(System.String)
extern "C"  void DbCommandBuilder_set_QuoteSuffix_m1289723220 (DbCommandBuilder_t1858692636 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DbCommand System.Data.Common.DbCommandBuilder::get_SourceCommand()
extern "C"  DbCommand_t673053565 * DbCommandBuilder_get_SourceCommand_m3711791097 (DbCommandBuilder_t1858692636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.DbCommandBuilder::Dispose(System.Boolean)
extern "C"  void DbCommandBuilder_Dispose_m1766393284 (DbCommandBuilder_t1858692636 * __this, bool ___disposing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DbCommand System.Data.Common.DbCommandBuilder::GetDeleteCommand()
extern "C"  DbCommand_t673053565 * DbCommandBuilder_GetDeleteCommand_m2558907444 (DbCommandBuilder_t1858692636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DbCommand System.Data.Common.DbCommandBuilder::GetDeleteCommand(System.Boolean)
extern "C"  DbCommand_t673053565 * DbCommandBuilder_GetDeleteCommand_m2358972341 (DbCommandBuilder_t1858692636 * __this, bool ___option0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DbCommand System.Data.Common.DbCommandBuilder::GetInsertCommand(System.Boolean,System.Data.DataRow)
extern "C"  DbCommand_t673053565 * DbCommandBuilder_GetInsertCommand_m2806161078 (DbCommandBuilder_t1858692636 * __this, bool ___option0, DataRow_t321465356 * ___row1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DbCommand System.Data.Common.DbCommandBuilder::GetUpdateCommand()
extern "C"  DbCommand_t673053565 * DbCommandBuilder_GetUpdateCommand_m373972470 (DbCommandBuilder_t1858692636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DbCommand System.Data.Common.DbCommandBuilder::GetUpdateCommand(System.Boolean)
extern "C"  DbCommand_t673053565 * DbCommandBuilder_GetUpdateCommand_m574402609 (DbCommandBuilder_t1858692636 * __this, bool ___option0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Data.Common.DbCommandBuilder::QuoteIdentifier(System.String)
extern "C"  String_t* DbCommandBuilder_QuoteIdentifier_m4086398068 (DbCommandBuilder_t1858692636 * __this, String_t* ___unquotedIdentifier0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Data.Common.DbCommandBuilder::UnquoteIdentifier(System.String)
extern "C"  String_t* DbCommandBuilder_UnquoteIdentifier_m2785289383 (DbCommandBuilder_t1858692636 * __this, String_t* ___quotedIdentifier0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.DbCommandBuilder::RowUpdatingHandler(System.Data.Common.RowUpdatingEventArgs)
extern "C"  void DbCommandBuilder_RowUpdatingHandler_m3404079495 (DbCommandBuilder_t1858692636 * __this, RowUpdatingEventArgs_t1243529829 * ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
