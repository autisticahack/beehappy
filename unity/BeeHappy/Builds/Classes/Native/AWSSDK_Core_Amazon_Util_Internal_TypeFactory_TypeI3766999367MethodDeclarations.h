﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Util.Internal.TypeFactory/TypeInfoWrapper
struct TypeInfoWrapper_t3766999367;
// System.Type
struct Type_t;
// System.Reflection.FieldInfo
struct FieldInfo_t;
// System.String
struct String_t;
// System.Collections.Generic.IEnumerable`1<System.Reflection.FieldInfo>
struct IEnumerable_1_t547167195;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// Amazon.Util.Internal.ITypeInfo[]
struct ITypeInfoU5BU5D_t39398432;
// System.Reflection.PropertyInfo
struct PropertyInfo_t;
// Amazon.Util.Internal.ITypeInfo
struct ITypeInfo_t3592676621;
// System.Reflection.Assembly
struct Assembly_t4268412390;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Amazon.Util.Internal.TypeFactory/TypeInfoWrapper::.ctor(System.Type)
extern "C"  void TypeInfoWrapper__ctor_m3855231117 (TypeInfoWrapper_t3766999367 * __this, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Amazon.Util.Internal.TypeFactory/TypeInfoWrapper::get_BaseType()
extern "C"  Type_t * TypeInfoWrapper_get_BaseType_m3677328616 (TypeInfoWrapper_t3766999367 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.FieldInfo Amazon.Util.Internal.TypeFactory/TypeInfoWrapper::GetField(System.String)
extern "C"  FieldInfo_t * TypeInfoWrapper_GetField_m2942177857 (TypeInfoWrapper_t3766999367 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<System.Reflection.FieldInfo> Amazon.Util.Internal.TypeFactory/TypeInfoWrapper::GetFields()
extern "C"  Il2CppObject* TypeInfoWrapper_GetFields_m528571895 (TypeInfoWrapper_t3766999367 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Util.Internal.TypeFactory/TypeInfoWrapper::get_IsEnum()
extern "C"  bool TypeInfoWrapper_get_IsEnum_m2230776560 (TypeInfoWrapper_t3766999367 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo Amazon.Util.Internal.TypeFactory/TypeInfoWrapper::GetMethod(System.String,Amazon.Util.Internal.ITypeInfo[])
extern "C"  MethodInfo_t * TypeInfoWrapper_GetMethod_m473540063 (TypeInfoWrapper_t3766999367 * __this, String_t* ___name0, ITypeInfoU5BU5D_t39398432* ___paramTypes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.PropertyInfo Amazon.Util.Internal.TypeFactory/TypeInfoWrapper::GetProperty(System.String)
extern "C"  PropertyInfo_t * TypeInfoWrapper_GetProperty_m63218089 (TypeInfoWrapper_t3766999367 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Util.Internal.TypeFactory/TypeInfoWrapper::IsAssignableFrom(Amazon.Util.Internal.ITypeInfo)
extern "C"  bool TypeInfoWrapper_IsAssignableFrom_m1142487801 (TypeInfoWrapper_t3766999367 * __this, Il2CppObject * ___typeInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Assembly Amazon.Util.Internal.TypeFactory/TypeInfoWrapper::get_Assembly()
extern "C"  Assembly_t4268412390 * TypeInfoWrapper_get_Assembly_m1606958774 (TypeInfoWrapper_t3766999367 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
