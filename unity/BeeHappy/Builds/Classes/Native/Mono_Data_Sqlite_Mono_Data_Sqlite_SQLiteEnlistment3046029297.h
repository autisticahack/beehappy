﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Mono.Data.Sqlite.SqliteTransaction
struct SqliteTransaction_t2796588764;
// System.Transactions.Transaction
struct Transaction_t869361102;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Data.Sqlite.SQLiteEnlistment
struct  SQLiteEnlistment_t3046029297  : public Il2CppObject
{
public:
	// Mono.Data.Sqlite.SqliteTransaction Mono.Data.Sqlite.SQLiteEnlistment::_transaction
	SqliteTransaction_t2796588764 * ____transaction_0;
	// System.Transactions.Transaction Mono.Data.Sqlite.SQLiteEnlistment::_scope
	Transaction_t869361102 * ____scope_1;
	// System.Boolean Mono.Data.Sqlite.SQLiteEnlistment::_disposeConnection
	bool ____disposeConnection_2;

public:
	inline static int32_t get_offset_of__transaction_0() { return static_cast<int32_t>(offsetof(SQLiteEnlistment_t3046029297, ____transaction_0)); }
	inline SqliteTransaction_t2796588764 * get__transaction_0() const { return ____transaction_0; }
	inline SqliteTransaction_t2796588764 ** get_address_of__transaction_0() { return &____transaction_0; }
	inline void set__transaction_0(SqliteTransaction_t2796588764 * value)
	{
		____transaction_0 = value;
		Il2CppCodeGenWriteBarrier(&____transaction_0, value);
	}

	inline static int32_t get_offset_of__scope_1() { return static_cast<int32_t>(offsetof(SQLiteEnlistment_t3046029297, ____scope_1)); }
	inline Transaction_t869361102 * get__scope_1() const { return ____scope_1; }
	inline Transaction_t869361102 ** get_address_of__scope_1() { return &____scope_1; }
	inline void set__scope_1(Transaction_t869361102 * value)
	{
		____scope_1 = value;
		Il2CppCodeGenWriteBarrier(&____scope_1, value);
	}

	inline static int32_t get_offset_of__disposeConnection_2() { return static_cast<int32_t>(offsetof(SQLiteEnlistment_t3046029297, ____disposeConnection_2)); }
	inline bool get__disposeConnection_2() const { return ____disposeConnection_2; }
	inline bool* get_address_of__disposeConnection_2() { return &____disposeConnection_2; }
	inline void set__disposeConnection_2(bool value)
	{
		____disposeConnection_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
