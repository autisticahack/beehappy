﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Amazon.CognitoSync.SyncManager.SyncConflict>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m3802088352(__this, ___l0, method) ((  void (*) (Enumerator_t1934161115 *, List_1_t2399431441 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Amazon.CognitoSync.SyncManager.SyncConflict>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1470989714(__this, method) ((  void (*) (Enumerator_t1934161115 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Amazon.CognitoSync.SyncManager.SyncConflict>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1712341390(__this, method) ((  Il2CppObject * (*) (Enumerator_t1934161115 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Amazon.CognitoSync.SyncManager.SyncConflict>::Dispose()
#define Enumerator_Dispose_m1610223363(__this, method) ((  void (*) (Enumerator_t1934161115 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Amazon.CognitoSync.SyncManager.SyncConflict>::VerifyState()
#define Enumerator_VerifyState_m489448834(__this, method) ((  void (*) (Enumerator_t1934161115 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Amazon.CognitoSync.SyncManager.SyncConflict>::MoveNext()
#define Enumerator_MoveNext_m2843959312(__this, method) ((  bool (*) (Enumerator_t1934161115 *, const MethodInfo*))Enumerator_MoveNext_m984484162_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Amazon.CognitoSync.SyncManager.SyncConflict>::get_Current()
#define Enumerator_get_Current_m1743861522(__this, method) ((  SyncConflict_t3030310309 * (*) (Enumerator_t1934161115 *, const MethodInfo*))Enumerator_get_Current_m2193722336_gshared)(__this, method)
