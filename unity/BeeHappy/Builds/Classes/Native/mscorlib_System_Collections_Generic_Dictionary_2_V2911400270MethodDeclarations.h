﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>
struct Dictionary_2_t1224867506;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2911400270.h"
#include "AWSSDK_Core_Amazon_Util_Internal_PlatformServices_1632807378.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1575581033_gshared (Enumerator_t2911400270 * __this, Dictionary_2_t1224867506 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m1575581033(__this, ___host0, method) ((  void (*) (Enumerator_t2911400270 *, Dictionary_2_t1224867506 *, const MethodInfo*))Enumerator__ctor_m1575581033_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3458593286_gshared (Enumerator_t2911400270 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3458593286(__this, method) ((  Il2CppObject * (*) (Enumerator_t2911400270 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3458593286_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m203558566_gshared (Enumerator_t2911400270 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m203558566(__this, method) ((  void (*) (Enumerator_t2911400270 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m203558566_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::Dispose()
extern "C"  void Enumerator_Dispose_m3181308769_gshared (Enumerator_t2911400270 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3181308769(__this, method) ((  void (*) (Enumerator_t2911400270 *, const MethodInfo*))Enumerator_Dispose_m3181308769_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2790411218_gshared (Enumerator_t2911400270 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2790411218(__this, method) ((  bool (*) (Enumerator_t2911400270 *, const MethodInfo*))Enumerator_MoveNext_m2790411218_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m1134368040_gshared (Enumerator_t2911400270 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1134368040(__this, method) ((  int32_t (*) (Enumerator_t2911400270 *, const MethodInfo*))Enumerator_get_Current_m1134368040_gshared)(__this, method)
