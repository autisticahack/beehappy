﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Data.Common.DbTransaction
struct DbTransaction_t3114611728;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Data.Common.DbTransaction::.ctor()
extern "C"  void DbTransaction__ctor_m1406086638 (DbTransaction_t3114611728 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.DbTransaction::Dispose()
extern "C"  void DbTransaction_Dispose_m3039393647 (DbTransaction_t3114611728 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.DbTransaction::Dispose(System.Boolean)
extern "C"  void DbTransaction_Dispose_m2024986834 (DbTransaction_t3114611728 * __this, bool ___disposing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
