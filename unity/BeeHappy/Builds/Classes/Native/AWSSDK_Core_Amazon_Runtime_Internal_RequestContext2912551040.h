﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.Runtime.Internal.IRequest
struct IRequest_t2400804350;
// Amazon.Runtime.Internal.Util.RequestMetrics
struct RequestMetrics_t218029284;
// Amazon.Runtime.Internal.Auth.AbstractAWSSigner
struct AbstractAWSSigner_t2114314031;
// Amazon.Runtime.IClientConfig
struct IClientConfig_t4078933728;
// Amazon.Runtime.AmazonWebServiceRequest
struct AmazonWebServiceRequest_t3384026212;
// Amazon.Runtime.Internal.Transform.IMarshaller`2<Amazon.Runtime.Internal.IRequest,Amazon.Runtime.AmazonWebServiceRequest>
struct IMarshaller_2_t3817076711;
// Amazon.Runtime.Internal.Transform.ResponseUnmarshaller
struct ResponseUnmarshaller_t3934041557;
// Amazon.Runtime.ImmutableCredentials
struct ImmutableCredentials_t282353664;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.RequestContext
struct  RequestContext_t2912551040  : public Il2CppObject
{
public:
	// Amazon.Runtime.Internal.IRequest Amazon.Runtime.Internal.RequestContext::<Request>k__BackingField
	Il2CppObject * ___U3CRequestU3Ek__BackingField_0;
	// Amazon.Runtime.Internal.Util.RequestMetrics Amazon.Runtime.Internal.RequestContext::<Metrics>k__BackingField
	RequestMetrics_t218029284 * ___U3CMetricsU3Ek__BackingField_1;
	// Amazon.Runtime.Internal.Auth.AbstractAWSSigner Amazon.Runtime.Internal.RequestContext::<Signer>k__BackingField
	AbstractAWSSigner_t2114314031 * ___U3CSignerU3Ek__BackingField_2;
	// Amazon.Runtime.IClientConfig Amazon.Runtime.Internal.RequestContext::<ClientConfig>k__BackingField
	Il2CppObject * ___U3CClientConfigU3Ek__BackingField_3;
	// System.Int32 Amazon.Runtime.Internal.RequestContext::<Retries>k__BackingField
	int32_t ___U3CRetriesU3Ek__BackingField_4;
	// System.Boolean Amazon.Runtime.Internal.RequestContext::<IsSigned>k__BackingField
	bool ___U3CIsSignedU3Ek__BackingField_5;
	// System.Boolean Amazon.Runtime.Internal.RequestContext::<IsAsync>k__BackingField
	bool ___U3CIsAsyncU3Ek__BackingField_6;
	// Amazon.Runtime.AmazonWebServiceRequest Amazon.Runtime.Internal.RequestContext::<OriginalRequest>k__BackingField
	AmazonWebServiceRequest_t3384026212 * ___U3COriginalRequestU3Ek__BackingField_7;
	// Amazon.Runtime.Internal.Transform.IMarshaller`2<Amazon.Runtime.Internal.IRequest,Amazon.Runtime.AmazonWebServiceRequest> Amazon.Runtime.Internal.RequestContext::<Marshaller>k__BackingField
	Il2CppObject* ___U3CMarshallerU3Ek__BackingField_8;
	// Amazon.Runtime.Internal.Transform.ResponseUnmarshaller Amazon.Runtime.Internal.RequestContext::<Unmarshaller>k__BackingField
	ResponseUnmarshaller_t3934041557 * ___U3CUnmarshallerU3Ek__BackingField_9;
	// Amazon.Runtime.ImmutableCredentials Amazon.Runtime.Internal.RequestContext::<ImmutableCredentials>k__BackingField
	ImmutableCredentials_t282353664 * ___U3CImmutableCredentialsU3Ek__BackingField_10;

public:
	inline static int32_t get_offset_of_U3CRequestU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RequestContext_t2912551040, ___U3CRequestU3Ek__BackingField_0)); }
	inline Il2CppObject * get_U3CRequestU3Ek__BackingField_0() const { return ___U3CRequestU3Ek__BackingField_0; }
	inline Il2CppObject ** get_address_of_U3CRequestU3Ek__BackingField_0() { return &___U3CRequestU3Ek__BackingField_0; }
	inline void set_U3CRequestU3Ek__BackingField_0(Il2CppObject * value)
	{
		___U3CRequestU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CRequestU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CMetricsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RequestContext_t2912551040, ___U3CMetricsU3Ek__BackingField_1)); }
	inline RequestMetrics_t218029284 * get_U3CMetricsU3Ek__BackingField_1() const { return ___U3CMetricsU3Ek__BackingField_1; }
	inline RequestMetrics_t218029284 ** get_address_of_U3CMetricsU3Ek__BackingField_1() { return &___U3CMetricsU3Ek__BackingField_1; }
	inline void set_U3CMetricsU3Ek__BackingField_1(RequestMetrics_t218029284 * value)
	{
		___U3CMetricsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CMetricsU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CSignerU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(RequestContext_t2912551040, ___U3CSignerU3Ek__BackingField_2)); }
	inline AbstractAWSSigner_t2114314031 * get_U3CSignerU3Ek__BackingField_2() const { return ___U3CSignerU3Ek__BackingField_2; }
	inline AbstractAWSSigner_t2114314031 ** get_address_of_U3CSignerU3Ek__BackingField_2() { return &___U3CSignerU3Ek__BackingField_2; }
	inline void set_U3CSignerU3Ek__BackingField_2(AbstractAWSSigner_t2114314031 * value)
	{
		___U3CSignerU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CSignerU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CClientConfigU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(RequestContext_t2912551040, ___U3CClientConfigU3Ek__BackingField_3)); }
	inline Il2CppObject * get_U3CClientConfigU3Ek__BackingField_3() const { return ___U3CClientConfigU3Ek__BackingField_3; }
	inline Il2CppObject ** get_address_of_U3CClientConfigU3Ek__BackingField_3() { return &___U3CClientConfigU3Ek__BackingField_3; }
	inline void set_U3CClientConfigU3Ek__BackingField_3(Il2CppObject * value)
	{
		___U3CClientConfigU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CClientConfigU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3CRetriesU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(RequestContext_t2912551040, ___U3CRetriesU3Ek__BackingField_4)); }
	inline int32_t get_U3CRetriesU3Ek__BackingField_4() const { return ___U3CRetriesU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CRetriesU3Ek__BackingField_4() { return &___U3CRetriesU3Ek__BackingField_4; }
	inline void set_U3CRetriesU3Ek__BackingField_4(int32_t value)
	{
		___U3CRetriesU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CIsSignedU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(RequestContext_t2912551040, ___U3CIsSignedU3Ek__BackingField_5)); }
	inline bool get_U3CIsSignedU3Ek__BackingField_5() const { return ___U3CIsSignedU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CIsSignedU3Ek__BackingField_5() { return &___U3CIsSignedU3Ek__BackingField_5; }
	inline void set_U3CIsSignedU3Ek__BackingField_5(bool value)
	{
		___U3CIsSignedU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CIsAsyncU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(RequestContext_t2912551040, ___U3CIsAsyncU3Ek__BackingField_6)); }
	inline bool get_U3CIsAsyncU3Ek__BackingField_6() const { return ___U3CIsAsyncU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CIsAsyncU3Ek__BackingField_6() { return &___U3CIsAsyncU3Ek__BackingField_6; }
	inline void set_U3CIsAsyncU3Ek__BackingField_6(bool value)
	{
		___U3CIsAsyncU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3COriginalRequestU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(RequestContext_t2912551040, ___U3COriginalRequestU3Ek__BackingField_7)); }
	inline AmazonWebServiceRequest_t3384026212 * get_U3COriginalRequestU3Ek__BackingField_7() const { return ___U3COriginalRequestU3Ek__BackingField_7; }
	inline AmazonWebServiceRequest_t3384026212 ** get_address_of_U3COriginalRequestU3Ek__BackingField_7() { return &___U3COriginalRequestU3Ek__BackingField_7; }
	inline void set_U3COriginalRequestU3Ek__BackingField_7(AmazonWebServiceRequest_t3384026212 * value)
	{
		___U3COriginalRequestU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3COriginalRequestU3Ek__BackingField_7, value);
	}

	inline static int32_t get_offset_of_U3CMarshallerU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(RequestContext_t2912551040, ___U3CMarshallerU3Ek__BackingField_8)); }
	inline Il2CppObject* get_U3CMarshallerU3Ek__BackingField_8() const { return ___U3CMarshallerU3Ek__BackingField_8; }
	inline Il2CppObject** get_address_of_U3CMarshallerU3Ek__BackingField_8() { return &___U3CMarshallerU3Ek__BackingField_8; }
	inline void set_U3CMarshallerU3Ek__BackingField_8(Il2CppObject* value)
	{
		___U3CMarshallerU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CMarshallerU3Ek__BackingField_8, value);
	}

	inline static int32_t get_offset_of_U3CUnmarshallerU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(RequestContext_t2912551040, ___U3CUnmarshallerU3Ek__BackingField_9)); }
	inline ResponseUnmarshaller_t3934041557 * get_U3CUnmarshallerU3Ek__BackingField_9() const { return ___U3CUnmarshallerU3Ek__BackingField_9; }
	inline ResponseUnmarshaller_t3934041557 ** get_address_of_U3CUnmarshallerU3Ek__BackingField_9() { return &___U3CUnmarshallerU3Ek__BackingField_9; }
	inline void set_U3CUnmarshallerU3Ek__BackingField_9(ResponseUnmarshaller_t3934041557 * value)
	{
		___U3CUnmarshallerU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CUnmarshallerU3Ek__BackingField_9, value);
	}

	inline static int32_t get_offset_of_U3CImmutableCredentialsU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(RequestContext_t2912551040, ___U3CImmutableCredentialsU3Ek__BackingField_10)); }
	inline ImmutableCredentials_t282353664 * get_U3CImmutableCredentialsU3Ek__BackingField_10() const { return ___U3CImmutableCredentialsU3Ek__BackingField_10; }
	inline ImmutableCredentials_t282353664 ** get_address_of_U3CImmutableCredentialsU3Ek__BackingField_10() { return &___U3CImmutableCredentialsU3Ek__BackingField_10; }
	inline void set_U3CImmutableCredentialsU3Ek__BackingField_10(ImmutableCredentials_t282353664 * value)
	{
		___U3CImmutableCredentialsU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CImmutableCredentialsU3Ek__BackingField_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
