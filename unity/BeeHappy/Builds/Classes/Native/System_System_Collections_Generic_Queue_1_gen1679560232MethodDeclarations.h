﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Queue_1_gen2509106130MethodDeclarations.h"

// System.Void System.Collections.Generic.Queue`1<Amazon.Runtime.IUnityHttpRequest>::.ctor()
#define Queue_1__ctor_m3189541843(__this, method) ((  void (*) (Queue_1_t1679560232 *, const MethodInfo*))Queue_1__ctor_m1845245813_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<Amazon.Runtime.IUnityHttpRequest>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Queue_1_System_Collections_ICollection_CopyTo_m1605518436(__this, ___array0, ___idx1, method) ((  void (*) (Queue_1_t1679560232 *, Il2CppArray *, int32_t, const MethodInfo*))Queue_1_System_Collections_ICollection_CopyTo_m772787461_gshared)(__this, ___array0, ___idx1, method)
// System.Boolean System.Collections.Generic.Queue`1<Amazon.Runtime.IUnityHttpRequest>::System.Collections.ICollection.get_IsSynchronized()
#define Queue_1_System_Collections_ICollection_get_IsSynchronized_m1670989704(__this, method) ((  bool (*) (Queue_1_t1679560232 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_IsSynchronized_m307125669_gshared)(__this, method)
// System.Object System.Collections.Generic.Queue`1<Amazon.Runtime.IUnityHttpRequest>::System.Collections.ICollection.get_SyncRoot()
#define Queue_1_System_Collections_ICollection_get_SyncRoot_m1516276852(__this, method) ((  Il2CppObject * (*) (Queue_1_t1679560232 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_SyncRoot_m311152041_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Queue`1<Amazon.Runtime.IUnityHttpRequest>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2184175310(__this, method) ((  Il2CppObject* (*) (Queue_1_t1679560232 *, const MethodInfo*))Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m4179169157_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Queue`1<Amazon.Runtime.IUnityHttpRequest>::System.Collections.IEnumerable.GetEnumerator()
#define Queue_1_System_Collections_IEnumerable_GetEnumerator_m743171995(__this, method) ((  Il2CppObject * (*) (Queue_1_t1679560232 *, const MethodInfo*))Queue_1_System_Collections_IEnumerable_GetEnumerator_m251608368_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<Amazon.Runtime.IUnityHttpRequest>::CopyTo(T[],System.Int32)
#define Queue_1_CopyTo_m1156952339(__this, ___array0, ___idx1, method) ((  void (*) (Queue_1_t1679560232 *, IUnityHttpRequestU5BU5D_t1824570408*, int32_t, const MethodInfo*))Queue_1_CopyTo_m1419617898_gshared)(__this, ___array0, ___idx1, method)
// T System.Collections.Generic.Queue`1<Amazon.Runtime.IUnityHttpRequest>::Dequeue()
#define Queue_1_Dequeue_m3951473501(__this, method) ((  Il2CppObject * (*) (Queue_1_t1679560232 *, const MethodInfo*))Queue_1_Dequeue_m4118160228_gshared)(__this, method)
// T System.Collections.Generic.Queue`1<Amazon.Runtime.IUnityHttpRequest>::Peek()
#define Queue_1_Peek_m928461828(__this, method) ((  Il2CppObject * (*) (Queue_1_t1679560232 *, const MethodInfo*))Queue_1_Peek_m1463479953_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<Amazon.Runtime.IUnityHttpRequest>::Enqueue(T)
#define Queue_1_Enqueue_m3202618404(__this, ___item0, method) ((  void (*) (Queue_1_t1679560232 *, Il2CppObject *, const MethodInfo*))Queue_1_Enqueue_m2424099095_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Queue`1<Amazon.Runtime.IUnityHttpRequest>::SetCapacity(System.Int32)
#define Queue_1_SetCapacity_m1341381013(__this, ___new_size0, method) ((  void (*) (Queue_1_t1679560232 *, int32_t, const MethodInfo*))Queue_1_SetCapacity_m3858927842_gshared)(__this, ___new_size0, method)
// System.Int32 System.Collections.Generic.Queue`1<Amazon.Runtime.IUnityHttpRequest>::get_Count()
#define Queue_1_get_Count_m1404474147(__this, method) ((  int32_t (*) (Queue_1_t1679560232 *, const MethodInfo*))Queue_1_get_Count_m3795587777_gshared)(__this, method)
// System.Collections.Generic.Queue`1/Enumerator<T> System.Collections.Generic.Queue`1<Amazon.Runtime.IUnityHttpRequest>::GetEnumerator()
#define Queue_1_GetEnumerator_m911588581(__this, method) ((  Enumerator_t2189623312  (*) (Queue_1_t1679560232 *, const MethodInfo*))Queue_1_GetEnumerator_m2842671368_gshared)(__this, method)
