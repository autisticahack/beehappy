﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Type
struct Type_t;
// System.Reflection.PropertyInfo[]
struct PropertyInfoU5BU5D_t1736152084;
// System.Reflection.MethodInfo[]
struct MethodInfoU5BU5D_t152480188;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Object
struct Il2CppObject;
// Amazon.Runtime.Internal.DownloadHandlerBufferWrapper
struct DownloadHandlerBufferWrapper_t1800693841;
// Amazon.Runtime.Internal.UploadHandlerRawWrapper
struct UploadHandlerRawWrapper_t3528989734;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.UnityWebRequestWrapper
struct  UnityWebRequestWrapper_t1542496577  : public Il2CppObject
{
public:
	// System.Object Amazon.Runtime.Internal.UnityWebRequestWrapper::unityWebRequestInstance
	Il2CppObject * ___unityWebRequestInstance_15;
	// Amazon.Runtime.Internal.DownloadHandlerBufferWrapper Amazon.Runtime.Internal.UnityWebRequestWrapper::downloadHandler
	DownloadHandlerBufferWrapper_t1800693841 * ___downloadHandler_16;
	// Amazon.Runtime.Internal.UploadHandlerRawWrapper Amazon.Runtime.Internal.UnityWebRequestWrapper::uploadHandler
	UploadHandlerRawWrapper_t3528989734 * ___uploadHandler_17;
	// System.Boolean Amazon.Runtime.Internal.UnityWebRequestWrapper::disposedValue
	bool ___disposedValue_18;

public:
	inline static int32_t get_offset_of_unityWebRequestInstance_15() { return static_cast<int32_t>(offsetof(UnityWebRequestWrapper_t1542496577, ___unityWebRequestInstance_15)); }
	inline Il2CppObject * get_unityWebRequestInstance_15() const { return ___unityWebRequestInstance_15; }
	inline Il2CppObject ** get_address_of_unityWebRequestInstance_15() { return &___unityWebRequestInstance_15; }
	inline void set_unityWebRequestInstance_15(Il2CppObject * value)
	{
		___unityWebRequestInstance_15 = value;
		Il2CppCodeGenWriteBarrier(&___unityWebRequestInstance_15, value);
	}

	inline static int32_t get_offset_of_downloadHandler_16() { return static_cast<int32_t>(offsetof(UnityWebRequestWrapper_t1542496577, ___downloadHandler_16)); }
	inline DownloadHandlerBufferWrapper_t1800693841 * get_downloadHandler_16() const { return ___downloadHandler_16; }
	inline DownloadHandlerBufferWrapper_t1800693841 ** get_address_of_downloadHandler_16() { return &___downloadHandler_16; }
	inline void set_downloadHandler_16(DownloadHandlerBufferWrapper_t1800693841 * value)
	{
		___downloadHandler_16 = value;
		Il2CppCodeGenWriteBarrier(&___downloadHandler_16, value);
	}

	inline static int32_t get_offset_of_uploadHandler_17() { return static_cast<int32_t>(offsetof(UnityWebRequestWrapper_t1542496577, ___uploadHandler_17)); }
	inline UploadHandlerRawWrapper_t3528989734 * get_uploadHandler_17() const { return ___uploadHandler_17; }
	inline UploadHandlerRawWrapper_t3528989734 ** get_address_of_uploadHandler_17() { return &___uploadHandler_17; }
	inline void set_uploadHandler_17(UploadHandlerRawWrapper_t3528989734 * value)
	{
		___uploadHandler_17 = value;
		Il2CppCodeGenWriteBarrier(&___uploadHandler_17, value);
	}

	inline static int32_t get_offset_of_disposedValue_18() { return static_cast<int32_t>(offsetof(UnityWebRequestWrapper_t1542496577, ___disposedValue_18)); }
	inline bool get_disposedValue_18() const { return ___disposedValue_18; }
	inline bool* get_address_of_disposedValue_18() { return &___disposedValue_18; }
	inline void set_disposedValue_18(bool value)
	{
		___disposedValue_18 = value;
	}
};

struct UnityWebRequestWrapper_t1542496577_StaticFields
{
public:
	// System.Type Amazon.Runtime.Internal.UnityWebRequestWrapper::unityWebRequestType
	Type_t * ___unityWebRequestType_0;
	// System.Reflection.PropertyInfo[] Amazon.Runtime.Internal.UnityWebRequestWrapper::unityWebRequestProperties
	PropertyInfoU5BU5D_t1736152084* ___unityWebRequestProperties_1;
	// System.Reflection.MethodInfo[] Amazon.Runtime.Internal.UnityWebRequestWrapper::unityWebRequestMethods
	MethodInfoU5BU5D_t152480188* ___unityWebRequestMethods_2;
	// System.Reflection.MethodInfo Amazon.Runtime.Internal.UnityWebRequestWrapper::setRequestHeaderMethod
	MethodInfo_t * ___setRequestHeaderMethod_3;
	// System.Reflection.MethodInfo Amazon.Runtime.Internal.UnityWebRequestWrapper::sendMethod
	MethodInfo_t * ___sendMethod_4;
	// System.Reflection.MethodInfo Amazon.Runtime.Internal.UnityWebRequestWrapper::getResponseHeadersMethod
	MethodInfo_t * ___getResponseHeadersMethod_5;
	// System.Reflection.MethodInfo Amazon.Runtime.Internal.UnityWebRequestWrapper::isDoneGetMethod
	MethodInfo_t * ___isDoneGetMethod_6;
	// System.Reflection.MethodInfo Amazon.Runtime.Internal.UnityWebRequestWrapper::downloadProgressGetMethod
	MethodInfo_t * ___downloadProgressGetMethod_7;
	// System.Reflection.MethodInfo Amazon.Runtime.Internal.UnityWebRequestWrapper::uploadProgressGetMethod
	MethodInfo_t * ___uploadProgressGetMethod_8;
	// System.Reflection.MethodInfo Amazon.Runtime.Internal.UnityWebRequestWrapper::isErrorGetMethod
	MethodInfo_t * ___isErrorGetMethod_9;
	// System.Reflection.MethodInfo Amazon.Runtime.Internal.UnityWebRequestWrapper::downloadedBytesGetMethod
	MethodInfo_t * ___downloadedBytesGetMethod_10;
	// System.Reflection.MethodInfo Amazon.Runtime.Internal.UnityWebRequestWrapper::responseCodeGetMethod
	MethodInfo_t * ___responseCodeGetMethod_11;
	// System.Reflection.MethodInfo Amazon.Runtime.Internal.UnityWebRequestWrapper::downloadHandlerSetMethod
	MethodInfo_t * ___downloadHandlerSetMethod_12;
	// System.Reflection.MethodInfo Amazon.Runtime.Internal.UnityWebRequestWrapper::uploadHandlerSetMethod
	MethodInfo_t * ___uploadHandlerSetMethod_13;
	// System.Reflection.MethodInfo Amazon.Runtime.Internal.UnityWebRequestWrapper::errorGetMethod
	MethodInfo_t * ___errorGetMethod_14;

public:
	inline static int32_t get_offset_of_unityWebRequestType_0() { return static_cast<int32_t>(offsetof(UnityWebRequestWrapper_t1542496577_StaticFields, ___unityWebRequestType_0)); }
	inline Type_t * get_unityWebRequestType_0() const { return ___unityWebRequestType_0; }
	inline Type_t ** get_address_of_unityWebRequestType_0() { return &___unityWebRequestType_0; }
	inline void set_unityWebRequestType_0(Type_t * value)
	{
		___unityWebRequestType_0 = value;
		Il2CppCodeGenWriteBarrier(&___unityWebRequestType_0, value);
	}

	inline static int32_t get_offset_of_unityWebRequestProperties_1() { return static_cast<int32_t>(offsetof(UnityWebRequestWrapper_t1542496577_StaticFields, ___unityWebRequestProperties_1)); }
	inline PropertyInfoU5BU5D_t1736152084* get_unityWebRequestProperties_1() const { return ___unityWebRequestProperties_1; }
	inline PropertyInfoU5BU5D_t1736152084** get_address_of_unityWebRequestProperties_1() { return &___unityWebRequestProperties_1; }
	inline void set_unityWebRequestProperties_1(PropertyInfoU5BU5D_t1736152084* value)
	{
		___unityWebRequestProperties_1 = value;
		Il2CppCodeGenWriteBarrier(&___unityWebRequestProperties_1, value);
	}

	inline static int32_t get_offset_of_unityWebRequestMethods_2() { return static_cast<int32_t>(offsetof(UnityWebRequestWrapper_t1542496577_StaticFields, ___unityWebRequestMethods_2)); }
	inline MethodInfoU5BU5D_t152480188* get_unityWebRequestMethods_2() const { return ___unityWebRequestMethods_2; }
	inline MethodInfoU5BU5D_t152480188** get_address_of_unityWebRequestMethods_2() { return &___unityWebRequestMethods_2; }
	inline void set_unityWebRequestMethods_2(MethodInfoU5BU5D_t152480188* value)
	{
		___unityWebRequestMethods_2 = value;
		Il2CppCodeGenWriteBarrier(&___unityWebRequestMethods_2, value);
	}

	inline static int32_t get_offset_of_setRequestHeaderMethod_3() { return static_cast<int32_t>(offsetof(UnityWebRequestWrapper_t1542496577_StaticFields, ___setRequestHeaderMethod_3)); }
	inline MethodInfo_t * get_setRequestHeaderMethod_3() const { return ___setRequestHeaderMethod_3; }
	inline MethodInfo_t ** get_address_of_setRequestHeaderMethod_3() { return &___setRequestHeaderMethod_3; }
	inline void set_setRequestHeaderMethod_3(MethodInfo_t * value)
	{
		___setRequestHeaderMethod_3 = value;
		Il2CppCodeGenWriteBarrier(&___setRequestHeaderMethod_3, value);
	}

	inline static int32_t get_offset_of_sendMethod_4() { return static_cast<int32_t>(offsetof(UnityWebRequestWrapper_t1542496577_StaticFields, ___sendMethod_4)); }
	inline MethodInfo_t * get_sendMethod_4() const { return ___sendMethod_4; }
	inline MethodInfo_t ** get_address_of_sendMethod_4() { return &___sendMethod_4; }
	inline void set_sendMethod_4(MethodInfo_t * value)
	{
		___sendMethod_4 = value;
		Il2CppCodeGenWriteBarrier(&___sendMethod_4, value);
	}

	inline static int32_t get_offset_of_getResponseHeadersMethod_5() { return static_cast<int32_t>(offsetof(UnityWebRequestWrapper_t1542496577_StaticFields, ___getResponseHeadersMethod_5)); }
	inline MethodInfo_t * get_getResponseHeadersMethod_5() const { return ___getResponseHeadersMethod_5; }
	inline MethodInfo_t ** get_address_of_getResponseHeadersMethod_5() { return &___getResponseHeadersMethod_5; }
	inline void set_getResponseHeadersMethod_5(MethodInfo_t * value)
	{
		___getResponseHeadersMethod_5 = value;
		Il2CppCodeGenWriteBarrier(&___getResponseHeadersMethod_5, value);
	}

	inline static int32_t get_offset_of_isDoneGetMethod_6() { return static_cast<int32_t>(offsetof(UnityWebRequestWrapper_t1542496577_StaticFields, ___isDoneGetMethod_6)); }
	inline MethodInfo_t * get_isDoneGetMethod_6() const { return ___isDoneGetMethod_6; }
	inline MethodInfo_t ** get_address_of_isDoneGetMethod_6() { return &___isDoneGetMethod_6; }
	inline void set_isDoneGetMethod_6(MethodInfo_t * value)
	{
		___isDoneGetMethod_6 = value;
		Il2CppCodeGenWriteBarrier(&___isDoneGetMethod_6, value);
	}

	inline static int32_t get_offset_of_downloadProgressGetMethod_7() { return static_cast<int32_t>(offsetof(UnityWebRequestWrapper_t1542496577_StaticFields, ___downloadProgressGetMethod_7)); }
	inline MethodInfo_t * get_downloadProgressGetMethod_7() const { return ___downloadProgressGetMethod_7; }
	inline MethodInfo_t ** get_address_of_downloadProgressGetMethod_7() { return &___downloadProgressGetMethod_7; }
	inline void set_downloadProgressGetMethod_7(MethodInfo_t * value)
	{
		___downloadProgressGetMethod_7 = value;
		Il2CppCodeGenWriteBarrier(&___downloadProgressGetMethod_7, value);
	}

	inline static int32_t get_offset_of_uploadProgressGetMethod_8() { return static_cast<int32_t>(offsetof(UnityWebRequestWrapper_t1542496577_StaticFields, ___uploadProgressGetMethod_8)); }
	inline MethodInfo_t * get_uploadProgressGetMethod_8() const { return ___uploadProgressGetMethod_8; }
	inline MethodInfo_t ** get_address_of_uploadProgressGetMethod_8() { return &___uploadProgressGetMethod_8; }
	inline void set_uploadProgressGetMethod_8(MethodInfo_t * value)
	{
		___uploadProgressGetMethod_8 = value;
		Il2CppCodeGenWriteBarrier(&___uploadProgressGetMethod_8, value);
	}

	inline static int32_t get_offset_of_isErrorGetMethod_9() { return static_cast<int32_t>(offsetof(UnityWebRequestWrapper_t1542496577_StaticFields, ___isErrorGetMethod_9)); }
	inline MethodInfo_t * get_isErrorGetMethod_9() const { return ___isErrorGetMethod_9; }
	inline MethodInfo_t ** get_address_of_isErrorGetMethod_9() { return &___isErrorGetMethod_9; }
	inline void set_isErrorGetMethod_9(MethodInfo_t * value)
	{
		___isErrorGetMethod_9 = value;
		Il2CppCodeGenWriteBarrier(&___isErrorGetMethod_9, value);
	}

	inline static int32_t get_offset_of_downloadedBytesGetMethod_10() { return static_cast<int32_t>(offsetof(UnityWebRequestWrapper_t1542496577_StaticFields, ___downloadedBytesGetMethod_10)); }
	inline MethodInfo_t * get_downloadedBytesGetMethod_10() const { return ___downloadedBytesGetMethod_10; }
	inline MethodInfo_t ** get_address_of_downloadedBytesGetMethod_10() { return &___downloadedBytesGetMethod_10; }
	inline void set_downloadedBytesGetMethod_10(MethodInfo_t * value)
	{
		___downloadedBytesGetMethod_10 = value;
		Il2CppCodeGenWriteBarrier(&___downloadedBytesGetMethod_10, value);
	}

	inline static int32_t get_offset_of_responseCodeGetMethod_11() { return static_cast<int32_t>(offsetof(UnityWebRequestWrapper_t1542496577_StaticFields, ___responseCodeGetMethod_11)); }
	inline MethodInfo_t * get_responseCodeGetMethod_11() const { return ___responseCodeGetMethod_11; }
	inline MethodInfo_t ** get_address_of_responseCodeGetMethod_11() { return &___responseCodeGetMethod_11; }
	inline void set_responseCodeGetMethod_11(MethodInfo_t * value)
	{
		___responseCodeGetMethod_11 = value;
		Il2CppCodeGenWriteBarrier(&___responseCodeGetMethod_11, value);
	}

	inline static int32_t get_offset_of_downloadHandlerSetMethod_12() { return static_cast<int32_t>(offsetof(UnityWebRequestWrapper_t1542496577_StaticFields, ___downloadHandlerSetMethod_12)); }
	inline MethodInfo_t * get_downloadHandlerSetMethod_12() const { return ___downloadHandlerSetMethod_12; }
	inline MethodInfo_t ** get_address_of_downloadHandlerSetMethod_12() { return &___downloadHandlerSetMethod_12; }
	inline void set_downloadHandlerSetMethod_12(MethodInfo_t * value)
	{
		___downloadHandlerSetMethod_12 = value;
		Il2CppCodeGenWriteBarrier(&___downloadHandlerSetMethod_12, value);
	}

	inline static int32_t get_offset_of_uploadHandlerSetMethod_13() { return static_cast<int32_t>(offsetof(UnityWebRequestWrapper_t1542496577_StaticFields, ___uploadHandlerSetMethod_13)); }
	inline MethodInfo_t * get_uploadHandlerSetMethod_13() const { return ___uploadHandlerSetMethod_13; }
	inline MethodInfo_t ** get_address_of_uploadHandlerSetMethod_13() { return &___uploadHandlerSetMethod_13; }
	inline void set_uploadHandlerSetMethod_13(MethodInfo_t * value)
	{
		___uploadHandlerSetMethod_13 = value;
		Il2CppCodeGenWriteBarrier(&___uploadHandlerSetMethod_13, value);
	}

	inline static int32_t get_offset_of_errorGetMethod_14() { return static_cast<int32_t>(offsetof(UnityWebRequestWrapper_t1542496577_StaticFields, ___errorGetMethod_14)); }
	inline MethodInfo_t * get_errorGetMethod_14() const { return ___errorGetMethod_14; }
	inline MethodInfo_t ** get_address_of_errorGetMethod_14() { return &___errorGetMethod_14; }
	inline void set_errorGetMethod_14(MethodInfo_t * value)
	{
		___errorGetMethod_14 = value;
		Il2CppCodeGenWriteBarrier(&___errorGetMethod_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
