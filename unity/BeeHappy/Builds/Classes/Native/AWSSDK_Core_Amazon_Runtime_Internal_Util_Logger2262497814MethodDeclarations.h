﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.Util.Logger
struct Logger_t2262497814;
// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;
// System.ComponentModel.PropertyChangedEventArgs
struct PropertyChangedEventArgs_t1689446432;
// System.Exception
struct Exception_t1927440687;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t3614634134;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_Object2689449295.h"
#include "System_System_ComponentModel_PropertyChangedEventA1689446432.h"
#include "mscorlib_System_Exception1927440687.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Amazon.Runtime.Internal.Util.Logger::.ctor()
extern "C"  void Logger__ctor_m147150772 (Logger_t2262497814 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.Logger::.ctor(System.Type)
extern "C"  void Logger__ctor_m1766346557 (Logger_t2262497814 * __this, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.Logger::ConfigsChanged(System.Object,System.ComponentModel.PropertyChangedEventArgs)
extern "C"  void Logger_ConfigsChanged_m991377470 (Logger_t2262497814 * __this, Il2CppObject * ___sender0, PropertyChangedEventArgs_t1689446432 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.Logger::ConfigureLoggers()
extern "C"  void Logger_ConfigureLoggers_m4143587345 (Logger_t2262497814 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.Internal.Util.Logger Amazon.Runtime.Internal.Util.Logger::GetLogger(System.Type)
extern "C"  Logger_t2262497814 * Logger_GetLogger_m1987131669 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.Internal.Util.Logger Amazon.Runtime.Internal.Util.Logger::get_EmptyLogger()
extern "C"  Logger_t2262497814 * Logger_get_EmptyLogger_m1058404432 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.Logger::Error(System.Exception,System.String,System.Object[])
extern "C"  void Logger_Error_m2198844034 (Logger_t2262497814 * __this, Exception_t1927440687 * ___exception0, String_t* ___messageFormat1, ObjectU5BU5D_t3614634134* ___args2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.Logger::Debug(System.Exception,System.String,System.Object[])
extern "C"  void Logger_Debug_m1469802385 (Logger_t2262497814 * __this, Exception_t1927440687 * ___exception0, String_t* ___messageFormat1, ObjectU5BU5D_t3614634134* ___args2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.Logger::DebugFormat(System.String,System.Object[])
extern "C"  void Logger_DebugFormat_m3209558936 (Logger_t2262497814 * __this, String_t* ___messageFormat0, ObjectU5BU5D_t3614634134* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.Logger::InfoFormat(System.String,System.Object[])
extern "C"  void Logger_InfoFormat_m1449629313 (Logger_t2262497814 * __this, String_t* ___messageFormat0, ObjectU5BU5D_t3614634134* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.Logger::.cctor()
extern "C"  void Logger__cctor_m2207663961 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
