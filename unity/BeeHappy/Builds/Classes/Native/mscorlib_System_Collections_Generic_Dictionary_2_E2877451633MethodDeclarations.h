﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3601534125MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,Amazon.CognitoSync.Internal.CognitoCredentialsRetriever/CSRequestCache/CSRequest>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m2876196292(__this, ___dictionary0, method) ((  void (*) (Enumerator_t2877451633 *, Dictionary_2_t1557426931 *, const MethodInfo*))Enumerator__ctor_m3742107451_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,Amazon.CognitoSync.Internal.CognitoCredentialsRetriever/CSRequestCache/CSRequest>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2059594681(__this, method) ((  Il2CppObject * (*) (Enumerator_t2877451633 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m229223308_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,Amazon.CognitoSync.Internal.CognitoCredentialsRetriever/CSRequestCache/CSRequest>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2760656997(__this, method) ((  void (*) (Enumerator_t2877451633 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3225937576_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Type,Amazon.CognitoSync.Internal.CognitoCredentialsRetriever/CSRequestCache/CSRequest>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3704914480(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t2877451633 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m221119093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,Amazon.CognitoSync.Internal.CognitoCredentialsRetriever/CSRequestCache/CSRequest>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3372767355(__this, method) ((  Il2CppObject * (*) (Enumerator_t2877451633 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m467957770_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,Amazon.CognitoSync.Internal.CognitoCredentialsRetriever/CSRequestCache/CSRequest>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1757930155(__this, method) ((  Il2CppObject * (*) (Enumerator_t2877451633 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2325383168_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Type,Amazon.CognitoSync.Internal.CognitoCredentialsRetriever/CSRequestCache/CSRequest>::MoveNext()
#define Enumerator_MoveNext_m140759181(__this, method) ((  bool (*) (Enumerator_t2877451633 *, const MethodInfo*))Enumerator_MoveNext_m3349738440_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Type,Amazon.CognitoSync.Internal.CognitoCredentialsRetriever/CSRequestCache/CSRequest>::get_Current()
#define Enumerator_get_Current_m3019585893(__this, method) ((  KeyValuePair_2_t3609739449  (*) (Enumerator_t2877451633 *, const MethodInfo*))Enumerator_get_Current_m25299632_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Type,Amazon.CognitoSync.Internal.CognitoCredentialsRetriever/CSRequestCache/CSRequest>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m2515540156(__this, method) ((  Type_t * (*) (Enumerator_t2877451633 *, const MethodInfo*))Enumerator_get_CurrentKey_m3839846791_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Type,Amazon.CognitoSync.Internal.CognitoCredentialsRetriever/CSRequestCache/CSRequest>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m3069026380(__this, method) ((  CSRequest_t3915036330 * (*) (Enumerator_t2877451633 *, const MethodInfo*))Enumerator_get_CurrentValue_m402763047_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,Amazon.CognitoSync.Internal.CognitoCredentialsRetriever/CSRequestCache/CSRequest>::Reset()
#define Enumerator_Reset_m2089876098(__this, method) ((  void (*) (Enumerator_t2877451633 *, const MethodInfo*))Enumerator_Reset_m3129803197_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,Amazon.CognitoSync.Internal.CognitoCredentialsRetriever/CSRequestCache/CSRequest>::VerifyState()
#define Enumerator_VerifyState_m1405847759(__this, method) ((  void (*) (Enumerator_t2877451633 *, const MethodInfo*))Enumerator_VerifyState_m262343092_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,Amazon.CognitoSync.Internal.CognitoCredentialsRetriever/CSRequestCache/CSRequest>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m3140838445(__this, method) ((  void (*) (Enumerator_t2877451633 *, const MethodInfo*))Enumerator_VerifyCurrent_m1702320752_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,Amazon.CognitoSync.Internal.CognitoCredentialsRetriever/CSRequestCache/CSRequest>::Dispose()
#define Enumerator_Dispose_m1874309780(__this, method) ((  void (*) (Enumerator_t2877451633 *, const MethodInfo*))Enumerator_Dispose_m1905011127_gshared)(__this, method)
