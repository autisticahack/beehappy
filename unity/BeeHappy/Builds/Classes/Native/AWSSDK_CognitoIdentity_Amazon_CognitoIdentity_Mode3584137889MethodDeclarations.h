﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetCredentialsForIdentityRequestMarshaller
struct GetCredentialsForIdentityRequestMarshaller_t3584137889;
// Amazon.Runtime.Internal.IRequest
struct IRequest_t2400804350;
// Amazon.Runtime.AmazonWebServiceRequest
struct AmazonWebServiceRequest_t3384026212;
// Amazon.CognitoIdentity.Model.GetCredentialsForIdentityRequest
struct GetCredentialsForIdentityRequest_t932830458;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_Runtime_AmazonWebServiceRequest3384026212.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Model932830458.h"

// Amazon.Runtime.Internal.IRequest Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetCredentialsForIdentityRequestMarshaller::Marshall(Amazon.Runtime.AmazonWebServiceRequest)
extern "C"  Il2CppObject * GetCredentialsForIdentityRequestMarshaller_Marshall_m225982315 (GetCredentialsForIdentityRequestMarshaller_t3584137889 * __this, AmazonWebServiceRequest_t3384026212 * ___input0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.Internal.IRequest Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetCredentialsForIdentityRequestMarshaller::Marshall(Amazon.CognitoIdentity.Model.GetCredentialsForIdentityRequest)
extern "C"  Il2CppObject * GetCredentialsForIdentityRequestMarshaller_Marshall_m722107579 (GetCredentialsForIdentityRequestMarshaller_t3584137889 * __this, GetCredentialsForIdentityRequest_t932830458 * ___publicRequest0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetCredentialsForIdentityRequestMarshaller::.ctor()
extern "C"  void GetCredentialsForIdentityRequestMarshaller__ctor_m2870727695 (GetCredentialsForIdentityRequestMarshaller_t3584137889 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
