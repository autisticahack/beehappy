﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.Record>
struct List_1_t237920701;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.CognitoSync.SyncManager.Dataset/<>c__DisplayClass40_0
struct  U3CU3Ec__DisplayClass40_0_t525675437  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.Record> Amazon.CognitoSync.SyncManager.Dataset/<>c__DisplayClass40_0::conflictRecords
	List_1_t237920701 * ___conflictRecords_0;

public:
	inline static int32_t get_offset_of_conflictRecords_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass40_0_t525675437, ___conflictRecords_0)); }
	inline List_1_t237920701 * get_conflictRecords_0() const { return ___conflictRecords_0; }
	inline List_1_t237920701 ** get_address_of_conflictRecords_0() { return &___conflictRecords_0; }
	inline void set_conflictRecords_0(List_1_t237920701 * value)
	{
		___conflictRecords_0 = value;
		Il2CppCodeGenWriteBarrier(&___conflictRecords_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
