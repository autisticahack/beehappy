﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ThirdParty.Json.LitJson.WrapperFactory
struct WrapperFactory_t327905379;
// System.Object
struct Il2CppObject;
// ThirdParty.Json.LitJson.IJsonWrapper
struct IJsonWrapper_t3095378610;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void ThirdParty.Json.LitJson.WrapperFactory::.ctor(System.Object,System.IntPtr)
extern "C"  void WrapperFactory__ctor_m632703600 (WrapperFactory_t327905379 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ThirdParty.Json.LitJson.IJsonWrapper ThirdParty.Json.LitJson.WrapperFactory::Invoke()
extern "C"  Il2CppObject * WrapperFactory_Invoke_m3781398413 (WrapperFactory_t327905379 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult ThirdParty.Json.LitJson.WrapperFactory::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * WrapperFactory_BeginInvoke_m1687643501 (WrapperFactory_t327905379 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ThirdParty.Json.LitJson.IJsonWrapper ThirdParty.Json.LitJson.WrapperFactory::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * WrapperFactory_EndInvoke_m1873551427 (WrapperFactory_t327905379 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
