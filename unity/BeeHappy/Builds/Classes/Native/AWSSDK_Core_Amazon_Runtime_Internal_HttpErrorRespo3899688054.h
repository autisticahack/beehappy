﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AWSSDK_Core_Amazon_Runtime_Internal_ExceptionHandl2197621571.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.HttpErrorResponseExceptionHandler
struct  HttpErrorResponseExceptionHandler_t3899688054  : public ExceptionHandler_1_t2197621571
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
