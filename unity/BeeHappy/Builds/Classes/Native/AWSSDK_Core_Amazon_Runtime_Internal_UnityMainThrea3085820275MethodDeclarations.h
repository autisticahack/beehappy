﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.UnityMainThreadDispatcher/<InvokeRequest>d__7
struct U3CInvokeRequestU3Ed__7_t3085820275;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Amazon.Runtime.Internal.UnityMainThreadDispatcher/<InvokeRequest>d__7::.ctor(System.Int32)
extern "C"  void U3CInvokeRequestU3Ed__7__ctor_m37779867 (U3CInvokeRequestU3Ed__7_t3085820275 * __this, int32_t ___U3CU3E1__state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.UnityMainThreadDispatcher/<InvokeRequest>d__7::System.IDisposable.Dispose()
extern "C"  void U3CInvokeRequestU3Ed__7_System_IDisposable_Dispose_m947118869 (U3CInvokeRequestU3Ed__7_t3085820275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.UnityMainThreadDispatcher/<InvokeRequest>d__7::MoveNext()
extern "C"  bool U3CInvokeRequestU3Ed__7_MoveNext_m186794370 (U3CInvokeRequestU3Ed__7_t3085820275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Amazon.Runtime.Internal.UnityMainThreadDispatcher/<InvokeRequest>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern "C"  Il2CppObject * U3CInvokeRequestU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m979412527 (U3CInvokeRequestU3Ed__7_t3085820275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.UnityMainThreadDispatcher/<InvokeRequest>d__7::System.Collections.IEnumerator.Reset()
extern "C"  void U3CInvokeRequestU3Ed__7_System_Collections_IEnumerator_Reset_m2746332476 (U3CInvokeRequestU3Ed__7_t3085820275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Amazon.Runtime.Internal.UnityMainThreadDispatcher/<InvokeRequest>d__7::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CInvokeRequestU3Ed__7_System_Collections_IEnumerator_get_Current_m611042300 (U3CInvokeRequestU3Ed__7_t3085820275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
