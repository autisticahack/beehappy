﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.EventHandler`1<Amazon.Util.Internal.PlatformServices.NetworkStatusEventArgs>
struct EventHandler_1_t198474825;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Util.Internal.PlatformServices.NetworkReachability
struct  NetworkReachability_t3059923765  : public Il2CppObject
{
public:
	// System.EventHandler`1<Amazon.Util.Internal.PlatformServices.NetworkStatusEventArgs> Amazon.Util.Internal.PlatformServices.NetworkReachability::mNetworkReachabilityChanged
	EventHandler_1_t198474825 * ___mNetworkReachabilityChanged_0;

public:
	inline static int32_t get_offset_of_mNetworkReachabilityChanged_0() { return static_cast<int32_t>(offsetof(NetworkReachability_t3059923765, ___mNetworkReachabilityChanged_0)); }
	inline EventHandler_1_t198474825 * get_mNetworkReachabilityChanged_0() const { return ___mNetworkReachabilityChanged_0; }
	inline EventHandler_1_t198474825 ** get_address_of_mNetworkReachabilityChanged_0() { return &___mNetworkReachabilityChanged_0; }
	inline void set_mNetworkReachabilityChanged_0(EventHandler_1_t198474825 * value)
	{
		___mNetworkReachabilityChanged_0 = value;
		Il2CppCodeGenWriteBarrier(&___mNetworkReachabilityChanged_0, value);
	}
};

struct NetworkReachability_t3059923765_StaticFields
{
public:
	// System.Object Amazon.Util.Internal.PlatformServices.NetworkReachability::reachabilityChangedLock
	Il2CppObject * ___reachabilityChangedLock_1;

public:
	inline static int32_t get_offset_of_reachabilityChangedLock_1() { return static_cast<int32_t>(offsetof(NetworkReachability_t3059923765_StaticFields, ___reachabilityChangedLock_1)); }
	inline Il2CppObject * get_reachabilityChangedLock_1() const { return ___reachabilityChangedLock_1; }
	inline Il2CppObject ** get_address_of_reachabilityChangedLock_1() { return &___reachabilityChangedLock_1; }
	inline void set_reachabilityChangedLock_1(Il2CppObject * value)
	{
		___reachabilityChangedLock_1 = value;
		Il2CppCodeGenWriteBarrier(&___reachabilityChangedLock_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
