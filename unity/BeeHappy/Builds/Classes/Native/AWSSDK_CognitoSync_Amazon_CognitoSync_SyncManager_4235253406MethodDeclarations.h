﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1642385972;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.String Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage/RecordColumns::BuildQuery(System.String)
extern "C"  String_t* RecordColumns_BuildQuery_m1616805679 (Il2CppObject * __this /* static, unused */, String_t* ___conditions0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage/RecordColumns::BuildInsert()
extern "C"  String_t* RecordColumns_BuildInsert_m977178382 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage/RecordColumns::BuildInsert(System.String[])
extern "C"  String_t* RecordColumns_BuildInsert_m1872372432 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t1642385972* ___fieldList0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage/RecordColumns::BuildUpdate(System.String[],System.String)
extern "C"  String_t* RecordColumns_BuildUpdate_m1090902632 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t1642385972* ___fieldList0, String_t* ___conditions1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage/RecordColumns::BuildDelete(System.String)
extern "C"  String_t* RecordColumns_BuildDelete_m1270389362 (Il2CppObject * __this /* static, unused */, String_t* ___conditions0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage/RecordColumns::.cctor()
extern "C"  void RecordColumns__cctor_m4261851391 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
