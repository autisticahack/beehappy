﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>
struct DefaultComparer_t3254737539;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_Util_Internal_PlatformServices_1632807378.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::.ctor()
extern "C"  void DefaultComparer__ctor_m2795851432_gshared (DefaultComparer_t3254737539 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m2795851432(__this, method) ((  void (*) (DefaultComparer_t3254737539 *, const MethodInfo*))DefaultComparer__ctor_m2795851432_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m3842412613_gshared (DefaultComparer_t3254737539 * __this, int32_t ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m3842412613(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t3254737539 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m3842412613_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m1322810173_gshared (DefaultComparer_t3254737539 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m1322810173(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t3254737539 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m1322810173_gshared)(__this, ___x0, ___y1, method)
