﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ThirdParty.iOS4Unity.UILocalNotification
struct UILocalNotification_t869364318;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr2504060609.h"

// System.Void ThirdParty.iOS4Unity.UILocalNotification::.cctor()
extern "C"  void UILocalNotification__cctor_m3710969671 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.iOS4Unity.UILocalNotification::.ctor(System.IntPtr)
extern "C"  void UILocalNotification__ctor_m2010710626 (UILocalNotification_t869364318 * __this, IntPtr_t ___handle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
