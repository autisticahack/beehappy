﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.CognitoSync.Model.Internal.MarshallTransformations.DatasetUnmarshaller
struct DatasetUnmarshaller_t3862241748;
// Amazon.CognitoSync.Model.Dataset
struct Dataset_t149289424;
// Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext
struct XmlUnmarshallerContext_t1179575220;
// Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext
struct JsonUnmarshallerContext_t456235889;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_XmlU1179575220.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_JsonU456235889.h"

// Amazon.CognitoSync.Model.Dataset Amazon.CognitoSync.Model.Internal.MarshallTransformations.DatasetUnmarshaller::Amazon.Runtime.Internal.Transform.IUnmarshaller<Amazon.CognitoSync.Model.Dataset,Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext>.Unmarshall(Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext)
extern "C"  Dataset_t149289424 * DatasetUnmarshaller_Amazon_Runtime_Internal_Transform_IUnmarshallerU3CAmazon_CognitoSync_Model_DatasetU2CAmazon_Runtime_Internal_Transform_XmlUnmarshallerContextU3E_Unmarshall_m1220202280 (DatasetUnmarshaller_t3862241748 * __this, XmlUnmarshallerContext_t1179575220 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.CognitoSync.Model.Dataset Amazon.CognitoSync.Model.Internal.MarshallTransformations.DatasetUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern "C"  Dataset_t149289424 * DatasetUnmarshaller_Unmarshall_m1560376747 (DatasetUnmarshaller_t3862241748 * __this, JsonUnmarshallerContext_t456235889 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.CognitoSync.Model.Internal.MarshallTransformations.DatasetUnmarshaller Amazon.CognitoSync.Model.Internal.MarshallTransformations.DatasetUnmarshaller::get_Instance()
extern "C"  DatasetUnmarshaller_t3862241748 * DatasetUnmarshaller_get_Instance_m1600747291 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Model.Internal.MarshallTransformations.DatasetUnmarshaller::.ctor()
extern "C"  void DatasetUnmarshaller__ctor_m3267075674 (DatasetUnmarshaller_t3862241748 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Model.Internal.MarshallTransformations.DatasetUnmarshaller::.cctor()
extern "C"  void DatasetUnmarshaller__cctor_m1302246731 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
