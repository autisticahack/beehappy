﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Nullable_1_gen3251239280.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.CognitoSync.SyncManager.Record
struct  Record_t868799569  : public Il2CppObject
{
public:
	// System.String Amazon.CognitoSync.SyncManager.Record::_key
	String_t* ____key_0;
	// System.String Amazon.CognitoSync.SyncManager.Record::_value
	String_t* ____value_1;
	// System.Int64 Amazon.CognitoSync.SyncManager.Record::_syncCount
	int64_t ____syncCount_2;
	// System.Nullable`1<System.DateTime> Amazon.CognitoSync.SyncManager.Record::_lastModifiedDate
	Nullable_1_t3251239280  ____lastModifiedDate_3;
	// System.String Amazon.CognitoSync.SyncManager.Record::_lastModifiedBy
	String_t* ____lastModifiedBy_4;
	// System.Nullable`1<System.DateTime> Amazon.CognitoSync.SyncManager.Record::_deviceLastModifiedDate
	Nullable_1_t3251239280  ____deviceLastModifiedDate_5;
	// System.Boolean Amazon.CognitoSync.SyncManager.Record::_modified
	bool ____modified_6;

public:
	inline static int32_t get_offset_of__key_0() { return static_cast<int32_t>(offsetof(Record_t868799569, ____key_0)); }
	inline String_t* get__key_0() const { return ____key_0; }
	inline String_t** get_address_of__key_0() { return &____key_0; }
	inline void set__key_0(String_t* value)
	{
		____key_0 = value;
		Il2CppCodeGenWriteBarrier(&____key_0, value);
	}

	inline static int32_t get_offset_of__value_1() { return static_cast<int32_t>(offsetof(Record_t868799569, ____value_1)); }
	inline String_t* get__value_1() const { return ____value_1; }
	inline String_t** get_address_of__value_1() { return &____value_1; }
	inline void set__value_1(String_t* value)
	{
		____value_1 = value;
		Il2CppCodeGenWriteBarrier(&____value_1, value);
	}

	inline static int32_t get_offset_of__syncCount_2() { return static_cast<int32_t>(offsetof(Record_t868799569, ____syncCount_2)); }
	inline int64_t get__syncCount_2() const { return ____syncCount_2; }
	inline int64_t* get_address_of__syncCount_2() { return &____syncCount_2; }
	inline void set__syncCount_2(int64_t value)
	{
		____syncCount_2 = value;
	}

	inline static int32_t get_offset_of__lastModifiedDate_3() { return static_cast<int32_t>(offsetof(Record_t868799569, ____lastModifiedDate_3)); }
	inline Nullable_1_t3251239280  get__lastModifiedDate_3() const { return ____lastModifiedDate_3; }
	inline Nullable_1_t3251239280 * get_address_of__lastModifiedDate_3() { return &____lastModifiedDate_3; }
	inline void set__lastModifiedDate_3(Nullable_1_t3251239280  value)
	{
		____lastModifiedDate_3 = value;
	}

	inline static int32_t get_offset_of__lastModifiedBy_4() { return static_cast<int32_t>(offsetof(Record_t868799569, ____lastModifiedBy_4)); }
	inline String_t* get__lastModifiedBy_4() const { return ____lastModifiedBy_4; }
	inline String_t** get_address_of__lastModifiedBy_4() { return &____lastModifiedBy_4; }
	inline void set__lastModifiedBy_4(String_t* value)
	{
		____lastModifiedBy_4 = value;
		Il2CppCodeGenWriteBarrier(&____lastModifiedBy_4, value);
	}

	inline static int32_t get_offset_of__deviceLastModifiedDate_5() { return static_cast<int32_t>(offsetof(Record_t868799569, ____deviceLastModifiedDate_5)); }
	inline Nullable_1_t3251239280  get__deviceLastModifiedDate_5() const { return ____deviceLastModifiedDate_5; }
	inline Nullable_1_t3251239280 * get_address_of__deviceLastModifiedDate_5() { return &____deviceLastModifiedDate_5; }
	inline void set__deviceLastModifiedDate_5(Nullable_1_t3251239280  value)
	{
		____deviceLastModifiedDate_5 = value;
	}

	inline static int32_t get_offset_of__modified_6() { return static_cast<int32_t>(offsetof(Record_t868799569, ____modified_6)); }
	inline bool get__modified_6() const { return ____modified_6; }
	inline bool* get_address_of__modified_6() { return &____modified_6; }
	inline void set__modified_6(bool value)
	{
		____modified_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
