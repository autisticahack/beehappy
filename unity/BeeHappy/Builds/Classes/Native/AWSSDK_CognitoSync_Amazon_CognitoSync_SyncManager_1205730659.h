﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Nullable_1_gen3251239280.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.CognitoSync.SyncManager.DatasetMetadata
struct  DatasetMetadata_t1205730659  : public Il2CppObject
{
public:
	// System.String Amazon.CognitoSync.SyncManager.DatasetMetadata::_datasetName
	String_t* ____datasetName_0;
	// System.Nullable`1<System.DateTime> Amazon.CognitoSync.SyncManager.DatasetMetadata::_creationDate
	Nullable_1_t3251239280  ____creationDate_1;
	// System.Nullable`1<System.DateTime> Amazon.CognitoSync.SyncManager.DatasetMetadata::_lastModifiedDate
	Nullable_1_t3251239280  ____lastModifiedDate_2;
	// System.String Amazon.CognitoSync.SyncManager.DatasetMetadata::_lastModifiedBy
	String_t* ____lastModifiedBy_3;
	// System.Int64 Amazon.CognitoSync.SyncManager.DatasetMetadata::_storageSizeBytes
	int64_t ____storageSizeBytes_4;
	// System.Int64 Amazon.CognitoSync.SyncManager.DatasetMetadata::_recordCount
	int64_t ____recordCount_5;

public:
	inline static int32_t get_offset_of__datasetName_0() { return static_cast<int32_t>(offsetof(DatasetMetadata_t1205730659, ____datasetName_0)); }
	inline String_t* get__datasetName_0() const { return ____datasetName_0; }
	inline String_t** get_address_of__datasetName_0() { return &____datasetName_0; }
	inline void set__datasetName_0(String_t* value)
	{
		____datasetName_0 = value;
		Il2CppCodeGenWriteBarrier(&____datasetName_0, value);
	}

	inline static int32_t get_offset_of__creationDate_1() { return static_cast<int32_t>(offsetof(DatasetMetadata_t1205730659, ____creationDate_1)); }
	inline Nullable_1_t3251239280  get__creationDate_1() const { return ____creationDate_1; }
	inline Nullable_1_t3251239280 * get_address_of__creationDate_1() { return &____creationDate_1; }
	inline void set__creationDate_1(Nullable_1_t3251239280  value)
	{
		____creationDate_1 = value;
	}

	inline static int32_t get_offset_of__lastModifiedDate_2() { return static_cast<int32_t>(offsetof(DatasetMetadata_t1205730659, ____lastModifiedDate_2)); }
	inline Nullable_1_t3251239280  get__lastModifiedDate_2() const { return ____lastModifiedDate_2; }
	inline Nullable_1_t3251239280 * get_address_of__lastModifiedDate_2() { return &____lastModifiedDate_2; }
	inline void set__lastModifiedDate_2(Nullable_1_t3251239280  value)
	{
		____lastModifiedDate_2 = value;
	}

	inline static int32_t get_offset_of__lastModifiedBy_3() { return static_cast<int32_t>(offsetof(DatasetMetadata_t1205730659, ____lastModifiedBy_3)); }
	inline String_t* get__lastModifiedBy_3() const { return ____lastModifiedBy_3; }
	inline String_t** get_address_of__lastModifiedBy_3() { return &____lastModifiedBy_3; }
	inline void set__lastModifiedBy_3(String_t* value)
	{
		____lastModifiedBy_3 = value;
		Il2CppCodeGenWriteBarrier(&____lastModifiedBy_3, value);
	}

	inline static int32_t get_offset_of__storageSizeBytes_4() { return static_cast<int32_t>(offsetof(DatasetMetadata_t1205730659, ____storageSizeBytes_4)); }
	inline int64_t get__storageSizeBytes_4() const { return ____storageSizeBytes_4; }
	inline int64_t* get_address_of__storageSizeBytes_4() { return &____storageSizeBytes_4; }
	inline void set__storageSizeBytes_4(int64_t value)
	{
		____storageSizeBytes_4 = value;
	}

	inline static int32_t get_offset_of__recordCount_5() { return static_cast<int32_t>(offsetof(DatasetMetadata_t1205730659, ____recordCount_5)); }
	inline int64_t get__recordCount_5() const { return ____recordCount_5; }
	inline int64_t* get_address_of__recordCount_5() { return &____recordCount_5; }
	inline void set__recordCount_5(int64_t value)
	{
		____recordCount_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
