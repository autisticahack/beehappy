﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ThirdParty.Json.LitJson.OrderedDictionaryEnumerator
struct OrderedDictionaryEnumerator_t1664192327;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,ThirdParty.Json.LitJson.JsonData>>
struct IEnumerator_1_t1410900363;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Object ThirdParty.Json.LitJson.OrderedDictionaryEnumerator::get_Current()
extern "C"  Il2CppObject * OrderedDictionaryEnumerator_get_Current_m3299941819 (OrderedDictionaryEnumerator_t1664192327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.DictionaryEntry ThirdParty.Json.LitJson.OrderedDictionaryEnumerator::get_Entry()
extern "C"  DictionaryEntry_t3048875398  OrderedDictionaryEnumerator_get_Entry_m928514144 (OrderedDictionaryEnumerator_t1664192327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ThirdParty.Json.LitJson.OrderedDictionaryEnumerator::get_Key()
extern "C"  Il2CppObject * OrderedDictionaryEnumerator_get_Key_m3835242549 (OrderedDictionaryEnumerator_t1664192327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ThirdParty.Json.LitJson.OrderedDictionaryEnumerator::get_Value()
extern "C"  Il2CppObject * OrderedDictionaryEnumerator_get_Value_m3178181661 (OrderedDictionaryEnumerator_t1664192327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.OrderedDictionaryEnumerator::.ctor(System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,ThirdParty.Json.LitJson.JsonData>>)
extern "C"  void OrderedDictionaryEnumerator__ctor_m631766896 (OrderedDictionaryEnumerator_t1664192327 * __this, Il2CppObject* ___enumerator0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ThirdParty.Json.LitJson.OrderedDictionaryEnumerator::MoveNext()
extern "C"  bool OrderedDictionaryEnumerator_MoveNext_m1405683412 (OrderedDictionaryEnumerator_t1664192327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.OrderedDictionaryEnumerator::Reset()
extern "C"  void OrderedDictionaryEnumerator_Reset_m263692563 (OrderedDictionaryEnumerator_t1664192327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
