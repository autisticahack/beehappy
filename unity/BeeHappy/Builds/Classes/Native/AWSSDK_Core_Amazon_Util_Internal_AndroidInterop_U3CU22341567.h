﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.Util.Internal.AndroidInterop/<>c
struct U3CU3Ec_t22341567;
// System.Func`2<System.Reflection.MethodInfo,System.Boolean>
struct Func_2_t499475658;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Util.Internal.AndroidInterop/<>c
struct  U3CU3Ec_t22341567  : public Il2CppObject
{
public:

public:
};

struct U3CU3Ec_t22341567_StaticFields
{
public:
	// Amazon.Util.Internal.AndroidInterop/<>c Amazon.Util.Internal.AndroidInterop/<>c::<>9
	U3CU3Ec_t22341567 * ___U3CU3E9_0;
	// System.Func`2<System.Reflection.MethodInfo,System.Boolean> Amazon.Util.Internal.AndroidInterop/<>c::<>9__1_0
	Func_2_t499475658 * ___U3CU3E9__1_0_1;
	// System.Func`2<System.Reflection.MethodInfo,System.Boolean> Amazon.Util.Internal.AndroidInterop/<>c::<>9__1_1
	Func_2_t499475658 * ___U3CU3E9__1_1_2;
	// System.Func`2<System.Reflection.MethodInfo,System.Boolean> Amazon.Util.Internal.AndroidInterop/<>c::<>9__3_0
	Func_2_t499475658 * ___U3CU3E9__3_0_3;
	// System.Func`2<System.Reflection.MethodInfo,System.Boolean> Amazon.Util.Internal.AndroidInterop/<>c::<>9__3_1
	Func_2_t499475658 * ___U3CU3E9__3_1_4;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t22341567_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t22341567 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t22341567 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t22341567 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9_0, value);
	}

	inline static int32_t get_offset_of_U3CU3E9__1_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t22341567_StaticFields, ___U3CU3E9__1_0_1)); }
	inline Func_2_t499475658 * get_U3CU3E9__1_0_1() const { return ___U3CU3E9__1_0_1; }
	inline Func_2_t499475658 ** get_address_of_U3CU3E9__1_0_1() { return &___U3CU3E9__1_0_1; }
	inline void set_U3CU3E9__1_0_1(Func_2_t499475658 * value)
	{
		___U3CU3E9__1_0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9__1_0_1, value);
	}

	inline static int32_t get_offset_of_U3CU3E9__1_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t22341567_StaticFields, ___U3CU3E9__1_1_2)); }
	inline Func_2_t499475658 * get_U3CU3E9__1_1_2() const { return ___U3CU3E9__1_1_2; }
	inline Func_2_t499475658 ** get_address_of_U3CU3E9__1_1_2() { return &___U3CU3E9__1_1_2; }
	inline void set_U3CU3E9__1_1_2(Func_2_t499475658 * value)
	{
		___U3CU3E9__1_1_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9__1_1_2, value);
	}

	inline static int32_t get_offset_of_U3CU3E9__3_0_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t22341567_StaticFields, ___U3CU3E9__3_0_3)); }
	inline Func_2_t499475658 * get_U3CU3E9__3_0_3() const { return ___U3CU3E9__3_0_3; }
	inline Func_2_t499475658 ** get_address_of_U3CU3E9__3_0_3() { return &___U3CU3E9__3_0_3; }
	inline void set_U3CU3E9__3_0_3(Func_2_t499475658 * value)
	{
		___U3CU3E9__3_0_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9__3_0_3, value);
	}

	inline static int32_t get_offset_of_U3CU3E9__3_1_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_t22341567_StaticFields, ___U3CU3E9__3_1_4)); }
	inline Func_2_t499475658 * get_U3CU3E9__3_1_4() const { return ___U3CU3E9__3_1_4; }
	inline Func_2_t499475658 ** get_address_of_U3CU3E9__3_1_4() { return &___U3CU3E9__3_1_4; }
	inline void set_U3CU3E9__3_1_4(Func_2_t499475658 * value)
	{
		___U3CU3E9__3_1_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9__3_1_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
