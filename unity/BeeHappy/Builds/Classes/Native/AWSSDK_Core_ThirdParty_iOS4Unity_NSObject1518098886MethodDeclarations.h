﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ThirdParty.iOS4Unity.NSObject
struct NSObject_t1518098886;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr2504060609.h"

// System.Void ThirdParty.iOS4Unity.NSObject::.cctor()
extern "C"  void NSObject__cctor_m2429373631 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.iOS4Unity.NSObject::Finalize()
extern "C"  void NSObject_Finalize_m3206165236 (NSObject_t1518098886 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.iOS4Unity.NSObject::.ctor(System.IntPtr)
extern "C"  void NSObject__ctor_m139361934 (NSObject_t1518098886 * __this, IntPtr_t ___handle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ThirdParty.iOS4Unity.NSObject::get_Description()
extern "C"  String_t* NSObject_get_Description_m2772944032 (NSObject_t1518098886 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ThirdParty.iOS4Unity.NSObject::ToString()
extern "C"  String_t* NSObject_ToString_m908822323 (NSObject_t1518098886 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.iOS4Unity.NSObject::Dispose()
extern "C"  void NSObject_Dispose_m815884943 (NSObject_t1518098886 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
