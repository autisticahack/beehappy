﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.CognitoSync.Internal.CognitoCredentialsRetriever/CSRequestCache/CSRequest
struct CSRequest_t3915036330;
// System.Type
struct Type_t;
// Amazon.CognitoSync.AmazonCognitoSyncRequest
struct AmazonCognitoSyncRequest_t2140463921;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type1303803226.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_AmazonCognit2140463921.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Amazon.CognitoSync.Internal.CognitoCredentialsRetriever/CSRequestCache/CSRequest::.ctor(System.Type)
extern "C"  void CSRequest__ctor_m3819777363 (CSRequest_t3915036330 * __this, Type_t * ___requestType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Internal.CognitoCredentialsRetriever/CSRequestCache/CSRequest::SetFields(Amazon.CognitoSync.AmazonCognitoSyncRequest,System.String,System.String)
extern "C"  void CSRequest_SetFields_m2140614302 (CSRequest_t3915036330 * __this, AmazonCognitoSyncRequest_t2140463921 * ___request0, String_t* ___identityPoolId1, String_t* ___identityId2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Internal.CognitoCredentialsRetriever/CSRequestCache/CSRequest::.cctor()
extern "C"  void CSRequest__cctor_m1071657023 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
