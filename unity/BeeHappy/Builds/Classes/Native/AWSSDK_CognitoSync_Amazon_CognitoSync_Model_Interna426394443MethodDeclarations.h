﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.CognitoSync.Model.Internal.MarshallTransformations.DeleteDatasetRequestMarshaller
struct DeleteDatasetRequestMarshaller_t426394443;
// Amazon.Runtime.Internal.IRequest
struct IRequest_t2400804350;
// Amazon.Runtime.AmazonWebServiceRequest
struct AmazonWebServiceRequest_t3384026212;
// Amazon.CognitoSync.Model.DeleteDatasetRequest
struct DeleteDatasetRequest_t3672322944;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_Runtime_AmazonWebServiceRequest3384026212.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_Model_Delete3672322944.h"

// Amazon.Runtime.Internal.IRequest Amazon.CognitoSync.Model.Internal.MarshallTransformations.DeleteDatasetRequestMarshaller::Marshall(Amazon.Runtime.AmazonWebServiceRequest)
extern "C"  Il2CppObject * DeleteDatasetRequestMarshaller_Marshall_m169510511 (DeleteDatasetRequestMarshaller_t426394443 * __this, AmazonWebServiceRequest_t3384026212 * ___input0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.Internal.IRequest Amazon.CognitoSync.Model.Internal.MarshallTransformations.DeleteDatasetRequestMarshaller::Marshall(Amazon.CognitoSync.Model.DeleteDatasetRequest)
extern "C"  Il2CppObject * DeleteDatasetRequestMarshaller_Marshall_m3203581126 (DeleteDatasetRequestMarshaller_t426394443 * __this, DeleteDatasetRequest_t3672322944 * ___publicRequest0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Model.Internal.MarshallTransformations.DeleteDatasetRequestMarshaller::.ctor()
extern "C"  void DeleteDatasetRequestMarshaller__ctor_m2603075967 (DeleteDatasetRequestMarshaller_t426394443 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
