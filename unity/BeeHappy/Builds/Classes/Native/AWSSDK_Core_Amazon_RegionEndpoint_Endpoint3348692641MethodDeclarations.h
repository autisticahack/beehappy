﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.RegionEndpoint/Endpoint
struct Endpoint_t3348692641;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Amazon.RegionEndpoint/Endpoint::.ctor(System.String,System.String,System.String)
extern "C"  void Endpoint__ctor_m2275888738 (Endpoint_t3348692641 * __this, String_t* ___hostname0, String_t* ___authregion1, String_t* ___signatureVersionOverride2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.RegionEndpoint/Endpoint::get_Hostname()
extern "C"  String_t* Endpoint_get_Hostname_m1537838887 (Endpoint_t3348692641 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.RegionEndpoint/Endpoint::set_Hostname(System.String)
extern "C"  void Endpoint_set_Hostname_m3880491180 (Endpoint_t3348692641 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.RegionEndpoint/Endpoint::get_AuthRegion()
extern "C"  String_t* Endpoint_get_AuthRegion_m402449776 (Endpoint_t3348692641 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.RegionEndpoint/Endpoint::set_AuthRegion(System.String)
extern "C"  void Endpoint_set_AuthRegion_m1885930299 (Endpoint_t3348692641 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.RegionEndpoint/Endpoint::ToString()
extern "C"  String_t* Endpoint_ToString_m2416860999 (Endpoint_t3348692641 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.RegionEndpoint/Endpoint::get_SignatureVersionOverride()
extern "C"  String_t* Endpoint_get_SignatureVersionOverride_m1220385000 (Endpoint_t3348692641 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.RegionEndpoint/Endpoint::set_SignatureVersionOverride(System.String)
extern "C"  void Endpoint_set_SignatureVersionOverride_m1330771629 (Endpoint_t3348692641 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
