﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.SecurityToken.Model.Internal.MarshallTransformations.CredentialsUnmarshaller
struct CredentialsUnmarshaller_t3599710454;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.SecurityToken.Model.Internal.MarshallTransformations.CredentialsUnmarshaller
struct  CredentialsUnmarshaller_t3599710454  : public Il2CppObject
{
public:

public:
};

struct CredentialsUnmarshaller_t3599710454_StaticFields
{
public:
	// Amazon.SecurityToken.Model.Internal.MarshallTransformations.CredentialsUnmarshaller Amazon.SecurityToken.Model.Internal.MarshallTransformations.CredentialsUnmarshaller::_instance
	CredentialsUnmarshaller_t3599710454 * ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(CredentialsUnmarshaller_t3599710454_StaticFields, ____instance_0)); }
	inline CredentialsUnmarshaller_t3599710454 * get__instance_0() const { return ____instance_0; }
	inline CredentialsUnmarshaller_t3599710454 ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(CredentialsUnmarshaller_t3599710454 * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier(&____instance_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
