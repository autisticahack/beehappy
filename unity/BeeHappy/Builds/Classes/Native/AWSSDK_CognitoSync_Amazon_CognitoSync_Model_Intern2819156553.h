﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.CognitoSync.Model.Internal.MarshallTransformations.ListRecordsResponseUnmarshaller
struct ListRecordsResponseUnmarshaller_t2819156553;

#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_Json2685947887.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.CognitoSync.Model.Internal.MarshallTransformations.ListRecordsResponseUnmarshaller
struct  ListRecordsResponseUnmarshaller_t2819156553  : public JsonResponseUnmarshaller_t2685947887
{
public:

public:
};

struct ListRecordsResponseUnmarshaller_t2819156553_StaticFields
{
public:
	// Amazon.CognitoSync.Model.Internal.MarshallTransformations.ListRecordsResponseUnmarshaller Amazon.CognitoSync.Model.Internal.MarshallTransformations.ListRecordsResponseUnmarshaller::_instance
	ListRecordsResponseUnmarshaller_t2819156553 * ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(ListRecordsResponseUnmarshaller_t2819156553_StaticFields, ____instance_0)); }
	inline ListRecordsResponseUnmarshaller_t2819156553 * get__instance_0() const { return ____instance_0; }
	inline ListRecordsResponseUnmarshaller_t2819156553 ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(ListRecordsResponseUnmarshaller_t2819156553 * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier(&____instance_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
