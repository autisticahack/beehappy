﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.CognitoSync.SyncManager.SyncSuccessEventArgs
struct SyncSuccessEventArgs_t4277381347;
// System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.Record>
struct List_1_t237920701;

#include "codegen/il2cpp-codegen.h"

// System.Void Amazon.CognitoSync.SyncManager.SyncSuccessEventArgs::set_UpdatedRecords(System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.Record>)
extern "C"  void SyncSuccessEventArgs_set_UpdatedRecords_m1837622190 (SyncSuccessEventArgs_t4277381347 * __this, List_1_t237920701 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.SyncManager.SyncSuccessEventArgs::.ctor(System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.Record>)
extern "C"  void SyncSuccessEventArgs__ctor_m93252768 (SyncSuccessEventArgs_t4277381347 * __this, List_1_t237920701 * ___updatedRecords0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
