﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.PreRequestEventArgs
struct PreRequestEventArgs_t776850383;
// Amazon.Runtime.AmazonWebServiceRequest
struct AmazonWebServiceRequest_t3384026212;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_Runtime_AmazonWebServiceRequest3384026212.h"

// System.Void Amazon.Runtime.PreRequestEventArgs::.ctor()
extern "C"  void PreRequestEventArgs__ctor_m3480863066 (PreRequestEventArgs_t776850383 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.PreRequestEventArgs::set_Request(Amazon.Runtime.AmazonWebServiceRequest)
extern "C"  void PreRequestEventArgs_set_Request_m3754108162 (PreRequestEventArgs_t776850383 * __this, AmazonWebServiceRequest_t3384026212 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.PreRequestEventArgs Amazon.Runtime.PreRequestEventArgs::Create(Amazon.Runtime.AmazonWebServiceRequest)
extern "C"  PreRequestEventArgs_t776850383 * PreRequestEventArgs_Create_m386061276 (Il2CppObject * __this /* static, unused */, AmazonWebServiceRequest_t3384026212 * ___request0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
