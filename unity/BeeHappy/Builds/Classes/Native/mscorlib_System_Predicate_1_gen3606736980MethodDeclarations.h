﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Predicate_1_gen1132419410MethodDeclarations.h"

// System.Void System.Predicate`1<Amazon.CognitoSync.SyncManager.Record>::.ctor(System.Object,System.IntPtr)
#define Predicate_1__ctor_m2705284566(__this, ___object0, ___method1, method) ((  void (*) (Predicate_1_t3606736980 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m2289454599_gshared)(__this, ___object0, ___method1, method)
// System.Boolean System.Predicate`1<Amazon.CognitoSync.SyncManager.Record>::Invoke(T)
#define Predicate_1_Invoke_m1654017576(__this, ___obj0, method) ((  bool (*) (Predicate_1_t3606736980 *, Record_t868799569 *, const MethodInfo*))Predicate_1_Invoke_m4047721271_gshared)(__this, ___obj0, method)
// System.IAsyncResult System.Predicate`1<Amazon.CognitoSync.SyncManager.Record>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m2325928263(__this, ___obj0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Predicate_1_t3606736980 *, Record_t868799569 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Predicate_1_BeginInvoke_m3556950370_gshared)(__this, ___obj0, ___callback1, ___object2, method)
// System.Boolean System.Predicate`1<Amazon.CognitoSync.SyncManager.Record>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m2875122030(__this, ___result0, method) ((  bool (*) (Predicate_1_t3606736980 *, Il2CppObject *, const MethodInfo*))Predicate_1_EndInvoke_m3656575065_gshared)(__this, ___result0, method)
