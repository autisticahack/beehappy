﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AccelBrain.BinauralBeatController
struct BinauralBeatController_t3677369972;
// AccelBrain.BrainBeat
struct BrainBeat_t95439410;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AccelBrain_BrainBeat95439410.h"

// System.Void AccelBrain.BinauralBeatController::.ctor()
extern "C"  void BinauralBeatController__ctor_m1431970495 (BinauralBeatController_t3677369972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AccelBrain.BinauralBeatController::set__brainBeat(AccelBrain.BrainBeat)
extern "C"  void BinauralBeatController_set__brainBeat_m1492124385 (BinauralBeatController_t3677369972 * __this, BrainBeat_t95439410 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AccelBrain.BrainBeat AccelBrain.BinauralBeatController::get__brainBeat()
extern "C"  BrainBeat_t95439410 * BinauralBeatController_get__brainBeat_m2461452842 (BinauralBeatController_t3677369972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AccelBrain.BinauralBeatController::Start()
extern "C"  void BinauralBeatController_Start_m165627795 (BinauralBeatController_t3677369972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
