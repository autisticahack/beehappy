﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>
struct List_1_t3118708580;
// System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>
struct IEnumerable_1_t4041714493;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>
struct IEnumerator_1_t1225111275;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>
struct ICollection_1_t406695457;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>
struct ReadOnlyCollection_1_t3935373140;
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>[]
struct KeyValuePair_2U5BU5D_t598529833;
// System.Predicate`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>
struct Predicate_1_t2192557563;
// System.Collections.Generic.IComparer`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>
struct IComparer_1_t1704050570;
// System.Comparison`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>
struct Comparison_1_t716359003;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23749587448.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2653438254.h"

// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::.ctor()
extern "C"  void List_1__ctor_m1106233434_gshared (List_1_t3118708580 * __this, const MethodInfo* method);
#define List_1__ctor_m1106233434(__this, method) ((  void (*) (List_1_t3118708580 *, const MethodInfo*))List_1__ctor_m1106233434_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m353512431_gshared (List_1_t3118708580 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m353512431(__this, ___collection0, method) ((  void (*) (List_1_t3118708580 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m353512431_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m3000149037_gshared (List_1_t3118708580 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m3000149037(__this, ___capacity0, method) ((  void (*) (List_1_t3118708580 *, int32_t, const MethodInfo*))List_1__ctor_m3000149037_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::.cctor()
extern "C"  void List_1__cctor_m488705167_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m488705167(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m488705167_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2235906604_gshared (List_1_t3118708580 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2235906604(__this, method) ((  Il2CppObject* (*) (List_1_t3118708580 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2235906604_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m4147778178_gshared (List_1_t3118708580 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m4147778178(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3118708580 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m4147778178_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m3849525781_gshared (List_1_t3118708580 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m3849525781(__this, method) ((  Il2CppObject * (*) (List_1_t3118708580 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m3849525781_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m3060434834_gshared (List_1_t3118708580 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m3060434834(__this, ___item0, method) ((  int32_t (*) (List_1_t3118708580 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m3060434834_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m4114123420_gshared (List_1_t3118708580 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m4114123420(__this, ___item0, method) ((  bool (*) (List_1_t3118708580 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m4114123420_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m1216880480_gshared (List_1_t3118708580 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m1216880480(__this, ___item0, method) ((  int32_t (*) (List_1_t3118708580 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m1216880480_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m2440030925_gshared (List_1_t3118708580 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m2440030925(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3118708580 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m2440030925_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m3057835581_gshared (List_1_t3118708580 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m3057835581(__this, ___item0, method) ((  void (*) (List_1_t3118708580 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m3057835581_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3817827937_gshared (List_1_t3118708580 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3817827937(__this, method) ((  bool (*) (List_1_t3118708580 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3817827937_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m528997890_gshared (List_1_t3118708580 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m528997890(__this, method) ((  bool (*) (List_1_t3118708580 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m528997890_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m1872368178_gshared (List_1_t3118708580 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m1872368178(__this, method) ((  Il2CppObject * (*) (List_1_t3118708580 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1872368178_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m618167137_gshared (List_1_t3118708580 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m618167137(__this, method) ((  bool (*) (List_1_t3118708580 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m618167137_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m217451862_gshared (List_1_t3118708580 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m217451862(__this, method) ((  bool (*) (List_1_t3118708580 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m217451862_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m3475225337_gshared (List_1_t3118708580 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m3475225337(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t3118708580 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m3475225337_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m1846639756_gshared (List_1_t3118708580 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m1846639756(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3118708580 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m1846639756_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::Add(T)
extern "C"  void List_1_Add_m484558281_gshared (List_1_t3118708580 * __this, KeyValuePair_2_t3749587448  ___item0, const MethodInfo* method);
#define List_1_Add_m484558281(__this, ___item0, method) ((  void (*) (List_1_t3118708580 *, KeyValuePair_2_t3749587448 , const MethodInfo*))List_1_Add_m484558281_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m1061877264_gshared (List_1_t3118708580 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m1061877264(__this, ___newCount0, method) ((  void (*) (List_1_t3118708580 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m1061877264_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m689268888_gshared (List_1_t3118708580 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m689268888(__this, ___collection0, method) ((  void (*) (List_1_t3118708580 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m689268888_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m2180211016_gshared (List_1_t3118708580 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m2180211016(__this, ___enumerable0, method) ((  void (*) (List_1_t3118708580 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m2180211016_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m4281230909_gshared (List_1_t3118708580 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m4281230909(__this, ___collection0, method) ((  void (*) (List_1_t3118708580 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m4281230909_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t3935373140 * List_1_AsReadOnly_m264700350_gshared (List_1_t3118708580 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m264700350(__this, method) ((  ReadOnlyCollection_1_t3935373140 * (*) (List_1_t3118708580 *, const MethodInfo*))List_1_AsReadOnly_m264700350_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::Clear()
extern "C"  void List_1_Clear_m1356100165_gshared (List_1_t3118708580 * __this, const MethodInfo* method);
#define List_1_Clear_m1356100165(__this, method) ((  void (*) (List_1_t3118708580 *, const MethodInfo*))List_1_Clear_m1356100165_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::Contains(T)
extern "C"  bool List_1_Contains_m867048599_gshared (List_1_t3118708580 * __this, KeyValuePair_2_t3749587448  ___item0, const MethodInfo* method);
#define List_1_Contains_m867048599(__this, ___item0, method) ((  bool (*) (List_1_t3118708580 *, KeyValuePair_2_t3749587448 , const MethodInfo*))List_1_Contains_m867048599_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::CopyTo(T[])
extern "C"  void List_1_CopyTo_m636002738_gshared (List_1_t3118708580 * __this, KeyValuePair_2U5BU5D_t598529833* ___array0, const MethodInfo* method);
#define List_1_CopyTo_m636002738(__this, ___array0, method) ((  void (*) (List_1_t3118708580 *, KeyValuePair_2U5BU5D_t598529833*, const MethodInfo*))List_1_CopyTo_m636002738_gshared)(__this, ___array0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m2182131921_gshared (List_1_t3118708580 * __this, KeyValuePair_2U5BU5D_t598529833* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m2182131921(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3118708580 *, KeyValuePair_2U5BU5D_t598529833*, int32_t, const MethodInfo*))List_1_CopyTo_m2182131921_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::Find(System.Predicate`1<T>)
extern "C"  KeyValuePair_2_t3749587448  List_1_Find_m3210757867_gshared (List_1_t3118708580 * __this, Predicate_1_t2192557563 * ___match0, const MethodInfo* method);
#define List_1_Find_m3210757867(__this, ___match0, method) ((  KeyValuePair_2_t3749587448  (*) (List_1_t3118708580 *, Predicate_1_t2192557563 *, const MethodInfo*))List_1_Find_m3210757867_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m1388680066_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t2192557563 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m1388680066(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t2192557563 *, const MethodInfo*))List_1_CheckMatch_m1388680066_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m1092482895_gshared (List_1_t3118708580 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t2192557563 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m1092482895(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t3118708580 *, int32_t, int32_t, Predicate_1_t2192557563 *, const MethodInfo*))List_1_GetIndex_m1092482895_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::GetEnumerator()
extern "C"  Enumerator_t2653438254  List_1_GetEnumerator_m1834058818_gshared (List_1_t3118708580 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m1834058818(__this, method) ((  Enumerator_t2653438254  (*) (List_1_t3118708580 *, const MethodInfo*))List_1_GetEnumerator_m1834058818_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m2777891557_gshared (List_1_t3118708580 * __this, KeyValuePair_2_t3749587448  ___item0, const MethodInfo* method);
#define List_1_IndexOf_m2777891557(__this, ___item0, method) ((  int32_t (*) (List_1_t3118708580 *, KeyValuePair_2_t3749587448 , const MethodInfo*))List_1_IndexOf_m2777891557_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m3321700302_gshared (List_1_t3118708580 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m3321700302(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t3118708580 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3321700302_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m1619403637_gshared (List_1_t3118708580 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m1619403637(__this, ___index0, method) ((  void (*) (List_1_t3118708580 *, int32_t, const MethodInfo*))List_1_CheckIndex_m1619403637_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m1032614692_gshared (List_1_t3118708580 * __this, int32_t ___index0, KeyValuePair_2_t3749587448  ___item1, const MethodInfo* method);
#define List_1_Insert_m1032614692(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3118708580 *, int32_t, KeyValuePair_2_t3749587448 , const MethodInfo*))List_1_Insert_m1032614692_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m3468721075_gshared (List_1_t3118708580 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m3468721075(__this, ___collection0, method) ((  void (*) (List_1_t3118708580 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m3468721075_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::Remove(T)
extern "C"  bool List_1_Remove_m979067522_gshared (List_1_t3118708580 * __this, KeyValuePair_2_t3749587448  ___item0, const MethodInfo* method);
#define List_1_Remove_m979067522(__this, ___item0, method) ((  bool (*) (List_1_t3118708580 *, KeyValuePair_2_t3749587448 , const MethodInfo*))List_1_Remove_m979067522_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m1429561466_gshared (List_1_t3118708580 * __this, Predicate_1_t2192557563 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m1429561466(__this, ___match0, method) ((  int32_t (*) (List_1_t3118708580 *, Predicate_1_t2192557563 *, const MethodInfo*))List_1_RemoveAll_m1429561466_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m4079328960_gshared (List_1_t3118708580 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m4079328960(__this, ___index0, method) ((  void (*) (List_1_t3118708580 *, int32_t, const MethodInfo*))List_1_RemoveAt_m4079328960_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::Reverse()
extern "C"  void List_1_Reverse_m2818481836_gshared (List_1_t3118708580 * __this, const MethodInfo* method);
#define List_1_Reverse_m2818481836(__this, method) ((  void (*) (List_1_t3118708580 *, const MethodInfo*))List_1_Reverse_m2818481836_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::Sort()
extern "C"  void List_1_Sort_m2540290864_gshared (List_1_t3118708580 * __this, const MethodInfo* method);
#define List_1_Sort_m2540290864(__this, method) ((  void (*) (List_1_t3118708580 *, const MethodInfo*))List_1_Sort_m2540290864_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m2695943156_gshared (List_1_t3118708580 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m2695943156(__this, ___comparer0, method) ((  void (*) (List_1_t3118708580 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m2695943156_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m4036603239_gshared (List_1_t3118708580 * __this, Comparison_1_t716359003 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m4036603239(__this, ___comparison0, method) ((  void (*) (List_1_t3118708580 *, Comparison_1_t716359003 *, const MethodInfo*))List_1_Sort_m4036603239_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::ToArray()
extern "C"  KeyValuePair_2U5BU5D_t598529833* List_1_ToArray_m2835802167_gshared (List_1_t3118708580 * __this, const MethodInfo* method);
#define List_1_ToArray_m2835802167(__this, method) ((  KeyValuePair_2U5BU5D_t598529833* (*) (List_1_t3118708580 *, const MethodInfo*))List_1_ToArray_m2835802167_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::TrimExcess()
extern "C"  void List_1_TrimExcess_m2063481609_gshared (List_1_t3118708580 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m2063481609(__this, method) ((  void (*) (List_1_t3118708580 *, const MethodInfo*))List_1_TrimExcess_m2063481609_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m1698591343_gshared (List_1_t3118708580 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m1698591343(__this, method) ((  int32_t (*) (List_1_t3118708580 *, const MethodInfo*))List_1_get_Capacity_m1698591343_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m1358623928_gshared (List_1_t3118708580 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m1358623928(__this, ___value0, method) ((  void (*) (List_1_t3118708580 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1358623928_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::get_Count()
extern "C"  int32_t List_1_get_Count_m194499298_gshared (List_1_t3118708580 * __this, const MethodInfo* method);
#define List_1_get_Count_m194499298(__this, method) ((  int32_t (*) (List_1_t3118708580 *, const MethodInfo*))List_1_get_Count_m194499298_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::get_Item(System.Int32)
extern "C"  KeyValuePair_2_t3749587448  List_1_get_Item_m3290918958_gshared (List_1_t3118708580 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m3290918958(__this, ___index0, method) ((  KeyValuePair_2_t3749587448  (*) (List_1_t3118708580 *, int32_t, const MethodInfo*))List_1_get_Item_m3290918958_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m784122457_gshared (List_1_t3118708580 * __this, int32_t ___index0, KeyValuePair_2_t3749587448  ___value1, const MethodInfo* method);
#define List_1_set_Item_m784122457(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3118708580 *, int32_t, KeyValuePair_2_t3749587448 , const MethodInfo*))List_1_set_Item_m784122457_gshared)(__this, ___index0, ___value1, method)
