﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.ExecutionContext
struct ExecutionContext_t2943675185;
// Amazon.Runtime.IRequestContext
struct IRequestContext_t1620878341;
// Amazon.Runtime.IResponseContext
struct IResponseContext_t898521739;
// Amazon.Runtime.IExecutionContext
struct IExecutionContext_t2477130752;
// Amazon.Runtime.IAsyncExecutionContext
struct IAsyncExecutionContext_t3792344986;

#include "codegen/il2cpp-codegen.h"

// Amazon.Runtime.IRequestContext Amazon.Runtime.Internal.ExecutionContext::get_RequestContext()
extern "C"  Il2CppObject * ExecutionContext_get_RequestContext_m2658549096 (ExecutionContext_t2943675185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.ExecutionContext::set_RequestContext(Amazon.Runtime.IRequestContext)
extern "C"  void ExecutionContext_set_RequestContext_m3728714375 (ExecutionContext_t2943675185 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.IResponseContext Amazon.Runtime.Internal.ExecutionContext::get_ResponseContext()
extern "C"  Il2CppObject * ExecutionContext_get_ResponseContext_m2153432014 (ExecutionContext_t2943675185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.ExecutionContext::set_ResponseContext(Amazon.Runtime.IResponseContext)
extern "C"  void ExecutionContext_set_ResponseContext_m199009595 (ExecutionContext_t2943675185 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.ExecutionContext::.ctor(Amazon.Runtime.IRequestContext,Amazon.Runtime.IResponseContext)
extern "C"  void ExecutionContext__ctor_m2901964359 (ExecutionContext_t2943675185 * __this, Il2CppObject * ___requestContext0, Il2CppObject * ___responseContext1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.IExecutionContext Amazon.Runtime.Internal.ExecutionContext::CreateFromAsyncContext(Amazon.Runtime.IAsyncExecutionContext)
extern "C"  Il2CppObject * ExecutionContext_CreateFromAsyncContext_m4244430103 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___asyncContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
