﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.UnityMainThreadDispatcher
struct UnityMainThreadDispatcher_t4098072663;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// Amazon.Runtime.IUnityHttpRequest
struct IUnityHttpRequest_t1859903397;

#include "codegen/il2cpp-codegen.h"

// System.Void Amazon.Runtime.Internal.UnityMainThreadDispatcher::Awake()
extern "C"  void UnityMainThreadDispatcher_Awake_m431792698 (UnityMainThreadDispatcher_t4098072663 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.UnityMainThreadDispatcher::Update()
extern "C"  void UnityMainThreadDispatcher_Update_m3817354668 (UnityMainThreadDispatcher_t4098072663 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.UnityMainThreadDispatcher::ProcessRequests()
extern "C"  void UnityMainThreadDispatcher_ProcessRequests_m3921503362 (UnityMainThreadDispatcher_t4098072663 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Amazon.Runtime.Internal.UnityMainThreadDispatcher::InvokeRequest(Amazon.Runtime.IUnityHttpRequest)
extern "C"  Il2CppObject * UnityMainThreadDispatcher_InvokeRequest_m387108235 (UnityMainThreadDispatcher_t4098072663 * __this, Il2CppObject * ___request0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.UnityMainThreadDispatcher::.ctor()
extern "C"  void UnityMainThreadDispatcher__ctor_m3355029385 (UnityMainThreadDispatcher_t4098072663 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
