﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetCredentialsForIdentityResponseUnmarshaller
struct GetCredentialsForIdentityResponseUnmarshaller_t2007495598;
// Amazon.Runtime.AmazonWebServiceResponse
struct AmazonWebServiceResponse_t529043356;
// Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext
struct JsonUnmarshallerContext_t456235889;
// Amazon.Runtime.AmazonServiceException
struct AmazonServiceException_t3748559634;
// System.Exception
struct Exception_t1927440687;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_JsonU456235889.h"
#include "mscorlib_System_Exception1927440687.h"
#include "System_System_Net_HttpStatusCode1898409641.h"

// Amazon.Runtime.AmazonWebServiceResponse Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetCredentialsForIdentityResponseUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern "C"  AmazonWebServiceResponse_t529043356 * GetCredentialsForIdentityResponseUnmarshaller_Unmarshall_m634220126 (GetCredentialsForIdentityResponseUnmarshaller_t2007495598 * __this, JsonUnmarshallerContext_t456235889 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.AmazonServiceException Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetCredentialsForIdentityResponseUnmarshaller::UnmarshallException(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,System.Exception,System.Net.HttpStatusCode)
extern "C"  AmazonServiceException_t3748559634 * GetCredentialsForIdentityResponseUnmarshaller_UnmarshallException_m3198160240 (GetCredentialsForIdentityResponseUnmarshaller_t2007495598 * __this, JsonUnmarshallerContext_t456235889 * ___context0, Exception_t1927440687 * ___innerException1, int32_t ___statusCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetCredentialsForIdentityResponseUnmarshaller Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetCredentialsForIdentityResponseUnmarshaller::get_Instance()
extern "C"  GetCredentialsForIdentityResponseUnmarshaller_t2007495598 * GetCredentialsForIdentityResponseUnmarshaller_get_Instance_m1533679114 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetCredentialsForIdentityResponseUnmarshaller::.ctor()
extern "C"  void GetCredentialsForIdentityResponseUnmarshaller__ctor_m294464060 (GetCredentialsForIdentityResponseUnmarshaller_t2007495598 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetCredentialsForIdentityResponseUnmarshaller::.cctor()
extern "C"  void GetCredentialsForIdentityResponseUnmarshaller__cctor_m3139304585 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
