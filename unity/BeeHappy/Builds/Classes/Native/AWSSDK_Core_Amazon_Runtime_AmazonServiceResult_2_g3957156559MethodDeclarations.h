﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_Runtime_AmazonServiceResult_2_g1630799325MethodDeclarations.h"

// System.Void Amazon.Runtime.AmazonServiceResult`2<Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest,Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityResponse>::set_Request(TRequest)
#define AmazonServiceResult_2_set_Request_m2144456864(__this, ___value0, method) ((  void (*) (AmazonServiceResult_2_t3957156559 *, AssumeRoleWithWebIdentityRequest_t193094215 *, const MethodInfo*))AmazonServiceResult_2_set_Request_m704866296_gshared)(__this, ___value0, method)
// TResponse Amazon.Runtime.AmazonServiceResult`2<Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest,Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityResponse>::get_Response()
#define AmazonServiceResult_2_get_Response_m2633586654(__this, method) ((  AssumeRoleWithWebIdentityResponse_t3931705881 * (*) (AmazonServiceResult_2_t3957156559 *, const MethodInfo*))AmazonServiceResult_2_get_Response_m2633022317_gshared)(__this, method)
// System.Void Amazon.Runtime.AmazonServiceResult`2<Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest,Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityResponse>::set_Response(TResponse)
#define AmazonServiceResult_2_set_Response_m3430621454(__this, ___value0, method) ((  void (*) (AmazonServiceResult_2_t3957156559 *, AssumeRoleWithWebIdentityResponse_t3931705881 *, const MethodInfo*))AmazonServiceResult_2_set_Response_m2527206554_gshared)(__this, ___value0, method)
// System.Exception Amazon.Runtime.AmazonServiceResult`2<Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest,Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityResponse>::get_Exception()
#define AmazonServiceResult_2_get_Exception_m2483909116(__this, method) ((  Exception_t1927440687 * (*) (AmazonServiceResult_2_t3957156559 *, const MethodInfo*))AmazonServiceResult_2_get_Exception_m3649734926_gshared)(__this, method)
// System.Void Amazon.Runtime.AmazonServiceResult`2<Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest,Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityResponse>::set_Exception(System.Exception)
#define AmazonServiceResult_2_set_Exception_m3477610831(__this, ___value0, method) ((  void (*) (AmazonServiceResult_2_t3957156559 *, Exception_t1927440687 *, const MethodInfo*))AmazonServiceResult_2_set_Exception_m2511823045_gshared)(__this, ___value0, method)
// System.Void Amazon.Runtime.AmazonServiceResult`2<Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest,Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityResponse>::set_state(System.Object)
#define AmazonServiceResult_2_set_state_m338222977(__this, ___value0, method) ((  void (*) (AmazonServiceResult_2_t3957156559 *, Il2CppObject *, const MethodInfo*))AmazonServiceResult_2_set_state_m3162568831_gshared)(__this, ___value0, method)
// System.Void Amazon.Runtime.AmazonServiceResult`2<Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest,Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityResponse>::.ctor(TRequest,TResponse,System.Exception,System.Object)
#define AmazonServiceResult_2__ctor_m1060740062(__this, ___request0, ___response1, ___exception2, ___state3, method) ((  void (*) (AmazonServiceResult_2_t3957156559 *, AssumeRoleWithWebIdentityRequest_t193094215 *, AssumeRoleWithWebIdentityResponse_t3931705881 *, Exception_t1927440687 *, Il2CppObject *, const MethodInfo*))AmazonServiceResult_2__ctor_m2485307111_gshared)(__this, ___request0, ___response1, ___exception2, ___state3, method)
