﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.Util.WrapperStream
struct WrapperStream_t816765491;
// System.IO.Stream
struct Stream_t3255436806;
// System.Func`2<System.IO.Stream,System.Boolean>
struct Func_2_t4109217329;
// System.Byte[]
struct ByteU5BU5D_t3397334013;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_Stream3255436806.h"
#include "mscorlib_System_IO_SeekOrigin2475945306.h"

// System.IO.Stream Amazon.Runtime.Internal.Util.WrapperStream::get_BaseStream()
extern "C"  Stream_t3255436806 * WrapperStream_get_BaseStream_m2476258189 (WrapperStream_t816765491 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.WrapperStream::set_BaseStream(System.IO.Stream)
extern "C"  void WrapperStream_set_BaseStream_m278286216 (WrapperStream_t816765491 * __this, Stream_t3255436806 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.WrapperStream::.ctor(System.IO.Stream)
extern "C"  void WrapperStream__ctor_m773200536 (WrapperStream_t816765491 * __this, Stream_t3255436806 * ___baseStream0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream Amazon.Runtime.Internal.Util.WrapperStream::GetNonWrapperBaseStream()
extern "C"  Stream_t3255436806 * WrapperStream_GetNonWrapperBaseStream_m3965714472 (WrapperStream_t816765491 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream Amazon.Runtime.Internal.Util.WrapperStream::GetSeekableBaseStream()
extern "C"  Stream_t3255436806 * WrapperStream_GetSeekableBaseStream_m4218896318 (WrapperStream_t816765491 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream Amazon.Runtime.Internal.Util.WrapperStream::GetNonWrapperBaseStream(System.IO.Stream)
extern "C"  Stream_t3255436806 * WrapperStream_GetNonWrapperBaseStream_m788423185 (Il2CppObject * __this /* static, unused */, Stream_t3255436806 * ___stream0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream Amazon.Runtime.Internal.Util.WrapperStream::SearchWrappedStream(System.Func`2<System.IO.Stream,System.Boolean>)
extern "C"  Stream_t3255436806 * WrapperStream_SearchWrappedStream_m3439853611 (WrapperStream_t816765491 * __this, Func_2_t4109217329 * ___condition0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream Amazon.Runtime.Internal.Util.WrapperStream::SearchWrappedStream(System.IO.Stream,System.Func`2<System.IO.Stream,System.Boolean>)
extern "C"  Stream_t3255436806 * WrapperStream_SearchWrappedStream_m3169410270 (Il2CppObject * __this /* static, unused */, Stream_t3255436806 * ___stream0, Func_2_t4109217329 * ___condition1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.Util.WrapperStream::get_CanRead()
extern "C"  bool WrapperStream_get_CanRead_m3792033364 (WrapperStream_t816765491 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.Util.WrapperStream::get_CanSeek()
extern "C"  bool WrapperStream_get_CanSeek_m2091433916 (WrapperStream_t816765491 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.Util.WrapperStream::get_CanWrite()
extern "C"  bool WrapperStream_get_CanWrite_m1468942181 (WrapperStream_t816765491 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.WrapperStream::Close()
extern "C"  void WrapperStream_Close_m1130225307 (WrapperStream_t816765491 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Amazon.Runtime.Internal.Util.WrapperStream::get_Length()
extern "C"  int64_t WrapperStream_get_Length_m3177564249 (WrapperStream_t816765491 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Amazon.Runtime.Internal.Util.WrapperStream::get_Position()
extern "C"  int64_t WrapperStream_get_Position_m235213418 (WrapperStream_t816765491 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.WrapperStream::set_Position(System.Int64)
extern "C"  void WrapperStream_set_Position_m1410486475 (WrapperStream_t816765491 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Amazon.Runtime.Internal.Util.WrapperStream::get_ReadTimeout()
extern "C"  int32_t WrapperStream_get_ReadTimeout_m3593404099 (WrapperStream_t816765491 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Amazon.Runtime.Internal.Util.WrapperStream::get_WriteTimeout()
extern "C"  int32_t WrapperStream_get_WriteTimeout_m1123484454 (WrapperStream_t816765491 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.WrapperStream::Flush()
extern "C"  void WrapperStream_Flush_m1130385225 (WrapperStream_t816765491 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Amazon.Runtime.Internal.Util.WrapperStream::Read(System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t WrapperStream_Read_m2465825746 (WrapperStream_t816765491 * __this, ByteU5BU5D_t3397334013* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Amazon.Runtime.Internal.Util.WrapperStream::Seek(System.Int64,System.IO.SeekOrigin)
extern "C"  int64_t WrapperStream_Seek_m2232534777 (WrapperStream_t816765491 * __this, int64_t ___offset0, int32_t ___origin1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.WrapperStream::SetLength(System.Int64)
extern "C"  void WrapperStream_SetLength_m3388665845 (WrapperStream_t816765491 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.WrapperStream::Write(System.Byte[],System.Int32,System.Int32)
extern "C"  void WrapperStream_Write_m2140118391 (WrapperStream_t816765491 * __this, ByteU5BU5D_t3397334013* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
