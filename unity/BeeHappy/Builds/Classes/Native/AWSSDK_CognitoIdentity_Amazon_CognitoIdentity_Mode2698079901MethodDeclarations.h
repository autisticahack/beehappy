﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.CognitoIdentity.Model.GetOpenIdTokenRequest
struct GetOpenIdTokenRequest_t2698079901;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.String Amazon.CognitoIdentity.Model.GetOpenIdTokenRequest::get_IdentityId()
extern "C"  String_t* GetOpenIdTokenRequest_get_IdentityId_m4120850273 (GetOpenIdTokenRequest_t2698079901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoIdentity.Model.GetOpenIdTokenRequest::set_IdentityId(System.String)
extern "C"  void GetOpenIdTokenRequest_set_IdentityId_m3421098958 (GetOpenIdTokenRequest_t2698079901 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.CognitoIdentity.Model.GetOpenIdTokenRequest::IsSetIdentityId()
extern "C"  bool GetOpenIdTokenRequest_IsSetIdentityId_m2534485277 (GetOpenIdTokenRequest_t2698079901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> Amazon.CognitoIdentity.Model.GetOpenIdTokenRequest::get_Logins()
extern "C"  Dictionary_2_t3943999495 * GetOpenIdTokenRequest_get_Logins_m3321650339 (GetOpenIdTokenRequest_t2698079901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoIdentity.Model.GetOpenIdTokenRequest::set_Logins(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void GetOpenIdTokenRequest_set_Logins_m2054782372 (GetOpenIdTokenRequest_t2698079901 * __this, Dictionary_2_t3943999495 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.CognitoIdentity.Model.GetOpenIdTokenRequest::IsSetLogins()
extern "C"  bool GetOpenIdTokenRequest_IsSetLogins_m2650968532 (GetOpenIdTokenRequest_t2698079901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoIdentity.Model.GetOpenIdTokenRequest::.ctor()
extern "C"  void GetOpenIdTokenRequest__ctor_m2858717872 (GetOpenIdTokenRequest_t2698079901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
