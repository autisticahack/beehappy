﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Type
struct Type_t;
// System.Reflection.PropertyInfo[]
struct PropertyInfoU5BU5D_t1736152084;
// System.Reflection.MethodInfo[]
struct MethodInfoU5BU5D_t152480188;
// System.Reflection.PropertyInfo
struct PropertyInfo_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.DownloadHandlerBufferWrapper
struct  DownloadHandlerBufferWrapper_t1800693841  : public Il2CppObject
{
public:
	// System.Object Amazon.Runtime.Internal.DownloadHandlerBufferWrapper::<Instance>k__BackingField
	Il2CppObject * ___U3CInstanceU3Ek__BackingField_5;
	// System.Boolean Amazon.Runtime.Internal.DownloadHandlerBufferWrapper::disposedValue
	bool ___disposedValue_6;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(DownloadHandlerBufferWrapper_t1800693841, ___U3CInstanceU3Ek__BackingField_5)); }
	inline Il2CppObject * get_U3CInstanceU3Ek__BackingField_5() const { return ___U3CInstanceU3Ek__BackingField_5; }
	inline Il2CppObject ** get_address_of_U3CInstanceU3Ek__BackingField_5() { return &___U3CInstanceU3Ek__BackingField_5; }
	inline void set_U3CInstanceU3Ek__BackingField_5(Il2CppObject * value)
	{
		___U3CInstanceU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CInstanceU3Ek__BackingField_5, value);
	}

	inline static int32_t get_offset_of_disposedValue_6() { return static_cast<int32_t>(offsetof(DownloadHandlerBufferWrapper_t1800693841, ___disposedValue_6)); }
	inline bool get_disposedValue_6() const { return ___disposedValue_6; }
	inline bool* get_address_of_disposedValue_6() { return &___disposedValue_6; }
	inline void set_disposedValue_6(bool value)
	{
		___disposedValue_6 = value;
	}
};

struct DownloadHandlerBufferWrapper_t1800693841_StaticFields
{
public:
	// System.Type Amazon.Runtime.Internal.DownloadHandlerBufferWrapper::downloadHandlerBufferType
	Type_t * ___downloadHandlerBufferType_0;
	// System.Reflection.PropertyInfo[] Amazon.Runtime.Internal.DownloadHandlerBufferWrapper::downloadHandlerBufferProperties
	PropertyInfoU5BU5D_t1736152084* ___downloadHandlerBufferProperties_1;
	// System.Reflection.MethodInfo[] Amazon.Runtime.Internal.DownloadHandlerBufferWrapper::downloadHandlerBufferMethods
	MethodInfoU5BU5D_t152480188* ___downloadHandlerBufferMethods_2;
	// System.Reflection.PropertyInfo Amazon.Runtime.Internal.DownloadHandlerBufferWrapper::dataProperty
	PropertyInfo_t * ___dataProperty_3;
	// System.Reflection.MethodInfo Amazon.Runtime.Internal.DownloadHandlerBufferWrapper::dataGetMethod
	MethodInfo_t * ___dataGetMethod_4;

public:
	inline static int32_t get_offset_of_downloadHandlerBufferType_0() { return static_cast<int32_t>(offsetof(DownloadHandlerBufferWrapper_t1800693841_StaticFields, ___downloadHandlerBufferType_0)); }
	inline Type_t * get_downloadHandlerBufferType_0() const { return ___downloadHandlerBufferType_0; }
	inline Type_t ** get_address_of_downloadHandlerBufferType_0() { return &___downloadHandlerBufferType_0; }
	inline void set_downloadHandlerBufferType_0(Type_t * value)
	{
		___downloadHandlerBufferType_0 = value;
		Il2CppCodeGenWriteBarrier(&___downloadHandlerBufferType_0, value);
	}

	inline static int32_t get_offset_of_downloadHandlerBufferProperties_1() { return static_cast<int32_t>(offsetof(DownloadHandlerBufferWrapper_t1800693841_StaticFields, ___downloadHandlerBufferProperties_1)); }
	inline PropertyInfoU5BU5D_t1736152084* get_downloadHandlerBufferProperties_1() const { return ___downloadHandlerBufferProperties_1; }
	inline PropertyInfoU5BU5D_t1736152084** get_address_of_downloadHandlerBufferProperties_1() { return &___downloadHandlerBufferProperties_1; }
	inline void set_downloadHandlerBufferProperties_1(PropertyInfoU5BU5D_t1736152084* value)
	{
		___downloadHandlerBufferProperties_1 = value;
		Il2CppCodeGenWriteBarrier(&___downloadHandlerBufferProperties_1, value);
	}

	inline static int32_t get_offset_of_downloadHandlerBufferMethods_2() { return static_cast<int32_t>(offsetof(DownloadHandlerBufferWrapper_t1800693841_StaticFields, ___downloadHandlerBufferMethods_2)); }
	inline MethodInfoU5BU5D_t152480188* get_downloadHandlerBufferMethods_2() const { return ___downloadHandlerBufferMethods_2; }
	inline MethodInfoU5BU5D_t152480188** get_address_of_downloadHandlerBufferMethods_2() { return &___downloadHandlerBufferMethods_2; }
	inline void set_downloadHandlerBufferMethods_2(MethodInfoU5BU5D_t152480188* value)
	{
		___downloadHandlerBufferMethods_2 = value;
		Il2CppCodeGenWriteBarrier(&___downloadHandlerBufferMethods_2, value);
	}

	inline static int32_t get_offset_of_dataProperty_3() { return static_cast<int32_t>(offsetof(DownloadHandlerBufferWrapper_t1800693841_StaticFields, ___dataProperty_3)); }
	inline PropertyInfo_t * get_dataProperty_3() const { return ___dataProperty_3; }
	inline PropertyInfo_t ** get_address_of_dataProperty_3() { return &___dataProperty_3; }
	inline void set_dataProperty_3(PropertyInfo_t * value)
	{
		___dataProperty_3 = value;
		Il2CppCodeGenWriteBarrier(&___dataProperty_3, value);
	}

	inline static int32_t get_offset_of_dataGetMethod_4() { return static_cast<int32_t>(offsetof(DownloadHandlerBufferWrapper_t1800693841_StaticFields, ___dataGetMethod_4)); }
	inline MethodInfo_t * get_dataGetMethod_4() const { return ___dataGetMethod_4; }
	inline MethodInfo_t ** get_address_of_dataGetMethod_4() { return &___dataGetMethod_4; }
	inline void set_dataGetMethod_4(MethodInfo_t * value)
	{
		___dataGetMethod_4 = value;
		Il2CppCodeGenWriteBarrier(&___dataGetMethod_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
