﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_Model_Interna426394443.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_Model_Interna204284014.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_Model_Intern1142681020.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_Model_Intern2819156553.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_Model_Intern2263579438.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_Model_Intern3056833063.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_Model_Intern3453248271.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_Model_Intern3670264418.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_SyncManager_1222571519.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_SyncManager_3040902086.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_SyncManager_2745667881.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_SyncManager_2502617919.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_SyncManager_D713795432.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_SyncManager_D525675437.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_SyncManager_1829637511.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_SyncManager_D808000598.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_SyncManager_2111962513.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_SyncManager_4277381347.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_SyncManager_1748345498.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_SyncManager_3262698613.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_SyncManager_D604719473.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_SyncManager_2856416498.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_SyncManager_1234943264.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_SyncManager_3132007981.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_SyncManager_4165705311.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_SyncManager_1205730659.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_SyncManager_2321405236.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_SyncManager_R868799569.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_SyncManager_3030310309.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_SyncManager_I101590051.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_SyncManager_3359635340.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_SyncManager_3980096245.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_SyncManager_4235253406.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_SyncManager_3708524595.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_SyncManager_I516935539.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_Internal_Cog1718611005.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_Internal_Cog1356420430.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_Internal_Cog3915036330.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_Internal_Ama3874488976.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_Internal_Ama2640152593.h"
#include "UnityEngine_UI_U3CModuleU3E3783534214.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventSyste3466835263.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigg1967201810.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigg3959312622.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigg3365010046.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigg2524067914.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_ExecuteEve1693084770.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_MoveDirect1406276862.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycasterM3179336627.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResul21186376.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_UIBehaviou3960014691.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_AxisEventD1524870173.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_AbstractEv1333959294.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseEventD2681005625.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1599784723.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve2981963041.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1414739712.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseInput621514313.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseInputM1295781545.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerInp1441575871.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerInp2688375492.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerInp3572864619.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerInp3709210170.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_StandaloneIn70867863.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_Standalone2680906638.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_TouchInput2561058385.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseRaycas2336171397.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_Physics2DR3236822917.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PhysicsRayc249603239.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_Color3438117476.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_Color1328781136.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_Color3293839588.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_Float2986189219.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_Float2824271922.h"
#include "UnityEngine_UI_UnityEngine_UI_AnimationTriggers3244928895.h"
#include "UnityEngine_UI_UnityEngine_UI_Button2872111280.h"
#include "UnityEngine_UI_UnityEngine_UI_Button_ButtonClicked2455055323.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2800 = { sizeof (DeleteDatasetRequestMarshaller_t426394443), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2801 = { sizeof (DeleteDatasetResponseUnmarshaller_t204284014), -1, sizeof(DeleteDatasetResponseUnmarshaller_t204284014_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2801[1] = 
{
	DeleteDatasetResponseUnmarshaller_t204284014_StaticFields::get_offset_of__instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2802 = { sizeof (ListRecordsRequestMarshaller_t1142681020), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2803 = { sizeof (ListRecordsResponseUnmarshaller_t2819156553), -1, sizeof(ListRecordsResponseUnmarshaller_t2819156553_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2803[1] = 
{
	ListRecordsResponseUnmarshaller_t2819156553_StaticFields::get_offset_of__instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2804 = { sizeof (RecordPatchMarshaller_t2263579438), -1, sizeof(RecordPatchMarshaller_t2263579438_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2804[1] = 
{
	RecordPatchMarshaller_t2263579438_StaticFields::get_offset_of_Instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2805 = { sizeof (RecordUnmarshaller_t3056833063), -1, sizeof(RecordUnmarshaller_t3056833063_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2805[1] = 
{
	RecordUnmarshaller_t3056833063_StaticFields::get_offset_of__instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2806 = { sizeof (UpdateRecordsRequestMarshaller_t3453248271), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2807 = { sizeof (UpdateRecordsResponseUnmarshaller_t3670264418), -1, sizeof(UpdateRecordsResponseUnmarshaller_t3670264418_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2807[1] = 
{
	UpdateRecordsResponseUnmarshaller_t3670264418_StaticFields::get_offset_of__instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2808 = { sizeof (CognitoSyncManager_t1222571519), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2808[5] = 
{
	CognitoSyncManager_t1222571519::get_offset_of__logger_0(),
	CognitoSyncManager_t1222571519::get_offset_of__disposed_1(),
	CognitoSyncManager_t1222571519::get_offset_of_Local_2(),
	CognitoSyncManager_t1222571519::get_offset_of_Remote_3(),
	CognitoSyncManager_t1222571519::get_offset_of_CognitoCredentials_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2809 = { sizeof (Dataset_t3040902086), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2809[17] = 
{
	0,
	Dataset_t3040902086::get_offset_of__datasetName_1(),
	Dataset_t3040902086::get_offset_of__local_2(),
	Dataset_t3040902086::get_offset_of__remote_3(),
	Dataset_t3040902086::get_offset_of__cognitoCredentials_4(),
	Dataset_t3040902086::get_offset_of_waitingForConnectivity_5(),
	Dataset_t3040902086::get_offset_of__disposed_6(),
	Dataset_t3040902086::get_offset_of__logger_7(),
	Dataset_t3040902086::get_offset_of_locked_8(),
	Dataset_t3040902086::get_offset_of_queuedSync_9(),
	Dataset_t3040902086::get_offset_of_mOnSyncSuccess_10(),
	Dataset_t3040902086::get_offset_of_mOnSyncFailure_11(),
	Dataset_t3040902086::get_offset_of_OnSyncConflict_12(),
	Dataset_t3040902086::get_offset_of_OnDatasetDeleted_13(),
	Dataset_t3040902086::get_offset_of_OnDatasetMerged_14(),
	Dataset_t3040902086::get_offset_of__netReachability_15(),
	Dataset_t3040902086::get_offset_of_OnConnectivityOptions_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2810 = { sizeof (SyncConflictDelegate_t2745667881), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2811 = { sizeof (DatasetDeletedDelegate_t2502617919), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2812 = { sizeof (DatasetMergedDelegate_t713795432), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2813 = { sizeof (U3CU3Ec__DisplayClass40_0_t525675437), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2813[1] = 
{
	U3CU3Ec__DisplayClass40_0_t525675437::get_offset_of_conflictRecords_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2814 = { sizeof (U3CU3Ec__DisplayClass71_0_t1829637511), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2814[2] = 
{
	U3CU3Ec__DisplayClass71_0_t1829637511::get_offset_of_options_0(),
	U3CU3Ec__DisplayClass71_0_t1829637511::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2815 = { sizeof (U3CU3Ec__DisplayClass72_0_t808000598), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2815[2] = 
{
	U3CU3Ec__DisplayClass72_0_t808000598::get_offset_of_records_0(),
	U3CU3Ec__DisplayClass72_0_t808000598::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2816 = { sizeof (U3CU3Ec__DisplayClass73_0_t2111962513), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2816[2] = 
{
	U3CU3Ec__DisplayClass73_0_t2111962513::get_offset_of_exception_0(),
	U3CU3Ec__DisplayClass73_0_t2111962513::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2817 = { sizeof (SyncSuccessEventArgs_t4277381347), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2817[1] = 
{
	SyncSuccessEventArgs_t4277381347::get_offset_of_U3CUpdatedRecordsU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2818 = { sizeof (SyncFailureEventArgs_t1748345498), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2818[1] = 
{
	SyncFailureEventArgs_t1748345498::get_offset_of_U3CExceptionU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2819 = { sizeof (DataConflictException_t3262698613), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2820 = { sizeof (DataLimitExceededException_t604719473), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2821 = { sizeof (DatasetNotFoundException_t2856416498), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2822 = { sizeof (DataStorageException_t1234943264), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2823 = { sizeof (NetworkException_t3132007981), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2824 = { sizeof (SyncManagerException_t4165705311), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2825 = { sizeof (DatasetMetadata_t1205730659), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2825[6] = 
{
	DatasetMetadata_t1205730659::get_offset_of__datasetName_0(),
	DatasetMetadata_t1205730659::get_offset_of__creationDate_1(),
	DatasetMetadata_t1205730659::get_offset_of__lastModifiedDate_2(),
	DatasetMetadata_t1205730659::get_offset_of__lastModifiedBy_3(),
	DatasetMetadata_t1205730659::get_offset_of__storageSizeBytes_4(),
	DatasetMetadata_t1205730659::get_offset_of__recordCount_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2826 = { sizeof (DatasetUpdates_t2321405236), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2826[7] = 
{
	DatasetUpdates_t2321405236::get_offset_of__datasetName_0(),
	DatasetUpdates_t2321405236::get_offset_of__records_1(),
	DatasetUpdates_t2321405236::get_offset_of__syncCount_2(),
	DatasetUpdates_t2321405236::get_offset_of__syncSessionToken_3(),
	DatasetUpdates_t2321405236::get_offset_of__exists_4(),
	DatasetUpdates_t2321405236::get_offset_of__deleted_5(),
	DatasetUpdates_t2321405236::get_offset_of__mergedDatasetNameList_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2827 = { sizeof (Record_t868799569), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2827[7] = 
{
	Record_t868799569::get_offset_of__key_0(),
	Record_t868799569::get_offset_of__value_1(),
	Record_t868799569::get_offset_of__syncCount_2(),
	Record_t868799569::get_offset_of__lastModifiedDate_3(),
	Record_t868799569::get_offset_of__lastModifiedBy_4(),
	Record_t868799569::get_offset_of__deviceLastModifiedDate_5(),
	Record_t868799569::get_offset_of__modified_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2828 = { sizeof (SyncConflict_t3030310309), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2828[3] = 
{
	SyncConflict_t3030310309::get_offset_of__key_0(),
	SyncConflict_t3030310309::get_offset_of__remoteRecord_1(),
	SyncConflict_t3030310309::get_offset_of__localRecord_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2829 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2830 = { sizeof (CognitoSyncStorage_t101590051), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2830[3] = 
{
	CognitoSyncStorage_t101590051::get_offset_of_identityPoolId_0(),
	CognitoSyncStorage_t101590051::get_offset_of_client_1(),
	CognitoSyncStorage_t101590051::get_offset_of_cognitoCredentials_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2831 = { sizeof (SQLiteLocalStorage_t3359635340), -1, sizeof(SQLiteLocalStorage_t3359635340_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2831[5] = 
{
	SQLiteLocalStorage_t3359635340::get_offset_of__logger_0(),
	SQLiteLocalStorage_t3359635340_StaticFields::get_offset_of_sqlite_lock_1(),
	SQLiteLocalStorage_t3359635340::get_offset_of_directoryPath_2(),
	SQLiteLocalStorage_t3359635340::get_offset_of_filePath_3(),
	SQLiteLocalStorage_t3359635340::get_offset_of_connection_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2832 = { sizeof (DatasetColumns_t3980096245), -1, sizeof(DatasetColumns_t3980096245_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2832[11] = 
{
	DatasetColumns_t3980096245_StaticFields::get_offset_of_ALL_0(),
	DatasetColumns_t3980096245_StaticFields::get_offset_of_IDENTITY_ID_IDX_1(),
	DatasetColumns_t3980096245_StaticFields::get_offset_of_DATASET_NAME_IDX_2(),
	DatasetColumns_t3980096245_StaticFields::get_offset_of_CREATION_TIMESTAMP_IDX_3(),
	DatasetColumns_t3980096245_StaticFields::get_offset_of_LAST_MODIFIED_TIMESTAMP_IDX_4(),
	DatasetColumns_t3980096245_StaticFields::get_offset_of_LAST_MODIFIED_BY_IDX_5(),
	DatasetColumns_t3980096245_StaticFields::get_offset_of_STORAGE_SIZE_BYTES_IDX_6(),
	DatasetColumns_t3980096245_StaticFields::get_offset_of_RECORD_COUNT_IDX_7(),
	DatasetColumns_t3980096245_StaticFields::get_offset_of_LAST_SYNC_COUNT_IDX_8(),
	DatasetColumns_t3980096245_StaticFields::get_offset_of_LAST_SYNC_TIMESTAMP_IDX_9(),
	DatasetColumns_t3980096245_StaticFields::get_offset_of_LAST_SYNC_RESULT_IDX_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2833 = { sizeof (RecordColumns_t4235253406), -1, sizeof(RecordColumns_t4235253406_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2833[10] = 
{
	RecordColumns_t4235253406_StaticFields::get_offset_of_ALL_0(),
	RecordColumns_t4235253406_StaticFields::get_offset_of_IDENTITY_ID_IDX_1(),
	RecordColumns_t4235253406_StaticFields::get_offset_of_DATASET_NAME_IDX_2(),
	RecordColumns_t4235253406_StaticFields::get_offset_of_KEY_IDX_3(),
	RecordColumns_t4235253406_StaticFields::get_offset_of_VALUE_IDX_4(),
	RecordColumns_t4235253406_StaticFields::get_offset_of_SYNC_COUNT_IDX_5(),
	RecordColumns_t4235253406_StaticFields::get_offset_of_LAST_MODIFIED_TIMESTAMP_IDX_6(),
	RecordColumns_t4235253406_StaticFields::get_offset_of_LAST_MODIFIED_BY_IDX_7(),
	RecordColumns_t4235253406_StaticFields::get_offset_of_DEVICE_LAST_MODIFIED_TIMESTAMP_IDX_8(),
	RecordColumns_t4235253406_StaticFields::get_offset_of_MODIFIED_IDX_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2834 = { sizeof (Statement_t3708524595), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2834[2] = 
{
	Statement_t3708524595::get_offset_of_U3CQueryU3Ek__BackingField_0(),
	Statement_t3708524595::get_offset_of_U3CParametersU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2835 = { sizeof (DatasetUtils_t516935539), -1, sizeof(DatasetUtils_t516935539_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2835[2] = 
{
	DatasetUtils_t516935539_StaticFields::get_offset_of_DATASET_NAME_PATTERN_0(),
	DatasetUtils_t516935539_StaticFields::get_offset_of_UNKNOWN_IDENTITY_ID_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2836 = { sizeof (CognitoCredentialsRetriever_t1718611005), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2837 = { sizeof (CSRequestCache_t1356420430), -1, sizeof(CSRequestCache_t1356420430_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2837[1] = 
{
	CSRequestCache_t1356420430_StaticFields::get_offset_of_requestCache_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2838 = { sizeof (CSRequest_t3915036330), -1, sizeof(CSRequest_t3915036330_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2838[4] = 
{
	CSRequest_t3915036330::get_offset_of_requestType_0(),
	CSRequest_t3915036330::get_offset_of_identityPoolIdProperty_1(),
	CSRequest_t3915036330::get_offset_of_identityIdProperty_2(),
	CSRequest_t3915036330_StaticFields::get_offset_of_SyncRequestType_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2839 = { sizeof (AmazonCognitoSyncPostMarshallHandler_t3874488976), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2840 = { sizeof (AmazonCognitoSyncPostSignHandler_t2640152593), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2841 = { sizeof (U3CModuleU3E_t3783534229), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2842 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2843 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2844 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2845 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2846 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2847 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2848 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2849 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2850 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2851 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2852 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2853 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2854 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2855 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2856 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2857 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2858 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2859 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2860 = { sizeof (EventSystem_t3466835263), -1, sizeof(EventSystem_t3466835263_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2860[11] = 
{
	EventSystem_t3466835263::get_offset_of_m_SystemInputModules_2(),
	EventSystem_t3466835263::get_offset_of_m_CurrentInputModule_3(),
	EventSystem_t3466835263_StaticFields::get_offset_of_U3CcurrentU3Ek__BackingField_4(),
	EventSystem_t3466835263::get_offset_of_m_FirstSelected_5(),
	EventSystem_t3466835263::get_offset_of_m_sendNavigationEvents_6(),
	EventSystem_t3466835263::get_offset_of_m_DragThreshold_7(),
	EventSystem_t3466835263::get_offset_of_m_CurrentSelected_8(),
	EventSystem_t3466835263::get_offset_of_m_SelectionGuard_9(),
	EventSystem_t3466835263::get_offset_of_m_DummyData_10(),
	EventSystem_t3466835263_StaticFields::get_offset_of_s_RaycastComparer_11(),
	EventSystem_t3466835263_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2861 = { sizeof (EventTrigger_t1967201810), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2861[2] = 
{
	EventTrigger_t1967201810::get_offset_of_m_Delegates_2(),
	EventTrigger_t1967201810::get_offset_of_delegates_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2862 = { sizeof (TriggerEvent_t3959312622), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2863 = { sizeof (Entry_t3365010046), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2863[2] = 
{
	Entry_t3365010046::get_offset_of_eventID_0(),
	Entry_t3365010046::get_offset_of_callback_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2864 = { sizeof (EventTriggerType_t2524067914)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2864[18] = 
{
	EventTriggerType_t2524067914::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2865 = { sizeof (ExecuteEvents_t1693084770), -1, sizeof(ExecuteEvents_t1693084770_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2865[36] = 
{
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_PointerEnterHandler_0(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_PointerExitHandler_1(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_PointerDownHandler_2(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_PointerUpHandler_3(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_PointerClickHandler_4(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_InitializePotentialDragHandler_5(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_BeginDragHandler_6(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_DragHandler_7(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_EndDragHandler_8(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_DropHandler_9(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_ScrollHandler_10(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_UpdateSelectedHandler_11(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_SelectHandler_12(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_DeselectHandler_13(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_MoveHandler_14(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_SubmitHandler_15(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_CancelHandler_16(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_HandlerListPool_17(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_InternalTransformList_18(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_19(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_20(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_21(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3_22(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache4_23(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache5_24(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache6_25(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache7_26(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache8_27(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache9_28(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheA_29(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheB_30(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheC_31(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheD_32(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheE_33(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheF_34(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache10_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2866 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2867 = { sizeof (MoveDirection_t1406276862)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2867[6] = 
{
	MoveDirection_t1406276862::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2868 = { sizeof (RaycasterManager_t3179336627), -1, sizeof(RaycasterManager_t3179336627_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2868[1] = 
{
	RaycasterManager_t3179336627_StaticFields::get_offset_of_s_Raycasters_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2869 = { sizeof (RaycastResult_t21186376)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2869[10] = 
{
	RaycastResult_t21186376::get_offset_of_m_GameObject_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_module_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_distance_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_index_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_depth_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_sortingLayer_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_sortingOrder_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_worldPosition_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_worldNormal_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_screenPosition_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2870 = { sizeof (UIBehaviour_t3960014691), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2871 = { sizeof (AxisEventData_t1524870173), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2871[2] = 
{
	AxisEventData_t1524870173::get_offset_of_U3CmoveVectorU3Ek__BackingField_2(),
	AxisEventData_t1524870173::get_offset_of_U3CmoveDirU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2872 = { sizeof (AbstractEventData_t1333959294), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2872[1] = 
{
	AbstractEventData_t1333959294::get_offset_of_m_Used_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2873 = { sizeof (BaseEventData_t2681005625), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2873[1] = 
{
	BaseEventData_t2681005625::get_offset_of_m_EventSystem_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2874 = { sizeof (PointerEventData_t1599784723), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2874[21] = 
{
	PointerEventData_t1599784723::get_offset_of_U3CpointerEnterU3Ek__BackingField_2(),
	PointerEventData_t1599784723::get_offset_of_m_PointerPress_3(),
	PointerEventData_t1599784723::get_offset_of_U3ClastPressU3Ek__BackingField_4(),
	PointerEventData_t1599784723::get_offset_of_U3CrawPointerPressU3Ek__BackingField_5(),
	PointerEventData_t1599784723::get_offset_of_U3CpointerDragU3Ek__BackingField_6(),
	PointerEventData_t1599784723::get_offset_of_U3CpointerCurrentRaycastU3Ek__BackingField_7(),
	PointerEventData_t1599784723::get_offset_of_U3CpointerPressRaycastU3Ek__BackingField_8(),
	PointerEventData_t1599784723::get_offset_of_hovered_9(),
	PointerEventData_t1599784723::get_offset_of_U3CeligibleForClickU3Ek__BackingField_10(),
	PointerEventData_t1599784723::get_offset_of_U3CpointerIdU3Ek__BackingField_11(),
	PointerEventData_t1599784723::get_offset_of_U3CpositionU3Ek__BackingField_12(),
	PointerEventData_t1599784723::get_offset_of_U3CdeltaU3Ek__BackingField_13(),
	PointerEventData_t1599784723::get_offset_of_U3CpressPositionU3Ek__BackingField_14(),
	PointerEventData_t1599784723::get_offset_of_U3CworldPositionU3Ek__BackingField_15(),
	PointerEventData_t1599784723::get_offset_of_U3CworldNormalU3Ek__BackingField_16(),
	PointerEventData_t1599784723::get_offset_of_U3CclickTimeU3Ek__BackingField_17(),
	PointerEventData_t1599784723::get_offset_of_U3CclickCountU3Ek__BackingField_18(),
	PointerEventData_t1599784723::get_offset_of_U3CscrollDeltaU3Ek__BackingField_19(),
	PointerEventData_t1599784723::get_offset_of_U3CuseDragThresholdU3Ek__BackingField_20(),
	PointerEventData_t1599784723::get_offset_of_U3CdraggingU3Ek__BackingField_21(),
	PointerEventData_t1599784723::get_offset_of_U3CbuttonU3Ek__BackingField_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2875 = { sizeof (InputButton_t2981963041)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2875[4] = 
{
	InputButton_t2981963041::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2876 = { sizeof (FramePressState_t1414739712)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2876[5] = 
{
	FramePressState_t1414739712::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2877 = { sizeof (BaseInput_t621514313), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2878 = { sizeof (BaseInputModule_t1295781545), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2878[6] = 
{
	BaseInputModule_t1295781545::get_offset_of_m_RaycastResultCache_2(),
	BaseInputModule_t1295781545::get_offset_of_m_AxisEventData_3(),
	BaseInputModule_t1295781545::get_offset_of_m_EventSystem_4(),
	BaseInputModule_t1295781545::get_offset_of_m_BaseEventData_5(),
	BaseInputModule_t1295781545::get_offset_of_m_InputOverride_6(),
	BaseInputModule_t1295781545::get_offset_of_m_DefaultInput_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2879 = { sizeof (PointerInputModule_t1441575871), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2879[6] = 
{
	0,
	0,
	0,
	0,
	PointerInputModule_t1441575871::get_offset_of_m_PointerData_12(),
	PointerInputModule_t1441575871::get_offset_of_m_MouseState_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2880 = { sizeof (ButtonState_t2688375492), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2880[2] = 
{
	ButtonState_t2688375492::get_offset_of_m_Button_0(),
	ButtonState_t2688375492::get_offset_of_m_EventData_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2881 = { sizeof (MouseState_t3572864619), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2881[1] = 
{
	MouseState_t3572864619::get_offset_of_m_TrackedButtons_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2882 = { sizeof (MouseButtonEventData_t3709210170), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2882[2] = 
{
	MouseButtonEventData_t3709210170::get_offset_of_buttonState_0(),
	MouseButtonEventData_t3709210170::get_offset_of_buttonData_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2883 = { sizeof (StandaloneInputModule_t70867863), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2883[12] = 
{
	StandaloneInputModule_t70867863::get_offset_of_m_PrevActionTime_14(),
	StandaloneInputModule_t70867863::get_offset_of_m_LastMoveVector_15(),
	StandaloneInputModule_t70867863::get_offset_of_m_ConsecutiveMoveCount_16(),
	StandaloneInputModule_t70867863::get_offset_of_m_LastMousePosition_17(),
	StandaloneInputModule_t70867863::get_offset_of_m_MousePosition_18(),
	StandaloneInputModule_t70867863::get_offset_of_m_HorizontalAxis_19(),
	StandaloneInputModule_t70867863::get_offset_of_m_VerticalAxis_20(),
	StandaloneInputModule_t70867863::get_offset_of_m_SubmitButton_21(),
	StandaloneInputModule_t70867863::get_offset_of_m_CancelButton_22(),
	StandaloneInputModule_t70867863::get_offset_of_m_InputActionsPerSecond_23(),
	StandaloneInputModule_t70867863::get_offset_of_m_RepeatDelay_24(),
	StandaloneInputModule_t70867863::get_offset_of_m_ForceModuleActive_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2884 = { sizeof (InputMode_t2680906638)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2884[3] = 
{
	InputMode_t2680906638::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2885 = { sizeof (TouchInputModule_t2561058385), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2885[3] = 
{
	TouchInputModule_t2561058385::get_offset_of_m_LastMousePosition_14(),
	TouchInputModule_t2561058385::get_offset_of_m_MousePosition_15(),
	TouchInputModule_t2561058385::get_offset_of_m_ForceModuleActive_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2886 = { sizeof (BaseRaycaster_t2336171397), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2887 = { sizeof (Physics2DRaycaster_t3236822917), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2888 = { sizeof (PhysicsRaycaster_t249603239), -1, sizeof(PhysicsRaycaster_t249603239_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2888[4] = 
{
	0,
	PhysicsRaycaster_t249603239::get_offset_of_m_EventCamera_3(),
	PhysicsRaycaster_t249603239::get_offset_of_m_EventMask_4(),
	PhysicsRaycaster_t249603239_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2889 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2890 = { sizeof (ColorTween_t3438117476)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2890[6] = 
{
	ColorTween_t3438117476::get_offset_of_m_Target_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorTween_t3438117476::get_offset_of_m_StartColor_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorTween_t3438117476::get_offset_of_m_TargetColor_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorTween_t3438117476::get_offset_of_m_TweenMode_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorTween_t3438117476::get_offset_of_m_Duration_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorTween_t3438117476::get_offset_of_m_IgnoreTimeScale_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2891 = { sizeof (ColorTweenMode_t1328781136)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2891[4] = 
{
	ColorTweenMode_t1328781136::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2892 = { sizeof (ColorTweenCallback_t3293839588), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2893 = { sizeof (FloatTween_t2986189219)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2893[5] = 
{
	FloatTween_t2986189219::get_offset_of_m_Target_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FloatTween_t2986189219::get_offset_of_m_StartValue_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FloatTween_t2986189219::get_offset_of_m_TargetValue_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FloatTween_t2986189219::get_offset_of_m_Duration_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FloatTween_t2986189219::get_offset_of_m_IgnoreTimeScale_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2894 = { sizeof (FloatTweenCallback_t2824271922), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2895 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2895[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2896 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2896[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2897 = { sizeof (AnimationTriggers_t3244928895), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2897[8] = 
{
	0,
	0,
	0,
	0,
	AnimationTriggers_t3244928895::get_offset_of_m_NormalTrigger_4(),
	AnimationTriggers_t3244928895::get_offset_of_m_HighlightedTrigger_5(),
	AnimationTriggers_t3244928895::get_offset_of_m_PressedTrigger_6(),
	AnimationTriggers_t3244928895::get_offset_of_m_DisabledTrigger_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2898 = { sizeof (Button_t2872111280), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2898[1] = 
{
	Button_t2872111280::get_offset_of_m_OnClick_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2899 = { sizeof (ButtonClickedEvent_t2455055323), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
