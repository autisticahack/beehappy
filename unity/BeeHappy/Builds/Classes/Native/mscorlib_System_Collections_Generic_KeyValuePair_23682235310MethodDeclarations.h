﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_888819835MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.IntPtr,ThirdParty.iOS4Unity.Callbacks/Methods>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m2252198669(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3682235310 *, IntPtr_t, Methods_t1187897474 *, const MethodInfo*))KeyValuePair_2__ctor_m477426041_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.IntPtr,ThirdParty.iOS4Unity.Callbacks/Methods>::get_Key()
#define KeyValuePair_2_get_Key_m3640640279(__this, method) ((  IntPtr_t (*) (KeyValuePair_2_t3682235310 *, const MethodInfo*))KeyValuePair_2_get_Key_m1574332879_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.IntPtr,ThirdParty.iOS4Unity.Callbacks/Methods>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m2300976150(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3682235310 *, IntPtr_t, const MethodInfo*))KeyValuePair_2_set_Key_m4146602710_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.IntPtr,ThirdParty.iOS4Unity.Callbacks/Methods>::get_Value()
#define KeyValuePair_2_get_Value_m1405385319(__this, method) ((  Methods_t1187897474 * (*) (KeyValuePair_2_t3682235310 *, const MethodInfo*))KeyValuePair_2_get_Value_m544293807_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.IntPtr,ThirdParty.iOS4Unity.Callbacks/Methods>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1416031606(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3682235310 *, Methods_t1187897474 *, const MethodInfo*))KeyValuePair_2_set_Value_m1938889438_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.IntPtr,ThirdParty.iOS4Unity.Callbacks/Methods>::ToString()
#define KeyValuePair_2_ToString_m1143914304(__this, method) ((  String_t* (*) (KeyValuePair_2_t3682235310 *, const MethodInfo*))KeyValuePair_2_ToString_m2882821022_gshared)(__this, method)
