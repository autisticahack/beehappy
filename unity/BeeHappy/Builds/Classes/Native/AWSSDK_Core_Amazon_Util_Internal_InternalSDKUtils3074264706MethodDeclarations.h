﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Action
struct Action_t3226471752;
// Amazon.Runtime.AsyncOptions
struct AsyncOptions_t558351272;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "System_Core_System_Action3226471752.h"
#include "AWSSDK_Core_Amazon_Runtime_AsyncOptions558351272.h"

// System.String Amazon.Util.Internal.InternalSDKUtils::BuildUserAgentString(System.String)
extern "C"  String_t* InternalSDKUtils_BuildUserAgentString_m4237096378 (Il2CppObject * __this /* static, unused */, String_t* ___serviceSdkVersion0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Util.Internal.InternalSDKUtils::get_IsAndroid()
extern "C"  bool InternalSDKUtils_get_IsAndroid_m882919270 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Util.Internal.InternalSDKUtils::get_IsiOS()
extern "C"  bool InternalSDKUtils_get_IsiOS_m2101030324 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Amazon.Util.Internal.InternalSDKUtils::GetTypeFromUnityEngine(System.String)
extern "C"  Type_t * InternalSDKUtils_GetTypeFromUnityEngine_m1190087075 (Il2CppObject * __this /* static, unused */, String_t* ___typeName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Util.Internal.InternalSDKUtils::AsyncExecutor(System.Action,Amazon.Runtime.AsyncOptions)
extern "C"  void InternalSDKUtils_AsyncExecutor_m1800494534 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___action0, AsyncOptions_t558351272 * ___options1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Util.Internal.InternalSDKUtils::SafeExecute(System.Action)
extern "C"  void InternalSDKUtils_SafeExecute_m468138151 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Util.Internal.InternalSDKUtils::.cctor()
extern "C"  void InternalSDKUtils__cctor_m2673058827 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
