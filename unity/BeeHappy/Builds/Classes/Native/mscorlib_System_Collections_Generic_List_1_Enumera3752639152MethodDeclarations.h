﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<System.Xml.Linq.XElement>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m311297317(__this, ___l0, method) ((  void (*) (Enumerator_t3752639152 *, List_1_t4217909478 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Xml.Linq.XElement>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1743908541(__this, method) ((  void (*) (Enumerator_t3752639152 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Xml.Linq.XElement>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3960861537(__this, method) ((  Il2CppObject * (*) (Enumerator_t3752639152 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Xml.Linq.XElement>::Dispose()
#define Enumerator_Dispose_m2111067356(__this, method) ((  void (*) (Enumerator_t3752639152 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Xml.Linq.XElement>::VerifyState()
#define Enumerator_VerifyState_m1306269735(__this, method) ((  void (*) (Enumerator_t3752639152 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Xml.Linq.XElement>::MoveNext()
#define Enumerator_MoveNext_m717540021(__this, method) ((  bool (*) (Enumerator_t3752639152 *, const MethodInfo*))Enumerator_MoveNext_m984484162_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Xml.Linq.XElement>::get_Current()
#define Enumerator_get_Current_m1776166564(__this, method) ((  XElement_t553821050 * (*) (Enumerator_t3752639152 *, const MethodInfo*))Enumerator_get_Current_m2193722336_gshared)(__this, method)
