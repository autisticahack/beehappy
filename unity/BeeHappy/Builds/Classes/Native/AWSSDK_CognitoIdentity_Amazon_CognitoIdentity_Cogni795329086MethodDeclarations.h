﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.CognitoIdentity.CognitoAWSCredentials/<>c__DisplayClass59_0
struct U3CU3Ec__DisplayClass59_0_t795329086;
// Amazon.Runtime.AmazonServiceResult`2<Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest,Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityResponse>
struct AmazonServiceResult_2_t3957156559;

#include "codegen/il2cpp-codegen.h"

// System.Void Amazon.CognitoIdentity.CognitoAWSCredentials/<>c__DisplayClass59_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass59_0__ctor_m4011845581 (U3CU3Ec__DisplayClass59_0_t795329086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoIdentity.CognitoAWSCredentials/<>c__DisplayClass59_0::<GetStsCredentials>b__0(Amazon.Runtime.AmazonServiceResult`2<Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest,Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityResponse>)
extern "C"  void U3CU3Ec__DisplayClass59_0_U3CGetStsCredentialsU3Eb__0_m1388819973 (U3CU3Ec__DisplayClass59_0_t795329086 * __this, AmazonServiceResult_2_t3957156559 * ___assumeResult0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
