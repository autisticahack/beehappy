﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen621922706.h"
#include "mscorlib_System_Array3829468939.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_ObjectMetadata4058137740.h"

// System.Void System.Array/InternalEnumerator`1<ThirdParty.Json.LitJson.ObjectMetadata>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m4110446661_gshared (InternalEnumerator_1_t621922706 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m4110446661(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t621922706 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m4110446661_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3058495193_gshared (InternalEnumerator_1_t621922706 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3058495193(__this, method) ((  void (*) (InternalEnumerator_1_t621922706 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3058495193_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3040574665_gshared (InternalEnumerator_1_t621922706 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3040574665(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t621922706 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3040574665_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<ThirdParty.Json.LitJson.ObjectMetadata>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2710601380_gshared (InternalEnumerator_1_t621922706 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2710601380(__this, method) ((  void (*) (InternalEnumerator_1_t621922706 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2710601380_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<ThirdParty.Json.LitJson.ObjectMetadata>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2500732761_gshared (InternalEnumerator_1_t621922706 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2500732761(__this, method) ((  bool (*) (InternalEnumerator_1_t621922706 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2500732761_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<ThirdParty.Json.LitJson.ObjectMetadata>::get_Current()
extern "C"  ObjectMetadata_t4058137740  InternalEnumerator_1_get_Current_m2772624814_gshared (InternalEnumerator_1_t621922706 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m2772624814(__this, method) ((  ObjectMetadata_t4058137740  (*) (InternalEnumerator_1_t621922706 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2772624814_gshared)(__this, method)
