﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AWSSDK_CognitoSync_Amazon_CognitoSync_AmazonCognit2140463921.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.CognitoSync.Model.DeleteDatasetRequest
struct  DeleteDatasetRequest_t3672322944  : public AmazonCognitoSyncRequest_t2140463921
{
public:
	// System.String Amazon.CognitoSync.Model.DeleteDatasetRequest::_datasetName
	String_t* ____datasetName_3;
	// System.String Amazon.CognitoSync.Model.DeleteDatasetRequest::_identityId
	String_t* ____identityId_4;
	// System.String Amazon.CognitoSync.Model.DeleteDatasetRequest::_identityPoolId
	String_t* ____identityPoolId_5;

public:
	inline static int32_t get_offset_of__datasetName_3() { return static_cast<int32_t>(offsetof(DeleteDatasetRequest_t3672322944, ____datasetName_3)); }
	inline String_t* get__datasetName_3() const { return ____datasetName_3; }
	inline String_t** get_address_of__datasetName_3() { return &____datasetName_3; }
	inline void set__datasetName_3(String_t* value)
	{
		____datasetName_3 = value;
		Il2CppCodeGenWriteBarrier(&____datasetName_3, value);
	}

	inline static int32_t get_offset_of__identityId_4() { return static_cast<int32_t>(offsetof(DeleteDatasetRequest_t3672322944, ____identityId_4)); }
	inline String_t* get__identityId_4() const { return ____identityId_4; }
	inline String_t** get_address_of__identityId_4() { return &____identityId_4; }
	inline void set__identityId_4(String_t* value)
	{
		____identityId_4 = value;
		Il2CppCodeGenWriteBarrier(&____identityId_4, value);
	}

	inline static int32_t get_offset_of__identityPoolId_5() { return static_cast<int32_t>(offsetof(DeleteDatasetRequest_t3672322944, ____identityPoolId_5)); }
	inline String_t* get__identityPoolId_5() const { return ____identityPoolId_5; }
	inline String_t** get_address_of__identityPoolId_5() { return &____identityPoolId_5; }
	inline void set__identityPoolId_5(String_t* value)
	{
		____identityPoolId_5 = value;
		Il2CppCodeGenWriteBarrier(&____identityPoolId_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
