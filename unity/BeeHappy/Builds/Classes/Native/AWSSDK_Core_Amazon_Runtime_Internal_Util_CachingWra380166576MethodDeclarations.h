﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.Util.CachingWrapperStream
struct CachingWrapperStream_t380166576;
// System.Collections.Generic.List`1<System.Byte>
struct List_1_t3052225568;
// System.IO.Stream
struct Stream_t3255436806;
// System.Byte[]
struct ByteU5BU5D_t3397334013;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_Stream3255436806.h"
#include "mscorlib_System_IO_SeekOrigin2475945306.h"

// System.Collections.Generic.List`1<System.Byte> Amazon.Runtime.Internal.Util.CachingWrapperStream::get_AllReadBytes()
extern "C"  List_1_t3052225568 * CachingWrapperStream_get_AllReadBytes_m2978402087 (CachingWrapperStream_t380166576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.CachingWrapperStream::set_AllReadBytes(System.Collections.Generic.List`1<System.Byte>)
extern "C"  void CachingWrapperStream_set_AllReadBytes_m4280545998 (CachingWrapperStream_t380166576 * __this, List_1_t3052225568 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.CachingWrapperStream::.ctor(System.IO.Stream,System.Int32)
extern "C"  void CachingWrapperStream__ctor_m1559865718 (CachingWrapperStream_t380166576 * __this, Stream_t3255436806 * ___baseStream0, int32_t ___cacheLimit1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Amazon.Runtime.Internal.Util.CachingWrapperStream::Read(System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t CachingWrapperStream_Read_m2964885891 (CachingWrapperStream_t380166576 * __this, ByteU5BU5D_t3397334013* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.Util.CachingWrapperStream::get_CanSeek()
extern "C"  bool CachingWrapperStream_get_CanSeek_m1534315221 (CachingWrapperStream_t380166576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Amazon.Runtime.Internal.Util.CachingWrapperStream::get_Position()
extern "C"  int64_t CachingWrapperStream_get_Position_m1407692765 (CachingWrapperStream_t380166576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.CachingWrapperStream::set_Position(System.Int64)
extern "C"  void CachingWrapperStream_set_Position_m1887578060 (CachingWrapperStream_t380166576 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Amazon.Runtime.Internal.Util.CachingWrapperStream::Seek(System.Int64,System.IO.SeekOrigin)
extern "C"  int64_t CachingWrapperStream_Seek_m2959619922 (CachingWrapperStream_t380166576 * __this, int64_t ___offset0, int32_t ___origin1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
