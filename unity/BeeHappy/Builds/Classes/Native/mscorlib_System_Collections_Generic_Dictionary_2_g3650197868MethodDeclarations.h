﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>
struct Dictionary_2_t3650197868;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t1902082073;
// System.Collections.Generic.IDictionary`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>
struct IDictionary_2_t1649281289;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t3641524600;
// System.Collections.ICollection
struct ICollection_t91669223;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>[]
struct KeyValuePair_2U5BU5D_t794880967;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>>
struct IEnumerator_1_t3178034213;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t259680273;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>
struct KeyCollection_t1838728343;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>
struct ValueCollection_t2353257711;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serialization228987430.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21407543090.h"
#include "mscorlib_System_Array3829468939.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_ObjectMetadata4058137740.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En675255274.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::.ctor()
extern "C"  void Dictionary_2__ctor_m3233598835_gshared (Dictionary_2_t3650197868 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m3233598835(__this, method) ((  void (*) (Dictionary_2_t3650197868 *, const MethodInfo*))Dictionary_2__ctor_m3233598835_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m3281835442_gshared (Dictionary_2_t3650197868 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m3281835442(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t3650197868 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m3281835442_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C"  void Dictionary_2__ctor_m2912702289_gshared (Dictionary_2_t3650197868 * __this, Il2CppObject* ___dictionary0, const MethodInfo* method);
#define Dictionary_2__ctor_m2912702289(__this, ___dictionary0, method) ((  void (*) (Dictionary_2_t3650197868 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2912702289_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m381405342_gshared (Dictionary_2_t3650197868 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m381405342(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t3650197868 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m381405342_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m3549759486_gshared (Dictionary_2_t3650197868 * __this, Il2CppObject* ___dictionary0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m3549759486(__this, ___dictionary0, ___comparer1, method) ((  void (*) (Dictionary_2_t3650197868 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m3549759486_gshared)(__this, ___dictionary0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m4257903564_gshared (Dictionary_2_t3650197868 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m4257903564(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t3650197868 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2__ctor_m4257903564_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m3202036243_gshared (Dictionary_2_t3650197868 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m3202036243(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t3650197868 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m3202036243_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Keys_m4265052729_gshared (Dictionary_2_t3650197868 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m4265052729(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3650197868 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m4265052729_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.IDictionary.get_Values()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Values_m101303681_gshared (Dictionary_2_t3650197868 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Values_m101303681(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3650197868 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m101303681_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m1981544551_gshared (Dictionary_2_t3650197868 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m1981544551(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t3650197868 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m1981544551_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m4192660816_gshared (Dictionary_2_t3650197868 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m4192660816(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3650197868 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m4192660816_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m2617096617_gshared (Dictionary_2_t3650197868 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m2617096617(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3650197868 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m2617096617_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool Dictionary_2_System_Collections_IDictionary_Contains_m2124661901_gshared (Dictionary_2_t3650197868 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m2124661901(__this, ___key0, method) ((  bool (*) (Dictionary_2_t3650197868 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m2124661901_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m681497428_gshared (Dictionary_2_t3650197868 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m681497428(__this, ___key0, method) ((  void (*) (Dictionary_2_t3650197868 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m681497428_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m708788927_gshared (Dictionary_2_t3650197868 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m708788927(__this, method) ((  bool (*) (Dictionary_2_t3650197868 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m708788927_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m4234546227_gshared (Dictionary_2_t3650197868 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m4234546227(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3650197868 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m4234546227_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2009335361_gshared (Dictionary_2_t3650197868 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2009335361(__this, method) ((  bool (*) (Dictionary_2_t3650197868 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2009335361_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m2588295422_gshared (Dictionary_2_t3650197868 * __this, KeyValuePair_2_t1407543090  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m2588295422(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t3650197868 *, KeyValuePair_2_t1407543090 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m2588295422_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3052426710_gshared (Dictionary_2_t3650197868 * __this, KeyValuePair_2_t1407543090  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3052426710(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t3650197868 *, KeyValuePair_2_t1407543090 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3052426710_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m253684026_gshared (Dictionary_2_t3650197868 * __this, KeyValuePair_2U5BU5D_t794880967* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m253684026(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3650197868 *, KeyValuePair_2U5BU5D_t794880967*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m253684026_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1705510499_gshared (Dictionary_2_t3650197868 * __this, KeyValuePair_2_t1407543090  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1705510499(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t3650197868 *, KeyValuePair_2_t1407543090 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1705510499_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m4193012071_gshared (Dictionary_2_t3650197868 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m4193012071(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3650197868 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m4193012071_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m782973328_gshared (Dictionary_2_t3650197868 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m782973328(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3650197868 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m782973328_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3981952561_gshared (Dictionary_2_t3650197868 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3981952561(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t3650197868 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3981952561_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m51016422_gshared (Dictionary_2_t3650197868 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m51016422(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3650197868 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m51016422_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m677849015_gshared (Dictionary_2_t3650197868 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m677849015(__this, method) ((  int32_t (*) (Dictionary_2_t3650197868 *, const MethodInfo*))Dictionary_2_get_Count_m677849015_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::get_Item(TKey)
extern "C"  ObjectMetadata_t4058137740  Dictionary_2_get_Item_m177129922_gshared (Dictionary_2_t3650197868 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m177129922(__this, ___key0, method) ((  ObjectMetadata_t4058137740  (*) (Dictionary_2_t3650197868 *, Il2CppObject *, const MethodInfo*))Dictionary_2_get_Item_m177129922_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m2099637091_gshared (Dictionary_2_t3650197868 * __this, Il2CppObject * ___key0, ObjectMetadata_t4058137740  ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m2099637091(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3650197868 *, Il2CppObject *, ObjectMetadata_t4058137740 , const MethodInfo*))Dictionary_2_set_Item_m2099637091_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m699262123_gshared (Dictionary_2_t3650197868 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m699262123(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t3650197868 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m699262123_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m1198156970_gshared (Dictionary_2_t3650197868 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m1198156970(__this, ___size0, method) ((  void (*) (Dictionary_2_t3650197868 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m1198156970_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m4199752972_gshared (Dictionary_2_t3650197868 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m4199752972(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3650197868 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m4199752972_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t1407543090  Dictionary_2_make_pair_m697817906_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___key0, ObjectMetadata_t4058137740  ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m697817906(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t1407543090  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, ObjectMetadata_t4058137740 , const MethodInfo*))Dictionary_2_make_pair_m697817906_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::pick_key(TKey,TValue)
extern "C"  Il2CppObject * Dictionary_2_pick_key_m220157764_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___key0, ObjectMetadata_t4058137740  ___value1, const MethodInfo* method);
#define Dictionary_2_pick_key_m220157764(__this /* static, unused */, ___key0, ___value1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, ObjectMetadata_t4058137740 , const MethodInfo*))Dictionary_2_pick_key_m220157764_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::pick_value(TKey,TValue)
extern "C"  ObjectMetadata_t4058137740  Dictionary_2_pick_value_m2032715204_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___key0, ObjectMetadata_t4058137740  ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m2032715204(__this /* static, unused */, ___key0, ___value1, method) ((  ObjectMetadata_t4058137740  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, ObjectMetadata_t4058137740 , const MethodInfo*))Dictionary_2_pick_value_m2032715204_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m4232635359_gshared (Dictionary_2_t3650197868 * __this, KeyValuePair_2U5BU5D_t794880967* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m4232635359(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3650197868 *, KeyValuePair_2U5BU5D_t794880967*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m4232635359_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::Resize()
extern "C"  void Dictionary_2_Resize_m311089265_gshared (Dictionary_2_t3650197868 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m311089265(__this, method) ((  void (*) (Dictionary_2_t3650197868 *, const MethodInfo*))Dictionary_2_Resize_m311089265_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m2538412858_gshared (Dictionary_2_t3650197868 * __this, Il2CppObject * ___key0, ObjectMetadata_t4058137740  ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m2538412858(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3650197868 *, Il2CppObject *, ObjectMetadata_t4058137740 , const MethodInfo*))Dictionary_2_Add_m2538412858_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::Clear()
extern "C"  void Dictionary_2_Clear_m3313476698_gshared (Dictionary_2_t3650197868 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m3313476698(__this, method) ((  void (*) (Dictionary_2_t3650197868 *, const MethodInfo*))Dictionary_2_Clear_m3313476698_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m470296506_gshared (Dictionary_2_t3650197868 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m470296506(__this, ___key0, method) ((  bool (*) (Dictionary_2_t3650197868 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ContainsKey_m470296506_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m1858734074_gshared (Dictionary_2_t3650197868 * __this, ObjectMetadata_t4058137740  ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m1858734074(__this, ___value0, method) ((  bool (*) (Dictionary_2_t3650197868 *, ObjectMetadata_t4058137740 , const MethodInfo*))Dictionary_2_ContainsValue_m1858734074_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m1031057635_gshared (Dictionary_2_t3650197868 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m1031057635(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t3650197868 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2_GetObjectData_m1031057635_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m3865279091_gshared (Dictionary_2_t3650197868 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m3865279091(__this, ___sender0, method) ((  void (*) (Dictionary_2_t3650197868 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m3865279091_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m2384788990_gshared (Dictionary_2_t3650197868 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m2384788990(__this, ___key0, method) ((  bool (*) (Dictionary_2_t3650197868 *, Il2CppObject *, const MethodInfo*))Dictionary_2_Remove_m2384788990_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m1206163851_gshared (Dictionary_2_t3650197868 * __this, Il2CppObject * ___key0, ObjectMetadata_t4058137740 * ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m1206163851(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t3650197868 *, Il2CppObject *, ObjectMetadata_t4058137740 *, const MethodInfo*))Dictionary_2_TryGetValue_m1206163851_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::get_Keys()
extern "C"  KeyCollection_t1838728343 * Dictionary_2_get_Keys_m3769797026_gshared (Dictionary_2_t3650197868 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m3769797026(__this, method) ((  KeyCollection_t1838728343 * (*) (Dictionary_2_t3650197868 *, const MethodInfo*))Dictionary_2_get_Keys_m3769797026_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::get_Values()
extern "C"  ValueCollection_t2353257711 * Dictionary_2_get_Values_m3861249154_gshared (Dictionary_2_t3650197868 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m3861249154(__this, method) ((  ValueCollection_t2353257711 * (*) (Dictionary_2_t3650197868 *, const MethodInfo*))Dictionary_2_get_Values_m3861249154_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::ToTKey(System.Object)
extern "C"  Il2CppObject * Dictionary_2_ToTKey_m3951055777_gshared (Dictionary_2_t3650197868 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m3951055777(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t3650197868 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m3951055777_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::ToTValue(System.Object)
extern "C"  ObjectMetadata_t4058137740  Dictionary_2_ToTValue_m1298448449_gshared (Dictionary_2_t3650197868 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m1298448449(__this, ___value0, method) ((  ObjectMetadata_t4058137740  (*) (Dictionary_2_t3650197868 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m1298448449_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m428529059_gshared (Dictionary_2_t3650197868 * __this, KeyValuePair_2_t1407543090  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m428529059(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t3650197868 *, KeyValuePair_2_t1407543090 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m428529059_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::GetEnumerator()
extern "C"  Enumerator_t675255274  Dictionary_2_GetEnumerator_m1504302880_gshared (Dictionary_2_t3650197868 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m1504302880(__this, method) ((  Enumerator_t675255274  (*) (Dictionary_2_t3650197868 *, const MethodInfo*))Dictionary_2_GetEnumerator_m1504302880_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::<CopyTo>m__0(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Dictionary_2_U3CCopyToU3Em__0_m2259721177_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___key0, ObjectMetadata_t4058137740  ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m2259721177(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, ObjectMetadata_t4058137740 , const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m2259721177_gshared)(__this /* static, unused */, ___key0, ___value1, method)
