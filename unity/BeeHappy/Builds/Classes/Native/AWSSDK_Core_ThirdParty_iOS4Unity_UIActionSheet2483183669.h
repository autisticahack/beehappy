﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AWSSDK_Core_ThirdParty_iOS4Unity_UIView1452205135.h"
#include "mscorlib_System_IntPtr2504060609.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.iOS4Unity.UIActionSheet
struct  UIActionSheet_t2483183669  : public UIView_t1452205135
{
public:

public:
};

struct UIActionSheet_t2483183669_StaticFields
{
public:
	// System.IntPtr ThirdParty.iOS4Unity.UIActionSheet::_classHandle
	IntPtr_t ____classHandle_4;

public:
	inline static int32_t get_offset_of__classHandle_4() { return static_cast<int32_t>(offsetof(UIActionSheet_t2483183669_StaticFields, ____classHandle_4)); }
	inline IntPtr_t get__classHandle_4() const { return ____classHandle_4; }
	inline IntPtr_t* get_address_of__classHandle_4() { return &____classHandle_4; }
	inline void set__classHandle_4(IntPtr_t value)
	{
		____classHandle_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
