﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Data.Sqlite.SqliteParameter
struct SqliteParameter_t354437343;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"
#include "System_Data_System_Data_DbType3924915636.h"
#include "System_Data_System_Data_DataRowVersion3411859714.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteParameter354437343.h"
#include "System_Data_System_Data_ParameterDirection744727978.h"

// System.Void Mono.Data.Sqlite.SqliteParameter::.ctor()
extern "C"  void SqliteParameter__ctor_m1187617438 (SqliteParameter_t354437343 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteParameter::.ctor(System.String,System.Object)
extern "C"  void SqliteParameter__ctor_m1325680818 (SqliteParameter_t354437343 * __this, String_t* ___parameterName0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteParameter::.ctor(System.String,System.Data.DbType,System.Int32,System.String,System.Data.DataRowVersion)
extern "C"  void SqliteParameter__ctor_m3126855329 (SqliteParameter_t354437343 * __this, String_t* ___parameterName0, int32_t ___parameterType1, int32_t ___parameterSize2, String_t* ___sourceColumn3, int32_t ___rowVersion4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteParameter::.ctor(Mono.Data.Sqlite.SqliteParameter)
extern "C"  void SqliteParameter__ctor_m137838760 (SqliteParameter_t354437343 * __this, SqliteParameter_t354437343 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteParameter::.ctor(System.String,System.Data.DbType,System.Int32,System.Data.ParameterDirection,System.Boolean,System.Byte,System.Byte,System.String,System.Data.DataRowVersion,System.Object)
extern "C"  void SqliteParameter__ctor_m1945569417 (SqliteParameter_t354437343 * __this, String_t* ___parameterName0, int32_t ___parameterType1, int32_t ___parameterSize2, int32_t ___direction3, bool ___isNullable4, uint8_t ___precision5, uint8_t ___scale6, String_t* ___sourceColumn7, int32_t ___rowVersion8, Il2CppObject * ___value9, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Data.Sqlite.SqliteParameter::get_IsNullable()
extern "C"  bool SqliteParameter_get_IsNullable_m3655475668 (SqliteParameter_t354437343 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteParameter::set_IsNullable(System.Boolean)
extern "C"  void SqliteParameter_set_IsNullable_m1285578835 (SqliteParameter_t354437343 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.DbType Mono.Data.Sqlite.SqliteParameter::get_DbType()
extern "C"  int32_t SqliteParameter_get_DbType_m2832794153 (SqliteParameter_t354437343 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteParameter::set_DbType(System.Data.DbType)
extern "C"  void SqliteParameter_set_DbType_m1891256162 (SqliteParameter_t354437343 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.ParameterDirection Mono.Data.Sqlite.SqliteParameter::get_Direction()
extern "C"  int32_t SqliteParameter_get_Direction_m171629838 (SqliteParameter_t354437343 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteParameter::set_Direction(System.Data.ParameterDirection)
extern "C"  void SqliteParameter_set_Direction_m2653343995 (SqliteParameter_t354437343 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Data.Sqlite.SqliteParameter::get_ParameterName()
extern "C"  String_t* SqliteParameter_get_ParameterName_m112121022 (SqliteParameter_t354437343 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteParameter::set_ParameterName(System.String)
extern "C"  void SqliteParameter_set_ParameterName_m2591781135 (SqliteParameter_t354437343 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteParameter::set_Size(System.Int32)
extern "C"  void SqliteParameter_set_Size_m2110998811 (SqliteParameter_t354437343 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Data.Sqlite.SqliteParameter::get_SourceColumn()
extern "C"  String_t* SqliteParameter_get_SourceColumn_m4112080529 (SqliteParameter_t354437343 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteParameter::set_SourceColumn(System.String)
extern "C"  void SqliteParameter_set_SourceColumn_m3358420616 (SqliteParameter_t354437343 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteParameter::set_SourceColumnNullMapping(System.Boolean)
extern "C"  void SqliteParameter_set_SourceColumnNullMapping_m2417781524 (SqliteParameter_t354437343 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.DataRowVersion Mono.Data.Sqlite.SqliteParameter::get_SourceVersion()
extern "C"  int32_t SqliteParameter_get_SourceVersion_m2636744932 (SqliteParameter_t354437343 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteParameter::set_SourceVersion(System.Data.DataRowVersion)
extern "C"  void SqliteParameter_set_SourceVersion_m1711912513 (SqliteParameter_t354437343 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Mono.Data.Sqlite.SqliteParameter::get_Value()
extern "C"  Il2CppObject * SqliteParameter_get_Value_m2370469425 (SqliteParameter_t354437343 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteParameter::set_Value(System.Object)
extern "C"  void SqliteParameter_set_Value_m2595801592 (SqliteParameter_t354437343 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Mono.Data.Sqlite.SqliteParameter::Clone()
extern "C"  Il2CppObject * SqliteParameter_Clone_m1278415206 (SqliteParameter_t354437343 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
