﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.IList`1<ThirdParty.Json.LitJson.JsonData>
struct IList_1_t509225357;
// System.Collections.Generic.IDictionary`2<System.String,ThirdParty.Json.LitJson.JsonData>
struct IDictionary_2_t4177114735;
// System.String
struct String_t;
// System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<System.String,ThirdParty.Json.LitJson.JsonData>>
struct IList_1_t181349841;

#include "mscorlib_System_Object2689449295.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_JsonType808352724.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.Json.LitJson.JsonData
struct  JsonData_t4263252052  : public Il2CppObject
{
public:
	// System.Collections.Generic.IList`1<ThirdParty.Json.LitJson.JsonData> ThirdParty.Json.LitJson.JsonData::inst_array
	Il2CppObject* ___inst_array_0;
	// System.Boolean ThirdParty.Json.LitJson.JsonData::inst_boolean
	bool ___inst_boolean_1;
	// System.Double ThirdParty.Json.LitJson.JsonData::inst_double
	double ___inst_double_2;
	// System.UInt64 ThirdParty.Json.LitJson.JsonData::inst_number
	uint64_t ___inst_number_3;
	// System.Collections.Generic.IDictionary`2<System.String,ThirdParty.Json.LitJson.JsonData> ThirdParty.Json.LitJson.JsonData::inst_object
	Il2CppObject* ___inst_object_4;
	// System.String ThirdParty.Json.LitJson.JsonData::inst_string
	String_t* ___inst_string_5;
	// System.String ThirdParty.Json.LitJson.JsonData::json
	String_t* ___json_6;
	// ThirdParty.Json.LitJson.JsonType ThirdParty.Json.LitJson.JsonData::type
	int32_t ___type_7;
	// System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<System.String,ThirdParty.Json.LitJson.JsonData>> ThirdParty.Json.LitJson.JsonData::object_list
	Il2CppObject* ___object_list_8;

public:
	inline static int32_t get_offset_of_inst_array_0() { return static_cast<int32_t>(offsetof(JsonData_t4263252052, ___inst_array_0)); }
	inline Il2CppObject* get_inst_array_0() const { return ___inst_array_0; }
	inline Il2CppObject** get_address_of_inst_array_0() { return &___inst_array_0; }
	inline void set_inst_array_0(Il2CppObject* value)
	{
		___inst_array_0 = value;
		Il2CppCodeGenWriteBarrier(&___inst_array_0, value);
	}

	inline static int32_t get_offset_of_inst_boolean_1() { return static_cast<int32_t>(offsetof(JsonData_t4263252052, ___inst_boolean_1)); }
	inline bool get_inst_boolean_1() const { return ___inst_boolean_1; }
	inline bool* get_address_of_inst_boolean_1() { return &___inst_boolean_1; }
	inline void set_inst_boolean_1(bool value)
	{
		___inst_boolean_1 = value;
	}

	inline static int32_t get_offset_of_inst_double_2() { return static_cast<int32_t>(offsetof(JsonData_t4263252052, ___inst_double_2)); }
	inline double get_inst_double_2() const { return ___inst_double_2; }
	inline double* get_address_of_inst_double_2() { return &___inst_double_2; }
	inline void set_inst_double_2(double value)
	{
		___inst_double_2 = value;
	}

	inline static int32_t get_offset_of_inst_number_3() { return static_cast<int32_t>(offsetof(JsonData_t4263252052, ___inst_number_3)); }
	inline uint64_t get_inst_number_3() const { return ___inst_number_3; }
	inline uint64_t* get_address_of_inst_number_3() { return &___inst_number_3; }
	inline void set_inst_number_3(uint64_t value)
	{
		___inst_number_3 = value;
	}

	inline static int32_t get_offset_of_inst_object_4() { return static_cast<int32_t>(offsetof(JsonData_t4263252052, ___inst_object_4)); }
	inline Il2CppObject* get_inst_object_4() const { return ___inst_object_4; }
	inline Il2CppObject** get_address_of_inst_object_4() { return &___inst_object_4; }
	inline void set_inst_object_4(Il2CppObject* value)
	{
		___inst_object_4 = value;
		Il2CppCodeGenWriteBarrier(&___inst_object_4, value);
	}

	inline static int32_t get_offset_of_inst_string_5() { return static_cast<int32_t>(offsetof(JsonData_t4263252052, ___inst_string_5)); }
	inline String_t* get_inst_string_5() const { return ___inst_string_5; }
	inline String_t** get_address_of_inst_string_5() { return &___inst_string_5; }
	inline void set_inst_string_5(String_t* value)
	{
		___inst_string_5 = value;
		Il2CppCodeGenWriteBarrier(&___inst_string_5, value);
	}

	inline static int32_t get_offset_of_json_6() { return static_cast<int32_t>(offsetof(JsonData_t4263252052, ___json_6)); }
	inline String_t* get_json_6() const { return ___json_6; }
	inline String_t** get_address_of_json_6() { return &___json_6; }
	inline void set_json_6(String_t* value)
	{
		___json_6 = value;
		Il2CppCodeGenWriteBarrier(&___json_6, value);
	}

	inline static int32_t get_offset_of_type_7() { return static_cast<int32_t>(offsetof(JsonData_t4263252052, ___type_7)); }
	inline int32_t get_type_7() const { return ___type_7; }
	inline int32_t* get_address_of_type_7() { return &___type_7; }
	inline void set_type_7(int32_t value)
	{
		___type_7 = value;
	}

	inline static int32_t get_offset_of_object_list_8() { return static_cast<int32_t>(offsetof(JsonData_t4263252052, ___object_list_8)); }
	inline Il2CppObject* get_object_list_8() const { return ___object_list_8; }
	inline Il2CppObject** get_address_of_object_list_8() { return &___object_list_8; }
	inline void set_object_list_8(Il2CppObject* value)
	{
		___object_list_8 = value;
		Il2CppCodeGenWriteBarrier(&___object_list_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
