﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// AccelBrain.BrainBeat
struct BrainBeat_t95439410;

#include "AssemblyU2DCSharp_AccelBrain_BeatController2386125758.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AccelBrain.BinauralBeatController
struct  BinauralBeatController_t3677369972  : public BeatController_t2386125758
{
public:
	// AccelBrain.BrainBeat AccelBrain.BinauralBeatController::<_brainBeat>k__BackingField
	BrainBeat_t95439410 * ___U3C_brainBeatU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3C_brainBeatU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(BinauralBeatController_t3677369972, ___U3C_brainBeatU3Ek__BackingField_6)); }
	inline BrainBeat_t95439410 * get_U3C_brainBeatU3Ek__BackingField_6() const { return ___U3C_brainBeatU3Ek__BackingField_6; }
	inline BrainBeat_t95439410 ** get_address_of_U3C_brainBeatU3Ek__BackingField_6() { return &___U3C_brainBeatU3Ek__BackingField_6; }
	inline void set_U3C_brainBeatU3Ek__BackingField_6(BrainBeat_t95439410 * value)
	{
		___U3C_brainBeatU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3C_brainBeatU3Ek__BackingField_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
