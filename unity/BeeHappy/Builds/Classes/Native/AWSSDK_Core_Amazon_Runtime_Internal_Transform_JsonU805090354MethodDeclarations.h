﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext/JsonPathStack
struct JsonPathStack_t805090354;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Int32 Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext/JsonPathStack::get_CurrentDepth()
extern "C"  int32_t JsonPathStack_get_CurrentDepth_m3304165628 (JsonPathStack_t805090354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext/JsonPathStack::get_CurrentPath()
extern "C"  String_t* JsonPathStack_get_CurrentPath_m2593444197 (JsonPathStack_t805090354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext/JsonPathStack::Push(System.String)
extern "C"  void JsonPathStack_Push_m1596914589 (JsonPathStack_t805090354 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext/JsonPathStack::Pop()
extern "C"  String_t* JsonPathStack_Pop_m2083545931 (JsonPathStack_t805090354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext/JsonPathStack::Peek()
extern "C"  String_t* JsonPathStack_Peek_m4128912967 (JsonPathStack_t805090354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext/JsonPathStack::get_Count()
extern "C"  int32_t JsonPathStack_get_Count_m707905417 (JsonPathStack_t805090354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext/JsonPathStack::.ctor()
extern "C"  void JsonPathStack__ctor_m1614333681 (JsonPathStack_t805090354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
