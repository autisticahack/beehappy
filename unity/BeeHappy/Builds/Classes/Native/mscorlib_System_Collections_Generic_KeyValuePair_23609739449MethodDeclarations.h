﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,Amazon.CognitoSync.Internal.CognitoCredentialsRetriever/CSRequestCache/CSRequest>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m1208728366(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3609739449 *, Type_t *, CSRequest_t3915036330 *, const MethodInfo*))KeyValuePair_2__ctor_m1640124561_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Type,Amazon.CognitoSync.Internal.CognitoCredentialsRetriever/CSRequestCache/CSRequest>::get_Key()
#define KeyValuePair_2_get_Key_m3147816344(__this, method) ((  Type_t * (*) (KeyValuePair_2_t3609739449 *, const MethodInfo*))KeyValuePair_2_get_Key_m2561166459_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,Amazon.CognitoSync.Internal.CognitoCredentialsRetriever/CSRequestCache/CSRequest>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m3222431691(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3609739449 *, Type_t *, const MethodInfo*))KeyValuePair_2_set_Key_m744486900_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Type,Amazon.CognitoSync.Internal.CognitoCredentialsRetriever/CSRequestCache/CSRequest>::get_Value()
#define KeyValuePair_2_get_Value_m989546288(__this, method) ((  CSRequest_t3915036330 * (*) (KeyValuePair_2_t3609739449 *, const MethodInfo*))KeyValuePair_2_get_Value_m499643803_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,Amazon.CognitoSync.Internal.CognitoCredentialsRetriever/CSRequestCache/CSRequest>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m2279992499(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3609739449 *, CSRequest_t3915036330 *, const MethodInfo*))KeyValuePair_2_set_Value_m1416408204_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Type,Amazon.CognitoSync.Internal.CognitoCredentialsRetriever/CSRequestCache/CSRequest>::ToString()
#define KeyValuePair_2_ToString_m863279841(__this, method) ((  String_t* (*) (KeyValuePair_2_t3609739449 *, const MethodInfo*))KeyValuePair_2_ToString_m2613351884_gshared)(__this, method)
