﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.SimpleAsyncResult
struct  SimpleAsyncResult_t4203640901  : public Il2CppObject
{
public:
	// System.Object Amazon.Runtime.Internal.SimpleAsyncResult::<AsyncState>k__BackingField
	Il2CppObject * ___U3CAsyncStateU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CAsyncStateU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(SimpleAsyncResult_t4203640901, ___U3CAsyncStateU3Ek__BackingField_0)); }
	inline Il2CppObject * get_U3CAsyncStateU3Ek__BackingField_0() const { return ___U3CAsyncStateU3Ek__BackingField_0; }
	inline Il2CppObject ** get_address_of_U3CAsyncStateU3Ek__BackingField_0() { return &___U3CAsyncStateU3Ek__BackingField_0; }
	inline void set_U3CAsyncStateU3Ek__BackingField_0(Il2CppObject * value)
	{
		___U3CAsyncStateU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CAsyncStateU3Ek__BackingField_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
