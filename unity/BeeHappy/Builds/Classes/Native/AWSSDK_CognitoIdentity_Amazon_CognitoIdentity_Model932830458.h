﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;

#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Amazo492659996.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.CognitoIdentity.Model.GetCredentialsForIdentityRequest
struct  GetCredentialsForIdentityRequest_t932830458  : public AmazonCognitoIdentityRequest_t492659996
{
public:
	// System.String Amazon.CognitoIdentity.Model.GetCredentialsForIdentityRequest::_customRoleArn
	String_t* ____customRoleArn_3;
	// System.String Amazon.CognitoIdentity.Model.GetCredentialsForIdentityRequest::_identityId
	String_t* ____identityId_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> Amazon.CognitoIdentity.Model.GetCredentialsForIdentityRequest::_logins
	Dictionary_2_t3943999495 * ____logins_5;

public:
	inline static int32_t get_offset_of__customRoleArn_3() { return static_cast<int32_t>(offsetof(GetCredentialsForIdentityRequest_t932830458, ____customRoleArn_3)); }
	inline String_t* get__customRoleArn_3() const { return ____customRoleArn_3; }
	inline String_t** get_address_of__customRoleArn_3() { return &____customRoleArn_3; }
	inline void set__customRoleArn_3(String_t* value)
	{
		____customRoleArn_3 = value;
		Il2CppCodeGenWriteBarrier(&____customRoleArn_3, value);
	}

	inline static int32_t get_offset_of__identityId_4() { return static_cast<int32_t>(offsetof(GetCredentialsForIdentityRequest_t932830458, ____identityId_4)); }
	inline String_t* get__identityId_4() const { return ____identityId_4; }
	inline String_t** get_address_of__identityId_4() { return &____identityId_4; }
	inline void set__identityId_4(String_t* value)
	{
		____identityId_4 = value;
		Il2CppCodeGenWriteBarrier(&____identityId_4, value);
	}

	inline static int32_t get_offset_of__logins_5() { return static_cast<int32_t>(offsetof(GetCredentialsForIdentityRequest_t932830458, ____logins_5)); }
	inline Dictionary_2_t3943999495 * get__logins_5() const { return ____logins_5; }
	inline Dictionary_2_t3943999495 ** get_address_of__logins_5() { return &____logins_5; }
	inline void set__logins_5(Dictionary_2_t3943999495 * value)
	{
		____logins_5 = value;
		Il2CppCodeGenWriteBarrier(&____logins_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
