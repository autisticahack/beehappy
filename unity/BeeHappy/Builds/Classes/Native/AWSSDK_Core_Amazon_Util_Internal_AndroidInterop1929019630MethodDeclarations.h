﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t3614634134;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"

// System.Object Amazon.Util.Internal.AndroidInterop::GetJavaObjectStatically(System.String,System.String)
extern "C"  Il2CppObject * AndroidInterop_GetJavaObjectStatically_m2727320126 (Il2CppObject * __this /* static, unused */, String_t* ___className0, String_t* ___methodName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Amazon.Util.Internal.AndroidInterop::CallMethod(System.Object,System.String,System.Object[])
extern "C"  Il2CppObject * AndroidInterop_CallMethod_m9597468 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___androidJavaObject0, String_t* ___methodName1, ObjectU5BU5D_t3614634134* ___parameters2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Amazon.Util.Internal.AndroidInterop::GetStaticJavaField(System.String,System.String)
extern "C"  Il2CppObject * AndroidInterop_GetStaticJavaField_m61817757 (Il2CppObject * __this /* static, unused */, String_t* ___className0, String_t* ___methodName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Amazon.Util.Internal.AndroidInterop::GetAndroidContext()
extern "C"  Il2CppObject * AndroidInterop_GetAndroidContext_m3377409683 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
