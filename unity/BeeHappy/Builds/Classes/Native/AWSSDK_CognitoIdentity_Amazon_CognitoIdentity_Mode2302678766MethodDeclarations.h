﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.CognitoIdentity.Model.GetCredentialsForIdentityResponse
struct GetCredentialsForIdentityResponse_t2302678766;
// Amazon.CognitoIdentity.Model.Credentials
struct Credentials_t792472136;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Model792472136.h"
#include "mscorlib_System_String2029220233.h"

// Amazon.CognitoIdentity.Model.Credentials Amazon.CognitoIdentity.Model.GetCredentialsForIdentityResponse::get_Credentials()
extern "C"  Credentials_t792472136 * GetCredentialsForIdentityResponse_get_Credentials_m2722355219 (GetCredentialsForIdentityResponse_t2302678766 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoIdentity.Model.GetCredentialsForIdentityResponse::set_Credentials(Amazon.CognitoIdentity.Model.Credentials)
extern "C"  void GetCredentialsForIdentityResponse_set_Credentials_m823810448 (GetCredentialsForIdentityResponse_t2302678766 * __this, Credentials_t792472136 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.CognitoIdentity.Model.GetCredentialsForIdentityResponse::get_IdentityId()
extern "C"  String_t* GetCredentialsForIdentityResponse_get_IdentityId_m2716953226 (GetCredentialsForIdentityResponse_t2302678766 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoIdentity.Model.GetCredentialsForIdentityResponse::set_IdentityId(System.String)
extern "C"  void GetCredentialsForIdentityResponse_set_IdentityId_m2807465299 (GetCredentialsForIdentityResponse_t2302678766 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoIdentity.Model.GetCredentialsForIdentityResponse::.ctor()
extern "C"  void GetCredentialsForIdentityResponse__ctor_m2472739811 (GetCredentialsForIdentityResponse_t2302678766 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
