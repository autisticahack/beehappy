﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ThirdParty.iOS4Unity.UIUserNotificationSettings
struct UIUserNotificationSettings_t4227938467;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr2504060609.h"

// System.Void ThirdParty.iOS4Unity.UIUserNotificationSettings::.cctor()
extern "C"  void UIUserNotificationSettings__cctor_m2982131012 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.iOS4Unity.UIUserNotificationSettings::.ctor(System.IntPtr)
extern "C"  void UIUserNotificationSettings__ctor_m3822994115 (UIUserNotificationSettings_t4227938467 * __this, IntPtr_t ___handle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
