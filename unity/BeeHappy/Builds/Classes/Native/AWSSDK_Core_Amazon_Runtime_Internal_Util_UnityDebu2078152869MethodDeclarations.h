﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.Util.UnityDebugTraceListener
struct UnityDebugTraceListener_t2078152869;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Amazon.Runtime.Internal.Util.UnityDebugTraceListener::.ctor(System.String)
extern "C"  void UnityDebugTraceListener__ctor_m1108948213 (UnityDebugTraceListener_t2078152869 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
