﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.Util.Internal.ITypeInfo[]
struct ITypeInfoU5BU5D_t39398432;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Util.Internal.TypeFactory
struct  TypeFactory_t695630734  : public Il2CppObject
{
public:

public:
};

struct TypeFactory_t695630734_StaticFields
{
public:
	// Amazon.Util.Internal.ITypeInfo[] Amazon.Util.Internal.TypeFactory::EmptyTypes
	ITypeInfoU5BU5D_t39398432* ___EmptyTypes_0;

public:
	inline static int32_t get_offset_of_EmptyTypes_0() { return static_cast<int32_t>(offsetof(TypeFactory_t695630734_StaticFields, ___EmptyTypes_0)); }
	inline ITypeInfoU5BU5D_t39398432* get_EmptyTypes_0() const { return ___EmptyTypes_0; }
	inline ITypeInfoU5BU5D_t39398432** get_address_of_EmptyTypes_0() { return &___EmptyTypes_0; }
	inline void set_EmptyTypes_0(ITypeInfoU5BU5D_t39398432* value)
	{
		___EmptyTypes_0 = value;
		Il2CppCodeGenWriteBarrier(&___EmptyTypes_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
