﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2047162844MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,ThirdParty.Json.LitJson.ArrayMetadata>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m2306838344(__this, ___dictionary0, method) ((  void (*) (Enumerator_t97493317 *, Dictionary_2_t3072435911 *, const MethodInfo*))Enumerator__ctor_m1167548803_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m424085671(__this, method) ((  Il2CppObject * (*) (Enumerator_t97493317 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2209161860_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m640018379(__this, method) ((  void (*) (Enumerator_t97493317 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2326843624_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Type,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2375397506(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t97493317 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2138280373_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1913856481(__this, method) ((  Il2CppObject * (*) (Enumerator_t97493317 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1315557434_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1580772369(__this, method) ((  Il2CppObject * (*) (Enumerator_t97493317 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m4200658056_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Type,ThirdParty.Json.LitJson.ArrayMetadata>::MoveNext()
#define Enumerator_MoveNext_m3638049375(__this, method) ((  bool (*) (Enumerator_t97493317 *, const MethodInfo*))Enumerator_MoveNext_m2137572552_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Type,ThirdParty.Json.LitJson.ArrayMetadata>::get_Current()
#define Enumerator_get_Current_m1088731863(__this, method) ((  KeyValuePair_2_t829781133  (*) (Enumerator_t97493317 *, const MethodInfo*))Enumerator_get_Current_m923671264_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Type,ThirdParty.Json.LitJson.ArrayMetadata>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m1147065328(__this, method) ((  Type_t * (*) (Enumerator_t97493317 *, const MethodInfo*))Enumerator_get_CurrentKey_m3038333311_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Type,ThirdParty.Json.LitJson.ArrayMetadata>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m3278460656(__this, method) ((  ArrayMetadata_t1135078014  (*) (Enumerator_t97493317 *, const MethodInfo*))Enumerator_get_CurrentValue_m2382161119_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,ThirdParty.Json.LitJson.ArrayMetadata>::Reset()
#define Enumerator_Reset_m2836276294(__this, method) ((  void (*) (Enumerator_t97493317 *, const MethodInfo*))Enumerator_Reset_m2586713469_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,ThirdParty.Json.LitJson.ArrayMetadata>::VerifyState()
#define Enumerator_VerifyState_m3766037285(__this, method) ((  void (*) (Enumerator_t97493317 *, const MethodInfo*))Enumerator_VerifyState_m2203483644_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,ThirdParty.Json.LitJson.ArrayMetadata>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m2099004923(__this, method) ((  void (*) (Enumerator_t97493317 *, const MethodInfo*))Enumerator_VerifyCurrent_m3852554688_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,ThirdParty.Json.LitJson.ArrayMetadata>::Dispose()
#define Enumerator_Dispose_m3186961548(__this, method) ((  void (*) (Enumerator_t97493317 *, const MethodInfo*))Enumerator_Dispose_m4092052839_gshared)(__this, method)
