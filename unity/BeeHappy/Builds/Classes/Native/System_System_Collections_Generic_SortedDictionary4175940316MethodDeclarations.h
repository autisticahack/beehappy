﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_SortedDictionary2513450244MethodDeclarations.h"

// System.Void System.Collections.Generic.SortedDictionary`2/Node<System.String,System.String>::.ctor(TKey)
#define Node__ctor_m1955494788(__this, ___key0, method) ((  void (*) (Node_t4175940316 *, String_t*, const MethodInfo*))Node__ctor_m4091545924_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.SortedDictionary`2/Node<System.String,System.String>::.ctor(TKey,TValue)
#define Node__ctor_m2213862561(__this, ___key0, ___value1, method) ((  void (*) (Node_t4175940316 *, String_t*, String_t*, const MethodInfo*))Node__ctor_m2270571809_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.SortedDictionary`2/Node<System.String,System.String>::SwapValue(System.Collections.Generic.RBTree/Node)
#define Node_SwapValue_m1901466429(__this, ___other0, method) ((  void (*) (Node_t4175940316 *, Node_t2499136326 *, const MethodInfo*))Node_SwapValue_m4043495997_gshared)(__this, ___other0, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.SortedDictionary`2/Node<System.String,System.String>::AsKV()
#define Node_AsKV_m698056280(__this, method) ((  KeyValuePair_2_t1701344717  (*) (Node_t4175940316 *, const MethodInfo*))Node_AsKV_m1661049880_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.SortedDictionary`2/Node<System.String,System.String>::AsDE()
#define Node_AsDE_m2228742633(__this, method) ((  DictionaryEntry_t3048875398  (*) (Node_t4175940316 *, const MethodInfo*))Node_AsDE_m1705531881_gshared)(__this, method)
