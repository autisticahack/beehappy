﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.CognitoSync.SyncManager.Dataset/<>c__DisplayClass71_0
struct U3CU3Ec__DisplayClass71_0_t1829637511;

#include "codegen/il2cpp-codegen.h"

// System.Void Amazon.CognitoSync.SyncManager.Dataset/<>c__DisplayClass71_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass71_0__ctor_m2861125033 (U3CU3Ec__DisplayClass71_0_t1829637511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.SyncManager.Dataset/<>c__DisplayClass71_0::<SynchronizeHelperAsync>b__0()
extern "C"  void U3CU3Ec__DisplayClass71_0_U3CSynchronizeHelperAsyncU3Eb__0_m4059576143 (U3CU3Ec__DisplayClass71_0_t1829637511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
