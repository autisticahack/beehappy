﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// Mono.Data.Sqlite.SQLiteTypeNames
struct SQLiteTypeNames_t2939892384;
struct SQLiteTypeNames_t2939892384_marshaled_pinvoke;
struct SQLiteTypeNames_t2939892384_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SQLiteTypeNames2939892384.h"
#include "mscorlib_System_String2029220233.h"
#include "System_Data_System_Data_DbType3924915636.h"

// System.Void Mono.Data.Sqlite.SQLiteTypeNames::.ctor(System.String,System.Data.DbType)
extern "C"  void SQLiteTypeNames__ctor_m2735608842 (SQLiteTypeNames_t2939892384 * __this, String_t* ___newtypeName0, int32_t ___newdataType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct SQLiteTypeNames_t2939892384;
struct SQLiteTypeNames_t2939892384_marshaled_pinvoke;

extern "C" void SQLiteTypeNames_t2939892384_marshal_pinvoke(const SQLiteTypeNames_t2939892384& unmarshaled, SQLiteTypeNames_t2939892384_marshaled_pinvoke& marshaled);
extern "C" void SQLiteTypeNames_t2939892384_marshal_pinvoke_back(const SQLiteTypeNames_t2939892384_marshaled_pinvoke& marshaled, SQLiteTypeNames_t2939892384& unmarshaled);
extern "C" void SQLiteTypeNames_t2939892384_marshal_pinvoke_cleanup(SQLiteTypeNames_t2939892384_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct SQLiteTypeNames_t2939892384;
struct SQLiteTypeNames_t2939892384_marshaled_com;

extern "C" void SQLiteTypeNames_t2939892384_marshal_com(const SQLiteTypeNames_t2939892384& unmarshaled, SQLiteTypeNames_t2939892384_marshaled_com& marshaled);
extern "C" void SQLiteTypeNames_t2939892384_marshal_com_back(const SQLiteTypeNames_t2939892384_marshaled_com& marshaled, SQLiteTypeNames_t2939892384& unmarshaled);
extern "C" void SQLiteTypeNames_t2939892384_marshal_com_cleanup(SQLiteTypeNames_t2939892384_marshaled_com& marshaled);
