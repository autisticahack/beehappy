﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>
struct Dictionary_2_t1224867506;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2544892208.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23277180024.h"
#include "AWSSDK_Core_Amazon_Util_Internal_PlatformServices_1632807378.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1253376471_gshared (Enumerator_t2544892208 * __this, Dictionary_2_t1224867506 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m1253376471(__this, ___dictionary0, method) ((  void (*) (Enumerator_t2544892208 *, Dictionary_2_t1224867506 *, const MethodInfo*))Enumerator__ctor_m1253376471_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1783562472_gshared (Enumerator_t2544892208 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1783562472(__this, method) ((  Il2CppObject * (*) (Enumerator_t2544892208 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1783562472_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2320340484_gshared (Enumerator_t2544892208 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2320340484(__this, method) ((  void (*) (Enumerator_t2544892208 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2320340484_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m283920581_gshared (Enumerator_t2544892208 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m283920581(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t2544892208 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m283920581_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2409199898_gshared (Enumerator_t2544892208 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2409199898(__this, method) ((  Il2CppObject * (*) (Enumerator_t2544892208 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2409199898_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m948336_gshared (Enumerator_t2544892208 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m948336(__this, method) ((  Il2CppObject * (*) (Enumerator_t2544892208 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m948336_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3850333932_gshared (Enumerator_t2544892208 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3850333932(__this, method) ((  bool (*) (Enumerator_t2544892208 *, const MethodInfo*))Enumerator_MoveNext_m3850333932_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::get_Current()
extern "C"  KeyValuePair_2_t3277180024  Enumerator_get_Current_m663494932_gshared (Enumerator_t2544892208 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m663494932(__this, method) ((  KeyValuePair_2_t3277180024  (*) (Enumerator_t2544892208 *, const MethodInfo*))Enumerator_get_Current_m663494932_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::get_CurrentKey()
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m4133598171_gshared (Enumerator_t2544892208 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m4133598171(__this, method) ((  Il2CppObject * (*) (Enumerator_t2544892208 *, const MethodInfo*))Enumerator_get_CurrentKey_m4133598171_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::get_CurrentValue()
extern "C"  int32_t Enumerator_get_CurrentValue_m17055867_gshared (Enumerator_t2544892208 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m17055867(__this, method) ((  int32_t (*) (Enumerator_t2544892208 *, const MethodInfo*))Enumerator_get_CurrentValue_m17055867_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::Reset()
extern "C"  void Enumerator_Reset_m1928144285_gshared (Enumerator_t2544892208 * __this, const MethodInfo* method);
#define Enumerator_Reset_m1928144285(__this, method) ((  void (*) (Enumerator_t2544892208 *, const MethodInfo*))Enumerator_Reset_m1928144285_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::VerifyState()
extern "C"  void Enumerator_VerifyState_m1152067668_gshared (Enumerator_t2544892208 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m1152067668(__this, method) ((  void (*) (Enumerator_t2544892208 *, const MethodInfo*))Enumerator_VerifyState_m1152067668_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m2033497076_gshared (Enumerator_t2544892208 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m2033497076(__this, method) ((  void (*) (Enumerator_t2544892208 *, const MethodInfo*))Enumerator_VerifyCurrent_m2033497076_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::Dispose()
extern "C"  void Enumerator_Dispose_m1362422387_gshared (Enumerator_t2544892208 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1362422387(__this, method) ((  void (*) (Enumerator_t2544892208 *, const MethodInfo*))Enumerator_Dispose_m1362422387_gshared)(__this, method)
