﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4068908131.h"
#include "mscorlib_System_Array3829468939.h"
#include "System_Core_System_Collections_Generic_HashSet_1_L3210155869.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Xml.XmlNodeType>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3190579885_gshared (InternalEnumerator_1_t4068908131 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m3190579885(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t4068908131 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3190579885_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Xml.XmlNodeType>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1563353929_gshared (InternalEnumerator_1_t4068908131 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1563353929(__this, method) ((  void (*) (InternalEnumerator_1_t4068908131 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1563353929_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Xml.XmlNodeType>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3336375437_gshared (InternalEnumerator_1_t4068908131 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3336375437(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t4068908131 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3336375437_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Xml.XmlNodeType>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2772400608_gshared (InternalEnumerator_1_t4068908131 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2772400608(__this, method) ((  void (*) (InternalEnumerator_1_t4068908131 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2772400608_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Xml.XmlNodeType>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m197927561_gshared (InternalEnumerator_1_t4068908131 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m197927561(__this, method) ((  bool (*) (InternalEnumerator_1_t4068908131 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m197927561_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Xml.XmlNodeType>>::get_Current()
extern "C"  Link_t3210155869  InternalEnumerator_1_get_Current_m2963355544_gshared (InternalEnumerator_1_t4068908131 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m2963355544(__this, method) ((  Link_t3210155869  (*) (InternalEnumerator_1_t4068908131 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2963355544_gshared)(__this, method)
