﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Net.HttpStatusCode>
struct DefaultComparer_t3520339802;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_HttpStatusCode1898409641.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Net.HttpStatusCode>::.ctor()
extern "C"  void DefaultComparer__ctor_m276028195_gshared (DefaultComparer_t3520339802 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m276028195(__this, method) ((  void (*) (DefaultComparer_t3520339802 *, const MethodInfo*))DefaultComparer__ctor_m276028195_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Net.HttpStatusCode>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m4040695800_gshared (DefaultComparer_t3520339802 * __this, int32_t ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m4040695800(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t3520339802 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m4040695800_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Net.HttpStatusCode>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m2131891392_gshared (DefaultComparer_t3520339802 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m2131891392(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t3520339802 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m2131891392_gshared)(__this, ___x0, ___y1, method)
