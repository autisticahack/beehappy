﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage/Statement
struct Statement_t3708524595;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t3614634134;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.String Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage/Statement::get_Query()
extern "C"  String_t* Statement_get_Query_m3864646107 (Statement_t3708524595 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage/Statement::set_Query(System.String)
extern "C"  void Statement_set_Query_m3771265822 (Statement_t3708524595 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage/Statement::get_Parameters()
extern "C"  ObjectU5BU5D_t3614634134* Statement_get_Parameters_m3611165193 (Statement_t3708524595 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage/Statement::set_Parameters(System.Object[])
extern "C"  void Statement_set_Parameters_m3644043758 (Statement_t3708524595 * __this, ObjectU5BU5D_t3614634134* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage/Statement::.ctor()
extern "C"  void Statement__ctor_m1226248021 (Statement_t3708524595 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
