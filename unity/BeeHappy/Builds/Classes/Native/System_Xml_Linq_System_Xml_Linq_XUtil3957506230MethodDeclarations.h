﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerable
struct IEnumerable_t2911409499;
// System.Xml.Linq.XNode
struct XNode_t2707504214;
// System.Xml.Linq.XObject
struct XObject_t3550811009;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "System_Xml_Linq_System_Xml_Linq_XObject3550811009.h"

// System.String System.Xml.Linq.XUtil::ToString(System.Object)
extern "C"  String_t* XUtil_ToString_m2221736954 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerable System.Xml.Linq.XUtil::ExpandArray(System.Object)
extern "C"  Il2CppObject * XUtil_ExpandArray_m1006964352 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Linq.XNode System.Xml.Linq.XUtil::ToNode(System.Object)
extern "C"  XNode_t2707504214 * XUtil_ToNode_m2489943695 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Xml.Linq.XUtil::GetDetachedObject(System.Xml.Linq.XObject)
extern "C"  Il2CppObject * XUtil_GetDetachedObject_m293754142 (Il2CppObject * __this /* static, unused */, XObject_t3550811009 * ___child0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Xml.Linq.XUtil::Clone(System.Object)
extern "C"  Il2CppObject * XUtil_Clone_m964299053 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
