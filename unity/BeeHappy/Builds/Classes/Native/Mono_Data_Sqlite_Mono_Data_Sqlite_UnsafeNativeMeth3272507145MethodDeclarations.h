﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Byte[]
struct ByteU5BU5D_t3397334013;
// Mono.Data.Sqlite.SQLiteCallback
struct SQLiteCallback_t2492596833;
// Mono.Data.Sqlite.SQLiteFinalCallback
struct SQLiteFinalCallback_t3453741687;
// System.String
struct String_t;
// Mono.Data.Sqlite.SQLiteCollation
struct SQLiteCollation_t4048482007;
// Mono.Data.Sqlite.SQLiteUpdateCallback
struct SQLiteUpdateCallback_t4116372060;
// Mono.Data.Sqlite.SQLiteCommitCallback
struct SQLiteCommitCallback_t2277160688;
// Mono.Data.Sqlite.SQLiteRollbackCallback
struct SQLiteRollbackCallback_t380854181;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SQLiteCallback2492596833.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SQLiteFinalCallb3453741687.h"
#include "mscorlib_System_String2029220233.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_TypeAffinity326266980.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SQLiteCollation4048482007.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SQLiteUpdateCall4116372060.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SQLiteCommitCall2277160688.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SQLiteRollbackCal380854181.h"

// System.Int32 Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_close(System.IntPtr)
extern "C"  int32_t UnsafeNativeMethods_sqlite3_close_m3030298796 (Il2CppObject * __this /* static, unused */, IntPtr_t ___db0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_create_function(System.IntPtr,System.Byte[],System.Int32,System.Int32,System.IntPtr,Mono.Data.Sqlite.SQLiteCallback,Mono.Data.Sqlite.SQLiteCallback,Mono.Data.Sqlite.SQLiteFinalCallback)
extern "C"  int32_t UnsafeNativeMethods_sqlite3_create_function_m4287670660 (Il2CppObject * __this /* static, unused */, IntPtr_t ___db0, ByteU5BU5D_t3397334013* ___strName1, int32_t ___nArgs2, int32_t ___nType3, IntPtr_t ___pvUser4, SQLiteCallback_t2492596833 * ___func5, SQLiteCallback_t2492596833 * ___fstep6, SQLiteFinalCallback_t3453741687 * ___ffinal7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_finalize(System.IntPtr)
extern "C"  int32_t UnsafeNativeMethods_sqlite3_finalize_m1244767440 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_open_v2(System.Byte[],System.IntPtr&,System.Int32,System.IntPtr)
extern "C"  int32_t UnsafeNativeMethods_sqlite3_open_v2_m4157424113 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___utf8Filename0, IntPtr_t* ___db1, int32_t ___flags2, IntPtr_t ___vfs3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_open(System.Byte[],System.IntPtr&)
extern "C"  int32_t UnsafeNativeMethods_sqlite3_open_m2179285609 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___utf8Filename0, IntPtr_t* ___db1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_open16(System.String,System.IntPtr&)
extern "C"  int32_t UnsafeNativeMethods_sqlite3_open16_m2055329851 (Il2CppObject * __this /* static, unused */, String_t* ___fileName0, IntPtr_t* ___db1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_reset(System.IntPtr)
extern "C"  int32_t UnsafeNativeMethods_sqlite3_reset_m3333216081 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_bind_parameter_name(System.IntPtr,System.Int32)
extern "C"  IntPtr_t UnsafeNativeMethods_sqlite3_bind_parameter_name_m3104156945 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_column_decltype(System.IntPtr,System.Int32)
extern "C"  IntPtr_t UnsafeNativeMethods_sqlite3_column_decltype_m3251779425 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_column_name(System.IntPtr,System.Int32)
extern "C"  IntPtr_t UnsafeNativeMethods_sqlite3_column_name_m2261276492 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_column_name16(System.IntPtr,System.Int32)
extern "C"  IntPtr_t UnsafeNativeMethods_sqlite3_column_name16_m2018534403 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_column_text(System.IntPtr,System.Int32)
extern "C"  IntPtr_t UnsafeNativeMethods_sqlite3_column_text_m3120183164 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_column_text16(System.IntPtr,System.Int32)
extern "C"  IntPtr_t UnsafeNativeMethods_sqlite3_column_text16_m2312166393 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_errmsg(System.IntPtr)
extern "C"  IntPtr_t UnsafeNativeMethods_sqlite3_errmsg_m2305957663 (Il2CppObject * __this /* static, unused */, IntPtr_t ___db0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_prepare(System.IntPtr,System.IntPtr,System.Int32,System.IntPtr&,System.IntPtr&)
extern "C"  int32_t UnsafeNativeMethods_sqlite3_prepare_m1218186556 (Il2CppObject * __this /* static, unused */, IntPtr_t ___db0, IntPtr_t ___pSql1, int32_t ___nBytes2, IntPtr_t* ___stmt3, IntPtr_t* ___ptrRemain4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_table_column_metadata(System.IntPtr,System.Byte[],System.Byte[],System.Byte[],System.IntPtr&,System.IntPtr&,System.Int32&,System.Int32&,System.Int32&)
extern "C"  int32_t UnsafeNativeMethods_sqlite3_table_column_metadata_m2488453981 (Il2CppObject * __this /* static, unused */, IntPtr_t ___db0, ByteU5BU5D_t3397334013* ___dbName1, ByteU5BU5D_t3397334013* ___tblName2, ByteU5BU5D_t3397334013* ___colName3, IntPtr_t* ___ptrDataType4, IntPtr_t* ___ptrCollSeq5, int32_t* ___notNull6, int32_t* ___primaryKey7, int32_t* ___autoInc8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_value_text(System.IntPtr)
extern "C"  IntPtr_t UnsafeNativeMethods_sqlite3_value_text_m3579868390 (Il2CppObject * __this /* static, unused */, IntPtr_t ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_value_text16(System.IntPtr)
extern "C"  IntPtr_t UnsafeNativeMethods_sqlite3_value_text16_m705034897 (Il2CppObject * __this /* static, unused */, IntPtr_t ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_libversion()
extern "C"  IntPtr_t UnsafeNativeMethods_sqlite3_libversion_m3951572640 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_changes(System.IntPtr)
extern "C"  int32_t UnsafeNativeMethods_sqlite3_changes_m112843625 (Il2CppObject * __this /* static, unused */, IntPtr_t ___db0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_busy_timeout(System.IntPtr,System.Int32)
extern "C"  int32_t UnsafeNativeMethods_sqlite3_busy_timeout_m2310231148 (Il2CppObject * __this /* static, unused */, IntPtr_t ___db0, int32_t ___ms1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_bind_blob(System.IntPtr,System.Int32,System.Byte[],System.Int32,System.IntPtr)
extern "C"  int32_t UnsafeNativeMethods_sqlite3_bind_blob_m1323464756 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, int32_t ___index1, ByteU5BU5D_t3397334013* ___value2, int32_t ___nSize3, IntPtr_t ___nTransient4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_bind_double(System.IntPtr,System.Int32,System.Double)
extern "C"  int32_t UnsafeNativeMethods_sqlite3_bind_double_m3897171950 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, int32_t ___index1, double ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_bind_int(System.IntPtr,System.Int32,System.Int32)
extern "C"  int32_t UnsafeNativeMethods_sqlite3_bind_int_m3133120477 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, int32_t ___index1, int32_t ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_bind_int64(System.IntPtr,System.Int32,System.Int64)
extern "C"  int32_t UnsafeNativeMethods_sqlite3_bind_int64_m2398245640 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, int32_t ___index1, int64_t ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_bind_null(System.IntPtr,System.Int32)
extern "C"  int32_t UnsafeNativeMethods_sqlite3_bind_null_m2494046826 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_bind_text(System.IntPtr,System.Int32,System.Byte[],System.Int32,System.IntPtr)
extern "C"  int32_t UnsafeNativeMethods_sqlite3_bind_text_m1895672192 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, int32_t ___index1, ByteU5BU5D_t3397334013* ___value2, int32_t ___nlen3, IntPtr_t ___pvReserved4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_bind_parameter_count(System.IntPtr)
extern "C"  int32_t UnsafeNativeMethods_sqlite3_bind_parameter_count_m2793498695 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_column_count(System.IntPtr)
extern "C"  int32_t UnsafeNativeMethods_sqlite3_column_count_m3272869672 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_step(System.IntPtr)
extern "C"  int32_t UnsafeNativeMethods_sqlite3_step_m4267759096 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_column_double(System.IntPtr,System.Int32)
extern "C"  double UnsafeNativeMethods_sqlite3_column_double_m1182650062 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_column_int(System.IntPtr,System.Int32)
extern "C"  int32_t UnsafeNativeMethods_sqlite3_column_int_m659878539 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_column_int64(System.IntPtr,System.Int32)
extern "C"  int64_t UnsafeNativeMethods_sqlite3_column_int64_m894008454 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_column_blob(System.IntPtr,System.Int32)
extern "C"  IntPtr_t UnsafeNativeMethods_sqlite3_column_blob_m1052355676 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_column_bytes(System.IntPtr,System.Int32)
extern "C"  int32_t UnsafeNativeMethods_sqlite3_column_bytes_m4210774437 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Data.Sqlite.TypeAffinity Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_column_type(System.IntPtr,System.Int32)
extern "C"  int32_t UnsafeNativeMethods_sqlite3_column_type_m1562505628 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_create_collation(System.IntPtr,System.Byte[],System.Int32,System.IntPtr,Mono.Data.Sqlite.SQLiteCollation)
extern "C"  int32_t UnsafeNativeMethods_sqlite3_create_collation_m633881950 (Il2CppObject * __this /* static, unused */, IntPtr_t ___db0, ByteU5BU5D_t3397334013* ___strName1, int32_t ___nType2, IntPtr_t ___pvUser3, SQLiteCollation_t4048482007 * ___func4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_value_blob(System.IntPtr)
extern "C"  IntPtr_t UnsafeNativeMethods_sqlite3_value_blob_m3113680698 (Il2CppObject * __this /* static, unused */, IntPtr_t ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_value_bytes(System.IntPtr)
extern "C"  int32_t UnsafeNativeMethods_sqlite3_value_bytes_m98908177 (Il2CppObject * __this /* static, unused */, IntPtr_t ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_value_double(System.IntPtr)
extern "C"  double UnsafeNativeMethods_sqlite3_value_double_m2513364420 (Il2CppObject * __this /* static, unused */, IntPtr_t ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_value_int64(System.IntPtr)
extern "C"  int64_t UnsafeNativeMethods_sqlite3_value_int64_m1862595366 (Il2CppObject * __this /* static, unused */, IntPtr_t ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Data.Sqlite.TypeAffinity Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_value_type(System.IntPtr)
extern "C"  int32_t UnsafeNativeMethods_sqlite3_value_type_m2355582624 (Il2CppObject * __this /* static, unused */, IntPtr_t ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_result_blob(System.IntPtr,System.Byte[],System.Int32,System.IntPtr)
extern "C"  void UnsafeNativeMethods_sqlite3_result_blob_m4006635299 (Il2CppObject * __this /* static, unused */, IntPtr_t ___context0, ByteU5BU5D_t3397334013* ___value1, int32_t ___nSize2, IntPtr_t ___pvReserved3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_result_double(System.IntPtr,System.Double)
extern "C"  void UnsafeNativeMethods_sqlite3_result_double_m260580087 (Il2CppObject * __this /* static, unused */, IntPtr_t ___context0, double ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_result_error(System.IntPtr,System.Byte[],System.Int32)
extern "C"  void UnsafeNativeMethods_sqlite3_result_error_m3673102910 (Il2CppObject * __this /* static, unused */, IntPtr_t ___context0, ByteU5BU5D_t3397334013* ___strErr1, int32_t ___nLen2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_result_int64(System.IntPtr,System.Int64)
extern "C"  void UnsafeNativeMethods_sqlite3_result_int64_m550489943 (Il2CppObject * __this /* static, unused */, IntPtr_t ___context0, int64_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_result_null(System.IntPtr)
extern "C"  void UnsafeNativeMethods_sqlite3_result_null_m3076341275 (Il2CppObject * __this /* static, unused */, IntPtr_t ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_result_text(System.IntPtr,System.Byte[],System.Int32,System.IntPtr)
extern "C"  void UnsafeNativeMethods_sqlite3_result_text_m1862606261 (Il2CppObject * __this /* static, unused */, IntPtr_t ___context0, ByteU5BU5D_t3397334013* ___value1, int32_t ___nLen2, IntPtr_t ___pvReserved3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_aggregate_context(System.IntPtr,System.Int32)
extern "C"  IntPtr_t UnsafeNativeMethods_sqlite3_aggregate_context_m3765581763 (Il2CppObject * __this /* static, unused */, IntPtr_t ___context0, int32_t ___nBytes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_bind_text16(System.IntPtr,System.Int32,System.String,System.Int32,System.IntPtr)
extern "C"  int32_t UnsafeNativeMethods_sqlite3_bind_text16_m2522643478 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, int32_t ___index1, String_t* ___value2, int32_t ___nlen3, IntPtr_t ___pvReserved4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_result_error16(System.IntPtr,System.String,System.Int32)
extern "C"  void UnsafeNativeMethods_sqlite3_result_error16_m541955734 (Il2CppObject * __this /* static, unused */, IntPtr_t ___context0, String_t* ___strName1, int32_t ___nLen2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_result_text16(System.IntPtr,System.String,System.Int32,System.IntPtr)
extern "C"  void UnsafeNativeMethods_sqlite3_result_text16_m2928632139 (Il2CppObject * __this /* static, unused */, IntPtr_t ___context0, String_t* ___strName1, int32_t ___nLen2, IntPtr_t ___pvReserved3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_key(System.IntPtr,System.Byte[],System.Int32)
extern "C"  int32_t UnsafeNativeMethods_sqlite3_key_m3635363825 (Il2CppObject * __this /* static, unused */, IntPtr_t ___db0, ByteU5BU5D_t3397334013* ___key1, int32_t ___keylen2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_update_hook(System.IntPtr,Mono.Data.Sqlite.SQLiteUpdateCallback,System.IntPtr)
extern "C"  IntPtr_t UnsafeNativeMethods_sqlite3_update_hook_m937360285 (Il2CppObject * __this /* static, unused */, IntPtr_t ___db0, SQLiteUpdateCallback_t4116372060 * ___func1, IntPtr_t ___pvUser2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_commit_hook(System.IntPtr,Mono.Data.Sqlite.SQLiteCommitCallback,System.IntPtr)
extern "C"  IntPtr_t UnsafeNativeMethods_sqlite3_commit_hook_m2460352797 (Il2CppObject * __this /* static, unused */, IntPtr_t ___db0, SQLiteCommitCallback_t2277160688 * ___func1, IntPtr_t ___pvUser2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_rollback_hook(System.IntPtr,Mono.Data.Sqlite.SQLiteRollbackCallback,System.IntPtr)
extern "C"  IntPtr_t UnsafeNativeMethods_sqlite3_rollback_hook_m2793346605 (Il2CppObject * __this /* static, unused */, IntPtr_t ___db0, SQLiteRollbackCallback_t380854181 * ___func1, IntPtr_t ___pvUser2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_next_stmt(System.IntPtr,System.IntPtr)
extern "C"  IntPtr_t UnsafeNativeMethods_sqlite3_next_stmt_m642114043 (Il2CppObject * __this /* static, unused */, IntPtr_t ___db0, IntPtr_t ___stmt1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_exec(System.IntPtr,System.Byte[],System.IntPtr,System.IntPtr,System.IntPtr&)
extern "C"  int32_t UnsafeNativeMethods_sqlite3_exec_m2999405264 (Il2CppObject * __this /* static, unused */, IntPtr_t ___db0, ByteU5BU5D_t3397334013* ___strSql1, IntPtr_t ___pvCallback2, IntPtr_t ___pvParam3, IntPtr_t* ___errMsg4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
