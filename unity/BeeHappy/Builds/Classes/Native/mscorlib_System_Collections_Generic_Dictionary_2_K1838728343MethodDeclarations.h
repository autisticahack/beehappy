﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>
struct KeyCollection_t1838728343;
// System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>
struct Dictionary_2_t3650197868;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object[]
struct ObjectU5BU5D_t3614634134;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2044734010.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m1439175414_gshared (KeyCollection_t1838728343 * __this, Dictionary_2_t3650197868 * ___dictionary0, const MethodInfo* method);
#define KeyCollection__ctor_m1439175414(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t1838728343 *, Dictionary_2_t3650197868 *, const MethodInfo*))KeyCollection__ctor_m1439175414_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m741303632_gshared (KeyCollection_t1838728343 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m741303632(__this, ___item0, method) ((  void (*) (KeyCollection_t1838728343 *, Il2CppObject *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m741303632_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m849613147_gshared (KeyCollection_t1838728343 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m849613147(__this, method) ((  void (*) (KeyCollection_t1838728343 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m849613147_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3874625808_gshared (KeyCollection_t1838728343 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3874625808(__this, ___item0, method) ((  bool (*) (KeyCollection_t1838728343 *, Il2CppObject *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3874625808_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1301535531_gshared (KeyCollection_t1838728343 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1301535531(__this, ___item0, method) ((  bool (*) (KeyCollection_t1838728343 *, Il2CppObject *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1301535531_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3508981317_gshared (KeyCollection_t1838728343 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3508981317(__this, method) ((  Il2CppObject* (*) (KeyCollection_t1838728343 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3508981317_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m2690697909_gshared (KeyCollection_t1838728343 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m2690697909(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1838728343 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m2690697909_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1851700276_gshared (KeyCollection_t1838728343 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1851700276(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1838728343 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1851700276_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3045537847_gshared (KeyCollection_t1838728343 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3045537847(__this, method) ((  bool (*) (KeyCollection_t1838728343 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3045537847_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1756756033_gshared (KeyCollection_t1838728343 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1756756033(__this, method) ((  bool (*) (KeyCollection_t1838728343 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1756756033_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m659350425_gshared (KeyCollection_t1838728343 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m659350425(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1838728343 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m659350425_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m847362259_gshared (KeyCollection_t1838728343 * __this, ObjectU5BU5D_t3614634134* ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_CopyTo_m847362259(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1838728343 *, ObjectU5BU5D_t3614634134*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m847362259_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::GetEnumerator()
extern "C"  Enumerator_t2044734010  KeyCollection_GetEnumerator_m3668909536_gshared (KeyCollection_t1838728343 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m3668909536(__this, method) ((  Enumerator_t2044734010  (*) (KeyCollection_t1838728343 *, const MethodInfo*))KeyCollection_GetEnumerator_m3668909536_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m3604538821_gshared (KeyCollection_t1838728343 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m3604538821(__this, method) ((  int32_t (*) (KeyCollection_t1838728343 *, const MethodInfo*))KeyCollection_get_Count_m3604538821_gshared)(__this, method)
