﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_EventArgs3289624707.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.StreamTransferProgressArgs
struct  StreamTransferProgressArgs_t2639638063  : public EventArgs_t3289624707
{
public:
	// System.Int64 Amazon.Runtime.StreamTransferProgressArgs::_incrementTransferred
	int64_t ____incrementTransferred_1;
	// System.Int64 Amazon.Runtime.StreamTransferProgressArgs::_total
	int64_t ____total_2;
	// System.Int64 Amazon.Runtime.StreamTransferProgressArgs::_transferred
	int64_t ____transferred_3;

public:
	inline static int32_t get_offset_of__incrementTransferred_1() { return static_cast<int32_t>(offsetof(StreamTransferProgressArgs_t2639638063, ____incrementTransferred_1)); }
	inline int64_t get__incrementTransferred_1() const { return ____incrementTransferred_1; }
	inline int64_t* get_address_of__incrementTransferred_1() { return &____incrementTransferred_1; }
	inline void set__incrementTransferred_1(int64_t value)
	{
		____incrementTransferred_1 = value;
	}

	inline static int32_t get_offset_of__total_2() { return static_cast<int32_t>(offsetof(StreamTransferProgressArgs_t2639638063, ____total_2)); }
	inline int64_t get__total_2() const { return ____total_2; }
	inline int64_t* get_address_of__total_2() { return &____total_2; }
	inline void set__total_2(int64_t value)
	{
		____total_2 = value;
	}

	inline static int32_t get_offset_of__transferred_3() { return static_cast<int32_t>(offsetof(StreamTransferProgressArgs_t2639638063, ____transferred_3)); }
	inline int64_t get__transferred_3() const { return ____transferred_3; }
	inline int64_t* get_address_of__transferred_3() { return &____transferred_3; }
	inline void set__transferred_3(int64_t value)
	{
		____transferred_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
