﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.IDictionary`2<System.Type,Amazon.Runtime.IExceptionHandler>
struct IDictionary_2_t1347456016;

#include "AWSSDK_Core_Amazon_Runtime_Internal_PipelineHandle1486422324.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.ErrorHandler
struct  ErrorHandler_t1573534074  : public PipelineHandler_t1486422324
{
public:
	// System.Collections.Generic.IDictionary`2<System.Type,Amazon.Runtime.IExceptionHandler> Amazon.Runtime.Internal.ErrorHandler::_exceptionHandlers
	Il2CppObject* ____exceptionHandlers_3;

public:
	inline static int32_t get_offset_of__exceptionHandlers_3() { return static_cast<int32_t>(offsetof(ErrorHandler_t1573534074, ____exceptionHandlers_3)); }
	inline Il2CppObject* get__exceptionHandlers_3() const { return ____exceptionHandlers_3; }
	inline Il2CppObject** get_address_of__exceptionHandlers_3() { return &____exceptionHandlers_3; }
	inline void set__exceptionHandlers_3(Il2CppObject* value)
	{
		____exceptionHandlers_3 = value;
		Il2CppCodeGenWriteBarrier(&____exceptionHandlers_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
