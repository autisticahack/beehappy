﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Collections_Generic_EqualityCompar1019216526.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<ThirdParty.Json.LitJson.JsonToken>
struct  DefaultComparer_t4067511416  : public EqualityComparer_1_t1019216526
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
