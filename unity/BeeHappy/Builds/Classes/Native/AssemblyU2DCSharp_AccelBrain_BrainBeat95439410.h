﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AccelBrain.BrainBeat
struct  BrainBeat_t95439410  : public Il2CppObject
{
public:
	// System.Double AccelBrain.BrainBeat::<LeftFrequency>k__BackingField
	double ___U3CLeftFrequencyU3Ek__BackingField_0;
	// System.Double AccelBrain.BrainBeat::<RightFrequency>k__BackingField
	double ___U3CRightFrequencyU3Ek__BackingField_1;
	// System.Double AccelBrain.BrainBeat::<Gain>k__BackingField
	double ___U3CGainU3Ek__BackingField_2;
	// System.Double AccelBrain.BrainBeat::_SampleRate
	double ____SampleRate_3;
	// System.Double AccelBrain.BrainBeat::LeftIncrement
	double ___LeftIncrement_4;
	// System.Double AccelBrain.BrainBeat::RightIncrement
	double ___RightIncrement_5;
	// System.Double AccelBrain.BrainBeat::LeftPhase
	double ___LeftPhase_6;
	// System.Double AccelBrain.BrainBeat::RightPhase
	double ___RightPhase_7;
	// System.Boolean AccelBrain.BrainBeat::_PlayFlag
	bool ____PlayFlag_8;

public:
	inline static int32_t get_offset_of_U3CLeftFrequencyU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(BrainBeat_t95439410, ___U3CLeftFrequencyU3Ek__BackingField_0)); }
	inline double get_U3CLeftFrequencyU3Ek__BackingField_0() const { return ___U3CLeftFrequencyU3Ek__BackingField_0; }
	inline double* get_address_of_U3CLeftFrequencyU3Ek__BackingField_0() { return &___U3CLeftFrequencyU3Ek__BackingField_0; }
	inline void set_U3CLeftFrequencyU3Ek__BackingField_0(double value)
	{
		___U3CLeftFrequencyU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CRightFrequencyU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(BrainBeat_t95439410, ___U3CRightFrequencyU3Ek__BackingField_1)); }
	inline double get_U3CRightFrequencyU3Ek__BackingField_1() const { return ___U3CRightFrequencyU3Ek__BackingField_1; }
	inline double* get_address_of_U3CRightFrequencyU3Ek__BackingField_1() { return &___U3CRightFrequencyU3Ek__BackingField_1; }
	inline void set_U3CRightFrequencyU3Ek__BackingField_1(double value)
	{
		___U3CRightFrequencyU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CGainU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(BrainBeat_t95439410, ___U3CGainU3Ek__BackingField_2)); }
	inline double get_U3CGainU3Ek__BackingField_2() const { return ___U3CGainU3Ek__BackingField_2; }
	inline double* get_address_of_U3CGainU3Ek__BackingField_2() { return &___U3CGainU3Ek__BackingField_2; }
	inline void set_U3CGainU3Ek__BackingField_2(double value)
	{
		___U3CGainU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of__SampleRate_3() { return static_cast<int32_t>(offsetof(BrainBeat_t95439410, ____SampleRate_3)); }
	inline double get__SampleRate_3() const { return ____SampleRate_3; }
	inline double* get_address_of__SampleRate_3() { return &____SampleRate_3; }
	inline void set__SampleRate_3(double value)
	{
		____SampleRate_3 = value;
	}

	inline static int32_t get_offset_of_LeftIncrement_4() { return static_cast<int32_t>(offsetof(BrainBeat_t95439410, ___LeftIncrement_4)); }
	inline double get_LeftIncrement_4() const { return ___LeftIncrement_4; }
	inline double* get_address_of_LeftIncrement_4() { return &___LeftIncrement_4; }
	inline void set_LeftIncrement_4(double value)
	{
		___LeftIncrement_4 = value;
	}

	inline static int32_t get_offset_of_RightIncrement_5() { return static_cast<int32_t>(offsetof(BrainBeat_t95439410, ___RightIncrement_5)); }
	inline double get_RightIncrement_5() const { return ___RightIncrement_5; }
	inline double* get_address_of_RightIncrement_5() { return &___RightIncrement_5; }
	inline void set_RightIncrement_5(double value)
	{
		___RightIncrement_5 = value;
	}

	inline static int32_t get_offset_of_LeftPhase_6() { return static_cast<int32_t>(offsetof(BrainBeat_t95439410, ___LeftPhase_6)); }
	inline double get_LeftPhase_6() const { return ___LeftPhase_6; }
	inline double* get_address_of_LeftPhase_6() { return &___LeftPhase_6; }
	inline void set_LeftPhase_6(double value)
	{
		___LeftPhase_6 = value;
	}

	inline static int32_t get_offset_of_RightPhase_7() { return static_cast<int32_t>(offsetof(BrainBeat_t95439410, ___RightPhase_7)); }
	inline double get_RightPhase_7() const { return ___RightPhase_7; }
	inline double* get_address_of_RightPhase_7() { return &___RightPhase_7; }
	inline void set_RightPhase_7(double value)
	{
		___RightPhase_7 = value;
	}

	inline static int32_t get_offset_of__PlayFlag_8() { return static_cast<int32_t>(offsetof(BrainBeat_t95439410, ____PlayFlag_8)); }
	inline bool get__PlayFlag_8() const { return ____PlayFlag_8; }
	inline bool* get_address_of__PlayFlag_8() { return &____PlayFlag_8; }
	inline void set__PlayFlag_8(bool value)
	{
		____PlayFlag_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
