﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<ThirdParty.Json.LitJson.ObjectMetadata>
struct DefaultComparer_t1385100605;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_ObjectMetadata4058137740.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<ThirdParty.Json.LitJson.ObjectMetadata>::.ctor()
extern "C"  void DefaultComparer__ctor_m1787190768_gshared (DefaultComparer_t1385100605 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m1787190768(__this, method) ((  void (*) (DefaultComparer_t1385100605 *, const MethodInfo*))DefaultComparer__ctor_m1787190768_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<ThirdParty.Json.LitJson.ObjectMetadata>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m1466299329_gshared (DefaultComparer_t1385100605 * __this, ObjectMetadata_t4058137740  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m1466299329(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t1385100605 *, ObjectMetadata_t4058137740 , const MethodInfo*))DefaultComparer_GetHashCode_m1466299329_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<ThirdParty.Json.LitJson.ObjectMetadata>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m3676481381_gshared (DefaultComparer_t1385100605 * __this, ObjectMetadata_t4058137740  ___x0, ObjectMetadata_t4058137740  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m3676481381(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t1385100605 *, ObjectMetadata_t4058137740 , ObjectMetadata_t4058137740 , const MethodInfo*))DefaultComparer_Equals_m3676481381_gshared)(__this, ___x0, ___y1, method)
