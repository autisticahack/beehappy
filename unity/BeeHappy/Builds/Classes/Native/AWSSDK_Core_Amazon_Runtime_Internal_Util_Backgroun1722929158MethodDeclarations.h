﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.Util.BackgroundInvoker
struct BackgroundInvoker_t1722929158;

#include "codegen/il2cpp-codegen.h"

// System.Void Amazon.Runtime.Internal.Util.BackgroundInvoker::.ctor()
extern "C"  void BackgroundInvoker__ctor_m2916510020 (BackgroundInvoker_t1722929158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
