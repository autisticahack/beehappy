﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ThirdParty.Json.LitJson.ObjectMetadata
struct ObjectMetadata_t4058137740;
struct ObjectMetadata_t4058137740_marshaled_pinvoke;
struct ObjectMetadata_t4058137740_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct ObjectMetadata_t4058137740;
struct ObjectMetadata_t4058137740_marshaled_pinvoke;

extern "C" void ObjectMetadata_t4058137740_marshal_pinvoke(const ObjectMetadata_t4058137740& unmarshaled, ObjectMetadata_t4058137740_marshaled_pinvoke& marshaled);
extern "C" void ObjectMetadata_t4058137740_marshal_pinvoke_back(const ObjectMetadata_t4058137740_marshaled_pinvoke& marshaled, ObjectMetadata_t4058137740& unmarshaled);
extern "C" void ObjectMetadata_t4058137740_marshal_pinvoke_cleanup(ObjectMetadata_t4058137740_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct ObjectMetadata_t4058137740;
struct ObjectMetadata_t4058137740_marshaled_com;

extern "C" void ObjectMetadata_t4058137740_marshal_com(const ObjectMetadata_t4058137740& unmarshaled, ObjectMetadata_t4058137740_marshaled_com& marshaled);
extern "C" void ObjectMetadata_t4058137740_marshal_com_back(const ObjectMetadata_t4058137740_marshaled_com& marshaled, ObjectMetadata_t4058137740& unmarshaled);
extern "C" void ObjectMetadata_t4058137740_marshal_com_cleanup(ObjectMetadata_t4058137740_marshaled_com& marshaled);
