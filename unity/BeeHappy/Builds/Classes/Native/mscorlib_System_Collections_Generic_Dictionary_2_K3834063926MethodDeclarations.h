﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Object>
struct Dictionary_2_t1144560488;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3834063926.h"
#include "AWSSDK_Core_Amazon_Runtime_Metric3273440202.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Amazon.Runtime.Metric,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2774311225_gshared (Enumerator_t3834063926 * __this, Dictionary_2_t1144560488 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m2774311225(__this, ___host0, method) ((  void (*) (Enumerator_t3834063926 *, Dictionary_2_t1144560488 *, const MethodInfo*))Enumerator__ctor_m2774311225_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Amazon.Runtime.Metric,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2082606486_gshared (Enumerator_t3834063926 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2082606486(__this, method) ((  Il2CppObject * (*) (Enumerator_t3834063926 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2082606486_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Amazon.Runtime.Metric,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2940477526_gshared (Enumerator_t3834063926 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2940477526(__this, method) ((  void (*) (Enumerator_t3834063926 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2940477526_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Amazon.Runtime.Metric,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m2168347345_gshared (Enumerator_t3834063926 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2168347345(__this, method) ((  void (*) (Enumerator_t3834063926 *, const MethodInfo*))Enumerator_Dispose_m2168347345_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Amazon.Runtime.Metric,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m403331010_gshared (Enumerator_t3834063926 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m403331010(__this, method) ((  bool (*) (Enumerator_t3834063926 *, const MethodInfo*))Enumerator_MoveNext_m403331010_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Amazon.Runtime.Metric,System.Object>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m2865873134_gshared (Enumerator_t3834063926 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2865873134(__this, method) ((  int32_t (*) (Enumerator_t3834063926 *, const MethodInfo*))Enumerator_get_Current_m2865873134_gshared)(__this, method)
