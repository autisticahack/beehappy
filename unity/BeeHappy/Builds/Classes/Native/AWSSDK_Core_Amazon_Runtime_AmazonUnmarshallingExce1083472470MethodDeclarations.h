﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.AmazonUnmarshallingException
struct AmazonUnmarshallingException_t1083472470;
// System.String
struct String_t;
// System.Exception
struct Exception_t1927440687;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Exception1927440687.h"
#include "mscorlib_System_Text_StringBuilder1221177846.h"
#include "mscorlib_System_Runtime_Serialization_Serialization228987430.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"

// System.Void Amazon.Runtime.AmazonUnmarshallingException::.ctor(System.String,System.String,System.Exception)
extern "C"  void AmazonUnmarshallingException__ctor_m808635855 (AmazonUnmarshallingException_t1083472470 * __this, String_t* ___requestId0, String_t* ___lastKnownLocation1, Exception_t1927440687 * ___innerException2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.AmazonUnmarshallingException::.ctor(System.String,System.String,System.String,System.Exception)
extern "C"  void AmazonUnmarshallingException__ctor_m443546877 (AmazonUnmarshallingException_t1083472470 * __this, String_t* ___requestId0, String_t* ___lastKnownLocation1, String_t* ___responseBody2, Exception_t1927440687 * ___innerException3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Runtime.AmazonUnmarshallingException::get_LastKnownLocation()
extern "C"  String_t* AmazonUnmarshallingException_get_LastKnownLocation_m1146323539 (AmazonUnmarshallingException_t1083472470 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.AmazonUnmarshallingException::set_LastKnownLocation(System.String)
extern "C"  void AmazonUnmarshallingException_set_LastKnownLocation_m4260333194 (AmazonUnmarshallingException_t1083472470 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Runtime.AmazonUnmarshallingException::get_ResponseBody()
extern "C"  String_t* AmazonUnmarshallingException_get_ResponseBody_m1023221690 (AmazonUnmarshallingException_t1083472470 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.AmazonUnmarshallingException::set_ResponseBody(System.String)
extern "C"  void AmazonUnmarshallingException_set_ResponseBody_m3897122823 (AmazonUnmarshallingException_t1083472470 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Runtime.AmazonUnmarshallingException::get_Message()
extern "C"  String_t* AmazonUnmarshallingException_get_Message_m1902316834 (AmazonUnmarshallingException_t1083472470 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.AmazonUnmarshallingException::AppendFormat(System.Text.StringBuilder,System.String,System.String)
extern "C"  void AmazonUnmarshallingException_AppendFormat_m2957983322 (Il2CppObject * __this /* static, unused */, StringBuilder_t1221177846 * ___sb0, String_t* ___format1, String_t* ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.AmazonUnmarshallingException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void AmazonUnmarshallingException__ctor_m3979634544 (AmazonUnmarshallingException_t1083472470 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.AmazonUnmarshallingException::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void AmazonUnmarshallingException_GetObjectData_m1738040735 (AmazonUnmarshallingException_t1083472470 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
