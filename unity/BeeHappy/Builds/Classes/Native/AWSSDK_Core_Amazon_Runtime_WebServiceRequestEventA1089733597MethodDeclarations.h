﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.WebServiceRequestEventArgs
struct WebServiceRequestEventArgs_t1089733597;
// System.Collections.Generic.IDictionary`2<System.String,System.String>
struct IDictionary_2_t1943082916;
// System.String
struct String_t;
// System.Uri
struct Uri_t19570940;
// Amazon.Runtime.AmazonWebServiceRequest
struct AmazonWebServiceRequest_t3384026212;
// Amazon.Runtime.Internal.IRequest
struct IRequest_t2400804350;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "System_System_Uri19570940.h"
#include "AWSSDK_Core_Amazon_Runtime_AmazonWebServiceRequest3384026212.h"

// System.Void Amazon.Runtime.WebServiceRequestEventArgs::.ctor()
extern "C"  void WebServiceRequestEventArgs__ctor_m2786000230 (WebServiceRequestEventArgs_t1089733597 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.WebServiceRequestEventArgs::set_Headers(System.Collections.Generic.IDictionary`2<System.String,System.String>)
extern "C"  void WebServiceRequestEventArgs_set_Headers_m2304817221 (WebServiceRequestEventArgs_t1089733597 * __this, Il2CppObject* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.WebServiceRequestEventArgs::set_Parameters(System.Collections.Generic.IDictionary`2<System.String,System.String>)
extern "C"  void WebServiceRequestEventArgs_set_Parameters_m2256127339 (WebServiceRequestEventArgs_t1089733597 * __this, Il2CppObject* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.WebServiceRequestEventArgs::set_ServiceName(System.String)
extern "C"  void WebServiceRequestEventArgs_set_ServiceName_m1957333995 (WebServiceRequestEventArgs_t1089733597 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.WebServiceRequestEventArgs::set_Endpoint(System.Uri)
extern "C"  void WebServiceRequestEventArgs_set_Endpoint_m3345307553 (WebServiceRequestEventArgs_t1089733597 * __this, Uri_t19570940 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.WebServiceRequestEventArgs::set_Request(Amazon.Runtime.AmazonWebServiceRequest)
extern "C"  void WebServiceRequestEventArgs_set_Request_m2709801182 (WebServiceRequestEventArgs_t1089733597 * __this, AmazonWebServiceRequest_t3384026212 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.WebServiceRequestEventArgs Amazon.Runtime.WebServiceRequestEventArgs::Create(Amazon.Runtime.Internal.IRequest)
extern "C"  WebServiceRequestEventArgs_t1089733597 * WebServiceRequestEventArgs_Create_m1643386731 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___request0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
