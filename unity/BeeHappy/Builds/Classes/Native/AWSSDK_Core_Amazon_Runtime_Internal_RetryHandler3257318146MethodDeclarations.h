﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.RetryHandler
struct RetryHandler_t3257318146;
// Amazon.Runtime.Internal.Util.ILogger
struct ILogger_t676853571;
// Amazon.Runtime.RetryPolicy
struct RetryPolicy_t1476739446;
// Amazon.Runtime.IExecutionContext
struct IExecutionContext_t2477130752;
// Amazon.Runtime.IAsyncExecutionContext
struct IAsyncExecutionContext_t3792344986;
// Amazon.Runtime.IRequestContext
struct IRequestContext_t1620878341;
// System.Exception
struct Exception_t1927440687;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_Runtime_RetryPolicy1476739446.h"
#include "mscorlib_System_Exception1927440687.h"

// Amazon.Runtime.Internal.Util.ILogger Amazon.Runtime.Internal.RetryHandler::get_Logger()
extern "C"  Il2CppObject * RetryHandler_get_Logger_m945909458 (RetryHandler_t3257318146 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.RetryHandler::set_Logger(Amazon.Runtime.Internal.Util.ILogger)
extern "C"  void RetryHandler_set_Logger_m2725472085 (RetryHandler_t3257318146 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.RetryPolicy Amazon.Runtime.Internal.RetryHandler::get_RetryPolicy()
extern "C"  RetryPolicy_t1476739446 * RetryHandler_get_RetryPolicy_m1812199732 (RetryHandler_t3257318146 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.RetryHandler::set_RetryPolicy(Amazon.Runtime.RetryPolicy)
extern "C"  void RetryHandler_set_RetryPolicy_m3696310371 (RetryHandler_t3257318146 * __this, RetryPolicy_t1476739446 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.RetryHandler::.ctor(Amazon.Runtime.RetryPolicy)
extern "C"  void RetryHandler__ctor_m2449516482 (RetryHandler_t3257318146 * __this, RetryPolicy_t1476739446 * ___retryPolicy0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.RetryHandler::InvokeSync(Amazon.Runtime.IExecutionContext)
extern "C"  void RetryHandler_InvokeSync_m2376179149 (RetryHandler_t3257318146 * __this, Il2CppObject * ___executionContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.RetryHandler::InvokeAsyncCallback(Amazon.Runtime.IAsyncExecutionContext)
extern "C"  void RetryHandler_InvokeAsyncCallback_m546122007 (RetryHandler_t3257318146 * __this, Il2CppObject * ___executionContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.RetryHandler::PrepareForRetry(Amazon.Runtime.IRequestContext)
extern "C"  void RetryHandler_PrepareForRetry_m358166095 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___requestContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.RetryHandler::LogForRetry(Amazon.Runtime.IRequestContext,System.Exception)
extern "C"  void RetryHandler_LogForRetry_m1038471046 (RetryHandler_t3257318146 * __this, Il2CppObject * ___requestContext0, Exception_t1927440687 * ___exception1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.RetryHandler::LogForError(Amazon.Runtime.IRequestContext,System.Exception)
extern "C"  void RetryHandler_LogForError_m2295239502 (RetryHandler_t3257318146 * __this, Il2CppObject * ___requestContext0, Exception_t1927440687 * ___exception1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
