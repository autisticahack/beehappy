﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ThirdParty.iOS4Unity.NSMutableDictionary
struct NSMutableDictionary_t171997941;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr2504060609.h"

// System.Void ThirdParty.iOS4Unity.NSMutableDictionary::.cctor()
extern "C"  void NSMutableDictionary__cctor_m4283149650 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.iOS4Unity.NSMutableDictionary::.ctor(System.IntPtr)
extern "C"  void NSMutableDictionary__ctor_m126864237 (NSMutableDictionary_t171997941 * __this, IntPtr_t ___handle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
