﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ThirdParty.Json.LitJson.JsonWriter
struct JsonWriter_t3014444111;

#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_Mars3045611074.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Transform.JsonMarshallerContext
struct  JsonMarshallerContext_t4063314926  : public MarshallerContext_t3045611074
{
public:
	// ThirdParty.Json.LitJson.JsonWriter Amazon.Runtime.Internal.Transform.JsonMarshallerContext::<Writer>k__BackingField
	JsonWriter_t3014444111 * ___U3CWriterU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CWriterU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(JsonMarshallerContext_t4063314926, ___U3CWriterU3Ek__BackingField_1)); }
	inline JsonWriter_t3014444111 * get_U3CWriterU3Ek__BackingField_1() const { return ___U3CWriterU3Ek__BackingField_1; }
	inline JsonWriter_t3014444111 ** get_address_of_U3CWriterU3Ek__BackingField_1() { return &___U3CWriterU3Ek__BackingField_1; }
	inline void set_U3CWriterU3Ek__BackingField_1(JsonWriter_t3014444111 * value)
	{
		___U3CWriterU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CWriterU3Ek__BackingField_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
