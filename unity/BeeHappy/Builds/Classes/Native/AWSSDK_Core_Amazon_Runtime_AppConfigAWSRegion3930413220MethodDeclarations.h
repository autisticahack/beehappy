﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.AppConfigAWSRegion
struct AppConfigAWSRegion_t3930413220;

#include "codegen/il2cpp-codegen.h"

// System.Void Amazon.Runtime.AppConfigAWSRegion::.ctor()
extern "C"  void AppConfigAWSRegion__ctor_m1059351043 (AppConfigAWSRegion_t3930413220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
