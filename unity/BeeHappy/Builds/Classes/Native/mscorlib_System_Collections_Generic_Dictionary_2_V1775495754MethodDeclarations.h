﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V3725165281MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,ThirdParty.Json.LitJson.ArrayMetadata>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m1445253823(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t1775495754 *, Dictionary_2_t3072435911 *, const MethodInfo*))ValueCollection__ctor_m1108018630_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m4289990049(__this, ___item0, method) ((  void (*) (ValueCollection_t1775495754 *, ArrayMetadata_t1135078014 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3345332980_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1197200128(__this, method) ((  void (*) (ValueCollection_t1775495754 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1917629527_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2241042675(__this, ___item0, method) ((  bool (*) (ValueCollection_t1775495754 *, ArrayMetadata_t1135078014 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1785773216_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1946473330(__this, ___item0, method) ((  bool (*) (ValueCollection_t1775495754 *, ArrayMetadata_t1135078014 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m721874727_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m958507072(__this, method) ((  Il2CppObject* (*) (ValueCollection_t1775495754 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2748591009_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m1137853616(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t1775495754 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m280898373_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3685664349(__this, method) ((  Il2CppObject * (*) (ValueCollection_t1775495754 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m211044868_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2833553294(__this, method) ((  bool (*) (ValueCollection_t1775495754 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3396541731_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1543116424(__this, method) ((  bool (*) (ValueCollection_t1775495754 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3022974697_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m2225359424(__this, method) ((  Il2CppObject * (*) (ValueCollection_t1775495754 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m2568042789_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,ThirdParty.Json.LitJson.ArrayMetadata>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m736590344(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t1775495754 *, ArrayMetadataU5BU5D_t3927266763*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m752359783_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,ThirdParty.Json.LitJson.ArrayMetadata>::GetEnumerator()
#define ValueCollection_GetEnumerator_m894615459(__this, method) ((  Enumerator_t464001379  (*) (ValueCollection_t1775495754 *, const MethodInfo*))ValueCollection_GetEnumerator_m2382992780_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,ThirdParty.Json.LitJson.ArrayMetadata>::get_Count()
#define ValueCollection_get_Count_m1408243592(__this, method) ((  int32_t (*) (ValueCollection_t1775495754 *, const MethodInfo*))ValueCollection_get_Count_m1001996513_gshared)(__this, method)
