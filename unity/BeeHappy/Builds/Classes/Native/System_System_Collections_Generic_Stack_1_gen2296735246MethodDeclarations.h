﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Stack_1_gen3777177449MethodDeclarations.h"

// System.Void System.Collections.Generic.Stack`1<ThirdParty.Json.LitJson.WriterContext>::.ctor()
#define Stack_1__ctor_m1218044367(__this, method) ((  void (*) (Stack_1_t2296735246 *, const MethodInfo*))Stack_1__ctor_m1041657164_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1<ThirdParty.Json.LitJson.WriterContext>::System.Collections.ICollection.get_IsSynchronized()
#define Stack_1_System_Collections_ICollection_get_IsSynchronized_m1983725674(__this, method) ((  bool (*) (Stack_1_t2296735246 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_IsSynchronized_m2076161108_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1<ThirdParty.Json.LitJson.WriterContext>::System.Collections.ICollection.get_SyncRoot()
#define Stack_1_System_Collections_ICollection_get_SyncRoot_m973376692(__this, method) ((  Il2CppObject * (*) (Stack_1_t2296735246 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_SyncRoot_m3151629354_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<ThirdParty.Json.LitJson.WriterContext>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Stack_1_System_Collections_ICollection_CopyTo_m4117445518(__this, ___dest0, ___idx1, method) ((  void (*) (Stack_1_t2296735246 *, Il2CppArray *, int32_t, const MethodInfo*))Stack_1_System_Collections_ICollection_CopyTo_m2104527616_gshared)(__this, ___dest0, ___idx1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Stack`1<ThirdParty.Json.LitJson.WriterContext>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1004331100(__this, method) ((  Il2CppObject* (*) (Stack_1_t2296735246 *, const MethodInfo*))Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m680979874_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Stack`1<ThirdParty.Json.LitJson.WriterContext>::System.Collections.IEnumerable.GetEnumerator()
#define Stack_1_System_Collections_IEnumerable_GetEnumerator_m1069552051(__this, method) ((  Il2CppObject * (*) (Stack_1_t2296735246 *, const MethodInfo*))Stack_1_System_Collections_IEnumerable_GetEnumerator_m3875192475_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1<ThirdParty.Json.LitJson.WriterContext>::Contains(T)
#define Stack_1_Contains_m1883310805(__this, ___t0, method) ((  bool (*) (Stack_1_t2296735246 *, WriterContext_t1209007092 *, const MethodInfo*))Stack_1_Contains_m973625077_gshared)(__this, ___t0, method)
// System.Void System.Collections.Generic.Stack`1<ThirdParty.Json.LitJson.WriterContext>::CopyTo(T[],System.Int32)
#define Stack_1_CopyTo_m3515507283(__this, ___dest0, ___idx1, method) ((  void (*) (Stack_1_t2296735246 *, WriterContextU5BU5D_t2137321917*, int32_t, const MethodInfo*))Stack_1_CopyTo_m2005018155_gshared)(__this, ___dest0, ___idx1, method)
// T System.Collections.Generic.Stack`1<ThirdParty.Json.LitJson.WriterContext>::Peek()
#define Stack_1_Peek_m1248577930(__this, method) ((  WriterContext_t1209007092 * (*) (Stack_1_t2296735246 *, const MethodInfo*))Stack_1_Peek_m1548778538_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<ThirdParty.Json.LitJson.WriterContext>::Pop()
#define Stack_1_Pop_m2652175638(__this, method) ((  WriterContext_t1209007092 * (*) (Stack_1_t2296735246 *, const MethodInfo*))Stack_1_Pop_m535185982_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<ThirdParty.Json.LitJson.WriterContext>::Push(T)
#define Stack_1_Push_m193241330(__this, ___t0, method) ((  void (*) (Stack_1_t2296735246 *, WriterContext_t1209007092 *, const MethodInfo*))Stack_1_Push_m2122392216_gshared)(__this, ___t0, method)
// T[] System.Collections.Generic.Stack`1<ThirdParty.Json.LitJson.WriterContext>::ToArray()
#define Stack_1_ToArray_m1162183293(__this, method) ((  WriterContextU5BU5D_t2137321917* (*) (Stack_1_t2296735246 *, const MethodInfo*))Stack_1_ToArray_m2202918797_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Stack`1<ThirdParty.Json.LitJson.WriterContext>::get_Count()
#define Stack_1_get_Count_m4227312143(__this, method) ((  int32_t (*) (Stack_1_t2296735246 *, const MethodInfo*))Stack_1_get_Count_m4101767244_gshared)(__this, method)
// System.Collections.Generic.Stack`1/Enumerator<T> System.Collections.Generic.Stack`1<ThirdParty.Json.LitJson.WriterContext>::GetEnumerator()
#define Stack_1_GetEnumerator_m2624348492(__this, method) ((  Enumerator_t2946733606  (*) (Stack_1_t2296735246 *, const MethodInfo*))Stack_1_GetEnumerator_m287848754_gshared)(__this, method)
