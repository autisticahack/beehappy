﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_Data_System_Data_Common_DataContainer2398158623.h"
#include "System_Data_System_Data_Common_BitDataContainer1343889004.h"
#include "System_Data_System_Data_Common_CharDataContainer2982859723.h"
#include "System_Data_System_Data_Common_ByteDataContainer2208990629.h"
#include "System_Data_System_Data_Common_SByteDataContainer3110262680.h"
#include "System_Data_System_Data_Common_Int16DataContainer1700037739.h"
#include "System_Data_System_Data_Common_UInt16DataContainer3657339396.h"
#include "System_Data_System_Data_Common_Int32DataContainer2999025725.h"
#include "System_Data_System_Data_Common_UInt32DataContainer4285211530.h"
#include "System_Data_System_Data_Common_Int64DataContainer2950877372.h"
#include "System_Data_System_Data_Common_UInt64DataContainer3150804279.h"
#include "System_Data_System_Data_Common_SingleDataContainer3369330449.h"
#include "System_Data_System_Data_Common_DoubleDataContainer768771878.h"
#include "System_Data_System_Data_Common_ObjectDataContainer2927680112.h"
#include "System_Data_System_Data_Common_DateTimeDataContain4016646090.h"
#include "System_Data_System_Data_Common_DecimalDataContainer116516064.h"
#include "System_Data_System_Data_Common_StringDataContainer297709722.h"
#include "System_Data_System_Data_Common_DataTableMapping3453937356.h"
#include "System_Data_System_Data_Common_DataTableMappingColl394073712.h"
#include "System_Data_System_Data_Common_DbCommand673053565.h"
#include "System_Data_System_Data_Common_DbCommandBuilder1858692636.h"
#include "System_Data_System_Data_Common_DbConnection1449646780.h"
#include "System_Data_System_Data_Common_DbConnection_DataTy2825849738.h"
#include "System_Data_System_Data_Common_DbConnection_ColumnI894042583.h"
#include "System_Data_System_Data_Common_DbConnection_MetaDa2815702067.h"
#include "System_Data_System_Data_Common_DbConnection_Restri2125582038.h"
#include "System_Data_System_Data_Common_DbConnection_Reserv3801846590.h"
#include "System_Data_System_Data_Common_DbDataAdapter1474708225.h"
#include "System_Data_System_Data_Common_DbDataReader79450127.h"
#include "System_Data_System_Data_Common_DbDataRecord1591831971.h"
#include "System_Data_System_Data_Common_DbDataRecordImpl393023775.h"
#include "System_Data_System_Data_Common_DbEnumerator2633388082.h"
#include "System_Data_System_Data_Common_DbException1404275557.h"
#include "System_Data_System_Data_Common_DbMetaDataColumnName636683061.h"
#include "System_Data_System_Data_Common_DbParameter939375515.h"
#include "System_Data_System_Data_Common_DbParameterCollecti3118895993.h"
#include "System_Data_System_Data_Common_DbProviderFactory661609867.h"
#include "System_Data_System_Data_Common_DbProviderSpecificTy563873672.h"
#include "System_Data_System_Data_Common_DbTransaction3114611728.h"
#include "System_Data_System_Data_Common_DbTypes3560657065.h"
#include "System_Data_System_Data_Common_Index1936973642.h"
#include "System_Data_System_Data_Common_Key3988199635.h"
#include "System_Data_System_Data_Common_RecordCache828614321.h"
#include "System_Data_System_Data_Common_RowUpdatingEventArg1243529829.h"
#include "System_Data_System_Data_Common_SchemaInfo3205033891.h"
#include "System_Data_System_Data_Common_SchemaTableColumn1969474909.h"
#include "System_Data_System_Data_Common_SchemaTableOptional1564084131.h"
#include "System_Data_System_Data_SqlTypes_SqlBinary793232789.h"
#include "System_Data_System_Data_SqlTypes_SqlBoolean3839138046.h"
#include "System_Data_System_Data_SqlTypes_SqlByte3455748004.h"
#include "System_Data_System_Data_SqlTypes_SqlBytes810739863.h"
#include "System_Data_System_Data_SqlTypes_SqlChars4198130789.h"
#include "System_Data_System_Data_SqlTypes_SqlCompareOptions1210717665.h"
#include "System_Data_System_Data_SqlTypes_SqlDateTime3360333847.h"
#include "System_Data_System_Data_SqlTypes_SqlDecimal3734586185.h"
#include "System_Data_System_Data_SqlTypes_SqlDouble3827237195.h"
#include "System_Data_System_Data_SqlTypes_SqlGuid518445381.h"
#include "System_Data_System_Data_SqlTypes_SqlInt162033903892.h"
#include "System_Data_System_Data_SqlTypes_SqlInt32871104482.h"
#include "System_Data_System_Data_SqlTypes_SqlInt641630619367.h"
#include "System_Data_System_Data_SqlTypes_SqlMoney2080962266.h"
#include "System_Data_System_Data_SqlTypes_SqlNullValueExcep3352424041.h"
#include "System_Data_System_Data_SqlTypes_SqlSingle2352658522.h"
#include "System_Data_System_Data_SqlTypes_SqlString1659435999.h"
#include "System_Data_System_Data_SqlTypes_SqlTruncateExcepti202151371.h"
#include "System_Data_System_Data_SqlTypes_SqlTypeException2193175369.h"
#include "System_Data_System_Data_SqlTypes_SqlXml1067318651.h"
#include "System_Data_System_Data_SqlTypes_StorageState969348312.h"
#include "System_Data_System_Data_AcceptRejectRule449381895.h"
#include "System_Data_System_Data_ColumnTypeConverter1831565732.h"
#include "System_Data_System_Data_CommandBehavior1693351995.h"
#include "System_Data_System_Data_CommandType1117481605.h"
#include "System_Data_System_Data_ConflictOption2254355037.h"
#include "System_Data_System_Data_ConnectionState2517440941.h"
#include "System_Data_System_Data_Constraint3084057805.h"
#include "System_Data_System_Data_ConstraintCollection4088681537.h"
#include "System_Data_System_Data_ConstraintConverter2672338273.h"
#include "System_Data_System_Data_ConstraintException1983363612.h"
#include "System_Data_System_Data_DataCategoryAttribute1324422042.h"
#include "System_Data_System_Data_DataColumn2152532948.h"
#include "System_Data_System_Data_DataColumnChangeEventArgs3787575251.h"
#include "System_Data_System_Data_Doublet1180968275.h"
#include "System_Data_System_Data_DataColumnCollection195763618.h"
#include "System_Data_System_Data_DataException1371615661.h"
#include "System_Data_System_Data_DataRelation790111712.h"
#include "System_Data_System_Data_DataRelationCollection3958690162.h"
#include "System_Data_System_Data_DataRelationCollection_DataSe2876986.h"
#include "System_Data_System_Data_DataRelationCollection_Data613421182.h"
#include "System_Data_System_Data_DataRow321465356.h"
#include "System_Data_System_Data_DataRowAction1701244498.h"
#include "System_Data_System_Data_DataRowBuilder1684492161.h"
#include "System_Data_System_Data_DataRowChangeEventArgs949966087.h"
#include "System_Data_System_Data_DataRowCollection111352322.h"
#include "System_Data_System_Data_DataRowState3751910455.h"
#include "System_Data_System_Data_DataRowVersion3411859714.h"
#include "System_Data_System_Data_DataSet3097402844.h"
#include "System_Data_System_Data_DataSetDateTime574421329.h"
#include "System_Data_System_Data_DataTable3267612424.h"
#include "System_Data_System_Data_DataTableCollection787171642.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2500 = { sizeof (DataContainer_t2398158623), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2500[3] = 
{
	DataContainer_t2398158623::get_offset_of_null_values_0(),
	DataContainer_t2398158623::get_offset_of__type_1(),
	DataContainer_t2398158623::get_offset_of__column_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2501 = { sizeof (BitDataContainer_t1343889004), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2501[1] = 
{
	BitDataContainer_t1343889004::get_offset_of__values_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2502 = { sizeof (CharDataContainer_t2982859723), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2502[1] = 
{
	CharDataContainer_t2982859723::get_offset_of__values_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2503 = { sizeof (ByteDataContainer_t2208990629), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2503[1] = 
{
	ByteDataContainer_t2208990629::get_offset_of__values_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2504 = { sizeof (SByteDataContainer_t3110262680), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2504[1] = 
{
	SByteDataContainer_t3110262680::get_offset_of__values_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2505 = { sizeof (Int16DataContainer_t1700037739), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2505[1] = 
{
	Int16DataContainer_t1700037739::get_offset_of__values_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2506 = { sizeof (UInt16DataContainer_t3657339396), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2506[1] = 
{
	UInt16DataContainer_t3657339396::get_offset_of__values_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2507 = { sizeof (Int32DataContainer_t2999025725), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2507[1] = 
{
	Int32DataContainer_t2999025725::get_offset_of__values_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2508 = { sizeof (UInt32DataContainer_t4285211530), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2508[1] = 
{
	UInt32DataContainer_t4285211530::get_offset_of__values_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2509 = { sizeof (Int64DataContainer_t2950877372), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2509[1] = 
{
	Int64DataContainer_t2950877372::get_offset_of__values_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2510 = { sizeof (UInt64DataContainer_t3150804279), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2510[1] = 
{
	UInt64DataContainer_t3150804279::get_offset_of__values_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2511 = { sizeof (SingleDataContainer_t3369330449), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2511[1] = 
{
	SingleDataContainer_t3369330449::get_offset_of__values_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2512 = { sizeof (DoubleDataContainer_t768771878), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2512[1] = 
{
	DoubleDataContainer_t768771878::get_offset_of__values_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2513 = { sizeof (ObjectDataContainer_t2927680112), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2513[1] = 
{
	ObjectDataContainer_t2927680112::get_offset_of__values_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2514 = { sizeof (DateTimeDataContainer_t4016646090), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2515 = { sizeof (DecimalDataContainer_t116516064), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2516 = { sizeof (StringDataContainer_t297709722), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2517 = { sizeof (DataTableMapping_t3453937356), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2517[3] = 
{
	DataTableMapping_t3453937356::get_offset_of_sourceTable_1(),
	DataTableMapping_t3453937356::get_offset_of_dataSetTable_2(),
	DataTableMapping_t3453937356::get_offset_of_columnMappings_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2518 = { sizeof (DataTableMappingCollection_t394073712), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2518[3] = 
{
	DataTableMappingCollection_t394073712::get_offset_of_mappings_1(),
	DataTableMappingCollection_t394073712::get_offset_of_sourceTables_2(),
	DataTableMappingCollection_t394073712::get_offset_of_dataSetTables_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2519 = { sizeof (DbCommand_t673053565), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2520 = { sizeof (DbCommandBuilder_t1858692636), -1, sizeof(DbCommandBuilder_t1858692636_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2520[15] = 
{
	DbCommandBuilder_t1858692636::get_offset_of__disposed_4(),
	DbCommandBuilder_t1858692636::get_offset_of__dbSchemaTable_5(),
	DbCommandBuilder_t1858692636::get_offset_of__dbDataAdapter_6(),
	DbCommandBuilder_t1858692636::get_offset_of__catalogLocation_7(),
	DbCommandBuilder_t1858692636::get_offset_of__conflictOption_8(),
	DbCommandBuilder_t1858692636::get_offset_of__tableName_9(),
	DbCommandBuilder_t1858692636::get_offset_of__quotePrefix_10(),
	DbCommandBuilder_t1858692636::get_offset_of__quoteSuffix_11(),
	DbCommandBuilder_t1858692636::get_offset_of__dbCommand_12(),
	DbCommandBuilder_t1858692636::get_offset_of__deleteCommand_13(),
	DbCommandBuilder_t1858692636::get_offset_of__insertCommand_14(),
	DbCommandBuilder_t1858692636::get_offset_of__updateCommand_15(),
	DbCommandBuilder_t1858692636_StaticFields::get_offset_of_SEPARATOR_DEFAULT_16(),
	DbCommandBuilder_t1858692636_StaticFields::get_offset_of_clause1_17(),
	DbCommandBuilder_t1858692636_StaticFields::get_offset_of_clause2_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2521 = { sizeof (DbConnection_t1449646780), -1, sizeof(DbConnection_t1449646780_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2521[1] = 
{
	DbConnection_t1449646780_StaticFields::get_offset_of_U3CU3Ef__switchU24map1_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2522 = { sizeof (DataTypes_t2825849738), -1, sizeof(DataTypes_t2825849738_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2522[3] = 
{
	DataTypes_t2825849738_StaticFields::get_offset_of_columns_0(),
	DataTypes_t2825849738_StaticFields::get_offset_of_rows_1(),
	DataTypes_t2825849738_StaticFields::get_offset_of_instance_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2523 = { sizeof (ColumnInfo_t894042583)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2523[2] = 
{
	ColumnInfo_t894042583::get_offset_of_name_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColumnInfo_t894042583::get_offset_of_type_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2524 = { sizeof (MetaDataCollections_t2815702067), -1, sizeof(MetaDataCollections_t2815702067_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2524[3] = 
{
	MetaDataCollections_t2815702067_StaticFields::get_offset_of_columns_0(),
	MetaDataCollections_t2815702067_StaticFields::get_offset_of_rows_1(),
	MetaDataCollections_t2815702067_StaticFields::get_offset_of_instance_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2525 = { sizeof (Restrictions_t2125582038), -1, sizeof(Restrictions_t2125582038_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2525[3] = 
{
	Restrictions_t2125582038_StaticFields::get_offset_of_columns_0(),
	Restrictions_t2125582038_StaticFields::get_offset_of_rows_1(),
	Restrictions_t2125582038_StaticFields::get_offset_of_instance_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2526 = { sizeof (ReservedWords_t3801846590), -1, sizeof(ReservedWords_t3801846590_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2526[2] = 
{
	ReservedWords_t3801846590_StaticFields::get_offset_of_reservedWords_0(),
	ReservedWords_t3801846590_StaticFields::get_offset_of_instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2527 = { sizeof (DbDataAdapter_t1474708225), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2527[5] = 
{
	DbDataAdapter_t1474708225::get_offset_of__behavior_13(),
	DbDataAdapter_t1474708225::get_offset_of__selectCommand_14(),
	DbDataAdapter_t1474708225::get_offset_of__updateCommand_15(),
	DbDataAdapter_t1474708225::get_offset_of__deleteCommand_16(),
	DbDataAdapter_t1474708225::get_offset_of__insertCommand_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2528 = { sizeof (DbDataReader_t79450127), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2529 = { sizeof (DbDataRecord_t1591831971), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2530 = { sizeof (DbDataRecordImpl_t393023775), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2530[3] = 
{
	DbDataRecordImpl_t393023775::get_offset_of_schema_0(),
	DbDataRecordImpl_t393023775::get_offset_of_values_1(),
	DbDataRecordImpl_t393023775::get_offset_of_fieldCount_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2531 = { sizeof (DbEnumerator_t2633388082), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2531[4] = 
{
	DbEnumerator_t2633388082::get_offset_of_reader_0(),
	DbEnumerator_t2633388082::get_offset_of_closeReader_1(),
	DbEnumerator_t2633388082::get_offset_of_schema_2(),
	DbEnumerator_t2633388082::get_offset_of_values_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2532 = { sizeof (DbException_t1404275557), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2533 = { sizeof (DbMetaDataColumnNames_t636683061), -1, sizeof(DbMetaDataColumnNames_t636683061_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2533[43] = 
{
	DbMetaDataColumnNames_t636683061_StaticFields::get_offset_of_CollectionName_0(),
	DbMetaDataColumnNames_t636683061_StaticFields::get_offset_of_ColumnSize_1(),
	DbMetaDataColumnNames_t636683061_StaticFields::get_offset_of_CompositeIdentifierSeparatorPattern_2(),
	DbMetaDataColumnNames_t636683061_StaticFields::get_offset_of_CreateFormat_3(),
	DbMetaDataColumnNames_t636683061_StaticFields::get_offset_of_CreateParameters_4(),
	DbMetaDataColumnNames_t636683061_StaticFields::get_offset_of_DataSourceProductName_5(),
	DbMetaDataColumnNames_t636683061_StaticFields::get_offset_of_DataSourceProductVersion_6(),
	DbMetaDataColumnNames_t636683061_StaticFields::get_offset_of_DataType_7(),
	DbMetaDataColumnNames_t636683061_StaticFields::get_offset_of_DataSourceProductVersionNormalized_8(),
	DbMetaDataColumnNames_t636683061_StaticFields::get_offset_of_GroupByBehavior_9(),
	DbMetaDataColumnNames_t636683061_StaticFields::get_offset_of_IdentifierCase_10(),
	DbMetaDataColumnNames_t636683061_StaticFields::get_offset_of_IdentifierPattern_11(),
	DbMetaDataColumnNames_t636683061_StaticFields::get_offset_of_IsAutoIncrementable_12(),
	DbMetaDataColumnNames_t636683061_StaticFields::get_offset_of_IsBestMatch_13(),
	DbMetaDataColumnNames_t636683061_StaticFields::get_offset_of_IsCaseSensitive_14(),
	DbMetaDataColumnNames_t636683061_StaticFields::get_offset_of_IsConcurrencyType_15(),
	DbMetaDataColumnNames_t636683061_StaticFields::get_offset_of_IsFixedLength_16(),
	DbMetaDataColumnNames_t636683061_StaticFields::get_offset_of_IsFixedPrecisionScale_17(),
	DbMetaDataColumnNames_t636683061_StaticFields::get_offset_of_IsLiteralSupported_18(),
	DbMetaDataColumnNames_t636683061_StaticFields::get_offset_of_IsLong_19(),
	DbMetaDataColumnNames_t636683061_StaticFields::get_offset_of_IsNullable_20(),
	DbMetaDataColumnNames_t636683061_StaticFields::get_offset_of_IsSearchable_21(),
	DbMetaDataColumnNames_t636683061_StaticFields::get_offset_of_IsSearchableWithLike_22(),
	DbMetaDataColumnNames_t636683061_StaticFields::get_offset_of_IsUnsigned_23(),
	DbMetaDataColumnNames_t636683061_StaticFields::get_offset_of_LiteralPrefix_24(),
	DbMetaDataColumnNames_t636683061_StaticFields::get_offset_of_LiteralSuffix_25(),
	DbMetaDataColumnNames_t636683061_StaticFields::get_offset_of_MaximumScale_26(),
	DbMetaDataColumnNames_t636683061_StaticFields::get_offset_of_MinimumScale_27(),
	DbMetaDataColumnNames_t636683061_StaticFields::get_offset_of_NumberOfIdentifierParts_28(),
	DbMetaDataColumnNames_t636683061_StaticFields::get_offset_of_NumberOfRestrictions_29(),
	DbMetaDataColumnNames_t636683061_StaticFields::get_offset_of_OrderByColumnsInSelect_30(),
	DbMetaDataColumnNames_t636683061_StaticFields::get_offset_of_ParameterMarkerFormat_31(),
	DbMetaDataColumnNames_t636683061_StaticFields::get_offset_of_ParameterMarkerPattern_32(),
	DbMetaDataColumnNames_t636683061_StaticFields::get_offset_of_ParameterNameMaxLength_33(),
	DbMetaDataColumnNames_t636683061_StaticFields::get_offset_of_ParameterNamePattern_34(),
	DbMetaDataColumnNames_t636683061_StaticFields::get_offset_of_ProviderDbType_35(),
	DbMetaDataColumnNames_t636683061_StaticFields::get_offset_of_QuotedIdentifierCase_36(),
	DbMetaDataColumnNames_t636683061_StaticFields::get_offset_of_QuotedIdentifierPattern_37(),
	DbMetaDataColumnNames_t636683061_StaticFields::get_offset_of_ReservedWord_38(),
	DbMetaDataColumnNames_t636683061_StaticFields::get_offset_of_StatementSeparatorPattern_39(),
	DbMetaDataColumnNames_t636683061_StaticFields::get_offset_of_StringLiteralPattern_40(),
	DbMetaDataColumnNames_t636683061_StaticFields::get_offset_of_SupportedJoinOperators_41(),
	DbMetaDataColumnNames_t636683061_StaticFields::get_offset_of_TypeName_42(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2534 = { sizeof (DbParameter_t939375515), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2535 = { sizeof (DbParameterCollection_t3118895993), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2536 = { sizeof (DbProviderFactory_t661609867), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2537 = { sizeof (DbProviderSpecificTypePropertyAttribute_t563873672), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2537[1] = 
{
	DbProviderSpecificTypePropertyAttribute_t563873672::get_offset_of_isProviderSpecificTypeProperty_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2538 = { sizeof (DbTransaction_t3114611728), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2539 = { sizeof (DbTypes_t3560657065), -1, sizeof(DbTypes_t3560657065_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2539[21] = 
{
	DbTypes_t3560657065_StaticFields::get_offset_of_TypeOfBoolean_0(),
	DbTypes_t3560657065_StaticFields::get_offset_of_TypeOfSByte_1(),
	DbTypes_t3560657065_StaticFields::get_offset_of_TypeOfChar_2(),
	DbTypes_t3560657065_StaticFields::get_offset_of_TypeOfInt16_3(),
	DbTypes_t3560657065_StaticFields::get_offset_of_TypeOfInt32_4(),
	DbTypes_t3560657065_StaticFields::get_offset_of_TypeOfInt64_5(),
	DbTypes_t3560657065_StaticFields::get_offset_of_TypeOfByte_6(),
	DbTypes_t3560657065_StaticFields::get_offset_of_TypeOfUInt16_7(),
	DbTypes_t3560657065_StaticFields::get_offset_of_TypeOfUInt32_8(),
	DbTypes_t3560657065_StaticFields::get_offset_of_TypeOfUInt64_9(),
	DbTypes_t3560657065_StaticFields::get_offset_of_TypeOfDouble_10(),
	DbTypes_t3560657065_StaticFields::get_offset_of_TypeOfSingle_11(),
	DbTypes_t3560657065_StaticFields::get_offset_of_TypeOfDecimal_12(),
	DbTypes_t3560657065_StaticFields::get_offset_of_TypeOfString_13(),
	DbTypes_t3560657065_StaticFields::get_offset_of_TypeOfDateTime_14(),
	DbTypes_t3560657065_StaticFields::get_offset_of_TypeOfObject_15(),
	DbTypes_t3560657065_StaticFields::get_offset_of_TypeOfGuid_16(),
	DbTypes_t3560657065_StaticFields::get_offset_of_TypeOfType_17(),
	DbTypes_t3560657065_StaticFields::get_offset_of_TypeOfByteArray_18(),
	DbTypes_t3560657065_StaticFields::get_offset_of_TypeOfFloat_19(),
	DbTypes_t3560657065_StaticFields::get_offset_of_TypeOfTimespan_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2540 = { sizeof (Index_t1936973642), -1, sizeof(Index_t1936973642_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2540[7] = 
{
	Index_t1936973642_StaticFields::get_offset_of_empty_0(),
	Index_t1936973642::get_offset_of__array_1(),
	Index_t1936973642::get_offset_of__size_2(),
	Index_t1936973642::get_offset_of__key_3(),
	Index_t1936973642::get_offset_of__refCount_4(),
	Index_t1936973642::get_offset_of_know_have_duplicates_5(),
	Index_t1936973642::get_offset_of_know_no_duplicates_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2541 = { sizeof (Key_t3988199635), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2541[6] = 
{
	Key_t3988199635::get_offset_of__table_0(),
	Key_t3988199635::get_offset_of__columns_1(),
	Key_t3988199635::get_offset_of__sortDirection_2(),
	Key_t3988199635::get_offset_of__rowStateFilter_3(),
	Key_t3988199635::get_offset_of__filter_4(),
	Key_t3988199635::get_offset_of__tmpRow_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2542 = { sizeof (RecordCache_t828614321), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2542[5] = 
{
	RecordCache_t828614321::get_offset_of__records_0(),
	RecordCache_t828614321::get_offset_of__nextFreeIndex_1(),
	RecordCache_t828614321::get_offset_of__currentCapacity_2(),
	RecordCache_t828614321::get_offset_of__table_3(),
	RecordCache_t828614321::get_offset_of__rowsToRecords_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2543 = { sizeof (RowUpdatingEventArgs_t1243529829), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2543[5] = 
{
	RowUpdatingEventArgs_t1243529829::get_offset_of_dataRow_1(),
	RowUpdatingEventArgs_t1243529829::get_offset_of_command_2(),
	RowUpdatingEventArgs_t1243529829::get_offset_of_statementType_3(),
	RowUpdatingEventArgs_t1243529829::get_offset_of_status_4(),
	RowUpdatingEventArgs_t1243529829::get_offset_of_errors_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2544 = { sizeof (SchemaInfo_t3205033891), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2544[4] = 
{
	SchemaInfo_t3205033891::get_offset_of_columnName_0(),
	SchemaInfo_t3205033891::get_offset_of_dataTypeName_1(),
	SchemaInfo_t3205033891::get_offset_of_ordinal_2(),
	SchemaInfo_t3205033891::get_offset_of_fieldType_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2545 = { sizeof (SchemaTableColumn_t1969474909), -1, sizeof(SchemaTableColumn_t1969474909_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2545[17] = 
{
	SchemaTableColumn_t1969474909_StaticFields::get_offset_of_AllowDBNull_0(),
	SchemaTableColumn_t1969474909_StaticFields::get_offset_of_BaseColumnName_1(),
	SchemaTableColumn_t1969474909_StaticFields::get_offset_of_BaseSchemaName_2(),
	SchemaTableColumn_t1969474909_StaticFields::get_offset_of_BaseTableName_3(),
	SchemaTableColumn_t1969474909_StaticFields::get_offset_of_ColumnName_4(),
	SchemaTableColumn_t1969474909_StaticFields::get_offset_of_ColumnOrdinal_5(),
	SchemaTableColumn_t1969474909_StaticFields::get_offset_of_ColumnSize_6(),
	SchemaTableColumn_t1969474909_StaticFields::get_offset_of_DataType_7(),
	SchemaTableColumn_t1969474909_StaticFields::get_offset_of_IsAliased_8(),
	SchemaTableColumn_t1969474909_StaticFields::get_offset_of_IsExpression_9(),
	SchemaTableColumn_t1969474909_StaticFields::get_offset_of_IsKey_10(),
	SchemaTableColumn_t1969474909_StaticFields::get_offset_of_IsLong_11(),
	SchemaTableColumn_t1969474909_StaticFields::get_offset_of_IsUnique_12(),
	SchemaTableColumn_t1969474909_StaticFields::get_offset_of_NonVersionedProviderType_13(),
	SchemaTableColumn_t1969474909_StaticFields::get_offset_of_NumericPrecision_14(),
	SchemaTableColumn_t1969474909_StaticFields::get_offset_of_NumericScale_15(),
	SchemaTableColumn_t1969474909_StaticFields::get_offset_of_ProviderType_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2546 = { sizeof (SchemaTableOptionalColumn_t1564084131), -1, sizeof(SchemaTableOptionalColumn_t1564084131_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2546[14] = 
{
	SchemaTableOptionalColumn_t1564084131_StaticFields::get_offset_of_AutoIncrementSeed_0(),
	SchemaTableOptionalColumn_t1564084131_StaticFields::get_offset_of_AutoIncrementStep_1(),
	SchemaTableOptionalColumn_t1564084131_StaticFields::get_offset_of_BaseCatalogName_2(),
	SchemaTableOptionalColumn_t1564084131_StaticFields::get_offset_of_BaseColumnNamespace_3(),
	SchemaTableOptionalColumn_t1564084131_StaticFields::get_offset_of_BaseServerName_4(),
	SchemaTableOptionalColumn_t1564084131_StaticFields::get_offset_of_BaseTableNamespace_5(),
	SchemaTableOptionalColumn_t1564084131_StaticFields::get_offset_of_ColumnMapping_6(),
	SchemaTableOptionalColumn_t1564084131_StaticFields::get_offset_of_DefaultValue_7(),
	SchemaTableOptionalColumn_t1564084131_StaticFields::get_offset_of_Expression_8(),
	SchemaTableOptionalColumn_t1564084131_StaticFields::get_offset_of_IsAutoIncrement_9(),
	SchemaTableOptionalColumn_t1564084131_StaticFields::get_offset_of_IsHidden_10(),
	SchemaTableOptionalColumn_t1564084131_StaticFields::get_offset_of_IsReadOnly_11(),
	SchemaTableOptionalColumn_t1564084131_StaticFields::get_offset_of_IsRowVersion_12(),
	SchemaTableOptionalColumn_t1564084131_StaticFields::get_offset_of_ProviderSpecificDataType_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2547 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2548 = { sizeof (SqlBinary_t793232789)+ sizeof (Il2CppObject), sizeof(SqlBinary_t793232789_marshaled_pinvoke), sizeof(SqlBinary_t793232789_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2548[3] = 
{
	SqlBinary_t793232789::get_offset_of_value_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SqlBinary_t793232789::get_offset_of_notNull_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SqlBinary_t793232789_StaticFields::get_offset_of_Null_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2549 = { sizeof (SqlBoolean_t3839138046)+ sizeof (Il2CppObject), sizeof(SqlBoolean_t3839138046_marshaled_pinvoke), sizeof(SqlBoolean_t3839138046_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2549[7] = 
{
	SqlBoolean_t3839138046::get_offset_of_value_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SqlBoolean_t3839138046::get_offset_of_notNull_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SqlBoolean_t3839138046_StaticFields::get_offset_of_False_2(),
	SqlBoolean_t3839138046_StaticFields::get_offset_of_Null_3(),
	SqlBoolean_t3839138046_StaticFields::get_offset_of_One_4(),
	SqlBoolean_t3839138046_StaticFields::get_offset_of_True_5(),
	SqlBoolean_t3839138046_StaticFields::get_offset_of_Zero_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2550 = { sizeof (SqlByte_t3455748004)+ sizeof (Il2CppObject), sizeof(SqlByte_t3455748004_marshaled_pinvoke), sizeof(SqlByte_t3455748004_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2550[6] = 
{
	SqlByte_t3455748004::get_offset_of_value_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SqlByte_t3455748004::get_offset_of_notNull_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SqlByte_t3455748004_StaticFields::get_offset_of_MaxValue_2(),
	SqlByte_t3455748004_StaticFields::get_offset_of_MinValue_3(),
	SqlByte_t3455748004_StaticFields::get_offset_of_Null_4(),
	SqlByte_t3455748004_StaticFields::get_offset_of_Zero_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2551 = { sizeof (SqlBytes_t810739863), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2551[3] = 
{
	SqlBytes_t810739863::get_offset_of_notNull_0(),
	SqlBytes_t810739863::get_offset_of_buffer_1(),
	SqlBytes_t810739863::get_offset_of_storage_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2552 = { sizeof (SqlChars_t4198130789), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2552[3] = 
{
	SqlChars_t4198130789::get_offset_of_notNull_0(),
	SqlChars_t4198130789::get_offset_of_buffer_1(),
	SqlChars_t4198130789::get_offset_of_storage_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2553 = { sizeof (SqlCompareOptions_t1210717665)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2553[7] = 
{
	SqlCompareOptions_t1210717665::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2554 = { sizeof (SqlDateTime_t3360333847)+ sizeof (Il2CppObject), -1, sizeof(SqlDateTime_t3360333847_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2554[9] = 
{
	SqlDateTime_t3360333847::get_offset_of_value_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SqlDateTime_t3360333847::get_offset_of_notNull_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SqlDateTime_t3360333847_StaticFields::get_offset_of_MaxValue_2(),
	SqlDateTime_t3360333847_StaticFields::get_offset_of_MinValue_3(),
	SqlDateTime_t3360333847_StaticFields::get_offset_of_Null_4(),
	SqlDateTime_t3360333847_StaticFields::get_offset_of_SQLTicksPerHour_5(),
	SqlDateTime_t3360333847_StaticFields::get_offset_of_SQLTicksPerMinute_6(),
	SqlDateTime_t3360333847_StaticFields::get_offset_of_SQLTicksPerSecond_7(),
	SqlDateTime_t3360333847_StaticFields::get_offset_of_zero_day_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2555 = { sizeof (SqlDecimal_t3734586185)+ sizeof (Il2CppObject), sizeof(SqlDecimal_t3734586185_marshaled_pinvoke), sizeof(SqlDecimal_t3734586185_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2555[11] = 
{
	SqlDecimal_t3734586185::get_offset_of_value_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SqlDecimal_t3734586185::get_offset_of_precision_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SqlDecimal_t3734586185::get_offset_of_scale_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SqlDecimal_t3734586185::get_offset_of_positive_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SqlDecimal_t3734586185::get_offset_of_notNull_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SqlDecimal_t3734586185_StaticFields::get_offset_of_constantsDecadeInt32Factors_5(),
	SqlDecimal_t3734586185_StaticFields::get_offset_of_MaxPrecision_6(),
	SqlDecimal_t3734586185_StaticFields::get_offset_of_MaxScale_7(),
	SqlDecimal_t3734586185_StaticFields::get_offset_of_MaxValue_8(),
	SqlDecimal_t3734586185_StaticFields::get_offset_of_MinValue_9(),
	SqlDecimal_t3734586185_StaticFields::get_offset_of_Null_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2556 = { sizeof (SqlDouble_t3827237195)+ sizeof (Il2CppObject), sizeof(SqlDouble_t3827237195_marshaled_pinvoke), sizeof(SqlDouble_t3827237195_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2556[6] = 
{
	SqlDouble_t3827237195::get_offset_of_value_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SqlDouble_t3827237195::get_offset_of_notNull_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SqlDouble_t3827237195_StaticFields::get_offset_of_MaxValue_2(),
	SqlDouble_t3827237195_StaticFields::get_offset_of_MinValue_3(),
	SqlDouble_t3827237195_StaticFields::get_offset_of_Null_4(),
	SqlDouble_t3827237195_StaticFields::get_offset_of_Zero_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2557 = { sizeof (SqlGuid_t518445381)+ sizeof (Il2CppObject), sizeof(SqlGuid_t518445381_marshaled_pinvoke), sizeof(SqlGuid_t518445381_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2557[3] = 
{
	SqlGuid_t518445381::get_offset_of_value_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SqlGuid_t518445381::get_offset_of_notNull_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SqlGuid_t518445381_StaticFields::get_offset_of_Null_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2558 = { sizeof (SqlInt16_t2033903892)+ sizeof (Il2CppObject), sizeof(SqlInt16_t2033903892_marshaled_pinvoke), sizeof(SqlInt16_t2033903892_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2558[6] = 
{
	SqlInt16_t2033903892::get_offset_of_value_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SqlInt16_t2033903892::get_offset_of_notNull_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SqlInt16_t2033903892_StaticFields::get_offset_of_MaxValue_2(),
	SqlInt16_t2033903892_StaticFields::get_offset_of_MinValue_3(),
	SqlInt16_t2033903892_StaticFields::get_offset_of_Null_4(),
	SqlInt16_t2033903892_StaticFields::get_offset_of_Zero_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2559 = { sizeof (SqlInt32_t871104482)+ sizeof (Il2CppObject), sizeof(SqlInt32_t871104482_marshaled_pinvoke), sizeof(SqlInt32_t871104482_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2559[6] = 
{
	SqlInt32_t871104482::get_offset_of_value_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SqlInt32_t871104482::get_offset_of_notNull_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SqlInt32_t871104482_StaticFields::get_offset_of_MaxValue_2(),
	SqlInt32_t871104482_StaticFields::get_offset_of_MinValue_3(),
	SqlInt32_t871104482_StaticFields::get_offset_of_Null_4(),
	SqlInt32_t871104482_StaticFields::get_offset_of_Zero_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2560 = { sizeof (SqlInt64_t1630619367)+ sizeof (Il2CppObject), sizeof(SqlInt64_t1630619367_marshaled_pinvoke), sizeof(SqlInt64_t1630619367_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2560[6] = 
{
	SqlInt64_t1630619367::get_offset_of_value_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SqlInt64_t1630619367::get_offset_of_notNull_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SqlInt64_t1630619367_StaticFields::get_offset_of_MaxValue_2(),
	SqlInt64_t1630619367_StaticFields::get_offset_of_MinValue_3(),
	SqlInt64_t1630619367_StaticFields::get_offset_of_Null_4(),
	SqlInt64_t1630619367_StaticFields::get_offset_of_Zero_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2561 = { sizeof (SqlMoney_t2080962266)+ sizeof (Il2CppObject), sizeof(SqlMoney_t2080962266_marshaled_pinvoke), sizeof(SqlMoney_t2080962266_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2561[7] = 
{
	SqlMoney_t2080962266::get_offset_of_value_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SqlMoney_t2080962266::get_offset_of_notNull_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SqlMoney_t2080962266_StaticFields::get_offset_of_MaxValue_2(),
	SqlMoney_t2080962266_StaticFields::get_offset_of_MinValue_3(),
	SqlMoney_t2080962266_StaticFields::get_offset_of_Null_4(),
	SqlMoney_t2080962266_StaticFields::get_offset_of_Zero_5(),
	SqlMoney_t2080962266_StaticFields::get_offset_of_MoneyFormat_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2562 = { sizeof (SqlNullValueException_t3352424041), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2563 = { sizeof (SqlSingle_t2352658522)+ sizeof (Il2CppObject), sizeof(SqlSingle_t2352658522_marshaled_pinvoke), sizeof(SqlSingle_t2352658522_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2563[6] = 
{
	SqlSingle_t2352658522::get_offset_of_value_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SqlSingle_t2352658522::get_offset_of_notNull_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SqlSingle_t2352658522_StaticFields::get_offset_of_MaxValue_2(),
	SqlSingle_t2352658522_StaticFields::get_offset_of_MinValue_3(),
	SqlSingle_t2352658522_StaticFields::get_offset_of_Null_4(),
	SqlSingle_t2352658522_StaticFields::get_offset_of_Zero_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2564 = { sizeof (SqlString_t1659435999)+ sizeof (Il2CppObject), sizeof(SqlString_t1659435999_marshaled_pinvoke), sizeof(SqlString_t1659435999_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2564[11] = 
{
	SqlString_t1659435999::get_offset_of_value_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SqlString_t1659435999::get_offset_of_notNull_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SqlString_t1659435999::get_offset_of_lcid_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SqlString_t1659435999::get_offset_of_compareOptions_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SqlString_t1659435999_StaticFields::get_offset_of_BinarySort_4(),
	SqlString_t1659435999_StaticFields::get_offset_of_IgnoreCase_5(),
	SqlString_t1659435999_StaticFields::get_offset_of_IgnoreKanaType_6(),
	SqlString_t1659435999_StaticFields::get_offset_of_IgnoreNonSpace_7(),
	SqlString_t1659435999_StaticFields::get_offset_of_IgnoreWidth_8(),
	SqlString_t1659435999_StaticFields::get_offset_of_Null_9(),
	SqlString_t1659435999_StaticFields::get_offset_of_DecimalFormat_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2565 = { sizeof (SqlTruncateException_t202151371), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2566 = { sizeof (SqlTypeException_t2193175369), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2567 = { sizeof (SqlXml_t1067318651), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2567[2] = 
{
	SqlXml_t1067318651::get_offset_of_notNull_0(),
	SqlXml_t1067318651::get_offset_of_xmlValue_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2568 = { sizeof (StorageState_t969348312)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2568[4] = 
{
	StorageState_t969348312::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2569 = { sizeof (AcceptRejectRule_t449381895)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2569[3] = 
{
	AcceptRejectRule_t449381895::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2570 = { sizeof (ColumnTypeConverter_t1831565732), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2571 = { sizeof (CommandBehavior_t1693351995)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2571[8] = 
{
	CommandBehavior_t1693351995::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2572 = { sizeof (CommandType_t1117481605)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2572[4] = 
{
	CommandType_t1117481605::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2573 = { sizeof (ConflictOption_t2254355037)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2573[4] = 
{
	ConflictOption_t2254355037::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2574 = { sizeof (ConnectionState_t2517440941)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2574[7] = 
{
	ConnectionState_t2517440941::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2575 = { sizeof (Constraint_t3084057805), -1, sizeof(Constraint_t3084057805_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2575[8] = 
{
	Constraint_t3084057805_StaticFields::get_offset_of_beforeConstraintNameChange_0(),
	Constraint_t3084057805::get_offset_of_events_1(),
	Constraint_t3084057805::get_offset_of__constraintName_2(),
	Constraint_t3084057805::get_offset_of__properties_3(),
	Constraint_t3084057805::get_offset_of__index_4(),
	Constraint_t3084057805::get_offset_of__constraintCollection_5(),
	Constraint_t3084057805::get_offset_of_dataSet_6(),
	Constraint_t3084057805::get_offset_of_initInProgress_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2576 = { sizeof (ConstraintCollection_t4088681537), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2576[2] = 
{
	ConstraintCollection_t4088681537::get_offset_of_table_2(),
	ConstraintCollection_t4088681537::get_offset_of_CollectionChanged_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2577 = { sizeof (ConstraintConverter_t2672338273), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2578 = { sizeof (ConstraintException_t1983363612), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2579 = { sizeof (DataCategoryAttribute_t1324422042), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2580 = { sizeof (DataColumn_t2152532948), -1, sizeof(DataColumn_t2152532948_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2580[23] = 
{
	DataColumn_t2152532948::get_offset_of__eventHandlers_2(),
	DataColumn_t2152532948_StaticFields::get_offset_of__propertyChangedKey_3(),
	DataColumn_t2152532948::get_offset_of__allowDBNull_4(),
	DataColumn_t2152532948::get_offset_of__autoIncrement_5(),
	DataColumn_t2152532948::get_offset_of__autoIncrementSeed_6(),
	DataColumn_t2152532948::get_offset_of__autoIncrementStep_7(),
	DataColumn_t2152532948::get_offset_of__nextAutoIncrementValue_8(),
	DataColumn_t2152532948::get_offset_of__caption_9(),
	DataColumn_t2152532948::get_offset_of__columnMapping_10(),
	DataColumn_t2152532948::get_offset_of__columnName_11(),
	DataColumn_t2152532948::get_offset_of__defaultValue_12(),
	DataColumn_t2152532948::get_offset_of__expression_13(),
	DataColumn_t2152532948::get_offset_of__compiledExpression_14(),
	DataColumn_t2152532948::get_offset_of__extendedProperties_15(),
	DataColumn_t2152532948::get_offset_of__maxLength_16(),
	DataColumn_t2152532948::get_offset_of__nameSpace_17(),
	DataColumn_t2152532948::get_offset_of__ordinal_18(),
	DataColumn_t2152532948::get_offset_of__prefix_19(),
	DataColumn_t2152532948::get_offset_of__readOnly_20(),
	DataColumn_t2152532948::get_offset_of__table_21(),
	DataColumn_t2152532948::get_offset_of__unique_22(),
	DataColumn_t2152532948::get_offset_of__dataContainer_23(),
	DataColumn_t2152532948::get_offset_of__datetimeMode_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2581 = { sizeof (DataColumnChangeEventArgs_t3787575251), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2581[3] = 
{
	DataColumnChangeEventArgs_t3787575251::get_offset_of__column_1(),
	DataColumnChangeEventArgs_t3787575251::get_offset_of__row_2(),
	DataColumnChangeEventArgs_t3787575251::get_offset_of__proposedValue_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2582 = { sizeof (Doublet_t1180968275), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2582[2] = 
{
	Doublet_t1180968275::get_offset_of_count_0(),
	Doublet_t1180968275::get_offset_of_columnNames_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2583 = { sizeof (DataColumnCollection_t195763618), -1, sizeof(DataColumnCollection_t195763618_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2583[9] = 
{
	DataColumnCollection_t195763618::get_offset_of_columnNameCount_2(),
	DataColumnCollection_t195763618::get_offset_of_columnFromName_3(),
	DataColumnCollection_t195763618::get_offset_of_autoIncrement_4(),
	DataColumnCollection_t195763618::get_offset_of_defaultColumnIndex_5(),
	DataColumnCollection_t195763618::get_offset_of_parentTable_6(),
	DataColumnCollection_t195763618_StaticFields::get_offset_of_ColumnPrefix_7(),
	DataColumnCollection_t195763618_StaticFields::get_offset_of_TenColumns_8(),
	DataColumnCollection_t195763618::get_offset_of_CollectionChanged_9(),
	DataColumnCollection_t195763618::get_offset_of_CollectionMetaDataChanged_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2584 = { sizeof (DataException_t1371615661), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2585 = { sizeof (DataRelation_t790111712), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2585[12] = 
{
	DataRelation_t790111712::get_offset_of_dataSet_0(),
	DataRelation_t790111712::get_offset_of_relationName_1(),
	DataRelation_t790111712::get_offset_of_parentKeyConstraint_2(),
	DataRelation_t790111712::get_offset_of_childKeyConstraint_3(),
	DataRelation_t790111712::get_offset_of_parentColumns_4(),
	DataRelation_t790111712::get_offset_of_childColumns_5(),
	DataRelation_t790111712::get_offset_of_nested_6(),
	DataRelation_t790111712::get_offset_of_createConstraints_7(),
	DataRelation_t790111712::get_offset_of_initFinished_8(),
	DataRelation_t790111712::get_offset_of_extendedProperties_9(),
	DataRelation_t790111712::get_offset_of__parentTableNameSpace_10(),
	DataRelation_t790111712::get_offset_of__childTableNameSpace_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2586 = { sizeof (DataRelationCollection_t3958690162), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2586[3] = 
{
	DataRelationCollection_t3958690162::get_offset_of_inTransition_2(),
	DataRelationCollection_t3958690162::get_offset_of_index_3(),
	DataRelationCollection_t3958690162::get_offset_of_CollectionChanged_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2587 = { sizeof (DataSetRelationCollection_t2876986), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2587[1] = 
{
	DataSetRelationCollection_t2876986::get_offset_of_dataSet_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2588 = { sizeof (DataTableRelationCollection_t613421182), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2588[1] = 
{
	DataTableRelationCollection_t613421182::get_offset_of_dataTable_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2589 = { sizeof (DataRow_t321465356), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2589[13] = 
{
	DataRow_t321465356::get_offset_of__table_0(),
	DataRow_t321465356::get_offset_of__original_1(),
	DataRow_t321465356::get_offset_of__current_2(),
	DataRow_t321465356::get_offset_of__proposed_3(),
	DataRow_t321465356::get_offset_of__columnErrors_4(),
	DataRow_t321465356::get_offset_of_rowError_5(),
	DataRow_t321465356::get_offset_of_xmlRowID_6(),
	DataRow_t321465356::get_offset_of__nullConstraintViolation_7(),
	DataRow_t321465356::get_offset_of__nullConstraintMessage_8(),
	DataRow_t321465356::get_offset_of__inChangingEvent_9(),
	DataRow_t321465356::get_offset_of__rowId_10(),
	DataRow_t321465356::get_offset_of__rowChanged_11(),
	DataRow_t321465356::get_offset_of__inExpressionEvaluation_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2590 = { sizeof (DataRowAction_t1701244498)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2590[9] = 
{
	DataRowAction_t1701244498::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2591 = { sizeof (DataRowBuilder_t1684492161), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2591[2] = 
{
	DataRowBuilder_t1684492161::get_offset_of_table_0(),
	DataRowBuilder_t1684492161::get_offset_of__rowId_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2592 = { sizeof (DataRowChangeEventArgs_t949966087), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2592[2] = 
{
	DataRowChangeEventArgs_t949966087::get_offset_of_row_1(),
	DataRowChangeEventArgs_t949966087::get_offset_of_action_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2593 = { sizeof (DataRowCollection_t111352322), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2593[2] = 
{
	DataRowCollection_t111352322::get_offset_of_table_2(),
	DataRowCollection_t111352322::get_offset_of_ListChanged_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2594 = { sizeof (DataRowState_t3751910455)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2594[6] = 
{
	DataRowState_t3751910455::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2595 = { sizeof (DataRowVersion_t3411859714)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2595[5] = 
{
	DataRowVersion_t3411859714::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2596 = { sizeof (DataSet_t3097402844), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2596[13] = 
{
	DataSet_t3097402844::get_offset_of_dataSetName_2(),
	DataSet_t3097402844::get_offset_of__namespace_3(),
	DataSet_t3097402844::get_offset_of_prefix_4(),
	DataSet_t3097402844::get_offset_of_caseSensitive_5(),
	DataSet_t3097402844::get_offset_of_enforceConstraints_6(),
	DataSet_t3097402844::get_offset_of_tableCollection_7(),
	DataSet_t3097402844::get_offset_of_relationCollection_8(),
	DataSet_t3097402844::get_offset_of_properties_9(),
	DataSet_t3097402844::get_offset_of_locale_10(),
	DataSet_t3097402844::get_offset_of_tableAdapterSchemaInfo_11(),
	DataSet_t3097402844::get_offset_of_dataSetInitialized_12(),
	DataSet_t3097402844::get_offset_of_remotingFormat_13(),
	DataSet_t3097402844::get_offset_of_MergeFailed_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2597 = { sizeof (DataSetDateTime_t574421329)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2597[5] = 
{
	DataSetDateTime_t574421329::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2598 = { sizeof (DataTable_t3267612424), -1, sizeof(DataTable_t3267612424_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2598[40] = 
{
	DataTable_t3267612424::get_offset_of_dataSet_2(),
	DataTable_t3267612424::get_offset_of__caseSensitive_3(),
	DataTable_t3267612424::get_offset_of__columnCollection_4(),
	DataTable_t3267612424::get_offset_of__constraintCollection_5(),
	DataTable_t3267612424::get_offset_of__displayExpression_6(),
	DataTable_t3267612424::get_offset_of__extendedProperties_7(),
	DataTable_t3267612424::get_offset_of__locale_8(),
	DataTable_t3267612424::get_offset_of__minimumCapacity_9(),
	DataTable_t3267612424::get_offset_of__nameSpace_10(),
	DataTable_t3267612424::get_offset_of__childRelations_11(),
	DataTable_t3267612424::get_offset_of__parentRelations_12(),
	DataTable_t3267612424::get_offset_of__prefix_13(),
	DataTable_t3267612424::get_offset_of__primaryKeyConstraint_14(),
	DataTable_t3267612424::get_offset_of__rows_15(),
	DataTable_t3267612424::get_offset_of__site_16(),
	DataTable_t3267612424::get_offset_of__tableName_17(),
	DataTable_t3267612424::get_offset_of__duringDataLoad_18(),
	DataTable_t3267612424::get_offset_of__nullConstraintViolationDuringDataLoad_19(),
	DataTable_t3267612424::get_offset_of_dataSetPrevEnforceConstraints_20(),
	DataTable_t3267612424::get_offset_of_enforceConstraints_21(),
	DataTable_t3267612424::get_offset_of__rowBuilder_22(),
	DataTable_t3267612424::get_offset_of__indexes_23(),
	DataTable_t3267612424::get_offset_of__recordCache_24(),
	DataTable_t3267612424::get_offset_of__defaultValuesRowIndex_25(),
	DataTable_t3267612424::get_offset_of_fInitInProgress_26(),
	DataTable_t3267612424::get_offset_of__virginCaseSensitive_27(),
	DataTable_t3267612424::get_offset_of__propertyDescriptorsCache_28(),
	DataTable_t3267612424_StaticFields::get_offset_of__emptyColumnArray_29(),
	DataTable_t3267612424_StaticFields::get_offset_of_SortRegex_30(),
	DataTable_t3267612424::get_offset_of__latestPrimaryKeyCols_31(),
	DataTable_t3267612424::get_offset_of_empty_rows_32(),
	DataTable_t3267612424::get_offset_of_tableInitialized_33(),
	DataTable_t3267612424::get_offset_of_remotingFormat_34(),
	DataTable_t3267612424::get_offset_of_ColumnChanged_35(),
	DataTable_t3267612424::get_offset_of_ColumnChanging_36(),
	DataTable_t3267612424::get_offset_of_RowChanged_37(),
	DataTable_t3267612424::get_offset_of_RowChanging_38(),
	DataTable_t3267612424::get_offset_of_RowDeleted_39(),
	DataTable_t3267612424::get_offset_of_RowDeleting_40(),
	DataTable_t3267612424::get_offset_of_TableNewRow_41(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2599 = { sizeof (DataTableCollection_t787171642), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2599[3] = 
{
	DataTableCollection_t787171642::get_offset_of_dataSet_2(),
	DataTableCollection_t787171642::get_offset_of_CollectionChanged_3(),
	DataTableCollection_t787171642::get_offset_of_CollectionChanging_4(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
