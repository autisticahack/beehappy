﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.FallbackRegionFactory/RegionGenerator
struct RegionGenerator_t3123742622;
// System.Object
struct Il2CppObject;
// Amazon.Runtime.AWSRegion
struct AWSRegion_t969138115;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Amazon.Runtime.FallbackRegionFactory/RegionGenerator::.ctor(System.Object,System.IntPtr)
extern "C"  void RegionGenerator__ctor_m3513062517 (RegionGenerator_t3123742622 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.AWSRegion Amazon.Runtime.FallbackRegionFactory/RegionGenerator::Invoke()
extern "C"  AWSRegion_t969138115 * RegionGenerator_Invoke_m2245381041 (RegionGenerator_t3123742622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Amazon.Runtime.FallbackRegionFactory/RegionGenerator::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * RegionGenerator_BeginInvoke_m1995828536 (RegionGenerator_t3123742622 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.AWSRegion Amazon.Runtime.FallbackRegionFactory/RegionGenerator::EndInvoke(System.IAsyncResult)
extern "C"  AWSRegion_t969138115 * RegionGenerator_EndInvoke_m4228333267 (RegionGenerator_t3123742622 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
