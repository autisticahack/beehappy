﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "System_Transactions_System_Transactions_Transactio1588908416.h"
#include "System_Transactions_System_Transactions_IsolationL4045174252.h"
#include "mscorlib_System_TimeSpan3430258949.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Transactions.TransactionOptions::.ctor(System.Transactions.IsolationLevel,System.TimeSpan)
extern "C"  void TransactionOptions__ctor_m3596369679 (TransactionOptions_t1588908416 * __this, int32_t ___level0, TimeSpan_t3430258949  ___timeout1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Transactions.TransactionOptions::Equals(System.Object)
extern "C"  bool TransactionOptions_Equals_m1088096644 (TransactionOptions_t1588908416 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Transactions.TransactionOptions::GetHashCode()
extern "C"  int32_t TransactionOptions_GetHashCode_m2584502036 (TransactionOptions_t1588908416 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Transactions.TransactionOptions::op_Equality(System.Transactions.TransactionOptions,System.Transactions.TransactionOptions)
extern "C"  bool TransactionOptions_op_Equality_m1351512005 (Il2CppObject * __this /* static, unused */, TransactionOptions_t1588908416  ___o10, TransactionOptions_t1588908416  ___o21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
