﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.Runtime.ImmutableCredentials
struct ImmutableCredentials_t282353664;

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_DateTime693205669.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.RefreshingAWSCredentials/CredentialsRefreshState
struct  CredentialsRefreshState_t3294867821  : public Il2CppObject
{
public:
	// Amazon.Runtime.ImmutableCredentials Amazon.Runtime.RefreshingAWSCredentials/CredentialsRefreshState::<Credentials>k__BackingField
	ImmutableCredentials_t282353664 * ___U3CCredentialsU3Ek__BackingField_0;
	// System.DateTime Amazon.Runtime.RefreshingAWSCredentials/CredentialsRefreshState::<Expiration>k__BackingField
	DateTime_t693205669  ___U3CExpirationU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CCredentialsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CredentialsRefreshState_t3294867821, ___U3CCredentialsU3Ek__BackingField_0)); }
	inline ImmutableCredentials_t282353664 * get_U3CCredentialsU3Ek__BackingField_0() const { return ___U3CCredentialsU3Ek__BackingField_0; }
	inline ImmutableCredentials_t282353664 ** get_address_of_U3CCredentialsU3Ek__BackingField_0() { return &___U3CCredentialsU3Ek__BackingField_0; }
	inline void set_U3CCredentialsU3Ek__BackingField_0(ImmutableCredentials_t282353664 * value)
	{
		___U3CCredentialsU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCredentialsU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CExpirationU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CredentialsRefreshState_t3294867821, ___U3CExpirationU3Ek__BackingField_1)); }
	inline DateTime_t693205669  get_U3CExpirationU3Ek__BackingField_1() const { return ___U3CExpirationU3Ek__BackingField_1; }
	inline DateTime_t693205669 * get_address_of_U3CExpirationU3Ek__BackingField_1() { return &___U3CExpirationU3Ek__BackingField_1; }
	inline void set_U3CExpirationU3Ek__BackingField_1(DateTime_t693205669  value)
	{
		___U3CExpirationU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
