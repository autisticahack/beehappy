﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AWSSDK_Core_Amazon_Runtime_AmazonWebServiceResponse529043356.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.CognitoIdentity.Model.GetIdResponse
struct  GetIdResponse_t2091118072  : public AmazonWebServiceResponse_t529043356
{
public:
	// System.String Amazon.CognitoIdentity.Model.GetIdResponse::_identityId
	String_t* ____identityId_3;

public:
	inline static int32_t get_offset_of__identityId_3() { return static_cast<int32_t>(offsetof(GetIdResponse_t2091118072, ____identityId_3)); }
	inline String_t* get__identityId_3() const { return ____identityId_3; }
	inline String_t** get_address_of__identityId_3() { return &____identityId_3; }
	inline void set__identityId_3(String_t* value)
	{
		____identityId_3 = value;
		Il2CppCodeGenWriteBarrier(&____identityId_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
