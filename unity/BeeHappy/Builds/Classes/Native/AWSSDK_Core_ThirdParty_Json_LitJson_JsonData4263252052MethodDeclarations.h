﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ThirdParty.Json.LitJson.JsonData
struct JsonData_t4263252052;
// System.Object
struct Il2CppObject;
// System.Collections.ICollection
struct ICollection_t91669223;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t2321347278;
// System.String
struct String_t;
// System.Array
struct Il2CppArray;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t259680273;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.IDictionary
struct IDictionary_t596158605;
// System.Collections.IList
struct IList_t3321498491;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_JsonData4263252052.h"
#include "mscorlib_System_Array3829468939.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_JsonType808352724.h"

// System.Int32 ThirdParty.Json.LitJson.JsonData::get_Count()
extern "C"  int32_t JsonData_get_Count_m1144605257 (JsonData_t4263252052 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ThirdParty.Json.LitJson.JsonData::System.Collections.ICollection.get_Count()
extern "C"  int32_t JsonData_System_Collections_ICollection_get_Count_m3136486028 (JsonData_t4263252052 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ThirdParty.Json.LitJson.JsonData::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool JsonData_System_Collections_ICollection_get_IsSynchronized_m3228980993 (JsonData_t4263252052 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ThirdParty.Json.LitJson.JsonData::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * JsonData_System_Collections_ICollection_get_SyncRoot_m1214308533 (JsonData_t4263252052 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ICollection ThirdParty.Json.LitJson.JsonData::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * JsonData_System_Collections_IDictionary_get_Keys_m2998477023 (JsonData_t4263252052 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ICollection ThirdParty.Json.LitJson.JsonData::System.Collections.IDictionary.get_Values()
extern "C"  Il2CppObject * JsonData_System_Collections_IDictionary_get_Values_m4223920047 (JsonData_t4263252052 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ThirdParty.Json.LitJson.JsonData::System.Collections.IList.get_IsFixedSize()
extern "C"  bool JsonData_System_Collections_IList_get_IsFixedSize_m3467184660 (JsonData_t4263252052 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ThirdParty.Json.LitJson.JsonData::System.Collections.IList.get_IsReadOnly()
extern "C"  bool JsonData_System_Collections_IList_get_IsReadOnly_m1949416349 (JsonData_t4263252052 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ThirdParty.Json.LitJson.JsonData::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * JsonData_System_Collections_IDictionary_get_Item_m1479983749 (JsonData_t4263252052 * __this, Il2CppObject * ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.JsonData::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void JsonData_System_Collections_IDictionary_set_Item_m88929164 (JsonData_t4263252052 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ThirdParty.Json.LitJson.JsonData::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * JsonData_System_Collections_IList_get_Item_m3976338114 (JsonData_t4263252052 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.JsonData::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void JsonData_System_Collections_IList_set_Item_m3245737043 (JsonData_t4263252052 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<System.String> ThirdParty.Json.LitJson.JsonData::get_PropertyNames()
extern "C"  Il2CppObject* JsonData_get_PropertyNames_m2340996541 (JsonData_t4263252052 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ThirdParty.Json.LitJson.JsonData ThirdParty.Json.LitJson.JsonData::get_Item(System.String)
extern "C"  JsonData_t4263252052 * JsonData_get_Item_m3419504576 (JsonData_t4263252052 * __this, String_t* ___prop_name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.JsonData::set_Item(System.String,ThirdParty.Json.LitJson.JsonData)
extern "C"  void JsonData_set_Item_m1201291113 (JsonData_t4263252052 * __this, String_t* ___prop_name0, JsonData_t4263252052 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.JsonData::set_Item(System.Int32,ThirdParty.Json.LitJson.JsonData)
extern "C"  void JsonData_set_Item_m1528363106 (JsonData_t4263252052 * __this, int32_t ___index0, JsonData_t4263252052 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.JsonData::.ctor()
extern "C"  void JsonData__ctor_m2183287813 (JsonData_t4263252052 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.JsonData::.ctor(System.Object)
extern "C"  void JsonData__ctor_m3249347595 (JsonData_t4263252052 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ThirdParty.Json.LitJson.JsonData::op_Explicit(ThirdParty.Json.LitJson.JsonData)
extern "C"  bool JsonData_op_Explicit_m4097164641 (Il2CppObject * __this /* static, unused */, JsonData_t4263252052 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ThirdParty.Json.LitJson.JsonData::op_Explicit(ThirdParty.Json.LitJson.JsonData)
extern "C"  String_t* JsonData_op_Explicit_m1524888292 (Il2CppObject * __this /* static, unused */, JsonData_t4263252052 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.JsonData::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void JsonData_System_Collections_ICollection_CopyTo_m1746890421 (JsonData_t4263252052 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.JsonData::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void JsonData_System_Collections_IDictionary_Add_m180357303 (JsonData_t4263252052 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.JsonData::System.Collections.IDictionary.Clear()
extern "C"  void JsonData_System_Collections_IDictionary_Clear_m289246979 (JsonData_t4263252052 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ThirdParty.Json.LitJson.JsonData::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool JsonData_System_Collections_IDictionary_Contains_m166609687 (JsonData_t4263252052 * __this, Il2CppObject * ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IDictionaryEnumerator ThirdParty.Json.LitJson.JsonData::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * JsonData_System_Collections_IDictionary_GetEnumerator_m1549877592 (JsonData_t4263252052 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.JsonData::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void JsonData_System_Collections_IDictionary_Remove_m2528017504 (JsonData_t4263252052 * __this, Il2CppObject * ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ThirdParty.Json.LitJson.JsonData::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * JsonData_System_Collections_IEnumerable_GetEnumerator_m1963786586 (JsonData_t4263252052 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.JsonData::ThirdParty.Json.LitJson.IJsonWrapper.SetBoolean(System.Boolean)
extern "C"  void JsonData_ThirdParty_Json_LitJson_IJsonWrapper_SetBoolean_m3309887618 (JsonData_t4263252052 * __this, bool ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.JsonData::ThirdParty.Json.LitJson.IJsonWrapper.SetDouble(System.Double)
extern "C"  void JsonData_ThirdParty_Json_LitJson_IJsonWrapper_SetDouble_m1532392792 (JsonData_t4263252052 * __this, double ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.JsonData::ThirdParty.Json.LitJson.IJsonWrapper.SetInt(System.Int32)
extern "C"  void JsonData_ThirdParty_Json_LitJson_IJsonWrapper_SetInt_m2682965327 (JsonData_t4263252052 * __this, int32_t ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.JsonData::ThirdParty.Json.LitJson.IJsonWrapper.SetUInt(System.UInt32)
extern "C"  void JsonData_ThirdParty_Json_LitJson_IJsonWrapper_SetUInt_m2614768943 (JsonData_t4263252052 * __this, uint32_t ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.JsonData::ThirdParty.Json.LitJson.IJsonWrapper.SetLong(System.Int64)
extern "C"  void JsonData_ThirdParty_Json_LitJson_IJsonWrapper_SetLong_m1349946637 (JsonData_t4263252052 * __this, int64_t ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.JsonData::ThirdParty.Json.LitJson.IJsonWrapper.SetULong(System.UInt64)
extern "C"  void JsonData_ThirdParty_Json_LitJson_IJsonWrapper_SetULong_m4270264629 (JsonData_t4263252052 * __this, uint64_t ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.JsonData::ThirdParty.Json.LitJson.IJsonWrapper.SetString(System.String)
extern "C"  void JsonData_ThirdParty_Json_LitJson_IJsonWrapper_SetString_m929399128 (JsonData_t4263252052 * __this, String_t* ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ThirdParty.Json.LitJson.JsonData::System.Collections.IList.Add(System.Object)
extern "C"  int32_t JsonData_System_Collections_IList_Add_m4215642409 (JsonData_t4263252052 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.JsonData::System.Collections.IList.Clear()
extern "C"  void JsonData_System_Collections_IList_Clear_m562217805 (JsonData_t4263252052 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ThirdParty.Json.LitJson.JsonData::System.Collections.IList.Contains(System.Object)
extern "C"  bool JsonData_System_Collections_IList_Contains_m4270132069 (JsonData_t4263252052 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ThirdParty.Json.LitJson.JsonData::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t JsonData_System_Collections_IList_IndexOf_m1091158327 (JsonData_t4263252052 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.JsonData::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void JsonData_System_Collections_IList_Insert_m954013926 (JsonData_t4263252052 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.JsonData::System.Collections.IList.Remove(System.Object)
extern "C"  void JsonData_System_Collections_IList_Remove_m1084822036 (JsonData_t4263252052 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.JsonData::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void JsonData_System_Collections_IList_RemoveAt_m1723154408 (JsonData_t4263252052 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IDictionaryEnumerator ThirdParty.Json.LitJson.JsonData::System.Collections.Specialized.IOrderedDictionary.GetEnumerator()
extern "C"  Il2CppObject * JsonData_System_Collections_Specialized_IOrderedDictionary_GetEnumerator_m1610861286 (JsonData_t4263252052 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ICollection ThirdParty.Json.LitJson.JsonData::EnsureCollection()
extern "C"  Il2CppObject * JsonData_EnsureCollection_m2402886013 (JsonData_t4263252052 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IDictionary ThirdParty.Json.LitJson.JsonData::EnsureDictionary()
extern "C"  Il2CppObject * JsonData_EnsureDictionary_m1008139353 (JsonData_t4263252052 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IList ThirdParty.Json.LitJson.JsonData::EnsureList()
extern "C"  Il2CppObject * JsonData_EnsureList_m466073493 (JsonData_t4263252052 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ThirdParty.Json.LitJson.JsonData ThirdParty.Json.LitJson.JsonData::ToJsonData(System.Object)
extern "C"  JsonData_t4263252052 * JsonData_ToJsonData_m3157715733 (JsonData_t4263252052 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ThirdParty.Json.LitJson.JsonData::Add(System.Object)
extern "C"  int32_t JsonData_Add_m1775657322 (JsonData_t4263252052 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ThirdParty.Json.LitJson.JsonData::Equals(ThirdParty.Json.LitJson.JsonData)
extern "C"  bool JsonData_Equals_m310125024 (JsonData_t4263252052 * __this, JsonData_t4263252052 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.JsonData::SetJsonType(ThirdParty.Json.LitJson.JsonType)
extern "C"  void JsonData_SetJsonType_m3927776733 (JsonData_t4263252052 * __this, int32_t ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ThirdParty.Json.LitJson.JsonData::ToString()
extern "C"  String_t* JsonData_ToString_m3098875818 (JsonData_t4263252052 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
