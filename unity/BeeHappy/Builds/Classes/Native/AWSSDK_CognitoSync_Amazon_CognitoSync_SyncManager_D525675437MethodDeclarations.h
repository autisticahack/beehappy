﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.CognitoSync.SyncManager.Dataset/<>c__DisplayClass40_0
struct U3CU3Ec__DisplayClass40_0_t525675437;
// Amazon.CognitoSync.SyncManager.Record
struct Record_t868799569;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_SyncManager_R868799569.h"

// System.Void Amazon.CognitoSync.SyncManager.Dataset/<>c__DisplayClass40_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass40_0__ctor_m1274309551 (U3CU3Ec__DisplayClass40_0_t525675437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.CognitoSync.SyncManager.Dataset/<>c__DisplayClass40_0::<RunSyncOperation>b__0(Amazon.CognitoSync.SyncManager.Record)
extern "C"  bool U3CU3Ec__DisplayClass40_0_U3CRunSyncOperationU3Eb__0_m919333609 (U3CU3Ec__DisplayClass40_0_t525675437 * __this, Record_t868799569 * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
