﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UpdateText
struct UpdateText_t2942283158;

#include "codegen/il2cpp-codegen.h"

// System.Void UpdateText::.ctor()
extern "C"  void UpdateText__ctor_m2715801899 (UpdateText_t2942283158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UpdateText::Start()
extern "C"  void UpdateText_Start_m2436902519 (UpdateText_t2942283158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UpdateText::Update()
extern "C"  void UpdateText_Update_m2777719396 (UpdateText_t2942283158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
