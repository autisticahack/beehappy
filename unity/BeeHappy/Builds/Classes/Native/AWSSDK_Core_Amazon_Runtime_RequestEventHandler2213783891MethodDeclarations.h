﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.RequestEventHandler
struct RequestEventHandler_t2213783891;
// System.Object
struct Il2CppObject;
// Amazon.Runtime.RequestEventArgs
struct RequestEventArgs_t434225820;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AWSSDK_Core_Amazon_Runtime_RequestEventArgs434225820.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Amazon.Runtime.RequestEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void RequestEventHandler__ctor_m557237492 (RequestEventHandler_t2213783891 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.RequestEventHandler::Invoke(System.Object,Amazon.Runtime.RequestEventArgs)
extern "C"  void RequestEventHandler_Invoke_m80380576 (RequestEventHandler_t2213783891 * __this, Il2CppObject * ___sender0, RequestEventArgs_t434225820 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Amazon.Runtime.RequestEventHandler::BeginInvoke(System.Object,Amazon.Runtime.RequestEventArgs,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * RequestEventHandler_BeginInvoke_m2014594185 (RequestEventHandler_t2213783891 * __this, Il2CppObject * ___sender0, RequestEventArgs_t434225820 * ___e1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.RequestEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void RequestEventHandler_EndInvoke_m249436002 (RequestEventHandler_t2213783891 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
