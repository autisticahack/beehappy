﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Data.Sqlite.SQLite3_UTF16
struct SQLite3_UTF16_t3316796172;
// System.String
struct String_t;
// Mono.Data.Sqlite.SqliteStatement
struct SqliteStatement_t4106757957;

#include "codegen/il2cpp-codegen.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SQLiteDateFormat3821473988.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_String2029220233.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SQLiteOpenFlagsE1776899264.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteStatement4106757957.h"
#include "mscorlib_System_DateTime693205669.h"

// System.Void Mono.Data.Sqlite.SQLite3_UTF16::.ctor(Mono.Data.Sqlite.SQLiteDateFormats)
extern "C"  void SQLite3_UTF16__ctor_m2465485424 (SQLite3_UTF16_t3316796172 * __this, int32_t ___fmt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Data.Sqlite.SQLite3_UTF16::ToString(System.IntPtr,System.Int32)
extern "C"  String_t* SQLite3_UTF16_ToString_m588079703 (SQLite3_UTF16_t3316796172 * __this, IntPtr_t ___b0, int32_t ___nbytelen1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Data.Sqlite.SQLite3_UTF16::UTF16ToString(System.IntPtr,System.Int32)
extern "C"  String_t* SQLite3_UTF16_UTF16ToString_m4190961363 (Il2CppObject * __this /* static, unused */, IntPtr_t ___b0, int32_t ___nbytelen1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SQLite3_UTF16::Open(System.String,Mono.Data.Sqlite.SQLiteOpenFlagsEnum,System.Int32,System.Boolean)
extern "C"  void SQLite3_UTF16_Open_m871029912 (SQLite3_UTF16_t3316796172 * __this, String_t* ___strFilename0, int32_t ___flags1, int32_t ___maxPoolSize2, bool ___usePool3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SQLite3_UTF16::Bind_DateTime(Mono.Data.Sqlite.SqliteStatement,System.Int32,System.DateTime)
extern "C"  void SQLite3_UTF16_Bind_DateTime_m2300737003 (SQLite3_UTF16_t3316796172 * __this, SqliteStatement_t4106757957 * ___stmt0, int32_t ___index1, DateTime_t693205669  ___dt2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SQLite3_UTF16::Bind_Text(Mono.Data.Sqlite.SqliteStatement,System.Int32,System.String)
extern "C"  void SQLite3_UTF16_Bind_Text_m982093103 (SQLite3_UTF16_t3316796172 * __this, SqliteStatement_t4106757957 * ___stmt0, int32_t ___index1, String_t* ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime Mono.Data.Sqlite.SQLite3_UTF16::GetDateTime(Mono.Data.Sqlite.SqliteStatement,System.Int32)
extern "C"  DateTime_t693205669  SQLite3_UTF16_GetDateTime_m3816364436 (SQLite3_UTF16_t3316796172 * __this, SqliteStatement_t4106757957 * ___stmt0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Data.Sqlite.SQLite3_UTF16::ColumnName(Mono.Data.Sqlite.SqliteStatement,System.Int32)
extern "C"  String_t* SQLite3_UTF16_ColumnName_m1469175946 (SQLite3_UTF16_t3316796172 * __this, SqliteStatement_t4106757957 * ___stmt0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Data.Sqlite.SQLite3_UTF16::GetText(Mono.Data.Sqlite.SqliteStatement,System.Int32)
extern "C"  String_t* SQLite3_UTF16_GetText_m1925035862 (SQLite3_UTF16_t3316796172 * __this, SqliteStatement_t4106757957 * ___stmt0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Data.Sqlite.SQLite3_UTF16::GetParamValueText(System.IntPtr)
extern "C"  String_t* SQLite3_UTF16_GetParamValueText_m761464543 (SQLite3_UTF16_t3316796172 * __this, IntPtr_t ___ptr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SQLite3_UTF16::ReturnError(System.IntPtr,System.String)
extern "C"  void SQLite3_UTF16_ReturnError_m4017073017 (SQLite3_UTF16_t3316796172 * __this, IntPtr_t ___context0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SQLite3_UTF16::ReturnText(System.IntPtr,System.String)
extern "C"  void SQLite3_UTF16_ReturnText_m1638527140 (SQLite3_UTF16_t3316796172 * __this, IntPtr_t ___context0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
