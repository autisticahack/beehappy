﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4146492248.h"
#include "mscorlib_System_Array3829468939.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_PropertyMetada3287739986.h"

// System.Void System.Array/InternalEnumerator`1<ThirdParty.Json.LitJson.PropertyMetadata>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2195808295_gshared (InternalEnumerator_1_t4146492248 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m2195808295(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t4146492248 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2195808295_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<ThirdParty.Json.LitJson.PropertyMetadata>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1488024775_gshared (InternalEnumerator_1_t4146492248 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1488024775(__this, method) ((  void (*) (InternalEnumerator_1_t4146492248 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1488024775_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<ThirdParty.Json.LitJson.PropertyMetadata>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3085000283_gshared (InternalEnumerator_1_t4146492248 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3085000283(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t4146492248 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3085000283_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<ThirdParty.Json.LitJson.PropertyMetadata>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m349078886_gshared (InternalEnumerator_1_t4146492248 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m349078886(__this, method) ((  void (*) (InternalEnumerator_1_t4146492248 *, const MethodInfo*))InternalEnumerator_1_Dispose_m349078886_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<ThirdParty.Json.LitJson.PropertyMetadata>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2691932099_gshared (InternalEnumerator_1_t4146492248 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2691932099(__this, method) ((  bool (*) (InternalEnumerator_1_t4146492248 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2691932099_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<ThirdParty.Json.LitJson.PropertyMetadata>::get_Current()
extern "C"  PropertyMetadata_t3287739986  InternalEnumerator_1_get_Current_m2096054752_gshared (InternalEnumerator_1_t4146492248 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m2096054752(__this, method) ((  PropertyMetadata_t3287739986  (*) (InternalEnumerator_1_t4146492248 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2096054752_gshared)(__this, method)
