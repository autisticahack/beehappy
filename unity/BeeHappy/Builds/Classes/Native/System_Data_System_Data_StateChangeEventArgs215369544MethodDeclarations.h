﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Data.StateChangeEventArgs
struct StateChangeEventArgs_t215369544;

#include "codegen/il2cpp-codegen.h"
#include "System_Data_System_Data_ConnectionState2517440941.h"

// System.Void System.Data.StateChangeEventArgs::.ctor(System.Data.ConnectionState,System.Data.ConnectionState)
extern "C"  void StateChangeEventArgs__ctor_m2417330271 (StateChangeEventArgs_t215369544 * __this, int32_t ___originalState0, int32_t ___currentState1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
