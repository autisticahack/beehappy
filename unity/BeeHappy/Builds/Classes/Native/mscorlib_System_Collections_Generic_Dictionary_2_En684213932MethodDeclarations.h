﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Int64>
struct Dictionary_2_t3659156526;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En684213932.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21416501748.h"
#include "AWSSDK_Core_Amazon_Runtime_Metric3273440202.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Int64>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1188491919_gshared (Enumerator_t684213932 * __this, Dictionary_2_t3659156526 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m1188491919(__this, ___dictionary0, method) ((  void (*) (Enumerator_t684213932 *, Dictionary_2_t3659156526 *, const MethodInfo*))Enumerator__ctor_m1188491919_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Int64>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1574127952_gshared (Enumerator_t684213932 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1574127952(__this, method) ((  Il2CppObject * (*) (Enumerator_t684213932 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1574127952_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Int64>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1235301678_gshared (Enumerator_t684213932 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1235301678(__this, method) ((  void (*) (Enumerator_t684213932 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1235301678_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Int64>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1213929309_gshared (Enumerator_t684213932 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1213929309(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t684213932 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1213929309_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Int64>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2871216018_gshared (Enumerator_t684213932 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2871216018(__this, method) ((  Il2CppObject * (*) (Enumerator_t684213932 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2871216018_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Int64>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m800838336_gshared (Enumerator_t684213932 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m800838336(__this, method) ((  Il2CppObject * (*) (Enumerator_t684213932 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m800838336_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Int64>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1520629361_gshared (Enumerator_t684213932 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1520629361(__this, method) ((  bool (*) (Enumerator_t684213932 *, const MethodInfo*))Enumerator_MoveNext_m1520629361_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Int64>::get_Current()
extern "C"  KeyValuePair_2_t1416501748  Enumerator_get_Current_m2197039620_gshared (Enumerator_t684213932 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2197039620(__this, method) ((  KeyValuePair_2_t1416501748  (*) (Enumerator_t684213932 *, const MethodInfo*))Enumerator_get_Current_m2197039620_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Int64>::get_CurrentKey()
extern "C"  int32_t Enumerator_get_CurrentKey_m2231206827_gshared (Enumerator_t684213932 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m2231206827(__this, method) ((  int32_t (*) (Enumerator_t684213932 *, const MethodInfo*))Enumerator_get_CurrentKey_m2231206827_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Int64>::get_CurrentValue()
extern "C"  int64_t Enumerator_get_CurrentValue_m2007594771_gshared (Enumerator_t684213932 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m2007594771(__this, method) ((  int64_t (*) (Enumerator_t684213932 *, const MethodInfo*))Enumerator_get_CurrentValue_m2007594771_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Int64>::Reset()
extern "C"  void Enumerator_Reset_m1537962437_gshared (Enumerator_t684213932 * __this, const MethodInfo* method);
#define Enumerator_Reset_m1537962437(__this, method) ((  void (*) (Enumerator_t684213932 *, const MethodInfo*))Enumerator_Reset_m1537962437_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Int64>::VerifyState()
extern "C"  void Enumerator_VerifyState_m1404444218_gshared (Enumerator_t684213932 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m1404444218(__this, method) ((  void (*) (Enumerator_t684213932 *, const MethodInfo*))Enumerator_VerifyState_m1404444218_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Int64>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m2735614142_gshared (Enumerator_t684213932 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m2735614142(__this, method) ((  void (*) (Enumerator_t684213932 *, const MethodInfo*))Enumerator_VerifyCurrent_m2735614142_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Int64>::Dispose()
extern "C"  void Enumerator_Dispose_m2961717971_gshared (Enumerator_t684213932 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2961717971(__this, method) ((  void (*) (Enumerator_t684213932 *, const MethodInfo*))Enumerator_Dispose_m2961717971_gshared)(__this, method)
