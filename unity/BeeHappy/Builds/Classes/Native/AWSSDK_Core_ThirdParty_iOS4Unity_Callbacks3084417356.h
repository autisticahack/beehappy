﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.String,System.Delegate>
struct Dictionary_2_t642288257;
// System.Collections.Generic.Dictionary`2<System.IntPtr,System.Collections.Generic.Dictionary`2<System.IntPtr,ThirdParty.iOS4Unity.Callbacks/Methods>>
struct Dictionary_2_t2071948110;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.iOS4Unity.Callbacks
struct  Callbacks_t3084417356  : public Il2CppObject
{
public:

public:
};

struct Callbacks_t3084417356_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Delegate> ThirdParty.iOS4Unity.Callbacks::_delegates
	Dictionary_2_t642288257 * ____delegates_0;
	// System.Collections.Generic.Dictionary`2<System.IntPtr,System.Collections.Generic.Dictionary`2<System.IntPtr,ThirdParty.iOS4Unity.Callbacks/Methods>> ThirdParty.iOS4Unity.Callbacks::_callbacks
	Dictionary_2_t2071948110 * ____callbacks_1;

public:
	inline static int32_t get_offset_of__delegates_0() { return static_cast<int32_t>(offsetof(Callbacks_t3084417356_StaticFields, ____delegates_0)); }
	inline Dictionary_2_t642288257 * get__delegates_0() const { return ____delegates_0; }
	inline Dictionary_2_t642288257 ** get_address_of__delegates_0() { return &____delegates_0; }
	inline void set__delegates_0(Dictionary_2_t642288257 * value)
	{
		____delegates_0 = value;
		Il2CppCodeGenWriteBarrier(&____delegates_0, value);
	}

	inline static int32_t get_offset_of__callbacks_1() { return static_cast<int32_t>(offsetof(Callbacks_t3084417356_StaticFields, ____callbacks_1)); }
	inline Dictionary_2_t2071948110 * get__callbacks_1() const { return ____callbacks_1; }
	inline Dictionary_2_t2071948110 ** get_address_of__callbacks_1() { return &____callbacks_1; }
	inline void set__callbacks_1(Dictionary_2_t2071948110 * value)
	{
		____callbacks_1 = value;
		Il2CppCodeGenWriteBarrier(&____callbacks_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
