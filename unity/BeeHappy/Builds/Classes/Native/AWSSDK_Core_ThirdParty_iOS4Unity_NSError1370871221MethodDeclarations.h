﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ThirdParty.iOS4Unity.NSError
struct NSError_t1370871221;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr2504060609.h"

// System.Void ThirdParty.iOS4Unity.NSError::.cctor()
extern "C"  void NSError__cctor_m2801557134 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.iOS4Unity.NSError::.ctor(System.IntPtr)
extern "C"  void NSError__ctor_m1269248803 (NSError_t1370871221 * __this, IntPtr_t ___handle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ThirdParty.iOS4Unity.NSError::get_LocalizedDescription()
extern "C"  String_t* NSError_get_LocalizedDescription_m2148383518 (NSError_t1370871221 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ThirdParty.iOS4Unity.NSError::ToString()
extern "C"  String_t* NSError_ToString_m2159347078 (NSError_t1370871221 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
