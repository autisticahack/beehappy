﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Exception
struct Exception_t1927440687;
// Amazon.SecurityToken.Model.Credentials
struct Credentials_t3554032640;
// System.Threading.AutoResetEvent
struct AutoResetEvent_t15112628;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.CognitoIdentity.CognitoAWSCredentials/<>c__DisplayClass59_0
struct  U3CU3Ec__DisplayClass59_0_t795329086  : public Il2CppObject
{
public:
	// System.Exception Amazon.CognitoIdentity.CognitoAWSCredentials/<>c__DisplayClass59_0::exception
	Exception_t1927440687 * ___exception_0;
	// Amazon.SecurityToken.Model.Credentials Amazon.CognitoIdentity.CognitoAWSCredentials/<>c__DisplayClass59_0::credentials
	Credentials_t3554032640 * ___credentials_1;
	// System.Threading.AutoResetEvent Amazon.CognitoIdentity.CognitoAWSCredentials/<>c__DisplayClass59_0::ars
	AutoResetEvent_t15112628 * ___ars_2;

public:
	inline static int32_t get_offset_of_exception_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass59_0_t795329086, ___exception_0)); }
	inline Exception_t1927440687 * get_exception_0() const { return ___exception_0; }
	inline Exception_t1927440687 ** get_address_of_exception_0() { return &___exception_0; }
	inline void set_exception_0(Exception_t1927440687 * value)
	{
		___exception_0 = value;
		Il2CppCodeGenWriteBarrier(&___exception_0, value);
	}

	inline static int32_t get_offset_of_credentials_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass59_0_t795329086, ___credentials_1)); }
	inline Credentials_t3554032640 * get_credentials_1() const { return ___credentials_1; }
	inline Credentials_t3554032640 ** get_address_of_credentials_1() { return &___credentials_1; }
	inline void set_credentials_1(Credentials_t3554032640 * value)
	{
		___credentials_1 = value;
		Il2CppCodeGenWriteBarrier(&___credentials_1, value);
	}

	inline static int32_t get_offset_of_ars_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass59_0_t795329086, ___ars_2)); }
	inline AutoResetEvent_t15112628 * get_ars_2() const { return ___ars_2; }
	inline AutoResetEvent_t15112628 ** get_address_of_ars_2() { return &___ars_2; }
	inline void set_ars_2(AutoResetEvent_t15112628 * value)
	{
		___ars_2 = value;
		Il2CppCodeGenWriteBarrier(&___ars_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
