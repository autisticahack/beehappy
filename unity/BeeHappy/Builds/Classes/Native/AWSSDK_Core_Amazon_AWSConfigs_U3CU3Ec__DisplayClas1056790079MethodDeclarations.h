﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.AWSConfigs/<>c__DisplayClass91_0
struct U3CU3Ec__DisplayClass91_0_t1056790079;
// System.Xml.Linq.XAttribute
struct XAttribute_t3858477518;
// System.Xml.Linq.XElement
struct XElement_t553821050;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_Linq_System_Xml_Linq_XAttribute3858477518.h"
#include "System_Xml_Linq_System_Xml_Linq_XElement553821050.h"

// System.Void Amazon.AWSConfigs/<>c__DisplayClass91_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass91_0__ctor_m1098351842 (U3CU3Ec__DisplayClass91_0_t1056790079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.AWSConfigs/<>c__DisplayClass91_0::<GetObject>b__0(System.Xml.Linq.XAttribute)
extern "C"  bool U3CU3Ec__DisplayClass91_0_U3CGetObjectU3Eb__0_m2719004925 (U3CU3Ec__DisplayClass91_0_t1056790079 * __this, XAttribute_t3858477518 * ___a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.AWSConfigs/<>c__DisplayClass91_0::<GetObject>b__1(System.Xml.Linq.XElement)
extern "C"  bool U3CU3Ec__DisplayClass91_0_U3CGetObjectU3Eb__1_m2709135080 (U3CU3Ec__DisplayClass91_0_t1056790079 * __this, XElement_t553821050 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
