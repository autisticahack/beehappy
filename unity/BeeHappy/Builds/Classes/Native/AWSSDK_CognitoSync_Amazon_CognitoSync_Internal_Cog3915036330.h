﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.Util.Internal.ITypeInfo
struct ITypeInfo_t3592676621;
// System.Reflection.PropertyInfo
struct PropertyInfo_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.CognitoSync.Internal.CognitoCredentialsRetriever/CSRequestCache/CSRequest
struct  CSRequest_t3915036330  : public Il2CppObject
{
public:
	// Amazon.Util.Internal.ITypeInfo Amazon.CognitoSync.Internal.CognitoCredentialsRetriever/CSRequestCache/CSRequest::requestType
	Il2CppObject * ___requestType_0;
	// System.Reflection.PropertyInfo Amazon.CognitoSync.Internal.CognitoCredentialsRetriever/CSRequestCache/CSRequest::identityPoolIdProperty
	PropertyInfo_t * ___identityPoolIdProperty_1;
	// System.Reflection.PropertyInfo Amazon.CognitoSync.Internal.CognitoCredentialsRetriever/CSRequestCache/CSRequest::identityIdProperty
	PropertyInfo_t * ___identityIdProperty_2;

public:
	inline static int32_t get_offset_of_requestType_0() { return static_cast<int32_t>(offsetof(CSRequest_t3915036330, ___requestType_0)); }
	inline Il2CppObject * get_requestType_0() const { return ___requestType_0; }
	inline Il2CppObject ** get_address_of_requestType_0() { return &___requestType_0; }
	inline void set_requestType_0(Il2CppObject * value)
	{
		___requestType_0 = value;
		Il2CppCodeGenWriteBarrier(&___requestType_0, value);
	}

	inline static int32_t get_offset_of_identityPoolIdProperty_1() { return static_cast<int32_t>(offsetof(CSRequest_t3915036330, ___identityPoolIdProperty_1)); }
	inline PropertyInfo_t * get_identityPoolIdProperty_1() const { return ___identityPoolIdProperty_1; }
	inline PropertyInfo_t ** get_address_of_identityPoolIdProperty_1() { return &___identityPoolIdProperty_1; }
	inline void set_identityPoolIdProperty_1(PropertyInfo_t * value)
	{
		___identityPoolIdProperty_1 = value;
		Il2CppCodeGenWriteBarrier(&___identityPoolIdProperty_1, value);
	}

	inline static int32_t get_offset_of_identityIdProperty_2() { return static_cast<int32_t>(offsetof(CSRequest_t3915036330, ___identityIdProperty_2)); }
	inline PropertyInfo_t * get_identityIdProperty_2() const { return ___identityIdProperty_2; }
	inline PropertyInfo_t ** get_address_of_identityIdProperty_2() { return &___identityIdProperty_2; }
	inline void set_identityIdProperty_2(PropertyInfo_t * value)
	{
		___identityIdProperty_2 = value;
		Il2CppCodeGenWriteBarrier(&___identityIdProperty_2, value);
	}
};

struct CSRequest_t3915036330_StaticFields
{
public:
	// Amazon.Util.Internal.ITypeInfo Amazon.CognitoSync.Internal.CognitoCredentialsRetriever/CSRequestCache/CSRequest::SyncRequestType
	Il2CppObject * ___SyncRequestType_3;

public:
	inline static int32_t get_offset_of_SyncRequestType_3() { return static_cast<int32_t>(offsetof(CSRequest_t3915036330_StaticFields, ___SyncRequestType_3)); }
	inline Il2CppObject * get_SyncRequestType_3() const { return ___SyncRequestType_3; }
	inline Il2CppObject ** get_address_of_SyncRequestType_3() { return &___SyncRequestType_3; }
	inline void set_SyncRequestType_3(Il2CppObject * value)
	{
		___SyncRequestType_3 = value;
		Il2CppCodeGenWriteBarrier(&___SyncRequestType_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
