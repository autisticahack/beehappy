﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.CognitoSync.Model.DeleteDatasetResponse
struct DeleteDatasetResponse_t2927760190;
// Amazon.CognitoSync.Model.Dataset
struct Dataset_t149289424;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_Model_Dataset149289424.h"

// System.Void Amazon.CognitoSync.Model.DeleteDatasetResponse::set_Dataset(Amazon.CognitoSync.Model.Dataset)
extern "C"  void DeleteDatasetResponse_set_Dataset_m3015565085 (DeleteDatasetResponse_t2927760190 * __this, Dataset_t149289424 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Model.DeleteDatasetResponse::.ctor()
extern "C"  void DeleteDatasetResponse__ctor_m4100064467 (DeleteDatasetResponse_t2927760190 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
