﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ThirdParty.Json.LitJson.JsonReader
struct JsonReader_t354941621;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IO.TextReader
struct TextReader_t1561828458;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_JsonToken2445581255.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_IO_TextReader1561828458.h"

// ThirdParty.Json.LitJson.JsonToken ThirdParty.Json.LitJson.JsonReader::get_Token()
extern "C"  int32_t JsonReader_get_Token_m2164259962 (JsonReader_t354941621 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ThirdParty.Json.LitJson.JsonReader::get_Value()
extern "C"  Il2CppObject * JsonReader_get_Value_m158192595 (JsonReader_t354941621 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.JsonReader::.ctor(System.String)
extern "C"  void JsonReader__ctor_m4018925912 (JsonReader_t354941621 * __this, String_t* ___json_text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.JsonReader::.ctor(System.IO.TextReader)
extern "C"  void JsonReader__ctor_m690207655 (JsonReader_t354941621 * __this, TextReader_t1561828458 * ___reader0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.JsonReader::.ctor(System.IO.TextReader,System.Boolean)
extern "C"  void JsonReader__ctor_m1593852644 (JsonReader_t354941621 * __this, TextReader_t1561828458 * ___reader0, bool ___owned1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.JsonReader::ProcessNumber(System.String)
extern "C"  void JsonReader_ProcessNumber_m1347979804 (JsonReader_t354941621 * __this, String_t* ___number0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.JsonReader::ProcessSymbol()
extern "C"  void JsonReader_ProcessSymbol_m1556389329 (JsonReader_t354941621 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ThirdParty.Json.LitJson.JsonReader::ReadToken()
extern "C"  bool JsonReader_ReadToken_m2866069121 (JsonReader_t354941621 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.JsonReader::Close()
extern "C"  void JsonReader_Close_m1145586022 (JsonReader_t354941621 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ThirdParty.Json.LitJson.JsonReader::Read()
extern "C"  bool JsonReader_Read_m3114967242 (JsonReader_t354941621 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
