﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.CredentialsRetriever
struct CredentialsRetriever_t208022274;
// Amazon.Runtime.AWSCredentials
struct AWSCredentials_t3583921007;
// Amazon.Runtime.IExecutionContext
struct IExecutionContext_t2477130752;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// Amazon.Runtime.IAsyncExecutionContext
struct IAsyncExecutionContext_t3792344986;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_Runtime_AWSCredentials3583921007.h"

// System.Void Amazon.Runtime.Internal.CredentialsRetriever::.ctor(Amazon.Runtime.AWSCredentials)
extern "C"  void CredentialsRetriever__ctor_m1964840887 (CredentialsRetriever_t208022274 * __this, AWSCredentials_t3583921007 * ___credentials0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.AWSCredentials Amazon.Runtime.Internal.CredentialsRetriever::get_Credentials()
extern "C"  AWSCredentials_t3583921007 * CredentialsRetriever_get_Credentials_m555460133 (CredentialsRetriever_t208022274 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.CredentialsRetriever::set_Credentials(Amazon.Runtime.AWSCredentials)
extern "C"  void CredentialsRetriever_set_Credentials_m328509754 (CredentialsRetriever_t208022274 * __this, AWSCredentials_t3583921007 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.CredentialsRetriever::PreInvoke(Amazon.Runtime.IExecutionContext)
extern "C"  void CredentialsRetriever_PreInvoke_m3180113277 (CredentialsRetriever_t208022274 * __this, Il2CppObject * ___executionContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.CredentialsRetriever::InvokeSync(Amazon.Runtime.IExecutionContext)
extern "C"  void CredentialsRetriever_InvokeSync_m413936253 (CredentialsRetriever_t208022274 * __this, Il2CppObject * ___executionContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Amazon.Runtime.Internal.CredentialsRetriever::InvokeAsync(Amazon.Runtime.IAsyncExecutionContext)
extern "C"  Il2CppObject * CredentialsRetriever_InvokeAsync_m3169409672 (CredentialsRetriever_t208022274 * __this, Il2CppObject * ___executionContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
