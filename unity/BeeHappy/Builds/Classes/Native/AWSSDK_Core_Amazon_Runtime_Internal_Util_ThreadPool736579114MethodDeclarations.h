﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Util_ThreadPoo3928650719MethodDeclarations.h"

// System.Int32 Amazon.Runtime.Internal.Util.ThreadPoolThrottler`1<Amazon.Runtime.IAsyncExecutionContext>::get_MaxConcurentRequest()
#define ThreadPoolThrottler_1_get_MaxConcurentRequest_m1657091278(__this, method) ((  int32_t (*) (ThreadPoolThrottler_1_t736579114 *, const MethodInfo*))ThreadPoolThrottler_1_get_MaxConcurentRequest_m2980732324_gshared)(__this, method)
// System.Void Amazon.Runtime.Internal.Util.ThreadPoolThrottler`1<Amazon.Runtime.IAsyncExecutionContext>::set_MaxConcurentRequest(System.Int32)
#define ThreadPoolThrottler_1_set_MaxConcurentRequest_m1008232229(__this, ___value0, method) ((  void (*) (ThreadPoolThrottler_1_t736579114 *, int32_t, const MethodInfo*))ThreadPoolThrottler_1_set_MaxConcurentRequest_m267262387_gshared)(__this, ___value0, method)
// System.Void Amazon.Runtime.Internal.Util.ThreadPoolThrottler`1<Amazon.Runtime.IAsyncExecutionContext>::.ctor(System.Int32)
#define ThreadPoolThrottler_1__ctor_m4126104536(__this, ___maxConcurrentRequests0, method) ((  void (*) (ThreadPoolThrottler_1_t736579114 *, int32_t, const MethodInfo*))ThreadPoolThrottler_1__ctor_m378429414_gshared)(__this, ___maxConcurrentRequests0, method)
// System.Void Amazon.Runtime.Internal.Util.ThreadPoolThrottler`1<Amazon.Runtime.IAsyncExecutionContext>::Enqueue(T,System.Action`1<T>,System.Action`2<System.Exception,T>)
#define ThreadPoolThrottler_1_Enqueue_m3207928986(__this, ___executionContext0, ___callback1, ___errorCallback2, method) ((  void (*) (ThreadPoolThrottler_1_t736579114 *, Il2CppObject *, Action_1_t3594144368 *, Action_2_t1841170552 *, const MethodInfo*))ThreadPoolThrottler_1_Enqueue_m4160389796_gshared)(__this, ___executionContext0, ___callback1, ___errorCallback2, method)
// System.Void Amazon.Runtime.Internal.Util.ThreadPoolThrottler`1<Amazon.Runtime.IAsyncExecutionContext>::Callback(System.Object)
#define ThreadPoolThrottler_1_Callback_m2258161106(__this, ___state0, method) ((  void (*) (ThreadPoolThrottler_1_t736579114 *, Il2CppObject *, const MethodInfo*))ThreadPoolThrottler_1_Callback_m3696307736_gshared)(__this, ___state0, method)
// System.Void Amazon.Runtime.Internal.Util.ThreadPoolThrottler`1<Amazon.Runtime.IAsyncExecutionContext>::SignalCompletion()
#define ThreadPoolThrottler_1_SignalCompletion_m2635601827(__this, method) ((  void (*) (ThreadPoolThrottler_1_t736579114 *, const MethodInfo*))ThreadPoolThrottler_1_SignalCompletion_m3971342417_gshared)(__this, method)
// System.Void Amazon.Runtime.Internal.Util.ThreadPoolThrottler`1<Amazon.Runtime.IAsyncExecutionContext>::.cctor()
#define ThreadPoolThrottler_1__cctor_m2800832512(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ThreadPoolThrottler_1__cctor_m1038399746_gshared)(__this /* static, unused */, method)
