﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Data.Sqlite.SQLiteUpdateEventHandler
struct SQLiteUpdateEventHandler_t857020267;
// System.Object
struct Il2CppObject;
// Mono.Data.Sqlite.UpdateEventArgs
struct UpdateEventArgs_t978982032;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_UpdateEventArgs978982032.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Mono.Data.Sqlite.SQLiteUpdateEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void SQLiteUpdateEventHandler__ctor_m3851456860 (SQLiteUpdateEventHandler_t857020267 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SQLiteUpdateEventHandler::Invoke(System.Object,Mono.Data.Sqlite.UpdateEventArgs)
extern "C"  void SQLiteUpdateEventHandler_Invoke_m3072518897 (SQLiteUpdateEventHandler_t857020267 * __this, Il2CppObject * ___sender0, UpdateEventArgs_t978982032 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Mono.Data.Sqlite.SQLiteUpdateEventHandler::BeginInvoke(System.Object,Mono.Data.Sqlite.UpdateEventArgs,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * SQLiteUpdateEventHandler_BeginInvoke_m4165085528 (SQLiteUpdateEventHandler_t857020267 * __this, Il2CppObject * ___sender0, UpdateEventArgs_t978982032 * ___e1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SQLiteUpdateEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void SQLiteUpdateEventHandler_EndInvoke_m3351590694 (SQLiteUpdateEventHandler_t857020267 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
