﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityStandardAssets.ImageEffects.ContrastStretch
struct ContrastStretch_t3133742431;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;
// UnityEngine.Texture
struct Texture_t2243626319;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"
#include "UnityEngine_UnityEngine_Texture2243626319.h"

// System.Void UnityStandardAssets.ImageEffects.ContrastStretch::.ctor()
extern "C"  void ContrastStretch__ctor_m3000707324 (ContrastStretch_t3133742431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material UnityStandardAssets.ImageEffects.ContrastStretch::get_materialLum()
extern "C"  Material_t193706927 * ContrastStretch_get_materialLum_m3655799691 (ContrastStretch_t3133742431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material UnityStandardAssets.ImageEffects.ContrastStretch::get_materialReduce()
extern "C"  Material_t193706927 * ContrastStretch_get_materialReduce_m3519179691 (ContrastStretch_t3133742431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material UnityStandardAssets.ImageEffects.ContrastStretch::get_materialAdapt()
extern "C"  Material_t193706927 * ContrastStretch_get_materialAdapt_m2290505067 (ContrastStretch_t3133742431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material UnityStandardAssets.ImageEffects.ContrastStretch::get_materialApply()
extern "C"  Material_t193706927 * ContrastStretch_get_materialApply_m2237142327 (ContrastStretch_t3133742431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.ImageEffects.ContrastStretch::Start()
extern "C"  void ContrastStretch_Start_m389283952 (ContrastStretch_t3133742431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.ImageEffects.ContrastStretch::OnEnable()
extern "C"  void ContrastStretch_OnEnable_m2297214608 (ContrastStretch_t3133742431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.ImageEffects.ContrastStretch::OnDisable()
extern "C"  void ContrastStretch_OnDisable_m3247526803 (ContrastStretch_t3133742431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.ImageEffects.ContrastStretch::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void ContrastStretch_OnRenderImage_m3778957864 (ContrastStretch_t3133742431 * __this, RenderTexture_t2666733923 * ___source0, RenderTexture_t2666733923 * ___destination1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.ImageEffects.ContrastStretch::CalculateAdaptation(UnityEngine.Texture)
extern "C"  void ContrastStretch_CalculateAdaptation_m4262495939 (ContrastStretch_t3133742431 * __this, Texture_t2243626319 * ___curTexture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
