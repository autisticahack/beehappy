﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ThirdParty.Json.LitJson.Lexer
struct Lexer_t954714164;
// System.String
struct String_t;
// System.IO.TextReader
struct TextReader_t1561828458;
// ThirdParty.Json.LitJson.FsmContext
struct FsmContext_t4275593467;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_TextReader1561828458.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_FsmContext4275593467.h"

// System.Boolean ThirdParty.Json.LitJson.Lexer::get_EndOfInput()
extern "C"  bool Lexer_get_EndOfInput_m3261687552 (Lexer_t954714164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ThirdParty.Json.LitJson.Lexer::get_Token()
extern "C"  int32_t Lexer_get_Token_m4181743523 (Lexer_t954714164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ThirdParty.Json.LitJson.Lexer::get_StringValue()
extern "C"  String_t* Lexer_get_StringValue_m2285872777 (Lexer_t954714164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.Lexer::.cctor()
extern "C"  void Lexer__cctor_m2221215066 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.Lexer::.ctor(System.IO.TextReader)
extern "C"  void Lexer__ctor_m786021532 (Lexer_t954714164 * __this, TextReader_t1561828458 * ___reader0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ThirdParty.Json.LitJson.Lexer::HexValue(System.Int32)
extern "C"  int32_t Lexer_HexValue_m4231875178 (Il2CppObject * __this /* static, unused */, int32_t ___digit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.Lexer::PopulateFsmTables()
extern "C"  void Lexer_PopulateFsmTables_m484075034 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char ThirdParty.Json.LitJson.Lexer::ProcessEscChar(System.Int32)
extern "C"  Il2CppChar Lexer_ProcessEscChar_m530821430 (Il2CppObject * __this /* static, unused */, int32_t ___esc_char0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ThirdParty.Json.LitJson.Lexer::State1(ThirdParty.Json.LitJson.FsmContext)
extern "C"  bool Lexer_State1_m2106225368 (Il2CppObject * __this /* static, unused */, FsmContext_t4275593467 * ___ctx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ThirdParty.Json.LitJson.Lexer::State2(ThirdParty.Json.LitJson.FsmContext)
extern "C"  bool Lexer_State2_m934338055 (Il2CppObject * __this /* static, unused */, FsmContext_t4275593467 * ___ctx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ThirdParty.Json.LitJson.Lexer::State3(ThirdParty.Json.LitJson.FsmContext)
extern "C"  bool Lexer_State3_m3343688098 (Il2CppObject * __this /* static, unused */, FsmContext_t4275593467 * ___ctx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ThirdParty.Json.LitJson.Lexer::State4(ThirdParty.Json.LitJson.FsmContext)
extern "C"  bool Lexer_State4_m271821993 (Il2CppObject * __this /* static, unused */, FsmContext_t4275593467 * ___ctx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ThirdParty.Json.LitJson.Lexer::State5(ThirdParty.Json.LitJson.FsmContext)
extern "C"  bool Lexer_State5_m2681172036 (Il2CppObject * __this /* static, unused */, FsmContext_t4275593467 * ___ctx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ThirdParty.Json.LitJson.Lexer::State6(ThirdParty.Json.LitJson.FsmContext)
extern "C"  bool Lexer_State6_m1509284723 (Il2CppObject * __this /* static, unused */, FsmContext_t4275593467 * ___ctx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ThirdParty.Json.LitJson.Lexer::State7(ThirdParty.Json.LitJson.FsmContext)
extern "C"  bool Lexer_State7_m3918634766 (Il2CppObject * __this /* static, unused */, FsmContext_t4275593467 * ___ctx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ThirdParty.Json.LitJson.Lexer::State8(ThirdParty.Json.LitJson.FsmContext)
extern "C"  bool Lexer_State8_m3318232085 (Il2CppObject * __this /* static, unused */, FsmContext_t4275593467 * ___ctx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ThirdParty.Json.LitJson.Lexer::State9(ThirdParty.Json.LitJson.FsmContext)
extern "C"  bool Lexer_State9_m1432614832 (Il2CppObject * __this /* static, unused */, FsmContext_t4275593467 * ___ctx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ThirdParty.Json.LitJson.Lexer::State10(ThirdParty.Json.LitJson.FsmContext)
extern "C"  bool Lexer_State10_m2160325200 (Il2CppObject * __this /* static, unused */, FsmContext_t4275593467 * ___ctx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ThirdParty.Json.LitJson.Lexer::State11(ThirdParty.Json.LitJson.FsmContext)
extern "C"  bool Lexer_State11_m621563729 (Il2CppObject * __this /* static, unused */, FsmContext_t4275593467 * ___ctx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ThirdParty.Json.LitJson.Lexer::State12(ThirdParty.Json.LitJson.FsmContext)
extern "C"  bool Lexer_State12_m425405010 (Il2CppObject * __this /* static, unused */, FsmContext_t4275593467 * ___ctx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ThirdParty.Json.LitJson.Lexer::State13(ThirdParty.Json.LitJson.FsmContext)
extern "C"  bool Lexer_State13_m3181610835 (Il2CppObject * __this /* static, unused */, FsmContext_t4275593467 * ___ctx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ThirdParty.Json.LitJson.Lexer::State14(ThirdParty.Json.LitJson.FsmContext)
extern "C"  bool Lexer_State14_m2103223124 (Il2CppObject * __this /* static, unused */, FsmContext_t4275593467 * ___ctx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ThirdParty.Json.LitJson.Lexer::State15(ThirdParty.Json.LitJson.FsmContext)
extern "C"  bool Lexer_State15_m564461653 (Il2CppObject * __this /* static, unused */, FsmContext_t4275593467 * ___ctx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ThirdParty.Json.LitJson.Lexer::State16(ThirdParty.Json.LitJson.FsmContext)
extern "C"  bool Lexer_State16_m368302934 (Il2CppObject * __this /* static, unused */, FsmContext_t4275593467 * ___ctx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ThirdParty.Json.LitJson.Lexer::State17(ThirdParty.Json.LitJson.FsmContext)
extern "C"  bool Lexer_State17_m3124508759 (Il2CppObject * __this /* static, unused */, FsmContext_t4275593467 * ___ctx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ThirdParty.Json.LitJson.Lexer::State18(ThirdParty.Json.LitJson.FsmContext)
extern "C"  bool Lexer_State18_m3963376200 (Il2CppObject * __this /* static, unused */, FsmContext_t4275593467 * ___ctx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ThirdParty.Json.LitJson.Lexer::State19(ThirdParty.Json.LitJson.FsmContext)
extern "C"  bool Lexer_State19_m2424614729 (Il2CppObject * __this /* static, unused */, FsmContext_t4275593467 * ___ctx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ThirdParty.Json.LitJson.Lexer::State20(ThirdParty.Json.LitJson.FsmContext)
extern "C"  bool Lexer_State20_m3589906367 (Il2CppObject * __this /* static, unused */, FsmContext_t4275593467 * ___ctx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ThirdParty.Json.LitJson.Lexer::State21(ThirdParty.Json.LitJson.FsmContext)
extern "C"  bool Lexer_State21_m2051144896 (Il2CppObject * __this /* static, unused */, FsmContext_t4275593467 * ___ctx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ThirdParty.Json.LitJson.Lexer::State22(ThirdParty.Json.LitJson.FsmContext)
extern "C"  bool Lexer_State22_m1854986177 (Il2CppObject * __this /* static, unused */, FsmContext_t4275593467 * ___ctx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ThirdParty.Json.LitJson.Lexer::State23(ThirdParty.Json.LitJson.FsmContext)
extern "C"  bool Lexer_State23_m316224706 (Il2CppObject * __this /* static, unused */, FsmContext_t4275593467 * ___ctx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ThirdParty.Json.LitJson.Lexer::State24(ThirdParty.Json.LitJson.FsmContext)
extern "C"  bool Lexer_State24_m3532804291 (Il2CppObject * __this /* static, unused */, FsmContext_t4275593467 * ___ctx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ThirdParty.Json.LitJson.Lexer::State25(ThirdParty.Json.LitJson.FsmContext)
extern "C"  bool Lexer_State25_m1994042820 (Il2CppObject * __this /* static, unused */, FsmContext_t4275593467 * ___ctx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ThirdParty.Json.LitJson.Lexer::State26(ThirdParty.Json.LitJson.FsmContext)
extern "C"  bool Lexer_State26_m1797884101 (Il2CppObject * __this /* static, unused */, FsmContext_t4275593467 * ___ctx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ThirdParty.Json.LitJson.Lexer::State27(ThirdParty.Json.LitJson.FsmContext)
extern "C"  bool Lexer_State27_m259122630 (Il2CppObject * __this /* static, unused */, FsmContext_t4275593467 * ___ctx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ThirdParty.Json.LitJson.Lexer::State28(ThirdParty.Json.LitJson.FsmContext)
extern "C"  bool Lexer_State28_m1097990071 (Il2CppObject * __this /* static, unused */, FsmContext_t4275593467 * ___ctx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ThirdParty.Json.LitJson.Lexer::GetChar()
extern "C"  bool Lexer_GetChar_m3775205041 (Lexer_t954714164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ThirdParty.Json.LitJson.Lexer::NextChar()
extern "C"  int32_t Lexer_NextChar_m833558632 (Lexer_t954714164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ThirdParty.Json.LitJson.Lexer::NextToken()
extern "C"  bool Lexer_NextToken_m4229197011 (Lexer_t954714164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.Lexer::UngetChar()
extern "C"  void Lexer_UngetChar_m1417570926 (Lexer_t954714164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
