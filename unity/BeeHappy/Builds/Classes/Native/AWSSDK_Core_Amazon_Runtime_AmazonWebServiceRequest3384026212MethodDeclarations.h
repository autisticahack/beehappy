﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.AmazonWebServiceRequest
struct AmazonWebServiceRequest_t3384026212;
// System.EventHandler`1<Amazon.Runtime.StreamTransferProgressArgs>
struct EventHandler_1_t1230945235;
// System.Object
struct Il2CppObject;
// Amazon.Runtime.RequestEventArgs
struct RequestEventArgs_t434225820;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "AWSSDK_Core_Amazon_Runtime_RequestEventArgs434225820.h"

// System.EventHandler`1<Amazon.Runtime.StreamTransferProgressArgs> Amazon.Runtime.AmazonWebServiceRequest::Amazon.Runtime.Internal.IAmazonWebServiceRequest.get_StreamUploadProgressCallback()
extern "C"  EventHandler_1_t1230945235 * AmazonWebServiceRequest_Amazon_Runtime_Internal_IAmazonWebServiceRequest_get_StreamUploadProgressCallback_m373670487 (AmazonWebServiceRequest_t3384026212 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.AmazonWebServiceRequest::.ctor()
extern "C"  void AmazonWebServiceRequest__ctor_m2915505731 (AmazonWebServiceRequest_t3384026212 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.AmazonWebServiceRequest::FireBeforeRequestEvent(System.Object,Amazon.Runtime.RequestEventArgs)
extern "C"  void AmazonWebServiceRequest_FireBeforeRequestEvent_m702451209 (AmazonWebServiceRequest_t3384026212 * __this, Il2CppObject * ___sender0, RequestEventArgs_t434225820 * ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.AmazonWebServiceRequest::Amazon.Runtime.Internal.IAmazonWebServiceRequest.get_UseSigV4()
extern "C"  bool AmazonWebServiceRequest_Amazon_Runtime_Internal_IAmazonWebServiceRequest_get_UseSigV4_m18979324 (AmazonWebServiceRequest_t3384026212 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
