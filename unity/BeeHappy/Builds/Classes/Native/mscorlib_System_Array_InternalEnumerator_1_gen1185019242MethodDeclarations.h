﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1185019242.h"
#include "mscorlib_System_Array3829468939.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_TypeAffinity326266980.h"

// System.Void System.Array/InternalEnumerator`1<Mono.Data.Sqlite.TypeAffinity>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2229758508_gshared (InternalEnumerator_1_t1185019242 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m2229758508(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1185019242 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2229758508_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<Mono.Data.Sqlite.TypeAffinity>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3008605884_gshared (InternalEnumerator_1_t1185019242 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3008605884(__this, method) ((  void (*) (InternalEnumerator_1_t1185019242 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3008605884_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Mono.Data.Sqlite.TypeAffinity>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3917300378_gshared (InternalEnumerator_1_t1185019242 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3917300378(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1185019242 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3917300378_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Mono.Data.Sqlite.TypeAffinity>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m83382263_gshared (InternalEnumerator_1_t1185019242 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m83382263(__this, method) ((  void (*) (InternalEnumerator_1_t1185019242 *, const MethodInfo*))InternalEnumerator_1_Dispose_m83382263_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Mono.Data.Sqlite.TypeAffinity>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3801385252_gshared (InternalEnumerator_1_t1185019242 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m3801385252(__this, method) ((  bool (*) (InternalEnumerator_1_t1185019242 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3801385252_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Mono.Data.Sqlite.TypeAffinity>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m2534601347_gshared (InternalEnumerator_1_t1185019242 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m2534601347(__this, method) ((  int32_t (*) (InternalEnumerator_1_t1185019242 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2534601347_gshared)(__this, method)
