﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2028125793.h"
#include "mscorlib_System_Array3829468939.h"
#include "System_System_Net_WebExceptionStatus1169373531.h"

// System.Void System.Array/InternalEnumerator`1<System.Net.WebExceptionStatus>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1673268356_gshared (InternalEnumerator_1_t2028125793 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1673268356(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2028125793 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1673268356_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Net.WebExceptionStatus>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1953137076_gshared (InternalEnumerator_1_t2028125793 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1953137076(__this, method) ((  void (*) (InternalEnumerator_1_t2028125793 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1953137076_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Net.WebExceptionStatus>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3663221770_gshared (InternalEnumerator_1_t2028125793 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3663221770(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2028125793 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3663221770_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Net.WebExceptionStatus>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m6870975_gshared (InternalEnumerator_1_t2028125793 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m6870975(__this, method) ((  void (*) (InternalEnumerator_1_t2028125793 *, const MethodInfo*))InternalEnumerator_1_Dispose_m6870975_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Net.WebExceptionStatus>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3516735084_gshared (InternalEnumerator_1_t2028125793 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m3516735084(__this, method) ((  bool (*) (InternalEnumerator_1_t2028125793 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3516735084_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Net.WebExceptionStatus>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m3778517059_gshared (InternalEnumerator_1_t2028125793 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m3778517059(__this, method) ((  int32_t (*) (InternalEnumerator_1_t2028125793 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3778517059_gshared)(__this, method)
