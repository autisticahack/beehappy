﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetOpenIdTokenRequestMarshaller
struct GetOpenIdTokenRequestMarshaller_t1236206204;
// Amazon.Runtime.Internal.IRequest
struct IRequest_t2400804350;
// Amazon.Runtime.AmazonWebServiceRequest
struct AmazonWebServiceRequest_t3384026212;
// Amazon.CognitoIdentity.Model.GetOpenIdTokenRequest
struct GetOpenIdTokenRequest_t2698079901;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_Runtime_AmazonWebServiceRequest3384026212.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Mode2698079901.h"

// Amazon.Runtime.Internal.IRequest Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetOpenIdTokenRequestMarshaller::Marshall(Amazon.Runtime.AmazonWebServiceRequest)
extern "C"  Il2CppObject * GetOpenIdTokenRequestMarshaller_Marshall_m1044976942 (GetOpenIdTokenRequestMarshaller_t1236206204 * __this, AmazonWebServiceRequest_t3384026212 * ___input0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.Internal.IRequest Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetOpenIdTokenRequestMarshaller::Marshall(Amazon.CognitoIdentity.Model.GetOpenIdTokenRequest)
extern "C"  Il2CppObject * GetOpenIdTokenRequestMarshaller_Marshall_m3491085579 (GetOpenIdTokenRequestMarshaller_t1236206204 * __this, GetOpenIdTokenRequest_t2698079901 * ___publicRequest0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetOpenIdTokenRequestMarshaller::.ctor()
extern "C"  void GetOpenIdTokenRequestMarshaller__ctor_m355897200 (GetOpenIdTokenRequestMarshaller_t1236206204 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
