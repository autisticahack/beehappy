﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.Runtime.Internal.IRequest
struct IRequest_t2400804350;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Transform.MarshallerContext
struct  MarshallerContext_t3045611074  : public Il2CppObject
{
public:
	// Amazon.Runtime.Internal.IRequest Amazon.Runtime.Internal.Transform.MarshallerContext::<Request>k__BackingField
	Il2CppObject * ___U3CRequestU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CRequestU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MarshallerContext_t3045611074, ___U3CRequestU3Ek__BackingField_0)); }
	inline Il2CppObject * get_U3CRequestU3Ek__BackingField_0() const { return ___U3CRequestU3Ek__BackingField_0; }
	inline Il2CppObject ** get_address_of_U3CRequestU3Ek__BackingField_0() { return &___U3CRequestU3Ek__BackingField_0; }
	inline void set_U3CRequestU3Ek__BackingField_0(Il2CppObject * value)
	{
		___U3CRequestU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CRequestU3Ek__BackingField_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
