﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.Transform.ListUnmarshaller`2<System.Object,System.Object>
struct ListUnmarshaller_2_t1903008017;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;
// Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext
struct XmlUnmarshallerContext_t1179575220;
// Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext
struct JsonUnmarshallerContext_t456235889;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_XmlU1179575220.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_JsonU456235889.h"

// System.Void Amazon.Runtime.Internal.Transform.ListUnmarshaller`2<System.Object,System.Object>::.ctor(IUnmarshaller)
extern "C"  void ListUnmarshaller_2__ctor_m1344739393_gshared (ListUnmarshaller_2_t1903008017 * __this, Il2CppObject * ___iUnmarshaller0, const MethodInfo* method);
#define ListUnmarshaller_2__ctor_m1344739393(__this, ___iUnmarshaller0, method) ((  void (*) (ListUnmarshaller_2_t1903008017 *, Il2CppObject *, const MethodInfo*))ListUnmarshaller_2__ctor_m1344739393_gshared)(__this, ___iUnmarshaller0, method)
// System.Collections.Generic.List`1<I> Amazon.Runtime.Internal.Transform.ListUnmarshaller`2<System.Object,System.Object>::Unmarshall(Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext)
extern "C"  List_1_t2058570427 * ListUnmarshaller_2_Unmarshall_m4244807702_gshared (ListUnmarshaller_2_t1903008017 * __this, XmlUnmarshallerContext_t1179575220 * ___context0, const MethodInfo* method);
#define ListUnmarshaller_2_Unmarshall_m4244807702(__this, ___context0, method) ((  List_1_t2058570427 * (*) (ListUnmarshaller_2_t1903008017 *, XmlUnmarshallerContext_t1179575220 *, const MethodInfo*))ListUnmarshaller_2_Unmarshall_m4244807702_gshared)(__this, ___context0, method)
// System.Collections.Generic.List`1<I> Amazon.Runtime.Internal.Transform.ListUnmarshaller`2<System.Object,System.Object>::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern "C"  List_1_t2058570427 * ListUnmarshaller_2_Unmarshall_m3347864357_gshared (ListUnmarshaller_2_t1903008017 * __this, JsonUnmarshallerContext_t456235889 * ___context0, const MethodInfo* method);
#define ListUnmarshaller_2_Unmarshall_m3347864357(__this, ___context0, method) ((  List_1_t2058570427 * (*) (ListUnmarshaller_2_t1903008017 *, JsonUnmarshallerContext_t456235889 *, const MethodInfo*))ListUnmarshaller_2_Unmarshall_m3347864357_gshared)(__this, ___context0, method)
