﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// Amazon.CognitoIdentity.AmazonCognitoIdentityClient
struct AmazonCognitoIdentityClient_t3069350888;
// Amazon.SecurityToken.AmazonSecurityTokenServiceClient
struct AmazonSecurityTokenServiceClient_t1241355389;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;
// Amazon.CognitoIdentity.CognitoAWSCredentials/IdentityState
struct IdentityState_t2011390993;
// System.EventHandler`1<Amazon.CognitoIdentity.CognitoAWSCredentials/IdentityChangedArgs>
struct EventHandler_1_t248708383;

#include "AWSSDK_Core_Amazon_Runtime_RefreshingAWSCredential1767066958.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.CognitoIdentity.CognitoAWSCredentials
struct  CognitoAWSCredentials_t2370264792  : public RefreshingAWSCredentials_t1767066958
{
public:
	// System.String Amazon.CognitoIdentity.CognitoAWSCredentials::identityId
	String_t* ___identityId_4;
	// Amazon.CognitoIdentity.AmazonCognitoIdentityClient Amazon.CognitoIdentity.CognitoAWSCredentials::cib
	AmazonCognitoIdentityClient_t3069350888 * ___cib_6;
	// Amazon.SecurityToken.AmazonSecurityTokenServiceClient Amazon.CognitoIdentity.CognitoAWSCredentials::sts
	AmazonSecurityTokenServiceClient_t1241355389 * ___sts_7;
	// System.String Amazon.CognitoIdentity.CognitoAWSCredentials::<AccountId>k__BackingField
	String_t* ___U3CAccountIdU3Ek__BackingField_8;
	// System.String Amazon.CognitoIdentity.CognitoAWSCredentials::<IdentityPoolId>k__BackingField
	String_t* ___U3CIdentityPoolIdU3Ek__BackingField_9;
	// System.String Amazon.CognitoIdentity.CognitoAWSCredentials::<UnAuthRoleArn>k__BackingField
	String_t* ___U3CUnAuthRoleArnU3Ek__BackingField_10;
	// System.String Amazon.CognitoIdentity.CognitoAWSCredentials::<AuthRoleArn>k__BackingField
	String_t* ___U3CAuthRoleArnU3Ek__BackingField_11;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> Amazon.CognitoIdentity.CognitoAWSCredentials::<Logins>k__BackingField
	Dictionary_2_t3943999495 * ___U3CLoginsU3Ek__BackingField_12;
	// Amazon.CognitoIdentity.CognitoAWSCredentials/IdentityState Amazon.CognitoIdentity.CognitoAWSCredentials::_identityState
	IdentityState_t2011390993 * ____identityState_13;
	// System.EventHandler`1<Amazon.CognitoIdentity.CognitoAWSCredentials/IdentityChangedArgs> Amazon.CognitoIdentity.CognitoAWSCredentials::mIdentityChangedEvent
	EventHandler_1_t248708383 * ___mIdentityChangedEvent_14;

public:
	inline static int32_t get_offset_of_identityId_4() { return static_cast<int32_t>(offsetof(CognitoAWSCredentials_t2370264792, ___identityId_4)); }
	inline String_t* get_identityId_4() const { return ___identityId_4; }
	inline String_t** get_address_of_identityId_4() { return &___identityId_4; }
	inline void set_identityId_4(String_t* value)
	{
		___identityId_4 = value;
		Il2CppCodeGenWriteBarrier(&___identityId_4, value);
	}

	inline static int32_t get_offset_of_cib_6() { return static_cast<int32_t>(offsetof(CognitoAWSCredentials_t2370264792, ___cib_6)); }
	inline AmazonCognitoIdentityClient_t3069350888 * get_cib_6() const { return ___cib_6; }
	inline AmazonCognitoIdentityClient_t3069350888 ** get_address_of_cib_6() { return &___cib_6; }
	inline void set_cib_6(AmazonCognitoIdentityClient_t3069350888 * value)
	{
		___cib_6 = value;
		Il2CppCodeGenWriteBarrier(&___cib_6, value);
	}

	inline static int32_t get_offset_of_sts_7() { return static_cast<int32_t>(offsetof(CognitoAWSCredentials_t2370264792, ___sts_7)); }
	inline AmazonSecurityTokenServiceClient_t1241355389 * get_sts_7() const { return ___sts_7; }
	inline AmazonSecurityTokenServiceClient_t1241355389 ** get_address_of_sts_7() { return &___sts_7; }
	inline void set_sts_7(AmazonSecurityTokenServiceClient_t1241355389 * value)
	{
		___sts_7 = value;
		Il2CppCodeGenWriteBarrier(&___sts_7, value);
	}

	inline static int32_t get_offset_of_U3CAccountIdU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(CognitoAWSCredentials_t2370264792, ___U3CAccountIdU3Ek__BackingField_8)); }
	inline String_t* get_U3CAccountIdU3Ek__BackingField_8() const { return ___U3CAccountIdU3Ek__BackingField_8; }
	inline String_t** get_address_of_U3CAccountIdU3Ek__BackingField_8() { return &___U3CAccountIdU3Ek__BackingField_8; }
	inline void set_U3CAccountIdU3Ek__BackingField_8(String_t* value)
	{
		___U3CAccountIdU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CAccountIdU3Ek__BackingField_8, value);
	}

	inline static int32_t get_offset_of_U3CIdentityPoolIdU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(CognitoAWSCredentials_t2370264792, ___U3CIdentityPoolIdU3Ek__BackingField_9)); }
	inline String_t* get_U3CIdentityPoolIdU3Ek__BackingField_9() const { return ___U3CIdentityPoolIdU3Ek__BackingField_9; }
	inline String_t** get_address_of_U3CIdentityPoolIdU3Ek__BackingField_9() { return &___U3CIdentityPoolIdU3Ek__BackingField_9; }
	inline void set_U3CIdentityPoolIdU3Ek__BackingField_9(String_t* value)
	{
		___U3CIdentityPoolIdU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CIdentityPoolIdU3Ek__BackingField_9, value);
	}

	inline static int32_t get_offset_of_U3CUnAuthRoleArnU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(CognitoAWSCredentials_t2370264792, ___U3CUnAuthRoleArnU3Ek__BackingField_10)); }
	inline String_t* get_U3CUnAuthRoleArnU3Ek__BackingField_10() const { return ___U3CUnAuthRoleArnU3Ek__BackingField_10; }
	inline String_t** get_address_of_U3CUnAuthRoleArnU3Ek__BackingField_10() { return &___U3CUnAuthRoleArnU3Ek__BackingField_10; }
	inline void set_U3CUnAuthRoleArnU3Ek__BackingField_10(String_t* value)
	{
		___U3CUnAuthRoleArnU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CUnAuthRoleArnU3Ek__BackingField_10, value);
	}

	inline static int32_t get_offset_of_U3CAuthRoleArnU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(CognitoAWSCredentials_t2370264792, ___U3CAuthRoleArnU3Ek__BackingField_11)); }
	inline String_t* get_U3CAuthRoleArnU3Ek__BackingField_11() const { return ___U3CAuthRoleArnU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CAuthRoleArnU3Ek__BackingField_11() { return &___U3CAuthRoleArnU3Ek__BackingField_11; }
	inline void set_U3CAuthRoleArnU3Ek__BackingField_11(String_t* value)
	{
		___U3CAuthRoleArnU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CAuthRoleArnU3Ek__BackingField_11, value);
	}

	inline static int32_t get_offset_of_U3CLoginsU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(CognitoAWSCredentials_t2370264792, ___U3CLoginsU3Ek__BackingField_12)); }
	inline Dictionary_2_t3943999495 * get_U3CLoginsU3Ek__BackingField_12() const { return ___U3CLoginsU3Ek__BackingField_12; }
	inline Dictionary_2_t3943999495 ** get_address_of_U3CLoginsU3Ek__BackingField_12() { return &___U3CLoginsU3Ek__BackingField_12; }
	inline void set_U3CLoginsU3Ek__BackingField_12(Dictionary_2_t3943999495 * value)
	{
		___U3CLoginsU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CLoginsU3Ek__BackingField_12, value);
	}

	inline static int32_t get_offset_of__identityState_13() { return static_cast<int32_t>(offsetof(CognitoAWSCredentials_t2370264792, ____identityState_13)); }
	inline IdentityState_t2011390993 * get__identityState_13() const { return ____identityState_13; }
	inline IdentityState_t2011390993 ** get_address_of__identityState_13() { return &____identityState_13; }
	inline void set__identityState_13(IdentityState_t2011390993 * value)
	{
		____identityState_13 = value;
		Il2CppCodeGenWriteBarrier(&____identityState_13, value);
	}

	inline static int32_t get_offset_of_mIdentityChangedEvent_14() { return static_cast<int32_t>(offsetof(CognitoAWSCredentials_t2370264792, ___mIdentityChangedEvent_14)); }
	inline EventHandler_1_t248708383 * get_mIdentityChangedEvent_14() const { return ___mIdentityChangedEvent_14; }
	inline EventHandler_1_t248708383 ** get_address_of_mIdentityChangedEvent_14() { return &___mIdentityChangedEvent_14; }
	inline void set_mIdentityChangedEvent_14(EventHandler_1_t248708383 * value)
	{
		___mIdentityChangedEvent_14 = value;
		Il2CppCodeGenWriteBarrier(&___mIdentityChangedEvent_14, value);
	}
};

struct CognitoAWSCredentials_t2370264792_StaticFields
{
public:
	// System.Object Amazon.CognitoIdentity.CognitoAWSCredentials::refreshIdLock
	Il2CppObject * ___refreshIdLock_3;
	// System.Int32 Amazon.CognitoIdentity.CognitoAWSCredentials::DefaultDurationSeconds
	int32_t ___DefaultDurationSeconds_5;
	// System.String Amazon.CognitoIdentity.CognitoAWSCredentials::IDENTITY_ID_CACHE_KEY
	String_t* ___IDENTITY_ID_CACHE_KEY_15;
	// System.Object Amazon.CognitoIdentity.CognitoAWSCredentials::_lock
	Il2CppObject * ____lock_16;

public:
	inline static int32_t get_offset_of_refreshIdLock_3() { return static_cast<int32_t>(offsetof(CognitoAWSCredentials_t2370264792_StaticFields, ___refreshIdLock_3)); }
	inline Il2CppObject * get_refreshIdLock_3() const { return ___refreshIdLock_3; }
	inline Il2CppObject ** get_address_of_refreshIdLock_3() { return &___refreshIdLock_3; }
	inline void set_refreshIdLock_3(Il2CppObject * value)
	{
		___refreshIdLock_3 = value;
		Il2CppCodeGenWriteBarrier(&___refreshIdLock_3, value);
	}

	inline static int32_t get_offset_of_DefaultDurationSeconds_5() { return static_cast<int32_t>(offsetof(CognitoAWSCredentials_t2370264792_StaticFields, ___DefaultDurationSeconds_5)); }
	inline int32_t get_DefaultDurationSeconds_5() const { return ___DefaultDurationSeconds_5; }
	inline int32_t* get_address_of_DefaultDurationSeconds_5() { return &___DefaultDurationSeconds_5; }
	inline void set_DefaultDurationSeconds_5(int32_t value)
	{
		___DefaultDurationSeconds_5 = value;
	}

	inline static int32_t get_offset_of_IDENTITY_ID_CACHE_KEY_15() { return static_cast<int32_t>(offsetof(CognitoAWSCredentials_t2370264792_StaticFields, ___IDENTITY_ID_CACHE_KEY_15)); }
	inline String_t* get_IDENTITY_ID_CACHE_KEY_15() const { return ___IDENTITY_ID_CACHE_KEY_15; }
	inline String_t** get_address_of_IDENTITY_ID_CACHE_KEY_15() { return &___IDENTITY_ID_CACHE_KEY_15; }
	inline void set_IDENTITY_ID_CACHE_KEY_15(String_t* value)
	{
		___IDENTITY_ID_CACHE_KEY_15 = value;
		Il2CppCodeGenWriteBarrier(&___IDENTITY_ID_CACHE_KEY_15, value);
	}

	inline static int32_t get_offset_of__lock_16() { return static_cast<int32_t>(offsetof(CognitoAWSCredentials_t2370264792_StaticFields, ____lock_16)); }
	inline Il2CppObject * get__lock_16() const { return ____lock_16; }
	inline Il2CppObject ** get_address_of__lock_16() { return &____lock_16; }
	inline void set__lock_16(Il2CppObject * value)
	{
		____lock_16 = value;
		Il2CppCodeGenWriteBarrier(&____lock_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
