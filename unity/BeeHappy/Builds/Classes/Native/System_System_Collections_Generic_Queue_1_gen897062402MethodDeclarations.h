﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Queue_1_gen2509106130MethodDeclarations.h"

// System.Void System.Collections.Generic.Queue`1<System.WeakReference>::.ctor()
#define Queue_1__ctor_m2277940683(__this, method) ((  void (*) (Queue_1_t897062402 *, const MethodInfo*))Queue_1__ctor_m1845245813_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<System.WeakReference>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Queue_1_System_Collections_ICollection_CopyTo_m1755784091(__this, ___array0, ___idx1, method) ((  void (*) (Queue_1_t897062402 *, Il2CppArray *, int32_t, const MethodInfo*))Queue_1_System_Collections_ICollection_CopyTo_m772787461_gshared)(__this, ___array0, ___idx1, method)
// System.Boolean System.Collections.Generic.Queue`1<System.WeakReference>::System.Collections.ICollection.get_IsSynchronized()
#define Queue_1_System_Collections_ICollection_get_IsSynchronized_m44457915(__this, method) ((  bool (*) (Queue_1_t897062402 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_IsSynchronized_m307125669_gshared)(__this, method)
// System.Object System.Collections.Generic.Queue`1<System.WeakReference>::System.Collections.ICollection.get_SyncRoot()
#define Queue_1_System_Collections_ICollection_get_SyncRoot_m2539971579(__this, method) ((  Il2CppObject * (*) (Queue_1_t897062402 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_SyncRoot_m311152041_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Queue`1<System.WeakReference>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1817088111(__this, method) ((  Il2CppObject* (*) (Queue_1_t897062402 *, const MethodInfo*))Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m4179169157_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Queue`1<System.WeakReference>::System.Collections.IEnumerable.GetEnumerator()
#define Queue_1_System_Collections_IEnumerable_GetEnumerator_m3471220792(__this, method) ((  Il2CppObject * (*) (Queue_1_t897062402 *, const MethodInfo*))Queue_1_System_Collections_IEnumerable_GetEnumerator_m251608368_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<System.WeakReference>::CopyTo(T[],System.Int32)
#define Queue_1_CopyTo_m4280707770(__this, ___array0, ___idx1, method) ((  void (*) (Queue_1_t897062402 *, WeakReferenceU5BU5D_t1180133286*, int32_t, const MethodInfo*))Queue_1_CopyTo_m1419617898_gshared)(__this, ___array0, ___idx1, method)
// T System.Collections.Generic.Queue`1<System.WeakReference>::Dequeue()
#define Queue_1_Dequeue_m2844113765(__this, method) ((  WeakReference_t1077405567 * (*) (Queue_1_t897062402 *, const MethodInfo*))Queue_1_Dequeue_m4118160228_gshared)(__this, method)
// T System.Collections.Generic.Queue`1<System.WeakReference>::Peek()
#define Queue_1_Peek_m924143923(__this, method) ((  WeakReference_t1077405567 * (*) (Queue_1_t897062402 *, const MethodInfo*))Queue_1_Peek_m1463479953_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<System.WeakReference>::Enqueue(T)
#define Queue_1_Enqueue_m1095351132(__this, ___item0, method) ((  void (*) (Queue_1_t897062402 *, WeakReference_t1077405567 *, const MethodInfo*))Queue_1_Enqueue_m2424099095_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Queue`1<System.WeakReference>::SetCapacity(System.Int32)
#define Queue_1_SetCapacity_m795882314(__this, ___new_size0, method) ((  void (*) (Queue_1_t897062402 *, int32_t, const MethodInfo*))Queue_1_SetCapacity_m3858927842_gshared)(__this, ___new_size0, method)
// System.Int32 System.Collections.Generic.Queue`1<System.WeakReference>::get_Count()
#define Queue_1_get_Count_m3721756255(__this, method) ((  int32_t (*) (Queue_1_t897062402 *, const MethodInfo*))Queue_1_get_Count_m3795587777_gshared)(__this, method)
// System.Collections.Generic.Queue`1/Enumerator<T> System.Collections.Generic.Queue`1<System.WeakReference>::GetEnumerator()
#define Queue_1_GetEnumerator_m3547865718(__this, method) ((  Enumerator_t1407125482  (*) (Queue_1_t897062402 *, const MethodInfo*))Queue_1_GetEnumerator_m2842671368_gshared)(__this, method)
