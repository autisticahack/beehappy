﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.HashSet`1<System.Net.HttpStatusCode>
struct HashSet_1_t231870495;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Collections_Generic_HashSet_1_E3015153633.h"
#include "System_System_Net_HttpStatusCode1898409641.h"

// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Net.HttpStatusCode>::.ctor(System.Collections.Generic.HashSet`1<T>)
extern "C"  void Enumerator__ctor_m744187531_gshared (Enumerator_t3015153633 * __this, HashSet_1_t231870495 * ___hashset0, const MethodInfo* method);
#define Enumerator__ctor_m744187531(__this, ___hashset0, method) ((  void (*) (Enumerator_t3015153633 *, HashSet_1_t231870495 *, const MethodInfo*))Enumerator__ctor_m744187531_gshared)(__this, ___hashset0, method)
// System.Object System.Collections.Generic.HashSet`1/Enumerator<System.Net.HttpStatusCode>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m551399445_gshared (Enumerator_t3015153633 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m551399445(__this, method) ((  Il2CppObject * (*) (Enumerator_t3015153633 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m551399445_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Net.HttpStatusCode>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2791111993_gshared (Enumerator_t3015153633 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2791111993(__this, method) ((  void (*) (Enumerator_t3015153633 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2791111993_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1/Enumerator<System.Net.HttpStatusCode>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m884179409_gshared (Enumerator_t3015153633 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m884179409(__this, method) ((  bool (*) (Enumerator_t3015153633 *, const MethodInfo*))Enumerator_MoveNext_m884179409_gshared)(__this, method)
// T System.Collections.Generic.HashSet`1/Enumerator<System.Net.HttpStatusCode>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m58233140_gshared (Enumerator_t3015153633 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m58233140(__this, method) ((  int32_t (*) (Enumerator_t3015153633 *, const MethodInfo*))Enumerator_get_Current_m58233140_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Net.HttpStatusCode>::Dispose()
extern "C"  void Enumerator_Dispose_m3483823980_gshared (Enumerator_t3015153633 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3483823980(__this, method) ((  void (*) (Enumerator_t3015153633 *, const MethodInfo*))Enumerator_Dispose_m3483823980_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Net.HttpStatusCode>::CheckState()
extern "C"  void Enumerator_CheckState_m275831508_gshared (Enumerator_t3015153633 * __this, const MethodInfo* method);
#define Enumerator_CheckState_m275831508(__this, method) ((  void (*) (Enumerator_t3015153633 *, const MethodInfo*))Enumerator_CheckState_m275831508_gshared)(__this, method)
