﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<ThirdParty.Json.LitJson.ArrayMetadata>
struct DefaultComparer_t2757008175;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_ArrayMetadata1135078014.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<ThirdParty.Json.LitJson.ArrayMetadata>::.ctor()
extern "C"  void DefaultComparer__ctor_m2642953100_gshared (DefaultComparer_t2757008175 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m2642953100(__this, method) ((  void (*) (DefaultComparer_t2757008175 *, const MethodInfo*))DefaultComparer__ctor_m2642953100_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<ThirdParty.Json.LitJson.ArrayMetadata>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m2885894437_gshared (DefaultComparer_t2757008175 * __this, ArrayMetadata_t1135078014  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m2885894437(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t2757008175 *, ArrayMetadata_t1135078014 , const MethodInfo*))DefaultComparer_GetHashCode_m2885894437_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<ThirdParty.Json.LitJson.ArrayMetadata>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m1138115541_gshared (DefaultComparer_t2757008175 * __this, ArrayMetadata_t1135078014  ___x0, ArrayMetadata_t1135078014  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m1138115541(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t2757008175 *, ArrayMetadata_t1135078014 , ArrayMetadata_t1135078014 , const MethodInfo*))DefaultComparer_Equals_m1138115541_gshared)(__this, ___x0, ___y1, method)
