﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.Util.LogMessage
struct LogMessage_t4004523899;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.IFormatProvider
struct IFormatProvider_t2849799027;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Object[] Amazon.Runtime.Internal.Util.LogMessage::get_Args()
extern "C"  ObjectU5BU5D_t3614634134* LogMessage_get_Args_m4248813394 (LogMessage_t4004523899 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.LogMessage::set_Args(System.Object[])
extern "C"  void LogMessage_set_Args_m2727928063 (LogMessage_t4004523899 * __this, ObjectU5BU5D_t3614634134* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IFormatProvider Amazon.Runtime.Internal.Util.LogMessage::get_Provider()
extern "C"  Il2CppObject * LogMessage_get_Provider_m1211913116 (LogMessage_t4004523899 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.LogMessage::set_Provider(System.IFormatProvider)
extern "C"  void LogMessage_set_Provider_m3276386369 (LogMessage_t4004523899 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Runtime.Internal.Util.LogMessage::get_Format()
extern "C"  String_t* LogMessage_get_Format_m1598488650 (LogMessage_t4004523899 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.LogMessage::set_Format(System.String)
extern "C"  void LogMessage_set_Format_m3444611225 (LogMessage_t4004523899 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.LogMessage::.ctor(System.IFormatProvider,System.String,System.Object[])
extern "C"  void LogMessage__ctor_m3564625539 (LogMessage_t4004523899 * __this, Il2CppObject * ___provider0, String_t* ___format1, ObjectU5BU5D_t3614634134* ___args2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Runtime.Internal.Util.LogMessage::ToString()
extern "C"  String_t* LogMessage_ToString_m467678268 (LogMessage_t4004523899 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
