﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.CognitoSync.Model.ListRecordsRequest
struct ListRecordsRequest_t3074726983;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.String Amazon.CognitoSync.Model.ListRecordsRequest::get_DatasetName()
extern "C"  String_t* ListRecordsRequest_get_DatasetName_m1175231935 (ListRecordsRequest_t3074726983 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Model.ListRecordsRequest::set_DatasetName(System.String)
extern "C"  void ListRecordsRequest_set_DatasetName_m1741945328 (ListRecordsRequest_t3074726983 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.CognitoSync.Model.ListRecordsRequest::IsSetDatasetName()
extern "C"  bool ListRecordsRequest_IsSetDatasetName_m2896031423 (ListRecordsRequest_t3074726983 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.CognitoSync.Model.ListRecordsRequest::get_IdentityId()
extern "C"  String_t* ListRecordsRequest_get_IdentityId_m41680257 (ListRecordsRequest_t3074726983 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Model.ListRecordsRequest::set_IdentityId(System.String)
extern "C"  void ListRecordsRequest_set_IdentityId_m974396636 (ListRecordsRequest_t3074726983 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.CognitoSync.Model.ListRecordsRequest::IsSetIdentityId()
extern "C"  bool ListRecordsRequest_IsSetIdentityId_m2152091909 (ListRecordsRequest_t3074726983 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.CognitoSync.Model.ListRecordsRequest::get_IdentityPoolId()
extern "C"  String_t* ListRecordsRequest_get_IdentityPoolId_m233746853 (ListRecordsRequest_t3074726983 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Model.ListRecordsRequest::set_IdentityPoolId(System.String)
extern "C"  void ListRecordsRequest_set_IdentityPoolId_m2346773018 (ListRecordsRequest_t3074726983 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.CognitoSync.Model.ListRecordsRequest::IsSetIdentityPoolId()
extern "C"  bool ListRecordsRequest_IsSetIdentityPoolId_m2006376889 (ListRecordsRequest_t3074726983 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Amazon.CognitoSync.Model.ListRecordsRequest::get_LastSyncCount()
extern "C"  int64_t ListRecordsRequest_get_LastSyncCount_m443495050 (ListRecordsRequest_t3074726983 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Model.ListRecordsRequest::set_LastSyncCount(System.Int64)
extern "C"  void ListRecordsRequest_set_LastSyncCount_m768342443 (ListRecordsRequest_t3074726983 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.CognitoSync.Model.ListRecordsRequest::IsSetLastSyncCount()
extern "C"  bool ListRecordsRequest_IsSetLastSyncCount_m16632438 (ListRecordsRequest_t3074726983 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Amazon.CognitoSync.Model.ListRecordsRequest::get_MaxResults()
extern "C"  int32_t ListRecordsRequest_get_MaxResults_m4157758487 (ListRecordsRequest_t3074726983 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Model.ListRecordsRequest::set_MaxResults(System.Int32)
extern "C"  void ListRecordsRequest_set_MaxResults_m2547229740 (ListRecordsRequest_t3074726983 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.CognitoSync.Model.ListRecordsRequest::IsSetMaxResults()
extern "C"  bool ListRecordsRequest_IsSetMaxResults_m3789417014 (ListRecordsRequest_t3074726983 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.CognitoSync.Model.ListRecordsRequest::get_NextToken()
extern "C"  String_t* ListRecordsRequest_get_NextToken_m562181866 (ListRecordsRequest_t3074726983 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Model.ListRecordsRequest::set_NextToken(System.String)
extern "C"  void ListRecordsRequest_set_NextToken_m2158044181 (ListRecordsRequest_t3074726983 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.CognitoSync.Model.ListRecordsRequest::IsSetNextToken()
extern "C"  bool ListRecordsRequest_IsSetNextToken_m3018361882 (ListRecordsRequest_t3074726983 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.CognitoSync.Model.ListRecordsRequest::get_SyncSessionToken()
extern "C"  String_t* ListRecordsRequest_get_SyncSessionToken_m4211870776 (ListRecordsRequest_t3074726983 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.CognitoSync.Model.ListRecordsRequest::IsSetSyncSessionToken()
extern "C"  bool ListRecordsRequest_IsSetSyncSessionToken_m4060964444 (ListRecordsRequest_t3074726983 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Model.ListRecordsRequest::.ctor()
extern "C"  void ListRecordsRequest__ctor_m2832381186 (ListRecordsRequest_t3074726983 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
