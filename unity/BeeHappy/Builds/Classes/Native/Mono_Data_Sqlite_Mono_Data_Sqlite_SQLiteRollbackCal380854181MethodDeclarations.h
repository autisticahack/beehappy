﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Data.Sqlite.SQLiteRollbackCallback
struct SQLiteRollbackCallback_t380854181;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Mono.Data.Sqlite.SQLiteRollbackCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void SQLiteRollbackCallback__ctor_m1041686810 (SQLiteRollbackCallback_t380854181 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SQLiteRollbackCallback::Invoke(System.IntPtr)
extern "C"  void SQLiteRollbackCallback_Invoke_m2372851354 (SQLiteRollbackCallback_t380854181 * __this, IntPtr_t ___puser0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Mono.Data.Sqlite.SQLiteRollbackCallback::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * SQLiteRollbackCallback_BeginInvoke_m1351764105 (SQLiteRollbackCallback_t380854181 * __this, IntPtr_t ___puser0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SQLiteRollbackCallback::EndInvoke(System.IAsyncResult)
extern "C"  void SQLiteRollbackCallback_EndInvoke_m3987187636 (SQLiteRollbackCallback_t380854181 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
