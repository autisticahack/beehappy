﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.SecurityToken.Model.Internal.MarshallTransformations.AssumeRoleWithWebIdentityResponseUnmarshaller
struct AssumeRoleWithWebIdentityResponseUnmarshaller_t3698885375;
// Amazon.Runtime.AmazonWebServiceResponse
struct AmazonWebServiceResponse_t529043356;
// Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext
struct XmlUnmarshallerContext_t1179575220;
// Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityResponse
struct AssumeRoleWithWebIdentityResponse_t3931705881;
// Amazon.Runtime.AmazonServiceException
struct AmazonServiceException_t3748559634;
// System.Exception
struct Exception_t1927440687;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_XmlU1179575220.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_Model_As3931705881.h"
#include "mscorlib_System_Exception1927440687.h"
#include "System_System_Net_HttpStatusCode1898409641.h"

// Amazon.Runtime.AmazonWebServiceResponse Amazon.SecurityToken.Model.Internal.MarshallTransformations.AssumeRoleWithWebIdentityResponseUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext)
extern "C"  AmazonWebServiceResponse_t529043356 * AssumeRoleWithWebIdentityResponseUnmarshaller_Unmarshall_m3864513860 (AssumeRoleWithWebIdentityResponseUnmarshaller_t3698885375 * __this, XmlUnmarshallerContext_t1179575220 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.SecurityToken.Model.Internal.MarshallTransformations.AssumeRoleWithWebIdentityResponseUnmarshaller::UnmarshallResult(Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext,Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityResponse)
extern "C"  void AssumeRoleWithWebIdentityResponseUnmarshaller_UnmarshallResult_m2565070237 (Il2CppObject * __this /* static, unused */, XmlUnmarshallerContext_t1179575220 * ___context0, AssumeRoleWithWebIdentityResponse_t3931705881 * ___response1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.AmazonServiceException Amazon.SecurityToken.Model.Internal.MarshallTransformations.AssumeRoleWithWebIdentityResponseUnmarshaller::UnmarshallException(Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext,System.Exception,System.Net.HttpStatusCode)
extern "C"  AmazonServiceException_t3748559634 * AssumeRoleWithWebIdentityResponseUnmarshaller_UnmarshallException_m2512616822 (AssumeRoleWithWebIdentityResponseUnmarshaller_t3698885375 * __this, XmlUnmarshallerContext_t1179575220 * ___context0, Exception_t1927440687 * ___innerException1, int32_t ___statusCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.SecurityToken.Model.Internal.MarshallTransformations.AssumeRoleWithWebIdentityResponseUnmarshaller Amazon.SecurityToken.Model.Internal.MarshallTransformations.AssumeRoleWithWebIdentityResponseUnmarshaller::get_Instance()
extern "C"  AssumeRoleWithWebIdentityResponseUnmarshaller_t3698885375 * AssumeRoleWithWebIdentityResponseUnmarshaller_get_Instance_m3286108612 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.SecurityToken.Model.Internal.MarshallTransformations.AssumeRoleWithWebIdentityResponseUnmarshaller::.ctor()
extern "C"  void AssumeRoleWithWebIdentityResponseUnmarshaller__ctor_m3146897921 (AssumeRoleWithWebIdentityResponseUnmarshaller_t3698885375 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.SecurityToken.Model.Internal.MarshallTransformations.AssumeRoleWithWebIdentityResponseUnmarshaller::.cctor()
extern "C"  void AssumeRoleWithWebIdentityResponseUnmarshaller__cctor_m1770634210 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
