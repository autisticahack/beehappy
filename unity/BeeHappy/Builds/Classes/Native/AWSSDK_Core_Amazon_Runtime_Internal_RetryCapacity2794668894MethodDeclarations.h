﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.RetryCapacity
struct RetryCapacity_t2794668894;

#include "codegen/il2cpp-codegen.h"

// System.Int32 Amazon.Runtime.Internal.RetryCapacity::get_AvailableCapacity()
extern "C"  int32_t RetryCapacity_get_AvailableCapacity_m1061535170 (RetryCapacity_t2794668894 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.RetryCapacity::set_AvailableCapacity(System.Int32)
extern "C"  void RetryCapacity_set_AvailableCapacity_m1104884973 (RetryCapacity_t2794668894 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Amazon.Runtime.Internal.RetryCapacity::get_MaxCapacity()
extern "C"  int32_t RetryCapacity_get_MaxCapacity_m4059484251 (RetryCapacity_t2794668894 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.RetryCapacity::.ctor(System.Int32)
extern "C"  void RetryCapacity__ctor_m1016555601 (RetryCapacity_t2794668894 * __this, int32_t ___maxCapacity0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
