﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ThirdParty.Json.LitJson.JsonMapper/<>c
struct U3CU3Ec_t1591091833;
// ThirdParty.Json.LitJson.ExporterFunc
struct ExporterFunc_t173265409;
// ThirdParty.Json.LitJson.ImporterFunc
struct ImporterFunc_t850687278;
// ThirdParty.Json.LitJson.WrapperFactory
struct WrapperFactory_t327905379;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.Json.LitJson.JsonMapper/<>c
struct  U3CU3Ec_t1591091833  : public Il2CppObject
{
public:

public:
};

struct U3CU3Ec_t1591091833_StaticFields
{
public:
	// ThirdParty.Json.LitJson.JsonMapper/<>c ThirdParty.Json.LitJson.JsonMapper/<>c::<>9
	U3CU3Ec_t1591091833 * ___U3CU3E9_0;
	// ThirdParty.Json.LitJson.ExporterFunc ThirdParty.Json.LitJson.JsonMapper/<>c::<>9__24_0
	ExporterFunc_t173265409 * ___U3CU3E9__24_0_1;
	// ThirdParty.Json.LitJson.ExporterFunc ThirdParty.Json.LitJson.JsonMapper/<>c::<>9__24_1
	ExporterFunc_t173265409 * ___U3CU3E9__24_1_2;
	// ThirdParty.Json.LitJson.ExporterFunc ThirdParty.Json.LitJson.JsonMapper/<>c::<>9__24_2
	ExporterFunc_t173265409 * ___U3CU3E9__24_2_3;
	// ThirdParty.Json.LitJson.ExporterFunc ThirdParty.Json.LitJson.JsonMapper/<>c::<>9__24_3
	ExporterFunc_t173265409 * ___U3CU3E9__24_3_4;
	// ThirdParty.Json.LitJson.ExporterFunc ThirdParty.Json.LitJson.JsonMapper/<>c::<>9__24_4
	ExporterFunc_t173265409 * ___U3CU3E9__24_4_5;
	// ThirdParty.Json.LitJson.ExporterFunc ThirdParty.Json.LitJson.JsonMapper/<>c::<>9__24_5
	ExporterFunc_t173265409 * ___U3CU3E9__24_5_6;
	// ThirdParty.Json.LitJson.ExporterFunc ThirdParty.Json.LitJson.JsonMapper/<>c::<>9__24_6
	ExporterFunc_t173265409 * ___U3CU3E9__24_6_7;
	// ThirdParty.Json.LitJson.ExporterFunc ThirdParty.Json.LitJson.JsonMapper/<>c::<>9__24_7
	ExporterFunc_t173265409 * ___U3CU3E9__24_7_8;
	// ThirdParty.Json.LitJson.ExporterFunc ThirdParty.Json.LitJson.JsonMapper/<>c::<>9__24_8
	ExporterFunc_t173265409 * ___U3CU3E9__24_8_9;
	// ThirdParty.Json.LitJson.ExporterFunc ThirdParty.Json.LitJson.JsonMapper/<>c::<>9__24_9
	ExporterFunc_t173265409 * ___U3CU3E9__24_9_10;
	// ThirdParty.Json.LitJson.ExporterFunc ThirdParty.Json.LitJson.JsonMapper/<>c::<>9__24_10
	ExporterFunc_t173265409 * ___U3CU3E9__24_10_11;
	// ThirdParty.Json.LitJson.ImporterFunc ThirdParty.Json.LitJson.JsonMapper/<>c::<>9__25_0
	ImporterFunc_t850687278 * ___U3CU3E9__25_0_12;
	// ThirdParty.Json.LitJson.ImporterFunc ThirdParty.Json.LitJson.JsonMapper/<>c::<>9__25_1
	ImporterFunc_t850687278 * ___U3CU3E9__25_1_13;
	// ThirdParty.Json.LitJson.ImporterFunc ThirdParty.Json.LitJson.JsonMapper/<>c::<>9__25_2
	ImporterFunc_t850687278 * ___U3CU3E9__25_2_14;
	// ThirdParty.Json.LitJson.ImporterFunc ThirdParty.Json.LitJson.JsonMapper/<>c::<>9__25_3
	ImporterFunc_t850687278 * ___U3CU3E9__25_3_15;
	// ThirdParty.Json.LitJson.ImporterFunc ThirdParty.Json.LitJson.JsonMapper/<>c::<>9__25_4
	ImporterFunc_t850687278 * ___U3CU3E9__25_4_16;
	// ThirdParty.Json.LitJson.ImporterFunc ThirdParty.Json.LitJson.JsonMapper/<>c::<>9__25_5
	ImporterFunc_t850687278 * ___U3CU3E9__25_5_17;
	// ThirdParty.Json.LitJson.ImporterFunc ThirdParty.Json.LitJson.JsonMapper/<>c::<>9__25_6
	ImporterFunc_t850687278 * ___U3CU3E9__25_6_18;
	// ThirdParty.Json.LitJson.ImporterFunc ThirdParty.Json.LitJson.JsonMapper/<>c::<>9__25_7
	ImporterFunc_t850687278 * ___U3CU3E9__25_7_19;
	// ThirdParty.Json.LitJson.ImporterFunc ThirdParty.Json.LitJson.JsonMapper/<>c::<>9__25_8
	ImporterFunc_t850687278 * ___U3CU3E9__25_8_20;
	// ThirdParty.Json.LitJson.ImporterFunc ThirdParty.Json.LitJson.JsonMapper/<>c::<>9__25_9
	ImporterFunc_t850687278 * ___U3CU3E9__25_9_21;
	// ThirdParty.Json.LitJson.ImporterFunc ThirdParty.Json.LitJson.JsonMapper/<>c::<>9__25_10
	ImporterFunc_t850687278 * ___U3CU3E9__25_10_22;
	// ThirdParty.Json.LitJson.ImporterFunc ThirdParty.Json.LitJson.JsonMapper/<>c::<>9__25_11
	ImporterFunc_t850687278 * ___U3CU3E9__25_11_23;
	// ThirdParty.Json.LitJson.ImporterFunc ThirdParty.Json.LitJson.JsonMapper/<>c::<>9__25_12
	ImporterFunc_t850687278 * ___U3CU3E9__25_12_24;
	// ThirdParty.Json.LitJson.ImporterFunc ThirdParty.Json.LitJson.JsonMapper/<>c::<>9__25_13
	ImporterFunc_t850687278 * ___U3CU3E9__25_13_25;
	// ThirdParty.Json.LitJson.WrapperFactory ThirdParty.Json.LitJson.JsonMapper/<>c::<>9__31_0
	WrapperFactory_t327905379 * ___U3CU3E9__31_0_26;
	// ThirdParty.Json.LitJson.WrapperFactory ThirdParty.Json.LitJson.JsonMapper/<>c::<>9__32_0
	WrapperFactory_t327905379 * ___U3CU3E9__32_0_27;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1591091833_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t1591091833 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t1591091833 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t1591091833 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9_0, value);
	}

	inline static int32_t get_offset_of_U3CU3E9__24_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1591091833_StaticFields, ___U3CU3E9__24_0_1)); }
	inline ExporterFunc_t173265409 * get_U3CU3E9__24_0_1() const { return ___U3CU3E9__24_0_1; }
	inline ExporterFunc_t173265409 ** get_address_of_U3CU3E9__24_0_1() { return &___U3CU3E9__24_0_1; }
	inline void set_U3CU3E9__24_0_1(ExporterFunc_t173265409 * value)
	{
		___U3CU3E9__24_0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9__24_0_1, value);
	}

	inline static int32_t get_offset_of_U3CU3E9__24_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1591091833_StaticFields, ___U3CU3E9__24_1_2)); }
	inline ExporterFunc_t173265409 * get_U3CU3E9__24_1_2() const { return ___U3CU3E9__24_1_2; }
	inline ExporterFunc_t173265409 ** get_address_of_U3CU3E9__24_1_2() { return &___U3CU3E9__24_1_2; }
	inline void set_U3CU3E9__24_1_2(ExporterFunc_t173265409 * value)
	{
		___U3CU3E9__24_1_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9__24_1_2, value);
	}

	inline static int32_t get_offset_of_U3CU3E9__24_2_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1591091833_StaticFields, ___U3CU3E9__24_2_3)); }
	inline ExporterFunc_t173265409 * get_U3CU3E9__24_2_3() const { return ___U3CU3E9__24_2_3; }
	inline ExporterFunc_t173265409 ** get_address_of_U3CU3E9__24_2_3() { return &___U3CU3E9__24_2_3; }
	inline void set_U3CU3E9__24_2_3(ExporterFunc_t173265409 * value)
	{
		___U3CU3E9__24_2_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9__24_2_3, value);
	}

	inline static int32_t get_offset_of_U3CU3E9__24_3_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1591091833_StaticFields, ___U3CU3E9__24_3_4)); }
	inline ExporterFunc_t173265409 * get_U3CU3E9__24_3_4() const { return ___U3CU3E9__24_3_4; }
	inline ExporterFunc_t173265409 ** get_address_of_U3CU3E9__24_3_4() { return &___U3CU3E9__24_3_4; }
	inline void set_U3CU3E9__24_3_4(ExporterFunc_t173265409 * value)
	{
		___U3CU3E9__24_3_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9__24_3_4, value);
	}

	inline static int32_t get_offset_of_U3CU3E9__24_4_5() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1591091833_StaticFields, ___U3CU3E9__24_4_5)); }
	inline ExporterFunc_t173265409 * get_U3CU3E9__24_4_5() const { return ___U3CU3E9__24_4_5; }
	inline ExporterFunc_t173265409 ** get_address_of_U3CU3E9__24_4_5() { return &___U3CU3E9__24_4_5; }
	inline void set_U3CU3E9__24_4_5(ExporterFunc_t173265409 * value)
	{
		___U3CU3E9__24_4_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9__24_4_5, value);
	}

	inline static int32_t get_offset_of_U3CU3E9__24_5_6() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1591091833_StaticFields, ___U3CU3E9__24_5_6)); }
	inline ExporterFunc_t173265409 * get_U3CU3E9__24_5_6() const { return ___U3CU3E9__24_5_6; }
	inline ExporterFunc_t173265409 ** get_address_of_U3CU3E9__24_5_6() { return &___U3CU3E9__24_5_6; }
	inline void set_U3CU3E9__24_5_6(ExporterFunc_t173265409 * value)
	{
		___U3CU3E9__24_5_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9__24_5_6, value);
	}

	inline static int32_t get_offset_of_U3CU3E9__24_6_7() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1591091833_StaticFields, ___U3CU3E9__24_6_7)); }
	inline ExporterFunc_t173265409 * get_U3CU3E9__24_6_7() const { return ___U3CU3E9__24_6_7; }
	inline ExporterFunc_t173265409 ** get_address_of_U3CU3E9__24_6_7() { return &___U3CU3E9__24_6_7; }
	inline void set_U3CU3E9__24_6_7(ExporterFunc_t173265409 * value)
	{
		___U3CU3E9__24_6_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9__24_6_7, value);
	}

	inline static int32_t get_offset_of_U3CU3E9__24_7_8() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1591091833_StaticFields, ___U3CU3E9__24_7_8)); }
	inline ExporterFunc_t173265409 * get_U3CU3E9__24_7_8() const { return ___U3CU3E9__24_7_8; }
	inline ExporterFunc_t173265409 ** get_address_of_U3CU3E9__24_7_8() { return &___U3CU3E9__24_7_8; }
	inline void set_U3CU3E9__24_7_8(ExporterFunc_t173265409 * value)
	{
		___U3CU3E9__24_7_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9__24_7_8, value);
	}

	inline static int32_t get_offset_of_U3CU3E9__24_8_9() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1591091833_StaticFields, ___U3CU3E9__24_8_9)); }
	inline ExporterFunc_t173265409 * get_U3CU3E9__24_8_9() const { return ___U3CU3E9__24_8_9; }
	inline ExporterFunc_t173265409 ** get_address_of_U3CU3E9__24_8_9() { return &___U3CU3E9__24_8_9; }
	inline void set_U3CU3E9__24_8_9(ExporterFunc_t173265409 * value)
	{
		___U3CU3E9__24_8_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9__24_8_9, value);
	}

	inline static int32_t get_offset_of_U3CU3E9__24_9_10() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1591091833_StaticFields, ___U3CU3E9__24_9_10)); }
	inline ExporterFunc_t173265409 * get_U3CU3E9__24_9_10() const { return ___U3CU3E9__24_9_10; }
	inline ExporterFunc_t173265409 ** get_address_of_U3CU3E9__24_9_10() { return &___U3CU3E9__24_9_10; }
	inline void set_U3CU3E9__24_9_10(ExporterFunc_t173265409 * value)
	{
		___U3CU3E9__24_9_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9__24_9_10, value);
	}

	inline static int32_t get_offset_of_U3CU3E9__24_10_11() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1591091833_StaticFields, ___U3CU3E9__24_10_11)); }
	inline ExporterFunc_t173265409 * get_U3CU3E9__24_10_11() const { return ___U3CU3E9__24_10_11; }
	inline ExporterFunc_t173265409 ** get_address_of_U3CU3E9__24_10_11() { return &___U3CU3E9__24_10_11; }
	inline void set_U3CU3E9__24_10_11(ExporterFunc_t173265409 * value)
	{
		___U3CU3E9__24_10_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9__24_10_11, value);
	}

	inline static int32_t get_offset_of_U3CU3E9__25_0_12() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1591091833_StaticFields, ___U3CU3E9__25_0_12)); }
	inline ImporterFunc_t850687278 * get_U3CU3E9__25_0_12() const { return ___U3CU3E9__25_0_12; }
	inline ImporterFunc_t850687278 ** get_address_of_U3CU3E9__25_0_12() { return &___U3CU3E9__25_0_12; }
	inline void set_U3CU3E9__25_0_12(ImporterFunc_t850687278 * value)
	{
		___U3CU3E9__25_0_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9__25_0_12, value);
	}

	inline static int32_t get_offset_of_U3CU3E9__25_1_13() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1591091833_StaticFields, ___U3CU3E9__25_1_13)); }
	inline ImporterFunc_t850687278 * get_U3CU3E9__25_1_13() const { return ___U3CU3E9__25_1_13; }
	inline ImporterFunc_t850687278 ** get_address_of_U3CU3E9__25_1_13() { return &___U3CU3E9__25_1_13; }
	inline void set_U3CU3E9__25_1_13(ImporterFunc_t850687278 * value)
	{
		___U3CU3E9__25_1_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9__25_1_13, value);
	}

	inline static int32_t get_offset_of_U3CU3E9__25_2_14() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1591091833_StaticFields, ___U3CU3E9__25_2_14)); }
	inline ImporterFunc_t850687278 * get_U3CU3E9__25_2_14() const { return ___U3CU3E9__25_2_14; }
	inline ImporterFunc_t850687278 ** get_address_of_U3CU3E9__25_2_14() { return &___U3CU3E9__25_2_14; }
	inline void set_U3CU3E9__25_2_14(ImporterFunc_t850687278 * value)
	{
		___U3CU3E9__25_2_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9__25_2_14, value);
	}

	inline static int32_t get_offset_of_U3CU3E9__25_3_15() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1591091833_StaticFields, ___U3CU3E9__25_3_15)); }
	inline ImporterFunc_t850687278 * get_U3CU3E9__25_3_15() const { return ___U3CU3E9__25_3_15; }
	inline ImporterFunc_t850687278 ** get_address_of_U3CU3E9__25_3_15() { return &___U3CU3E9__25_3_15; }
	inline void set_U3CU3E9__25_3_15(ImporterFunc_t850687278 * value)
	{
		___U3CU3E9__25_3_15 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9__25_3_15, value);
	}

	inline static int32_t get_offset_of_U3CU3E9__25_4_16() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1591091833_StaticFields, ___U3CU3E9__25_4_16)); }
	inline ImporterFunc_t850687278 * get_U3CU3E9__25_4_16() const { return ___U3CU3E9__25_4_16; }
	inline ImporterFunc_t850687278 ** get_address_of_U3CU3E9__25_4_16() { return &___U3CU3E9__25_4_16; }
	inline void set_U3CU3E9__25_4_16(ImporterFunc_t850687278 * value)
	{
		___U3CU3E9__25_4_16 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9__25_4_16, value);
	}

	inline static int32_t get_offset_of_U3CU3E9__25_5_17() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1591091833_StaticFields, ___U3CU3E9__25_5_17)); }
	inline ImporterFunc_t850687278 * get_U3CU3E9__25_5_17() const { return ___U3CU3E9__25_5_17; }
	inline ImporterFunc_t850687278 ** get_address_of_U3CU3E9__25_5_17() { return &___U3CU3E9__25_5_17; }
	inline void set_U3CU3E9__25_5_17(ImporterFunc_t850687278 * value)
	{
		___U3CU3E9__25_5_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9__25_5_17, value);
	}

	inline static int32_t get_offset_of_U3CU3E9__25_6_18() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1591091833_StaticFields, ___U3CU3E9__25_6_18)); }
	inline ImporterFunc_t850687278 * get_U3CU3E9__25_6_18() const { return ___U3CU3E9__25_6_18; }
	inline ImporterFunc_t850687278 ** get_address_of_U3CU3E9__25_6_18() { return &___U3CU3E9__25_6_18; }
	inline void set_U3CU3E9__25_6_18(ImporterFunc_t850687278 * value)
	{
		___U3CU3E9__25_6_18 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9__25_6_18, value);
	}

	inline static int32_t get_offset_of_U3CU3E9__25_7_19() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1591091833_StaticFields, ___U3CU3E9__25_7_19)); }
	inline ImporterFunc_t850687278 * get_U3CU3E9__25_7_19() const { return ___U3CU3E9__25_7_19; }
	inline ImporterFunc_t850687278 ** get_address_of_U3CU3E9__25_7_19() { return &___U3CU3E9__25_7_19; }
	inline void set_U3CU3E9__25_7_19(ImporterFunc_t850687278 * value)
	{
		___U3CU3E9__25_7_19 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9__25_7_19, value);
	}

	inline static int32_t get_offset_of_U3CU3E9__25_8_20() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1591091833_StaticFields, ___U3CU3E9__25_8_20)); }
	inline ImporterFunc_t850687278 * get_U3CU3E9__25_8_20() const { return ___U3CU3E9__25_8_20; }
	inline ImporterFunc_t850687278 ** get_address_of_U3CU3E9__25_8_20() { return &___U3CU3E9__25_8_20; }
	inline void set_U3CU3E9__25_8_20(ImporterFunc_t850687278 * value)
	{
		___U3CU3E9__25_8_20 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9__25_8_20, value);
	}

	inline static int32_t get_offset_of_U3CU3E9__25_9_21() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1591091833_StaticFields, ___U3CU3E9__25_9_21)); }
	inline ImporterFunc_t850687278 * get_U3CU3E9__25_9_21() const { return ___U3CU3E9__25_9_21; }
	inline ImporterFunc_t850687278 ** get_address_of_U3CU3E9__25_9_21() { return &___U3CU3E9__25_9_21; }
	inline void set_U3CU3E9__25_9_21(ImporterFunc_t850687278 * value)
	{
		___U3CU3E9__25_9_21 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9__25_9_21, value);
	}

	inline static int32_t get_offset_of_U3CU3E9__25_10_22() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1591091833_StaticFields, ___U3CU3E9__25_10_22)); }
	inline ImporterFunc_t850687278 * get_U3CU3E9__25_10_22() const { return ___U3CU3E9__25_10_22; }
	inline ImporterFunc_t850687278 ** get_address_of_U3CU3E9__25_10_22() { return &___U3CU3E9__25_10_22; }
	inline void set_U3CU3E9__25_10_22(ImporterFunc_t850687278 * value)
	{
		___U3CU3E9__25_10_22 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9__25_10_22, value);
	}

	inline static int32_t get_offset_of_U3CU3E9__25_11_23() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1591091833_StaticFields, ___U3CU3E9__25_11_23)); }
	inline ImporterFunc_t850687278 * get_U3CU3E9__25_11_23() const { return ___U3CU3E9__25_11_23; }
	inline ImporterFunc_t850687278 ** get_address_of_U3CU3E9__25_11_23() { return &___U3CU3E9__25_11_23; }
	inline void set_U3CU3E9__25_11_23(ImporterFunc_t850687278 * value)
	{
		___U3CU3E9__25_11_23 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9__25_11_23, value);
	}

	inline static int32_t get_offset_of_U3CU3E9__25_12_24() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1591091833_StaticFields, ___U3CU3E9__25_12_24)); }
	inline ImporterFunc_t850687278 * get_U3CU3E9__25_12_24() const { return ___U3CU3E9__25_12_24; }
	inline ImporterFunc_t850687278 ** get_address_of_U3CU3E9__25_12_24() { return &___U3CU3E9__25_12_24; }
	inline void set_U3CU3E9__25_12_24(ImporterFunc_t850687278 * value)
	{
		___U3CU3E9__25_12_24 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9__25_12_24, value);
	}

	inline static int32_t get_offset_of_U3CU3E9__25_13_25() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1591091833_StaticFields, ___U3CU3E9__25_13_25)); }
	inline ImporterFunc_t850687278 * get_U3CU3E9__25_13_25() const { return ___U3CU3E9__25_13_25; }
	inline ImporterFunc_t850687278 ** get_address_of_U3CU3E9__25_13_25() { return &___U3CU3E9__25_13_25; }
	inline void set_U3CU3E9__25_13_25(ImporterFunc_t850687278 * value)
	{
		___U3CU3E9__25_13_25 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9__25_13_25, value);
	}

	inline static int32_t get_offset_of_U3CU3E9__31_0_26() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1591091833_StaticFields, ___U3CU3E9__31_0_26)); }
	inline WrapperFactory_t327905379 * get_U3CU3E9__31_0_26() const { return ___U3CU3E9__31_0_26; }
	inline WrapperFactory_t327905379 ** get_address_of_U3CU3E9__31_0_26() { return &___U3CU3E9__31_0_26; }
	inline void set_U3CU3E9__31_0_26(WrapperFactory_t327905379 * value)
	{
		___U3CU3E9__31_0_26 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9__31_0_26, value);
	}

	inline static int32_t get_offset_of_U3CU3E9__32_0_27() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1591091833_StaticFields, ___U3CU3E9__32_0_27)); }
	inline WrapperFactory_t327905379 * get_U3CU3E9__32_0_27() const { return ___U3CU3E9__32_0_27; }
	inline WrapperFactory_t327905379 ** get_address_of_U3CU3E9__32_0_27() { return &___U3CU3E9__32_0_27; }
	inline void set_U3CU3E9__32_0_27(WrapperFactory_t327905379 * value)
	{
		___U3CU3E9__32_0_27 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9__32_0_27, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
