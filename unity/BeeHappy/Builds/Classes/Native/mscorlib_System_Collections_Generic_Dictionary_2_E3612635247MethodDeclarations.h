﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2464585190MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Collections.Generic.List`1<Amazon.Runtime.IMetricsTiming>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m773803187(__this, ___dictionary0, method) ((  void (*) (Enumerator_t3612635247 *, Dictionary_2_t2292610545 *, const MethodInfo*))Enumerator__ctor_m2882573199_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Collections.Generic.List`1<Amazon.Runtime.IMetricsTiming>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1927985826(__this, method) ((  Il2CppObject * (*) (Enumerator_t3612635247 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2753081182_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Collections.Generic.List`1<Amazon.Runtime.IMetricsTiming>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1551391220(__this, method) ((  void (*) (Enumerator_t3612635247 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3448018626_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Collections.Generic.List`1<Amazon.Runtime.IMetricsTiming>>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1940767097(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t3612635247 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1386174965_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Collections.Generic.List`1<Amazon.Runtime.IMetricsTiming>>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1273164268(__this, method) ((  Il2CppObject * (*) (Enumerator_t3612635247 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3637591552_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Collections.Generic.List`1<Amazon.Runtime.IMetricsTiming>>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3930888606(__this, method) ((  Il2CppObject * (*) (Enumerator_t3612635247 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1974664938_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Collections.Generic.List`1<Amazon.Runtime.IMetricsTiming>>::MoveNext()
#define Enumerator_MoveNext_m829788909(__this, method) ((  bool (*) (Enumerator_t3612635247 *, const MethodInfo*))Enumerator_MoveNext_m3844071554_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Collections.Generic.List`1<Amazon.Runtime.IMetricsTiming>>::get_Current()
#define Enumerator_get_Current_m3280826578(__this, method) ((  KeyValuePair_2_t49955767  (*) (Enumerator_t3612635247 *, const MethodInfo*))Enumerator_get_Current_m1831421754_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Collections.Generic.List`1<Amazon.Runtime.IMetricsTiming>>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m3736665031(__this, method) ((  int32_t (*) (Enumerator_t3612635247 *, const MethodInfo*))Enumerator_get_CurrentKey_m2750410819_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Collections.Generic.List`1<Amazon.Runtime.IMetricsTiming>>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m1492466239(__this, method) ((  List_1_t3837499352 * (*) (Enumerator_t3612635247 *, const MethodInfo*))Enumerator_get_CurrentValue_m4218670947_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Collections.Generic.List`1<Amazon.Runtime.IMetricsTiming>>::Reset()
#define Enumerator_Reset_m4202646993(__this, method) ((  void (*) (Enumerator_t3612635247 *, const MethodInfo*))Enumerator_Reset_m1867529997_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Collections.Generic.List`1<Amazon.Runtime.IMetricsTiming>>::VerifyState()
#define Enumerator_VerifyState_m2914311388(__this, method) ((  void (*) (Enumerator_t3612635247 *, const MethodInfo*))Enumerator_VerifyState_m1404756950_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Collections.Generic.List`1<Amazon.Runtime.IMetricsTiming>>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m3693113252(__this, method) ((  void (*) (Enumerator_t3612635247 *, const MethodInfo*))Enumerator_VerifyCurrent_m4158200186_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Collections.Generic.List`1<Amazon.Runtime.IMetricsTiming>>::Dispose()
#define Enumerator_Dispose_m721827519(__this, method) ((  void (*) (Enumerator_t3612635247 *, const MethodInfo*))Enumerator_Dispose_m2167966803_gshared)(__this, method)
