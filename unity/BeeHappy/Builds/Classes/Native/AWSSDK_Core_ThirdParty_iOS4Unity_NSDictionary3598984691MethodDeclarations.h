﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ThirdParty.iOS4Unity.NSDictionary
struct NSDictionary_t3598984691;
// ThirdParty.iOS4Unity.NSObject
struct NSObject_t1518098886;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ThirdParty.iOS4Unity.NSDictionary::.cctor()
extern "C"  void NSDictionary__cctor_m1539228212 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.iOS4Unity.NSDictionary::.ctor(System.IntPtr)
extern "C"  void NSDictionary__ctor_m3585948123 (NSDictionary_t3598984691 * __this, IntPtr_t ___handle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ThirdParty.iOS4Unity.NSObject ThirdParty.iOS4Unity.NSDictionary::ObjectForKey(System.String)
extern "C"  NSObject_t1518098886 * NSDictionary_ObjectForKey_m3963796504 (NSDictionary_t3598984691 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
