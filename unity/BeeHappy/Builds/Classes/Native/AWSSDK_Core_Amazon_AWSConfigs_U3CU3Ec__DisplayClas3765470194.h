﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.TextAsset
struct TextAsset_t3973159845;
// System.Xml.Linq.XDocument
struct XDocument_t2733326047;
// System.Action
struct Action_t3226471752;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.AWSConfigs/<>c__DisplayClass89_0
struct  U3CU3Ec__DisplayClass89_0_t3765470194  : public Il2CppObject
{
public:
	// UnityEngine.TextAsset Amazon.AWSConfigs/<>c__DisplayClass89_0::awsConfig
	TextAsset_t3973159845 * ___awsConfig_0;
	// System.Xml.Linq.XDocument Amazon.AWSConfigs/<>c__DisplayClass89_0::xDoc
	XDocument_t2733326047 * ___xDoc_1;
	// System.Action Amazon.AWSConfigs/<>c__DisplayClass89_0::action
	Action_t3226471752 * ___action_2;

public:
	inline static int32_t get_offset_of_awsConfig_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass89_0_t3765470194, ___awsConfig_0)); }
	inline TextAsset_t3973159845 * get_awsConfig_0() const { return ___awsConfig_0; }
	inline TextAsset_t3973159845 ** get_address_of_awsConfig_0() { return &___awsConfig_0; }
	inline void set_awsConfig_0(TextAsset_t3973159845 * value)
	{
		___awsConfig_0 = value;
		Il2CppCodeGenWriteBarrier(&___awsConfig_0, value);
	}

	inline static int32_t get_offset_of_xDoc_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass89_0_t3765470194, ___xDoc_1)); }
	inline XDocument_t2733326047 * get_xDoc_1() const { return ___xDoc_1; }
	inline XDocument_t2733326047 ** get_address_of_xDoc_1() { return &___xDoc_1; }
	inline void set_xDoc_1(XDocument_t2733326047 * value)
	{
		___xDoc_1 = value;
		Il2CppCodeGenWriteBarrier(&___xDoc_1, value);
	}

	inline static int32_t get_offset_of_action_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass89_0_t3765470194, ___action_2)); }
	inline Action_t3226471752 * get_action_2() const { return ___action_2; }
	inline Action_t3226471752 ** get_address_of_action_2() { return &___action_2; }
	inline void set_action_2(Action_t3226471752 * value)
	{
		___action_2 = value;
		Il2CppCodeGenWriteBarrier(&___action_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
