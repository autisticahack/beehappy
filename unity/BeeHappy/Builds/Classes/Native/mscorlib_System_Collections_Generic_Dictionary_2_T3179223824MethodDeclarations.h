﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2573087293MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Type,ThirdParty.Json.LitJson.ArrayMetadata,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
#define Transform_1__ctor_m2665700212(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t3179223824 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m3198348921_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Type,ThirdParty.Json.LitJson.ArrayMetadata,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m3254504892(__this, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Transform_1_t3179223824 *, Type_t *, ArrayMetadata_t1135078014 , const MethodInfo*))Transform_1_Invoke_m2118969653_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Type,ThirdParty.Json.LitJson.ArrayMetadata,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
#define Transform_1_BeginInvoke_m3341688519(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t3179223824 *, Type_t *, ArrayMetadata_t1135078014 , AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m4063938384_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Type,ThirdParty.Json.LitJson.ArrayMetadata,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
#define Transform_1_EndInvoke_m240307658(__this, ___result0, method) ((  DictionaryEntry_t3048875398  (*) (Transform_1_t3179223824 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m1085171679_gshared)(__this, ___result0, method)
