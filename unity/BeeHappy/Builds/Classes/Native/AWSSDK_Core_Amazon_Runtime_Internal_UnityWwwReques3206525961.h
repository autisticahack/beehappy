﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.Runtime.Internal.UnityWwwRequest
struct UnityWwwRequest_t118609659;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.UnityWwwRequestFactory
struct  UnityWwwRequestFactory_t3206525961  : public Il2CppObject
{
public:
	// Amazon.Runtime.Internal.UnityWwwRequest Amazon.Runtime.Internal.UnityWwwRequestFactory::_unityWwwRequest
	UnityWwwRequest_t118609659 * ____unityWwwRequest_0;

public:
	inline static int32_t get_offset_of__unityWwwRequest_0() { return static_cast<int32_t>(offsetof(UnityWwwRequestFactory_t3206525961, ____unityWwwRequest_0)); }
	inline UnityWwwRequest_t118609659 * get__unityWwwRequest_0() const { return ____unityWwwRequest_0; }
	inline UnityWwwRequest_t118609659 ** get_address_of__unityWwwRequest_0() { return &____unityWwwRequest_0; }
	inline void set__unityWwwRequest_0(UnityWwwRequest_t118609659 * value)
	{
		____unityWwwRequest_0 = value;
		Il2CppCodeGenWriteBarrier(&____unityWwwRequest_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
