﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.Runtime.Internal.Transform.JsonErrorResponseUnmarshaller
struct JsonErrorResponseUnmarshaller_t4149926919;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Transform.JsonErrorResponseUnmarshaller
struct  JsonErrorResponseUnmarshaller_t4149926919  : public Il2CppObject
{
public:

public:
};

struct JsonErrorResponseUnmarshaller_t4149926919_StaticFields
{
public:
	// Amazon.Runtime.Internal.Transform.JsonErrorResponseUnmarshaller Amazon.Runtime.Internal.Transform.JsonErrorResponseUnmarshaller::instance
	JsonErrorResponseUnmarshaller_t4149926919 * ___instance_0;

public:
	inline static int32_t get_offset_of_instance_0() { return static_cast<int32_t>(offsetof(JsonErrorResponseUnmarshaller_t4149926919_StaticFields, ___instance_0)); }
	inline JsonErrorResponseUnmarshaller_t4149926919 * get_instance_0() const { return ___instance_0; }
	inline JsonErrorResponseUnmarshaller_t4149926919 ** get_address_of_instance_0() { return &___instance_0; }
	inline void set_instance_0(JsonErrorResponseUnmarshaller_t4149926919 * value)
	{
		___instance_0 = value;
		Il2CppCodeGenWriteBarrier(&___instance_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
