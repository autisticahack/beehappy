﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>
struct List_1_t146890807;
// System.Collections.Generic.IEnumerable`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>
struct IEnumerable_1_t1069896720;
// System.Collections.Generic.IEnumerator`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>
struct IEnumerator_1_t2548260798;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>
struct ICollection_1_t1729844980;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>
struct ReadOnlyCollection_1_t963555367;
// Mono.Data.Sqlite.SqliteKeyReader/KeyInfo[]
struct KeyInfoU5BU5D_t3526347178;
// System.Predicate`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>
struct Predicate_1_t3515707086;
// System.Collections.Generic.IComparer`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>
struct IComparer_1_t3027200093;
// System.Comparison`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>
struct Comparison_1_t2039508526;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteKeyReader_K777769675.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3976587777.h"

// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::.ctor()
extern "C"  void List_1__ctor_m4221036429_gshared (List_1_t146890807 * __this, const MethodInfo* method);
#define List_1__ctor_m4221036429(__this, method) ((  void (*) (List_1_t146890807 *, const MethodInfo*))List_1__ctor_m4221036429_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m1693782376_gshared (List_1_t146890807 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m1693782376(__this, ___collection0, method) ((  void (*) (List_1_t146890807 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m1693782376_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m1543610494_gshared (List_1_t146890807 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m1543610494(__this, ___capacity0, method) ((  void (*) (List_1_t146890807 *, int32_t, const MethodInfo*))List_1__ctor_m1543610494_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::.cctor()
extern "C"  void List_1__cctor_m4235497202_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m4235497202(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m4235497202_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1993918843_gshared (List_1_t146890807 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1993918843(__this, method) ((  Il2CppObject* (*) (List_1_t146890807 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1993918843_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m1844884631_gshared (List_1_t146890807 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m1844884631(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t146890807 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m1844884631_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m1392282144_gshared (List_1_t146890807 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m1392282144(__this, method) ((  Il2CppObject * (*) (List_1_t146890807 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1392282144_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m784789671_gshared (List_1_t146890807 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m784789671(__this, ___item0, method) ((  int32_t (*) (List_1_t146890807 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m784789671_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m2297756911_gshared (List_1_t146890807 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m2297756911(__this, ___item0, method) ((  bool (*) (List_1_t146890807 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m2297756911_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m699794481_gshared (List_1_t146890807 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m699794481(__this, ___item0, method) ((  int32_t (*) (List_1_t146890807 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m699794481_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m2235209058_gshared (List_1_t146890807 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m2235209058(__this, ___index0, ___item1, method) ((  void (*) (List_1_t146890807 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m2235209058_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m549889340_gshared (List_1_t146890807 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m549889340(__this, ___item0, method) ((  void (*) (List_1_t146890807 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m549889340_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m652897156_gshared (List_1_t146890807 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m652897156(__this, method) ((  bool (*) (List_1_t146890807 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m652897156_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m2716874911_gshared (List_1_t146890807 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m2716874911(__this, method) ((  bool (*) (List_1_t146890807 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m2716874911_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m780185587_gshared (List_1_t146890807 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m780185587(__this, method) ((  Il2CppObject * (*) (List_1_t146890807 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m780185587_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m3735848078_gshared (List_1_t146890807 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m3735848078(__this, method) ((  bool (*) (List_1_t146890807 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m3735848078_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m928062019_gshared (List_1_t146890807 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m928062019(__this, method) ((  bool (*) (List_1_t146890807 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m928062019_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m645820928_gshared (List_1_t146890807 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m645820928(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t146890807 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m645820928_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m2085358317_gshared (List_1_t146890807 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m2085358317(__this, ___index0, ___value1, method) ((  void (*) (List_1_t146890807 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m2085358317_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::Add(T)
extern "C"  void List_1_Add_m2815063937_gshared (List_1_t146890807 * __this, KeyInfo_t777769675  ___item0, const MethodInfo* method);
#define List_1_Add_m2815063937(__this, ___item0, method) ((  void (*) (List_1_t146890807 *, KeyInfo_t777769675 , const MethodInfo*))List_1_Add_m2815063937_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m2088301131_gshared (List_1_t146890807 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m2088301131(__this, ___newCount0, method) ((  void (*) (List_1_t146890807 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m2088301131_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m188392419_gshared (List_1_t146890807 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m188392419(__this, ___collection0, method) ((  void (*) (List_1_t146890807 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m188392419_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m2381338883_gshared (List_1_t146890807 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m2381338883(__this, ___enumerable0, method) ((  void (*) (List_1_t146890807 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m2381338883_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m4233832456_gshared (List_1_t146890807 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m4233832456(__this, ___collection0, method) ((  void (*) (List_1_t146890807 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m4233832456_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t963555367 * List_1_AsReadOnly_m2409301347_gshared (List_1_t146890807 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m2409301347(__this, method) ((  ReadOnlyCollection_1_t963555367 * (*) (List_1_t146890807 *, const MethodInfo*))List_1_AsReadOnly_m2409301347_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::Clear()
extern "C"  void List_1_Clear_m951464858_gshared (List_1_t146890807 * __this, const MethodInfo* method);
#define List_1_Clear_m951464858(__this, method) ((  void (*) (List_1_t146890807 *, const MethodInfo*))List_1_Clear_m951464858_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::Contains(T)
extern "C"  bool List_1_Contains_m4045485196_gshared (List_1_t146890807 * __this, KeyInfo_t777769675  ___item0, const MethodInfo* method);
#define List_1_Contains_m4045485196(__this, ___item0, method) ((  bool (*) (List_1_t146890807 *, KeyInfo_t777769675 , const MethodInfo*))List_1_Contains_m4045485196_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::CopyTo(T[])
extern "C"  void List_1_CopyTo_m3242741612_gshared (List_1_t146890807 * __this, KeyInfoU5BU5D_t3526347178* ___array0, const MethodInfo* method);
#define List_1_CopyTo_m3242741612(__this, ___array0, method) ((  void (*) (List_1_t146890807 *, KeyInfoU5BU5D_t3526347178*, const MethodInfo*))List_1_CopyTo_m3242741612_gshared)(__this, ___array0, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m3469899250_gshared (List_1_t146890807 * __this, KeyInfoU5BU5D_t3526347178* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m3469899250(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t146890807 *, KeyInfoU5BU5D_t3526347178*, int32_t, const MethodInfo*))List_1_CopyTo_m3469899250_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::Find(System.Predicate`1<T>)
extern "C"  KeyInfo_t777769675  List_1_Find_m4037001600_gshared (List_1_t146890807 * __this, Predicate_1_t3515707086 * ___match0, const MethodInfo* method);
#define List_1_Find_m4037001600(__this, ___match0, method) ((  KeyInfo_t777769675  (*) (List_1_t146890807 *, Predicate_1_t3515707086 *, const MethodInfo*))List_1_Find_m4037001600_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m3890664039_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t3515707086 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m3890664039(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t3515707086 *, const MethodInfo*))List_1_CheckMatch_m3890664039_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m2998779526_gshared (List_1_t146890807 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t3515707086 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m2998779526(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t146890807 *, int32_t, int32_t, Predicate_1_t3515707086 *, const MethodInfo*))List_1_GetIndex_m2998779526_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::GetEnumerator()
extern "C"  Enumerator_t3976587777  List_1_GetEnumerator_m2066083971_gshared (List_1_t146890807 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m2066083971(__this, method) ((  Enumerator_t3976587777  (*) (List_1_t146890807 *, const MethodInfo*))List_1_GetEnumerator_m2066083971_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m3546685372_gshared (List_1_t146890807 * __this, KeyInfo_t777769675  ___item0, const MethodInfo* method);
#define List_1_IndexOf_m3546685372(__this, ___item0, method) ((  int32_t (*) (List_1_t146890807 *, KeyInfo_t777769675 , const MethodInfo*))List_1_IndexOf_m3546685372_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m4064781683_gshared (List_1_t146890807 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m4064781683(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t146890807 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m4064781683_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m2266254874_gshared (List_1_t146890807 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m2266254874(__this, ___index0, method) ((  void (*) (List_1_t146890807 *, int32_t, const MethodInfo*))List_1_CheckIndex_m2266254874_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m3541223393_gshared (List_1_t146890807 * __this, int32_t ___index0, KeyInfo_t777769675  ___item1, const MethodInfo* method);
#define List_1_Insert_m3541223393(__this, ___index0, ___item1, method) ((  void (*) (List_1_t146890807 *, int32_t, KeyInfo_t777769675 , const MethodInfo*))List_1_Insert_m3541223393_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m2541592112_gshared (List_1_t146890807 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m2541592112(__this, ___collection0, method) ((  void (*) (List_1_t146890807 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m2541592112_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::Remove(T)
extern "C"  bool List_1_Remove_m1544951649_gshared (List_1_t146890807 * __this, KeyInfo_t777769675  ___item0, const MethodInfo* method);
#define List_1_Remove_m1544951649(__this, ___item0, method) ((  bool (*) (List_1_t146890807 *, KeyInfo_t777769675 , const MethodInfo*))List_1_Remove_m1544951649_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m568683711_gshared (List_1_t146890807 * __this, Predicate_1_t3515707086 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m568683711(__this, ___match0, method) ((  int32_t (*) (List_1_t146890807 *, Predicate_1_t3515707086 *, const MethodInfo*))List_1_RemoveAll_m568683711_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m3243198445_gshared (List_1_t146890807 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m3243198445(__this, ___index0, method) ((  void (*) (List_1_t146890807 *, int32_t, const MethodInfo*))List_1_RemoveAt_m3243198445_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::Reverse()
extern "C"  void List_1_Reverse_m1497570115_gshared (List_1_t146890807 * __this, const MethodInfo* method);
#define List_1_Reverse_m1497570115(__this, method) ((  void (*) (List_1_t146890807 *, const MethodInfo*))List_1_Reverse_m1497570115_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::Sort()
extern "C"  void List_1_Sort_m1050570641_gshared (List_1_t146890807 * __this, const MethodInfo* method);
#define List_1_Sort_m1050570641(__this, method) ((  void (*) (List_1_t146890807 *, const MethodInfo*))List_1_Sort_m1050570641_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m1097336325_gshared (List_1_t146890807 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m1097336325(__this, ___comparer0, method) ((  void (*) (List_1_t146890807 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m1097336325_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m2317118906_gshared (List_1_t146890807 * __this, Comparison_1_t2039508526 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m2317118906(__this, ___comparison0, method) ((  void (*) (List_1_t146890807 *, Comparison_1_t2039508526 *, const MethodInfo*))List_1_Sort_m2317118906_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::ToArray()
extern "C"  KeyInfoU5BU5D_t3526347178* List_1_ToArray_m3479274804_gshared (List_1_t146890807 * __this, const MethodInfo* method);
#define List_1_ToArray_m3479274804(__this, method) ((  KeyInfoU5BU5D_t3526347178* (*) (List_1_t146890807 *, const MethodInfo*))List_1_ToArray_m3479274804_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::TrimExcess()
extern "C"  void List_1_TrimExcess_m1651956024_gshared (List_1_t146890807 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m1651956024(__this, method) ((  void (*) (List_1_t146890807 *, const MethodInfo*))List_1_TrimExcess_m1651956024_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m3744024378_gshared (List_1_t146890807 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m3744024378(__this, method) ((  int32_t (*) (List_1_t146890807 *, const MethodInfo*))List_1_get_Capacity_m3744024378_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m824694019_gshared (List_1_t146890807 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m824694019(__this, ___value0, method) ((  void (*) (List_1_t146890807 *, int32_t, const MethodInfo*))List_1_set_Capacity_m824694019_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::get_Count()
extern "C"  int32_t List_1_get_Count_m1573363817_gshared (List_1_t146890807 * __this, const MethodInfo* method);
#define List_1_get_Count_m1573363817(__this, method) ((  int32_t (*) (List_1_t146890807 *, const MethodInfo*))List_1_get_Count_m1573363817_gshared)(__this, method)
// T System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::get_Item(System.Int32)
extern "C"  KeyInfo_t777769675  List_1_get_Item_m725117833_gshared (List_1_t146890807 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m725117833(__this, ___index0, method) ((  KeyInfo_t777769675  (*) (List_1_t146890807 *, int32_t, const MethodInfo*))List_1_get_Item_m725117833_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m920156696_gshared (List_1_t146890807 * __this, int32_t ___index0, KeyInfo_t777769675  ___value1, const MethodInfo* method);
#define List_1_set_Item_m920156696(__this, ___index0, ___value1, method) ((  void (*) (List_1_t146890807 *, int32_t, KeyInfo_t777769675 , const MethodInfo*))List_1_set_Item_m920156696_gshared)(__this, ___index0, ___value1, method)
