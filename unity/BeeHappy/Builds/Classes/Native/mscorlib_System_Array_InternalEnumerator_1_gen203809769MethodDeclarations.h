﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen203809769.h"
#include "mscorlib_System_Array3829468939.h"
#include "System_Core_System_Collections_Generic_HashSet_1_L3640024803.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Net.WebExceptionStatus>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2780584209_gshared (InternalEnumerator_1_t203809769 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m2780584209(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t203809769 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2780584209_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Net.WebExceptionStatus>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3305556013_gshared (InternalEnumerator_1_t203809769 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3305556013(__this, method) ((  void (*) (InternalEnumerator_1_t203809769 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3305556013_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Net.WebExceptionStatus>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m318160093_gshared (InternalEnumerator_1_t203809769 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m318160093(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t203809769 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m318160093_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Net.WebExceptionStatus>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2744553424_gshared (InternalEnumerator_1_t203809769 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2744553424(__this, method) ((  void (*) (InternalEnumerator_1_t203809769 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2744553424_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Net.WebExceptionStatus>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3539244549_gshared (InternalEnumerator_1_t203809769 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m3539244549(__this, method) ((  bool (*) (InternalEnumerator_1_t203809769 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3539244549_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Net.WebExceptionStatus>>::get_Current()
extern "C"  Link_t3640024803  InternalEnumerator_1_get_Current_m693061474_gshared (InternalEnumerator_1_t203809769 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m693061474(__this, method) ((  Link_t3640024803  (*) (InternalEnumerator_1_t203809769 *, const MethodInfo*))InternalEnumerator_1_get_Current_m693061474_gshared)(__this, method)
