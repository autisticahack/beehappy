﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Predicate_1_gen1132419410MethodDeclarations.h"

// System.Void System.Predicate`1<ThirdParty.Json.LitJson.JsonData>::.ctor(System.Object,System.IntPtr)
#define Predicate_1__ctor_m1466436819(__this, ___object0, ___method1, method) ((  void (*) (Predicate_1_t2706222167 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m2289454599_gshared)(__this, ___object0, ___method1, method)
// System.Boolean System.Predicate`1<ThirdParty.Json.LitJson.JsonData>::Invoke(T)
#define Predicate_1_Invoke_m208954107(__this, ___obj0, method) ((  bool (*) (Predicate_1_t2706222167 *, JsonData_t4263252052 *, const MethodInfo*))Predicate_1_Invoke_m4047721271_gshared)(__this, ___obj0, method)
// System.IAsyncResult System.Predicate`1<ThirdParty.Json.LitJson.JsonData>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m3408994888(__this, ___obj0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Predicate_1_t2706222167 *, JsonData_t4263252052 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Predicate_1_BeginInvoke_m3556950370_gshared)(__this, ___obj0, ___callback1, ___object2, method)
// System.Boolean System.Predicate`1<ThirdParty.Json.LitJson.JsonData>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m1216296753(__this, ___result0, method) ((  bool (*) (Predicate_1_t2706222167 *, Il2CppObject *, const MethodInfo*))Predicate_1_EndInvoke_m3656575065_gshared)(__this, ___result0, method)
