﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2275254010.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21416501748.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Amazon.Runtime.Metric,System.Int64>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3328237982_gshared (InternalEnumerator_1_t2275254010 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m3328237982(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2275254010 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3328237982_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Amazon.Runtime.Metric,System.Int64>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m236326766_gshared (InternalEnumerator_1_t2275254010 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m236326766(__this, method) ((  void (*) (InternalEnumerator_1_t2275254010 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m236326766_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Amazon.Runtime.Metric,System.Int64>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1664072336_gshared (InternalEnumerator_1_t2275254010 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1664072336(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2275254010 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1664072336_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Amazon.Runtime.Metric,System.Int64>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m623498131_gshared (InternalEnumerator_1_t2275254010 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m623498131(__this, method) ((  void (*) (InternalEnumerator_1_t2275254010 *, const MethodInfo*))InternalEnumerator_1_Dispose_m623498131_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Amazon.Runtime.Metric,System.Int64>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1397529990_gshared (InternalEnumerator_1_t2275254010 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1397529990(__this, method) ((  bool (*) (InternalEnumerator_1_t2275254010 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1397529990_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Amazon.Runtime.Metric,System.Int64>>::get_Current()
extern "C"  KeyValuePair_2_t1416501748  InternalEnumerator_1_get_Current_m1686240551_gshared (InternalEnumerator_1_t2275254010 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1686240551(__this, method) ((  KeyValuePair_2_t1416501748  (*) (InternalEnumerator_1_t2275254010 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1686240551_gshared)(__this, method)
