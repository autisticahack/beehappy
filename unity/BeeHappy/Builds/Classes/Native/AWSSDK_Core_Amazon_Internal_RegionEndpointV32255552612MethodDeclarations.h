﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Internal.RegionEndpointV3
struct RegionEndpointV3_t2255552612;
// System.String
struct String_t;
// ThirdParty.Json.LitJson.JsonData
struct JsonData_t4263252052;
// Amazon.RegionEndpoint/Endpoint
struct Endpoint_t3348692641;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_JsonData4263252052.h"

// System.String Amazon.Internal.RegionEndpointV3::get_RegionName()
extern "C"  String_t* RegionEndpointV3_get_RegionName_m1064315827 (RegionEndpointV3_t2255552612 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Internal.RegionEndpointV3::set_RegionName(System.String)
extern "C"  void RegionEndpointV3_set_RegionName_m1867594250 (RegionEndpointV3_t2255552612 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Internal.RegionEndpointV3::get_DisplayName()
extern "C"  String_t* RegionEndpointV3_get_DisplayName_m4259129729 (RegionEndpointV3_t2255552612 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Internal.RegionEndpointV3::set_DisplayName(System.String)
extern "C"  void RegionEndpointV3_set_DisplayName_m98818804 (RegionEndpointV3_t2255552612 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Internal.RegionEndpointV3::.ctor(System.String,System.String,ThirdParty.Json.LitJson.JsonData,ThirdParty.Json.LitJson.JsonData)
extern "C"  void RegionEndpointV3__ctor_m2188584794 (RegionEndpointV3_t2255552612 * __this, String_t* ___regionName0, String_t* ___displayName1, JsonData_t4263252052 * ___partition2, JsonData_t4263252052 * ___services3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.RegionEndpoint/Endpoint Amazon.Internal.RegionEndpointV3::GetEndpointForService(System.String,System.Boolean)
extern "C"  Endpoint_t3348692641 * RegionEndpointV3_GetEndpointForService_m1302679014 (RegionEndpointV3_t2255552612 * __this, String_t* ___serviceName0, bool ___dualStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.RegionEndpoint/Endpoint Amazon.Internal.RegionEndpointV3::CreateUnknownEndpoint(System.String,System.Boolean)
extern "C"  Endpoint_t3348692641 * RegionEndpointV3_CreateUnknownEndpoint_m1687537226 (RegionEndpointV3_t2255552612 * __this, String_t* ___serviceName0, bool ___dualStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Internal.RegionEndpointV3::ParseAllServices()
extern "C"  void RegionEndpointV3_ParseAllServices_m3166715360 (RegionEndpointV3_t2255552612 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Internal.RegionEndpointV3::AddServiceToMap(ThirdParty.Json.LitJson.JsonData,System.String)
extern "C"  void RegionEndpointV3_AddServiceToMap_m4106463847 (RegionEndpointV3_t2255552612 * __this, JsonData_t4263252052 * ___service0, String_t* ___serviceName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Internal.RegionEndpointV3::MergeJsonData(ThirdParty.Json.LitJson.JsonData,ThirdParty.Json.LitJson.JsonData)
extern "C"  void RegionEndpointV3_MergeJsonData_m307829668 (Il2CppObject * __this /* static, unused */, JsonData_t4263252052 * ___target0, JsonData_t4263252052 * ___source1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Internal.RegionEndpointV3::CreateEndpointAndAddToServiceMap(ThirdParty.Json.LitJson.JsonData,System.String,System.String)
extern "C"  void RegionEndpointV3_CreateEndpointAndAddToServiceMap_m14521297 (RegionEndpointV3_t2255552612 * __this, JsonData_t4263252052 * ___result0, String_t* ___regionName1, String_t* ___serviceName2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Internal.RegionEndpointV3::CreateEndpointAndAddToServiceMap(ThirdParty.Json.LitJson.JsonData,System.String,System.String,System.Boolean)
extern "C"  void RegionEndpointV3_CreateEndpointAndAddToServiceMap_m3165184268 (RegionEndpointV3_t2255552612 * __this, JsonData_t4263252052 * ___result0, String_t* ___regionName1, String_t* ___serviceName2, bool ___dualStack3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Internal.RegionEndpointV3::DetermineSignatureOverride(ThirdParty.Json.LitJson.JsonData,System.String)
extern "C"  String_t* RegionEndpointV3_DetermineSignatureOverride_m1080846990 (Il2CppObject * __this /* static, unused */, JsonData_t4263252052 * ___defaults0, String_t* ___serviceName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Internal.RegionEndpointV3::DetermineAuthRegion(ThirdParty.Json.LitJson.JsonData)
extern "C"  String_t* RegionEndpointV3_DetermineAuthRegion_m302674644 (Il2CppObject * __this /* static, unused */, JsonData_t4263252052 * ___credentialScope0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
