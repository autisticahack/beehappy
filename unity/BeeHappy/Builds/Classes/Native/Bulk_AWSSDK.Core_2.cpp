﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// ThirdParty.iOS4Unity.SKPaymentTransaction
struct SKPaymentTransaction_t2918057744;
// ThirdParty.iOS4Unity.SKProduct
struct SKProduct_t3309436227;
// ThirdParty.iOS4Unity.SKProductsRequest
struct SKProductsRequest_t3969930329;
// ThirdParty.iOS4Unity.SKProductsResponse
struct SKProductsResponse_t3540529521;
// ThirdParty.iOS4Unity.UIActionSheet
struct UIActionSheet_t2483183669;
// ThirdParty.iOS4Unity.UIActivityViewController
struct UIActivityViewController_t2230362714;
// ThirdParty.iOS4Unity.UIAlertView
struct UIAlertView_t2175232939;
// ThirdParty.iOS4Unity.UIApplication
struct UIApplication_t1857964820;
// ThirdParty.iOS4Unity.UIDevice
struct UIDevice_t860038178;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// ThirdParty.iOS4Unity.UIImage
struct UIImage_t3580593017;
// ThirdParty.iOS4Unity.UILocalNotification
struct UILocalNotification_t869364318;
// ThirdParty.iOS4Unity.UIScreen
struct UIScreen_t4048045402;
// ThirdParty.iOS4Unity.UIScreenMode
struct UIScreenMode_t609995313;
// ThirdParty.iOS4Unity.UIUserNotificationSettings
struct UIUserNotificationSettings_t4227938467;
// ThirdParty.iOS4Unity.UIView
struct UIView_t1452205135;
// ThirdParty.iOS4Unity.UIViewController
struct UIViewController_t1891120779;
// ThirdParty.iOS4Unity.UIWindow
struct UIWindow_t800018578;
// ThirdParty.Json.LitJson.ExporterFunc
struct ExporterFunc_t173265409;
// ThirdParty.Json.LitJson.JsonWriter
struct JsonWriter_t3014444111;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// ThirdParty.Json.LitJson.FsmContext
struct FsmContext_t4275593467;
// ThirdParty.Json.LitJson.ImporterFunc
struct ImporterFunc_t850687278;
// ThirdParty.Json.LitJson.JsonData
struct JsonData_t4263252052;
// System.Collections.ICollection
struct ICollection_t91669223;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t2321347278;
// System.Array
struct Il2CppArray;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t259680273;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.IDictionary
struct IDictionary_t596158605;
// System.Collections.IList
struct IList_t3321498491;
// ThirdParty.Json.LitJson.JsonException
struct JsonException_t1457213491;
// ThirdParty.Json.LitJson.IJsonWrapper
struct IJsonWrapper_t3095378610;
// ThirdParty.Json.LitJson.WrapperFactory
struct WrapperFactory_t327905379;
// ThirdParty.Json.LitJson.JsonReader
struct JsonReader_t354941621;
// System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,ThirdParty.Json.LitJson.ImporterFunc>>
struct IDictionary_2_t723569914;
// System.Type
struct Type_t;
// System.IO.TextReader
struct TextReader_t1561828458;
// ThirdParty.Json.LitJson.JsonMapper/<>c
struct U3CU3Ec_t1591091833;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;
// System.IO.TextWriter
struct TextWriter_t4027217640;
// System.Char[]
struct CharU5BU5D_t1328083999;
// ThirdParty.Json.LitJson.Lexer
struct Lexer_t954714164;
// ThirdParty.Json.LitJson.Lexer/StateHandler
struct StateHandler_t3489987002;
// ThirdParty.Json.LitJson.OrderedDictionaryEnumerator
struct OrderedDictionaryEnumerator_t1664192327;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,ThirdParty.Json.LitJson.JsonData>>
struct IEnumerator_1_t1410900363;
// ThirdParty.Json.LitJson.WriterContext
struct WriterContext_t1209007092;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_SKPaymentTransact2918057744.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_SKPaymentTransact2918057744MethodDeclarations.h"
#include "mscorlib_System_Void1841601450.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_ObjC468753822MethodDeclarations.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_NSObject1518098886MethodDeclarations.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_SKProduct3309436227.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_SKProduct3309436227MethodDeclarations.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_SKProductsRequest3969930329.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_SKProductsRequest3969930329MethodDeclarations.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_SKProductsRespons3540529521.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_SKProductsRespons3540529521MethodDeclarations.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_UIActionSheet2483183669.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_UIActionSheet2483183669MethodDeclarations.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_UIView1452205135MethodDeclarations.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_UIActivityViewCon2230362714.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_UIActivityViewCon2230362714MethodDeclarations.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_UIViewController1891120779MethodDeclarations.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_UIAlertView2175232939.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_UIAlertView2175232939MethodDeclarations.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_UIApplication1857964820.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_UIApplication1857964820MethodDeclarations.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_UIDevice860038178.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_UIDevice860038178MethodDeclarations.h"
#include "mscorlib_System_Int322071877448.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_Selector1939762325MethodDeclarations.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_Runtime1145037850MethodDeclarations.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_Runtime1145037850.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_NSObject1518098886.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_UIImage3580593017.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_UIImage3580593017MethodDeclarations.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_UILocalNotificatio869364318.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_UILocalNotificatio869364318MethodDeclarations.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_UIScreen4048045402.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_UIScreen4048045402MethodDeclarations.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_UIScreenMode609995313.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_UIScreenMode609995313MethodDeclarations.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_UIUserNotificatio4227938467.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_UIUserNotificatio4227938467MethodDeclarations.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_UIView1452205135.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_UIViewController1891120779.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_UIWindow800018578.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_UIWindow800018578MethodDeclarations.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_ArrayMetadata1135078014.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_ArrayMetadata1135078014MethodDeclarations.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_Condition2230619687.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_Condition2230619687MethodDeclarations.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_ExporterFunc173265409.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_ExporterFunc173265409MethodDeclarations.h"
#include "mscorlib_System_Object2689449295.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_JsonWriter3014444111.h"
#include "mscorlib_System_AsyncCallback163412349.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_FsmContext4275593467.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_FsmContext4275593467MethodDeclarations.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_ImporterFunc850687278.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_ImporterFunc850687278MethodDeclarations.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_JsonData4263252052.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_JsonData4263252052MethodDeclarations.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1398341365MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23935376536.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1398341365.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23935376536MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3632373184MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3632373184.h"
#include "mscorlib_System_ArgumentException3259014390MethodDeclarations.h"
#include "mscorlib_System_ArgumentException3259014390.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_JsonType808352724.h"
#include "mscorlib_System_Double4078015681.h"
#include "mscorlib_System_UInt642909196914.h"
#include "mscorlib_System_UInt322149682021.h"
#include "mscorlib_System_Int64909078037.h"
#include "mscorlib_System_InvalidCastException3625212209MethodDeclarations.h"
#include "mscorlib_System_InvalidCastException3625212209.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_OrderedDiction1664192327MethodDeclarations.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_OrderedDiction1664192327.h"
#include "mscorlib_System_InvalidOperationException721527559MethodDeclarations.h"
#include "mscorlib_System_InvalidOperationException721527559.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1883064018MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3304497668MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1883064018.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3304497668.h"
#include "mscorlib_System_UInt642909196914MethodDeclarations.h"
#include "mscorlib_System_Double4078015681MethodDeclarations.h"
#include "mscorlib_System_Boolean3825574718MethodDeclarations.h"
#include "mscorlib_System_Int322071877448MethodDeclarations.h"
#include "mscorlib_System_UInt322149682021MethodDeclarations.h"
#include "mscorlib_System_Int64909078037MethodDeclarations.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_JsonException1457213491.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_JsonException1457213491MethodDeclarations.h"
#include "mscorlib_System_Exception1927440687MethodDeclarations.h"
#include "mscorlib_System_Char3454481338.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_JsonMapper77474459.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_JsonMapper77474459MethodDeclarations.h"
#include "mscorlib_System_StringComparer1574862926MethodDeclarations.h"
#include "System_Core_System_Collections_Generic_HashSet_1_ge362681087MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3072435911MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge909378256MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1700528341MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1471071188MethodDeclarations.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_JsonWriter3014444111MethodDeclarations.h"
#include "mscorlib_System_Globalization_DateTimeFormatInfo2187473504MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2110623306MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2724486493MethodDeclarations.h"
#include "mscorlib_System_StringComparer1574862926.h"
#include "System_Core_System_Collections_Generic_HashSet_1_ge362681087.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3072435911.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge909378256.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1700528341.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1471071188.h"
#include "mscorlib_System_Globalization_DateTimeFormatInfo2187473504.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2110623306.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2724486493.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_WrapperFactory327905379.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_JsonReader354941621.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_JsonReader354941621MethodDeclarations.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_WrapperFactory327905379MethodDeclarations.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_JsonToken2445581255.h"
#include "mscorlib_System_Type1303803226MethodDeclarations.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_RuntimeTypeHandle2330101084.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_JsonMapper_U3C1591091833.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_JsonMapper_U3C1591091833MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2788045175MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2788045175.h"
#include "mscorlib_System_IO_TextReader1561828458.h"
#include "mscorlib_System_Convert2607082565MethodDeclarations.h"
#include "mscorlib_System_Byte3683104436.h"
#include "mscorlib_System_DateTime693205669.h"
#include "mscorlib_System_Decimal724701077.h"
#include "mscorlib_System_SByte454417549.h"
#include "mscorlib_System_Int164041245914.h"
#include "mscorlib_System_UInt16986882611.h"
#include "mscorlib_System_Single2076509932.h"
#include "mscorlib_System_IO_StringReader1480123486MethodDeclarations.h"
#include "mscorlib_System_IO_StringReader1480123486.h"
#include "System_System_Collections_Generic_Stack_1_gen3533309409MethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException628810857MethodDeclarations.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_Lexer954714164MethodDeclarations.h"
#include "System_System_Collections_Generic_Stack_1_gen3533309409.h"
#include "mscorlib_System_ArgumentNullException628810857.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_Lexer954714164.h"
#include "mscorlib_System_Globalization_CultureInfo3500843524MethodDeclarations.h"
#include "mscorlib_System_Globalization_CultureInfo3500843524.h"
#include "mscorlib_System_Globalization_NumberStyles3408984435.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_JsonToken2445581255MethodDeclarations.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_JsonType808352724MethodDeclarations.h"
#include "mscorlib_System_Globalization_NumberFormatInfo104580544MethodDeclarations.h"
#include "mscorlib_System_Globalization_NumberFormatInfo104580544.h"
#include "mscorlib_System_Text_StringBuilder1221177846MethodDeclarations.h"
#include "mscorlib_System_IO_StringWriter4139609088MethodDeclarations.h"
#include "mscorlib_System_Text_StringBuilder1221177846.h"
#include "mscorlib_System_IO_StringWriter4139609088.h"
#include "mscorlib_System_IO_TextWriter4027217640.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_WriterContext1209007092.h"
#include "System_System_Collections_Generic_Stack_1_gen2296735246MethodDeclarations.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_WriterContext1209007092MethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "System_System_Collections_Generic_Stack_1_gen2296735246.h"
#include "mscorlib_System_IO_TextWriter4027217640MethodDeclarations.h"
#include "AWSSDK_Core_Amazon_Util_AWSSDKUtils2036360342MethodDeclarations.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_Lexer_StateHan3489987002MethodDeclarations.h"
#include "mscorlib_System_Runtime_CompilerServices_RuntimeHel266230107MethodDeclarations.h"
#include "AWSSDK.Core_ArrayTypes.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_Lexer_StateHan3489987002.h"
#include "AWSSDK_Core_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "AWSSDK_Core_U3CPrivateImplementationDetailsU3E1486305137MethodDeclarations.h"
#include "AWSSDK_Core_U3CPrivateImplementationDetailsU3E___S2631791551.h"
#include "mscorlib_System_RuntimeFieldHandle2331729674.h"
#include "mscorlib_System_IO_TextReader1561828458MethodDeclarations.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_ObjectMetadata4058137740.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_ObjectMetadata4058137740MethodDeclarations.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398MethodDeclarations.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_PropertyMetada3287739986.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_PropertyMetada3287739986MethodDeclarations.h"

// T ThirdParty.iOS4Unity.Runtime::GetNSObject<System.Object>(System.IntPtr)
extern "C"  Il2CppObject * Runtime_GetNSObject_TisIl2CppObject_m1273815327_gshared (Il2CppObject * __this /* static, unused */, IntPtr_t ___handle0, const MethodInfo* method);
#define Runtime_GetNSObject_TisIl2CppObject_m1273815327(__this /* static, unused */, ___handle0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, IntPtr_t, const MethodInfo*))Runtime_GetNSObject_TisIl2CppObject_m1273815327_gshared)(__this /* static, unused */, ___handle0, method)
// T ThirdParty.iOS4Unity.Runtime::GetNSObject<ThirdParty.iOS4Unity.UIDevice>(System.IntPtr)
#define Runtime_GetNSObject_TisUIDevice_t860038178_m266059846(__this /* static, unused */, ___handle0, method) ((  UIDevice_t860038178 * (*) (Il2CppObject * /* static, unused */, IntPtr_t, const MethodInfo*))Runtime_GetNSObject_TisIl2CppObject_m1273815327_gshared)(__this /* static, unused */, ___handle0, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ThirdParty.iOS4Unity.SKPaymentTransaction::.cctor()
extern Il2CppClass* ObjC_t468753822_il2cpp_TypeInfo_var;
extern Il2CppClass* SKPaymentTransaction_t2918057744_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3855737166;
extern const uint32_t SKPaymentTransaction__cctor_m3267372721_MetadataUsageId;
extern "C"  void SKPaymentTransaction__cctor_m3267372721 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SKPaymentTransaction__cctor_m3267372721_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ObjC_t468753822_il2cpp_TypeInfo_var);
		IntPtr_t L_0 = ObjC_GetClass_m4189432679(NULL /*static, unused*/, _stringLiteral3855737166, /*hidden argument*/NULL);
		((SKPaymentTransaction_t2918057744_StaticFields*)SKPaymentTransaction_t2918057744_il2cpp_TypeInfo_var->static_fields)->set__classHandle_3(L_0);
		return;
	}
}
// System.Void ThirdParty.iOS4Unity.SKPaymentTransaction::.ctor(System.IntPtr)
extern Il2CppClass* NSObject_t1518098886_il2cpp_TypeInfo_var;
extern const uint32_t SKPaymentTransaction__ctor_m1324688674_MetadataUsageId;
extern "C"  void SKPaymentTransaction__ctor_m1324688674 (SKPaymentTransaction_t2918057744 * __this, IntPtr_t ___handle0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SKPaymentTransaction__ctor_m1324688674_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0 = ___handle0;
		IL2CPP_RUNTIME_CLASS_INIT(NSObject_t1518098886_il2cpp_TypeInfo_var);
		NSObject__ctor_m139361934(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ThirdParty.iOS4Unity.SKProduct::.cctor()
extern Il2CppClass* ObjC_t468753822_il2cpp_TypeInfo_var;
extern Il2CppClass* SKProduct_t3309436227_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2555156815;
extern const uint32_t SKProduct__cctor_m15642440_MetadataUsageId;
extern "C"  void SKProduct__cctor_m15642440 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SKProduct__cctor_m15642440_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ObjC_t468753822_il2cpp_TypeInfo_var);
		IntPtr_t L_0 = ObjC_GetClass_m4189432679(NULL /*static, unused*/, _stringLiteral2555156815, /*hidden argument*/NULL);
		((SKProduct_t3309436227_StaticFields*)SKProduct_t3309436227_il2cpp_TypeInfo_var->static_fields)->set__classHandle_3(L_0);
		return;
	}
}
// System.Void ThirdParty.iOS4Unity.SKProduct::.ctor(System.IntPtr)
extern Il2CppClass* NSObject_t1518098886_il2cpp_TypeInfo_var;
extern const uint32_t SKProduct__ctor_m2835725085_MetadataUsageId;
extern "C"  void SKProduct__ctor_m2835725085 (SKProduct_t3309436227 * __this, IntPtr_t ___handle0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SKProduct__ctor_m2835725085_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0 = ___handle0;
		IL2CPP_RUNTIME_CLASS_INIT(NSObject_t1518098886_il2cpp_TypeInfo_var);
		NSObject__ctor_m139361934(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ThirdParty.iOS4Unity.SKProductsRequest::.cctor()
extern Il2CppClass* ObjC_t468753822_il2cpp_TypeInfo_var;
extern Il2CppClass* SKProductsRequest_t3969930329_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4104322641;
extern const uint32_t SKProductsRequest__cctor_m43781874_MetadataUsageId;
extern "C"  void SKProductsRequest__cctor_m43781874 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SKProductsRequest__cctor_m43781874_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ObjC_t468753822_il2cpp_TypeInfo_var);
		IntPtr_t L_0 = ObjC_GetClass_m4189432679(NULL /*static, unused*/, _stringLiteral4104322641, /*hidden argument*/NULL);
		((SKProductsRequest_t3969930329_StaticFields*)SKProductsRequest_t3969930329_il2cpp_TypeInfo_var->static_fields)->set__classHandle_3(L_0);
		return;
	}
}
// System.Void ThirdParty.iOS4Unity.SKProductsRequest::.ctor(System.IntPtr)
extern Il2CppClass* NSObject_t1518098886_il2cpp_TypeInfo_var;
extern const uint32_t SKProductsRequest__ctor_m252108103_MetadataUsageId;
extern "C"  void SKProductsRequest__ctor_m252108103 (SKProductsRequest_t3969930329 * __this, IntPtr_t ___handle0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SKProductsRequest__ctor_m252108103_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0 = ___handle0;
		IL2CPP_RUNTIME_CLASS_INIT(NSObject_t1518098886_il2cpp_TypeInfo_var);
		NSObject__ctor_m139361934(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ThirdParty.iOS4Unity.SKProductsResponse::.cctor()
extern Il2CppClass* ObjC_t468753822_il2cpp_TypeInfo_var;
extern Il2CppClass* SKProductsResponse_t3540529521_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3609125635;
extern const uint32_t SKProductsResponse__cctor_m2132612730_MetadataUsageId;
extern "C"  void SKProductsResponse__cctor_m2132612730 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SKProductsResponse__cctor_m2132612730_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ObjC_t468753822_il2cpp_TypeInfo_var);
		IntPtr_t L_0 = ObjC_GetClass_m4189432679(NULL /*static, unused*/, _stringLiteral3609125635, /*hidden argument*/NULL);
		((SKProductsResponse_t3540529521_StaticFields*)SKProductsResponse_t3540529521_il2cpp_TypeInfo_var->static_fields)->set__classHandle_3(L_0);
		return;
	}
}
// System.Void ThirdParty.iOS4Unity.SKProductsResponse::.ctor(System.IntPtr)
extern Il2CppClass* NSObject_t1518098886_il2cpp_TypeInfo_var;
extern const uint32_t SKProductsResponse__ctor_m1522197705_MetadataUsageId;
extern "C"  void SKProductsResponse__ctor_m1522197705 (SKProductsResponse_t3540529521 * __this, IntPtr_t ___handle0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SKProductsResponse__ctor_m1522197705_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0 = ___handle0;
		IL2CPP_RUNTIME_CLASS_INIT(NSObject_t1518098886_il2cpp_TypeInfo_var);
		NSObject__ctor_m139361934(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ThirdParty.iOS4Unity.UIActionSheet::.cctor()
extern Il2CppClass* ObjC_t468753822_il2cpp_TypeInfo_var;
extern Il2CppClass* UIActionSheet_t2483183669_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral684790113;
extern const uint32_t UIActionSheet__cctor_m3366577070_MetadataUsageId;
extern "C"  void UIActionSheet__cctor_m3366577070 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIActionSheet__cctor_m3366577070_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ObjC_t468753822_il2cpp_TypeInfo_var);
		IntPtr_t L_0 = ObjC_GetClass_m4189432679(NULL /*static, unused*/, _stringLiteral684790113, /*hidden argument*/NULL);
		((UIActionSheet_t2483183669_StaticFields*)UIActionSheet_t2483183669_il2cpp_TypeInfo_var->static_fields)->set__classHandle_4(L_0);
		return;
	}
}
// System.Void ThirdParty.iOS4Unity.UIActionSheet::.ctor(System.IntPtr)
extern Il2CppClass* UIView_t1452205135_il2cpp_TypeInfo_var;
extern const uint32_t UIActionSheet__ctor_m2070526539_MetadataUsageId;
extern "C"  void UIActionSheet__ctor_m2070526539 (UIActionSheet_t2483183669 * __this, IntPtr_t ___handle0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIActionSheet__ctor_m2070526539_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0 = ___handle0;
		IL2CPP_RUNTIME_CLASS_INIT(UIView_t1452205135_il2cpp_TypeInfo_var);
		UIView__ctor_m2614097503(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ThirdParty.iOS4Unity.UIActivityViewController::.cctor()
extern Il2CppClass* ObjC_t468753822_il2cpp_TypeInfo_var;
extern Il2CppClass* UIActivityViewController_t2230362714_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3473104300;
extern const uint32_t UIActivityViewController__cctor_m2344876883_MetadataUsageId;
extern "C"  void UIActivityViewController__cctor_m2344876883 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIActivityViewController__cctor_m2344876883_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ObjC_t468753822_il2cpp_TypeInfo_var);
		IntPtr_t L_0 = ObjC_GetClass_m4189432679(NULL /*static, unused*/, _stringLiteral3473104300, /*hidden argument*/NULL);
		((UIActivityViewController_t2230362714_StaticFields*)UIActivityViewController_t2230362714_il2cpp_TypeInfo_var->static_fields)->set__classHandle_4(L_0);
		return;
	}
}
// System.Void ThirdParty.iOS4Unity.UIActivityViewController::.ctor(System.IntPtr)
extern Il2CppClass* UIViewController_t1891120779_il2cpp_TypeInfo_var;
extern const uint32_t UIActivityViewController__ctor_m1950878868_MetadataUsageId;
extern "C"  void UIActivityViewController__ctor_m1950878868 (UIActivityViewController_t2230362714 * __this, IntPtr_t ___handle0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIActivityViewController__ctor_m1950878868_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0 = ___handle0;
		IL2CPP_RUNTIME_CLASS_INIT(UIViewController_t1891120779_il2cpp_TypeInfo_var);
		UIViewController__ctor_m1825066843(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ThirdParty.iOS4Unity.UIAlertView::.cctor()
extern Il2CppClass* ObjC_t468753822_il2cpp_TypeInfo_var;
extern Il2CppClass* UIAlertView_t2175232939_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1576300195;
extern const uint32_t UIAlertView__cctor_m1022062200_MetadataUsageId;
extern "C"  void UIAlertView__cctor_m1022062200 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIAlertView__cctor_m1022062200_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ObjC_t468753822_il2cpp_TypeInfo_var);
		IntPtr_t L_0 = ObjC_GetClass_m4189432679(NULL /*static, unused*/, _stringLiteral1576300195, /*hidden argument*/NULL);
		((UIAlertView_t2175232939_StaticFields*)UIAlertView_t2175232939_il2cpp_TypeInfo_var->static_fields)->set__classHandle_3(L_0);
		return;
	}
}
// System.Void ThirdParty.iOS4Unity.UIAlertView::.ctor(System.IntPtr)
extern Il2CppClass* NSObject_t1518098886_il2cpp_TypeInfo_var;
extern const uint32_t UIAlertView__ctor_m882662477_MetadataUsageId;
extern "C"  void UIAlertView__ctor_m882662477 (UIAlertView_t2175232939 * __this, IntPtr_t ___handle0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIAlertView__ctor_m882662477_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0 = ___handle0;
		IL2CPP_RUNTIME_CLASS_INIT(NSObject_t1518098886_il2cpp_TypeInfo_var);
		NSObject__ctor_m139361934(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ThirdParty.iOS4Unity.UIApplication::.cctor()
extern Il2CppClass* ObjC_t468753822_il2cpp_TypeInfo_var;
extern Il2CppClass* UIApplication_t1857964820_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral951912982;
extern const uint32_t UIApplication__cctor_m3134345297_MetadataUsageId;
extern "C"  void UIApplication__cctor_m3134345297 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIApplication__cctor_m3134345297_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ObjC_t468753822_il2cpp_TypeInfo_var);
		IntPtr_t L_0 = ObjC_GetClass_m4189432679(NULL /*static, unused*/, _stringLiteral951912982, /*hidden argument*/NULL);
		((UIApplication_t1857964820_StaticFields*)UIApplication_t1857964820_il2cpp_TypeInfo_var->static_fields)->set__classHandle_3(L_0);
		return;
	}
}
// System.Void ThirdParty.iOS4Unity.UIApplication::.ctor(System.IntPtr)
extern Il2CppClass* NSObject_t1518098886_il2cpp_TypeInfo_var;
extern const uint32_t UIApplication__ctor_m1603570404_MetadataUsageId;
extern "C"  void UIApplication__ctor_m1603570404 (UIApplication_t1857964820 * __this, IntPtr_t ___handle0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIApplication__ctor_m1603570404_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0 = ___handle0;
		IL2CPP_RUNTIME_CLASS_INIT(NSObject_t1518098886_il2cpp_TypeInfo_var);
		NSObject__ctor_m139361934(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ThirdParty.iOS4Unity.UIDevice::.cctor()
extern Il2CppClass* UIDevice_t860038178_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjC_t468753822_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2176033914;
extern const uint32_t UIDevice__cctor_m1139141347_MetadataUsageId;
extern "C"  void UIDevice__cctor_m1139141347 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIDevice__cctor_m1139141347_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((UIDevice_t860038178_StaticFields*)UIDevice_t860038178_il2cpp_TypeInfo_var->static_fields)->set__majorVersion_4((-1));
		((UIDevice_t860038178_StaticFields*)UIDevice_t860038178_il2cpp_TypeInfo_var->static_fields)->set__minorVersion_5((-1));
		IL2CPP_RUNTIME_CLASS_INIT(ObjC_t468753822_il2cpp_TypeInfo_var);
		IntPtr_t L_0 = ObjC_GetClass_m4189432679(NULL /*static, unused*/, _stringLiteral2176033914, /*hidden argument*/NULL);
		((UIDevice_t860038178_StaticFields*)UIDevice_t860038178_il2cpp_TypeInfo_var->static_fields)->set__classHandle_3(L_0);
		return;
	}
}
// System.Void ThirdParty.iOS4Unity.UIDevice::.ctor(System.IntPtr)
extern Il2CppClass* NSObject_t1518098886_il2cpp_TypeInfo_var;
extern const uint32_t UIDevice__ctor_m217874266_MetadataUsageId;
extern "C"  void UIDevice__ctor_m217874266 (UIDevice_t860038178 * __this, IntPtr_t ___handle0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIDevice__ctor_m217874266_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0 = ___handle0;
		IL2CPP_RUNTIME_CLASS_INIT(NSObject_t1518098886_il2cpp_TypeInfo_var);
		NSObject__ctor_m139361934(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// ThirdParty.iOS4Unity.UIDevice ThirdParty.iOS4Unity.UIDevice::get_CurrentDevice()
extern Il2CppClass* UIDevice_t860038178_il2cpp_TypeInfo_var;
extern Il2CppClass* Selector_t1939762325_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjC_t468753822_il2cpp_TypeInfo_var;
extern Il2CppClass* Runtime_t1145037850_il2cpp_TypeInfo_var;
extern const MethodInfo* Runtime_GetNSObject_TisUIDevice_t860038178_m266059846_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2205711197;
extern const uint32_t UIDevice_get_CurrentDevice_m2709480474_MetadataUsageId;
extern "C"  UIDevice_t860038178 * UIDevice_get_CurrentDevice_m2709480474 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIDevice_get_CurrentDevice_m2709480474_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UIDevice_t860038178_il2cpp_TypeInfo_var);
		IntPtr_t L_0 = ((UIDevice_t860038178_StaticFields*)UIDevice_t860038178_il2cpp_TypeInfo_var->static_fields)->get__classHandle_3();
		IL2CPP_RUNTIME_CLASS_INIT(Selector_t1939762325_il2cpp_TypeInfo_var);
		IntPtr_t L_1 = Selector_GetHandle_m3802952106(NULL /*static, unused*/, _stringLiteral2205711197, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(ObjC_t468753822_il2cpp_TypeInfo_var);
		IntPtr_t L_2 = ObjC_MessageSendIntPtr_m4279986307(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Runtime_t1145037850_il2cpp_TypeInfo_var);
		UIDevice_t860038178 * L_3 = Runtime_GetNSObject_TisUIDevice_t860038178_m266059846(NULL /*static, unused*/, L_2, /*hidden argument*/Runtime_GetNSObject_TisUIDevice_t860038178_m266059846_MethodInfo_var);
		return L_3;
	}
}
// System.String ThirdParty.iOS4Unity.UIDevice::get_Model()
extern Il2CppClass* Selector_t1939762325_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjC_t468753822_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1601798155;
extern const uint32_t UIDevice_get_Model_m454493711_MetadataUsageId;
extern "C"  String_t* UIDevice_get_Model_m454493711 (UIDevice_t860038178 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIDevice_get_Model_m454493711_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0 = ((NSObject_t1518098886 *)__this)->get_Handle_1();
		IL2CPP_RUNTIME_CLASS_INIT(Selector_t1939762325_il2cpp_TypeInfo_var);
		IntPtr_t L_1 = Selector_GetHandle_m3802952106(NULL /*static, unused*/, _stringLiteral1601798155, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(ObjC_t468753822_il2cpp_TypeInfo_var);
		IntPtr_t L_2 = ObjC_MessageSendIntPtr_m4279986307(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		String_t* L_3 = ObjC_FromNSString_m3897991665(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.String ThirdParty.iOS4Unity.UIDevice::get_SystemName()
extern Il2CppClass* Selector_t1939762325_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjC_t468753822_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3538213704;
extern const uint32_t UIDevice_get_SystemName_m1474594034_MetadataUsageId;
extern "C"  String_t* UIDevice_get_SystemName_m1474594034 (UIDevice_t860038178 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIDevice_get_SystemName_m1474594034_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0 = ((NSObject_t1518098886 *)__this)->get_Handle_1();
		IL2CPP_RUNTIME_CLASS_INIT(Selector_t1939762325_il2cpp_TypeInfo_var);
		IntPtr_t L_1 = Selector_GetHandle_m3802952106(NULL /*static, unused*/, _stringLiteral3538213704, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(ObjC_t468753822_il2cpp_TypeInfo_var);
		IntPtr_t L_2 = ObjC_MessageSendIntPtr_m4279986307(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		String_t* L_3 = ObjC_FromNSString_m3897991665(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.String ThirdParty.iOS4Unity.UIDevice::get_SystemVersion()
extern Il2CppClass* Selector_t1939762325_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjC_t468753822_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral760441743;
extern const uint32_t UIDevice_get_SystemVersion_m1140415835_MetadataUsageId;
extern "C"  String_t* UIDevice_get_SystemVersion_m1140415835 (UIDevice_t860038178 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIDevice_get_SystemVersion_m1140415835_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0 = ((NSObject_t1518098886 *)__this)->get_Handle_1();
		IL2CPP_RUNTIME_CLASS_INIT(Selector_t1939762325_il2cpp_TypeInfo_var);
		IntPtr_t L_1 = Selector_GetHandle_m3802952106(NULL /*static, unused*/, _stringLiteral760441743, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(ObjC_t468753822_il2cpp_TypeInfo_var);
		IntPtr_t L_2 = ObjC_MessageSendIntPtr_m4279986307(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		String_t* L_3 = ObjC_FromNSString_m3897991665(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void ThirdParty.iOS4Unity.UIImage::.cctor()
extern Il2CppClass* ObjC_t468753822_il2cpp_TypeInfo_var;
extern Il2CppClass* UIImage_t3580593017_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3605175931;
extern const uint32_t UIImage__cctor_m1570545646_MetadataUsageId;
extern "C"  void UIImage__cctor_m1570545646 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIImage__cctor_m1570545646_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ObjC_t468753822_il2cpp_TypeInfo_var);
		IntPtr_t L_0 = ObjC_GetClass_m4189432679(NULL /*static, unused*/, _stringLiteral3605175931, /*hidden argument*/NULL);
		((UIImage_t3580593017_StaticFields*)UIImage_t3580593017_il2cpp_TypeInfo_var->static_fields)->set__classHandle_3(L_0);
		return;
	}
}
// System.Void ThirdParty.iOS4Unity.UIImage::.ctor(System.IntPtr)
extern Il2CppClass* NSObject_t1518098886_il2cpp_TypeInfo_var;
extern const uint32_t UIImage__ctor_m1068131209_MetadataUsageId;
extern "C"  void UIImage__ctor_m1068131209 (UIImage_t3580593017 * __this, IntPtr_t ___handle0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIImage__ctor_m1068131209_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0 = ___handle0;
		IL2CPP_RUNTIME_CLASS_INIT(NSObject_t1518098886_il2cpp_TypeInfo_var);
		NSObject__ctor_m139361934(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ThirdParty.iOS4Unity.UILocalNotification::.cctor()
extern Il2CppClass* ObjC_t468753822_il2cpp_TypeInfo_var;
extern Il2CppClass* UILocalNotification_t869364318_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4204481528;
extern const uint32_t UILocalNotification__cctor_m3710969671_MetadataUsageId;
extern "C"  void UILocalNotification__cctor_m3710969671 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UILocalNotification__cctor_m3710969671_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ObjC_t468753822_il2cpp_TypeInfo_var);
		IntPtr_t L_0 = ObjC_GetClass_m4189432679(NULL /*static, unused*/, _stringLiteral4204481528, /*hidden argument*/NULL);
		((UILocalNotification_t869364318_StaticFields*)UILocalNotification_t869364318_il2cpp_TypeInfo_var->static_fields)->set__classHandle_3(L_0);
		return;
	}
}
// System.Void ThirdParty.iOS4Unity.UILocalNotification::.ctor(System.IntPtr)
extern Il2CppClass* NSObject_t1518098886_il2cpp_TypeInfo_var;
extern const uint32_t UILocalNotification__ctor_m2010710626_MetadataUsageId;
extern "C"  void UILocalNotification__ctor_m2010710626 (UILocalNotification_t869364318 * __this, IntPtr_t ___handle0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UILocalNotification__ctor_m2010710626_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0 = ___handle0;
		IL2CPP_RUNTIME_CLASS_INIT(NSObject_t1518098886_il2cpp_TypeInfo_var);
		NSObject__ctor_m139361934(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ThirdParty.iOS4Unity.UIScreen::.cctor()
extern Il2CppClass* ObjC_t468753822_il2cpp_TypeInfo_var;
extern Il2CppClass* UIScreen_t4048045402_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3985054888;
extern const uint32_t UIScreen__cctor_m149469123_MetadataUsageId;
extern "C"  void UIScreen__cctor_m149469123 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIScreen__cctor_m149469123_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ObjC_t468753822_il2cpp_TypeInfo_var);
		IntPtr_t L_0 = ObjC_GetClass_m4189432679(NULL /*static, unused*/, _stringLiteral3985054888, /*hidden argument*/NULL);
		((UIScreen_t4048045402_StaticFields*)UIScreen_t4048045402_il2cpp_TypeInfo_var->static_fields)->set__classHandle_3(L_0);
		return;
	}
}
// System.Void ThirdParty.iOS4Unity.UIScreen::.ctor(System.IntPtr)
extern Il2CppClass* NSObject_t1518098886_il2cpp_TypeInfo_var;
extern const uint32_t UIScreen__ctor_m671030876_MetadataUsageId;
extern "C"  void UIScreen__ctor_m671030876 (UIScreen_t4048045402 * __this, IntPtr_t ___handle0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIScreen__ctor_m671030876_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0 = ___handle0;
		IL2CPP_RUNTIME_CLASS_INIT(NSObject_t1518098886_il2cpp_TypeInfo_var);
		NSObject__ctor_m139361934(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ThirdParty.iOS4Unity.UIScreenMode::.cctor()
extern Il2CppClass* ObjC_t468753822_il2cpp_TypeInfo_var;
extern Il2CppClass* UIScreenMode_t609995313_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1996535427;
extern const uint32_t UIScreenMode__cctor_m1019807450_MetadataUsageId;
extern "C"  void UIScreenMode__cctor_m1019807450 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIScreenMode__cctor_m1019807450_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ObjC_t468753822_il2cpp_TypeInfo_var);
		IntPtr_t L_0 = ObjC_GetClass_m4189432679(NULL /*static, unused*/, _stringLiteral1996535427, /*hidden argument*/NULL);
		((UIScreenMode_t609995313_StaticFields*)UIScreenMode_t609995313_il2cpp_TypeInfo_var->static_fields)->set__classHandle_3(L_0);
		return;
	}
}
// System.Void ThirdParty.iOS4Unity.UIScreenMode::.ctor(System.IntPtr)
extern Il2CppClass* NSObject_t1518098886_il2cpp_TypeInfo_var;
extern const uint32_t UIScreenMode__ctor_m2400519705_MetadataUsageId;
extern "C"  void UIScreenMode__ctor_m2400519705 (UIScreenMode_t609995313 * __this, IntPtr_t ___handle0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIScreenMode__ctor_m2400519705_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0 = ___handle0;
		IL2CPP_RUNTIME_CLASS_INIT(NSObject_t1518098886_il2cpp_TypeInfo_var);
		NSObject__ctor_m139361934(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ThirdParty.iOS4Unity.UIUserNotificationSettings::.cctor()
extern Il2CppClass* ObjC_t468753822_il2cpp_TypeInfo_var;
extern Il2CppClass* UIUserNotificationSettings_t4227938467_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2817670997;
extern const uint32_t UIUserNotificationSettings__cctor_m2982131012_MetadataUsageId;
extern "C"  void UIUserNotificationSettings__cctor_m2982131012 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIUserNotificationSettings__cctor_m2982131012_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ObjC_t468753822_il2cpp_TypeInfo_var);
		IntPtr_t L_0 = ObjC_GetClass_m4189432679(NULL /*static, unused*/, _stringLiteral2817670997, /*hidden argument*/NULL);
		((UIUserNotificationSettings_t4227938467_StaticFields*)UIUserNotificationSettings_t4227938467_il2cpp_TypeInfo_var->static_fields)->set__classHandle_3(L_0);
		return;
	}
}
// System.Void ThirdParty.iOS4Unity.UIUserNotificationSettings::.ctor(System.IntPtr)
extern Il2CppClass* NSObject_t1518098886_il2cpp_TypeInfo_var;
extern const uint32_t UIUserNotificationSettings__ctor_m3822994115_MetadataUsageId;
extern "C"  void UIUserNotificationSettings__ctor_m3822994115 (UIUserNotificationSettings_t4227938467 * __this, IntPtr_t ___handle0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIUserNotificationSettings__ctor_m3822994115_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0 = ___handle0;
		IL2CPP_RUNTIME_CLASS_INIT(NSObject_t1518098886_il2cpp_TypeInfo_var);
		NSObject__ctor_m139361934(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ThirdParty.iOS4Unity.UIView::.cctor()
extern Il2CppClass* ObjC_t468753822_il2cpp_TypeInfo_var;
extern Il2CppClass* UIView_t1452205135_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1917943773;
extern const uint32_t UIView__cctor_m401732648_MetadataUsageId;
extern "C"  void UIView__cctor_m401732648 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIView__cctor_m401732648_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ObjC_t468753822_il2cpp_TypeInfo_var);
		IntPtr_t L_0 = ObjC_GetClass_m4189432679(NULL /*static, unused*/, _stringLiteral1917943773, /*hidden argument*/NULL);
		((UIView_t1452205135_StaticFields*)UIView_t1452205135_il2cpp_TypeInfo_var->static_fields)->set__classHandle_3(L_0);
		return;
	}
}
// System.Void ThirdParty.iOS4Unity.UIView::.ctor(System.IntPtr)
extern Il2CppClass* NSObject_t1518098886_il2cpp_TypeInfo_var;
extern const uint32_t UIView__ctor_m2614097503_MetadataUsageId;
extern "C"  void UIView__ctor_m2614097503 (UIView_t1452205135 * __this, IntPtr_t ___handle0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIView__ctor_m2614097503_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0 = ___handle0;
		IL2CPP_RUNTIME_CLASS_INIT(NSObject_t1518098886_il2cpp_TypeInfo_var);
		NSObject__ctor_m139361934(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ThirdParty.iOS4Unity.UIViewController::.cctor()
extern Il2CppClass* ObjC_t468753822_il2cpp_TypeInfo_var;
extern Il2CppClass* UIViewController_t1891120779_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral486775117;
extern const uint32_t UIViewController__cctor_m252074844_MetadataUsageId;
extern "C"  void UIViewController__cctor_m252074844 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIViewController__cctor_m252074844_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ObjC_t468753822_il2cpp_TypeInfo_var);
		IntPtr_t L_0 = ObjC_GetClass_m4189432679(NULL /*static, unused*/, _stringLiteral486775117, /*hidden argument*/NULL);
		((UIViewController_t1891120779_StaticFields*)UIViewController_t1891120779_il2cpp_TypeInfo_var->static_fields)->set__classHandle_3(L_0);
		return;
	}
}
// System.Void ThirdParty.iOS4Unity.UIViewController::.ctor(System.IntPtr)
extern Il2CppClass* NSObject_t1518098886_il2cpp_TypeInfo_var;
extern const uint32_t UIViewController__ctor_m1825066843_MetadataUsageId;
extern "C"  void UIViewController__ctor_m1825066843 (UIViewController_t1891120779 * __this, IntPtr_t ___handle0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIViewController__ctor_m1825066843_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0 = ___handle0;
		IL2CPP_RUNTIME_CLASS_INIT(NSObject_t1518098886_il2cpp_TypeInfo_var);
		NSObject__ctor_m139361934(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ThirdParty.iOS4Unity.UIWindow::.cctor()
extern Il2CppClass* ObjC_t468753822_il2cpp_TypeInfo_var;
extern Il2CppClass* UIWindow_t800018578_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3565636244;
extern const uint32_t UIWindow__cctor_m3975553979_MetadataUsageId;
extern "C"  void UIWindow__cctor_m3975553979 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWindow__cctor_m3975553979_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ObjC_t468753822_il2cpp_TypeInfo_var);
		IntPtr_t L_0 = ObjC_GetClass_m4189432679(NULL /*static, unused*/, _stringLiteral3565636244, /*hidden argument*/NULL);
		((UIWindow_t800018578_StaticFields*)UIWindow_t800018578_il2cpp_TypeInfo_var->static_fields)->set__classHandle_4(L_0);
		return;
	}
}
// System.Void ThirdParty.iOS4Unity.UIWindow::.ctor(System.IntPtr)
extern Il2CppClass* UIView_t1452205135_il2cpp_TypeInfo_var;
extern const uint32_t UIWindow__ctor_m3442098196_MetadataUsageId;
extern "C"  void UIWindow__ctor_m3442098196 (UIWindow_t800018578 * __this, IntPtr_t ___handle0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWindow__ctor_m3442098196_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0 = ___handle0;
		IL2CPP_RUNTIME_CLASS_INIT(UIView_t1452205135_il2cpp_TypeInfo_var);
		UIView__ctor_m2614097503(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// Conversion methods for marshalling of: ThirdParty.Json.LitJson.ArrayMetadata
extern "C" void ArrayMetadata_t1135078014_marshal_pinvoke(const ArrayMetadata_t1135078014& unmarshaled, ArrayMetadata_t1135078014_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___element_type_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'element_type' of type 'ArrayMetadata': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___element_type_0Exception);
}
extern "C" void ArrayMetadata_t1135078014_marshal_pinvoke_back(const ArrayMetadata_t1135078014_marshaled_pinvoke& marshaled, ArrayMetadata_t1135078014& unmarshaled)
{
	Il2CppCodeGenException* ___element_type_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'element_type' of type 'ArrayMetadata': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___element_type_0Exception);
}
// Conversion method for clean up from marshalling of: ThirdParty.Json.LitJson.ArrayMetadata
extern "C" void ArrayMetadata_t1135078014_marshal_pinvoke_cleanup(ArrayMetadata_t1135078014_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: ThirdParty.Json.LitJson.ArrayMetadata
extern "C" void ArrayMetadata_t1135078014_marshal_com(const ArrayMetadata_t1135078014& unmarshaled, ArrayMetadata_t1135078014_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___element_type_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'element_type' of type 'ArrayMetadata': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___element_type_0Exception);
}
extern "C" void ArrayMetadata_t1135078014_marshal_com_back(const ArrayMetadata_t1135078014_marshaled_com& marshaled, ArrayMetadata_t1135078014& unmarshaled)
{
	Il2CppCodeGenException* ___element_type_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'element_type' of type 'ArrayMetadata': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___element_type_0Exception);
}
// Conversion method for clean up from marshalling of: ThirdParty.Json.LitJson.ArrayMetadata
extern "C" void ArrayMetadata_t1135078014_marshal_com_cleanup(ArrayMetadata_t1135078014_marshaled_com& marshaled)
{
}
// System.Void ThirdParty.Json.LitJson.ExporterFunc::.ctor(System.Object,System.IntPtr)
extern "C"  void ExporterFunc__ctor_m1402811244 (ExporterFunc_t173265409 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void ThirdParty.Json.LitJson.ExporterFunc::Invoke(System.Object,ThirdParty.Json.LitJson.JsonWriter)
extern "C"  void ExporterFunc_Invoke_m1119516307 (ExporterFunc_t173265409 * __this, Il2CppObject * ___obj0, JsonWriter_t3014444111 * ___writer1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ExporterFunc_Invoke_m1119516307((ExporterFunc_t173265409 *)__this->get_prev_9(),___obj0, ___writer1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___obj0, JsonWriter_t3014444111 * ___writer1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0, ___writer1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___obj0, JsonWriter_t3014444111 * ___writer1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0, ___writer1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, JsonWriter_t3014444111 * ___writer1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___obj0, ___writer1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult ThirdParty.Json.LitJson.ExporterFunc::BeginInvoke(System.Object,ThirdParty.Json.LitJson.JsonWriter,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ExporterFunc_BeginInvoke_m149663916 (ExporterFunc_t173265409 * __this, Il2CppObject * ___obj0, JsonWriter_t3014444111 * ___writer1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___obj0;
	__d_args[1] = ___writer1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void ThirdParty.Json.LitJson.ExporterFunc::EndInvoke(System.IAsyncResult)
extern "C"  void ExporterFunc_EndInvoke_m2740655690 (ExporterFunc_t173265409 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void ThirdParty.Json.LitJson.FsmContext::.ctor()
extern "C"  void FsmContext__ctor_m1039323702 (FsmContext_t4275593467 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ThirdParty.Json.LitJson.ImporterFunc::.ctor(System.Object,System.IntPtr)
extern "C"  void ImporterFunc__ctor_m637091809 (ImporterFunc_t850687278 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Object ThirdParty.Json.LitJson.ImporterFunc::Invoke(System.Object)
extern "C"  Il2CppObject * ImporterFunc_Invoke_m856546654 (ImporterFunc_t850687278 * __this, Il2CppObject * ___input0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ImporterFunc_Invoke_m856546654((ImporterFunc_t850687278 *)__this->get_prev_9(),___input0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___input0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___input0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___input0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___input0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___input0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult ThirdParty.Json.LitJson.ImporterFunc::BeginInvoke(System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ImporterFunc_BeginInvoke_m2544510086 (ImporterFunc_t850687278 * __this, Il2CppObject * ___input0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___input0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Object ThirdParty.Json.LitJson.ImporterFunc::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * ImporterFunc_EndInvoke_m2331328154 (ImporterFunc_t850687278 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Int32 ThirdParty.Json.LitJson.JsonData::get_Count()
extern Il2CppClass* ICollection_t91669223_il2cpp_TypeInfo_var;
extern const uint32_t JsonData_get_Count_m1144605257_MetadataUsageId;
extern "C"  int32_t JsonData_get_Count_m1144605257 (JsonData_t4263252052 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonData_get_Count_m1144605257_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = JsonData_EnsureCollection_m2402886013(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.ICollection::get_Count() */, ICollection_t91669223_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Int32 ThirdParty.Json.LitJson.JsonData::System.Collections.ICollection.get_Count()
extern "C"  int32_t JsonData_System_Collections_ICollection_get_Count_m3136486028 (JsonData_t4263252052 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = JsonData_get_Count_m1144605257(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean ThirdParty.Json.LitJson.JsonData::System.Collections.ICollection.get_IsSynchronized()
extern Il2CppClass* ICollection_t91669223_il2cpp_TypeInfo_var;
extern const uint32_t JsonData_System_Collections_ICollection_get_IsSynchronized_m3228980993_MetadataUsageId;
extern "C"  bool JsonData_System_Collections_ICollection_get_IsSynchronized_m3228980993 (JsonData_t4263252052 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonData_System_Collections_ICollection_get_IsSynchronized_m3228980993_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = JsonData_EnsureCollection_m2402886013(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.ICollection::get_IsSynchronized() */, ICollection_t91669223_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Object ThirdParty.Json.LitJson.JsonData::System.Collections.ICollection.get_SyncRoot()
extern Il2CppClass* ICollection_t91669223_il2cpp_TypeInfo_var;
extern const uint32_t JsonData_System_Collections_ICollection_get_SyncRoot_m1214308533_MetadataUsageId;
extern "C"  Il2CppObject * JsonData_System_Collections_ICollection_get_SyncRoot_m1214308533 (JsonData_t4263252052 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonData_System_Collections_ICollection_get_SyncRoot_m1214308533_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = JsonData_EnsureCollection_m2402886013(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t91669223_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Collections.ICollection ThirdParty.Json.LitJson.JsonData::System.Collections.IDictionary.get_Keys()
extern Il2CppClass* List_1_t1398341365_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_1_t4227503581_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_1_t1410900363_il2cpp_TypeInfo_var;
extern Il2CppClass* ICollection_1_t2981295538_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern Il2CppClass* ICollection_t91669223_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3854603248_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m1021780796_MethodInfo_var;
extern const uint32_t JsonData_System_Collections_IDictionary_get_Keys_m2998477023_MetadataUsageId;
extern "C"  Il2CppObject * JsonData_System_Collections_IDictionary_get_Keys_m2998477023 (JsonData_t4263252052 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonData_System_Collections_IDictionary_get_Keys_m2998477023_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject* V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	KeyValuePair_2_t3935376536  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		JsonData_EnsureDictionary_m1008139353(__this, /*hidden argument*/NULL);
		List_1_t1398341365 * L_0 = (List_1_t1398341365 *)il2cpp_codegen_object_new(List_1_t1398341365_il2cpp_TypeInfo_var);
		List_1__ctor_m3854603248(L_0, /*hidden argument*/List_1__ctor_m3854603248_MethodInfo_var);
		V_0 = (Il2CppObject*)L_0;
		Il2CppObject* L_1 = __this->get_object_list_8();
		NullCheck(L_1);
		Il2CppObject* L_2 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.String,ThirdParty.Json.LitJson.JsonData>>::GetEnumerator() */, IEnumerable_1_t4227503581_il2cpp_TypeInfo_var, L_1);
		V_1 = L_2;
	}

IL_0019:
	try
	{ // begin try (depth: 1)
		{
			goto IL_002f;
		}

IL_001b:
		{
			Il2CppObject* L_3 = V_1;
			NullCheck(L_3);
			KeyValuePair_2_t3935376536  L_4 = InterfaceFuncInvoker0< KeyValuePair_2_t3935376536  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,ThirdParty.Json.LitJson.JsonData>>::get_Current() */, IEnumerator_1_t1410900363_il2cpp_TypeInfo_var, L_3);
			V_2 = L_4;
			Il2CppObject* L_5 = V_0;
			String_t* L_6 = KeyValuePair_2_get_Key_m1021780796((&V_2), /*hidden argument*/KeyValuePair_2_get_Key_m1021780796_MethodInfo_var);
			NullCheck(L_5);
			InterfaceActionInvoker1< String_t* >::Invoke(2 /* System.Void System.Collections.Generic.ICollection`1<System.String>::Add(!0) */, ICollection_1_t2981295538_il2cpp_TypeInfo_var, L_5, L_6);
		}

IL_002f:
		{
			Il2CppObject* L_7 = V_1;
			NullCheck(L_7);
			bool L_8 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_7);
			if (L_8)
			{
				goto IL_001b;
			}
		}

IL_0037:
		{
			IL2CPP_LEAVE(0x43, FINALLY_0039);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0039;
	}

FINALLY_0039:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_9 = V_1;
			if (!L_9)
			{
				goto IL_0042;
			}
		}

IL_003c:
		{
			Il2CppObject* L_10 = V_1;
			NullCheck(L_10);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_10);
		}

IL_0042:
		{
			IL2CPP_END_FINALLY(57)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(57)
	{
		IL2CPP_JUMP_TBL(0x43, IL_0043)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0043:
	{
		Il2CppObject* L_11 = V_0;
		return ((Il2CppObject *)Castclass(L_11, ICollection_t91669223_il2cpp_TypeInfo_var));
	}
}
// System.Collections.ICollection ThirdParty.Json.LitJson.JsonData::System.Collections.IDictionary.get_Values()
extern Il2CppClass* List_1_t3632373184_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_1_t4227503581_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_1_t1410900363_il2cpp_TypeInfo_var;
extern Il2CppClass* ICollection_1_t920360061_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern Il2CppClass* ICollection_t91669223_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1935663359_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m3387678519_MethodInfo_var;
extern const uint32_t JsonData_System_Collections_IDictionary_get_Values_m4223920047_MetadataUsageId;
extern "C"  Il2CppObject * JsonData_System_Collections_IDictionary_get_Values_m4223920047 (JsonData_t4263252052 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonData_System_Collections_IDictionary_get_Values_m4223920047_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject* V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	KeyValuePair_2_t3935376536  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		JsonData_EnsureDictionary_m1008139353(__this, /*hidden argument*/NULL);
		List_1_t3632373184 * L_0 = (List_1_t3632373184 *)il2cpp_codegen_object_new(List_1_t3632373184_il2cpp_TypeInfo_var);
		List_1__ctor_m1935663359(L_0, /*hidden argument*/List_1__ctor_m1935663359_MethodInfo_var);
		V_0 = (Il2CppObject*)L_0;
		Il2CppObject* L_1 = __this->get_object_list_8();
		NullCheck(L_1);
		Il2CppObject* L_2 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.String,ThirdParty.Json.LitJson.JsonData>>::GetEnumerator() */, IEnumerable_1_t4227503581_il2cpp_TypeInfo_var, L_1);
		V_1 = L_2;
	}

IL_0019:
	try
	{ // begin try (depth: 1)
		{
			goto IL_002f;
		}

IL_001b:
		{
			Il2CppObject* L_3 = V_1;
			NullCheck(L_3);
			KeyValuePair_2_t3935376536  L_4 = InterfaceFuncInvoker0< KeyValuePair_2_t3935376536  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,ThirdParty.Json.LitJson.JsonData>>::get_Current() */, IEnumerator_1_t1410900363_il2cpp_TypeInfo_var, L_3);
			V_2 = L_4;
			Il2CppObject* L_5 = V_0;
			JsonData_t4263252052 * L_6 = KeyValuePair_2_get_Value_m3387678519((&V_2), /*hidden argument*/KeyValuePair_2_get_Value_m3387678519_MethodInfo_var);
			NullCheck(L_5);
			InterfaceActionInvoker1< JsonData_t4263252052 * >::Invoke(2 /* System.Void System.Collections.Generic.ICollection`1<ThirdParty.Json.LitJson.JsonData>::Add(!0) */, ICollection_1_t920360061_il2cpp_TypeInfo_var, L_5, L_6);
		}

IL_002f:
		{
			Il2CppObject* L_7 = V_1;
			NullCheck(L_7);
			bool L_8 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_7);
			if (L_8)
			{
				goto IL_001b;
			}
		}

IL_0037:
		{
			IL2CPP_LEAVE(0x43, FINALLY_0039);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0039;
	}

FINALLY_0039:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_9 = V_1;
			if (!L_9)
			{
				goto IL_0042;
			}
		}

IL_003c:
		{
			Il2CppObject* L_10 = V_1;
			NullCheck(L_10);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_10);
		}

IL_0042:
		{
			IL2CPP_END_FINALLY(57)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(57)
	{
		IL2CPP_JUMP_TBL(0x43, IL_0043)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0043:
	{
		Il2CppObject* L_11 = V_0;
		return ((Il2CppObject *)Castclass(L_11, ICollection_t91669223_il2cpp_TypeInfo_var));
	}
}
// System.Boolean ThirdParty.Json.LitJson.JsonData::System.Collections.IList.get_IsFixedSize()
extern Il2CppClass* IList_t3321498491_il2cpp_TypeInfo_var;
extern const uint32_t JsonData_System_Collections_IList_get_IsFixedSize_m3467184660_MetadataUsageId;
extern "C"  bool JsonData_System_Collections_IList_get_IsFixedSize_m3467184660 (JsonData_t4263252052 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonData_System_Collections_IList_get_IsFixedSize_m3467184660_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = JsonData_EnsureList_m466073493(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IList::get_IsFixedSize() */, IList_t3321498491_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Boolean ThirdParty.Json.LitJson.JsonData::System.Collections.IList.get_IsReadOnly()
extern Il2CppClass* IList_t3321498491_il2cpp_TypeInfo_var;
extern const uint32_t JsonData_System_Collections_IList_get_IsReadOnly_m1949416349_MetadataUsageId;
extern "C"  bool JsonData_System_Collections_IList_get_IsReadOnly_m1949416349 (JsonData_t4263252052 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonData_System_Collections_IList_get_IsReadOnly_m1949416349_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = JsonData_EnsureList_m466073493(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IList::get_IsReadOnly() */, IList_t3321498491_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Object ThirdParty.Json.LitJson.JsonData::System.Collections.IDictionary.get_Item(System.Object)
extern Il2CppClass* IDictionary_t596158605_il2cpp_TypeInfo_var;
extern const uint32_t JsonData_System_Collections_IDictionary_get_Item_m1479983749_MetadataUsageId;
extern "C"  Il2CppObject * JsonData_System_Collections_IDictionary_get_Item_m1479983749 (JsonData_t4263252052 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonData_System_Collections_IDictionary_get_Item_m1479983749_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = JsonData_EnsureDictionary_m1008139353(__this, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___key0;
		NullCheck(L_0);
		Il2CppObject * L_2 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t596158605_il2cpp_TypeInfo_var, L_0, L_1);
		return L_2;
	}
}
// System.Void ThirdParty.Json.LitJson.JsonData::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2427602584;
extern const uint32_t JsonData_System_Collections_IDictionary_set_Item_m88929164_MetadataUsageId;
extern "C"  void JsonData_System_Collections_IDictionary_set_Item_m88929164 (JsonData_t4263252052 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonData_System_Collections_IDictionary_set_Item_m88929164_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JsonData_t4263252052 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___key0;
		if (((String_t*)IsInstSealed(L_0, String_t_il2cpp_TypeInfo_var)))
		{
			goto IL_0013;
		}
	}
	{
		ArgumentException_t3259014390 * L_1 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_1, _stringLiteral2427602584, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0013:
	{
		Il2CppObject * L_2 = ___value1;
		JsonData_t4263252052 * L_3 = JsonData_ToJsonData_m3157715733(__this, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Il2CppObject * L_4 = ___key0;
		JsonData_t4263252052 * L_5 = V_0;
		JsonData_set_Item_m1201291113(__this, ((String_t*)CastclassSealed(L_4, String_t_il2cpp_TypeInfo_var)), L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Object ThirdParty.Json.LitJson.JsonData::System.Collections.IList.get_Item(System.Int32)
extern Il2CppClass* IList_t3321498491_il2cpp_TypeInfo_var;
extern const uint32_t JsonData_System_Collections_IList_get_Item_m3976338114_MetadataUsageId;
extern "C"  Il2CppObject * JsonData_System_Collections_IList_get_Item_m3976338114 (JsonData_t4263252052 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonData_System_Collections_IList_get_Item_m3976338114_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = JsonData_EnsureList_m466073493(__this, /*hidden argument*/NULL);
		int32_t L_1 = ___index0;
		NullCheck(L_0);
		Il2CppObject * L_2 = InterfaceFuncInvoker1< Il2CppObject *, int32_t >::Invoke(2 /* System.Object System.Collections.IList::get_Item(System.Int32) */, IList_t3321498491_il2cpp_TypeInfo_var, L_0, L_1);
		return L_2;
	}
}
// System.Void ThirdParty.Json.LitJson.JsonData::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void JsonData_System_Collections_IList_set_Item_m3245737043 (JsonData_t4263252052 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	JsonData_t4263252052 * V_0 = NULL;
	{
		JsonData_EnsureList_m466073493(__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___value1;
		JsonData_t4263252052 * L_1 = JsonData_ToJsonData_m3157715733(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = ___index0;
		JsonData_t4263252052 * L_3 = V_0;
		JsonData_set_Item_m1528363106(__this, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.IEnumerable`1<System.String> ThirdParty.Json.LitJson.JsonData::get_PropertyNames()
extern Il2CppClass* IDictionary_2_t4177114735_il2cpp_TypeInfo_var;
extern const uint32_t JsonData_get_PropertyNames_m2340996541_MetadataUsageId;
extern "C"  Il2CppObject* JsonData_get_PropertyNames_m2340996541 (JsonData_t4263252052 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonData_get_PropertyNames_m2340996541_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		JsonData_EnsureDictionary_m1008139353(__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = __this->get_inst_object_4();
		NullCheck(L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(6 /* System.Collections.Generic.ICollection`1<!0> System.Collections.Generic.IDictionary`2<System.String,ThirdParty.Json.LitJson.JsonData>::get_Keys() */, IDictionary_2_t4177114735_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// ThirdParty.Json.LitJson.JsonData ThirdParty.Json.LitJson.JsonData::get_Item(System.String)
extern Il2CppClass* IDictionary_2_t4177114735_il2cpp_TypeInfo_var;
extern const uint32_t JsonData_get_Item_m3419504576_MetadataUsageId;
extern "C"  JsonData_t4263252052 * JsonData_get_Item_m3419504576 (JsonData_t4263252052 * __this, String_t* ___prop_name0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonData_get_Item_m3419504576_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JsonData_t4263252052 * V_0 = NULL;
	{
		JsonData_EnsureDictionary_m1008139353(__this, /*hidden argument*/NULL);
		V_0 = (JsonData_t4263252052 *)NULL;
		Il2CppObject* L_0 = __this->get_inst_object_4();
		String_t* L_1 = ___prop_name0;
		NullCheck(L_0);
		InterfaceFuncInvoker2< bool, String_t*, JsonData_t4263252052 ** >::Invoke(3 /* System.Boolean System.Collections.Generic.IDictionary`2<System.String,ThirdParty.Json.LitJson.JsonData>::TryGetValue(!0,!1&) */, IDictionary_2_t4177114735_il2cpp_TypeInfo_var, L_0, L_1, (&V_0));
		JsonData_t4263252052 * L_2 = V_0;
		return L_2;
	}
}
// System.Void ThirdParty.Json.LitJson.JsonData::set_Item(System.String,ThirdParty.Json.LitJson.JsonData)
extern Il2CppClass* IDictionary_2_t4177114735_il2cpp_TypeInfo_var;
extern Il2CppClass* IList_1_t181349841_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ICollection_1_t592484545_il2cpp_TypeInfo_var;
extern const MethodInfo* KeyValuePair_2__ctor_m2784469631_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m1021780796_MethodInfo_var;
extern const uint32_t JsonData_set_Item_m1201291113_MetadataUsageId;
extern "C"  void JsonData_set_Item_m1201291113 (JsonData_t4263252052 * __this, String_t* ___prop_name0, JsonData_t4263252052 * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonData_set_Item_m1201291113_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t3935376536  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	KeyValuePair_2_t3935376536  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		JsonData_EnsureDictionary_m1008139353(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___prop_name0;
		JsonData_t4263252052 * L_1 = ___value1;
		KeyValuePair_2__ctor_m2784469631((&V_0), L_0, L_1, /*hidden argument*/KeyValuePair_2__ctor_m2784469631_MethodInfo_var);
		Il2CppObject* L_2 = __this->get_inst_object_4();
		String_t* L_3 = ___prop_name0;
		NullCheck(L_2);
		bool L_4 = InterfaceFuncInvoker1< bool, String_t* >::Invoke(1 /* System.Boolean System.Collections.Generic.IDictionary`2<System.String,ThirdParty.Json.LitJson.JsonData>::ContainsKey(!0) */, IDictionary_2_t4177114735_il2cpp_TypeInfo_var, L_2, L_3);
		if (!L_4)
		{
			goto IL_0061;
		}
	}
	{
		V_1 = 0;
		goto IL_0051;
	}

IL_0022:
	{
		Il2CppObject* L_5 = __this->get_object_list_8();
		int32_t L_6 = V_1;
		NullCheck(L_5);
		KeyValuePair_2_t3935376536  L_7 = InterfaceFuncInvoker1< KeyValuePair_2_t3935376536 , int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<System.String,ThirdParty.Json.LitJson.JsonData>>::get_Item(System.Int32) */, IList_1_t181349841_il2cpp_TypeInfo_var, L_5, L_6);
		V_2 = L_7;
		String_t* L_8 = KeyValuePair_2_get_Key_m1021780796((&V_2), /*hidden argument*/KeyValuePair_2_get_Key_m1021780796_MethodInfo_var);
		String_t* L_9 = ___prop_name0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_10 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_004d;
		}
	}
	{
		Il2CppObject* L_11 = __this->get_object_list_8();
		int32_t L_12 = V_1;
		KeyValuePair_2_t3935376536  L_13 = V_0;
		NullCheck(L_11);
		InterfaceActionInvoker2< int32_t, KeyValuePair_2_t3935376536  >::Invoke(4 /* System.Void System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<System.String,ThirdParty.Json.LitJson.JsonData>>::set_Item(System.Int32,!0) */, IList_1_t181349841_il2cpp_TypeInfo_var, L_11, L_12, L_13);
		goto IL_006d;
	}

IL_004d:
	{
		int32_t L_14 = V_1;
		V_1 = ((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_0051:
	{
		int32_t L_15 = V_1;
		Il2CppObject* L_16 = __this->get_object_list_8();
		NullCheck(L_16);
		int32_t L_17 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.String,ThirdParty.Json.LitJson.JsonData>>::get_Count() */, ICollection_1_t592484545_il2cpp_TypeInfo_var, L_16);
		if ((((int32_t)L_15) < ((int32_t)L_17)))
		{
			goto IL_0022;
		}
	}
	{
		goto IL_006d;
	}

IL_0061:
	{
		Il2CppObject* L_18 = __this->get_object_list_8();
		KeyValuePair_2_t3935376536  L_19 = V_0;
		NullCheck(L_18);
		InterfaceActionInvoker1< KeyValuePair_2_t3935376536  >::Invoke(2 /* System.Void System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.String,ThirdParty.Json.LitJson.JsonData>>::Add(!0) */, ICollection_1_t592484545_il2cpp_TypeInfo_var, L_18, L_19);
	}

IL_006d:
	{
		Il2CppObject* L_20 = __this->get_inst_object_4();
		String_t* L_21 = ___prop_name0;
		JsonData_t4263252052 * L_22 = ___value1;
		NullCheck(L_20);
		InterfaceActionInvoker2< String_t*, JsonData_t4263252052 * >::Invoke(5 /* System.Void System.Collections.Generic.IDictionary`2<System.String,ThirdParty.Json.LitJson.JsonData>::set_Item(!0,!1) */, IDictionary_2_t4177114735_il2cpp_TypeInfo_var, L_20, L_21, L_22);
		__this->set_json_6((String_t*)NULL);
		return;
	}
}
// System.Void ThirdParty.Json.LitJson.JsonData::set_Item(System.Int32,ThirdParty.Json.LitJson.JsonData)
extern Il2CppClass* IList_1_t509225357_il2cpp_TypeInfo_var;
extern Il2CppClass* IList_1_t181349841_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_2_t4177114735_il2cpp_TypeInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m1021780796_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2__ctor_m2784469631_MethodInfo_var;
extern const uint32_t JsonData_set_Item_m1528363106_MetadataUsageId;
extern "C"  void JsonData_set_Item_m1528363106 (JsonData_t4263252052 * __this, int32_t ___index0, JsonData_t4263252052 * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonData_set_Item_m1528363106_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t3935376536  V_0;
	memset(&V_0, 0, sizeof(V_0));
	KeyValuePair_2_t3935376536  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		JsonData_EnsureCollection_m2402886013(__this, /*hidden argument*/NULL);
		int32_t L_0 = __this->get_type_7();
		if ((!(((uint32_t)L_0) == ((uint32_t)2))))
		{
			goto IL_001f;
		}
	}
	{
		Il2CppObject* L_1 = __this->get_inst_array_0();
		int32_t L_2 = ___index0;
		JsonData_t4263252052 * L_3 = ___value1;
		NullCheck(L_1);
		InterfaceActionInvoker2< int32_t, JsonData_t4263252052 * >::Invoke(4 /* System.Void System.Collections.Generic.IList`1<ThirdParty.Json.LitJson.JsonData>::set_Item(System.Int32,!0) */, IList_1_t509225357_il2cpp_TypeInfo_var, L_1, L_2, L_3);
		goto IL_005b;
	}

IL_001f:
	{
		Il2CppObject* L_4 = __this->get_object_list_8();
		int32_t L_5 = ___index0;
		NullCheck(L_4);
		KeyValuePair_2_t3935376536  L_6 = InterfaceFuncInvoker1< KeyValuePair_2_t3935376536 , int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<System.String,ThirdParty.Json.LitJson.JsonData>>::get_Item(System.Int32) */, IList_1_t181349841_il2cpp_TypeInfo_var, L_4, L_5);
		V_0 = L_6;
		String_t* L_7 = KeyValuePair_2_get_Key_m1021780796((&V_0), /*hidden argument*/KeyValuePair_2_get_Key_m1021780796_MethodInfo_var);
		JsonData_t4263252052 * L_8 = ___value1;
		KeyValuePair_2__ctor_m2784469631((&V_1), L_7, L_8, /*hidden argument*/KeyValuePair_2__ctor_m2784469631_MethodInfo_var);
		Il2CppObject* L_9 = __this->get_object_list_8();
		int32_t L_10 = ___index0;
		KeyValuePair_2_t3935376536  L_11 = V_1;
		NullCheck(L_9);
		InterfaceActionInvoker2< int32_t, KeyValuePair_2_t3935376536  >::Invoke(4 /* System.Void System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<System.String,ThirdParty.Json.LitJson.JsonData>>::set_Item(System.Int32,!0) */, IList_1_t181349841_il2cpp_TypeInfo_var, L_9, L_10, L_11);
		Il2CppObject* L_12 = __this->get_inst_object_4();
		String_t* L_13 = KeyValuePair_2_get_Key_m1021780796((&V_0), /*hidden argument*/KeyValuePair_2_get_Key_m1021780796_MethodInfo_var);
		JsonData_t4263252052 * L_14 = ___value1;
		NullCheck(L_12);
		InterfaceActionInvoker2< String_t*, JsonData_t4263252052 * >::Invoke(5 /* System.Void System.Collections.Generic.IDictionary`2<System.String,ThirdParty.Json.LitJson.JsonData>::set_Item(!0,!1) */, IDictionary_2_t4177114735_il2cpp_TypeInfo_var, L_12, L_13, L_14);
	}

IL_005b:
	{
		__this->set_json_6((String_t*)NULL);
		return;
	}
}
// System.Void ThirdParty.Json.LitJson.JsonData::.ctor()
extern "C"  void JsonData__ctor_m2183287813 (JsonData_t4263252052 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ThirdParty.Json.LitJson.JsonData::.ctor(System.Object)
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern Il2CppClass* Double_t4078015681_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt64_t2909196914_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt32_t2149682021_il2cpp_TypeInfo_var;
extern Il2CppClass* Int64_t909078037_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4256503435;
extern const uint32_t JsonData__ctor_m3249347595_MetadataUsageId;
extern "C"  void JsonData__ctor_m3249347595 (JsonData_t4263252052 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonData__ctor_m3249347595_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___obj0;
		if (!((Il2CppObject *)IsInstSealed(L_0, Boolean_t3825574718_il2cpp_TypeInfo_var)))
		{
			goto IL_0023;
		}
	}
	{
		__this->set_type_7(((int32_t)9));
		Il2CppObject * L_1 = ___obj0;
		__this->set_inst_boolean_1(((*(bool*)((bool*)UnBox (L_1, Boolean_t3825574718_il2cpp_TypeInfo_var)))));
		return;
	}

IL_0023:
	{
		Il2CppObject * L_2 = ___obj0;
		if (!((Il2CppObject *)IsInstSealed(L_2, Double_t4078015681_il2cpp_TypeInfo_var)))
		{
			goto IL_003f;
		}
	}
	{
		__this->set_type_7(8);
		Il2CppObject * L_3 = ___obj0;
		__this->set_inst_double_2(((*(double*)((double*)UnBox (L_3, Double_t4078015681_il2cpp_TypeInfo_var)))));
		return;
	}

IL_003f:
	{
		Il2CppObject * L_4 = ___obj0;
		if (!((Il2CppObject *)IsInstSealed(L_4, Int32_t2071877448_il2cpp_TypeInfo_var)))
		{
			goto IL_005b;
		}
	}
	{
		__this->set_type_7(4);
		Il2CppObject * L_5 = ___obj0;
		__this->set_inst_number_3(((*(uint64_t*)((uint64_t*)UnBox (L_5, UInt64_t2909196914_il2cpp_TypeInfo_var)))));
		return;
	}

IL_005b:
	{
		Il2CppObject * L_6 = ___obj0;
		if (!((Il2CppObject *)IsInstSealed(L_6, UInt32_t2149682021_il2cpp_TypeInfo_var)))
		{
			goto IL_0077;
		}
	}
	{
		__this->set_type_7(5);
		Il2CppObject * L_7 = ___obj0;
		__this->set_inst_number_3(((*(uint64_t*)((uint64_t*)UnBox (L_7, UInt64_t2909196914_il2cpp_TypeInfo_var)))));
		return;
	}

IL_0077:
	{
		Il2CppObject * L_8 = ___obj0;
		if (!((Il2CppObject *)IsInstSealed(L_8, Int64_t909078037_il2cpp_TypeInfo_var)))
		{
			goto IL_0093;
		}
	}
	{
		__this->set_type_7(6);
		Il2CppObject * L_9 = ___obj0;
		__this->set_inst_number_3(((*(uint64_t*)((uint64_t*)UnBox (L_9, UInt64_t2909196914_il2cpp_TypeInfo_var)))));
		return;
	}

IL_0093:
	{
		Il2CppObject * L_10 = ___obj0;
		if (!((Il2CppObject *)IsInstSealed(L_10, UInt64_t2909196914_il2cpp_TypeInfo_var)))
		{
			goto IL_00af;
		}
	}
	{
		__this->set_type_7(7);
		Il2CppObject * L_11 = ___obj0;
		__this->set_inst_number_3(((*(uint64_t*)((uint64_t*)UnBox (L_11, UInt64_t2909196914_il2cpp_TypeInfo_var)))));
		return;
	}

IL_00af:
	{
		Il2CppObject * L_12 = ___obj0;
		if (!((String_t*)IsInstSealed(L_12, String_t_il2cpp_TypeInfo_var)))
		{
			goto IL_00cb;
		}
	}
	{
		__this->set_type_7(3);
		Il2CppObject * L_13 = ___obj0;
		__this->set_inst_string_5(((String_t*)CastclassSealed(L_13, String_t_il2cpp_TypeInfo_var)));
		return;
	}

IL_00cb:
	{
		ArgumentException_t3259014390 * L_14 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_14, _stringLiteral4256503435, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_14);
	}
}
// System.Boolean ThirdParty.Json.LitJson.JsonData::op_Explicit(ThirdParty.Json.LitJson.JsonData)
extern Il2CppClass* InvalidCastException_t3625212209_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral340782327;
extern const uint32_t JsonData_op_Explicit_m4097164641_MetadataUsageId;
extern "C"  bool JsonData_op_Explicit_m4097164641 (Il2CppObject * __this /* static, unused */, JsonData_t4263252052 * ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonData_op_Explicit_m4097164641_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		JsonData_t4263252052 * L_0 = ___data0;
		NullCheck(L_0);
		int32_t L_1 = L_0->get_type_7();
		if ((((int32_t)L_1) == ((int32_t)((int32_t)9))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidCastException_t3625212209 * L_2 = (InvalidCastException_t3625212209 *)il2cpp_codegen_object_new(InvalidCastException_t3625212209_il2cpp_TypeInfo_var);
		InvalidCastException__ctor_m2960334316(L_2, _stringLiteral340782327, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0015:
	{
		JsonData_t4263252052 * L_3 = ___data0;
		NullCheck(L_3);
		bool L_4 = L_3->get_inst_boolean_1();
		return L_4;
	}
}
// System.String ThirdParty.Json.LitJson.JsonData::op_Explicit(ThirdParty.Json.LitJson.JsonData)
extern Il2CppClass* InvalidCastException_t3625212209_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2508555007;
extern const uint32_t JsonData_op_Explicit_m1524888292_MetadataUsageId;
extern "C"  String_t* JsonData_op_Explicit_m1524888292 (Il2CppObject * __this /* static, unused */, JsonData_t4263252052 * ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonData_op_Explicit_m1524888292_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		JsonData_t4263252052 * L_0 = ___data0;
		NullCheck(L_0);
		int32_t L_1 = L_0->get_type_7();
		if ((((int32_t)L_1) == ((int32_t)3)))
		{
			goto IL_0014;
		}
	}
	{
		InvalidCastException_t3625212209 * L_2 = (InvalidCastException_t3625212209 *)il2cpp_codegen_object_new(InvalidCastException_t3625212209_il2cpp_TypeInfo_var);
		InvalidCastException__ctor_m2960334316(L_2, _stringLiteral2508555007, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0014:
	{
		JsonData_t4263252052 * L_3 = ___data0;
		NullCheck(L_3);
		String_t* L_4 = L_3->get_inst_string_5();
		return L_4;
	}
}
// System.Void ThirdParty.Json.LitJson.JsonData::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern Il2CppClass* ICollection_t91669223_il2cpp_TypeInfo_var;
extern const uint32_t JsonData_System_Collections_ICollection_CopyTo_m1746890421_MetadataUsageId;
extern "C"  void JsonData_System_Collections_ICollection_CopyTo_m1746890421 (JsonData_t4263252052 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonData_System_Collections_ICollection_CopyTo_m1746890421_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = JsonData_EnsureCollection_m2402886013(__this, /*hidden argument*/NULL);
		Il2CppArray * L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck(L_0);
		InterfaceActionInvoker2< Il2CppArray *, int32_t >::Invoke(3 /* System.Void System.Collections.ICollection::CopyTo(System.Array,System.Int32) */, ICollection_t91669223_il2cpp_TypeInfo_var, L_0, L_1, L_2);
		return;
	}
}
// System.Void ThirdParty.Json.LitJson.JsonData::System.Collections.IDictionary.Add(System.Object,System.Object)
extern Il2CppClass* IDictionary_t596158605_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ICollection_1_t592484545_il2cpp_TypeInfo_var;
extern const MethodInfo* KeyValuePair_2__ctor_m2784469631_MethodInfo_var;
extern const uint32_t JsonData_System_Collections_IDictionary_Add_m180357303_MetadataUsageId;
extern "C"  void JsonData_System_Collections_IDictionary_Add_m180357303 (JsonData_t4263252052 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonData_System_Collections_IDictionary_Add_m180357303_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JsonData_t4263252052 * V_0 = NULL;
	KeyValuePair_2_t3935376536  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Il2CppObject * L_0 = ___value1;
		JsonData_t4263252052 * L_1 = JsonData_ToJsonData_m3157715733(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Il2CppObject * L_2 = JsonData_EnsureDictionary_m1008139353(__this, /*hidden argument*/NULL);
		Il2CppObject * L_3 = ___key0;
		JsonData_t4263252052 * L_4 = V_0;
		NullCheck(L_2);
		InterfaceActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(4 /* System.Void System.Collections.IDictionary::Add(System.Object,System.Object) */, IDictionary_t596158605_il2cpp_TypeInfo_var, L_2, L_3, L_4);
		Il2CppObject * L_5 = ___key0;
		JsonData_t4263252052 * L_6 = V_0;
		KeyValuePair_2__ctor_m2784469631((&V_1), ((String_t*)CastclassSealed(L_5, String_t_il2cpp_TypeInfo_var)), L_6, /*hidden argument*/KeyValuePair_2__ctor_m2784469631_MethodInfo_var);
		Il2CppObject* L_7 = __this->get_object_list_8();
		KeyValuePair_2_t3935376536  L_8 = V_1;
		NullCheck(L_7);
		InterfaceActionInvoker1< KeyValuePair_2_t3935376536  >::Invoke(2 /* System.Void System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.String,ThirdParty.Json.LitJson.JsonData>>::Add(!0) */, ICollection_1_t592484545_il2cpp_TypeInfo_var, L_7, L_8);
		__this->set_json_6((String_t*)NULL);
		return;
	}
}
// System.Void ThirdParty.Json.LitJson.JsonData::System.Collections.IDictionary.Clear()
extern Il2CppClass* IDictionary_t596158605_il2cpp_TypeInfo_var;
extern Il2CppClass* ICollection_1_t592484545_il2cpp_TypeInfo_var;
extern const uint32_t JsonData_System_Collections_IDictionary_Clear_m289246979_MetadataUsageId;
extern "C"  void JsonData_System_Collections_IDictionary_Clear_m289246979 (JsonData_t4263252052 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonData_System_Collections_IDictionary_Clear_m289246979_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = JsonData_EnsureDictionary_m1008139353(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		InterfaceActionInvoker0::Invoke(5 /* System.Void System.Collections.IDictionary::Clear() */, IDictionary_t596158605_il2cpp_TypeInfo_var, L_0);
		Il2CppObject* L_1 = __this->get_object_list_8();
		NullCheck(L_1);
		InterfaceActionInvoker0::Invoke(3 /* System.Void System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.String,ThirdParty.Json.LitJson.JsonData>>::Clear() */, ICollection_1_t592484545_il2cpp_TypeInfo_var, L_1);
		__this->set_json_6((String_t*)NULL);
		return;
	}
}
// System.Boolean ThirdParty.Json.LitJson.JsonData::System.Collections.IDictionary.Contains(System.Object)
extern Il2CppClass* IDictionary_t596158605_il2cpp_TypeInfo_var;
extern const uint32_t JsonData_System_Collections_IDictionary_Contains_m166609687_MetadataUsageId;
extern "C"  bool JsonData_System_Collections_IDictionary_Contains_m166609687 (JsonData_t4263252052 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonData_System_Collections_IDictionary_Contains_m166609687_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = JsonData_EnsureDictionary_m1008139353(__this, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___key0;
		NullCheck(L_0);
		bool L_2 = InterfaceFuncInvoker1< bool, Il2CppObject * >::Invoke(6 /* System.Boolean System.Collections.IDictionary::Contains(System.Object) */, IDictionary_t596158605_il2cpp_TypeInfo_var, L_0, L_1);
		return L_2;
	}
}
// System.Collections.IDictionaryEnumerator ThirdParty.Json.LitJson.JsonData::System.Collections.IDictionary.GetEnumerator()
extern Il2CppClass* IOrderedDictionary_t252115128_il2cpp_TypeInfo_var;
extern const uint32_t JsonData_System_Collections_IDictionary_GetEnumerator_m1549877592_MetadataUsageId;
extern "C"  Il2CppObject * JsonData_System_Collections_IDictionary_GetEnumerator_m1549877592 (JsonData_t4263252052 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonData_System_Collections_IDictionary_GetEnumerator_m1549877592_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IDictionaryEnumerator System.Collections.Specialized.IOrderedDictionary::GetEnumerator() */, IOrderedDictionary_t252115128_il2cpp_TypeInfo_var, __this);
		return L_0;
	}
}
// System.Void ThirdParty.Json.LitJson.JsonData::System.Collections.IDictionary.Remove(System.Object)
extern Il2CppClass* IDictionary_t596158605_il2cpp_TypeInfo_var;
extern Il2CppClass* IList_1_t181349841_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ICollection_1_t592484545_il2cpp_TypeInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m1021780796_MethodInfo_var;
extern const uint32_t JsonData_System_Collections_IDictionary_Remove_m2528017504_MetadataUsageId;
extern "C"  void JsonData_System_Collections_IDictionary_Remove_m2528017504 (JsonData_t4263252052 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonData_System_Collections_IDictionary_Remove_m2528017504_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	KeyValuePair_2_t3935376536  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Il2CppObject * L_0 = JsonData_EnsureDictionary_m1008139353(__this, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___key0;
		NullCheck(L_0);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(8 /* System.Void System.Collections.IDictionary::Remove(System.Object) */, IDictionary_t596158605_il2cpp_TypeInfo_var, L_0, L_1);
		V_0 = 0;
		goto IL_0043;
	}

IL_0010:
	{
		Il2CppObject* L_2 = __this->get_object_list_8();
		int32_t L_3 = V_0;
		NullCheck(L_2);
		KeyValuePair_2_t3935376536  L_4 = InterfaceFuncInvoker1< KeyValuePair_2_t3935376536 , int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<System.String,ThirdParty.Json.LitJson.JsonData>>::get_Item(System.Int32) */, IList_1_t181349841_il2cpp_TypeInfo_var, L_2, L_3);
		V_1 = L_4;
		String_t* L_5 = KeyValuePair_2_get_Key_m1021780796((&V_1), /*hidden argument*/KeyValuePair_2_get_Key_m1021780796_MethodInfo_var);
		Il2CppObject * L_6 = ___key0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_7 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_5, ((String_t*)CastclassSealed(L_6, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_003f;
		}
	}
	{
		Il2CppObject* L_8 = __this->get_object_list_8();
		int32_t L_9 = V_0;
		NullCheck(L_8);
		InterfaceActionInvoker1< int32_t >::Invoke(2 /* System.Void System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<System.String,ThirdParty.Json.LitJson.JsonData>>::RemoveAt(System.Int32) */, IList_1_t181349841_il2cpp_TypeInfo_var, L_8, L_9);
		goto IL_0051;
	}

IL_003f:
	{
		int32_t L_10 = V_0;
		V_0 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0043:
	{
		int32_t L_11 = V_0;
		Il2CppObject* L_12 = __this->get_object_list_8();
		NullCheck(L_12);
		int32_t L_13 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.String,ThirdParty.Json.LitJson.JsonData>>::get_Count() */, ICollection_1_t592484545_il2cpp_TypeInfo_var, L_12);
		if ((((int32_t)L_11) < ((int32_t)L_13)))
		{
			goto IL_0010;
		}
	}

IL_0051:
	{
		__this->set_json_6((String_t*)NULL);
		return;
	}
}
// System.Collections.IEnumerator ThirdParty.Json.LitJson.JsonData::System.Collections.IEnumerable.GetEnumerator()
extern Il2CppClass* IEnumerable_t2911409499_il2cpp_TypeInfo_var;
extern const uint32_t JsonData_System_Collections_IEnumerable_GetEnumerator_m1963786586_MetadataUsageId;
extern "C"  Il2CppObject * JsonData_System_Collections_IEnumerable_GetEnumerator_m1963786586 (JsonData_t4263252052 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonData_System_Collections_IEnumerable_GetEnumerator_m1963786586_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = JsonData_EnsureCollection_m2402886013(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t2911409499_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Void ThirdParty.Json.LitJson.JsonData::ThirdParty.Json.LitJson.IJsonWrapper.SetBoolean(System.Boolean)
extern "C"  void JsonData_ThirdParty_Json_LitJson_IJsonWrapper_SetBoolean_m3309887618 (JsonData_t4263252052 * __this, bool ___val0, const MethodInfo* method)
{
	{
		__this->set_type_7(((int32_t)9));
		bool L_0 = ___val0;
		__this->set_inst_boolean_1(L_0);
		__this->set_json_6((String_t*)NULL);
		return;
	}
}
// System.Void ThirdParty.Json.LitJson.JsonData::ThirdParty.Json.LitJson.IJsonWrapper.SetDouble(System.Double)
extern "C"  void JsonData_ThirdParty_Json_LitJson_IJsonWrapper_SetDouble_m1532392792 (JsonData_t4263252052 * __this, double ___val0, const MethodInfo* method)
{
	{
		__this->set_type_7(8);
		double L_0 = ___val0;
		__this->set_inst_double_2(L_0);
		__this->set_json_6((String_t*)NULL);
		return;
	}
}
// System.Void ThirdParty.Json.LitJson.JsonData::ThirdParty.Json.LitJson.IJsonWrapper.SetInt(System.Int32)
extern "C"  void JsonData_ThirdParty_Json_LitJson_IJsonWrapper_SetInt_m2682965327 (JsonData_t4263252052 * __this, int32_t ___val0, const MethodInfo* method)
{
	{
		__this->set_type_7(4);
		int32_t L_0 = ___val0;
		__this->set_inst_number_3((((int64_t)((int64_t)L_0))));
		__this->set_json_6((String_t*)NULL);
		return;
	}
}
// System.Void ThirdParty.Json.LitJson.JsonData::ThirdParty.Json.LitJson.IJsonWrapper.SetUInt(System.UInt32)
extern "C"  void JsonData_ThirdParty_Json_LitJson_IJsonWrapper_SetUInt_m2614768943 (JsonData_t4263252052 * __this, uint32_t ___val0, const MethodInfo* method)
{
	{
		__this->set_type_7(5);
		uint32_t L_0 = ___val0;
		__this->set_inst_number_3((((int64_t)((uint64_t)L_0))));
		__this->set_json_6((String_t*)NULL);
		return;
	}
}
// System.Void ThirdParty.Json.LitJson.JsonData::ThirdParty.Json.LitJson.IJsonWrapper.SetLong(System.Int64)
extern "C"  void JsonData_ThirdParty_Json_LitJson_IJsonWrapper_SetLong_m1349946637 (JsonData_t4263252052 * __this, int64_t ___val0, const MethodInfo* method)
{
	{
		__this->set_type_7(6);
		int64_t L_0 = ___val0;
		__this->set_inst_number_3(L_0);
		__this->set_json_6((String_t*)NULL);
		return;
	}
}
// System.Void ThirdParty.Json.LitJson.JsonData::ThirdParty.Json.LitJson.IJsonWrapper.SetULong(System.UInt64)
extern "C"  void JsonData_ThirdParty_Json_LitJson_IJsonWrapper_SetULong_m4270264629 (JsonData_t4263252052 * __this, uint64_t ___val0, const MethodInfo* method)
{
	{
		__this->set_type_7(7);
		uint64_t L_0 = ___val0;
		__this->set_inst_number_3(L_0);
		__this->set_json_6((String_t*)NULL);
		return;
	}
}
// System.Void ThirdParty.Json.LitJson.JsonData::ThirdParty.Json.LitJson.IJsonWrapper.SetString(System.String)
extern "C"  void JsonData_ThirdParty_Json_LitJson_IJsonWrapper_SetString_m929399128 (JsonData_t4263252052 * __this, String_t* ___val0, const MethodInfo* method)
{
	{
		__this->set_type_7(3);
		String_t* L_0 = ___val0;
		__this->set_inst_string_5(L_0);
		__this->set_json_6((String_t*)NULL);
		return;
	}
}
// System.Int32 ThirdParty.Json.LitJson.JsonData::System.Collections.IList.Add(System.Object)
extern "C"  int32_t JsonData_System_Collections_IList_Add_m4215642409 (JsonData_t4263252052 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		int32_t L_1 = JsonData_Add_m1775657322(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void ThirdParty.Json.LitJson.JsonData::System.Collections.IList.Clear()
extern Il2CppClass* IList_t3321498491_il2cpp_TypeInfo_var;
extern const uint32_t JsonData_System_Collections_IList_Clear_m562217805_MetadataUsageId;
extern "C"  void JsonData_System_Collections_IList_Clear_m562217805 (JsonData_t4263252052 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonData_System_Collections_IList_Clear_m562217805_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = JsonData_EnsureList_m466073493(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		InterfaceActionInvoker0::Invoke(5 /* System.Void System.Collections.IList::Clear() */, IList_t3321498491_il2cpp_TypeInfo_var, L_0);
		__this->set_json_6((String_t*)NULL);
		return;
	}
}
// System.Boolean ThirdParty.Json.LitJson.JsonData::System.Collections.IList.Contains(System.Object)
extern Il2CppClass* IList_t3321498491_il2cpp_TypeInfo_var;
extern const uint32_t JsonData_System_Collections_IList_Contains_m4270132069_MetadataUsageId;
extern "C"  bool JsonData_System_Collections_IList_Contains_m4270132069 (JsonData_t4263252052 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonData_System_Collections_IList_Contains_m4270132069_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = JsonData_EnsureList_m466073493(__this, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___value0;
		NullCheck(L_0);
		bool L_2 = InterfaceFuncInvoker1< bool, Il2CppObject * >::Invoke(6 /* System.Boolean System.Collections.IList::Contains(System.Object) */, IList_t3321498491_il2cpp_TypeInfo_var, L_0, L_1);
		return L_2;
	}
}
// System.Int32 ThirdParty.Json.LitJson.JsonData::System.Collections.IList.IndexOf(System.Object)
extern Il2CppClass* IList_t3321498491_il2cpp_TypeInfo_var;
extern const uint32_t JsonData_System_Collections_IList_IndexOf_m1091158327_MetadataUsageId;
extern "C"  int32_t JsonData_System_Collections_IList_IndexOf_m1091158327 (JsonData_t4263252052 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonData_System_Collections_IList_IndexOf_m1091158327_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = JsonData_EnsureList_m466073493(__this, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___value0;
		NullCheck(L_0);
		int32_t L_2 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(7 /* System.Int32 System.Collections.IList::IndexOf(System.Object) */, IList_t3321498491_il2cpp_TypeInfo_var, L_0, L_1);
		return L_2;
	}
}
// System.Void ThirdParty.Json.LitJson.JsonData::System.Collections.IList.Insert(System.Int32,System.Object)
extern Il2CppClass* IList_t3321498491_il2cpp_TypeInfo_var;
extern const uint32_t JsonData_System_Collections_IList_Insert_m954013926_MetadataUsageId;
extern "C"  void JsonData_System_Collections_IList_Insert_m954013926 (JsonData_t4263252052 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonData_System_Collections_IList_Insert_m954013926_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = JsonData_EnsureList_m466073493(__this, /*hidden argument*/NULL);
		int32_t L_1 = ___index0;
		Il2CppObject * L_2 = ___value1;
		NullCheck(L_0);
		InterfaceActionInvoker2< int32_t, Il2CppObject * >::Invoke(8 /* System.Void System.Collections.IList::Insert(System.Int32,System.Object) */, IList_t3321498491_il2cpp_TypeInfo_var, L_0, L_1, L_2);
		__this->set_json_6((String_t*)NULL);
		return;
	}
}
// System.Void ThirdParty.Json.LitJson.JsonData::System.Collections.IList.Remove(System.Object)
extern Il2CppClass* IList_t3321498491_il2cpp_TypeInfo_var;
extern const uint32_t JsonData_System_Collections_IList_Remove_m1084822036_MetadataUsageId;
extern "C"  void JsonData_System_Collections_IList_Remove_m1084822036 (JsonData_t4263252052 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonData_System_Collections_IList_Remove_m1084822036_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = JsonData_EnsureList_m466073493(__this, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___value0;
		NullCheck(L_0);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(9 /* System.Void System.Collections.IList::Remove(System.Object) */, IList_t3321498491_il2cpp_TypeInfo_var, L_0, L_1);
		__this->set_json_6((String_t*)NULL);
		return;
	}
}
// System.Void ThirdParty.Json.LitJson.JsonData::System.Collections.IList.RemoveAt(System.Int32)
extern Il2CppClass* IList_t3321498491_il2cpp_TypeInfo_var;
extern const uint32_t JsonData_System_Collections_IList_RemoveAt_m1723154408_MetadataUsageId;
extern "C"  void JsonData_System_Collections_IList_RemoveAt_m1723154408 (JsonData_t4263252052 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonData_System_Collections_IList_RemoveAt_m1723154408_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = JsonData_EnsureList_m466073493(__this, /*hidden argument*/NULL);
		int32_t L_1 = ___index0;
		NullCheck(L_0);
		InterfaceActionInvoker1< int32_t >::Invoke(10 /* System.Void System.Collections.IList::RemoveAt(System.Int32) */, IList_t3321498491_il2cpp_TypeInfo_var, L_0, L_1);
		__this->set_json_6((String_t*)NULL);
		return;
	}
}
// System.Collections.IDictionaryEnumerator ThirdParty.Json.LitJson.JsonData::System.Collections.Specialized.IOrderedDictionary.GetEnumerator()
extern Il2CppClass* IEnumerable_1_t4227503581_il2cpp_TypeInfo_var;
extern Il2CppClass* OrderedDictionaryEnumerator_t1664192327_il2cpp_TypeInfo_var;
extern const uint32_t JsonData_System_Collections_Specialized_IOrderedDictionary_GetEnumerator_m1610861286_MetadataUsageId;
extern "C"  Il2CppObject * JsonData_System_Collections_Specialized_IOrderedDictionary_GetEnumerator_m1610861286 (JsonData_t4263252052 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonData_System_Collections_Specialized_IOrderedDictionary_GetEnumerator_m1610861286_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		JsonData_EnsureDictionary_m1008139353(__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = __this->get_object_list_8();
		NullCheck(L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.String,ThirdParty.Json.LitJson.JsonData>>::GetEnumerator() */, IEnumerable_1_t4227503581_il2cpp_TypeInfo_var, L_0);
		OrderedDictionaryEnumerator_t1664192327 * L_2 = (OrderedDictionaryEnumerator_t1664192327 *)il2cpp_codegen_object_new(OrderedDictionaryEnumerator_t1664192327_il2cpp_TypeInfo_var);
		OrderedDictionaryEnumerator__ctor_m631766896(L_2, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Collections.ICollection ThirdParty.Json.LitJson.JsonData::EnsureCollection()
extern Il2CppClass* ICollection_t91669223_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4279808014;
extern const uint32_t JsonData_EnsureCollection_m2402886013_MetadataUsageId;
extern "C"  Il2CppObject * JsonData_EnsureCollection_m2402886013 (JsonData_t4263252052 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonData_EnsureCollection_m2402886013_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_type_7();
		if ((!(((uint32_t)L_0) == ((uint32_t)2))))
		{
			goto IL_0015;
		}
	}
	{
		Il2CppObject* L_1 = __this->get_inst_array_0();
		return ((Il2CppObject *)Castclass(L_1, ICollection_t91669223_il2cpp_TypeInfo_var));
	}

IL_0015:
	{
		int32_t L_2 = __this->get_type_7();
		if ((!(((uint32_t)L_2) == ((uint32_t)1))))
		{
			goto IL_002a;
		}
	}
	{
		Il2CppObject* L_3 = __this->get_inst_object_4();
		return ((Il2CppObject *)Castclass(L_3, ICollection_t91669223_il2cpp_TypeInfo_var));
	}

IL_002a:
	{
		InvalidOperationException_t721527559 * L_4 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_4, _stringLiteral4279808014, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}
}
// System.Collections.IDictionary ThirdParty.Json.LitJson.JsonData::EnsureDictionary()
extern Il2CppClass* IDictionary_t596158605_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t1883064018_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t3304497668_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m3262415100_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m66208350_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1390127078;
extern const uint32_t JsonData_EnsureDictionary_m1008139353_MetadataUsageId;
extern "C"  Il2CppObject * JsonData_EnsureDictionary_m1008139353 (JsonData_t4263252052 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonData_EnsureDictionary_m1008139353_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_type_7();
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0015;
		}
	}
	{
		Il2CppObject* L_1 = __this->get_inst_object_4();
		return ((Il2CppObject *)Castclass(L_1, IDictionary_t596158605_il2cpp_TypeInfo_var));
	}

IL_0015:
	{
		int32_t L_2 = __this->get_type_7();
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, _stringLiteral1390127078, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0028:
	{
		__this->set_type_7(1);
		Dictionary_2_t1883064018 * L_4 = (Dictionary_2_t1883064018 *)il2cpp_codegen_object_new(Dictionary_2_t1883064018_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m3262415100(L_4, /*hidden argument*/Dictionary_2__ctor_m3262415100_MethodInfo_var);
		__this->set_inst_object_4(L_4);
		List_1_t3304497668 * L_5 = (List_1_t3304497668 *)il2cpp_codegen_object_new(List_1_t3304497668_il2cpp_TypeInfo_var);
		List_1__ctor_m66208350(L_5, /*hidden argument*/List_1__ctor_m66208350_MethodInfo_var);
		__this->set_object_list_8(L_5);
		Il2CppObject* L_6 = __this->get_inst_object_4();
		return ((Il2CppObject *)Castclass(L_6, IDictionary_t596158605_il2cpp_TypeInfo_var));
	}
}
// System.Collections.IList ThirdParty.Json.LitJson.JsonData::EnsureList()
extern Il2CppClass* IList_t3321498491_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t3632373184_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1935663359_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2449718858;
extern const uint32_t JsonData_EnsureList_m466073493_MetadataUsageId;
extern "C"  Il2CppObject * JsonData_EnsureList_m466073493 (JsonData_t4263252052 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonData_EnsureList_m466073493_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_type_7();
		if ((!(((uint32_t)L_0) == ((uint32_t)2))))
		{
			goto IL_0015;
		}
	}
	{
		Il2CppObject* L_1 = __this->get_inst_array_0();
		return ((Il2CppObject *)Castclass(L_1, IList_t3321498491_il2cpp_TypeInfo_var));
	}

IL_0015:
	{
		int32_t L_2 = __this->get_type_7();
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, _stringLiteral2449718858, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0028:
	{
		__this->set_type_7(2);
		List_1_t3632373184 * L_4 = (List_1_t3632373184 *)il2cpp_codegen_object_new(List_1_t3632373184_il2cpp_TypeInfo_var);
		List_1__ctor_m1935663359(L_4, /*hidden argument*/List_1__ctor_m1935663359_MethodInfo_var);
		__this->set_inst_array_0(L_4);
		Il2CppObject* L_5 = __this->get_inst_array_0();
		return ((Il2CppObject *)Castclass(L_5, IList_t3321498491_il2cpp_TypeInfo_var));
	}
}
// ThirdParty.Json.LitJson.JsonData ThirdParty.Json.LitJson.JsonData::ToJsonData(System.Object)
extern Il2CppClass* JsonData_t4263252052_il2cpp_TypeInfo_var;
extern const uint32_t JsonData_ToJsonData_m3157715733_MetadataUsageId;
extern "C"  JsonData_t4263252052 * JsonData_ToJsonData_m3157715733 (JsonData_t4263252052 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonData_ToJsonData_m3157715733_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___obj0;
		if (L_0)
		{
			goto IL_0005;
		}
	}
	{
		return (JsonData_t4263252052 *)NULL;
	}

IL_0005:
	{
		Il2CppObject * L_1 = ___obj0;
		if (!((JsonData_t4263252052 *)IsInstClass(L_1, JsonData_t4263252052_il2cpp_TypeInfo_var)))
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_2 = ___obj0;
		return ((JsonData_t4263252052 *)CastclassClass(L_2, JsonData_t4263252052_il2cpp_TypeInfo_var));
	}

IL_0014:
	{
		Il2CppObject * L_3 = ___obj0;
		JsonData_t4263252052 * L_4 = (JsonData_t4263252052 *)il2cpp_codegen_object_new(JsonData_t4263252052_il2cpp_TypeInfo_var);
		JsonData__ctor_m3249347595(L_4, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Int32 ThirdParty.Json.LitJson.JsonData::Add(System.Object)
extern Il2CppClass* IList_t3321498491_il2cpp_TypeInfo_var;
extern const uint32_t JsonData_Add_m1775657322_MetadataUsageId;
extern "C"  int32_t JsonData_Add_m1775657322 (JsonData_t4263252052 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonData_Add_m1775657322_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JsonData_t4263252052 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___value0;
		JsonData_t4263252052 * L_1 = JsonData_ToJsonData_m3157715733(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		__this->set_json_6((String_t*)NULL);
		Il2CppObject * L_2 = JsonData_EnsureList_m466073493(__this, /*hidden argument*/NULL);
		JsonData_t4263252052 * L_3 = V_0;
		NullCheck(L_2);
		int32_t L_4 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(4 /* System.Int32 System.Collections.IList::Add(System.Object) */, IList_t3321498491_il2cpp_TypeInfo_var, L_2, L_3);
		return L_4;
	}
}
// System.Boolean ThirdParty.Json.LitJson.JsonData::Equals(ThirdParty.Json.LitJson.JsonData)
extern "C"  bool JsonData_Equals_m310125024 (JsonData_t4263252052 * __this, JsonData_t4263252052 * ___x0, const MethodInfo* method)
{
	bool V_0 = false;
	bool V_1 = false;
	bool V_2 = false;
	int32_t V_3 = 0;
	int32_t G_B6_0 = 0;
	int32_t G_B8_0 = 0;
	int32_t G_B7_0 = 0;
	int32_t G_B9_0 = 0;
	int32_t G_B9_1 = 0;
	int32_t G_B11_0 = 0;
	int32_t G_B10_0 = 0;
	int32_t G_B12_0 = 0;
	int32_t G_B12_1 = 0;
	int32_t G_B14_0 = 0;
	int32_t G_B13_0 = 0;
	int32_t G_B15_0 = 0;
	int32_t G_B15_1 = 0;
	{
		JsonData_t4263252052 * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0005;
		}
	}
	{
		return (bool)0;
	}

IL_0005:
	{
		JsonData_t4263252052 * L_1 = ___x0;
		NullCheck(L_1);
		int32_t L_2 = L_1->get_type_7();
		int32_t L_3 = __this->get_type_7();
		if ((((int32_t)L_2) == ((int32_t)L_3)))
		{
			goto IL_0073;
		}
	}
	{
		int32_t L_4 = __this->get_type_7();
		if ((((int32_t)L_4) == ((int32_t)4)))
		{
			goto IL_0027;
		}
	}
	{
		int32_t L_5 = __this->get_type_7();
		G_B6_0 = ((((int32_t)L_5) == ((int32_t)6))? 1 : 0);
		goto IL_0028;
	}

IL_0027:
	{
		G_B6_0 = 1;
	}

IL_0028:
	{
		int32_t L_6 = __this->get_type_7();
		G_B7_0 = G_B6_0;
		if ((((int32_t)L_6) == ((int32_t)5)))
		{
			G_B8_0 = G_B6_0;
			goto IL_003c;
		}
	}
	{
		int32_t L_7 = __this->get_type_7();
		G_B9_0 = ((((int32_t)L_7) == ((int32_t)7))? 1 : 0);
		G_B9_1 = G_B7_0;
		goto IL_003d;
	}

IL_003c:
	{
		G_B9_0 = 1;
		G_B9_1 = G_B8_0;
	}

IL_003d:
	{
		V_0 = (bool)G_B9_0;
		JsonData_t4263252052 * L_8 = ___x0;
		NullCheck(L_8);
		int32_t L_9 = L_8->get_type_7();
		G_B10_0 = G_B9_1;
		if ((((int32_t)L_9) == ((int32_t)4)))
		{
			G_B11_0 = G_B9_1;
			goto IL_0052;
		}
	}
	{
		JsonData_t4263252052 * L_10 = ___x0;
		NullCheck(L_10);
		int32_t L_11 = L_10->get_type_7();
		G_B12_0 = ((((int32_t)L_11) == ((int32_t)6))? 1 : 0);
		G_B12_1 = G_B10_0;
		goto IL_0053;
	}

IL_0052:
	{
		G_B12_0 = 1;
		G_B12_1 = G_B11_0;
	}

IL_0053:
	{
		V_1 = (bool)G_B12_0;
		JsonData_t4263252052 * L_12 = ___x0;
		NullCheck(L_12);
		int32_t L_13 = L_12->get_type_7();
		G_B13_0 = G_B12_1;
		if ((((int32_t)L_13) == ((int32_t)5)))
		{
			G_B14_0 = G_B12_1;
			goto IL_0068;
		}
	}
	{
		JsonData_t4263252052 * L_14 = ___x0;
		NullCheck(L_14);
		int32_t L_15 = L_14->get_type_7();
		G_B15_0 = ((((int32_t)L_15) == ((int32_t)7))? 1 : 0);
		G_B15_1 = G_B13_0;
		goto IL_0069;
	}

IL_0068:
	{
		G_B15_0 = 1;
		G_B15_1 = G_B14_0;
	}

IL_0069:
	{
		V_2 = (bool)G_B15_0;
		bool L_16 = V_1;
		if ((((int32_t)G_B15_1) == ((int32_t)L_16)))
		{
			goto IL_0073;
		}
	}
	{
		bool L_17 = V_0;
		bool L_18 = V_2;
		if ((((int32_t)L_17) == ((int32_t)L_18)))
		{
			goto IL_0073;
		}
	}
	{
		return (bool)0;
	}

IL_0073:
	{
		int32_t L_19 = __this->get_type_7();
		V_3 = L_19;
		int32_t L_20 = V_3;
		if (L_20 == 0)
		{
			goto IL_00aa;
		}
		if (L_20 == 1)
		{
			goto IL_00ac;
		}
		if (L_20 == 2)
		{
			goto IL_00be;
		}
		if (L_20 == 3)
		{
			goto IL_00d0;
		}
		if (L_20 == 4)
		{
			goto IL_00e2;
		}
		if (L_20 == 5)
		{
			goto IL_00e2;
		}
		if (L_20 == 6)
		{
			goto IL_00e2;
		}
		if (L_20 == 7)
		{
			goto IL_00e2;
		}
		if (L_20 == 8)
		{
			goto IL_00f4;
		}
		if (L_20 == 9)
		{
			goto IL_0106;
		}
	}
	{
		goto IL_0118;
	}

IL_00aa:
	{
		return (bool)1;
	}

IL_00ac:
	{
		Il2CppObject* L_21 = __this->get_inst_object_4();
		JsonData_t4263252052 * L_22 = ___x0;
		NullCheck(L_22);
		Il2CppObject* L_23 = L_22->get_inst_object_4();
		NullCheck(L_21);
		bool L_24 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, L_21, L_23);
		return L_24;
	}

IL_00be:
	{
		Il2CppObject* L_25 = __this->get_inst_array_0();
		JsonData_t4263252052 * L_26 = ___x0;
		NullCheck(L_26);
		Il2CppObject* L_27 = L_26->get_inst_array_0();
		NullCheck(L_25);
		bool L_28 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, L_25, L_27);
		return L_28;
	}

IL_00d0:
	{
		String_t* L_29 = __this->get_inst_string_5();
		JsonData_t4263252052 * L_30 = ___x0;
		NullCheck(L_30);
		String_t* L_31 = L_30->get_inst_string_5();
		NullCheck(L_29);
		bool L_32 = String_Equals_m2633592423(L_29, L_31, /*hidden argument*/NULL);
		return L_32;
	}

IL_00e2:
	{
		uint64_t* L_33 = __this->get_address_of_inst_number_3();
		JsonData_t4263252052 * L_34 = ___x0;
		NullCheck(L_34);
		uint64_t L_35 = L_34->get_inst_number_3();
		bool L_36 = UInt64_Equals_m1977009287(L_33, L_35, /*hidden argument*/NULL);
		return L_36;
	}

IL_00f4:
	{
		double* L_37 = __this->get_address_of_inst_double_2();
		JsonData_t4263252052 * L_38 = ___x0;
		NullCheck(L_38);
		double L_39 = L_38->get_inst_double_2();
		bool L_40 = Double_Equals_m920556135(L_37, L_39, /*hidden argument*/NULL);
		return L_40;
	}

IL_0106:
	{
		bool* L_41 = __this->get_address_of_inst_boolean_1();
		JsonData_t4263252052 * L_42 = ___x0;
		NullCheck(L_42);
		bool L_43 = L_42->get_inst_boolean_1();
		bool L_44 = Boolean_Equals_m294106711(L_41, L_43, /*hidden argument*/NULL);
		return L_44;
	}

IL_0118:
	{
		return (bool)0;
	}
}
// System.Void ThirdParty.Json.LitJson.JsonData::SetJsonType(ThirdParty.Json.LitJson.JsonType)
extern Il2CppClass* Dictionary_2_t1883064018_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t3304497668_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t3632373184_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m3262415100_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m66208350_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m1935663359_MethodInfo_var;
extern const uint32_t JsonData_SetJsonType_m3927776733_MetadataUsageId;
extern "C"  void JsonData_SetJsonType_m3927776733 (JsonData_t4263252052 * __this, int32_t ___type0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonData_SetJsonType_m3927776733_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_type_7();
		int32_t L_1 = ___type0;
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_000a;
		}
	}
	{
		return;
	}

IL_000a:
	{
		int32_t L_2 = ___type0;
		if (L_2 == 0)
		{
			goto IL_00a8;
		}
		if (L_2 == 1)
		{
			goto IL_003a;
		}
		if (L_2 == 2)
		{
			goto IL_0052;
		}
		if (L_2 == 3)
		{
			goto IL_005f;
		}
		if (L_2 == 4)
		{
			goto IL_0068;
		}
		if (L_2 == 5)
		{
			goto IL_0072;
		}
		if (L_2 == 6)
		{
			goto IL_007c;
		}
		if (L_2 == 7)
		{
			goto IL_0086;
		}
		if (L_2 == 8)
		{
			goto IL_0090;
		}
		if (L_2 == 9)
		{
			goto IL_00a1;
		}
	}
	{
		goto IL_00a8;
	}

IL_003a:
	{
		Dictionary_2_t1883064018 * L_3 = (Dictionary_2_t1883064018 *)il2cpp_codegen_object_new(Dictionary_2_t1883064018_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m3262415100(L_3, /*hidden argument*/Dictionary_2__ctor_m3262415100_MethodInfo_var);
		__this->set_inst_object_4(L_3);
		List_1_t3304497668 * L_4 = (List_1_t3304497668 *)il2cpp_codegen_object_new(List_1_t3304497668_il2cpp_TypeInfo_var);
		List_1__ctor_m66208350(L_4, /*hidden argument*/List_1__ctor_m66208350_MethodInfo_var);
		__this->set_object_list_8(L_4);
		goto IL_00a8;
	}

IL_0052:
	{
		List_1_t3632373184 * L_5 = (List_1_t3632373184 *)il2cpp_codegen_object_new(List_1_t3632373184_il2cpp_TypeInfo_var);
		List_1__ctor_m1935663359(L_5, /*hidden argument*/List_1__ctor_m1935663359_MethodInfo_var);
		__this->set_inst_array_0(L_5);
		goto IL_00a8;
	}

IL_005f:
	{
		__this->set_inst_string_5((String_t*)NULL);
		goto IL_00a8;
	}

IL_0068:
	{
		__this->set_inst_number_3((((int64_t)((int64_t)0))));
		goto IL_00a8;
	}

IL_0072:
	{
		__this->set_inst_number_3((((int64_t)((int64_t)0))));
		goto IL_00a8;
	}

IL_007c:
	{
		__this->set_inst_number_3((((int64_t)((int64_t)0))));
		goto IL_00a8;
	}

IL_0086:
	{
		__this->set_inst_number_3((((int64_t)((int64_t)0))));
		goto IL_00a8;
	}

IL_0090:
	{
		__this->set_inst_double_2((0.0));
		goto IL_00a8;
	}

IL_00a1:
	{
		__this->set_inst_boolean_1((bool)0);
	}

IL_00a8:
	{
		int32_t L_6 = ___type0;
		__this->set_type_7(L_6);
		return;
	}
}
// System.String ThirdParty.Json.LitJson.JsonData::ToString()
extern Il2CppCodeGenString* _stringLiteral2824362461;
extern Il2CppCodeGenString* _stringLiteral2360174553;
extern Il2CppCodeGenString* _stringLiteral3964593059;
extern const uint32_t JsonData_ToString_m3098875818_MetadataUsageId;
extern "C"  String_t* JsonData_ToString_m3098875818 (JsonData_t4263252052 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonData_ToString_m3098875818_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	uint32_t V_2 = 0;
	int64_t V_3 = 0;
	{
		int32_t L_0 = __this->get_type_7();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 0)
		{
			goto IL_008e;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 1)
		{
			goto IL_0035;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 2)
		{
			goto IL_0094;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 3)
		{
			goto IL_0053;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 4)
		{
			goto IL_0063;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 5)
		{
			goto IL_0073;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 6)
		{
			goto IL_0082;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 7)
		{
			goto IL_0047;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 8)
		{
			goto IL_003b;
		}
	}
	{
		goto IL_009b;
	}

IL_0035:
	{
		return _stringLiteral2824362461;
	}

IL_003b:
	{
		bool* L_2 = __this->get_address_of_inst_boolean_1();
		String_t* L_3 = Boolean_ToString_m1253164328(L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0047:
	{
		double* L_4 = __this->get_address_of_inst_double_2();
		String_t* L_5 = Double_ToString_m1864290157(L_4, /*hidden argument*/NULL);
		return L_5;
	}

IL_0053:
	{
		uint64_t L_6 = __this->get_inst_number_3();
		V_1 = (((int32_t)((int32_t)L_6)));
		String_t* L_7 = Int32_ToString_m2960866144((&V_1), /*hidden argument*/NULL);
		return L_7;
	}

IL_0063:
	{
		uint64_t L_8 = __this->get_inst_number_3();
		V_2 = (((int32_t)((uint32_t)L_8)));
		String_t* L_9 = UInt32_ToString_m554020223((&V_2), /*hidden argument*/NULL);
		return L_9;
	}

IL_0073:
	{
		uint64_t L_10 = __this->get_inst_number_3();
		V_3 = L_10;
		String_t* L_11 = Int64_ToString_m689375889((&V_3), /*hidden argument*/NULL);
		return L_11;
	}

IL_0082:
	{
		uint64_t* L_12 = __this->get_address_of_inst_number_3();
		String_t* L_13 = UInt64_ToString_m446228920(L_12, /*hidden argument*/NULL);
		return L_13;
	}

IL_008e:
	{
		return _stringLiteral2360174553;
	}

IL_0094:
	{
		String_t* L_14 = __this->get_inst_string_5();
		return L_14;
	}

IL_009b:
	{
		return _stringLiteral3964593059;
	}
}
// System.Void ThirdParty.Json.LitJson.JsonException::.ctor()
extern "C"  void JsonException__ctor_m2064664612 (JsonException_t1457213491 * __this, const MethodInfo* method)
{
	{
		Exception__ctor_m3886110570(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ThirdParty.Json.LitJson.JsonException::.ctor(System.Int32)
extern Il2CppClass* Char_t3454481338_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1896442200;
extern const uint32_t JsonException__ctor_m1345348707_MetadataUsageId;
extern "C"  void JsonException__ctor_m1345348707 (JsonException_t1457213491 * __this, int32_t ___c0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonException__ctor_m1345348707_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___c0;
		Il2CppChar L_1 = ((Il2CppChar)(((int32_t)((uint16_t)L_0))));
		Il2CppObject * L_2 = Box(Char_t3454481338_il2cpp_TypeInfo_var, &L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral1896442200, L_2, /*hidden argument*/NULL);
		Exception__ctor_m485833136(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ThirdParty.Json.LitJson.JsonException::.ctor(System.String)
extern "C"  void JsonException__ctor_m1029591446 (JsonException_t1457213491 * __this, String_t* ___message0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		Exception__ctor_m485833136(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ThirdParty.Json.LitJson.JsonMapper::.cctor()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonMapper_t77474459_il2cpp_TypeInfo_var;
extern Il2CppClass* StringComparer_t1574862926_il2cpp_TypeInfo_var;
extern Il2CppClass* HashSet_1_t362681087_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t3072435911_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t909378256_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t1700528341_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t1471071188_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonWriter_t3014444111_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTimeFormatInfo_t2187473504_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t2110623306_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t2724486493_il2cpp_TypeInfo_var;
extern const MethodInfo* HashSet_1__ctor_m136758834_MethodInfo_var;
extern const MethodInfo* HashSet_1_Add_m563345073_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m2666705261_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1033535593_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m2017035269_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m381355744_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m2649880954_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1426988876_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3743961169;
extern Il2CppCodeGenString* _stringLiteral1554779067;
extern Il2CppCodeGenString* _stringLiteral1857678454;
extern Il2CppCodeGenString* _stringLiteral1188955108;
extern const uint32_t JsonMapper__cctor_m1834007045_MetadataUsageId;
extern "C"  void JsonMapper__cctor_m1834007045 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper__cctor_m1834007045_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m2551263788(L_0, /*hidden argument*/NULL);
		((JsonMapper_t77474459_StaticFields*)JsonMapper_t77474459_il2cpp_TypeInfo_var->static_fields)->set_array_metadata_lock_7(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m2551263788(L_1, /*hidden argument*/NULL);
		((JsonMapper_t77474459_StaticFields*)JsonMapper_t77474459_il2cpp_TypeInfo_var->static_fields)->set_conv_ops_lock_9(L_1);
		Il2CppObject * L_2 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m2551263788(L_2, /*hidden argument*/NULL);
		((JsonMapper_t77474459_StaticFields*)JsonMapper_t77474459_il2cpp_TypeInfo_var->static_fields)->set_object_metadata_lock_11(L_2);
		Il2CppObject * L_3 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m2551263788(L_3, /*hidden argument*/NULL);
		((JsonMapper_t77474459_StaticFields*)JsonMapper_t77474459_il2cpp_TypeInfo_var->static_fields)->set_type_properties_lock_13(L_3);
		Il2CppObject * L_4 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m2551263788(L_4, /*hidden argument*/NULL);
		((JsonMapper_t77474459_StaticFields*)JsonMapper_t77474459_il2cpp_TypeInfo_var->static_fields)->set_static_writer_lock_15(L_4);
		IL2CPP_RUNTIME_CLASS_INIT(StringComparer_t1574862926_il2cpp_TypeInfo_var);
		StringComparer_t1574862926 * L_5 = StringComparer_get_Ordinal_m3140767557(NULL /*static, unused*/, /*hidden argument*/NULL);
		HashSet_1_t362681087 * L_6 = (HashSet_1_t362681087 *)il2cpp_codegen_object_new(HashSet_1_t362681087_il2cpp_TypeInfo_var);
		HashSet_1__ctor_m136758834(L_6, L_5, /*hidden argument*/HashSet_1__ctor_m136758834_MethodInfo_var);
		HashSet_1_t362681087 * L_7 = L_6;
		NullCheck(L_7);
		HashSet_1_Add_m563345073(L_7, _stringLiteral3743961169, /*hidden argument*/HashSet_1_Add_m563345073_MethodInfo_var);
		HashSet_1_t362681087 * L_8 = L_7;
		NullCheck(L_8);
		HashSet_1_Add_m563345073(L_8, _stringLiteral1554779067, /*hidden argument*/HashSet_1_Add_m563345073_MethodInfo_var);
		HashSet_1_t362681087 * L_9 = L_8;
		NullCheck(L_9);
		HashSet_1_Add_m563345073(L_9, _stringLiteral1857678454, /*hidden argument*/HashSet_1_Add_m563345073_MethodInfo_var);
		HashSet_1_t362681087 * L_10 = L_9;
		NullCheck(L_10);
		HashSet_1_Add_m563345073(L_10, _stringLiteral1188955108, /*hidden argument*/HashSet_1_Add_m563345073_MethodInfo_var);
		((JsonMapper_t77474459_StaticFields*)JsonMapper_t77474459_il2cpp_TypeInfo_var->static_fields)->set_dictionary_properties_to_ignore_16(L_10);
		((JsonMapper_t77474459_StaticFields*)JsonMapper_t77474459_il2cpp_TypeInfo_var->static_fields)->set_max_nesting_depth_0(((int32_t)100));
		Dictionary_2_t3072435911 * L_11 = (Dictionary_2_t3072435911 *)il2cpp_codegen_object_new(Dictionary_2_t3072435911_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2666705261(L_11, /*hidden argument*/Dictionary_2__ctor_m2666705261_MethodInfo_var);
		((JsonMapper_t77474459_StaticFields*)JsonMapper_t77474459_il2cpp_TypeInfo_var->static_fields)->set_array_metadata_6(L_11);
		Dictionary_2_t909378256 * L_12 = (Dictionary_2_t909378256 *)il2cpp_codegen_object_new(Dictionary_2_t909378256_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1033535593(L_12, /*hidden argument*/Dictionary_2__ctor_m1033535593_MethodInfo_var);
		((JsonMapper_t77474459_StaticFields*)JsonMapper_t77474459_il2cpp_TypeInfo_var->static_fields)->set_conv_ops_8(L_12);
		Dictionary_2_t1700528341 * L_13 = (Dictionary_2_t1700528341 *)il2cpp_codegen_object_new(Dictionary_2_t1700528341_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2017035269(L_13, /*hidden argument*/Dictionary_2__ctor_m2017035269_MethodInfo_var);
		((JsonMapper_t77474459_StaticFields*)JsonMapper_t77474459_il2cpp_TypeInfo_var->static_fields)->set_object_metadata_10(L_13);
		Dictionary_2_t1471071188 * L_14 = (Dictionary_2_t1471071188 *)il2cpp_codegen_object_new(Dictionary_2_t1471071188_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m381355744(L_14, /*hidden argument*/Dictionary_2__ctor_m381355744_MethodInfo_var);
		((JsonMapper_t77474459_StaticFields*)JsonMapper_t77474459_il2cpp_TypeInfo_var->static_fields)->set_type_properties_12(L_14);
		JsonWriter_t3014444111 * L_15 = (JsonWriter_t3014444111 *)il2cpp_codegen_object_new(JsonWriter_t3014444111_il2cpp_TypeInfo_var);
		JsonWriter__ctor_m1529769822(L_15, /*hidden argument*/NULL);
		((JsonMapper_t77474459_StaticFields*)JsonMapper_t77474459_il2cpp_TypeInfo_var->static_fields)->set_static_writer_14(L_15);
		IL2CPP_RUNTIME_CLASS_INIT(DateTimeFormatInfo_t2187473504_il2cpp_TypeInfo_var);
		DateTimeFormatInfo_t2187473504 * L_16 = DateTimeFormatInfo_get_InvariantInfo_m1865598692(NULL /*static, unused*/, /*hidden argument*/NULL);
		((JsonMapper_t77474459_StaticFields*)JsonMapper_t77474459_il2cpp_TypeInfo_var->static_fields)->set_datetime_format_1(L_16);
		Dictionary_2_t2110623306 * L_17 = (Dictionary_2_t2110623306 *)il2cpp_codegen_object_new(Dictionary_2_t2110623306_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2649880954(L_17, /*hidden argument*/Dictionary_2__ctor_m2649880954_MethodInfo_var);
		((JsonMapper_t77474459_StaticFields*)JsonMapper_t77474459_il2cpp_TypeInfo_var->static_fields)->set_base_exporters_table_2(L_17);
		Dictionary_2_t2110623306 * L_18 = (Dictionary_2_t2110623306 *)il2cpp_codegen_object_new(Dictionary_2_t2110623306_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2649880954(L_18, /*hidden argument*/Dictionary_2__ctor_m2649880954_MethodInfo_var);
		((JsonMapper_t77474459_StaticFields*)JsonMapper_t77474459_il2cpp_TypeInfo_var->static_fields)->set_custom_exporters_table_3(L_18);
		Dictionary_2_t2724486493 * L_19 = (Dictionary_2_t2724486493 *)il2cpp_codegen_object_new(Dictionary_2_t2724486493_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1426988876(L_19, /*hidden argument*/Dictionary_2__ctor_m1426988876_MethodInfo_var);
		((JsonMapper_t77474459_StaticFields*)JsonMapper_t77474459_il2cpp_TypeInfo_var->static_fields)->set_base_importers_table_4(L_19);
		Dictionary_2_t2724486493 * L_20 = (Dictionary_2_t2724486493 *)il2cpp_codegen_object_new(Dictionary_2_t2724486493_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1426988876(L_20, /*hidden argument*/Dictionary_2__ctor_m1426988876_MethodInfo_var);
		((JsonMapper_t77474459_StaticFields*)JsonMapper_t77474459_il2cpp_TypeInfo_var->static_fields)->set_custom_importers_table_5(L_20);
		JsonMapper_RegisterBaseExporters_m2987482258(NULL /*static, unused*/, /*hidden argument*/NULL);
		JsonMapper_RegisterBaseImporters_m2686124695(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// ThirdParty.Json.LitJson.IJsonWrapper ThirdParty.Json.LitJson.JsonMapper::ReadValue(ThirdParty.Json.LitJson.WrapperFactory,ThirdParty.Json.LitJson.JsonReader)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* IJsonWrapper_t3095378610_il2cpp_TypeInfo_var;
extern Il2CppClass* Double_t4078015681_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt32_t2149682021_il2cpp_TypeInfo_var;
extern Il2CppClass* Int64_t909078037_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt64_t2909196914_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonMapper_t77474459_il2cpp_TypeInfo_var;
extern Il2CppClass* IList_t3321498491_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_t596158605_il2cpp_TypeInfo_var;
extern const uint32_t JsonMapper_ReadValue_m2812052028_MetadataUsageId;
extern "C"  Il2CppObject * JsonMapper_ReadValue_m2812052028 (Il2CppObject * __this /* static, unused */, WrapperFactory_t327905379 * ___factory0, JsonReader_t354941621 * ___reader1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_ReadValue_m2812052028_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	String_t* V_2 = NULL;
	{
		JsonReader_t354941621 * L_0 = ___reader1;
		NullCheck(L_0);
		JsonReader_Read_m3114967242(L_0, /*hidden argument*/NULL);
		JsonReader_t354941621 * L_1 = ___reader1;
		NullCheck(L_1);
		int32_t L_2 = JsonReader_get_Token_m2164259962(L_1, /*hidden argument*/NULL);
		if ((((int32_t)L_2) == ((int32_t)5)))
		{
			goto IL_001a;
		}
	}
	{
		JsonReader_t354941621 * L_3 = ___reader1;
		NullCheck(L_3);
		int32_t L_4 = JsonReader_get_Token_m2164259962(L_3, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_4) == ((uint32_t)((int32_t)13)))))
		{
			goto IL_001c;
		}
	}

IL_001a:
	{
		return (Il2CppObject *)NULL;
	}

IL_001c:
	{
		WrapperFactory_t327905379 * L_5 = ___factory0;
		NullCheck(L_5);
		Il2CppObject * L_6 = WrapperFactory_Invoke_m3781398413(L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		JsonReader_t354941621 * L_7 = ___reader1;
		NullCheck(L_7);
		int32_t L_8 = JsonReader_get_Token_m2164259962(L_7, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_8) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_0040;
		}
	}
	{
		Il2CppObject * L_9 = V_0;
		JsonReader_t354941621 * L_10 = ___reader1;
		NullCheck(L_10);
		Il2CppObject * L_11 = JsonReader_get_Value_m158192595(L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		InterfaceActionInvoker1< String_t* >::Invoke(7 /* System.Void ThirdParty.Json.LitJson.IJsonWrapper::SetString(System.String) */, IJsonWrapper_t3095378610_il2cpp_TypeInfo_var, L_9, ((String_t*)CastclassSealed(L_11, String_t_il2cpp_TypeInfo_var)));
		Il2CppObject * L_12 = V_0;
		return L_12;
	}

IL_0040:
	{
		JsonReader_t354941621 * L_13 = ___reader1;
		NullCheck(L_13);
		int32_t L_14 = JsonReader_get_Token_m2164259962(L_13, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_14) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_005d;
		}
	}
	{
		Il2CppObject * L_15 = V_0;
		JsonReader_t354941621 * L_16 = ___reader1;
		NullCheck(L_16);
		Il2CppObject * L_17 = JsonReader_get_Value_m158192595(L_16, /*hidden argument*/NULL);
		NullCheck(L_15);
		InterfaceActionInvoker1< double >::Invoke(1 /* System.Void ThirdParty.Json.LitJson.IJsonWrapper::SetDouble(System.Double) */, IJsonWrapper_t3095378610_il2cpp_TypeInfo_var, L_15, ((*(double*)((double*)UnBox (L_17, Double_t4078015681_il2cpp_TypeInfo_var)))));
		Il2CppObject * L_18 = V_0;
		return L_18;
	}

IL_005d:
	{
		JsonReader_t354941621 * L_19 = ___reader1;
		NullCheck(L_19);
		int32_t L_20 = JsonReader_get_Token_m2164259962(L_19, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_20) == ((uint32_t)6))))
		{
			goto IL_0079;
		}
	}
	{
		Il2CppObject * L_21 = V_0;
		JsonReader_t354941621 * L_22 = ___reader1;
		NullCheck(L_22);
		Il2CppObject * L_23 = JsonReader_get_Value_m158192595(L_22, /*hidden argument*/NULL);
		NullCheck(L_21);
		InterfaceActionInvoker1< int32_t >::Invoke(2 /* System.Void ThirdParty.Json.LitJson.IJsonWrapper::SetInt(System.Int32) */, IJsonWrapper_t3095378610_il2cpp_TypeInfo_var, L_21, ((*(int32_t*)((int32_t*)UnBox (L_23, Int32_t2071877448_il2cpp_TypeInfo_var)))));
		Il2CppObject * L_24 = V_0;
		return L_24;
	}

IL_0079:
	{
		JsonReader_t354941621 * L_25 = ___reader1;
		NullCheck(L_25);
		int32_t L_26 = JsonReader_get_Token_m2164259962(L_25, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_26) == ((uint32_t)7))))
		{
			goto IL_0095;
		}
	}
	{
		Il2CppObject * L_27 = V_0;
		JsonReader_t354941621 * L_28 = ___reader1;
		NullCheck(L_28);
		Il2CppObject * L_29 = JsonReader_get_Value_m158192595(L_28, /*hidden argument*/NULL);
		NullCheck(L_27);
		InterfaceActionInvoker1< uint32_t >::Invoke(3 /* System.Void ThirdParty.Json.LitJson.IJsonWrapper::SetUInt(System.UInt32) */, IJsonWrapper_t3095378610_il2cpp_TypeInfo_var, L_27, ((*(uint32_t*)((uint32_t*)UnBox (L_29, UInt32_t2149682021_il2cpp_TypeInfo_var)))));
		Il2CppObject * L_30 = V_0;
		return L_30;
	}

IL_0095:
	{
		JsonReader_t354941621 * L_31 = ___reader1;
		NullCheck(L_31);
		int32_t L_32 = JsonReader_get_Token_m2164259962(L_31, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_32) == ((uint32_t)8))))
		{
			goto IL_00b1;
		}
	}
	{
		Il2CppObject * L_33 = V_0;
		JsonReader_t354941621 * L_34 = ___reader1;
		NullCheck(L_34);
		Il2CppObject * L_35 = JsonReader_get_Value_m158192595(L_34, /*hidden argument*/NULL);
		NullCheck(L_33);
		InterfaceActionInvoker1< int64_t >::Invoke(5 /* System.Void ThirdParty.Json.LitJson.IJsonWrapper::SetLong(System.Int64) */, IJsonWrapper_t3095378610_il2cpp_TypeInfo_var, L_33, ((*(int64_t*)((int64_t*)UnBox (L_35, Int64_t909078037_il2cpp_TypeInfo_var)))));
		Il2CppObject * L_36 = V_0;
		return L_36;
	}

IL_00b1:
	{
		JsonReader_t354941621 * L_37 = ___reader1;
		NullCheck(L_37);
		int32_t L_38 = JsonReader_get_Token_m2164259962(L_37, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_38) == ((uint32_t)((int32_t)9)))))
		{
			goto IL_00ce;
		}
	}
	{
		Il2CppObject * L_39 = V_0;
		JsonReader_t354941621 * L_40 = ___reader1;
		NullCheck(L_40);
		Il2CppObject * L_41 = JsonReader_get_Value_m158192595(L_40, /*hidden argument*/NULL);
		NullCheck(L_39);
		InterfaceActionInvoker1< uint64_t >::Invoke(6 /* System.Void ThirdParty.Json.LitJson.IJsonWrapper::SetULong(System.UInt64) */, IJsonWrapper_t3095378610_il2cpp_TypeInfo_var, L_39, ((*(uint64_t*)((uint64_t*)UnBox (L_41, UInt64_t2909196914_il2cpp_TypeInfo_var)))));
		Il2CppObject * L_42 = V_0;
		return L_42;
	}

IL_00ce:
	{
		JsonReader_t354941621 * L_43 = ___reader1;
		NullCheck(L_43);
		int32_t L_44 = JsonReader_get_Token_m2164259962(L_43, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_44) == ((uint32_t)((int32_t)12)))))
		{
			goto IL_00eb;
		}
	}
	{
		Il2CppObject * L_45 = V_0;
		JsonReader_t354941621 * L_46 = ___reader1;
		NullCheck(L_46);
		Il2CppObject * L_47 = JsonReader_get_Value_m158192595(L_46, /*hidden argument*/NULL);
		NullCheck(L_45);
		InterfaceActionInvoker1< bool >::Invoke(0 /* System.Void ThirdParty.Json.LitJson.IJsonWrapper::SetBoolean(System.Boolean) */, IJsonWrapper_t3095378610_il2cpp_TypeInfo_var, L_45, ((*(bool*)((bool*)UnBox (L_47, Boolean_t3825574718_il2cpp_TypeInfo_var)))));
		Il2CppObject * L_48 = V_0;
		return L_48;
	}

IL_00eb:
	{
		JsonReader_t354941621 * L_49 = ___reader1;
		NullCheck(L_49);
		int32_t L_50 = JsonReader_get_Token_m2164259962(L_49, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_50) == ((uint32_t)4))))
		{
			goto IL_0119;
		}
	}
	{
		Il2CppObject * L_51 = V_0;
		NullCheck(L_51);
		InterfaceActionInvoker1< int32_t >::Invoke(4 /* System.Void ThirdParty.Json.LitJson.IJsonWrapper::SetJsonType(ThirdParty.Json.LitJson.JsonType) */, IJsonWrapper_t3095378610_il2cpp_TypeInfo_var, L_51, 2);
	}

IL_00fb:
	{
		WrapperFactory_t327905379 * L_52 = ___factory0;
		JsonReader_t354941621 * L_53 = ___reader1;
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t77474459_il2cpp_TypeInfo_var);
		Il2CppObject * L_54 = JsonMapper_ReadValue_m2812052028(NULL /*static, unused*/, L_52, L_53, /*hidden argument*/NULL);
		V_1 = L_54;
		Il2CppObject * L_55 = V_1;
		if (L_55)
		{
			goto IL_010f;
		}
	}
	{
		JsonReader_t354941621 * L_56 = ___reader1;
		NullCheck(L_56);
		int32_t L_57 = JsonReader_get_Token_m2164259962(L_56, /*hidden argument*/NULL);
		if ((((int32_t)L_57) == ((int32_t)5)))
		{
			goto IL_0155;
		}
	}

IL_010f:
	{
		Il2CppObject * L_58 = V_0;
		Il2CppObject * L_59 = V_1;
		NullCheck(L_58);
		InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(4 /* System.Int32 System.Collections.IList::Add(System.Object) */, IList_t3321498491_il2cpp_TypeInfo_var, L_58, L_59);
		goto IL_00fb;
	}

IL_0119:
	{
		JsonReader_t354941621 * L_60 = ___reader1;
		NullCheck(L_60);
		int32_t L_61 = JsonReader_get_Token_m2164259962(L_60, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_61) == ((uint32_t)1))))
		{
			goto IL_0155;
		}
	}
	{
		Il2CppObject * L_62 = V_0;
		NullCheck(L_62);
		InterfaceActionInvoker1< int32_t >::Invoke(4 /* System.Void ThirdParty.Json.LitJson.IJsonWrapper::SetJsonType(ThirdParty.Json.LitJson.JsonType) */, IJsonWrapper_t3095378610_il2cpp_TypeInfo_var, L_62, 1);
	}

IL_0129:
	{
		JsonReader_t354941621 * L_63 = ___reader1;
		NullCheck(L_63);
		JsonReader_Read_m3114967242(L_63, /*hidden argument*/NULL);
		JsonReader_t354941621 * L_64 = ___reader1;
		NullCheck(L_64);
		int32_t L_65 = JsonReader_get_Token_m2164259962(L_64, /*hidden argument*/NULL);
		if ((((int32_t)L_65) == ((int32_t)3)))
		{
			goto IL_0155;
		}
	}
	{
		JsonReader_t354941621 * L_66 = ___reader1;
		NullCheck(L_66);
		Il2CppObject * L_67 = JsonReader_get_Value_m158192595(L_66, /*hidden argument*/NULL);
		V_2 = ((String_t*)CastclassSealed(L_67, String_t_il2cpp_TypeInfo_var));
		Il2CppObject * L_68 = V_0;
		String_t* L_69 = V_2;
		WrapperFactory_t327905379 * L_70 = ___factory0;
		JsonReader_t354941621 * L_71 = ___reader1;
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t77474459_il2cpp_TypeInfo_var);
		Il2CppObject * L_72 = JsonMapper_ReadValue_m2812052028(NULL /*static, unused*/, L_70, L_71, /*hidden argument*/NULL);
		NullCheck(L_68);
		InterfaceActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(1 /* System.Void System.Collections.IDictionary::set_Item(System.Object,System.Object) */, IDictionary_t596158605_il2cpp_TypeInfo_var, L_68, L_69, L_72);
		goto IL_0129;
	}

IL_0155:
	{
		Il2CppObject * L_73 = V_0;
		return L_73;
	}
}
// System.Void ThirdParty.Json.LitJson.JsonMapper::RegisterBaseExporters()
extern const Il2CppType* Byte_t3683104436_0_0_0_var;
extern const Il2CppType* Char_t3454481338_0_0_0_var;
extern const Il2CppType* DateTime_t693205669_0_0_0_var;
extern const Il2CppType* Decimal_t724701077_0_0_0_var;
extern const Il2CppType* SByte_t454417549_0_0_0_var;
extern const Il2CppType* Int16_t4041245914_0_0_0_var;
extern const Il2CppType* UInt16_t986882611_0_0_0_var;
extern const Il2CppType* UInt32_t2149682021_0_0_0_var;
extern const Il2CppType* UInt64_t2909196914_0_0_0_var;
extern const Il2CppType* Single_t2076509932_0_0_0_var;
extern const Il2CppType* Int64_t909078037_0_0_0_var;
extern Il2CppClass* JsonMapper_t77474459_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* U3CU3Ec_t1591091833_il2cpp_TypeInfo_var;
extern Il2CppClass* ExporterFunc_t173265409_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_2_t109706727_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CU3Ec_U3CRegisterBaseExportersU3Eb__24_0_m3647892888_MethodInfo_var;
extern const MethodInfo* U3CU3Ec_U3CRegisterBaseExportersU3Eb__24_1_m403640019_MethodInfo_var;
extern const MethodInfo* U3CU3Ec_U3CRegisterBaseExportersU3Eb__24_2_m626278178_MethodInfo_var;
extern const MethodInfo* U3CU3Ec_U3CRegisterBaseExportersU3Eb__24_3_m564382813_MethodInfo_var;
extern const MethodInfo* U3CU3Ec_U3CRegisterBaseExportersU3Eb__24_4_m3305872516_MethodInfo_var;
extern const MethodInfo* U3CU3Ec_U3CRegisterBaseExportersU3Eb__24_5_m61619647_MethodInfo_var;
extern const MethodInfo* U3CU3Ec_U3CRegisterBaseExportersU3Eb__24_6_m284257806_MethodInfo_var;
extern const MethodInfo* U3CU3Ec_U3CRegisterBaseExportersU3Eb__24_7_m222362441_MethodInfo_var;
extern const MethodInfo* U3CU3Ec_U3CRegisterBaseExportersU3Eb__24_8_m931111872_MethodInfo_var;
extern const MethodInfo* U3CU3Ec_U3CRegisterBaseExportersU3Eb__24_9_m1981826299_MethodInfo_var;
extern const MethodInfo* U3CU3Ec_U3CRegisterBaseExportersU3Eb__24_10_m3919046323_MethodInfo_var;
extern const uint32_t JsonMapper_RegisterBaseExporters_m2987482258_MetadataUsageId;
extern "C"  void JsonMapper_RegisterBaseExporters_m2987482258 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_RegisterBaseExporters_m2987482258_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ExporterFunc_t173265409 * G_B2_0 = NULL;
	Type_t * G_B2_1 = NULL;
	Il2CppObject* G_B2_2 = NULL;
	ExporterFunc_t173265409 * G_B1_0 = NULL;
	Type_t * G_B1_1 = NULL;
	Il2CppObject* G_B1_2 = NULL;
	ExporterFunc_t173265409 * G_B4_0 = NULL;
	Type_t * G_B4_1 = NULL;
	Il2CppObject* G_B4_2 = NULL;
	ExporterFunc_t173265409 * G_B3_0 = NULL;
	Type_t * G_B3_1 = NULL;
	Il2CppObject* G_B3_2 = NULL;
	ExporterFunc_t173265409 * G_B6_0 = NULL;
	Type_t * G_B6_1 = NULL;
	Il2CppObject* G_B6_2 = NULL;
	ExporterFunc_t173265409 * G_B5_0 = NULL;
	Type_t * G_B5_1 = NULL;
	Il2CppObject* G_B5_2 = NULL;
	ExporterFunc_t173265409 * G_B8_0 = NULL;
	Type_t * G_B8_1 = NULL;
	Il2CppObject* G_B8_2 = NULL;
	ExporterFunc_t173265409 * G_B7_0 = NULL;
	Type_t * G_B7_1 = NULL;
	Il2CppObject* G_B7_2 = NULL;
	ExporterFunc_t173265409 * G_B10_0 = NULL;
	Type_t * G_B10_1 = NULL;
	Il2CppObject* G_B10_2 = NULL;
	ExporterFunc_t173265409 * G_B9_0 = NULL;
	Type_t * G_B9_1 = NULL;
	Il2CppObject* G_B9_2 = NULL;
	ExporterFunc_t173265409 * G_B12_0 = NULL;
	Type_t * G_B12_1 = NULL;
	Il2CppObject* G_B12_2 = NULL;
	ExporterFunc_t173265409 * G_B11_0 = NULL;
	Type_t * G_B11_1 = NULL;
	Il2CppObject* G_B11_2 = NULL;
	ExporterFunc_t173265409 * G_B14_0 = NULL;
	Type_t * G_B14_1 = NULL;
	Il2CppObject* G_B14_2 = NULL;
	ExporterFunc_t173265409 * G_B13_0 = NULL;
	Type_t * G_B13_1 = NULL;
	Il2CppObject* G_B13_2 = NULL;
	ExporterFunc_t173265409 * G_B16_0 = NULL;
	Type_t * G_B16_1 = NULL;
	Il2CppObject* G_B16_2 = NULL;
	ExporterFunc_t173265409 * G_B15_0 = NULL;
	Type_t * G_B15_1 = NULL;
	Il2CppObject* G_B15_2 = NULL;
	ExporterFunc_t173265409 * G_B18_0 = NULL;
	Type_t * G_B18_1 = NULL;
	Il2CppObject* G_B18_2 = NULL;
	ExporterFunc_t173265409 * G_B17_0 = NULL;
	Type_t * G_B17_1 = NULL;
	Il2CppObject* G_B17_2 = NULL;
	ExporterFunc_t173265409 * G_B20_0 = NULL;
	Type_t * G_B20_1 = NULL;
	Il2CppObject* G_B20_2 = NULL;
	ExporterFunc_t173265409 * G_B19_0 = NULL;
	Type_t * G_B19_1 = NULL;
	Il2CppObject* G_B19_2 = NULL;
	ExporterFunc_t173265409 * G_B22_0 = NULL;
	Type_t * G_B22_1 = NULL;
	Il2CppObject* G_B22_2 = NULL;
	ExporterFunc_t173265409 * G_B21_0 = NULL;
	Type_t * G_B21_1 = NULL;
	Il2CppObject* G_B21_2 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t77474459_il2cpp_TypeInfo_var);
		Il2CppObject* L_0 = ((JsonMapper_t77474459_StaticFields*)JsonMapper_t77474459_il2cpp_TypeInfo_var->static_fields)->get_base_exporters_table_2();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Byte_t3683104436_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t1591091833_il2cpp_TypeInfo_var);
		ExporterFunc_t173265409 * L_2 = ((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->get_U3CU3E9__24_0_1();
		ExporterFunc_t173265409 * L_3 = L_2;
		G_B1_0 = L_3;
		G_B1_1 = L_1;
		G_B1_2 = L_0;
		if (L_3)
		{
			G_B2_0 = L_3;
			G_B2_1 = L_1;
			G_B2_2 = L_0;
			goto IL_002e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t1591091833_il2cpp_TypeInfo_var);
		U3CU3Ec_t1591091833 * L_4 = ((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->get_U3CU3E9_0();
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)U3CU3Ec_U3CRegisterBaseExportersU3Eb__24_0_m3647892888_MethodInfo_var);
		ExporterFunc_t173265409 * L_6 = (ExporterFunc_t173265409 *)il2cpp_codegen_object_new(ExporterFunc_t173265409_il2cpp_TypeInfo_var);
		ExporterFunc__ctor_m1402811244(L_6, L_4, L_5, /*hidden argument*/NULL);
		ExporterFunc_t173265409 * L_7 = L_6;
		((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->set_U3CU3E9__24_0_1(L_7);
		G_B2_0 = L_7;
		G_B2_1 = G_B1_1;
		G_B2_2 = G_B1_2;
	}

IL_002e:
	{
		NullCheck(G_B2_2);
		InterfaceActionInvoker2< Type_t *, ExporterFunc_t173265409 * >::Invoke(5 /* System.Void System.Collections.Generic.IDictionary`2<System.Type,ThirdParty.Json.LitJson.ExporterFunc>::set_Item(!0,!1) */, IDictionary_2_t109706727_il2cpp_TypeInfo_var, G_B2_2, G_B2_1, G_B2_0);
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t77474459_il2cpp_TypeInfo_var);
		Il2CppObject* L_8 = ((JsonMapper_t77474459_StaticFields*)JsonMapper_t77474459_il2cpp_TypeInfo_var->static_fields)->get_base_exporters_table_2();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_9 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Char_t3454481338_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t1591091833_il2cpp_TypeInfo_var);
		ExporterFunc_t173265409 * L_10 = ((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->get_U3CU3E9__24_1_2();
		ExporterFunc_t173265409 * L_11 = L_10;
		G_B3_0 = L_11;
		G_B3_1 = L_9;
		G_B3_2 = L_8;
		if (L_11)
		{
			G_B4_0 = L_11;
			G_B4_1 = L_9;
			G_B4_2 = L_8;
			goto IL_0061;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t1591091833_il2cpp_TypeInfo_var);
		U3CU3Ec_t1591091833 * L_12 = ((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->get_U3CU3E9_0();
		IntPtr_t L_13;
		L_13.set_m_value_0((void*)(void*)U3CU3Ec_U3CRegisterBaseExportersU3Eb__24_1_m403640019_MethodInfo_var);
		ExporterFunc_t173265409 * L_14 = (ExporterFunc_t173265409 *)il2cpp_codegen_object_new(ExporterFunc_t173265409_il2cpp_TypeInfo_var);
		ExporterFunc__ctor_m1402811244(L_14, L_12, L_13, /*hidden argument*/NULL);
		ExporterFunc_t173265409 * L_15 = L_14;
		((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->set_U3CU3E9__24_1_2(L_15);
		G_B4_0 = L_15;
		G_B4_1 = G_B3_1;
		G_B4_2 = G_B3_2;
	}

IL_0061:
	{
		NullCheck(G_B4_2);
		InterfaceActionInvoker2< Type_t *, ExporterFunc_t173265409 * >::Invoke(5 /* System.Void System.Collections.Generic.IDictionary`2<System.Type,ThirdParty.Json.LitJson.ExporterFunc>::set_Item(!0,!1) */, IDictionary_2_t109706727_il2cpp_TypeInfo_var, G_B4_2, G_B4_1, G_B4_0);
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t77474459_il2cpp_TypeInfo_var);
		Il2CppObject* L_16 = ((JsonMapper_t77474459_StaticFields*)JsonMapper_t77474459_il2cpp_TypeInfo_var->static_fields)->get_base_exporters_table_2();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_17 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(DateTime_t693205669_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t1591091833_il2cpp_TypeInfo_var);
		ExporterFunc_t173265409 * L_18 = ((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->get_U3CU3E9__24_2_3();
		ExporterFunc_t173265409 * L_19 = L_18;
		G_B5_0 = L_19;
		G_B5_1 = L_17;
		G_B5_2 = L_16;
		if (L_19)
		{
			G_B6_0 = L_19;
			G_B6_1 = L_17;
			G_B6_2 = L_16;
			goto IL_0094;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t1591091833_il2cpp_TypeInfo_var);
		U3CU3Ec_t1591091833 * L_20 = ((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->get_U3CU3E9_0();
		IntPtr_t L_21;
		L_21.set_m_value_0((void*)(void*)U3CU3Ec_U3CRegisterBaseExportersU3Eb__24_2_m626278178_MethodInfo_var);
		ExporterFunc_t173265409 * L_22 = (ExporterFunc_t173265409 *)il2cpp_codegen_object_new(ExporterFunc_t173265409_il2cpp_TypeInfo_var);
		ExporterFunc__ctor_m1402811244(L_22, L_20, L_21, /*hidden argument*/NULL);
		ExporterFunc_t173265409 * L_23 = L_22;
		((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->set_U3CU3E9__24_2_3(L_23);
		G_B6_0 = L_23;
		G_B6_1 = G_B5_1;
		G_B6_2 = G_B5_2;
	}

IL_0094:
	{
		NullCheck(G_B6_2);
		InterfaceActionInvoker2< Type_t *, ExporterFunc_t173265409 * >::Invoke(5 /* System.Void System.Collections.Generic.IDictionary`2<System.Type,ThirdParty.Json.LitJson.ExporterFunc>::set_Item(!0,!1) */, IDictionary_2_t109706727_il2cpp_TypeInfo_var, G_B6_2, G_B6_1, G_B6_0);
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t77474459_il2cpp_TypeInfo_var);
		Il2CppObject* L_24 = ((JsonMapper_t77474459_StaticFields*)JsonMapper_t77474459_il2cpp_TypeInfo_var->static_fields)->get_base_exporters_table_2();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_25 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Decimal_t724701077_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t1591091833_il2cpp_TypeInfo_var);
		ExporterFunc_t173265409 * L_26 = ((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->get_U3CU3E9__24_3_4();
		ExporterFunc_t173265409 * L_27 = L_26;
		G_B7_0 = L_27;
		G_B7_1 = L_25;
		G_B7_2 = L_24;
		if (L_27)
		{
			G_B8_0 = L_27;
			G_B8_1 = L_25;
			G_B8_2 = L_24;
			goto IL_00c7;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t1591091833_il2cpp_TypeInfo_var);
		U3CU3Ec_t1591091833 * L_28 = ((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->get_U3CU3E9_0();
		IntPtr_t L_29;
		L_29.set_m_value_0((void*)(void*)U3CU3Ec_U3CRegisterBaseExportersU3Eb__24_3_m564382813_MethodInfo_var);
		ExporterFunc_t173265409 * L_30 = (ExporterFunc_t173265409 *)il2cpp_codegen_object_new(ExporterFunc_t173265409_il2cpp_TypeInfo_var);
		ExporterFunc__ctor_m1402811244(L_30, L_28, L_29, /*hidden argument*/NULL);
		ExporterFunc_t173265409 * L_31 = L_30;
		((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->set_U3CU3E9__24_3_4(L_31);
		G_B8_0 = L_31;
		G_B8_1 = G_B7_1;
		G_B8_2 = G_B7_2;
	}

IL_00c7:
	{
		NullCheck(G_B8_2);
		InterfaceActionInvoker2< Type_t *, ExporterFunc_t173265409 * >::Invoke(5 /* System.Void System.Collections.Generic.IDictionary`2<System.Type,ThirdParty.Json.LitJson.ExporterFunc>::set_Item(!0,!1) */, IDictionary_2_t109706727_il2cpp_TypeInfo_var, G_B8_2, G_B8_1, G_B8_0);
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t77474459_il2cpp_TypeInfo_var);
		Il2CppObject* L_32 = ((JsonMapper_t77474459_StaticFields*)JsonMapper_t77474459_il2cpp_TypeInfo_var->static_fields)->get_base_exporters_table_2();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_33 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(SByte_t454417549_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t1591091833_il2cpp_TypeInfo_var);
		ExporterFunc_t173265409 * L_34 = ((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->get_U3CU3E9__24_4_5();
		ExporterFunc_t173265409 * L_35 = L_34;
		G_B9_0 = L_35;
		G_B9_1 = L_33;
		G_B9_2 = L_32;
		if (L_35)
		{
			G_B10_0 = L_35;
			G_B10_1 = L_33;
			G_B10_2 = L_32;
			goto IL_00fa;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t1591091833_il2cpp_TypeInfo_var);
		U3CU3Ec_t1591091833 * L_36 = ((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->get_U3CU3E9_0();
		IntPtr_t L_37;
		L_37.set_m_value_0((void*)(void*)U3CU3Ec_U3CRegisterBaseExportersU3Eb__24_4_m3305872516_MethodInfo_var);
		ExporterFunc_t173265409 * L_38 = (ExporterFunc_t173265409 *)il2cpp_codegen_object_new(ExporterFunc_t173265409_il2cpp_TypeInfo_var);
		ExporterFunc__ctor_m1402811244(L_38, L_36, L_37, /*hidden argument*/NULL);
		ExporterFunc_t173265409 * L_39 = L_38;
		((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->set_U3CU3E9__24_4_5(L_39);
		G_B10_0 = L_39;
		G_B10_1 = G_B9_1;
		G_B10_2 = G_B9_2;
	}

IL_00fa:
	{
		NullCheck(G_B10_2);
		InterfaceActionInvoker2< Type_t *, ExporterFunc_t173265409 * >::Invoke(5 /* System.Void System.Collections.Generic.IDictionary`2<System.Type,ThirdParty.Json.LitJson.ExporterFunc>::set_Item(!0,!1) */, IDictionary_2_t109706727_il2cpp_TypeInfo_var, G_B10_2, G_B10_1, G_B10_0);
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t77474459_il2cpp_TypeInfo_var);
		Il2CppObject* L_40 = ((JsonMapper_t77474459_StaticFields*)JsonMapper_t77474459_il2cpp_TypeInfo_var->static_fields)->get_base_exporters_table_2();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_41 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Int16_t4041245914_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t1591091833_il2cpp_TypeInfo_var);
		ExporterFunc_t173265409 * L_42 = ((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->get_U3CU3E9__24_5_6();
		ExporterFunc_t173265409 * L_43 = L_42;
		G_B11_0 = L_43;
		G_B11_1 = L_41;
		G_B11_2 = L_40;
		if (L_43)
		{
			G_B12_0 = L_43;
			G_B12_1 = L_41;
			G_B12_2 = L_40;
			goto IL_012d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t1591091833_il2cpp_TypeInfo_var);
		U3CU3Ec_t1591091833 * L_44 = ((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->get_U3CU3E9_0();
		IntPtr_t L_45;
		L_45.set_m_value_0((void*)(void*)U3CU3Ec_U3CRegisterBaseExportersU3Eb__24_5_m61619647_MethodInfo_var);
		ExporterFunc_t173265409 * L_46 = (ExporterFunc_t173265409 *)il2cpp_codegen_object_new(ExporterFunc_t173265409_il2cpp_TypeInfo_var);
		ExporterFunc__ctor_m1402811244(L_46, L_44, L_45, /*hidden argument*/NULL);
		ExporterFunc_t173265409 * L_47 = L_46;
		((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->set_U3CU3E9__24_5_6(L_47);
		G_B12_0 = L_47;
		G_B12_1 = G_B11_1;
		G_B12_2 = G_B11_2;
	}

IL_012d:
	{
		NullCheck(G_B12_2);
		InterfaceActionInvoker2< Type_t *, ExporterFunc_t173265409 * >::Invoke(5 /* System.Void System.Collections.Generic.IDictionary`2<System.Type,ThirdParty.Json.LitJson.ExporterFunc>::set_Item(!0,!1) */, IDictionary_2_t109706727_il2cpp_TypeInfo_var, G_B12_2, G_B12_1, G_B12_0);
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t77474459_il2cpp_TypeInfo_var);
		Il2CppObject* L_48 = ((JsonMapper_t77474459_StaticFields*)JsonMapper_t77474459_il2cpp_TypeInfo_var->static_fields)->get_base_exporters_table_2();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_49 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(UInt16_t986882611_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t1591091833_il2cpp_TypeInfo_var);
		ExporterFunc_t173265409 * L_50 = ((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->get_U3CU3E9__24_6_7();
		ExporterFunc_t173265409 * L_51 = L_50;
		G_B13_0 = L_51;
		G_B13_1 = L_49;
		G_B13_2 = L_48;
		if (L_51)
		{
			G_B14_0 = L_51;
			G_B14_1 = L_49;
			G_B14_2 = L_48;
			goto IL_0160;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t1591091833_il2cpp_TypeInfo_var);
		U3CU3Ec_t1591091833 * L_52 = ((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->get_U3CU3E9_0();
		IntPtr_t L_53;
		L_53.set_m_value_0((void*)(void*)U3CU3Ec_U3CRegisterBaseExportersU3Eb__24_6_m284257806_MethodInfo_var);
		ExporterFunc_t173265409 * L_54 = (ExporterFunc_t173265409 *)il2cpp_codegen_object_new(ExporterFunc_t173265409_il2cpp_TypeInfo_var);
		ExporterFunc__ctor_m1402811244(L_54, L_52, L_53, /*hidden argument*/NULL);
		ExporterFunc_t173265409 * L_55 = L_54;
		((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->set_U3CU3E9__24_6_7(L_55);
		G_B14_0 = L_55;
		G_B14_1 = G_B13_1;
		G_B14_2 = G_B13_2;
	}

IL_0160:
	{
		NullCheck(G_B14_2);
		InterfaceActionInvoker2< Type_t *, ExporterFunc_t173265409 * >::Invoke(5 /* System.Void System.Collections.Generic.IDictionary`2<System.Type,ThirdParty.Json.LitJson.ExporterFunc>::set_Item(!0,!1) */, IDictionary_2_t109706727_il2cpp_TypeInfo_var, G_B14_2, G_B14_1, G_B14_0);
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t77474459_il2cpp_TypeInfo_var);
		Il2CppObject* L_56 = ((JsonMapper_t77474459_StaticFields*)JsonMapper_t77474459_il2cpp_TypeInfo_var->static_fields)->get_base_exporters_table_2();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_57 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(UInt32_t2149682021_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t1591091833_il2cpp_TypeInfo_var);
		ExporterFunc_t173265409 * L_58 = ((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->get_U3CU3E9__24_7_8();
		ExporterFunc_t173265409 * L_59 = L_58;
		G_B15_0 = L_59;
		G_B15_1 = L_57;
		G_B15_2 = L_56;
		if (L_59)
		{
			G_B16_0 = L_59;
			G_B16_1 = L_57;
			G_B16_2 = L_56;
			goto IL_0193;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t1591091833_il2cpp_TypeInfo_var);
		U3CU3Ec_t1591091833 * L_60 = ((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->get_U3CU3E9_0();
		IntPtr_t L_61;
		L_61.set_m_value_0((void*)(void*)U3CU3Ec_U3CRegisterBaseExportersU3Eb__24_7_m222362441_MethodInfo_var);
		ExporterFunc_t173265409 * L_62 = (ExporterFunc_t173265409 *)il2cpp_codegen_object_new(ExporterFunc_t173265409_il2cpp_TypeInfo_var);
		ExporterFunc__ctor_m1402811244(L_62, L_60, L_61, /*hidden argument*/NULL);
		ExporterFunc_t173265409 * L_63 = L_62;
		((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->set_U3CU3E9__24_7_8(L_63);
		G_B16_0 = L_63;
		G_B16_1 = G_B15_1;
		G_B16_2 = G_B15_2;
	}

IL_0193:
	{
		NullCheck(G_B16_2);
		InterfaceActionInvoker2< Type_t *, ExporterFunc_t173265409 * >::Invoke(5 /* System.Void System.Collections.Generic.IDictionary`2<System.Type,ThirdParty.Json.LitJson.ExporterFunc>::set_Item(!0,!1) */, IDictionary_2_t109706727_il2cpp_TypeInfo_var, G_B16_2, G_B16_1, G_B16_0);
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t77474459_il2cpp_TypeInfo_var);
		Il2CppObject* L_64 = ((JsonMapper_t77474459_StaticFields*)JsonMapper_t77474459_il2cpp_TypeInfo_var->static_fields)->get_base_exporters_table_2();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_65 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(UInt64_t2909196914_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t1591091833_il2cpp_TypeInfo_var);
		ExporterFunc_t173265409 * L_66 = ((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->get_U3CU3E9__24_8_9();
		ExporterFunc_t173265409 * L_67 = L_66;
		G_B17_0 = L_67;
		G_B17_1 = L_65;
		G_B17_2 = L_64;
		if (L_67)
		{
			G_B18_0 = L_67;
			G_B18_1 = L_65;
			G_B18_2 = L_64;
			goto IL_01c6;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t1591091833_il2cpp_TypeInfo_var);
		U3CU3Ec_t1591091833 * L_68 = ((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->get_U3CU3E9_0();
		IntPtr_t L_69;
		L_69.set_m_value_0((void*)(void*)U3CU3Ec_U3CRegisterBaseExportersU3Eb__24_8_m931111872_MethodInfo_var);
		ExporterFunc_t173265409 * L_70 = (ExporterFunc_t173265409 *)il2cpp_codegen_object_new(ExporterFunc_t173265409_il2cpp_TypeInfo_var);
		ExporterFunc__ctor_m1402811244(L_70, L_68, L_69, /*hidden argument*/NULL);
		ExporterFunc_t173265409 * L_71 = L_70;
		((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->set_U3CU3E9__24_8_9(L_71);
		G_B18_0 = L_71;
		G_B18_1 = G_B17_1;
		G_B18_2 = G_B17_2;
	}

IL_01c6:
	{
		NullCheck(G_B18_2);
		InterfaceActionInvoker2< Type_t *, ExporterFunc_t173265409 * >::Invoke(5 /* System.Void System.Collections.Generic.IDictionary`2<System.Type,ThirdParty.Json.LitJson.ExporterFunc>::set_Item(!0,!1) */, IDictionary_2_t109706727_il2cpp_TypeInfo_var, G_B18_2, G_B18_1, G_B18_0);
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t77474459_il2cpp_TypeInfo_var);
		Il2CppObject* L_72 = ((JsonMapper_t77474459_StaticFields*)JsonMapper_t77474459_il2cpp_TypeInfo_var->static_fields)->get_base_exporters_table_2();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_73 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Single_t2076509932_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t1591091833_il2cpp_TypeInfo_var);
		ExporterFunc_t173265409 * L_74 = ((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->get_U3CU3E9__24_9_10();
		ExporterFunc_t173265409 * L_75 = L_74;
		G_B19_0 = L_75;
		G_B19_1 = L_73;
		G_B19_2 = L_72;
		if (L_75)
		{
			G_B20_0 = L_75;
			G_B20_1 = L_73;
			G_B20_2 = L_72;
			goto IL_01f9;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t1591091833_il2cpp_TypeInfo_var);
		U3CU3Ec_t1591091833 * L_76 = ((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->get_U3CU3E9_0();
		IntPtr_t L_77;
		L_77.set_m_value_0((void*)(void*)U3CU3Ec_U3CRegisterBaseExportersU3Eb__24_9_m1981826299_MethodInfo_var);
		ExporterFunc_t173265409 * L_78 = (ExporterFunc_t173265409 *)il2cpp_codegen_object_new(ExporterFunc_t173265409_il2cpp_TypeInfo_var);
		ExporterFunc__ctor_m1402811244(L_78, L_76, L_77, /*hidden argument*/NULL);
		ExporterFunc_t173265409 * L_79 = L_78;
		((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->set_U3CU3E9__24_9_10(L_79);
		G_B20_0 = L_79;
		G_B20_1 = G_B19_1;
		G_B20_2 = G_B19_2;
	}

IL_01f9:
	{
		NullCheck(G_B20_2);
		InterfaceActionInvoker2< Type_t *, ExporterFunc_t173265409 * >::Invoke(5 /* System.Void System.Collections.Generic.IDictionary`2<System.Type,ThirdParty.Json.LitJson.ExporterFunc>::set_Item(!0,!1) */, IDictionary_2_t109706727_il2cpp_TypeInfo_var, G_B20_2, G_B20_1, G_B20_0);
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t77474459_il2cpp_TypeInfo_var);
		Il2CppObject* L_80 = ((JsonMapper_t77474459_StaticFields*)JsonMapper_t77474459_il2cpp_TypeInfo_var->static_fields)->get_base_exporters_table_2();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_81 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Int64_t909078037_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t1591091833_il2cpp_TypeInfo_var);
		ExporterFunc_t173265409 * L_82 = ((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->get_U3CU3E9__24_10_11();
		ExporterFunc_t173265409 * L_83 = L_82;
		G_B21_0 = L_83;
		G_B21_1 = L_81;
		G_B21_2 = L_80;
		if (L_83)
		{
			G_B22_0 = L_83;
			G_B22_1 = L_81;
			G_B22_2 = L_80;
			goto IL_022c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t1591091833_il2cpp_TypeInfo_var);
		U3CU3Ec_t1591091833 * L_84 = ((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->get_U3CU3E9_0();
		IntPtr_t L_85;
		L_85.set_m_value_0((void*)(void*)U3CU3Ec_U3CRegisterBaseExportersU3Eb__24_10_m3919046323_MethodInfo_var);
		ExporterFunc_t173265409 * L_86 = (ExporterFunc_t173265409 *)il2cpp_codegen_object_new(ExporterFunc_t173265409_il2cpp_TypeInfo_var);
		ExporterFunc__ctor_m1402811244(L_86, L_84, L_85, /*hidden argument*/NULL);
		ExporterFunc_t173265409 * L_87 = L_86;
		((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->set_U3CU3E9__24_10_11(L_87);
		G_B22_0 = L_87;
		G_B22_1 = G_B21_1;
		G_B22_2 = G_B21_2;
	}

IL_022c:
	{
		NullCheck(G_B22_2);
		InterfaceActionInvoker2< Type_t *, ExporterFunc_t173265409 * >::Invoke(5 /* System.Void System.Collections.Generic.IDictionary`2<System.Type,ThirdParty.Json.LitJson.ExporterFunc>::set_Item(!0,!1) */, IDictionary_2_t109706727_il2cpp_TypeInfo_var, G_B22_2, G_B22_1, G_B22_0);
		return;
	}
}
// System.Void ThirdParty.Json.LitJson.JsonMapper::RegisterBaseImporters()
extern const Il2CppType* Int32_t2071877448_0_0_0_var;
extern const Il2CppType* Byte_t3683104436_0_0_0_var;
extern const Il2CppType* UInt64_t2909196914_0_0_0_var;
extern const Il2CppType* SByte_t454417549_0_0_0_var;
extern const Il2CppType* Int16_t4041245914_0_0_0_var;
extern const Il2CppType* UInt16_t986882611_0_0_0_var;
extern const Il2CppType* UInt32_t2149682021_0_0_0_var;
extern const Il2CppType* Single_t2076509932_0_0_0_var;
extern const Il2CppType* Double_t4078015681_0_0_0_var;
extern const Il2CppType* Decimal_t724701077_0_0_0_var;
extern const Il2CppType* Int64_t909078037_0_0_0_var;
extern const Il2CppType* String_t_0_0_0_var;
extern const Il2CppType* Char_t3454481338_0_0_0_var;
extern const Il2CppType* DateTime_t693205669_0_0_0_var;
extern Il2CppClass* U3CU3Ec_t1591091833_il2cpp_TypeInfo_var;
extern Il2CppClass* ImporterFunc_t850687278_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonMapper_t77474459_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_0_m2312218722_MethodInfo_var;
extern const MethodInfo* U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_1_m542108775_MethodInfo_var;
extern const MethodInfo* U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_2_m2023383788_MethodInfo_var;
extern const MethodInfo* U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_3_m253273841_MethodInfo_var;
extern const MethodInfo* U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_4_m785232718_MethodInfo_var;
extern const MethodInfo* U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_5_m3310090067_MethodInfo_var;
extern const MethodInfo* U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_6_m496397784_MethodInfo_var;
extern const MethodInfo* U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_7_m3021255133_MethodInfo_var;
extern const MethodInfo* U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_8_m3565818426_MethodInfo_var;
extern const MethodInfo* U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_9_m1795708479_MethodInfo_var;
extern const MethodInfo* U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_10_m1231844419_MethodInfo_var;
extern const MethodInfo* U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_11_m1950251426_MethodInfo_var;
extern const MethodInfo* U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_12_m2105962245_MethodInfo_var;
extern const MethodInfo* U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_13_m2617176164_MethodInfo_var;
extern const uint32_t JsonMapper_RegisterBaseImporters_m2686124695_MetadataUsageId;
extern "C"  void JsonMapper_RegisterBaseImporters_m2686124695 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_RegisterBaseImporters_m2686124695_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ImporterFunc_t850687278 * V_0 = NULL;
	ImporterFunc_t850687278 * G_B2_0 = NULL;
	ImporterFunc_t850687278 * G_B1_0 = NULL;
	ImporterFunc_t850687278 * G_B4_0 = NULL;
	ImporterFunc_t850687278 * G_B3_0 = NULL;
	ImporterFunc_t850687278 * G_B6_0 = NULL;
	ImporterFunc_t850687278 * G_B5_0 = NULL;
	ImporterFunc_t850687278 * G_B8_0 = NULL;
	ImporterFunc_t850687278 * G_B7_0 = NULL;
	ImporterFunc_t850687278 * G_B10_0 = NULL;
	ImporterFunc_t850687278 * G_B9_0 = NULL;
	ImporterFunc_t850687278 * G_B12_0 = NULL;
	ImporterFunc_t850687278 * G_B11_0 = NULL;
	ImporterFunc_t850687278 * G_B14_0 = NULL;
	ImporterFunc_t850687278 * G_B13_0 = NULL;
	ImporterFunc_t850687278 * G_B16_0 = NULL;
	ImporterFunc_t850687278 * G_B15_0 = NULL;
	ImporterFunc_t850687278 * G_B18_0 = NULL;
	ImporterFunc_t850687278 * G_B17_0 = NULL;
	ImporterFunc_t850687278 * G_B20_0 = NULL;
	ImporterFunc_t850687278 * G_B19_0 = NULL;
	ImporterFunc_t850687278 * G_B22_0 = NULL;
	ImporterFunc_t850687278 * G_B21_0 = NULL;
	ImporterFunc_t850687278 * G_B24_0 = NULL;
	ImporterFunc_t850687278 * G_B23_0 = NULL;
	ImporterFunc_t850687278 * G_B26_0 = NULL;
	ImporterFunc_t850687278 * G_B25_0 = NULL;
	ImporterFunc_t850687278 * G_B28_0 = NULL;
	ImporterFunc_t850687278 * G_B27_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t1591091833_il2cpp_TypeInfo_var);
		ImporterFunc_t850687278 * L_0 = ((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->get_U3CU3E9__25_0_12();
		ImporterFunc_t850687278 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t1591091833_il2cpp_TypeInfo_var);
		U3CU3Ec_t1591091833 * L_2 = ((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->get_U3CU3E9_0();
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_0_m2312218722_MethodInfo_var);
		ImporterFunc_t850687278 * L_4 = (ImporterFunc_t850687278 *)il2cpp_codegen_object_new(ImporterFunc_t850687278_il2cpp_TypeInfo_var);
		ImporterFunc__ctor_m637091809(L_4, L_2, L_3, /*hidden argument*/NULL);
		ImporterFunc_t850687278 * L_5 = L_4;
		((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->set_U3CU3E9__25_0_12(L_5);
		G_B2_0 = L_5;
	}

IL_001f:
	{
		V_0 = G_B2_0;
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t77474459_il2cpp_TypeInfo_var);
		Il2CppObject* L_6 = ((JsonMapper_t77474459_StaticFields*)JsonMapper_t77474459_il2cpp_TypeInfo_var->static_fields)->get_base_importers_table_4();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_7 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Int32_t2071877448_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_8 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Byte_t3683104436_0_0_0_var), /*hidden argument*/NULL);
		ImporterFunc_t850687278 * L_9 = V_0;
		JsonMapper_RegisterImporter_m1039024633(NULL /*static, unused*/, L_6, L_7, L_8, L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t1591091833_il2cpp_TypeInfo_var);
		ImporterFunc_t850687278 * L_10 = ((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->get_U3CU3E9__25_1_13();
		ImporterFunc_t850687278 * L_11 = L_10;
		G_B3_0 = L_11;
		if (L_11)
		{
			G_B4_0 = L_11;
			goto IL_005e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t1591091833_il2cpp_TypeInfo_var);
		U3CU3Ec_t1591091833 * L_12 = ((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->get_U3CU3E9_0();
		IntPtr_t L_13;
		L_13.set_m_value_0((void*)(void*)U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_1_m542108775_MethodInfo_var);
		ImporterFunc_t850687278 * L_14 = (ImporterFunc_t850687278 *)il2cpp_codegen_object_new(ImporterFunc_t850687278_il2cpp_TypeInfo_var);
		ImporterFunc__ctor_m637091809(L_14, L_12, L_13, /*hidden argument*/NULL);
		ImporterFunc_t850687278 * L_15 = L_14;
		((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->set_U3CU3E9__25_1_13(L_15);
		G_B4_0 = L_15;
	}

IL_005e:
	{
		V_0 = G_B4_0;
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t77474459_il2cpp_TypeInfo_var);
		Il2CppObject* L_16 = ((JsonMapper_t77474459_StaticFields*)JsonMapper_t77474459_il2cpp_TypeInfo_var->static_fields)->get_base_importers_table_4();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_17 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Int32_t2071877448_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_18 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(UInt64_t2909196914_0_0_0_var), /*hidden argument*/NULL);
		ImporterFunc_t850687278 * L_19 = V_0;
		JsonMapper_RegisterImporter_m1039024633(NULL /*static, unused*/, L_16, L_17, L_18, L_19, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t1591091833_il2cpp_TypeInfo_var);
		ImporterFunc_t850687278 * L_20 = ((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->get_U3CU3E9__25_2_14();
		ImporterFunc_t850687278 * L_21 = L_20;
		G_B5_0 = L_21;
		if (L_21)
		{
			G_B6_0 = L_21;
			goto IL_009d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t1591091833_il2cpp_TypeInfo_var);
		U3CU3Ec_t1591091833 * L_22 = ((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->get_U3CU3E9_0();
		IntPtr_t L_23;
		L_23.set_m_value_0((void*)(void*)U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_2_m2023383788_MethodInfo_var);
		ImporterFunc_t850687278 * L_24 = (ImporterFunc_t850687278 *)il2cpp_codegen_object_new(ImporterFunc_t850687278_il2cpp_TypeInfo_var);
		ImporterFunc__ctor_m637091809(L_24, L_22, L_23, /*hidden argument*/NULL);
		ImporterFunc_t850687278 * L_25 = L_24;
		((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->set_U3CU3E9__25_2_14(L_25);
		G_B6_0 = L_25;
	}

IL_009d:
	{
		V_0 = G_B6_0;
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t77474459_il2cpp_TypeInfo_var);
		Il2CppObject* L_26 = ((JsonMapper_t77474459_StaticFields*)JsonMapper_t77474459_il2cpp_TypeInfo_var->static_fields)->get_base_importers_table_4();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_27 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Int32_t2071877448_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_28 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(SByte_t454417549_0_0_0_var), /*hidden argument*/NULL);
		ImporterFunc_t850687278 * L_29 = V_0;
		JsonMapper_RegisterImporter_m1039024633(NULL /*static, unused*/, L_26, L_27, L_28, L_29, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t1591091833_il2cpp_TypeInfo_var);
		ImporterFunc_t850687278 * L_30 = ((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->get_U3CU3E9__25_3_15();
		ImporterFunc_t850687278 * L_31 = L_30;
		G_B7_0 = L_31;
		if (L_31)
		{
			G_B8_0 = L_31;
			goto IL_00dc;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t1591091833_il2cpp_TypeInfo_var);
		U3CU3Ec_t1591091833 * L_32 = ((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->get_U3CU3E9_0();
		IntPtr_t L_33;
		L_33.set_m_value_0((void*)(void*)U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_3_m253273841_MethodInfo_var);
		ImporterFunc_t850687278 * L_34 = (ImporterFunc_t850687278 *)il2cpp_codegen_object_new(ImporterFunc_t850687278_il2cpp_TypeInfo_var);
		ImporterFunc__ctor_m637091809(L_34, L_32, L_33, /*hidden argument*/NULL);
		ImporterFunc_t850687278 * L_35 = L_34;
		((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->set_U3CU3E9__25_3_15(L_35);
		G_B8_0 = L_35;
	}

IL_00dc:
	{
		V_0 = G_B8_0;
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t77474459_il2cpp_TypeInfo_var);
		Il2CppObject* L_36 = ((JsonMapper_t77474459_StaticFields*)JsonMapper_t77474459_il2cpp_TypeInfo_var->static_fields)->get_base_importers_table_4();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_37 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Int32_t2071877448_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_38 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Int16_t4041245914_0_0_0_var), /*hidden argument*/NULL);
		ImporterFunc_t850687278 * L_39 = V_0;
		JsonMapper_RegisterImporter_m1039024633(NULL /*static, unused*/, L_36, L_37, L_38, L_39, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t1591091833_il2cpp_TypeInfo_var);
		ImporterFunc_t850687278 * L_40 = ((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->get_U3CU3E9__25_4_16();
		ImporterFunc_t850687278 * L_41 = L_40;
		G_B9_0 = L_41;
		if (L_41)
		{
			G_B10_0 = L_41;
			goto IL_011b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t1591091833_il2cpp_TypeInfo_var);
		U3CU3Ec_t1591091833 * L_42 = ((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->get_U3CU3E9_0();
		IntPtr_t L_43;
		L_43.set_m_value_0((void*)(void*)U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_4_m785232718_MethodInfo_var);
		ImporterFunc_t850687278 * L_44 = (ImporterFunc_t850687278 *)il2cpp_codegen_object_new(ImporterFunc_t850687278_il2cpp_TypeInfo_var);
		ImporterFunc__ctor_m637091809(L_44, L_42, L_43, /*hidden argument*/NULL);
		ImporterFunc_t850687278 * L_45 = L_44;
		((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->set_U3CU3E9__25_4_16(L_45);
		G_B10_0 = L_45;
	}

IL_011b:
	{
		V_0 = G_B10_0;
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t77474459_il2cpp_TypeInfo_var);
		Il2CppObject* L_46 = ((JsonMapper_t77474459_StaticFields*)JsonMapper_t77474459_il2cpp_TypeInfo_var->static_fields)->get_base_importers_table_4();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_47 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Int32_t2071877448_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_48 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(UInt16_t986882611_0_0_0_var), /*hidden argument*/NULL);
		ImporterFunc_t850687278 * L_49 = V_0;
		JsonMapper_RegisterImporter_m1039024633(NULL /*static, unused*/, L_46, L_47, L_48, L_49, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t1591091833_il2cpp_TypeInfo_var);
		ImporterFunc_t850687278 * L_50 = ((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->get_U3CU3E9__25_5_17();
		ImporterFunc_t850687278 * L_51 = L_50;
		G_B11_0 = L_51;
		if (L_51)
		{
			G_B12_0 = L_51;
			goto IL_015a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t1591091833_il2cpp_TypeInfo_var);
		U3CU3Ec_t1591091833 * L_52 = ((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->get_U3CU3E9_0();
		IntPtr_t L_53;
		L_53.set_m_value_0((void*)(void*)U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_5_m3310090067_MethodInfo_var);
		ImporterFunc_t850687278 * L_54 = (ImporterFunc_t850687278 *)il2cpp_codegen_object_new(ImporterFunc_t850687278_il2cpp_TypeInfo_var);
		ImporterFunc__ctor_m637091809(L_54, L_52, L_53, /*hidden argument*/NULL);
		ImporterFunc_t850687278 * L_55 = L_54;
		((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->set_U3CU3E9__25_5_17(L_55);
		G_B12_0 = L_55;
	}

IL_015a:
	{
		V_0 = G_B12_0;
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t77474459_il2cpp_TypeInfo_var);
		Il2CppObject* L_56 = ((JsonMapper_t77474459_StaticFields*)JsonMapper_t77474459_il2cpp_TypeInfo_var->static_fields)->get_base_importers_table_4();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_57 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Int32_t2071877448_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_58 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(UInt32_t2149682021_0_0_0_var), /*hidden argument*/NULL);
		ImporterFunc_t850687278 * L_59 = V_0;
		JsonMapper_RegisterImporter_m1039024633(NULL /*static, unused*/, L_56, L_57, L_58, L_59, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t1591091833_il2cpp_TypeInfo_var);
		ImporterFunc_t850687278 * L_60 = ((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->get_U3CU3E9__25_6_18();
		ImporterFunc_t850687278 * L_61 = L_60;
		G_B13_0 = L_61;
		if (L_61)
		{
			G_B14_0 = L_61;
			goto IL_0199;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t1591091833_il2cpp_TypeInfo_var);
		U3CU3Ec_t1591091833 * L_62 = ((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->get_U3CU3E9_0();
		IntPtr_t L_63;
		L_63.set_m_value_0((void*)(void*)U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_6_m496397784_MethodInfo_var);
		ImporterFunc_t850687278 * L_64 = (ImporterFunc_t850687278 *)il2cpp_codegen_object_new(ImporterFunc_t850687278_il2cpp_TypeInfo_var);
		ImporterFunc__ctor_m637091809(L_64, L_62, L_63, /*hidden argument*/NULL);
		ImporterFunc_t850687278 * L_65 = L_64;
		((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->set_U3CU3E9__25_6_18(L_65);
		G_B14_0 = L_65;
	}

IL_0199:
	{
		V_0 = G_B14_0;
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t77474459_il2cpp_TypeInfo_var);
		Il2CppObject* L_66 = ((JsonMapper_t77474459_StaticFields*)JsonMapper_t77474459_il2cpp_TypeInfo_var->static_fields)->get_base_importers_table_4();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_67 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Int32_t2071877448_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_68 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Single_t2076509932_0_0_0_var), /*hidden argument*/NULL);
		ImporterFunc_t850687278 * L_69 = V_0;
		JsonMapper_RegisterImporter_m1039024633(NULL /*static, unused*/, L_66, L_67, L_68, L_69, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t1591091833_il2cpp_TypeInfo_var);
		ImporterFunc_t850687278 * L_70 = ((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->get_U3CU3E9__25_7_19();
		ImporterFunc_t850687278 * L_71 = L_70;
		G_B15_0 = L_71;
		if (L_71)
		{
			G_B16_0 = L_71;
			goto IL_01d8;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t1591091833_il2cpp_TypeInfo_var);
		U3CU3Ec_t1591091833 * L_72 = ((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->get_U3CU3E9_0();
		IntPtr_t L_73;
		L_73.set_m_value_0((void*)(void*)U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_7_m3021255133_MethodInfo_var);
		ImporterFunc_t850687278 * L_74 = (ImporterFunc_t850687278 *)il2cpp_codegen_object_new(ImporterFunc_t850687278_il2cpp_TypeInfo_var);
		ImporterFunc__ctor_m637091809(L_74, L_72, L_73, /*hidden argument*/NULL);
		ImporterFunc_t850687278 * L_75 = L_74;
		((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->set_U3CU3E9__25_7_19(L_75);
		G_B16_0 = L_75;
	}

IL_01d8:
	{
		V_0 = G_B16_0;
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t77474459_il2cpp_TypeInfo_var);
		Il2CppObject* L_76 = ((JsonMapper_t77474459_StaticFields*)JsonMapper_t77474459_il2cpp_TypeInfo_var->static_fields)->get_base_importers_table_4();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_77 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Int32_t2071877448_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_78 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Double_t4078015681_0_0_0_var), /*hidden argument*/NULL);
		ImporterFunc_t850687278 * L_79 = V_0;
		JsonMapper_RegisterImporter_m1039024633(NULL /*static, unused*/, L_76, L_77, L_78, L_79, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t1591091833_il2cpp_TypeInfo_var);
		ImporterFunc_t850687278 * L_80 = ((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->get_U3CU3E9__25_8_20();
		ImporterFunc_t850687278 * L_81 = L_80;
		G_B17_0 = L_81;
		if (L_81)
		{
			G_B18_0 = L_81;
			goto IL_0217;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t1591091833_il2cpp_TypeInfo_var);
		U3CU3Ec_t1591091833 * L_82 = ((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->get_U3CU3E9_0();
		IntPtr_t L_83;
		L_83.set_m_value_0((void*)(void*)U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_8_m3565818426_MethodInfo_var);
		ImporterFunc_t850687278 * L_84 = (ImporterFunc_t850687278 *)il2cpp_codegen_object_new(ImporterFunc_t850687278_il2cpp_TypeInfo_var);
		ImporterFunc__ctor_m637091809(L_84, L_82, L_83, /*hidden argument*/NULL);
		ImporterFunc_t850687278 * L_85 = L_84;
		((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->set_U3CU3E9__25_8_20(L_85);
		G_B18_0 = L_85;
	}

IL_0217:
	{
		V_0 = G_B18_0;
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t77474459_il2cpp_TypeInfo_var);
		Il2CppObject* L_86 = ((JsonMapper_t77474459_StaticFields*)JsonMapper_t77474459_il2cpp_TypeInfo_var->static_fields)->get_base_importers_table_4();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_87 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Double_t4078015681_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_88 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Decimal_t724701077_0_0_0_var), /*hidden argument*/NULL);
		ImporterFunc_t850687278 * L_89 = V_0;
		JsonMapper_RegisterImporter_m1039024633(NULL /*static, unused*/, L_86, L_87, L_88, L_89, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t1591091833_il2cpp_TypeInfo_var);
		ImporterFunc_t850687278 * L_90 = ((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->get_U3CU3E9__25_9_21();
		ImporterFunc_t850687278 * L_91 = L_90;
		G_B19_0 = L_91;
		if (L_91)
		{
			G_B20_0 = L_91;
			goto IL_0256;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t1591091833_il2cpp_TypeInfo_var);
		U3CU3Ec_t1591091833 * L_92 = ((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->get_U3CU3E9_0();
		IntPtr_t L_93;
		L_93.set_m_value_0((void*)(void*)U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_9_m1795708479_MethodInfo_var);
		ImporterFunc_t850687278 * L_94 = (ImporterFunc_t850687278 *)il2cpp_codegen_object_new(ImporterFunc_t850687278_il2cpp_TypeInfo_var);
		ImporterFunc__ctor_m637091809(L_94, L_92, L_93, /*hidden argument*/NULL);
		ImporterFunc_t850687278 * L_95 = L_94;
		((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->set_U3CU3E9__25_9_21(L_95);
		G_B20_0 = L_95;
	}

IL_0256:
	{
		V_0 = G_B20_0;
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t77474459_il2cpp_TypeInfo_var);
		Il2CppObject* L_96 = ((JsonMapper_t77474459_StaticFields*)JsonMapper_t77474459_il2cpp_TypeInfo_var->static_fields)->get_base_importers_table_4();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_97 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Double_t4078015681_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_98 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Single_t2076509932_0_0_0_var), /*hidden argument*/NULL);
		ImporterFunc_t850687278 * L_99 = V_0;
		JsonMapper_RegisterImporter_m1039024633(NULL /*static, unused*/, L_96, L_97, L_98, L_99, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t1591091833_il2cpp_TypeInfo_var);
		ImporterFunc_t850687278 * L_100 = ((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->get_U3CU3E9__25_10_22();
		ImporterFunc_t850687278 * L_101 = L_100;
		G_B21_0 = L_101;
		if (L_101)
		{
			G_B22_0 = L_101;
			goto IL_0295;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t1591091833_il2cpp_TypeInfo_var);
		U3CU3Ec_t1591091833 * L_102 = ((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->get_U3CU3E9_0();
		IntPtr_t L_103;
		L_103.set_m_value_0((void*)(void*)U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_10_m1231844419_MethodInfo_var);
		ImporterFunc_t850687278 * L_104 = (ImporterFunc_t850687278 *)il2cpp_codegen_object_new(ImporterFunc_t850687278_il2cpp_TypeInfo_var);
		ImporterFunc__ctor_m637091809(L_104, L_102, L_103, /*hidden argument*/NULL);
		ImporterFunc_t850687278 * L_105 = L_104;
		((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->set_U3CU3E9__25_10_22(L_105);
		G_B22_0 = L_105;
	}

IL_0295:
	{
		V_0 = G_B22_0;
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t77474459_il2cpp_TypeInfo_var);
		Il2CppObject* L_106 = ((JsonMapper_t77474459_StaticFields*)JsonMapper_t77474459_il2cpp_TypeInfo_var->static_fields)->get_base_importers_table_4();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_107 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Int64_t909078037_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_108 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(UInt32_t2149682021_0_0_0_var), /*hidden argument*/NULL);
		ImporterFunc_t850687278 * L_109 = V_0;
		JsonMapper_RegisterImporter_m1039024633(NULL /*static, unused*/, L_106, L_107, L_108, L_109, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t1591091833_il2cpp_TypeInfo_var);
		ImporterFunc_t850687278 * L_110 = ((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->get_U3CU3E9__25_11_23();
		ImporterFunc_t850687278 * L_111 = L_110;
		G_B23_0 = L_111;
		if (L_111)
		{
			G_B24_0 = L_111;
			goto IL_02d4;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t1591091833_il2cpp_TypeInfo_var);
		U3CU3Ec_t1591091833 * L_112 = ((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->get_U3CU3E9_0();
		IntPtr_t L_113;
		L_113.set_m_value_0((void*)(void*)U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_11_m1950251426_MethodInfo_var);
		ImporterFunc_t850687278 * L_114 = (ImporterFunc_t850687278 *)il2cpp_codegen_object_new(ImporterFunc_t850687278_il2cpp_TypeInfo_var);
		ImporterFunc__ctor_m637091809(L_114, L_112, L_113, /*hidden argument*/NULL);
		ImporterFunc_t850687278 * L_115 = L_114;
		((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->set_U3CU3E9__25_11_23(L_115);
		G_B24_0 = L_115;
	}

IL_02d4:
	{
		V_0 = G_B24_0;
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t77474459_il2cpp_TypeInfo_var);
		Il2CppObject* L_116 = ((JsonMapper_t77474459_StaticFields*)JsonMapper_t77474459_il2cpp_TypeInfo_var->static_fields)->get_base_importers_table_4();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_117 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_118 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Char_t3454481338_0_0_0_var), /*hidden argument*/NULL);
		ImporterFunc_t850687278 * L_119 = V_0;
		JsonMapper_RegisterImporter_m1039024633(NULL /*static, unused*/, L_116, L_117, L_118, L_119, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t1591091833_il2cpp_TypeInfo_var);
		ImporterFunc_t850687278 * L_120 = ((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->get_U3CU3E9__25_12_24();
		ImporterFunc_t850687278 * L_121 = L_120;
		G_B25_0 = L_121;
		if (L_121)
		{
			G_B26_0 = L_121;
			goto IL_0313;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t1591091833_il2cpp_TypeInfo_var);
		U3CU3Ec_t1591091833 * L_122 = ((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->get_U3CU3E9_0();
		IntPtr_t L_123;
		L_123.set_m_value_0((void*)(void*)U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_12_m2105962245_MethodInfo_var);
		ImporterFunc_t850687278 * L_124 = (ImporterFunc_t850687278 *)il2cpp_codegen_object_new(ImporterFunc_t850687278_il2cpp_TypeInfo_var);
		ImporterFunc__ctor_m637091809(L_124, L_122, L_123, /*hidden argument*/NULL);
		ImporterFunc_t850687278 * L_125 = L_124;
		((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->set_U3CU3E9__25_12_24(L_125);
		G_B26_0 = L_125;
	}

IL_0313:
	{
		V_0 = G_B26_0;
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t77474459_il2cpp_TypeInfo_var);
		Il2CppObject* L_126 = ((JsonMapper_t77474459_StaticFields*)JsonMapper_t77474459_il2cpp_TypeInfo_var->static_fields)->get_base_importers_table_4();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_127 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_128 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(DateTime_t693205669_0_0_0_var), /*hidden argument*/NULL);
		ImporterFunc_t850687278 * L_129 = V_0;
		JsonMapper_RegisterImporter_m1039024633(NULL /*static, unused*/, L_126, L_127, L_128, L_129, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t1591091833_il2cpp_TypeInfo_var);
		ImporterFunc_t850687278 * L_130 = ((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->get_U3CU3E9__25_13_25();
		ImporterFunc_t850687278 * L_131 = L_130;
		G_B27_0 = L_131;
		if (L_131)
		{
			G_B28_0 = L_131;
			goto IL_0352;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t1591091833_il2cpp_TypeInfo_var);
		U3CU3Ec_t1591091833 * L_132 = ((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->get_U3CU3E9_0();
		IntPtr_t L_133;
		L_133.set_m_value_0((void*)(void*)U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_13_m2617176164_MethodInfo_var);
		ImporterFunc_t850687278 * L_134 = (ImporterFunc_t850687278 *)il2cpp_codegen_object_new(ImporterFunc_t850687278_il2cpp_TypeInfo_var);
		ImporterFunc__ctor_m637091809(L_134, L_132, L_133, /*hidden argument*/NULL);
		ImporterFunc_t850687278 * L_135 = L_134;
		((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->set_U3CU3E9__25_13_25(L_135);
		G_B28_0 = L_135;
	}

IL_0352:
	{
		V_0 = G_B28_0;
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t77474459_il2cpp_TypeInfo_var);
		Il2CppObject* L_136 = ((JsonMapper_t77474459_StaticFields*)JsonMapper_t77474459_il2cpp_TypeInfo_var->static_fields)->get_base_importers_table_4();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_137 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Int32_t2071877448_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_138 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Int64_t909078037_0_0_0_var), /*hidden argument*/NULL);
		ImporterFunc_t850687278 * L_139 = V_0;
		JsonMapper_RegisterImporter_m1039024633(NULL /*static, unused*/, L_136, L_137, L_138, L_139, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ThirdParty.Json.LitJson.JsonMapper::RegisterImporter(System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,ThirdParty.Json.LitJson.ImporterFunc>>,System.Type,System.Type,ThirdParty.Json.LitJson.ImporterFunc)
extern Il2CppClass* IDictionary_2_t723569914_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t2788045175_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_2_t787128596_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1880985693_MethodInfo_var;
extern const uint32_t JsonMapper_RegisterImporter_m1039024633_MetadataUsageId;
extern "C"  void JsonMapper_RegisterImporter_m1039024633 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___table0, Type_t * ___json_type1, Type_t * ___value_type2, ImporterFunc_t850687278 * ___importer3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_RegisterImporter_m1039024633_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject* L_0 = ___table0;
		Type_t * L_1 = ___json_type1;
		NullCheck(L_0);
		bool L_2 = InterfaceFuncInvoker1< bool, Type_t * >::Invoke(1 /* System.Boolean System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,ThirdParty.Json.LitJson.ImporterFunc>>::ContainsKey(!0) */, IDictionary_2_t723569914_il2cpp_TypeInfo_var, L_0, L_1);
		if (L_2)
		{
			goto IL_0015;
		}
	}
	{
		Il2CppObject* L_3 = ___table0;
		Type_t * L_4 = ___json_type1;
		Dictionary_2_t2788045175 * L_5 = (Dictionary_2_t2788045175 *)il2cpp_codegen_object_new(Dictionary_2_t2788045175_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1880985693(L_5, /*hidden argument*/Dictionary_2__ctor_m1880985693_MethodInfo_var);
		NullCheck(L_3);
		InterfaceActionInvoker2< Type_t *, Il2CppObject* >::Invoke(0 /* System.Void System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,ThirdParty.Json.LitJson.ImporterFunc>>::Add(!0,!1) */, IDictionary_2_t723569914_il2cpp_TypeInfo_var, L_3, L_4, L_5);
	}

IL_0015:
	{
		Il2CppObject* L_6 = ___table0;
		Type_t * L_7 = ___json_type1;
		NullCheck(L_6);
		Il2CppObject* L_8 = InterfaceFuncInvoker1< Il2CppObject*, Type_t * >::Invoke(4 /* !1 System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,ThirdParty.Json.LitJson.ImporterFunc>>::get_Item(!0) */, IDictionary_2_t723569914_il2cpp_TypeInfo_var, L_6, L_7);
		Type_t * L_9 = ___value_type2;
		ImporterFunc_t850687278 * L_10 = ___importer3;
		NullCheck(L_8);
		InterfaceActionInvoker2< Type_t *, ImporterFunc_t850687278 * >::Invoke(5 /* System.Void System.Collections.Generic.IDictionary`2<System.Type,ThirdParty.Json.LitJson.ImporterFunc>::set_Item(!0,!1) */, IDictionary_2_t787128596_il2cpp_TypeInfo_var, L_8, L_9, L_10);
		return;
	}
}
// ThirdParty.Json.LitJson.JsonData ThirdParty.Json.LitJson.JsonMapper::ToObject(System.IO.TextReader)
extern Il2CppClass* JsonReader_t354941621_il2cpp_TypeInfo_var;
extern Il2CppClass* U3CU3Ec_t1591091833_il2cpp_TypeInfo_var;
extern Il2CppClass* WrapperFactory_t327905379_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonMapper_t77474459_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonData_t4263252052_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CU3Ec_U3CToObjectU3Eb__31_0_m3161780688_MethodInfo_var;
extern const uint32_t JsonMapper_ToObject_m233897228_MetadataUsageId;
extern "C"  JsonData_t4263252052 * JsonMapper_ToObject_m233897228 (Il2CppObject * __this /* static, unused */, TextReader_t1561828458 * ___reader0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_ToObject_m233897228_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JsonReader_t354941621 * V_0 = NULL;
	WrapperFactory_t327905379 * G_B2_0 = NULL;
	WrapperFactory_t327905379 * G_B1_0 = NULL;
	{
		TextReader_t1561828458 * L_0 = ___reader0;
		JsonReader_t354941621 * L_1 = (JsonReader_t354941621 *)il2cpp_codegen_object_new(JsonReader_t354941621_il2cpp_TypeInfo_var);
		JsonReader__ctor_m690207655(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t1591091833_il2cpp_TypeInfo_var);
		WrapperFactory_t327905379 * L_2 = ((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->get_U3CU3E9__31_0_26();
		WrapperFactory_t327905379 * L_3 = L_2;
		G_B1_0 = L_3;
		if (L_3)
		{
			G_B2_0 = L_3;
			goto IL_0026;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t1591091833_il2cpp_TypeInfo_var);
		U3CU3Ec_t1591091833 * L_4 = ((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->get_U3CU3E9_0();
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)U3CU3Ec_U3CToObjectU3Eb__31_0_m3161780688_MethodInfo_var);
		WrapperFactory_t327905379 * L_6 = (WrapperFactory_t327905379 *)il2cpp_codegen_object_new(WrapperFactory_t327905379_il2cpp_TypeInfo_var);
		WrapperFactory__ctor_m632703600(L_6, L_4, L_5, /*hidden argument*/NULL);
		WrapperFactory_t327905379 * L_7 = L_6;
		((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->set_U3CU3E9__31_0_26(L_7);
		G_B2_0 = L_7;
	}

IL_0026:
	{
		JsonReader_t354941621 * L_8 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t77474459_il2cpp_TypeInfo_var);
		Il2CppObject * L_9 = JsonMapper_ToWrapper_m3543010123(NULL /*static, unused*/, G_B2_0, L_8, /*hidden argument*/NULL);
		return ((JsonData_t4263252052 *)CastclassClass(L_9, JsonData_t4263252052_il2cpp_TypeInfo_var));
	}
}
// ThirdParty.Json.LitJson.JsonData ThirdParty.Json.LitJson.JsonMapper::ToObject(System.String)
extern Il2CppClass* U3CU3Ec_t1591091833_il2cpp_TypeInfo_var;
extern Il2CppClass* WrapperFactory_t327905379_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonMapper_t77474459_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonData_t4263252052_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CU3Ec_U3CToObjectU3Eb__32_0_m464330091_MethodInfo_var;
extern const uint32_t JsonMapper_ToObject_m515800907_MetadataUsageId;
extern "C"  JsonData_t4263252052 * JsonMapper_ToObject_m515800907 (Il2CppObject * __this /* static, unused */, String_t* ___json0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_ToObject_m515800907_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	WrapperFactory_t327905379 * G_B2_0 = NULL;
	WrapperFactory_t327905379 * G_B1_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t1591091833_il2cpp_TypeInfo_var);
		WrapperFactory_t327905379 * L_0 = ((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->get_U3CU3E9__32_0_27();
		WrapperFactory_t327905379 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t1591091833_il2cpp_TypeInfo_var);
		U3CU3Ec_t1591091833 * L_2 = ((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->get_U3CU3E9_0();
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)U3CU3Ec_U3CToObjectU3Eb__32_0_m464330091_MethodInfo_var);
		WrapperFactory_t327905379 * L_4 = (WrapperFactory_t327905379 *)il2cpp_codegen_object_new(WrapperFactory_t327905379_il2cpp_TypeInfo_var);
		WrapperFactory__ctor_m632703600(L_4, L_2, L_3, /*hidden argument*/NULL);
		WrapperFactory_t327905379 * L_5 = L_4;
		((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->set_U3CU3E9__32_0_27(L_5);
		G_B2_0 = L_5;
	}

IL_001f:
	{
		String_t* L_6 = ___json0;
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t77474459_il2cpp_TypeInfo_var);
		Il2CppObject * L_7 = JsonMapper_ToWrapper_m1983722396(NULL /*static, unused*/, G_B2_0, L_6, /*hidden argument*/NULL);
		return ((JsonData_t4263252052 *)CastclassClass(L_7, JsonData_t4263252052_il2cpp_TypeInfo_var));
	}
}
// ThirdParty.Json.LitJson.IJsonWrapper ThirdParty.Json.LitJson.JsonMapper::ToWrapper(ThirdParty.Json.LitJson.WrapperFactory,ThirdParty.Json.LitJson.JsonReader)
extern Il2CppClass* JsonMapper_t77474459_il2cpp_TypeInfo_var;
extern const uint32_t JsonMapper_ToWrapper_m3543010123_MetadataUsageId;
extern "C"  Il2CppObject * JsonMapper_ToWrapper_m3543010123 (Il2CppObject * __this /* static, unused */, WrapperFactory_t327905379 * ___factory0, JsonReader_t354941621 * ___reader1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_ToWrapper_m3543010123_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		WrapperFactory_t327905379 * L_0 = ___factory0;
		JsonReader_t354941621 * L_1 = ___reader1;
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t77474459_il2cpp_TypeInfo_var);
		Il2CppObject * L_2 = JsonMapper_ReadValue_m2812052028(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// ThirdParty.Json.LitJson.IJsonWrapper ThirdParty.Json.LitJson.JsonMapper::ToWrapper(ThirdParty.Json.LitJson.WrapperFactory,System.String)
extern Il2CppClass* JsonReader_t354941621_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonMapper_t77474459_il2cpp_TypeInfo_var;
extern const uint32_t JsonMapper_ToWrapper_m1983722396_MetadataUsageId;
extern "C"  Il2CppObject * JsonMapper_ToWrapper_m1983722396 (Il2CppObject * __this /* static, unused */, WrapperFactory_t327905379 * ___factory0, String_t* ___json1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_ToWrapper_m1983722396_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JsonReader_t354941621 * V_0 = NULL;
	{
		String_t* L_0 = ___json1;
		JsonReader_t354941621 * L_1 = (JsonReader_t354941621 *)il2cpp_codegen_object_new(JsonReader_t354941621_il2cpp_TypeInfo_var);
		JsonReader__ctor_m4018925912(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		WrapperFactory_t327905379 * L_2 = ___factory0;
		JsonReader_t354941621 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t77474459_il2cpp_TypeInfo_var);
		Il2CppObject * L_4 = JsonMapper_ReadValue_m2812052028(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void ThirdParty.Json.LitJson.JsonMapper/<>c::.cctor()
extern Il2CppClass* U3CU3Ec_t1591091833_il2cpp_TypeInfo_var;
extern const uint32_t U3CU3Ec__cctor_m1749976183_MetadataUsageId;
extern "C"  void U3CU3Ec__cctor_m1749976183 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__cctor_m1749976183_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_t1591091833 * L_0 = (U3CU3Ec_t1591091833 *)il2cpp_codegen_object_new(U3CU3Ec_t1591091833_il2cpp_TypeInfo_var);
		U3CU3Ec__ctor_m48908184(L_0, /*hidden argument*/NULL);
		((U3CU3Ec_t1591091833_StaticFields*)U3CU3Ec_t1591091833_il2cpp_TypeInfo_var->static_fields)->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void ThirdParty.Json.LitJson.JsonMapper/<>c::.ctor()
extern "C"  void U3CU3Ec__ctor_m48908184 (U3CU3Ec_t1591091833 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseExporters>b__24_0(System.Object,ThirdParty.Json.LitJson.JsonWriter)
extern Il2CppClass* Byte_t3683104436_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern const uint32_t U3CU3Ec_U3CRegisterBaseExportersU3Eb__24_0_m3647892888_MetadataUsageId;
extern "C"  void U3CU3Ec_U3CRegisterBaseExportersU3Eb__24_0_m3647892888 (U3CU3Ec_t1591091833 * __this, Il2CppObject * ___obj0, JsonWriter_t3014444111 * ___writer1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec_U3CRegisterBaseExportersU3Eb__24_0_m3647892888_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		JsonWriter_t3014444111 * L_0 = ___writer1;
		Il2CppObject * L_1 = ___obj0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		int32_t L_2 = Convert_ToInt32_m3487847046(NULL /*static, unused*/, ((*(uint8_t*)((uint8_t*)UnBox (L_1, Byte_t3683104436_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		NullCheck(L_0);
		JsonWriter_Write_m2327906996(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseExporters>b__24_1(System.Object,ThirdParty.Json.LitJson.JsonWriter)
extern Il2CppClass* Char_t3454481338_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern const uint32_t U3CU3Ec_U3CRegisterBaseExportersU3Eb__24_1_m403640019_MetadataUsageId;
extern "C"  void U3CU3Ec_U3CRegisterBaseExportersU3Eb__24_1_m403640019 (U3CU3Ec_t1591091833 * __this, Il2CppObject * ___obj0, JsonWriter_t3014444111 * ___writer1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec_U3CRegisterBaseExportersU3Eb__24_1_m403640019_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		JsonWriter_t3014444111 * L_0 = ___writer1;
		Il2CppObject * L_1 = ___obj0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		String_t* L_2 = Convert_ToString_m3532930908(NULL /*static, unused*/, ((*(Il2CppChar*)((Il2CppChar*)UnBox (L_1, Char_t3454481338_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		NullCheck(L_0);
		JsonWriter_Write_m3974344637(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseExporters>b__24_2(System.Object,ThirdParty.Json.LitJson.JsonWriter)
extern Il2CppClass* DateTime_t693205669_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonMapper_t77474459_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern const uint32_t U3CU3Ec_U3CRegisterBaseExportersU3Eb__24_2_m626278178_MetadataUsageId;
extern "C"  void U3CU3Ec_U3CRegisterBaseExportersU3Eb__24_2_m626278178 (U3CU3Ec_t1591091833 * __this, Il2CppObject * ___obj0, JsonWriter_t3014444111 * ___writer1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec_U3CRegisterBaseExportersU3Eb__24_2_m626278178_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		JsonWriter_t3014444111 * L_0 = ___writer1;
		Il2CppObject * L_1 = ___obj0;
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t77474459_il2cpp_TypeInfo_var);
		Il2CppObject * L_2 = ((JsonMapper_t77474459_StaticFields*)JsonMapper_t77474459_il2cpp_TypeInfo_var->static_fields)->get_datetime_format_1();
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		String_t* L_3 = Convert_ToString_m1913821633(NULL /*static, unused*/, ((*(DateTime_t693205669 *)((DateTime_t693205669 *)UnBox (L_1, DateTime_t693205669_il2cpp_TypeInfo_var)))), L_2, /*hidden argument*/NULL);
		NullCheck(L_0);
		JsonWriter_Write_m3974344637(L_0, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseExporters>b__24_3(System.Object,ThirdParty.Json.LitJson.JsonWriter)
extern Il2CppClass* Decimal_t724701077_il2cpp_TypeInfo_var;
extern const uint32_t U3CU3Ec_U3CRegisterBaseExportersU3Eb__24_3_m564382813_MetadataUsageId;
extern "C"  void U3CU3Ec_U3CRegisterBaseExportersU3Eb__24_3_m564382813 (U3CU3Ec_t1591091833 * __this, Il2CppObject * ___obj0, JsonWriter_t3014444111 * ___writer1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec_U3CRegisterBaseExportersU3Eb__24_3_m564382813_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		JsonWriter_t3014444111 * L_0 = ___writer1;
		Il2CppObject * L_1 = ___obj0;
		NullCheck(L_0);
		JsonWriter_Write_m580542055(L_0, ((*(Decimal_t724701077 *)((Decimal_t724701077 *)UnBox (L_1, Decimal_t724701077_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		return;
	}
}
// System.Void ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseExporters>b__24_4(System.Object,ThirdParty.Json.LitJson.JsonWriter)
extern Il2CppClass* SByte_t454417549_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern const uint32_t U3CU3Ec_U3CRegisterBaseExportersU3Eb__24_4_m3305872516_MetadataUsageId;
extern "C"  void U3CU3Ec_U3CRegisterBaseExportersU3Eb__24_4_m3305872516 (U3CU3Ec_t1591091833 * __this, Il2CppObject * ___obj0, JsonWriter_t3014444111 * ___writer1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec_U3CRegisterBaseExportersU3Eb__24_4_m3305872516_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		JsonWriter_t3014444111 * L_0 = ___writer1;
		Il2CppObject * L_1 = ___obj0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		int32_t L_2 = Convert_ToInt32_m2209788057(NULL /*static, unused*/, ((*(int8_t*)((int8_t*)UnBox (L_1, SByte_t454417549_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		NullCheck(L_0);
		JsonWriter_Write_m2327906996(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseExporters>b__24_5(System.Object,ThirdParty.Json.LitJson.JsonWriter)
extern Il2CppClass* Int16_t4041245914_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern const uint32_t U3CU3Ec_U3CRegisterBaseExportersU3Eb__24_5_m61619647_MetadataUsageId;
extern "C"  void U3CU3Ec_U3CRegisterBaseExportersU3Eb__24_5_m61619647 (U3CU3Ec_t1591091833 * __this, Il2CppObject * ___obj0, JsonWriter_t3014444111 * ___writer1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec_U3CRegisterBaseExportersU3Eb__24_5_m61619647_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		JsonWriter_t3014444111 * L_0 = ___writer1;
		Il2CppObject * L_1 = ___obj0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		int32_t L_2 = Convert_ToInt32_m2297793940(NULL /*static, unused*/, ((*(int16_t*)((int16_t*)UnBox (L_1, Int16_t4041245914_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		NullCheck(L_0);
		JsonWriter_Write_m2327906996(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseExporters>b__24_6(System.Object,ThirdParty.Json.LitJson.JsonWriter)
extern Il2CppClass* UInt16_t986882611_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern const uint32_t U3CU3Ec_U3CRegisterBaseExportersU3Eb__24_6_m284257806_MetadataUsageId;
extern "C"  void U3CU3Ec_U3CRegisterBaseExportersU3Eb__24_6_m284257806 (U3CU3Ec_t1591091833 * __this, Il2CppObject * ___obj0, JsonWriter_t3014444111 * ___writer1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec_U3CRegisterBaseExportersU3Eb__24_6_m284257806_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		JsonWriter_t3014444111 * L_0 = ___writer1;
		Il2CppObject * L_1 = ___obj0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		int32_t L_2 = Convert_ToInt32_m1805519569(NULL /*static, unused*/, ((*(uint16_t*)((uint16_t*)UnBox (L_1, UInt16_t986882611_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		NullCheck(L_0);
		JsonWriter_Write_m2327906996(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseExporters>b__24_7(System.Object,ThirdParty.Json.LitJson.JsonWriter)
extern Il2CppClass* UInt32_t2149682021_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern const uint32_t U3CU3Ec_U3CRegisterBaseExportersU3Eb__24_7_m222362441_MetadataUsageId;
extern "C"  void U3CU3Ec_U3CRegisterBaseExportersU3Eb__24_7_m222362441 (U3CU3Ec_t1591091833 * __this, Il2CppObject * ___obj0, JsonWriter_t3014444111 * ___writer1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec_U3CRegisterBaseExportersU3Eb__24_7_m222362441_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		JsonWriter_t3014444111 * L_0 = ___writer1;
		Il2CppObject * L_1 = ___obj0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		uint64_t L_2 = Convert_ToUInt64_m1109896503(NULL /*static, unused*/, ((*(uint32_t*)((uint32_t*)UnBox (L_1, UInt32_t2149682021_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		NullCheck(L_0);
		JsonWriter_Write_m281441472(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseExporters>b__24_8(System.Object,ThirdParty.Json.LitJson.JsonWriter)
extern Il2CppClass* UInt64_t2909196914_il2cpp_TypeInfo_var;
extern const uint32_t U3CU3Ec_U3CRegisterBaseExportersU3Eb__24_8_m931111872_MetadataUsageId;
extern "C"  void U3CU3Ec_U3CRegisterBaseExportersU3Eb__24_8_m931111872 (U3CU3Ec_t1591091833 * __this, Il2CppObject * ___obj0, JsonWriter_t3014444111 * ___writer1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec_U3CRegisterBaseExportersU3Eb__24_8_m931111872_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		JsonWriter_t3014444111 * L_0 = ___writer1;
		Il2CppObject * L_1 = ___obj0;
		NullCheck(L_0);
		JsonWriter_Write_m281441472(L_0, ((*(uint64_t*)((uint64_t*)UnBox (L_1, UInt64_t2909196914_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		return;
	}
}
// System.Void ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseExporters>b__24_9(System.Object,ThirdParty.Json.LitJson.JsonWriter)
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern const uint32_t U3CU3Ec_U3CRegisterBaseExportersU3Eb__24_9_m1981826299_MetadataUsageId;
extern "C"  void U3CU3Ec_U3CRegisterBaseExportersU3Eb__24_9_m1981826299 (U3CU3Ec_t1591091833 * __this, Il2CppObject * ___obj0, JsonWriter_t3014444111 * ___writer1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec_U3CRegisterBaseExportersU3Eb__24_9_m1981826299_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		JsonWriter_t3014444111 * L_0 = ___writer1;
		Il2CppObject * L_1 = ___obj0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		double L_2 = Convert_ToDouble_m2956884076(NULL /*static, unused*/, ((*(float*)((float*)UnBox (L_1, Single_t2076509932_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		NullCheck(L_0);
		JsonWriter_Write_m1257361297(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseExporters>b__24_10(System.Object,ThirdParty.Json.LitJson.JsonWriter)
extern Il2CppClass* Int64_t909078037_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern const uint32_t U3CU3Ec_U3CRegisterBaseExportersU3Eb__24_10_m3919046323_MetadataUsageId;
extern "C"  void U3CU3Ec_U3CRegisterBaseExportersU3Eb__24_10_m3919046323 (U3CU3Ec_t1591091833 * __this, Il2CppObject * ___obj0, JsonWriter_t3014444111 * ___writer1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec_U3CRegisterBaseExportersU3Eb__24_10_m3919046323_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		JsonWriter_t3014444111 * L_0 = ___writer1;
		Il2CppObject * L_1 = ___obj0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		double L_2 = Convert_ToDouble_m2357261297(NULL /*static, unused*/, ((*(int64_t*)((int64_t*)UnBox (L_1, Int64_t909078037_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		NullCheck(L_0);
		JsonWriter_Write_m1257361297(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Object ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseImporters>b__25_0(System.Object)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern Il2CppClass* Byte_t3683104436_il2cpp_TypeInfo_var;
extern const uint32_t U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_0_m2312218722_MetadataUsageId;
extern "C"  Il2CppObject * U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_0_m2312218722 (U3CU3Ec_t1591091833 * __this, Il2CppObject * ___input0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_0_m2312218722_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___input0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		uint8_t L_1 = Convert_ToByte_m1201588964(NULL /*static, unused*/, ((*(int32_t*)((int32_t*)UnBox (L_0, Int32_t2071877448_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		uint8_t L_2 = L_1;
		Il2CppObject * L_3 = Box(Byte_t3683104436_il2cpp_TypeInfo_var, &L_2);
		return L_3;
	}
}
// System.Object ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseImporters>b__25_1(System.Object)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt64_t2909196914_il2cpp_TypeInfo_var;
extern const uint32_t U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_1_m542108775_MetadataUsageId;
extern "C"  Il2CppObject * U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_1_m542108775 (U3CU3Ec_t1591091833 * __this, Il2CppObject * ___input0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_1_m542108775_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___input0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		uint64_t L_1 = Convert_ToUInt64_m1691869628(NULL /*static, unused*/, ((*(int32_t*)((int32_t*)UnBox (L_0, Int32_t2071877448_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		uint64_t L_2 = L_1;
		Il2CppObject * L_3 = Box(UInt64_t2909196914_il2cpp_TypeInfo_var, &L_2);
		return L_3;
	}
}
// System.Object ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseImporters>b__25_2(System.Object)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern Il2CppClass* SByte_t454417549_il2cpp_TypeInfo_var;
extern const uint32_t U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_2_m2023383788_MetadataUsageId;
extern "C"  Il2CppObject * U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_2_m2023383788 (U3CU3Ec_t1591091833 * __this, Il2CppObject * ___input0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_2_m2023383788_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___input0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		int8_t L_1 = Convert_ToSByte_m3208712702(NULL /*static, unused*/, ((*(int32_t*)((int32_t*)UnBox (L_0, Int32_t2071877448_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		int8_t L_2 = L_1;
		Il2CppObject * L_3 = Box(SByte_t454417549_il2cpp_TypeInfo_var, &L_2);
		return L_3;
	}
}
// System.Object ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseImporters>b__25_3(System.Object)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern Il2CppClass* Int16_t4041245914_il2cpp_TypeInfo_var;
extern const uint32_t U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_3_m253273841_MetadataUsageId;
extern "C"  Il2CppObject * U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_3_m253273841 (U3CU3Ec_t1591091833 * __this, Il2CppObject * ___input0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_3_m253273841_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___input0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		int16_t L_1 = Convert_ToInt16_m3374388286(NULL /*static, unused*/, ((*(int32_t*)((int32_t*)UnBox (L_0, Int32_t2071877448_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		int16_t L_2 = L_1;
		Il2CppObject * L_3 = Box(Int16_t4041245914_il2cpp_TypeInfo_var, &L_2);
		return L_3;
	}
}
// System.Object ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseImporters>b__25_4(System.Object)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt16_t986882611_il2cpp_TypeInfo_var;
extern const uint32_t U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_4_m785232718_MetadataUsageId;
extern "C"  Il2CppObject * U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_4_m785232718 (U3CU3Ec_t1591091833 * __this, Il2CppObject * ___input0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_4_m785232718_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___input0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		uint16_t L_1 = Convert_ToUInt16_m1665900934(NULL /*static, unused*/, ((*(int32_t*)((int32_t*)UnBox (L_0, Int32_t2071877448_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		uint16_t L_2 = L_1;
		Il2CppObject * L_3 = Box(UInt16_t986882611_il2cpp_TypeInfo_var, &L_2);
		return L_3;
	}
}
// System.Object ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseImporters>b__25_5(System.Object)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt32_t2149682021_il2cpp_TypeInfo_var;
extern const uint32_t U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_5_m3310090067_MetadataUsageId;
extern "C"  Il2CppObject * U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_5_m3310090067 (U3CU3Ec_t1591091833 * __this, Il2CppObject * ___input0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_5_m3310090067_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___input0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		uint32_t L_1 = Convert_ToUInt32_m3085255294(NULL /*static, unused*/, ((*(int32_t*)((int32_t*)UnBox (L_0, Int32_t2071877448_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		uint32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(UInt32_t2149682021_il2cpp_TypeInfo_var, &L_2);
		return L_3;
	}
}
// System.Object ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseImporters>b__25_6(System.Object)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern const uint32_t U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_6_m496397784_MetadataUsageId;
extern "C"  Il2CppObject * U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_6_m496397784 (U3CU3Ec_t1591091833 * __this, Il2CppObject * ___input0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_6_m496397784_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___input0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		float L_1 = Convert_ToSingle_m915696580(NULL /*static, unused*/, ((*(int32_t*)((int32_t*)UnBox (L_0, Int32_t2071877448_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		float L_2 = L_1;
		Il2CppObject * L_3 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_2);
		return L_3;
	}
}
// System.Object ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseImporters>b__25_7(System.Object)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern Il2CppClass* Double_t4078015681_il2cpp_TypeInfo_var;
extern const uint32_t U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_7_m3021255133_MetadataUsageId;
extern "C"  Il2CppObject * U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_7_m3021255133 (U3CU3Ec_t1591091833 * __this, Il2CppObject * ___input0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_7_m3021255133_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___input0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		double L_1 = Convert_ToDouble_m1550692470(NULL /*static, unused*/, ((*(int32_t*)((int32_t*)UnBox (L_0, Int32_t2071877448_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		double L_2 = L_1;
		Il2CppObject * L_3 = Box(Double_t4078015681_il2cpp_TypeInfo_var, &L_2);
		return L_3;
	}
}
// System.Object ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseImporters>b__25_8(System.Object)
extern Il2CppClass* Double_t4078015681_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern Il2CppClass* Decimal_t724701077_il2cpp_TypeInfo_var;
extern const uint32_t U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_8_m3565818426_MetadataUsageId;
extern "C"  Il2CppObject * U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_8_m3565818426 (U3CU3Ec_t1591091833 * __this, Il2CppObject * ___input0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_8_m3565818426_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___input0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		Decimal_t724701077  L_1 = Convert_ToDecimal_m1354200421(NULL /*static, unused*/, ((*(double*)((double*)UnBox (L_0, Double_t4078015681_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		Decimal_t724701077  L_2 = L_1;
		Il2CppObject * L_3 = Box(Decimal_t724701077_il2cpp_TypeInfo_var, &L_2);
		return L_3;
	}
}
// System.Object ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseImporters>b__25_9(System.Object)
extern Il2CppClass* Double_t4078015681_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern const uint32_t U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_9_m1795708479_MetadataUsageId;
extern "C"  Il2CppObject * U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_9_m1795708479 (U3CU3Ec_t1591091833 * __this, Il2CppObject * ___input0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_9_m1795708479_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___input0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		float L_1 = Convert_ToSingle_m3602825646(NULL /*static, unused*/, (((float)((float)((*(double*)((double*)UnBox (L_0, Double_t4078015681_il2cpp_TypeInfo_var))))))), /*hidden argument*/NULL);
		float L_2 = L_1;
		Il2CppObject * L_3 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_2);
		return L_3;
	}
}
// System.Object ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseImporters>b__25_10(System.Object)
extern Il2CppClass* Int64_t909078037_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt32_t2149682021_il2cpp_TypeInfo_var;
extern const uint32_t U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_10_m1231844419_MetadataUsageId;
extern "C"  Il2CppObject * U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_10_m1231844419 (U3CU3Ec_t1591091833 * __this, Il2CppObject * ___input0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_10_m1231844419_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___input0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		uint32_t L_1 = Convert_ToUInt32_m1922455781(NULL /*static, unused*/, ((*(int64_t*)((int64_t*)UnBox (L_0, Int64_t909078037_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		uint32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(UInt32_t2149682021_il2cpp_TypeInfo_var, &L_2);
		return L_3;
	}
}
// System.Object ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseImporters>b__25_11(System.Object)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern Il2CppClass* Char_t3454481338_il2cpp_TypeInfo_var;
extern const uint32_t U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_11_m1950251426_MetadataUsageId;
extern "C"  Il2CppObject * U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_11_m1950251426 (U3CU3Ec_t1591091833 * __this, Il2CppObject * ___input0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_11_m1950251426_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___input0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		Il2CppChar L_1 = Convert_ToChar_m2864089609(NULL /*static, unused*/, ((String_t*)CastclassSealed(L_0, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		Il2CppChar L_2 = L_1;
		Il2CppObject * L_3 = Box(Char_t3454481338_il2cpp_TypeInfo_var, &L_2);
		return L_3;
	}
}
// System.Object ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseImporters>b__25_12(System.Object)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonMapper_t77474459_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTime_t693205669_il2cpp_TypeInfo_var;
extern const uint32_t U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_12_m2105962245_MetadataUsageId;
extern "C"  Il2CppObject * U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_12_m2105962245 (U3CU3Ec_t1591091833 * __this, Il2CppObject * ___input0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_12_m2105962245_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___input0;
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t77474459_il2cpp_TypeInfo_var);
		Il2CppObject * L_1 = ((JsonMapper_t77474459_StaticFields*)JsonMapper_t77474459_il2cpp_TypeInfo_var->static_fields)->get_datetime_format_1();
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		DateTime_t693205669  L_2 = Convert_ToDateTime_m261774113(NULL /*static, unused*/, ((String_t*)CastclassSealed(L_0, String_t_il2cpp_TypeInfo_var)), L_1, /*hidden argument*/NULL);
		DateTime_t693205669  L_3 = L_2;
		Il2CppObject * L_4 = Box(DateTime_t693205669_il2cpp_TypeInfo_var, &L_3);
		return L_4;
	}
}
// System.Object ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseImporters>b__25_13(System.Object)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern Il2CppClass* Int64_t909078037_il2cpp_TypeInfo_var;
extern const uint32_t U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_13_m2617176164_MetadataUsageId;
extern "C"  Il2CppObject * U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_13_m2617176164 (U3CU3Ec_t1591091833 * __this, Il2CppObject * ___input0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_13_m2617176164_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___input0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		int64_t L_1 = Convert_ToInt64_m2310639998(NULL /*static, unused*/, ((*(int32_t*)((int32_t*)UnBox (L_0, Int32_t2071877448_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		int64_t L_2 = L_1;
		Il2CppObject * L_3 = Box(Int64_t909078037_il2cpp_TypeInfo_var, &L_2);
		return L_3;
	}
}
// ThirdParty.Json.LitJson.IJsonWrapper ThirdParty.Json.LitJson.JsonMapper/<>c::<ToObject>b__31_0()
extern Il2CppClass* JsonData_t4263252052_il2cpp_TypeInfo_var;
extern const uint32_t U3CU3Ec_U3CToObjectU3Eb__31_0_m3161780688_MetadataUsageId;
extern "C"  Il2CppObject * U3CU3Ec_U3CToObjectU3Eb__31_0_m3161780688 (U3CU3Ec_t1591091833 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec_U3CToObjectU3Eb__31_0_m3161780688_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		JsonData_t4263252052 * L_0 = (JsonData_t4263252052 *)il2cpp_codegen_object_new(JsonData_t4263252052_il2cpp_TypeInfo_var);
		JsonData__ctor_m2183287813(L_0, /*hidden argument*/NULL);
		return L_0;
	}
}
// ThirdParty.Json.LitJson.IJsonWrapper ThirdParty.Json.LitJson.JsonMapper/<>c::<ToObject>b__32_0()
extern Il2CppClass* JsonData_t4263252052_il2cpp_TypeInfo_var;
extern const uint32_t U3CU3Ec_U3CToObjectU3Eb__32_0_m464330091_MetadataUsageId;
extern "C"  Il2CppObject * U3CU3Ec_U3CToObjectU3Eb__32_0_m464330091 (U3CU3Ec_t1591091833 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec_U3CToObjectU3Eb__32_0_m464330091_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		JsonData_t4263252052 * L_0 = (JsonData_t4263252052 *)il2cpp_codegen_object_new(JsonData_t4263252052_il2cpp_TypeInfo_var);
		JsonData__ctor_m2183287813(L_0, /*hidden argument*/NULL);
		return L_0;
	}
}
// ThirdParty.Json.LitJson.JsonToken ThirdParty.Json.LitJson.JsonReader::get_Token()
extern "C"  int32_t JsonReader_get_Token_m2164259962 (JsonReader_t354941621 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_token_12();
		return L_0;
	}
}
// System.Object ThirdParty.Json.LitJson.JsonReader::get_Value()
extern "C"  Il2CppObject * JsonReader_get_Value_m158192595 (JsonReader_t354941621 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_token_value_11();
		return L_0;
	}
}
// System.Void ThirdParty.Json.LitJson.JsonReader::.ctor(System.String)
extern Il2CppClass* StringReader_t1480123486_il2cpp_TypeInfo_var;
extern const uint32_t JsonReader__ctor_m4018925912_MetadataUsageId;
extern "C"  void JsonReader__ctor_m4018925912 (JsonReader_t354941621 * __this, String_t* ___json_text0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonReader__ctor_m4018925912_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___json_text0;
		StringReader_t1480123486 * L_1 = (StringReader_t1480123486 *)il2cpp_codegen_object_new(StringReader_t1480123486_il2cpp_TypeInfo_var);
		StringReader__ctor_m643998729(L_1, L_0, /*hidden argument*/NULL);
		JsonReader__ctor_m1593852644(__this, L_1, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ThirdParty.Json.LitJson.JsonReader::.ctor(System.IO.TextReader)
extern "C"  void JsonReader__ctor_m690207655 (JsonReader_t354941621 * __this, TextReader_t1561828458 * ___reader0, const MethodInfo* method)
{
	{
		TextReader_t1561828458 * L_0 = ___reader0;
		JsonReader__ctor_m1593852644(__this, L_0, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ThirdParty.Json.LitJson.JsonReader::.ctor(System.IO.TextReader,System.Boolean)
extern Il2CppClass* Stack_1_t3533309409_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppClass* Lexer_t954714164_il2cpp_TypeInfo_var;
extern const MethodInfo* Stack_1__ctor_m3467349316_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral51169761;
extern const uint32_t JsonReader__ctor_m1593852644_MetadataUsageId;
extern "C"  void JsonReader__ctor_m1593852644 (JsonReader_t354941621 * __this, TextReader_t1561828458 * ___reader0, bool ___owned1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonReader__ctor_m1593852644_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Stack_1_t3533309409 * L_0 = (Stack_1_t3533309409 *)il2cpp_codegen_object_new(Stack_1_t3533309409_il2cpp_TypeInfo_var);
		Stack_1__ctor_m3467349316(L_0, /*hidden argument*/Stack_1__ctor_m3467349316_MethodInfo_var);
		__this->set_depth_0(L_0);
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		TextReader_t1561828458 * L_1 = ___reader0;
		if (L_1)
		{
			goto IL_001f;
		}
	}
	{
		ArgumentNullException_t628810857 * L_2 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_2, _stringLiteral51169761, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_001f:
	{
		__this->set_parser_in_string_6((bool)0);
		__this->set_parser_return_7((bool)0);
		__this->set_read_started_8((bool)0);
		TextReader_t1561828458 * L_3 = ___reader0;
		Lexer_t954714164 * L_4 = (Lexer_t954714164 *)il2cpp_codegen_object_new(Lexer_t954714164_il2cpp_TypeInfo_var);
		Lexer__ctor_m786021532(L_4, L_3, /*hidden argument*/NULL);
		__this->set_lexer_5(L_4);
		__this->set_end_of_input_4((bool)0);
		__this->set_end_of_json_3((bool)0);
		TextReader_t1561828458 * L_5 = ___reader0;
		__this->set_reader_9(L_5);
		bool L_6 = ___owned1;
		__this->set_reader_is_owned_10(L_6);
		return;
	}
}
// System.Void ThirdParty.Json.LitJson.JsonReader::ProcessNumber(System.String)
extern Il2CppClass* CultureInfo_t3500843524_il2cpp_TypeInfo_var;
extern Il2CppClass* Double_t4078015681_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt32_t2149682021_il2cpp_TypeInfo_var;
extern Il2CppClass* Int64_t909078037_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt64_t2909196914_il2cpp_TypeInfo_var;
extern const uint32_t JsonReader_ProcessNumber_m1347979804_MetadataUsageId;
extern "C"  void JsonReader_ProcessNumber_m1347979804 (JsonReader_t354941621 * __this, String_t* ___number0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonReader_ProcessNumber_m1347979804_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	uint32_t V_1 = 0;
	int64_t V_2 = 0;
	uint64_t V_3 = 0;
	double V_4 = 0.0;
	{
		String_t* L_0 = ___number0;
		NullCheck(L_0);
		int32_t L_1 = String_IndexOf_m2358239236(L_0, ((int32_t)46), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)(-1)))))
		{
			goto IL_0021;
		}
	}
	{
		String_t* L_2 = ___number0;
		NullCheck(L_2);
		int32_t L_3 = String_IndexOf_m2358239236(L_2, ((int32_t)101), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)(-1)))))
		{
			goto IL_0021;
		}
	}
	{
		String_t* L_4 = ___number0;
		NullCheck(L_4);
		int32_t L_5 = String_IndexOf_m2358239236(L_4, ((int32_t)69), /*hidden argument*/NULL);
		if ((((int32_t)L_5) == ((int32_t)(-1))))
		{
			goto IL_004b;
		}
	}

IL_0021:
	{
		String_t* L_6 = ___number0;
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3500843524_il2cpp_TypeInfo_var);
		CultureInfo_t3500843524 * L_7 = CultureInfo_get_InvariantCulture_m398972276(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_8 = Double_TryParse_m815528105(NULL /*static, unused*/, L_6, ((int32_t)511), L_7, (&V_4), /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_004b;
		}
	}
	{
		__this->set_token_12(((int32_t)10));
		double L_9 = V_4;
		double L_10 = L_9;
		Il2CppObject * L_11 = Box(Double_t4078015681_il2cpp_TypeInfo_var, &L_10);
		__this->set_token_value_11(L_11);
		return;
	}

IL_004b:
	{
		String_t* L_12 = ___number0;
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3500843524_il2cpp_TypeInfo_var);
		CultureInfo_t3500843524 * L_13 = CultureInfo_get_InvariantCulture_m398972276(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_14 = Int32_TryParse_m2941352213(NULL /*static, unused*/, L_12, ((int32_t)511), L_13, (&V_0), /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0073;
		}
	}
	{
		__this->set_token_12(6);
		int32_t L_15 = V_0;
		int32_t L_16 = L_15;
		Il2CppObject * L_17 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_16);
		__this->set_token_value_11(L_17);
		return;
	}

IL_0073:
	{
		String_t* L_18 = ___number0;
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3500843524_il2cpp_TypeInfo_var);
		CultureInfo_t3500843524 * L_19 = CultureInfo_get_InvariantCulture_m398972276(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_20 = UInt32_TryParse_m3987111861(NULL /*static, unused*/, L_18, ((int32_t)511), L_19, (&V_1), /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_009b;
		}
	}
	{
		__this->set_token_12(7);
		uint32_t L_21 = V_1;
		uint32_t L_22 = L_21;
		Il2CppObject * L_23 = Box(UInt32_t2149682021_il2cpp_TypeInfo_var, &L_22);
		__this->set_token_value_11(L_23);
		return;
	}

IL_009b:
	{
		String_t* L_24 = ___number0;
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3500843524_il2cpp_TypeInfo_var);
		CultureInfo_t3500843524 * L_25 = CultureInfo_get_InvariantCulture_m398972276(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_26 = Int64_TryParse_m3093198325(NULL /*static, unused*/, L_24, ((int32_t)511), L_25, (&V_2), /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_00c3;
		}
	}
	{
		__this->set_token_12(8);
		int64_t L_27 = V_2;
		int64_t L_28 = L_27;
		Il2CppObject * L_29 = Box(Int64_t909078037_il2cpp_TypeInfo_var, &L_28);
		__this->set_token_value_11(L_29);
		return;
	}

IL_00c3:
	{
		String_t* L_30 = ___number0;
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3500843524_il2cpp_TypeInfo_var);
		CultureInfo_t3500843524 * L_31 = CultureInfo_get_InvariantCulture_m398972276(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_32 = UInt64_TryParse_m3975883397(NULL /*static, unused*/, L_30, ((int32_t)511), L_31, (&V_3), /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_00ec;
		}
	}
	{
		__this->set_token_12(((int32_t)9));
		uint64_t L_33 = V_3;
		uint64_t L_34 = L_33;
		Il2CppObject * L_35 = Box(UInt64_t2909196914_il2cpp_TypeInfo_var, &L_34);
		__this->set_token_value_11(L_35);
		return;
	}

IL_00ec:
	{
		__this->set_token_12(((int32_t)9));
		uint64_t L_36 = ((uint64_t)(((int64_t)((int64_t)0))));
		Il2CppObject * L_37 = Box(UInt64_t2909196914_il2cpp_TypeInfo_var, &L_36);
		__this->set_token_value_11(L_37);
		return;
	}
}
// System.Void ThirdParty.Json.LitJson.JsonReader::ProcessSymbol()
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern const uint32_t JsonReader_ProcessSymbol_m1556389329_MetadataUsageId;
extern "C"  void JsonReader_ProcessSymbol_m1556389329 (JsonReader_t354941621 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonReader_ProcessSymbol_m1556389329_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_current_symbol_2();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)91)))))
		{
			goto IL_0019;
		}
	}
	{
		__this->set_token_12(4);
		__this->set_parser_return_7((bool)1);
		return;
	}

IL_0019:
	{
		int32_t L_1 = __this->get_current_symbol_2();
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)93)))))
		{
			goto IL_0032;
		}
	}
	{
		__this->set_token_12(5);
		__this->set_parser_return_7((bool)1);
		return;
	}

IL_0032:
	{
		int32_t L_2 = __this->get_current_symbol_2();
		if ((!(((uint32_t)L_2) == ((uint32_t)((int32_t)123)))))
		{
			goto IL_004b;
		}
	}
	{
		__this->set_token_12(1);
		__this->set_parser_return_7((bool)1);
		return;
	}

IL_004b:
	{
		int32_t L_3 = __this->get_current_symbol_2();
		if ((!(((uint32_t)L_3) == ((uint32_t)((int32_t)125)))))
		{
			goto IL_0064;
		}
	}
	{
		__this->set_token_12(3);
		__this->set_parser_return_7((bool)1);
		return;
	}

IL_0064:
	{
		int32_t L_4 = __this->get_current_symbol_2();
		if ((!(((uint32_t)L_4) == ((uint32_t)((int32_t)34)))))
		{
			goto IL_009d;
		}
	}
	{
		bool L_5 = __this->get_parser_in_string_6();
		if (!L_5)
		{
			goto IL_0085;
		}
	}
	{
		__this->set_parser_in_string_6((bool)0);
		__this->set_parser_return_7((bool)1);
		return;
	}

IL_0085:
	{
		int32_t L_6 = __this->get_token_12();
		if (L_6)
		{
			goto IL_0095;
		}
	}
	{
		__this->set_token_12(((int32_t)11));
	}

IL_0095:
	{
		__this->set_parser_in_string_6((bool)1);
		return;
	}

IL_009d:
	{
		int32_t L_7 = __this->get_current_symbol_2();
		if ((!(((uint32_t)L_7) == ((uint32_t)((int32_t)65541)))))
		{
			goto IL_00bc;
		}
	}
	{
		Lexer_t954714164 * L_8 = __this->get_lexer_5();
		NullCheck(L_8);
		String_t* L_9 = Lexer_get_StringValue_m2285872777(L_8, /*hidden argument*/NULL);
		__this->set_token_value_11(L_9);
		return;
	}

IL_00bc:
	{
		int32_t L_10 = __this->get_current_symbol_2();
		if ((!(((uint32_t)L_10) == ((uint32_t)((int32_t)65539)))))
		{
			goto IL_00e5;
		}
	}
	{
		__this->set_token_12(((int32_t)12));
		bool L_11 = ((bool)0);
		Il2CppObject * L_12 = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &L_11);
		__this->set_token_value_11(L_12);
		__this->set_parser_return_7((bool)1);
		return;
	}

IL_00e5:
	{
		int32_t L_13 = __this->get_current_symbol_2();
		if ((!(((uint32_t)L_13) == ((uint32_t)((int32_t)65540)))))
		{
			goto IL_0102;
		}
	}
	{
		__this->set_token_12(((int32_t)13));
		__this->set_parser_return_7((bool)1);
		return;
	}

IL_0102:
	{
		int32_t L_14 = __this->get_current_symbol_2();
		if ((!(((uint32_t)L_14) == ((uint32_t)((int32_t)65537)))))
		{
			goto IL_0128;
		}
	}
	{
		Lexer_t954714164 * L_15 = __this->get_lexer_5();
		NullCheck(L_15);
		String_t* L_16 = Lexer_get_StringValue_m2285872777(L_15, /*hidden argument*/NULL);
		JsonReader_ProcessNumber_m1347979804(__this, L_16, /*hidden argument*/NULL);
		__this->set_parser_return_7((bool)1);
		return;
	}

IL_0128:
	{
		int32_t L_17 = __this->get_current_symbol_2();
		if ((!(((uint32_t)L_17) == ((uint32_t)((int32_t)65546)))))
		{
			goto IL_013d;
		}
	}
	{
		__this->set_token_12(2);
		return;
	}

IL_013d:
	{
		int32_t L_18 = __this->get_current_symbol_2();
		if ((!(((uint32_t)L_18) == ((uint32_t)((int32_t)65538)))))
		{
			goto IL_0165;
		}
	}
	{
		__this->set_token_12(((int32_t)12));
		bool L_19 = ((bool)1);
		Il2CppObject * L_20 = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &L_19);
		__this->set_token_value_11(L_20);
		__this->set_parser_return_7((bool)1);
	}

IL_0165:
	{
		return;
	}
}
// System.Boolean ThirdParty.Json.LitJson.JsonReader::ReadToken()
extern "C"  bool JsonReader_ReadToken_m2866069121 (JsonReader_t354941621 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_end_of_input_4();
		if (!L_0)
		{
			goto IL_000a;
		}
	}
	{
		return (bool)0;
	}

IL_000a:
	{
		Lexer_t954714164 * L_1 = __this->get_lexer_5();
		NullCheck(L_1);
		Lexer_NextToken_m4229197011(L_1, /*hidden argument*/NULL);
		Lexer_t954714164 * L_2 = __this->get_lexer_5();
		NullCheck(L_2);
		bool L_3 = Lexer_get_EndOfInput_m3261687552(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002b;
		}
	}
	{
		JsonReader_Close_m1145586022(__this, /*hidden argument*/NULL);
		return (bool)0;
	}

IL_002b:
	{
		Lexer_t954714164 * L_4 = __this->get_lexer_5();
		NullCheck(L_4);
		int32_t L_5 = Lexer_get_Token_m4181743523(L_4, /*hidden argument*/NULL);
		__this->set_current_input_1(L_5);
		return (bool)1;
	}
}
// System.Void ThirdParty.Json.LitJson.JsonReader::Close()
extern "C"  void JsonReader_Close_m1145586022 (JsonReader_t354941621 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_end_of_input_4();
		if (!L_0)
		{
			goto IL_0009;
		}
	}
	{
		return;
	}

IL_0009:
	{
		__this->set_end_of_input_4((bool)1);
		__this->set_end_of_json_3((bool)1);
		__this->set_reader_9((TextReader_t1561828458 *)NULL);
		return;
	}
}
// System.Boolean ThirdParty.Json.LitJson.JsonReader::Read()
extern Il2CppClass* JsonToken_t2445581255_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonException_t1457213491_il2cpp_TypeInfo_var;
extern const MethodInfo* Stack_1_Push_m912983667_MethodInfo_var;
extern const MethodInfo* Stack_1_Peek_m2985740121_MethodInfo_var;
extern const MethodInfo* Stack_1_Pop_m607882821_MethodInfo_var;
extern const MethodInfo* Stack_1_get_Count_m1641722018_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3745640987;
extern Il2CppCodeGenString* _stringLiteral2857305135;
extern Il2CppCodeGenString* _stringLiteral3820272292;
extern Il2CppCodeGenString* _stringLiteral3763652101;
extern const uint32_t JsonReader_Read_m3114967242_MetadataUsageId;
extern "C"  bool JsonReader_Read_m3114967242 (JsonReader_t354941621 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonReader_Read_m3114967242_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		bool L_0 = __this->get_end_of_input_4();
		if (!L_0)
		{
			goto IL_000a;
		}
	}
	{
		return (bool)0;
	}

IL_000a:
	{
		bool L_1 = __this->get_end_of_json_3();
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		__this->set_end_of_json_3((bool)0);
	}

IL_0019:
	{
		__this->set_token_12(0);
		__this->set_parser_in_string_6((bool)0);
		__this->set_parser_return_7((bool)0);
		bool L_2 = __this->get_read_started_8();
		if (L_2)
		{
			goto IL_0047;
		}
	}
	{
		__this->set_read_started_8((bool)1);
		bool L_3 = JsonReader_ReadToken_m2866069121(__this, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0047;
		}
	}
	{
		return (bool)0;
	}

IL_0047:
	{
		int32_t L_4 = __this->get_current_input_1();
		__this->set_current_symbol_2(L_4);
		JsonReader_ProcessSymbol_m1556389329(__this, /*hidden argument*/NULL);
		bool L_5 = __this->get_parser_return_7();
		if (!L_5)
		{
			goto IL_024c;
		}
	}
	{
		int32_t L_6 = __this->get_token_12();
		if ((((int32_t)L_6) == ((int32_t)1)))
		{
			goto IL_0076;
		}
	}
	{
		int32_t L_7 = __this->get_token_12();
		if ((!(((uint32_t)L_7) == ((uint32_t)4))))
		{
			goto IL_008c;
		}
	}

IL_0076:
	{
		Stack_1_t3533309409 * L_8 = __this->get_depth_0();
		int32_t L_9 = __this->get_token_12();
		NullCheck(L_8);
		Stack_1_Push_m912983667(L_8, L_9, /*hidden argument*/Stack_1_Push_m912983667_MethodInfo_var);
		goto IL_017d;
	}

IL_008c:
	{
		int32_t L_10 = __this->get_token_12();
		if ((((int32_t)L_10) == ((int32_t)3)))
		{
			goto IL_00a1;
		}
	}
	{
		int32_t L_11 = __this->get_token_12();
		if ((!(((uint32_t)L_11) == ((uint32_t)5))))
		{
			goto IL_0131;
		}
	}

IL_00a1:
	{
		Stack_1_t3533309409 * L_12 = __this->get_depth_0();
		NullCheck(L_12);
		int32_t L_13 = Stack_1_Peek_m2985740121(L_12, /*hidden argument*/Stack_1_Peek_m2985740121_MethodInfo_var);
		if ((!(((uint32_t)L_13) == ((uint32_t)2))))
		{
			goto IL_00bb;
		}
	}
	{
		Stack_1_t3533309409 * L_14 = __this->get_depth_0();
		NullCheck(L_14);
		Stack_1_Pop_m607882821(L_14, /*hidden argument*/Stack_1_Pop_m607882821_MethodInfo_var);
	}

IL_00bb:
	{
		Stack_1_t3533309409 * L_15 = __this->get_depth_0();
		NullCheck(L_15);
		int32_t L_16 = Stack_1_Pop_m607882821(L_15, /*hidden argument*/Stack_1_Pop_m607882821_MethodInfo_var);
		V_0 = L_16;
		int32_t L_17 = __this->get_token_12();
		if ((!(((uint32_t)L_17) == ((uint32_t)3))))
		{
			goto IL_00f1;
		}
	}
	{
		int32_t L_18 = V_0;
		if ((((int32_t)L_18) == ((int32_t)1)))
		{
			goto IL_00f1;
		}
	}
	{
		Il2CppObject * L_19 = Box(JsonToken_t2445581255_il2cpp_TypeInfo_var, (&V_0));
		NullCheck(L_19);
		String_t* L_20 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_19);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_21 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral3745640987, L_20, /*hidden argument*/NULL);
		JsonException_t1457213491 * L_22 = (JsonException_t1457213491 *)il2cpp_codegen_object_new(JsonException_t1457213491_il2cpp_TypeInfo_var);
		JsonException__ctor_m1029591446(L_22, L_21, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_22);
	}

IL_00f1:
	{
		int32_t L_23 = __this->get_token_12();
		if ((!(((uint32_t)L_23) == ((uint32_t)5))))
		{
			goto IL_011b;
		}
	}
	{
		int32_t L_24 = V_0;
		if ((((int32_t)L_24) == ((int32_t)4)))
		{
			goto IL_011b;
		}
	}
	{
		Il2CppObject * L_25 = Box(JsonToken_t2445581255_il2cpp_TypeInfo_var, (&V_0));
		NullCheck(L_25);
		String_t* L_26 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_25);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral2857305135, L_26, /*hidden argument*/NULL);
		JsonException_t1457213491 * L_28 = (JsonException_t1457213491 *)il2cpp_codegen_object_new(JsonException_t1457213491_il2cpp_TypeInfo_var);
		JsonException__ctor_m1029591446(L_28, L_27, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_28);
	}

IL_011b:
	{
		Stack_1_t3533309409 * L_29 = __this->get_depth_0();
		NullCheck(L_29);
		int32_t L_30 = Stack_1_get_Count_m1641722018(L_29, /*hidden argument*/Stack_1_get_Count_m1641722018_MethodInfo_var);
		if (L_30)
		{
			goto IL_017d;
		}
	}
	{
		__this->set_end_of_json_3((bool)1);
		goto IL_017d;
	}

IL_0131:
	{
		Stack_1_t3533309409 * L_31 = __this->get_depth_0();
		NullCheck(L_31);
		int32_t L_32 = Stack_1_get_Count_m1641722018(L_31, /*hidden argument*/Stack_1_get_Count_m1641722018_MethodInfo_var);
		if ((((int32_t)L_32) <= ((int32_t)0)))
		{
			goto IL_017d;
		}
	}
	{
		Stack_1_t3533309409 * L_33 = __this->get_depth_0();
		NullCheck(L_33);
		int32_t L_34 = Stack_1_Peek_m2985740121(L_33, /*hidden argument*/Stack_1_Peek_m2985740121_MethodInfo_var);
		if ((((int32_t)L_34) == ((int32_t)2)))
		{
			goto IL_017d;
		}
	}
	{
		int32_t L_35 = __this->get_token_12();
		if ((!(((uint32_t)L_35) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_017d;
		}
	}
	{
		Stack_1_t3533309409 * L_36 = __this->get_depth_0();
		NullCheck(L_36);
		int32_t L_37 = Stack_1_Peek_m2985740121(L_36, /*hidden argument*/Stack_1_Peek_m2985740121_MethodInfo_var);
		if ((!(((uint32_t)L_37) == ((uint32_t)1))))
		{
			goto IL_017d;
		}
	}
	{
		__this->set_token_12(2);
		Stack_1_t3533309409 * L_38 = __this->get_depth_0();
		int32_t L_39 = __this->get_token_12();
		NullCheck(L_38);
		Stack_1_Push_m912983667(L_38, L_39, /*hidden argument*/Stack_1_Push_m912983667_MethodInfo_var);
	}

IL_017d:
	{
		int32_t L_40 = __this->get_token_12();
		if ((((int32_t)L_40) == ((int32_t)3)))
		{
			goto IL_01e6;
		}
	}
	{
		int32_t L_41 = __this->get_token_12();
		if ((((int32_t)L_41) == ((int32_t)5)))
		{
			goto IL_01e6;
		}
	}
	{
		int32_t L_42 = __this->get_token_12();
		if ((((int32_t)L_42) == ((int32_t)((int32_t)11))))
		{
			goto IL_01e6;
		}
	}
	{
		int32_t L_43 = __this->get_token_12();
		if ((((int32_t)L_43) == ((int32_t)((int32_t)12))))
		{
			goto IL_01e6;
		}
	}
	{
		int32_t L_44 = __this->get_token_12();
		if ((((int32_t)L_44) == ((int32_t)((int32_t)10))))
		{
			goto IL_01e6;
		}
	}
	{
		int32_t L_45 = __this->get_token_12();
		if ((((int32_t)L_45) == ((int32_t)6)))
		{
			goto IL_01e6;
		}
	}
	{
		int32_t L_46 = __this->get_token_12();
		if ((((int32_t)L_46) == ((int32_t)7)))
		{
			goto IL_01e6;
		}
	}
	{
		int32_t L_47 = __this->get_token_12();
		if ((((int32_t)L_47) == ((int32_t)8)))
		{
			goto IL_01e6;
		}
	}
	{
		int32_t L_48 = __this->get_token_12();
		if ((((int32_t)L_48) == ((int32_t)((int32_t)9))))
		{
			goto IL_01e6;
		}
	}
	{
		int32_t L_49 = __this->get_token_12();
		if ((((int32_t)L_49) == ((int32_t)((int32_t)13))))
		{
			goto IL_01e6;
		}
	}
	{
		int32_t L_50 = __this->get_token_12();
		if ((!(((uint32_t)L_50) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_022a;
		}
	}

IL_01e6:
	{
		Stack_1_t3533309409 * L_51 = __this->get_depth_0();
		NullCheck(L_51);
		int32_t L_52 = Stack_1_get_Count_m1641722018(L_51, /*hidden argument*/Stack_1_get_Count_m1641722018_MethodInfo_var);
		if (L_52)
		{
			goto IL_0210;
		}
	}
	{
		int32_t L_53 = __this->get_token_12();
		if ((((int32_t)L_53) == ((int32_t)5)))
		{
			goto IL_022a;
		}
	}
	{
		int32_t L_54 = __this->get_token_12();
		if ((((int32_t)L_54) == ((int32_t)3)))
		{
			goto IL_022a;
		}
	}
	{
		JsonException_t1457213491 * L_55 = (JsonException_t1457213491 *)il2cpp_codegen_object_new(JsonException_t1457213491_il2cpp_TypeInfo_var);
		JsonException__ctor_m1029591446(L_55, _stringLiteral3820272292, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_55);
	}

IL_0210:
	{
		Stack_1_t3533309409 * L_56 = __this->get_depth_0();
		NullCheck(L_56);
		int32_t L_57 = Stack_1_Peek_m2985740121(L_56, /*hidden argument*/Stack_1_Peek_m2985740121_MethodInfo_var);
		if ((!(((uint32_t)L_57) == ((uint32_t)2))))
		{
			goto IL_022a;
		}
	}
	{
		Stack_1_t3533309409 * L_58 = __this->get_depth_0();
		NullCheck(L_58);
		Stack_1_Pop_m607882821(L_58, /*hidden argument*/Stack_1_Pop_m607882821_MethodInfo_var);
	}

IL_022a:
	{
		bool L_59 = JsonReader_ReadToken_m2866069121(__this, /*hidden argument*/NULL);
		if (L_59)
		{
			goto IL_024a;
		}
	}
	{
		Stack_1_t3533309409 * L_60 = __this->get_depth_0();
		NullCheck(L_60);
		int32_t L_61 = Stack_1_get_Count_m1641722018(L_60, /*hidden argument*/Stack_1_get_Count_m1641722018_MethodInfo_var);
		if (!L_61)
		{
			goto IL_024a;
		}
	}
	{
		JsonException_t1457213491 * L_62 = (JsonException_t1457213491 *)il2cpp_codegen_object_new(JsonException_t1457213491_il2cpp_TypeInfo_var);
		JsonException__ctor_m1029591446(L_62, _stringLiteral3763652101, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_62);
	}

IL_024a:
	{
		return (bool)1;
	}

IL_024c:
	{
		bool L_63 = JsonReader_ReadToken_m2866069121(__this, /*hidden argument*/NULL);
		if (L_63)
		{
			goto IL_0047;
		}
	}
	{
		Stack_1_t3533309409 * L_64 = __this->get_depth_0();
		NullCheck(L_64);
		int32_t L_65 = Stack_1_get_Count_m1641722018(L_64, /*hidden argument*/Stack_1_get_Count_m1641722018_MethodInfo_var);
		if (!L_65)
		{
			goto IL_026f;
		}
	}
	{
		JsonException_t1457213491 * L_66 = (JsonException_t1457213491 *)il2cpp_codegen_object_new(JsonException_t1457213491_il2cpp_TypeInfo_var);
		JsonException__ctor_m1029591446(L_66, _stringLiteral3763652101, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_66);
	}

IL_026f:
	{
		__this->set_end_of_input_4((bool)1);
		return (bool)0;
	}
}
// System.Void ThirdParty.Json.LitJson.JsonWriter::.cctor()
extern Il2CppClass* NumberFormatInfo_t104580544_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonWriter_t3014444111_il2cpp_TypeInfo_var;
extern const uint32_t JsonWriter__cctor_m1823466769_MetadataUsageId;
extern "C"  void JsonWriter__cctor_m1823466769 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter__cctor_m1823466769_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(NumberFormatInfo_t104580544_il2cpp_TypeInfo_var);
		NumberFormatInfo_t104580544 * L_0 = NumberFormatInfo_get_InvariantInfo_m2658215204(NULL /*static, unused*/, /*hidden argument*/NULL);
		((JsonWriter_t3014444111_StaticFields*)JsonWriter_t3014444111_il2cpp_TypeInfo_var->static_fields)->set_number_format_0(L_0);
		return;
	}
}
// System.Void ThirdParty.Json.LitJson.JsonWriter::.ctor()
extern Il2CppClass* StringBuilder_t1221177846_il2cpp_TypeInfo_var;
extern Il2CppClass* StringWriter_t4139609088_il2cpp_TypeInfo_var;
extern const uint32_t JsonWriter__ctor_m1529769822_MetadataUsageId;
extern "C"  void JsonWriter__ctor_m1529769822 (JsonWriter_t3014444111 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter__ctor_m1529769822_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_0 = (StringBuilder_t1221177846 *)il2cpp_codegen_object_new(StringBuilder_t1221177846_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m3946851802(L_0, /*hidden argument*/NULL);
		__this->set_inst_string_builder_7(L_0);
		StringBuilder_t1221177846 * L_1 = __this->get_inst_string_builder_7();
		StringWriter_t4139609088 * L_2 = (StringWriter_t4139609088 *)il2cpp_codegen_object_new(StringWriter_t4139609088_il2cpp_TypeInfo_var);
		StringWriter__ctor_m1896773521(L_2, L_1, /*hidden argument*/NULL);
		__this->set_writer_10(L_2);
		JsonWriter_Init_m302664798(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ThirdParty.Json.LitJson.JsonWriter::.ctor(System.Text.StringBuilder)
extern Il2CppClass* StringWriter_t4139609088_il2cpp_TypeInfo_var;
extern const uint32_t JsonWriter__ctor_m83397816_MetadataUsageId;
extern "C"  void JsonWriter__ctor_m83397816 (JsonWriter_t3014444111 * __this, StringBuilder_t1221177846 * ___sb0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter__ctor_m83397816_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		StringBuilder_t1221177846 * L_0 = ___sb0;
		StringWriter_t4139609088 * L_1 = (StringWriter_t4139609088 *)il2cpp_codegen_object_new(StringWriter_t4139609088_il2cpp_TypeInfo_var);
		StringWriter__ctor_m1896773521(L_1, L_0, /*hidden argument*/NULL);
		JsonWriter__ctor_m2344288487(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ThirdParty.Json.LitJson.JsonWriter::.ctor(System.IO.TextWriter)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3496454067;
extern const uint32_t JsonWriter__ctor_m2344288487_MetadataUsageId;
extern "C"  void JsonWriter__ctor_m2344288487 (JsonWriter_t3014444111 * __this, TextWriter_t4027217640 * ___writer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter__ctor_m2344288487_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		TextWriter_t4027217640 * L_0 = ___writer0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, _stringLiteral3496454067, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0014:
	{
		TextWriter_t4027217640 * L_2 = ___writer0;
		__this->set_writer_10(L_2);
		JsonWriter_Init_m302664798(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ThirdParty.Json.LitJson.JsonWriter::DoValidation(ThirdParty.Json.LitJson.Condition)
extern Il2CppClass* JsonException_t1457213491_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3899888009;
extern Il2CppCodeGenString* _stringLiteral2438288433;
extern Il2CppCodeGenString* _stringLiteral2704635905;
extern Il2CppCodeGenString* _stringLiteral4099653570;
extern Il2CppCodeGenString* _stringLiteral682467066;
extern Il2CppCodeGenString* _stringLiteral4225979484;
extern const uint32_t JsonWriter_DoValidation_m2832088507_MetadataUsageId;
extern "C"  void JsonWriter_DoValidation_m2832088507 (JsonWriter_t3014444111 * __this, int32_t ___cond0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_DoValidation_m2832088507_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		WriterContext_t1209007092 * L_0 = __this->get_context_1();
		NullCheck(L_0);
		bool L_1 = L_0->get_ExpectingValue_3();
		if (L_1)
		{
			goto IL_0020;
		}
	}
	{
		WriterContext_t1209007092 * L_2 = __this->get_context_1();
		WriterContext_t1209007092 * L_3 = L_2;
		NullCheck(L_3);
		int32_t L_4 = L_3->get_Count_0();
		NullCheck(L_3);
		L_3->set_Count_0(((int32_t)((int32_t)L_4+(int32_t)1)));
	}

IL_0020:
	{
		bool L_5 = __this->get_validate_9();
		if (L_5)
		{
			goto IL_0029;
		}
	}
	{
		return;
	}

IL_0029:
	{
		bool L_6 = __this->get_has_reached_end_3();
		if (!L_6)
		{
			goto IL_003c;
		}
	}
	{
		JsonException_t1457213491 * L_7 = (JsonException_t1457213491 *)il2cpp_codegen_object_new(JsonException_t1457213491_il2cpp_TypeInfo_var);
		JsonException__ctor_m1029591446(L_7, _stringLiteral3899888009, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_003c:
	{
		int32_t L_8 = ___cond0;
		if (L_8 == 0)
		{
			goto IL_0057;
		}
		if (L_8 == 1)
		{
			goto IL_0072;
		}
		if (L_8 == 2)
		{
			goto IL_009a;
		}
		if (L_8 == 3)
		{
			goto IL_00bf;
		}
		if (L_8 == 4)
		{
			goto IL_00e4;
		}
	}
	{
		return;
	}

IL_0057:
	{
		WriterContext_t1209007092 * L_9 = __this->get_context_1();
		NullCheck(L_9);
		bool L_10 = L_9->get_InArray_1();
		if (L_10)
		{
			goto IL_0116;
		}
	}
	{
		JsonException_t1457213491 * L_11 = (JsonException_t1457213491 *)il2cpp_codegen_object_new(JsonException_t1457213491_il2cpp_TypeInfo_var);
		JsonException__ctor_m1029591446(L_11, _stringLiteral2438288433, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_11);
	}

IL_0072:
	{
		WriterContext_t1209007092 * L_12 = __this->get_context_1();
		NullCheck(L_12);
		bool L_13 = L_12->get_InObject_2();
		if (!L_13)
		{
			goto IL_008f;
		}
	}
	{
		WriterContext_t1209007092 * L_14 = __this->get_context_1();
		NullCheck(L_14);
		bool L_15 = L_14->get_ExpectingValue_3();
		if (!L_15)
		{
			goto IL_0116;
		}
	}

IL_008f:
	{
		JsonException_t1457213491 * L_16 = (JsonException_t1457213491 *)il2cpp_codegen_object_new(JsonException_t1457213491_il2cpp_TypeInfo_var);
		JsonException__ctor_m1029591446(L_16, _stringLiteral2704635905, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_16);
	}

IL_009a:
	{
		WriterContext_t1209007092 * L_17 = __this->get_context_1();
		NullCheck(L_17);
		bool L_18 = L_17->get_InObject_2();
		if (!L_18)
		{
			goto IL_0116;
		}
	}
	{
		WriterContext_t1209007092 * L_19 = __this->get_context_1();
		NullCheck(L_19);
		bool L_20 = L_19->get_ExpectingValue_3();
		if (L_20)
		{
			goto IL_0116;
		}
	}
	{
		JsonException_t1457213491 * L_21 = (JsonException_t1457213491 *)il2cpp_codegen_object_new(JsonException_t1457213491_il2cpp_TypeInfo_var);
		JsonException__ctor_m1029591446(L_21, _stringLiteral4099653570, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}

IL_00bf:
	{
		WriterContext_t1209007092 * L_22 = __this->get_context_1();
		NullCheck(L_22);
		bool L_23 = L_22->get_InObject_2();
		if (!L_23)
		{
			goto IL_00d9;
		}
	}
	{
		WriterContext_t1209007092 * L_24 = __this->get_context_1();
		NullCheck(L_24);
		bool L_25 = L_24->get_ExpectingValue_3();
		if (!L_25)
		{
			goto IL_0116;
		}
	}

IL_00d9:
	{
		JsonException_t1457213491 * L_26 = (JsonException_t1457213491 *)il2cpp_codegen_object_new(JsonException_t1457213491_il2cpp_TypeInfo_var);
		JsonException__ctor_m1029591446(L_26, _stringLiteral682467066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_26);
	}

IL_00e4:
	{
		WriterContext_t1209007092 * L_27 = __this->get_context_1();
		NullCheck(L_27);
		bool L_28 = L_27->get_InArray_1();
		if (L_28)
		{
			goto IL_0116;
		}
	}
	{
		WriterContext_t1209007092 * L_29 = __this->get_context_1();
		NullCheck(L_29);
		bool L_30 = L_29->get_InObject_2();
		if (!L_30)
		{
			goto IL_010b;
		}
	}
	{
		WriterContext_t1209007092 * L_31 = __this->get_context_1();
		NullCheck(L_31);
		bool L_32 = L_31->get_ExpectingValue_3();
		if (L_32)
		{
			goto IL_0116;
		}
	}

IL_010b:
	{
		JsonException_t1457213491 * L_33 = (JsonException_t1457213491 *)il2cpp_codegen_object_new(JsonException_t1457213491_il2cpp_TypeInfo_var);
		JsonException__ctor_m1029591446(L_33, _stringLiteral4225979484, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_33);
	}

IL_0116:
	{
		return;
	}
}
// System.Void ThirdParty.Json.LitJson.JsonWriter::Init()
extern Il2CppClass* CharU5BU5D_t1328083999_il2cpp_TypeInfo_var;
extern Il2CppClass* Stack_1_t2296735246_il2cpp_TypeInfo_var;
extern Il2CppClass* WriterContext_t1209007092_il2cpp_TypeInfo_var;
extern const MethodInfo* Stack_1__ctor_m1218044367_MethodInfo_var;
extern const MethodInfo* Stack_1_Push_m193241330_MethodInfo_var;
extern const uint32_t JsonWriter_Init_m302664798_MetadataUsageId;
extern "C"  void JsonWriter_Init_m302664798 (JsonWriter_t3014444111 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Init_m302664798_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_has_reached_end_3((bool)0);
		__this->set_hex_seq_4(((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)4)));
		__this->set_indentation_5(0);
		__this->set_indent_value_6(4);
		__this->set_pretty_print_8((bool)0);
		__this->set_validate_9((bool)1);
		Stack_1_t2296735246 * L_0 = (Stack_1_t2296735246 *)il2cpp_codegen_object_new(Stack_1_t2296735246_il2cpp_TypeInfo_var);
		Stack_1__ctor_m1218044367(L_0, /*hidden argument*/Stack_1__ctor_m1218044367_MethodInfo_var);
		__this->set_ctx_stack_2(L_0);
		WriterContext_t1209007092 * L_1 = (WriterContext_t1209007092 *)il2cpp_codegen_object_new(WriterContext_t1209007092_il2cpp_TypeInfo_var);
		WriterContext__ctor_m2481251725(L_1, /*hidden argument*/NULL);
		__this->set_context_1(L_1);
		Stack_1_t2296735246 * L_2 = __this->get_ctx_stack_2();
		WriterContext_t1209007092 * L_3 = __this->get_context_1();
		NullCheck(L_2);
		Stack_1_Push_m193241330(L_2, L_3, /*hidden argument*/Stack_1_Push_m193241330_MethodInfo_var);
		return;
	}
}
// System.Void ThirdParty.Json.LitJson.JsonWriter::IntToHex(System.Int32,System.Char[])
extern "C"  void JsonWriter_IntToHex_m4158006151 (Il2CppObject * __this /* static, unused */, int32_t ___n0, CharU5BU5D_t1328083999* ___hex1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_1 = 0;
		goto IL_0030;
	}

IL_0004:
	{
		int32_t L_0 = ___n0;
		V_0 = ((int32_t)((int32_t)L_0%(int32_t)((int32_t)16)));
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) >= ((int32_t)((int32_t)10))))
		{
			goto IL_001a;
		}
	}
	{
		CharU5BU5D_t1328083999* L_2 = ___hex1;
		int32_t L_3 = V_1;
		int32_t L_4 = V_0;
		NullCheck(L_2);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)3-(int32_t)L_3))), (Il2CppChar)(((int32_t)((uint16_t)((int32_t)((int32_t)((int32_t)48)+(int32_t)L_4))))));
		goto IL_0027;
	}

IL_001a:
	{
		CharU5BU5D_t1328083999* L_5 = ___hex1;
		int32_t L_6 = V_1;
		int32_t L_7 = V_0;
		NullCheck(L_5);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)3-(int32_t)L_6))), (Il2CppChar)(((int32_t)((uint16_t)((int32_t)((int32_t)((int32_t)65)+(int32_t)((int32_t)((int32_t)L_7-(int32_t)((int32_t)10)))))))));
	}

IL_0027:
	{
		int32_t L_8 = ___n0;
		___n0 = ((int32_t)((int32_t)L_8>>(int32_t)4));
		int32_t L_9 = V_1;
		V_1 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0030:
	{
		int32_t L_10 = V_1;
		if ((((int32_t)L_10) < ((int32_t)4)))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void ThirdParty.Json.LitJson.JsonWriter::Indent()
extern "C"  void JsonWriter_Indent_m2307597358 (JsonWriter_t3014444111 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_pretty_print_8();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		int32_t L_1 = __this->get_indentation_5();
		int32_t L_2 = __this->get_indent_value_6();
		__this->set_indentation_5(((int32_t)((int32_t)L_1+(int32_t)L_2)));
	}

IL_001b:
	{
		return;
	}
}
// System.Void ThirdParty.Json.LitJson.JsonWriter::Put(System.String)
extern "C"  void JsonWriter_Put_m564997329 (JsonWriter_t3014444111 * __this, String_t* ___str0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		bool L_0 = __this->get_pretty_print_8();
		if (!L_0)
		{
			goto IL_0033;
		}
	}
	{
		WriterContext_t1209007092 * L_1 = __this->get_context_1();
		NullCheck(L_1);
		bool L_2 = L_1->get_ExpectingValue_3();
		if (L_2)
		{
			goto IL_0033;
		}
	}
	{
		V_0 = 0;
		goto IL_002a;
	}

IL_0019:
	{
		TextWriter_t4027217640 * L_3 = __this->get_writer_10();
		NullCheck(L_3);
		VirtActionInvoker1< Il2CppChar >::Invoke(10 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_3, ((int32_t)32));
		int32_t L_4 = V_0;
		V_0 = ((int32_t)((int32_t)L_4+(int32_t)1));
	}

IL_002a:
	{
		int32_t L_5 = V_0;
		int32_t L_6 = __this->get_indentation_5();
		if ((((int32_t)L_5) < ((int32_t)L_6)))
		{
			goto IL_0019;
		}
	}

IL_0033:
	{
		TextWriter_t4027217640 * L_7 = __this->get_writer_10();
		String_t* L_8 = ___str0;
		NullCheck(L_7);
		VirtActionInvoker1< String_t* >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.String) */, L_7, L_8);
		return;
	}
}
// System.Void ThirdParty.Json.LitJson.JsonWriter::PutNewline()
extern "C"  void JsonWriter_PutNewline_m1108742881 (JsonWriter_t3014444111 * __this, const MethodInfo* method)
{
	{
		JsonWriter_PutNewline_m347485110(__this, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ThirdParty.Json.LitJson.JsonWriter::PutNewline(System.Boolean)
extern Il2CppCodeGenString* _stringLiteral2162321587;
extern const uint32_t JsonWriter_PutNewline_m347485110_MetadataUsageId;
extern "C"  void JsonWriter_PutNewline_m347485110 (JsonWriter_t3014444111 * __this, bool ___add_comma0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_PutNewline_m347485110_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ___add_comma0;
		if (!L_0)
		{
			goto IL_002b;
		}
	}
	{
		WriterContext_t1209007092 * L_1 = __this->get_context_1();
		NullCheck(L_1);
		bool L_2 = L_1->get_ExpectingValue_3();
		if (L_2)
		{
			goto IL_002b;
		}
	}
	{
		WriterContext_t1209007092 * L_3 = __this->get_context_1();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_Count_0();
		if ((((int32_t)L_4) <= ((int32_t)1)))
		{
			goto IL_002b;
		}
	}
	{
		TextWriter_t4027217640 * L_5 = __this->get_writer_10();
		NullCheck(L_5);
		VirtActionInvoker1< Il2CppChar >::Invoke(10 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_5, ((int32_t)44));
	}

IL_002b:
	{
		bool L_6 = __this->get_pretty_print_8();
		if (!L_6)
		{
			goto IL_0050;
		}
	}
	{
		WriterContext_t1209007092 * L_7 = __this->get_context_1();
		NullCheck(L_7);
		bool L_8 = L_7->get_ExpectingValue_3();
		if (L_8)
		{
			goto IL_0050;
		}
	}
	{
		TextWriter_t4027217640 * L_9 = __this->get_writer_10();
		NullCheck(L_9);
		VirtActionInvoker1< String_t* >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.String) */, L_9, _stringLiteral2162321587);
	}

IL_0050:
	{
		return;
	}
}
// System.Void ThirdParty.Json.LitJson.JsonWriter::PutString(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonWriter_t3014444111_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3062999056;
extern Il2CppCodeGenString* _stringLiteral381169868;
extern Il2CppCodeGenString* _stringLiteral3869568110;
extern Il2CppCodeGenString* _stringLiteral3419229416;
extern Il2CppCodeGenString* _stringLiteral1093630588;
extern Il2CppCodeGenString* _stringLiteral2303484169;
extern const uint32_t JsonWriter_PutString_m1961797724_MetadataUsageId;
extern "C"  void JsonWriter_PutString_m1961797724 (JsonWriter_t3014444111 * __this, String_t* ___str0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_PutString_m1961797724_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Il2CppChar V_2 = 0x0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		JsonWriter_Put_m564997329(__this, L_0, /*hidden argument*/NULL);
		TextWriter_t4027217640 * L_1 = __this->get_writer_10();
		NullCheck(L_1);
		VirtActionInvoker1< Il2CppChar >::Invoke(10 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_1, ((int32_t)34));
		String_t* L_2 = ___str0;
		NullCheck(L_2);
		int32_t L_3 = String_get_Length_m1606060069(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		V_1 = 0;
		goto IL_014a;
	}

IL_0026:
	{
		String_t* L_4 = ___str0;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		Il2CppChar L_6 = String_get_Chars_m4230566705(L_4, L_5, /*hidden argument*/NULL);
		V_2 = L_6;
		Il2CppChar L_7 = V_2;
		if (((int32_t)((int32_t)L_7-(int32_t)8)) == 0)
		{
			goto IL_00cc;
		}
		if (((int32_t)((int32_t)L_7-(int32_t)8)) == 1)
		{
			goto IL_0087;
		}
		if (((int32_t)((int32_t)L_7-(int32_t)8)) == 2)
		{
			goto IL_005d;
		}
		if (((int32_t)((int32_t)L_7-(int32_t)8)) == 3)
		{
			goto IL_00de;
		}
		if (((int32_t)((int32_t)L_7-(int32_t)8)) == 4)
		{
			goto IL_00ba;
		}
		if (((int32_t)((int32_t)L_7-(int32_t)8)) == 5)
		{
			goto IL_0072;
		}
	}
	{
		Il2CppChar L_8 = V_2;
		if ((((int32_t)L_8) == ((int32_t)((int32_t)34))))
		{
			goto IL_009c;
		}
	}
	{
		Il2CppChar L_9 = V_2;
		if ((((int32_t)L_9) == ((int32_t)((int32_t)92))))
		{
			goto IL_009c;
		}
	}
	{
		goto IL_00de;
	}

IL_005d:
	{
		TextWriter_t4027217640 * L_10 = __this->get_writer_10();
		NullCheck(L_10);
		VirtActionInvoker1< String_t* >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.String) */, L_10, _stringLiteral3062999056);
		goto IL_0146;
	}

IL_0072:
	{
		TextWriter_t4027217640 * L_11 = __this->get_writer_10();
		NullCheck(L_11);
		VirtActionInvoker1< String_t* >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.String) */, L_11, _stringLiteral381169868);
		goto IL_0146;
	}

IL_0087:
	{
		TextWriter_t4027217640 * L_12 = __this->get_writer_10();
		NullCheck(L_12);
		VirtActionInvoker1< String_t* >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.String) */, L_12, _stringLiteral3869568110);
		goto IL_0146;
	}

IL_009c:
	{
		TextWriter_t4027217640 * L_13 = __this->get_writer_10();
		NullCheck(L_13);
		VirtActionInvoker1< Il2CppChar >::Invoke(10 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_13, ((int32_t)92));
		TextWriter_t4027217640 * L_14 = __this->get_writer_10();
		Il2CppChar L_15 = V_2;
		NullCheck(L_14);
		VirtActionInvoker1< Il2CppChar >::Invoke(10 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_14, L_15);
		goto IL_0146;
	}

IL_00ba:
	{
		TextWriter_t4027217640 * L_16 = __this->get_writer_10();
		NullCheck(L_16);
		VirtActionInvoker1< String_t* >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.String) */, L_16, _stringLiteral3419229416);
		goto IL_0146;
	}

IL_00cc:
	{
		TextWriter_t4027217640 * L_17 = __this->get_writer_10();
		NullCheck(L_17);
		VirtActionInvoker1< String_t* >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.String) */, L_17, _stringLiteral1093630588);
		goto IL_0146;
	}

IL_00de:
	{
		Il2CppChar L_18 = V_2;
		if ((((int32_t)L_18) < ((int32_t)((int32_t)32))))
		{
			goto IL_00f6;
		}
	}
	{
		Il2CppChar L_19 = V_2;
		if ((((int32_t)L_19) > ((int32_t)((int32_t)126))))
		{
			goto IL_00f6;
		}
	}
	{
		TextWriter_t4027217640 * L_20 = __this->get_writer_10();
		Il2CppChar L_21 = V_2;
		NullCheck(L_20);
		VirtActionInvoker1< Il2CppChar >::Invoke(10 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_20, L_21);
		goto IL_0146;
	}

IL_00f6:
	{
		Il2CppChar L_22 = V_2;
		if ((((int32_t)L_22) < ((int32_t)((int32_t)32))))
		{
			goto IL_010b;
		}
	}
	{
		Il2CppChar L_23 = V_2;
		if ((((int32_t)L_23) < ((int32_t)((int32_t)128))))
		{
			goto IL_013a;
		}
	}
	{
		Il2CppChar L_24 = V_2;
		if ((((int32_t)L_24) >= ((int32_t)((int32_t)160))))
		{
			goto IL_013a;
		}
	}

IL_010b:
	{
		Il2CppChar L_25 = V_2;
		CharU5BU5D_t1328083999* L_26 = __this->get_hex_seq_4();
		IL2CPP_RUNTIME_CLASS_INIT(JsonWriter_t3014444111_il2cpp_TypeInfo_var);
		JsonWriter_IntToHex_m4158006151(NULL /*static, unused*/, L_25, L_26, /*hidden argument*/NULL);
		TextWriter_t4027217640 * L_27 = __this->get_writer_10();
		NullCheck(L_27);
		VirtActionInvoker1< String_t* >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.String) */, L_27, _stringLiteral2303484169);
		TextWriter_t4027217640 * L_28 = __this->get_writer_10();
		CharU5BU5D_t1328083999* L_29 = __this->get_hex_seq_4();
		NullCheck(L_28);
		VirtActionInvoker1< CharU5BU5D_t1328083999* >::Invoke(11 /* System.Void System.IO.TextWriter::Write(System.Char[]) */, L_28, L_29);
		goto IL_0146;
	}

IL_013a:
	{
		TextWriter_t4027217640 * L_30 = __this->get_writer_10();
		Il2CppChar L_31 = V_2;
		NullCheck(L_30);
		VirtActionInvoker1< Il2CppChar >::Invoke(10 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_30, L_31);
	}

IL_0146:
	{
		int32_t L_32 = V_1;
		V_1 = ((int32_t)((int32_t)L_32+(int32_t)1));
	}

IL_014a:
	{
		int32_t L_33 = V_1;
		int32_t L_34 = V_0;
		if ((((int32_t)L_33) < ((int32_t)L_34)))
		{
			goto IL_0026;
		}
	}
	{
		TextWriter_t4027217640 * L_35 = __this->get_writer_10();
		NullCheck(L_35);
		VirtActionInvoker1< Il2CppChar >::Invoke(10 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_35, ((int32_t)34));
		return;
	}
}
// System.Void ThirdParty.Json.LitJson.JsonWriter::Unindent()
extern "C"  void JsonWriter_Unindent_m339292545 (JsonWriter_t3014444111 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_pretty_print_8();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		int32_t L_1 = __this->get_indentation_5();
		int32_t L_2 = __this->get_indent_value_6();
		__this->set_indentation_5(((int32_t)((int32_t)L_1-(int32_t)L_2)));
	}

IL_001b:
	{
		return;
	}
}
// System.String ThirdParty.Json.LitJson.JsonWriter::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t JsonWriter_ToString_m3989428233_MetadataUsageId;
extern "C"  String_t* JsonWriter_ToString_m3989428233 (JsonWriter_t3014444111 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_ToString_m3989428233_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		StringBuilder_t1221177846 * L_0 = __this->get_inst_string_builder_7();
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_1;
	}

IL_000e:
	{
		StringBuilder_t1221177846 * L_2 = __this->get_inst_string_builder_7();
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		return L_3;
	}
}
// System.Void ThirdParty.Json.LitJson.JsonWriter::Write(System.Decimal)
extern Il2CppClass* JsonWriter_t3014444111_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern const uint32_t JsonWriter_Write_m580542055_MetadataUsageId;
extern "C"  void JsonWriter_Write_m580542055 (JsonWriter_t3014444111 * __this, Decimal_t724701077  ___number0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Write_m580542055_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		JsonWriter_DoValidation_m2832088507(__this, 4, /*hidden argument*/NULL);
		JsonWriter_PutNewline_m1108742881(__this, /*hidden argument*/NULL);
		Decimal_t724701077  L_0 = ___number0;
		IL2CPP_RUNTIME_CLASS_INIT(JsonWriter_t3014444111_il2cpp_TypeInfo_var);
		NumberFormatInfo_t104580544 * L_1 = ((JsonWriter_t3014444111_StaticFields*)JsonWriter_t3014444111_il2cpp_TypeInfo_var->static_fields)->get_number_format_0();
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		String_t* L_2 = Convert_ToString_m629719119(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		JsonWriter_Put_m564997329(__this, L_2, /*hidden argument*/NULL);
		WriterContext_t1209007092 * L_3 = __this->get_context_1();
		NullCheck(L_3);
		L_3->set_ExpectingValue_3((bool)0);
		return;
	}
}
// System.Void ThirdParty.Json.LitJson.JsonWriter::Write(System.Double)
extern Il2CppClass* CultureInfo_t3500843524_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029424;
extern Il2CppCodeGenString* _stringLiteral98844756;
extern const uint32_t JsonWriter_Write_m1257361297_MetadataUsageId;
extern "C"  void JsonWriter_Write_m1257361297 (JsonWriter_t3014444111 * __this, double ___number0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Write_m1257361297_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		JsonWriter_DoValidation_m2832088507(__this, 4, /*hidden argument*/NULL);
		JsonWriter_PutNewline_m1108742881(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3500843524_il2cpp_TypeInfo_var);
		CultureInfo_t3500843524 * L_0 = CultureInfo_get_InvariantCulture_m398972276(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_1 = Double_ToString_m1474956491((&___number0), _stringLiteral372029424, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = V_0;
		JsonWriter_Put_m564997329(__this, L_2, /*hidden argument*/NULL);
		String_t* L_3 = V_0;
		NullCheck(L_3);
		int32_t L_4 = String_IndexOf_m2358239236(L_3, ((int32_t)46), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_4) == ((uint32_t)(-1)))))
		{
			goto IL_004c;
		}
	}
	{
		String_t* L_5 = V_0;
		NullCheck(L_5);
		int32_t L_6 = String_IndexOf_m2358239236(L_5, ((int32_t)69), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_6) == ((uint32_t)(-1)))))
		{
			goto IL_004c;
		}
	}
	{
		TextWriter_t4027217640 * L_7 = __this->get_writer_10();
		NullCheck(L_7);
		VirtActionInvoker1< String_t* >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.String) */, L_7, _stringLiteral98844756);
	}

IL_004c:
	{
		WriterContext_t1209007092 * L_8 = __this->get_context_1();
		NullCheck(L_8);
		L_8->set_ExpectingValue_3((bool)0);
		return;
	}
}
// System.Void ThirdParty.Json.LitJson.JsonWriter::Write(System.Int32)
extern Il2CppClass* JsonWriter_t3014444111_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern const uint32_t JsonWriter_Write_m2327906996_MetadataUsageId;
extern "C"  void JsonWriter_Write_m2327906996 (JsonWriter_t3014444111 * __this, int32_t ___number0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Write_m2327906996_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		JsonWriter_DoValidation_m2832088507(__this, 4, /*hidden argument*/NULL);
		JsonWriter_PutNewline_m1108742881(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___number0;
		IL2CPP_RUNTIME_CLASS_INIT(JsonWriter_t3014444111_il2cpp_TypeInfo_var);
		NumberFormatInfo_t104580544 * L_1 = ((JsonWriter_t3014444111_StaticFields*)JsonWriter_t3014444111_il2cpp_TypeInfo_var->static_fields)->get_number_format_0();
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		String_t* L_2 = Convert_ToString_m3611991202(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		JsonWriter_Put_m564997329(__this, L_2, /*hidden argument*/NULL);
		WriterContext_t1209007092 * L_3 = __this->get_context_1();
		NullCheck(L_3);
		L_3->set_ExpectingValue_3((bool)0);
		return;
	}
}
// System.Void ThirdParty.Json.LitJson.JsonWriter::Write(System.Int64)
extern Il2CppClass* JsonWriter_t3014444111_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern const uint32_t JsonWriter_Write_m1165107417_MetadataUsageId;
extern "C"  void JsonWriter_Write_m1165107417 (JsonWriter_t3014444111 * __this, int64_t ___number0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Write_m1165107417_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		JsonWriter_DoValidation_m2832088507(__this, 4, /*hidden argument*/NULL);
		JsonWriter_PutNewline_m1108742881(__this, /*hidden argument*/NULL);
		int64_t L_0 = ___number0;
		IL2CPP_RUNTIME_CLASS_INIT(JsonWriter_t3014444111_il2cpp_TypeInfo_var);
		NumberFormatInfo_t104580544 * L_1 = ((JsonWriter_t3014444111_StaticFields*)JsonWriter_t3014444111_il2cpp_TypeInfo_var->static_fields)->get_number_format_0();
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		String_t* L_2 = Convert_ToString_m2021429525(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		JsonWriter_Put_m564997329(__this, L_2, /*hidden argument*/NULL);
		WriterContext_t1209007092 * L_3 = __this->get_context_1();
		NullCheck(L_3);
		L_3->set_ExpectingValue_3((bool)0);
		return;
	}
}
// System.Void ThirdParty.Json.LitJson.JsonWriter::Write(System.String)
extern Il2CppCodeGenString* _stringLiteral1743624307;
extern const uint32_t JsonWriter_Write_m3974344637_MetadataUsageId;
extern "C"  void JsonWriter_Write_m3974344637 (JsonWriter_t3014444111 * __this, String_t* ___str0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Write_m3974344637_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		JsonWriter_DoValidation_m2832088507(__this, 4, /*hidden argument*/NULL);
		JsonWriter_PutNewline_m1108742881(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___str0;
		if (L_0)
		{
			goto IL_001d;
		}
	}
	{
		JsonWriter_Put_m564997329(__this, _stringLiteral1743624307, /*hidden argument*/NULL);
		goto IL_0024;
	}

IL_001d:
	{
		String_t* L_1 = ___str0;
		JsonWriter_PutString_m1961797724(__this, L_1, /*hidden argument*/NULL);
	}

IL_0024:
	{
		WriterContext_t1209007092 * L_2 = __this->get_context_1();
		NullCheck(L_2);
		L_2->set_ExpectingValue_3((bool)0);
		return;
	}
}
// System.Void ThirdParty.Json.LitJson.JsonWriter::Write(System.UInt64)
extern Il2CppClass* JsonWriter_t3014444111_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern const uint32_t JsonWriter_Write_m281441472_MetadataUsageId;
extern "C"  void JsonWriter_Write_m281441472 (JsonWriter_t3014444111 * __this, uint64_t ___number0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Write_m281441472_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		JsonWriter_DoValidation_m2832088507(__this, 4, /*hidden argument*/NULL);
		JsonWriter_PutNewline_m1108742881(__this, /*hidden argument*/NULL);
		uint64_t L_0 = ___number0;
		IL2CPP_RUNTIME_CLASS_INIT(JsonWriter_t3014444111_il2cpp_TypeInfo_var);
		NumberFormatInfo_t104580544 * L_1 = ((JsonWriter_t3014444111_StaticFields*)JsonWriter_t3014444111_il2cpp_TypeInfo_var->static_fields)->get_number_format_0();
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		String_t* L_2 = Convert_ToString_m3551783374(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		JsonWriter_Put_m564997329(__this, L_2, /*hidden argument*/NULL);
		WriterContext_t1209007092 * L_3 = __this->get_context_1();
		NullCheck(L_3);
		L_3->set_ExpectingValue_3((bool)0);
		return;
	}
}
// System.Void ThirdParty.Json.LitJson.JsonWriter::Write(System.DateTime)
extern Il2CppClass* AWSSDKUtils_t2036360342_il2cpp_TypeInfo_var;
extern Il2CppClass* CultureInfo_t3500843524_il2cpp_TypeInfo_var;
extern const uint32_t JsonWriter_Write_m3398380085_MetadataUsageId;
extern "C"  void JsonWriter_Write_m3398380085 (JsonWriter_t3014444111 * __this, DateTime_t693205669  ___date0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Write_m3398380085_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	double V_0 = 0.0;
	{
		JsonWriter_DoValidation_m2832088507(__this, 4, /*hidden argument*/NULL);
		JsonWriter_PutNewline_m1108742881(__this, /*hidden argument*/NULL);
		DateTime_t693205669  L_0 = ___date0;
		IL2CPP_RUNTIME_CLASS_INIT(AWSSDKUtils_t2036360342_il2cpp_TypeInfo_var);
		double L_1 = AWSSDKUtils_ConvertToUnixEpochMilliSeconds_m936372869(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3500843524_il2cpp_TypeInfo_var);
		CultureInfo_t3500843524 * L_2 = CultureInfo_get_InvariantCulture_m398972276(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_3 = Double_ToString_m1085043609((&V_0), L_2, /*hidden argument*/NULL);
		JsonWriter_Put_m564997329(__this, L_3, /*hidden argument*/NULL);
		WriterContext_t1209007092 * L_4 = __this->get_context_1();
		NullCheck(L_4);
		L_4->set_ExpectingValue_3((bool)0);
		return;
	}
}
// System.Void ThirdParty.Json.LitJson.JsonWriter::WriteArrayEnd()
extern const MethodInfo* Stack_1_Pop_m2652175638_MethodInfo_var;
extern const MethodInfo* Stack_1_get_Count_m4227312143_MethodInfo_var;
extern const MethodInfo* Stack_1_Peek_m1248577930_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029425;
extern const uint32_t JsonWriter_WriteArrayEnd_m361558409_MetadataUsageId;
extern "C"  void JsonWriter_WriteArrayEnd_m361558409 (JsonWriter_t3014444111 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_WriteArrayEnd_m361558409_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		JsonWriter_DoValidation_m2832088507(__this, 0, /*hidden argument*/NULL);
		JsonWriter_PutNewline_m347485110(__this, (bool)0, /*hidden argument*/NULL);
		Stack_1_t2296735246 * L_0 = __this->get_ctx_stack_2();
		NullCheck(L_0);
		Stack_1_Pop_m2652175638(L_0, /*hidden argument*/Stack_1_Pop_m2652175638_MethodInfo_var);
		Stack_1_t2296735246 * L_1 = __this->get_ctx_stack_2();
		NullCheck(L_1);
		int32_t L_2 = Stack_1_get_Count_m4227312143(L_1, /*hidden argument*/Stack_1_get_Count_m4227312143_MethodInfo_var);
		if ((!(((uint32_t)L_2) == ((uint32_t)1))))
		{
			goto IL_0031;
		}
	}
	{
		__this->set_has_reached_end_3((bool)1);
		goto IL_004e;
	}

IL_0031:
	{
		Stack_1_t2296735246 * L_3 = __this->get_ctx_stack_2();
		NullCheck(L_3);
		WriterContext_t1209007092 * L_4 = Stack_1_Peek_m1248577930(L_3, /*hidden argument*/Stack_1_Peek_m1248577930_MethodInfo_var);
		__this->set_context_1(L_4);
		WriterContext_t1209007092 * L_5 = __this->get_context_1();
		NullCheck(L_5);
		L_5->set_ExpectingValue_3((bool)0);
	}

IL_004e:
	{
		JsonWriter_Unindent_m339292545(__this, /*hidden argument*/NULL);
		JsonWriter_Put_m564997329(__this, _stringLiteral372029425, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ThirdParty.Json.LitJson.JsonWriter::WriteArrayStart()
extern Il2CppClass* WriterContext_t1209007092_il2cpp_TypeInfo_var;
extern const MethodInfo* Stack_1_Push_m193241330_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029431;
extern const uint32_t JsonWriter_WriteArrayStart_m2755874534_MetadataUsageId;
extern "C"  void JsonWriter_WriteArrayStart_m2755874534 (JsonWriter_t3014444111 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_WriteArrayStart_m2755874534_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		JsonWriter_DoValidation_m2832088507(__this, 2, /*hidden argument*/NULL);
		JsonWriter_PutNewline_m1108742881(__this, /*hidden argument*/NULL);
		JsonWriter_Put_m564997329(__this, _stringLiteral372029431, /*hidden argument*/NULL);
		WriterContext_t1209007092 * L_0 = (WriterContext_t1209007092 *)il2cpp_codegen_object_new(WriterContext_t1209007092_il2cpp_TypeInfo_var);
		WriterContext__ctor_m2481251725(L_0, /*hidden argument*/NULL);
		__this->set_context_1(L_0);
		WriterContext_t1209007092 * L_1 = __this->get_context_1();
		NullCheck(L_1);
		L_1->set_InArray_1((bool)1);
		Stack_1_t2296735246 * L_2 = __this->get_ctx_stack_2();
		WriterContext_t1209007092 * L_3 = __this->get_context_1();
		NullCheck(L_2);
		Stack_1_Push_m193241330(L_2, L_3, /*hidden argument*/Stack_1_Push_m193241330_MethodInfo_var);
		JsonWriter_Indent_m2307597358(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ThirdParty.Json.LitJson.JsonWriter::WriteObjectEnd()
extern const MethodInfo* Stack_1_Pop_m2652175638_MethodInfo_var;
extern const MethodInfo* Stack_1_get_Count_m4227312143_MethodInfo_var;
extern const MethodInfo* Stack_1_Peek_m1248577930_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029393;
extern const uint32_t JsonWriter_WriteObjectEnd_m4253837441_MetadataUsageId;
extern "C"  void JsonWriter_WriteObjectEnd_m4253837441 (JsonWriter_t3014444111 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_WriteObjectEnd_m4253837441_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		JsonWriter_DoValidation_m2832088507(__this, 1, /*hidden argument*/NULL);
		JsonWriter_PutNewline_m347485110(__this, (bool)0, /*hidden argument*/NULL);
		Stack_1_t2296735246 * L_0 = __this->get_ctx_stack_2();
		NullCheck(L_0);
		Stack_1_Pop_m2652175638(L_0, /*hidden argument*/Stack_1_Pop_m2652175638_MethodInfo_var);
		Stack_1_t2296735246 * L_1 = __this->get_ctx_stack_2();
		NullCheck(L_1);
		int32_t L_2 = Stack_1_get_Count_m4227312143(L_1, /*hidden argument*/Stack_1_get_Count_m4227312143_MethodInfo_var);
		if ((!(((uint32_t)L_2) == ((uint32_t)1))))
		{
			goto IL_0031;
		}
	}
	{
		__this->set_has_reached_end_3((bool)1);
		goto IL_004e;
	}

IL_0031:
	{
		Stack_1_t2296735246 * L_3 = __this->get_ctx_stack_2();
		NullCheck(L_3);
		WriterContext_t1209007092 * L_4 = Stack_1_Peek_m1248577930(L_3, /*hidden argument*/Stack_1_Peek_m1248577930_MethodInfo_var);
		__this->set_context_1(L_4);
		WriterContext_t1209007092 * L_5 = __this->get_context_1();
		NullCheck(L_5);
		L_5->set_ExpectingValue_3((bool)0);
	}

IL_004e:
	{
		JsonWriter_Unindent_m339292545(__this, /*hidden argument*/NULL);
		JsonWriter_Put_m564997329(__this, _stringLiteral372029393, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ThirdParty.Json.LitJson.JsonWriter::WriteObjectStart()
extern Il2CppClass* WriterContext_t1209007092_il2cpp_TypeInfo_var;
extern const MethodInfo* Stack_1_Push_m193241330_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029399;
extern const uint32_t JsonWriter_WriteObjectStart_m3468068136_MetadataUsageId;
extern "C"  void JsonWriter_WriteObjectStart_m3468068136 (JsonWriter_t3014444111 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_WriteObjectStart_m3468068136_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		JsonWriter_DoValidation_m2832088507(__this, 2, /*hidden argument*/NULL);
		JsonWriter_PutNewline_m1108742881(__this, /*hidden argument*/NULL);
		JsonWriter_Put_m564997329(__this, _stringLiteral372029399, /*hidden argument*/NULL);
		WriterContext_t1209007092 * L_0 = (WriterContext_t1209007092 *)il2cpp_codegen_object_new(WriterContext_t1209007092_il2cpp_TypeInfo_var);
		WriterContext__ctor_m2481251725(L_0, /*hidden argument*/NULL);
		__this->set_context_1(L_0);
		WriterContext_t1209007092 * L_1 = __this->get_context_1();
		NullCheck(L_1);
		L_1->set_InObject_2((bool)1);
		Stack_1_t2296735246 * L_2 = __this->get_ctx_stack_2();
		WriterContext_t1209007092 * L_3 = __this->get_context_1();
		NullCheck(L_2);
		Stack_1_Push_m193241330(L_2, L_3, /*hidden argument*/Stack_1_Push_m193241330_MethodInfo_var);
		JsonWriter_Indent_m2307597358(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ThirdParty.Json.LitJson.JsonWriter::WritePropertyName(System.String)
extern Il2CppCodeGenString* _stringLiteral811305496;
extern const uint32_t JsonWriter_WritePropertyName_m1171400361_MetadataUsageId;
extern "C"  void JsonWriter_WritePropertyName_m1171400361 (JsonWriter_t3014444111 * __this, String_t* ___property_name0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_WritePropertyName_m1171400361_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		JsonWriter_DoValidation_m2832088507(__this, 3, /*hidden argument*/NULL);
		JsonWriter_PutNewline_m1108742881(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___property_name0;
		JsonWriter_PutString_m1961797724(__this, L_0, /*hidden argument*/NULL);
		bool L_1 = __this->get_pretty_print_8();
		if (!L_1)
		{
			goto IL_007c;
		}
	}
	{
		String_t* L_2 = ___property_name0;
		NullCheck(L_2);
		int32_t L_3 = String_get_Length_m1606060069(L_2, /*hidden argument*/NULL);
		WriterContext_t1209007092 * L_4 = __this->get_context_1();
		NullCheck(L_4);
		int32_t L_5 = L_4->get_Padding_4();
		if ((((int32_t)L_3) <= ((int32_t)L_5)))
		{
			goto IL_0040;
		}
	}
	{
		WriterContext_t1209007092 * L_6 = __this->get_context_1();
		String_t* L_7 = ___property_name0;
		NullCheck(L_7);
		int32_t L_8 = String_get_Length_m1606060069(L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		L_6->set_Padding_4(L_8);
	}

IL_0040:
	{
		WriterContext_t1209007092 * L_9 = __this->get_context_1();
		NullCheck(L_9);
		int32_t L_10 = L_9->get_Padding_4();
		String_t* L_11 = ___property_name0;
		NullCheck(L_11);
		int32_t L_12 = String_get_Length_m1606060069(L_11, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_10-(int32_t)L_12));
		goto IL_0066;
	}

IL_0055:
	{
		TextWriter_t4027217640 * L_13 = __this->get_writer_10();
		NullCheck(L_13);
		VirtActionInvoker1< Il2CppChar >::Invoke(10 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_13, ((int32_t)32));
		int32_t L_14 = V_0;
		V_0 = ((int32_t)((int32_t)L_14-(int32_t)1));
	}

IL_0066:
	{
		int32_t L_15 = V_0;
		if ((((int32_t)L_15) >= ((int32_t)0)))
		{
			goto IL_0055;
		}
	}
	{
		TextWriter_t4027217640 * L_16 = __this->get_writer_10();
		NullCheck(L_16);
		VirtActionInvoker1< String_t* >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.String) */, L_16, _stringLiteral811305496);
		goto IL_0089;
	}

IL_007c:
	{
		TextWriter_t4027217640 * L_17 = __this->get_writer_10();
		NullCheck(L_17);
		VirtActionInvoker1< Il2CppChar >::Invoke(10 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_17, ((int32_t)58));
	}

IL_0089:
	{
		WriterContext_t1209007092 * L_18 = __this->get_context_1();
		NullCheck(L_18);
		L_18->set_ExpectingValue_3((bool)1);
		return;
	}
}
// System.Boolean ThirdParty.Json.LitJson.Lexer::get_EndOfInput()
extern "C"  bool Lexer_get_EndOfInput_m3261687552 (Lexer_t954714164 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_end_of_input_4();
		return L_0;
	}
}
// System.Int32 ThirdParty.Json.LitJson.Lexer::get_Token()
extern "C"  int32_t Lexer_get_Token_m4181743523 (Lexer_t954714164 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_token_12();
		return L_0;
	}
}
// System.String ThirdParty.Json.LitJson.Lexer::get_StringValue()
extern "C"  String_t* Lexer_get_StringValue_m2285872777 (Lexer_t954714164 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_string_value_11();
		return L_0;
	}
}
// System.Void ThirdParty.Json.LitJson.Lexer::.cctor()
extern Il2CppClass* Lexer_t954714164_il2cpp_TypeInfo_var;
extern const uint32_t Lexer__cctor_m2221215066_MetadataUsageId;
extern "C"  void Lexer__cctor_m2221215066 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Lexer__cctor_m2221215066_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Lexer_PopulateFsmTables_m484075034(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ThirdParty.Json.LitJson.Lexer::.ctor(System.IO.TextReader)
extern Il2CppClass* StringBuilder_t1221177846_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmContext_t4275593467_il2cpp_TypeInfo_var;
extern const uint32_t Lexer__ctor_m786021532_MetadataUsageId;
extern "C"  void Lexer__ctor_m786021532 (Lexer_t954714164 * __this, TextReader_t1561828458 * ___reader0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Lexer__ctor_m786021532_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		__this->set_allow_comments_2((bool)1);
		__this->set_allow_single_quoted_strings_3((bool)1);
		__this->set_input_buffer_6(0);
		StringBuilder_t1221177846 * L_0 = (StringBuilder_t1221177846 *)il2cpp_codegen_object_new(StringBuilder_t1221177846_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m536337337(L_0, ((int32_t)128), /*hidden argument*/NULL);
		__this->set_string_buffer_10(L_0);
		__this->set_state_9(1);
		__this->set_end_of_input_4((bool)0);
		TextReader_t1561828458 * L_1 = ___reader0;
		__this->set_reader_8(L_1);
		FsmContext_t4275593467 * L_2 = (FsmContext_t4275593467 *)il2cpp_codegen_object_new(FsmContext_t4275593467_il2cpp_TypeInfo_var);
		FsmContext__ctor_m1039323702(L_2, /*hidden argument*/NULL);
		__this->set_fsm_context_5(L_2);
		FsmContext_t4275593467 * L_3 = __this->get_fsm_context_5();
		NullCheck(L_3);
		L_3->set_L_2(__this);
		return;
	}
}
// System.Int32 ThirdParty.Json.LitJson.Lexer::HexValue(System.Int32)
extern "C"  int32_t Lexer_HexValue_m4231875178 (Il2CppObject * __this /* static, unused */, int32_t ___digit0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___digit0;
		if (((int32_t)((int32_t)L_0-(int32_t)((int32_t)65))) == 0)
		{
			goto IL_0044;
		}
		if (((int32_t)((int32_t)L_0-(int32_t)((int32_t)65))) == 1)
		{
			goto IL_0047;
		}
		if (((int32_t)((int32_t)L_0-(int32_t)((int32_t)65))) == 2)
		{
			goto IL_004a;
		}
		if (((int32_t)((int32_t)L_0-(int32_t)((int32_t)65))) == 3)
		{
			goto IL_004d;
		}
		if (((int32_t)((int32_t)L_0-(int32_t)((int32_t)65))) == 4)
		{
			goto IL_0050;
		}
		if (((int32_t)((int32_t)L_0-(int32_t)((int32_t)65))) == 5)
		{
			goto IL_0053;
		}
	}
	{
		int32_t L_1 = ___digit0;
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)97))) == 0)
		{
			goto IL_0044;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)97))) == 1)
		{
			goto IL_0047;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)97))) == 2)
		{
			goto IL_004a;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)97))) == 3)
		{
			goto IL_004d;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)97))) == 4)
		{
			goto IL_0050;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)97))) == 5)
		{
			goto IL_0053;
		}
	}
	{
		goto IL_0056;
	}

IL_0044:
	{
		return ((int32_t)10);
	}

IL_0047:
	{
		return ((int32_t)11);
	}

IL_004a:
	{
		return ((int32_t)12);
	}

IL_004d:
	{
		return ((int32_t)13);
	}

IL_0050:
	{
		return ((int32_t)14);
	}

IL_0053:
	{
		return ((int32_t)15);
	}

IL_0056:
	{
		int32_t L_2 = ___digit0;
		return ((int32_t)((int32_t)L_2-(int32_t)((int32_t)48)));
	}
}
// System.Void ThirdParty.Json.LitJson.Lexer::PopulateFsmTables()
extern Il2CppClass* StateHandlerU5BU5D_t1374508575_il2cpp_TypeInfo_var;
extern Il2CppClass* StateHandler_t3489987002_il2cpp_TypeInfo_var;
extern Il2CppClass* Lexer_t954714164_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var;
extern const MethodInfo* Lexer_State1_m2106225368_MethodInfo_var;
extern const MethodInfo* Lexer_State2_m934338055_MethodInfo_var;
extern const MethodInfo* Lexer_State3_m3343688098_MethodInfo_var;
extern const MethodInfo* Lexer_State4_m271821993_MethodInfo_var;
extern const MethodInfo* Lexer_State5_m2681172036_MethodInfo_var;
extern const MethodInfo* Lexer_State6_m1509284723_MethodInfo_var;
extern const MethodInfo* Lexer_State7_m3918634766_MethodInfo_var;
extern const MethodInfo* Lexer_State8_m3318232085_MethodInfo_var;
extern const MethodInfo* Lexer_State9_m1432614832_MethodInfo_var;
extern const MethodInfo* Lexer_State10_m2160325200_MethodInfo_var;
extern const MethodInfo* Lexer_State11_m621563729_MethodInfo_var;
extern const MethodInfo* Lexer_State12_m425405010_MethodInfo_var;
extern const MethodInfo* Lexer_State13_m3181610835_MethodInfo_var;
extern const MethodInfo* Lexer_State14_m2103223124_MethodInfo_var;
extern const MethodInfo* Lexer_State15_m564461653_MethodInfo_var;
extern const MethodInfo* Lexer_State16_m368302934_MethodInfo_var;
extern const MethodInfo* Lexer_State17_m3124508759_MethodInfo_var;
extern const MethodInfo* Lexer_State18_m3963376200_MethodInfo_var;
extern const MethodInfo* Lexer_State19_m2424614729_MethodInfo_var;
extern const MethodInfo* Lexer_State20_m3589906367_MethodInfo_var;
extern const MethodInfo* Lexer_State21_m2051144896_MethodInfo_var;
extern const MethodInfo* Lexer_State22_m1854986177_MethodInfo_var;
extern const MethodInfo* Lexer_State23_m316224706_MethodInfo_var;
extern const MethodInfo* Lexer_State24_m3532804291_MethodInfo_var;
extern const MethodInfo* Lexer_State25_m1994042820_MethodInfo_var;
extern const MethodInfo* Lexer_State26_m1797884101_MethodInfo_var;
extern const MethodInfo* Lexer_State27_m259122630_MethodInfo_var;
extern const MethodInfo* Lexer_State28_m1097990071_MethodInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t1486305143____50B1635D1FB2907A171B71751E1A3FA79423CA17_0_FieldInfo_var;
extern const uint32_t Lexer_PopulateFsmTables_m484075034_MetadataUsageId;
extern "C"  void Lexer_PopulateFsmTables_m484075034 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Lexer_PopulateFsmTables_m484075034_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		StateHandlerU5BU5D_t1374508575* L_0 = ((StateHandlerU5BU5D_t1374508575*)SZArrayNew(StateHandlerU5BU5D_t1374508575_il2cpp_TypeInfo_var, (uint32_t)((int32_t)28)));
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)Lexer_State1_m2106225368_MethodInfo_var);
		StateHandler_t3489987002 * L_2 = (StateHandler_t3489987002 *)il2cpp_codegen_object_new(StateHandler_t3489987002_il2cpp_TypeInfo_var);
		StateHandler__ctor_m1057417747(L_2, NULL, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_2);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (StateHandler_t3489987002 *)L_2);
		StateHandlerU5BU5D_t1374508575* L_3 = L_0;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)Lexer_State2_m934338055_MethodInfo_var);
		StateHandler_t3489987002 * L_5 = (StateHandler_t3489987002 *)il2cpp_codegen_object_new(StateHandler_t3489987002_il2cpp_TypeInfo_var);
		StateHandler__ctor_m1057417747(L_5, NULL, L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, L_5);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (StateHandler_t3489987002 *)L_5);
		StateHandlerU5BU5D_t1374508575* L_6 = L_3;
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)Lexer_State3_m3343688098_MethodInfo_var);
		StateHandler_t3489987002 * L_8 = (StateHandler_t3489987002 *)il2cpp_codegen_object_new(StateHandler_t3489987002_il2cpp_TypeInfo_var);
		StateHandler__ctor_m1057417747(L_8, NULL, L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_8);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (StateHandler_t3489987002 *)L_8);
		StateHandlerU5BU5D_t1374508575* L_9 = L_6;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)Lexer_State4_m271821993_MethodInfo_var);
		StateHandler_t3489987002 * L_11 = (StateHandler_t3489987002 *)il2cpp_codegen_object_new(StateHandler_t3489987002_il2cpp_TypeInfo_var);
		StateHandler__ctor_m1057417747(L_11, NULL, L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, L_11);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(3), (StateHandler_t3489987002 *)L_11);
		StateHandlerU5BU5D_t1374508575* L_12 = L_9;
		IntPtr_t L_13;
		L_13.set_m_value_0((void*)(void*)Lexer_State5_m2681172036_MethodInfo_var);
		StateHandler_t3489987002 * L_14 = (StateHandler_t3489987002 *)il2cpp_codegen_object_new(StateHandler_t3489987002_il2cpp_TypeInfo_var);
		StateHandler__ctor_m1057417747(L_14, NULL, L_13, /*hidden argument*/NULL);
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_14);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (StateHandler_t3489987002 *)L_14);
		StateHandlerU5BU5D_t1374508575* L_15 = L_12;
		IntPtr_t L_16;
		L_16.set_m_value_0((void*)(void*)Lexer_State6_m1509284723_MethodInfo_var);
		StateHandler_t3489987002 * L_17 = (StateHandler_t3489987002 *)il2cpp_codegen_object_new(StateHandler_t3489987002_il2cpp_TypeInfo_var);
		StateHandler__ctor_m1057417747(L_17, NULL, L_16, /*hidden argument*/NULL);
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, L_17);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(5), (StateHandler_t3489987002 *)L_17);
		StateHandlerU5BU5D_t1374508575* L_18 = L_15;
		IntPtr_t L_19;
		L_19.set_m_value_0((void*)(void*)Lexer_State7_m3918634766_MethodInfo_var);
		StateHandler_t3489987002 * L_20 = (StateHandler_t3489987002 *)il2cpp_codegen_object_new(StateHandler_t3489987002_il2cpp_TypeInfo_var);
		StateHandler__ctor_m1057417747(L_20, NULL, L_19, /*hidden argument*/NULL);
		NullCheck(L_18);
		ArrayElementTypeCheck (L_18, L_20);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(6), (StateHandler_t3489987002 *)L_20);
		StateHandlerU5BU5D_t1374508575* L_21 = L_18;
		IntPtr_t L_22;
		L_22.set_m_value_0((void*)(void*)Lexer_State8_m3318232085_MethodInfo_var);
		StateHandler_t3489987002 * L_23 = (StateHandler_t3489987002 *)il2cpp_codegen_object_new(StateHandler_t3489987002_il2cpp_TypeInfo_var);
		StateHandler__ctor_m1057417747(L_23, NULL, L_22, /*hidden argument*/NULL);
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, L_23);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(7), (StateHandler_t3489987002 *)L_23);
		StateHandlerU5BU5D_t1374508575* L_24 = L_21;
		IntPtr_t L_25;
		L_25.set_m_value_0((void*)(void*)Lexer_State9_m1432614832_MethodInfo_var);
		StateHandler_t3489987002 * L_26 = (StateHandler_t3489987002 *)il2cpp_codegen_object_new(StateHandler_t3489987002_il2cpp_TypeInfo_var);
		StateHandler__ctor_m1057417747(L_26, NULL, L_25, /*hidden argument*/NULL);
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, L_26);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(8), (StateHandler_t3489987002 *)L_26);
		StateHandlerU5BU5D_t1374508575* L_27 = L_24;
		IntPtr_t L_28;
		L_28.set_m_value_0((void*)(void*)Lexer_State10_m2160325200_MethodInfo_var);
		StateHandler_t3489987002 * L_29 = (StateHandler_t3489987002 *)il2cpp_codegen_object_new(StateHandler_t3489987002_il2cpp_TypeInfo_var);
		StateHandler__ctor_m1057417747(L_29, NULL, L_28, /*hidden argument*/NULL);
		NullCheck(L_27);
		ArrayElementTypeCheck (L_27, L_29);
		(L_27)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (StateHandler_t3489987002 *)L_29);
		StateHandlerU5BU5D_t1374508575* L_30 = L_27;
		IntPtr_t L_31;
		L_31.set_m_value_0((void*)(void*)Lexer_State11_m621563729_MethodInfo_var);
		StateHandler_t3489987002 * L_32 = (StateHandler_t3489987002 *)il2cpp_codegen_object_new(StateHandler_t3489987002_il2cpp_TypeInfo_var);
		StateHandler__ctor_m1057417747(L_32, NULL, L_31, /*hidden argument*/NULL);
		NullCheck(L_30);
		ArrayElementTypeCheck (L_30, L_32);
		(L_30)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)10)), (StateHandler_t3489987002 *)L_32);
		StateHandlerU5BU5D_t1374508575* L_33 = L_30;
		IntPtr_t L_34;
		L_34.set_m_value_0((void*)(void*)Lexer_State12_m425405010_MethodInfo_var);
		StateHandler_t3489987002 * L_35 = (StateHandler_t3489987002 *)il2cpp_codegen_object_new(StateHandler_t3489987002_il2cpp_TypeInfo_var);
		StateHandler__ctor_m1057417747(L_35, NULL, L_34, /*hidden argument*/NULL);
		NullCheck(L_33);
		ArrayElementTypeCheck (L_33, L_35);
		(L_33)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)11)), (StateHandler_t3489987002 *)L_35);
		StateHandlerU5BU5D_t1374508575* L_36 = L_33;
		IntPtr_t L_37;
		L_37.set_m_value_0((void*)(void*)Lexer_State13_m3181610835_MethodInfo_var);
		StateHandler_t3489987002 * L_38 = (StateHandler_t3489987002 *)il2cpp_codegen_object_new(StateHandler_t3489987002_il2cpp_TypeInfo_var);
		StateHandler__ctor_m1057417747(L_38, NULL, L_37, /*hidden argument*/NULL);
		NullCheck(L_36);
		ArrayElementTypeCheck (L_36, L_38);
		(L_36)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)12)), (StateHandler_t3489987002 *)L_38);
		StateHandlerU5BU5D_t1374508575* L_39 = L_36;
		IntPtr_t L_40;
		L_40.set_m_value_0((void*)(void*)Lexer_State14_m2103223124_MethodInfo_var);
		StateHandler_t3489987002 * L_41 = (StateHandler_t3489987002 *)il2cpp_codegen_object_new(StateHandler_t3489987002_il2cpp_TypeInfo_var);
		StateHandler__ctor_m1057417747(L_41, NULL, L_40, /*hidden argument*/NULL);
		NullCheck(L_39);
		ArrayElementTypeCheck (L_39, L_41);
		(L_39)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)13)), (StateHandler_t3489987002 *)L_41);
		StateHandlerU5BU5D_t1374508575* L_42 = L_39;
		IntPtr_t L_43;
		L_43.set_m_value_0((void*)(void*)Lexer_State15_m564461653_MethodInfo_var);
		StateHandler_t3489987002 * L_44 = (StateHandler_t3489987002 *)il2cpp_codegen_object_new(StateHandler_t3489987002_il2cpp_TypeInfo_var);
		StateHandler__ctor_m1057417747(L_44, NULL, L_43, /*hidden argument*/NULL);
		NullCheck(L_42);
		ArrayElementTypeCheck (L_42, L_44);
		(L_42)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)14)), (StateHandler_t3489987002 *)L_44);
		StateHandlerU5BU5D_t1374508575* L_45 = L_42;
		IntPtr_t L_46;
		L_46.set_m_value_0((void*)(void*)Lexer_State16_m368302934_MethodInfo_var);
		StateHandler_t3489987002 * L_47 = (StateHandler_t3489987002 *)il2cpp_codegen_object_new(StateHandler_t3489987002_il2cpp_TypeInfo_var);
		StateHandler__ctor_m1057417747(L_47, NULL, L_46, /*hidden argument*/NULL);
		NullCheck(L_45);
		ArrayElementTypeCheck (L_45, L_47);
		(L_45)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)15)), (StateHandler_t3489987002 *)L_47);
		StateHandlerU5BU5D_t1374508575* L_48 = L_45;
		IntPtr_t L_49;
		L_49.set_m_value_0((void*)(void*)Lexer_State17_m3124508759_MethodInfo_var);
		StateHandler_t3489987002 * L_50 = (StateHandler_t3489987002 *)il2cpp_codegen_object_new(StateHandler_t3489987002_il2cpp_TypeInfo_var);
		StateHandler__ctor_m1057417747(L_50, NULL, L_49, /*hidden argument*/NULL);
		NullCheck(L_48);
		ArrayElementTypeCheck (L_48, L_50);
		(L_48)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)16)), (StateHandler_t3489987002 *)L_50);
		StateHandlerU5BU5D_t1374508575* L_51 = L_48;
		IntPtr_t L_52;
		L_52.set_m_value_0((void*)(void*)Lexer_State18_m3963376200_MethodInfo_var);
		StateHandler_t3489987002 * L_53 = (StateHandler_t3489987002 *)il2cpp_codegen_object_new(StateHandler_t3489987002_il2cpp_TypeInfo_var);
		StateHandler__ctor_m1057417747(L_53, NULL, L_52, /*hidden argument*/NULL);
		NullCheck(L_51);
		ArrayElementTypeCheck (L_51, L_53);
		(L_51)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)17)), (StateHandler_t3489987002 *)L_53);
		StateHandlerU5BU5D_t1374508575* L_54 = L_51;
		IntPtr_t L_55;
		L_55.set_m_value_0((void*)(void*)Lexer_State19_m2424614729_MethodInfo_var);
		StateHandler_t3489987002 * L_56 = (StateHandler_t3489987002 *)il2cpp_codegen_object_new(StateHandler_t3489987002_il2cpp_TypeInfo_var);
		StateHandler__ctor_m1057417747(L_56, NULL, L_55, /*hidden argument*/NULL);
		NullCheck(L_54);
		ArrayElementTypeCheck (L_54, L_56);
		(L_54)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)18)), (StateHandler_t3489987002 *)L_56);
		StateHandlerU5BU5D_t1374508575* L_57 = L_54;
		IntPtr_t L_58;
		L_58.set_m_value_0((void*)(void*)Lexer_State20_m3589906367_MethodInfo_var);
		StateHandler_t3489987002 * L_59 = (StateHandler_t3489987002 *)il2cpp_codegen_object_new(StateHandler_t3489987002_il2cpp_TypeInfo_var);
		StateHandler__ctor_m1057417747(L_59, NULL, L_58, /*hidden argument*/NULL);
		NullCheck(L_57);
		ArrayElementTypeCheck (L_57, L_59);
		(L_57)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)19)), (StateHandler_t3489987002 *)L_59);
		StateHandlerU5BU5D_t1374508575* L_60 = L_57;
		IntPtr_t L_61;
		L_61.set_m_value_0((void*)(void*)Lexer_State21_m2051144896_MethodInfo_var);
		StateHandler_t3489987002 * L_62 = (StateHandler_t3489987002 *)il2cpp_codegen_object_new(StateHandler_t3489987002_il2cpp_TypeInfo_var);
		StateHandler__ctor_m1057417747(L_62, NULL, L_61, /*hidden argument*/NULL);
		NullCheck(L_60);
		ArrayElementTypeCheck (L_60, L_62);
		(L_60)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)20)), (StateHandler_t3489987002 *)L_62);
		StateHandlerU5BU5D_t1374508575* L_63 = L_60;
		IntPtr_t L_64;
		L_64.set_m_value_0((void*)(void*)Lexer_State22_m1854986177_MethodInfo_var);
		StateHandler_t3489987002 * L_65 = (StateHandler_t3489987002 *)il2cpp_codegen_object_new(StateHandler_t3489987002_il2cpp_TypeInfo_var);
		StateHandler__ctor_m1057417747(L_65, NULL, L_64, /*hidden argument*/NULL);
		NullCheck(L_63);
		ArrayElementTypeCheck (L_63, L_65);
		(L_63)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)21)), (StateHandler_t3489987002 *)L_65);
		StateHandlerU5BU5D_t1374508575* L_66 = L_63;
		IntPtr_t L_67;
		L_67.set_m_value_0((void*)(void*)Lexer_State23_m316224706_MethodInfo_var);
		StateHandler_t3489987002 * L_68 = (StateHandler_t3489987002 *)il2cpp_codegen_object_new(StateHandler_t3489987002_il2cpp_TypeInfo_var);
		StateHandler__ctor_m1057417747(L_68, NULL, L_67, /*hidden argument*/NULL);
		NullCheck(L_66);
		ArrayElementTypeCheck (L_66, L_68);
		(L_66)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)22)), (StateHandler_t3489987002 *)L_68);
		StateHandlerU5BU5D_t1374508575* L_69 = L_66;
		IntPtr_t L_70;
		L_70.set_m_value_0((void*)(void*)Lexer_State24_m3532804291_MethodInfo_var);
		StateHandler_t3489987002 * L_71 = (StateHandler_t3489987002 *)il2cpp_codegen_object_new(StateHandler_t3489987002_il2cpp_TypeInfo_var);
		StateHandler__ctor_m1057417747(L_71, NULL, L_70, /*hidden argument*/NULL);
		NullCheck(L_69);
		ArrayElementTypeCheck (L_69, L_71);
		(L_69)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)23)), (StateHandler_t3489987002 *)L_71);
		StateHandlerU5BU5D_t1374508575* L_72 = L_69;
		IntPtr_t L_73;
		L_73.set_m_value_0((void*)(void*)Lexer_State25_m1994042820_MethodInfo_var);
		StateHandler_t3489987002 * L_74 = (StateHandler_t3489987002 *)il2cpp_codegen_object_new(StateHandler_t3489987002_il2cpp_TypeInfo_var);
		StateHandler__ctor_m1057417747(L_74, NULL, L_73, /*hidden argument*/NULL);
		NullCheck(L_72);
		ArrayElementTypeCheck (L_72, L_74);
		(L_72)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)24)), (StateHandler_t3489987002 *)L_74);
		StateHandlerU5BU5D_t1374508575* L_75 = L_72;
		IntPtr_t L_76;
		L_76.set_m_value_0((void*)(void*)Lexer_State26_m1797884101_MethodInfo_var);
		StateHandler_t3489987002 * L_77 = (StateHandler_t3489987002 *)il2cpp_codegen_object_new(StateHandler_t3489987002_il2cpp_TypeInfo_var);
		StateHandler__ctor_m1057417747(L_77, NULL, L_76, /*hidden argument*/NULL);
		NullCheck(L_75);
		ArrayElementTypeCheck (L_75, L_77);
		(L_75)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)25)), (StateHandler_t3489987002 *)L_77);
		StateHandlerU5BU5D_t1374508575* L_78 = L_75;
		IntPtr_t L_79;
		L_79.set_m_value_0((void*)(void*)Lexer_State27_m259122630_MethodInfo_var);
		StateHandler_t3489987002 * L_80 = (StateHandler_t3489987002 *)il2cpp_codegen_object_new(StateHandler_t3489987002_il2cpp_TypeInfo_var);
		StateHandler__ctor_m1057417747(L_80, NULL, L_79, /*hidden argument*/NULL);
		NullCheck(L_78);
		ArrayElementTypeCheck (L_78, L_80);
		(L_78)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)26)), (StateHandler_t3489987002 *)L_80);
		StateHandlerU5BU5D_t1374508575* L_81 = L_78;
		IntPtr_t L_82;
		L_82.set_m_value_0((void*)(void*)Lexer_State28_m1097990071_MethodInfo_var);
		StateHandler_t3489987002 * L_83 = (StateHandler_t3489987002 *)il2cpp_codegen_object_new(StateHandler_t3489987002_il2cpp_TypeInfo_var);
		StateHandler__ctor_m1057417747(L_83, NULL, L_82, /*hidden argument*/NULL);
		NullCheck(L_81);
		ArrayElementTypeCheck (L_81, L_83);
		(L_81)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)27)), (StateHandler_t3489987002 *)L_83);
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t954714164_il2cpp_TypeInfo_var);
		((Lexer_t954714164_StaticFields*)Lexer_t954714164_il2cpp_TypeInfo_var->static_fields)->set_fsm_handler_table_1(L_81);
		Int32U5BU5D_t3030399641* L_84 = ((Int32U5BU5D_t3030399641*)SZArrayNew(Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var, (uint32_t)((int32_t)28)));
		RuntimeHelpers_InitializeArray_m3920580167(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_84, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t1486305143____50B1635D1FB2907A171B71751E1A3FA79423CA17_0_FieldInfo_var), /*hidden argument*/NULL);
		((Lexer_t954714164_StaticFields*)Lexer_t954714164_il2cpp_TypeInfo_var->static_fields)->set_fsm_return_table_0(L_84);
		return;
	}
}
// System.Char ThirdParty.Json.LitJson.Lexer::ProcessEscChar(System.Int32)
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern const uint32_t Lexer_ProcessEscChar_m530821430_MetadataUsageId;
extern "C"  Il2CppChar Lexer_ProcessEscChar_m530821430 (Il2CppObject * __this /* static, unused */, int32_t ___esc_char0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Lexer_ProcessEscChar_m530821430_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___esc_char0;
		if ((((int32_t)L_0) > ((int32_t)((int32_t)92))))
		{
			goto IL_0022;
		}
	}
	{
		int32_t L_1 = ___esc_char0;
		if ((((int32_t)L_1) > ((int32_t)((int32_t)39))))
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_2 = ___esc_char0;
		if ((((int32_t)L_2) == ((int32_t)((int32_t)34))))
		{
			goto IL_0044;
		}
	}
	{
		int32_t L_3 = ___esc_char0;
		if ((((int32_t)L_3) == ((int32_t)((int32_t)39))))
		{
			goto IL_0044;
		}
	}
	{
		goto IL_0059;
	}

IL_0016:
	{
		int32_t L_4 = ___esc_char0;
		if ((((int32_t)L_4) == ((int32_t)((int32_t)47))))
		{
			goto IL_0044;
		}
	}
	{
		int32_t L_5 = ___esc_char0;
		if ((((int32_t)L_5) == ((int32_t)((int32_t)92))))
		{
			goto IL_0044;
		}
	}
	{
		goto IL_0059;
	}

IL_0022:
	{
		int32_t L_6 = ___esc_char0;
		if ((((int32_t)L_6) > ((int32_t)((int32_t)102))))
		{
			goto IL_0033;
		}
	}
	{
		int32_t L_7 = ___esc_char0;
		if ((((int32_t)L_7) == ((int32_t)((int32_t)98))))
		{
			goto IL_0054;
		}
	}
	{
		int32_t L_8 = ___esc_char0;
		if ((((int32_t)L_8) == ((int32_t)((int32_t)102))))
		{
			goto IL_0056;
		}
	}
	{
		goto IL_0059;
	}

IL_0033:
	{
		int32_t L_9 = ___esc_char0;
		if ((((int32_t)L_9) == ((int32_t)((int32_t)110))))
		{
			goto IL_004b;
		}
	}
	{
		int32_t L_10 = ___esc_char0;
		if ((((int32_t)L_10) == ((int32_t)((int32_t)114))))
		{
			goto IL_0051;
		}
	}
	{
		int32_t L_11 = ___esc_char0;
		if ((((int32_t)L_11) == ((int32_t)((int32_t)116))))
		{
			goto IL_004e;
		}
	}
	{
		goto IL_0059;
	}

IL_0044:
	{
		int32_t L_12 = ___esc_char0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		Il2CppChar L_13 = Convert_ToChar_m3827339132(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		return L_13;
	}

IL_004b:
	{
		return ((int32_t)10);
	}

IL_004e:
	{
		return ((int32_t)9);
	}

IL_0051:
	{
		return ((int32_t)13);
	}

IL_0054:
	{
		return 8;
	}

IL_0056:
	{
		return ((int32_t)12);
	}

IL_0059:
	{
		return ((int32_t)63);
	}
}
// System.Boolean ThirdParty.Json.LitJson.Lexer::State1(ThirdParty.Json.LitJson.FsmContext)
extern "C"  bool Lexer_State1_m2106225368 (Il2CppObject * __this /* static, unused */, FsmContext_t4275593467 * ___ctx0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		goto IL_01d9;
	}

IL_0005:
	{
		FsmContext_t4275593467 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t954714164 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		int32_t L_2 = L_1->get_input_char_7();
		if ((((int32_t)L_2) == ((int32_t)((int32_t)32))))
		{
			goto IL_01d9;
		}
	}
	{
		FsmContext_t4275593467 * L_3 = ___ctx0;
		NullCheck(L_3);
		Lexer_t954714164 * L_4 = L_3->get_L_2();
		NullCheck(L_4);
		int32_t L_5 = L_4->get_input_char_7();
		if ((((int32_t)L_5) < ((int32_t)((int32_t)9))))
		{
			goto IL_0038;
		}
	}
	{
		FsmContext_t4275593467 * L_6 = ___ctx0;
		NullCheck(L_6);
		Lexer_t954714164 * L_7 = L_6->get_L_2();
		NullCheck(L_7);
		int32_t L_8 = L_7->get_input_char_7();
		if ((((int32_t)L_8) <= ((int32_t)((int32_t)13))))
		{
			goto IL_01d9;
		}
	}

IL_0038:
	{
		FsmContext_t4275593467 * L_9 = ___ctx0;
		NullCheck(L_9);
		Lexer_t954714164 * L_10 = L_9->get_L_2();
		NullCheck(L_10);
		int32_t L_11 = L_10->get_input_char_7();
		if ((((int32_t)L_11) < ((int32_t)((int32_t)49))))
		{
			goto IL_007c;
		}
	}
	{
		FsmContext_t4275593467 * L_12 = ___ctx0;
		NullCheck(L_12);
		Lexer_t954714164 * L_13 = L_12->get_L_2();
		NullCheck(L_13);
		int32_t L_14 = L_13->get_input_char_7();
		if ((((int32_t)L_14) > ((int32_t)((int32_t)57))))
		{
			goto IL_007c;
		}
	}
	{
		FsmContext_t4275593467 * L_15 = ___ctx0;
		NullCheck(L_15);
		Lexer_t954714164 * L_16 = L_15->get_L_2();
		NullCheck(L_16);
		StringBuilder_t1221177846 * L_17 = L_16->get_string_buffer_10();
		FsmContext_t4275593467 * L_18 = ___ctx0;
		NullCheck(L_18);
		Lexer_t954714164 * L_19 = L_18->get_L_2();
		NullCheck(L_19);
		int32_t L_20 = L_19->get_input_char_7();
		NullCheck(L_17);
		StringBuilder_Append_m3618697540(L_17, (((int32_t)((uint16_t)L_20))), /*hidden argument*/NULL);
		FsmContext_t4275593467 * L_21 = ___ctx0;
		NullCheck(L_21);
		L_21->set_NextState_1(3);
		return (bool)1;
	}

IL_007c:
	{
		FsmContext_t4275593467 * L_22 = ___ctx0;
		NullCheck(L_22);
		Lexer_t954714164 * L_23 = L_22->get_L_2();
		NullCheck(L_23);
		int32_t L_24 = L_23->get_input_char_7();
		V_0 = L_24;
		int32_t L_25 = V_0;
		if ((((int32_t)L_25) > ((int32_t)((int32_t)91))))
		{
			goto IL_00d0;
		}
	}
	{
		int32_t L_26 = V_0;
		if ((((int32_t)L_26) > ((int32_t)((int32_t)39))))
		{
			goto IL_00a4;
		}
	}
	{
		int32_t L_27 = V_0;
		if ((((int32_t)L_27) == ((int32_t)((int32_t)34))))
		{
			goto IL_0106;
		}
	}
	{
		int32_t L_28 = V_0;
		if ((((int32_t)L_28) == ((int32_t)((int32_t)39))))
		{
			goto IL_0191;
		}
	}
	{
		goto IL_01d7;
	}

IL_00a4:
	{
		int32_t L_29 = V_0;
		if (((int32_t)((int32_t)L_29-(int32_t)((int32_t)44))) == 0)
		{
			goto IL_0117;
		}
		if (((int32_t)((int32_t)L_29-(int32_t)((int32_t)44))) == 1)
		{
			goto IL_0127;
		}
		if (((int32_t)((int32_t)L_29-(int32_t)((int32_t)44))) == 2)
		{
			goto IL_01d7;
		}
		if (((int32_t)((int32_t)L_29-(int32_t)((int32_t)44))) == 3)
		{
			goto IL_01be;
		}
		if (((int32_t)((int32_t)L_29-(int32_t)((int32_t)44))) == 4)
		{
			goto IL_014d;
		}
	}
	{
		int32_t L_30 = V_0;
		if ((((int32_t)L_30) == ((int32_t)((int32_t)58))))
		{
			goto IL_0117;
		}
	}
	{
		int32_t L_31 = V_0;
		if ((((int32_t)L_31) == ((int32_t)((int32_t)91))))
		{
			goto IL_0117;
		}
	}
	{
		goto IL_01d7;
	}

IL_00d0:
	{
		int32_t L_32 = V_0;
		if ((((int32_t)L_32) > ((int32_t)((int32_t)110))))
		{
			goto IL_00ef;
		}
	}
	{
		int32_t L_33 = V_0;
		if ((((int32_t)L_33) == ((int32_t)((int32_t)93))))
		{
			goto IL_0117;
		}
	}
	{
		int32_t L_34 = V_0;
		if ((((int32_t)L_34) == ((int32_t)((int32_t)102))))
		{
			goto IL_0173;
		}
	}
	{
		int32_t L_35 = V_0;
		if ((((int32_t)L_35) == ((int32_t)((int32_t)110))))
		{
			goto IL_017d;
		}
	}
	{
		goto IL_01d7;
	}

IL_00ef:
	{
		int32_t L_36 = V_0;
		if ((((int32_t)L_36) == ((int32_t)((int32_t)116))))
		{
			goto IL_0187;
		}
	}
	{
		int32_t L_37 = V_0;
		if ((((int32_t)L_37) == ((int32_t)((int32_t)123))))
		{
			goto IL_0117;
		}
	}
	{
		int32_t L_38 = V_0;
		if ((((int32_t)L_38) == ((int32_t)((int32_t)125))))
		{
			goto IL_0117;
		}
	}
	{
		goto IL_01d7;
	}

IL_0106:
	{
		FsmContext_t4275593467 * L_39 = ___ctx0;
		NullCheck(L_39);
		L_39->set_NextState_1(((int32_t)19));
		FsmContext_t4275593467 * L_40 = ___ctx0;
		NullCheck(L_40);
		L_40->set_Return_0((bool)1);
		return (bool)1;
	}

IL_0117:
	{
		FsmContext_t4275593467 * L_41 = ___ctx0;
		NullCheck(L_41);
		L_41->set_NextState_1(1);
		FsmContext_t4275593467 * L_42 = ___ctx0;
		NullCheck(L_42);
		L_42->set_Return_0((bool)1);
		return (bool)1;
	}

IL_0127:
	{
		FsmContext_t4275593467 * L_43 = ___ctx0;
		NullCheck(L_43);
		Lexer_t954714164 * L_44 = L_43->get_L_2();
		NullCheck(L_44);
		StringBuilder_t1221177846 * L_45 = L_44->get_string_buffer_10();
		FsmContext_t4275593467 * L_46 = ___ctx0;
		NullCheck(L_46);
		Lexer_t954714164 * L_47 = L_46->get_L_2();
		NullCheck(L_47);
		int32_t L_48 = L_47->get_input_char_7();
		NullCheck(L_45);
		StringBuilder_Append_m3618697540(L_45, (((int32_t)((uint16_t)L_48))), /*hidden argument*/NULL);
		FsmContext_t4275593467 * L_49 = ___ctx0;
		NullCheck(L_49);
		L_49->set_NextState_1(2);
		return (bool)1;
	}

IL_014d:
	{
		FsmContext_t4275593467 * L_50 = ___ctx0;
		NullCheck(L_50);
		Lexer_t954714164 * L_51 = L_50->get_L_2();
		NullCheck(L_51);
		StringBuilder_t1221177846 * L_52 = L_51->get_string_buffer_10();
		FsmContext_t4275593467 * L_53 = ___ctx0;
		NullCheck(L_53);
		Lexer_t954714164 * L_54 = L_53->get_L_2();
		NullCheck(L_54);
		int32_t L_55 = L_54->get_input_char_7();
		NullCheck(L_52);
		StringBuilder_Append_m3618697540(L_52, (((int32_t)((uint16_t)L_55))), /*hidden argument*/NULL);
		FsmContext_t4275593467 * L_56 = ___ctx0;
		NullCheck(L_56);
		L_56->set_NextState_1(4);
		return (bool)1;
	}

IL_0173:
	{
		FsmContext_t4275593467 * L_57 = ___ctx0;
		NullCheck(L_57);
		L_57->set_NextState_1(((int32_t)12));
		return (bool)1;
	}

IL_017d:
	{
		FsmContext_t4275593467 * L_58 = ___ctx0;
		NullCheck(L_58);
		L_58->set_NextState_1(((int32_t)16));
		return (bool)1;
	}

IL_0187:
	{
		FsmContext_t4275593467 * L_59 = ___ctx0;
		NullCheck(L_59);
		L_59->set_NextState_1(((int32_t)9));
		return (bool)1;
	}

IL_0191:
	{
		FsmContext_t4275593467 * L_60 = ___ctx0;
		NullCheck(L_60);
		Lexer_t954714164 * L_61 = L_60->get_L_2();
		NullCheck(L_61);
		bool L_62 = L_61->get_allow_single_quoted_strings_3();
		if (L_62)
		{
			goto IL_01a0;
		}
	}
	{
		return (bool)0;
	}

IL_01a0:
	{
		FsmContext_t4275593467 * L_63 = ___ctx0;
		NullCheck(L_63);
		Lexer_t954714164 * L_64 = L_63->get_L_2();
		NullCheck(L_64);
		L_64->set_input_char_7(((int32_t)34));
		FsmContext_t4275593467 * L_65 = ___ctx0;
		NullCheck(L_65);
		L_65->set_NextState_1(((int32_t)23));
		FsmContext_t4275593467 * L_66 = ___ctx0;
		NullCheck(L_66);
		L_66->set_Return_0((bool)1);
		return (bool)1;
	}

IL_01be:
	{
		FsmContext_t4275593467 * L_67 = ___ctx0;
		NullCheck(L_67);
		Lexer_t954714164 * L_68 = L_67->get_L_2();
		NullCheck(L_68);
		bool L_69 = L_68->get_allow_comments_2();
		if (L_69)
		{
			goto IL_01cd;
		}
	}
	{
		return (bool)0;
	}

IL_01cd:
	{
		FsmContext_t4275593467 * L_70 = ___ctx0;
		NullCheck(L_70);
		L_70->set_NextState_1(((int32_t)25));
		return (bool)1;
	}

IL_01d7:
	{
		return (bool)0;
	}

IL_01d9:
	{
		FsmContext_t4275593467 * L_71 = ___ctx0;
		NullCheck(L_71);
		Lexer_t954714164 * L_72 = L_71->get_L_2();
		NullCheck(L_72);
		bool L_73 = Lexer_GetChar_m3775205041(L_72, /*hidden argument*/NULL);
		if (L_73)
		{
			goto IL_0005;
		}
	}
	{
		return (bool)1;
	}
}
// System.Boolean ThirdParty.Json.LitJson.Lexer::State2(ThirdParty.Json.LitJson.FsmContext)
extern "C"  bool Lexer_State2_m934338055 (Il2CppObject * __this /* static, unused */, FsmContext_t4275593467 * ___ctx0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		FsmContext_t4275593467 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t954714164 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		Lexer_GetChar_m3775205041(L_1, /*hidden argument*/NULL);
		FsmContext_t4275593467 * L_2 = ___ctx0;
		NullCheck(L_2);
		Lexer_t954714164 * L_3 = L_2->get_L_2();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_input_char_7();
		if ((((int32_t)L_4) < ((int32_t)((int32_t)49))))
		{
			goto IL_0050;
		}
	}
	{
		FsmContext_t4275593467 * L_5 = ___ctx0;
		NullCheck(L_5);
		Lexer_t954714164 * L_6 = L_5->get_L_2();
		NullCheck(L_6);
		int32_t L_7 = L_6->get_input_char_7();
		if ((((int32_t)L_7) > ((int32_t)((int32_t)57))))
		{
			goto IL_0050;
		}
	}
	{
		FsmContext_t4275593467 * L_8 = ___ctx0;
		NullCheck(L_8);
		Lexer_t954714164 * L_9 = L_8->get_L_2();
		NullCheck(L_9);
		StringBuilder_t1221177846 * L_10 = L_9->get_string_buffer_10();
		FsmContext_t4275593467 * L_11 = ___ctx0;
		NullCheck(L_11);
		Lexer_t954714164 * L_12 = L_11->get_L_2();
		NullCheck(L_12);
		int32_t L_13 = L_12->get_input_char_7();
		NullCheck(L_10);
		StringBuilder_Append_m3618697540(L_10, (((int32_t)((uint16_t)L_13))), /*hidden argument*/NULL);
		FsmContext_t4275593467 * L_14 = ___ctx0;
		NullCheck(L_14);
		L_14->set_NextState_1(3);
		return (bool)1;
	}

IL_0050:
	{
		FsmContext_t4275593467 * L_15 = ___ctx0;
		NullCheck(L_15);
		Lexer_t954714164 * L_16 = L_15->get_L_2();
		NullCheck(L_16);
		int32_t L_17 = L_16->get_input_char_7();
		V_0 = L_17;
		int32_t L_18 = V_0;
		if ((!(((uint32_t)L_18) == ((uint32_t)((int32_t)48)))))
		{
			goto IL_0087;
		}
	}
	{
		FsmContext_t4275593467 * L_19 = ___ctx0;
		NullCheck(L_19);
		Lexer_t954714164 * L_20 = L_19->get_L_2();
		NullCheck(L_20);
		StringBuilder_t1221177846 * L_21 = L_20->get_string_buffer_10();
		FsmContext_t4275593467 * L_22 = ___ctx0;
		NullCheck(L_22);
		Lexer_t954714164 * L_23 = L_22->get_L_2();
		NullCheck(L_23);
		int32_t L_24 = L_23->get_input_char_7();
		NullCheck(L_21);
		StringBuilder_Append_m3618697540(L_21, (((int32_t)((uint16_t)L_24))), /*hidden argument*/NULL);
		FsmContext_t4275593467 * L_25 = ___ctx0;
		NullCheck(L_25);
		L_25->set_NextState_1(4);
		return (bool)1;
	}

IL_0087:
	{
		return (bool)0;
	}
}
// System.Boolean ThirdParty.Json.LitJson.Lexer::State3(ThirdParty.Json.LitJson.FsmContext)
extern "C"  bool Lexer_State3_m3343688098 (Il2CppObject * __this /* static, unused */, FsmContext_t4275593467 * ___ctx0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		goto IL_011c;
	}

IL_0005:
	{
		FsmContext_t4275593467 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t954714164 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		int32_t L_2 = L_1->get_input_char_7();
		if ((((int32_t)L_2) < ((int32_t)((int32_t)48))))
		{
			goto IL_0045;
		}
	}
	{
		FsmContext_t4275593467 * L_3 = ___ctx0;
		NullCheck(L_3);
		Lexer_t954714164 * L_4 = L_3->get_L_2();
		NullCheck(L_4);
		int32_t L_5 = L_4->get_input_char_7();
		if ((((int32_t)L_5) > ((int32_t)((int32_t)57))))
		{
			goto IL_0045;
		}
	}
	{
		FsmContext_t4275593467 * L_6 = ___ctx0;
		NullCheck(L_6);
		Lexer_t954714164 * L_7 = L_6->get_L_2();
		NullCheck(L_7);
		StringBuilder_t1221177846 * L_8 = L_7->get_string_buffer_10();
		FsmContext_t4275593467 * L_9 = ___ctx0;
		NullCheck(L_9);
		Lexer_t954714164 * L_10 = L_9->get_L_2();
		NullCheck(L_10);
		int32_t L_11 = L_10->get_input_char_7();
		NullCheck(L_8);
		StringBuilder_Append_m3618697540(L_8, (((int32_t)((uint16_t)L_11))), /*hidden argument*/NULL);
		goto IL_011c;
	}

IL_0045:
	{
		FsmContext_t4275593467 * L_12 = ___ctx0;
		NullCheck(L_12);
		Lexer_t954714164 * L_13 = L_12->get_L_2();
		NullCheck(L_13);
		int32_t L_14 = L_13->get_input_char_7();
		if ((((int32_t)L_14) == ((int32_t)((int32_t)32))))
		{
			goto IL_0072;
		}
	}
	{
		FsmContext_t4275593467 * L_15 = ___ctx0;
		NullCheck(L_15);
		Lexer_t954714164 * L_16 = L_15->get_L_2();
		NullCheck(L_16);
		int32_t L_17 = L_16->get_input_char_7();
		if ((((int32_t)L_17) < ((int32_t)((int32_t)9))))
		{
			goto IL_0082;
		}
	}
	{
		FsmContext_t4275593467 * L_18 = ___ctx0;
		NullCheck(L_18);
		Lexer_t954714164 * L_19 = L_18->get_L_2();
		NullCheck(L_19);
		int32_t L_20 = L_19->get_input_char_7();
		if ((((int32_t)L_20) > ((int32_t)((int32_t)13))))
		{
			goto IL_0082;
		}
	}

IL_0072:
	{
		FsmContext_t4275593467 * L_21 = ___ctx0;
		NullCheck(L_21);
		L_21->set_Return_0((bool)1);
		FsmContext_t4275593467 * L_22 = ___ctx0;
		NullCheck(L_22);
		L_22->set_NextState_1(1);
		return (bool)1;
	}

IL_0082:
	{
		FsmContext_t4275593467 * L_23 = ___ctx0;
		NullCheck(L_23);
		Lexer_t954714164 * L_24 = L_23->get_L_2();
		NullCheck(L_24);
		int32_t L_25 = L_24->get_input_char_7();
		V_0 = L_25;
		int32_t L_26 = V_0;
		if ((((int32_t)L_26) > ((int32_t)((int32_t)69))))
		{
			goto IL_00a4;
		}
	}
	{
		int32_t L_27 = V_0;
		if ((((int32_t)L_27) == ((int32_t)((int32_t)44))))
		{
			goto IL_00b3;
		}
	}
	{
		int32_t L_28 = V_0;
		if ((((int32_t)L_28) == ((int32_t)((int32_t)46))))
		{
			goto IL_00ce;
		}
	}
	{
		int32_t L_29 = V_0;
		if ((((int32_t)L_29) == ((int32_t)((int32_t)69))))
		{
			goto IL_00f4;
		}
	}
	{
		goto IL_011a;
	}

IL_00a4:
	{
		int32_t L_30 = V_0;
		if ((((int32_t)L_30) == ((int32_t)((int32_t)93))))
		{
			goto IL_00b3;
		}
	}
	{
		int32_t L_31 = V_0;
		if ((((int32_t)L_31) == ((int32_t)((int32_t)101))))
		{
			goto IL_00f4;
		}
	}
	{
		int32_t L_32 = V_0;
		if ((!(((uint32_t)L_32) == ((uint32_t)((int32_t)125)))))
		{
			goto IL_011a;
		}
	}

IL_00b3:
	{
		FsmContext_t4275593467 * L_33 = ___ctx0;
		NullCheck(L_33);
		Lexer_t954714164 * L_34 = L_33->get_L_2();
		NullCheck(L_34);
		Lexer_UngetChar_m1417570926(L_34, /*hidden argument*/NULL);
		FsmContext_t4275593467 * L_35 = ___ctx0;
		NullCheck(L_35);
		L_35->set_Return_0((bool)1);
		FsmContext_t4275593467 * L_36 = ___ctx0;
		NullCheck(L_36);
		L_36->set_NextState_1(1);
		return (bool)1;
	}

IL_00ce:
	{
		FsmContext_t4275593467 * L_37 = ___ctx0;
		NullCheck(L_37);
		Lexer_t954714164 * L_38 = L_37->get_L_2();
		NullCheck(L_38);
		StringBuilder_t1221177846 * L_39 = L_38->get_string_buffer_10();
		FsmContext_t4275593467 * L_40 = ___ctx0;
		NullCheck(L_40);
		Lexer_t954714164 * L_41 = L_40->get_L_2();
		NullCheck(L_41);
		int32_t L_42 = L_41->get_input_char_7();
		NullCheck(L_39);
		StringBuilder_Append_m3618697540(L_39, (((int32_t)((uint16_t)L_42))), /*hidden argument*/NULL);
		FsmContext_t4275593467 * L_43 = ___ctx0;
		NullCheck(L_43);
		L_43->set_NextState_1(5);
		return (bool)1;
	}

IL_00f4:
	{
		FsmContext_t4275593467 * L_44 = ___ctx0;
		NullCheck(L_44);
		Lexer_t954714164 * L_45 = L_44->get_L_2();
		NullCheck(L_45);
		StringBuilder_t1221177846 * L_46 = L_45->get_string_buffer_10();
		FsmContext_t4275593467 * L_47 = ___ctx0;
		NullCheck(L_47);
		Lexer_t954714164 * L_48 = L_47->get_L_2();
		NullCheck(L_48);
		int32_t L_49 = L_48->get_input_char_7();
		NullCheck(L_46);
		StringBuilder_Append_m3618697540(L_46, (((int32_t)((uint16_t)L_49))), /*hidden argument*/NULL);
		FsmContext_t4275593467 * L_50 = ___ctx0;
		NullCheck(L_50);
		L_50->set_NextState_1(7);
		return (bool)1;
	}

IL_011a:
	{
		return (bool)0;
	}

IL_011c:
	{
		FsmContext_t4275593467 * L_51 = ___ctx0;
		NullCheck(L_51);
		Lexer_t954714164 * L_52 = L_51->get_L_2();
		NullCheck(L_52);
		bool L_53 = Lexer_GetChar_m3775205041(L_52, /*hidden argument*/NULL);
		if (L_53)
		{
			goto IL_0005;
		}
	}
	{
		return (bool)1;
	}
}
// System.Boolean ThirdParty.Json.LitJson.Lexer::State4(ThirdParty.Json.LitJson.FsmContext)
extern "C"  bool Lexer_State4_m271821993 (Il2CppObject * __this /* static, unused */, FsmContext_t4275593467 * ___ctx0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		FsmContext_t4275593467 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t954714164 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		Lexer_GetChar_m3775205041(L_1, /*hidden argument*/NULL);
		FsmContext_t4275593467 * L_2 = ___ctx0;
		NullCheck(L_2);
		Lexer_t954714164 * L_3 = L_2->get_L_2();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_input_char_7();
		if ((((int32_t)L_4) == ((int32_t)((int32_t)32))))
		{
			goto IL_0039;
		}
	}
	{
		FsmContext_t4275593467 * L_5 = ___ctx0;
		NullCheck(L_5);
		Lexer_t954714164 * L_6 = L_5->get_L_2();
		NullCheck(L_6);
		int32_t L_7 = L_6->get_input_char_7();
		if ((((int32_t)L_7) < ((int32_t)((int32_t)9))))
		{
			goto IL_0049;
		}
	}
	{
		FsmContext_t4275593467 * L_8 = ___ctx0;
		NullCheck(L_8);
		Lexer_t954714164 * L_9 = L_8->get_L_2();
		NullCheck(L_9);
		int32_t L_10 = L_9->get_input_char_7();
		if ((((int32_t)L_10) > ((int32_t)((int32_t)13))))
		{
			goto IL_0049;
		}
	}

IL_0039:
	{
		FsmContext_t4275593467 * L_11 = ___ctx0;
		NullCheck(L_11);
		L_11->set_Return_0((bool)1);
		FsmContext_t4275593467 * L_12 = ___ctx0;
		NullCheck(L_12);
		L_12->set_NextState_1(1);
		return (bool)1;
	}

IL_0049:
	{
		FsmContext_t4275593467 * L_13 = ___ctx0;
		NullCheck(L_13);
		Lexer_t954714164 * L_14 = L_13->get_L_2();
		NullCheck(L_14);
		int32_t L_15 = L_14->get_input_char_7();
		V_0 = L_15;
		int32_t L_16 = V_0;
		if ((((int32_t)L_16) > ((int32_t)((int32_t)69))))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_17 = V_0;
		if ((((int32_t)L_17) == ((int32_t)((int32_t)44))))
		{
			goto IL_007a;
		}
	}
	{
		int32_t L_18 = V_0;
		if ((((int32_t)L_18) == ((int32_t)((int32_t)46))))
		{
			goto IL_0095;
		}
	}
	{
		int32_t L_19 = V_0;
		if ((((int32_t)L_19) == ((int32_t)((int32_t)69))))
		{
			goto IL_00bb;
		}
	}
	{
		goto IL_00e1;
	}

IL_006b:
	{
		int32_t L_20 = V_0;
		if ((((int32_t)L_20) == ((int32_t)((int32_t)93))))
		{
			goto IL_007a;
		}
	}
	{
		int32_t L_21 = V_0;
		if ((((int32_t)L_21) == ((int32_t)((int32_t)101))))
		{
			goto IL_00bb;
		}
	}
	{
		int32_t L_22 = V_0;
		if ((!(((uint32_t)L_22) == ((uint32_t)((int32_t)125)))))
		{
			goto IL_00e1;
		}
	}

IL_007a:
	{
		FsmContext_t4275593467 * L_23 = ___ctx0;
		NullCheck(L_23);
		Lexer_t954714164 * L_24 = L_23->get_L_2();
		NullCheck(L_24);
		Lexer_UngetChar_m1417570926(L_24, /*hidden argument*/NULL);
		FsmContext_t4275593467 * L_25 = ___ctx0;
		NullCheck(L_25);
		L_25->set_Return_0((bool)1);
		FsmContext_t4275593467 * L_26 = ___ctx0;
		NullCheck(L_26);
		L_26->set_NextState_1(1);
		return (bool)1;
	}

IL_0095:
	{
		FsmContext_t4275593467 * L_27 = ___ctx0;
		NullCheck(L_27);
		Lexer_t954714164 * L_28 = L_27->get_L_2();
		NullCheck(L_28);
		StringBuilder_t1221177846 * L_29 = L_28->get_string_buffer_10();
		FsmContext_t4275593467 * L_30 = ___ctx0;
		NullCheck(L_30);
		Lexer_t954714164 * L_31 = L_30->get_L_2();
		NullCheck(L_31);
		int32_t L_32 = L_31->get_input_char_7();
		NullCheck(L_29);
		StringBuilder_Append_m3618697540(L_29, (((int32_t)((uint16_t)L_32))), /*hidden argument*/NULL);
		FsmContext_t4275593467 * L_33 = ___ctx0;
		NullCheck(L_33);
		L_33->set_NextState_1(5);
		return (bool)1;
	}

IL_00bb:
	{
		FsmContext_t4275593467 * L_34 = ___ctx0;
		NullCheck(L_34);
		Lexer_t954714164 * L_35 = L_34->get_L_2();
		NullCheck(L_35);
		StringBuilder_t1221177846 * L_36 = L_35->get_string_buffer_10();
		FsmContext_t4275593467 * L_37 = ___ctx0;
		NullCheck(L_37);
		Lexer_t954714164 * L_38 = L_37->get_L_2();
		NullCheck(L_38);
		int32_t L_39 = L_38->get_input_char_7();
		NullCheck(L_36);
		StringBuilder_Append_m3618697540(L_36, (((int32_t)((uint16_t)L_39))), /*hidden argument*/NULL);
		FsmContext_t4275593467 * L_40 = ___ctx0;
		NullCheck(L_40);
		L_40->set_NextState_1(7);
		return (bool)1;
	}

IL_00e1:
	{
		return (bool)0;
	}
}
// System.Boolean ThirdParty.Json.LitJson.Lexer::State5(ThirdParty.Json.LitJson.FsmContext)
extern "C"  bool Lexer_State5_m2681172036 (Il2CppObject * __this /* static, unused */, FsmContext_t4275593467 * ___ctx0, const MethodInfo* method)
{
	{
		FsmContext_t4275593467 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t954714164 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		Lexer_GetChar_m3775205041(L_1, /*hidden argument*/NULL);
		FsmContext_t4275593467 * L_2 = ___ctx0;
		NullCheck(L_2);
		Lexer_t954714164 * L_3 = L_2->get_L_2();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_input_char_7();
		if ((((int32_t)L_4) < ((int32_t)((int32_t)48))))
		{
			goto IL_0050;
		}
	}
	{
		FsmContext_t4275593467 * L_5 = ___ctx0;
		NullCheck(L_5);
		Lexer_t954714164 * L_6 = L_5->get_L_2();
		NullCheck(L_6);
		int32_t L_7 = L_6->get_input_char_7();
		if ((((int32_t)L_7) > ((int32_t)((int32_t)57))))
		{
			goto IL_0050;
		}
	}
	{
		FsmContext_t4275593467 * L_8 = ___ctx0;
		NullCheck(L_8);
		Lexer_t954714164 * L_9 = L_8->get_L_2();
		NullCheck(L_9);
		StringBuilder_t1221177846 * L_10 = L_9->get_string_buffer_10();
		FsmContext_t4275593467 * L_11 = ___ctx0;
		NullCheck(L_11);
		Lexer_t954714164 * L_12 = L_11->get_L_2();
		NullCheck(L_12);
		int32_t L_13 = L_12->get_input_char_7();
		NullCheck(L_10);
		StringBuilder_Append_m3618697540(L_10, (((int32_t)((uint16_t)L_13))), /*hidden argument*/NULL);
		FsmContext_t4275593467 * L_14 = ___ctx0;
		NullCheck(L_14);
		L_14->set_NextState_1(6);
		return (bool)1;
	}

IL_0050:
	{
		return (bool)0;
	}
}
// System.Boolean ThirdParty.Json.LitJson.Lexer::State6(ThirdParty.Json.LitJson.FsmContext)
extern "C"  bool Lexer_State6_m1509284723 (Il2CppObject * __this /* static, unused */, FsmContext_t4275593467 * ___ctx0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		goto IL_00f1;
	}

IL_0005:
	{
		FsmContext_t4275593467 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t954714164 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		int32_t L_2 = L_1->get_input_char_7();
		if ((((int32_t)L_2) < ((int32_t)((int32_t)48))))
		{
			goto IL_0045;
		}
	}
	{
		FsmContext_t4275593467 * L_3 = ___ctx0;
		NullCheck(L_3);
		Lexer_t954714164 * L_4 = L_3->get_L_2();
		NullCheck(L_4);
		int32_t L_5 = L_4->get_input_char_7();
		if ((((int32_t)L_5) > ((int32_t)((int32_t)57))))
		{
			goto IL_0045;
		}
	}
	{
		FsmContext_t4275593467 * L_6 = ___ctx0;
		NullCheck(L_6);
		Lexer_t954714164 * L_7 = L_6->get_L_2();
		NullCheck(L_7);
		StringBuilder_t1221177846 * L_8 = L_7->get_string_buffer_10();
		FsmContext_t4275593467 * L_9 = ___ctx0;
		NullCheck(L_9);
		Lexer_t954714164 * L_10 = L_9->get_L_2();
		NullCheck(L_10);
		int32_t L_11 = L_10->get_input_char_7();
		NullCheck(L_8);
		StringBuilder_Append_m3618697540(L_8, (((int32_t)((uint16_t)L_11))), /*hidden argument*/NULL);
		goto IL_00f1;
	}

IL_0045:
	{
		FsmContext_t4275593467 * L_12 = ___ctx0;
		NullCheck(L_12);
		Lexer_t954714164 * L_13 = L_12->get_L_2();
		NullCheck(L_13);
		int32_t L_14 = L_13->get_input_char_7();
		if ((((int32_t)L_14) == ((int32_t)((int32_t)32))))
		{
			goto IL_0072;
		}
	}
	{
		FsmContext_t4275593467 * L_15 = ___ctx0;
		NullCheck(L_15);
		Lexer_t954714164 * L_16 = L_15->get_L_2();
		NullCheck(L_16);
		int32_t L_17 = L_16->get_input_char_7();
		if ((((int32_t)L_17) < ((int32_t)((int32_t)9))))
		{
			goto IL_0082;
		}
	}
	{
		FsmContext_t4275593467 * L_18 = ___ctx0;
		NullCheck(L_18);
		Lexer_t954714164 * L_19 = L_18->get_L_2();
		NullCheck(L_19);
		int32_t L_20 = L_19->get_input_char_7();
		if ((((int32_t)L_20) > ((int32_t)((int32_t)13))))
		{
			goto IL_0082;
		}
	}

IL_0072:
	{
		FsmContext_t4275593467 * L_21 = ___ctx0;
		NullCheck(L_21);
		L_21->set_Return_0((bool)1);
		FsmContext_t4275593467 * L_22 = ___ctx0;
		NullCheck(L_22);
		L_22->set_NextState_1(1);
		return (bool)1;
	}

IL_0082:
	{
		FsmContext_t4275593467 * L_23 = ___ctx0;
		NullCheck(L_23);
		Lexer_t954714164 * L_24 = L_23->get_L_2();
		NullCheck(L_24);
		int32_t L_25 = L_24->get_input_char_7();
		V_0 = L_25;
		int32_t L_26 = V_0;
		if ((((int32_t)L_26) > ((int32_t)((int32_t)69))))
		{
			goto IL_009f;
		}
	}
	{
		int32_t L_27 = V_0;
		if ((((int32_t)L_27) == ((int32_t)((int32_t)44))))
		{
			goto IL_00ae;
		}
	}
	{
		int32_t L_28 = V_0;
		if ((((int32_t)L_28) == ((int32_t)((int32_t)69))))
		{
			goto IL_00c9;
		}
	}
	{
		goto IL_00ef;
	}

IL_009f:
	{
		int32_t L_29 = V_0;
		if ((((int32_t)L_29) == ((int32_t)((int32_t)93))))
		{
			goto IL_00ae;
		}
	}
	{
		int32_t L_30 = V_0;
		if ((((int32_t)L_30) == ((int32_t)((int32_t)101))))
		{
			goto IL_00c9;
		}
	}
	{
		int32_t L_31 = V_0;
		if ((!(((uint32_t)L_31) == ((uint32_t)((int32_t)125)))))
		{
			goto IL_00ef;
		}
	}

IL_00ae:
	{
		FsmContext_t4275593467 * L_32 = ___ctx0;
		NullCheck(L_32);
		Lexer_t954714164 * L_33 = L_32->get_L_2();
		NullCheck(L_33);
		Lexer_UngetChar_m1417570926(L_33, /*hidden argument*/NULL);
		FsmContext_t4275593467 * L_34 = ___ctx0;
		NullCheck(L_34);
		L_34->set_Return_0((bool)1);
		FsmContext_t4275593467 * L_35 = ___ctx0;
		NullCheck(L_35);
		L_35->set_NextState_1(1);
		return (bool)1;
	}

IL_00c9:
	{
		FsmContext_t4275593467 * L_36 = ___ctx0;
		NullCheck(L_36);
		Lexer_t954714164 * L_37 = L_36->get_L_2();
		NullCheck(L_37);
		StringBuilder_t1221177846 * L_38 = L_37->get_string_buffer_10();
		FsmContext_t4275593467 * L_39 = ___ctx0;
		NullCheck(L_39);
		Lexer_t954714164 * L_40 = L_39->get_L_2();
		NullCheck(L_40);
		int32_t L_41 = L_40->get_input_char_7();
		NullCheck(L_38);
		StringBuilder_Append_m3618697540(L_38, (((int32_t)((uint16_t)L_41))), /*hidden argument*/NULL);
		FsmContext_t4275593467 * L_42 = ___ctx0;
		NullCheck(L_42);
		L_42->set_NextState_1(7);
		return (bool)1;
	}

IL_00ef:
	{
		return (bool)0;
	}

IL_00f1:
	{
		FsmContext_t4275593467 * L_43 = ___ctx0;
		NullCheck(L_43);
		Lexer_t954714164 * L_44 = L_43->get_L_2();
		NullCheck(L_44);
		bool L_45 = Lexer_GetChar_m3775205041(L_44, /*hidden argument*/NULL);
		if (L_45)
		{
			goto IL_0005;
		}
	}
	{
		return (bool)1;
	}
}
// System.Boolean ThirdParty.Json.LitJson.Lexer::State7(ThirdParty.Json.LitJson.FsmContext)
extern "C"  bool Lexer_State7_m3918634766 (Il2CppObject * __this /* static, unused */, FsmContext_t4275593467 * ___ctx0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		FsmContext_t4275593467 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t954714164 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		Lexer_GetChar_m3775205041(L_1, /*hidden argument*/NULL);
		FsmContext_t4275593467 * L_2 = ___ctx0;
		NullCheck(L_2);
		Lexer_t954714164 * L_3 = L_2->get_L_2();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_input_char_7();
		if ((((int32_t)L_4) < ((int32_t)((int32_t)48))))
		{
			goto IL_0050;
		}
	}
	{
		FsmContext_t4275593467 * L_5 = ___ctx0;
		NullCheck(L_5);
		Lexer_t954714164 * L_6 = L_5->get_L_2();
		NullCheck(L_6);
		int32_t L_7 = L_6->get_input_char_7();
		if ((((int32_t)L_7) > ((int32_t)((int32_t)57))))
		{
			goto IL_0050;
		}
	}
	{
		FsmContext_t4275593467 * L_8 = ___ctx0;
		NullCheck(L_8);
		Lexer_t954714164 * L_9 = L_8->get_L_2();
		NullCheck(L_9);
		StringBuilder_t1221177846 * L_10 = L_9->get_string_buffer_10();
		FsmContext_t4275593467 * L_11 = ___ctx0;
		NullCheck(L_11);
		Lexer_t954714164 * L_12 = L_11->get_L_2();
		NullCheck(L_12);
		int32_t L_13 = L_12->get_input_char_7();
		NullCheck(L_10);
		StringBuilder_Append_m3618697540(L_10, (((int32_t)((uint16_t)L_13))), /*hidden argument*/NULL);
		FsmContext_t4275593467 * L_14 = ___ctx0;
		NullCheck(L_14);
		L_14->set_NextState_1(8);
		return (bool)1;
	}

IL_0050:
	{
		FsmContext_t4275593467 * L_15 = ___ctx0;
		NullCheck(L_15);
		Lexer_t954714164 * L_16 = L_15->get_L_2();
		NullCheck(L_16);
		int32_t L_17 = L_16->get_input_char_7();
		V_0 = L_17;
		int32_t L_18 = V_0;
		if ((((int32_t)L_18) == ((int32_t)((int32_t)43))))
		{
			goto IL_0066;
		}
	}
	{
		int32_t L_19 = V_0;
		if ((!(((uint32_t)L_19) == ((uint32_t)((int32_t)45)))))
		{
			goto IL_008c;
		}
	}

IL_0066:
	{
		FsmContext_t4275593467 * L_20 = ___ctx0;
		NullCheck(L_20);
		Lexer_t954714164 * L_21 = L_20->get_L_2();
		NullCheck(L_21);
		StringBuilder_t1221177846 * L_22 = L_21->get_string_buffer_10();
		FsmContext_t4275593467 * L_23 = ___ctx0;
		NullCheck(L_23);
		Lexer_t954714164 * L_24 = L_23->get_L_2();
		NullCheck(L_24);
		int32_t L_25 = L_24->get_input_char_7();
		NullCheck(L_22);
		StringBuilder_Append_m3618697540(L_22, (((int32_t)((uint16_t)L_25))), /*hidden argument*/NULL);
		FsmContext_t4275593467 * L_26 = ___ctx0;
		NullCheck(L_26);
		L_26->set_NextState_1(8);
		return (bool)1;
	}

IL_008c:
	{
		return (bool)0;
	}
}
// System.Boolean ThirdParty.Json.LitJson.Lexer::State8(ThirdParty.Json.LitJson.FsmContext)
extern "C"  bool Lexer_State8_m3318232085 (Il2CppObject * __this /* static, unused */, FsmContext_t4275593467 * ___ctx0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		goto IL_00b7;
	}

IL_0005:
	{
		FsmContext_t4275593467 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t954714164 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		int32_t L_2 = L_1->get_input_char_7();
		if ((((int32_t)L_2) < ((int32_t)((int32_t)48))))
		{
			goto IL_0042;
		}
	}
	{
		FsmContext_t4275593467 * L_3 = ___ctx0;
		NullCheck(L_3);
		Lexer_t954714164 * L_4 = L_3->get_L_2();
		NullCheck(L_4);
		int32_t L_5 = L_4->get_input_char_7();
		if ((((int32_t)L_5) > ((int32_t)((int32_t)57))))
		{
			goto IL_0042;
		}
	}
	{
		FsmContext_t4275593467 * L_6 = ___ctx0;
		NullCheck(L_6);
		Lexer_t954714164 * L_7 = L_6->get_L_2();
		NullCheck(L_7);
		StringBuilder_t1221177846 * L_8 = L_7->get_string_buffer_10();
		FsmContext_t4275593467 * L_9 = ___ctx0;
		NullCheck(L_9);
		Lexer_t954714164 * L_10 = L_9->get_L_2();
		NullCheck(L_10);
		int32_t L_11 = L_10->get_input_char_7();
		NullCheck(L_8);
		StringBuilder_Append_m3618697540(L_8, (((int32_t)((uint16_t)L_11))), /*hidden argument*/NULL);
		goto IL_00b7;
	}

IL_0042:
	{
		FsmContext_t4275593467 * L_12 = ___ctx0;
		NullCheck(L_12);
		Lexer_t954714164 * L_13 = L_12->get_L_2();
		NullCheck(L_13);
		int32_t L_14 = L_13->get_input_char_7();
		if ((((int32_t)L_14) == ((int32_t)((int32_t)32))))
		{
			goto IL_006f;
		}
	}
	{
		FsmContext_t4275593467 * L_15 = ___ctx0;
		NullCheck(L_15);
		Lexer_t954714164 * L_16 = L_15->get_L_2();
		NullCheck(L_16);
		int32_t L_17 = L_16->get_input_char_7();
		if ((((int32_t)L_17) < ((int32_t)((int32_t)9))))
		{
			goto IL_007f;
		}
	}
	{
		FsmContext_t4275593467 * L_18 = ___ctx0;
		NullCheck(L_18);
		Lexer_t954714164 * L_19 = L_18->get_L_2();
		NullCheck(L_19);
		int32_t L_20 = L_19->get_input_char_7();
		if ((((int32_t)L_20) > ((int32_t)((int32_t)13))))
		{
			goto IL_007f;
		}
	}

IL_006f:
	{
		FsmContext_t4275593467 * L_21 = ___ctx0;
		NullCheck(L_21);
		L_21->set_Return_0((bool)1);
		FsmContext_t4275593467 * L_22 = ___ctx0;
		NullCheck(L_22);
		L_22->set_NextState_1(1);
		return (bool)1;
	}

IL_007f:
	{
		FsmContext_t4275593467 * L_23 = ___ctx0;
		NullCheck(L_23);
		Lexer_t954714164 * L_24 = L_23->get_L_2();
		NullCheck(L_24);
		int32_t L_25 = L_24->get_input_char_7();
		V_0 = L_25;
		int32_t L_26 = V_0;
		if ((((int32_t)L_26) == ((int32_t)((int32_t)44))))
		{
			goto IL_009a;
		}
	}
	{
		int32_t L_27 = V_0;
		if ((((int32_t)L_27) == ((int32_t)((int32_t)93))))
		{
			goto IL_009a;
		}
	}
	{
		int32_t L_28 = V_0;
		if ((!(((uint32_t)L_28) == ((uint32_t)((int32_t)125)))))
		{
			goto IL_00b5;
		}
	}

IL_009a:
	{
		FsmContext_t4275593467 * L_29 = ___ctx0;
		NullCheck(L_29);
		Lexer_t954714164 * L_30 = L_29->get_L_2();
		NullCheck(L_30);
		Lexer_UngetChar_m1417570926(L_30, /*hidden argument*/NULL);
		FsmContext_t4275593467 * L_31 = ___ctx0;
		NullCheck(L_31);
		L_31->set_Return_0((bool)1);
		FsmContext_t4275593467 * L_32 = ___ctx0;
		NullCheck(L_32);
		L_32->set_NextState_1(1);
		return (bool)1;
	}

IL_00b5:
	{
		return (bool)0;
	}

IL_00b7:
	{
		FsmContext_t4275593467 * L_33 = ___ctx0;
		NullCheck(L_33);
		Lexer_t954714164 * L_34 = L_33->get_L_2();
		NullCheck(L_34);
		bool L_35 = Lexer_GetChar_m3775205041(L_34, /*hidden argument*/NULL);
		if (L_35)
		{
			goto IL_0005;
		}
	}
	{
		return (bool)1;
	}
}
// System.Boolean ThirdParty.Json.LitJson.Lexer::State9(ThirdParty.Json.LitJson.FsmContext)
extern "C"  bool Lexer_State9_m1432614832 (Il2CppObject * __this /* static, unused */, FsmContext_t4275593467 * ___ctx0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		FsmContext_t4275593467 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t954714164 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		Lexer_GetChar_m3775205041(L_1, /*hidden argument*/NULL);
		FsmContext_t4275593467 * L_2 = ___ctx0;
		NullCheck(L_2);
		Lexer_t954714164 * L_3 = L_2->get_L_2();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_input_char_7();
		V_0 = L_4;
		int32_t L_5 = V_0;
		if ((!(((uint32_t)L_5) == ((uint32_t)((int32_t)114)))))
		{
			goto IL_0027;
		}
	}
	{
		FsmContext_t4275593467 * L_6 = ___ctx0;
		NullCheck(L_6);
		L_6->set_NextState_1(((int32_t)10));
		return (bool)1;
	}

IL_0027:
	{
		return (bool)0;
	}
}
// System.Boolean ThirdParty.Json.LitJson.Lexer::State10(ThirdParty.Json.LitJson.FsmContext)
extern "C"  bool Lexer_State10_m2160325200 (Il2CppObject * __this /* static, unused */, FsmContext_t4275593467 * ___ctx0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		FsmContext_t4275593467 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t954714164 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		Lexer_GetChar_m3775205041(L_1, /*hidden argument*/NULL);
		FsmContext_t4275593467 * L_2 = ___ctx0;
		NullCheck(L_2);
		Lexer_t954714164 * L_3 = L_2->get_L_2();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_input_char_7();
		V_0 = L_4;
		int32_t L_5 = V_0;
		if ((!(((uint32_t)L_5) == ((uint32_t)((int32_t)117)))))
		{
			goto IL_0027;
		}
	}
	{
		FsmContext_t4275593467 * L_6 = ___ctx0;
		NullCheck(L_6);
		L_6->set_NextState_1(((int32_t)11));
		return (bool)1;
	}

IL_0027:
	{
		return (bool)0;
	}
}
// System.Boolean ThirdParty.Json.LitJson.Lexer::State11(ThirdParty.Json.LitJson.FsmContext)
extern "C"  bool Lexer_State11_m621563729 (Il2CppObject * __this /* static, unused */, FsmContext_t4275593467 * ___ctx0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		FsmContext_t4275593467 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t954714164 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		Lexer_GetChar_m3775205041(L_1, /*hidden argument*/NULL);
		FsmContext_t4275593467 * L_2 = ___ctx0;
		NullCheck(L_2);
		Lexer_t954714164 * L_3 = L_2->get_L_2();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_input_char_7();
		V_0 = L_4;
		int32_t L_5 = V_0;
		if ((!(((uint32_t)L_5) == ((uint32_t)((int32_t)101)))))
		{
			goto IL_002d;
		}
	}
	{
		FsmContext_t4275593467 * L_6 = ___ctx0;
		NullCheck(L_6);
		L_6->set_Return_0((bool)1);
		FsmContext_t4275593467 * L_7 = ___ctx0;
		NullCheck(L_7);
		L_7->set_NextState_1(1);
		return (bool)1;
	}

IL_002d:
	{
		return (bool)0;
	}
}
// System.Boolean ThirdParty.Json.LitJson.Lexer::State12(ThirdParty.Json.LitJson.FsmContext)
extern "C"  bool Lexer_State12_m425405010 (Il2CppObject * __this /* static, unused */, FsmContext_t4275593467 * ___ctx0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		FsmContext_t4275593467 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t954714164 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		Lexer_GetChar_m3775205041(L_1, /*hidden argument*/NULL);
		FsmContext_t4275593467 * L_2 = ___ctx0;
		NullCheck(L_2);
		Lexer_t954714164 * L_3 = L_2->get_L_2();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_input_char_7();
		V_0 = L_4;
		int32_t L_5 = V_0;
		if ((!(((uint32_t)L_5) == ((uint32_t)((int32_t)97)))))
		{
			goto IL_0027;
		}
	}
	{
		FsmContext_t4275593467 * L_6 = ___ctx0;
		NullCheck(L_6);
		L_6->set_NextState_1(((int32_t)13));
		return (bool)1;
	}

IL_0027:
	{
		return (bool)0;
	}
}
// System.Boolean ThirdParty.Json.LitJson.Lexer::State13(ThirdParty.Json.LitJson.FsmContext)
extern "C"  bool Lexer_State13_m3181610835 (Il2CppObject * __this /* static, unused */, FsmContext_t4275593467 * ___ctx0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		FsmContext_t4275593467 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t954714164 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		Lexer_GetChar_m3775205041(L_1, /*hidden argument*/NULL);
		FsmContext_t4275593467 * L_2 = ___ctx0;
		NullCheck(L_2);
		Lexer_t954714164 * L_3 = L_2->get_L_2();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_input_char_7();
		V_0 = L_4;
		int32_t L_5 = V_0;
		if ((!(((uint32_t)L_5) == ((uint32_t)((int32_t)108)))))
		{
			goto IL_0027;
		}
	}
	{
		FsmContext_t4275593467 * L_6 = ___ctx0;
		NullCheck(L_6);
		L_6->set_NextState_1(((int32_t)14));
		return (bool)1;
	}

IL_0027:
	{
		return (bool)0;
	}
}
// System.Boolean ThirdParty.Json.LitJson.Lexer::State14(ThirdParty.Json.LitJson.FsmContext)
extern "C"  bool Lexer_State14_m2103223124 (Il2CppObject * __this /* static, unused */, FsmContext_t4275593467 * ___ctx0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		FsmContext_t4275593467 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t954714164 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		Lexer_GetChar_m3775205041(L_1, /*hidden argument*/NULL);
		FsmContext_t4275593467 * L_2 = ___ctx0;
		NullCheck(L_2);
		Lexer_t954714164 * L_3 = L_2->get_L_2();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_input_char_7();
		V_0 = L_4;
		int32_t L_5 = V_0;
		if ((!(((uint32_t)L_5) == ((uint32_t)((int32_t)115)))))
		{
			goto IL_0027;
		}
	}
	{
		FsmContext_t4275593467 * L_6 = ___ctx0;
		NullCheck(L_6);
		L_6->set_NextState_1(((int32_t)15));
		return (bool)1;
	}

IL_0027:
	{
		return (bool)0;
	}
}
// System.Boolean ThirdParty.Json.LitJson.Lexer::State15(ThirdParty.Json.LitJson.FsmContext)
extern "C"  bool Lexer_State15_m564461653 (Il2CppObject * __this /* static, unused */, FsmContext_t4275593467 * ___ctx0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		FsmContext_t4275593467 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t954714164 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		Lexer_GetChar_m3775205041(L_1, /*hidden argument*/NULL);
		FsmContext_t4275593467 * L_2 = ___ctx0;
		NullCheck(L_2);
		Lexer_t954714164 * L_3 = L_2->get_L_2();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_input_char_7();
		V_0 = L_4;
		int32_t L_5 = V_0;
		if ((!(((uint32_t)L_5) == ((uint32_t)((int32_t)101)))))
		{
			goto IL_002d;
		}
	}
	{
		FsmContext_t4275593467 * L_6 = ___ctx0;
		NullCheck(L_6);
		L_6->set_Return_0((bool)1);
		FsmContext_t4275593467 * L_7 = ___ctx0;
		NullCheck(L_7);
		L_7->set_NextState_1(1);
		return (bool)1;
	}

IL_002d:
	{
		return (bool)0;
	}
}
// System.Boolean ThirdParty.Json.LitJson.Lexer::State16(ThirdParty.Json.LitJson.FsmContext)
extern "C"  bool Lexer_State16_m368302934 (Il2CppObject * __this /* static, unused */, FsmContext_t4275593467 * ___ctx0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		FsmContext_t4275593467 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t954714164 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		Lexer_GetChar_m3775205041(L_1, /*hidden argument*/NULL);
		FsmContext_t4275593467 * L_2 = ___ctx0;
		NullCheck(L_2);
		Lexer_t954714164 * L_3 = L_2->get_L_2();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_input_char_7();
		V_0 = L_4;
		int32_t L_5 = V_0;
		if ((!(((uint32_t)L_5) == ((uint32_t)((int32_t)117)))))
		{
			goto IL_0027;
		}
	}
	{
		FsmContext_t4275593467 * L_6 = ___ctx0;
		NullCheck(L_6);
		L_6->set_NextState_1(((int32_t)17));
		return (bool)1;
	}

IL_0027:
	{
		return (bool)0;
	}
}
// System.Boolean ThirdParty.Json.LitJson.Lexer::State17(ThirdParty.Json.LitJson.FsmContext)
extern "C"  bool Lexer_State17_m3124508759 (Il2CppObject * __this /* static, unused */, FsmContext_t4275593467 * ___ctx0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		FsmContext_t4275593467 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t954714164 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		Lexer_GetChar_m3775205041(L_1, /*hidden argument*/NULL);
		FsmContext_t4275593467 * L_2 = ___ctx0;
		NullCheck(L_2);
		Lexer_t954714164 * L_3 = L_2->get_L_2();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_input_char_7();
		V_0 = L_4;
		int32_t L_5 = V_0;
		if ((!(((uint32_t)L_5) == ((uint32_t)((int32_t)108)))))
		{
			goto IL_0027;
		}
	}
	{
		FsmContext_t4275593467 * L_6 = ___ctx0;
		NullCheck(L_6);
		L_6->set_NextState_1(((int32_t)18));
		return (bool)1;
	}

IL_0027:
	{
		return (bool)0;
	}
}
// System.Boolean ThirdParty.Json.LitJson.Lexer::State18(ThirdParty.Json.LitJson.FsmContext)
extern "C"  bool Lexer_State18_m3963376200 (Il2CppObject * __this /* static, unused */, FsmContext_t4275593467 * ___ctx0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		FsmContext_t4275593467 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t954714164 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		Lexer_GetChar_m3775205041(L_1, /*hidden argument*/NULL);
		FsmContext_t4275593467 * L_2 = ___ctx0;
		NullCheck(L_2);
		Lexer_t954714164 * L_3 = L_2->get_L_2();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_input_char_7();
		V_0 = L_4;
		int32_t L_5 = V_0;
		if ((!(((uint32_t)L_5) == ((uint32_t)((int32_t)108)))))
		{
			goto IL_002d;
		}
	}
	{
		FsmContext_t4275593467 * L_6 = ___ctx0;
		NullCheck(L_6);
		L_6->set_Return_0((bool)1);
		FsmContext_t4275593467 * L_7 = ___ctx0;
		NullCheck(L_7);
		L_7->set_NextState_1(1);
		return (bool)1;
	}

IL_002d:
	{
		return (bool)0;
	}
}
// System.Boolean ThirdParty.Json.LitJson.Lexer::State19(ThirdParty.Json.LitJson.FsmContext)
extern "C"  bool Lexer_State19_m2424614729 (Il2CppObject * __this /* static, unused */, FsmContext_t4275593467 * ___ctx0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		goto IL_0065;
	}

IL_0002:
	{
		FsmContext_t4275593467 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t954714164 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		int32_t L_2 = L_1->get_input_char_7();
		V_0 = L_2;
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)((int32_t)34))))
		{
			goto IL_001a;
		}
	}
	{
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) == ((int32_t)((int32_t)92))))
		{
			goto IL_0036;
		}
	}
	{
		goto IL_0048;
	}

IL_001a:
	{
		FsmContext_t4275593467 * L_5 = ___ctx0;
		NullCheck(L_5);
		Lexer_t954714164 * L_6 = L_5->get_L_2();
		NullCheck(L_6);
		Lexer_UngetChar_m1417570926(L_6, /*hidden argument*/NULL);
		FsmContext_t4275593467 * L_7 = ___ctx0;
		NullCheck(L_7);
		L_7->set_Return_0((bool)1);
		FsmContext_t4275593467 * L_8 = ___ctx0;
		NullCheck(L_8);
		L_8->set_NextState_1(((int32_t)20));
		return (bool)1;
	}

IL_0036:
	{
		FsmContext_t4275593467 * L_9 = ___ctx0;
		NullCheck(L_9);
		L_9->set_StateStack_3(((int32_t)19));
		FsmContext_t4275593467 * L_10 = ___ctx0;
		NullCheck(L_10);
		L_10->set_NextState_1(((int32_t)21));
		return (bool)1;
	}

IL_0048:
	{
		FsmContext_t4275593467 * L_11 = ___ctx0;
		NullCheck(L_11);
		Lexer_t954714164 * L_12 = L_11->get_L_2();
		NullCheck(L_12);
		StringBuilder_t1221177846 * L_13 = L_12->get_string_buffer_10();
		FsmContext_t4275593467 * L_14 = ___ctx0;
		NullCheck(L_14);
		Lexer_t954714164 * L_15 = L_14->get_L_2();
		NullCheck(L_15);
		int32_t L_16 = L_15->get_input_char_7();
		NullCheck(L_13);
		StringBuilder_Append_m3618697540(L_13, (((int32_t)((uint16_t)L_16))), /*hidden argument*/NULL);
	}

IL_0065:
	{
		FsmContext_t4275593467 * L_17 = ___ctx0;
		NullCheck(L_17);
		Lexer_t954714164 * L_18 = L_17->get_L_2();
		NullCheck(L_18);
		bool L_19 = Lexer_GetChar_m3775205041(L_18, /*hidden argument*/NULL);
		if (L_19)
		{
			goto IL_0002;
		}
	}
	{
		return (bool)1;
	}
}
// System.Boolean ThirdParty.Json.LitJson.Lexer::State20(ThirdParty.Json.LitJson.FsmContext)
extern "C"  bool Lexer_State20_m3589906367 (Il2CppObject * __this /* static, unused */, FsmContext_t4275593467 * ___ctx0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		FsmContext_t4275593467 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t954714164 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		Lexer_GetChar_m3775205041(L_1, /*hidden argument*/NULL);
		FsmContext_t4275593467 * L_2 = ___ctx0;
		NullCheck(L_2);
		Lexer_t954714164 * L_3 = L_2->get_L_2();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_input_char_7();
		V_0 = L_4;
		int32_t L_5 = V_0;
		if ((!(((uint32_t)L_5) == ((uint32_t)((int32_t)34)))))
		{
			goto IL_002d;
		}
	}
	{
		FsmContext_t4275593467 * L_6 = ___ctx0;
		NullCheck(L_6);
		L_6->set_Return_0((bool)1);
		FsmContext_t4275593467 * L_7 = ___ctx0;
		NullCheck(L_7);
		L_7->set_NextState_1(1);
		return (bool)1;
	}

IL_002d:
	{
		return (bool)0;
	}
}
// System.Boolean ThirdParty.Json.LitJson.Lexer::State21(ThirdParty.Json.LitJson.FsmContext)
extern Il2CppClass* Lexer_t954714164_il2cpp_TypeInfo_var;
extern const uint32_t Lexer_State21_m2051144896_MetadataUsageId;
extern "C"  bool Lexer_State21_m2051144896 (Il2CppObject * __this /* static, unused */, FsmContext_t4275593467 * ___ctx0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Lexer_State21_m2051144896_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		FsmContext_t4275593467 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t954714164 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		Lexer_GetChar_m3775205041(L_1, /*hidden argument*/NULL);
		FsmContext_t4275593467 * L_2 = ___ctx0;
		NullCheck(L_2);
		Lexer_t954714164 * L_3 = L_2->get_L_2();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_input_char_7();
		V_0 = L_4;
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) > ((int32_t)((int32_t)92))))
		{
			goto IL_003a;
		}
	}
	{
		int32_t L_6 = V_0;
		if ((((int32_t)L_6) > ((int32_t)((int32_t)39))))
		{
			goto IL_002e;
		}
	}
	{
		int32_t L_7 = V_0;
		if ((((int32_t)L_7) == ((int32_t)((int32_t)34))))
		{
			goto IL_0075;
		}
	}
	{
		int32_t L_8 = V_0;
		if ((((int32_t)L_8) == ((int32_t)((int32_t)39))))
		{
			goto IL_0075;
		}
	}
	{
		goto IL_00a4;
	}

IL_002e:
	{
		int32_t L_9 = V_0;
		if ((((int32_t)L_9) == ((int32_t)((int32_t)47))))
		{
			goto IL_0075;
		}
	}
	{
		int32_t L_10 = V_0;
		if ((((int32_t)L_10) == ((int32_t)((int32_t)92))))
		{
			goto IL_0075;
		}
	}
	{
		goto IL_00a4;
	}

IL_003a:
	{
		int32_t L_11 = V_0;
		if ((((int32_t)L_11) > ((int32_t)((int32_t)102))))
		{
			goto IL_004b;
		}
	}
	{
		int32_t L_12 = V_0;
		if ((((int32_t)L_12) == ((int32_t)((int32_t)98))))
		{
			goto IL_0075;
		}
	}
	{
		int32_t L_13 = V_0;
		if ((((int32_t)L_13) == ((int32_t)((int32_t)102))))
		{
			goto IL_0075;
		}
	}
	{
		goto IL_00a4;
	}

IL_004b:
	{
		int32_t L_14 = V_0;
		if ((((int32_t)L_14) == ((int32_t)((int32_t)110))))
		{
			goto IL_0075;
		}
	}
	{
		int32_t L_15 = V_0;
		if (((int32_t)((int32_t)L_15-(int32_t)((int32_t)114))) == 0)
		{
			goto IL_0075;
		}
		if (((int32_t)((int32_t)L_15-(int32_t)((int32_t)114))) == 1)
		{
			goto IL_00a4;
		}
		if (((int32_t)((int32_t)L_15-(int32_t)((int32_t)114))) == 2)
		{
			goto IL_0075;
		}
		if (((int32_t)((int32_t)L_15-(int32_t)((int32_t)114))) == 3)
		{
			goto IL_006b;
		}
	}
	{
		goto IL_00a4;
	}

IL_006b:
	{
		FsmContext_t4275593467 * L_16 = ___ctx0;
		NullCheck(L_16);
		L_16->set_NextState_1(((int32_t)22));
		return (bool)1;
	}

IL_0075:
	{
		FsmContext_t4275593467 * L_17 = ___ctx0;
		NullCheck(L_17);
		Lexer_t954714164 * L_18 = L_17->get_L_2();
		NullCheck(L_18);
		StringBuilder_t1221177846 * L_19 = L_18->get_string_buffer_10();
		FsmContext_t4275593467 * L_20 = ___ctx0;
		NullCheck(L_20);
		Lexer_t954714164 * L_21 = L_20->get_L_2();
		NullCheck(L_21);
		int32_t L_22 = L_21->get_input_char_7();
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t954714164_il2cpp_TypeInfo_var);
		Il2CppChar L_23 = Lexer_ProcessEscChar_m530821430(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		NullCheck(L_19);
		StringBuilder_Append_m3618697540(L_19, L_23, /*hidden argument*/NULL);
		FsmContext_t4275593467 * L_24 = ___ctx0;
		FsmContext_t4275593467 * L_25 = ___ctx0;
		NullCheck(L_25);
		int32_t L_26 = L_25->get_StateStack_3();
		NullCheck(L_24);
		L_24->set_NextState_1(L_26);
		return (bool)1;
	}

IL_00a4:
	{
		return (bool)0;
	}
}
// System.Boolean ThirdParty.Json.LitJson.Lexer::State22(ThirdParty.Json.LitJson.FsmContext)
extern Il2CppClass* Lexer_t954714164_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern const uint32_t Lexer_State22_m1854986177_MetadataUsageId;
extern "C"  bool Lexer_State22_m1854986177 (Il2CppObject * __this /* static, unused */, FsmContext_t4275593467 * ___ctx0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Lexer_State22_m1854986177_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = 0;
		V_1 = ((int32_t)4096);
		FsmContext_t4275593467 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t954714164 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		L_1->set_unichar_13(0);
		goto IL_00d5;
	}

IL_0019:
	{
		FsmContext_t4275593467 * L_2 = ___ctx0;
		NullCheck(L_2);
		Lexer_t954714164 * L_3 = L_2->get_L_2();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_input_char_7();
		if ((((int32_t)L_4) < ((int32_t)((int32_t)48))))
		{
			goto IL_0037;
		}
	}
	{
		FsmContext_t4275593467 * L_5 = ___ctx0;
		NullCheck(L_5);
		Lexer_t954714164 * L_6 = L_5->get_L_2();
		NullCheck(L_6);
		int32_t L_7 = L_6->get_input_char_7();
		if ((((int32_t)L_7) <= ((int32_t)((int32_t)57))))
		{
			goto IL_0073;
		}
	}

IL_0037:
	{
		FsmContext_t4275593467 * L_8 = ___ctx0;
		NullCheck(L_8);
		Lexer_t954714164 * L_9 = L_8->get_L_2();
		NullCheck(L_9);
		int32_t L_10 = L_9->get_input_char_7();
		if ((((int32_t)L_10) < ((int32_t)((int32_t)65))))
		{
			goto IL_0055;
		}
	}
	{
		FsmContext_t4275593467 * L_11 = ___ctx0;
		NullCheck(L_11);
		Lexer_t954714164 * L_12 = L_11->get_L_2();
		NullCheck(L_12);
		int32_t L_13 = L_12->get_input_char_7();
		if ((((int32_t)L_13) <= ((int32_t)((int32_t)70))))
		{
			goto IL_0073;
		}
	}

IL_0055:
	{
		FsmContext_t4275593467 * L_14 = ___ctx0;
		NullCheck(L_14);
		Lexer_t954714164 * L_15 = L_14->get_L_2();
		NullCheck(L_15);
		int32_t L_16 = L_15->get_input_char_7();
		if ((((int32_t)L_16) < ((int32_t)((int32_t)97))))
		{
			goto IL_00d3;
		}
	}
	{
		FsmContext_t4275593467 * L_17 = ___ctx0;
		NullCheck(L_17);
		Lexer_t954714164 * L_18 = L_17->get_L_2();
		NullCheck(L_18);
		int32_t L_19 = L_18->get_input_char_7();
		if ((((int32_t)L_19) > ((int32_t)((int32_t)102))))
		{
			goto IL_00d3;
		}
	}

IL_0073:
	{
		FsmContext_t4275593467 * L_20 = ___ctx0;
		NullCheck(L_20);
		Lexer_t954714164 * L_21 = L_20->get_L_2();
		Lexer_t954714164 * L_22 = L_21;
		NullCheck(L_22);
		int32_t L_23 = L_22->get_unichar_13();
		FsmContext_t4275593467 * L_24 = ___ctx0;
		NullCheck(L_24);
		Lexer_t954714164 * L_25 = L_24->get_L_2();
		NullCheck(L_25);
		int32_t L_26 = L_25->get_input_char_7();
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t954714164_il2cpp_TypeInfo_var);
		int32_t L_27 = Lexer_HexValue_m4231875178(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
		int32_t L_28 = V_1;
		NullCheck(L_22);
		L_22->set_unichar_13(((int32_t)((int32_t)L_23+(int32_t)((int32_t)((int32_t)L_27*(int32_t)L_28)))));
		int32_t L_29 = V_0;
		V_0 = ((int32_t)((int32_t)L_29+(int32_t)1));
		int32_t L_30 = V_1;
		V_1 = ((int32_t)((int32_t)L_30/(int32_t)((int32_t)16)));
		int32_t L_31 = V_0;
		if ((!(((uint32_t)L_31) == ((uint32_t)4))))
		{
			goto IL_00d5;
		}
	}
	{
		FsmContext_t4275593467 * L_32 = ___ctx0;
		NullCheck(L_32);
		Lexer_t954714164 * L_33 = L_32->get_L_2();
		NullCheck(L_33);
		StringBuilder_t1221177846 * L_34 = L_33->get_string_buffer_10();
		FsmContext_t4275593467 * L_35 = ___ctx0;
		NullCheck(L_35);
		Lexer_t954714164 * L_36 = L_35->get_L_2();
		NullCheck(L_36);
		int32_t L_37 = L_36->get_unichar_13();
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		Il2CppChar L_38 = Convert_ToChar_m3827339132(NULL /*static, unused*/, L_37, /*hidden argument*/NULL);
		NullCheck(L_34);
		StringBuilder_Append_m3618697540(L_34, L_38, /*hidden argument*/NULL);
		FsmContext_t4275593467 * L_39 = ___ctx0;
		FsmContext_t4275593467 * L_40 = ___ctx0;
		NullCheck(L_40);
		int32_t L_41 = L_40->get_StateStack_3();
		NullCheck(L_39);
		L_39->set_NextState_1(L_41);
		return (bool)1;
	}

IL_00d3:
	{
		return (bool)0;
	}

IL_00d5:
	{
		FsmContext_t4275593467 * L_42 = ___ctx0;
		NullCheck(L_42);
		Lexer_t954714164 * L_43 = L_42->get_L_2();
		NullCheck(L_43);
		bool L_44 = Lexer_GetChar_m3775205041(L_43, /*hidden argument*/NULL);
		if (L_44)
		{
			goto IL_0019;
		}
	}
	{
		return (bool)1;
	}
}
// System.Boolean ThirdParty.Json.LitJson.Lexer::State23(ThirdParty.Json.LitJson.FsmContext)
extern "C"  bool Lexer_State23_m316224706 (Il2CppObject * __this /* static, unused */, FsmContext_t4275593467 * ___ctx0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		goto IL_0065;
	}

IL_0002:
	{
		FsmContext_t4275593467 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t954714164 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		int32_t L_2 = L_1->get_input_char_7();
		V_0 = L_2;
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)((int32_t)39))))
		{
			goto IL_001a;
		}
	}
	{
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) == ((int32_t)((int32_t)92))))
		{
			goto IL_0036;
		}
	}
	{
		goto IL_0048;
	}

IL_001a:
	{
		FsmContext_t4275593467 * L_5 = ___ctx0;
		NullCheck(L_5);
		Lexer_t954714164 * L_6 = L_5->get_L_2();
		NullCheck(L_6);
		Lexer_UngetChar_m1417570926(L_6, /*hidden argument*/NULL);
		FsmContext_t4275593467 * L_7 = ___ctx0;
		NullCheck(L_7);
		L_7->set_Return_0((bool)1);
		FsmContext_t4275593467 * L_8 = ___ctx0;
		NullCheck(L_8);
		L_8->set_NextState_1(((int32_t)24));
		return (bool)1;
	}

IL_0036:
	{
		FsmContext_t4275593467 * L_9 = ___ctx0;
		NullCheck(L_9);
		L_9->set_StateStack_3(((int32_t)23));
		FsmContext_t4275593467 * L_10 = ___ctx0;
		NullCheck(L_10);
		L_10->set_NextState_1(((int32_t)21));
		return (bool)1;
	}

IL_0048:
	{
		FsmContext_t4275593467 * L_11 = ___ctx0;
		NullCheck(L_11);
		Lexer_t954714164 * L_12 = L_11->get_L_2();
		NullCheck(L_12);
		StringBuilder_t1221177846 * L_13 = L_12->get_string_buffer_10();
		FsmContext_t4275593467 * L_14 = ___ctx0;
		NullCheck(L_14);
		Lexer_t954714164 * L_15 = L_14->get_L_2();
		NullCheck(L_15);
		int32_t L_16 = L_15->get_input_char_7();
		NullCheck(L_13);
		StringBuilder_Append_m3618697540(L_13, (((int32_t)((uint16_t)L_16))), /*hidden argument*/NULL);
	}

IL_0065:
	{
		FsmContext_t4275593467 * L_17 = ___ctx0;
		NullCheck(L_17);
		Lexer_t954714164 * L_18 = L_17->get_L_2();
		NullCheck(L_18);
		bool L_19 = Lexer_GetChar_m3775205041(L_18, /*hidden argument*/NULL);
		if (L_19)
		{
			goto IL_0002;
		}
	}
	{
		return (bool)1;
	}
}
// System.Boolean ThirdParty.Json.LitJson.Lexer::State24(ThirdParty.Json.LitJson.FsmContext)
extern "C"  bool Lexer_State24_m3532804291 (Il2CppObject * __this /* static, unused */, FsmContext_t4275593467 * ___ctx0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		FsmContext_t4275593467 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t954714164 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		Lexer_GetChar_m3775205041(L_1, /*hidden argument*/NULL);
		FsmContext_t4275593467 * L_2 = ___ctx0;
		NullCheck(L_2);
		Lexer_t954714164 * L_3 = L_2->get_L_2();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_input_char_7();
		V_0 = L_4;
		int32_t L_5 = V_0;
		if ((!(((uint32_t)L_5) == ((uint32_t)((int32_t)39)))))
		{
			goto IL_003a;
		}
	}
	{
		FsmContext_t4275593467 * L_6 = ___ctx0;
		NullCheck(L_6);
		Lexer_t954714164 * L_7 = L_6->get_L_2();
		NullCheck(L_7);
		L_7->set_input_char_7(((int32_t)34));
		FsmContext_t4275593467 * L_8 = ___ctx0;
		NullCheck(L_8);
		L_8->set_Return_0((bool)1);
		FsmContext_t4275593467 * L_9 = ___ctx0;
		NullCheck(L_9);
		L_9->set_NextState_1(1);
		return (bool)1;
	}

IL_003a:
	{
		return (bool)0;
	}
}
// System.Boolean ThirdParty.Json.LitJson.Lexer::State25(ThirdParty.Json.LitJson.FsmContext)
extern "C"  bool Lexer_State25_m1994042820 (Il2CppObject * __this /* static, unused */, FsmContext_t4275593467 * ___ctx0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		FsmContext_t4275593467 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t954714164 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		Lexer_GetChar_m3775205041(L_1, /*hidden argument*/NULL);
		FsmContext_t4275593467 * L_2 = ___ctx0;
		NullCheck(L_2);
		Lexer_t954714164 * L_3 = L_2->get_L_2();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_input_char_7();
		V_0 = L_4;
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) == ((int32_t)((int32_t)42))))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_6 = V_0;
		if ((((int32_t)L_6) == ((int32_t)((int32_t)47))))
		{
			goto IL_002e;
		}
	}
	{
		goto IL_0038;
	}

IL_0024:
	{
		FsmContext_t4275593467 * L_7 = ___ctx0;
		NullCheck(L_7);
		L_7->set_NextState_1(((int32_t)27));
		return (bool)1;
	}

IL_002e:
	{
		FsmContext_t4275593467 * L_8 = ___ctx0;
		NullCheck(L_8);
		L_8->set_NextState_1(((int32_t)26));
		return (bool)1;
	}

IL_0038:
	{
		return (bool)0;
	}
}
// System.Boolean ThirdParty.Json.LitJson.Lexer::State26(ThirdParty.Json.LitJson.FsmContext)
extern "C"  bool Lexer_State26_m1797884101 (Il2CppObject * __this /* static, unused */, FsmContext_t4275593467 * ___ctx0, const MethodInfo* method)
{
	{
		goto IL_001a;
	}

IL_0002:
	{
		FsmContext_t4275593467 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t954714164 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		int32_t L_2 = L_1->get_input_char_7();
		if ((!(((uint32_t)L_2) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_001a;
		}
	}
	{
		FsmContext_t4275593467 * L_3 = ___ctx0;
		NullCheck(L_3);
		L_3->set_NextState_1(1);
		return (bool)1;
	}

IL_001a:
	{
		FsmContext_t4275593467 * L_4 = ___ctx0;
		NullCheck(L_4);
		Lexer_t954714164 * L_5 = L_4->get_L_2();
		NullCheck(L_5);
		bool L_6 = Lexer_GetChar_m3775205041(L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0002;
		}
	}
	{
		return (bool)1;
	}
}
// System.Boolean ThirdParty.Json.LitJson.Lexer::State27(ThirdParty.Json.LitJson.FsmContext)
extern "C"  bool Lexer_State27_m259122630 (Il2CppObject * __this /* static, unused */, FsmContext_t4275593467 * ___ctx0, const MethodInfo* method)
{
	{
		goto IL_001b;
	}

IL_0002:
	{
		FsmContext_t4275593467 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t954714164 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		int32_t L_2 = L_1->get_input_char_7();
		if ((!(((uint32_t)L_2) == ((uint32_t)((int32_t)42)))))
		{
			goto IL_001b;
		}
	}
	{
		FsmContext_t4275593467 * L_3 = ___ctx0;
		NullCheck(L_3);
		L_3->set_NextState_1(((int32_t)28));
		return (bool)1;
	}

IL_001b:
	{
		FsmContext_t4275593467 * L_4 = ___ctx0;
		NullCheck(L_4);
		Lexer_t954714164 * L_5 = L_4->get_L_2();
		NullCheck(L_5);
		bool L_6 = Lexer_GetChar_m3775205041(L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0002;
		}
	}
	{
		return (bool)1;
	}
}
// System.Boolean ThirdParty.Json.LitJson.Lexer::State28(ThirdParty.Json.LitJson.FsmContext)
extern "C"  bool Lexer_State28_m1097990071 (Il2CppObject * __this /* static, unused */, FsmContext_t4275593467 * ___ctx0, const MethodInfo* method)
{
	{
		goto IL_0033;
	}

IL_0002:
	{
		FsmContext_t4275593467 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t954714164 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		int32_t L_2 = L_1->get_input_char_7();
		if ((((int32_t)L_2) == ((int32_t)((int32_t)42))))
		{
			goto IL_0033;
		}
	}
	{
		FsmContext_t4275593467 * L_3 = ___ctx0;
		NullCheck(L_3);
		Lexer_t954714164 * L_4 = L_3->get_L_2();
		NullCheck(L_4);
		int32_t L_5 = L_4->get_input_char_7();
		if ((!(((uint32_t)L_5) == ((uint32_t)((int32_t)47)))))
		{
			goto IL_0029;
		}
	}
	{
		FsmContext_t4275593467 * L_6 = ___ctx0;
		NullCheck(L_6);
		L_6->set_NextState_1(1);
		return (bool)1;
	}

IL_0029:
	{
		FsmContext_t4275593467 * L_7 = ___ctx0;
		NullCheck(L_7);
		L_7->set_NextState_1(((int32_t)27));
		return (bool)1;
	}

IL_0033:
	{
		FsmContext_t4275593467 * L_8 = ___ctx0;
		NullCheck(L_8);
		Lexer_t954714164 * L_9 = L_8->get_L_2();
		NullCheck(L_9);
		bool L_10 = Lexer_GetChar_m3775205041(L_9, /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_0002;
		}
	}
	{
		return (bool)1;
	}
}
// System.Boolean ThirdParty.Json.LitJson.Lexer::GetChar()
extern "C"  bool Lexer_GetChar_m3775205041 (Lexer_t954714164 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = Lexer_NextChar_m833558632(__this, /*hidden argument*/NULL);
		int32_t L_1 = L_0;
		V_0 = L_1;
		__this->set_input_char_7(L_1);
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)(-1))))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)1;
	}

IL_0014:
	{
		__this->set_end_of_input_4((bool)1);
		return (bool)0;
	}
}
// System.Int32 ThirdParty.Json.LitJson.Lexer::NextChar()
extern "C"  int32_t Lexer_NextChar_m833558632 (Lexer_t954714164 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_input_buffer_6();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = __this->get_input_buffer_6();
		__this->set_input_buffer_6(0);
		return L_1;
	}

IL_0016:
	{
		TextReader_t1561828458 * L_2 = __this->get_reader_8();
		NullCheck(L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.IO.TextReader::Read() */, L_2);
		return L_3;
	}
}
// System.Boolean ThirdParty.Json.LitJson.Lexer::NextToken()
extern Il2CppClass* Lexer_t954714164_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonException_t1457213491_il2cpp_TypeInfo_var;
extern const uint32_t Lexer_NextToken_m4229197011_MetadataUsageId;
extern "C"  bool Lexer_NextToken_m4229197011 (Lexer_t954714164 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Lexer_NextToken_m4229197011_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		FsmContext_t4275593467 * L_0 = __this->get_fsm_context_5();
		NullCheck(L_0);
		L_0->set_Return_0((bool)0);
	}

IL_000c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t954714164_il2cpp_TypeInfo_var);
		StateHandlerU5BU5D_t1374508575* L_1 = ((Lexer_t954714164_StaticFields*)Lexer_t954714164_il2cpp_TypeInfo_var->static_fields)->get_fsm_handler_table_1();
		int32_t L_2 = __this->get_state_9();
		NullCheck(L_1);
		int32_t L_3 = ((int32_t)((int32_t)L_2-(int32_t)1));
		StateHandler_t3489987002 * L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		FsmContext_t4275593467 * L_5 = __this->get_fsm_context_5();
		NullCheck(L_4);
		bool L_6 = StateHandler_Invoke_m4291935966(L_4, L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0033;
		}
	}
	{
		int32_t L_7 = __this->get_input_char_7();
		JsonException_t1457213491 * L_8 = (JsonException_t1457213491 *)il2cpp_codegen_object_new(JsonException_t1457213491_il2cpp_TypeInfo_var);
		JsonException__ctor_m1345348707(L_8, L_7, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_0033:
	{
		bool L_9 = __this->get_end_of_input_4();
		if (!L_9)
		{
			goto IL_003d;
		}
	}
	{
		return (bool)0;
	}

IL_003d:
	{
		FsmContext_t4275593467 * L_10 = __this->get_fsm_context_5();
		NullCheck(L_10);
		bool L_11 = L_10->get_Return_0();
		if (!L_11)
		{
			goto IL_00a7;
		}
	}
	{
		StringBuilder_t1221177846 * L_12 = __this->get_string_buffer_10();
		NullCheck(L_12);
		String_t* L_13 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_12);
		__this->set_string_value_11(L_13);
		StringBuilder_t1221177846 * L_14 = __this->get_string_buffer_10();
		NullCheck(L_14);
		StringBuilder_set_Length_m3039225444(L_14, 0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t954714164_il2cpp_TypeInfo_var);
		Int32U5BU5D_t3030399641* L_15 = ((Lexer_t954714164_StaticFields*)Lexer_t954714164_il2cpp_TypeInfo_var->static_fields)->get_fsm_return_table_0();
		int32_t L_16 = __this->get_state_9();
		NullCheck(L_15);
		int32_t L_17 = ((int32_t)((int32_t)L_16-(int32_t)1));
		int32_t L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		__this->set_token_12(L_18);
		int32_t L_19 = __this->get_token_12();
		if ((!(((uint32_t)L_19) == ((uint32_t)((int32_t)65542)))))
		{
			goto IL_0094;
		}
	}
	{
		int32_t L_20 = __this->get_input_char_7();
		__this->set_token_12(L_20);
	}

IL_0094:
	{
		FsmContext_t4275593467 * L_21 = __this->get_fsm_context_5();
		NullCheck(L_21);
		int32_t L_22 = L_21->get_NextState_1();
		__this->set_state_9(L_22);
		return (bool)1;
	}

IL_00a7:
	{
		FsmContext_t4275593467 * L_23 = __this->get_fsm_context_5();
		NullCheck(L_23);
		int32_t L_24 = L_23->get_NextState_1();
		__this->set_state_9(L_24);
		goto IL_000c;
	}
}
// System.Void ThirdParty.Json.LitJson.Lexer::UngetChar()
extern "C"  void Lexer_UngetChar_m1417570926 (Lexer_t954714164 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_input_char_7();
		__this->set_input_buffer_6(L_0);
		return;
	}
}
// System.Void ThirdParty.Json.LitJson.Lexer/StateHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void StateHandler__ctor_m1057417747 (StateHandler_t3489987002 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean ThirdParty.Json.LitJson.Lexer/StateHandler::Invoke(ThirdParty.Json.LitJson.FsmContext)
extern "C"  bool StateHandler_Invoke_m4291935966 (StateHandler_t3489987002 * __this, FsmContext_t4275593467 * ___ctx0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		StateHandler_Invoke_m4291935966((StateHandler_t3489987002 *)__this->get_prev_9(),___ctx0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, FsmContext_t4275593467 * ___ctx0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___ctx0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (void* __this, FsmContext_t4275593467 * ___ctx0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___ctx0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___ctx0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult ThirdParty.Json.LitJson.Lexer/StateHandler::BeginInvoke(ThirdParty.Json.LitJson.FsmContext,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * StateHandler_BeginInvoke_m2133288829 (StateHandler_t3489987002 * __this, FsmContext_t4275593467 * ___ctx0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___ctx0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean ThirdParty.Json.LitJson.Lexer/StateHandler::EndInvoke(System.IAsyncResult)
extern "C"  bool StateHandler_EndInvoke_m2698481297 (StateHandler_t3489987002 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// Conversion methods for marshalling of: ThirdParty.Json.LitJson.ObjectMetadata
extern "C" void ObjectMetadata_t4058137740_marshal_pinvoke(const ObjectMetadata_t4058137740& unmarshaled, ObjectMetadata_t4058137740_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___element_type_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'element_type' of type 'ObjectMetadata': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___element_type_0Exception);
}
extern "C" void ObjectMetadata_t4058137740_marshal_pinvoke_back(const ObjectMetadata_t4058137740_marshaled_pinvoke& marshaled, ObjectMetadata_t4058137740& unmarshaled)
{
	Il2CppCodeGenException* ___element_type_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'element_type' of type 'ObjectMetadata': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___element_type_0Exception);
}
// Conversion method for clean up from marshalling of: ThirdParty.Json.LitJson.ObjectMetadata
extern "C" void ObjectMetadata_t4058137740_marshal_pinvoke_cleanup(ObjectMetadata_t4058137740_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: ThirdParty.Json.LitJson.ObjectMetadata
extern "C" void ObjectMetadata_t4058137740_marshal_com(const ObjectMetadata_t4058137740& unmarshaled, ObjectMetadata_t4058137740_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___element_type_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'element_type' of type 'ObjectMetadata': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___element_type_0Exception);
}
extern "C" void ObjectMetadata_t4058137740_marshal_com_back(const ObjectMetadata_t4058137740_marshaled_com& marshaled, ObjectMetadata_t4058137740& unmarshaled)
{
	Il2CppCodeGenException* ___element_type_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'element_type' of type 'ObjectMetadata': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___element_type_0Exception);
}
// Conversion method for clean up from marshalling of: ThirdParty.Json.LitJson.ObjectMetadata
extern "C" void ObjectMetadata_t4058137740_marshal_com_cleanup(ObjectMetadata_t4058137740_marshaled_com& marshaled)
{
}
// System.Object ThirdParty.Json.LitJson.OrderedDictionaryEnumerator::get_Current()
extern Il2CppClass* DictionaryEntry_t3048875398_il2cpp_TypeInfo_var;
extern const uint32_t OrderedDictionaryEnumerator_get_Current_m3299941819_MetadataUsageId;
extern "C"  Il2CppObject * OrderedDictionaryEnumerator_get_Current_m3299941819 (OrderedDictionaryEnumerator_t1664192327 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OrderedDictionaryEnumerator_get_Current_m3299941819_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		DictionaryEntry_t3048875398  L_0 = OrderedDictionaryEnumerator_get_Entry_m928514144(__this, /*hidden argument*/NULL);
		DictionaryEntry_t3048875398  L_1 = L_0;
		Il2CppObject * L_2 = Box(DictionaryEntry_t3048875398_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Collections.DictionaryEntry ThirdParty.Json.LitJson.OrderedDictionaryEnumerator::get_Entry()
extern Il2CppClass* IEnumerator_1_t1410900363_il2cpp_TypeInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m1021780796_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m3387678519_MethodInfo_var;
extern const uint32_t OrderedDictionaryEnumerator_get_Entry_m928514144_MetadataUsageId;
extern "C"  DictionaryEntry_t3048875398  OrderedDictionaryEnumerator_get_Entry_m928514144 (OrderedDictionaryEnumerator_t1664192327 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OrderedDictionaryEnumerator_get_Entry_m928514144_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t3935376536  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Il2CppObject* L_0 = __this->get_list_enumerator_0();
		NullCheck(L_0);
		KeyValuePair_2_t3935376536  L_1 = InterfaceFuncInvoker0< KeyValuePair_2_t3935376536  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,ThirdParty.Json.LitJson.JsonData>>::get_Current() */, IEnumerator_1_t1410900363_il2cpp_TypeInfo_var, L_0);
		V_0 = L_1;
		String_t* L_2 = KeyValuePair_2_get_Key_m1021780796((&V_0), /*hidden argument*/KeyValuePair_2_get_Key_m1021780796_MethodInfo_var);
		JsonData_t4263252052 * L_3 = KeyValuePair_2_get_Value_m3387678519((&V_0), /*hidden argument*/KeyValuePair_2_get_Value_m3387678519_MethodInfo_var);
		DictionaryEntry_t3048875398  L_4;
		memset(&L_4, 0, sizeof(L_4));
		DictionaryEntry__ctor_m2901884110(&L_4, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Object ThirdParty.Json.LitJson.OrderedDictionaryEnumerator::get_Key()
extern Il2CppClass* IEnumerator_1_t1410900363_il2cpp_TypeInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m1021780796_MethodInfo_var;
extern const uint32_t OrderedDictionaryEnumerator_get_Key_m3835242549_MetadataUsageId;
extern "C"  Il2CppObject * OrderedDictionaryEnumerator_get_Key_m3835242549 (OrderedDictionaryEnumerator_t1664192327 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OrderedDictionaryEnumerator_get_Key_m3835242549_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t3935376536  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Il2CppObject* L_0 = __this->get_list_enumerator_0();
		NullCheck(L_0);
		KeyValuePair_2_t3935376536  L_1 = InterfaceFuncInvoker0< KeyValuePair_2_t3935376536  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,ThirdParty.Json.LitJson.JsonData>>::get_Current() */, IEnumerator_1_t1410900363_il2cpp_TypeInfo_var, L_0);
		V_0 = L_1;
		String_t* L_2 = KeyValuePair_2_get_Key_m1021780796((&V_0), /*hidden argument*/KeyValuePair_2_get_Key_m1021780796_MethodInfo_var);
		return L_2;
	}
}
// System.Object ThirdParty.Json.LitJson.OrderedDictionaryEnumerator::get_Value()
extern Il2CppClass* IEnumerator_1_t1410900363_il2cpp_TypeInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m3387678519_MethodInfo_var;
extern const uint32_t OrderedDictionaryEnumerator_get_Value_m3178181661_MetadataUsageId;
extern "C"  Il2CppObject * OrderedDictionaryEnumerator_get_Value_m3178181661 (OrderedDictionaryEnumerator_t1664192327 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OrderedDictionaryEnumerator_get_Value_m3178181661_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t3935376536  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Il2CppObject* L_0 = __this->get_list_enumerator_0();
		NullCheck(L_0);
		KeyValuePair_2_t3935376536  L_1 = InterfaceFuncInvoker0< KeyValuePair_2_t3935376536  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,ThirdParty.Json.LitJson.JsonData>>::get_Current() */, IEnumerator_1_t1410900363_il2cpp_TypeInfo_var, L_0);
		V_0 = L_1;
		JsonData_t4263252052 * L_2 = KeyValuePair_2_get_Value_m3387678519((&V_0), /*hidden argument*/KeyValuePair_2_get_Value_m3387678519_MethodInfo_var);
		return L_2;
	}
}
// System.Void ThirdParty.Json.LitJson.OrderedDictionaryEnumerator::.ctor(System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,ThirdParty.Json.LitJson.JsonData>>)
extern "C"  void OrderedDictionaryEnumerator__ctor_m631766896 (OrderedDictionaryEnumerator_t1664192327 * __this, Il2CppObject* ___enumerator0, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___enumerator0;
		__this->set_list_enumerator_0(L_0);
		return;
	}
}
// System.Boolean ThirdParty.Json.LitJson.OrderedDictionaryEnumerator::MoveNext()
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern const uint32_t OrderedDictionaryEnumerator_MoveNext_m1405683412_MetadataUsageId;
extern "C"  bool OrderedDictionaryEnumerator_MoveNext_m1405683412 (OrderedDictionaryEnumerator_t1664192327 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OrderedDictionaryEnumerator_MoveNext_m1405683412_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject* L_0 = __this->get_list_enumerator_0();
		NullCheck(L_0);
		bool L_1 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Void ThirdParty.Json.LitJson.OrderedDictionaryEnumerator::Reset()
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern const uint32_t OrderedDictionaryEnumerator_Reset_m263692563_MetadataUsageId;
extern "C"  void OrderedDictionaryEnumerator_Reset_m263692563 (OrderedDictionaryEnumerator_t1664192327 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OrderedDictionaryEnumerator_Reset_m263692563_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject* L_0 = __this->get_list_enumerator_0();
		NullCheck(L_0);
		InterfaceActionInvoker0::Invoke(2 /* System.Void System.Collections.IEnumerator::Reset() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_0);
		return;
	}
}
// Conversion methods for marshalling of: ThirdParty.Json.LitJson.PropertyMetadata
extern "C" void PropertyMetadata_t3287739986_marshal_pinvoke(const PropertyMetadata_t3287739986& unmarshaled, PropertyMetadata_t3287739986_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___Info_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'Info' of type 'PropertyMetadata': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___Info_0Exception);
}
extern "C" void PropertyMetadata_t3287739986_marshal_pinvoke_back(const PropertyMetadata_t3287739986_marshaled_pinvoke& marshaled, PropertyMetadata_t3287739986& unmarshaled)
{
	Il2CppCodeGenException* ___Info_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'Info' of type 'PropertyMetadata': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___Info_0Exception);
}
// Conversion method for clean up from marshalling of: ThirdParty.Json.LitJson.PropertyMetadata
extern "C" void PropertyMetadata_t3287739986_marshal_pinvoke_cleanup(PropertyMetadata_t3287739986_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: ThirdParty.Json.LitJson.PropertyMetadata
extern "C" void PropertyMetadata_t3287739986_marshal_com(const PropertyMetadata_t3287739986& unmarshaled, PropertyMetadata_t3287739986_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___Info_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'Info' of type 'PropertyMetadata': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___Info_0Exception);
}
extern "C" void PropertyMetadata_t3287739986_marshal_com_back(const PropertyMetadata_t3287739986_marshaled_com& marshaled, PropertyMetadata_t3287739986& unmarshaled)
{
	Il2CppCodeGenException* ___Info_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'Info' of type 'PropertyMetadata': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___Info_0Exception);
}
// Conversion method for clean up from marshalling of: ThirdParty.Json.LitJson.PropertyMetadata
extern "C" void PropertyMetadata_t3287739986_marshal_com_cleanup(PropertyMetadata_t3287739986_marshaled_com& marshaled)
{
}
// System.Void ThirdParty.Json.LitJson.WrapperFactory::.ctor(System.Object,System.IntPtr)
extern "C"  void WrapperFactory__ctor_m632703600 (WrapperFactory_t327905379 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// ThirdParty.Json.LitJson.IJsonWrapper ThirdParty.Json.LitJson.WrapperFactory::Invoke()
extern "C"  Il2CppObject * WrapperFactory_Invoke_m3781398413 (WrapperFactory_t327905379 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		WrapperFactory_Invoke_m3781398413((WrapperFactory_t327905379 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult ThirdParty.Json.LitJson.WrapperFactory::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * WrapperFactory_BeginInvoke_m1687643501 (WrapperFactory_t327905379 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// ThirdParty.Json.LitJson.IJsonWrapper ThirdParty.Json.LitJson.WrapperFactory::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * WrapperFactory_EndInvoke_m1873551427 (WrapperFactory_t327905379 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void ThirdParty.Json.LitJson.WriterContext::.ctor()
extern "C"  void WriterContext__ctor_m2481251725 (WriterContext_t1209007092 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
