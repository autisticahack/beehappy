﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.ConstantClass
struct ConstantClass_t4000559886;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AWSSDK_Core_Amazon_Runtime_ConstantClass4000559886.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void Amazon.Runtime.ConstantClass::.ctor(System.String)
extern "C"  void ConstantClass__ctor_m1376881199 (ConstantClass_t4000559886 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Runtime.ConstantClass::get_Value()
extern "C"  String_t* ConstantClass_get_Value_m297235302 (ConstantClass_t4000559886 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.ConstantClass::set_Value(System.String)
extern "C"  void ConstantClass_set_Value_m3357717205 (ConstantClass_t4000559886 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Runtime.ConstantClass::ToString()
extern "C"  String_t* ConstantClass_ToString_m3150293938 (ConstantClass_t4000559886 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Runtime.ConstantClass::op_Implicit(Amazon.Runtime.ConstantClass)
extern "C"  String_t* ConstantClass_op_Implicit_m868155573 (Il2CppObject * __this /* static, unused */, ConstantClass_t4000559886 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.ConstantClass Amazon.Runtime.ConstantClass::Intern()
extern "C"  ConstantClass_t4000559886 * ConstantClass_Intern_m2056848824 (ConstantClass_t4000559886 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.ConstantClass::LoadFields(System.Type)
extern "C"  void ConstantClass_LoadFields_m2848367005 (Il2CppObject * __this /* static, unused */, Type_t * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Amazon.Runtime.ConstantClass::GetHashCode()
extern "C"  int32_t ConstantClass_GetHashCode_m4164241352 (ConstantClass_t4000559886 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.ConstantClass::Equals(System.Object)
extern "C"  bool ConstantClass_Equals_m3033257948 (ConstantClass_t4000559886 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.ConstantClass::Equals(Amazon.Runtime.ConstantClass)
extern "C"  bool ConstantClass_Equals_m2538934048 (ConstantClass_t4000559886 * __this, ConstantClass_t4000559886 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.ConstantClass::op_Equality(Amazon.Runtime.ConstantClass,Amazon.Runtime.ConstantClass)
extern "C"  bool ConstantClass_op_Equality_m2241424781 (Il2CppObject * __this /* static, unused */, ConstantClass_t4000559886 * ___a0, ConstantClass_t4000559886 * ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.ConstantClass::op_Inequality(Amazon.Runtime.ConstantClass,Amazon.Runtime.ConstantClass)
extern "C"  bool ConstantClass_op_Inequality_m2916729676 (Il2CppObject * __this /* static, unused */, ConstantClass_t4000559886 * ___a0, ConstantClass_t4000559886 * ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.ConstantClass::.cctor()
extern "C"  void ConstantClass__cctor_m643825018 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
