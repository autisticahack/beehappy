﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<Amazon.Runtime.Metric,System.Int64>
struct ValueCollection_t2362216369;
// System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Int64>
struct Dictionary_2_t3659156526;
// System.Collections.Generic.IEnumerator`1<System.Int64>
struct IEnumerator_1_t2679569160;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Int64[]
struct Int64U5BU5D_t717125112;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1050721994.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Amazon.Runtime.Metric,System.Int64>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m2887733138_gshared (ValueCollection_t2362216369 * __this, Dictionary_2_t3659156526 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m2887733138(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t2362216369 *, Dictionary_2_t3659156526 *, const MethodInfo*))ValueCollection__ctor_m2887733138_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Amazon.Runtime.Metric,System.Int64>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3186695692_gshared (ValueCollection_t2362216369 * __this, int64_t ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3186695692(__this, ___item0, method) ((  void (*) (ValueCollection_t2362216369 *, int64_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3186695692_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Amazon.Runtime.Metric,System.Int64>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m549208119_gshared (ValueCollection_t2362216369 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m549208119(__this, method) ((  void (*) (ValueCollection_t2362216369 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m549208119_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Amazon.Runtime.Metric,System.Int64>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m873160972_gshared (ValueCollection_t2362216369 * __this, int64_t ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m873160972(__this, ___item0, method) ((  bool (*) (ValueCollection_t2362216369 *, int64_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m873160972_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Amazon.Runtime.Metric,System.Int64>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3891119087_gshared (ValueCollection_t2362216369 * __this, int64_t ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3891119087(__this, ___item0, method) ((  bool (*) (ValueCollection_t2362216369 *, int64_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3891119087_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<Amazon.Runtime.Metric,System.Int64>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3277928129_gshared (ValueCollection_t2362216369 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3277928129(__this, method) ((  Il2CppObject* (*) (ValueCollection_t2362216369 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3277928129_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Amazon.Runtime.Metric,System.Int64>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m3003853841_gshared (ValueCollection_t2362216369 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m3003853841(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2362216369 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m3003853841_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<Amazon.Runtime.Metric,System.Int64>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m891744880_gshared (ValueCollection_t2362216369 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m891744880(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2362216369 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m891744880_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Amazon.Runtime.Metric,System.Int64>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2598728811_gshared (ValueCollection_t2362216369 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2598728811(__this, method) ((  bool (*) (ValueCollection_t2362216369 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2598728811_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Amazon.Runtime.Metric,System.Int64>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m307686685_gshared (ValueCollection_t2362216369 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m307686685(__this, method) ((  bool (*) (ValueCollection_t2362216369 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m307686685_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<Amazon.Runtime.Metric,System.Int64>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m2981050245_gshared (ValueCollection_t2362216369 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m2981050245(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2362216369 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m2981050245_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Amazon.Runtime.Metric,System.Int64>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m2365403327_gshared (ValueCollection_t2362216369 * __this, Int64U5BU5D_t717125112* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m2365403327(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2362216369 *, Int64U5BU5D_t717125112*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m2365403327_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<Amazon.Runtime.Metric,System.Int64>::GetEnumerator()
extern "C"  Enumerator_t1050721994  ValueCollection_GetEnumerator_m3659315370_gshared (ValueCollection_t2362216369 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m3659315370(__this, method) ((  Enumerator_t1050721994  (*) (ValueCollection_t2362216369 *, const MethodInfo*))ValueCollection_GetEnumerator_m3659315370_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<Amazon.Runtime.Metric,System.Int64>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m1620867361_gshared (ValueCollection_t2362216369 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m1620867361(__this, method) ((  int32_t (*) (ValueCollection_t2362216369 *, const MethodInfo*))ValueCollection_get_Count_m1620867361_gshared)(__this, method)
