﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AWSSDK_Core_Amazon_Runtime_AWSCredentials3583921007.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.AnonymousAWSCredentials
struct  AnonymousAWSCredentials_t3877662854  : public AWSCredentials_t3583921007
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
