﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.Util.Internal.AndroidInterop/<>c__6`1<System.Object>
struct U3CU3Ec__6_1_t2218249864;
// System.Func`2<System.Reflection.MethodInfo,System.Boolean>
struct Func_2_t499475658;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Util.Internal.AndroidInterop/<>c__6`1<System.Object>
struct  U3CU3Ec__6_1_t2218249864  : public Il2CppObject
{
public:

public:
};

struct U3CU3Ec__6_1_t2218249864_StaticFields
{
public:
	// Amazon.Util.Internal.AndroidInterop/<>c__6`1<T> Amazon.Util.Internal.AndroidInterop/<>c__6`1::<>9
	U3CU3Ec__6_1_t2218249864 * ___U3CU3E9_0;
	// System.Func`2<System.Reflection.MethodInfo,System.Boolean> Amazon.Util.Internal.AndroidInterop/<>c__6`1::<>9__6_0
	Func_2_t499475658 * ___U3CU3E9__6_0_1;
	// System.Func`2<System.Reflection.MethodInfo,System.Boolean> Amazon.Util.Internal.AndroidInterop/<>c__6`1::<>9__6_1
	Func_2_t499475658 * ___U3CU3E9__6_1_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__6_1_t2218249864_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec__6_1_t2218249864 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec__6_1_t2218249864 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec__6_1_t2218249864 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9_0, value);
	}

	inline static int32_t get_offset_of_U3CU3E9__6_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__6_1_t2218249864_StaticFields, ___U3CU3E9__6_0_1)); }
	inline Func_2_t499475658 * get_U3CU3E9__6_0_1() const { return ___U3CU3E9__6_0_1; }
	inline Func_2_t499475658 ** get_address_of_U3CU3E9__6_0_1() { return &___U3CU3E9__6_0_1; }
	inline void set_U3CU3E9__6_0_1(Func_2_t499475658 * value)
	{
		___U3CU3E9__6_0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9__6_0_1, value);
	}

	inline static int32_t get_offset_of_U3CU3E9__6_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__6_1_t2218249864_StaticFields, ___U3CU3E9__6_1_2)); }
	inline Func_2_t499475658 * get_U3CU3E9__6_1_2() const { return ___U3CU3E9__6_1_2; }
	inline Func_2_t499475658 ** get_address_of_U3CU3E9__6_1_2() { return &___U3CU3E9__6_1_2; }
	inline void set_U3CU3E9__6_1_2(Func_2_t499475658 * value)
	{
		___U3CU3E9__6_1_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9__6_1_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
