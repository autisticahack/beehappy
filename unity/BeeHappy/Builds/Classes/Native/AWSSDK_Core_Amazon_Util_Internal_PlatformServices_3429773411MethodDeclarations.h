﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Util.Internal.PlatformServices.ApplicationSettings
struct ApplicationSettings_t3429773411;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AWSSDK_Core_Amazon_Util_Internal_PlatformServices_A788768262.h"

// System.Void Amazon.Util.Internal.PlatformServices.ApplicationSettings::SetValue(System.String,System.String,Amazon.Util.Internal.PlatformServices.ApplicationSettingsMode)
extern "C"  void ApplicationSettings_SetValue_m2405348141 (ApplicationSettings_t3429773411 * __this, String_t* ___key0, String_t* ___value1, int32_t ___mode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Util.Internal.PlatformServices.ApplicationSettings::GetValue(System.String,Amazon.Util.Internal.PlatformServices.ApplicationSettingsMode)
extern "C"  String_t* ApplicationSettings_GetValue_m1200660752 (ApplicationSettings_t3429773411 * __this, String_t* ___key0, int32_t ___mode1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Util.Internal.PlatformServices.ApplicationSettings::RemoveValue(System.String,Amazon.Util.Internal.PlatformServices.ApplicationSettingsMode)
extern "C"  void ApplicationSettings_RemoveValue_m553295191 (ApplicationSettings_t3429773411 * __this, String_t* ___key0, int32_t ___mode1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
