﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;

#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Amazo492659996.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.CognitoIdentity.Model.GetIdRequest
struct  GetIdRequest_t4078561340  : public AmazonCognitoIdentityRequest_t492659996
{
public:
	// System.String Amazon.CognitoIdentity.Model.GetIdRequest::_accountId
	String_t* ____accountId_3;
	// System.String Amazon.CognitoIdentity.Model.GetIdRequest::_identityPoolId
	String_t* ____identityPoolId_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> Amazon.CognitoIdentity.Model.GetIdRequest::_logins
	Dictionary_2_t3943999495 * ____logins_5;

public:
	inline static int32_t get_offset_of__accountId_3() { return static_cast<int32_t>(offsetof(GetIdRequest_t4078561340, ____accountId_3)); }
	inline String_t* get__accountId_3() const { return ____accountId_3; }
	inline String_t** get_address_of__accountId_3() { return &____accountId_3; }
	inline void set__accountId_3(String_t* value)
	{
		____accountId_3 = value;
		Il2CppCodeGenWriteBarrier(&____accountId_3, value);
	}

	inline static int32_t get_offset_of__identityPoolId_4() { return static_cast<int32_t>(offsetof(GetIdRequest_t4078561340, ____identityPoolId_4)); }
	inline String_t* get__identityPoolId_4() const { return ____identityPoolId_4; }
	inline String_t** get_address_of__identityPoolId_4() { return &____identityPoolId_4; }
	inline void set__identityPoolId_4(String_t* value)
	{
		____identityPoolId_4 = value;
		Il2CppCodeGenWriteBarrier(&____identityPoolId_4, value);
	}

	inline static int32_t get_offset_of__logins_5() { return static_cast<int32_t>(offsetof(GetIdRequest_t4078561340, ____logins_5)); }
	inline Dictionary_2_t3943999495 * get__logins_5() const { return ____logins_5; }
	inline Dictionary_2_t3943999495 ** get_address_of__logins_5() { return &____logins_5; }
	inline void set__logins_5(Dictionary_2_t3943999495 * value)
	{
		____logins_5 = value;
		Il2CppCodeGenWriteBarrier(&____logins_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
