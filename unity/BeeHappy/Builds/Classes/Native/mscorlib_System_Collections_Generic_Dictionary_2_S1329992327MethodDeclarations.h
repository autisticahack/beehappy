﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>
struct ShimEnumerator_t1329992327;
// System.Collections.Generic.Dictionary`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>
struct Dictionary_2_t1224867506;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m2676277834_gshared (ShimEnumerator_t1329992327 * __this, Dictionary_2_t1224867506 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m2676277834(__this, ___host0, method) ((  void (*) (ShimEnumerator_t1329992327 *, Dictionary_2_t1224867506 *, const MethodInfo*))ShimEnumerator__ctor_m2676277834_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m2738243069_gshared (ShimEnumerator_t1329992327 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m2738243069(__this, method) ((  bool (*) (ShimEnumerator_t1329992327 *, const MethodInfo*))ShimEnumerator_MoveNext_m2738243069_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::get_Entry()
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m2717618457_gshared (ShimEnumerator_t1329992327 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m2717618457(__this, method) ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t1329992327 *, const MethodInfo*))ShimEnumerator_get_Entry_m2717618457_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m4266238612_gshared (ShimEnumerator_t1329992327 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m4266238612(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1329992327 *, const MethodInfo*))ShimEnumerator_get_Key_m4266238612_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m2503482038_gshared (ShimEnumerator_t1329992327 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m2503482038(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1329992327 *, const MethodInfo*))ShimEnumerator_get_Value_m2503482038_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m3392383502_gshared (ShimEnumerator_t1329992327 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m3392383502(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1329992327 *, const MethodInfo*))ShimEnumerator_get_Current_m3392383502_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::Reset()
extern "C"  void ShimEnumerator_Reset_m3134598960_gshared (ShimEnumerator_t1329992327 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m3134598960(__this, method) ((  void (*) (ShimEnumerator_t1329992327 *, const MethodInfo*))ShimEnumerator_Reset_m3134598960_gshared)(__this, method)
