﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"

extern const Il2CppType Il2CppObject_0_0_0;
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0 = { 1, GenInst_Il2CppObject_0_0_0_Types };
extern const Il2CppType Int32_t2071877448_0_0_0;
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Types[] = { &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0 = { 1, GenInst_Int32_t2071877448_0_0_0_Types };
extern const Il2CppType Char_t3454481338_0_0_0;
static const Il2CppType* GenInst_Char_t3454481338_0_0_0_Types[] = { &Char_t3454481338_0_0_0 };
extern const Il2CppGenericInst GenInst_Char_t3454481338_0_0_0 = { 1, GenInst_Char_t3454481338_0_0_0_Types };
extern const Il2CppType IConvertible_t908092482_0_0_0;
static const Il2CppType* GenInst_IConvertible_t908092482_0_0_0_Types[] = { &IConvertible_t908092482_0_0_0 };
extern const Il2CppGenericInst GenInst_IConvertible_t908092482_0_0_0 = { 1, GenInst_IConvertible_t908092482_0_0_0_Types };
extern const Il2CppType IComparable_t1857082765_0_0_0;
static const Il2CppType* GenInst_IComparable_t1857082765_0_0_0_Types[] = { &IComparable_t1857082765_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_t1857082765_0_0_0 = { 1, GenInst_IComparable_t1857082765_0_0_0_Types };
extern const Il2CppType IComparable_1_t991353265_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t991353265_0_0_0_Types[] = { &IComparable_1_t991353265_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t991353265_0_0_0 = { 1, GenInst_IComparable_1_t991353265_0_0_0_Types };
extern const Il2CppType IEquatable_1_t1363496211_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t1363496211_0_0_0_Types[] = { &IEquatable_1_t1363496211_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1363496211_0_0_0 = { 1, GenInst_IEquatable_1_t1363496211_0_0_0_Types };
extern const Il2CppType ValueType_t3507792607_0_0_0;
static const Il2CppType* GenInst_ValueType_t3507792607_0_0_0_Types[] = { &ValueType_t3507792607_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueType_t3507792607_0_0_0 = { 1, GenInst_ValueType_t3507792607_0_0_0_Types };
extern const Il2CppType Int64_t909078037_0_0_0;
static const Il2CppType* GenInst_Int64_t909078037_0_0_0_Types[] = { &Int64_t909078037_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t909078037_0_0_0 = { 1, GenInst_Int64_t909078037_0_0_0_Types };
extern const Il2CppType UInt32_t2149682021_0_0_0;
static const Il2CppType* GenInst_UInt32_t2149682021_0_0_0_Types[] = { &UInt32_t2149682021_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt32_t2149682021_0_0_0 = { 1, GenInst_UInt32_t2149682021_0_0_0_Types };
extern const Il2CppType UInt64_t2909196914_0_0_0;
static const Il2CppType* GenInst_UInt64_t2909196914_0_0_0_Types[] = { &UInt64_t2909196914_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt64_t2909196914_0_0_0 = { 1, GenInst_UInt64_t2909196914_0_0_0_Types };
extern const Il2CppType Byte_t3683104436_0_0_0;
static const Il2CppType* GenInst_Byte_t3683104436_0_0_0_Types[] = { &Byte_t3683104436_0_0_0 };
extern const Il2CppGenericInst GenInst_Byte_t3683104436_0_0_0 = { 1, GenInst_Byte_t3683104436_0_0_0_Types };
extern const Il2CppType SByte_t454417549_0_0_0;
static const Il2CppType* GenInst_SByte_t454417549_0_0_0_Types[] = { &SByte_t454417549_0_0_0 };
extern const Il2CppGenericInst GenInst_SByte_t454417549_0_0_0 = { 1, GenInst_SByte_t454417549_0_0_0_Types };
extern const Il2CppType Int16_t4041245914_0_0_0;
static const Il2CppType* GenInst_Int16_t4041245914_0_0_0_Types[] = { &Int16_t4041245914_0_0_0 };
extern const Il2CppGenericInst GenInst_Int16_t4041245914_0_0_0 = { 1, GenInst_Int16_t4041245914_0_0_0_Types };
extern const Il2CppType UInt16_t986882611_0_0_0;
static const Il2CppType* GenInst_UInt16_t986882611_0_0_0_Types[] = { &UInt16_t986882611_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt16_t986882611_0_0_0 = { 1, GenInst_UInt16_t986882611_0_0_0_Types };
extern const Il2CppType String_t_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_Types[] = { &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0 = { 1, GenInst_String_t_0_0_0_Types };
extern const Il2CppType IEnumerable_t2911409499_0_0_0;
static const Il2CppType* GenInst_IEnumerable_t2911409499_0_0_0_Types[] = { &IEnumerable_t2911409499_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_t2911409499_0_0_0 = { 1, GenInst_IEnumerable_t2911409499_0_0_0_Types };
extern const Il2CppType ICloneable_t3853279282_0_0_0;
static const Il2CppType* GenInst_ICloneable_t3853279282_0_0_0_Types[] = { &ICloneable_t3853279282_0_0_0 };
extern const Il2CppGenericInst GenInst_ICloneable_t3853279282_0_0_0 = { 1, GenInst_ICloneable_t3853279282_0_0_0_Types };
extern const Il2CppType IComparable_1_t3861059456_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3861059456_0_0_0_Types[] = { &IComparable_1_t3861059456_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3861059456_0_0_0 = { 1, GenInst_IComparable_1_t3861059456_0_0_0_Types };
extern const Il2CppType IEquatable_1_t4233202402_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t4233202402_0_0_0_Types[] = { &IEquatable_1_t4233202402_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t4233202402_0_0_0 = { 1, GenInst_IEquatable_1_t4233202402_0_0_0_Types };
extern const Il2CppType Type_t_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_Types[] = { &Type_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0 = { 1, GenInst_Type_t_0_0_0_Types };
extern const Il2CppType IReflect_t3412036974_0_0_0;
static const Il2CppType* GenInst_IReflect_t3412036974_0_0_0_Types[] = { &IReflect_t3412036974_0_0_0 };
extern const Il2CppGenericInst GenInst_IReflect_t3412036974_0_0_0 = { 1, GenInst_IReflect_t3412036974_0_0_0_Types };
extern const Il2CppType _Type_t102776839_0_0_0;
static const Il2CppType* GenInst__Type_t102776839_0_0_0_Types[] = { &_Type_t102776839_0_0_0 };
extern const Il2CppGenericInst GenInst__Type_t102776839_0_0_0 = { 1, GenInst__Type_t102776839_0_0_0_Types };
extern const Il2CppType MemberInfo_t_0_0_0;
static const Il2CppType* GenInst_MemberInfo_t_0_0_0_Types[] = { &MemberInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MemberInfo_t_0_0_0 = { 1, GenInst_MemberInfo_t_0_0_0_Types };
extern const Il2CppType ICustomAttributeProvider_t502202687_0_0_0;
static const Il2CppType* GenInst_ICustomAttributeProvider_t502202687_0_0_0_Types[] = { &ICustomAttributeProvider_t502202687_0_0_0 };
extern const Il2CppGenericInst GenInst_ICustomAttributeProvider_t502202687_0_0_0 = { 1, GenInst_ICustomAttributeProvider_t502202687_0_0_0_Types };
extern const Il2CppType _MemberInfo_t332722161_0_0_0;
static const Il2CppType* GenInst__MemberInfo_t332722161_0_0_0_Types[] = { &_MemberInfo_t332722161_0_0_0 };
extern const Il2CppGenericInst GenInst__MemberInfo_t332722161_0_0_0 = { 1, GenInst__MemberInfo_t332722161_0_0_0_Types };
extern const Il2CppType IFormattable_t1523031934_0_0_0;
static const Il2CppType* GenInst_IFormattable_t1523031934_0_0_0_Types[] = { &IFormattable_t1523031934_0_0_0 };
extern const Il2CppGenericInst GenInst_IFormattable_t1523031934_0_0_0 = { 1, GenInst_IFormattable_t1523031934_0_0_0_Types };
extern const Il2CppType IComparable_1_t3903716671_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3903716671_0_0_0_Types[] = { &IComparable_1_t3903716671_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3903716671_0_0_0 = { 1, GenInst_IComparable_1_t3903716671_0_0_0_Types };
extern const Il2CppType IEquatable_1_t4275859617_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t4275859617_0_0_0_Types[] = { &IEquatable_1_t4275859617_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t4275859617_0_0_0 = { 1, GenInst_IEquatable_1_t4275859617_0_0_0_Types };
extern const Il2CppType Double_t4078015681_0_0_0;
static const Il2CppType* GenInst_Double_t4078015681_0_0_0_Types[] = { &Double_t4078015681_0_0_0 };
extern const Il2CppGenericInst GenInst_Double_t4078015681_0_0_0 = { 1, GenInst_Double_t4078015681_0_0_0_Types };
extern const Il2CppType IComparable_1_t1614887608_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1614887608_0_0_0_Types[] = { &IComparable_1_t1614887608_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1614887608_0_0_0 = { 1, GenInst_IComparable_1_t1614887608_0_0_0_Types };
extern const Il2CppType IEquatable_1_t1987030554_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t1987030554_0_0_0_Types[] = { &IEquatable_1_t1987030554_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1987030554_0_0_0 = { 1, GenInst_IEquatable_1_t1987030554_0_0_0_Types };
extern const Il2CppType IComparable_1_t3981521244_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3981521244_0_0_0_Types[] = { &IComparable_1_t3981521244_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3981521244_0_0_0 = { 1, GenInst_IComparable_1_t3981521244_0_0_0_Types };
extern const Il2CppType IEquatable_1_t58696894_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t58696894_0_0_0_Types[] = { &IEquatable_1_t58696894_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t58696894_0_0_0 = { 1, GenInst_IEquatable_1_t58696894_0_0_0_Types };
extern const Il2CppType IComparable_1_t1219976363_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1219976363_0_0_0_Types[] = { &IComparable_1_t1219976363_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1219976363_0_0_0 = { 1, GenInst_IComparable_1_t1219976363_0_0_0_Types };
extern const Il2CppType IEquatable_1_t1592119309_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t1592119309_0_0_0_Types[] = { &IEquatable_1_t1592119309_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1592119309_0_0_0 = { 1, GenInst_IEquatable_1_t1592119309_0_0_0_Types };
extern const Il2CppType Single_t2076509932_0_0_0;
static const Il2CppType* GenInst_Single_t2076509932_0_0_0_Types[] = { &Single_t2076509932_0_0_0 };
extern const Il2CppGenericInst GenInst_Single_t2076509932_0_0_0 = { 1, GenInst_Single_t2076509932_0_0_0_Types };
extern const Il2CppType IComparable_1_t3908349155_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3908349155_0_0_0_Types[] = { &IComparable_1_t3908349155_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3908349155_0_0_0 = { 1, GenInst_IComparable_1_t3908349155_0_0_0_Types };
extern const Il2CppType IEquatable_1_t4280492101_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t4280492101_0_0_0_Types[] = { &IEquatable_1_t4280492101_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t4280492101_0_0_0 = { 1, GenInst_IEquatable_1_t4280492101_0_0_0_Types };
extern const Il2CppType Decimal_t724701077_0_0_0;
static const Il2CppType* GenInst_Decimal_t724701077_0_0_0_Types[] = { &Decimal_t724701077_0_0_0 };
extern const Il2CppGenericInst GenInst_Decimal_t724701077_0_0_0 = { 1, GenInst_Decimal_t724701077_0_0_0_Types };
extern const Il2CppType Boolean_t3825574718_0_0_0;
static const Il2CppType* GenInst_Boolean_t3825574718_0_0_0_Types[] = { &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t3825574718_0_0_0 = { 1, GenInst_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType Delegate_t3022476291_0_0_0;
static const Il2CppType* GenInst_Delegate_t3022476291_0_0_0_Types[] = { &Delegate_t3022476291_0_0_0 };
extern const Il2CppGenericInst GenInst_Delegate_t3022476291_0_0_0 = { 1, GenInst_Delegate_t3022476291_0_0_0_Types };
extern const Il2CppType ISerializable_t1245643778_0_0_0;
static const Il2CppType* GenInst_ISerializable_t1245643778_0_0_0_Types[] = { &ISerializable_t1245643778_0_0_0 };
extern const Il2CppGenericInst GenInst_ISerializable_t1245643778_0_0_0 = { 1, GenInst_ISerializable_t1245643778_0_0_0_Types };
extern const Il2CppType ParameterInfo_t2249040075_0_0_0;
static const Il2CppType* GenInst_ParameterInfo_t2249040075_0_0_0_Types[] = { &ParameterInfo_t2249040075_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterInfo_t2249040075_0_0_0 = { 1, GenInst_ParameterInfo_t2249040075_0_0_0_Types };
extern const Il2CppType _ParameterInfo_t470209990_0_0_0;
static const Il2CppType* GenInst__ParameterInfo_t470209990_0_0_0_Types[] = { &_ParameterInfo_t470209990_0_0_0 };
extern const Il2CppGenericInst GenInst__ParameterInfo_t470209990_0_0_0 = { 1, GenInst__ParameterInfo_t470209990_0_0_0_Types };
extern const Il2CppType ParameterModifier_t1820634920_0_0_0;
static const Il2CppType* GenInst_ParameterModifier_t1820634920_0_0_0_Types[] = { &ParameterModifier_t1820634920_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterModifier_t1820634920_0_0_0 = { 1, GenInst_ParameterModifier_t1820634920_0_0_0_Types };
extern const Il2CppType IComparable_1_t2818721834_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t2818721834_0_0_0_Types[] = { &IComparable_1_t2818721834_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t2818721834_0_0_0 = { 1, GenInst_IComparable_1_t2818721834_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3190864780_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3190864780_0_0_0_Types[] = { &IEquatable_1_t3190864780_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3190864780_0_0_0 = { 1, GenInst_IEquatable_1_t3190864780_0_0_0_Types };
extern const Il2CppType IComparable_1_t446068841_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t446068841_0_0_0_Types[] = { &IComparable_1_t446068841_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t446068841_0_0_0 = { 1, GenInst_IComparable_1_t446068841_0_0_0_Types };
extern const Il2CppType IEquatable_1_t818211787_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t818211787_0_0_0_Types[] = { &IEquatable_1_t818211787_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t818211787_0_0_0 = { 1, GenInst_IEquatable_1_t818211787_0_0_0_Types };
extern const Il2CppType IComparable_1_t1578117841_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1578117841_0_0_0_Types[] = { &IComparable_1_t1578117841_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1578117841_0_0_0 = { 1, GenInst_IComparable_1_t1578117841_0_0_0_Types };
extern const Il2CppType IEquatable_1_t1950260787_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t1950260787_0_0_0_Types[] = { &IEquatable_1_t1950260787_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1950260787_0_0_0 = { 1, GenInst_IEquatable_1_t1950260787_0_0_0_Types };
extern const Il2CppType IComparable_1_t2286256772_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t2286256772_0_0_0_Types[] = { &IComparable_1_t2286256772_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t2286256772_0_0_0 = { 1, GenInst_IComparable_1_t2286256772_0_0_0_Types };
extern const Il2CppType IEquatable_1_t2658399718_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t2658399718_0_0_0_Types[] = { &IEquatable_1_t2658399718_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2658399718_0_0_0 = { 1, GenInst_IEquatable_1_t2658399718_0_0_0_Types };
extern const Il2CppType IComparable_1_t2740917260_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t2740917260_0_0_0_Types[] = { &IComparable_1_t2740917260_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t2740917260_0_0_0 = { 1, GenInst_IComparable_1_t2740917260_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3113060206_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3113060206_0_0_0_Types[] = { &IEquatable_1_t3113060206_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3113060206_0_0_0 = { 1, GenInst_IEquatable_1_t3113060206_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType FieldInfo_t_0_0_0;
static const Il2CppType* GenInst_FieldInfo_t_0_0_0_Types[] = { &FieldInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_FieldInfo_t_0_0_0 = { 1, GenInst_FieldInfo_t_0_0_0_Types };
extern const Il2CppType _FieldInfo_t2511231167_0_0_0;
static const Il2CppType* GenInst__FieldInfo_t2511231167_0_0_0_Types[] = { &_FieldInfo_t2511231167_0_0_0 };
extern const Il2CppGenericInst GenInst__FieldInfo_t2511231167_0_0_0 = { 1, GenInst__FieldInfo_t2511231167_0_0_0_Types };
extern const Il2CppType MethodInfo_t_0_0_0;
static const Il2CppType* GenInst_MethodInfo_t_0_0_0_Types[] = { &MethodInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodInfo_t_0_0_0 = { 1, GenInst_MethodInfo_t_0_0_0_Types };
extern const Il2CppType _MethodInfo_t3642518830_0_0_0;
static const Il2CppType* GenInst__MethodInfo_t3642518830_0_0_0_Types[] = { &_MethodInfo_t3642518830_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodInfo_t3642518830_0_0_0 = { 1, GenInst__MethodInfo_t3642518830_0_0_0_Types };
extern const Il2CppType MethodBase_t904190842_0_0_0;
static const Il2CppType* GenInst_MethodBase_t904190842_0_0_0_Types[] = { &MethodBase_t904190842_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodBase_t904190842_0_0_0 = { 1, GenInst_MethodBase_t904190842_0_0_0_Types };
extern const Il2CppType _MethodBase_t1935530873_0_0_0;
static const Il2CppType* GenInst__MethodBase_t1935530873_0_0_0_Types[] = { &_MethodBase_t1935530873_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodBase_t1935530873_0_0_0 = { 1, GenInst__MethodBase_t1935530873_0_0_0_Types };
extern const Il2CppType PropertyInfo_t_0_0_0;
static const Il2CppType* GenInst_PropertyInfo_t_0_0_0_Types[] = { &PropertyInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_PropertyInfo_t_0_0_0 = { 1, GenInst_PropertyInfo_t_0_0_0_Types };
extern const Il2CppType _PropertyInfo_t1567586598_0_0_0;
static const Il2CppType* GenInst__PropertyInfo_t1567586598_0_0_0_Types[] = { &_PropertyInfo_t1567586598_0_0_0 };
extern const Il2CppGenericInst GenInst__PropertyInfo_t1567586598_0_0_0 = { 1, GenInst__PropertyInfo_t1567586598_0_0_0_Types };
extern const Il2CppType ConstructorInfo_t2851816542_0_0_0;
static const Il2CppType* GenInst_ConstructorInfo_t2851816542_0_0_0_Types[] = { &ConstructorInfo_t2851816542_0_0_0 };
extern const Il2CppGenericInst GenInst_ConstructorInfo_t2851816542_0_0_0 = { 1, GenInst_ConstructorInfo_t2851816542_0_0_0_Types };
extern const Il2CppType _ConstructorInfo_t3269099341_0_0_0;
static const Il2CppType* GenInst__ConstructorInfo_t3269099341_0_0_0_Types[] = { &_ConstructorInfo_t3269099341_0_0_0 };
extern const Il2CppGenericInst GenInst__ConstructorInfo_t3269099341_0_0_0 = { 1, GenInst__ConstructorInfo_t3269099341_0_0_0_Types };
extern const Il2CppType IntPtr_t_0_0_0;
static const Il2CppType* GenInst_IntPtr_t_0_0_0_Types[] = { &IntPtr_t_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0 = { 1, GenInst_IntPtr_t_0_0_0_Types };
extern const Il2CppType TableRange_t2011406615_0_0_0;
static const Il2CppType* GenInst_TableRange_t2011406615_0_0_0_Types[] = { &TableRange_t2011406615_0_0_0 };
extern const Il2CppGenericInst GenInst_TableRange_t2011406615_0_0_0 = { 1, GenInst_TableRange_t2011406615_0_0_0_Types };
extern const Il2CppType TailoringInfo_t1449609243_0_0_0;
static const Il2CppType* GenInst_TailoringInfo_t1449609243_0_0_0_Types[] = { &TailoringInfo_t1449609243_0_0_0 };
extern const Il2CppGenericInst GenInst_TailoringInfo_t1449609243_0_0_0 = { 1, GenInst_TailoringInfo_t1449609243_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &String_t_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3716250094_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3716250094_0_0_0_Types[] = { &KeyValuePair_2_t3716250094_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3716250094_0_0_0 = { 1, GenInst_KeyValuePair_2_t3716250094_0_0_0_Types };
extern const Il2CppType Link_t2723257478_0_0_0;
static const Il2CppType* GenInst_Link_t2723257478_0_0_0_Types[] = { &Link_t2723257478_0_0_0 };
extern const Il2CppGenericInst GenInst_Link_t2723257478_0_0_0 = { 1, GenInst_Link_t2723257478_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int32_t2071877448_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int32_t2071877448_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types };
extern const Il2CppType DictionaryEntry_t3048875398_0_0_0;
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int32_t2071877448_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_DictionaryEntry_t3048875398_0_0_0_Types[] = { &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_DictionaryEntry_t3048875398_0_0_0 = { 1, GenInst_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3716250094_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int32_t2071877448_0_0_0, &KeyValuePair_2_t3716250094_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3716250094_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3716250094_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &Int32_t2071877448_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1744001932_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1744001932_0_0_0_Types[] = { &KeyValuePair_2_t1744001932_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1744001932_0_0_0 = { 1, GenInst_KeyValuePair_2_t1744001932_0_0_0_Types };
extern const Il2CppType Contraction_t1673853792_0_0_0;
static const Il2CppType* GenInst_Contraction_t1673853792_0_0_0_Types[] = { &Contraction_t1673853792_0_0_0 };
extern const Il2CppGenericInst GenInst_Contraction_t1673853792_0_0_0 = { 1, GenInst_Contraction_t1673853792_0_0_0_Types };
extern const Il2CppType Level2Map_t3322505726_0_0_0;
static const Il2CppType* GenInst_Level2Map_t3322505726_0_0_0_Types[] = { &Level2Map_t3322505726_0_0_0 };
extern const Il2CppGenericInst GenInst_Level2Map_t3322505726_0_0_0 = { 1, GenInst_Level2Map_t3322505726_0_0_0_Types };
extern const Il2CppType BigInteger_t925946152_0_0_0;
static const Il2CppType* GenInst_BigInteger_t925946152_0_0_0_Types[] = { &BigInteger_t925946152_0_0_0 };
extern const Il2CppGenericInst GenInst_BigInteger_t925946152_0_0_0 = { 1, GenInst_BigInteger_t925946152_0_0_0_Types };
extern const Il2CppType KeySizes_t3144736271_0_0_0;
static const Il2CppType* GenInst_KeySizes_t3144736271_0_0_0_Types[] = { &KeySizes_t3144736271_0_0_0 };
extern const Il2CppGenericInst GenInst_KeySizes_t3144736271_0_0_0 = { 1, GenInst_KeySizes_t3144736271_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t38854645_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t38854645_0_0_0_Types[] = { &KeyValuePair_2_t38854645_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t38854645_0_0_0 = { 1, GenInst_KeyValuePair_2_t38854645_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t38854645_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t38854645_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t38854645_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t38854645_0_0_0_Types };
extern const Il2CppType IComparable_1_t1362446645_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1362446645_0_0_0_Types[] = { &IComparable_1_t1362446645_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1362446645_0_0_0 = { 1, GenInst_IComparable_1_t1362446645_0_0_0_Types };
extern const Il2CppType IEquatable_1_t1734589591_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t1734589591_0_0_0_Types[] = { &IEquatable_1_t1734589591_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1734589591_0_0_0 = { 1, GenInst_IEquatable_1_t1734589591_0_0_0_Types };
extern const Il2CppType Slot_t2022531261_0_0_0;
static const Il2CppType* GenInst_Slot_t2022531261_0_0_0_Types[] = { &Slot_t2022531261_0_0_0 };
extern const Il2CppGenericInst GenInst_Slot_t2022531261_0_0_0 = { 1, GenInst_Slot_t2022531261_0_0_0_Types };
extern const Il2CppType Slot_t2267560602_0_0_0;
static const Il2CppType* GenInst_Slot_t2267560602_0_0_0_Types[] = { &Slot_t2267560602_0_0_0 };
extern const Il2CppGenericInst GenInst_Slot_t2267560602_0_0_0 = { 1, GenInst_Slot_t2267560602_0_0_0_Types };
extern const Il2CppType StackFrame_t2050294881_0_0_0;
static const Il2CppType* GenInst_StackFrame_t2050294881_0_0_0_Types[] = { &StackFrame_t2050294881_0_0_0 };
extern const Il2CppGenericInst GenInst_StackFrame_t2050294881_0_0_0 = { 1, GenInst_StackFrame_t2050294881_0_0_0_Types };
extern const Il2CppType Calendar_t585061108_0_0_0;
static const Il2CppType* GenInst_Calendar_t585061108_0_0_0_Types[] = { &Calendar_t585061108_0_0_0 };
extern const Il2CppGenericInst GenInst_Calendar_t585061108_0_0_0 = { 1, GenInst_Calendar_t585061108_0_0_0_Types };
extern const Il2CppType ModuleBuilder_t4156028127_0_0_0;
static const Il2CppType* GenInst_ModuleBuilder_t4156028127_0_0_0_Types[] = { &ModuleBuilder_t4156028127_0_0_0 };
extern const Il2CppGenericInst GenInst_ModuleBuilder_t4156028127_0_0_0 = { 1, GenInst_ModuleBuilder_t4156028127_0_0_0_Types };
extern const Il2CppType _ModuleBuilder_t1075102050_0_0_0;
static const Il2CppType* GenInst__ModuleBuilder_t1075102050_0_0_0_Types[] = { &_ModuleBuilder_t1075102050_0_0_0 };
extern const Il2CppGenericInst GenInst__ModuleBuilder_t1075102050_0_0_0 = { 1, GenInst__ModuleBuilder_t1075102050_0_0_0_Types };
extern const Il2CppType Module_t4282841206_0_0_0;
static const Il2CppType* GenInst_Module_t4282841206_0_0_0_Types[] = { &Module_t4282841206_0_0_0 };
extern const Il2CppGenericInst GenInst_Module_t4282841206_0_0_0 = { 1, GenInst_Module_t4282841206_0_0_0_Types };
extern const Il2CppType _Module_t2144668161_0_0_0;
static const Il2CppType* GenInst__Module_t2144668161_0_0_0_Types[] = { &_Module_t2144668161_0_0_0 };
extern const Il2CppGenericInst GenInst__Module_t2144668161_0_0_0 = { 1, GenInst__Module_t2144668161_0_0_0_Types };
extern const Il2CppType MonoResource_t3127387157_0_0_0;
static const Il2CppType* GenInst_MonoResource_t3127387157_0_0_0_Types[] = { &MonoResource_t3127387157_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoResource_t3127387157_0_0_0 = { 1, GenInst_MonoResource_t3127387157_0_0_0_Types };
extern const Il2CppType ParameterBuilder_t3344728474_0_0_0;
static const Il2CppType* GenInst_ParameterBuilder_t3344728474_0_0_0_Types[] = { &ParameterBuilder_t3344728474_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterBuilder_t3344728474_0_0_0 = { 1, GenInst_ParameterBuilder_t3344728474_0_0_0_Types };
extern const Il2CppType _ParameterBuilder_t2251638747_0_0_0;
static const Il2CppType* GenInst__ParameterBuilder_t2251638747_0_0_0_Types[] = { &_ParameterBuilder_t2251638747_0_0_0 };
extern const Il2CppGenericInst GenInst__ParameterBuilder_t2251638747_0_0_0 = { 1, GenInst__ParameterBuilder_t2251638747_0_0_0_Types };
extern const Il2CppType TypeU5BU5D_t1664964607_0_0_0;
static const Il2CppType* GenInst_TypeU5BU5D_t1664964607_0_0_0_Types[] = { &TypeU5BU5D_t1664964607_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeU5BU5D_t1664964607_0_0_0 = { 1, GenInst_TypeU5BU5D_t1664964607_0_0_0_Types };
extern const Il2CppType Il2CppArray_0_0_0;
static const Il2CppType* GenInst_Il2CppArray_0_0_0_Types[] = { &Il2CppArray_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppArray_0_0_0 = { 1, GenInst_Il2CppArray_0_0_0_Types };
extern const Il2CppType ICollection_t91669223_0_0_0;
static const Il2CppType* GenInst_ICollection_t91669223_0_0_0_Types[] = { &ICollection_t91669223_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_t91669223_0_0_0 = { 1, GenInst_ICollection_t91669223_0_0_0_Types };
extern const Il2CppType IList_t3321498491_0_0_0;
static const Il2CppType* GenInst_IList_t3321498491_0_0_0_Types[] = { &IList_t3321498491_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_t3321498491_0_0_0 = { 1, GenInst_IList_t3321498491_0_0_0_Types };
extern const Il2CppType LocalBuilder_t2116499186_0_0_0;
static const Il2CppType* GenInst_LocalBuilder_t2116499186_0_0_0_Types[] = { &LocalBuilder_t2116499186_0_0_0 };
extern const Il2CppGenericInst GenInst_LocalBuilder_t2116499186_0_0_0 = { 1, GenInst_LocalBuilder_t2116499186_0_0_0_Types };
extern const Il2CppType _LocalBuilder_t61912499_0_0_0;
static const Il2CppType* GenInst__LocalBuilder_t61912499_0_0_0_Types[] = { &_LocalBuilder_t61912499_0_0_0 };
extern const Il2CppGenericInst GenInst__LocalBuilder_t61912499_0_0_0 = { 1, GenInst__LocalBuilder_t61912499_0_0_0_Types };
extern const Il2CppType LocalVariableInfo_t1749284021_0_0_0;
static const Il2CppType* GenInst_LocalVariableInfo_t1749284021_0_0_0_Types[] = { &LocalVariableInfo_t1749284021_0_0_0 };
extern const Il2CppGenericInst GenInst_LocalVariableInfo_t1749284021_0_0_0 = { 1, GenInst_LocalVariableInfo_t1749284021_0_0_0_Types };
extern const Il2CppType ILTokenInfo_t149559338_0_0_0;
static const Il2CppType* GenInst_ILTokenInfo_t149559338_0_0_0_Types[] = { &ILTokenInfo_t149559338_0_0_0 };
extern const Il2CppGenericInst GenInst_ILTokenInfo_t149559338_0_0_0 = { 1, GenInst_ILTokenInfo_t149559338_0_0_0_Types };
extern const Il2CppType LabelData_t3712112744_0_0_0;
static const Il2CppType* GenInst_LabelData_t3712112744_0_0_0_Types[] = { &LabelData_t3712112744_0_0_0 };
extern const Il2CppGenericInst GenInst_LabelData_t3712112744_0_0_0 = { 1, GenInst_LabelData_t3712112744_0_0_0_Types };
extern const Il2CppType LabelFixup_t4090909514_0_0_0;
static const Il2CppType* GenInst_LabelFixup_t4090909514_0_0_0_Types[] = { &LabelFixup_t4090909514_0_0_0 };
extern const Il2CppGenericInst GenInst_LabelFixup_t4090909514_0_0_0 = { 1, GenInst_LabelFixup_t4090909514_0_0_0_Types };
extern const Il2CppType GenericTypeParameterBuilder_t1370236603_0_0_0;
static const Il2CppType* GenInst_GenericTypeParameterBuilder_t1370236603_0_0_0_Types[] = { &GenericTypeParameterBuilder_t1370236603_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericTypeParameterBuilder_t1370236603_0_0_0 = { 1, GenInst_GenericTypeParameterBuilder_t1370236603_0_0_0_Types };
extern const Il2CppType TypeBuilder_t3308873219_0_0_0;
static const Il2CppType* GenInst_TypeBuilder_t3308873219_0_0_0_Types[] = { &TypeBuilder_t3308873219_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeBuilder_t3308873219_0_0_0 = { 1, GenInst_TypeBuilder_t3308873219_0_0_0_Types };
extern const Il2CppType _TypeBuilder_t2783404358_0_0_0;
static const Il2CppType* GenInst__TypeBuilder_t2783404358_0_0_0_Types[] = { &_TypeBuilder_t2783404358_0_0_0 };
extern const Il2CppGenericInst GenInst__TypeBuilder_t2783404358_0_0_0 = { 1, GenInst__TypeBuilder_t2783404358_0_0_0_Types };
extern const Il2CppType MethodBuilder_t644187984_0_0_0;
static const Il2CppType* GenInst_MethodBuilder_t644187984_0_0_0_Types[] = { &MethodBuilder_t644187984_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodBuilder_t644187984_0_0_0 = { 1, GenInst_MethodBuilder_t644187984_0_0_0_Types };
extern const Il2CppType _MethodBuilder_t3932949077_0_0_0;
static const Il2CppType* GenInst__MethodBuilder_t3932949077_0_0_0_Types[] = { &_MethodBuilder_t3932949077_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodBuilder_t3932949077_0_0_0 = { 1, GenInst__MethodBuilder_t3932949077_0_0_0_Types };
extern const Il2CppType ConstructorBuilder_t700974433_0_0_0;
static const Il2CppType* GenInst_ConstructorBuilder_t700974433_0_0_0_Types[] = { &ConstructorBuilder_t700974433_0_0_0 };
extern const Il2CppGenericInst GenInst_ConstructorBuilder_t700974433_0_0_0 = { 1, GenInst_ConstructorBuilder_t700974433_0_0_0_Types };
extern const Il2CppType _ConstructorBuilder_t1236878896_0_0_0;
static const Il2CppType* GenInst__ConstructorBuilder_t1236878896_0_0_0_Types[] = { &_ConstructorBuilder_t1236878896_0_0_0 };
extern const Il2CppGenericInst GenInst__ConstructorBuilder_t1236878896_0_0_0 = { 1, GenInst__ConstructorBuilder_t1236878896_0_0_0_Types };
extern const Il2CppType PropertyBuilder_t3694255912_0_0_0;
static const Il2CppType* GenInst_PropertyBuilder_t3694255912_0_0_0_Types[] = { &PropertyBuilder_t3694255912_0_0_0 };
extern const Il2CppGenericInst GenInst_PropertyBuilder_t3694255912_0_0_0 = { 1, GenInst_PropertyBuilder_t3694255912_0_0_0_Types };
extern const Il2CppType _PropertyBuilder_t3341912621_0_0_0;
static const Il2CppType* GenInst__PropertyBuilder_t3341912621_0_0_0_Types[] = { &_PropertyBuilder_t3341912621_0_0_0 };
extern const Il2CppGenericInst GenInst__PropertyBuilder_t3341912621_0_0_0 = { 1, GenInst__PropertyBuilder_t3341912621_0_0_0_Types };
extern const Il2CppType FieldBuilder_t2784804005_0_0_0;
static const Il2CppType* GenInst_FieldBuilder_t2784804005_0_0_0_Types[] = { &FieldBuilder_t2784804005_0_0_0 };
extern const Il2CppGenericInst GenInst_FieldBuilder_t2784804005_0_0_0 = { 1, GenInst_FieldBuilder_t2784804005_0_0_0_Types };
extern const Il2CppType _FieldBuilder_t1895266044_0_0_0;
static const Il2CppType* GenInst__FieldBuilder_t1895266044_0_0_0_Types[] = { &_FieldBuilder_t1895266044_0_0_0 };
extern const Il2CppGenericInst GenInst__FieldBuilder_t1895266044_0_0_0 = { 1, GenInst__FieldBuilder_t1895266044_0_0_0_Types };
extern const Il2CppType AssemblyName_t894705941_0_0_0;
static const Il2CppType* GenInst_AssemblyName_t894705941_0_0_0_Types[] = { &AssemblyName_t894705941_0_0_0 };
extern const Il2CppGenericInst GenInst_AssemblyName_t894705941_0_0_0 = { 1, GenInst_AssemblyName_t894705941_0_0_0_Types };
extern const Il2CppType _AssemblyName_t582342236_0_0_0;
static const Il2CppType* GenInst__AssemblyName_t582342236_0_0_0_Types[] = { &_AssemblyName_t582342236_0_0_0 };
extern const Il2CppGenericInst GenInst__AssemblyName_t582342236_0_0_0 = { 1, GenInst__AssemblyName_t582342236_0_0_0_Types };
extern const Il2CppType IDeserializationCallback_t327125377_0_0_0;
static const Il2CppType* GenInst_IDeserializationCallback_t327125377_0_0_0_Types[] = { &IDeserializationCallback_t327125377_0_0_0 };
extern const Il2CppGenericInst GenInst_IDeserializationCallback_t327125377_0_0_0 = { 1, GenInst_IDeserializationCallback_t327125377_0_0_0_Types };
extern const Il2CppType CustomAttributeTypedArgument_t1498197914_0_0_0;
static const Il2CppType* GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0_Types[] = { &CustomAttributeTypedArgument_t1498197914_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0 = { 1, GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0_Types };
extern const Il2CppType CustomAttributeNamedArgument_t94157543_0_0_0;
static const Il2CppType* GenInst_CustomAttributeNamedArgument_t94157543_0_0_0_Types[] = { &CustomAttributeNamedArgument_t94157543_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeNamedArgument_t94157543_0_0_0 = { 1, GenInst_CustomAttributeNamedArgument_t94157543_0_0_0_Types };
extern const Il2CppType CustomAttributeData_t3093286891_0_0_0;
static const Il2CppType* GenInst_CustomAttributeData_t3093286891_0_0_0_Types[] = { &CustomAttributeData_t3093286891_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeData_t3093286891_0_0_0 = { 1, GenInst_CustomAttributeData_t3093286891_0_0_0_Types };
extern const Il2CppType Exception_t1927440687_0_0_0;
static const Il2CppType* GenInst_Exception_t1927440687_0_0_0_Types[] = { &Exception_t1927440687_0_0_0 };
extern const Il2CppGenericInst GenInst_Exception_t1927440687_0_0_0 = { 1, GenInst_Exception_t1927440687_0_0_0_Types };
extern const Il2CppType _Exception_t3026971024_0_0_0;
static const Il2CppType* GenInst__Exception_t3026971024_0_0_0_Types[] = { &_Exception_t3026971024_0_0_0 };
extern const Il2CppGenericInst GenInst__Exception_t3026971024_0_0_0 = { 1, GenInst__Exception_t3026971024_0_0_0_Types };
extern const Il2CppType ResourceInfo_t3933049236_0_0_0;
static const Il2CppType* GenInst_ResourceInfo_t3933049236_0_0_0_Types[] = { &ResourceInfo_t3933049236_0_0_0 };
extern const Il2CppGenericInst GenInst_ResourceInfo_t3933049236_0_0_0 = { 1, GenInst_ResourceInfo_t3933049236_0_0_0_Types };
extern const Il2CppType ResourceCacheItem_t333236149_0_0_0;
static const Il2CppType* GenInst_ResourceCacheItem_t333236149_0_0_0_Types[] = { &ResourceCacheItem_t333236149_0_0_0 };
extern const Il2CppGenericInst GenInst_ResourceCacheItem_t333236149_0_0_0 = { 1, GenInst_ResourceCacheItem_t333236149_0_0_0_Types };
extern const Il2CppType IContextProperty_t287246399_0_0_0;
static const Il2CppType* GenInst_IContextProperty_t287246399_0_0_0_Types[] = { &IContextProperty_t287246399_0_0_0 };
extern const Il2CppGenericInst GenInst_IContextProperty_t287246399_0_0_0 = { 1, GenInst_IContextProperty_t287246399_0_0_0_Types };
extern const Il2CppType Header_t2756440555_0_0_0;
static const Il2CppType* GenInst_Header_t2756440555_0_0_0_Types[] = { &Header_t2756440555_0_0_0 };
extern const Il2CppGenericInst GenInst_Header_t2756440555_0_0_0 = { 1, GenInst_Header_t2756440555_0_0_0_Types };
extern const Il2CppType ITrackingHandler_t2759960940_0_0_0;
static const Il2CppType* GenInst_ITrackingHandler_t2759960940_0_0_0_Types[] = { &ITrackingHandler_t2759960940_0_0_0 };
extern const Il2CppGenericInst GenInst_ITrackingHandler_t2759960940_0_0_0 = { 1, GenInst_ITrackingHandler_t2759960940_0_0_0_Types };
extern const Il2CppType IContextAttribute_t2439121372_0_0_0;
static const Il2CppType* GenInst_IContextAttribute_t2439121372_0_0_0_Types[] = { &IContextAttribute_t2439121372_0_0_0 };
extern const Il2CppGenericInst GenInst_IContextAttribute_t2439121372_0_0_0 = { 1, GenInst_IContextAttribute_t2439121372_0_0_0_Types };
extern const Il2CppType DateTime_t693205669_0_0_0;
static const Il2CppType* GenInst_DateTime_t693205669_0_0_0_Types[] = { &DateTime_t693205669_0_0_0 };
extern const Il2CppGenericInst GenInst_DateTime_t693205669_0_0_0 = { 1, GenInst_DateTime_t693205669_0_0_0_Types };
extern const Il2CppType IComparable_1_t2525044892_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t2525044892_0_0_0_Types[] = { &IComparable_1_t2525044892_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t2525044892_0_0_0 = { 1, GenInst_IComparable_1_t2525044892_0_0_0_Types };
extern const Il2CppType IEquatable_1_t2897187838_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t2897187838_0_0_0_Types[] = { &IEquatable_1_t2897187838_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2897187838_0_0_0 = { 1, GenInst_IEquatable_1_t2897187838_0_0_0_Types };
extern const Il2CppType IComparable_1_t2556540300_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t2556540300_0_0_0_Types[] = { &IComparable_1_t2556540300_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t2556540300_0_0_0 = { 1, GenInst_IComparable_1_t2556540300_0_0_0_Types };
extern const Il2CppType IEquatable_1_t2928683246_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t2928683246_0_0_0_Types[] = { &IEquatable_1_t2928683246_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2928683246_0_0_0 = { 1, GenInst_IEquatable_1_t2928683246_0_0_0_Types };
extern const Il2CppType TimeSpan_t3430258949_0_0_0;
static const Il2CppType* GenInst_TimeSpan_t3430258949_0_0_0_Types[] = { &TimeSpan_t3430258949_0_0_0 };
extern const Il2CppGenericInst GenInst_TimeSpan_t3430258949_0_0_0 = { 1, GenInst_TimeSpan_t3430258949_0_0_0_Types };
extern const Il2CppType IComparable_1_t967130876_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t967130876_0_0_0_Types[] = { &IComparable_1_t967130876_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t967130876_0_0_0 = { 1, GenInst_IComparable_1_t967130876_0_0_0_Types };
extern const Il2CppType IEquatable_1_t1339273822_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t1339273822_0_0_0_Types[] = { &IEquatable_1_t1339273822_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1339273822_0_0_0 = { 1, GenInst_IEquatable_1_t1339273822_0_0_0_Types };
extern const Il2CppType TypeTag_t141209596_0_0_0;
static const Il2CppType* GenInst_TypeTag_t141209596_0_0_0_Types[] = { &TypeTag_t141209596_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeTag_t141209596_0_0_0 = { 1, GenInst_TypeTag_t141209596_0_0_0_Types };
extern const Il2CppType Enum_t2459695545_0_0_0;
static const Il2CppType* GenInst_Enum_t2459695545_0_0_0_Types[] = { &Enum_t2459695545_0_0_0 };
extern const Il2CppGenericInst GenInst_Enum_t2459695545_0_0_0 = { 1, GenInst_Enum_t2459695545_0_0_0_Types };
extern const Il2CppType MonoType_t_0_0_0;
static const Il2CppType* GenInst_MonoType_t_0_0_0_Types[] = { &MonoType_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoType_t_0_0_0 = { 1, GenInst_MonoType_t_0_0_0_Types };
extern const Il2CppType StrongName_t2988747270_0_0_0;
static const Il2CppType* GenInst_StrongName_t2988747270_0_0_0_Types[] = { &StrongName_t2988747270_0_0_0 };
extern const Il2CppGenericInst GenInst_StrongName_t2988747270_0_0_0 = { 1, GenInst_StrongName_t2988747270_0_0_0_Types };
extern const Il2CppType WaitHandle_t677569169_0_0_0;
static const Il2CppType* GenInst_WaitHandle_t677569169_0_0_0_Types[] = { &WaitHandle_t677569169_0_0_0 };
extern const Il2CppGenericInst GenInst_WaitHandle_t677569169_0_0_0 = { 1, GenInst_WaitHandle_t677569169_0_0_0_Types };
extern const Il2CppType IDisposable_t2427283555_0_0_0;
static const Il2CppType* GenInst_IDisposable_t2427283555_0_0_0_Types[] = { &IDisposable_t2427283555_0_0_0 };
extern const Il2CppGenericInst GenInst_IDisposable_t2427283555_0_0_0 = { 1, GenInst_IDisposable_t2427283555_0_0_0_Types };
extern const Il2CppType MarshalByRefObject_t1285298191_0_0_0;
static const Il2CppType* GenInst_MarshalByRefObject_t1285298191_0_0_0_Types[] = { &MarshalByRefObject_t1285298191_0_0_0 };
extern const Il2CppGenericInst GenInst_MarshalByRefObject_t1285298191_0_0_0 = { 1, GenInst_MarshalByRefObject_t1285298191_0_0_0_Types };
extern const Il2CppType Assembly_t4268412390_0_0_0;
static const Il2CppType* GenInst_Assembly_t4268412390_0_0_0_Types[] = { &Assembly_t4268412390_0_0_0 };
extern const Il2CppGenericInst GenInst_Assembly_t4268412390_0_0_0 = { 1, GenInst_Assembly_t4268412390_0_0_0_Types };
extern const Il2CppType _Assembly_t2937922309_0_0_0;
static const Il2CppType* GenInst__Assembly_t2937922309_0_0_0_Types[] = { &_Assembly_t2937922309_0_0_0 };
extern const Il2CppGenericInst GenInst__Assembly_t2937922309_0_0_0 = { 1, GenInst__Assembly_t2937922309_0_0_0_Types };
extern const Il2CppType DateTimeOffset_t1362988906_0_0_0;
static const Il2CppType* GenInst_DateTimeOffset_t1362988906_0_0_0_Types[] = { &DateTimeOffset_t1362988906_0_0_0 };
extern const Il2CppGenericInst GenInst_DateTimeOffset_t1362988906_0_0_0 = { 1, GenInst_DateTimeOffset_t1362988906_0_0_0_Types };
extern const Il2CppType Guid_t2533601593_0_0_0;
static const Il2CppType* GenInst_Guid_t2533601593_0_0_0_Types[] = { &Guid_t2533601593_0_0_0 };
extern const Il2CppGenericInst GenInst_Guid_t2533601593_0_0_0 = { 1, GenInst_Guid_t2533601593_0_0_0_Types };
extern const Il2CppType Version_t1755874712_0_0_0;
static const Il2CppType* GenInst_Version_t1755874712_0_0_0_Types[] = { &Version_t1755874712_0_0_0 };
extern const Il2CppGenericInst GenInst_Version_t1755874712_0_0_0 = { 1, GenInst_Version_t1755874712_0_0_0_Types };
extern const Il2CppType BigInteger_t925946153_0_0_0;
static const Il2CppType* GenInst_BigInteger_t925946153_0_0_0_Types[] = { &BigInteger_t925946153_0_0_0 };
extern const Il2CppGenericInst GenInst_BigInteger_t925946153_0_0_0 = { 1, GenInst_BigInteger_t925946153_0_0_0_Types };
extern const Il2CppType ByteU5BU5D_t3397334013_0_0_0;
static const Il2CppType* GenInst_ByteU5BU5D_t3397334013_0_0_0_Types[] = { &ByteU5BU5D_t3397334013_0_0_0 };
extern const Il2CppGenericInst GenInst_ByteU5BU5D_t3397334013_0_0_0 = { 1, GenInst_ByteU5BU5D_t3397334013_0_0_0_Types };
extern const Il2CppType X509Certificate_t283079845_0_0_0;
static const Il2CppType* GenInst_X509Certificate_t283079845_0_0_0_Types[] = { &X509Certificate_t283079845_0_0_0 };
extern const Il2CppGenericInst GenInst_X509Certificate_t283079845_0_0_0 = { 1, GenInst_X509Certificate_t283079845_0_0_0_Types };
extern const Il2CppType ClientCertificateType_t4001384466_0_0_0;
static const Il2CppType* GenInst_ClientCertificateType_t4001384466_0_0_0_Types[] = { &ClientCertificateType_t4001384466_0_0_0 };
extern const Il2CppGenericInst GenInst_ClientCertificateType_t4001384466_0_0_0 = { 1, GenInst_ClientCertificateType_t4001384466_0_0_0_Types };
extern const Il2CppType Node_t2499136326_0_0_0;
static const Il2CppType* GenInst_Node_t2499136326_0_0_0_Types[] = { &Node_t2499136326_0_0_0 };
extern const Il2CppGenericInst GenInst_Node_t2499136326_0_0_0 = { 1, GenInst_Node_t2499136326_0_0_0_Types };
extern const Il2CppType PropertyDescriptor_t4250402154_0_0_0;
static const Il2CppType* GenInst_PropertyDescriptor_t4250402154_0_0_0_Types[] = { &PropertyDescriptor_t4250402154_0_0_0 };
extern const Il2CppGenericInst GenInst_PropertyDescriptor_t4250402154_0_0_0 = { 1, GenInst_PropertyDescriptor_t4250402154_0_0_0_Types };
extern const Il2CppType MemberDescriptor_t3749827553_0_0_0;
static const Il2CppType* GenInst_MemberDescriptor_t3749827553_0_0_0_Types[] = { &MemberDescriptor_t3749827553_0_0_0 };
extern const Il2CppGenericInst GenInst_MemberDescriptor_t3749827553_0_0_0 = { 1, GenInst_MemberDescriptor_t3749827553_0_0_0_Types };
extern const Il2CppType LinkedList_1_t2743332604_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_LinkedList_1_t2743332604_0_0_0_Types[] = { &Type_t_0_0_0, &LinkedList_1_t2743332604_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_LinkedList_1_t2743332604_0_0_0 = { 2, GenInst_Type_t_0_0_0_LinkedList_1_t2743332604_0_0_0_Types };
extern const Il2CppType TypeDescriptionProvider_t2438624375_0_0_0;
static const Il2CppType* GenInst_TypeDescriptionProvider_t2438624375_0_0_0_Types[] = { &TypeDescriptionProvider_t2438624375_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeDescriptionProvider_t2438624375_0_0_0 = { 1, GenInst_TypeDescriptionProvider_t2438624375_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_LinkedList_1_t2743332604_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Type_t_0_0_0, &LinkedList_1_t2743332604_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_LinkedList_1_t2743332604_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_LinkedList_1_t2743332604_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2438035723_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2438035723_0_0_0_Types[] = { &KeyValuePair_2_t2438035723_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2438035723_0_0_0 = { 1, GenInst_KeyValuePair_2_t2438035723_0_0_0_Types };
extern const Il2CppType WeakObjectWrapper_t2012978780_0_0_0;
static const Il2CppType* GenInst_WeakObjectWrapper_t2012978780_0_0_0_LinkedList_1_t2743332604_0_0_0_Types[] = { &WeakObjectWrapper_t2012978780_0_0_0, &LinkedList_1_t2743332604_0_0_0 };
extern const Il2CppGenericInst GenInst_WeakObjectWrapper_t2012978780_0_0_0_LinkedList_1_t2743332604_0_0_0 = { 2, GenInst_WeakObjectWrapper_t2012978780_0_0_0_LinkedList_1_t2743332604_0_0_0_Types };
static const Il2CppType* GenInst_WeakObjectWrapper_t2012978780_0_0_0_Types[] = { &WeakObjectWrapper_t2012978780_0_0_0 };
extern const Il2CppGenericInst GenInst_WeakObjectWrapper_t2012978780_0_0_0 = { 1, GenInst_WeakObjectWrapper_t2012978780_0_0_0_Types };
static const Il2CppType* GenInst_WeakObjectWrapper_t2012978780_0_0_0_LinkedList_1_t2743332604_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &WeakObjectWrapper_t2012978780_0_0_0, &LinkedList_1_t2743332604_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_WeakObjectWrapper_t2012978780_0_0_0_LinkedList_1_t2743332604_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_WeakObjectWrapper_t2012978780_0_0_0_LinkedList_1_t2743332604_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3261256129_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3261256129_0_0_0_Types[] = { &KeyValuePair_2_t3261256129_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3261256129_0_0_0 = { 1, GenInst_KeyValuePair_2_t3261256129_0_0_0_Types };
extern const Il2CppType X509ChainStatus_t4278378721_0_0_0;
static const Il2CppType* GenInst_X509ChainStatus_t4278378721_0_0_0_Types[] = { &X509ChainStatus_t4278378721_0_0_0 };
extern const Il2CppGenericInst GenInst_X509ChainStatus_t4278378721_0_0_0 = { 1, GenInst_X509ChainStatus_t4278378721_0_0_0_Types };
extern const Il2CppType IPAddress_t1399971723_0_0_0;
static const Il2CppType* GenInst_IPAddress_t1399971723_0_0_0_Types[] = { &IPAddress_t1399971723_0_0_0 };
extern const Il2CppGenericInst GenInst_IPAddress_t1399971723_0_0_0 = { 1, GenInst_IPAddress_t1399971723_0_0_0_Types };
extern const Il2CppType ArraySegment_1_t2594217482_0_0_0;
static const Il2CppType* GenInst_ArraySegment_1_t2594217482_0_0_0_Types[] = { &ArraySegment_1_t2594217482_0_0_0 };
extern const Il2CppGenericInst GenInst_ArraySegment_1_t2594217482_0_0_0 = { 1, GenInst_ArraySegment_1_t2594217482_0_0_0_Types };
extern const Il2CppType Cookie_t3154017544_0_0_0;
static const Il2CppType* GenInst_Cookie_t3154017544_0_0_0_Types[] = { &Cookie_t3154017544_0_0_0 };
extern const Il2CppGenericInst GenInst_Cookie_t3154017544_0_0_0 = { 1, GenInst_Cookie_t3154017544_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &String_t_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1174980068_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1174980068_0_0_0_Types[] = { &KeyValuePair_2_t1174980068_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1174980068_0_0_0 = { 1, GenInst_KeyValuePair_2_t1174980068_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Boolean_t3825574718_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Boolean_t3825574718_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Boolean_t3825574718_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_KeyValuePair_2_t1174980068_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Boolean_t3825574718_0_0_0, &KeyValuePair_2_t1174980068_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_KeyValuePair_2_t1174980068_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_KeyValuePair_2_t1174980068_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &Boolean_t3825574718_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3497699202_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3497699202_0_0_0_Types[] = { &KeyValuePair_2_t3497699202_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3497699202_0_0_0 = { 1, GenInst_KeyValuePair_2_t3497699202_0_0_0_Types };
extern const Il2CppType Capture_t4157900610_0_0_0;
static const Il2CppType* GenInst_Capture_t4157900610_0_0_0_Types[] = { &Capture_t4157900610_0_0_0 };
extern const Il2CppGenericInst GenInst_Capture_t4157900610_0_0_0 = { 1, GenInst_Capture_t4157900610_0_0_0_Types };
extern const Il2CppType Group_t3761430853_0_0_0;
static const Il2CppType* GenInst_Group_t3761430853_0_0_0_Types[] = { &Group_t3761430853_0_0_0 };
extern const Il2CppGenericInst GenInst_Group_t3761430853_0_0_0 = { 1, GenInst_Group_t3761430853_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3132015601_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3132015601_0_0_0_Types[] = { &KeyValuePair_2_t3132015601_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3132015601_0_0_0 = { 1, GenInst_KeyValuePair_2_t3132015601_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Int32_t2071877448_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Int32_t2071877448_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3132015601_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Int32_t2071877448_0_0_0, &KeyValuePair_2_t3132015601_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3132015601_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3132015601_0_0_0_Types };
extern const Il2CppType Mark_t2724874473_0_0_0;
static const Il2CppType* GenInst_Mark_t2724874473_0_0_0_Types[] = { &Mark_t2724874473_0_0_0 };
extern const Il2CppGenericInst GenInst_Mark_t2724874473_0_0_0 = { 1, GenInst_Mark_t2724874473_0_0_0_Types };
extern const Il2CppType UriScheme_t1876590943_0_0_0;
static const Il2CppType* GenInst_UriScheme_t1876590943_0_0_0_Types[] = { &UriScheme_t1876590943_0_0_0 };
extern const Il2CppGenericInst GenInst_UriScheme_t1876590943_0_0_0 = { 1, GenInst_UriScheme_t1876590943_0_0_0_Types };
extern const Il2CppType Link_t865133271_0_0_0;
static const Il2CppType* GenInst_Link_t865133271_0_0_0_Types[] = { &Link_t865133271_0_0_0 };
extern const Il2CppGenericInst GenInst_Link_t865133271_0_0_0 = { 1, GenInst_Link_t865133271_0_0_0_Types };
extern const Il2CppType LockDetails_t2550015005_0_0_0;
static const Il2CppType* GenInst_LockDetails_t2550015005_0_0_0_Types[] = { &LockDetails_t2550015005_0_0_0 };
extern const Il2CppGenericInst GenInst_LockDetails_t2550015005_0_0_0 = { 1, GenInst_LockDetails_t2550015005_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 4, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType XmlSchemaAttribute_t4015859774_0_0_0;
static const Il2CppType* GenInst_XmlSchemaAttribute_t4015859774_0_0_0_Types[] = { &XmlSchemaAttribute_t4015859774_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlSchemaAttribute_t4015859774_0_0_0 = { 1, GenInst_XmlSchemaAttribute_t4015859774_0_0_0_Types };
extern const Il2CppType XmlSchemaAnnotated_t2082486936_0_0_0;
static const Il2CppType* GenInst_XmlSchemaAnnotated_t2082486936_0_0_0_Types[] = { &XmlSchemaAnnotated_t2082486936_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlSchemaAnnotated_t2082486936_0_0_0 = { 1, GenInst_XmlSchemaAnnotated_t2082486936_0_0_0_Types };
extern const Il2CppType XmlSchemaObject_t2050913741_0_0_0;
static const Il2CppType* GenInst_XmlSchemaObject_t2050913741_0_0_0_Types[] = { &XmlSchemaObject_t2050913741_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlSchemaObject_t2050913741_0_0_0 = { 1, GenInst_XmlSchemaObject_t2050913741_0_0_0_Types };
extern const Il2CppType XsdIdentityPath_t2037874_0_0_0;
static const Il2CppType* GenInst_XsdIdentityPath_t2037874_0_0_0_Types[] = { &XsdIdentityPath_t2037874_0_0_0 };
extern const Il2CppGenericInst GenInst_XsdIdentityPath_t2037874_0_0_0 = { 1, GenInst_XsdIdentityPath_t2037874_0_0_0_Types };
extern const Il2CppType XsdIdentityField_t2563516441_0_0_0;
static const Il2CppType* GenInst_XsdIdentityField_t2563516441_0_0_0_Types[] = { &XsdIdentityField_t2563516441_0_0_0 };
extern const Il2CppGenericInst GenInst_XsdIdentityField_t2563516441_0_0_0 = { 1, GenInst_XsdIdentityField_t2563516441_0_0_0_Types };
extern const Il2CppType XsdIdentityStep_t452377251_0_0_0;
static const Il2CppType* GenInst_XsdIdentityStep_t452377251_0_0_0_Types[] = { &XsdIdentityStep_t452377251_0_0_0 };
extern const Il2CppGenericInst GenInst_XsdIdentityStep_t452377251_0_0_0 = { 1, GenInst_XsdIdentityStep_t452377251_0_0_0_Types };
extern const Il2CppType XmlAttribute_t175731005_0_0_0;
static const Il2CppType* GenInst_XmlAttribute_t175731005_0_0_0_Types[] = { &XmlAttribute_t175731005_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlAttribute_t175731005_0_0_0 = { 1, GenInst_XmlAttribute_t175731005_0_0_0_Types };
extern const Il2CppType IHasXmlChildNode_t2048545686_0_0_0;
static const Il2CppType* GenInst_IHasXmlChildNode_t2048545686_0_0_0_Types[] = { &IHasXmlChildNode_t2048545686_0_0_0 };
extern const Il2CppGenericInst GenInst_IHasXmlChildNode_t2048545686_0_0_0 = { 1, GenInst_IHasXmlChildNode_t2048545686_0_0_0_Types };
extern const Il2CppType XmlNode_t616554813_0_0_0;
static const Il2CppType* GenInst_XmlNode_t616554813_0_0_0_Types[] = { &XmlNode_t616554813_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlNode_t616554813_0_0_0 = { 1, GenInst_XmlNode_t616554813_0_0_0_Types };
extern const Il2CppType IXPathNavigable_t845515791_0_0_0;
static const Il2CppType* GenInst_IXPathNavigable_t845515791_0_0_0_Types[] = { &IXPathNavigable_t845515791_0_0_0 };
extern const Il2CppGenericInst GenInst_IXPathNavigable_t845515791_0_0_0 = { 1, GenInst_IXPathNavigable_t845515791_0_0_0_Types };
extern const Il2CppType XmlQualifiedName_t1944712516_0_0_0;
static const Il2CppType* GenInst_XmlQualifiedName_t1944712516_0_0_0_Types[] = { &XmlQualifiedName_t1944712516_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlQualifiedName_t1944712516_0_0_0 = { 1, GenInst_XmlQualifiedName_t1944712516_0_0_0_Types };
extern const Il2CppType Regex_t1803876613_0_0_0;
static const Il2CppType* GenInst_Regex_t1803876613_0_0_0_Types[] = { &Regex_t1803876613_0_0_0 };
extern const Il2CppGenericInst GenInst_Regex_t1803876613_0_0_0 = { 1, GenInst_Regex_t1803876613_0_0_0_Types };
extern const Il2CppType XmlSchemaSimpleType_t248156492_0_0_0;
static const Il2CppType* GenInst_XmlSchemaSimpleType_t248156492_0_0_0_Types[] = { &XmlSchemaSimpleType_t248156492_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlSchemaSimpleType_t248156492_0_0_0 = { 1, GenInst_XmlSchemaSimpleType_t248156492_0_0_0_Types };
extern const Il2CppType XmlSchemaType_t1795078578_0_0_0;
static const Il2CppType* GenInst_XmlSchemaType_t1795078578_0_0_0_Types[] = { &XmlSchemaType_t1795078578_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlSchemaType_t1795078578_0_0_0 = { 1, GenInst_XmlSchemaType_t1795078578_0_0_0_Types };
extern const Il2CppType XmlException_t4188277960_0_0_0;
static const Il2CppType* GenInst_XmlException_t4188277960_0_0_0_Types[] = { &XmlException_t4188277960_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlException_t4188277960_0_0_0 = { 1, GenInst_XmlException_t4188277960_0_0_0_Types };
extern const Il2CppType SystemException_t3877406272_0_0_0;
static const Il2CppType* GenInst_SystemException_t3877406272_0_0_0_Types[] = { &SystemException_t3877406272_0_0_0 };
extern const Il2CppGenericInst GenInst_SystemException_t3877406272_0_0_0 = { 1, GenInst_SystemException_t3877406272_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1430411454_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1430411454_0_0_0_Types[] = { &KeyValuePair_2_t1430411454_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1430411454_0_0_0 = { 1, GenInst_KeyValuePair_2_t1430411454_0_0_0_Types };
extern const Il2CppType DTDNode_t1758286970_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_DTDNode_t1758286970_0_0_0_Types[] = { &String_t_0_0_0, &DTDNode_t1758286970_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_DTDNode_t1758286970_0_0_0 = { 2, GenInst_String_t_0_0_0_DTDNode_t1758286970_0_0_0_Types };
static const Il2CppType* GenInst_DTDNode_t1758286970_0_0_0_Types[] = { &DTDNode_t1758286970_0_0_0 };
extern const Il2CppGenericInst GenInst_DTDNode_t1758286970_0_0_0 = { 1, GenInst_DTDNode_t1758286970_0_0_0_Types };
extern const Il2CppType AttributeSlot_t1499247213_0_0_0;
static const Il2CppType* GenInst_AttributeSlot_t1499247213_0_0_0_Types[] = { &AttributeSlot_t1499247213_0_0_0 };
extern const Il2CppGenericInst GenInst_AttributeSlot_t1499247213_0_0_0 = { 1, GenInst_AttributeSlot_t1499247213_0_0_0_Types };
extern const Il2CppType Entry_t2583369454_0_0_0;
static const Il2CppType* GenInst_Entry_t2583369454_0_0_0_Types[] = { &Entry_t2583369454_0_0_0 };
extern const Il2CppGenericInst GenInst_Entry_t2583369454_0_0_0 = { 1, GenInst_Entry_t2583369454_0_0_0_Types };
extern const Il2CppType NsDecl_t3210081295_0_0_0;
static const Il2CppType* GenInst_NsDecl_t3210081295_0_0_0_Types[] = { &NsDecl_t3210081295_0_0_0 };
extern const Il2CppGenericInst GenInst_NsDecl_t3210081295_0_0_0 = { 1, GenInst_NsDecl_t3210081295_0_0_0_Types };
extern const Il2CppType NsScope_t2513625351_0_0_0;
static const Il2CppType* GenInst_NsScope_t2513625351_0_0_0_Types[] = { &NsScope_t2513625351_0_0_0 };
extern const Il2CppGenericInst GenInst_NsScope_t2513625351_0_0_0 = { 1, GenInst_NsScope_t2513625351_0_0_0_Types };
extern const Il2CppType XmlAttributeTokenInfo_t3353594030_0_0_0;
static const Il2CppType* GenInst_XmlAttributeTokenInfo_t3353594030_0_0_0_Types[] = { &XmlAttributeTokenInfo_t3353594030_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlAttributeTokenInfo_t3353594030_0_0_0 = { 1, GenInst_XmlAttributeTokenInfo_t3353594030_0_0_0_Types };
extern const Il2CppType XmlTokenInfo_t254587324_0_0_0;
static const Il2CppType* GenInst_XmlTokenInfo_t254587324_0_0_0_Types[] = { &XmlTokenInfo_t254587324_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlTokenInfo_t254587324_0_0_0 = { 1, GenInst_XmlTokenInfo_t254587324_0_0_0_Types };
extern const Il2CppType TagName_t2340974457_0_0_0;
static const Il2CppType* GenInst_TagName_t2340974457_0_0_0_Types[] = { &TagName_t2340974457_0_0_0 };
extern const Il2CppGenericInst GenInst_TagName_t2340974457_0_0_0 = { 1, GenInst_TagName_t2340974457_0_0_0_Types };
extern const Il2CppType XmlNodeInfo_t3709371029_0_0_0;
static const Il2CppType* GenInst_XmlNodeInfo_t3709371029_0_0_0_Types[] = { &XmlNodeInfo_t3709371029_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlNodeInfo_t3709371029_0_0_0 = { 1, GenInst_XmlNodeInfo_t3709371029_0_0_0_Types };
extern const Il2CppType XAttribute_t3858477518_0_0_0;
static const Il2CppType* GenInst_XAttribute_t3858477518_0_0_0_Types[] = { &XAttribute_t3858477518_0_0_0 };
extern const Il2CppGenericInst GenInst_XAttribute_t3858477518_0_0_0 = { 1, GenInst_XAttribute_t3858477518_0_0_0_Types };
extern const Il2CppType XObject_t3550811009_0_0_0;
static const Il2CppType* GenInst_XObject_t3550811009_0_0_0_Types[] = { &XObject_t3550811009_0_0_0 };
extern const Il2CppGenericInst GenInst_XObject_t3550811009_0_0_0 = { 1, GenInst_XObject_t3550811009_0_0_0_Types };
extern const Il2CppType IXmlLineInfo_t135184468_0_0_0;
static const Il2CppType* GenInst_IXmlLineInfo_t135184468_0_0_0_Types[] = { &IXmlLineInfo_t135184468_0_0_0 };
extern const Il2CppGenericInst GenInst_IXmlLineInfo_t135184468_0_0_0 = { 1, GenInst_IXmlLineInfo_t135184468_0_0_0_Types };
extern const Il2CppType XNode_t2707504214_0_0_0;
static const Il2CppType* GenInst_XNode_t2707504214_0_0_0_Types[] = { &XNode_t2707504214_0_0_0 };
extern const Il2CppGenericInst GenInst_XNode_t2707504214_0_0_0 = { 1, GenInst_XNode_t2707504214_0_0_0_Types };
extern const Il2CppType XElement_t553821050_0_0_0;
static const Il2CppType* GenInst_XElement_t553821050_0_0_0_Types[] = { &XElement_t553821050_0_0_0 };
extern const Il2CppGenericInst GenInst_XElement_t553821050_0_0_0 = { 1, GenInst_XElement_t553821050_0_0_0_Types };
extern const Il2CppType XName_t785190363_0_0_0;
static const Il2CppType* GenInst_XName_t785190363_0_0_0_Types[] = { &XName_t785190363_0_0_0 };
extern const Il2CppGenericInst GenInst_XName_t785190363_0_0_0 = { 1, GenInst_XName_t785190363_0_0_0_Types };
extern const Il2CppType XNamespace_t1613015075_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_XNamespace_t1613015075_0_0_0_Types[] = { &String_t_0_0_0, &XNamespace_t1613015075_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_XNamespace_t1613015075_0_0_0 = { 2, GenInst_String_t_0_0_0_XNamespace_t1613015075_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_XNamespace_t1613015075_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &XNamespace_t1613015075_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_XNamespace_t1613015075_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_XNamespace_t1613015075_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1285139559_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1285139559_0_0_0_Types[] = { &KeyValuePair_2_t1285139559_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1285139559_0_0_0 = { 1, GenInst_KeyValuePair_2_t1285139559_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_XName_t785190363_0_0_0_Types[] = { &String_t_0_0_0, &XName_t785190363_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_XName_t785190363_0_0_0 = { 2, GenInst_String_t_0_0_0_XName_t785190363_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_XName_t785190363_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &XName_t785190363_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_XName_t785190363_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_XName_t785190363_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t457314847_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t457314847_0_0_0_Types[] = { &KeyValuePair_2_t457314847_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t457314847_0_0_0 = { 1, GenInst_KeyValuePair_2_t457314847_0_0_0_Types };
extern const Il2CppType Object_t1021602117_0_0_0;
static const Il2CppType* GenInst_Object_t1021602117_0_0_0_Types[] = { &Object_t1021602117_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t1021602117_0_0_0 = { 1, GenInst_Object_t1021602117_0_0_0_Types };
extern const Il2CppType Camera_t189460977_0_0_0;
static const Il2CppType* GenInst_Camera_t189460977_0_0_0_Types[] = { &Camera_t189460977_0_0_0 };
extern const Il2CppGenericInst GenInst_Camera_t189460977_0_0_0 = { 1, GenInst_Camera_t189460977_0_0_0_Types };
extern const Il2CppType Behaviour_t955675639_0_0_0;
static const Il2CppType* GenInst_Behaviour_t955675639_0_0_0_Types[] = { &Behaviour_t955675639_0_0_0 };
extern const Il2CppGenericInst GenInst_Behaviour_t955675639_0_0_0 = { 1, GenInst_Behaviour_t955675639_0_0_0_Types };
extern const Il2CppType Component_t3819376471_0_0_0;
static const Il2CppType* GenInst_Component_t3819376471_0_0_0_Types[] = { &Component_t3819376471_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_t3819376471_0_0_0 = { 1, GenInst_Component_t3819376471_0_0_0_Types };
extern const Il2CppType Display_t3666191348_0_0_0;
static const Il2CppType* GenInst_Display_t3666191348_0_0_0_Types[] = { &Display_t3666191348_0_0_0 };
extern const Il2CppGenericInst GenInst_Display_t3666191348_0_0_0 = { 1, GenInst_Display_t3666191348_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t3825574718_0_0_0_String_t_0_0_0_Types[] = { &Boolean_t3825574718_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t3825574718_0_0_0_String_t_0_0_0 = { 2, GenInst_Boolean_t3825574718_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t3825574718_0_0_0_Il2CppObject_0_0_0_Types[] = { &Boolean_t3825574718_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t3825574718_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Boolean_t3825574718_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType AchievementDescription_t3110978151_0_0_0;
static const Il2CppType* GenInst_AchievementDescription_t3110978151_0_0_0_Types[] = { &AchievementDescription_t3110978151_0_0_0 };
extern const Il2CppGenericInst GenInst_AchievementDescription_t3110978151_0_0_0 = { 1, GenInst_AchievementDescription_t3110978151_0_0_0_Types };
extern const Il2CppType IAchievementDescription_t3498529102_0_0_0;
static const Il2CppType* GenInst_IAchievementDescription_t3498529102_0_0_0_Types[] = { &IAchievementDescription_t3498529102_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievementDescription_t3498529102_0_0_0 = { 1, GenInst_IAchievementDescription_t3498529102_0_0_0_Types };
extern const Il2CppType UserProfile_t3365630962_0_0_0;
static const Il2CppType* GenInst_UserProfile_t3365630962_0_0_0_Types[] = { &UserProfile_t3365630962_0_0_0 };
extern const Il2CppGenericInst GenInst_UserProfile_t3365630962_0_0_0 = { 1, GenInst_UserProfile_t3365630962_0_0_0_Types };
extern const Il2CppType IUserProfile_t4108565527_0_0_0;
static const Il2CppType* GenInst_IUserProfile_t4108565527_0_0_0_Types[] = { &IUserProfile_t4108565527_0_0_0 };
extern const Il2CppGenericInst GenInst_IUserProfile_t4108565527_0_0_0 = { 1, GenInst_IUserProfile_t4108565527_0_0_0_Types };
extern const Il2CppType GcLeaderboard_t453887929_0_0_0;
static const Il2CppType* GenInst_GcLeaderboard_t453887929_0_0_0_Types[] = { &GcLeaderboard_t453887929_0_0_0 };
extern const Il2CppGenericInst GenInst_GcLeaderboard_t453887929_0_0_0 = { 1, GenInst_GcLeaderboard_t453887929_0_0_0_Types };
extern const Il2CppType IAchievementDescriptionU5BU5D_t4083280315_0_0_0;
static const Il2CppType* GenInst_IAchievementDescriptionU5BU5D_t4083280315_0_0_0_Types[] = { &IAchievementDescriptionU5BU5D_t4083280315_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievementDescriptionU5BU5D_t4083280315_0_0_0 = { 1, GenInst_IAchievementDescriptionU5BU5D_t4083280315_0_0_0_Types };
extern const Il2CppType IAchievementU5BU5D_t2709554645_0_0_0;
static const Il2CppType* GenInst_IAchievementU5BU5D_t2709554645_0_0_0_Types[] = { &IAchievementU5BU5D_t2709554645_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievementU5BU5D_t2709554645_0_0_0 = { 1, GenInst_IAchievementU5BU5D_t2709554645_0_0_0_Types };
extern const Il2CppType IAchievement_t1752291260_0_0_0;
static const Il2CppType* GenInst_IAchievement_t1752291260_0_0_0_Types[] = { &IAchievement_t1752291260_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievement_t1752291260_0_0_0 = { 1, GenInst_IAchievement_t1752291260_0_0_0_Types };
extern const Il2CppType GcAchievementData_t1754866149_0_0_0;
static const Il2CppType* GenInst_GcAchievementData_t1754866149_0_0_0_Types[] = { &GcAchievementData_t1754866149_0_0_0 };
extern const Il2CppGenericInst GenInst_GcAchievementData_t1754866149_0_0_0 = { 1, GenInst_GcAchievementData_t1754866149_0_0_0_Types };
extern const Il2CppType Achievement_t1333316625_0_0_0;
static const Il2CppType* GenInst_Achievement_t1333316625_0_0_0_Types[] = { &Achievement_t1333316625_0_0_0 };
extern const Il2CppGenericInst GenInst_Achievement_t1333316625_0_0_0 = { 1, GenInst_Achievement_t1333316625_0_0_0_Types };
extern const Il2CppType IScoreU5BU5D_t3237304636_0_0_0;
static const Il2CppType* GenInst_IScoreU5BU5D_t3237304636_0_0_0_Types[] = { &IScoreU5BU5D_t3237304636_0_0_0 };
extern const Il2CppGenericInst GenInst_IScoreU5BU5D_t3237304636_0_0_0 = { 1, GenInst_IScoreU5BU5D_t3237304636_0_0_0_Types };
extern const Il2CppType IScore_t513966369_0_0_0;
static const Il2CppType* GenInst_IScore_t513966369_0_0_0_Types[] = { &IScore_t513966369_0_0_0 };
extern const Il2CppGenericInst GenInst_IScore_t513966369_0_0_0 = { 1, GenInst_IScore_t513966369_0_0_0_Types };
extern const Il2CppType GcScoreData_t3676783238_0_0_0;
static const Il2CppType* GenInst_GcScoreData_t3676783238_0_0_0_Types[] = { &GcScoreData_t3676783238_0_0_0 };
extern const Il2CppGenericInst GenInst_GcScoreData_t3676783238_0_0_0 = { 1, GenInst_GcScoreData_t3676783238_0_0_0_Types };
extern const Il2CppType Score_t2307748940_0_0_0;
static const Il2CppType* GenInst_Score_t2307748940_0_0_0_Types[] = { &Score_t2307748940_0_0_0 };
extern const Il2CppGenericInst GenInst_Score_t2307748940_0_0_0 = { 1, GenInst_Score_t2307748940_0_0_0_Types };
extern const Il2CppType IUserProfileU5BU5D_t3461248430_0_0_0;
static const Il2CppType* GenInst_IUserProfileU5BU5D_t3461248430_0_0_0_Types[] = { &IUserProfileU5BU5D_t3461248430_0_0_0 };
extern const Il2CppGenericInst GenInst_IUserProfileU5BU5D_t3461248430_0_0_0 = { 1, GenInst_IUserProfileU5BU5D_t3461248430_0_0_0_Types };
extern const Il2CppType Vector2_t2243707579_0_0_0;
static const Il2CppType* GenInst_Vector2_t2243707579_0_0_0_Types[] = { &Vector2_t2243707579_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector2_t2243707579_0_0_0 = { 1, GenInst_Vector2_t2243707579_0_0_0_Types };
extern const Il2CppType Keyframe_t1449471340_0_0_0;
static const Il2CppType* GenInst_Keyframe_t1449471340_0_0_0_Types[] = { &Keyframe_t1449471340_0_0_0 };
extern const Il2CppGenericInst GenInst_Keyframe_t1449471340_0_0_0 = { 1, GenInst_Keyframe_t1449471340_0_0_0_Types };
extern const Il2CppType Vector3_t2243707580_0_0_0;
static const Il2CppType* GenInst_Vector3_t2243707580_0_0_0_Types[] = { &Vector3_t2243707580_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector3_t2243707580_0_0_0 = { 1, GenInst_Vector3_t2243707580_0_0_0_Types };
extern const Il2CppType Vector4_t2243707581_0_0_0;
static const Il2CppType* GenInst_Vector4_t2243707581_0_0_0_Types[] = { &Vector4_t2243707581_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector4_t2243707581_0_0_0 = { 1, GenInst_Vector4_t2243707581_0_0_0_Types };
extern const Il2CppType Color32_t874517518_0_0_0;
static const Il2CppType* GenInst_Color32_t874517518_0_0_0_Types[] = { &Color32_t874517518_0_0_0 };
extern const Il2CppGenericInst GenInst_Color32_t874517518_0_0_0 = { 1, GenInst_Color32_t874517518_0_0_0_Types };
extern const Il2CppType Color_t2020392075_0_0_0;
static const Il2CppType* GenInst_Color_t2020392075_0_0_0_Types[] = { &Color_t2020392075_0_0_0 };
extern const Il2CppGenericInst GenInst_Color_t2020392075_0_0_0 = { 1, GenInst_Color_t2020392075_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_String_t_0_0_0_Types[] = { &String_t_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_String_t_0_0_0 = { 2, GenInst_String_t_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &String_t_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1701344717_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1701344717_0_0_0_Types[] = { &KeyValuePair_2_t1701344717_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1701344717_0_0_0 = { 1, GenInst_KeyValuePair_2_t1701344717_0_0_0_Types };
extern const Il2CppType Playable_t3667545548_0_0_0;
static const Il2CppType* GenInst_Playable_t3667545548_0_0_0_Types[] = { &Playable_t3667545548_0_0_0 };
extern const Il2CppGenericInst GenInst_Playable_t3667545548_0_0_0 = { 1, GenInst_Playable_t3667545548_0_0_0_Types };
extern const Il2CppType Scene_t1684909666_0_0_0;
extern const Il2CppType LoadSceneMode_t2981886439_0_0_0;
static const Il2CppType* GenInst_Scene_t1684909666_0_0_0_LoadSceneMode_t2981886439_0_0_0_Types[] = { &Scene_t1684909666_0_0_0, &LoadSceneMode_t2981886439_0_0_0 };
extern const Il2CppGenericInst GenInst_Scene_t1684909666_0_0_0_LoadSceneMode_t2981886439_0_0_0 = { 2, GenInst_Scene_t1684909666_0_0_0_LoadSceneMode_t2981886439_0_0_0_Types };
static const Il2CppType* GenInst_Scene_t1684909666_0_0_0_Types[] = { &Scene_t1684909666_0_0_0 };
extern const Il2CppGenericInst GenInst_Scene_t1684909666_0_0_0 = { 1, GenInst_Scene_t1684909666_0_0_0_Types };
static const Il2CppType* GenInst_Scene_t1684909666_0_0_0_Scene_t1684909666_0_0_0_Types[] = { &Scene_t1684909666_0_0_0, &Scene_t1684909666_0_0_0 };
extern const Il2CppGenericInst GenInst_Scene_t1684909666_0_0_0_Scene_t1684909666_0_0_0 = { 2, GenInst_Scene_t1684909666_0_0_0_Scene_t1684909666_0_0_0_Types };
extern const Il2CppType ContactPoint_t1376425630_0_0_0;
static const Il2CppType* GenInst_ContactPoint_t1376425630_0_0_0_Types[] = { &ContactPoint_t1376425630_0_0_0 };
extern const Il2CppGenericInst GenInst_ContactPoint_t1376425630_0_0_0 = { 1, GenInst_ContactPoint_t1376425630_0_0_0_Types };
extern const Il2CppType Rigidbody2D_t502193897_0_0_0;
static const Il2CppType* GenInst_Rigidbody2D_t502193897_0_0_0_Types[] = { &Rigidbody2D_t502193897_0_0_0 };
extern const Il2CppGenericInst GenInst_Rigidbody2D_t502193897_0_0_0 = { 1, GenInst_Rigidbody2D_t502193897_0_0_0_Types };
extern const Il2CppType ContactPoint2D_t3659330976_0_0_0;
static const Il2CppType* GenInst_ContactPoint2D_t3659330976_0_0_0_Types[] = { &ContactPoint2D_t3659330976_0_0_0 };
extern const Il2CppGenericInst GenInst_ContactPoint2D_t3659330976_0_0_0 = { 1, GenInst_ContactPoint2D_t3659330976_0_0_0_Types };
extern const Il2CppType UIVertex_t1204258818_0_0_0;
static const Il2CppType* GenInst_UIVertex_t1204258818_0_0_0_Types[] = { &UIVertex_t1204258818_0_0_0 };
extern const Il2CppGenericInst GenInst_UIVertex_t1204258818_0_0_0 = { 1, GenInst_UIVertex_t1204258818_0_0_0_Types };
extern const Il2CppType UICharInfo_t3056636800_0_0_0;
static const Il2CppType* GenInst_UICharInfo_t3056636800_0_0_0_Types[] = { &UICharInfo_t3056636800_0_0_0 };
extern const Il2CppGenericInst GenInst_UICharInfo_t3056636800_0_0_0 = { 1, GenInst_UICharInfo_t3056636800_0_0_0_Types };
extern const Il2CppType UILineInfo_t3621277874_0_0_0;
static const Il2CppType* GenInst_UILineInfo_t3621277874_0_0_0_Types[] = { &UILineInfo_t3621277874_0_0_0 };
extern const Il2CppGenericInst GenInst_UILineInfo_t3621277874_0_0_0 = { 1, GenInst_UILineInfo_t3621277874_0_0_0_Types };
extern const Il2CppType Font_t4239498691_0_0_0;
static const Il2CppType* GenInst_Font_t4239498691_0_0_0_Types[] = { &Font_t4239498691_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t4239498691_0_0_0 = { 1, GenInst_Font_t4239498691_0_0_0_Types };
extern const Il2CppType GUILayoutOption_t4183744904_0_0_0;
static const Il2CppType* GenInst_GUILayoutOption_t4183744904_0_0_0_Types[] = { &GUILayoutOption_t4183744904_0_0_0 };
extern const Il2CppGenericInst GenInst_GUILayoutOption_t4183744904_0_0_0 = { 1, GenInst_GUILayoutOption_t4183744904_0_0_0_Types };
extern const Il2CppType GUILayoutEntry_t3828586629_0_0_0;
static const Il2CppType* GenInst_GUILayoutEntry_t3828586629_0_0_0_Types[] = { &GUILayoutEntry_t3828586629_0_0_0 };
extern const Il2CppGenericInst GenInst_GUILayoutEntry_t3828586629_0_0_0 = { 1, GenInst_GUILayoutEntry_t3828586629_0_0_0_Types };
extern const Il2CppType LayoutCache_t3120781045_0_0_0;
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &LayoutCache_t3120781045_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3749587448_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3749587448_0_0_0_Types[] = { &KeyValuePair_2_t3749587448_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3749587448_0_0_0 = { 1, GenInst_KeyValuePair_2_t3749587448_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Il2CppObject_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t3749587448_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t3749587448_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t3749587448_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t3749587448_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &LayoutCache_t3120781045_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4180919198_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4180919198_0_0_0_Types[] = { &KeyValuePair_2_t4180919198_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4180919198_0_0_0 = { 1, GenInst_KeyValuePair_2_t4180919198_0_0_0_Types };
extern const Il2CppType GUIStyle_t1799908754_0_0_0;
static const Il2CppType* GenInst_GUIStyle_t1799908754_0_0_0_Types[] = { &GUIStyle_t1799908754_0_0_0 };
extern const Il2CppGenericInst GenInst_GUIStyle_t1799908754_0_0_0 = { 1, GenInst_GUIStyle_t1799908754_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_Types[] = { &String_t_0_0_0, &GUIStyle_t1799908754_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0 = { 2, GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &GUIStyle_t1799908754_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1472033238_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1472033238_0_0_0_Types[] = { &KeyValuePair_2_t1472033238_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1472033238_0_0_0 = { 1, GenInst_KeyValuePair_2_t1472033238_0_0_0_Types };
extern const Il2CppType Event_t3028476042_0_0_0;
extern const Il2CppType TextEditOp_t3138797698_0_0_0;
static const Il2CppType* GenInst_Event_t3028476042_0_0_0_TextEditOp_t3138797698_0_0_0_Types[] = { &Event_t3028476042_0_0_0, &TextEditOp_t3138797698_0_0_0 };
extern const Il2CppGenericInst GenInst_Event_t3028476042_0_0_0_TextEditOp_t3138797698_0_0_0 = { 2, GenInst_Event_t3028476042_0_0_0_TextEditOp_t3138797698_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_Types[] = { &Il2CppObject_0_0_0, &TextEditOp_t3138797698_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t488203048_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t488203048_0_0_0_Types[] = { &KeyValuePair_2_t488203048_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t488203048_0_0_0 = { 1, GenInst_KeyValuePair_2_t488203048_0_0_0_Types };
static const Il2CppType* GenInst_TextEditOp_t3138797698_0_0_0_Types[] = { &TextEditOp_t3138797698_0_0_0 };
extern const Il2CppGenericInst GenInst_TextEditOp_t3138797698_0_0_0 = { 1, GenInst_TextEditOp_t3138797698_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &TextEditOp_t3138797698_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_TextEditOp_t3138797698_0_0_0_Types[] = { &Il2CppObject_0_0_0, &TextEditOp_t3138797698_0_0_0, &TextEditOp_t3138797698_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_TextEditOp_t3138797698_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_TextEditOp_t3138797698_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Il2CppObject_0_0_0, &TextEditOp_t3138797698_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_KeyValuePair_2_t488203048_0_0_0_Types[] = { &Il2CppObject_0_0_0, &TextEditOp_t3138797698_0_0_0, &KeyValuePair_2_t488203048_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_KeyValuePair_2_t488203048_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_KeyValuePair_2_t488203048_0_0_0_Types };
static const Il2CppType* GenInst_Event_t3028476042_0_0_0_Types[] = { &Event_t3028476042_0_0_0 };
extern const Il2CppGenericInst GenInst_Event_t3028476042_0_0_0 = { 1, GenInst_Event_t3028476042_0_0_0_Types };
static const Il2CppType* GenInst_Event_t3028476042_0_0_0_TextEditOp_t3138797698_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Event_t3028476042_0_0_0, &TextEditOp_t3138797698_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Event_t3028476042_0_0_0_TextEditOp_t3138797698_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Event_t3028476042_0_0_0_TextEditOp_t3138797698_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3799506081_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3799506081_0_0_0_Types[] = { &KeyValuePair_2_t3799506081_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3799506081_0_0_0 = { 1, GenInst_KeyValuePair_2_t3799506081_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Il2CppObject_0_0_0_Types[] = { &String_t_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_String_t_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2361573779_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2361573779_0_0_0_Types[] = { &KeyValuePair_2_t2361573779_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2361573779_0_0_0 = { 1, GenInst_KeyValuePair_2_t2361573779_0_0_0_Types };
extern const Il2CppType DisallowMultipleComponent_t2656950_0_0_0;
static const Il2CppType* GenInst_DisallowMultipleComponent_t2656950_0_0_0_Types[] = { &DisallowMultipleComponent_t2656950_0_0_0 };
extern const Il2CppGenericInst GenInst_DisallowMultipleComponent_t2656950_0_0_0 = { 1, GenInst_DisallowMultipleComponent_t2656950_0_0_0_Types };
extern const Il2CppType Attribute_t542643598_0_0_0;
static const Il2CppType* GenInst_Attribute_t542643598_0_0_0_Types[] = { &Attribute_t542643598_0_0_0 };
extern const Il2CppGenericInst GenInst_Attribute_t542643598_0_0_0 = { 1, GenInst_Attribute_t542643598_0_0_0_Types };
extern const Il2CppType _Attribute_t1557664299_0_0_0;
static const Il2CppType* GenInst__Attribute_t1557664299_0_0_0_Types[] = { &_Attribute_t1557664299_0_0_0 };
extern const Il2CppGenericInst GenInst__Attribute_t1557664299_0_0_0 = { 1, GenInst__Attribute_t1557664299_0_0_0_Types };
extern const Il2CppType ExecuteInEditMode_t3043633143_0_0_0;
static const Il2CppType* GenInst_ExecuteInEditMode_t3043633143_0_0_0_Types[] = { &ExecuteInEditMode_t3043633143_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteInEditMode_t3043633143_0_0_0 = { 1, GenInst_ExecuteInEditMode_t3043633143_0_0_0_Types };
extern const Il2CppType RequireComponent_t864575032_0_0_0;
static const Il2CppType* GenInst_RequireComponent_t864575032_0_0_0_Types[] = { &RequireComponent_t864575032_0_0_0 };
extern const Il2CppGenericInst GenInst_RequireComponent_t864575032_0_0_0 = { 1, GenInst_RequireComponent_t864575032_0_0_0_Types };
extern const Il2CppType HitInfo_t1761367055_0_0_0;
static const Il2CppType* GenInst_HitInfo_t1761367055_0_0_0_Types[] = { &HitInfo_t1761367055_0_0_0 };
extern const Il2CppGenericInst GenInst_HitInfo_t1761367055_0_0_0 = { 1, GenInst_HitInfo_t1761367055_0_0_0_Types };
extern const Il2CppType PersistentCall_t3793436469_0_0_0;
static const Il2CppType* GenInst_PersistentCall_t3793436469_0_0_0_Types[] = { &PersistentCall_t3793436469_0_0_0 };
extern const Il2CppGenericInst GenInst_PersistentCall_t3793436469_0_0_0 = { 1, GenInst_PersistentCall_t3793436469_0_0_0_Types };
extern const Il2CppType BaseInvokableCall_t2229564840_0_0_0;
static const Il2CppType* GenInst_BaseInvokableCall_t2229564840_0_0_0_Types[] = { &BaseInvokableCall_t2229564840_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseInvokableCall_t2229564840_0_0_0 = { 1, GenInst_BaseInvokableCall_t2229564840_0_0_0_Types };
extern const Il2CppType Func_2_t3675469371_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_Func_2_t3675469371_0_0_0_Types[] = { &Type_t_0_0_0, &Func_2_t3675469371_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_Func_2_t3675469371_0_0_0 = { 2, GenInst_Type_t_0_0_0_Func_2_t3675469371_0_0_0_Types };
static const Il2CppType* GenInst_IntPtr_t_0_0_0_Il2CppObject_0_0_0_Types[] = { &IntPtr_t_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_IntPtr_t_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_Func_2_t3675469371_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Type_t_0_0_0, &Func_2_t3675469371_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_Func_2_t3675469371_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_Func_2_t3675469371_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3370172490_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3370172490_0_0_0_Types[] = { &KeyValuePair_2_t3370172490_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3370172490_0_0_0 = { 1, GenInst_KeyValuePair_2_t3370172490_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t888819835_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t888819835_0_0_0_Types[] = { &KeyValuePair_2_t888819835_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t888819835_0_0_0 = { 1, GenInst_KeyValuePair_2_t888819835_0_0_0_Types };
static const Il2CppType* GenInst_IntPtr_t_0_0_0_Il2CppObject_0_0_0_IntPtr_t_0_0_0_Types[] = { &IntPtr_t_0_0_0, &Il2CppObject_0_0_0, &IntPtr_t_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_Il2CppObject_0_0_0_IntPtr_t_0_0_0 = { 3, GenInst_IntPtr_t_0_0_0_Il2CppObject_0_0_0_IntPtr_t_0_0_0_Types };
static const Il2CppType* GenInst_IntPtr_t_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &IntPtr_t_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_IntPtr_t_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_IntPtr_t_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &IntPtr_t_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_IntPtr_t_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_IntPtr_t_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t888819835_0_0_0_Types[] = { &IntPtr_t_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t888819835_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t888819835_0_0_0 = { 3, GenInst_IntPtr_t_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t888819835_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Delegate_t3022476291_0_0_0_Types[] = { &String_t_0_0_0, &Delegate_t3022476291_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Delegate_t3022476291_0_0_0 = { 2, GenInst_String_t_0_0_0_Delegate_t3022476291_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Delegate_t3022476291_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &Delegate_t3022476291_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Delegate_t3022476291_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_Delegate_t3022476291_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2694600775_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2694600775_0_0_0_Types[] = { &KeyValuePair_2_t2694600775_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2694600775_0_0_0 = { 1, GenInst_KeyValuePair_2_t2694600775_0_0_0_Types };
extern const Il2CppType Dictionary_2_t1629922792_0_0_0;
static const Il2CppType* GenInst_IntPtr_t_0_0_0_Dictionary_2_t1629922792_0_0_0_Types[] = { &IntPtr_t_0_0_0, &Dictionary_2_t1629922792_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_Dictionary_2_t1629922792_0_0_0 = { 2, GenInst_IntPtr_t_0_0_0_Dictionary_2_t1629922792_0_0_0_Types };
extern const Il2CppType Methods_t1187897474_0_0_0;
static const Il2CppType* GenInst_IntPtr_t_0_0_0_Methods_t1187897474_0_0_0_Types[] = { &IntPtr_t_0_0_0, &Methods_t1187897474_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_Methods_t1187897474_0_0_0 = { 2, GenInst_IntPtr_t_0_0_0_Methods_t1187897474_0_0_0_Types };
static const Il2CppType* GenInst_IntPtr_t_0_0_0_Methods_t1187897474_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &IntPtr_t_0_0_0, &Methods_t1187897474_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_Methods_t1187897474_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_IntPtr_t_0_0_0_Methods_t1187897474_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3682235310_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3682235310_0_0_0_Types[] = { &KeyValuePair_2_t3682235310_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3682235310_0_0_0 = { 1, GenInst_KeyValuePair_2_t3682235310_0_0_0_Types };
static const Il2CppType* GenInst_IntPtr_t_0_0_0_Dictionary_2_t1629922792_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &IntPtr_t_0_0_0, &Dictionary_2_t1629922792_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_Dictionary_2_t1629922792_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_IntPtr_t_0_0_0_Dictionary_2_t1629922792_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4124260628_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4124260628_0_0_0_Types[] = { &KeyValuePair_2_t4124260628_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4124260628_0_0_0 = { 1, GenInst_KeyValuePair_2_t4124260628_0_0_0_Types };
extern const Il2CppType JsonData_t4263252052_0_0_0;
static const Il2CppType* GenInst_JsonData_t4263252052_0_0_0_Types[] = { &JsonData_t4263252052_0_0_0 };
extern const Il2CppGenericInst GenInst_JsonData_t4263252052_0_0_0 = { 1, GenInst_JsonData_t4263252052_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3935376536_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3935376536_0_0_0_Types[] = { &KeyValuePair_2_t3935376536_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3935376536_0_0_0 = { 1, GenInst_KeyValuePair_2_t3935376536_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_JsonData_t4263252052_0_0_0_Types[] = { &String_t_0_0_0, &JsonData_t4263252052_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_JsonData_t4263252052_0_0_0 = { 2, GenInst_String_t_0_0_0_JsonData_t4263252052_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_JsonData_t4263252052_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &JsonData_t4263252052_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_JsonData_t4263252052_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_JsonData_t4263252052_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType PropertyMetadata_t3287739986_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_PropertyMetadata_t3287739986_0_0_0_Types[] = { &String_t_0_0_0, &PropertyMetadata_t3287739986_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_PropertyMetadata_t3287739986_0_0_0 = { 2, GenInst_String_t_0_0_0_PropertyMetadata_t3287739986_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_PropertyMetadata_t3287739986_0_0_0_Types[] = { &Il2CppObject_0_0_0, &PropertyMetadata_t3287739986_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_PropertyMetadata_t3287739986_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_PropertyMetadata_t3287739986_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t637145336_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t637145336_0_0_0_Types[] = { &KeyValuePair_2_t637145336_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t637145336_0_0_0 = { 1, GenInst_KeyValuePair_2_t637145336_0_0_0_Types };
extern const Il2CppType ExporterFunc_t173265409_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_ExporterFunc_t173265409_0_0_0_Types[] = { &Type_t_0_0_0, &ExporterFunc_t173265409_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ExporterFunc_t173265409_0_0_0 = { 2, GenInst_Type_t_0_0_0_ExporterFunc_t173265409_0_0_0_Types };
extern const Il2CppType IDictionary_2_t787128596_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_IDictionary_2_t787128596_0_0_0_Types[] = { &Type_t_0_0_0, &IDictionary_2_t787128596_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IDictionary_2_t787128596_0_0_0 = { 2, GenInst_Type_t_0_0_0_IDictionary_2_t787128596_0_0_0_Types };
extern const Il2CppType ImporterFunc_t850687278_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_ImporterFunc_t850687278_0_0_0_Types[] = { &Type_t_0_0_0, &ImporterFunc_t850687278_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ImporterFunc_t850687278_0_0_0 = { 2, GenInst_Type_t_0_0_0_ImporterFunc_t850687278_0_0_0_Types };
extern const Il2CppType ArrayMetadata_t1135078014_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_ArrayMetadata_t1135078014_0_0_0_Types[] = { &Type_t_0_0_0, &ArrayMetadata_t1135078014_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ArrayMetadata_t1135078014_0_0_0 = { 2, GenInst_Type_t_0_0_0_ArrayMetadata_t1135078014_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ArrayMetadata_t1135078014_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ArrayMetadata_t1135078014_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ArrayMetadata_t1135078014_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_ArrayMetadata_t1135078014_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2779450660_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2779450660_0_0_0_Types[] = { &KeyValuePair_2_t2779450660_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2779450660_0_0_0 = { 1, GenInst_KeyValuePair_2_t2779450660_0_0_0_Types };
extern const Il2CppType IDictionary_2_t3266987655_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_IDictionary_2_t3266987655_0_0_0_Types[] = { &Type_t_0_0_0, &IDictionary_2_t3266987655_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IDictionary_2_t3266987655_0_0_0 = { 2, GenInst_Type_t_0_0_0_IDictionary_2_t3266987655_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_MethodInfo_t_0_0_0_Types[] = { &Type_t_0_0_0, &MethodInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_MethodInfo_t_0_0_0 = { 2, GenInst_Type_t_0_0_0_MethodInfo_t_0_0_0_Types };
extern const Il2CppType ObjectMetadata_t4058137740_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_ObjectMetadata_t4058137740_0_0_0_Types[] = { &Type_t_0_0_0, &ObjectMetadata_t4058137740_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ObjectMetadata_t4058137740_0_0_0 = { 2, GenInst_Type_t_0_0_0_ObjectMetadata_t4058137740_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ObjectMetadata_t4058137740_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ObjectMetadata_t4058137740_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ObjectMetadata_t4058137740_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_ObjectMetadata_t4058137740_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1407543090_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1407543090_0_0_0_Types[] = { &KeyValuePair_2_t1407543090_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1407543090_0_0_0 = { 1, GenInst_KeyValuePair_2_t1407543090_0_0_0_Types };
extern const Il2CppType IList_1_t3828680587_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_IList_1_t3828680587_0_0_0_Types[] = { &Type_t_0_0_0, &IList_1_t3828680587_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IList_1_t3828680587_0_0_0 = { 2, GenInst_Type_t_0_0_0_IList_1_t3828680587_0_0_0_Types };
static const Il2CppType* GenInst_PropertyMetadata_t3287739986_0_0_0_Types[] = { &PropertyMetadata_t3287739986_0_0_0 };
extern const Il2CppGenericInst GenInst_PropertyMetadata_t3287739986_0_0_0 = { 1, GenInst_PropertyMetadata_t3287739986_0_0_0_Types };
static const Il2CppType* GenInst_ArrayMetadata_t1135078014_0_0_0_Types[] = { &ArrayMetadata_t1135078014_0_0_0 };
extern const Il2CppGenericInst GenInst_ArrayMetadata_t1135078014_0_0_0 = { 1, GenInst_ArrayMetadata_t1135078014_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ArrayMetadata_t1135078014_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ArrayMetadata_t1135078014_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ArrayMetadata_t1135078014_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_ArrayMetadata_t1135078014_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ArrayMetadata_t1135078014_0_0_0_ArrayMetadata_t1135078014_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ArrayMetadata_t1135078014_0_0_0, &ArrayMetadata_t1135078014_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ArrayMetadata_t1135078014_0_0_0_ArrayMetadata_t1135078014_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_ArrayMetadata_t1135078014_0_0_0_ArrayMetadata_t1135078014_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ArrayMetadata_t1135078014_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ArrayMetadata_t1135078014_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ArrayMetadata_t1135078014_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_ArrayMetadata_t1135078014_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ArrayMetadata_t1135078014_0_0_0_KeyValuePair_2_t2779450660_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ArrayMetadata_t1135078014_0_0_0, &KeyValuePair_2_t2779450660_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ArrayMetadata_t1135078014_0_0_0_KeyValuePair_2_t2779450660_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_ArrayMetadata_t1135078014_0_0_0_KeyValuePair_2_t2779450660_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_ArrayMetadata_t1135078014_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Type_t_0_0_0, &ArrayMetadata_t1135078014_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ArrayMetadata_t1135078014_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_ArrayMetadata_t1135078014_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t829781133_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t829781133_0_0_0_Types[] = { &KeyValuePair_2_t829781133_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t829781133_0_0_0 = { 1, GenInst_KeyValuePair_2_t829781133_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_IDictionary_2_t3266987655_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Type_t_0_0_0, &IDictionary_2_t3266987655_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IDictionary_2_t3266987655_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_IDictionary_2_t3266987655_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2961690774_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2961690774_0_0_0_Types[] = { &KeyValuePair_2_t2961690774_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2961690774_0_0_0 = { 1, GenInst_KeyValuePair_2_t2961690774_0_0_0_Types };
static const Il2CppType* GenInst_ObjectMetadata_t4058137740_0_0_0_Types[] = { &ObjectMetadata_t4058137740_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectMetadata_t4058137740_0_0_0 = { 1, GenInst_ObjectMetadata_t4058137740_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ObjectMetadata_t4058137740_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ObjectMetadata_t4058137740_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ObjectMetadata_t4058137740_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_ObjectMetadata_t4058137740_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ObjectMetadata_t4058137740_0_0_0_ObjectMetadata_t4058137740_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ObjectMetadata_t4058137740_0_0_0, &ObjectMetadata_t4058137740_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ObjectMetadata_t4058137740_0_0_0_ObjectMetadata_t4058137740_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_ObjectMetadata_t4058137740_0_0_0_ObjectMetadata_t4058137740_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ObjectMetadata_t4058137740_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ObjectMetadata_t4058137740_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ObjectMetadata_t4058137740_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_ObjectMetadata_t4058137740_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ObjectMetadata_t4058137740_0_0_0_KeyValuePair_2_t1407543090_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ObjectMetadata_t4058137740_0_0_0, &KeyValuePair_2_t1407543090_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ObjectMetadata_t4058137740_0_0_0_KeyValuePair_2_t1407543090_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_ObjectMetadata_t4058137740_0_0_0_KeyValuePair_2_t1407543090_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_ObjectMetadata_t4058137740_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Type_t_0_0_0, &ObjectMetadata_t4058137740_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ObjectMetadata_t4058137740_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_ObjectMetadata_t4058137740_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3752840859_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3752840859_0_0_0_Types[] = { &KeyValuePair_2_t3752840859_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3752840859_0_0_0 = { 1, GenInst_KeyValuePair_2_t3752840859_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_IList_1_t3828680587_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Type_t_0_0_0, &IList_1_t3828680587_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IList_1_t3828680587_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_IList_1_t3828680587_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3523383706_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3523383706_0_0_0_Types[] = { &KeyValuePair_2_t3523383706_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3523383706_0_0_0 = { 1, GenInst_KeyValuePair_2_t3523383706_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_ExporterFunc_t173265409_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Type_t_0_0_0, &ExporterFunc_t173265409_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ExporterFunc_t173265409_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_ExporterFunc_t173265409_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4162935824_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4162935824_0_0_0_Types[] = { &KeyValuePair_2_t4162935824_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4162935824_0_0_0 = { 1, GenInst_KeyValuePair_2_t4162935824_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_IDictionary_2_t787128596_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Type_t_0_0_0, &IDictionary_2_t787128596_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IDictionary_2_t787128596_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_IDictionary_2_t787128596_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t481831715_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t481831715_0_0_0_Types[] = { &KeyValuePair_2_t481831715_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t481831715_0_0_0 = { 1, GenInst_KeyValuePair_2_t481831715_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_ImporterFunc_t850687278_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Type_t_0_0_0, &ImporterFunc_t850687278_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ImporterFunc_t850687278_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_ImporterFunc_t850687278_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t545390397_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t545390397_0_0_0_Types[] = { &KeyValuePair_2_t545390397_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t545390397_0_0_0 = { 1, GenInst_KeyValuePair_2_t545390397_0_0_0_Types };
extern const Il2CppType JsonToken_t2445581255_0_0_0;
static const Il2CppType* GenInst_JsonToken_t2445581255_0_0_0_Types[] = { &JsonToken_t2445581255_0_0_0 };
extern const Il2CppGenericInst GenInst_JsonToken_t2445581255_0_0_0 = { 1, GenInst_JsonToken_t2445581255_0_0_0_Types };
extern const Il2CppType WriterContext_t1209007092_0_0_0;
static const Il2CppType* GenInst_WriterContext_t1209007092_0_0_0_Types[] = { &WriterContext_t1209007092_0_0_0 };
extern const Il2CppGenericInst GenInst_WriterContext_t1209007092_0_0_0 = { 1, GenInst_WriterContext_t1209007092_0_0_0_Types };
extern const Il2CppType StateHandler_t3489987002_0_0_0;
static const Il2CppType* GenInst_StateHandler_t3489987002_0_0_0_Types[] = { &StateHandler_t3489987002_0_0_0 };
extern const Il2CppGenericInst GenInst_StateHandler_t3489987002_0_0_0 = { 1, GenInst_StateHandler_t3489987002_0_0_0_Types };
extern const Il2CppType MulticastDelegate_t3201952435_0_0_0;
static const Il2CppType* GenInst_MulticastDelegate_t3201952435_0_0_0_Types[] = { &MulticastDelegate_t3201952435_0_0_0 };
extern const Il2CppGenericInst GenInst_MulticastDelegate_t3201952435_0_0_0 = { 1, GenInst_MulticastDelegate_t3201952435_0_0_0_Types };
extern const Il2CppType List_1_t2784070411_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_List_1_t2784070411_0_0_0_Types[] = { &String_t_0_0_0, &List_1_t2784070411_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t2784070411_0_0_0 = { 2, GenInst_String_t_0_0_0_List_1_t2784070411_0_0_0_Types };
extern const Il2CppType TraceListener_t3414949279_0_0_0;
static const Il2CppType* GenInst_TraceListener_t3414949279_0_0_0_Types[] = { &TraceListener_t3414949279_0_0_0 };
extern const Il2CppGenericInst GenInst_TraceListener_t3414949279_0_0_0 = { 1, GenInst_TraceListener_t3414949279_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_List_1_t2784070411_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &List_1_t2784070411_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t2784070411_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_List_1_t2784070411_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2456194895_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2456194895_0_0_0_Types[] = { &KeyValuePair_2_t2456194895_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2456194895_0_0_0 = { 1, GenInst_KeyValuePair_2_t2456194895_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_XElement_t553821050_0_0_0_Types[] = { &String_t_0_0_0, &XElement_t553821050_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_XElement_t553821050_0_0_0 = { 2, GenInst_String_t_0_0_0_XElement_t553821050_0_0_0_Types };
static const Il2CppType* GenInst_XAttribute_t3858477518_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &XAttribute_t3858477518_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_XAttribute_t3858477518_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_XAttribute_t3858477518_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const Il2CppType* GenInst_XElement_t553821050_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &XElement_t553821050_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_XElement_t553821050_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_XElement_t553821050_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_XElement_t553821050_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &XElement_t553821050_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_XElement_t553821050_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_XElement_t553821050_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t225945534_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t225945534_0_0_0_Types[] = { &KeyValuePair_2_t225945534_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t225945534_0_0_0 = { 1, GenInst_KeyValuePair_2_t225945534_0_0_0_Types };
extern const Il2CppType RegionEndpoint_t661522805_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_RegionEndpoint_t661522805_0_0_0_Types[] = { &String_t_0_0_0, &RegionEndpoint_t661522805_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_RegionEndpoint_t661522805_0_0_0 = { 2, GenInst_String_t_0_0_0_RegionEndpoint_t661522805_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_RegionEndpoint_t661522805_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &RegionEndpoint_t661522805_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_RegionEndpoint_t661522805_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_RegionEndpoint_t661522805_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t333647289_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t333647289_0_0_0_Types[] = { &KeyValuePair_2_t333647289_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t333647289_0_0_0 = { 1, GenInst_KeyValuePair_2_t333647289_0_0_0_Types };
extern const Il2CppType RegionEndpoint_t1885241449_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_RegionEndpoint_t1885241449_0_0_0_Types[] = { &String_t_0_0_0, &RegionEndpoint_t1885241449_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_RegionEndpoint_t1885241449_0_0_0 = { 2, GenInst_String_t_0_0_0_RegionEndpoint_t1885241449_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_RegionEndpoint_t1885241449_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &RegionEndpoint_t1885241449_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_RegionEndpoint_t1885241449_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_RegionEndpoint_t1885241449_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1557365933_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1557365933_0_0_0_Types[] = { &KeyValuePair_2_t1557365933_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1557365933_0_0_0 = { 1, GenInst_KeyValuePair_2_t1557365933_0_0_0_Types };
extern const Il2CppType Endpoint_t3348692641_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_Endpoint_t3348692641_0_0_0_Types[] = { &String_t_0_0_0, &Endpoint_t3348692641_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Endpoint_t3348692641_0_0_0 = { 2, GenInst_String_t_0_0_0_Endpoint_t3348692641_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Endpoint_t3348692641_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &Endpoint_t3348692641_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Endpoint_t3348692641_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_Endpoint_t3348692641_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3020817125_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3020817125_0_0_0_Types[] = { &KeyValuePair_2_t3020817125_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3020817125_0_0_0 = { 1, GenInst_KeyValuePair_2_t3020817125_0_0_0_Types };
extern const Il2CppType IRegionEndpoint_t544433888_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_IRegionEndpoint_t544433888_0_0_0_Types[] = { &String_t_0_0_0, &IRegionEndpoint_t544433888_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_IRegionEndpoint_t544433888_0_0_0 = { 2, GenInst_String_t_0_0_0_IRegionEndpoint_t544433888_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_IRegionEndpoint_t544433888_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &IRegionEndpoint_t544433888_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_IRegionEndpoint_t544433888_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_IRegionEndpoint_t544433888_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t216558372_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t216558372_0_0_0_Types[] = { &KeyValuePair_2_t216558372_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t216558372_0_0_0 = { 1, GenInst_KeyValuePair_2_t216558372_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_String_t_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_String_t_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &String_t_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3089358386_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3089358386_0_0_0_Types[] = { &KeyValuePair_2_t3089358386_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3089358386_0_0_0 = { 1, GenInst_KeyValuePair_2_t3089358386_0_0_0_Types };
extern const Il2CppType Action_t3226471752_0_0_0;
static const Il2CppType* GenInst_Action_t3226471752_0_0_0_Types[] = { &Action_t3226471752_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_t3226471752_0_0_0 = { 1, GenInst_Action_t3226471752_0_0_0_Types };
extern const Il2CppType InstantiationModel_t1632807378_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_InstantiationModel_t1632807378_0_0_0_Types[] = { &Type_t_0_0_0, &InstantiationModel_t1632807378_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_InstantiationModel_t1632807378_0_0_0 = { 2, GenInst_Type_t_0_0_0_InstantiationModel_t1632807378_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_InstantiationModel_t1632807378_0_0_0_Types[] = { &Il2CppObject_0_0_0, &InstantiationModel_t1632807378_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_InstantiationModel_t1632807378_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_InstantiationModel_t1632807378_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3277180024_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3277180024_0_0_0_Types[] = { &KeyValuePair_2_t3277180024_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3277180024_0_0_0 = { 1, GenInst_KeyValuePair_2_t3277180024_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_Il2CppObject_0_0_0_Types[] = { &Type_t_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Type_t_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_Type_t_0_0_0_Types[] = { &Type_t_0_0_0, &Type_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_Type_t_0_0_0 = { 2, GenInst_Type_t_0_0_0_Type_t_0_0_0_Types };
extern const Il2CppType ITypeInfo_t3592676621_0_0_0;
static const Il2CppType* GenInst_ITypeInfo_t3592676621_0_0_0_Types[] = { &ITypeInfo_t3592676621_0_0_0 };
extern const Il2CppGenericInst GenInst_ITypeInfo_t3592676621_0_0_0 = { 1, GenInst_ITypeInfo_t3592676621_0_0_0_Types };
static const Il2CppType* GenInst_MethodInfo_t_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &MethodInfo_t_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodInfo_t_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_MethodInfo_t_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType NetworkStatusEventArgs_t1607167653_0_0_0;
static const Il2CppType* GenInst_NetworkStatusEventArgs_t1607167653_0_0_0_Types[] = { &NetworkStatusEventArgs_t1607167653_0_0_0 };
extern const Il2CppGenericInst GenInst_NetworkStatusEventArgs_t1607167653_0_0_0 = { 1, GenInst_NetworkStatusEventArgs_t1607167653_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1327510497_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1327510497_0_0_0_Types[] = { &KeyValuePair_2_t1327510497_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1327510497_0_0_0 = { 1, GenInst_KeyValuePair_2_t1327510497_0_0_0_Types };
static const Il2CppType* GenInst_InstantiationModel_t1632807378_0_0_0_Types[] = { &InstantiationModel_t1632807378_0_0_0 };
extern const Il2CppGenericInst GenInst_InstantiationModel_t1632807378_0_0_0 = { 1, GenInst_InstantiationModel_t1632807378_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_InstantiationModel_t1632807378_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &InstantiationModel_t1632807378_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_InstantiationModel_t1632807378_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_InstantiationModel_t1632807378_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_InstantiationModel_t1632807378_0_0_0_InstantiationModel_t1632807378_0_0_0_Types[] = { &Il2CppObject_0_0_0, &InstantiationModel_t1632807378_0_0_0, &InstantiationModel_t1632807378_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_InstantiationModel_t1632807378_0_0_0_InstantiationModel_t1632807378_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_InstantiationModel_t1632807378_0_0_0_InstantiationModel_t1632807378_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_InstantiationModel_t1632807378_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Il2CppObject_0_0_0, &InstantiationModel_t1632807378_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_InstantiationModel_t1632807378_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_InstantiationModel_t1632807378_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_InstantiationModel_t1632807378_0_0_0_KeyValuePair_2_t3277180024_0_0_0_Types[] = { &Il2CppObject_0_0_0, &InstantiationModel_t1632807378_0_0_0, &KeyValuePair_2_t3277180024_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_InstantiationModel_t1632807378_0_0_0_KeyValuePair_2_t3277180024_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_InstantiationModel_t1632807378_0_0_0_KeyValuePair_2_t3277180024_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_InstantiationModel_t1632807378_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Type_t_0_0_0, &InstantiationModel_t1632807378_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_InstantiationModel_t1632807378_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_InstantiationModel_t1632807378_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Type_t_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2384152414_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2384152414_0_0_0_Types[] = { &KeyValuePair_2_t2384152414_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2384152414_0_0_0 = { 1, GenInst_KeyValuePair_2_t2384152414_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_Type_t_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Type_t_0_0_0, &Type_t_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_Type_t_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_Type_t_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t998506345_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t998506345_0_0_0_Types[] = { &KeyValuePair_2_t998506345_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t998506345_0_0_0 = { 1, GenInst_KeyValuePair_2_t998506345_0_0_0_Types };
extern const Il2CppType IRequest_t2400804350_0_0_0;
extern const Il2CppType AmazonWebServiceRequest_t3384026212_0_0_0;
static const Il2CppType* GenInst_IRequest_t2400804350_0_0_0_AmazonWebServiceRequest_t3384026212_0_0_0_Types[] = { &IRequest_t2400804350_0_0_0, &AmazonWebServiceRequest_t3384026212_0_0_0 };
extern const Il2CppGenericInst GenInst_IRequest_t2400804350_0_0_0_AmazonWebServiceRequest_t3384026212_0_0_0 = { 2, GenInst_IRequest_t2400804350_0_0_0_AmazonWebServiceRequest_t3384026212_0_0_0_Types };
extern const Il2CppType AmazonWebServiceResponse_t529043356_0_0_0;
extern const Il2CppType AsyncOptions_t558351272_0_0_0;
static const Il2CppType* GenInst_AmazonWebServiceRequest_t3384026212_0_0_0_AmazonWebServiceResponse_t529043356_0_0_0_Exception_t1927440687_0_0_0_AsyncOptions_t558351272_0_0_0_Types[] = { &AmazonWebServiceRequest_t3384026212_0_0_0, &AmazonWebServiceResponse_t529043356_0_0_0, &Exception_t1927440687_0_0_0, &AsyncOptions_t558351272_0_0_0 };
extern const Il2CppGenericInst GenInst_AmazonWebServiceRequest_t3384026212_0_0_0_AmazonWebServiceResponse_t529043356_0_0_0_Exception_t1927440687_0_0_0_AsyncOptions_t558351272_0_0_0 = { 4, GenInst_AmazonWebServiceRequest_t3384026212_0_0_0_AmazonWebServiceResponse_t529043356_0_0_0_Exception_t1927440687_0_0_0_AsyncOptions_t558351272_0_0_0_Types };
extern const Il2CppType StreamTransferProgressArgs_t2639638063_0_0_0;
static const Il2CppType* GenInst_StreamTransferProgressArgs_t2639638063_0_0_0_Types[] = { &StreamTransferProgressArgs_t2639638063_0_0_0 };
extern const Il2CppGenericInst GenInst_StreamTransferProgressArgs_t2639638063_0_0_0 = { 1, GenInst_StreamTransferProgressArgs_t2639638063_0_0_0_Types };
extern const Il2CppType IExecutionContext_t2477130752_0_0_0;
static const Il2CppType* GenInst_IExecutionContext_t2477130752_0_0_0_Types[] = { &IExecutionContext_t2477130752_0_0_0 };
extern const Il2CppGenericInst GenInst_IExecutionContext_t2477130752_0_0_0 = { 1, GenInst_IExecutionContext_t2477130752_0_0_0_Types };
static const Il2CppType* GenInst_IExecutionContext_t2477130752_0_0_0_Exception_t1927440687_0_0_0_Types[] = { &IExecutionContext_t2477130752_0_0_0, &Exception_t1927440687_0_0_0 };
extern const Il2CppGenericInst GenInst_IExecutionContext_t2477130752_0_0_0_Exception_t1927440687_0_0_0 = { 2, GenInst_IExecutionContext_t2477130752_0_0_0_Exception_t1927440687_0_0_0_Types };
extern const Il2CppType IPipelineHandler_t2320756001_0_0_0;
static const Il2CppType* GenInst_IPipelineHandler_t2320756001_0_0_0_Types[] = { &IPipelineHandler_t2320756001_0_0_0 };
extern const Il2CppGenericInst GenInst_IPipelineHandler_t2320756001_0_0_0 = { 1, GenInst_IPipelineHandler_t2320756001_0_0_0_Types };
extern const Il2CppType RegionGenerator_t3123742622_0_0_0;
static const Il2CppType* GenInst_RegionGenerator_t3123742622_0_0_0_Types[] = { &RegionGenerator_t3123742622_0_0_0 };
extern const Il2CppGenericInst GenInst_RegionGenerator_t3123742622_0_0_0 = { 1, GenInst_RegionGenerator_t3123742622_0_0_0_Types };
extern const Il2CppType Dictionary_2_t1620371852_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_Dictionary_2_t1620371852_0_0_0_Types[] = { &Type_t_0_0_0, &Dictionary_2_t1620371852_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_Dictionary_2_t1620371852_0_0_0 = { 2, GenInst_Type_t_0_0_0_Dictionary_2_t1620371852_0_0_0_Types };
extern const Il2CppType ConstantClass_t4000559886_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_ConstantClass_t4000559886_0_0_0_Types[] = { &String_t_0_0_0, &ConstantClass_t4000559886_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_ConstantClass_t4000559886_0_0_0 = { 2, GenInst_String_t_0_0_0_ConstantClass_t4000559886_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_ConstantClass_t4000559886_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &ConstantClass_t4000559886_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_ConstantClass_t4000559886_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_ConstantClass_t4000559886_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3672684370_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3672684370_0_0_0_Types[] = { &KeyValuePair_2_t3672684370_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3672684370_0_0_0 = { 1, GenInst_KeyValuePair_2_t3672684370_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_Dictionary_2_t1620371852_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Type_t_0_0_0, &Dictionary_2_t1620371852_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_Dictionary_2_t1620371852_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_Dictionary_2_t1620371852_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1315074971_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1315074971_0_0_0_Types[] = { &KeyValuePair_2_t1315074971_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1315074971_0_0_0 = { 1, GenInst_KeyValuePair_2_t1315074971_0_0_0_Types };
extern const Il2CppType RetryCapacity_t2794668894_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_RetryCapacity_t2794668894_0_0_0_Types[] = { &String_t_0_0_0, &RetryCapacity_t2794668894_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_RetryCapacity_t2794668894_0_0_0 = { 2, GenInst_String_t_0_0_0_RetryCapacity_t2794668894_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_RetryCapacity_t2794668894_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &RetryCapacity_t2794668894_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_RetryCapacity_t2794668894_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_RetryCapacity_t2794668894_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2466793378_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2466793378_0_0_0_Types[] = { &KeyValuePair_2_t2466793378_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2466793378_0_0_0 = { 1, GenInst_KeyValuePair_2_t2466793378_0_0_0_Types };
extern const Il2CppType Stream_t3255436806_0_0_0;
static const Il2CppType* GenInst_Stream_t3255436806_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Stream_t3255436806_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Stream_t3255436806_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Stream_t3255436806_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType IExceptionHandler_t1411014698_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_IExceptionHandler_t1411014698_0_0_0_Types[] = { &Type_t_0_0_0, &IExceptionHandler_t1411014698_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IExceptionHandler_t1411014698_0_0_0 = { 2, GenInst_Type_t_0_0_0_IExceptionHandler_t1411014698_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_IExceptionHandler_t1411014698_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Type_t_0_0_0, &IExceptionHandler_t1411014698_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IExceptionHandler_t1411014698_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_IExceptionHandler_t1411014698_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1105717817_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1105717817_0_0_0_Types[] = { &KeyValuePair_2_t1105717817_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1105717817_0_0_0 = { 1, GenInst_KeyValuePair_2_t1105717817_0_0_0_Types };
extern const Il2CppType HttpErrorResponseException_t3191555406_0_0_0;
static const Il2CppType* GenInst_HttpErrorResponseException_t3191555406_0_0_0_Types[] = { &HttpErrorResponseException_t3191555406_0_0_0 };
extern const Il2CppGenericInst GenInst_HttpErrorResponseException_t3191555406_0_0_0 = { 1, GenInst_HttpErrorResponseException_t3191555406_0_0_0_Types };
extern const Il2CppType HttpStatusCode_t1898409641_0_0_0;
static const Il2CppType* GenInst_HttpStatusCode_t1898409641_0_0_0_Types[] = { &HttpStatusCode_t1898409641_0_0_0 };
extern const Il2CppGenericInst GenInst_HttpStatusCode_t1898409641_0_0_0 = { 1, GenInst_HttpStatusCode_t1898409641_0_0_0_Types };
extern const Il2CppType WebExceptionStatus_t1169373531_0_0_0;
static const Il2CppType* GenInst_WebExceptionStatus_t1169373531_0_0_0_Types[] = { &WebExceptionStatus_t1169373531_0_0_0 };
extern const Il2CppGenericInst GenInst_WebExceptionStatus_t1169373531_0_0_0 = { 1, GenInst_WebExceptionStatus_t1169373531_0_0_0_Types };
extern const Il2CppType Link_t74093617_0_0_0;
static const Il2CppType* GenInst_Link_t74093617_0_0_0_Types[] = { &Link_t74093617_0_0_0 };
extern const Il2CppGenericInst GenInst_Link_t74093617_0_0_0 = { 1, GenInst_Link_t74093617_0_0_0_Types };
extern const Il2CppType Link_t3640024803_0_0_0;
static const Il2CppType* GenInst_Link_t3640024803_0_0_0_Types[] = { &Link_t3640024803_0_0_0 };
extern const Il2CppGenericInst GenInst_Link_t3640024803_0_0_0 = { 1, GenInst_Link_t3640024803_0_0_0_Types };
extern const Il2CppType IAsyncExecutionContext_t3792344986_0_0_0;
static const Il2CppType* GenInst_IAsyncExecutionContext_t3792344986_0_0_0_Types[] = { &IAsyncExecutionContext_t3792344986_0_0_0 };
extern const Il2CppGenericInst GenInst_IAsyncExecutionContext_t3792344986_0_0_0 = { 1, GenInst_IAsyncExecutionContext_t3792344986_0_0_0_Types };
extern const Il2CppType ThreadPoolOptions_1_t1583506835_0_0_0;
static const Il2CppType* GenInst_ThreadPoolOptions_1_t1583506835_0_0_0_Types[] = { &ThreadPoolOptions_1_t1583506835_0_0_0 };
extern const Il2CppGenericInst GenInst_ThreadPoolOptions_1_t1583506835_0_0_0 = { 1, GenInst_ThreadPoolOptions_1_t1583506835_0_0_0_Types };
static const Il2CppType* GenInst_Exception_t1927440687_0_0_0_Il2CppObject_0_0_0_Types[] = { &Exception_t1927440687_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Exception_t1927440687_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Exception_t1927440687_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType ThreadPoolOptions_1_t3222238215_0_0_0;
static const Il2CppType* GenInst_ThreadPoolOptions_1_t3222238215_0_0_0_Types[] = { &ThreadPoolOptions_1_t3222238215_0_0_0 };
extern const Il2CppGenericInst GenInst_ThreadPoolOptions_1_t3222238215_0_0_0 = { 1, GenInst_ThreadPoolOptions_1_t3222238215_0_0_0_Types };
static const Il2CppType* GenInst_IAsyncExecutionContext_t3792344986_0_0_0_IAsyncExecutionContext_t3792344986_0_0_0_Types[] = { &IAsyncExecutionContext_t3792344986_0_0_0, &IAsyncExecutionContext_t3792344986_0_0_0 };
extern const Il2CppGenericInst GenInst_IAsyncExecutionContext_t3792344986_0_0_0_IAsyncExecutionContext_t3792344986_0_0_0 = { 2, GenInst_IAsyncExecutionContext_t3792344986_0_0_0_IAsyncExecutionContext_t3792344986_0_0_0_Types };
static const Il2CppType* GenInst_Exception_t1927440687_0_0_0_IAsyncExecutionContext_t3792344986_0_0_0_Types[] = { &Exception_t1927440687_0_0_0, &IAsyncExecutionContext_t3792344986_0_0_0 };
extern const Il2CppGenericInst GenInst_Exception_t1927440687_0_0_0_IAsyncExecutionContext_t3792344986_0_0_0 = { 2, GenInst_Exception_t1927440687_0_0_0_IAsyncExecutionContext_t3792344986_0_0_0_Types };
extern const Il2CppType IUnityHttpRequest_t1859903397_0_0_0;
static const Il2CppType* GenInst_IUnityHttpRequest_t1859903397_0_0_0_Types[] = { &IUnityHttpRequest_t1859903397_0_0_0 };
extern const Il2CppGenericInst GenInst_IUnityHttpRequest_t1859903397_0_0_0 = { 1, GenInst_IUnityHttpRequest_t1859903397_0_0_0_Types };
extern const Il2CppType RuntimeAsyncResult_t4279356013_0_0_0;
static const Il2CppType* GenInst_RuntimeAsyncResult_t4279356013_0_0_0_Types[] = { &RuntimeAsyncResult_t4279356013_0_0_0 };
extern const Il2CppGenericInst GenInst_RuntimeAsyncResult_t4279356013_0_0_0 = { 1, GenInst_RuntimeAsyncResult_t4279356013_0_0_0_Types };
extern const Il2CppType Logger_t2262497814_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_Logger_t2262497814_0_0_0_Types[] = { &Type_t_0_0_0, &Logger_t2262497814_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_Logger_t2262497814_0_0_0 = { 2, GenInst_Type_t_0_0_0_Logger_t2262497814_0_0_0_Types };
extern const Il2CppType InternalLogger_t2972373343_0_0_0;
static const Il2CppType* GenInst_InternalLogger_t2972373343_0_0_0_Types[] = { &InternalLogger_t2972373343_0_0_0 };
extern const Il2CppGenericInst GenInst_InternalLogger_t2972373343_0_0_0 = { 1, GenInst_InternalLogger_t2972373343_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_Logger_t2262497814_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Type_t_0_0_0, &Logger_t2262497814_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_Logger_t2262497814_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_Logger_t2262497814_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1957200933_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1957200933_0_0_0_Types[] = { &KeyValuePair_2_t1957200933_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1957200933_0_0_0 = { 1, GenInst_KeyValuePair_2_t1957200933_0_0_0_Types };
extern const Il2CppType Metric_t3273440202_0_0_0;
extern const Il2CppType List_1_t2058570427_0_0_0;
static const Il2CppType* GenInst_Metric_t3273440202_0_0_0_List_1_t2058570427_0_0_0_Types[] = { &Metric_t3273440202_0_0_0, &List_1_t2058570427_0_0_0 };
extern const Il2CppGenericInst GenInst_Metric_t3273440202_0_0_0_List_1_t2058570427_0_0_0 = { 2, GenInst_Metric_t3273440202_0_0_0_List_1_t2058570427_0_0_0_Types };
static const Il2CppType* GenInst_Metric_t3273440202_0_0_0_Il2CppObject_0_0_0_Types[] = { &Metric_t3273440202_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Metric_t3273440202_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Metric_t3273440202_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3196873006_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3196873006_0_0_0_Types[] = { &KeyValuePair_2_t3196873006_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3196873006_0_0_0 = { 1, GenInst_KeyValuePair_2_t3196873006_0_0_0_Types };
static const Il2CppType* GenInst_Metric_t3273440202_0_0_0_Types[] = { &Metric_t3273440202_0_0_0 };
extern const Il2CppGenericInst GenInst_Metric_t3273440202_0_0_0 = { 1, GenInst_Metric_t3273440202_0_0_0_Types };
static const Il2CppType* GenInst_Metric_t3273440202_0_0_0_Il2CppObject_0_0_0_Metric_t3273440202_0_0_0_Types[] = { &Metric_t3273440202_0_0_0, &Il2CppObject_0_0_0, &Metric_t3273440202_0_0_0 };
extern const Il2CppGenericInst GenInst_Metric_t3273440202_0_0_0_Il2CppObject_0_0_0_Metric_t3273440202_0_0_0 = { 3, GenInst_Metric_t3273440202_0_0_0_Il2CppObject_0_0_0_Metric_t3273440202_0_0_0_Types };
static const Il2CppType* GenInst_Metric_t3273440202_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &Metric_t3273440202_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Metric_t3273440202_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Metric_t3273440202_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Metric_t3273440202_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Metric_t3273440202_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Metric_t3273440202_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Metric_t3273440202_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Metric_t3273440202_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t3196873006_0_0_0_Types[] = { &Metric_t3273440202_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t3196873006_0_0_0 };
extern const Il2CppGenericInst GenInst_Metric_t3273440202_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t3196873006_0_0_0 = { 3, GenInst_Metric_t3273440202_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t3196873006_0_0_0_Types };
static const Il2CppType* GenInst_Metric_t3273440202_0_0_0_List_1_t2058570427_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Metric_t3273440202_0_0_0, &List_1_t2058570427_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Metric_t3273440202_0_0_0_List_1_t2058570427_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Metric_t3273440202_0_0_0_List_1_t2058570427_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2565994138_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2565994138_0_0_0_Types[] = { &KeyValuePair_2_t2565994138_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2565994138_0_0_0 = { 1, GenInst_KeyValuePair_2_t2565994138_0_0_0_Types };
extern const Il2CppType List_1_t3837499352_0_0_0;
static const Il2CppType* GenInst_Metric_t3273440202_0_0_0_List_1_t3837499352_0_0_0_Types[] = { &Metric_t3273440202_0_0_0, &List_1_t3837499352_0_0_0 };
extern const Il2CppGenericInst GenInst_Metric_t3273440202_0_0_0_List_1_t3837499352_0_0_0 = { 2, GenInst_Metric_t3273440202_0_0_0_List_1_t3837499352_0_0_0_Types };
extern const Il2CppType IMetricsTiming_t173410924_0_0_0;
static const Il2CppType* GenInst_IMetricsTiming_t173410924_0_0_0_Types[] = { &IMetricsTiming_t173410924_0_0_0 };
extern const Il2CppGenericInst GenInst_IMetricsTiming_t173410924_0_0_0 = { 1, GenInst_IMetricsTiming_t173410924_0_0_0_Types };
static const Il2CppType* GenInst_Metric_t3273440202_0_0_0_List_1_t3837499352_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Metric_t3273440202_0_0_0, &List_1_t3837499352_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Metric_t3273440202_0_0_0_List_1_t3837499352_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Metric_t3273440202_0_0_0_List_1_t3837499352_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t49955767_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t49955767_0_0_0_Types[] = { &KeyValuePair_2_t49955767_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t49955767_0_0_0 = { 1, GenInst_KeyValuePair_2_t49955767_0_0_0_Types };
static const Il2CppType* GenInst_Metric_t3273440202_0_0_0_Int64_t909078037_0_0_0_Types[] = { &Metric_t3273440202_0_0_0, &Int64_t909078037_0_0_0 };
extern const Il2CppGenericInst GenInst_Metric_t3273440202_0_0_0_Int64_t909078037_0_0_0 = { 2, GenInst_Metric_t3273440202_0_0_0_Int64_t909078037_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1416501748_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1416501748_0_0_0_Types[] = { &KeyValuePair_2_t1416501748_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1416501748_0_0_0 = { 1, GenInst_KeyValuePair_2_t1416501748_0_0_0_Types };
static const Il2CppType* GenInst_Metric_t3273440202_0_0_0_Int64_t909078037_0_0_0_Metric_t3273440202_0_0_0_Types[] = { &Metric_t3273440202_0_0_0, &Int64_t909078037_0_0_0, &Metric_t3273440202_0_0_0 };
extern const Il2CppGenericInst GenInst_Metric_t3273440202_0_0_0_Int64_t909078037_0_0_0_Metric_t3273440202_0_0_0 = { 3, GenInst_Metric_t3273440202_0_0_0_Int64_t909078037_0_0_0_Metric_t3273440202_0_0_0_Types };
static const Il2CppType* GenInst_Metric_t3273440202_0_0_0_Int64_t909078037_0_0_0_Int64_t909078037_0_0_0_Types[] = { &Metric_t3273440202_0_0_0, &Int64_t909078037_0_0_0, &Int64_t909078037_0_0_0 };
extern const Il2CppGenericInst GenInst_Metric_t3273440202_0_0_0_Int64_t909078037_0_0_0_Int64_t909078037_0_0_0 = { 3, GenInst_Metric_t3273440202_0_0_0_Int64_t909078037_0_0_0_Int64_t909078037_0_0_0_Types };
static const Il2CppType* GenInst_Metric_t3273440202_0_0_0_Int64_t909078037_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Metric_t3273440202_0_0_0, &Int64_t909078037_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Metric_t3273440202_0_0_0_Int64_t909078037_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Metric_t3273440202_0_0_0_Int64_t909078037_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Metric_t3273440202_0_0_0_Int64_t909078037_0_0_0_KeyValuePair_2_t1416501748_0_0_0_Types[] = { &Metric_t3273440202_0_0_0, &Int64_t909078037_0_0_0, &KeyValuePair_2_t1416501748_0_0_0 };
extern const Il2CppGenericInst GenInst_Metric_t3273440202_0_0_0_Int64_t909078037_0_0_0_KeyValuePair_2_t1416501748_0_0_0 = { 3, GenInst_Metric_t3273440202_0_0_0_Int64_t909078037_0_0_0_KeyValuePair_2_t1416501748_0_0_0_Types };
extern const Il2CppType Timing_t847194262_0_0_0;
static const Il2CppType* GenInst_Metric_t3273440202_0_0_0_Timing_t847194262_0_0_0_Types[] = { &Metric_t3273440202_0_0_0, &Timing_t847194262_0_0_0 };
extern const Il2CppGenericInst GenInst_Metric_t3273440202_0_0_0_Timing_t847194262_0_0_0 = { 2, GenInst_Metric_t3273440202_0_0_0_Timing_t847194262_0_0_0_Types };
static const Il2CppType* GenInst_Metric_t3273440202_0_0_0_Timing_t847194262_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Metric_t3273440202_0_0_0, &Timing_t847194262_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Metric_t3273440202_0_0_0_Timing_t847194262_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Metric_t3273440202_0_0_0_Timing_t847194262_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1354617973_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1354617973_0_0_0_Types[] = { &KeyValuePair_2_t1354617973_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1354617973_0_0_0 = { 1, GenInst_KeyValuePair_2_t1354617973_0_0_0_Types };
extern const Il2CppType MetricError_t964444806_0_0_0;
static const Il2CppType* GenInst_MetricError_t964444806_0_0_0_Types[] = { &MetricError_t964444806_0_0_0 };
extern const Il2CppGenericInst GenInst_MetricError_t964444806_0_0_0 = { 1, GenInst_MetricError_t964444806_0_0_0_Types };
static const Il2CppType* GenInst_Metric_t3273440202_0_0_0_String_t_0_0_0_Types[] = { &Metric_t3273440202_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Metric_t3273440202_0_0_0_String_t_0_0_0 = { 2, GenInst_Metric_t3273440202_0_0_0_String_t_0_0_0_Types };
extern const Il2CppType ErrorResponse_t3502566035_0_0_0;
extern const Il2CppType XmlUnmarshallerContext_t1179575220_0_0_0;
static const Il2CppType* GenInst_ErrorResponse_t3502566035_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0_Types[] = { &ErrorResponse_t3502566035_0_0_0, &XmlUnmarshallerContext_t1179575220_0_0_0 };
extern const Il2CppGenericInst GenInst_ErrorResponse_t3502566035_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0 = { 2, GenInst_ErrorResponse_t3502566035_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0_Types };
extern const Il2CppType JsonUnmarshallerContext_t456235889_0_0_0;
static const Il2CppType* GenInst_ErrorResponse_t3502566035_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0_Types[] = { &ErrorResponse_t3502566035_0_0_0, &JsonUnmarshallerContext_t456235889_0_0_0 };
extern const Il2CppGenericInst GenInst_ErrorResponse_t3502566035_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0 = { 2, GenInst_ErrorResponse_t3502566035_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0_Types };
extern const Il2CppType UnmarshallerContext_t1608322995_0_0_0;
static const Il2CppType* GenInst_AmazonWebServiceResponse_t529043356_0_0_0_UnmarshallerContext_t1608322995_0_0_0_Types[] = { &AmazonWebServiceResponse_t529043356_0_0_0, &UnmarshallerContext_t1608322995_0_0_0 };
extern const Il2CppGenericInst GenInst_AmazonWebServiceResponse_t529043356_0_0_0_UnmarshallerContext_t1608322995_0_0_0 = { 2, GenInst_AmazonWebServiceResponse_t529043356_0_0_0_UnmarshallerContext_t1608322995_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &XmlUnmarshallerContext_t1179575220_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &JsonUnmarshallerContext_t456235889_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0_Types };
static const Il2CppType* GenInst_Int64_t909078037_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0_Types[] = { &Int64_t909078037_0_0_0, &XmlUnmarshallerContext_t1179575220_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t909078037_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0 = { 2, GenInst_Int64_t909078037_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0_Types };
static const Il2CppType* GenInst_Int64_t909078037_0_0_0_Il2CppObject_0_0_0_Types[] = { &Int64_t909078037_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t909078037_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Int64_t909078037_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Int64_t909078037_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0_Types[] = { &Int64_t909078037_0_0_0, &JsonUnmarshallerContext_t456235889_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t909078037_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0 = { 2, GenInst_Int64_t909078037_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t3825574718_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0_Types[] = { &Boolean_t3825574718_0_0_0, &XmlUnmarshallerContext_t1179575220_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t3825574718_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0 = { 2, GenInst_Boolean_t3825574718_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t3825574718_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0_Types[] = { &Boolean_t3825574718_0_0_0, &JsonUnmarshallerContext_t456235889_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t3825574718_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0 = { 2, GenInst_Boolean_t3825574718_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0_Types[] = { &String_t_0_0_0, &XmlUnmarshallerContext_t1179575220_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0 = { 2, GenInst_String_t_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0_Types[] = { &String_t_0_0_0, &JsonUnmarshallerContext_t456235889_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0 = { 2, GenInst_String_t_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0_Types };
static const Il2CppType* GenInst_DateTime_t693205669_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0_Types[] = { &DateTime_t693205669_0_0_0, &XmlUnmarshallerContext_t1179575220_0_0_0 };
extern const Il2CppGenericInst GenInst_DateTime_t693205669_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0 = { 2, GenInst_DateTime_t693205669_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0_Types };
static const Il2CppType* GenInst_DateTime_t693205669_0_0_0_Il2CppObject_0_0_0_Types[] = { &DateTime_t693205669_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_DateTime_t693205669_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_DateTime_t693205669_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_DateTime_t693205669_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0_Types[] = { &DateTime_t693205669_0_0_0, &JsonUnmarshallerContext_t456235889_0_0_0 };
extern const Il2CppGenericInst GenInst_DateTime_t693205669_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0 = { 2, GenInst_DateTime_t693205669_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0_Types };
extern const Il2CppType ResponseMetadata_t527027456_0_0_0;
static const Il2CppType* GenInst_ResponseMetadata_t527027456_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0_Types[] = { &ResponseMetadata_t527027456_0_0_0, &XmlUnmarshallerContext_t1179575220_0_0_0 };
extern const Il2CppGenericInst GenInst_ResponseMetadata_t527027456_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0 = { 2, GenInst_ResponseMetadata_t527027456_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0_Types };
static const Il2CppType* GenInst_ResponseMetadata_t527027456_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0_Types[] = { &ResponseMetadata_t527027456_0_0_0, &JsonUnmarshallerContext_t456235889_0_0_0 };
extern const Il2CppGenericInst GenInst_ResponseMetadata_t527027456_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0 = { 2, GenInst_ResponseMetadata_t527027456_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0_Types };
static const Il2CppType* GenInst_List_1_t2058570427_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0_Types[] = { &List_1_t2058570427_0_0_0, &XmlUnmarshallerContext_t1179575220_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t2058570427_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0 = { 2, GenInst_List_1_t2058570427_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0_Types };
static const Il2CppType* GenInst_List_1_t2058570427_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0_Types[] = { &List_1_t2058570427_0_0_0, &JsonUnmarshallerContext_t456235889_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t2058570427_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0 = { 2, GenInst_List_1_t2058570427_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0_Types[] = { &Il2CppObject_0_0_0, &XmlUnmarshallerContext_t1179575220_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0_Types[] = { &Il2CppObject_0_0_0, &JsonUnmarshallerContext_t456235889_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0_Types };
extern const Il2CppType XmlNodeType_t739504597_0_0_0;
static const Il2CppType* GenInst_XmlNodeType_t739504597_0_0_0_Types[] = { &XmlNodeType_t739504597_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlNodeType_t739504597_0_0_0 = { 1, GenInst_XmlNodeType_t739504597_0_0_0_Types };
extern const Il2CppType Link_t3210155869_0_0_0;
static const Il2CppType* GenInst_Link_t3210155869_0_0_0_Types[] = { &Link_t3210155869_0_0_0 };
extern const Il2CppGenericInst GenInst_Link_t3210155869_0_0_0 = { 1, GenInst_Link_t3210155869_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1701344717_0_0_0_String_t_0_0_0_Types[] = { &KeyValuePair_2_t1701344717_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1701344717_0_0_0_String_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t1701344717_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t38854645_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t38854645_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t38854645_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t38854645_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1701344717_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &KeyValuePair_2_t1701344717_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1701344717_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_KeyValuePair_2_t1701344717_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t38854645_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &KeyValuePair_2_t38854645_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t38854645_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_KeyValuePair_2_t38854645_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType IEnlistmentNotification_t1718399847_0_0_0;
static const Il2CppType* GenInst_IEnlistmentNotification_t1718399847_0_0_0_Types[] = { &IEnlistmentNotification_t1718399847_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnlistmentNotification_t1718399847_0_0_0 = { 1, GenInst_IEnlistmentNotification_t1718399847_0_0_0_Types };
extern const Il2CppType ISinglePhaseNotification_t242502729_0_0_0;
static const Il2CppType* GenInst_ISinglePhaseNotification_t242502729_0_0_0_Types[] = { &ISinglePhaseNotification_t242502729_0_0_0 };
extern const Il2CppGenericInst GenInst_ISinglePhaseNotification_t242502729_0_0_0 = { 1, GenInst_ISinglePhaseNotification_t242502729_0_0_0_Types };
extern const Il2CppType AssumeRoleWithWebIdentityRequest_t193094215_0_0_0;
extern const Il2CppType AssumeRoleWithWebIdentityResponse_t3931705881_0_0_0;
static const Il2CppType* GenInst_AssumeRoleWithWebIdentityRequest_t193094215_0_0_0_AssumeRoleWithWebIdentityResponse_t3931705881_0_0_0_Types[] = { &AssumeRoleWithWebIdentityRequest_t193094215_0_0_0, &AssumeRoleWithWebIdentityResponse_t3931705881_0_0_0 };
extern const Il2CppGenericInst GenInst_AssumeRoleWithWebIdentityRequest_t193094215_0_0_0_AssumeRoleWithWebIdentityResponse_t3931705881_0_0_0 = { 2, GenInst_AssumeRoleWithWebIdentityRequest_t193094215_0_0_0_AssumeRoleWithWebIdentityResponse_t3931705881_0_0_0_Types };
extern const Il2CppType AssumedRoleUser_t150458319_0_0_0;
static const Il2CppType* GenInst_AssumedRoleUser_t150458319_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0_Types[] = { &AssumedRoleUser_t150458319_0_0_0, &XmlUnmarshallerContext_t1179575220_0_0_0 };
extern const Il2CppGenericInst GenInst_AssumedRoleUser_t150458319_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0 = { 2, GenInst_AssumedRoleUser_t150458319_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0_Types };
static const Il2CppType* GenInst_AssumedRoleUser_t150458319_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0_Types[] = { &AssumedRoleUser_t150458319_0_0_0, &JsonUnmarshallerContext_t456235889_0_0_0 };
extern const Il2CppGenericInst GenInst_AssumedRoleUser_t150458319_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0 = { 2, GenInst_AssumedRoleUser_t150458319_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0_Types };
static const Il2CppType* GenInst_IRequest_t2400804350_0_0_0_AssumeRoleWithWebIdentityRequest_t193094215_0_0_0_Types[] = { &IRequest_t2400804350_0_0_0, &AssumeRoleWithWebIdentityRequest_t193094215_0_0_0 };
extern const Il2CppGenericInst GenInst_IRequest_t2400804350_0_0_0_AssumeRoleWithWebIdentityRequest_t193094215_0_0_0 = { 2, GenInst_IRequest_t2400804350_0_0_0_AssumeRoleWithWebIdentityRequest_t193094215_0_0_0_Types };
extern const Il2CppType Credentials_t3554032640_0_0_0;
static const Il2CppType* GenInst_Credentials_t3554032640_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0_Types[] = { &Credentials_t3554032640_0_0_0, &XmlUnmarshallerContext_t1179575220_0_0_0 };
extern const Il2CppGenericInst GenInst_Credentials_t3554032640_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0 = { 2, GenInst_Credentials_t3554032640_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0_Types };
static const Il2CppType* GenInst_Credentials_t3554032640_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0_Types[] = { &Credentials_t3554032640_0_0_0, &JsonUnmarshallerContext_t456235889_0_0_0 };
extern const Il2CppGenericInst GenInst_Credentials_t3554032640_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0 = { 2, GenInst_Credentials_t3554032640_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0_Types };
extern const Il2CppType DataRow_t321465356_0_0_0;
static const Il2CppType* GenInst_DataRow_t321465356_0_0_0_Types[] = { &DataRow_t321465356_0_0_0 };
extern const Il2CppGenericInst GenInst_DataRow_t321465356_0_0_0 = { 1, GenInst_DataRow_t321465356_0_0_0_Types };
extern const Il2CppType DataColumn_t2152532948_0_0_0;
static const Il2CppType* GenInst_DataColumn_t2152532948_0_0_0_Types[] = { &DataColumn_t2152532948_0_0_0 };
extern const Il2CppGenericInst GenInst_DataColumn_t2152532948_0_0_0 = { 1, GenInst_DataColumn_t2152532948_0_0_0_Types };
extern const Il2CppType MarshalByValueComponent_t3997823175_0_0_0;
static const Il2CppType* GenInst_MarshalByValueComponent_t3997823175_0_0_0_Types[] = { &MarshalByValueComponent_t3997823175_0_0_0 };
extern const Il2CppGenericInst GenInst_MarshalByValueComponent_t3997823175_0_0_0 = { 1, GenInst_MarshalByValueComponent_t3997823175_0_0_0_Types };
extern const Il2CppType IServiceProvider_t2397848447_0_0_0;
static const Il2CppType* GenInst_IServiceProvider_t2397848447_0_0_0_Types[] = { &IServiceProvider_t2397848447_0_0_0 };
extern const Il2CppGenericInst GenInst_IServiceProvider_t2397848447_0_0_0 = { 1, GenInst_IServiceProvider_t2397848447_0_0_0_Types };
extern const Il2CppType IComponent_t1000253244_0_0_0;
static const Il2CppType* GenInst_IComponent_t1000253244_0_0_0_Types[] = { &IComponent_t1000253244_0_0_0 };
extern const Il2CppGenericInst GenInst_IComponent_t1000253244_0_0_0 = { 1, GenInst_IComponent_t1000253244_0_0_0_Types };
extern const Il2CppType DataColumnMapping_t969655534_0_0_0;
static const Il2CppType* GenInst_DataColumnMapping_t969655534_0_0_0_Types[] = { &DataColumnMapping_t969655534_0_0_0 };
extern const Il2CppGenericInst GenInst_DataColumnMapping_t969655534_0_0_0 = { 1, GenInst_DataColumnMapping_t969655534_0_0_0_Types };
extern const Il2CppType IColumnMapping_t1034865995_0_0_0;
static const Il2CppType* GenInst_IColumnMapping_t1034865995_0_0_0_Types[] = { &IColumnMapping_t1034865995_0_0_0 };
extern const Il2CppGenericInst GenInst_IColumnMapping_t1034865995_0_0_0 = { 1, GenInst_IColumnMapping_t1034865995_0_0_0_Types };
extern const Il2CppType ColumnInfo_t894042583_0_0_0;
static const Il2CppType* GenInst_ColumnInfo_t894042583_0_0_0_Types[] = { &ColumnInfo_t894042583_0_0_0 };
extern const Il2CppGenericInst GenInst_ColumnInfo_t894042583_0_0_0 = { 1, GenInst_ColumnInfo_t894042583_0_0_0_Types };
extern const Il2CppType ObjectU5BU5D_t3614634134_0_0_0;
static const Il2CppType* GenInst_ObjectU5BU5D_t3614634134_0_0_0_Types[] = { &ObjectU5BU5D_t3614634134_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectU5BU5D_t3614634134_0_0_0 = { 1, GenInst_ObjectU5BU5D_t3614634134_0_0_0_Types };
extern const Il2CppType SchemaInfo_t3205033891_0_0_0;
static const Il2CppType* GenInst_SchemaInfo_t3205033891_0_0_0_Types[] = { &SchemaInfo_t3205033891_0_0_0 };
extern const Il2CppGenericInst GenInst_SchemaInfo_t3205033891_0_0_0 = { 1, GenInst_SchemaInfo_t3205033891_0_0_0_Types };
extern const Il2CppType ListSortDirection_t4186912589_0_0_0;
static const Il2CppType* GenInst_ListSortDirection_t4186912589_0_0_0_Types[] = { &ListSortDirection_t4186912589_0_0_0 };
extern const Il2CppGenericInst GenInst_ListSortDirection_t4186912589_0_0_0 = { 1, GenInst_ListSortDirection_t4186912589_0_0_0_Types };
extern const Il2CppType DataTable_t3267612424_0_0_0;
static const Il2CppType* GenInst_DataTable_t3267612424_0_0_0_Types[] = { &DataTable_t3267612424_0_0_0 };
extern const Il2CppGenericInst GenInst_DataTable_t3267612424_0_0_0 = { 1, GenInst_DataTable_t3267612424_0_0_0_Types };
extern const Il2CppType IXmlSerializable_t2623090263_0_0_0;
static const Il2CppType* GenInst_IXmlSerializable_t2623090263_0_0_0_Types[] = { &IXmlSerializable_t2623090263_0_0_0 };
extern const Il2CppGenericInst GenInst_IXmlSerializable_t2623090263_0_0_0 = { 1, GenInst_IXmlSerializable_t2623090263_0_0_0_Types };
extern const Il2CppType IListSource_t3806143940_0_0_0;
static const Il2CppType* GenInst_IListSource_t3806143940_0_0_0_Types[] = { &IListSource_t3806143940_0_0_0 };
extern const Il2CppGenericInst GenInst_IListSource_t3806143940_0_0_0 = { 1, GenInst_IListSource_t3806143940_0_0_0_Types };
extern const Il2CppType ISupportInitialize_t1887203168_0_0_0;
static const Il2CppType* GenInst_ISupportInitialize_t1887203168_0_0_0_Types[] = { &ISupportInitialize_t1887203168_0_0_0 };
extern const Il2CppGenericInst GenInst_ISupportInitialize_t1887203168_0_0_0 = { 1, GenInst_ISupportInitialize_t1887203168_0_0_0_Types };
extern const Il2CppType ISupportInitializeNotification_t2045243097_0_0_0;
static const Il2CppType* GenInst_ISupportInitializeNotification_t2045243097_0_0_0_Types[] = { &ISupportInitializeNotification_t2045243097_0_0_0 };
extern const Il2CppGenericInst GenInst_ISupportInitializeNotification_t2045243097_0_0_0 = { 1, GenInst_ISupportInitializeNotification_t2045243097_0_0_0_Types };
extern const Il2CppType DataRelation_t790111712_0_0_0;
static const Il2CppType* GenInst_DataRelation_t790111712_0_0_0_Types[] = { &DataRelation_t790111712_0_0_0 };
extern const Il2CppGenericInst GenInst_DataRelation_t790111712_0_0_0 = { 1, GenInst_DataRelation_t790111712_0_0_0_Types };
extern const Il2CppType DbSourceMethodInfo_t4064904528_0_0_0;
static const Il2CppType* GenInst_DbSourceMethodInfo_t4064904528_0_0_0_Types[] = { &DbSourceMethodInfo_t4064904528_0_0_0 };
extern const Il2CppGenericInst GenInst_DbSourceMethodInfo_t4064904528_0_0_0 = { 1, GenInst_DbSourceMethodInfo_t4064904528_0_0_0_Types };
extern const Il2CppType IdentityChangedArgs_t1657401211_0_0_0;
static const Il2CppType* GenInst_IdentityChangedArgs_t1657401211_0_0_0_Types[] = { &IdentityChangedArgs_t1657401211_0_0_0 };
extern const Il2CppGenericInst GenInst_IdentityChangedArgs_t1657401211_0_0_0 = { 1, GenInst_IdentityChangedArgs_t1657401211_0_0_0_Types };
extern const Il2CppType Credentials_t792472136_0_0_0;
static const Il2CppType* GenInst_Credentials_t792472136_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0_Types[] = { &Credentials_t792472136_0_0_0, &XmlUnmarshallerContext_t1179575220_0_0_0 };
extern const Il2CppGenericInst GenInst_Credentials_t792472136_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0 = { 2, GenInst_Credentials_t792472136_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0_Types };
static const Il2CppType* GenInst_Credentials_t792472136_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0_Types[] = { &Credentials_t792472136_0_0_0, &JsonUnmarshallerContext_t456235889_0_0_0 };
extern const Il2CppGenericInst GenInst_Credentials_t792472136_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0 = { 2, GenInst_Credentials_t792472136_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0_Types };
extern const Il2CppType GetCredentialsForIdentityRequest_t932830458_0_0_0;
static const Il2CppType* GenInst_IRequest_t2400804350_0_0_0_GetCredentialsForIdentityRequest_t932830458_0_0_0_Types[] = { &IRequest_t2400804350_0_0_0, &GetCredentialsForIdentityRequest_t932830458_0_0_0 };
extern const Il2CppGenericInst GenInst_IRequest_t2400804350_0_0_0_GetCredentialsForIdentityRequest_t932830458_0_0_0 = { 2, GenInst_IRequest_t2400804350_0_0_0_GetCredentialsForIdentityRequest_t932830458_0_0_0_Types };
extern const Il2CppType GetIdRequest_t4078561340_0_0_0;
static const Il2CppType* GenInst_IRequest_t2400804350_0_0_0_GetIdRequest_t4078561340_0_0_0_Types[] = { &IRequest_t2400804350_0_0_0, &GetIdRequest_t4078561340_0_0_0 };
extern const Il2CppGenericInst GenInst_IRequest_t2400804350_0_0_0_GetIdRequest_t4078561340_0_0_0 = { 2, GenInst_IRequest_t2400804350_0_0_0_GetIdRequest_t4078561340_0_0_0_Types };
extern const Il2CppType GetOpenIdTokenRequest_t2698079901_0_0_0;
static const Il2CppType* GenInst_IRequest_t2400804350_0_0_0_GetOpenIdTokenRequest_t2698079901_0_0_0_Types[] = { &IRequest_t2400804350_0_0_0, &GetOpenIdTokenRequest_t2698079901_0_0_0 };
extern const Il2CppGenericInst GenInst_IRequest_t2400804350_0_0_0_GetOpenIdTokenRequest_t2698079901_0_0_0 = { 2, GenInst_IRequest_t2400804350_0_0_0_GetOpenIdTokenRequest_t2698079901_0_0_0_Types };
extern const Il2CppType SqliteFunction_t2830561746_0_0_0;
static const Il2CppType* GenInst_SqliteFunction_t2830561746_0_0_0_Types[] = { &SqliteFunction_t2830561746_0_0_0 };
extern const Il2CppGenericInst GenInst_SqliteFunction_t2830561746_0_0_0 = { 1, GenInst_SqliteFunction_t2830561746_0_0_0_Types };
extern const Il2CppType RowUpdatingEventArgs_t1243529829_0_0_0;
static const Il2CppType* GenInst_RowUpdatingEventArgs_t1243529829_0_0_0_Types[] = { &RowUpdatingEventArgs_t1243529829_0_0_0 };
extern const Il2CppGenericInst GenInst_RowUpdatingEventArgs_t1243529829_0_0_0 = { 1, GenInst_RowUpdatingEventArgs_t1243529829_0_0_0_Types };
extern const Il2CppType SqliteStatement_t4106757957_0_0_0;
static const Il2CppType* GenInst_SqliteStatement_t4106757957_0_0_0_Types[] = { &SqliteStatement_t4106757957_0_0_0 };
extern const Il2CppGenericInst GenInst_SqliteStatement_t4106757957_0_0_0 = { 1, GenInst_SqliteStatement_t4106757957_0_0_0_Types };
extern const Il2CppType Pool_t3179814164_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_Pool_t3179814164_0_0_0_Types[] = { &String_t_0_0_0, &Pool_t3179814164_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Pool_t3179814164_0_0_0 = { 2, GenInst_String_t_0_0_0_Pool_t3179814164_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2851938648_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2851938648_0_0_0_Types[] = { &KeyValuePair_2_t2851938648_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2851938648_0_0_0 = { 1, GenInst_KeyValuePair_2_t2851938648_0_0_0_Types };
extern const Il2CppType WeakReference_t1077405567_0_0_0;
static const Il2CppType* GenInst_WeakReference_t1077405567_0_0_0_Types[] = { &WeakReference_t1077405567_0_0_0 };
extern const Il2CppGenericInst GenInst_WeakReference_t1077405567_0_0_0 = { 1, GenInst_WeakReference_t1077405567_0_0_0_Types };
extern const Il2CppType DbType_t3924915636_0_0_0;
static const Il2CppType* GenInst_DbType_t3924915636_0_0_0_Types[] = { &DbType_t3924915636_0_0_0 };
extern const Il2CppGenericInst GenInst_DbType_t3924915636_0_0_0 = { 1, GenInst_DbType_t3924915636_0_0_0_Types };
extern const Il2CppType SQLiteTypeNames_t2939892384_0_0_0;
static const Il2CppType* GenInst_SQLiteTypeNames_t2939892384_0_0_0_Types[] = { &SQLiteTypeNames_t2939892384_0_0_0 };
extern const Il2CppGenericInst GenInst_SQLiteTypeNames_t2939892384_0_0_0 = { 1, GenInst_SQLiteTypeNames_t2939892384_0_0_0_Types };
extern const Il2CppType TypeAffinity_t326266980_0_0_0;
static const Il2CppType* GenInst_TypeAffinity_t326266980_0_0_0_Types[] = { &TypeAffinity_t326266980_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeAffinity_t326266980_0_0_0 = { 1, GenInst_TypeAffinity_t326266980_0_0_0_Types };
extern const Il2CppType SQLiteType_t3850755444_0_0_0;
static const Il2CppType* GenInst_SQLiteType_t3850755444_0_0_0_Types[] = { &SQLiteType_t3850755444_0_0_0 };
extern const Il2CppGenericInst GenInst_SQLiteType_t3850755444_0_0_0 = { 1, GenInst_SQLiteType_t3850755444_0_0_0_Types };
extern const Il2CppType AggregateData_t1049664947_0_0_0;
static const Il2CppType* GenInst_Int64_t909078037_0_0_0_AggregateData_t1049664947_0_0_0_Types[] = { &Int64_t909078037_0_0_0, &AggregateData_t1049664947_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t909078037_0_0_0_AggregateData_t1049664947_0_0_0 = { 2, GenInst_Int64_t909078037_0_0_0_AggregateData_t1049664947_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1436312919_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1436312919_0_0_0_Types[] = { &KeyValuePair_2_t1436312919_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1436312919_0_0_0 = { 1, GenInst_KeyValuePair_2_t1436312919_0_0_0_Types };
static const Il2CppType* GenInst_Int64_t909078037_0_0_0_Il2CppObject_0_0_0_Int64_t909078037_0_0_0_Types[] = { &Int64_t909078037_0_0_0, &Il2CppObject_0_0_0, &Int64_t909078037_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t909078037_0_0_0_Il2CppObject_0_0_0_Int64_t909078037_0_0_0 = { 3, GenInst_Int64_t909078037_0_0_0_Il2CppObject_0_0_0_Int64_t909078037_0_0_0_Types };
static const Il2CppType* GenInst_Int64_t909078037_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &Int64_t909078037_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t909078037_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Int64_t909078037_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Int64_t909078037_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int64_t909078037_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t909078037_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int64_t909078037_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Int64_t909078037_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t1436312919_0_0_0_Types[] = { &Int64_t909078037_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t1436312919_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t909078037_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t1436312919_0_0_0 = { 3, GenInst_Int64_t909078037_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t1436312919_0_0_0_Types };
static const Il2CppType* GenInst_Int64_t909078037_0_0_0_AggregateData_t1049664947_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int64_t909078037_0_0_0, &AggregateData_t1049664947_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t909078037_0_0_0_AggregateData_t1049664947_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int64_t909078037_0_0_0_AggregateData_t1049664947_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4091495867_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4091495867_0_0_0_Types[] = { &KeyValuePair_2_t4091495867_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4091495867_0_0_0 = { 1, GenInst_KeyValuePair_2_t4091495867_0_0_0_Types };
extern const Il2CppType SqliteFunctionAttribute_t2332230_0_0_0;
static const Il2CppType* GenInst_SqliteFunctionAttribute_t2332230_0_0_0_Types[] = { &SqliteFunctionAttribute_t2332230_0_0_0 };
extern const Il2CppGenericInst GenInst_SqliteFunctionAttribute_t2332230_0_0_0 = { 1, GenInst_SqliteFunctionAttribute_t2332230_0_0_0_Types };
extern const Il2CppType KeyInfo_t777769675_0_0_0;
static const Il2CppType* GenInst_KeyInfo_t777769675_0_0_0_Types[] = { &KeyInfo_t777769675_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyInfo_t777769675_0_0_0 = { 1, GenInst_KeyInfo_t777769675_0_0_0_Types };
extern const Il2CppType List_1_t1398341365_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_List_1_t1398341365_0_0_0_Types[] = { &String_t_0_0_0, &List_1_t1398341365_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t1398341365_0_0_0 = { 2, GenInst_String_t_0_0_0_List_1_t1398341365_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_List_1_t1398341365_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &List_1_t1398341365_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t1398341365_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_List_1_t1398341365_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1070465849_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1070465849_0_0_0_Types[] = { &KeyValuePair_2_t1070465849_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1070465849_0_0_0 = { 1, GenInst_KeyValuePair_2_t1070465849_0_0_0_Types };
extern const Il2CppType SqliteParameter_t354437343_0_0_0;
static const Il2CppType* GenInst_SqliteParameter_t354437343_0_0_0_Types[] = { &SqliteParameter_t354437343_0_0_0 };
extern const Il2CppGenericInst GenInst_SqliteParameter_t354437343_0_0_0 = { 1, GenInst_SqliteParameter_t354437343_0_0_0_Types };
extern const Il2CppType Record_t441586865_0_0_0;
static const Il2CppType* GenInst_Record_t441586865_0_0_0_Types[] = { &Record_t441586865_0_0_0 };
extern const Il2CppGenericInst GenInst_Record_t441586865_0_0_0 = { 1, GenInst_Record_t441586865_0_0_0_Types };
extern const Il2CppType RecordPatch_t97905615_0_0_0;
static const Il2CppType* GenInst_RecordPatch_t97905615_0_0_0_Types[] = { &RecordPatch_t97905615_0_0_0 };
extern const Il2CppGenericInst GenInst_RecordPatch_t97905615_0_0_0 = { 1, GenInst_RecordPatch_t97905615_0_0_0_Types };
extern const Il2CppType Dataset_t149289424_0_0_0;
static const Il2CppType* GenInst_Dataset_t149289424_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0_Types[] = { &Dataset_t149289424_0_0_0, &XmlUnmarshallerContext_t1179575220_0_0_0 };
extern const Il2CppGenericInst GenInst_Dataset_t149289424_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0 = { 2, GenInst_Dataset_t149289424_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0_Types };
static const Il2CppType* GenInst_Dataset_t149289424_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0_Types[] = { &Dataset_t149289424_0_0_0, &JsonUnmarshallerContext_t456235889_0_0_0 };
extern const Il2CppGenericInst GenInst_Dataset_t149289424_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0 = { 2, GenInst_Dataset_t149289424_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0_Types };
extern const Il2CppType DeleteDatasetRequest_t3672322944_0_0_0;
static const Il2CppType* GenInst_IRequest_t2400804350_0_0_0_DeleteDatasetRequest_t3672322944_0_0_0_Types[] = { &IRequest_t2400804350_0_0_0, &DeleteDatasetRequest_t3672322944_0_0_0 };
extern const Il2CppGenericInst GenInst_IRequest_t2400804350_0_0_0_DeleteDatasetRequest_t3672322944_0_0_0 = { 2, GenInst_IRequest_t2400804350_0_0_0_DeleteDatasetRequest_t3672322944_0_0_0_Types };
extern const Il2CppType ListRecordsRequest_t3074726983_0_0_0;
static const Il2CppType* GenInst_IRequest_t2400804350_0_0_0_ListRecordsRequest_t3074726983_0_0_0_Types[] = { &IRequest_t2400804350_0_0_0, &ListRecordsRequest_t3074726983_0_0_0 };
extern const Il2CppGenericInst GenInst_IRequest_t2400804350_0_0_0_ListRecordsRequest_t3074726983_0_0_0 = { 2, GenInst_IRequest_t2400804350_0_0_0_ListRecordsRequest_t3074726983_0_0_0_Types };
extern const Il2CppType StringUnmarshaller_t3953260147_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_StringUnmarshaller_t3953260147_0_0_0_Types[] = { &String_t_0_0_0, &StringUnmarshaller_t3953260147_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_StringUnmarshaller_t3953260147_0_0_0 = { 2, GenInst_String_t_0_0_0_StringUnmarshaller_t3953260147_0_0_0_Types };
extern const Il2CppType RecordUnmarshaller_t3056833063_0_0_0;
static const Il2CppType* GenInst_Record_t441586865_0_0_0_RecordUnmarshaller_t3056833063_0_0_0_Types[] = { &Record_t441586865_0_0_0, &RecordUnmarshaller_t3056833063_0_0_0 };
extern const Il2CppGenericInst GenInst_Record_t441586865_0_0_0_RecordUnmarshaller_t3056833063_0_0_0 = { 2, GenInst_Record_t441586865_0_0_0_RecordUnmarshaller_t3056833063_0_0_0_Types };
extern const Il2CppType JsonMarshallerContext_t4063314926_0_0_0;
static const Il2CppType* GenInst_RecordPatch_t97905615_0_0_0_JsonMarshallerContext_t4063314926_0_0_0_Types[] = { &RecordPatch_t97905615_0_0_0, &JsonMarshallerContext_t4063314926_0_0_0 };
extern const Il2CppGenericInst GenInst_RecordPatch_t97905615_0_0_0_JsonMarshallerContext_t4063314926_0_0_0 = { 2, GenInst_RecordPatch_t97905615_0_0_0_JsonMarshallerContext_t4063314926_0_0_0_Types };
static const Il2CppType* GenInst_Record_t441586865_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0_Types[] = { &Record_t441586865_0_0_0, &XmlUnmarshallerContext_t1179575220_0_0_0 };
extern const Il2CppGenericInst GenInst_Record_t441586865_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0 = { 2, GenInst_Record_t441586865_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0_Types };
static const Il2CppType* GenInst_Record_t441586865_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0_Types[] = { &Record_t441586865_0_0_0, &JsonUnmarshallerContext_t456235889_0_0_0 };
extern const Il2CppGenericInst GenInst_Record_t441586865_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0 = { 2, GenInst_Record_t441586865_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0_Types };
extern const Il2CppType UpdateRecordsRequest_t197801344_0_0_0;
static const Il2CppType* GenInst_IRequest_t2400804350_0_0_0_UpdateRecordsRequest_t197801344_0_0_0_Types[] = { &IRequest_t2400804350_0_0_0, &UpdateRecordsRequest_t197801344_0_0_0 };
extern const Il2CppGenericInst GenInst_IRequest_t2400804350_0_0_0_UpdateRecordsRequest_t197801344_0_0_0 = { 2, GenInst_IRequest_t2400804350_0_0_0_UpdateRecordsRequest_t197801344_0_0_0_Types };
extern const Il2CppType DatasetMetadata_t1205730659_0_0_0;
static const Il2CppType* GenInst_DatasetMetadata_t1205730659_0_0_0_Types[] = { &DatasetMetadata_t1205730659_0_0_0 };
extern const Il2CppGenericInst GenInst_DatasetMetadata_t1205730659_0_0_0 = { 1, GenInst_DatasetMetadata_t1205730659_0_0_0_Types };
extern const Il2CppType Record_t868799569_0_0_0;
static const Il2CppType* GenInst_Record_t868799569_0_0_0_Types[] = { &Record_t868799569_0_0_0 };
extern const Il2CppGenericInst GenInst_Record_t868799569_0_0_0 = { 1, GenInst_Record_t868799569_0_0_0_Types };
extern const Il2CppType SyncSuccessEventArgs_t4277381347_0_0_0;
static const Il2CppType* GenInst_SyncSuccessEventArgs_t4277381347_0_0_0_Types[] = { &SyncSuccessEventArgs_t4277381347_0_0_0 };
extern const Il2CppGenericInst GenInst_SyncSuccessEventArgs_t4277381347_0_0_0 = { 1, GenInst_SyncSuccessEventArgs_t4277381347_0_0_0_Types };
extern const Il2CppType SyncFailureEventArgs_t1748345498_0_0_0;
static const Il2CppType* GenInst_SyncFailureEventArgs_t1748345498_0_0_0_Types[] = { &SyncFailureEventArgs_t1748345498_0_0_0 };
extern const Il2CppGenericInst GenInst_SyncFailureEventArgs_t1748345498_0_0_0 = { 1, GenInst_SyncFailureEventArgs_t1748345498_0_0_0_Types };
extern const Il2CppType SyncConflict_t3030310309_0_0_0;
static const Il2CppType* GenInst_SyncConflict_t3030310309_0_0_0_Types[] = { &SyncConflict_t3030310309_0_0_0 };
extern const Il2CppGenericInst GenInst_SyncConflict_t3030310309_0_0_0 = { 1, GenInst_SyncConflict_t3030310309_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Record_t868799569_0_0_0_Types[] = { &String_t_0_0_0, &Record_t868799569_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Record_t868799569_0_0_0 = { 2, GenInst_String_t_0_0_0_Record_t868799569_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Record_t868799569_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &Record_t868799569_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Record_t868799569_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_Record_t868799569_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t540924053_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t540924053_0_0_0_Types[] = { &KeyValuePair_2_t540924053_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t540924053_0_0_0 = { 1, GenInst_KeyValuePair_2_t540924053_0_0_0_Types };
extern const Il2CppType Statement_t3708524595_0_0_0;
static const Il2CppType* GenInst_Statement_t3708524595_0_0_0_Types[] = { &Statement_t3708524595_0_0_0 };
extern const Il2CppGenericInst GenInst_Statement_t3708524595_0_0_0 = { 1, GenInst_Statement_t3708524595_0_0_0_Types };
extern const Il2CppType CSRequest_t3915036330_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_CSRequest_t3915036330_0_0_0_Types[] = { &Type_t_0_0_0, &CSRequest_t3915036330_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_CSRequest_t3915036330_0_0_0 = { 2, GenInst_Type_t_0_0_0_CSRequest_t3915036330_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_CSRequest_t3915036330_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Type_t_0_0_0, &CSRequest_t3915036330_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_CSRequest_t3915036330_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_CSRequest_t3915036330_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3609739449_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3609739449_0_0_0_Types[] = { &KeyValuePair_2_t3609739449_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3609739449_0_0_0 = { 1, GenInst_KeyValuePair_2_t3609739449_0_0_0_Types };
extern const Il2CppType BaseInputModule_t1295781545_0_0_0;
static const Il2CppType* GenInst_BaseInputModule_t1295781545_0_0_0_Types[] = { &BaseInputModule_t1295781545_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseInputModule_t1295781545_0_0_0 = { 1, GenInst_BaseInputModule_t1295781545_0_0_0_Types };
extern const Il2CppType RaycastResult_t21186376_0_0_0;
static const Il2CppType* GenInst_RaycastResult_t21186376_0_0_0_Types[] = { &RaycastResult_t21186376_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastResult_t21186376_0_0_0 = { 1, GenInst_RaycastResult_t21186376_0_0_0_Types };
extern const Il2CppType IDeselectHandler_t3182198310_0_0_0;
static const Il2CppType* GenInst_IDeselectHandler_t3182198310_0_0_0_Types[] = { &IDeselectHandler_t3182198310_0_0_0 };
extern const Il2CppGenericInst GenInst_IDeselectHandler_t3182198310_0_0_0 = { 1, GenInst_IDeselectHandler_t3182198310_0_0_0_Types };
extern const Il2CppType IEventSystemHandler_t2741188318_0_0_0;
static const Il2CppType* GenInst_IEventSystemHandler_t2741188318_0_0_0_Types[] = { &IEventSystemHandler_t2741188318_0_0_0 };
extern const Il2CppGenericInst GenInst_IEventSystemHandler_t2741188318_0_0_0 = { 1, GenInst_IEventSystemHandler_t2741188318_0_0_0_Types };
extern const Il2CppType List_1_t2110309450_0_0_0;
static const Il2CppType* GenInst_List_1_t2110309450_0_0_0_Types[] = { &List_1_t2110309450_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t2110309450_0_0_0 = { 1, GenInst_List_1_t2110309450_0_0_0_Types };
static const Il2CppType* GenInst_List_1_t2058570427_0_0_0_Types[] = { &List_1_t2058570427_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t2058570427_0_0_0 = { 1, GenInst_List_1_t2058570427_0_0_0_Types };
extern const Il2CppType List_1_t3188497603_0_0_0;
static const Il2CppType* GenInst_List_1_t3188497603_0_0_0_Types[] = { &List_1_t3188497603_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t3188497603_0_0_0 = { 1, GenInst_List_1_t3188497603_0_0_0_Types };
extern const Il2CppType ISelectHandler_t2812555161_0_0_0;
static const Il2CppType* GenInst_ISelectHandler_t2812555161_0_0_0_Types[] = { &ISelectHandler_t2812555161_0_0_0 };
extern const Il2CppGenericInst GenInst_ISelectHandler_t2812555161_0_0_0 = { 1, GenInst_ISelectHandler_t2812555161_0_0_0_Types };
extern const Il2CppType BaseRaycaster_t2336171397_0_0_0;
static const Il2CppType* GenInst_BaseRaycaster_t2336171397_0_0_0_Types[] = { &BaseRaycaster_t2336171397_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseRaycaster_t2336171397_0_0_0 = { 1, GenInst_BaseRaycaster_t2336171397_0_0_0_Types };
extern const Il2CppType Entry_t3365010046_0_0_0;
static const Il2CppType* GenInst_Entry_t3365010046_0_0_0_Types[] = { &Entry_t3365010046_0_0_0 };
extern const Il2CppGenericInst GenInst_Entry_t3365010046_0_0_0 = { 1, GenInst_Entry_t3365010046_0_0_0_Types };
extern const Il2CppType BaseEventData_t2681005625_0_0_0;
static const Il2CppType* GenInst_BaseEventData_t2681005625_0_0_0_Types[] = { &BaseEventData_t2681005625_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseEventData_t2681005625_0_0_0 = { 1, GenInst_BaseEventData_t2681005625_0_0_0_Types };
extern const Il2CppType IPointerEnterHandler_t193164956_0_0_0;
static const Il2CppType* GenInst_IPointerEnterHandler_t193164956_0_0_0_Types[] = { &IPointerEnterHandler_t193164956_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerEnterHandler_t193164956_0_0_0 = { 1, GenInst_IPointerEnterHandler_t193164956_0_0_0_Types };
extern const Il2CppType IPointerExitHandler_t461019860_0_0_0;
static const Il2CppType* GenInst_IPointerExitHandler_t461019860_0_0_0_Types[] = { &IPointerExitHandler_t461019860_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerExitHandler_t461019860_0_0_0 = { 1, GenInst_IPointerExitHandler_t461019860_0_0_0_Types };
extern const Il2CppType IPointerDownHandler_t3929046918_0_0_0;
static const Il2CppType* GenInst_IPointerDownHandler_t3929046918_0_0_0_Types[] = { &IPointerDownHandler_t3929046918_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerDownHandler_t3929046918_0_0_0 = { 1, GenInst_IPointerDownHandler_t3929046918_0_0_0_Types };
extern const Il2CppType IPointerUpHandler_t1847764461_0_0_0;
static const Il2CppType* GenInst_IPointerUpHandler_t1847764461_0_0_0_Types[] = { &IPointerUpHandler_t1847764461_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerUpHandler_t1847764461_0_0_0 = { 1, GenInst_IPointerUpHandler_t1847764461_0_0_0_Types };
extern const Il2CppType IPointerClickHandler_t96169666_0_0_0;
static const Il2CppType* GenInst_IPointerClickHandler_t96169666_0_0_0_Types[] = { &IPointerClickHandler_t96169666_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerClickHandler_t96169666_0_0_0 = { 1, GenInst_IPointerClickHandler_t96169666_0_0_0_Types };
extern const Il2CppType IInitializePotentialDragHandler_t3350809087_0_0_0;
static const Il2CppType* GenInst_IInitializePotentialDragHandler_t3350809087_0_0_0_Types[] = { &IInitializePotentialDragHandler_t3350809087_0_0_0 };
extern const Il2CppGenericInst GenInst_IInitializePotentialDragHandler_t3350809087_0_0_0 = { 1, GenInst_IInitializePotentialDragHandler_t3350809087_0_0_0_Types };
extern const Il2CppType IBeginDragHandler_t3135127860_0_0_0;
static const Il2CppType* GenInst_IBeginDragHandler_t3135127860_0_0_0_Types[] = { &IBeginDragHandler_t3135127860_0_0_0 };
extern const Il2CppGenericInst GenInst_IBeginDragHandler_t3135127860_0_0_0 = { 1, GenInst_IBeginDragHandler_t3135127860_0_0_0_Types };
extern const Il2CppType IDragHandler_t2583993319_0_0_0;
static const Il2CppType* GenInst_IDragHandler_t2583993319_0_0_0_Types[] = { &IDragHandler_t2583993319_0_0_0 };
extern const Il2CppGenericInst GenInst_IDragHandler_t2583993319_0_0_0 = { 1, GenInst_IDragHandler_t2583993319_0_0_0_Types };
extern const Il2CppType IEndDragHandler_t1349123600_0_0_0;
static const Il2CppType* GenInst_IEndDragHandler_t1349123600_0_0_0_Types[] = { &IEndDragHandler_t1349123600_0_0_0 };
extern const Il2CppGenericInst GenInst_IEndDragHandler_t1349123600_0_0_0 = { 1, GenInst_IEndDragHandler_t1349123600_0_0_0_Types };
extern const Il2CppType IDropHandler_t2390101210_0_0_0;
static const Il2CppType* GenInst_IDropHandler_t2390101210_0_0_0_Types[] = { &IDropHandler_t2390101210_0_0_0 };
extern const Il2CppGenericInst GenInst_IDropHandler_t2390101210_0_0_0 = { 1, GenInst_IDropHandler_t2390101210_0_0_0_Types };
extern const Il2CppType IScrollHandler_t3834677510_0_0_0;
static const Il2CppType* GenInst_IScrollHandler_t3834677510_0_0_0_Types[] = { &IScrollHandler_t3834677510_0_0_0 };
extern const Il2CppGenericInst GenInst_IScrollHandler_t3834677510_0_0_0 = { 1, GenInst_IScrollHandler_t3834677510_0_0_0_Types };
extern const Il2CppType IUpdateSelectedHandler_t3778909353_0_0_0;
static const Il2CppType* GenInst_IUpdateSelectedHandler_t3778909353_0_0_0_Types[] = { &IUpdateSelectedHandler_t3778909353_0_0_0 };
extern const Il2CppGenericInst GenInst_IUpdateSelectedHandler_t3778909353_0_0_0 = { 1, GenInst_IUpdateSelectedHandler_t3778909353_0_0_0_Types };
extern const Il2CppType IMoveHandler_t2611925506_0_0_0;
static const Il2CppType* GenInst_IMoveHandler_t2611925506_0_0_0_Types[] = { &IMoveHandler_t2611925506_0_0_0 };
extern const Il2CppGenericInst GenInst_IMoveHandler_t2611925506_0_0_0 = { 1, GenInst_IMoveHandler_t2611925506_0_0_0_Types };
extern const Il2CppType ISubmitHandler_t525803901_0_0_0;
static const Il2CppType* GenInst_ISubmitHandler_t525803901_0_0_0_Types[] = { &ISubmitHandler_t525803901_0_0_0 };
extern const Il2CppGenericInst GenInst_ISubmitHandler_t525803901_0_0_0 = { 1, GenInst_ISubmitHandler_t525803901_0_0_0_Types };
extern const Il2CppType ICancelHandler_t1980319651_0_0_0;
static const Il2CppType* GenInst_ICancelHandler_t1980319651_0_0_0_Types[] = { &ICancelHandler_t1980319651_0_0_0 };
extern const Il2CppGenericInst GenInst_ICancelHandler_t1980319651_0_0_0 = { 1, GenInst_ICancelHandler_t1980319651_0_0_0_Types };
extern const Il2CppType Transform_t3275118058_0_0_0;
static const Il2CppType* GenInst_Transform_t3275118058_0_0_0_Types[] = { &Transform_t3275118058_0_0_0 };
extern const Il2CppGenericInst GenInst_Transform_t3275118058_0_0_0 = { 1, GenInst_Transform_t3275118058_0_0_0_Types };
extern const Il2CppType GameObject_t1756533147_0_0_0;
static const Il2CppType* GenInst_GameObject_t1756533147_0_0_0_Types[] = { &GameObject_t1756533147_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_t1756533147_0_0_0 = { 1, GenInst_GameObject_t1756533147_0_0_0_Types };
extern const Il2CppType BaseInput_t621514313_0_0_0;
static const Il2CppType* GenInst_BaseInput_t621514313_0_0_0_Types[] = { &BaseInput_t621514313_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseInput_t621514313_0_0_0 = { 1, GenInst_BaseInput_t621514313_0_0_0_Types };
extern const Il2CppType UIBehaviour_t3960014691_0_0_0;
static const Il2CppType* GenInst_UIBehaviour_t3960014691_0_0_0_Types[] = { &UIBehaviour_t3960014691_0_0_0 };
extern const Il2CppGenericInst GenInst_UIBehaviour_t3960014691_0_0_0 = { 1, GenInst_UIBehaviour_t3960014691_0_0_0_Types };
extern const Il2CppType MonoBehaviour_t1158329972_0_0_0;
static const Il2CppType* GenInst_MonoBehaviour_t1158329972_0_0_0_Types[] = { &MonoBehaviour_t1158329972_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoBehaviour_t1158329972_0_0_0 = { 1, GenInst_MonoBehaviour_t1158329972_0_0_0_Types };
extern const Il2CppType PointerEventData_t1599784723_0_0_0;
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &PointerEventData_t1599784723_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &PointerEventData_t1599784723_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2659922876_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2659922876_0_0_0_Types[] = { &KeyValuePair_2_t2659922876_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2659922876_0_0_0 = { 1, GenInst_KeyValuePair_2_t2659922876_0_0_0_Types };
static const Il2CppType* GenInst_PointerEventData_t1599784723_0_0_0_Types[] = { &PointerEventData_t1599784723_0_0_0 };
extern const Il2CppGenericInst GenInst_PointerEventData_t1599784723_0_0_0 = { 1, GenInst_PointerEventData_t1599784723_0_0_0_Types };
extern const Il2CppType ButtonState_t2688375492_0_0_0;
static const Il2CppType* GenInst_ButtonState_t2688375492_0_0_0_Types[] = { &ButtonState_t2688375492_0_0_0 };
extern const Il2CppGenericInst GenInst_ButtonState_t2688375492_0_0_0 = { 1, GenInst_ButtonState_t2688375492_0_0_0_Types };
extern const Il2CppType RaycastHit2D_t4063908774_0_0_0;
static const Il2CppType* GenInst_RaycastHit2D_t4063908774_0_0_0_Types[] = { &RaycastHit2D_t4063908774_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastHit2D_t4063908774_0_0_0 = { 1, GenInst_RaycastHit2D_t4063908774_0_0_0_Types };
extern const Il2CppType RaycastHit_t87180320_0_0_0;
static const Il2CppType* GenInst_RaycastHit_t87180320_0_0_0_Types[] = { &RaycastHit_t87180320_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastHit_t87180320_0_0_0 = { 1, GenInst_RaycastHit_t87180320_0_0_0_Types };
extern const Il2CppType ICanvasElement_t986520779_0_0_0;
static const Il2CppType* GenInst_ICanvasElement_t986520779_0_0_0_Types[] = { &ICanvasElement_t986520779_0_0_0 };
extern const Il2CppGenericInst GenInst_ICanvasElement_t986520779_0_0_0 = { 1, GenInst_ICanvasElement_t986520779_0_0_0_Types };
static const Il2CppType* GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &ICanvasElement_t986520779_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0_Types };
static const Il2CppType* GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &ICanvasElement_t986520779_0_0_0, &Int32_t2071877448_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType ColorBlock_t2652774230_0_0_0;
static const Il2CppType* GenInst_ColorBlock_t2652774230_0_0_0_Types[] = { &ColorBlock_t2652774230_0_0_0 };
extern const Il2CppGenericInst GenInst_ColorBlock_t2652774230_0_0_0 = { 1, GenInst_ColorBlock_t2652774230_0_0_0_Types };
extern const Il2CppType OptionData_t2420267500_0_0_0;
static const Il2CppType* GenInst_OptionData_t2420267500_0_0_0_Types[] = { &OptionData_t2420267500_0_0_0 };
extern const Il2CppGenericInst GenInst_OptionData_t2420267500_0_0_0 = { 1, GenInst_OptionData_t2420267500_0_0_0_Types };
extern const Il2CppType DropdownItem_t4139978805_0_0_0;
static const Il2CppType* GenInst_DropdownItem_t4139978805_0_0_0_Types[] = { &DropdownItem_t4139978805_0_0_0 };
extern const Il2CppGenericInst GenInst_DropdownItem_t4139978805_0_0_0 = { 1, GenInst_DropdownItem_t4139978805_0_0_0_Types };
extern const Il2CppType FloatTween_t2986189219_0_0_0;
static const Il2CppType* GenInst_FloatTween_t2986189219_0_0_0_Types[] = { &FloatTween_t2986189219_0_0_0 };
extern const Il2CppGenericInst GenInst_FloatTween_t2986189219_0_0_0 = { 1, GenInst_FloatTween_t2986189219_0_0_0_Types };
extern const Il2CppType Sprite_t309593783_0_0_0;
static const Il2CppType* GenInst_Sprite_t309593783_0_0_0_Types[] = { &Sprite_t309593783_0_0_0 };
extern const Il2CppGenericInst GenInst_Sprite_t309593783_0_0_0 = { 1, GenInst_Sprite_t309593783_0_0_0_Types };
extern const Il2CppType Canvas_t209405766_0_0_0;
static const Il2CppType* GenInst_Canvas_t209405766_0_0_0_Types[] = { &Canvas_t209405766_0_0_0 };
extern const Il2CppGenericInst GenInst_Canvas_t209405766_0_0_0 = { 1, GenInst_Canvas_t209405766_0_0_0_Types };
extern const Il2CppType List_1_t3873494194_0_0_0;
static const Il2CppType* GenInst_List_1_t3873494194_0_0_0_Types[] = { &List_1_t3873494194_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t3873494194_0_0_0 = { 1, GenInst_List_1_t3873494194_0_0_0_Types };
extern const Il2CppType HashSet_1_t2984649583_0_0_0;
static const Il2CppType* GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0_Types[] = { &Font_t4239498691_0_0_0, &HashSet_1_t2984649583_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0 = { 2, GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0_Types };
extern const Il2CppType Text_t356221433_0_0_0;
static const Il2CppType* GenInst_Text_t356221433_0_0_0_Types[] = { &Text_t356221433_0_0_0 };
extern const Il2CppGenericInst GenInst_Text_t356221433_0_0_0 = { 1, GenInst_Text_t356221433_0_0_0_Types };
static const Il2CppType* GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Font_t4239498691_0_0_0, &HashSet_1_t2984649583_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t850112849_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t850112849_0_0_0_Types[] = { &KeyValuePair_2_t850112849_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t850112849_0_0_0 = { 1, GenInst_KeyValuePair_2_t850112849_0_0_0_Types };
extern const Il2CppType ColorTween_t3438117476_0_0_0;
static const Il2CppType* GenInst_ColorTween_t3438117476_0_0_0_Types[] = { &ColorTween_t3438117476_0_0_0 };
extern const Il2CppGenericInst GenInst_ColorTween_t3438117476_0_0_0 = { 1, GenInst_ColorTween_t3438117476_0_0_0_Types };
extern const Il2CppType Graphic_t2426225576_0_0_0;
static const Il2CppType* GenInst_Graphic_t2426225576_0_0_0_Types[] = { &Graphic_t2426225576_0_0_0 };
extern const Il2CppGenericInst GenInst_Graphic_t2426225576_0_0_0 = { 1, GenInst_Graphic_t2426225576_0_0_0_Types };
extern const Il2CppType IndexedSet_1_t286373651_0_0_0;
static const Il2CppType* GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0_Types[] = { &Canvas_t209405766_0_0_0, &IndexedSet_1_t286373651_0_0_0 };
extern const Il2CppGenericInst GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0 = { 2, GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0_Types };
static const Il2CppType* GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &Graphic_t2426225576_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0_Types };
static const Il2CppType* GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Graphic_t2426225576_0_0_0, &Int32_t2071877448_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Canvas_t209405766_0_0_0, &IndexedSet_1_t286373651_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2391682566_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2391682566_0_0_0_Types[] = { &KeyValuePair_2_t2391682566_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2391682566_0_0_0 = { 1, GenInst_KeyValuePair_2_t2391682566_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3010968081_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3010968081_0_0_0_Types[] = { &KeyValuePair_2_t3010968081_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3010968081_0_0_0 = { 1, GenInst_KeyValuePair_2_t3010968081_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1912381698_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1912381698_0_0_0_Types[] = { &KeyValuePair_2_t1912381698_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1912381698_0_0_0 = { 1, GenInst_KeyValuePair_2_t1912381698_0_0_0_Types };
extern const Il2CppType ContentType_t1028629049_0_0_0;
static const Il2CppType* GenInst_ContentType_t1028629049_0_0_0_Types[] = { &ContentType_t1028629049_0_0_0 };
extern const Il2CppGenericInst GenInst_ContentType_t1028629049_0_0_0 = { 1, GenInst_ContentType_t1028629049_0_0_0_Types };
extern const Il2CppType Mask_t2977958238_0_0_0;
static const Il2CppType* GenInst_Mask_t2977958238_0_0_0_Types[] = { &Mask_t2977958238_0_0_0 };
extern const Il2CppGenericInst GenInst_Mask_t2977958238_0_0_0 = { 1, GenInst_Mask_t2977958238_0_0_0_Types };
extern const Il2CppType List_1_t2347079370_0_0_0;
static const Il2CppType* GenInst_List_1_t2347079370_0_0_0_Types[] = { &List_1_t2347079370_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t2347079370_0_0_0 = { 1, GenInst_List_1_t2347079370_0_0_0_Types };
extern const Il2CppType RectMask2D_t1156185964_0_0_0;
static const Il2CppType* GenInst_RectMask2D_t1156185964_0_0_0_Types[] = { &RectMask2D_t1156185964_0_0_0 };
extern const Il2CppGenericInst GenInst_RectMask2D_t1156185964_0_0_0 = { 1, GenInst_RectMask2D_t1156185964_0_0_0_Types };
extern const Il2CppType List_1_t525307096_0_0_0;
static const Il2CppType* GenInst_List_1_t525307096_0_0_0_Types[] = { &List_1_t525307096_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t525307096_0_0_0 = { 1, GenInst_List_1_t525307096_0_0_0_Types };
extern const Il2CppType Navigation_t1571958496_0_0_0;
static const Il2CppType* GenInst_Navigation_t1571958496_0_0_0_Types[] = { &Navigation_t1571958496_0_0_0 };
extern const Il2CppGenericInst GenInst_Navigation_t1571958496_0_0_0 = { 1, GenInst_Navigation_t1571958496_0_0_0_Types };
extern const Il2CppType IClippable_t1941276057_0_0_0;
static const Il2CppType* GenInst_IClippable_t1941276057_0_0_0_Types[] = { &IClippable_t1941276057_0_0_0 };
extern const Il2CppGenericInst GenInst_IClippable_t1941276057_0_0_0 = { 1, GenInst_IClippable_t1941276057_0_0_0_Types };
extern const Il2CppType Selectable_t1490392188_0_0_0;
static const Il2CppType* GenInst_Selectable_t1490392188_0_0_0_Types[] = { &Selectable_t1490392188_0_0_0 };
extern const Il2CppGenericInst GenInst_Selectable_t1490392188_0_0_0 = { 1, GenInst_Selectable_t1490392188_0_0_0_Types };
extern const Il2CppType SpriteState_t1353336012_0_0_0;
static const Il2CppType* GenInst_SpriteState_t1353336012_0_0_0_Types[] = { &SpriteState_t1353336012_0_0_0 };
extern const Il2CppGenericInst GenInst_SpriteState_t1353336012_0_0_0 = { 1, GenInst_SpriteState_t1353336012_0_0_0_Types };
extern const Il2CppType CanvasGroup_t3296560743_0_0_0;
static const Il2CppType* GenInst_CanvasGroup_t3296560743_0_0_0_Types[] = { &CanvasGroup_t3296560743_0_0_0 };
extern const Il2CppGenericInst GenInst_CanvasGroup_t3296560743_0_0_0 = { 1, GenInst_CanvasGroup_t3296560743_0_0_0_Types };
extern const Il2CppType MatEntry_t3157325053_0_0_0;
static const Il2CppType* GenInst_MatEntry_t3157325053_0_0_0_Types[] = { &MatEntry_t3157325053_0_0_0 };
extern const Il2CppGenericInst GenInst_MatEntry_t3157325053_0_0_0 = { 1, GenInst_MatEntry_t3157325053_0_0_0_Types };
extern const Il2CppType Toggle_t3976754468_0_0_0;
static const Il2CppType* GenInst_Toggle_t3976754468_0_0_0_Types[] = { &Toggle_t3976754468_0_0_0 };
extern const Il2CppGenericInst GenInst_Toggle_t3976754468_0_0_0 = { 1, GenInst_Toggle_t3976754468_0_0_0_Types };
static const Il2CppType* GenInst_Toggle_t3976754468_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Toggle_t3976754468_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Toggle_t3976754468_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Toggle_t3976754468_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType IClipper_t900477982_0_0_0;
static const Il2CppType* GenInst_IClipper_t900477982_0_0_0_Types[] = { &IClipper_t900477982_0_0_0 };
extern const Il2CppGenericInst GenInst_IClipper_t900477982_0_0_0 = { 1, GenInst_IClipper_t900477982_0_0_0_Types };
static const Il2CppType* GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &IClipper_t900477982_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0_Types };
static const Il2CppType* GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &IClipper_t900477982_0_0_0, &Int32_t2071877448_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t379984643_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t379984643_0_0_0_Types[] = { &KeyValuePair_2_t379984643_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t379984643_0_0_0 = { 1, GenInst_KeyValuePair_2_t379984643_0_0_0_Types };
extern const Il2CppType RectTransform_t3349966182_0_0_0;
static const Il2CppType* GenInst_RectTransform_t3349966182_0_0_0_Types[] = { &RectTransform_t3349966182_0_0_0 };
extern const Il2CppGenericInst GenInst_RectTransform_t3349966182_0_0_0 = { 1, GenInst_RectTransform_t3349966182_0_0_0_Types };
extern const Il2CppType LayoutRebuilder_t2155218138_0_0_0;
static const Il2CppType* GenInst_LayoutRebuilder_t2155218138_0_0_0_Types[] = { &LayoutRebuilder_t2155218138_0_0_0 };
extern const Il2CppGenericInst GenInst_LayoutRebuilder_t2155218138_0_0_0 = { 1, GenInst_LayoutRebuilder_t2155218138_0_0_0_Types };
extern const Il2CppType ILayoutElement_t1975293769_0_0_0;
static const Il2CppType* GenInst_ILayoutElement_t1975293769_0_0_0_Single_t2076509932_0_0_0_Types[] = { &ILayoutElement_t1975293769_0_0_0, &Single_t2076509932_0_0_0 };
extern const Il2CppGenericInst GenInst_ILayoutElement_t1975293769_0_0_0_Single_t2076509932_0_0_0 = { 2, GenInst_ILayoutElement_t1975293769_0_0_0_Single_t2076509932_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Single_t2076509932_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Single_t2076509932_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Single_t2076509932_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Single_t2076509932_0_0_0_Types };
extern const Il2CppType List_1_t1612828712_0_0_0;
static const Il2CppType* GenInst_List_1_t1612828712_0_0_0_Types[] = { &List_1_t1612828712_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1612828712_0_0_0 = { 1, GenInst_List_1_t1612828712_0_0_0_Types };
extern const Il2CppType List_1_t243638650_0_0_0;
static const Il2CppType* GenInst_List_1_t243638650_0_0_0_Types[] = { &List_1_t243638650_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t243638650_0_0_0 = { 1, GenInst_List_1_t243638650_0_0_0_Types };
extern const Il2CppType List_1_t1612828711_0_0_0;
static const Il2CppType* GenInst_List_1_t1612828711_0_0_0_Types[] = { &List_1_t1612828711_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1612828711_0_0_0 = { 1, GenInst_List_1_t1612828711_0_0_0_Types };
extern const Il2CppType List_1_t1612828713_0_0_0;
static const Il2CppType* GenInst_List_1_t1612828713_0_0_0_Types[] = { &List_1_t1612828713_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1612828713_0_0_0 = { 1, GenInst_List_1_t1612828713_0_0_0_Types };
extern const Il2CppType List_1_t1440998580_0_0_0;
static const Il2CppType* GenInst_List_1_t1440998580_0_0_0_Types[] = { &List_1_t1440998580_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1440998580_0_0_0 = { 1, GenInst_List_1_t1440998580_0_0_0_Types };
extern const Il2CppType List_1_t573379950_0_0_0;
static const Il2CppType* GenInst_List_1_t573379950_0_0_0_Types[] = { &List_1_t573379950_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t573379950_0_0_0 = { 1, GenInst_List_1_t573379950_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType FieldWithTarget_t2256174789_0_0_0;
static const Il2CppType* GenInst_FieldWithTarget_t2256174789_0_0_0_Types[] = { &FieldWithTarget_t2256174789_0_0_0 };
extern const Il2CppGenericInst GenInst_FieldWithTarget_t2256174789_0_0_0 = { 1, GenInst_FieldWithTarget_t2256174789_0_0_0_Types };
extern const Il2CppType RenderTexture_t2666733923_0_0_0;
static const Il2CppType* GenInst_RenderTexture_t2666733923_0_0_0_Types[] = { &RenderTexture_t2666733923_0_0_0 };
extern const Il2CppGenericInst GenInst_RenderTexture_t2666733923_0_0_0 = { 1, GenInst_RenderTexture_t2666733923_0_0_0_Types };
extern const Il2CppType Texture_t2243626319_0_0_0;
static const Il2CppType* GenInst_Texture_t2243626319_0_0_0_Types[] = { &Texture_t2243626319_0_0_0 };
extern const Il2CppGenericInst GenInst_Texture_t2243626319_0_0_0 = { 1, GenInst_Texture_t2243626319_0_0_0_Types };
extern const Il2CppType Mesh_t1356156583_0_0_0;
static const Il2CppType* GenInst_Mesh_t1356156583_0_0_0_Types[] = { &Mesh_t1356156583_0_0_0 };
extern const Il2CppGenericInst GenInst_Mesh_t1356156583_0_0_0 = { 1, GenInst_Mesh_t1356156583_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t4048664256_gp_0_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t4048664256_gp_0_0_0_0_Types[] = { &IEnumerable_1_t4048664256_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t4048664256_gp_0_0_0_0 = { 1, GenInst_IEnumerable_1_t4048664256_gp_0_0_0_0_Types };
extern const Il2CppType Array_InternalArray__IEnumerable_GetEnumerator_m2949663298_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m2949663298_gp_0_0_0_0_Types[] = { &Array_InternalArray__IEnumerable_GetEnumerator_m2949663298_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m2949663298_gp_0_0_0_0 = { 1, GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m2949663298_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m1730553742_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1730553742_gp_0_0_0_0_Array_Sort_m1730553742_gp_0_0_0_0_Types[] = { &Array_Sort_m1730553742_gp_0_0_0_0, &Array_Sort_m1730553742_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1730553742_gp_0_0_0_0_Array_Sort_m1730553742_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m1730553742_gp_0_0_0_0_Array_Sort_m1730553742_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m3106198730_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m3106198730_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m3106198730_gp_0_0_0_0_Array_Sort_m3106198730_gp_1_0_0_0_Types[] = { &Array_Sort_m3106198730_gp_0_0_0_0, &Array_Sort_m3106198730_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m3106198730_gp_0_0_0_0_Array_Sort_m3106198730_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m3106198730_gp_0_0_0_0_Array_Sort_m3106198730_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m2090966156_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m2090966156_gp_0_0_0_0_Types[] = { &Array_Sort_m2090966156_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m2090966156_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m2090966156_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Array_Sort_m2090966156_gp_0_0_0_0_Array_Sort_m2090966156_gp_0_0_0_0_Types[] = { &Array_Sort_m2090966156_gp_0_0_0_0, &Array_Sort_m2090966156_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m2090966156_gp_0_0_0_0_Array_Sort_m2090966156_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m2090966156_gp_0_0_0_0_Array_Sort_m2090966156_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m1985772939_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1985772939_gp_0_0_0_0_Types[] = { &Array_Sort_m1985772939_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1985772939_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m1985772939_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m1985772939_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1985772939_gp_0_0_0_0_Array_Sort_m1985772939_gp_1_0_0_0_Types[] = { &Array_Sort_m1985772939_gp_0_0_0_0, &Array_Sort_m1985772939_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1985772939_gp_0_0_0_0_Array_Sort_m1985772939_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m1985772939_gp_0_0_0_0_Array_Sort_m1985772939_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m2736815140_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m2736815140_gp_0_0_0_0_Array_Sort_m2736815140_gp_0_0_0_0_Types[] = { &Array_Sort_m2736815140_gp_0_0_0_0, &Array_Sort_m2736815140_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m2736815140_gp_0_0_0_0_Array_Sort_m2736815140_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m2736815140_gp_0_0_0_0_Array_Sort_m2736815140_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m2468799988_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m2468799988_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m2468799988_gp_0_0_0_0_Array_Sort_m2468799988_gp_1_0_0_0_Types[] = { &Array_Sort_m2468799988_gp_0_0_0_0, &Array_Sort_m2468799988_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m2468799988_gp_0_0_0_0_Array_Sort_m2468799988_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m2468799988_gp_0_0_0_0_Array_Sort_m2468799988_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m2587948790_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m2587948790_gp_0_0_0_0_Types[] = { &Array_Sort_m2587948790_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m2587948790_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m2587948790_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Array_Sort_m2587948790_gp_0_0_0_0_Array_Sort_m2587948790_gp_0_0_0_0_Types[] = { &Array_Sort_m2587948790_gp_0_0_0_0, &Array_Sort_m2587948790_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m2587948790_gp_0_0_0_0_Array_Sort_m2587948790_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m2587948790_gp_0_0_0_0_Array_Sort_m2587948790_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m1279015767_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1279015767_gp_0_0_0_0_Types[] = { &Array_Sort_m1279015767_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1279015767_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m1279015767_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m1279015767_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1279015767_gp_1_0_0_0_Types[] = { &Array_Sort_m1279015767_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1279015767_gp_1_0_0_0 = { 1, GenInst_Array_Sort_m1279015767_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Array_Sort_m1279015767_gp_0_0_0_0_Array_Sort_m1279015767_gp_1_0_0_0_Types[] = { &Array_Sort_m1279015767_gp_0_0_0_0, &Array_Sort_m1279015767_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1279015767_gp_0_0_0_0_Array_Sort_m1279015767_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m1279015767_gp_0_0_0_0_Array_Sort_m1279015767_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m52621935_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m52621935_gp_0_0_0_0_Types[] = { &Array_Sort_m52621935_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m52621935_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m52621935_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m3546416104_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m3546416104_gp_0_0_0_0_Types[] = { &Array_Sort_m3546416104_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m3546416104_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m3546416104_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m533480027_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m533480027_gp_0_0_0_0_Types[] = { &Array_qsort_m533480027_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m533480027_gp_0_0_0_0 = { 1, GenInst_Array_qsort_m533480027_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m533480027_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m533480027_gp_0_0_0_0_Array_qsort_m533480027_gp_1_0_0_0_Types[] = { &Array_qsort_m533480027_gp_0_0_0_0, &Array_qsort_m533480027_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m533480027_gp_0_0_0_0_Array_qsort_m533480027_gp_1_0_0_0 = { 2, GenInst_Array_qsort_m533480027_gp_0_0_0_0_Array_qsort_m533480027_gp_1_0_0_0_Types };
extern const Il2CppType Array_compare_m940423571_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_compare_m940423571_gp_0_0_0_0_Types[] = { &Array_compare_m940423571_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_compare_m940423571_gp_0_0_0_0 = { 1, GenInst_Array_compare_m940423571_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m565008110_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m565008110_gp_0_0_0_0_Types[] = { &Array_qsort_m565008110_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m565008110_gp_0_0_0_0 = { 1, GenInst_Array_qsort_m565008110_gp_0_0_0_0_Types };
extern const Il2CppType Array_Resize_m1201602141_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Resize_m1201602141_gp_0_0_0_0_Types[] = { &Array_Resize_m1201602141_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Resize_m1201602141_gp_0_0_0_0 = { 1, GenInst_Array_Resize_m1201602141_gp_0_0_0_0_Types };
extern const Il2CppType Array_TrueForAll_m2783802133_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_TrueForAll_m2783802133_gp_0_0_0_0_Types[] = { &Array_TrueForAll_m2783802133_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_TrueForAll_m2783802133_gp_0_0_0_0 = { 1, GenInst_Array_TrueForAll_m2783802133_gp_0_0_0_0_Types };
extern const Il2CppType Array_ForEach_m3775633118_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_ForEach_m3775633118_gp_0_0_0_0_Types[] = { &Array_ForEach_m3775633118_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_ForEach_m3775633118_gp_0_0_0_0 = { 1, GenInst_Array_ForEach_m3775633118_gp_0_0_0_0_Types };
extern const Il2CppType Array_ConvertAll_m1734974082_gp_0_0_0_0;
extern const Il2CppType Array_ConvertAll_m1734974082_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_ConvertAll_m1734974082_gp_0_0_0_0_Array_ConvertAll_m1734974082_gp_1_0_0_0_Types[] = { &Array_ConvertAll_m1734974082_gp_0_0_0_0, &Array_ConvertAll_m1734974082_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_ConvertAll_m1734974082_gp_0_0_0_0_Array_ConvertAll_m1734974082_gp_1_0_0_0 = { 2, GenInst_Array_ConvertAll_m1734974082_gp_0_0_0_0_Array_ConvertAll_m1734974082_gp_1_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m934773128_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m934773128_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m934773128_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m934773128_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m934773128_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m3202023711_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m3202023711_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m3202023711_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m3202023711_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m3202023711_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m352384762_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m352384762_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m352384762_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m352384762_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m352384762_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m1593955424_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m1593955424_gp_0_0_0_0_Types[] = { &Array_FindIndex_m1593955424_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m1593955424_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m1593955424_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m1546138173_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m1546138173_gp_0_0_0_0_Types[] = { &Array_FindIndex_m1546138173_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m1546138173_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m1546138173_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m1082322798_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m1082322798_gp_0_0_0_0_Types[] = { &Array_FindIndex_m1082322798_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m1082322798_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m1082322798_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m525402987_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m525402987_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m525402987_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m525402987_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m525402987_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m3577113407_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m3577113407_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m3577113407_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m3577113407_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m3577113407_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m1033585031_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m1033585031_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m1033585031_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m1033585031_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m1033585031_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m3052238307_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m3052238307_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m3052238307_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m3052238307_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m3052238307_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m1306290405_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m1306290405_gp_0_0_0_0_Types[] = { &Array_IndexOf_m1306290405_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m1306290405_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m1306290405_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m2825795862_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m2825795862_gp_0_0_0_0_Types[] = { &Array_IndexOf_m2825795862_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m2825795862_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m2825795862_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m2841140625_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m2841140625_gp_0_0_0_0_Types[] = { &Array_IndexOf_m2841140625_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m2841140625_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m2841140625_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m3304283431_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m3304283431_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m3304283431_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m3304283431_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m3304283431_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m3860096562_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m3860096562_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m3860096562_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m3860096562_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m3860096562_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m2100440379_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m2100440379_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m2100440379_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m2100440379_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m2100440379_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindAll_m982349212_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindAll_m982349212_gp_0_0_0_0_Types[] = { &Array_FindAll_m982349212_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindAll_m982349212_gp_0_0_0_0 = { 1, GenInst_Array_FindAll_m982349212_gp_0_0_0_0_Types };
extern const Il2CppType Array_Exists_m1825464757_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Exists_m1825464757_gp_0_0_0_0_Types[] = { &Array_Exists_m1825464757_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Exists_m1825464757_gp_0_0_0_0 = { 1, GenInst_Array_Exists_m1825464757_gp_0_0_0_0_Types };
extern const Il2CppType Array_AsReadOnly_m1258056624_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_AsReadOnly_m1258056624_gp_0_0_0_0_Types[] = { &Array_AsReadOnly_m1258056624_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_AsReadOnly_m1258056624_gp_0_0_0_0 = { 1, GenInst_Array_AsReadOnly_m1258056624_gp_0_0_0_0_Types };
extern const Il2CppType Array_Find_m2529971459_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Find_m2529971459_gp_0_0_0_0_Types[] = { &Array_Find_m2529971459_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Find_m2529971459_gp_0_0_0_0 = { 1, GenInst_Array_Find_m2529971459_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLast_m3929249453_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLast_m3929249453_gp_0_0_0_0_Types[] = { &Array_FindLast_m3929249453_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLast_m3929249453_gp_0_0_0_0 = { 1, GenInst_Array_FindLast_m3929249453_gp_0_0_0_0_Types };
extern const Il2CppType InternalEnumerator_1_t3582267753_gp_0_0_0_0;
static const Il2CppType* GenInst_InternalEnumerator_1_t3582267753_gp_0_0_0_0_Types[] = { &InternalEnumerator_1_t3582267753_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InternalEnumerator_1_t3582267753_gp_0_0_0_0 = { 1, GenInst_InternalEnumerator_1_t3582267753_gp_0_0_0_0_Types };
extern const Il2CppType ArrayReadOnlyList_1_t3367196019_gp_0_0_0_0;
static const Il2CppType* GenInst_ArrayReadOnlyList_1_t3367196019_gp_0_0_0_0_Types[] = { &ArrayReadOnlyList_1_t3367196019_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ArrayReadOnlyList_1_t3367196019_gp_0_0_0_0 = { 1, GenInst_ArrayReadOnlyList_1_t3367196019_gp_0_0_0_0_Types };
extern const Il2CppType U3CGetEnumeratorU3Ec__Iterator0_t1667190089_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CGetEnumeratorU3Ec__Iterator0_t1667190089_gp_0_0_0_0_Types[] = { &U3CGetEnumeratorU3Ec__Iterator0_t1667190089_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CGetEnumeratorU3Ec__Iterator0_t1667190089_gp_0_0_0_0 = { 1, GenInst_U3CGetEnumeratorU3Ec__Iterator0_t1667190089_gp_0_0_0_0_Types };
extern const Il2CppType IList_1_t3737699284_gp_0_0_0_0;
static const Il2CppType* GenInst_IList_1_t3737699284_gp_0_0_0_0_Types[] = { &IList_1_t3737699284_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t3737699284_gp_0_0_0_0 = { 1, GenInst_IList_1_t3737699284_gp_0_0_0_0_Types };
extern const Il2CppType ICollection_1_t1552160836_gp_0_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t1552160836_gp_0_0_0_0_Types[] = { &ICollection_1_t1552160836_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t1552160836_gp_0_0_0_0 = { 1, GenInst_ICollection_1_t1552160836_gp_0_0_0_0_Types };
extern const Il2CppType Nullable_1_t1398937014_gp_0_0_0_0;
static const Il2CppType* GenInst_Nullable_1_t1398937014_gp_0_0_0_0_Types[] = { &Nullable_1_t1398937014_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Nullable_1_t1398937014_gp_0_0_0_0 = { 1, GenInst_Nullable_1_t1398937014_gp_0_0_0_0_Types };
extern const Il2CppType Comparer_1_t1036860714_gp_0_0_0_0;
static const Il2CppType* GenInst_Comparer_1_t1036860714_gp_0_0_0_0_Types[] = { &Comparer_1_t1036860714_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Comparer_1_t1036860714_gp_0_0_0_0 = { 1, GenInst_Comparer_1_t1036860714_gp_0_0_0_0_Types };
extern const Il2CppType DefaultComparer_t3074655092_gp_0_0_0_0;
static const Il2CppType* GenInst_DefaultComparer_t3074655092_gp_0_0_0_0_Types[] = { &DefaultComparer_t3074655092_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_DefaultComparer_t3074655092_gp_0_0_0_0 = { 1, GenInst_DefaultComparer_t3074655092_gp_0_0_0_0_Types };
extern const Il2CppType GenericComparer_1_t1787398723_gp_0_0_0_0;
static const Il2CppType* GenInst_GenericComparer_1_t1787398723_gp_0_0_0_0_Types[] = { &GenericComparer_1_t1787398723_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericComparer_1_t1787398723_gp_0_0_0_0 = { 1, GenInst_GenericComparer_1_t1787398723_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_t2276497324_gp_0_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Types[] = { &Dictionary_2_t2276497324_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2276497324_gp_0_0_0_0 = { 1, GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_t2276497324_gp_1_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Types[] = { &Dictionary_2_t2276497324_gp_0_0_0_0, &Dictionary_2_t2276497324_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0 = { 2, GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3180694294_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3180694294_0_0_0_Types[] = { &KeyValuePair_2_t3180694294_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3180694294_0_0_0 = { 1, GenInst_KeyValuePair_2_t3180694294_0_0_0_Types };
extern const Il2CppType Dictionary_2_Do_CopyTo_m404672235_gp_0_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m404672235_gp_0_0_0_0_Types[] = { &Dictionary_2_t2276497324_gp_0_0_0_0, &Dictionary_2_t2276497324_gp_1_0_0_0, &Dictionary_2_Do_CopyTo_m404672235_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m404672235_gp_0_0_0_0 = { 3, GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m404672235_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0_Types[] = { &Dictionary_2_t2276497324_gp_0_0_0_0, &Dictionary_2_t2276497324_gp_1_0_0_0, &Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0 = { 3, GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0_Il2CppObject_0_0_0_Types[] = { &Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Dictionary_2_t2276497324_gp_0_0_0_0, &Dictionary_2_t2276497324_gp_1_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType ShimEnumerator_t3895203923_gp_0_0_0_0;
extern const Il2CppType ShimEnumerator_t3895203923_gp_1_0_0_0;
static const Il2CppType* GenInst_ShimEnumerator_t3895203923_gp_0_0_0_0_ShimEnumerator_t3895203923_gp_1_0_0_0_Types[] = { &ShimEnumerator_t3895203923_gp_0_0_0_0, &ShimEnumerator_t3895203923_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ShimEnumerator_t3895203923_gp_0_0_0_0_ShimEnumerator_t3895203923_gp_1_0_0_0 = { 2, GenInst_ShimEnumerator_t3895203923_gp_0_0_0_0_ShimEnumerator_t3895203923_gp_1_0_0_0_Types };
extern const Il2CppType Enumerator_t2089681430_gp_0_0_0_0;
extern const Il2CppType Enumerator_t2089681430_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t2089681430_gp_0_0_0_0_Enumerator_t2089681430_gp_1_0_0_0_Types[] = { &Enumerator_t2089681430_gp_0_0_0_0, &Enumerator_t2089681430_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t2089681430_gp_0_0_0_0_Enumerator_t2089681430_gp_1_0_0_0 = { 2, GenInst_Enumerator_t2089681430_gp_0_0_0_0_Enumerator_t2089681430_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3434615342_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3434615342_0_0_0_Types[] = { &KeyValuePair_2_t3434615342_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3434615342_0_0_0 = { 1, GenInst_KeyValuePair_2_t3434615342_0_0_0_Types };
extern const Il2CppType KeyCollection_t1229212677_gp_0_0_0_0;
extern const Il2CppType KeyCollection_t1229212677_gp_1_0_0_0;
static const Il2CppType* GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0_Types[] = { &KeyCollection_t1229212677_gp_0_0_0_0, &KeyCollection_t1229212677_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0 = { 2, GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_KeyCollection_t1229212677_gp_0_0_0_0_Types[] = { &KeyCollection_t1229212677_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t1229212677_gp_0_0_0_0 = { 1, GenInst_KeyCollection_t1229212677_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t83320710_gp_0_0_0_0;
extern const Il2CppType Enumerator_t83320710_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t83320710_gp_0_0_0_0_Enumerator_t83320710_gp_1_0_0_0_Types[] = { &Enumerator_t83320710_gp_0_0_0_0, &Enumerator_t83320710_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t83320710_gp_0_0_0_0_Enumerator_t83320710_gp_1_0_0_0 = { 2, GenInst_Enumerator_t83320710_gp_0_0_0_0_Enumerator_t83320710_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerator_t83320710_gp_0_0_0_0_Types[] = { &Enumerator_t83320710_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t83320710_gp_0_0_0_0 = { 1, GenInst_Enumerator_t83320710_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0_KeyCollection_t1229212677_gp_0_0_0_0_Types[] = { &KeyCollection_t1229212677_gp_0_0_0_0, &KeyCollection_t1229212677_gp_1_0_0_0, &KeyCollection_t1229212677_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0_KeyCollection_t1229212677_gp_0_0_0_0 = { 3, GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0_KeyCollection_t1229212677_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_0_0_0_0_Types[] = { &KeyCollection_t1229212677_gp_0_0_0_0, &KeyCollection_t1229212677_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_0_0_0_0 = { 2, GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_0_0_0_0_Types };
extern const Il2CppType ValueCollection_t2262344653_gp_0_0_0_0;
extern const Il2CppType ValueCollection_t2262344653_gp_1_0_0_0;
static const Il2CppType* GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_Types[] = { &ValueCollection_t2262344653_gp_0_0_0_0, &ValueCollection_t2262344653_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0 = { 2, GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t2262344653_gp_1_0_0_0_Types[] = { &ValueCollection_t2262344653_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t2262344653_gp_1_0_0_0 = { 1, GenInst_ValueCollection_t2262344653_gp_1_0_0_0_Types };
extern const Il2CppType Enumerator_t3111723616_gp_0_0_0_0;
extern const Il2CppType Enumerator_t3111723616_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t3111723616_gp_0_0_0_0_Enumerator_t3111723616_gp_1_0_0_0_Types[] = { &Enumerator_t3111723616_gp_0_0_0_0, &Enumerator_t3111723616_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t3111723616_gp_0_0_0_0_Enumerator_t3111723616_gp_1_0_0_0 = { 2, GenInst_Enumerator_t3111723616_gp_0_0_0_0_Enumerator_t3111723616_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerator_t3111723616_gp_1_0_0_0_Types[] = { &Enumerator_t3111723616_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t3111723616_gp_1_0_0_0 = { 1, GenInst_Enumerator_t3111723616_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_Types[] = { &ValueCollection_t2262344653_gp_0_0_0_0, &ValueCollection_t2262344653_gp_1_0_0_0, &ValueCollection_t2262344653_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0 = { 3, GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t2262344653_gp_1_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_Types[] = { &ValueCollection_t2262344653_gp_1_0_0_0, &ValueCollection_t2262344653_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t2262344653_gp_1_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0 = { 2, GenInst_ValueCollection_t2262344653_gp_1_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_DictionaryEntry_t3048875398_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &DictionaryEntry_t3048875398_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_DictionaryEntry_t3048875398_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 2, GenInst_DictionaryEntry_t3048875398_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_KeyValuePair_2_t3180694294_0_0_0_Types[] = { &Dictionary_2_t2276497324_gp_0_0_0_0, &Dictionary_2_t2276497324_gp_1_0_0_0, &KeyValuePair_2_t3180694294_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_KeyValuePair_2_t3180694294_0_0_0 = { 3, GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_KeyValuePair_2_t3180694294_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3180694294_0_0_0_KeyValuePair_2_t3180694294_0_0_0_Types[] = { &KeyValuePair_2_t3180694294_0_0_0, &KeyValuePair_2_t3180694294_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3180694294_0_0_0_KeyValuePair_2_t3180694294_0_0_0 = { 2, GenInst_KeyValuePair_2_t3180694294_0_0_0_KeyValuePair_2_t3180694294_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t2276497324_gp_1_0_0_0_Types[] = { &Dictionary_2_t2276497324_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2276497324_gp_1_0_0_0 = { 1, GenInst_Dictionary_2_t2276497324_gp_1_0_0_0_Types };
extern const Il2CppType EqualityComparer_1_t2066709010_gp_0_0_0_0;
static const Il2CppType* GenInst_EqualityComparer_1_t2066709010_gp_0_0_0_0_Types[] = { &EqualityComparer_1_t2066709010_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_EqualityComparer_1_t2066709010_gp_0_0_0_0 = { 1, GenInst_EqualityComparer_1_t2066709010_gp_0_0_0_0_Types };
extern const Il2CppType DefaultComparer_t1766400012_gp_0_0_0_0;
static const Il2CppType* GenInst_DefaultComparer_t1766400012_gp_0_0_0_0_Types[] = { &DefaultComparer_t1766400012_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_DefaultComparer_t1766400012_gp_0_0_0_0 = { 1, GenInst_DefaultComparer_t1766400012_gp_0_0_0_0_Types };
extern const Il2CppType GenericEqualityComparer_1_t2202941003_gp_0_0_0_0;
static const Il2CppType* GenInst_GenericEqualityComparer_1_t2202941003_gp_0_0_0_0_Types[] = { &GenericEqualityComparer_1_t2202941003_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericEqualityComparer_1_t2202941003_gp_0_0_0_0 = { 1, GenInst_GenericEqualityComparer_1_t2202941003_gp_0_0_0_0_Types };
extern const Il2CppType IDictionary_2_t3502329323_gp_0_0_0_0;
static const Il2CppType* GenInst_IDictionary_2_t3502329323_gp_0_0_0_0_Types[] = { &IDictionary_2_t3502329323_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_2_t3502329323_gp_0_0_0_0 = { 1, GenInst_IDictionary_2_t3502329323_gp_0_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4174120762_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4174120762_0_0_0_Types[] = { &KeyValuePair_2_t4174120762_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4174120762_0_0_0 = { 1, GenInst_KeyValuePair_2_t4174120762_0_0_0_Types };
extern const Il2CppType IDictionary_2_t3502329323_gp_1_0_0_0;
static const Il2CppType* GenInst_IDictionary_2_t3502329323_gp_0_0_0_0_IDictionary_2_t3502329323_gp_1_0_0_0_Types[] = { &IDictionary_2_t3502329323_gp_0_0_0_0, &IDictionary_2_t3502329323_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_2_t3502329323_gp_0_0_0_0_IDictionary_2_t3502329323_gp_1_0_0_0 = { 2, GenInst_IDictionary_2_t3502329323_gp_0_0_0_0_IDictionary_2_t3502329323_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1988958766_gp_0_0_0_0;
extern const Il2CppType KeyValuePair_2_t1988958766_gp_1_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1988958766_gp_0_0_0_0_KeyValuePair_2_t1988958766_gp_1_0_0_0_Types[] = { &KeyValuePair_2_t1988958766_gp_0_0_0_0, &KeyValuePair_2_t1988958766_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1988958766_gp_0_0_0_0_KeyValuePair_2_t1988958766_gp_1_0_0_0 = { 2, GenInst_KeyValuePair_2_t1988958766_gp_0_0_0_0_KeyValuePair_2_t1988958766_gp_1_0_0_0_Types };
extern const Il2CppType List_1_t1169184319_gp_0_0_0_0;
static const Il2CppType* GenInst_List_1_t1169184319_gp_0_0_0_0_Types[] = { &List_1_t1169184319_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1169184319_gp_0_0_0_0 = { 1, GenInst_List_1_t1169184319_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t1292967705_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t1292967705_gp_0_0_0_0_Types[] = { &Enumerator_t1292967705_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t1292967705_gp_0_0_0_0 = { 1, GenInst_Enumerator_t1292967705_gp_0_0_0_0_Types };
extern const Il2CppType Collection_1_t686054069_gp_0_0_0_0;
static const Il2CppType* GenInst_Collection_1_t686054069_gp_0_0_0_0_Types[] = { &Collection_1_t686054069_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Collection_1_t686054069_gp_0_0_0_0 = { 1, GenInst_Collection_1_t686054069_gp_0_0_0_0_Types };
extern const Il2CppType ReadOnlyCollection_1_t3540981679_gp_0_0_0_0;
static const Il2CppType* GenInst_ReadOnlyCollection_1_t3540981679_gp_0_0_0_0_Types[] = { &ReadOnlyCollection_1_t3540981679_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ReadOnlyCollection_1_t3540981679_gp_0_0_0_0 = { 1, GenInst_ReadOnlyCollection_1_t3540981679_gp_0_0_0_0_Types };
extern const Il2CppType MonoProperty_GetterAdapterFrame_m4157835592_gp_0_0_0_0;
extern const Il2CppType MonoProperty_GetterAdapterFrame_m4157835592_gp_1_0_0_0;
static const Il2CppType* GenInst_MonoProperty_GetterAdapterFrame_m4157835592_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m4157835592_gp_1_0_0_0_Types[] = { &MonoProperty_GetterAdapterFrame_m4157835592_gp_0_0_0_0, &MonoProperty_GetterAdapterFrame_m4157835592_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoProperty_GetterAdapterFrame_m4157835592_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m4157835592_gp_1_0_0_0 = { 2, GenInst_MonoProperty_GetterAdapterFrame_m4157835592_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m4157835592_gp_1_0_0_0_Types };
extern const Il2CppType MonoProperty_StaticGetterAdapterFrame_m1249362843_gp_0_0_0_0;
static const Il2CppType* GenInst_MonoProperty_StaticGetterAdapterFrame_m1249362843_gp_0_0_0_0_Types[] = { &MonoProperty_StaticGetterAdapterFrame_m1249362843_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoProperty_StaticGetterAdapterFrame_m1249362843_gp_0_0_0_0 = { 1, GenInst_MonoProperty_StaticGetterAdapterFrame_m1249362843_gp_0_0_0_0_Types };
extern const Il2CppType ArraySegment_1_t1001032761_gp_0_0_0_0;
static const Il2CppType* GenInst_ArraySegment_1_t1001032761_gp_0_0_0_0_Types[] = { &ArraySegment_1_t1001032761_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ArraySegment_1_t1001032761_gp_0_0_0_0 = { 1, GenInst_ArraySegment_1_t1001032761_gp_0_0_0_0_Types };
extern const Il2CppType LinkedList_1_t3556217344_gp_0_0_0_0;
static const Il2CppType* GenInst_LinkedList_1_t3556217344_gp_0_0_0_0_Types[] = { &LinkedList_1_t3556217344_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_LinkedList_1_t3556217344_gp_0_0_0_0 = { 1, GenInst_LinkedList_1_t3556217344_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t4145643798_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t4145643798_gp_0_0_0_0_Types[] = { &Enumerator_t4145643798_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t4145643798_gp_0_0_0_0 = { 1, GenInst_Enumerator_t4145643798_gp_0_0_0_0_Types };
extern const Il2CppType LinkedListNode_1_t2172356692_gp_0_0_0_0;
static const Il2CppType* GenInst_LinkedListNode_1_t2172356692_gp_0_0_0_0_Types[] = { &LinkedListNode_1_t2172356692_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_LinkedListNode_1_t2172356692_gp_0_0_0_0 = { 1, GenInst_LinkedListNode_1_t2172356692_gp_0_0_0_0_Types };
extern const Il2CppType Queue_1_t1458930734_gp_0_0_0_0;
static const Il2CppType* GenInst_Queue_1_t1458930734_gp_0_0_0_0_Types[] = { &Queue_1_t1458930734_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Queue_1_t1458930734_gp_0_0_0_0 = { 1, GenInst_Queue_1_t1458930734_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t4000919638_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t4000919638_gp_0_0_0_0_Types[] = { &Enumerator_t4000919638_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t4000919638_gp_0_0_0_0 = { 1, GenInst_Enumerator_t4000919638_gp_0_0_0_0_Types };
extern const Il2CppType RBTree_Intern_m3121628056_gp_0_0_0_0;
static const Il2CppType* GenInst_RBTree_Intern_m3121628056_gp_0_0_0_0_Types[] = { &RBTree_Intern_m3121628056_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_RBTree_Intern_m3121628056_gp_0_0_0_0 = { 1, GenInst_RBTree_Intern_m3121628056_gp_0_0_0_0_Types };
extern const Il2CppType RBTree_Remove_m2480195356_gp_0_0_0_0;
static const Il2CppType* GenInst_RBTree_Remove_m2480195356_gp_0_0_0_0_Types[] = { &RBTree_Remove_m2480195356_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_RBTree_Remove_m2480195356_gp_0_0_0_0 = { 1, GenInst_RBTree_Remove_m2480195356_gp_0_0_0_0_Types };
extern const Il2CppType RBTree_Lookup_m3378201690_gp_0_0_0_0;
static const Il2CppType* GenInst_RBTree_Lookup_m3378201690_gp_0_0_0_0_Types[] = { &RBTree_Lookup_m3378201690_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_RBTree_Lookup_m3378201690_gp_0_0_0_0 = { 1, GenInst_RBTree_Lookup_m3378201690_gp_0_0_0_0_Types };
extern const Il2CppType RBTree_find_key_m2107656200_gp_0_0_0_0;
static const Il2CppType* GenInst_RBTree_find_key_m2107656200_gp_0_0_0_0_Types[] = { &RBTree_find_key_m2107656200_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_RBTree_find_key_m2107656200_gp_0_0_0_0 = { 1, GenInst_RBTree_find_key_m2107656200_gp_0_0_0_0_Types };
extern const Il2CppType SortedDictionary_2_t3426860295_gp_0_0_0_0;
static const Il2CppType* GenInst_SortedDictionary_2_t3426860295_gp_0_0_0_0_Types[] = { &SortedDictionary_2_t3426860295_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_SortedDictionary_2_t3426860295_gp_0_0_0_0 = { 1, GenInst_SortedDictionary_2_t3426860295_gp_0_0_0_0_Types };
extern const Il2CppType SortedDictionary_2_t3426860295_gp_1_0_0_0;
static const Il2CppType* GenInst_SortedDictionary_2_t3426860295_gp_0_0_0_0_SortedDictionary_2_t3426860295_gp_1_0_0_0_Types[] = { &SortedDictionary_2_t3426860295_gp_0_0_0_0, &SortedDictionary_2_t3426860295_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_SortedDictionary_2_t3426860295_gp_0_0_0_0_SortedDictionary_2_t3426860295_gp_1_0_0_0 = { 2, GenInst_SortedDictionary_2_t3426860295_gp_0_0_0_0_SortedDictionary_2_t3426860295_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4073929546_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4073929546_0_0_0_Types[] = { &KeyValuePair_2_t4073929546_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4073929546_0_0_0 = { 1, GenInst_KeyValuePair_2_t4073929546_0_0_0_Types };
extern const Il2CppType Node_t1473068371_gp_0_0_0_0;
extern const Il2CppType Node_t1473068371_gp_1_0_0_0;
static const Il2CppType* GenInst_Node_t1473068371_gp_0_0_0_0_Node_t1473068371_gp_1_0_0_0_Types[] = { &Node_t1473068371_gp_0_0_0_0, &Node_t1473068371_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Node_t1473068371_gp_0_0_0_0_Node_t1473068371_gp_1_0_0_0 = { 2, GenInst_Node_t1473068371_gp_0_0_0_0_Node_t1473068371_gp_1_0_0_0_Types };
extern const Il2CppType NodeHelper_t357096749_gp_0_0_0_0;
static const Il2CppType* GenInst_NodeHelper_t357096749_gp_0_0_0_0_Types[] = { &NodeHelper_t357096749_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_NodeHelper_t357096749_gp_0_0_0_0 = { 1, GenInst_NodeHelper_t357096749_gp_0_0_0_0_Types };
extern const Il2CppType NodeHelper_t357096749_gp_1_0_0_0;
static const Il2CppType* GenInst_NodeHelper_t357096749_gp_0_0_0_0_NodeHelper_t357096749_gp_1_0_0_0_Types[] = { &NodeHelper_t357096749_gp_0_0_0_0, &NodeHelper_t357096749_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_NodeHelper_t357096749_gp_0_0_0_0_NodeHelper_t357096749_gp_1_0_0_0 = { 2, GenInst_NodeHelper_t357096749_gp_0_0_0_0_NodeHelper_t357096749_gp_1_0_0_0_Types };
extern const Il2CppType ValueCollection_t2735575576_gp_0_0_0_0;
extern const Il2CppType ValueCollection_t2735575576_gp_1_0_0_0;
static const Il2CppType* GenInst_ValueCollection_t2735575576_gp_0_0_0_0_ValueCollection_t2735575576_gp_1_0_0_0_Types[] = { &ValueCollection_t2735575576_gp_0_0_0_0, &ValueCollection_t2735575576_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t2735575576_gp_0_0_0_0_ValueCollection_t2735575576_gp_1_0_0_0 = { 2, GenInst_ValueCollection_t2735575576_gp_0_0_0_0_ValueCollection_t2735575576_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t2735575576_gp_1_0_0_0_Types[] = { &ValueCollection_t2735575576_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t2735575576_gp_1_0_0_0 = { 1, GenInst_ValueCollection_t2735575576_gp_1_0_0_0_Types };
extern const Il2CppType Enumerator_t1353355221_gp_0_0_0_0;
extern const Il2CppType Enumerator_t1353355221_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t1353355221_gp_0_0_0_0_Enumerator_t1353355221_gp_1_0_0_0_Types[] = { &Enumerator_t1353355221_gp_0_0_0_0, &Enumerator_t1353355221_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t1353355221_gp_0_0_0_0_Enumerator_t1353355221_gp_1_0_0_0 = { 2, GenInst_Enumerator_t1353355221_gp_0_0_0_0_Enumerator_t1353355221_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerator_t1353355221_gp_1_0_0_0_Types[] = { &Enumerator_t1353355221_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t1353355221_gp_1_0_0_0 = { 1, GenInst_Enumerator_t1353355221_gp_1_0_0_0_Types };
extern const Il2CppType KeyCollection_t3620397270_gp_0_0_0_0;
extern const Il2CppType KeyCollection_t3620397270_gp_1_0_0_0;
static const Il2CppType* GenInst_KeyCollection_t3620397270_gp_0_0_0_0_KeyCollection_t3620397270_gp_1_0_0_0_Types[] = { &KeyCollection_t3620397270_gp_0_0_0_0, &KeyCollection_t3620397270_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t3620397270_gp_0_0_0_0_KeyCollection_t3620397270_gp_1_0_0_0 = { 2, GenInst_KeyCollection_t3620397270_gp_0_0_0_0_KeyCollection_t3620397270_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_KeyCollection_t3620397270_gp_0_0_0_0_Types[] = { &KeyCollection_t3620397270_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t3620397270_gp_0_0_0_0 = { 1, GenInst_KeyCollection_t3620397270_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t4275149413_gp_0_0_0_0;
extern const Il2CppType Enumerator_t4275149413_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t4275149413_gp_0_0_0_0_Enumerator_t4275149413_gp_1_0_0_0_Types[] = { &Enumerator_t4275149413_gp_0_0_0_0, &Enumerator_t4275149413_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t4275149413_gp_0_0_0_0_Enumerator_t4275149413_gp_1_0_0_0 = { 2, GenInst_Enumerator_t4275149413_gp_0_0_0_0_Enumerator_t4275149413_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerator_t4275149413_gp_0_0_0_0_Types[] = { &Enumerator_t4275149413_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t4275149413_gp_0_0_0_0 = { 1, GenInst_Enumerator_t4275149413_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t824916987_gp_0_0_0_0;
extern const Il2CppType Enumerator_t824916987_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t824916987_gp_0_0_0_0_Enumerator_t824916987_gp_1_0_0_0_Types[] = { &Enumerator_t824916987_gp_0_0_0_0, &Enumerator_t824916987_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t824916987_gp_0_0_0_0_Enumerator_t824916987_gp_1_0_0_0 = { 2, GenInst_Enumerator_t824916987_gp_0_0_0_0_Enumerator_t824916987_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t404405498_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t404405498_0_0_0_Types[] = { &KeyValuePair_2_t404405498_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t404405498_0_0_0 = { 1, GenInst_KeyValuePair_2_t404405498_0_0_0_Types };
static const Il2CppType* GenInst_SortedDictionary_2_t3426860295_gp_1_0_0_0_Types[] = { &SortedDictionary_2_t3426860295_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_SortedDictionary_2_t3426860295_gp_1_0_0_0 = { 1, GenInst_SortedDictionary_2_t3426860295_gp_1_0_0_0_Types };
extern const Il2CppType SortedList_2_t3146765111_gp_0_0_0_0;
static const Il2CppType* GenInst_SortedList_2_t3146765111_gp_0_0_0_0_Types[] = { &SortedList_2_t3146765111_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_SortedList_2_t3146765111_gp_0_0_0_0 = { 1, GenInst_SortedList_2_t3146765111_gp_0_0_0_0_Types };
extern const Il2CppType SortedList_2_t3146765111_gp_1_0_0_0;
static const Il2CppType* GenInst_SortedList_2_t3146765111_gp_0_0_0_0_SortedList_2_t3146765111_gp_1_0_0_0_Types[] = { &SortedList_2_t3146765111_gp_0_0_0_0, &SortedList_2_t3146765111_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_SortedList_2_t3146765111_gp_0_0_0_0_SortedList_2_t3146765111_gp_1_0_0_0 = { 2, GenInst_SortedList_2_t3146765111_gp_0_0_0_0_SortedList_2_t3146765111_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4007177354_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4007177354_0_0_0_Types[] = { &KeyValuePair_2_t4007177354_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4007177354_0_0_0 = { 1, GenInst_KeyValuePair_2_t4007177354_0_0_0_Types };
extern const Il2CppType EnumeratorMode_t215745978_gp_0_0_0_0;
extern const Il2CppType EnumeratorMode_t215745978_gp_1_0_0_0;
static const Il2CppType* GenInst_EnumeratorMode_t215745978_gp_0_0_0_0_EnumeratorMode_t215745978_gp_1_0_0_0_Types[] = { &EnumeratorMode_t215745978_gp_0_0_0_0, &EnumeratorMode_t215745978_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_EnumeratorMode_t215745978_gp_0_0_0_0_EnumeratorMode_t215745978_gp_1_0_0_0 = { 2, GenInst_EnumeratorMode_t215745978_gp_0_0_0_0_EnumeratorMode_t215745978_gp_1_0_0_0_Types };
extern const Il2CppType Enumerator_t1609510401_gp_0_0_0_0;
extern const Il2CppType Enumerator_t1609510401_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t1609510401_gp_0_0_0_0_Enumerator_t1609510401_gp_1_0_0_0_Types[] = { &Enumerator_t1609510401_gp_0_0_0_0, &Enumerator_t1609510401_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t1609510401_gp_0_0_0_0_Enumerator_t1609510401_gp_1_0_0_0 = { 2, GenInst_Enumerator_t1609510401_gp_0_0_0_0_Enumerator_t1609510401_gp_1_0_0_0_Types };
extern const Il2CppType KeyEnumerator_t3557860136_gp_0_0_0_0;
extern const Il2CppType KeyEnumerator_t3557860136_gp_1_0_0_0;
static const Il2CppType* GenInst_KeyEnumerator_t3557860136_gp_0_0_0_0_KeyEnumerator_t3557860136_gp_1_0_0_0_Types[] = { &KeyEnumerator_t3557860136_gp_0_0_0_0, &KeyEnumerator_t3557860136_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyEnumerator_t3557860136_gp_0_0_0_0_KeyEnumerator_t3557860136_gp_1_0_0_0 = { 2, GenInst_KeyEnumerator_t3557860136_gp_0_0_0_0_KeyEnumerator_t3557860136_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_KeyEnumerator_t3557860136_gp_0_0_0_0_Types[] = { &KeyEnumerator_t3557860136_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyEnumerator_t3557860136_gp_0_0_0_0 = { 1, GenInst_KeyEnumerator_t3557860136_gp_0_0_0_0_Types };
extern const Il2CppType ValueEnumerator_t2021630038_gp_0_0_0_0;
extern const Il2CppType ValueEnumerator_t2021630038_gp_1_0_0_0;
static const Il2CppType* GenInst_ValueEnumerator_t2021630038_gp_0_0_0_0_ValueEnumerator_t2021630038_gp_1_0_0_0_Types[] = { &ValueEnumerator_t2021630038_gp_0_0_0_0, &ValueEnumerator_t2021630038_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueEnumerator_t2021630038_gp_0_0_0_0_ValueEnumerator_t2021630038_gp_1_0_0_0 = { 2, GenInst_ValueEnumerator_t2021630038_gp_0_0_0_0_ValueEnumerator_t2021630038_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueEnumerator_t2021630038_gp_1_0_0_0_Types[] = { &ValueEnumerator_t2021630038_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueEnumerator_t2021630038_gp_1_0_0_0 = { 1, GenInst_ValueEnumerator_t2021630038_gp_1_0_0_0_Types };
extern const Il2CppType ListKeys_t708716807_gp_0_0_0_0;
extern const Il2CppType ListKeys_t708716807_gp_1_0_0_0;
static const Il2CppType* GenInst_ListKeys_t708716807_gp_0_0_0_0_ListKeys_t708716807_gp_1_0_0_0_Types[] = { &ListKeys_t708716807_gp_0_0_0_0, &ListKeys_t708716807_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ListKeys_t708716807_gp_0_0_0_0_ListKeys_t708716807_gp_1_0_0_0 = { 2, GenInst_ListKeys_t708716807_gp_0_0_0_0_ListKeys_t708716807_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ListKeys_t708716807_gp_0_0_0_0_Types[] = { &ListKeys_t708716807_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ListKeys_t708716807_gp_0_0_0_0 = { 1, GenInst_ListKeys_t708716807_gp_0_0_0_0_Types };
extern const Il2CppType GetEnumeratorU3Ec__Iterator2_t2910182623_gp_0_0_0_0;
extern const Il2CppType GetEnumeratorU3Ec__Iterator2_t2910182623_gp_1_0_0_0;
static const Il2CppType* GenInst_GetEnumeratorU3Ec__Iterator2_t2910182623_gp_0_0_0_0_GetEnumeratorU3Ec__Iterator2_t2910182623_gp_1_0_0_0_Types[] = { &GetEnumeratorU3Ec__Iterator2_t2910182623_gp_0_0_0_0, &GetEnumeratorU3Ec__Iterator2_t2910182623_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_GetEnumeratorU3Ec__Iterator2_t2910182623_gp_0_0_0_0_GetEnumeratorU3Ec__Iterator2_t2910182623_gp_1_0_0_0 = { 2, GenInst_GetEnumeratorU3Ec__Iterator2_t2910182623_gp_0_0_0_0_GetEnumeratorU3Ec__Iterator2_t2910182623_gp_1_0_0_0_Types };
extern const Il2CppType ListValues_t4054146799_gp_0_0_0_0;
extern const Il2CppType ListValues_t4054146799_gp_1_0_0_0;
static const Il2CppType* GenInst_ListValues_t4054146799_gp_0_0_0_0_ListValues_t4054146799_gp_1_0_0_0_Types[] = { &ListValues_t4054146799_gp_0_0_0_0, &ListValues_t4054146799_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ListValues_t4054146799_gp_0_0_0_0_ListValues_t4054146799_gp_1_0_0_0 = { 2, GenInst_ListValues_t4054146799_gp_0_0_0_0_ListValues_t4054146799_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ListValues_t4054146799_gp_1_0_0_0_Types[] = { &ListValues_t4054146799_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ListValues_t4054146799_gp_1_0_0_0 = { 1, GenInst_ListValues_t4054146799_gp_1_0_0_0_Types };
extern const Il2CppType GetEnumeratorU3Ec__Iterator3_t2100474802_gp_0_0_0_0;
extern const Il2CppType GetEnumeratorU3Ec__Iterator3_t2100474802_gp_1_0_0_0;
static const Il2CppType* GenInst_GetEnumeratorU3Ec__Iterator3_t2100474802_gp_0_0_0_0_GetEnumeratorU3Ec__Iterator3_t2100474802_gp_1_0_0_0_Types[] = { &GetEnumeratorU3Ec__Iterator3_t2100474802_gp_0_0_0_0, &GetEnumeratorU3Ec__Iterator3_t2100474802_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_GetEnumeratorU3Ec__Iterator3_t2100474802_gp_0_0_0_0_GetEnumeratorU3Ec__Iterator3_t2100474802_gp_1_0_0_0 = { 2, GenInst_GetEnumeratorU3Ec__Iterator3_t2100474802_gp_0_0_0_0_GetEnumeratorU3Ec__Iterator3_t2100474802_gp_1_0_0_0_Types };
extern const Il2CppType GetEnumeratorU3Ec__Iterator0_t3888417572_gp_0_0_0_0;
extern const Il2CppType GetEnumeratorU3Ec__Iterator0_t3888417572_gp_1_0_0_0;
static const Il2CppType* GenInst_GetEnumeratorU3Ec__Iterator0_t3888417572_gp_0_0_0_0_GetEnumeratorU3Ec__Iterator0_t3888417572_gp_1_0_0_0_Types[] = { &GetEnumeratorU3Ec__Iterator0_t3888417572_gp_0_0_0_0, &GetEnumeratorU3Ec__Iterator0_t3888417572_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_GetEnumeratorU3Ec__Iterator0_t3888417572_gp_0_0_0_0_GetEnumeratorU3Ec__Iterator0_t3888417572_gp_1_0_0_0 = { 2, GenInst_GetEnumeratorU3Ec__Iterator0_t3888417572_gp_0_0_0_0_GetEnumeratorU3Ec__Iterator0_t3888417572_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3522999862_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3522999862_0_0_0_Types[] = { &KeyValuePair_2_t3522999862_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3522999862_0_0_0 = { 1, GenInst_KeyValuePair_2_t3522999862_0_0_0_Types };
extern const Il2CppType U3CGetEnumeratorU3Ec__Iterator1_t3346222761_gp_0_0_0_0;
extern const Il2CppType U3CGetEnumeratorU3Ec__Iterator1_t3346222761_gp_1_0_0_0;
static const Il2CppType* GenInst_U3CGetEnumeratorU3Ec__Iterator1_t3346222761_gp_0_0_0_0_U3CGetEnumeratorU3Ec__Iterator1_t3346222761_gp_1_0_0_0_Types[] = { &U3CGetEnumeratorU3Ec__Iterator1_t3346222761_gp_0_0_0_0, &U3CGetEnumeratorU3Ec__Iterator1_t3346222761_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CGetEnumeratorU3Ec__Iterator1_t3346222761_gp_0_0_0_0_U3CGetEnumeratorU3Ec__Iterator1_t3346222761_gp_1_0_0_0 = { 2, GenInst_U3CGetEnumeratorU3Ec__Iterator1_t3346222761_gp_0_0_0_0_U3CGetEnumeratorU3Ec__Iterator1_t3346222761_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t533259906_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t533259906_0_0_0_Types[] = { &KeyValuePair_2_t533259906_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t533259906_0_0_0 = { 1, GenInst_KeyValuePair_2_t533259906_0_0_0_Types };
extern const Il2CppType Stack_1_t4016656541_gp_0_0_0_0;
static const Il2CppType* GenInst_Stack_1_t4016656541_gp_0_0_0_0_Types[] = { &Stack_1_t4016656541_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Stack_1_t4016656541_gp_0_0_0_0 = { 1, GenInst_Stack_1_t4016656541_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t546412149_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t546412149_gp_0_0_0_0_Types[] = { &Enumerator_t546412149_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t546412149_gp_0_0_0_0 = { 1, GenInst_Enumerator_t546412149_gp_0_0_0_0_Types };
extern const Il2CppType HashSet_1_t2624254809_gp_0_0_0_0;
static const Il2CppType* GenInst_HashSet_1_t2624254809_gp_0_0_0_0_Types[] = { &HashSet_1_t2624254809_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_HashSet_1_t2624254809_gp_0_0_0_0 = { 1, GenInst_HashSet_1_t2624254809_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t2109956843_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t2109956843_gp_0_0_0_0_Types[] = { &Enumerator_t2109956843_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t2109956843_gp_0_0_0_0 = { 1, GenInst_Enumerator_t2109956843_gp_0_0_0_0_Types };
extern const Il2CppType PrimeHelper_t3424417428_gp_0_0_0_0;
static const Il2CppType* GenInst_PrimeHelper_t3424417428_gp_0_0_0_0_Types[] = { &PrimeHelper_t3424417428_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PrimeHelper_t3424417428_gp_0_0_0_0 = { 1, GenInst_PrimeHelper_t3424417428_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Contains_m1284016302_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Contains_m1284016302_gp_0_0_0_0_Types[] = { &Enumerable_Contains_m1284016302_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Contains_m1284016302_gp_0_0_0_0 = { 1, GenInst_Enumerable_Contains_m1284016302_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Contains_m4622279_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Contains_m4622279_gp_0_0_0_0_Types[] = { &Enumerable_Contains_m4622279_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Contains_m4622279_gp_0_0_0_0 = { 1, GenInst_Enumerable_Contains_m4622279_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Count_m1561720045_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Count_m1561720045_gp_0_0_0_0_Types[] = { &Enumerable_Count_m1561720045_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Count_m1561720045_gp_0_0_0_0 = { 1, GenInst_Enumerable_Count_m1561720045_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_First_m4120844597_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_First_m4120844597_gp_0_0_0_0_Types[] = { &Enumerable_First_m4120844597_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_First_m4120844597_gp_0_0_0_0 = { 1, GenInst_Enumerable_First_m4120844597_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_First_m4120844597_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Enumerable_First_m4120844597_gp_0_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_First_m4120844597_gp_0_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Enumerable_First_m4120844597_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType Enumerable_First_m1693250038_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_First_m1693250038_gp_0_0_0_0_Types[] = { &Enumerable_First_m1693250038_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_First_m1693250038_gp_0_0_0_0 = { 1, GenInst_Enumerable_First_m1693250038_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_First_m2837459031_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_First_m2837459031_gp_0_0_0_0_Types[] = { &Enumerable_First_m2837459031_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_First_m2837459031_gp_0_0_0_0 = { 1, GenInst_Enumerable_First_m2837459031_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_First_m2837459031_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Enumerable_First_m2837459031_gp_0_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_First_m2837459031_gp_0_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Enumerable_First_m2837459031_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType Enumerable_OrderBy_m920500904_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_OrderBy_m920500904_gp_0_0_0_0_Types[] = { &Enumerable_OrderBy_m920500904_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_OrderBy_m920500904_gp_0_0_0_0 = { 1, GenInst_Enumerable_OrderBy_m920500904_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_OrderBy_m920500904_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_OrderBy_m920500904_gp_0_0_0_0_Enumerable_OrderBy_m920500904_gp_1_0_0_0_Types[] = { &Enumerable_OrderBy_m920500904_gp_0_0_0_0, &Enumerable_OrderBy_m920500904_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_OrderBy_m920500904_gp_0_0_0_0_Enumerable_OrderBy_m920500904_gp_1_0_0_0 = { 2, GenInst_Enumerable_OrderBy_m920500904_gp_0_0_0_0_Enumerable_OrderBy_m920500904_gp_1_0_0_0_Types };
extern const Il2CppType Enumerable_OrderBy_m4222740363_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_OrderBy_m4222740363_gp_0_0_0_0_Types[] = { &Enumerable_OrderBy_m4222740363_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_OrderBy_m4222740363_gp_0_0_0_0 = { 1, GenInst_Enumerable_OrderBy_m4222740363_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_OrderBy_m4222740363_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_OrderBy_m4222740363_gp_0_0_0_0_Enumerable_OrderBy_m4222740363_gp_1_0_0_0_Types[] = { &Enumerable_OrderBy_m4222740363_gp_0_0_0_0, &Enumerable_OrderBy_m4222740363_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_OrderBy_m4222740363_gp_0_0_0_0_Enumerable_OrderBy_m4222740363_gp_1_0_0_0 = { 2, GenInst_Enumerable_OrderBy_m4222740363_gp_0_0_0_0_Enumerable_OrderBy_m4222740363_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_OrderBy_m4222740363_gp_1_0_0_0_Types[] = { &Enumerable_OrderBy_m4222740363_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_OrderBy_m4222740363_gp_1_0_0_0 = { 1, GenInst_Enumerable_OrderBy_m4222740363_gp_1_0_0_0_Types };
extern const Il2CppType Enumerable_Select_m2459603006_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Select_m2459603006_gp_0_0_0_0_Types[] = { &Enumerable_Select_m2459603006_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Select_m2459603006_gp_0_0_0_0 = { 1, GenInst_Enumerable_Select_m2459603006_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Select_m2459603006_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_Select_m2459603006_gp_0_0_0_0_Enumerable_Select_m2459603006_gp_1_0_0_0_Types[] = { &Enumerable_Select_m2459603006_gp_0_0_0_0, &Enumerable_Select_m2459603006_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Select_m2459603006_gp_0_0_0_0_Enumerable_Select_m2459603006_gp_1_0_0_0 = { 2, GenInst_Enumerable_Select_m2459603006_gp_0_0_0_0_Enumerable_Select_m2459603006_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_Select_m2459603006_gp_1_0_0_0_Types[] = { &Enumerable_Select_m2459603006_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Select_m2459603006_gp_1_0_0_0 = { 1, GenInst_Enumerable_Select_m2459603006_gp_1_0_0_0_Types };
extern const Il2CppType Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0_Types[] = { &Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0_Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0_Types[] = { &Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0, &Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0_Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0 = { 2, GenInst_Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0_Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0_Types[] = { &Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0 = { 1, GenInst_Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0_Types };
extern const Il2CppType Enumerable_Single_m1979188101_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Single_m1979188101_gp_0_0_0_0_Types[] = { &Enumerable_Single_m1979188101_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Single_m1979188101_gp_0_0_0_0 = { 1, GenInst_Enumerable_Single_m1979188101_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_Single_m1979188101_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Enumerable_Single_m1979188101_gp_0_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Single_m1979188101_gp_0_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Enumerable_Single_m1979188101_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType Enumerable_SingleOrDefault_m2813552737_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_SingleOrDefault_m2813552737_gp_0_0_0_0_Types[] = { &Enumerable_SingleOrDefault_m2813552737_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_SingleOrDefault_m2813552737_gp_0_0_0_0 = { 1, GenInst_Enumerable_SingleOrDefault_m2813552737_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_SingleOrDefault_m2813552737_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Enumerable_SingleOrDefault_m2813552737_gp_0_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_SingleOrDefault_m2813552737_gp_0_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Enumerable_SingleOrDefault_m2813552737_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType Enumerable_ToArray_m2343256994_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_ToArray_m2343256994_gp_0_0_0_0_Types[] = { &Enumerable_ToArray_m2343256994_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToArray_m2343256994_gp_0_0_0_0 = { 1, GenInst_Enumerable_ToArray_m2343256994_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Where_m2409552823_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Where_m2409552823_gp_0_0_0_0_Types[] = { &Enumerable_Where_m2409552823_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Where_m2409552823_gp_0_0_0_0 = { 1, GenInst_Enumerable_Where_m2409552823_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_Where_m2409552823_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Enumerable_Where_m2409552823_gp_0_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Where_m2409552823_gp_0_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Enumerable_Where_m2409552823_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0_Types[] = { &Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0;
static const Il2CppType* GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0_Types[] = { &U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0 = { 1, GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0_Types };
extern const Il2CppType U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0_Types[] = { &U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0 = { 1, GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0_Types[] = { &U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0, &U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0 = { 2, GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0_Types };
extern const Il2CppType U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0_Types[] = { &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0 = { 1, GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType IOrderedEnumerable_1_t641749975_gp_0_0_0_0;
static const Il2CppType* GenInst_IOrderedEnumerable_1_t641749975_gp_0_0_0_0_Types[] = { &IOrderedEnumerable_1_t641749975_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IOrderedEnumerable_1_t641749975_gp_0_0_0_0 = { 1, GenInst_IOrderedEnumerable_1_t641749975_gp_0_0_0_0_Types };
extern const Il2CppType OrderedEnumerable_1_t753306046_gp_0_0_0_0;
static const Il2CppType* GenInst_OrderedEnumerable_1_t753306046_gp_0_0_0_0_Types[] = { &OrderedEnumerable_1_t753306046_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_OrderedEnumerable_1_t753306046_gp_0_0_0_0 = { 1, GenInst_OrderedEnumerable_1_t753306046_gp_0_0_0_0_Types };
extern const Il2CppType OrderedSequence_2_t1023848160_gp_0_0_0_0;
static const Il2CppType* GenInst_OrderedSequence_2_t1023848160_gp_0_0_0_0_Types[] = { &OrderedSequence_2_t1023848160_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_OrderedSequence_2_t1023848160_gp_0_0_0_0 = { 1, GenInst_OrderedSequence_2_t1023848160_gp_0_0_0_0_Types };
extern const Il2CppType OrderedSequence_2_t1023848160_gp_1_0_0_0;
static const Il2CppType* GenInst_OrderedSequence_2_t1023848160_gp_0_0_0_0_OrderedSequence_2_t1023848160_gp_1_0_0_0_Types[] = { &OrderedSequence_2_t1023848160_gp_0_0_0_0, &OrderedSequence_2_t1023848160_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_OrderedSequence_2_t1023848160_gp_0_0_0_0_OrderedSequence_2_t1023848160_gp_1_0_0_0 = { 2, GenInst_OrderedSequence_2_t1023848160_gp_0_0_0_0_OrderedSequence_2_t1023848160_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_OrderedSequence_2_t1023848160_gp_1_0_0_0_Types[] = { &OrderedSequence_2_t1023848160_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_OrderedSequence_2_t1023848160_gp_1_0_0_0 = { 1, GenInst_OrderedSequence_2_t1023848160_gp_1_0_0_0_Types };
extern const Il2CppType QuickSort_1_t1290221672_gp_0_0_0_0;
static const Il2CppType* GenInst_QuickSort_1_t1290221672_gp_0_0_0_0_Types[] = { &QuickSort_1_t1290221672_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_QuickSort_1_t1290221672_gp_0_0_0_0 = { 1, GenInst_QuickSort_1_t1290221672_gp_0_0_0_0_Types };
extern const Il2CppType U3CSortU3Ec__Iterator21_t163525460_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CSortU3Ec__Iterator21_t163525460_gp_0_0_0_0_Types[] = { &U3CSortU3Ec__Iterator21_t163525460_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CSortU3Ec__Iterator21_t163525460_gp_0_0_0_0 = { 1, GenInst_U3CSortU3Ec__Iterator21_t163525460_gp_0_0_0_0_Types };
extern const Il2CppType SortContext_1_t4088581714_gp_0_0_0_0;
static const Il2CppType* GenInst_SortContext_1_t4088581714_gp_0_0_0_0_Types[] = { &SortContext_1_t4088581714_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_SortContext_1_t4088581714_gp_0_0_0_0 = { 1, GenInst_SortContext_1_t4088581714_gp_0_0_0_0_Types };
extern const Il2CppType SortSequenceContext_2_t3419387730_gp_0_0_0_0;
static const Il2CppType* GenInst_SortSequenceContext_2_t3419387730_gp_0_0_0_0_Types[] = { &SortSequenceContext_2_t3419387730_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_SortSequenceContext_2_t3419387730_gp_0_0_0_0 = { 1, GenInst_SortSequenceContext_2_t3419387730_gp_0_0_0_0_Types };
extern const Il2CppType SortSequenceContext_2_t3419387730_gp_1_0_0_0;
static const Il2CppType* GenInst_SortSequenceContext_2_t3419387730_gp_0_0_0_0_SortSequenceContext_2_t3419387730_gp_1_0_0_0_Types[] = { &SortSequenceContext_2_t3419387730_gp_0_0_0_0, &SortSequenceContext_2_t3419387730_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_SortSequenceContext_2_t3419387730_gp_0_0_0_0_SortSequenceContext_2_t3419387730_gp_1_0_0_0 = { 2, GenInst_SortSequenceContext_2_t3419387730_gp_0_0_0_0_SortSequenceContext_2_t3419387730_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_SortSequenceContext_2_t3419387730_gp_1_0_0_0_Types[] = { &SortSequenceContext_2_t3419387730_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_SortSequenceContext_2_t3419387730_gp_1_0_0_0 = { 1, GenInst_SortSequenceContext_2_t3419387730_gp_1_0_0_0_Types };
extern const Il2CppType Component_GetComponentInChildren_m3417738402_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentInChildren_m3417738402_gp_0_0_0_0_Types[] = { &Component_GetComponentInChildren_m3417738402_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentInChildren_m3417738402_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentInChildren_m3417738402_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m1286417916_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m1286417916_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m1286417916_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m1286417916_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m1286417916_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m1989846061_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m1989846061_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m1989846061_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m1989846061_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m1989846061_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m3998291033_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m3998291033_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m3998291033_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m3998291033_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m3998291033_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m3421358420_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m3421358420_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m3421358420_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m3421358420_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m3421358420_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInParent_m825036157_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInParent_m825036157_gp_0_0_0_0_Types[] = { &Component_GetComponentsInParent_m825036157_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInParent_m825036157_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInParent_m825036157_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInParent_m3873375864_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInParent_m3873375864_gp_0_0_0_0_Types[] = { &Component_GetComponentsInParent_m3873375864_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInParent_m3873375864_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInParent_m3873375864_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInParent_m1600202230_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInParent_m1600202230_gp_0_0_0_0_Types[] = { &Component_GetComponentsInParent_m1600202230_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInParent_m1600202230_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInParent_m1600202230_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponents_m3990064736_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponents_m3990064736_gp_0_0_0_0_Types[] = { &Component_GetComponents_m3990064736_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponents_m3990064736_gp_0_0_0_0 = { 1, GenInst_Component_GetComponents_m3990064736_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponents_m2051523689_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponents_m2051523689_gp_0_0_0_0_Types[] = { &Component_GetComponents_m2051523689_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponents_m2051523689_gp_0_0_0_0 = { 1, GenInst_Component_GetComponents_m2051523689_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponentInChildren_m3844288190_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponentInChildren_m3844288190_gp_0_0_0_0_Types[] = { &GameObject_GetComponentInChildren_m3844288190_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentInChildren_m3844288190_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentInChildren_m3844288190_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponents_m2621570726_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponents_m2621570726_gp_0_0_0_0_Types[] = { &GameObject_GetComponents_m2621570726_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponents_m2621570726_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponents_m2621570726_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponentsInChildren_m1933507101_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponentsInChildren_m1933507101_gp_0_0_0_0_Types[] = { &GameObject_GetComponentsInChildren_m1933507101_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInChildren_m1933507101_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInChildren_m1933507101_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponentsInParent_m4177085118_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponentsInParent_m4177085118_gp_0_0_0_0_Types[] = { &GameObject_GetComponentsInParent_m4177085118_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInParent_m4177085118_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInParent_m4177085118_gp_0_0_0_0_Types };
extern const Il2CppType Mesh_GetAllocArrayFromChannel_m3109360642_gp_0_0_0_0;
static const Il2CppType* GenInst_Mesh_GetAllocArrayFromChannel_m3109360642_gp_0_0_0_0_Types[] = { &Mesh_GetAllocArrayFromChannel_m3109360642_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Mesh_GetAllocArrayFromChannel_m3109360642_gp_0_0_0_0 = { 1, GenInst_Mesh_GetAllocArrayFromChannel_m3109360642_gp_0_0_0_0_Types };
extern const Il2CppType Mesh_SafeLength_m3101579087_gp_0_0_0_0;
static const Il2CppType* GenInst_Mesh_SafeLength_m3101579087_gp_0_0_0_0_Types[] = { &Mesh_SafeLength_m3101579087_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Mesh_SafeLength_m3101579087_gp_0_0_0_0 = { 1, GenInst_Mesh_SafeLength_m3101579087_gp_0_0_0_0_Types };
extern const Il2CppType Mesh_SetListForChannel_m3999848894_gp_0_0_0_0;
static const Il2CppType* GenInst_Mesh_SetListForChannel_m3999848894_gp_0_0_0_0_Types[] = { &Mesh_SetListForChannel_m3999848894_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Mesh_SetListForChannel_m3999848894_gp_0_0_0_0 = { 1, GenInst_Mesh_SetListForChannel_m3999848894_gp_0_0_0_0_Types };
extern const Il2CppType Mesh_SetListForChannel_m4171325764_gp_0_0_0_0;
static const Il2CppType* GenInst_Mesh_SetListForChannel_m4171325764_gp_0_0_0_0_Types[] = { &Mesh_SetListForChannel_m4171325764_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Mesh_SetListForChannel_m4171325764_gp_0_0_0_0 = { 1, GenInst_Mesh_SetListForChannel_m4171325764_gp_0_0_0_0_Types };
extern const Il2CppType Mesh_SetUvsImpl_m3197944684_gp_0_0_0_0;
static const Il2CppType* GenInst_Mesh_SetUvsImpl_m3197944684_gp_0_0_0_0_Types[] = { &Mesh_SetUvsImpl_m3197944684_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Mesh_SetUvsImpl_m3197944684_gp_0_0_0_0 = { 1, GenInst_Mesh_SetUvsImpl_m3197944684_gp_0_0_0_0_Types };
extern const Il2CppType Object_Instantiate_m2530741872_gp_0_0_0_0;
static const Il2CppType* GenInst_Object_Instantiate_m2530741872_gp_0_0_0_0_Types[] = { &Object_Instantiate_m2530741872_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_Instantiate_m2530741872_gp_0_0_0_0 = { 1, GenInst_Object_Instantiate_m2530741872_gp_0_0_0_0_Types };
extern const Il2CppType Object_FindObjectsOfType_m894835059_gp_0_0_0_0;
static const Il2CppType* GenInst_Object_FindObjectsOfType_m894835059_gp_0_0_0_0_Types[] = { &Object_FindObjectsOfType_m894835059_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_FindObjectsOfType_m894835059_gp_0_0_0_0 = { 1, GenInst_Object_FindObjectsOfType_m894835059_gp_0_0_0_0_Types };
extern const Il2CppType GenericMixerPlayable_CastTo_m984828924_gp_0_0_0_0;
static const Il2CppType* GenInst_GenericMixerPlayable_CastTo_m984828924_gp_0_0_0_0_Types[] = { &GenericMixerPlayable_CastTo_m984828924_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericMixerPlayable_CastTo_m984828924_gp_0_0_0_0 = { 1, GenInst_GenericMixerPlayable_CastTo_m984828924_gp_0_0_0_0_Types };
extern const Il2CppType AnimationPlayable_CastTo_m2315139850_gp_0_0_0_0;
static const Il2CppType* GenInst_AnimationPlayable_CastTo_m2315139850_gp_0_0_0_0_Types[] = { &AnimationPlayable_CastTo_m2315139850_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_AnimationPlayable_CastTo_m2315139850_gp_0_0_0_0 = { 1, GenInst_AnimationPlayable_CastTo_m2315139850_gp_0_0_0_0_Types };
extern const Il2CppType CustomAnimationPlayable_CastTo_m2887957467_gp_0_0_0_0;
static const Il2CppType* GenInst_CustomAnimationPlayable_CastTo_m2887957467_gp_0_0_0_0_Types[] = { &CustomAnimationPlayable_CastTo_m2887957467_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAnimationPlayable_CastTo_m2887957467_gp_0_0_0_0 = { 1, GenInst_CustomAnimationPlayable_CastTo_m2887957467_gp_0_0_0_0_Types };
extern const Il2CppType InvokableCall_1_t476640868_gp_0_0_0_0;
static const Il2CppType* GenInst_InvokableCall_1_t476640868_gp_0_0_0_0_Types[] = { &InvokableCall_1_t476640868_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_1_t476640868_gp_0_0_0_0 = { 1, GenInst_InvokableCall_1_t476640868_gp_0_0_0_0_Types };
extern const Il2CppType UnityAction_1_t2490859068_0_0_0;
static const Il2CppType* GenInst_UnityAction_1_t2490859068_0_0_0_Types[] = { &UnityAction_1_t2490859068_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityAction_1_t2490859068_0_0_0 = { 1, GenInst_UnityAction_1_t2490859068_0_0_0_Types };
extern const Il2CppType InvokableCall_2_t2042724809_gp_0_0_0_0;
extern const Il2CppType InvokableCall_2_t2042724809_gp_1_0_0_0;
static const Il2CppType* GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0_InvokableCall_2_t2042724809_gp_1_0_0_0_Types[] = { &InvokableCall_2_t2042724809_gp_0_0_0_0, &InvokableCall_2_t2042724809_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0_InvokableCall_2_t2042724809_gp_1_0_0_0 = { 2, GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0_InvokableCall_2_t2042724809_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0_Types[] = { &InvokableCall_2_t2042724809_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0 = { 1, GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_2_t2042724809_gp_1_0_0_0_Types[] = { &InvokableCall_2_t2042724809_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t2042724809_gp_1_0_0_0 = { 1, GenInst_InvokableCall_2_t2042724809_gp_1_0_0_0_Types };
extern const Il2CppType InvokableCall_3_t3608808750_gp_0_0_0_0;
extern const Il2CppType InvokableCall_3_t3608808750_gp_1_0_0_0;
extern const Il2CppType InvokableCall_3_t3608808750_gp_2_0_0_0;
static const Il2CppType* GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0_InvokableCall_3_t3608808750_gp_1_0_0_0_InvokableCall_3_t3608808750_gp_2_0_0_0_Types[] = { &InvokableCall_3_t3608808750_gp_0_0_0_0, &InvokableCall_3_t3608808750_gp_1_0_0_0, &InvokableCall_3_t3608808750_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0_InvokableCall_3_t3608808750_gp_1_0_0_0_InvokableCall_3_t3608808750_gp_2_0_0_0 = { 3, GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0_InvokableCall_3_t3608808750_gp_1_0_0_0_InvokableCall_3_t3608808750_gp_2_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0_Types[] = { &InvokableCall_3_t3608808750_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0 = { 1, GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_3_t3608808750_gp_1_0_0_0_Types[] = { &InvokableCall_3_t3608808750_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3608808750_gp_1_0_0_0 = { 1, GenInst_InvokableCall_3_t3608808750_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_3_t3608808750_gp_2_0_0_0_Types[] = { &InvokableCall_3_t3608808750_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3608808750_gp_2_0_0_0 = { 1, GenInst_InvokableCall_3_t3608808750_gp_2_0_0_0_Types };
extern const Il2CppType InvokableCall_4_t879925395_gp_0_0_0_0;
extern const Il2CppType InvokableCall_4_t879925395_gp_1_0_0_0;
extern const Il2CppType InvokableCall_4_t879925395_gp_2_0_0_0;
extern const Il2CppType InvokableCall_4_t879925395_gp_3_0_0_0;
static const Il2CppType* GenInst_InvokableCall_4_t879925395_gp_0_0_0_0_InvokableCall_4_t879925395_gp_1_0_0_0_InvokableCall_4_t879925395_gp_2_0_0_0_InvokableCall_4_t879925395_gp_3_0_0_0_Types[] = { &InvokableCall_4_t879925395_gp_0_0_0_0, &InvokableCall_4_t879925395_gp_1_0_0_0, &InvokableCall_4_t879925395_gp_2_0_0_0, &InvokableCall_4_t879925395_gp_3_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t879925395_gp_0_0_0_0_InvokableCall_4_t879925395_gp_1_0_0_0_InvokableCall_4_t879925395_gp_2_0_0_0_InvokableCall_4_t879925395_gp_3_0_0_0 = { 4, GenInst_InvokableCall_4_t879925395_gp_0_0_0_0_InvokableCall_4_t879925395_gp_1_0_0_0_InvokableCall_4_t879925395_gp_2_0_0_0_InvokableCall_4_t879925395_gp_3_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t879925395_gp_0_0_0_0_Types[] = { &InvokableCall_4_t879925395_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t879925395_gp_0_0_0_0 = { 1, GenInst_InvokableCall_4_t879925395_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t879925395_gp_1_0_0_0_Types[] = { &InvokableCall_4_t879925395_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t879925395_gp_1_0_0_0 = { 1, GenInst_InvokableCall_4_t879925395_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t879925395_gp_2_0_0_0_Types[] = { &InvokableCall_4_t879925395_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t879925395_gp_2_0_0_0 = { 1, GenInst_InvokableCall_4_t879925395_gp_2_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t879925395_gp_3_0_0_0_Types[] = { &InvokableCall_4_t879925395_gp_3_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t879925395_gp_3_0_0_0 = { 1, GenInst_InvokableCall_4_t879925395_gp_3_0_0_0_Types };
extern const Il2CppType CachedInvokableCall_1_t224769006_gp_0_0_0_0;
static const Il2CppType* GenInst_CachedInvokableCall_1_t224769006_gp_0_0_0_0_Types[] = { &CachedInvokableCall_1_t224769006_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CachedInvokableCall_1_t224769006_gp_0_0_0_0 = { 1, GenInst_CachedInvokableCall_1_t224769006_gp_0_0_0_0_Types };
extern const Il2CppType UnityEvent_1_t4075366602_gp_0_0_0_0;
static const Il2CppType* GenInst_UnityEvent_1_t4075366602_gp_0_0_0_0_Types[] = { &UnityEvent_1_t4075366602_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_1_t4075366602_gp_0_0_0_0 = { 1, GenInst_UnityEvent_1_t4075366602_gp_0_0_0_0_Types };
extern const Il2CppType UnityEvent_2_t4075366599_gp_0_0_0_0;
extern const Il2CppType UnityEvent_2_t4075366599_gp_1_0_0_0;
static const Il2CppType* GenInst_UnityEvent_2_t4075366599_gp_0_0_0_0_UnityEvent_2_t4075366599_gp_1_0_0_0_Types[] = { &UnityEvent_2_t4075366599_gp_0_0_0_0, &UnityEvent_2_t4075366599_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_2_t4075366599_gp_0_0_0_0_UnityEvent_2_t4075366599_gp_1_0_0_0 = { 2, GenInst_UnityEvent_2_t4075366599_gp_0_0_0_0_UnityEvent_2_t4075366599_gp_1_0_0_0_Types };
extern const Il2CppType UnityEvent_3_t4075366600_gp_0_0_0_0;
extern const Il2CppType UnityEvent_3_t4075366600_gp_1_0_0_0;
extern const Il2CppType UnityEvent_3_t4075366600_gp_2_0_0_0;
static const Il2CppType* GenInst_UnityEvent_3_t4075366600_gp_0_0_0_0_UnityEvent_3_t4075366600_gp_1_0_0_0_UnityEvent_3_t4075366600_gp_2_0_0_0_Types[] = { &UnityEvent_3_t4075366600_gp_0_0_0_0, &UnityEvent_3_t4075366600_gp_1_0_0_0, &UnityEvent_3_t4075366600_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_3_t4075366600_gp_0_0_0_0_UnityEvent_3_t4075366600_gp_1_0_0_0_UnityEvent_3_t4075366600_gp_2_0_0_0 = { 3, GenInst_UnityEvent_3_t4075366600_gp_0_0_0_0_UnityEvent_3_t4075366600_gp_1_0_0_0_UnityEvent_3_t4075366600_gp_2_0_0_0_Types };
extern const Il2CppType UnityEvent_4_t4075366597_gp_0_0_0_0;
extern const Il2CppType UnityEvent_4_t4075366597_gp_1_0_0_0;
extern const Il2CppType UnityEvent_4_t4075366597_gp_2_0_0_0;
extern const Il2CppType UnityEvent_4_t4075366597_gp_3_0_0_0;
static const Il2CppType* GenInst_UnityEvent_4_t4075366597_gp_0_0_0_0_UnityEvent_4_t4075366597_gp_1_0_0_0_UnityEvent_4_t4075366597_gp_2_0_0_0_UnityEvent_4_t4075366597_gp_3_0_0_0_Types[] = { &UnityEvent_4_t4075366597_gp_0_0_0_0, &UnityEvent_4_t4075366597_gp_1_0_0_0, &UnityEvent_4_t4075366597_gp_2_0_0_0, &UnityEvent_4_t4075366597_gp_3_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_4_t4075366597_gp_0_0_0_0_UnityEvent_4_t4075366597_gp_1_0_0_0_UnityEvent_4_t4075366597_gp_2_0_0_0_UnityEvent_4_t4075366597_gp_3_0_0_0 = { 4, GenInst_UnityEvent_4_t4075366597_gp_0_0_0_0_UnityEvent_4_t4075366597_gp_1_0_0_0_UnityEvent_4_t4075366597_gp_2_0_0_0_UnityEvent_4_t4075366597_gp_3_0_0_0_Types };
extern const Il2CppType Runtime_GetNSObject_m58080407_gp_0_0_0_0;
static const Il2CppType* GenInst_Runtime_GetNSObject_m58080407_gp_0_0_0_0_Types[] = { &Runtime_GetNSObject_m58080407_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Runtime_GetNSObject_m58080407_gp_0_0_0_0 = { 1, GenInst_Runtime_GetNSObject_m58080407_gp_0_0_0_0_Types };
extern const Il2CppType AWSConfigs_GetConfigEnum_m1225161757_gp_0_0_0_0;
static const Il2CppType* GenInst_AWSConfigs_GetConfigEnum_m1225161757_gp_0_0_0_0_Types[] = { &AWSConfigs_GetConfigEnum_m1225161757_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_AWSConfigs_GetConfigEnum_m1225161757_gp_0_0_0_0 = { 1, GenInst_AWSConfigs_GetConfigEnum_m1225161757_gp_0_0_0_0_Types };
extern const Il2CppType AWSConfigs_ParseEnum_m486237848_gp_0_0_0_0;
static const Il2CppType* GenInst_AWSConfigs_ParseEnum_m486237848_gp_0_0_0_0_Types[] = { &AWSConfigs_ParseEnum_m486237848_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_AWSConfigs_ParseEnum_m486237848_gp_0_0_0_0 = { 1, GenInst_AWSConfigs_ParseEnum_m486237848_gp_0_0_0_0_Types };
extern const Il2CppType AWSConfigs_GetSection_m3599347093_gp_0_0_0_0;
static const Il2CppType* GenInst_AWSConfigs_GetSection_m3599347093_gp_0_0_0_0_Types[] = { &AWSConfigs_GetSection_m3599347093_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_AWSConfigs_GetSection_m3599347093_gp_0_0_0_0 = { 1, GenInst_AWSConfigs_GetSection_m3599347093_gp_0_0_0_0_Types };
extern const Il2CppType AWSSDKUtils_InvokeInBackground_m898943770_gp_0_0_0_0;
static const Il2CppType* GenInst_AWSSDKUtils_InvokeInBackground_m898943770_gp_0_0_0_0_Types[] = { &AWSSDKUtils_InvokeInBackground_m898943770_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_AWSSDKUtils_InvokeInBackground_m898943770_gp_0_0_0_0 = { 1, GenInst_AWSSDKUtils_InvokeInBackground_m898943770_gp_0_0_0_0_Types };
extern const Il2CppType U3CU3Ec__DisplayClass37_0_1_t500617122_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CU3Ec__DisplayClass37_0_1_t500617122_gp_0_0_0_0_Types[] = { &U3CU3Ec__DisplayClass37_0_1_t500617122_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CU3Ec__DisplayClass37_0_1_t500617122_gp_0_0_0_0 = { 1, GenInst_U3CU3Ec__DisplayClass37_0_1_t500617122_gp_0_0_0_0_Types };
extern const Il2CppType AndroidInterop_CallMethod_m2060567458_gp_0_0_0_0;
static const Il2CppType* GenInst_AndroidInterop_CallMethod_m2060567458_gp_0_0_0_0_Types[] = { &AndroidInterop_CallMethod_m2060567458_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidInterop_CallMethod_m2060567458_gp_0_0_0_0 = { 1, GenInst_AndroidInterop_CallMethod_m2060567458_gp_0_0_0_0_Types };
extern const Il2CppType AndroidInterop_GetJavaField_m3750603155_gp_0_0_0_0;
static const Il2CppType* GenInst_AndroidInterop_GetJavaField_m3750603155_gp_0_0_0_0_Types[] = { &AndroidInterop_GetJavaField_m3750603155_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidInterop_GetJavaField_m3750603155_gp_0_0_0_0 = { 1, GenInst_AndroidInterop_GetJavaField_m3750603155_gp_0_0_0_0_Types };
extern const Il2CppType U3CU3Ec__2_1_t1696963520_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CU3Ec__2_1_t1696963520_gp_0_0_0_0_Types[] = { &U3CU3Ec__2_1_t1696963520_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CU3Ec__2_1_t1696963520_gp_0_0_0_0 = { 1, GenInst_U3CU3Ec__2_1_t1696963520_gp_0_0_0_0_Types };
extern const Il2CppType U3CU3Ec__6_1_t2261613524_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CU3Ec__6_1_t2261613524_gp_0_0_0_0_Types[] = { &U3CU3Ec__6_1_t2261613524_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CU3Ec__6_1_t2261613524_gp_0_0_0_0 = { 1, GenInst_U3CU3Ec__6_1_t2261613524_gp_0_0_0_0_Types };
extern const Il2CppType ServiceFactory_GetService_m3760662002_gp_0_0_0_0;
static const Il2CppType* GenInst_ServiceFactory_GetService_m3760662002_gp_0_0_0_0_Types[] = { &ServiceFactory_GetService_m3760662002_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ServiceFactory_GetService_m3760662002_gp_0_0_0_0 = { 1, GenInst_ServiceFactory_GetService_m3760662002_gp_0_0_0_0_Types };
extern const Il2CppType IHttpRequestFactory_1_t4193720315_gp_0_0_0_0;
static const Il2CppType* GenInst_IHttpRequestFactory_1_t4193720315_gp_0_0_0_0_Types[] = { &IHttpRequestFactory_1_t4193720315_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IHttpRequestFactory_1_t4193720315_gp_0_0_0_0 = { 1, GenInst_IHttpRequestFactory_1_t4193720315_gp_0_0_0_0_Types };
extern const Il2CppType AmazonServiceCallback_2_t3530050132_gp_0_0_0_0;
extern const Il2CppType AmazonServiceCallback_2_t3530050132_gp_1_0_0_0;
static const Il2CppType* GenInst_AmazonServiceCallback_2_t3530050132_gp_0_0_0_0_AmazonServiceCallback_2_t3530050132_gp_1_0_0_0_Types[] = { &AmazonServiceCallback_2_t3530050132_gp_0_0_0_0, &AmazonServiceCallback_2_t3530050132_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_AmazonServiceCallback_2_t3530050132_gp_0_0_0_0_AmazonServiceCallback_2_t3530050132_gp_1_0_0_0 = { 2, GenInst_AmazonServiceCallback_2_t3530050132_gp_0_0_0_0_AmazonServiceCallback_2_t3530050132_gp_1_0_0_0_Types };
extern const Il2CppType AmazonServiceResult_2_t529501990_gp_0_0_0_0;
extern const Il2CppType AmazonServiceResult_2_t529501990_gp_1_0_0_0;
static const Il2CppType* GenInst_AmazonServiceResult_2_t529501990_gp_0_0_0_0_AmazonServiceResult_2_t529501990_gp_1_0_0_0_Types[] = { &AmazonServiceResult_2_t529501990_gp_0_0_0_0, &AmazonServiceResult_2_t529501990_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_AmazonServiceResult_2_t529501990_gp_0_0_0_0_AmazonServiceResult_2_t529501990_gp_1_0_0_0 = { 2, GenInst_AmazonServiceResult_2_t529501990_gp_0_0_0_0_AmazonServiceResult_2_t529501990_gp_1_0_0_0_Types };
extern const Il2CppType ExceptionHandler_1_t3179100912_gp_0_0_0_0;
static const Il2CppType* GenInst_ExceptionHandler_1_t3179100912_gp_0_0_0_0_Types[] = { &ExceptionHandler_1_t3179100912_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExceptionHandler_1_t3179100912_gp_0_0_0_0 = { 1, GenInst_ExceptionHandler_1_t3179100912_gp_0_0_0_0_Types };
extern const Il2CppType HttpHandler_1_t1383532101_gp_0_0_0_0;
static const Il2CppType* GenInst_HttpHandler_1_t1383532101_gp_0_0_0_0_Types[] = { &HttpHandler_1_t1383532101_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_HttpHandler_1_t1383532101_gp_0_0_0_0 = { 1, GenInst_HttpHandler_1_t1383532101_gp_0_0_0_0_Types };
extern const Il2CppType DefaultRetryPolicy_IsInnerException_m2974190552_gp_0_0_0_0;
static const Il2CppType* GenInst_DefaultRetryPolicy_IsInnerException_m2974190552_gp_0_0_0_0_Types[] = { &DefaultRetryPolicy_IsInnerException_m2974190552_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_DefaultRetryPolicy_IsInnerException_m2974190552_gp_0_0_0_0 = { 1, GenInst_DefaultRetryPolicy_IsInnerException_m2974190552_gp_0_0_0_0_Types };
extern const Il2CppType AlwaysSendList_1_t1375725976_gp_0_0_0_0;
static const Il2CppType* GenInst_AlwaysSendList_1_t1375725976_gp_0_0_0_0_Types[] = { &AlwaysSendList_1_t1375725976_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_AlwaysSendList_1_t1375725976_gp_0_0_0_0 = { 1, GenInst_AlwaysSendList_1_t1375725976_gp_0_0_0_0_Types };
extern const Il2CppType BackgroundDispatcher_1_t295417742_gp_0_0_0_0;
static const Il2CppType* GenInst_BackgroundDispatcher_1_t295417742_gp_0_0_0_0_Types[] = { &BackgroundDispatcher_1_t295417742_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_BackgroundDispatcher_1_t295417742_gp_0_0_0_0 = { 1, GenInst_BackgroundDispatcher_1_t295417742_gp_0_0_0_0_Types };
extern const Il2CppType ThreadPoolThrottler_1_t1982498819_gp_0_0_0_0;
static const Il2CppType* GenInst_ThreadPoolThrottler_1_t1982498819_gp_0_0_0_0_Types[] = { &ThreadPoolThrottler_1_t1982498819_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ThreadPoolThrottler_1_t1982498819_gp_0_0_0_0 = { 1, GenInst_ThreadPoolThrottler_1_t1982498819_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Exception_t1927440687_0_0_0_ThreadPoolThrottler_1_t1982498819_gp_0_0_0_0_Types[] = { &Exception_t1927440687_0_0_0, &ThreadPoolThrottler_1_t1982498819_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Exception_t1927440687_0_0_0_ThreadPoolThrottler_1_t1982498819_gp_0_0_0_0 = { 2, GenInst_Exception_t1927440687_0_0_0_ThreadPoolThrottler_1_t1982498819_gp_0_0_0_0_Types };
extern const Il2CppType ThreadPoolOptions_1_t170443295_0_0_0;
static const Il2CppType* GenInst_ThreadPoolOptions_1_t170443295_0_0_0_Types[] = { &ThreadPoolOptions_1_t170443295_0_0_0 };
extern const Il2CppGenericInst GenInst_ThreadPoolOptions_1_t170443295_0_0_0 = { 1, GenInst_ThreadPoolOptions_1_t170443295_0_0_0_Types };
static const Il2CppType* GenInst_ThreadPoolThrottler_1_t1982498819_gp_0_0_0_0_ThreadPoolThrottler_1_t1982498819_gp_0_0_0_0_Types[] = { &ThreadPoolThrottler_1_t1982498819_gp_0_0_0_0, &ThreadPoolThrottler_1_t1982498819_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ThreadPoolThrottler_1_t1982498819_gp_0_0_0_0_ThreadPoolThrottler_1_t1982498819_gp_0_0_0_0 = { 2, GenInst_ThreadPoolThrottler_1_t1982498819_gp_0_0_0_0_ThreadPoolThrottler_1_t1982498819_gp_0_0_0_0_Types };
extern const Il2CppType ThreadPoolOptions_1_t2545192696_gp_1_0_0_0;
static const Il2CppType* GenInst_ThreadPoolOptions_1_t2545192696_gp_1_0_0_0_Types[] = { &ThreadPoolOptions_1_t2545192696_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ThreadPoolOptions_1_t2545192696_gp_1_0_0_0 = { 1, GenInst_ThreadPoolOptions_1_t2545192696_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Exception_t1927440687_0_0_0_ThreadPoolOptions_1_t2545192696_gp_1_0_0_0_Types[] = { &Exception_t1927440687_0_0_0, &ThreadPoolOptions_1_t2545192696_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Exception_t1927440687_0_0_0_ThreadPoolOptions_1_t2545192696_gp_1_0_0_0 = { 2, GenInst_Exception_t1927440687_0_0_0_ThreadPoolOptions_1_t2545192696_gp_1_0_0_0_Types };
extern const Il2CppType IResponseUnmarshaller_2_t2708327486_gp_0_0_0_0;
extern const Il2CppType IResponseUnmarshaller_2_t2708327486_gp_1_0_0_0;
static const Il2CppType* GenInst_IResponseUnmarshaller_2_t2708327486_gp_0_0_0_0_IResponseUnmarshaller_2_t2708327486_gp_1_0_0_0_Types[] = { &IResponseUnmarshaller_2_t2708327486_gp_0_0_0_0, &IResponseUnmarshaller_2_t2708327486_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_IResponseUnmarshaller_2_t2708327486_gp_0_0_0_0_IResponseUnmarshaller_2_t2708327486_gp_1_0_0_0 = { 2, GenInst_IResponseUnmarshaller_2_t2708327486_gp_0_0_0_0_IResponseUnmarshaller_2_t2708327486_gp_1_0_0_0_Types };
extern const Il2CppType ListUnmarshaller_2_t3080449122_gp_0_0_0_0;
static const Il2CppType* GenInst_ListUnmarshaller_2_t3080449122_gp_0_0_0_0_Types[] = { &ListUnmarshaller_2_t3080449122_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ListUnmarshaller_2_t3080449122_gp_0_0_0_0 = { 1, GenInst_ListUnmarshaller_2_t3080449122_gp_0_0_0_0_Types };
extern const Il2CppType List_1_t3097202703_0_0_0;
static const Il2CppType* GenInst_List_1_t3097202703_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0_Types[] = { &List_1_t3097202703_0_0_0, &XmlUnmarshallerContext_t1179575220_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t3097202703_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0 = { 2, GenInst_List_1_t3097202703_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0_Types };
static const Il2CppType* GenInst_List_1_t3097202703_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0_Types[] = { &List_1_t3097202703_0_0_0, &JsonUnmarshallerContext_t456235889_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t3097202703_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0 = { 2, GenInst_List_1_t3097202703_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0_Types };
static const Il2CppType* GenInst_ListUnmarshaller_2_t3080449122_gp_0_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0_Types[] = { &ListUnmarshaller_2_t3080449122_gp_0_0_0_0, &XmlUnmarshallerContext_t1179575220_0_0_0 };
extern const Il2CppGenericInst GenInst_ListUnmarshaller_2_t3080449122_gp_0_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0 = { 2, GenInst_ListUnmarshaller_2_t3080449122_gp_0_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0_Types };
static const Il2CppType* GenInst_ListUnmarshaller_2_t3080449122_gp_0_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0_Types[] = { &ListUnmarshaller_2_t3080449122_gp_0_0_0_0, &JsonUnmarshallerContext_t456235889_0_0_0 };
extern const Il2CppGenericInst GenInst_ListUnmarshaller_2_t3080449122_gp_0_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0 = { 2, GenInst_ListUnmarshaller_2_t3080449122_gp_0_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0_Types };
extern const Il2CppType UnmarshallerExtensions_Add_m3566798727_gp_0_0_0_0;
extern const Il2CppType UnmarshallerExtensions_Add_m3566798727_gp_1_0_0_0;
static const Il2CppType* GenInst_UnmarshallerExtensions_Add_m3566798727_gp_0_0_0_0_UnmarshallerExtensions_Add_m3566798727_gp_1_0_0_0_Types[] = { &UnmarshallerExtensions_Add_m3566798727_gp_0_0_0_0, &UnmarshallerExtensions_Add_m3566798727_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_UnmarshallerExtensions_Add_m3566798727_gp_0_0_0_0_UnmarshallerExtensions_Add_m3566798727_gp_1_0_0_0 = { 2, GenInst_UnmarshallerExtensions_Add_m3566798727_gp_0_0_0_0_UnmarshallerExtensions_Add_m3566798727_gp_1_0_0_0_Types };
extern const Il2CppType ExecuteEvents_Execute_m1961163955_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_Execute_m1961163955_gp_0_0_0_0_Types[] = { &ExecuteEvents_Execute_m1961163955_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_Execute_m1961163955_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_Execute_m1961163955_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_ExecuteHierarchy_m1189839031_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_ExecuteHierarchy_m1189839031_gp_0_0_0_0_Types[] = { &ExecuteEvents_ExecuteHierarchy_m1189839031_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_ExecuteHierarchy_m1189839031_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_ExecuteHierarchy_m1189839031_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_GetEventList_m1741636447_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_GetEventList_m1741636447_gp_0_0_0_0_Types[] = { &ExecuteEvents_GetEventList_m1741636447_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_GetEventList_m1741636447_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_GetEventList_m1741636447_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_CanHandleEvent_m3661808413_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_CanHandleEvent_m3661808413_gp_0_0_0_0_Types[] = { &ExecuteEvents_CanHandleEvent_m3661808413_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_CanHandleEvent_m3661808413_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_CanHandleEvent_m3661808413_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_GetEventHandler_m609328278_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_GetEventHandler_m609328278_gp_0_0_0_0_Types[] = { &ExecuteEvents_GetEventHandler_m609328278_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_GetEventHandler_m609328278_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_GetEventHandler_m609328278_gp_0_0_0_0_Types };
extern const Il2CppType TweenRunner_1_t2584777480_gp_0_0_0_0;
static const Il2CppType* GenInst_TweenRunner_1_t2584777480_gp_0_0_0_0_Types[] = { &TweenRunner_1_t2584777480_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenRunner_1_t2584777480_gp_0_0_0_0 = { 1, GenInst_TweenRunner_1_t2584777480_gp_0_0_0_0_Types };
extern const Il2CppType Dropdown_GetOrAddComponent_m1533008304_gp_0_0_0_0;
static const Il2CppType* GenInst_Dropdown_GetOrAddComponent_m1533008304_gp_0_0_0_0_Types[] = { &Dropdown_GetOrAddComponent_m1533008304_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dropdown_GetOrAddComponent_m1533008304_gp_0_0_0_0 = { 1, GenInst_Dropdown_GetOrAddComponent_m1533008304_gp_0_0_0_0_Types };
extern const Il2CppType SetPropertyUtility_SetEquatableStruct_m753578693_gp_0_0_0_0;
static const Il2CppType* GenInst_SetPropertyUtility_SetEquatableStruct_m753578693_gp_0_0_0_0_Types[] = { &SetPropertyUtility_SetEquatableStruct_m753578693_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_SetPropertyUtility_SetEquatableStruct_m753578693_gp_0_0_0_0 = { 1, GenInst_SetPropertyUtility_SetEquatableStruct_m753578693_gp_0_0_0_0_Types };
extern const Il2CppType IndexedSet_1_t573160278_gp_0_0_0_0;
static const Il2CppType* GenInst_IndexedSet_1_t573160278_gp_0_0_0_0_Types[] = { &IndexedSet_1_t573160278_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IndexedSet_1_t573160278_gp_0_0_0_0 = { 1, GenInst_IndexedSet_1_t573160278_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_IndexedSet_1_t573160278_gp_0_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &IndexedSet_1_t573160278_gp_0_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_IndexedSet_1_t573160278_gp_0_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_IndexedSet_1_t573160278_gp_0_0_0_0_Int32_t2071877448_0_0_0_Types };
extern const Il2CppType ListPool_1_t1984115411_gp_0_0_0_0;
static const Il2CppType* GenInst_ListPool_1_t1984115411_gp_0_0_0_0_Types[] = { &ListPool_1_t1984115411_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ListPool_1_t1984115411_gp_0_0_0_0 = { 1, GenInst_ListPool_1_t1984115411_gp_0_0_0_0_Types };
extern const Il2CppType List_1_t2000868992_0_0_0;
static const Il2CppType* GenInst_List_1_t2000868992_0_0_0_Types[] = { &List_1_t2000868992_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t2000868992_0_0_0 = { 1, GenInst_List_1_t2000868992_0_0_0_Types };
extern const Il2CppType ObjectPool_1_t4265859154_gp_0_0_0_0;
static const Il2CppType* GenInst_ObjectPool_1_t4265859154_gp_0_0_0_0_Types[] = { &ObjectPool_1_t4265859154_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectPool_1_t4265859154_gp_0_0_0_0 = { 1, GenInst_ObjectPool_1_t4265859154_gp_0_0_0_0_Types };
extern const Il2CppType DefaultExecutionOrder_t2717914595_0_0_0;
static const Il2CppType* GenInst_DefaultExecutionOrder_t2717914595_0_0_0_Types[] = { &DefaultExecutionOrder_t2717914595_0_0_0 };
extern const Il2CppGenericInst GenInst_DefaultExecutionOrder_t2717914595_0_0_0 = { 1, GenInst_DefaultExecutionOrder_t2717914595_0_0_0_Types };
extern const Il2CppType GUILayer_t3254902478_0_0_0;
static const Il2CppType* GenInst_GUILayer_t3254902478_0_0_0_Types[] = { &GUILayer_t3254902478_0_0_0 };
extern const Il2CppGenericInst GenInst_GUILayer_t3254902478_0_0_0 = { 1, GenInst_GUILayer_t3254902478_0_0_0_Types };
extern const Il2CppType LoggingOptions_t2865640765_0_0_0;
static const Il2CppType* GenInst_LoggingOptions_t2865640765_0_0_0_Types[] = { &LoggingOptions_t2865640765_0_0_0 };
extern const Il2CppGenericInst GenInst_LoggingOptions_t2865640765_0_0_0 = { 1, GenInst_LoggingOptions_t2865640765_0_0_0_Types };
extern const Il2CppType ResponseLoggingOption_t3443611737_0_0_0;
static const Il2CppType* GenInst_ResponseLoggingOption_t3443611737_0_0_0_Types[] = { &ResponseLoggingOption_t3443611737_0_0_0 };
extern const Il2CppGenericInst GenInst_ResponseLoggingOption_t3443611737_0_0_0 = { 1, GenInst_ResponseLoggingOption_t3443611737_0_0_0_Types };
extern const Il2CppType ThreadAbortException_t1150575753_0_0_0;
static const Il2CppType* GenInst_ThreadAbortException_t1150575753_0_0_0_Types[] = { &ThreadAbortException_t1150575753_0_0_0 };
extern const Il2CppGenericInst GenInst_ThreadAbortException_t1150575753_0_0_0 = { 1, GenInst_ThreadAbortException_t1150575753_0_0_0_Types };
extern const Il2CppType WebException_t3368933679_0_0_0;
static const Il2CppType* GenInst_WebException_t3368933679_0_0_0_Types[] = { &WebException_t3368933679_0_0_0 };
extern const Il2CppGenericInst GenInst_WebException_t3368933679_0_0_0 = { 1, GenInst_WebException_t3368933679_0_0_0_Types };
extern const Il2CppType INetworkReachability_t2670889314_0_0_0;
static const Il2CppType* GenInst_INetworkReachability_t2670889314_0_0_0_Types[] = { &INetworkReachability_t2670889314_0_0_0 };
extern const Il2CppGenericInst GenInst_INetworkReachability_t2670889314_0_0_0 = { 1, GenInst_INetworkReachability_t2670889314_0_0_0_Types };
extern const Il2CppType UnityInitializer_t2778189483_0_0_0;
static const Il2CppType* GenInst_UnityInitializer_t2778189483_0_0_0_Types[] = { &UnityInitializer_t2778189483_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityInitializer_t2778189483_0_0_0 = { 1, GenInst_UnityInitializer_t2778189483_0_0_0_Types };
extern const Il2CppType UnityMainThreadDispatcher_t4098072663_0_0_0;
static const Il2CppType* GenInst_UnityMainThreadDispatcher_t4098072663_0_0_0_Types[] = { &UnityMainThreadDispatcher_t4098072663_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityMainThreadDispatcher_t4098072663_0_0_0 = { 1, GenInst_UnityMainThreadDispatcher_t4098072663_0_0_0_Types };
extern const Il2CppType NSDictionary_t3598984691_0_0_0;
static const Il2CppType* GenInst_NSDictionary_t3598984691_0_0_0_Types[] = { &NSDictionary_t3598984691_0_0_0 };
extern const Il2CppGenericInst GenInst_NSDictionary_t3598984691_0_0_0 = { 1, GenInst_NSDictionary_t3598984691_0_0_0_Types };
extern const Il2CppType IEnvironmentInfo_t2314249358_0_0_0;
static const Il2CppType* GenInst_IEnvironmentInfo_t2314249358_0_0_0_Types[] = { &IEnvironmentInfo_t2314249358_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnvironmentInfo_t2314249358_0_0_0 = { 1, GenInst_IEnvironmentInfo_t2314249358_0_0_0_Types };
extern const Il2CppType AWSSection_t3096528870_0_0_0;
static const Il2CppType* GenInst_AWSSection_t3096528870_0_0_0_Types[] = { &AWSSection_t3096528870_0_0_0 };
extern const Il2CppGenericInst GenInst_AWSSection_t3096528870_0_0_0 = { 1, GenInst_AWSSection_t3096528870_0_0_0_Types };
extern const Il2CppType NSBundle_t3599697655_0_0_0;
static const Il2CppType* GenInst_NSBundle_t3599697655_0_0_0_Types[] = { &NSBundle_t3599697655_0_0_0 };
extern const Il2CppGenericInst GenInst_NSBundle_t3599697655_0_0_0 = { 1, GenInst_NSBundle_t3599697655_0_0_0_Types };
extern const Il2CppType NSObject_t1518098886_0_0_0;
static const Il2CppType* GenInst_NSObject_t1518098886_0_0_0_Types[] = { &NSObject_t1518098886_0_0_0 };
extern const Il2CppGenericInst GenInst_NSObject_t1518098886_0_0_0 = { 1, GenInst_NSObject_t1518098886_0_0_0_Types };
extern const Il2CppType NSLocale_t2224424797_0_0_0;
static const Il2CppType* GenInst_NSLocale_t2224424797_0_0_0_Types[] = { &NSLocale_t2224424797_0_0_0 };
extern const Il2CppGenericInst GenInst_NSLocale_t2224424797_0_0_0 = { 1, GenInst_NSLocale_t2224424797_0_0_0_Types };
extern const Il2CppType UIDevice_t860038178_0_0_0;
static const Il2CppType* GenInst_UIDevice_t860038178_0_0_0_Types[] = { &UIDevice_t860038178_0_0_0 };
extern const Il2CppGenericInst GenInst_UIDevice_t860038178_0_0_0 = { 1, GenInst_UIDevice_t860038178_0_0_0_Types };
static const Il2CppType* GenInst_AssumeRoleWithWebIdentityRequest_t193094215_0_0_0_Types[] = { &AssumeRoleWithWebIdentityRequest_t193094215_0_0_0 };
extern const Il2CppGenericInst GenInst_AssumeRoleWithWebIdentityRequest_t193094215_0_0_0 = { 1, GenInst_AssumeRoleWithWebIdentityRequest_t193094215_0_0_0_Types };
extern const Il2CppType GetCredentialsForIdentityResponse_t2302678766_0_0_0;
static const Il2CppType* GenInst_GetCredentialsForIdentityRequest_t932830458_0_0_0_GetCredentialsForIdentityResponse_t2302678766_0_0_0_Types[] = { &GetCredentialsForIdentityRequest_t932830458_0_0_0, &GetCredentialsForIdentityResponse_t2302678766_0_0_0 };
extern const Il2CppGenericInst GenInst_GetCredentialsForIdentityRequest_t932830458_0_0_0_GetCredentialsForIdentityResponse_t2302678766_0_0_0 = { 2, GenInst_GetCredentialsForIdentityRequest_t932830458_0_0_0_GetCredentialsForIdentityResponse_t2302678766_0_0_0_Types };
extern const Il2CppType GetIdResponse_t2091118072_0_0_0;
static const Il2CppType* GenInst_GetIdRequest_t4078561340_0_0_0_GetIdResponse_t2091118072_0_0_0_Types[] = { &GetIdRequest_t4078561340_0_0_0, &GetIdResponse_t2091118072_0_0_0 };
extern const Il2CppGenericInst GenInst_GetIdRequest_t4078561340_0_0_0_GetIdResponse_t2091118072_0_0_0 = { 2, GenInst_GetIdRequest_t4078561340_0_0_0_GetIdResponse_t2091118072_0_0_0_Types };
extern const Il2CppType GetOpenIdTokenResponse_t955597635_0_0_0;
static const Il2CppType* GenInst_GetOpenIdTokenRequest_t2698079901_0_0_0_GetOpenIdTokenResponse_t955597635_0_0_0_Types[] = { &GetOpenIdTokenRequest_t2698079901_0_0_0, &GetOpenIdTokenResponse_t955597635_0_0_0 };
extern const Il2CppGenericInst GenInst_GetOpenIdTokenRequest_t2698079901_0_0_0_GetOpenIdTokenResponse_t955597635_0_0_0 = { 2, GenInst_GetOpenIdTokenRequest_t2698079901_0_0_0_GetOpenIdTokenResponse_t955597635_0_0_0_Types };
extern const Il2CppType IApplicationSettings_t2849513616_0_0_0;
static const Il2CppType* GenInst_IApplicationSettings_t2849513616_0_0_0_Types[] = { &IApplicationSettings_t2849513616_0_0_0 };
extern const Il2CppGenericInst GenInst_IApplicationSettings_t2849513616_0_0_0 = { 1, GenInst_IApplicationSettings_t2849513616_0_0_0_Types };
extern const Il2CppType CredentialsRetriever_t208022274_0_0_0;
static const Il2CppType* GenInst_CredentialsRetriever_t208022274_0_0_0_Types[] = { &CredentialsRetriever_t208022274_0_0_0 };
extern const Il2CppGenericInst GenInst_CredentialsRetriever_t208022274_0_0_0 = { 1, GenInst_CredentialsRetriever_t208022274_0_0_0_Types };
extern const Il2CppType Marshaller_t3371889241_0_0_0;
static const Il2CppType* GenInst_Marshaller_t3371889241_0_0_0_Types[] = { &Marshaller_t3371889241_0_0_0 };
extern const Il2CppGenericInst GenInst_Marshaller_t3371889241_0_0_0 = { 1, GenInst_Marshaller_t3371889241_0_0_0_Types };
extern const Il2CppType Signer_t1810841758_0_0_0;
static const Il2CppType* GenInst_Signer_t1810841758_0_0_0_Types[] = { &Signer_t1810841758_0_0_0 };
extern const Il2CppGenericInst GenInst_Signer_t1810841758_0_0_0 = { 1, GenInst_Signer_t1810841758_0_0_0_Types };
extern const Il2CppType DeleteDatasetResponse_t2927760190_0_0_0;
static const Il2CppType* GenInst_DeleteDatasetRequest_t3672322944_0_0_0_DeleteDatasetResponse_t2927760190_0_0_0_Types[] = { &DeleteDatasetRequest_t3672322944_0_0_0, &DeleteDatasetResponse_t2927760190_0_0_0 };
extern const Il2CppGenericInst GenInst_DeleteDatasetRequest_t3672322944_0_0_0_DeleteDatasetResponse_t2927760190_0_0_0 = { 2, GenInst_DeleteDatasetRequest_t3672322944_0_0_0_DeleteDatasetResponse_t2927760190_0_0_0_Types };
extern const Il2CppType ListRecordsResponse_t1441155223_0_0_0;
static const Il2CppType* GenInst_ListRecordsRequest_t3074726983_0_0_0_ListRecordsResponse_t1441155223_0_0_0_Types[] = { &ListRecordsRequest_t3074726983_0_0_0, &ListRecordsResponse_t1441155223_0_0_0 };
extern const Il2CppGenericInst GenInst_ListRecordsRequest_t3074726983_0_0_0_ListRecordsResponse_t1441155223_0_0_0 = { 2, GenInst_ListRecordsRequest_t3074726983_0_0_0_ListRecordsResponse_t1441155223_0_0_0_Types };
extern const Il2CppType UpdateRecordsResponse_t1893058994_0_0_0;
static const Il2CppType* GenInst_UpdateRecordsRequest_t197801344_0_0_0_UpdateRecordsResponse_t1893058994_0_0_0_Types[] = { &UpdateRecordsRequest_t197801344_0_0_0, &UpdateRecordsResponse_t1893058994_0_0_0 };
extern const Il2CppGenericInst GenInst_UpdateRecordsRequest_t197801344_0_0_0_UpdateRecordsResponse_t1893058994_0_0_0 = { 2, GenInst_UpdateRecordsRequest_t197801344_0_0_0_UpdateRecordsResponse_t1893058994_0_0_0_Types };
extern const Il2CppType EventSystem_t3466835263_0_0_0;
static const Il2CppType* GenInst_EventSystem_t3466835263_0_0_0_Types[] = { &EventSystem_t3466835263_0_0_0 };
extern const Il2CppGenericInst GenInst_EventSystem_t3466835263_0_0_0 = { 1, GenInst_EventSystem_t3466835263_0_0_0_Types };
extern const Il2CppType AxisEventData_t1524870173_0_0_0;
static const Il2CppType* GenInst_AxisEventData_t1524870173_0_0_0_Types[] = { &AxisEventData_t1524870173_0_0_0 };
extern const Il2CppGenericInst GenInst_AxisEventData_t1524870173_0_0_0 = { 1, GenInst_AxisEventData_t1524870173_0_0_0_Types };
extern const Il2CppType SpriteRenderer_t1209076198_0_0_0;
static const Il2CppType* GenInst_SpriteRenderer_t1209076198_0_0_0_Types[] = { &SpriteRenderer_t1209076198_0_0_0 };
extern const Il2CppGenericInst GenInst_SpriteRenderer_t1209076198_0_0_0 = { 1, GenInst_SpriteRenderer_t1209076198_0_0_0_Types };
extern const Il2CppType AspectMode_t1166448724_0_0_0;
static const Il2CppType* GenInst_AspectMode_t1166448724_0_0_0_Types[] = { &AspectMode_t1166448724_0_0_0 };
extern const Il2CppGenericInst GenInst_AspectMode_t1166448724_0_0_0 = { 1, GenInst_AspectMode_t1166448724_0_0_0_Types };
extern const Il2CppType FitMode_t4030874534_0_0_0;
static const Il2CppType* GenInst_FitMode_t4030874534_0_0_0_Types[] = { &FitMode_t4030874534_0_0_0 };
extern const Il2CppGenericInst GenInst_FitMode_t4030874534_0_0_0 = { 1, GenInst_FitMode_t4030874534_0_0_0_Types };
extern const Il2CppType GraphicRaycaster_t410733016_0_0_0;
static const Il2CppType* GenInst_GraphicRaycaster_t410733016_0_0_0_Types[] = { &GraphicRaycaster_t410733016_0_0_0 };
extern const Il2CppGenericInst GenInst_GraphicRaycaster_t410733016_0_0_0 = { 1, GenInst_GraphicRaycaster_t410733016_0_0_0_Types };
extern const Il2CppType Image_t2042527209_0_0_0;
static const Il2CppType* GenInst_Image_t2042527209_0_0_0_Types[] = { &Image_t2042527209_0_0_0 };
extern const Il2CppGenericInst GenInst_Image_t2042527209_0_0_0 = { 1, GenInst_Image_t2042527209_0_0_0_Types };
extern const Il2CppType Button_t2872111280_0_0_0;
static const Il2CppType* GenInst_Button_t2872111280_0_0_0_Types[] = { &Button_t2872111280_0_0_0 };
extern const Il2CppGenericInst GenInst_Button_t2872111280_0_0_0 = { 1, GenInst_Button_t2872111280_0_0_0_Types };
extern const Il2CppType Dropdown_t1985816271_0_0_0;
static const Il2CppType* GenInst_Dropdown_t1985816271_0_0_0_Types[] = { &Dropdown_t1985816271_0_0_0 };
extern const Il2CppGenericInst GenInst_Dropdown_t1985816271_0_0_0 = { 1, GenInst_Dropdown_t1985816271_0_0_0_Types };
extern const Il2CppType CanvasRenderer_t261436805_0_0_0;
static const Il2CppType* GenInst_CanvasRenderer_t261436805_0_0_0_Types[] = { &CanvasRenderer_t261436805_0_0_0 };
extern const Il2CppGenericInst GenInst_CanvasRenderer_t261436805_0_0_0 = { 1, GenInst_CanvasRenderer_t261436805_0_0_0_Types };
extern const Il2CppType Corner_t1077473318_0_0_0;
static const Il2CppType* GenInst_Corner_t1077473318_0_0_0_Types[] = { &Corner_t1077473318_0_0_0 };
extern const Il2CppGenericInst GenInst_Corner_t1077473318_0_0_0 = { 1, GenInst_Corner_t1077473318_0_0_0_Types };
extern const Il2CppType Axis_t1431825778_0_0_0;
static const Il2CppType* GenInst_Axis_t1431825778_0_0_0_Types[] = { &Axis_t1431825778_0_0_0 };
extern const Il2CppGenericInst GenInst_Axis_t1431825778_0_0_0 = { 1, GenInst_Axis_t1431825778_0_0_0_Types };
extern const Il2CppType Constraint_t3558160636_0_0_0;
static const Il2CppType* GenInst_Constraint_t3558160636_0_0_0_Types[] = { &Constraint_t3558160636_0_0_0 };
extern const Il2CppGenericInst GenInst_Constraint_t3558160636_0_0_0 = { 1, GenInst_Constraint_t3558160636_0_0_0_Types };
extern const Il2CppType Type_t3352948571_0_0_0;
static const Il2CppType* GenInst_Type_t3352948571_0_0_0_Types[] = { &Type_t3352948571_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t3352948571_0_0_0 = { 1, GenInst_Type_t3352948571_0_0_0_Types };
extern const Il2CppType FillMethod_t1640962579_0_0_0;
static const Il2CppType* GenInst_FillMethod_t1640962579_0_0_0_Types[] = { &FillMethod_t1640962579_0_0_0 };
extern const Il2CppGenericInst GenInst_FillMethod_t1640962579_0_0_0 = { 1, GenInst_FillMethod_t1640962579_0_0_0_Types };
extern const Il2CppType SubmitEvent_t907918422_0_0_0;
static const Il2CppType* GenInst_SubmitEvent_t907918422_0_0_0_Types[] = { &SubmitEvent_t907918422_0_0_0 };
extern const Il2CppGenericInst GenInst_SubmitEvent_t907918422_0_0_0 = { 1, GenInst_SubmitEvent_t907918422_0_0_0_Types };
extern const Il2CppType OnChangeEvent_t2863344003_0_0_0;
static const Il2CppType* GenInst_OnChangeEvent_t2863344003_0_0_0_Types[] = { &OnChangeEvent_t2863344003_0_0_0 };
extern const Il2CppGenericInst GenInst_OnChangeEvent_t2863344003_0_0_0 = { 1, GenInst_OnChangeEvent_t2863344003_0_0_0_Types };
extern const Il2CppType OnValidateInput_t1946318473_0_0_0;
static const Il2CppType* GenInst_OnValidateInput_t1946318473_0_0_0_Types[] = { &OnValidateInput_t1946318473_0_0_0 };
extern const Il2CppGenericInst GenInst_OnValidateInput_t1946318473_0_0_0 = { 1, GenInst_OnValidateInput_t1946318473_0_0_0_Types };
extern const Il2CppType LineType_t2931319356_0_0_0;
static const Il2CppType* GenInst_LineType_t2931319356_0_0_0_Types[] = { &LineType_t2931319356_0_0_0 };
extern const Il2CppGenericInst GenInst_LineType_t2931319356_0_0_0 = { 1, GenInst_LineType_t2931319356_0_0_0_Types };
extern const Il2CppType InputType_t1274231802_0_0_0;
static const Il2CppType* GenInst_InputType_t1274231802_0_0_0_Types[] = { &InputType_t1274231802_0_0_0 };
extern const Il2CppGenericInst GenInst_InputType_t1274231802_0_0_0 = { 1, GenInst_InputType_t1274231802_0_0_0_Types };
extern const Il2CppType TouchScreenKeyboardType_t875112366_0_0_0;
static const Il2CppType* GenInst_TouchScreenKeyboardType_t875112366_0_0_0_Types[] = { &TouchScreenKeyboardType_t875112366_0_0_0 };
extern const Il2CppGenericInst GenInst_TouchScreenKeyboardType_t875112366_0_0_0 = { 1, GenInst_TouchScreenKeyboardType_t875112366_0_0_0_Types };
extern const Il2CppType CharacterValidation_t3437478890_0_0_0;
static const Il2CppType* GenInst_CharacterValidation_t3437478890_0_0_0_Types[] = { &CharacterValidation_t3437478890_0_0_0 };
extern const Il2CppGenericInst GenInst_CharacterValidation_t3437478890_0_0_0 = { 1, GenInst_CharacterValidation_t3437478890_0_0_0_Types };
extern const Il2CppType LayoutElement_t2808691390_0_0_0;
static const Il2CppType* GenInst_LayoutElement_t2808691390_0_0_0_Types[] = { &LayoutElement_t2808691390_0_0_0 };
extern const Il2CppGenericInst GenInst_LayoutElement_t2808691390_0_0_0 = { 1, GenInst_LayoutElement_t2808691390_0_0_0_Types };
extern const Il2CppType RectOffset_t3387826427_0_0_0;
static const Il2CppType* GenInst_RectOffset_t3387826427_0_0_0_Types[] = { &RectOffset_t3387826427_0_0_0 };
extern const Il2CppGenericInst GenInst_RectOffset_t3387826427_0_0_0 = { 1, GenInst_RectOffset_t3387826427_0_0_0_Types };
extern const Il2CppType TextAnchor_t112990806_0_0_0;
static const Il2CppType* GenInst_TextAnchor_t112990806_0_0_0_Types[] = { &TextAnchor_t112990806_0_0_0 };
extern const Il2CppGenericInst GenInst_TextAnchor_t112990806_0_0_0 = { 1, GenInst_TextAnchor_t112990806_0_0_0_Types };
extern const Il2CppType Direction_t3696775921_0_0_0;
static const Il2CppType* GenInst_Direction_t3696775921_0_0_0_Types[] = { &Direction_t3696775921_0_0_0 };
extern const Il2CppGenericInst GenInst_Direction_t3696775921_0_0_0 = { 1, GenInst_Direction_t3696775921_0_0_0_Types };
extern const Il2CppType Transition_t605142169_0_0_0;
static const Il2CppType* GenInst_Transition_t605142169_0_0_0_Types[] = { &Transition_t605142169_0_0_0 };
extern const Il2CppGenericInst GenInst_Transition_t605142169_0_0_0 = { 1, GenInst_Transition_t605142169_0_0_0_Types };
extern const Il2CppType AnimationTriggers_t3244928895_0_0_0;
static const Il2CppType* GenInst_AnimationTriggers_t3244928895_0_0_0_Types[] = { &AnimationTriggers_t3244928895_0_0_0 };
extern const Il2CppGenericInst GenInst_AnimationTriggers_t3244928895_0_0_0 = { 1, GenInst_AnimationTriggers_t3244928895_0_0_0_Types };
extern const Il2CppType Animator_t69676727_0_0_0;
static const Il2CppType* GenInst_Animator_t69676727_0_0_0_Types[] = { &Animator_t69676727_0_0_0 };
extern const Il2CppGenericInst GenInst_Animator_t69676727_0_0_0 = { 1, GenInst_Animator_t69676727_0_0_0_Types };
extern const Il2CppType Direction_t1525323322_0_0_0;
static const Il2CppType* GenInst_Direction_t1525323322_0_0_0_Types[] = { &Direction_t1525323322_0_0_0 };
extern const Il2CppGenericInst GenInst_Direction_t1525323322_0_0_0 = { 1, GenInst_Direction_t1525323322_0_0_0_Types };
extern const Il2CppType Rigidbody_t4233889191_0_0_0;
static const Il2CppType* GenInst_Rigidbody_t4233889191_0_0_0_Types[] = { &Rigidbody_t4233889191_0_0_0 };
extern const Il2CppGenericInst GenInst_Rigidbody_t4233889191_0_0_0 = { 1, GenInst_Rigidbody_t4233889191_0_0_0_Types };
static const Il2CppType* GenInst_KeyInfo_t777769675_0_0_0_KeyInfo_t777769675_0_0_0_Types[] = { &KeyInfo_t777769675_0_0_0, &KeyInfo_t777769675_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyInfo_t777769675_0_0_0_KeyInfo_t777769675_0_0_0 = { 2, GenInst_KeyInfo_t777769675_0_0_0_KeyInfo_t777769675_0_0_0_Types };
static const Il2CppType* GenInst_Byte_t3683104436_0_0_0_Byte_t3683104436_0_0_0_Types[] = { &Byte_t3683104436_0_0_0, &Byte_t3683104436_0_0_0 };
extern const Il2CppGenericInst GenInst_Byte_t3683104436_0_0_0_Byte_t3683104436_0_0_0 = { 2, GenInst_Byte_t3683104436_0_0_0_Byte_t3683104436_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3749587448_0_0_0_KeyValuePair_2_t3749587448_0_0_0_Types[] = { &KeyValuePair_2_t3749587448_0_0_0, &KeyValuePair_2_t3749587448_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3749587448_0_0_0_KeyValuePair_2_t3749587448_0_0_0 = { 2, GenInst_KeyValuePair_2_t3749587448_0_0_0_KeyValuePair_2_t3749587448_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t38854645_0_0_0_KeyValuePair_2_t38854645_0_0_0_Types[] = { &KeyValuePair_2_t38854645_0_0_0, &KeyValuePair_2_t38854645_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t38854645_0_0_0_KeyValuePair_2_t38854645_0_0_0 = { 2, GenInst_KeyValuePair_2_t38854645_0_0_0_KeyValuePair_2_t38854645_0_0_0_Types };
static const Il2CppType* GenInst_CustomAttributeNamedArgument_t94157543_0_0_0_CustomAttributeNamedArgument_t94157543_0_0_0_Types[] = { &CustomAttributeNamedArgument_t94157543_0_0_0, &CustomAttributeNamedArgument_t94157543_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeNamedArgument_t94157543_0_0_0_CustomAttributeNamedArgument_t94157543_0_0_0 = { 2, GenInst_CustomAttributeNamedArgument_t94157543_0_0_0_CustomAttributeNamedArgument_t94157543_0_0_0_Types };
static const Il2CppType* GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0_CustomAttributeTypedArgument_t1498197914_0_0_0_Types[] = { &CustomAttributeTypedArgument_t1498197914_0_0_0, &CustomAttributeTypedArgument_t1498197914_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0_CustomAttributeTypedArgument_t1498197914_0_0_0 = { 2, GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0_CustomAttributeTypedArgument_t1498197914_0_0_0_Types };
static const Il2CppType* GenInst_Color32_t874517518_0_0_0_Color32_t874517518_0_0_0_Types[] = { &Color32_t874517518_0_0_0, &Color32_t874517518_0_0_0 };
extern const Il2CppGenericInst GenInst_Color32_t874517518_0_0_0_Color32_t874517518_0_0_0 = { 2, GenInst_Color32_t874517518_0_0_0_Color32_t874517518_0_0_0_Types };
static const Il2CppType* GenInst_RaycastResult_t21186376_0_0_0_RaycastResult_t21186376_0_0_0_Types[] = { &RaycastResult_t21186376_0_0_0, &RaycastResult_t21186376_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastResult_t21186376_0_0_0_RaycastResult_t21186376_0_0_0 = { 2, GenInst_RaycastResult_t21186376_0_0_0_RaycastResult_t21186376_0_0_0_Types };
static const Il2CppType* GenInst_Playable_t3667545548_0_0_0_Playable_t3667545548_0_0_0_Types[] = { &Playable_t3667545548_0_0_0, &Playable_t3667545548_0_0_0 };
extern const Il2CppGenericInst GenInst_Playable_t3667545548_0_0_0_Playable_t3667545548_0_0_0 = { 2, GenInst_Playable_t3667545548_0_0_0_Playable_t3667545548_0_0_0_Types };
static const Il2CppType* GenInst_UICharInfo_t3056636800_0_0_0_UICharInfo_t3056636800_0_0_0_Types[] = { &UICharInfo_t3056636800_0_0_0, &UICharInfo_t3056636800_0_0_0 };
extern const Il2CppGenericInst GenInst_UICharInfo_t3056636800_0_0_0_UICharInfo_t3056636800_0_0_0 = { 2, GenInst_UICharInfo_t3056636800_0_0_0_UICharInfo_t3056636800_0_0_0_Types };
static const Il2CppType* GenInst_UILineInfo_t3621277874_0_0_0_UILineInfo_t3621277874_0_0_0_Types[] = { &UILineInfo_t3621277874_0_0_0, &UILineInfo_t3621277874_0_0_0 };
extern const Il2CppGenericInst GenInst_UILineInfo_t3621277874_0_0_0_UILineInfo_t3621277874_0_0_0 = { 2, GenInst_UILineInfo_t3621277874_0_0_0_UILineInfo_t3621277874_0_0_0_Types };
static const Il2CppType* GenInst_UIVertex_t1204258818_0_0_0_UIVertex_t1204258818_0_0_0_Types[] = { &UIVertex_t1204258818_0_0_0, &UIVertex_t1204258818_0_0_0 };
extern const Il2CppGenericInst GenInst_UIVertex_t1204258818_0_0_0_UIVertex_t1204258818_0_0_0 = { 2, GenInst_UIVertex_t1204258818_0_0_0_UIVertex_t1204258818_0_0_0_Types };
static const Il2CppType* GenInst_Vector2_t2243707579_0_0_0_Vector2_t2243707579_0_0_0_Types[] = { &Vector2_t2243707579_0_0_0, &Vector2_t2243707579_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector2_t2243707579_0_0_0_Vector2_t2243707579_0_0_0 = { 2, GenInst_Vector2_t2243707579_0_0_0_Vector2_t2243707579_0_0_0_Types };
static const Il2CppType* GenInst_Vector3_t2243707580_0_0_0_Vector3_t2243707580_0_0_0_Types[] = { &Vector3_t2243707580_0_0_0, &Vector3_t2243707580_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector3_t2243707580_0_0_0_Vector3_t2243707580_0_0_0 = { 2, GenInst_Vector3_t2243707580_0_0_0_Vector3_t2243707580_0_0_0_Types };
static const Il2CppType* GenInst_Vector4_t2243707581_0_0_0_Vector4_t2243707581_0_0_0_Types[] = { &Vector4_t2243707581_0_0_0, &Vector4_t2243707581_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector4_t2243707581_0_0_0_Vector4_t2243707581_0_0_0 = { 2, GenInst_Vector4_t2243707581_0_0_0_Vector4_t2243707581_0_0_0_Types };
static const Il2CppType* GenInst_Metric_t3273440202_0_0_0_Metric_t3273440202_0_0_0_Types[] = { &Metric_t3273440202_0_0_0, &Metric_t3273440202_0_0_0 };
extern const Il2CppGenericInst GenInst_Metric_t3273440202_0_0_0_Metric_t3273440202_0_0_0 = { 2, GenInst_Metric_t3273440202_0_0_0_Metric_t3273440202_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1416501748_0_0_0_KeyValuePair_2_t1416501748_0_0_0_Types[] = { &KeyValuePair_2_t1416501748_0_0_0, &KeyValuePair_2_t1416501748_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1416501748_0_0_0_KeyValuePair_2_t1416501748_0_0_0 = { 2, GenInst_KeyValuePair_2_t1416501748_0_0_0_KeyValuePair_2_t1416501748_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1416501748_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t1416501748_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1416501748_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t1416501748_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Int64_t909078037_0_0_0_Int64_t909078037_0_0_0_Types[] = { &Int64_t909078037_0_0_0, &Int64_t909078037_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t909078037_0_0_0_Int64_t909078037_0_0_0 = { 2, GenInst_Int64_t909078037_0_0_0_Int64_t909078037_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3196873006_0_0_0_KeyValuePair_2_t3196873006_0_0_0_Types[] = { &KeyValuePair_2_t3196873006_0_0_0, &KeyValuePair_2_t3196873006_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3196873006_0_0_0_KeyValuePair_2_t3196873006_0_0_0 = { 2, GenInst_KeyValuePair_2_t3196873006_0_0_0_KeyValuePair_2_t3196873006_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3196873006_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t3196873006_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3196873006_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t3196873006_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3132015601_0_0_0_KeyValuePair_2_t3132015601_0_0_0_Types[] = { &KeyValuePair_2_t3132015601_0_0_0, &KeyValuePair_2_t3132015601_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3132015601_0_0_0_KeyValuePair_2_t3132015601_0_0_0 = { 2, GenInst_KeyValuePair_2_t3132015601_0_0_0_KeyValuePair_2_t3132015601_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3132015601_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t3132015601_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3132015601_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t3132015601_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3749587448_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t3749587448_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3749587448_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t3749587448_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1436312919_0_0_0_KeyValuePair_2_t1436312919_0_0_0_Types[] = { &KeyValuePair_2_t1436312919_0_0_0, &KeyValuePair_2_t1436312919_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1436312919_0_0_0_KeyValuePair_2_t1436312919_0_0_0 = { 2, GenInst_KeyValuePair_2_t1436312919_0_0_0_KeyValuePair_2_t1436312919_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1436312919_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t1436312919_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1436312919_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t1436312919_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t888819835_0_0_0_KeyValuePair_2_t888819835_0_0_0_Types[] = { &KeyValuePair_2_t888819835_0_0_0, &KeyValuePair_2_t888819835_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t888819835_0_0_0_KeyValuePair_2_t888819835_0_0_0 = { 2, GenInst_KeyValuePair_2_t888819835_0_0_0_KeyValuePair_2_t888819835_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t888819835_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t888819835_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t888819835_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t888819835_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_IntPtr_t_0_0_0_IntPtr_t_0_0_0_Types[] = { &IntPtr_t_0_0_0, &IntPtr_t_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_IntPtr_t_0_0_0 = { 2, GenInst_IntPtr_t_0_0_0_IntPtr_t_0_0_0_Types };
static const Il2CppType* GenInst_InstantiationModel_t1632807378_0_0_0_InstantiationModel_t1632807378_0_0_0_Types[] = { &InstantiationModel_t1632807378_0_0_0, &InstantiationModel_t1632807378_0_0_0 };
extern const Il2CppGenericInst GenInst_InstantiationModel_t1632807378_0_0_0_InstantiationModel_t1632807378_0_0_0 = { 2, GenInst_InstantiationModel_t1632807378_0_0_0_InstantiationModel_t1632807378_0_0_0_Types };
static const Il2CppType* GenInst_InstantiationModel_t1632807378_0_0_0_Il2CppObject_0_0_0_Types[] = { &InstantiationModel_t1632807378_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_InstantiationModel_t1632807378_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_InstantiationModel_t1632807378_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3277180024_0_0_0_KeyValuePair_2_t3277180024_0_0_0_Types[] = { &KeyValuePair_2_t3277180024_0_0_0, &KeyValuePair_2_t3277180024_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3277180024_0_0_0_KeyValuePair_2_t3277180024_0_0_0 = { 2, GenInst_KeyValuePair_2_t3277180024_0_0_0_KeyValuePair_2_t3277180024_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3277180024_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t3277180024_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3277180024_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t3277180024_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Boolean_t3825574718_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1174980068_0_0_0_KeyValuePair_2_t1174980068_0_0_0_Types[] = { &KeyValuePair_2_t1174980068_0_0_0, &KeyValuePair_2_t1174980068_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1174980068_0_0_0_KeyValuePair_2_t1174980068_0_0_0 = { 2, GenInst_KeyValuePair_2_t1174980068_0_0_0_KeyValuePair_2_t1174980068_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1174980068_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t1174980068_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1174980068_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t1174980068_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3716250094_0_0_0_KeyValuePair_2_t3716250094_0_0_0_Types[] = { &KeyValuePair_2_t3716250094_0_0_0, &KeyValuePair_2_t3716250094_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3716250094_0_0_0_KeyValuePair_2_t3716250094_0_0_0 = { 2, GenInst_KeyValuePair_2_t3716250094_0_0_0_KeyValuePair_2_t3716250094_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3716250094_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t3716250094_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3716250094_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t3716250094_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2779450660_0_0_0_KeyValuePair_2_t2779450660_0_0_0_Types[] = { &KeyValuePair_2_t2779450660_0_0_0, &KeyValuePair_2_t2779450660_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2779450660_0_0_0_KeyValuePair_2_t2779450660_0_0_0 = { 2, GenInst_KeyValuePair_2_t2779450660_0_0_0_KeyValuePair_2_t2779450660_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2779450660_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t2779450660_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2779450660_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t2779450660_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_ArrayMetadata_t1135078014_0_0_0_Il2CppObject_0_0_0_Types[] = { &ArrayMetadata_t1135078014_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_ArrayMetadata_t1135078014_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_ArrayMetadata_t1135078014_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_ArrayMetadata_t1135078014_0_0_0_ArrayMetadata_t1135078014_0_0_0_Types[] = { &ArrayMetadata_t1135078014_0_0_0, &ArrayMetadata_t1135078014_0_0_0 };
extern const Il2CppGenericInst GenInst_ArrayMetadata_t1135078014_0_0_0_ArrayMetadata_t1135078014_0_0_0 = { 2, GenInst_ArrayMetadata_t1135078014_0_0_0_ArrayMetadata_t1135078014_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1407543090_0_0_0_KeyValuePair_2_t1407543090_0_0_0_Types[] = { &KeyValuePair_2_t1407543090_0_0_0, &KeyValuePair_2_t1407543090_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1407543090_0_0_0_KeyValuePair_2_t1407543090_0_0_0 = { 2, GenInst_KeyValuePair_2_t1407543090_0_0_0_KeyValuePair_2_t1407543090_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1407543090_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t1407543090_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1407543090_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t1407543090_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_ObjectMetadata_t4058137740_0_0_0_Il2CppObject_0_0_0_Types[] = { &ObjectMetadata_t4058137740_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectMetadata_t4058137740_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_ObjectMetadata_t4058137740_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_ObjectMetadata_t4058137740_0_0_0_ObjectMetadata_t4058137740_0_0_0_Types[] = { &ObjectMetadata_t4058137740_0_0_0, &ObjectMetadata_t4058137740_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectMetadata_t4058137740_0_0_0_ObjectMetadata_t4058137740_0_0_0 = { 2, GenInst_ObjectMetadata_t4058137740_0_0_0_ObjectMetadata_t4058137740_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t488203048_0_0_0_KeyValuePair_2_t488203048_0_0_0_Types[] = { &KeyValuePair_2_t488203048_0_0_0, &KeyValuePair_2_t488203048_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t488203048_0_0_0_KeyValuePair_2_t488203048_0_0_0 = { 2, GenInst_KeyValuePair_2_t488203048_0_0_0_KeyValuePair_2_t488203048_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t488203048_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t488203048_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t488203048_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t488203048_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_TextEditOp_t3138797698_0_0_0_Il2CppObject_0_0_0_Types[] = { &TextEditOp_t3138797698_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_TextEditOp_t3138797698_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_TextEditOp_t3138797698_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_TextEditOp_t3138797698_0_0_0_TextEditOp_t3138797698_0_0_0_Types[] = { &TextEditOp_t3138797698_0_0_0, &TextEditOp_t3138797698_0_0_0 };
extern const Il2CppGenericInst GenInst_TextEditOp_t3138797698_0_0_0_TextEditOp_t3138797698_0_0_0 = { 2, GenInst_TextEditOp_t3138797698_0_0_0_TextEditOp_t3138797698_0_0_0_Types };
extern const Il2CppType LoggingSection_t905443946_0_0_0;
static const Il2CppType* GenInst_LoggingSection_t905443946_0_0_0_Types[] = { &LoggingSection_t905443946_0_0_0 };
extern const Il2CppGenericInst GenInst_LoggingSection_t905443946_0_0_0 = { 1, GenInst_LoggingSection_t905443946_0_0_0_Types };
extern const Il2CppGenericInst* const g_Il2CppGenericInstTable[1072] = 
{
	&GenInst_Il2CppObject_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0,
	&GenInst_Char_t3454481338_0_0_0,
	&GenInst_IConvertible_t908092482_0_0_0,
	&GenInst_IComparable_t1857082765_0_0_0,
	&GenInst_IComparable_1_t991353265_0_0_0,
	&GenInst_IEquatable_1_t1363496211_0_0_0,
	&GenInst_ValueType_t3507792607_0_0_0,
	&GenInst_Int64_t909078037_0_0_0,
	&GenInst_UInt32_t2149682021_0_0_0,
	&GenInst_UInt64_t2909196914_0_0_0,
	&GenInst_Byte_t3683104436_0_0_0,
	&GenInst_SByte_t454417549_0_0_0,
	&GenInst_Int16_t4041245914_0_0_0,
	&GenInst_UInt16_t986882611_0_0_0,
	&GenInst_String_t_0_0_0,
	&GenInst_IEnumerable_t2911409499_0_0_0,
	&GenInst_ICloneable_t3853279282_0_0_0,
	&GenInst_IComparable_1_t3861059456_0_0_0,
	&GenInst_IEquatable_1_t4233202402_0_0_0,
	&GenInst_Type_t_0_0_0,
	&GenInst_IReflect_t3412036974_0_0_0,
	&GenInst__Type_t102776839_0_0_0,
	&GenInst_MemberInfo_t_0_0_0,
	&GenInst_ICustomAttributeProvider_t502202687_0_0_0,
	&GenInst__MemberInfo_t332722161_0_0_0,
	&GenInst_IFormattable_t1523031934_0_0_0,
	&GenInst_IComparable_1_t3903716671_0_0_0,
	&GenInst_IEquatable_1_t4275859617_0_0_0,
	&GenInst_Double_t4078015681_0_0_0,
	&GenInst_IComparable_1_t1614887608_0_0_0,
	&GenInst_IEquatable_1_t1987030554_0_0_0,
	&GenInst_IComparable_1_t3981521244_0_0_0,
	&GenInst_IEquatable_1_t58696894_0_0_0,
	&GenInst_IComparable_1_t1219976363_0_0_0,
	&GenInst_IEquatable_1_t1592119309_0_0_0,
	&GenInst_Single_t2076509932_0_0_0,
	&GenInst_IComparable_1_t3908349155_0_0_0,
	&GenInst_IEquatable_1_t4280492101_0_0_0,
	&GenInst_Decimal_t724701077_0_0_0,
	&GenInst_Boolean_t3825574718_0_0_0,
	&GenInst_Delegate_t3022476291_0_0_0,
	&GenInst_ISerializable_t1245643778_0_0_0,
	&GenInst_ParameterInfo_t2249040075_0_0_0,
	&GenInst__ParameterInfo_t470209990_0_0_0,
	&GenInst_ParameterModifier_t1820634920_0_0_0,
	&GenInst_IComparable_1_t2818721834_0_0_0,
	&GenInst_IEquatable_1_t3190864780_0_0_0,
	&GenInst_IComparable_1_t446068841_0_0_0,
	&GenInst_IEquatable_1_t818211787_0_0_0,
	&GenInst_IComparable_1_t1578117841_0_0_0,
	&GenInst_IEquatable_1_t1950260787_0_0_0,
	&GenInst_IComparable_1_t2286256772_0_0_0,
	&GenInst_IEquatable_1_t2658399718_0_0_0,
	&GenInst_IComparable_1_t2740917260_0_0_0,
	&GenInst_IEquatable_1_t3113060206_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_FieldInfo_t_0_0_0,
	&GenInst__FieldInfo_t2511231167_0_0_0,
	&GenInst_MethodInfo_t_0_0_0,
	&GenInst__MethodInfo_t3642518830_0_0_0,
	&GenInst_MethodBase_t904190842_0_0_0,
	&GenInst__MethodBase_t1935530873_0_0_0,
	&GenInst_PropertyInfo_t_0_0_0,
	&GenInst__PropertyInfo_t1567586598_0_0_0,
	&GenInst_ConstructorInfo_t2851816542_0_0_0,
	&GenInst__ConstructorInfo_t3269099341_0_0_0,
	&GenInst_IntPtr_t_0_0_0,
	&GenInst_TableRange_t2011406615_0_0_0,
	&GenInst_TailoringInfo_t1449609243_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_KeyValuePair_2_t3716250094_0_0_0,
	&GenInst_Link_t2723257478_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3716250094_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t1744001932_0_0_0,
	&GenInst_Contraction_t1673853792_0_0_0,
	&GenInst_Level2Map_t3322505726_0_0_0,
	&GenInst_BigInteger_t925946152_0_0_0,
	&GenInst_KeySizes_t3144736271_0_0_0,
	&GenInst_KeyValuePair_2_t38854645_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t38854645_0_0_0,
	&GenInst_IComparable_1_t1362446645_0_0_0,
	&GenInst_IEquatable_1_t1734589591_0_0_0,
	&GenInst_Slot_t2022531261_0_0_0,
	&GenInst_Slot_t2267560602_0_0_0,
	&GenInst_StackFrame_t2050294881_0_0_0,
	&GenInst_Calendar_t585061108_0_0_0,
	&GenInst_ModuleBuilder_t4156028127_0_0_0,
	&GenInst__ModuleBuilder_t1075102050_0_0_0,
	&GenInst_Module_t4282841206_0_0_0,
	&GenInst__Module_t2144668161_0_0_0,
	&GenInst_MonoResource_t3127387157_0_0_0,
	&GenInst_ParameterBuilder_t3344728474_0_0_0,
	&GenInst__ParameterBuilder_t2251638747_0_0_0,
	&GenInst_TypeU5BU5D_t1664964607_0_0_0,
	&GenInst_Il2CppArray_0_0_0,
	&GenInst_ICollection_t91669223_0_0_0,
	&GenInst_IList_t3321498491_0_0_0,
	&GenInst_LocalBuilder_t2116499186_0_0_0,
	&GenInst__LocalBuilder_t61912499_0_0_0,
	&GenInst_LocalVariableInfo_t1749284021_0_0_0,
	&GenInst_ILTokenInfo_t149559338_0_0_0,
	&GenInst_LabelData_t3712112744_0_0_0,
	&GenInst_LabelFixup_t4090909514_0_0_0,
	&GenInst_GenericTypeParameterBuilder_t1370236603_0_0_0,
	&GenInst_TypeBuilder_t3308873219_0_0_0,
	&GenInst__TypeBuilder_t2783404358_0_0_0,
	&GenInst_MethodBuilder_t644187984_0_0_0,
	&GenInst__MethodBuilder_t3932949077_0_0_0,
	&GenInst_ConstructorBuilder_t700974433_0_0_0,
	&GenInst__ConstructorBuilder_t1236878896_0_0_0,
	&GenInst_PropertyBuilder_t3694255912_0_0_0,
	&GenInst__PropertyBuilder_t3341912621_0_0_0,
	&GenInst_FieldBuilder_t2784804005_0_0_0,
	&GenInst__FieldBuilder_t1895266044_0_0_0,
	&GenInst_AssemblyName_t894705941_0_0_0,
	&GenInst__AssemblyName_t582342236_0_0_0,
	&GenInst_IDeserializationCallback_t327125377_0_0_0,
	&GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0,
	&GenInst_CustomAttributeNamedArgument_t94157543_0_0_0,
	&GenInst_CustomAttributeData_t3093286891_0_0_0,
	&GenInst_Exception_t1927440687_0_0_0,
	&GenInst__Exception_t3026971024_0_0_0,
	&GenInst_ResourceInfo_t3933049236_0_0_0,
	&GenInst_ResourceCacheItem_t333236149_0_0_0,
	&GenInst_IContextProperty_t287246399_0_0_0,
	&GenInst_Header_t2756440555_0_0_0,
	&GenInst_ITrackingHandler_t2759960940_0_0_0,
	&GenInst_IContextAttribute_t2439121372_0_0_0,
	&GenInst_DateTime_t693205669_0_0_0,
	&GenInst_IComparable_1_t2525044892_0_0_0,
	&GenInst_IEquatable_1_t2897187838_0_0_0,
	&GenInst_IComparable_1_t2556540300_0_0_0,
	&GenInst_IEquatable_1_t2928683246_0_0_0,
	&GenInst_TimeSpan_t3430258949_0_0_0,
	&GenInst_IComparable_1_t967130876_0_0_0,
	&GenInst_IEquatable_1_t1339273822_0_0_0,
	&GenInst_TypeTag_t141209596_0_0_0,
	&GenInst_Enum_t2459695545_0_0_0,
	&GenInst_MonoType_t_0_0_0,
	&GenInst_StrongName_t2988747270_0_0_0,
	&GenInst_WaitHandle_t677569169_0_0_0,
	&GenInst_IDisposable_t2427283555_0_0_0,
	&GenInst_MarshalByRefObject_t1285298191_0_0_0,
	&GenInst_Assembly_t4268412390_0_0_0,
	&GenInst__Assembly_t2937922309_0_0_0,
	&GenInst_DateTimeOffset_t1362988906_0_0_0,
	&GenInst_Guid_t2533601593_0_0_0,
	&GenInst_Version_t1755874712_0_0_0,
	&GenInst_BigInteger_t925946153_0_0_0,
	&GenInst_ByteU5BU5D_t3397334013_0_0_0,
	&GenInst_X509Certificate_t283079845_0_0_0,
	&GenInst_ClientCertificateType_t4001384466_0_0_0,
	&GenInst_Node_t2499136326_0_0_0,
	&GenInst_PropertyDescriptor_t4250402154_0_0_0,
	&GenInst_MemberDescriptor_t3749827553_0_0_0,
	&GenInst_Type_t_0_0_0_LinkedList_1_t2743332604_0_0_0,
	&GenInst_TypeDescriptionProvider_t2438624375_0_0_0,
	&GenInst_Type_t_0_0_0_LinkedList_1_t2743332604_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t2438035723_0_0_0,
	&GenInst_WeakObjectWrapper_t2012978780_0_0_0_LinkedList_1_t2743332604_0_0_0,
	&GenInst_WeakObjectWrapper_t2012978780_0_0_0,
	&GenInst_WeakObjectWrapper_t2012978780_0_0_0_LinkedList_1_t2743332604_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t3261256129_0_0_0,
	&GenInst_X509ChainStatus_t4278378721_0_0_0,
	&GenInst_IPAddress_t1399971723_0_0_0,
	&GenInst_ArraySegment_1_t2594217482_0_0_0,
	&GenInst_Cookie_t3154017544_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_KeyValuePair_2_t1174980068_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_KeyValuePair_2_t1174980068_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t3497699202_0_0_0,
	&GenInst_Capture_t4157900610_0_0_0,
	&GenInst_Group_t3761430853_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_KeyValuePair_2_t3132015601_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3132015601_0_0_0,
	&GenInst_Mark_t2724874473_0_0_0,
	&GenInst_UriScheme_t1876590943_0_0_0,
	&GenInst_Link_t865133271_0_0_0,
	&GenInst_LockDetails_t2550015005_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_XmlSchemaAttribute_t4015859774_0_0_0,
	&GenInst_XmlSchemaAnnotated_t2082486936_0_0_0,
	&GenInst_XmlSchemaObject_t2050913741_0_0_0,
	&GenInst_XsdIdentityPath_t2037874_0_0_0,
	&GenInst_XsdIdentityField_t2563516441_0_0_0,
	&GenInst_XsdIdentityStep_t452377251_0_0_0,
	&GenInst_XmlAttribute_t175731005_0_0_0,
	&GenInst_IHasXmlChildNode_t2048545686_0_0_0,
	&GenInst_XmlNode_t616554813_0_0_0,
	&GenInst_IXPathNavigable_t845515791_0_0_0,
	&GenInst_XmlQualifiedName_t1944712516_0_0_0,
	&GenInst_Regex_t1803876613_0_0_0,
	&GenInst_XmlSchemaSimpleType_t248156492_0_0_0,
	&GenInst_XmlSchemaType_t1795078578_0_0_0,
	&GenInst_XmlException_t4188277960_0_0_0,
	&GenInst_SystemException_t3877406272_0_0_0,
	&GenInst_KeyValuePair_2_t1430411454_0_0_0,
	&GenInst_String_t_0_0_0_DTDNode_t1758286970_0_0_0,
	&GenInst_DTDNode_t1758286970_0_0_0,
	&GenInst_AttributeSlot_t1499247213_0_0_0,
	&GenInst_Entry_t2583369454_0_0_0,
	&GenInst_NsDecl_t3210081295_0_0_0,
	&GenInst_NsScope_t2513625351_0_0_0,
	&GenInst_XmlAttributeTokenInfo_t3353594030_0_0_0,
	&GenInst_XmlTokenInfo_t254587324_0_0_0,
	&GenInst_TagName_t2340974457_0_0_0,
	&GenInst_XmlNodeInfo_t3709371029_0_0_0,
	&GenInst_XAttribute_t3858477518_0_0_0,
	&GenInst_XObject_t3550811009_0_0_0,
	&GenInst_IXmlLineInfo_t135184468_0_0_0,
	&GenInst_XNode_t2707504214_0_0_0,
	&GenInst_XElement_t553821050_0_0_0,
	&GenInst_XName_t785190363_0_0_0,
	&GenInst_String_t_0_0_0_XNamespace_t1613015075_0_0_0,
	&GenInst_String_t_0_0_0_XNamespace_t1613015075_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t1285139559_0_0_0,
	&GenInst_String_t_0_0_0_XName_t785190363_0_0_0,
	&GenInst_String_t_0_0_0_XName_t785190363_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t457314847_0_0_0,
	&GenInst_Object_t1021602117_0_0_0,
	&GenInst_Camera_t189460977_0_0_0,
	&GenInst_Behaviour_t955675639_0_0_0,
	&GenInst_Component_t3819376471_0_0_0,
	&GenInst_Display_t3666191348_0_0_0,
	&GenInst_Boolean_t3825574718_0_0_0_String_t_0_0_0,
	&GenInst_Boolean_t3825574718_0_0_0_Il2CppObject_0_0_0,
	&GenInst_AchievementDescription_t3110978151_0_0_0,
	&GenInst_IAchievementDescription_t3498529102_0_0_0,
	&GenInst_UserProfile_t3365630962_0_0_0,
	&GenInst_IUserProfile_t4108565527_0_0_0,
	&GenInst_GcLeaderboard_t453887929_0_0_0,
	&GenInst_IAchievementDescriptionU5BU5D_t4083280315_0_0_0,
	&GenInst_IAchievementU5BU5D_t2709554645_0_0_0,
	&GenInst_IAchievement_t1752291260_0_0_0,
	&GenInst_GcAchievementData_t1754866149_0_0_0,
	&GenInst_Achievement_t1333316625_0_0_0,
	&GenInst_IScoreU5BU5D_t3237304636_0_0_0,
	&GenInst_IScore_t513966369_0_0_0,
	&GenInst_GcScoreData_t3676783238_0_0_0,
	&GenInst_Score_t2307748940_0_0_0,
	&GenInst_IUserProfileU5BU5D_t3461248430_0_0_0,
	&GenInst_Vector2_t2243707579_0_0_0,
	&GenInst_Keyframe_t1449471340_0_0_0,
	&GenInst_Vector3_t2243707580_0_0_0,
	&GenInst_Vector4_t2243707581_0_0_0,
	&GenInst_Color32_t874517518_0_0_0,
	&GenInst_Color_t2020392075_0_0_0,
	&GenInst_String_t_0_0_0_String_t_0_0_0,
	&GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t1701344717_0_0_0,
	&GenInst_Playable_t3667545548_0_0_0,
	&GenInst_Scene_t1684909666_0_0_0_LoadSceneMode_t2981886439_0_0_0,
	&GenInst_Scene_t1684909666_0_0_0,
	&GenInst_Scene_t1684909666_0_0_0_Scene_t1684909666_0_0_0,
	&GenInst_ContactPoint_t1376425630_0_0_0,
	&GenInst_Rigidbody2D_t502193897_0_0_0,
	&GenInst_ContactPoint2D_t3659330976_0_0_0,
	&GenInst_UIVertex_t1204258818_0_0_0,
	&GenInst_UICharInfo_t3056636800_0_0_0,
	&GenInst_UILineInfo_t3621277874_0_0_0,
	&GenInst_Font_t4239498691_0_0_0,
	&GenInst_GUILayoutOption_t4183744904_0_0_0,
	&GenInst_GUILayoutEntry_t3828586629_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t3749587448_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t3749587448_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t4180919198_0_0_0,
	&GenInst_GUIStyle_t1799908754_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t1472033238_0_0_0,
	&GenInst_Event_t3028476042_0_0_0_TextEditOp_t3138797698_0_0_0,
	&GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0,
	&GenInst_KeyValuePair_2_t488203048_0_0_0,
	&GenInst_TextEditOp_t3138797698_0_0_0,
	&GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_TextEditOp_t3138797698_0_0_0,
	&GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_KeyValuePair_2_t488203048_0_0_0,
	&GenInst_Event_t3028476042_0_0_0,
	&GenInst_Event_t3028476042_0_0_0_TextEditOp_t3138797698_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t3799506081_0_0_0,
	&GenInst_String_t_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t2361573779_0_0_0,
	&GenInst_DisallowMultipleComponent_t2656950_0_0_0,
	&GenInst_Attribute_t542643598_0_0_0,
	&GenInst__Attribute_t1557664299_0_0_0,
	&GenInst_ExecuteInEditMode_t3043633143_0_0_0,
	&GenInst_RequireComponent_t864575032_0_0_0,
	&GenInst_HitInfo_t1761367055_0_0_0,
	&GenInst_PersistentCall_t3793436469_0_0_0,
	&GenInst_BaseInvokableCall_t2229564840_0_0_0,
	&GenInst_Type_t_0_0_0_Func_2_t3675469371_0_0_0,
	&GenInst_IntPtr_t_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Type_t_0_0_0_Func_2_t3675469371_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t3370172490_0_0_0,
	&GenInst_KeyValuePair_2_t888819835_0_0_0,
	&GenInst_IntPtr_t_0_0_0_Il2CppObject_0_0_0_IntPtr_t_0_0_0,
	&GenInst_IntPtr_t_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_IntPtr_t_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_IntPtr_t_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t888819835_0_0_0,
	&GenInst_String_t_0_0_0_Delegate_t3022476291_0_0_0,
	&GenInst_String_t_0_0_0_Delegate_t3022476291_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t2694600775_0_0_0,
	&GenInst_IntPtr_t_0_0_0_Dictionary_2_t1629922792_0_0_0,
	&GenInst_IntPtr_t_0_0_0_Methods_t1187897474_0_0_0,
	&GenInst_IntPtr_t_0_0_0_Methods_t1187897474_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t3682235310_0_0_0,
	&GenInst_IntPtr_t_0_0_0_Dictionary_2_t1629922792_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t4124260628_0_0_0,
	&GenInst_JsonData_t4263252052_0_0_0,
	&GenInst_KeyValuePair_2_t3935376536_0_0_0,
	&GenInst_String_t_0_0_0_JsonData_t4263252052_0_0_0,
	&GenInst_String_t_0_0_0_JsonData_t4263252052_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_String_t_0_0_0_PropertyMetadata_t3287739986_0_0_0,
	&GenInst_Il2CppObject_0_0_0_PropertyMetadata_t3287739986_0_0_0,
	&GenInst_KeyValuePair_2_t637145336_0_0_0,
	&GenInst_Type_t_0_0_0_ExporterFunc_t173265409_0_0_0,
	&GenInst_Type_t_0_0_0_IDictionary_2_t787128596_0_0_0,
	&GenInst_Type_t_0_0_0_ImporterFunc_t850687278_0_0_0,
	&GenInst_Type_t_0_0_0_ArrayMetadata_t1135078014_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ArrayMetadata_t1135078014_0_0_0,
	&GenInst_KeyValuePair_2_t2779450660_0_0_0,
	&GenInst_Type_t_0_0_0_IDictionary_2_t3266987655_0_0_0,
	&GenInst_Type_t_0_0_0_MethodInfo_t_0_0_0,
	&GenInst_Type_t_0_0_0_ObjectMetadata_t4058137740_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ObjectMetadata_t4058137740_0_0_0,
	&GenInst_KeyValuePair_2_t1407543090_0_0_0,
	&GenInst_Type_t_0_0_0_IList_1_t3828680587_0_0_0,
	&GenInst_PropertyMetadata_t3287739986_0_0_0,
	&GenInst_ArrayMetadata_t1135078014_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ArrayMetadata_t1135078014_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ArrayMetadata_t1135078014_0_0_0_ArrayMetadata_t1135078014_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ArrayMetadata_t1135078014_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ArrayMetadata_t1135078014_0_0_0_KeyValuePair_2_t2779450660_0_0_0,
	&GenInst_Type_t_0_0_0_ArrayMetadata_t1135078014_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t829781133_0_0_0,
	&GenInst_Type_t_0_0_0_IDictionary_2_t3266987655_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t2961690774_0_0_0,
	&GenInst_ObjectMetadata_t4058137740_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ObjectMetadata_t4058137740_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ObjectMetadata_t4058137740_0_0_0_ObjectMetadata_t4058137740_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ObjectMetadata_t4058137740_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ObjectMetadata_t4058137740_0_0_0_KeyValuePair_2_t1407543090_0_0_0,
	&GenInst_Type_t_0_0_0_ObjectMetadata_t4058137740_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t3752840859_0_0_0,
	&GenInst_Type_t_0_0_0_IList_1_t3828680587_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t3523383706_0_0_0,
	&GenInst_Type_t_0_0_0_ExporterFunc_t173265409_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t4162935824_0_0_0,
	&GenInst_Type_t_0_0_0_IDictionary_2_t787128596_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t481831715_0_0_0,
	&GenInst_Type_t_0_0_0_ImporterFunc_t850687278_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t545390397_0_0_0,
	&GenInst_JsonToken_t2445581255_0_0_0,
	&GenInst_WriterContext_t1209007092_0_0_0,
	&GenInst_StateHandler_t3489987002_0_0_0,
	&GenInst_MulticastDelegate_t3201952435_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t2784070411_0_0_0,
	&GenInst_TraceListener_t3414949279_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t2784070411_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t2456194895_0_0_0,
	&GenInst_String_t_0_0_0_XElement_t553821050_0_0_0,
	&GenInst_XAttribute_t3858477518_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_XElement_t553821050_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_String_t_0_0_0_XElement_t553821050_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t225945534_0_0_0,
	&GenInst_String_t_0_0_0_RegionEndpoint_t661522805_0_0_0,
	&GenInst_String_t_0_0_0_RegionEndpoint_t661522805_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t333647289_0_0_0,
	&GenInst_String_t_0_0_0_RegionEndpoint_t1885241449_0_0_0,
	&GenInst_String_t_0_0_0_RegionEndpoint_t1885241449_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t1557365933_0_0_0,
	&GenInst_String_t_0_0_0_Endpoint_t3348692641_0_0_0,
	&GenInst_String_t_0_0_0_Endpoint_t3348692641_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t3020817125_0_0_0,
	&GenInst_String_t_0_0_0_IRegionEndpoint_t544433888_0_0_0,
	&GenInst_String_t_0_0_0_IRegionEndpoint_t544433888_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t216558372_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_String_t_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t3089358386_0_0_0,
	&GenInst_Action_t3226471752_0_0_0,
	&GenInst_Type_t_0_0_0_InstantiationModel_t1632807378_0_0_0,
	&GenInst_Il2CppObject_0_0_0_InstantiationModel_t1632807378_0_0_0,
	&GenInst_KeyValuePair_2_t3277180024_0_0_0,
	&GenInst_Type_t_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Type_t_0_0_0_Type_t_0_0_0,
	&GenInst_ITypeInfo_t3592676621_0_0_0,
	&GenInst_MethodInfo_t_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_NetworkStatusEventArgs_t1607167653_0_0_0,
	&GenInst_KeyValuePair_2_t1327510497_0_0_0,
	&GenInst_InstantiationModel_t1632807378_0_0_0,
	&GenInst_Il2CppObject_0_0_0_InstantiationModel_t1632807378_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_InstantiationModel_t1632807378_0_0_0_InstantiationModel_t1632807378_0_0_0,
	&GenInst_Il2CppObject_0_0_0_InstantiationModel_t1632807378_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Il2CppObject_0_0_0_InstantiationModel_t1632807378_0_0_0_KeyValuePair_2_t3277180024_0_0_0,
	&GenInst_Type_t_0_0_0_InstantiationModel_t1632807378_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Type_t_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t2384152414_0_0_0,
	&GenInst_Type_t_0_0_0_Type_t_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t998506345_0_0_0,
	&GenInst_IRequest_t2400804350_0_0_0_AmazonWebServiceRequest_t3384026212_0_0_0,
	&GenInst_AmazonWebServiceRequest_t3384026212_0_0_0_AmazonWebServiceResponse_t529043356_0_0_0_Exception_t1927440687_0_0_0_AsyncOptions_t558351272_0_0_0,
	&GenInst_StreamTransferProgressArgs_t2639638063_0_0_0,
	&GenInst_IExecutionContext_t2477130752_0_0_0,
	&GenInst_IExecutionContext_t2477130752_0_0_0_Exception_t1927440687_0_0_0,
	&GenInst_IPipelineHandler_t2320756001_0_0_0,
	&GenInst_RegionGenerator_t3123742622_0_0_0,
	&GenInst_Type_t_0_0_0_Dictionary_2_t1620371852_0_0_0,
	&GenInst_String_t_0_0_0_ConstantClass_t4000559886_0_0_0,
	&GenInst_String_t_0_0_0_ConstantClass_t4000559886_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t3672684370_0_0_0,
	&GenInst_Type_t_0_0_0_Dictionary_2_t1620371852_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t1315074971_0_0_0,
	&GenInst_String_t_0_0_0_RetryCapacity_t2794668894_0_0_0,
	&GenInst_String_t_0_0_0_RetryCapacity_t2794668894_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t2466793378_0_0_0,
	&GenInst_Stream_t3255436806_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Type_t_0_0_0_IExceptionHandler_t1411014698_0_0_0,
	&GenInst_Type_t_0_0_0_IExceptionHandler_t1411014698_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t1105717817_0_0_0,
	&GenInst_HttpErrorResponseException_t3191555406_0_0_0,
	&GenInst_HttpStatusCode_t1898409641_0_0_0,
	&GenInst_WebExceptionStatus_t1169373531_0_0_0,
	&GenInst_Link_t74093617_0_0_0,
	&GenInst_Link_t3640024803_0_0_0,
	&GenInst_IAsyncExecutionContext_t3792344986_0_0_0,
	&GenInst_ThreadPoolOptions_1_t1583506835_0_0_0,
	&GenInst_Exception_t1927440687_0_0_0_Il2CppObject_0_0_0,
	&GenInst_ThreadPoolOptions_1_t3222238215_0_0_0,
	&GenInst_IAsyncExecutionContext_t3792344986_0_0_0_IAsyncExecutionContext_t3792344986_0_0_0,
	&GenInst_Exception_t1927440687_0_0_0_IAsyncExecutionContext_t3792344986_0_0_0,
	&GenInst_IUnityHttpRequest_t1859903397_0_0_0,
	&GenInst_RuntimeAsyncResult_t4279356013_0_0_0,
	&GenInst_Type_t_0_0_0_Logger_t2262497814_0_0_0,
	&GenInst_InternalLogger_t2972373343_0_0_0,
	&GenInst_Type_t_0_0_0_Logger_t2262497814_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t1957200933_0_0_0,
	&GenInst_Metric_t3273440202_0_0_0_List_1_t2058570427_0_0_0,
	&GenInst_Metric_t3273440202_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t3196873006_0_0_0,
	&GenInst_Metric_t3273440202_0_0_0,
	&GenInst_Metric_t3273440202_0_0_0_Il2CppObject_0_0_0_Metric_t3273440202_0_0_0,
	&GenInst_Metric_t3273440202_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Metric_t3273440202_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Metric_t3273440202_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t3196873006_0_0_0,
	&GenInst_Metric_t3273440202_0_0_0_List_1_t2058570427_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t2565994138_0_0_0,
	&GenInst_Metric_t3273440202_0_0_0_List_1_t3837499352_0_0_0,
	&GenInst_IMetricsTiming_t173410924_0_0_0,
	&GenInst_Metric_t3273440202_0_0_0_List_1_t3837499352_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t49955767_0_0_0,
	&GenInst_Metric_t3273440202_0_0_0_Int64_t909078037_0_0_0,
	&GenInst_KeyValuePair_2_t1416501748_0_0_0,
	&GenInst_Metric_t3273440202_0_0_0_Int64_t909078037_0_0_0_Metric_t3273440202_0_0_0,
	&GenInst_Metric_t3273440202_0_0_0_Int64_t909078037_0_0_0_Int64_t909078037_0_0_0,
	&GenInst_Metric_t3273440202_0_0_0_Int64_t909078037_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Metric_t3273440202_0_0_0_Int64_t909078037_0_0_0_KeyValuePair_2_t1416501748_0_0_0,
	&GenInst_Metric_t3273440202_0_0_0_Timing_t847194262_0_0_0,
	&GenInst_Metric_t3273440202_0_0_0_Timing_t847194262_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t1354617973_0_0_0,
	&GenInst_MetricError_t964444806_0_0_0,
	&GenInst_Metric_t3273440202_0_0_0_String_t_0_0_0,
	&GenInst_ErrorResponse_t3502566035_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0,
	&GenInst_ErrorResponse_t3502566035_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0,
	&GenInst_AmazonWebServiceResponse_t529043356_0_0_0_UnmarshallerContext_t1608322995_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0,
	&GenInst_Int64_t909078037_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0,
	&GenInst_Int64_t909078037_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Int64_t909078037_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0,
	&GenInst_Boolean_t3825574718_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0,
	&GenInst_Boolean_t3825574718_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0,
	&GenInst_String_t_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0,
	&GenInst_String_t_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0,
	&GenInst_DateTime_t693205669_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0,
	&GenInst_DateTime_t693205669_0_0_0_Il2CppObject_0_0_0,
	&GenInst_DateTime_t693205669_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0,
	&GenInst_ResponseMetadata_t527027456_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0,
	&GenInst_ResponseMetadata_t527027456_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0,
	&GenInst_List_1_t2058570427_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0,
	&GenInst_List_1_t2058570427_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0,
	&GenInst_Il2CppObject_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0,
	&GenInst_Il2CppObject_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0,
	&GenInst_XmlNodeType_t739504597_0_0_0,
	&GenInst_Link_t3210155869_0_0_0,
	&GenInst_KeyValuePair_2_t1701344717_0_0_0_String_t_0_0_0,
	&GenInst_KeyValuePair_2_t38854645_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t1701344717_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_KeyValuePair_2_t38854645_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_IEnlistmentNotification_t1718399847_0_0_0,
	&GenInst_ISinglePhaseNotification_t242502729_0_0_0,
	&GenInst_AssumeRoleWithWebIdentityRequest_t193094215_0_0_0_AssumeRoleWithWebIdentityResponse_t3931705881_0_0_0,
	&GenInst_AssumedRoleUser_t150458319_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0,
	&GenInst_AssumedRoleUser_t150458319_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0,
	&GenInst_IRequest_t2400804350_0_0_0_AssumeRoleWithWebIdentityRequest_t193094215_0_0_0,
	&GenInst_Credentials_t3554032640_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0,
	&GenInst_Credentials_t3554032640_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0,
	&GenInst_DataRow_t321465356_0_0_0,
	&GenInst_DataColumn_t2152532948_0_0_0,
	&GenInst_MarshalByValueComponent_t3997823175_0_0_0,
	&GenInst_IServiceProvider_t2397848447_0_0_0,
	&GenInst_IComponent_t1000253244_0_0_0,
	&GenInst_DataColumnMapping_t969655534_0_0_0,
	&GenInst_IColumnMapping_t1034865995_0_0_0,
	&GenInst_ColumnInfo_t894042583_0_0_0,
	&GenInst_ObjectU5BU5D_t3614634134_0_0_0,
	&GenInst_SchemaInfo_t3205033891_0_0_0,
	&GenInst_ListSortDirection_t4186912589_0_0_0,
	&GenInst_DataTable_t3267612424_0_0_0,
	&GenInst_IXmlSerializable_t2623090263_0_0_0,
	&GenInst_IListSource_t3806143940_0_0_0,
	&GenInst_ISupportInitialize_t1887203168_0_0_0,
	&GenInst_ISupportInitializeNotification_t2045243097_0_0_0,
	&GenInst_DataRelation_t790111712_0_0_0,
	&GenInst_DbSourceMethodInfo_t4064904528_0_0_0,
	&GenInst_IdentityChangedArgs_t1657401211_0_0_0,
	&GenInst_Credentials_t792472136_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0,
	&GenInst_Credentials_t792472136_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0,
	&GenInst_IRequest_t2400804350_0_0_0_GetCredentialsForIdentityRequest_t932830458_0_0_0,
	&GenInst_IRequest_t2400804350_0_0_0_GetIdRequest_t4078561340_0_0_0,
	&GenInst_IRequest_t2400804350_0_0_0_GetOpenIdTokenRequest_t2698079901_0_0_0,
	&GenInst_SqliteFunction_t2830561746_0_0_0,
	&GenInst_RowUpdatingEventArgs_t1243529829_0_0_0,
	&GenInst_SqliteStatement_t4106757957_0_0_0,
	&GenInst_String_t_0_0_0_Pool_t3179814164_0_0_0,
	&GenInst_KeyValuePair_2_t2851938648_0_0_0,
	&GenInst_WeakReference_t1077405567_0_0_0,
	&GenInst_DbType_t3924915636_0_0_0,
	&GenInst_SQLiteTypeNames_t2939892384_0_0_0,
	&GenInst_TypeAffinity_t326266980_0_0_0,
	&GenInst_SQLiteType_t3850755444_0_0_0,
	&GenInst_Int64_t909078037_0_0_0_AggregateData_t1049664947_0_0_0,
	&GenInst_KeyValuePair_2_t1436312919_0_0_0,
	&GenInst_Int64_t909078037_0_0_0_Il2CppObject_0_0_0_Int64_t909078037_0_0_0,
	&GenInst_Int64_t909078037_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Int64_t909078037_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Int64_t909078037_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t1436312919_0_0_0,
	&GenInst_Int64_t909078037_0_0_0_AggregateData_t1049664947_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t4091495867_0_0_0,
	&GenInst_SqliteFunctionAttribute_t2332230_0_0_0,
	&GenInst_KeyInfo_t777769675_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t1398341365_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t1398341365_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t1070465849_0_0_0,
	&GenInst_SqliteParameter_t354437343_0_0_0,
	&GenInst_Record_t441586865_0_0_0,
	&GenInst_RecordPatch_t97905615_0_0_0,
	&GenInst_Dataset_t149289424_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0,
	&GenInst_Dataset_t149289424_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0,
	&GenInst_IRequest_t2400804350_0_0_0_DeleteDatasetRequest_t3672322944_0_0_0,
	&GenInst_IRequest_t2400804350_0_0_0_ListRecordsRequest_t3074726983_0_0_0,
	&GenInst_String_t_0_0_0_StringUnmarshaller_t3953260147_0_0_0,
	&GenInst_Record_t441586865_0_0_0_RecordUnmarshaller_t3056833063_0_0_0,
	&GenInst_RecordPatch_t97905615_0_0_0_JsonMarshallerContext_t4063314926_0_0_0,
	&GenInst_Record_t441586865_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0,
	&GenInst_Record_t441586865_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0,
	&GenInst_IRequest_t2400804350_0_0_0_UpdateRecordsRequest_t197801344_0_0_0,
	&GenInst_DatasetMetadata_t1205730659_0_0_0,
	&GenInst_Record_t868799569_0_0_0,
	&GenInst_SyncSuccessEventArgs_t4277381347_0_0_0,
	&GenInst_SyncFailureEventArgs_t1748345498_0_0_0,
	&GenInst_SyncConflict_t3030310309_0_0_0,
	&GenInst_String_t_0_0_0_Record_t868799569_0_0_0,
	&GenInst_String_t_0_0_0_Record_t868799569_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t540924053_0_0_0,
	&GenInst_Statement_t3708524595_0_0_0,
	&GenInst_Type_t_0_0_0_CSRequest_t3915036330_0_0_0,
	&GenInst_Type_t_0_0_0_CSRequest_t3915036330_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t3609739449_0_0_0,
	&GenInst_BaseInputModule_t1295781545_0_0_0,
	&GenInst_RaycastResult_t21186376_0_0_0,
	&GenInst_IDeselectHandler_t3182198310_0_0_0,
	&GenInst_IEventSystemHandler_t2741188318_0_0_0,
	&GenInst_List_1_t2110309450_0_0_0,
	&GenInst_List_1_t2058570427_0_0_0,
	&GenInst_List_1_t3188497603_0_0_0,
	&GenInst_ISelectHandler_t2812555161_0_0_0,
	&GenInst_BaseRaycaster_t2336171397_0_0_0,
	&GenInst_Entry_t3365010046_0_0_0,
	&GenInst_BaseEventData_t2681005625_0_0_0,
	&GenInst_IPointerEnterHandler_t193164956_0_0_0,
	&GenInst_IPointerExitHandler_t461019860_0_0_0,
	&GenInst_IPointerDownHandler_t3929046918_0_0_0,
	&GenInst_IPointerUpHandler_t1847764461_0_0_0,
	&GenInst_IPointerClickHandler_t96169666_0_0_0,
	&GenInst_IInitializePotentialDragHandler_t3350809087_0_0_0,
	&GenInst_IBeginDragHandler_t3135127860_0_0_0,
	&GenInst_IDragHandler_t2583993319_0_0_0,
	&GenInst_IEndDragHandler_t1349123600_0_0_0,
	&GenInst_IDropHandler_t2390101210_0_0_0,
	&GenInst_IScrollHandler_t3834677510_0_0_0,
	&GenInst_IUpdateSelectedHandler_t3778909353_0_0_0,
	&GenInst_IMoveHandler_t2611925506_0_0_0,
	&GenInst_ISubmitHandler_t525803901_0_0_0,
	&GenInst_ICancelHandler_t1980319651_0_0_0,
	&GenInst_Transform_t3275118058_0_0_0,
	&GenInst_GameObject_t1756533147_0_0_0,
	&GenInst_BaseInput_t621514313_0_0_0,
	&GenInst_UIBehaviour_t3960014691_0_0_0,
	&GenInst_MonoBehaviour_t1158329972_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t2659922876_0_0_0,
	&GenInst_PointerEventData_t1599784723_0_0_0,
	&GenInst_ButtonState_t2688375492_0_0_0,
	&GenInst_RaycastHit2D_t4063908774_0_0_0,
	&GenInst_RaycastHit_t87180320_0_0_0,
	&GenInst_ICanvasElement_t986520779_0_0_0,
	&GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_ColorBlock_t2652774230_0_0_0,
	&GenInst_OptionData_t2420267500_0_0_0,
	&GenInst_DropdownItem_t4139978805_0_0_0,
	&GenInst_FloatTween_t2986189219_0_0_0,
	&GenInst_Sprite_t309593783_0_0_0,
	&GenInst_Canvas_t209405766_0_0_0,
	&GenInst_List_1_t3873494194_0_0_0,
	&GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0,
	&GenInst_Text_t356221433_0_0_0,
	&GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t850112849_0_0_0,
	&GenInst_ColorTween_t3438117476_0_0_0,
	&GenInst_Graphic_t2426225576_0_0_0,
	&GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0,
	&GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t2391682566_0_0_0,
	&GenInst_KeyValuePair_2_t3010968081_0_0_0,
	&GenInst_KeyValuePair_2_t1912381698_0_0_0,
	&GenInst_ContentType_t1028629049_0_0_0,
	&GenInst_Mask_t2977958238_0_0_0,
	&GenInst_List_1_t2347079370_0_0_0,
	&GenInst_RectMask2D_t1156185964_0_0_0,
	&GenInst_List_1_t525307096_0_0_0,
	&GenInst_Navigation_t1571958496_0_0_0,
	&GenInst_IClippable_t1941276057_0_0_0,
	&GenInst_Selectable_t1490392188_0_0_0,
	&GenInst_SpriteState_t1353336012_0_0_0,
	&GenInst_CanvasGroup_t3296560743_0_0_0,
	&GenInst_MatEntry_t3157325053_0_0_0,
	&GenInst_Toggle_t3976754468_0_0_0,
	&GenInst_Toggle_t3976754468_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_IClipper_t900477982_0_0_0,
	&GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t379984643_0_0_0,
	&GenInst_RectTransform_t3349966182_0_0_0,
	&GenInst_LayoutRebuilder_t2155218138_0_0_0,
	&GenInst_ILayoutElement_t1975293769_0_0_0_Single_t2076509932_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Single_t2076509932_0_0_0,
	&GenInst_List_1_t1612828712_0_0_0,
	&GenInst_List_1_t243638650_0_0_0,
	&GenInst_List_1_t1612828711_0_0_0,
	&GenInst_List_1_t1612828713_0_0_0,
	&GenInst_List_1_t1440998580_0_0_0,
	&GenInst_List_1_t573379950_0_0_0,
	&GenInst_String_t_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_FieldWithTarget_t2256174789_0_0_0,
	&GenInst_RenderTexture_t2666733923_0_0_0,
	&GenInst_Texture_t2243626319_0_0_0,
	&GenInst_Mesh_t1356156583_0_0_0,
	&GenInst_IEnumerable_1_t4048664256_gp_0_0_0_0,
	&GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m2949663298_gp_0_0_0_0,
	&GenInst_Array_Sort_m1730553742_gp_0_0_0_0_Array_Sort_m1730553742_gp_0_0_0_0,
	&GenInst_Array_Sort_m3106198730_gp_0_0_0_0_Array_Sort_m3106198730_gp_1_0_0_0,
	&GenInst_Array_Sort_m2090966156_gp_0_0_0_0,
	&GenInst_Array_Sort_m2090966156_gp_0_0_0_0_Array_Sort_m2090966156_gp_0_0_0_0,
	&GenInst_Array_Sort_m1985772939_gp_0_0_0_0,
	&GenInst_Array_Sort_m1985772939_gp_0_0_0_0_Array_Sort_m1985772939_gp_1_0_0_0,
	&GenInst_Array_Sort_m2736815140_gp_0_0_0_0_Array_Sort_m2736815140_gp_0_0_0_0,
	&GenInst_Array_Sort_m2468799988_gp_0_0_0_0_Array_Sort_m2468799988_gp_1_0_0_0,
	&GenInst_Array_Sort_m2587948790_gp_0_0_0_0,
	&GenInst_Array_Sort_m2587948790_gp_0_0_0_0_Array_Sort_m2587948790_gp_0_0_0_0,
	&GenInst_Array_Sort_m1279015767_gp_0_0_0_0,
	&GenInst_Array_Sort_m1279015767_gp_1_0_0_0,
	&GenInst_Array_Sort_m1279015767_gp_0_0_0_0_Array_Sort_m1279015767_gp_1_0_0_0,
	&GenInst_Array_Sort_m52621935_gp_0_0_0_0,
	&GenInst_Array_Sort_m3546416104_gp_0_0_0_0,
	&GenInst_Array_qsort_m533480027_gp_0_0_0_0,
	&GenInst_Array_qsort_m533480027_gp_0_0_0_0_Array_qsort_m533480027_gp_1_0_0_0,
	&GenInst_Array_compare_m940423571_gp_0_0_0_0,
	&GenInst_Array_qsort_m565008110_gp_0_0_0_0,
	&GenInst_Array_Resize_m1201602141_gp_0_0_0_0,
	&GenInst_Array_TrueForAll_m2783802133_gp_0_0_0_0,
	&GenInst_Array_ForEach_m3775633118_gp_0_0_0_0,
	&GenInst_Array_ConvertAll_m1734974082_gp_0_0_0_0_Array_ConvertAll_m1734974082_gp_1_0_0_0,
	&GenInst_Array_FindLastIndex_m934773128_gp_0_0_0_0,
	&GenInst_Array_FindLastIndex_m3202023711_gp_0_0_0_0,
	&GenInst_Array_FindLastIndex_m352384762_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m1593955424_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m1546138173_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m1082322798_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m525402987_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m3577113407_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m1033585031_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m3052238307_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m1306290405_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m2825795862_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m2841140625_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m3304283431_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m3860096562_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m2100440379_gp_0_0_0_0,
	&GenInst_Array_FindAll_m982349212_gp_0_0_0_0,
	&GenInst_Array_Exists_m1825464757_gp_0_0_0_0,
	&GenInst_Array_AsReadOnly_m1258056624_gp_0_0_0_0,
	&GenInst_Array_Find_m2529971459_gp_0_0_0_0,
	&GenInst_Array_FindLast_m3929249453_gp_0_0_0_0,
	&GenInst_InternalEnumerator_1_t3582267753_gp_0_0_0_0,
	&GenInst_ArrayReadOnlyList_1_t3367196019_gp_0_0_0_0,
	&GenInst_U3CGetEnumeratorU3Ec__Iterator0_t1667190089_gp_0_0_0_0,
	&GenInst_IList_1_t3737699284_gp_0_0_0_0,
	&GenInst_ICollection_1_t1552160836_gp_0_0_0_0,
	&GenInst_Nullable_1_t1398937014_gp_0_0_0_0,
	&GenInst_Comparer_1_t1036860714_gp_0_0_0_0,
	&GenInst_DefaultComparer_t3074655092_gp_0_0_0_0,
	&GenInst_GenericComparer_1_t1787398723_gp_0_0_0_0,
	&GenInst_Dictionary_2_t2276497324_gp_0_0_0_0,
	&GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t3180694294_0_0_0,
	&GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m404672235_gp_0_0_0_0,
	&GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0,
	&GenInst_Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_ShimEnumerator_t3895203923_gp_0_0_0_0_ShimEnumerator_t3895203923_gp_1_0_0_0,
	&GenInst_Enumerator_t2089681430_gp_0_0_0_0_Enumerator_t2089681430_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t3434615342_0_0_0,
	&GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0,
	&GenInst_KeyCollection_t1229212677_gp_0_0_0_0,
	&GenInst_Enumerator_t83320710_gp_0_0_0_0_Enumerator_t83320710_gp_1_0_0_0,
	&GenInst_Enumerator_t83320710_gp_0_0_0_0,
	&GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0_KeyCollection_t1229212677_gp_0_0_0_0,
	&GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_0_0_0_0,
	&GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0,
	&GenInst_ValueCollection_t2262344653_gp_1_0_0_0,
	&GenInst_Enumerator_t3111723616_gp_0_0_0_0_Enumerator_t3111723616_gp_1_0_0_0,
	&GenInst_Enumerator_t3111723616_gp_1_0_0_0,
	&GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0,
	&GenInst_ValueCollection_t2262344653_gp_1_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0,
	&GenInst_DictionaryEntry_t3048875398_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_KeyValuePair_2_t3180694294_0_0_0,
	&GenInst_KeyValuePair_2_t3180694294_0_0_0_KeyValuePair_2_t3180694294_0_0_0,
	&GenInst_Dictionary_2_t2276497324_gp_1_0_0_0,
	&GenInst_EqualityComparer_1_t2066709010_gp_0_0_0_0,
	&GenInst_DefaultComparer_t1766400012_gp_0_0_0_0,
	&GenInst_GenericEqualityComparer_1_t2202941003_gp_0_0_0_0,
	&GenInst_IDictionary_2_t3502329323_gp_0_0_0_0,
	&GenInst_KeyValuePair_2_t4174120762_0_0_0,
	&GenInst_IDictionary_2_t3502329323_gp_0_0_0_0_IDictionary_2_t3502329323_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t1988958766_gp_0_0_0_0_KeyValuePair_2_t1988958766_gp_1_0_0_0,
	&GenInst_List_1_t1169184319_gp_0_0_0_0,
	&GenInst_Enumerator_t1292967705_gp_0_0_0_0,
	&GenInst_Collection_1_t686054069_gp_0_0_0_0,
	&GenInst_ReadOnlyCollection_1_t3540981679_gp_0_0_0_0,
	&GenInst_MonoProperty_GetterAdapterFrame_m4157835592_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m4157835592_gp_1_0_0_0,
	&GenInst_MonoProperty_StaticGetterAdapterFrame_m1249362843_gp_0_0_0_0,
	&GenInst_ArraySegment_1_t1001032761_gp_0_0_0_0,
	&GenInst_LinkedList_1_t3556217344_gp_0_0_0_0,
	&GenInst_Enumerator_t4145643798_gp_0_0_0_0,
	&GenInst_LinkedListNode_1_t2172356692_gp_0_0_0_0,
	&GenInst_Queue_1_t1458930734_gp_0_0_0_0,
	&GenInst_Enumerator_t4000919638_gp_0_0_0_0,
	&GenInst_RBTree_Intern_m3121628056_gp_0_0_0_0,
	&GenInst_RBTree_Remove_m2480195356_gp_0_0_0_0,
	&GenInst_RBTree_Lookup_m3378201690_gp_0_0_0_0,
	&GenInst_RBTree_find_key_m2107656200_gp_0_0_0_0,
	&GenInst_SortedDictionary_2_t3426860295_gp_0_0_0_0,
	&GenInst_SortedDictionary_2_t3426860295_gp_0_0_0_0_SortedDictionary_2_t3426860295_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t4073929546_0_0_0,
	&GenInst_Node_t1473068371_gp_0_0_0_0_Node_t1473068371_gp_1_0_0_0,
	&GenInst_NodeHelper_t357096749_gp_0_0_0_0,
	&GenInst_NodeHelper_t357096749_gp_0_0_0_0_NodeHelper_t357096749_gp_1_0_0_0,
	&GenInst_ValueCollection_t2735575576_gp_0_0_0_0_ValueCollection_t2735575576_gp_1_0_0_0,
	&GenInst_ValueCollection_t2735575576_gp_1_0_0_0,
	&GenInst_Enumerator_t1353355221_gp_0_0_0_0_Enumerator_t1353355221_gp_1_0_0_0,
	&GenInst_Enumerator_t1353355221_gp_1_0_0_0,
	&GenInst_KeyCollection_t3620397270_gp_0_0_0_0_KeyCollection_t3620397270_gp_1_0_0_0,
	&GenInst_KeyCollection_t3620397270_gp_0_0_0_0,
	&GenInst_Enumerator_t4275149413_gp_0_0_0_0_Enumerator_t4275149413_gp_1_0_0_0,
	&GenInst_Enumerator_t4275149413_gp_0_0_0_0,
	&GenInst_Enumerator_t824916987_gp_0_0_0_0_Enumerator_t824916987_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t404405498_0_0_0,
	&GenInst_SortedDictionary_2_t3426860295_gp_1_0_0_0,
	&GenInst_SortedList_2_t3146765111_gp_0_0_0_0,
	&GenInst_SortedList_2_t3146765111_gp_0_0_0_0_SortedList_2_t3146765111_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t4007177354_0_0_0,
	&GenInst_EnumeratorMode_t215745978_gp_0_0_0_0_EnumeratorMode_t215745978_gp_1_0_0_0,
	&GenInst_Enumerator_t1609510401_gp_0_0_0_0_Enumerator_t1609510401_gp_1_0_0_0,
	&GenInst_KeyEnumerator_t3557860136_gp_0_0_0_0_KeyEnumerator_t3557860136_gp_1_0_0_0,
	&GenInst_KeyEnumerator_t3557860136_gp_0_0_0_0,
	&GenInst_ValueEnumerator_t2021630038_gp_0_0_0_0_ValueEnumerator_t2021630038_gp_1_0_0_0,
	&GenInst_ValueEnumerator_t2021630038_gp_1_0_0_0,
	&GenInst_ListKeys_t708716807_gp_0_0_0_0_ListKeys_t708716807_gp_1_0_0_0,
	&GenInst_ListKeys_t708716807_gp_0_0_0_0,
	&GenInst_GetEnumeratorU3Ec__Iterator2_t2910182623_gp_0_0_0_0_GetEnumeratorU3Ec__Iterator2_t2910182623_gp_1_0_0_0,
	&GenInst_ListValues_t4054146799_gp_0_0_0_0_ListValues_t4054146799_gp_1_0_0_0,
	&GenInst_ListValues_t4054146799_gp_1_0_0_0,
	&GenInst_GetEnumeratorU3Ec__Iterator3_t2100474802_gp_0_0_0_0_GetEnumeratorU3Ec__Iterator3_t2100474802_gp_1_0_0_0,
	&GenInst_GetEnumeratorU3Ec__Iterator0_t3888417572_gp_0_0_0_0_GetEnumeratorU3Ec__Iterator0_t3888417572_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t3522999862_0_0_0,
	&GenInst_U3CGetEnumeratorU3Ec__Iterator1_t3346222761_gp_0_0_0_0_U3CGetEnumeratorU3Ec__Iterator1_t3346222761_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t533259906_0_0_0,
	&GenInst_Stack_1_t4016656541_gp_0_0_0_0,
	&GenInst_Enumerator_t546412149_gp_0_0_0_0,
	&GenInst_HashSet_1_t2624254809_gp_0_0_0_0,
	&GenInst_Enumerator_t2109956843_gp_0_0_0_0,
	&GenInst_PrimeHelper_t3424417428_gp_0_0_0_0,
	&GenInst_Enumerable_Contains_m1284016302_gp_0_0_0_0,
	&GenInst_Enumerable_Contains_m4622279_gp_0_0_0_0,
	&GenInst_Enumerable_Count_m1561720045_gp_0_0_0_0,
	&GenInst_Enumerable_First_m4120844597_gp_0_0_0_0,
	&GenInst_Enumerable_First_m4120844597_gp_0_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Enumerable_First_m1693250038_gp_0_0_0_0,
	&GenInst_Enumerable_First_m2837459031_gp_0_0_0_0,
	&GenInst_Enumerable_First_m2837459031_gp_0_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Enumerable_OrderBy_m920500904_gp_0_0_0_0,
	&GenInst_Enumerable_OrderBy_m920500904_gp_0_0_0_0_Enumerable_OrderBy_m920500904_gp_1_0_0_0,
	&GenInst_Enumerable_OrderBy_m4222740363_gp_0_0_0_0,
	&GenInst_Enumerable_OrderBy_m4222740363_gp_0_0_0_0_Enumerable_OrderBy_m4222740363_gp_1_0_0_0,
	&GenInst_Enumerable_OrderBy_m4222740363_gp_1_0_0_0,
	&GenInst_Enumerable_Select_m2459603006_gp_0_0_0_0,
	&GenInst_Enumerable_Select_m2459603006_gp_0_0_0_0_Enumerable_Select_m2459603006_gp_1_0_0_0,
	&GenInst_Enumerable_Select_m2459603006_gp_1_0_0_0,
	&GenInst_Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0,
	&GenInst_Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0_Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0,
	&GenInst_Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0,
	&GenInst_Enumerable_Single_m1979188101_gp_0_0_0_0,
	&GenInst_Enumerable_Single_m1979188101_gp_0_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Enumerable_SingleOrDefault_m2813552737_gp_0_0_0_0,
	&GenInst_Enumerable_SingleOrDefault_m2813552737_gp_0_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Enumerable_ToArray_m2343256994_gp_0_0_0_0,
	&GenInst_Enumerable_Where_m2409552823_gp_0_0_0_0,
	&GenInst_Enumerable_Where_m2409552823_gp_0_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0,
	&GenInst_Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0,
	&GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0,
	&GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0,
	&GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0,
	&GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_IOrderedEnumerable_1_t641749975_gp_0_0_0_0,
	&GenInst_OrderedEnumerable_1_t753306046_gp_0_0_0_0,
	&GenInst_OrderedSequence_2_t1023848160_gp_0_0_0_0,
	&GenInst_OrderedSequence_2_t1023848160_gp_0_0_0_0_OrderedSequence_2_t1023848160_gp_1_0_0_0,
	&GenInst_OrderedSequence_2_t1023848160_gp_1_0_0_0,
	&GenInst_QuickSort_1_t1290221672_gp_0_0_0_0,
	&GenInst_U3CSortU3Ec__Iterator21_t163525460_gp_0_0_0_0,
	&GenInst_SortContext_1_t4088581714_gp_0_0_0_0,
	&GenInst_SortSequenceContext_2_t3419387730_gp_0_0_0_0,
	&GenInst_SortSequenceContext_2_t3419387730_gp_0_0_0_0_SortSequenceContext_2_t3419387730_gp_1_0_0_0,
	&GenInst_SortSequenceContext_2_t3419387730_gp_1_0_0_0,
	&GenInst_Component_GetComponentInChildren_m3417738402_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m1286417916_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m1989846061_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m3998291033_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m3421358420_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInParent_m825036157_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInParent_m3873375864_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInParent_m1600202230_gp_0_0_0_0,
	&GenInst_Component_GetComponents_m3990064736_gp_0_0_0_0,
	&GenInst_Component_GetComponents_m2051523689_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentInChildren_m3844288190_gp_0_0_0_0,
	&GenInst_GameObject_GetComponents_m2621570726_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentsInChildren_m1933507101_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentsInParent_m4177085118_gp_0_0_0_0,
	&GenInst_Mesh_GetAllocArrayFromChannel_m3109360642_gp_0_0_0_0,
	&GenInst_Mesh_SafeLength_m3101579087_gp_0_0_0_0,
	&GenInst_Mesh_SetListForChannel_m3999848894_gp_0_0_0_0,
	&GenInst_Mesh_SetListForChannel_m4171325764_gp_0_0_0_0,
	&GenInst_Mesh_SetUvsImpl_m3197944684_gp_0_0_0_0,
	&GenInst_Object_Instantiate_m2530741872_gp_0_0_0_0,
	&GenInst_Object_FindObjectsOfType_m894835059_gp_0_0_0_0,
	&GenInst_GenericMixerPlayable_CastTo_m984828924_gp_0_0_0_0,
	&GenInst_AnimationPlayable_CastTo_m2315139850_gp_0_0_0_0,
	&GenInst_CustomAnimationPlayable_CastTo_m2887957467_gp_0_0_0_0,
	&GenInst_InvokableCall_1_t476640868_gp_0_0_0_0,
	&GenInst_UnityAction_1_t2490859068_0_0_0,
	&GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0_InvokableCall_2_t2042724809_gp_1_0_0_0,
	&GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0,
	&GenInst_InvokableCall_2_t2042724809_gp_1_0_0_0,
	&GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0_InvokableCall_3_t3608808750_gp_1_0_0_0_InvokableCall_3_t3608808750_gp_2_0_0_0,
	&GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0,
	&GenInst_InvokableCall_3_t3608808750_gp_1_0_0_0,
	&GenInst_InvokableCall_3_t3608808750_gp_2_0_0_0,
	&GenInst_InvokableCall_4_t879925395_gp_0_0_0_0_InvokableCall_4_t879925395_gp_1_0_0_0_InvokableCall_4_t879925395_gp_2_0_0_0_InvokableCall_4_t879925395_gp_3_0_0_0,
	&GenInst_InvokableCall_4_t879925395_gp_0_0_0_0,
	&GenInst_InvokableCall_4_t879925395_gp_1_0_0_0,
	&GenInst_InvokableCall_4_t879925395_gp_2_0_0_0,
	&GenInst_InvokableCall_4_t879925395_gp_3_0_0_0,
	&GenInst_CachedInvokableCall_1_t224769006_gp_0_0_0_0,
	&GenInst_UnityEvent_1_t4075366602_gp_0_0_0_0,
	&GenInst_UnityEvent_2_t4075366599_gp_0_0_0_0_UnityEvent_2_t4075366599_gp_1_0_0_0,
	&GenInst_UnityEvent_3_t4075366600_gp_0_0_0_0_UnityEvent_3_t4075366600_gp_1_0_0_0_UnityEvent_3_t4075366600_gp_2_0_0_0,
	&GenInst_UnityEvent_4_t4075366597_gp_0_0_0_0_UnityEvent_4_t4075366597_gp_1_0_0_0_UnityEvent_4_t4075366597_gp_2_0_0_0_UnityEvent_4_t4075366597_gp_3_0_0_0,
	&GenInst_Runtime_GetNSObject_m58080407_gp_0_0_0_0,
	&GenInst_AWSConfigs_GetConfigEnum_m1225161757_gp_0_0_0_0,
	&GenInst_AWSConfigs_ParseEnum_m486237848_gp_0_0_0_0,
	&GenInst_AWSConfigs_GetSection_m3599347093_gp_0_0_0_0,
	&GenInst_AWSSDKUtils_InvokeInBackground_m898943770_gp_0_0_0_0,
	&GenInst_U3CU3Ec__DisplayClass37_0_1_t500617122_gp_0_0_0_0,
	&GenInst_AndroidInterop_CallMethod_m2060567458_gp_0_0_0_0,
	&GenInst_AndroidInterop_GetJavaField_m3750603155_gp_0_0_0_0,
	&GenInst_U3CU3Ec__2_1_t1696963520_gp_0_0_0_0,
	&GenInst_U3CU3Ec__6_1_t2261613524_gp_0_0_0_0,
	&GenInst_ServiceFactory_GetService_m3760662002_gp_0_0_0_0,
	&GenInst_IHttpRequestFactory_1_t4193720315_gp_0_0_0_0,
	&GenInst_AmazonServiceCallback_2_t3530050132_gp_0_0_0_0_AmazonServiceCallback_2_t3530050132_gp_1_0_0_0,
	&GenInst_AmazonServiceResult_2_t529501990_gp_0_0_0_0_AmazonServiceResult_2_t529501990_gp_1_0_0_0,
	&GenInst_ExceptionHandler_1_t3179100912_gp_0_0_0_0,
	&GenInst_HttpHandler_1_t1383532101_gp_0_0_0_0,
	&GenInst_DefaultRetryPolicy_IsInnerException_m2974190552_gp_0_0_0_0,
	&GenInst_AlwaysSendList_1_t1375725976_gp_0_0_0_0,
	&GenInst_BackgroundDispatcher_1_t295417742_gp_0_0_0_0,
	&GenInst_ThreadPoolThrottler_1_t1982498819_gp_0_0_0_0,
	&GenInst_Exception_t1927440687_0_0_0_ThreadPoolThrottler_1_t1982498819_gp_0_0_0_0,
	&GenInst_ThreadPoolOptions_1_t170443295_0_0_0,
	&GenInst_ThreadPoolThrottler_1_t1982498819_gp_0_0_0_0_ThreadPoolThrottler_1_t1982498819_gp_0_0_0_0,
	&GenInst_ThreadPoolOptions_1_t2545192696_gp_1_0_0_0,
	&GenInst_Exception_t1927440687_0_0_0_ThreadPoolOptions_1_t2545192696_gp_1_0_0_0,
	&GenInst_IResponseUnmarshaller_2_t2708327486_gp_0_0_0_0_IResponseUnmarshaller_2_t2708327486_gp_1_0_0_0,
	&GenInst_ListUnmarshaller_2_t3080449122_gp_0_0_0_0,
	&GenInst_List_1_t3097202703_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0,
	&GenInst_List_1_t3097202703_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0,
	&GenInst_ListUnmarshaller_2_t3080449122_gp_0_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0,
	&GenInst_ListUnmarshaller_2_t3080449122_gp_0_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0,
	&GenInst_UnmarshallerExtensions_Add_m3566798727_gp_0_0_0_0_UnmarshallerExtensions_Add_m3566798727_gp_1_0_0_0,
	&GenInst_ExecuteEvents_Execute_m1961163955_gp_0_0_0_0,
	&GenInst_ExecuteEvents_ExecuteHierarchy_m1189839031_gp_0_0_0_0,
	&GenInst_ExecuteEvents_GetEventList_m1741636447_gp_0_0_0_0,
	&GenInst_ExecuteEvents_CanHandleEvent_m3661808413_gp_0_0_0_0,
	&GenInst_ExecuteEvents_GetEventHandler_m609328278_gp_0_0_0_0,
	&GenInst_TweenRunner_1_t2584777480_gp_0_0_0_0,
	&GenInst_Dropdown_GetOrAddComponent_m1533008304_gp_0_0_0_0,
	&GenInst_SetPropertyUtility_SetEquatableStruct_m753578693_gp_0_0_0_0,
	&GenInst_IndexedSet_1_t573160278_gp_0_0_0_0,
	&GenInst_IndexedSet_1_t573160278_gp_0_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_ListPool_1_t1984115411_gp_0_0_0_0,
	&GenInst_List_1_t2000868992_0_0_0,
	&GenInst_ObjectPool_1_t4265859154_gp_0_0_0_0,
	&GenInst_DefaultExecutionOrder_t2717914595_0_0_0,
	&GenInst_GUILayer_t3254902478_0_0_0,
	&GenInst_LoggingOptions_t2865640765_0_0_0,
	&GenInst_ResponseLoggingOption_t3443611737_0_0_0,
	&GenInst_ThreadAbortException_t1150575753_0_0_0,
	&GenInst_WebException_t3368933679_0_0_0,
	&GenInst_INetworkReachability_t2670889314_0_0_0,
	&GenInst_UnityInitializer_t2778189483_0_0_0,
	&GenInst_UnityMainThreadDispatcher_t4098072663_0_0_0,
	&GenInst_NSDictionary_t3598984691_0_0_0,
	&GenInst_IEnvironmentInfo_t2314249358_0_0_0,
	&GenInst_AWSSection_t3096528870_0_0_0,
	&GenInst_NSBundle_t3599697655_0_0_0,
	&GenInst_NSObject_t1518098886_0_0_0,
	&GenInst_NSLocale_t2224424797_0_0_0,
	&GenInst_UIDevice_t860038178_0_0_0,
	&GenInst_AssumeRoleWithWebIdentityRequest_t193094215_0_0_0,
	&GenInst_GetCredentialsForIdentityRequest_t932830458_0_0_0_GetCredentialsForIdentityResponse_t2302678766_0_0_0,
	&GenInst_GetIdRequest_t4078561340_0_0_0_GetIdResponse_t2091118072_0_0_0,
	&GenInst_GetOpenIdTokenRequest_t2698079901_0_0_0_GetOpenIdTokenResponse_t955597635_0_0_0,
	&GenInst_IApplicationSettings_t2849513616_0_0_0,
	&GenInst_CredentialsRetriever_t208022274_0_0_0,
	&GenInst_Marshaller_t3371889241_0_0_0,
	&GenInst_Signer_t1810841758_0_0_0,
	&GenInst_DeleteDatasetRequest_t3672322944_0_0_0_DeleteDatasetResponse_t2927760190_0_0_0,
	&GenInst_ListRecordsRequest_t3074726983_0_0_0_ListRecordsResponse_t1441155223_0_0_0,
	&GenInst_UpdateRecordsRequest_t197801344_0_0_0_UpdateRecordsResponse_t1893058994_0_0_0,
	&GenInst_EventSystem_t3466835263_0_0_0,
	&GenInst_AxisEventData_t1524870173_0_0_0,
	&GenInst_SpriteRenderer_t1209076198_0_0_0,
	&GenInst_AspectMode_t1166448724_0_0_0,
	&GenInst_FitMode_t4030874534_0_0_0,
	&GenInst_GraphicRaycaster_t410733016_0_0_0,
	&GenInst_Image_t2042527209_0_0_0,
	&GenInst_Button_t2872111280_0_0_0,
	&GenInst_Dropdown_t1985816271_0_0_0,
	&GenInst_CanvasRenderer_t261436805_0_0_0,
	&GenInst_Corner_t1077473318_0_0_0,
	&GenInst_Axis_t1431825778_0_0_0,
	&GenInst_Constraint_t3558160636_0_0_0,
	&GenInst_Type_t3352948571_0_0_0,
	&GenInst_FillMethod_t1640962579_0_0_0,
	&GenInst_SubmitEvent_t907918422_0_0_0,
	&GenInst_OnChangeEvent_t2863344003_0_0_0,
	&GenInst_OnValidateInput_t1946318473_0_0_0,
	&GenInst_LineType_t2931319356_0_0_0,
	&GenInst_InputType_t1274231802_0_0_0,
	&GenInst_TouchScreenKeyboardType_t875112366_0_0_0,
	&GenInst_CharacterValidation_t3437478890_0_0_0,
	&GenInst_LayoutElement_t2808691390_0_0_0,
	&GenInst_RectOffset_t3387826427_0_0_0,
	&GenInst_TextAnchor_t112990806_0_0_0,
	&GenInst_Direction_t3696775921_0_0_0,
	&GenInst_Transition_t605142169_0_0_0,
	&GenInst_AnimationTriggers_t3244928895_0_0_0,
	&GenInst_Animator_t69676727_0_0_0,
	&GenInst_Direction_t1525323322_0_0_0,
	&GenInst_Rigidbody_t4233889191_0_0_0,
	&GenInst_KeyInfo_t777769675_0_0_0_KeyInfo_t777769675_0_0_0,
	&GenInst_Byte_t3683104436_0_0_0_Byte_t3683104436_0_0_0,
	&GenInst_KeyValuePair_2_t3749587448_0_0_0_KeyValuePair_2_t3749587448_0_0_0,
	&GenInst_KeyValuePair_2_t38854645_0_0_0_KeyValuePair_2_t38854645_0_0_0,
	&GenInst_CustomAttributeNamedArgument_t94157543_0_0_0_CustomAttributeNamedArgument_t94157543_0_0_0,
	&GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0_CustomAttributeTypedArgument_t1498197914_0_0_0,
	&GenInst_Color32_t874517518_0_0_0_Color32_t874517518_0_0_0,
	&GenInst_RaycastResult_t21186376_0_0_0_RaycastResult_t21186376_0_0_0,
	&GenInst_Playable_t3667545548_0_0_0_Playable_t3667545548_0_0_0,
	&GenInst_UICharInfo_t3056636800_0_0_0_UICharInfo_t3056636800_0_0_0,
	&GenInst_UILineInfo_t3621277874_0_0_0_UILineInfo_t3621277874_0_0_0,
	&GenInst_UIVertex_t1204258818_0_0_0_UIVertex_t1204258818_0_0_0,
	&GenInst_Vector2_t2243707579_0_0_0_Vector2_t2243707579_0_0_0,
	&GenInst_Vector3_t2243707580_0_0_0_Vector3_t2243707580_0_0_0,
	&GenInst_Vector4_t2243707581_0_0_0_Vector4_t2243707581_0_0_0,
	&GenInst_Metric_t3273440202_0_0_0_Metric_t3273440202_0_0_0,
	&GenInst_KeyValuePair_2_t1416501748_0_0_0_KeyValuePair_2_t1416501748_0_0_0,
	&GenInst_KeyValuePair_2_t1416501748_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Int64_t909078037_0_0_0_Int64_t909078037_0_0_0,
	&GenInst_KeyValuePair_2_t3196873006_0_0_0_KeyValuePair_2_t3196873006_0_0_0,
	&GenInst_KeyValuePair_2_t3196873006_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t3132015601_0_0_0_KeyValuePair_2_t3132015601_0_0_0,
	&GenInst_KeyValuePair_2_t3132015601_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t3749587448_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t1436312919_0_0_0_KeyValuePair_2_t1436312919_0_0_0,
	&GenInst_KeyValuePair_2_t1436312919_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t888819835_0_0_0_KeyValuePair_2_t888819835_0_0_0,
	&GenInst_KeyValuePair_2_t888819835_0_0_0_Il2CppObject_0_0_0,
	&GenInst_IntPtr_t_0_0_0_IntPtr_t_0_0_0,
	&GenInst_InstantiationModel_t1632807378_0_0_0_InstantiationModel_t1632807378_0_0_0,
	&GenInst_InstantiationModel_t1632807378_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t3277180024_0_0_0_KeyValuePair_2_t3277180024_0_0_0,
	&GenInst_KeyValuePair_2_t3277180024_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_KeyValuePair_2_t1174980068_0_0_0_KeyValuePair_2_t1174980068_0_0_0,
	&GenInst_KeyValuePair_2_t1174980068_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t3716250094_0_0_0_KeyValuePair_2_t3716250094_0_0_0,
	&GenInst_KeyValuePair_2_t3716250094_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t2779450660_0_0_0_KeyValuePair_2_t2779450660_0_0_0,
	&GenInst_KeyValuePair_2_t2779450660_0_0_0_Il2CppObject_0_0_0,
	&GenInst_ArrayMetadata_t1135078014_0_0_0_Il2CppObject_0_0_0,
	&GenInst_ArrayMetadata_t1135078014_0_0_0_ArrayMetadata_t1135078014_0_0_0,
	&GenInst_KeyValuePair_2_t1407543090_0_0_0_KeyValuePair_2_t1407543090_0_0_0,
	&GenInst_KeyValuePair_2_t1407543090_0_0_0_Il2CppObject_0_0_0,
	&GenInst_ObjectMetadata_t4058137740_0_0_0_Il2CppObject_0_0_0,
	&GenInst_ObjectMetadata_t4058137740_0_0_0_ObjectMetadata_t4058137740_0_0_0,
	&GenInst_KeyValuePair_2_t488203048_0_0_0_KeyValuePair_2_t488203048_0_0_0,
	&GenInst_KeyValuePair_2_t488203048_0_0_0_Il2CppObject_0_0_0,
	&GenInst_TextEditOp_t3138797698_0_0_0_Il2CppObject_0_0_0,
	&GenInst_TextEditOp_t3138797698_0_0_0_TextEditOp_t3138797698_0_0_0,
	&GenInst_LoggingSection_t905443946_0_0_0,
};
