﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.Unmarshaller
struct Unmarshaller_t1994457618;
// Amazon.Runtime.IExecutionContext
struct IExecutionContext_t2477130752;
// Amazon.Runtime.IAsyncExecutionContext
struct IAsyncExecutionContext_t3792344986;
// Amazon.Runtime.AmazonWebServiceResponse
struct AmazonWebServiceResponse_t529043356;
// Amazon.Runtime.Internal.Transform.UnmarshallerContext
struct UnmarshallerContext_t1608322995;
// Amazon.Runtime.IRequestContext
struct IRequestContext_t1620878341;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_Unma1608322995.h"

// System.Void Amazon.Runtime.Internal.Unmarshaller::.ctor(System.Boolean)
extern "C"  void Unmarshaller__ctor_m598181187 (Unmarshaller_t1994457618 * __this, bool ___supportsResponseLogging0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Unmarshaller::InvokeSync(Amazon.Runtime.IExecutionContext)
extern "C"  void Unmarshaller_InvokeSync_m3753605365 (Unmarshaller_t1994457618 * __this, Il2CppObject * ___executionContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Unmarshaller::InvokeAsyncCallback(Amazon.Runtime.IAsyncExecutionContext)
extern "C"  void Unmarshaller_InvokeAsyncCallback_m3242749287 (Unmarshaller_t1994457618 * __this, Il2CppObject * ___executionContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Unmarshaller::Unmarshall(Amazon.Runtime.IExecutionContext)
extern "C"  void Unmarshaller_Unmarshall_m1586489165 (Unmarshaller_t1994457618 * __this, Il2CppObject * ___executionContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.AmazonWebServiceResponse Amazon.Runtime.Internal.Unmarshaller::UnmarshallResponse(Amazon.Runtime.Internal.Transform.UnmarshallerContext,Amazon.Runtime.IRequestContext)
extern "C"  AmazonWebServiceResponse_t529043356 * Unmarshaller_UnmarshallResponse_m2924884744 (Unmarshaller_t1994457618 * __this, UnmarshallerContext_t1608322995 * ___context0, Il2CppObject * ___requestContext1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.Unmarshaller::ShouldLogResponseBody(System.Boolean,Amazon.Runtime.IRequestContext)
extern "C"  bool Unmarshaller_ShouldLogResponseBody_m3908578072 (Il2CppObject * __this /* static, unused */, bool ___supportsResponseLogging0, Il2CppObject * ___requestContext1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
