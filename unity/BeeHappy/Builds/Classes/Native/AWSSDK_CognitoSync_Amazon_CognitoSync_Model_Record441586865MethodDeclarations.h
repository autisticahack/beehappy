﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.CognitoSync.Model.Record
struct Record_t441586865;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime693205669.h"
#include "mscorlib_System_String2029220233.h"

// System.DateTime Amazon.CognitoSync.Model.Record::get_DeviceLastModifiedDate()
extern "C"  DateTime_t693205669  Record_get_DeviceLastModifiedDate_m1671571673 (Record_t441586865 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Model.Record::set_DeviceLastModifiedDate(System.DateTime)
extern "C"  void Record_set_DeviceLastModifiedDate_m4241908948 (Record_t441586865 * __this, DateTime_t693205669  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.CognitoSync.Model.Record::get_Key()
extern "C"  String_t* Record_get_Key_m3007674313 (Record_t441586865 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Model.Record::set_Key(System.String)
extern "C"  void Record_set_Key_m533819514 (Record_t441586865 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.CognitoSync.Model.Record::get_LastModifiedBy()
extern "C"  String_t* Record_get_LastModifiedBy_m1186534462 (Record_t441586865 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Model.Record::set_LastModifiedBy(System.String)
extern "C"  void Record_set_LastModifiedBy_m3142691705 (Record_t441586865 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime Amazon.CognitoSync.Model.Record::get_LastModifiedDate()
extern "C"  DateTime_t693205669  Record_get_LastModifiedDate_m4106323493 (Record_t441586865 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Model.Record::set_LastModifiedDate(System.DateTime)
extern "C"  void Record_set_LastModifiedDate_m3994144806 (Record_t441586865 * __this, DateTime_t693205669  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Amazon.CognitoSync.Model.Record::get_SyncCount()
extern "C"  int64_t Record_get_SyncCount_m4003797492 (Record_t441586865 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Model.Record::set_SyncCount(System.Int64)
extern "C"  void Record_set_SyncCount_m879056681 (Record_t441586865 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.CognitoSync.Model.Record::get_Value()
extern "C"  String_t* Record_get_Value_m3860440865 (Record_t441586865 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Model.Record::set_Value(System.String)
extern "C"  void Record_set_Value_m4079513444 (Record_t441586865 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Model.Record::.ctor()
extern "C"  void Record__ctor_m3038027352 (Record_t441586865 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
