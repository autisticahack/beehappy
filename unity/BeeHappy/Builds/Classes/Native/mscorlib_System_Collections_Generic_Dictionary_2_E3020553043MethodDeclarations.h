﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En675255274MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,ThirdParty.Json.LitJson.ObjectMetadata>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m3235335334(__this, ___dictionary0, method) ((  void (*) (Enumerator_t3020553043 *, Dictionary_2_t1700528341 *, const MethodInfo*))Enumerator__ctor_m3882269915_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m227344551(__this, method) ((  Il2CppObject * (*) (Enumerator_t3020553043 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3006262826_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2940621431(__this, method) ((  void (*) (Enumerator_t3020553043 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m786642932_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Type,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2765794982(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t3020553043 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3805059129_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m4007969549(__this, method) ((  Il2CppObject * (*) (Enumerator_t3020553043 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3135173292_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2912663917(__this, method) ((  Il2CppObject * (*) (Enumerator_t3020553043 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3285338134_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Type,ThirdParty.Json.LitJson.ObjectMetadata>::MoveNext()
#define Enumerator_MoveNext_m3362052147(__this, method) ((  bool (*) (Enumerator_t3020553043 *, const MethodInfo*))Enumerator_MoveNext_m513463628_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Type,ThirdParty.Json.LitJson.ObjectMetadata>::get_Current()
#define Enumerator_get_Current_m4173487003(__this, method) ((  KeyValuePair_2_t3752840859  (*) (Enumerator_t3020553043 *, const MethodInfo*))Enumerator_get_Current_m2333579100_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Type,ThirdParty.Json.LitJson.ObjectMetadata>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m183068262(__this, method) ((  Type_t * (*) (Enumerator_t3020553043 *, const MethodInfo*))Enumerator_get_CurrentKey_m329261871_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Type,ThirdParty.Json.LitJson.ObjectMetadata>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m688347110(__this, method) ((  ObjectMetadata_t4058137740  (*) (Enumerator_t3020553043 *, const MethodInfo*))Enumerator_get_CurrentValue_m2906228503_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,ThirdParty.Json.LitJson.ObjectMetadata>::Reset()
#define Enumerator_Reset_m3084931216(__this, method) ((  void (*) (Enumerator_t3020553043 *, const MethodInfo*))Enumerator_Reset_m975923089_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,ThirdParty.Json.LitJson.ObjectMetadata>::VerifyState()
#define Enumerator_VerifyState_m2984722525(__this, method) ((  void (*) (Enumerator_t3020553043 *, const MethodInfo*))Enumerator_VerifyState_m3950116612_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,ThirdParty.Json.LitJson.ObjectMetadata>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m3649792735(__this, method) ((  void (*) (Enumerator_t3020553043 *, const MethodInfo*))Enumerator_VerifyCurrent_m424726820_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,ThirdParty.Json.LitJson.ObjectMetadata>::Dispose()
#define Enumerator_Dispose_m3425649874(__this, method) ((  void (*) (Enumerator_t3020553043 *, const MethodInfo*))Enumerator_Dispose_m3127619583_gshared)(__this, method)
