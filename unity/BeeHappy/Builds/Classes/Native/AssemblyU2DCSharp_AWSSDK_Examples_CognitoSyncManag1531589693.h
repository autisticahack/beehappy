﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.CognitoSync.SyncManager.Dataset
struct Dataset_t3040902086;
// System.String
struct String_t;
// Amazon.CognitoIdentity.CognitoAWSCredentials
struct CognitoAWSCredentials_t2370264792;
// Amazon.CognitoSync.SyncManager.CognitoSyncManager
struct CognitoSyncManager_t1222571519;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AWSSDK.Examples.CognitoSyncManagerSample
struct  CognitoSyncManagerSample_t1531589693  : public MonoBehaviour_t1158329972
{
public:
	// Amazon.CognitoSync.SyncManager.Dataset AWSSDK.Examples.CognitoSyncManagerSample::playerInfo
	Dataset_t3040902086 * ___playerInfo_2;
	// System.String AWSSDK.Examples.CognitoSyncManagerSample::playerName
	String_t* ___playerName_3;
	// System.String AWSSDK.Examples.CognitoSyncManagerSample::alias
	String_t* ___alias_4;
	// System.String AWSSDK.Examples.CognitoSyncManagerSample::statusMessage
	String_t* ___statusMessage_5;
	// System.String AWSSDK.Examples.CognitoSyncManagerSample::IdentityPoolId
	String_t* ___IdentityPoolId_6;
	// System.String AWSSDK.Examples.CognitoSyncManagerSample::Region
	String_t* ___Region_7;
	// Amazon.CognitoIdentity.CognitoAWSCredentials AWSSDK.Examples.CognitoSyncManagerSample::_credentials
	CognitoAWSCredentials_t2370264792 * ____credentials_8;
	// Amazon.CognitoSync.SyncManager.CognitoSyncManager AWSSDK.Examples.CognitoSyncManagerSample::_syncManager
	CognitoSyncManager_t1222571519 * ____syncManager_9;

public:
	inline static int32_t get_offset_of_playerInfo_2() { return static_cast<int32_t>(offsetof(CognitoSyncManagerSample_t1531589693, ___playerInfo_2)); }
	inline Dataset_t3040902086 * get_playerInfo_2() const { return ___playerInfo_2; }
	inline Dataset_t3040902086 ** get_address_of_playerInfo_2() { return &___playerInfo_2; }
	inline void set_playerInfo_2(Dataset_t3040902086 * value)
	{
		___playerInfo_2 = value;
		Il2CppCodeGenWriteBarrier(&___playerInfo_2, value);
	}

	inline static int32_t get_offset_of_playerName_3() { return static_cast<int32_t>(offsetof(CognitoSyncManagerSample_t1531589693, ___playerName_3)); }
	inline String_t* get_playerName_3() const { return ___playerName_3; }
	inline String_t** get_address_of_playerName_3() { return &___playerName_3; }
	inline void set_playerName_3(String_t* value)
	{
		___playerName_3 = value;
		Il2CppCodeGenWriteBarrier(&___playerName_3, value);
	}

	inline static int32_t get_offset_of_alias_4() { return static_cast<int32_t>(offsetof(CognitoSyncManagerSample_t1531589693, ___alias_4)); }
	inline String_t* get_alias_4() const { return ___alias_4; }
	inline String_t** get_address_of_alias_4() { return &___alias_4; }
	inline void set_alias_4(String_t* value)
	{
		___alias_4 = value;
		Il2CppCodeGenWriteBarrier(&___alias_4, value);
	}

	inline static int32_t get_offset_of_statusMessage_5() { return static_cast<int32_t>(offsetof(CognitoSyncManagerSample_t1531589693, ___statusMessage_5)); }
	inline String_t* get_statusMessage_5() const { return ___statusMessage_5; }
	inline String_t** get_address_of_statusMessage_5() { return &___statusMessage_5; }
	inline void set_statusMessage_5(String_t* value)
	{
		___statusMessage_5 = value;
		Il2CppCodeGenWriteBarrier(&___statusMessage_5, value);
	}

	inline static int32_t get_offset_of_IdentityPoolId_6() { return static_cast<int32_t>(offsetof(CognitoSyncManagerSample_t1531589693, ___IdentityPoolId_6)); }
	inline String_t* get_IdentityPoolId_6() const { return ___IdentityPoolId_6; }
	inline String_t** get_address_of_IdentityPoolId_6() { return &___IdentityPoolId_6; }
	inline void set_IdentityPoolId_6(String_t* value)
	{
		___IdentityPoolId_6 = value;
		Il2CppCodeGenWriteBarrier(&___IdentityPoolId_6, value);
	}

	inline static int32_t get_offset_of_Region_7() { return static_cast<int32_t>(offsetof(CognitoSyncManagerSample_t1531589693, ___Region_7)); }
	inline String_t* get_Region_7() const { return ___Region_7; }
	inline String_t** get_address_of_Region_7() { return &___Region_7; }
	inline void set_Region_7(String_t* value)
	{
		___Region_7 = value;
		Il2CppCodeGenWriteBarrier(&___Region_7, value);
	}

	inline static int32_t get_offset_of__credentials_8() { return static_cast<int32_t>(offsetof(CognitoSyncManagerSample_t1531589693, ____credentials_8)); }
	inline CognitoAWSCredentials_t2370264792 * get__credentials_8() const { return ____credentials_8; }
	inline CognitoAWSCredentials_t2370264792 ** get_address_of__credentials_8() { return &____credentials_8; }
	inline void set__credentials_8(CognitoAWSCredentials_t2370264792 * value)
	{
		____credentials_8 = value;
		Il2CppCodeGenWriteBarrier(&____credentials_8, value);
	}

	inline static int32_t get_offset_of__syncManager_9() { return static_cast<int32_t>(offsetof(CognitoSyncManagerSample_t1531589693, ____syncManager_9)); }
	inline CognitoSyncManager_t1222571519 * get__syncManager_9() const { return ____syncManager_9; }
	inline CognitoSyncManager_t1222571519 ** get_address_of__syncManager_9() { return &____syncManager_9; }
	inline void set__syncManager_9(CognitoSyncManager_t1222571519 * value)
	{
		____syncManager_9 = value;
		Il2CppCodeGenWriteBarrier(&____syncManager_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
