﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.Runtime.Internal.Transform.IntUnmarshaller
struct IntUnmarshaller_t2174001701;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Transform.IntUnmarshaller
struct  IntUnmarshaller_t2174001701  : public Il2CppObject
{
public:

public:
};

struct IntUnmarshaller_t2174001701_StaticFields
{
public:
	// Amazon.Runtime.Internal.Transform.IntUnmarshaller Amazon.Runtime.Internal.Transform.IntUnmarshaller::_instance
	IntUnmarshaller_t2174001701 * ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(IntUnmarshaller_t2174001701_StaticFields, ____instance_0)); }
	inline IntUnmarshaller_t2174001701 * get__instance_0() const { return ____instance_0; }
	inline IntUnmarshaller_t2174001701 ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(IntUnmarshaller_t2174001701 * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier(&____instance_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
