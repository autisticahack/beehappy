﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac3343836395.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac3928470916.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac2260664863.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac3435657708.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac2213949596.h"
#include "UnityEngine_UI_UnityEngine_UI_VertexHelper385374196.h"
#include "UnityEngine_UI_UnityEngine_UI_BaseMeshEffect1728560551.h"
#include "UnityEngine_UI_UnityEngine_UI_Outline1417504278.h"
#include "UnityEngine_UI_UnityEngine_UI_PositionAsUV11102546563.h"
#include "UnityEngine_UI_UnityEngine_UI_Shadow4269599528.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E_1568637717.h"
#include "UnityEngine_Analytics_U3CModuleU3E3783534214.h"
#include "UnityEngine_Analytics_UnityEngine_Analytics_Analyt2191537572.h"
#include "UnityEngine_Analytics_UnityEngine_Analytics_Analyt1068911718.h"
#include "UnityEngine_Analytics_UnityEngine_Analytics_Tracka1304606600.h"
#include "UnityEngine_Analytics_UnityEngine_Analytics_Tracka2256174789.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharp_AccelBrain_BeatController2386125758.h"
#include "AssemblyU2DCSharp_AccelBrain_BinauralBeatControlle3677369972.h"
#include "AssemblyU2DCSharp_AccelBrain_BinauralBeat4195950836.h"
#include "AssemblyU2DCSharp_AccelBrain_BrainBeat95439410.h"
#include "AssemblyU2DCSharp_AccelBrain_MonauralBeat928071059.h"
#include "AssemblyU2DCSharp_AccelBrain_MonauralBeatControlle1412419075.h"
#include "AssemblyU2DCSharp_AWSSDK_Examples_CognitoSyncManag1531589693.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects1254291669.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects3706046914.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects3259153605.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects2543710313.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects4077469257.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects1274006168.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects3629569411.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects1989801646.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects3790432936.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects_496834990.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects3850268328.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects_681392339.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects2913820361.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects_579860294.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects2174076389.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects2318278682.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects3313275655.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects2905677972.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects2925629392.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects3591885494.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects3964716834.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects3523901841.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects3694918262.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects2385315007.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects1672908095.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects_163796146.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects3133742431.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects3095868303.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects3961919654.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects2459464934.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects3059663215.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects3904103385.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects3902263335.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects_457390305.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects1921949650.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects1282715729.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects_785857862.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects_859433922.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects_863619287.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects3948012467.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects4036041399.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects_517806655.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects2907318477.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects1887427701.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects2899312312.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects3375869057.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects2152133263.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects1845967802.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects_761286994.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects3972324734.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects_962057283.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects3563669597.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects2806915419.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects3465217523.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects3901407784.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects2916142869.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects1836015611.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects4146040027.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects3401316463.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects_430511954.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects2215595694.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects1171761296.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects3310062628.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects1008153775.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects1046072227.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects1381705816.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects3322560050.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects3949418959.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects4170634026.h"
#include "AssemblyU2DCSharp_BHSceneManager812796361.h"
#include "AssemblyU2DCSharp_BeardRotator1832164839.h"
#include "AssemblyU2DCSharp_BeardRotator_State2595507119.h"
#include "AssemblyU2DCSharp_BobRandomiser2069133119.h"
#include "AssemblyU2DCSharp_GlobalControl733410152.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3000 = { sizeof (ReflectionMethodsCache_t3343836395), -1, sizeof(ReflectionMethodsCache_t3343836395_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3000[5] = 
{
	ReflectionMethodsCache_t3343836395::get_offset_of_raycast3D_0(),
	ReflectionMethodsCache_t3343836395::get_offset_of_raycast3DAll_1(),
	ReflectionMethodsCache_t3343836395::get_offset_of_raycast2D_2(),
	ReflectionMethodsCache_t3343836395::get_offset_of_getRayIntersectionAll_3(),
	ReflectionMethodsCache_t3343836395_StaticFields::get_offset_of_s_ReflectionMethodsCache_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3001 = { sizeof (Raycast3DCallback_t3928470916), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3002 = { sizeof (Raycast2DCallback_t2260664863), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3003 = { sizeof (RaycastAllCallback_t3435657708), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3004 = { sizeof (GetRayIntersectionAllCallback_t2213949596), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3005 = { sizeof (VertexHelper_t385374196), -1, sizeof(VertexHelper_t385374196_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3005[9] = 
{
	VertexHelper_t385374196::get_offset_of_m_Positions_0(),
	VertexHelper_t385374196::get_offset_of_m_Colors_1(),
	VertexHelper_t385374196::get_offset_of_m_Uv0S_2(),
	VertexHelper_t385374196::get_offset_of_m_Uv1S_3(),
	VertexHelper_t385374196::get_offset_of_m_Normals_4(),
	VertexHelper_t385374196::get_offset_of_m_Tangents_5(),
	VertexHelper_t385374196::get_offset_of_m_Indices_6(),
	VertexHelper_t385374196_StaticFields::get_offset_of_s_DefaultTangent_7(),
	VertexHelper_t385374196_StaticFields::get_offset_of_s_DefaultNormal_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3006 = { sizeof (BaseMeshEffect_t1728560551), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3006[1] = 
{
	BaseMeshEffect_t1728560551::get_offset_of_m_Graphic_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3007 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3008 = { sizeof (Outline_t1417504278), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3009 = { sizeof (PositionAsUV1_t1102546563), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3010 = { sizeof (Shadow_t4269599528), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3010[4] = 
{
	Shadow_t4269599528::get_offset_of_m_EffectColor_3(),
	Shadow_t4269599528::get_offset_of_m_EffectDistance_4(),
	Shadow_t4269599528::get_offset_of_m_UseGraphicAlpha_5(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3011 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305146), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3011[1] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields::get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3012 = { sizeof (U24ArrayTypeU3D12_t1568637717)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D12_t1568637717 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3013 = { sizeof (U3CModuleU3E_t3783534230), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3014 = { sizeof (AnalyticsTracker_t2191537572), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3014[5] = 
{
	AnalyticsTracker_t2191537572::get_offset_of_m_EventName_2(),
	AnalyticsTracker_t2191537572::get_offset_of_m_Dict_3(),
	AnalyticsTracker_t2191537572::get_offset_of_m_PrevDictHash_4(),
	AnalyticsTracker_t2191537572::get_offset_of_m_TrackableProperty_5(),
	AnalyticsTracker_t2191537572::get_offset_of_m_Trigger_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3015 = { sizeof (Trigger_t1068911718)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3015[8] = 
{
	Trigger_t1068911718::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3016 = { sizeof (TrackableProperty_t1304606600), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3016[2] = 
{
	0,
	TrackableProperty_t1304606600::get_offset_of_m_Fields_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3017 = { sizeof (FieldWithTarget_t2256174789), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3017[6] = 
{
	FieldWithTarget_t2256174789::get_offset_of_m_ParamName_0(),
	FieldWithTarget_t2256174789::get_offset_of_m_Target_1(),
	FieldWithTarget_t2256174789::get_offset_of_m_FieldPath_2(),
	FieldWithTarget_t2256174789::get_offset_of_m_TypeString_3(),
	FieldWithTarget_t2256174789::get_offset_of_m_DoStatic_4(),
	FieldWithTarget_t2256174789::get_offset_of_m_StaticString_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3018 = { sizeof (U3CModuleU3E_t3783534231), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3019 = { sizeof (BeatController_t2386125758), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3019[4] = 
{
	BeatController_t2386125758::get_offset_of_LeftFrequency_2(),
	BeatController_t2386125758::get_offset_of_RightFrequency_3(),
	BeatController_t2386125758::get_offset_of_Gain_4(),
	BeatController_t2386125758::get_offset_of_SampleRate_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3020 = { sizeof (BinauralBeatController_t3677369972), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3020[1] = 
{
	BinauralBeatController_t3677369972::get_offset_of_U3C_brainBeatU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3021 = { sizeof (BinauralBeat_t4195950836), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3022 = { sizeof (BrainBeat_t95439410), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3022[9] = 
{
	BrainBeat_t95439410::get_offset_of_U3CLeftFrequencyU3Ek__BackingField_0(),
	BrainBeat_t95439410::get_offset_of_U3CRightFrequencyU3Ek__BackingField_1(),
	BrainBeat_t95439410::get_offset_of_U3CGainU3Ek__BackingField_2(),
	BrainBeat_t95439410::get_offset_of__SampleRate_3(),
	BrainBeat_t95439410::get_offset_of_LeftIncrement_4(),
	BrainBeat_t95439410::get_offset_of_RightIncrement_5(),
	BrainBeat_t95439410::get_offset_of_LeftPhase_6(),
	BrainBeat_t95439410::get_offset_of_RightPhase_7(),
	BrainBeat_t95439410::get_offset_of__PlayFlag_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3023 = { sizeof (MonauralBeat_t928071059), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3024 = { sizeof (MonauralBeatController_t1412419075), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3024[1] = 
{
	MonauralBeatController_t1412419075::get_offset_of_U3C_brainBeatU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3025 = { sizeof (CognitoSyncManagerSample_t1531589693), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3025[8] = 
{
	CognitoSyncManagerSample_t1531589693::get_offset_of_playerInfo_2(),
	CognitoSyncManagerSample_t1531589693::get_offset_of_playerName_3(),
	CognitoSyncManagerSample_t1531589693::get_offset_of_alias_4(),
	CognitoSyncManagerSample_t1531589693::get_offset_of_statusMessage_5(),
	CognitoSyncManagerSample_t1531589693::get_offset_of_IdentityPoolId_6(),
	CognitoSyncManagerSample_t1531589693::get_offset_of_Region_7(),
	CognitoSyncManagerSample_t1531589693::get_offset_of__credentials_8(),
	CognitoSyncManagerSample_t1531589693::get_offset_of__syncManager_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3026 = { sizeof (AAMode_t1254291669)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3026[8] = 
{
	AAMode_t1254291669::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3027 = { sizeof (Antialiasing_t3706046914), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3027[22] = 
{
	Antialiasing_t3706046914::get_offset_of_mode_5(),
	Antialiasing_t3706046914::get_offset_of_showGeneratedNormals_6(),
	Antialiasing_t3706046914::get_offset_of_offsetScale_7(),
	Antialiasing_t3706046914::get_offset_of_blurRadius_8(),
	Antialiasing_t3706046914::get_offset_of_edgeThresholdMin_9(),
	Antialiasing_t3706046914::get_offset_of_edgeThreshold_10(),
	Antialiasing_t3706046914::get_offset_of_edgeSharpness_11(),
	Antialiasing_t3706046914::get_offset_of_dlaaSharp_12(),
	Antialiasing_t3706046914::get_offset_of_ssaaShader_13(),
	Antialiasing_t3706046914::get_offset_of_ssaa_14(),
	Antialiasing_t3706046914::get_offset_of_dlaaShader_15(),
	Antialiasing_t3706046914::get_offset_of_dlaa_16(),
	Antialiasing_t3706046914::get_offset_of_nfaaShader_17(),
	Antialiasing_t3706046914::get_offset_of_nfaa_18(),
	Antialiasing_t3706046914::get_offset_of_shaderFXAAPreset2_19(),
	Antialiasing_t3706046914::get_offset_of_materialFXAAPreset2_20(),
	Antialiasing_t3706046914::get_offset_of_shaderFXAAPreset3_21(),
	Antialiasing_t3706046914::get_offset_of_materialFXAAPreset3_22(),
	Antialiasing_t3706046914::get_offset_of_shaderFXAAII_23(),
	Antialiasing_t3706046914::get_offset_of_materialFXAAII_24(),
	Antialiasing_t3706046914::get_offset_of_shaderFXAAIII_25(),
	Antialiasing_t3706046914::get_offset_of_materialFXAAIII_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3028 = { sizeof (Bloom_t3259153605), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3028[30] = 
{
	Bloom_t3259153605::get_offset_of_tweakMode_5(),
	Bloom_t3259153605::get_offset_of_screenBlendMode_6(),
	Bloom_t3259153605::get_offset_of_hdr_7(),
	Bloom_t3259153605::get_offset_of_doHdr_8(),
	Bloom_t3259153605::get_offset_of_sepBlurSpread_9(),
	Bloom_t3259153605::get_offset_of_quality_10(),
	Bloom_t3259153605::get_offset_of_bloomIntensity_11(),
	Bloom_t3259153605::get_offset_of_bloomThreshold_12(),
	Bloom_t3259153605::get_offset_of_bloomThresholdColor_13(),
	Bloom_t3259153605::get_offset_of_bloomBlurIterations_14(),
	Bloom_t3259153605::get_offset_of_hollywoodFlareBlurIterations_15(),
	Bloom_t3259153605::get_offset_of_flareRotation_16(),
	Bloom_t3259153605::get_offset_of_lensflareMode_17(),
	Bloom_t3259153605::get_offset_of_hollyStretchWidth_18(),
	Bloom_t3259153605::get_offset_of_lensflareIntensity_19(),
	Bloom_t3259153605::get_offset_of_lensflareThreshold_20(),
	Bloom_t3259153605::get_offset_of_lensFlareSaturation_21(),
	Bloom_t3259153605::get_offset_of_flareColorA_22(),
	Bloom_t3259153605::get_offset_of_flareColorB_23(),
	Bloom_t3259153605::get_offset_of_flareColorC_24(),
	Bloom_t3259153605::get_offset_of_flareColorD_25(),
	Bloom_t3259153605::get_offset_of_lensFlareVignetteMask_26(),
	Bloom_t3259153605::get_offset_of_lensFlareShader_27(),
	Bloom_t3259153605::get_offset_of_lensFlareMaterial_28(),
	Bloom_t3259153605::get_offset_of_screenBlendShader_29(),
	Bloom_t3259153605::get_offset_of_screenBlend_30(),
	Bloom_t3259153605::get_offset_of_blurAndFlaresShader_31(),
	Bloom_t3259153605::get_offset_of_blurAndFlaresMaterial_32(),
	Bloom_t3259153605::get_offset_of_brightPassFilterShader_33(),
	Bloom_t3259153605::get_offset_of_brightPassFilterMaterial_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3029 = { sizeof (LensFlareStyle_t2543710313)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3029[4] = 
{
	LensFlareStyle_t2543710313::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3030 = { sizeof (TweakMode_t4077469257)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3030[3] = 
{
	TweakMode_t4077469257::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3031 = { sizeof (HDRBloomMode_t1274006168)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3031[4] = 
{
	HDRBloomMode_t1274006168::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3032 = { sizeof (BloomScreenBlendMode_t3629569411)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3032[3] = 
{
	BloomScreenBlendMode_t3629569411::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3033 = { sizeof (BloomQuality_t1989801646)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3033[3] = 
{
	BloomQuality_t1989801646::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3034 = { sizeof (LensflareStyle34_t3790432936)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3034[4] = 
{
	LensflareStyle34_t3790432936::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3035 = { sizeof (TweakMode34_t496834990)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3035[3] = 
{
	TweakMode34_t496834990::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3036 = { sizeof (HDRBloomMode_t3850268328)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3036[4] = 
{
	HDRBloomMode_t3850268328::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3037 = { sizeof (BloomScreenBlendMode_t681392339)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3037[3] = 
{
	BloomScreenBlendMode_t681392339::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3038 = { sizeof (BloomAndFlares_t2913820361), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3038[34] = 
{
	BloomAndFlares_t2913820361::get_offset_of_tweakMode_5(),
	BloomAndFlares_t2913820361::get_offset_of_screenBlendMode_6(),
	BloomAndFlares_t2913820361::get_offset_of_hdr_7(),
	BloomAndFlares_t2913820361::get_offset_of_doHdr_8(),
	BloomAndFlares_t2913820361::get_offset_of_sepBlurSpread_9(),
	BloomAndFlares_t2913820361::get_offset_of_useSrcAlphaAsMask_10(),
	BloomAndFlares_t2913820361::get_offset_of_bloomIntensity_11(),
	BloomAndFlares_t2913820361::get_offset_of_bloomThreshold_12(),
	BloomAndFlares_t2913820361::get_offset_of_bloomBlurIterations_13(),
	BloomAndFlares_t2913820361::get_offset_of_lensflares_14(),
	BloomAndFlares_t2913820361::get_offset_of_hollywoodFlareBlurIterations_15(),
	BloomAndFlares_t2913820361::get_offset_of_lensflareMode_16(),
	BloomAndFlares_t2913820361::get_offset_of_hollyStretchWidth_17(),
	BloomAndFlares_t2913820361::get_offset_of_lensflareIntensity_18(),
	BloomAndFlares_t2913820361::get_offset_of_lensflareThreshold_19(),
	BloomAndFlares_t2913820361::get_offset_of_flareColorA_20(),
	BloomAndFlares_t2913820361::get_offset_of_flareColorB_21(),
	BloomAndFlares_t2913820361::get_offset_of_flareColorC_22(),
	BloomAndFlares_t2913820361::get_offset_of_flareColorD_23(),
	BloomAndFlares_t2913820361::get_offset_of_lensFlareVignetteMask_24(),
	BloomAndFlares_t2913820361::get_offset_of_lensFlareShader_25(),
	BloomAndFlares_t2913820361::get_offset_of_lensFlareMaterial_26(),
	BloomAndFlares_t2913820361::get_offset_of_vignetteShader_27(),
	BloomAndFlares_t2913820361::get_offset_of_vignetteMaterial_28(),
	BloomAndFlares_t2913820361::get_offset_of_separableBlurShader_29(),
	BloomAndFlares_t2913820361::get_offset_of_separableBlurMaterial_30(),
	BloomAndFlares_t2913820361::get_offset_of_addBrightStuffOneOneShader_31(),
	BloomAndFlares_t2913820361::get_offset_of_addBrightStuffBlendOneOneMaterial_32(),
	BloomAndFlares_t2913820361::get_offset_of_screenBlendShader_33(),
	BloomAndFlares_t2913820361::get_offset_of_screenBlend_34(),
	BloomAndFlares_t2913820361::get_offset_of_hollywoodFlaresShader_35(),
	BloomAndFlares_t2913820361::get_offset_of_hollywoodFlaresMaterial_36(),
	BloomAndFlares_t2913820361::get_offset_of_brightPassFilterShader_37(),
	BloomAndFlares_t2913820361::get_offset_of_brightPassFilterMaterial_38(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3039 = { sizeof (BloomOptimized_t579860294), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3039[8] = 
{
	BloomOptimized_t579860294::get_offset_of_threshold_5(),
	BloomOptimized_t579860294::get_offset_of_intensity_6(),
	BloomOptimized_t579860294::get_offset_of_blurSize_7(),
	BloomOptimized_t579860294::get_offset_of_resolution_8(),
	BloomOptimized_t579860294::get_offset_of_blurIterations_9(),
	BloomOptimized_t579860294::get_offset_of_blurType_10(),
	BloomOptimized_t579860294::get_offset_of_fastBloomShader_11(),
	BloomOptimized_t579860294::get_offset_of_fastBloomMaterial_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3040 = { sizeof (Resolution_t2174076389)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3040[3] = 
{
	Resolution_t2174076389::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3041 = { sizeof (BlurType_t2318278682)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3041[3] = 
{
	BlurType_t2318278682::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3042 = { sizeof (Blur_t3313275655), -1, sizeof(Blur_t3313275655_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3042[4] = 
{
	Blur_t3313275655::get_offset_of_iterations_2(),
	Blur_t3313275655::get_offset_of_blurSpread_3(),
	Blur_t3313275655::get_offset_of_blurShader_4(),
	Blur_t3313275655_StaticFields::get_offset_of_m_Material_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3043 = { sizeof (BlurOptimized_t2905677972), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3043[6] = 
{
	BlurOptimized_t2905677972::get_offset_of_downsample_5(),
	BlurOptimized_t2905677972::get_offset_of_blurSize_6(),
	BlurOptimized_t2905677972::get_offset_of_blurIterations_7(),
	BlurOptimized_t2905677972::get_offset_of_blurType_8(),
	BlurOptimized_t2905677972::get_offset_of_blurShader_9(),
	BlurOptimized_t2905677972::get_offset_of_blurMaterial_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3044 = { sizeof (BlurType_t2925629392)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3044[3] = 
{
	BlurType_t2925629392::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3045 = { sizeof (CameraMotionBlur_t3591885494), -1, sizeof(CameraMotionBlur_t3591885494_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3045[30] = 
{
	CameraMotionBlur_t3591885494_StaticFields::get_offset_of_MAX_RADIUS_5(),
	CameraMotionBlur_t3591885494::get_offset_of_filterType_6(),
	CameraMotionBlur_t3591885494::get_offset_of_preview_7(),
	CameraMotionBlur_t3591885494::get_offset_of_previewScale_8(),
	CameraMotionBlur_t3591885494::get_offset_of_movementScale_9(),
	CameraMotionBlur_t3591885494::get_offset_of_rotationScale_10(),
	CameraMotionBlur_t3591885494::get_offset_of_maxVelocity_11(),
	CameraMotionBlur_t3591885494::get_offset_of_minVelocity_12(),
	CameraMotionBlur_t3591885494::get_offset_of_velocityScale_13(),
	CameraMotionBlur_t3591885494::get_offset_of_softZDistance_14(),
	CameraMotionBlur_t3591885494::get_offset_of_velocityDownsample_15(),
	CameraMotionBlur_t3591885494::get_offset_of_excludeLayers_16(),
	CameraMotionBlur_t3591885494::get_offset_of_tmpCam_17(),
	CameraMotionBlur_t3591885494::get_offset_of_shader_18(),
	CameraMotionBlur_t3591885494::get_offset_of_dx11MotionBlurShader_19(),
	CameraMotionBlur_t3591885494::get_offset_of_replacementClear_20(),
	CameraMotionBlur_t3591885494::get_offset_of_motionBlurMaterial_21(),
	CameraMotionBlur_t3591885494::get_offset_of_dx11MotionBlurMaterial_22(),
	CameraMotionBlur_t3591885494::get_offset_of_noiseTexture_23(),
	CameraMotionBlur_t3591885494::get_offset_of_jitter_24(),
	CameraMotionBlur_t3591885494::get_offset_of_showVelocity_25(),
	CameraMotionBlur_t3591885494::get_offset_of_showVelocityScale_26(),
	CameraMotionBlur_t3591885494::get_offset_of_currentViewProjMat_27(),
	CameraMotionBlur_t3591885494::get_offset_of_prevViewProjMat_28(),
	CameraMotionBlur_t3591885494::get_offset_of_prevFrameCount_29(),
	CameraMotionBlur_t3591885494::get_offset_of_wasActive_30(),
	CameraMotionBlur_t3591885494::get_offset_of_prevFrameForward_31(),
	CameraMotionBlur_t3591885494::get_offset_of_prevFrameUp_32(),
	CameraMotionBlur_t3591885494::get_offset_of_prevFramePos_33(),
	CameraMotionBlur_t3591885494::get_offset_of__camera_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3046 = { sizeof (MotionBlurFilter_t3964716834)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3046[6] = 
{
	MotionBlurFilter_t3964716834::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3047 = { sizeof (ColorCorrectionCurves_t3523901841), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3047[24] = 
{
	ColorCorrectionCurves_t3523901841::get_offset_of_redChannel_5(),
	ColorCorrectionCurves_t3523901841::get_offset_of_greenChannel_6(),
	ColorCorrectionCurves_t3523901841::get_offset_of_blueChannel_7(),
	ColorCorrectionCurves_t3523901841::get_offset_of_useDepthCorrection_8(),
	ColorCorrectionCurves_t3523901841::get_offset_of_zCurve_9(),
	ColorCorrectionCurves_t3523901841::get_offset_of_depthRedChannel_10(),
	ColorCorrectionCurves_t3523901841::get_offset_of_depthGreenChannel_11(),
	ColorCorrectionCurves_t3523901841::get_offset_of_depthBlueChannel_12(),
	ColorCorrectionCurves_t3523901841::get_offset_of_ccMaterial_13(),
	ColorCorrectionCurves_t3523901841::get_offset_of_ccDepthMaterial_14(),
	ColorCorrectionCurves_t3523901841::get_offset_of_selectiveCcMaterial_15(),
	ColorCorrectionCurves_t3523901841::get_offset_of_rgbChannelTex_16(),
	ColorCorrectionCurves_t3523901841::get_offset_of_rgbDepthChannelTex_17(),
	ColorCorrectionCurves_t3523901841::get_offset_of_zCurveTex_18(),
	ColorCorrectionCurves_t3523901841::get_offset_of_saturation_19(),
	ColorCorrectionCurves_t3523901841::get_offset_of_selectiveCc_20(),
	ColorCorrectionCurves_t3523901841::get_offset_of_selectiveFromColor_21(),
	ColorCorrectionCurves_t3523901841::get_offset_of_selectiveToColor_22(),
	ColorCorrectionCurves_t3523901841::get_offset_of_mode_23(),
	ColorCorrectionCurves_t3523901841::get_offset_of_updateTextures_24(),
	ColorCorrectionCurves_t3523901841::get_offset_of_colorCorrectionCurvesShader_25(),
	ColorCorrectionCurves_t3523901841::get_offset_of_simpleColorCorrectionCurvesShader_26(),
	ColorCorrectionCurves_t3523901841::get_offset_of_colorCorrectionSelectiveShader_27(),
	ColorCorrectionCurves_t3523901841::get_offset_of_updateTexturesOnStartup_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3048 = { sizeof (ColorCorrectionMode_t3694918262)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3048[3] = 
{
	ColorCorrectionMode_t3694918262::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3049 = { sizeof (ColorCorrectionLookup_t2385315007), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3049[4] = 
{
	ColorCorrectionLookup_t2385315007::get_offset_of_shader_5(),
	ColorCorrectionLookup_t2385315007::get_offset_of_material_6(),
	ColorCorrectionLookup_t2385315007::get_offset_of_converted3DLut_7(),
	ColorCorrectionLookup_t2385315007::get_offset_of_basedOnTempTex_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3050 = { sizeof (ColorCorrectionRamp_t1672908095), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3050[1] = 
{
	ColorCorrectionRamp_t1672908095::get_offset_of_textureRamp_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3051 = { sizeof (ContrastEnhance_t163796146), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3051[7] = 
{
	ContrastEnhance_t163796146::get_offset_of_intensity_5(),
	ContrastEnhance_t163796146::get_offset_of_threshold_6(),
	ContrastEnhance_t163796146::get_offset_of_separableBlurMaterial_7(),
	ContrastEnhance_t163796146::get_offset_of_contrastCompositeMaterial_8(),
	ContrastEnhance_t163796146::get_offset_of_blurSpread_9(),
	ContrastEnhance_t163796146::get_offset_of_separableBlurShader_10(),
	ContrastEnhance_t163796146::get_offset_of_contrastCompositeShader_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3052 = { sizeof (ContrastStretch_t3133742431), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3052[13] = 
{
	ContrastStretch_t3133742431::get_offset_of_adaptationSpeed_2(),
	ContrastStretch_t3133742431::get_offset_of_limitMinimum_3(),
	ContrastStretch_t3133742431::get_offset_of_limitMaximum_4(),
	ContrastStretch_t3133742431::get_offset_of_adaptRenderTex_5(),
	ContrastStretch_t3133742431::get_offset_of_curAdaptIndex_6(),
	ContrastStretch_t3133742431::get_offset_of_shaderLum_7(),
	ContrastStretch_t3133742431::get_offset_of_m_materialLum_8(),
	ContrastStretch_t3133742431::get_offset_of_shaderReduce_9(),
	ContrastStretch_t3133742431::get_offset_of_m_materialReduce_10(),
	ContrastStretch_t3133742431::get_offset_of_shaderAdapt_11(),
	ContrastStretch_t3133742431::get_offset_of_m_materialAdapt_12(),
	ContrastStretch_t3133742431::get_offset_of_shaderApply_13(),
	ContrastStretch_t3133742431::get_offset_of_m_materialApply_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3053 = { sizeof (CreaseShading_t3095868303), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3053[9] = 
{
	CreaseShading_t3095868303::get_offset_of_intensity_5(),
	CreaseShading_t3095868303::get_offset_of_softness_6(),
	CreaseShading_t3095868303::get_offset_of_spread_7(),
	CreaseShading_t3095868303::get_offset_of_blurShader_8(),
	CreaseShading_t3095868303::get_offset_of_blurMaterial_9(),
	CreaseShading_t3095868303::get_offset_of_depthFetchShader_10(),
	CreaseShading_t3095868303::get_offset_of_depthFetchMaterial_11(),
	CreaseShading_t3095868303::get_offset_of_creaseApplyShader_12(),
	CreaseShading_t3095868303::get_offset_of_creaseApplyMaterial_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3054 = { sizeof (DepthOfField_t3961919654), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3054[25] = 
{
	DepthOfField_t3961919654::get_offset_of_visualizeFocus_5(),
	DepthOfField_t3961919654::get_offset_of_focalLength_6(),
	DepthOfField_t3961919654::get_offset_of_focalSize_7(),
	DepthOfField_t3961919654::get_offset_of_aperture_8(),
	DepthOfField_t3961919654::get_offset_of_focalTransform_9(),
	DepthOfField_t3961919654::get_offset_of_maxBlurSize_10(),
	DepthOfField_t3961919654::get_offset_of_highResolution_11(),
	DepthOfField_t3961919654::get_offset_of_blurType_12(),
	DepthOfField_t3961919654::get_offset_of_blurSampleCount_13(),
	DepthOfField_t3961919654::get_offset_of_nearBlur_14(),
	DepthOfField_t3961919654::get_offset_of_foregroundOverlap_15(),
	DepthOfField_t3961919654::get_offset_of_dofHdrShader_16(),
	DepthOfField_t3961919654::get_offset_of_dofHdrMaterial_17(),
	DepthOfField_t3961919654::get_offset_of_dx11BokehShader_18(),
	DepthOfField_t3961919654::get_offset_of_dx11bokehMaterial_19(),
	DepthOfField_t3961919654::get_offset_of_dx11BokehThreshold_20(),
	DepthOfField_t3961919654::get_offset_of_dx11SpawnHeuristic_21(),
	DepthOfField_t3961919654::get_offset_of_dx11BokehTexture_22(),
	DepthOfField_t3961919654::get_offset_of_dx11BokehScale_23(),
	DepthOfField_t3961919654::get_offset_of_dx11BokehIntensity_24(),
	DepthOfField_t3961919654::get_offset_of_focalDistance01_25(),
	DepthOfField_t3961919654::get_offset_of_cbDrawArgs_26(),
	DepthOfField_t3961919654::get_offset_of_cbPoints_27(),
	DepthOfField_t3961919654::get_offset_of_internalBlurWidth_28(),
	DepthOfField_t3961919654::get_offset_of_cachedCamera_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3055 = { sizeof (BlurType_t2459464934)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3055[3] = 
{
	BlurType_t2459464934::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3056 = { sizeof (BlurSampleCount_t3059663215)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3056[4] = 
{
	BlurSampleCount_t3059663215::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3057 = { sizeof (DepthOfFieldDeprecated_t3904103385), -1, sizeof(DepthOfFieldDeprecated_t3904103385_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3057[43] = 
{
	DepthOfFieldDeprecated_t3904103385_StaticFields::get_offset_of_SMOOTH_DOWNSAMPLE_PASS_5(),
	DepthOfFieldDeprecated_t3904103385_StaticFields::get_offset_of_BOKEH_EXTRA_BLUR_6(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_quality_7(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_resolution_8(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_simpleTweakMode_9(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_focalPoint_10(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_smoothness_11(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_focalZDistance_12(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_focalZStartCurve_13(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_focalZEndCurve_14(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_focalStartCurve_15(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_focalEndCurve_16(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_focalDistance01_17(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_objectFocus_18(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_focalSize_19(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_bluriness_20(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_maxBlurSpread_21(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_foregroundBlurExtrude_22(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_dofBlurShader_23(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_dofBlurMaterial_24(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_dofShader_25(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_dofMaterial_26(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_visualize_27(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_bokehDestination_28(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_widthOverHeight_29(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_oneOverBaseSize_30(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_bokeh_31(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_bokehSupport_32(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_bokehShader_33(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_bokehTexture_34(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_bokehScale_35(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_bokehIntensity_36(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_bokehThresholdContrast_37(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_bokehThresholdLuminance_38(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_bokehDownsample_39(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_bokehMaterial_40(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of__camera_41(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_foregroundTexture_42(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_mediumRezWorkTexture_43(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_finalDefocus_44(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_lowRezWorkTexture_45(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_bokehSource_46(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_bokehSource2_47(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3058 = { sizeof (Dof34QualitySetting_t3902263335)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3058[3] = 
{
	Dof34QualitySetting_t3902263335::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3059 = { sizeof (DofResolution_t457390305)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3059[4] = 
{
	DofResolution_t457390305::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3060 = { sizeof (DofBlurriness_t1921949650)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3060[4] = 
{
	DofBlurriness_t1921949650::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3061 = { sizeof (BokehDestination_t1282715729)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3061[4] = 
{
	BokehDestination_t1282715729::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3062 = { sizeof (EdgeDetection_t785857862), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3062[11] = 
{
	EdgeDetection_t785857862::get_offset_of_mode_5(),
	EdgeDetection_t785857862::get_offset_of_sensitivityDepth_6(),
	EdgeDetection_t785857862::get_offset_of_sensitivityNormals_7(),
	EdgeDetection_t785857862::get_offset_of_lumThreshold_8(),
	EdgeDetection_t785857862::get_offset_of_edgeExp_9(),
	EdgeDetection_t785857862::get_offset_of_sampleDist_10(),
	EdgeDetection_t785857862::get_offset_of_edgesOnly_11(),
	EdgeDetection_t785857862::get_offset_of_edgesOnlyBgColor_12(),
	EdgeDetection_t785857862::get_offset_of_edgeDetectShader_13(),
	EdgeDetection_t785857862::get_offset_of_edgeDetectMaterial_14(),
	EdgeDetection_t785857862::get_offset_of_oldMode_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3063 = { sizeof (EdgeDetectMode_t859433922)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3063[6] = 
{
	EdgeDetectMode_t859433922::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3064 = { sizeof (Fisheye_t863619287), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3064[4] = 
{
	Fisheye_t863619287::get_offset_of_strengthX_5(),
	Fisheye_t863619287::get_offset_of_strengthY_6(),
	Fisheye_t863619287::get_offset_of_fishEyeShader_7(),
	Fisheye_t863619287::get_offset_of_fisheyeMaterial_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3065 = { sizeof (GlobalFog_t3948012467), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3065[9] = 
{
	GlobalFog_t3948012467::get_offset_of_distanceFog_5(),
	GlobalFog_t3948012467::get_offset_of_excludeFarPixels_6(),
	GlobalFog_t3948012467::get_offset_of_useRadialDistance_7(),
	GlobalFog_t3948012467::get_offset_of_heightFog_8(),
	GlobalFog_t3948012467::get_offset_of_height_9(),
	GlobalFog_t3948012467::get_offset_of_heightDensity_10(),
	GlobalFog_t3948012467::get_offset_of_startDistance_11(),
	GlobalFog_t3948012467::get_offset_of_fogShader_12(),
	GlobalFog_t3948012467::get_offset_of_fogMaterial_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3066 = { sizeof (Grayscale_t4036041399), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3066[2] = 
{
	Grayscale_t4036041399::get_offset_of_textureRamp_4(),
	Grayscale_t4036041399::get_offset_of_rampOffset_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3067 = { sizeof (ImageEffectBase_t517806655), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3067[2] = 
{
	ImageEffectBase_t517806655::get_offset_of_shader_2(),
	ImageEffectBase_t517806655::get_offset_of_m_Material_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3068 = { sizeof (ImageEffects_t2907318477), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3069 = { sizeof (MotionBlur_t1887427701), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3069[3] = 
{
	MotionBlur_t1887427701::get_offset_of_blurAmount_4(),
	MotionBlur_t1887427701::get_offset_of_extraBlur_5(),
	MotionBlur_t1887427701::get_offset_of_accumTexture_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3070 = { sizeof (NoiseAndGrain_t2899312312), -1, sizeof(NoiseAndGrain_t2899312312_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3070[18] = 
{
	NoiseAndGrain_t2899312312::get_offset_of_intensityMultiplier_5(),
	NoiseAndGrain_t2899312312::get_offset_of_generalIntensity_6(),
	NoiseAndGrain_t2899312312::get_offset_of_blackIntensity_7(),
	NoiseAndGrain_t2899312312::get_offset_of_whiteIntensity_8(),
	NoiseAndGrain_t2899312312::get_offset_of_midGrey_9(),
	NoiseAndGrain_t2899312312::get_offset_of_dx11Grain_10(),
	NoiseAndGrain_t2899312312::get_offset_of_softness_11(),
	NoiseAndGrain_t2899312312::get_offset_of_monochrome_12(),
	NoiseAndGrain_t2899312312::get_offset_of_intensities_13(),
	NoiseAndGrain_t2899312312::get_offset_of_tiling_14(),
	NoiseAndGrain_t2899312312::get_offset_of_monochromeTiling_15(),
	NoiseAndGrain_t2899312312::get_offset_of_filterMode_16(),
	NoiseAndGrain_t2899312312::get_offset_of_noiseTexture_17(),
	NoiseAndGrain_t2899312312::get_offset_of_noiseShader_18(),
	NoiseAndGrain_t2899312312::get_offset_of_noiseMaterial_19(),
	NoiseAndGrain_t2899312312::get_offset_of_dx11NoiseShader_20(),
	NoiseAndGrain_t2899312312::get_offset_of_dx11NoiseMaterial_21(),
	NoiseAndGrain_t2899312312_StaticFields::get_offset_of_TILE_AMOUNT_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3071 = { sizeof (NoiseAndScratches_t3375869057), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3071[18] = 
{
	NoiseAndScratches_t3375869057::get_offset_of_monochrome_2(),
	NoiseAndScratches_t3375869057::get_offset_of_rgbFallback_3(),
	NoiseAndScratches_t3375869057::get_offset_of_grainIntensityMin_4(),
	NoiseAndScratches_t3375869057::get_offset_of_grainIntensityMax_5(),
	NoiseAndScratches_t3375869057::get_offset_of_grainSize_6(),
	NoiseAndScratches_t3375869057::get_offset_of_scratchIntensityMin_7(),
	NoiseAndScratches_t3375869057::get_offset_of_scratchIntensityMax_8(),
	NoiseAndScratches_t3375869057::get_offset_of_scratchFPS_9(),
	NoiseAndScratches_t3375869057::get_offset_of_scratchJitter_10(),
	NoiseAndScratches_t3375869057::get_offset_of_grainTexture_11(),
	NoiseAndScratches_t3375869057::get_offset_of_scratchTexture_12(),
	NoiseAndScratches_t3375869057::get_offset_of_shaderRGB_13(),
	NoiseAndScratches_t3375869057::get_offset_of_shaderYUV_14(),
	NoiseAndScratches_t3375869057::get_offset_of_m_MaterialRGB_15(),
	NoiseAndScratches_t3375869057::get_offset_of_m_MaterialYUV_16(),
	NoiseAndScratches_t3375869057::get_offset_of_scratchTimeLeft_17(),
	NoiseAndScratches_t3375869057::get_offset_of_scratchX_18(),
	NoiseAndScratches_t3375869057::get_offset_of_scratchY_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3072 = { sizeof (PostEffectsBase_t2152133263), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3072[3] = 
{
	PostEffectsBase_t2152133263::get_offset_of_supportHDRTextures_2(),
	PostEffectsBase_t2152133263::get_offset_of_supportDX11_3(),
	PostEffectsBase_t2152133263::get_offset_of_isSupported_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3073 = { sizeof (PostEffectsHelper_t1845967802), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3074 = { sizeof (Quads_t761286994), -1, sizeof(Quads_t761286994_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3074[2] = 
{
	Quads_t761286994_StaticFields::get_offset_of_meshes_0(),
	Quads_t761286994_StaticFields::get_offset_of_currentQuads_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3075 = { sizeof (ScreenOverlay_t3972324734), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3075[5] = 
{
	ScreenOverlay_t3972324734::get_offset_of_blendMode_5(),
	ScreenOverlay_t3972324734::get_offset_of_intensity_6(),
	ScreenOverlay_t3972324734::get_offset_of_texture_7(),
	ScreenOverlay_t3972324734::get_offset_of_overlayShader_8(),
	ScreenOverlay_t3972324734::get_offset_of_overlayMaterial_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3076 = { sizeof (OverlayBlendMode_t962057283)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3076[6] = 
{
	OverlayBlendMode_t962057283::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3077 = { sizeof (ScreenSpaceAmbientObscurance_t3563669597), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3077[8] = 
{
	ScreenSpaceAmbientObscurance_t3563669597::get_offset_of_intensity_5(),
	ScreenSpaceAmbientObscurance_t3563669597::get_offset_of_radius_6(),
	ScreenSpaceAmbientObscurance_t3563669597::get_offset_of_blurIterations_7(),
	ScreenSpaceAmbientObscurance_t3563669597::get_offset_of_blurFilterDistance_8(),
	ScreenSpaceAmbientObscurance_t3563669597::get_offset_of_downsample_9(),
	ScreenSpaceAmbientObscurance_t3563669597::get_offset_of_rand_10(),
	ScreenSpaceAmbientObscurance_t3563669597::get_offset_of_aoShader_11(),
	ScreenSpaceAmbientObscurance_t3563669597::get_offset_of_aoMaterial_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3078 = { sizeof (ScreenSpaceAmbientOcclusion_t2806915419), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3078[11] = 
{
	ScreenSpaceAmbientOcclusion_t2806915419::get_offset_of_m_Radius_2(),
	ScreenSpaceAmbientOcclusion_t2806915419::get_offset_of_m_SampleCount_3(),
	ScreenSpaceAmbientOcclusion_t2806915419::get_offset_of_m_OcclusionIntensity_4(),
	ScreenSpaceAmbientOcclusion_t2806915419::get_offset_of_m_Blur_5(),
	ScreenSpaceAmbientOcclusion_t2806915419::get_offset_of_m_Downsampling_6(),
	ScreenSpaceAmbientOcclusion_t2806915419::get_offset_of_m_OcclusionAttenuation_7(),
	ScreenSpaceAmbientOcclusion_t2806915419::get_offset_of_m_MinZ_8(),
	ScreenSpaceAmbientOcclusion_t2806915419::get_offset_of_m_SSAOShader_9(),
	ScreenSpaceAmbientOcclusion_t2806915419::get_offset_of_m_SSAOMaterial_10(),
	ScreenSpaceAmbientOcclusion_t2806915419::get_offset_of_m_RandomTexture_11(),
	ScreenSpaceAmbientOcclusion_t2806915419::get_offset_of_m_Supported_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3079 = { sizeof (SSAOSamples_t3465217523)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3079[4] = 
{
	SSAOSamples_t3465217523::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3080 = { sizeof (SepiaTone_t3901407784), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3081 = { sizeof (SunShafts_t2916142869), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3081[14] = 
{
	SunShafts_t2916142869::get_offset_of_resolution_5(),
	SunShafts_t2916142869::get_offset_of_screenBlendMode_6(),
	SunShafts_t2916142869::get_offset_of_sunTransform_7(),
	SunShafts_t2916142869::get_offset_of_radialBlurIterations_8(),
	SunShafts_t2916142869::get_offset_of_sunColor_9(),
	SunShafts_t2916142869::get_offset_of_sunThreshold_10(),
	SunShafts_t2916142869::get_offset_of_sunShaftBlurRadius_11(),
	SunShafts_t2916142869::get_offset_of_sunShaftIntensity_12(),
	SunShafts_t2916142869::get_offset_of_maxRadius_13(),
	SunShafts_t2916142869::get_offset_of_useDepthTexture_14(),
	SunShafts_t2916142869::get_offset_of_sunShaftsShader_15(),
	SunShafts_t2916142869::get_offset_of_sunShaftsMaterial_16(),
	SunShafts_t2916142869::get_offset_of_simpleClearShader_17(),
	SunShafts_t2916142869::get_offset_of_simpleClearMaterial_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3082 = { sizeof (SunShaftsResolution_t1836015611)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3082[4] = 
{
	SunShaftsResolution_t1836015611::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3083 = { sizeof (ShaftsScreenBlendMode_t4146040027)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3083[3] = 
{
	ShaftsScreenBlendMode_t4146040027::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3084 = { sizeof (TiltShift_t3401316463), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3084[7] = 
{
	TiltShift_t3401316463::get_offset_of_mode_5(),
	TiltShift_t3401316463::get_offset_of_quality_6(),
	TiltShift_t3401316463::get_offset_of_blurArea_7(),
	TiltShift_t3401316463::get_offset_of_maxBlurSize_8(),
	TiltShift_t3401316463::get_offset_of_downsample_9(),
	TiltShift_t3401316463::get_offset_of_tiltShiftShader_10(),
	TiltShift_t3401316463::get_offset_of_tiltShiftMaterial_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3085 = { sizeof (TiltShiftMode_t430511954)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3085[3] = 
{
	TiltShiftMode_t430511954::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3086 = { sizeof (TiltShiftQuality_t2215595694)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3086[4] = 
{
	TiltShiftQuality_t2215595694::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3087 = { sizeof (Tonemapping_t1171761296), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3087[13] = 
{
	Tonemapping_t1171761296::get_offset_of_type_5(),
	Tonemapping_t1171761296::get_offset_of_adaptiveTextureSize_6(),
	Tonemapping_t1171761296::get_offset_of_remapCurve_7(),
	Tonemapping_t1171761296::get_offset_of_curveTex_8(),
	Tonemapping_t1171761296::get_offset_of_exposureAdjustment_9(),
	Tonemapping_t1171761296::get_offset_of_middleGrey_10(),
	Tonemapping_t1171761296::get_offset_of_white_11(),
	Tonemapping_t1171761296::get_offset_of_adaptionSpeed_12(),
	Tonemapping_t1171761296::get_offset_of_tonemapper_13(),
	Tonemapping_t1171761296::get_offset_of_validRenderTextureFormat_14(),
	Tonemapping_t1171761296::get_offset_of_tonemapMaterial_15(),
	Tonemapping_t1171761296::get_offset_of_rt_16(),
	Tonemapping_t1171761296::get_offset_of_rtFormat_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3088 = { sizeof (TonemapperType_t3310062628)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3088[8] = 
{
	TonemapperType_t3310062628::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3089 = { sizeof (AdaptiveTexSize_t1008153775)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3089[8] = 
{
	AdaptiveTexSize_t1008153775::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3090 = { sizeof (Triangles_t1046072227), -1, sizeof(Triangles_t1046072227_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3090[2] = 
{
	Triangles_t1046072227_StaticFields::get_offset_of_meshes_0(),
	Triangles_t1046072227_StaticFields::get_offset_of_currentTris_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3091 = { sizeof (Twirl_t1381705816), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3091[3] = 
{
	Twirl_t1381705816::get_offset_of_radius_4(),
	Twirl_t1381705816::get_offset_of_angle_5(),
	Twirl_t1381705816::get_offset_of_center_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3092 = { sizeof (VignetteAndChromaticAberration_t3322560050), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3092[14] = 
{
	VignetteAndChromaticAberration_t3322560050::get_offset_of_mode_5(),
	VignetteAndChromaticAberration_t3322560050::get_offset_of_intensity_6(),
	VignetteAndChromaticAberration_t3322560050::get_offset_of_chromaticAberration_7(),
	VignetteAndChromaticAberration_t3322560050::get_offset_of_axialAberration_8(),
	VignetteAndChromaticAberration_t3322560050::get_offset_of_blur_9(),
	VignetteAndChromaticAberration_t3322560050::get_offset_of_blurSpread_10(),
	VignetteAndChromaticAberration_t3322560050::get_offset_of_luminanceDependency_11(),
	VignetteAndChromaticAberration_t3322560050::get_offset_of_blurDistance_12(),
	VignetteAndChromaticAberration_t3322560050::get_offset_of_vignetteShader_13(),
	VignetteAndChromaticAberration_t3322560050::get_offset_of_separableBlurShader_14(),
	VignetteAndChromaticAberration_t3322560050::get_offset_of_chromAberrationShader_15(),
	VignetteAndChromaticAberration_t3322560050::get_offset_of_m_VignetteMaterial_16(),
	VignetteAndChromaticAberration_t3322560050::get_offset_of_m_SeparableBlurMaterial_17(),
	VignetteAndChromaticAberration_t3322560050::get_offset_of_m_ChromAberrationMaterial_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3093 = { sizeof (AberrationMode_t3949418959)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3093[3] = 
{
	AberrationMode_t3949418959::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3094 = { sizeof (Vortex_t4170634026), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3094[3] = 
{
	Vortex_t4170634026::get_offset_of_radius_4(),
	Vortex_t4170634026::get_offset_of_angle_5(),
	Vortex_t4170634026::get_offset_of_center_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3095 = { sizeof (BHSceneManager_t812796361), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3095[1] = 
{
	BHSceneManager_t812796361::get_offset_of_SceneName_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3096 = { sizeof (BeardRotator_t1832164839), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3096[3] = 
{
	BeardRotator_t1832164839::get_offset_of_RotationSpeed_2(),
	BeardRotator_t1832164839::get_offset_of_Title_3(),
	BeardRotator_t1832164839::get_offset_of_m_BeardState_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3097 = { sizeof (State_t2595507119)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3097[4] = 
{
	State_t2595507119::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3098 = { sizeof (BobRandomiser_t2069133119), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3099 = { sizeof (GlobalControl_t733410152), -1, sizeof(GlobalControl_t733410152_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3099[2] = 
{
	GlobalControl_t733410152_StaticFields::get_offset_of_Instance_2(),
	GlobalControl_t733410152::get_offset_of_TestString_3(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
