﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Specialized.StringDictionary
struct StringDictionary_t1070889667;
// System.String
struct String_t;

#include "mscorlib_System_MarshalByRefObject1285298191.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.TraceListener
struct  TraceListener_t3414949279  : public MarshalByRefObject_t1285298191
{
public:
	// System.Int32 System.Diagnostics.TraceListener::indentSize
	int32_t ___indentSize_1;
	// System.Collections.Specialized.StringDictionary System.Diagnostics.TraceListener::attributes
	StringDictionary_t1070889667 * ___attributes_2;
	// System.String System.Diagnostics.TraceListener::name
	String_t* ___name_3;
	// System.Boolean System.Diagnostics.TraceListener::needIndent
	bool ___needIndent_4;

public:
	inline static int32_t get_offset_of_indentSize_1() { return static_cast<int32_t>(offsetof(TraceListener_t3414949279, ___indentSize_1)); }
	inline int32_t get_indentSize_1() const { return ___indentSize_1; }
	inline int32_t* get_address_of_indentSize_1() { return &___indentSize_1; }
	inline void set_indentSize_1(int32_t value)
	{
		___indentSize_1 = value;
	}

	inline static int32_t get_offset_of_attributes_2() { return static_cast<int32_t>(offsetof(TraceListener_t3414949279, ___attributes_2)); }
	inline StringDictionary_t1070889667 * get_attributes_2() const { return ___attributes_2; }
	inline StringDictionary_t1070889667 ** get_address_of_attributes_2() { return &___attributes_2; }
	inline void set_attributes_2(StringDictionary_t1070889667 * value)
	{
		___attributes_2 = value;
		Il2CppCodeGenWriteBarrier(&___attributes_2, value);
	}

	inline static int32_t get_offset_of_name_3() { return static_cast<int32_t>(offsetof(TraceListener_t3414949279, ___name_3)); }
	inline String_t* get_name_3() const { return ___name_3; }
	inline String_t** get_address_of_name_3() { return &___name_3; }
	inline void set_name_3(String_t* value)
	{
		___name_3 = value;
		Il2CppCodeGenWriteBarrier(&___name_3, value);
	}

	inline static int32_t get_offset_of_needIndent_4() { return static_cast<int32_t>(offsetof(TraceListener_t3414949279, ___needIndent_4)); }
	inline bool get_needIndent_4() const { return ___needIndent_4; }
	inline bool* get_address_of_needIndent_4() { return &___needIndent_4; }
	inline void set_needIndent_4(bool value)
	{
		___needIndent_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
