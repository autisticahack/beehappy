﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Util.AWSSDKUtils/<>c
struct U3CU3Ec_t3667845516;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Amazon.Util.AWSSDKUtils/<>c::.cctor()
extern "C"  void U3CU3Ec__cctor_m2016912490 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Util.AWSSDKUtils/<>c::.ctor()
extern "C"  void U3CU3Ec__ctor_m2095463945 (U3CU3Ec_t3667845516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Util.AWSSDKUtils/<>c::<CanonicalizeResourcePath>b__28_0(System.String)
extern "C"  String_t* U3CU3Ec_U3CCanonicalizeResourcePathU3Eb__28_0_m3331287108 (U3CU3Ec_t3667845516 * __this, String_t* ___segment0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
