﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.Linq.XAttribute
struct XAttribute_t3858477518;
// System.Xml.Linq.XName
struct XName_t785190363;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_Linq_System_Xml_Linq_XAttribute3858477518.h"
#include "System_Xml_Linq_System_Xml_Linq_XName785190363.h"
#include "mscorlib_System_Object2689449295.h"
#include "System_Xml_System_Xml_XmlNodeType739504597.h"
#include "mscorlib_System_String2029220233.h"

// System.Void System.Xml.Linq.XAttribute::.ctor(System.Xml.Linq.XAttribute)
extern "C"  void XAttribute__ctor_m2559189009 (XAttribute_t3858477518 * __this, XAttribute_t3858477518 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Linq.XAttribute::.ctor(System.Xml.Linq.XName,System.Object)
extern "C"  void XAttribute__ctor_m2350776210 (XAttribute_t3858477518 * __this, XName_t785190363 * ___name0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Linq.XAttribute::.cctor()
extern "C"  void XAttribute__cctor_m2493520020 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.Linq.XAttribute::get_IsNamespaceDeclaration()
extern "C"  bool XAttribute_get_IsNamespaceDeclaration_m86030665 (XAttribute_t3858477518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Linq.XName System.Xml.Linq.XAttribute::get_Name()
extern "C"  XName_t785190363 * XAttribute_get_Name_m2127497767 (XAttribute_t3858477518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Linq.XAttribute System.Xml.Linq.XAttribute::get_NextAttribute()
extern "C"  XAttribute_t3858477518 * XAttribute_get_NextAttribute_m2269700464 (XAttribute_t3858477518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Linq.XAttribute::set_NextAttribute(System.Xml.Linq.XAttribute)
extern "C"  void XAttribute_set_NextAttribute_m2644290891 (XAttribute_t3858477518 * __this, XAttribute_t3858477518 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlNodeType System.Xml.Linq.XAttribute::get_NodeType()
extern "C"  int32_t XAttribute_get_NodeType_m1732519816 (XAttribute_t3858477518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Linq.XAttribute::set_PreviousAttribute(System.Xml.Linq.XAttribute)
extern "C"  void XAttribute_set_PreviousAttribute_m2159252545 (XAttribute_t3858477518 * __this, XAttribute_t3858477518 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.Linq.XAttribute::get_Value()
extern "C"  String_t* XAttribute_get_Value_m3732914648 (XAttribute_t3858477518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Linq.XAttribute::set_Value(System.String)
extern "C"  void XAttribute_set_Value_m4018836401 (XAttribute_t3858477518 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Linq.XAttribute::Remove()
extern "C"  void XAttribute_Remove_m4091960603 (XAttribute_t3858477518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Linq.XAttribute::SetValue(System.Object)
extern "C"  void XAttribute_SetValue_m2874773340 (XAttribute_t3858477518 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.Linq.XAttribute::ToString()
extern "C"  String_t* XAttribute_ToString_m2658312316 (XAttribute_t3858477518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
