﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.CognitoSync.Internal.CognitoCredentialsRetriever
struct CognitoCredentialsRetriever_t1718611005;
// Amazon.Runtime.AWSCredentials
struct AWSCredentials_t3583921007;
// Amazon.Runtime.IExecutionContext
struct IExecutionContext_t2477130752;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_Runtime_AWSCredentials3583921007.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Amazon.CognitoSync.Internal.CognitoCredentialsRetriever::.ctor(Amazon.Runtime.AWSCredentials)
extern "C"  void CognitoCredentialsRetriever__ctor_m3280091399 (CognitoCredentialsRetriever_t1718611005 * __this, AWSCredentials_t3583921007 * ___credentials0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Internal.CognitoCredentialsRetriever::PreInvoke(Amazon.Runtime.IExecutionContext)
extern "C"  void CognitoCredentialsRetriever_PreInvoke_m3070986217 (CognitoCredentialsRetriever_t1718611005 * __this, Il2CppObject * ___executionContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Internal.CognitoCredentialsRetriever::SetIdentity(Amazon.Runtime.IExecutionContext,System.String,System.String)
extern "C"  void CognitoCredentialsRetriever_SetIdentity_m2384410800 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___executionContext0, String_t* ___identityId1, String_t* ___identityPoolId2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
