﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3210635913MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,ThirdParty.Json.LitJson.ArrayMetadata>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m2391914383(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t1260966386 *, Dictionary_2_t3072435911 *, const MethodInfo*))KeyCollection__ctor_m547435096_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3127006681(__this, ___item0, method) ((  void (*) (KeyCollection_t1260966386 *, Type_t *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1668870622_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3089433920(__this, method) ((  void (*) (KeyCollection_t1260966386 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2756193719_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1332611147(__this, ___item0, method) ((  bool (*) (KeyCollection_t1260966386 *, Type_t *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m833199734_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m170495108(__this, ___item0, method) ((  bool (*) (KeyCollection_t1260966386 *, Type_t *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3201040055_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3380537034(__this, method) ((  Il2CppObject* (*) (KeyCollection_t1260966386 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1066449161_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m3637842666(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1260966386 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m3169515245_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m288413989(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1260966386 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m4259404390_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2161026382(__this, method) ((  bool (*) (KeyCollection_t1260966386 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m853124483_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3261331978(__this, method) ((  bool (*) (KeyCollection_t1260966386 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1428134417_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m2710727914(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1260966386 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m2308578909_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,ThirdParty.Json.LitJson.ArrayMetadata>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m2140790664(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1260966386 *, TypeU5BU5D_t1664964607*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m4000657415_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,ThirdParty.Json.LitJson.ArrayMetadata>::GetEnumerator()
#define KeyCollection_GetEnumerator_m3562653923(__this, method) ((  Enumerator_t1466972053  (*) (KeyCollection_t1260966386 *, const MethodInfo*))KeyCollection_GetEnumerator_m1247305548_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,ThirdParty.Json.LitJson.ArrayMetadata>::get_Count()
#define KeyCollection_get_Count_m3763548802(__this, method) ((  int32_t (*) (KeyCollection_t1260966386 *, const MethodInfo*))KeyCollection_get_Count_m3260694481_gshared)(__this, method)
