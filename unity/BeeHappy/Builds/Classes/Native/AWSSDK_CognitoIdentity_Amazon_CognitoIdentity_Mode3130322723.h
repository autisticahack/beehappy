﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetOpenIdTokenResponseUnmarshaller
struct GetOpenIdTokenResponseUnmarshaller_t3130322723;

#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_Json2685947887.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetOpenIdTokenResponseUnmarshaller
struct  GetOpenIdTokenResponseUnmarshaller_t3130322723  : public JsonResponseUnmarshaller_t2685947887
{
public:

public:
};

struct GetOpenIdTokenResponseUnmarshaller_t3130322723_StaticFields
{
public:
	// Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetOpenIdTokenResponseUnmarshaller Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetOpenIdTokenResponseUnmarshaller::_instance
	GetOpenIdTokenResponseUnmarshaller_t3130322723 * ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(GetOpenIdTokenResponseUnmarshaller_t3130322723_StaticFields, ____instance_0)); }
	inline GetOpenIdTokenResponseUnmarshaller_t3130322723 * get__instance_0() const { return ____instance_0; }
	inline GetOpenIdTokenResponseUnmarshaller_t3130322723 ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(GetOpenIdTokenResponseUnmarshaller_t3130322723 * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier(&____instance_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
