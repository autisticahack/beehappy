﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.Dictionary`2<System.String,Amazon.Runtime.ConstantClass>>
struct Dictionary_2_t3557729749;
// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.ConstantClass
struct  ConstantClass_t4000559886  : public Il2CppObject
{
public:
	// System.String Amazon.Runtime.ConstantClass::<Value>k__BackingField
	String_t* ___U3CValueU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CValueU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ConstantClass_t4000559886, ___U3CValueU3Ek__BackingField_2)); }
	inline String_t* get_U3CValueU3Ek__BackingField_2() const { return ___U3CValueU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CValueU3Ek__BackingField_2() { return &___U3CValueU3Ek__BackingField_2; }
	inline void set_U3CValueU3Ek__BackingField_2(String_t* value)
	{
		___U3CValueU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CValueU3Ek__BackingField_2, value);
	}
};

struct ConstantClass_t4000559886_StaticFields
{
public:
	// System.Object Amazon.Runtime.ConstantClass::staticFieldsLock
	Il2CppObject * ___staticFieldsLock_0;
	// System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.Dictionary`2<System.String,Amazon.Runtime.ConstantClass>> Amazon.Runtime.ConstantClass::staticFields
	Dictionary_2_t3557729749 * ___staticFields_1;

public:
	inline static int32_t get_offset_of_staticFieldsLock_0() { return static_cast<int32_t>(offsetof(ConstantClass_t4000559886_StaticFields, ___staticFieldsLock_0)); }
	inline Il2CppObject * get_staticFieldsLock_0() const { return ___staticFieldsLock_0; }
	inline Il2CppObject ** get_address_of_staticFieldsLock_0() { return &___staticFieldsLock_0; }
	inline void set_staticFieldsLock_0(Il2CppObject * value)
	{
		___staticFieldsLock_0 = value;
		Il2CppCodeGenWriteBarrier(&___staticFieldsLock_0, value);
	}

	inline static int32_t get_offset_of_staticFields_1() { return static_cast<int32_t>(offsetof(ConstantClass_t4000559886_StaticFields, ___staticFields_1)); }
	inline Dictionary_2_t3557729749 * get_staticFields_1() const { return ___staticFields_1; }
	inline Dictionary_2_t3557729749 ** get_address_of_staticFields_1() { return &___staticFields_1; }
	inline void set_staticFields_1(Dictionary_2_t3557729749 * value)
	{
		___staticFields_1 = value;
		Il2CppCodeGenWriteBarrier(&___staticFields_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
