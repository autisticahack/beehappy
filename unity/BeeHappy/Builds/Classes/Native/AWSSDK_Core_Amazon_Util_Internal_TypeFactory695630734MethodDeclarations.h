﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Util.Internal.ITypeInfo
struct ITypeInfo_t3592676621;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type1303803226.h"

// Amazon.Util.Internal.ITypeInfo Amazon.Util.Internal.TypeFactory::GetTypeInfo(System.Type)
extern "C"  Il2CppObject * TypeFactory_GetTypeInfo_m2820024382 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Util.Internal.TypeFactory::.cctor()
extern "C"  void TypeFactory__cctor_m4098164655 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
