﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen3961629604MethodDeclarations.h"

// System.Void System.Func`2<System.Xml.Linq.XElement,System.Boolean>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m2939206643(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t4203202669 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m1354888807_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<System.Xml.Linq.XElement,System.Boolean>::Invoke(T)
#define Func_2_Invoke_m4160045733(__this, ___arg10, method) ((  bool (*) (Func_2_t4203202669 *, XElement_t553821050 *, const MethodInfo*))Func_2_Invoke_m2968608789_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<System.Xml.Linq.XElement,System.Boolean>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m2735278456(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t4203202669 *, XElement_t553821050 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m1429757044_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<System.Xml.Linq.XElement,System.Boolean>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m1048561891(__this, ___result0, method) ((  bool (*) (Func_2_t4203202669 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m924416567_gshared)(__this, ___result0, method)
