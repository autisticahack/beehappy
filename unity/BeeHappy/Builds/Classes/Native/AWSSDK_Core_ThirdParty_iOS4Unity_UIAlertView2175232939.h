﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AWSSDK_Core_ThirdParty_iOS4Unity_NSObject1518098886.h"
#include "mscorlib_System_IntPtr2504060609.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.iOS4Unity.UIAlertView
struct  UIAlertView_t2175232939  : public NSObject_t1518098886
{
public:

public:
};

struct UIAlertView_t2175232939_StaticFields
{
public:
	// System.IntPtr ThirdParty.iOS4Unity.UIAlertView::_classHandle
	IntPtr_t ____classHandle_3;

public:
	inline static int32_t get_offset_of__classHandle_3() { return static_cast<int32_t>(offsetof(UIAlertView_t2175232939_StaticFields, ____classHandle_3)); }
	inline IntPtr_t get__classHandle_3() const { return ____classHandle_3; }
	inline IntPtr_t* get_address_of__classHandle_3() { return &____classHandle_3; }
	inline void set__classHandle_3(IntPtr_t value)
	{
		____classHandle_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
