﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.Int32,System.String>
struct Dictionary_2_t1037045868;
// System.String
struct String_t;
// Amazon.Runtime.Internal.Util.BackgroundInvoker
struct BackgroundInvoker_t1722929158;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_DateTime693205669.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Util.AWSSDKUtils
struct  AWSSDKUtils_t2036360342  : public Il2CppObject
{
public:

public:
};

struct AWSSDKUtils_t2036360342_StaticFields
{
public:
	// System.DateTime Amazon.Util.AWSSDKUtils::EPOCH_START
	DateTime_t693205669  ___EPOCH_START_0;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.String> Amazon.Util.AWSSDKUtils::RFCEncodingSchemes
	Dictionary_2_t1037045868 * ___RFCEncodingSchemes_1;
	// System.String Amazon.Util.AWSSDKUtils::ValidPathCharacters
	String_t* ___ValidPathCharacters_2;
	// Amazon.Runtime.Internal.Util.BackgroundInvoker Amazon.Util.AWSSDKUtils::_dispatcher
	BackgroundInvoker_t1722929158 * ____dispatcher_3;
	// System.Object Amazon.Util.AWSSDKUtils::_preserveStackTraceLookupLock
	Il2CppObject * ____preserveStackTraceLookupLock_4;
	// System.Boolean Amazon.Util.AWSSDKUtils::_preserveStackTraceLookup
	bool ____preserveStackTraceLookup_5;

public:
	inline static int32_t get_offset_of_EPOCH_START_0() { return static_cast<int32_t>(offsetof(AWSSDKUtils_t2036360342_StaticFields, ___EPOCH_START_0)); }
	inline DateTime_t693205669  get_EPOCH_START_0() const { return ___EPOCH_START_0; }
	inline DateTime_t693205669 * get_address_of_EPOCH_START_0() { return &___EPOCH_START_0; }
	inline void set_EPOCH_START_0(DateTime_t693205669  value)
	{
		___EPOCH_START_0 = value;
	}

	inline static int32_t get_offset_of_RFCEncodingSchemes_1() { return static_cast<int32_t>(offsetof(AWSSDKUtils_t2036360342_StaticFields, ___RFCEncodingSchemes_1)); }
	inline Dictionary_2_t1037045868 * get_RFCEncodingSchemes_1() const { return ___RFCEncodingSchemes_1; }
	inline Dictionary_2_t1037045868 ** get_address_of_RFCEncodingSchemes_1() { return &___RFCEncodingSchemes_1; }
	inline void set_RFCEncodingSchemes_1(Dictionary_2_t1037045868 * value)
	{
		___RFCEncodingSchemes_1 = value;
		Il2CppCodeGenWriteBarrier(&___RFCEncodingSchemes_1, value);
	}

	inline static int32_t get_offset_of_ValidPathCharacters_2() { return static_cast<int32_t>(offsetof(AWSSDKUtils_t2036360342_StaticFields, ___ValidPathCharacters_2)); }
	inline String_t* get_ValidPathCharacters_2() const { return ___ValidPathCharacters_2; }
	inline String_t** get_address_of_ValidPathCharacters_2() { return &___ValidPathCharacters_2; }
	inline void set_ValidPathCharacters_2(String_t* value)
	{
		___ValidPathCharacters_2 = value;
		Il2CppCodeGenWriteBarrier(&___ValidPathCharacters_2, value);
	}

	inline static int32_t get_offset_of__dispatcher_3() { return static_cast<int32_t>(offsetof(AWSSDKUtils_t2036360342_StaticFields, ____dispatcher_3)); }
	inline BackgroundInvoker_t1722929158 * get__dispatcher_3() const { return ____dispatcher_3; }
	inline BackgroundInvoker_t1722929158 ** get_address_of__dispatcher_3() { return &____dispatcher_3; }
	inline void set__dispatcher_3(BackgroundInvoker_t1722929158 * value)
	{
		____dispatcher_3 = value;
		Il2CppCodeGenWriteBarrier(&____dispatcher_3, value);
	}

	inline static int32_t get_offset_of__preserveStackTraceLookupLock_4() { return static_cast<int32_t>(offsetof(AWSSDKUtils_t2036360342_StaticFields, ____preserveStackTraceLookupLock_4)); }
	inline Il2CppObject * get__preserveStackTraceLookupLock_4() const { return ____preserveStackTraceLookupLock_4; }
	inline Il2CppObject ** get_address_of__preserveStackTraceLookupLock_4() { return &____preserveStackTraceLookupLock_4; }
	inline void set__preserveStackTraceLookupLock_4(Il2CppObject * value)
	{
		____preserveStackTraceLookupLock_4 = value;
		Il2CppCodeGenWriteBarrier(&____preserveStackTraceLookupLock_4, value);
	}

	inline static int32_t get_offset_of__preserveStackTraceLookup_5() { return static_cast<int32_t>(offsetof(AWSSDKUtils_t2036360342_StaticFields, ____preserveStackTraceLookup_5)); }
	inline bool get__preserveStackTraceLookup_5() const { return ____preserveStackTraceLookup_5; }
	inline bool* get_address_of__preserveStackTraceLookup_5() { return &____preserveStackTraceLookup_5; }
	inline void set__preserveStackTraceLookup_5(bool value)
	{
		____preserveStackTraceLookup_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
