﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4135932286.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23277180024.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m879357244_gshared (InternalEnumerator_1_t4135932286 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m879357244(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t4135932286 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m879357244_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m172580908_gshared (InternalEnumerator_1_t4135932286 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m172580908(__this, method) ((  void (*) (InternalEnumerator_1_t4135932286 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m172580908_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2587373224_gshared (InternalEnumerator_1_t4135932286 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2587373224(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t4135932286 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2587373224_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1199677275_gshared (InternalEnumerator_1_t4135932286 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1199677275(__this, method) ((  void (*) (InternalEnumerator_1_t4135932286 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1199677275_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1526791348_gshared (InternalEnumerator_1_t4135932286 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1526791348(__this, method) ((  bool (*) (InternalEnumerator_1_t4135932286 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1526791348_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>>::get_Current()
extern "C"  KeyValuePair_2_t3277180024  InternalEnumerator_1_get_Current_m4215253123_gshared (InternalEnumerator_1_t4135932286 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m4215253123(__this, method) ((  KeyValuePair_2_t3277180024  (*) (InternalEnumerator_1_t4135932286 *, const MethodInfo*))InternalEnumerator_1_get_Current_m4215253123_gshared)(__this, method)
