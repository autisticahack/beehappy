﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Internal.RegionEndpointV3/ServiceMap
struct ServiceMap_t3203095671;
// System.Collections.Generic.Dictionary`2<System.String,Amazon.RegionEndpoint/Endpoint>
struct Dictionary_2_t968504607;
// System.String
struct String_t;
// Amazon.RegionEndpoint/Endpoint
struct Endpoint_t3348692641;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AWSSDK_Core_Amazon_RegionEndpoint_Endpoint3348692641.h"

// System.Collections.Generic.Dictionary`2<System.String,Amazon.RegionEndpoint/Endpoint> Amazon.Internal.RegionEndpointV3/ServiceMap::GetMap(System.Boolean)
extern "C"  Dictionary_2_t968504607 * ServiceMap_GetMap_m3091021604 (ServiceMap_t3203095671 * __this, bool ___dualStack0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Internal.RegionEndpointV3/ServiceMap::ContainsKey(System.String)
extern "C"  bool ServiceMap_ContainsKey_m1731776152 (ServiceMap_t3203095671 * __this, String_t* ___servicName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Internal.RegionEndpointV3/ServiceMap::Add(System.String,System.Boolean,Amazon.RegionEndpoint/Endpoint)
extern "C"  void ServiceMap_Add_m137559301 (ServiceMap_t3203095671 * __this, String_t* ___serviceName0, bool ___dualStack1, Endpoint_t3348692641 * ___endpoint2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Internal.RegionEndpointV3/ServiceMap::TryGetEndpoint(System.String,System.Boolean,Amazon.RegionEndpoint/Endpoint&)
extern "C"  bool ServiceMap_TryGetEndpoint_m3013127620 (ServiceMap_t3203095671 * __this, String_t* ___serviceName0, bool ___dualStack1, Endpoint_t3348692641 ** ___endpoint2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Internal.RegionEndpointV3/ServiceMap::.ctor()
extern "C"  void ServiceMap__ctor_m1812401888 (ServiceMap_t3203095671 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
