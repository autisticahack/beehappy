﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.Runtime.Internal.Util.Logger
struct Logger_t2262497814;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AWSSDK_Core_Amazon_Util_Internal_PlatformServices_N879095062.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.UnityMainThreadDispatcher
struct  UnityMainThreadDispatcher_t4098072663  : public MonoBehaviour_t1158329972
{
public:
	// Amazon.Runtime.Internal.Util.Logger Amazon.Runtime.Internal.UnityMainThreadDispatcher::_logger
	Logger_t2262497814 * ____logger_2;
	// System.Single Amazon.Runtime.Internal.UnityMainThreadDispatcher::_nextUpdateTime
	float ____nextUpdateTime_3;
	// System.Single Amazon.Runtime.Internal.UnityMainThreadDispatcher::_updateInterval
	float ____updateInterval_4;
	// Amazon.Util.Internal.PlatformServices.NetworkStatus Amazon.Runtime.Internal.UnityMainThreadDispatcher::_currentNetworkStatus
	int32_t ____currentNetworkStatus_5;

public:
	inline static int32_t get_offset_of__logger_2() { return static_cast<int32_t>(offsetof(UnityMainThreadDispatcher_t4098072663, ____logger_2)); }
	inline Logger_t2262497814 * get__logger_2() const { return ____logger_2; }
	inline Logger_t2262497814 ** get_address_of__logger_2() { return &____logger_2; }
	inline void set__logger_2(Logger_t2262497814 * value)
	{
		____logger_2 = value;
		Il2CppCodeGenWriteBarrier(&____logger_2, value);
	}

	inline static int32_t get_offset_of__nextUpdateTime_3() { return static_cast<int32_t>(offsetof(UnityMainThreadDispatcher_t4098072663, ____nextUpdateTime_3)); }
	inline float get__nextUpdateTime_3() const { return ____nextUpdateTime_3; }
	inline float* get_address_of__nextUpdateTime_3() { return &____nextUpdateTime_3; }
	inline void set__nextUpdateTime_3(float value)
	{
		____nextUpdateTime_3 = value;
	}

	inline static int32_t get_offset_of__updateInterval_4() { return static_cast<int32_t>(offsetof(UnityMainThreadDispatcher_t4098072663, ____updateInterval_4)); }
	inline float get__updateInterval_4() const { return ____updateInterval_4; }
	inline float* get_address_of__updateInterval_4() { return &____updateInterval_4; }
	inline void set__updateInterval_4(float value)
	{
		____updateInterval_4 = value;
	}

	inline static int32_t get_offset_of__currentNetworkStatus_5() { return static_cast<int32_t>(offsetof(UnityMainThreadDispatcher_t4098072663, ____currentNetworkStatus_5)); }
	inline int32_t get__currentNetworkStatus_5() const { return ____currentNetworkStatus_5; }
	inline int32_t* get_address_of__currentNetworkStatus_5() { return &____currentNetworkStatus_5; }
	inline void set__currentNetworkStatus_5(int32_t value)
	{
		____currentNetworkStatus_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
