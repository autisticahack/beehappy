﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.Runtime.IHttpRequestFactory`1<System.String>
struct IHttpRequestFactory_1_t3761175889;
// System.Object
struct Il2CppObject;

#include "AWSSDK_Core_Amazon_Runtime_Internal_PipelineHandle1486422324.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.HttpHandler`1<System.String>
struct  HttpHandler_1_t3510606603  : public PipelineHandler_t1486422324
{
public:
	// System.Boolean Amazon.Runtime.Internal.HttpHandler`1::_disposed
	bool ____disposed_3;
	// Amazon.Runtime.IHttpRequestFactory`1<TRequestContent> Amazon.Runtime.Internal.HttpHandler`1::_requestFactory
	Il2CppObject* ____requestFactory_4;
	// System.Object Amazon.Runtime.Internal.HttpHandler`1::<CallbackSender>k__BackingField
	Il2CppObject * ___U3CCallbackSenderU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of__disposed_3() { return static_cast<int32_t>(offsetof(HttpHandler_1_t3510606603, ____disposed_3)); }
	inline bool get__disposed_3() const { return ____disposed_3; }
	inline bool* get_address_of__disposed_3() { return &____disposed_3; }
	inline void set__disposed_3(bool value)
	{
		____disposed_3 = value;
	}

	inline static int32_t get_offset_of__requestFactory_4() { return static_cast<int32_t>(offsetof(HttpHandler_1_t3510606603, ____requestFactory_4)); }
	inline Il2CppObject* get__requestFactory_4() const { return ____requestFactory_4; }
	inline Il2CppObject** get_address_of__requestFactory_4() { return &____requestFactory_4; }
	inline void set__requestFactory_4(Il2CppObject* value)
	{
		____requestFactory_4 = value;
		Il2CppCodeGenWriteBarrier(&____requestFactory_4, value);
	}

	inline static int32_t get_offset_of_U3CCallbackSenderU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(HttpHandler_1_t3510606603, ___U3CCallbackSenderU3Ek__BackingField_5)); }
	inline Il2CppObject * get_U3CCallbackSenderU3Ek__BackingField_5() const { return ___U3CCallbackSenderU3Ek__BackingField_5; }
	inline Il2CppObject ** get_address_of_U3CCallbackSenderU3Ek__BackingField_5() { return &___U3CCallbackSenderU3Ek__BackingField_5; }
	inline void set_U3CCallbackSenderU3Ek__BackingField_5(Il2CppObject * value)
	{
		___U3CCallbackSenderU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCallbackSenderU3Ek__BackingField_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
