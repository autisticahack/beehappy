﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.UploadHandlerRawWrapper
struct  UploadHandlerRawWrapper_t3528989734  : public Il2CppObject
{
public:
	// System.Object Amazon.Runtime.Internal.UploadHandlerRawWrapper::<Instance>k__BackingField
	Il2CppObject * ___U3CInstanceU3Ek__BackingField_1;
	// System.Boolean Amazon.Runtime.Internal.UploadHandlerRawWrapper::disposedValue
	bool ___disposedValue_2;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(UploadHandlerRawWrapper_t3528989734, ___U3CInstanceU3Ek__BackingField_1)); }
	inline Il2CppObject * get_U3CInstanceU3Ek__BackingField_1() const { return ___U3CInstanceU3Ek__BackingField_1; }
	inline Il2CppObject ** get_address_of_U3CInstanceU3Ek__BackingField_1() { return &___U3CInstanceU3Ek__BackingField_1; }
	inline void set_U3CInstanceU3Ek__BackingField_1(Il2CppObject * value)
	{
		___U3CInstanceU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CInstanceU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_disposedValue_2() { return static_cast<int32_t>(offsetof(UploadHandlerRawWrapper_t3528989734, ___disposedValue_2)); }
	inline bool get_disposedValue_2() const { return ___disposedValue_2; }
	inline bool* get_address_of_disposedValue_2() { return &___disposedValue_2; }
	inline void set_disposedValue_2(bool value)
	{
		___disposedValue_2 = value;
	}
};

struct UploadHandlerRawWrapper_t3528989734_StaticFields
{
public:
	// System.Type Amazon.Runtime.Internal.UploadHandlerRawWrapper::uploadHandlerRawType
	Type_t * ___uploadHandlerRawType_0;

public:
	inline static int32_t get_offset_of_uploadHandlerRawType_0() { return static_cast<int32_t>(offsetof(UploadHandlerRawWrapper_t3528989734_StaticFields, ___uploadHandlerRawType_0)); }
	inline Type_t * get_uploadHandlerRawType_0() const { return ___uploadHandlerRawType_0; }
	inline Type_t ** get_address_of_uploadHandlerRawType_0() { return &___uploadHandlerRawType_0; }
	inline void set_uploadHandlerRawType_0(Type_t * value)
	{
		___uploadHandlerRawType_0 = value;
		Il2CppCodeGenWriteBarrier(&___uploadHandlerRawType_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
