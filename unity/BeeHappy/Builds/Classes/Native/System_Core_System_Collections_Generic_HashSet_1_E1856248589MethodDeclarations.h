﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.HashSet`1<System.Xml.XmlNodeType>
struct HashSet_1_t3367932747;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Collections_Generic_HashSet_1_E1856248589.h"
#include "System_Xml_System_Xml_XmlNodeType739504597.h"

// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Xml.XmlNodeType>::.ctor(System.Collections.Generic.HashSet`1<T>)
extern "C"  void Enumerator__ctor_m3015685481_gshared (Enumerator_t1856248589 * __this, HashSet_1_t3367932747 * ___hashset0, const MethodInfo* method);
#define Enumerator__ctor_m3015685481(__this, ___hashset0, method) ((  void (*) (Enumerator_t1856248589 *, HashSet_1_t3367932747 *, const MethodInfo*))Enumerator__ctor_m3015685481_gshared)(__this, ___hashset0, method)
// System.Object System.Collections.Generic.HashSet`1/Enumerator<System.Xml.XmlNodeType>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3179671655_gshared (Enumerator_t1856248589 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3179671655(__this, method) ((  Il2CppObject * (*) (Enumerator_t1856248589 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3179671655_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Xml.XmlNodeType>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1767948475_gshared (Enumerator_t1856248589 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1767948475(__this, method) ((  void (*) (Enumerator_t1856248589 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1767948475_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1/Enumerator<System.Xml.XmlNodeType>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3772677035_gshared (Enumerator_t1856248589 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3772677035(__this, method) ((  bool (*) (Enumerator_t1856248589 *, const MethodInfo*))Enumerator_MoveNext_m3772677035_gshared)(__this, method)
// T System.Collections.Generic.HashSet`1/Enumerator<System.Xml.XmlNodeType>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m3299983176_gshared (Enumerator_t1856248589 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3299983176(__this, method) ((  int32_t (*) (Enumerator_t1856248589 *, const MethodInfo*))Enumerator_get_Current_m3299983176_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Xml.XmlNodeType>::Dispose()
extern "C"  void Enumerator_Dispose_m247550566_gshared (Enumerator_t1856248589 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m247550566(__this, method) ((  void (*) (Enumerator_t1856248589 *, const MethodInfo*))Enumerator_Dispose_m247550566_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Xml.XmlNodeType>::CheckState()
extern "C"  void Enumerator_CheckState_m2179511538_gshared (Enumerator_t1856248589 * __this, const MethodInfo* method);
#define Enumerator_CheckState_m2179511538(__this, method) ((  void (*) (Enumerator_t1856248589 *, const MethodInfo*))Enumerator_CheckState_m2179511538_gshared)(__this, method)
