﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>
struct Dictionary_2_t3650197868;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2044734010.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m4120924977_gshared (Enumerator_t2044734010 * __this, Dictionary_2_t3650197868 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m4120924977(__this, ___host0, method) ((  void (*) (Enumerator_t2044734010 *, Dictionary_2_t3650197868 *, const MethodInfo*))Enumerator__ctor_m4120924977_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1515014342_gshared (Enumerator_t2044734010 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1515014342(__this, method) ((  Il2CppObject * (*) (Enumerator_t2044734010 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1515014342_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3947696880_gshared (Enumerator_t2044734010 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3947696880(__this, method) ((  void (*) (Enumerator_t2044734010 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3947696880_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::Dispose()
extern "C"  void Enumerator_Dispose_m165842793_gshared (Enumerator_t2044734010 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m165842793(__this, method) ((  void (*) (Enumerator_t2044734010 *, const MethodInfo*))Enumerator_Dispose_m165842793_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3358238188_gshared (Enumerator_t2044734010 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3358238188(__this, method) ((  bool (*) (Enumerator_t2044734010 *, const MethodInfo*))Enumerator_MoveNext_m3358238188_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m3799973880_gshared (Enumerator_t2044734010 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3799973880(__this, method) ((  Il2CppObject * (*) (Enumerator_t2044734010 *, const MethodInfo*))Enumerator_get_Current_m3799973880_gshared)(__this, method)
