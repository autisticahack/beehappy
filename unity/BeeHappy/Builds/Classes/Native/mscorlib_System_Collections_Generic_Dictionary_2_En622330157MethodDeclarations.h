﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2464585190MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,Amazon.Runtime.Internal.Util.Timing>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m709943636(__this, ___dictionary0, method) ((  void (*) (Enumerator_t622330157 *, Dictionary_2_t3597272751 *, const MethodInfo*))Enumerator__ctor_m2882573199_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,Amazon.Runtime.Internal.Util.Timing>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2107279937(__this, method) ((  Il2CppObject * (*) (Enumerator_t622330157 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2753081182_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,Amazon.Runtime.Internal.Util.Timing>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m714836457(__this, method) ((  void (*) (Enumerator_t622330157 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3448018626_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,Amazon.Runtime.Internal.Util.Timing>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1426144550(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t622330157 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1386174965_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,Amazon.Runtime.Internal.Util.Timing>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m985349375(__this, method) ((  Il2CppObject * (*) (Enumerator_t622330157 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3637591552_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,Amazon.Runtime.Internal.Util.Timing>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m350027463(__this, method) ((  Il2CppObject * (*) (Enumerator_t622330157 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1974664938_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,Amazon.Runtime.Internal.Util.Timing>::MoveNext()
#define Enumerator_MoveNext_m1360849257(__this, method) ((  bool (*) (Enumerator_t622330157 *, const MethodInfo*))Enumerator_MoveNext_m3844071554_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,Amazon.Runtime.Internal.Util.Timing>::get_Current()
#define Enumerator_get_Current_m2099453813(__this, method) ((  KeyValuePair_2_t1354617973  (*) (Enumerator_t622330157 *, const MethodInfo*))Enumerator_get_Current_m1831421754_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,Amazon.Runtime.Internal.Util.Timing>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m1804943828(__this, method) ((  int32_t (*) (Enumerator_t622330157 *, const MethodInfo*))Enumerator_get_CurrentKey_m2750410819_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,Amazon.Runtime.Internal.Util.Timing>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m652534676(__this, method) ((  Timing_t847194262 * (*) (Enumerator_t622330157 *, const MethodInfo*))Enumerator_get_CurrentValue_m4218670947_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,Amazon.Runtime.Internal.Util.Timing>::Reset()
#define Enumerator_Reset_m3333729998(__this, method) ((  void (*) (Enumerator_t622330157 *, const MethodInfo*))Enumerator_Reset_m1867529997_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,Amazon.Runtime.Internal.Util.Timing>::VerifyState()
#define Enumerator_VerifyState_m4070808695(__this, method) ((  void (*) (Enumerator_t622330157 *, const MethodInfo*))Enumerator_VerifyState_m1404756950_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,Amazon.Runtime.Internal.Util.Timing>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m3889885753(__this, method) ((  void (*) (Enumerator_t622330157 *, const MethodInfo*))Enumerator_VerifyCurrent_m4158200186_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,Amazon.Runtime.Internal.Util.Timing>::Dispose()
#define Enumerator_Dispose_m2423998964(__this, method) ((  void (*) (Enumerator_t622330157 *, const MethodInfo*))Enumerator_Dispose_m2167966803_gshared)(__this, method)
