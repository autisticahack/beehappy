﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.LoggingSection
struct LoggingSection_t905443946;
// System.String
struct String_t;
// Amazon.ProxySection
struct ProxySection_t4280332351;
// System.Collections.Generic.IDictionary`2<System.String,System.Xml.Linq.XElement>
struct IDictionary_2_t467683733;

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Nullable_1_gen2088641033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.AWSSection
struct  AWSSection_t3096528870  : public Il2CppObject
{
public:
	// Amazon.LoggingSection Amazon.AWSSection::<Logging>k__BackingField
	LoggingSection_t905443946 * ___U3CLoggingU3Ek__BackingField_0;
	// System.String Amazon.AWSSection::<EndpointDefinition>k__BackingField
	String_t* ___U3CEndpointDefinitionU3Ek__BackingField_1;
	// System.String Amazon.AWSSection::<Region>k__BackingField
	String_t* ___U3CRegionU3Ek__BackingField_2;
	// System.Nullable`1<System.Boolean> Amazon.AWSSection::<UseSdkCache>k__BackingField
	Nullable_1_t2088641033  ___U3CUseSdkCacheU3Ek__BackingField_3;
	// System.Nullable`1<System.Boolean> Amazon.AWSSection::<CorrectForClockSkew>k__BackingField
	Nullable_1_t2088641033  ___U3CCorrectForClockSkewU3Ek__BackingField_4;
	// Amazon.ProxySection Amazon.AWSSection::<Proxy>k__BackingField
	ProxySection_t4280332351 * ___U3CProxyU3Ek__BackingField_5;
	// System.String Amazon.AWSSection::<ProfileName>k__BackingField
	String_t* ___U3CProfileNameU3Ek__BackingField_6;
	// System.String Amazon.AWSSection::<ProfilesLocation>k__BackingField
	String_t* ___U3CProfilesLocationU3Ek__BackingField_7;
	// System.String Amazon.AWSSection::<ApplicationName>k__BackingField
	String_t* ___U3CApplicationNameU3Ek__BackingField_8;
	// System.Collections.Generic.IDictionary`2<System.String,System.Xml.Linq.XElement> Amazon.AWSSection::_serviceSections
	Il2CppObject* ____serviceSections_9;

public:
	inline static int32_t get_offset_of_U3CLoggingU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AWSSection_t3096528870, ___U3CLoggingU3Ek__BackingField_0)); }
	inline LoggingSection_t905443946 * get_U3CLoggingU3Ek__BackingField_0() const { return ___U3CLoggingU3Ek__BackingField_0; }
	inline LoggingSection_t905443946 ** get_address_of_U3CLoggingU3Ek__BackingField_0() { return &___U3CLoggingU3Ek__BackingField_0; }
	inline void set_U3CLoggingU3Ek__BackingField_0(LoggingSection_t905443946 * value)
	{
		___U3CLoggingU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CLoggingU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CEndpointDefinitionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AWSSection_t3096528870, ___U3CEndpointDefinitionU3Ek__BackingField_1)); }
	inline String_t* get_U3CEndpointDefinitionU3Ek__BackingField_1() const { return ___U3CEndpointDefinitionU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CEndpointDefinitionU3Ek__BackingField_1() { return &___U3CEndpointDefinitionU3Ek__BackingField_1; }
	inline void set_U3CEndpointDefinitionU3Ek__BackingField_1(String_t* value)
	{
		___U3CEndpointDefinitionU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CEndpointDefinitionU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CRegionU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AWSSection_t3096528870, ___U3CRegionU3Ek__BackingField_2)); }
	inline String_t* get_U3CRegionU3Ek__BackingField_2() const { return ___U3CRegionU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CRegionU3Ek__BackingField_2() { return &___U3CRegionU3Ek__BackingField_2; }
	inline void set_U3CRegionU3Ek__BackingField_2(String_t* value)
	{
		___U3CRegionU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CRegionU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CUseSdkCacheU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AWSSection_t3096528870, ___U3CUseSdkCacheU3Ek__BackingField_3)); }
	inline Nullable_1_t2088641033  get_U3CUseSdkCacheU3Ek__BackingField_3() const { return ___U3CUseSdkCacheU3Ek__BackingField_3; }
	inline Nullable_1_t2088641033 * get_address_of_U3CUseSdkCacheU3Ek__BackingField_3() { return &___U3CUseSdkCacheU3Ek__BackingField_3; }
	inline void set_U3CUseSdkCacheU3Ek__BackingField_3(Nullable_1_t2088641033  value)
	{
		___U3CUseSdkCacheU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CCorrectForClockSkewU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(AWSSection_t3096528870, ___U3CCorrectForClockSkewU3Ek__BackingField_4)); }
	inline Nullable_1_t2088641033  get_U3CCorrectForClockSkewU3Ek__BackingField_4() const { return ___U3CCorrectForClockSkewU3Ek__BackingField_4; }
	inline Nullable_1_t2088641033 * get_address_of_U3CCorrectForClockSkewU3Ek__BackingField_4() { return &___U3CCorrectForClockSkewU3Ek__BackingField_4; }
	inline void set_U3CCorrectForClockSkewU3Ek__BackingField_4(Nullable_1_t2088641033  value)
	{
		___U3CCorrectForClockSkewU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CProxyU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(AWSSection_t3096528870, ___U3CProxyU3Ek__BackingField_5)); }
	inline ProxySection_t4280332351 * get_U3CProxyU3Ek__BackingField_5() const { return ___U3CProxyU3Ek__BackingField_5; }
	inline ProxySection_t4280332351 ** get_address_of_U3CProxyU3Ek__BackingField_5() { return &___U3CProxyU3Ek__BackingField_5; }
	inline void set_U3CProxyU3Ek__BackingField_5(ProxySection_t4280332351 * value)
	{
		___U3CProxyU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CProxyU3Ek__BackingField_5, value);
	}

	inline static int32_t get_offset_of_U3CProfileNameU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(AWSSection_t3096528870, ___U3CProfileNameU3Ek__BackingField_6)); }
	inline String_t* get_U3CProfileNameU3Ek__BackingField_6() const { return ___U3CProfileNameU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CProfileNameU3Ek__BackingField_6() { return &___U3CProfileNameU3Ek__BackingField_6; }
	inline void set_U3CProfileNameU3Ek__BackingField_6(String_t* value)
	{
		___U3CProfileNameU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CProfileNameU3Ek__BackingField_6, value);
	}

	inline static int32_t get_offset_of_U3CProfilesLocationU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(AWSSection_t3096528870, ___U3CProfilesLocationU3Ek__BackingField_7)); }
	inline String_t* get_U3CProfilesLocationU3Ek__BackingField_7() const { return ___U3CProfilesLocationU3Ek__BackingField_7; }
	inline String_t** get_address_of_U3CProfilesLocationU3Ek__BackingField_7() { return &___U3CProfilesLocationU3Ek__BackingField_7; }
	inline void set_U3CProfilesLocationU3Ek__BackingField_7(String_t* value)
	{
		___U3CProfilesLocationU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CProfilesLocationU3Ek__BackingField_7, value);
	}

	inline static int32_t get_offset_of_U3CApplicationNameU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(AWSSection_t3096528870, ___U3CApplicationNameU3Ek__BackingField_8)); }
	inline String_t* get_U3CApplicationNameU3Ek__BackingField_8() const { return ___U3CApplicationNameU3Ek__BackingField_8; }
	inline String_t** get_address_of_U3CApplicationNameU3Ek__BackingField_8() { return &___U3CApplicationNameU3Ek__BackingField_8; }
	inline void set_U3CApplicationNameU3Ek__BackingField_8(String_t* value)
	{
		___U3CApplicationNameU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CApplicationNameU3Ek__BackingField_8, value);
	}

	inline static int32_t get_offset_of__serviceSections_9() { return static_cast<int32_t>(offsetof(AWSSection_t3096528870, ____serviceSections_9)); }
	inline Il2CppObject* get__serviceSections_9() const { return ____serviceSections_9; }
	inline Il2CppObject** get_address_of__serviceSections_9() { return &____serviceSections_9; }
	inline void set__serviceSections_9(Il2CppObject* value)
	{
		____serviceSections_9 = value;
		Il2CppCodeGenWriteBarrier(&____serviceSections_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
