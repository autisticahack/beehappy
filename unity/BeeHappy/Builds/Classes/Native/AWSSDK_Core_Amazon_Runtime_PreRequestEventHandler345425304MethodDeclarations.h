﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.PreRequestEventHandler
struct PreRequestEventHandler_t345425304;
// System.Object
struct Il2CppObject;
// Amazon.Runtime.PreRequestEventArgs
struct PreRequestEventArgs_t776850383;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AWSSDK_Core_Amazon_Runtime_PreRequestEventArgs776850383.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Amazon.Runtime.PreRequestEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void PreRequestEventHandler__ctor_m3172338083 (PreRequestEventHandler_t345425304 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.PreRequestEventHandler::Invoke(System.Object,Amazon.Runtime.PreRequestEventArgs)
extern "C"  void PreRequestEventHandler_Invoke_m2641399506 (PreRequestEventHandler_t345425304 * __this, Il2CppObject * ___sender0, PreRequestEventArgs_t776850383 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Amazon.Runtime.PreRequestEventHandler::BeginInvoke(System.Object,Amazon.Runtime.PreRequestEventArgs,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * PreRequestEventHandler_BeginInvoke_m349249045 (PreRequestEventHandler_t345425304 * __this, Il2CppObject * ___sender0, PreRequestEventArgs_t776850383 * ___e1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.PreRequestEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void PreRequestEventHandler_EndInvoke_m982183521 (PreRequestEventHandler_t345425304 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
