﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Queue_1_gen2509106130MethodDeclarations.h"

// System.Void System.Collections.Generic.Queue`1<Amazon.Runtime.Internal.Util.ThreadPoolThrottler`1/ThreadPoolOptions`1<Amazon.Runtime.IAsyncExecutionContext,Amazon.Runtime.IAsyncExecutionContext>>::.ctor()
#define Queue_1__ctor_m559880193(__this, method) ((  void (*) (Queue_1_t3041895050 *, const MethodInfo*))Queue_1__ctor_m1845245813_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<Amazon.Runtime.Internal.Util.ThreadPoolThrottler`1/ThreadPoolOptions`1<Amazon.Runtime.IAsyncExecutionContext,Amazon.Runtime.IAsyncExecutionContext>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Queue_1_System_Collections_ICollection_CopyTo_m3020636273(__this, ___array0, ___idx1, method) ((  void (*) (Queue_1_t3041895050 *, Il2CppArray *, int32_t, const MethodInfo*))Queue_1_System_Collections_ICollection_CopyTo_m772787461_gshared)(__this, ___array0, ___idx1, method)
// System.Boolean System.Collections.Generic.Queue`1<Amazon.Runtime.Internal.Util.ThreadPoolThrottler`1/ThreadPoolOptions`1<Amazon.Runtime.IAsyncExecutionContext,Amazon.Runtime.IAsyncExecutionContext>>::System.Collections.ICollection.get_IsSynchronized()
#define Queue_1_System_Collections_ICollection_get_IsSynchronized_m844187089(__this, method) ((  bool (*) (Queue_1_t3041895050 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_IsSynchronized_m307125669_gshared)(__this, method)
// System.Object System.Collections.Generic.Queue`1<Amazon.Runtime.Internal.Util.ThreadPoolThrottler`1/ThreadPoolOptions`1<Amazon.Runtime.IAsyncExecutionContext,Amazon.Runtime.IAsyncExecutionContext>>::System.Collections.ICollection.get_SyncRoot()
#define Queue_1_System_Collections_ICollection_get_SyncRoot_m2542745093(__this, method) ((  Il2CppObject * (*) (Queue_1_t3041895050 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_SyncRoot_m311152041_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Queue`1<Amazon.Runtime.Internal.Util.ThreadPoolThrottler`1/ThreadPoolOptions`1<Amazon.Runtime.IAsyncExecutionContext,Amazon.Runtime.IAsyncExecutionContext>>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1548117105(__this, method) ((  Il2CppObject* (*) (Queue_1_t3041895050 *, const MethodInfo*))Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m4179169157_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Queue`1<Amazon.Runtime.Internal.Util.ThreadPoolThrottler`1/ThreadPoolOptions`1<Amazon.Runtime.IAsyncExecutionContext,Amazon.Runtime.IAsyncExecutionContext>>::System.Collections.IEnumerable.GetEnumerator()
#define Queue_1_System_Collections_IEnumerable_GetEnumerator_m4175160540(__this, method) ((  Il2CppObject * (*) (Queue_1_t3041895050 *, const MethodInfo*))Queue_1_System_Collections_IEnumerable_GetEnumerator_m251608368_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<Amazon.Runtime.Internal.Util.ThreadPoolThrottler`1/ThreadPoolOptions`1<Amazon.Runtime.IAsyncExecutionContext,Amazon.Runtime.IAsyncExecutionContext>>::CopyTo(T[],System.Int32)
#define Queue_1_CopyTo_m304309030(__this, ___array0, ___idx1, method) ((  void (*) (Queue_1_t3041895050 *, ThreadPoolOptions_1U5BU5D_t662351870*, int32_t, const MethodInfo*))Queue_1_CopyTo_m1419617898_gshared)(__this, ___array0, ___idx1, method)
// T System.Collections.Generic.Queue`1<Amazon.Runtime.Internal.Util.ThreadPoolThrottler`1/ThreadPoolOptions`1<Amazon.Runtime.IAsyncExecutionContext,Amazon.Runtime.IAsyncExecutionContext>>::Dequeue()
#define Queue_1_Dequeue_m3700064504(__this, method) ((  ThreadPoolOptions_1_t3222238215 * (*) (Queue_1_t3041895050 *, const MethodInfo*))Queue_1_Dequeue_m4118160228_gshared)(__this, method)
// T System.Collections.Generic.Queue`1<Amazon.Runtime.Internal.Util.ThreadPoolThrottler`1/ThreadPoolOptions`1<Amazon.Runtime.IAsyncExecutionContext,Amazon.Runtime.IAsyncExecutionContext>>::Peek()
#define Queue_1_Peek_m4110510005(__this, method) ((  ThreadPoolOptions_1_t3222238215 * (*) (Queue_1_t3041895050 *, const MethodInfo*))Queue_1_Peek_m1463479953_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<Amazon.Runtime.Internal.Util.ThreadPoolThrottler`1/ThreadPoolOptions`1<Amazon.Runtime.IAsyncExecutionContext,Amazon.Runtime.IAsyncExecutionContext>>::Enqueue(T)
#define Queue_1_Enqueue_m3808968659(__this, ___item0, method) ((  void (*) (Queue_1_t3041895050 *, ThreadPoolOptions_1_t3222238215 *, const MethodInfo*))Queue_1_Enqueue_m2424099095_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Queue`1<Amazon.Runtime.Internal.Util.ThreadPoolThrottler`1/ThreadPoolOptions`1<Amazon.Runtime.IAsyncExecutionContext,Amazon.Runtime.IAsyncExecutionContext>>::SetCapacity(System.Int32)
#define Queue_1_SetCapacity_m1594949934(__this, ___new_size0, method) ((  void (*) (Queue_1_t3041895050 *, int32_t, const MethodInfo*))Queue_1_SetCapacity_m3858927842_gshared)(__this, ___new_size0, method)
// System.Int32 System.Collections.Generic.Queue`1<Amazon.Runtime.Internal.Util.ThreadPoolThrottler`1/ThreadPoolOptions`1<Amazon.Runtime.IAsyncExecutionContext,Amazon.Runtime.IAsyncExecutionContext>>::get_Count()
#define Queue_1_get_Count_m1628541709(__this, method) ((  int32_t (*) (Queue_1_t3041895050 *, const MethodInfo*))Queue_1_get_Count_m3795587777_gshared)(__this, method)
// System.Collections.Generic.Queue`1/Enumerator<T> System.Collections.Generic.Queue`1<Amazon.Runtime.Internal.Util.ThreadPoolThrottler`1/ThreadPoolOptions`1<Amazon.Runtime.IAsyncExecutionContext,Amazon.Runtime.IAsyncExecutionContext>>::GetEnumerator()
#define Queue_1_GetEnumerator_m1016880644(__this, method) ((  Enumerator_t3551958130  (*) (Queue_1_t3041895050 *, const MethodInfo*))Queue_1_GetEnumerator_m2842671368_gshared)(__this, method)
