﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


// Amazon.CognitoSync.Model.Record
struct Record_t441586865;
// Amazon.CognitoSync.Model.RecordPatch
struct RecordPatch_t97905615;
// Amazon.CognitoSync.SyncManager.DatasetMetadata
struct DatasetMetadata_t1205730659;
// Amazon.CognitoSync.SyncManager.Record
struct Record_t868799569;
// Amazon.CognitoSync.SyncManager.SyncConflict
struct SyncConflict_t3030310309;
// Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage/Statement
struct Statement_t3708524595;
// Amazon.CognitoSync.Internal.CognitoCredentialsRetriever/CSRequestCache/CSRequest
struct CSRequest_t3915036330;

#include "mscorlib_System_Array3829468939.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_Model_Record441586865.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_Model_RecordPa97905615.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_SyncManager_1205730659.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_SyncManager_R868799569.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_SyncManager_3030310309.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_SyncManager_3708524595.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_Internal_Cog3915036330.h"

#pragma once
// Amazon.CognitoSync.Model.Record[]
struct RecordU5BU5D_t103396716  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Record_t441586865 * m_Items[1];

public:
	inline Record_t441586865 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Record_t441586865 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Record_t441586865 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Record_t441586865 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Record_t441586865 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Record_t441586865 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Amazon.CognitoSync.Model.RecordPatch[]
struct RecordPatchU5BU5D_t1231009814  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) RecordPatch_t97905615 * m_Items[1];

public:
	inline RecordPatch_t97905615 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RecordPatch_t97905615 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RecordPatch_t97905615 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RecordPatch_t97905615 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RecordPatch_t97905615 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RecordPatch_t97905615 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Amazon.CognitoSync.SyncManager.DatasetMetadata[]
struct DatasetMetadataU5BU5D_t1005480370  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) DatasetMetadata_t1205730659 * m_Items[1];

public:
	inline DatasetMetadata_t1205730659 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline DatasetMetadata_t1205730659 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, DatasetMetadata_t1205730659 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline DatasetMetadata_t1205730659 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline DatasetMetadata_t1205730659 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, DatasetMetadata_t1205730659 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Amazon.CognitoSync.SyncManager.Record[]
struct RecordU5BU5D_t2520965964  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Record_t868799569 * m_Items[1];

public:
	inline Record_t868799569 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Record_t868799569 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Record_t868799569 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Record_t868799569 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Record_t868799569 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Record_t868799569 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Amazon.CognitoSync.SyncManager.SyncConflict[]
struct SyncConflictU5BU5D_t2337015336  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) SyncConflict_t3030310309 * m_Items[1];

public:
	inline SyncConflict_t3030310309 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline SyncConflict_t3030310309 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, SyncConflict_t3030310309 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline SyncConflict_t3030310309 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline SyncConflict_t3030310309 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, SyncConflict_t3030310309 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage/Statement[]
struct StatementU5BU5D_t2768861602  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Statement_t3708524595 * m_Items[1];

public:
	inline Statement_t3708524595 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Statement_t3708524595 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Statement_t3708524595 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Statement_t3708524595 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Statement_t3708524595 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Statement_t3708524595 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Amazon.CognitoSync.Internal.CognitoCredentialsRetriever/CSRequestCache/CSRequest[]
struct CSRequestU5BU5D_t37082991  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CSRequest_t3915036330 * m_Items[1];

public:
	inline CSRequest_t3915036330 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline CSRequest_t3915036330 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, CSRequest_t3915036330 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline CSRequest_t3915036330 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline CSRequest_t3915036330 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, CSRequest_t3915036330 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
