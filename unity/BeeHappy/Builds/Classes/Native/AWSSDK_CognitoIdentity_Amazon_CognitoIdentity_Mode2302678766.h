﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.CognitoIdentity.Model.Credentials
struct Credentials_t792472136;
// System.String
struct String_t;

#include "AWSSDK_Core_Amazon_Runtime_AmazonWebServiceResponse529043356.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.CognitoIdentity.Model.GetCredentialsForIdentityResponse
struct  GetCredentialsForIdentityResponse_t2302678766  : public AmazonWebServiceResponse_t529043356
{
public:
	// Amazon.CognitoIdentity.Model.Credentials Amazon.CognitoIdentity.Model.GetCredentialsForIdentityResponse::_credentials
	Credentials_t792472136 * ____credentials_3;
	// System.String Amazon.CognitoIdentity.Model.GetCredentialsForIdentityResponse::_identityId
	String_t* ____identityId_4;

public:
	inline static int32_t get_offset_of__credentials_3() { return static_cast<int32_t>(offsetof(GetCredentialsForIdentityResponse_t2302678766, ____credentials_3)); }
	inline Credentials_t792472136 * get__credentials_3() const { return ____credentials_3; }
	inline Credentials_t792472136 ** get_address_of__credentials_3() { return &____credentials_3; }
	inline void set__credentials_3(Credentials_t792472136 * value)
	{
		____credentials_3 = value;
		Il2CppCodeGenWriteBarrier(&____credentials_3, value);
	}

	inline static int32_t get_offset_of__identityId_4() { return static_cast<int32_t>(offsetof(GetCredentialsForIdentityResponse_t2302678766, ____identityId_4)); }
	inline String_t* get__identityId_4() const { return ____identityId_4; }
	inline String_t** get_address_of__identityId_4() { return &____identityId_4; }
	inline void set__identityId_4(String_t* value)
	{
		____identityId_4 = value;
		Il2CppCodeGenWriteBarrier(&____identityId_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
