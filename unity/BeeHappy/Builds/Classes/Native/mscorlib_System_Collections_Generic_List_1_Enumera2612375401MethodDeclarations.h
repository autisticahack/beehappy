﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage/Statement>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m1339107714(__this, ___l0, method) ((  void (*) (Enumerator_t2612375401 *, List_1_t3077645727 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage/Statement>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2064309232(__this, method) ((  void (*) (Enumerator_t2612375401 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage/Statement>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2481299366(__this, method) ((  Il2CppObject * (*) (Enumerator_t2612375401 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage/Statement>::Dispose()
#define Enumerator_Dispose_m4141762731(__this, method) ((  void (*) (Enumerator_t2612375401 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage/Statement>::VerifyState()
#define Enumerator_VerifyState_m1811719836(__this, method) ((  void (*) (Enumerator_t2612375401 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage/Statement>::MoveNext()
#define Enumerator_MoveNext_m4207742242(__this, method) ((  bool (*) (Enumerator_t2612375401 *, const MethodInfo*))Enumerator_MoveNext_m984484162_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage/Statement>::get_Current()
#define Enumerator_get_Current_m2788869760(__this, method) ((  Statement_t3708524595 * (*) (Enumerator_t2612375401 *, const MethodInfo*))Enumerator_get_Current_m2193722336_gshared)(__this, method)
