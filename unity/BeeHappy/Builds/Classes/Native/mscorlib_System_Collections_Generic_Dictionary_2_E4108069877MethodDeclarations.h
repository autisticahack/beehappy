﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3601534125MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,ThirdParty.Json.LitJson.ImporterFunc>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m3232587408(__this, ___dictionary0, method) ((  void (*) (Enumerator_t4108069877 *, Dictionary_2_t2788045175 *, const MethodInfo*))Enumerator__ctor_m3742107451_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,ThirdParty.Json.LitJson.ImporterFunc>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m823757263(__this, method) ((  Il2CppObject * (*) (Enumerator_t4108069877 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m229223308_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,ThirdParty.Json.LitJson.ImporterFunc>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1049855751(__this, method) ((  void (*) (Enumerator_t4108069877 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3225937576_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Type,ThirdParty.Json.LitJson.ImporterFunc>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1790158460(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t4108069877 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m221119093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,ThirdParty.Json.LitJson.ImporterFunc>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m846420397(__this, method) ((  Il2CppObject * (*) (Enumerator_t4108069877 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m467957770_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,ThirdParty.Json.LitJson.ImporterFunc>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3894927093(__this, method) ((  Il2CppObject * (*) (Enumerator_t4108069877 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2325383168_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Type,ThirdParty.Json.LitJson.ImporterFunc>::MoveNext()
#define Enumerator_MoveNext_m1365544387(__this, method) ((  bool (*) (Enumerator_t4108069877 *, const MethodInfo*))Enumerator_MoveNext_m3349738440_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Type,ThirdParty.Json.LitJson.ImporterFunc>::get_Current()
#define Enumerator_get_Current_m3290777843(__this, method) ((  KeyValuePair_2_t545390397  (*) (Enumerator_t4108069877 *, const MethodInfo*))Enumerator_get_Current_m25299632_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Type,ThirdParty.Json.LitJson.ImporterFunc>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m2887808944(__this, method) ((  Type_t * (*) (Enumerator_t4108069877 *, const MethodInfo*))Enumerator_get_CurrentKey_m3839846791_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Type,ThirdParty.Json.LitJson.ImporterFunc>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m2401154856(__this, method) ((  ImporterFunc_t850687278 * (*) (Enumerator_t4108069877 *, const MethodInfo*))Enumerator_get_CurrentValue_m402763047_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,ThirdParty.Json.LitJson.ImporterFunc>::Reset()
#define Enumerator_Reset_m2833476986(__this, method) ((  void (*) (Enumerator_t4108069877 *, const MethodInfo*))Enumerator_Reset_m3129803197_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,ThirdParty.Json.LitJson.ImporterFunc>::VerifyState()
#define Enumerator_VerifyState_m382180485(__this, method) ((  void (*) (Enumerator_t4108069877 *, const MethodInfo*))Enumerator_VerifyState_m262343092_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,ThirdParty.Json.LitJson.ImporterFunc>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m1054655735(__this, method) ((  void (*) (Enumerator_t4108069877 *, const MethodInfo*))Enumerator_VerifyCurrent_m1702320752_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,ThirdParty.Json.LitJson.ImporterFunc>::Dispose()
#define Enumerator_Dispose_m214233612(__this, method) ((  void (*) (Enumerator_t4108069877 *, const MethodInfo*))Enumerator_Dispose_m1905011127_gshared)(__this, method)
