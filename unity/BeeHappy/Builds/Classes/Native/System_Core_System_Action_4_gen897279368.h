﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.Runtime.AmazonWebServiceRequest
struct AmazonWebServiceRequest_t3384026212;
// Amazon.Runtime.AmazonWebServiceResponse
struct AmazonWebServiceResponse_t529043356;
// System.Exception
struct Exception_t1927440687;
// Amazon.Runtime.AsyncOptions
struct AsyncOptions_t558351272;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_MulticastDelegate3201952435.h"
#include "mscorlib_System_Void1841601450.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`4<Amazon.Runtime.AmazonWebServiceRequest,Amazon.Runtime.AmazonWebServiceResponse,System.Exception,Amazon.Runtime.AsyncOptions>
struct  Action_4_t897279368  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
