﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.LinkedList`1<System.ComponentModel.TypeDescriptionProvider>>
struct Dictionary_2_t385723205;
// System.Collections.Generic.Dictionary`2<System.ComponentModel.WeakObjectWrapper,System.Collections.Generic.LinkedList`1<System.ComponentModel.TypeDescriptionProvider>>
struct Dictionary_2_t1208943611;
// System.ComponentModel.RefreshEventHandler
struct RefreshEventHandler_t456069287;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.TypeDescriptor
struct  TypeDescriptor_t3595688691  : public Il2CppObject
{
public:

public:
};

struct TypeDescriptor_t3595688691_StaticFields
{
public:
	// System.Object System.ComponentModel.TypeDescriptor::creatingDefaultConverters
	Il2CppObject * ___creatingDefaultConverters_0;
	// System.Collections.Hashtable System.ComponentModel.TypeDescriptor::componentTable
	Hashtable_t909839986 * ___componentTable_1;
	// System.Collections.Hashtable System.ComponentModel.TypeDescriptor::typeTable
	Hashtable_t909839986 * ___typeTable_2;
	// System.Object System.ComponentModel.TypeDescriptor::typeDescriptionProvidersLock
	Il2CppObject * ___typeDescriptionProvidersLock_3;
	// System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.LinkedList`1<System.ComponentModel.TypeDescriptionProvider>> System.ComponentModel.TypeDescriptor::typeDescriptionProviders
	Dictionary_2_t385723205 * ___typeDescriptionProviders_4;
	// System.Object System.ComponentModel.TypeDescriptor::componentDescriptionProvidersLock
	Il2CppObject * ___componentDescriptionProvidersLock_5;
	// System.Collections.Generic.Dictionary`2<System.ComponentModel.WeakObjectWrapper,System.Collections.Generic.LinkedList`1<System.ComponentModel.TypeDescriptionProvider>> System.ComponentModel.TypeDescriptor::componentDescriptionProviders
	Dictionary_2_t1208943611 * ___componentDescriptionProviders_6;
	// System.ComponentModel.RefreshEventHandler System.ComponentModel.TypeDescriptor::Refreshed
	RefreshEventHandler_t456069287 * ___Refreshed_7;

public:
	inline static int32_t get_offset_of_creatingDefaultConverters_0() { return static_cast<int32_t>(offsetof(TypeDescriptor_t3595688691_StaticFields, ___creatingDefaultConverters_0)); }
	inline Il2CppObject * get_creatingDefaultConverters_0() const { return ___creatingDefaultConverters_0; }
	inline Il2CppObject ** get_address_of_creatingDefaultConverters_0() { return &___creatingDefaultConverters_0; }
	inline void set_creatingDefaultConverters_0(Il2CppObject * value)
	{
		___creatingDefaultConverters_0 = value;
		Il2CppCodeGenWriteBarrier(&___creatingDefaultConverters_0, value);
	}

	inline static int32_t get_offset_of_componentTable_1() { return static_cast<int32_t>(offsetof(TypeDescriptor_t3595688691_StaticFields, ___componentTable_1)); }
	inline Hashtable_t909839986 * get_componentTable_1() const { return ___componentTable_1; }
	inline Hashtable_t909839986 ** get_address_of_componentTable_1() { return &___componentTable_1; }
	inline void set_componentTable_1(Hashtable_t909839986 * value)
	{
		___componentTable_1 = value;
		Il2CppCodeGenWriteBarrier(&___componentTable_1, value);
	}

	inline static int32_t get_offset_of_typeTable_2() { return static_cast<int32_t>(offsetof(TypeDescriptor_t3595688691_StaticFields, ___typeTable_2)); }
	inline Hashtable_t909839986 * get_typeTable_2() const { return ___typeTable_2; }
	inline Hashtable_t909839986 ** get_address_of_typeTable_2() { return &___typeTable_2; }
	inline void set_typeTable_2(Hashtable_t909839986 * value)
	{
		___typeTable_2 = value;
		Il2CppCodeGenWriteBarrier(&___typeTable_2, value);
	}

	inline static int32_t get_offset_of_typeDescriptionProvidersLock_3() { return static_cast<int32_t>(offsetof(TypeDescriptor_t3595688691_StaticFields, ___typeDescriptionProvidersLock_3)); }
	inline Il2CppObject * get_typeDescriptionProvidersLock_3() const { return ___typeDescriptionProvidersLock_3; }
	inline Il2CppObject ** get_address_of_typeDescriptionProvidersLock_3() { return &___typeDescriptionProvidersLock_3; }
	inline void set_typeDescriptionProvidersLock_3(Il2CppObject * value)
	{
		___typeDescriptionProvidersLock_3 = value;
		Il2CppCodeGenWriteBarrier(&___typeDescriptionProvidersLock_3, value);
	}

	inline static int32_t get_offset_of_typeDescriptionProviders_4() { return static_cast<int32_t>(offsetof(TypeDescriptor_t3595688691_StaticFields, ___typeDescriptionProviders_4)); }
	inline Dictionary_2_t385723205 * get_typeDescriptionProviders_4() const { return ___typeDescriptionProviders_4; }
	inline Dictionary_2_t385723205 ** get_address_of_typeDescriptionProviders_4() { return &___typeDescriptionProviders_4; }
	inline void set_typeDescriptionProviders_4(Dictionary_2_t385723205 * value)
	{
		___typeDescriptionProviders_4 = value;
		Il2CppCodeGenWriteBarrier(&___typeDescriptionProviders_4, value);
	}

	inline static int32_t get_offset_of_componentDescriptionProvidersLock_5() { return static_cast<int32_t>(offsetof(TypeDescriptor_t3595688691_StaticFields, ___componentDescriptionProvidersLock_5)); }
	inline Il2CppObject * get_componentDescriptionProvidersLock_5() const { return ___componentDescriptionProvidersLock_5; }
	inline Il2CppObject ** get_address_of_componentDescriptionProvidersLock_5() { return &___componentDescriptionProvidersLock_5; }
	inline void set_componentDescriptionProvidersLock_5(Il2CppObject * value)
	{
		___componentDescriptionProvidersLock_5 = value;
		Il2CppCodeGenWriteBarrier(&___componentDescriptionProvidersLock_5, value);
	}

	inline static int32_t get_offset_of_componentDescriptionProviders_6() { return static_cast<int32_t>(offsetof(TypeDescriptor_t3595688691_StaticFields, ___componentDescriptionProviders_6)); }
	inline Dictionary_2_t1208943611 * get_componentDescriptionProviders_6() const { return ___componentDescriptionProviders_6; }
	inline Dictionary_2_t1208943611 ** get_address_of_componentDescriptionProviders_6() { return &___componentDescriptionProviders_6; }
	inline void set_componentDescriptionProviders_6(Dictionary_2_t1208943611 * value)
	{
		___componentDescriptionProviders_6 = value;
		Il2CppCodeGenWriteBarrier(&___componentDescriptionProviders_6, value);
	}

	inline static int32_t get_offset_of_Refreshed_7() { return static_cast<int32_t>(offsetof(TypeDescriptor_t3595688691_StaticFields, ___Refreshed_7)); }
	inline RefreshEventHandler_t456069287 * get_Refreshed_7() const { return ___Refreshed_7; }
	inline RefreshEventHandler_t456069287 ** get_address_of_Refreshed_7() { return &___Refreshed_7; }
	inline void set_Refreshed_7(RefreshEventHandler_t456069287 * value)
	{
		___Refreshed_7 = value;
		Il2CppCodeGenWriteBarrier(&___Refreshed_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
