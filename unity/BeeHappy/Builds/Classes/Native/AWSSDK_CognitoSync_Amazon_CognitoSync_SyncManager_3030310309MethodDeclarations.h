﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.CognitoSync.SyncManager.SyncConflict
struct SyncConflict_t3030310309;
// System.String
struct String_t;
// Amazon.CognitoSync.SyncManager.Record
struct Record_t868799569;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_SyncManager_R868799569.h"

// System.String Amazon.CognitoSync.SyncManager.SyncConflict::get_Key()
extern "C"  String_t* SyncConflict_get_Key_m1398052022 (SyncConflict_t3030310309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.CognitoSync.SyncManager.Record Amazon.CognitoSync.SyncManager.SyncConflict::get_RemoteRecord()
extern "C"  Record_t868799569 * SyncConflict_get_RemoteRecord_m1759036439 (SyncConflict_t3030310309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.CognitoSync.SyncManager.Record Amazon.CognitoSync.SyncManager.SyncConflict::get_LocalRecord()
extern "C"  Record_t868799569 * SyncConflict_get_LocalRecord_m878790868 (SyncConflict_t3030310309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.SyncManager.SyncConflict::.ctor(Amazon.CognitoSync.SyncManager.Record,Amazon.CognitoSync.SyncManager.Record)
extern "C"  void SyncConflict__ctor_m905517389 (SyncConflict_t3030310309 * __this, Record_t868799569 * ___remoteRecord0, Record_t868799569 * ___localRecord1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.CognitoSync.SyncManager.Record Amazon.CognitoSync.SyncManager.SyncConflict::ResolveWithRemoteRecord()
extern "C"  Record_t868799569 * SyncConflict_ResolveWithRemoteRecord_m3739226986 (SyncConflict_t3030310309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.CognitoSync.SyncManager.Record Amazon.CognitoSync.SyncManager.SyncConflict::ResolveWithLocalRecord()
extern "C"  Record_t868799569 * SyncConflict_ResolveWithLocalRecord_m310730057 (SyncConflict_t3030310309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
