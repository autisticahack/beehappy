﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.RetryCapacity
struct  RetryCapacity_t2794668894  : public Il2CppObject
{
public:
	// System.Int32 Amazon.Runtime.Internal.RetryCapacity::_maxCapacity
	int32_t ____maxCapacity_0;
	// System.Int32 Amazon.Runtime.Internal.RetryCapacity::<AvailableCapacity>k__BackingField
	int32_t ___U3CAvailableCapacityU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of__maxCapacity_0() { return static_cast<int32_t>(offsetof(RetryCapacity_t2794668894, ____maxCapacity_0)); }
	inline int32_t get__maxCapacity_0() const { return ____maxCapacity_0; }
	inline int32_t* get_address_of__maxCapacity_0() { return &____maxCapacity_0; }
	inline void set__maxCapacity_0(int32_t value)
	{
		____maxCapacity_0 = value;
	}

	inline static int32_t get_offset_of_U3CAvailableCapacityU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RetryCapacity_t2794668894, ___U3CAvailableCapacityU3Ek__BackingField_1)); }
	inline int32_t get_U3CAvailableCapacityU3Ek__BackingField_1() const { return ___U3CAvailableCapacityU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CAvailableCapacityU3Ek__BackingField_1() { return &___U3CAvailableCapacityU3Ek__BackingField_1; }
	inline void set_U3CAvailableCapacityU3Ek__BackingField_1(int32_t value)
	{
		___U3CAvailableCapacityU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
