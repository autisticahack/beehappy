﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.MetricsHandler
struct MetricsHandler_t1107617423;
// Amazon.Runtime.IExecutionContext
struct IExecutionContext_t2477130752;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// Amazon.Runtime.IAsyncExecutionContext
struct IAsyncExecutionContext_t3792344986;

#include "codegen/il2cpp-codegen.h"

// System.Void Amazon.Runtime.Internal.MetricsHandler::InvokeSync(Amazon.Runtime.IExecutionContext)
extern "C"  void MetricsHandler_InvokeSync_m2715208032 (MetricsHandler_t1107617423 * __this, Il2CppObject * ___executionContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Amazon.Runtime.Internal.MetricsHandler::InvokeAsync(Amazon.Runtime.IAsyncExecutionContext)
extern "C"  Il2CppObject * MetricsHandler_InvokeAsync_m1129257111 (MetricsHandler_t1107617423 * __this, Il2CppObject * ___executionContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.MetricsHandler::InvokeAsyncCallback(Amazon.Runtime.IAsyncExecutionContext)
extern "C"  void MetricsHandler_InvokeAsyncCallback_m1312367628 (MetricsHandler_t1107617423 * __this, Il2CppObject * ___executionContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.MetricsHandler::.ctor()
extern "C"  void MetricsHandler__ctor_m3968354007 (MetricsHandler_t1107617423 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
