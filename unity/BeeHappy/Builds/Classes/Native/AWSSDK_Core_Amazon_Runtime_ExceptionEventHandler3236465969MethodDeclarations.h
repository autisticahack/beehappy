﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.ExceptionEventHandler
struct ExceptionEventHandler_t3236465969;
// System.Object
struct Il2CppObject;
// Amazon.Runtime.ExceptionEventArgs
struct ExceptionEventArgs_t154100464;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AWSSDK_Core_Amazon_Runtime_ExceptionEventArgs154100464.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Amazon.Runtime.ExceptionEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void ExceptionEventHandler__ctor_m679755276 (ExceptionEventHandler_t3236465969 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.ExceptionEventHandler::Invoke(System.Object,Amazon.Runtime.ExceptionEventArgs)
extern "C"  void ExceptionEventHandler_Invoke_m1040915392 (ExceptionEventHandler_t3236465969 * __this, Il2CppObject * ___sender0, ExceptionEventArgs_t154100464 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Amazon.Runtime.ExceptionEventHandler::BeginInvoke(System.Object,Amazon.Runtime.ExceptionEventArgs,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ExceptionEventHandler_BeginInvoke_m3422687305 (ExceptionEventHandler_t3236465969 * __this, Il2CppObject * ___sender0, ExceptionEventArgs_t154100464 * ___e1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.ExceptionEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void ExceptionEventHandler_EndInvoke_m1353827394 (ExceptionEventHandler_t3236465969 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
