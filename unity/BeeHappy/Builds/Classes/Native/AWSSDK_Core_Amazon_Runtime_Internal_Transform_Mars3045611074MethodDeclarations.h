﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.Transform.MarshallerContext
struct MarshallerContext_t3045611074;
// Amazon.Runtime.Internal.IRequest
struct IRequest_t2400804350;

#include "codegen/il2cpp-codegen.h"

// System.Void Amazon.Runtime.Internal.Transform.MarshallerContext::set_Request(Amazon.Runtime.Internal.IRequest)
extern "C"  void MarshallerContext_set_Request_m4023503353 (MarshallerContext_t3045611074 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Transform.MarshallerContext::.ctor(Amazon.Runtime.Internal.IRequest)
extern "C"  void MarshallerContext__ctor_m2200929509 (MarshallerContext_t3045611074 * __this, Il2CppObject * ___request0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
