﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3601534125MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<System.Diagnostics.TraceListener>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m3770962657(__this, ___dictionary0, method) ((  void (*) (Enumerator_t1723907079 *, Dictionary_2_t403882377 *, const MethodInfo*))Enumerator__ctor_m3742107451_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<System.Diagnostics.TraceListener>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m792510948(__this, method) ((  Il2CppObject * (*) (Enumerator_t1723907079 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m229223308_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<System.Diagnostics.TraceListener>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1831400388(__this, method) ((  void (*) (Enumerator_t1723907079 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3225937576_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<System.Diagnostics.TraceListener>>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3461857755(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t1723907079 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m221119093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<System.Diagnostics.TraceListener>>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m4019894030(__this, method) ((  Il2CppObject * (*) (Enumerator_t1723907079 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m467957770_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<System.Diagnostics.TraceListener>>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2405829512(__this, method) ((  Il2CppObject * (*) (Enumerator_t1723907079 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2325383168_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<System.Diagnostics.TraceListener>>::MoveNext()
#define Enumerator_MoveNext_m1042873336(__this, method) ((  bool (*) (Enumerator_t1723907079 *, const MethodInfo*))Enumerator_MoveNext_m3349738440_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<System.Diagnostics.TraceListener>>::get_Current()
#define Enumerator_get_Current_m1142397628(__this, method) ((  KeyValuePair_2_t2456194895  (*) (Enumerator_t1723907079 *, const MethodInfo*))Enumerator_get_Current_m25299632_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<System.Diagnostics.TraceListener>>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m339117029(__this, method) ((  String_t* (*) (Enumerator_t1723907079 *, const MethodInfo*))Enumerator_get_CurrentKey_m3839846791_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<System.Diagnostics.TraceListener>>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m1316473285(__this, method) ((  List_1_t2784070411 * (*) (Enumerator_t1723907079 *, const MethodInfo*))Enumerator_get_CurrentValue_m402763047_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<System.Diagnostics.TraceListener>>::Reset()
#define Enumerator_Reset_m4090052607(__this, method) ((  void (*) (Enumerator_t1723907079 *, const MethodInfo*))Enumerator_Reset_m3129803197_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<System.Diagnostics.TraceListener>>::VerifyState()
#define Enumerator_VerifyState_m1865499880(__this, method) ((  void (*) (Enumerator_t1723907079 *, const MethodInfo*))Enumerator_VerifyState_m262343092_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<System.Diagnostics.TraceListener>>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m1418610740(__this, method) ((  void (*) (Enumerator_t1723907079 *, const MethodInfo*))Enumerator_VerifyCurrent_m1702320752_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<System.Diagnostics.TraceListener>>::Dispose()
#define Enumerator_Dispose_m3947506729(__this, method) ((  void (*) (Enumerator_t1723907079 *, const MethodInfo*))Enumerator_Dispose_m1905011127_gshared)(__this, method)
