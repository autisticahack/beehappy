﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1<Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>
struct EqualityComparer_1_t206442649;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.Generic.EqualityComparer`1<Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m2643746943_gshared (EqualityComparer_1_t206442649 * __this, const MethodInfo* method);
#define EqualityComparer_1__ctor_m2643746943(__this, method) ((  void (*) (EqualityComparer_1_t206442649 *, const MethodInfo*))EqualityComparer_1__ctor_m2643746943_gshared)(__this, method)
// System.Void System.Collections.Generic.EqualityComparer`1<Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m3084714564_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1__cctor_m3084714564(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1__cctor_m3084714564_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m2978214778_gshared (EqualityComparer_1_t206442649 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m2978214778(__this, ___obj0, method) ((  int32_t (*) (EqualityComparer_1_t206442649 *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m2978214778_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1<Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m3289765700_gshared (EqualityComparer_1_t206442649 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m3289765700(__this, ___x0, ___y1, method) ((  bool (*) (EqualityComparer_1_t206442649 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m3289765700_gshared)(__this, ___x0, ___y1, method)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::get_Default()
extern "C"  EqualityComparer_1_t206442649 * EqualityComparer_1_get_Default_m4042453167_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1_get_Default_m4042453167(__this /* static, unused */, method) ((  EqualityComparer_1_t206442649 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1_get_Default_m4042453167_gshared)(__this /* static, unused */, method)
