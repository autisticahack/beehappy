﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2757161903.h"
#include "mscorlib_System_Array3829468939.h"
#include "System_System_Net_HttpStatusCode1898409641.h"

// System.Void System.Array/InternalEnumerator`1<System.Net.HttpStatusCode>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2837138010_gshared (InternalEnumerator_1_t2757161903 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m2837138010(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2757161903 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2837138010_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Net.HttpStatusCode>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2120205006_gshared (InternalEnumerator_1_t2757161903 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2120205006(__this, method) ((  void (*) (InternalEnumerator_1_t2757161903 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2120205006_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Net.HttpStatusCode>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1183746640_gshared (InternalEnumerator_1_t2757161903 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1183746640(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2757161903 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1183746640_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Net.HttpStatusCode>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1998542645_gshared (InternalEnumerator_1_t2757161903 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1998542645(__this, method) ((  void (*) (InternalEnumerator_1_t2757161903 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1998542645_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Net.HttpStatusCode>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1274050794_gshared (InternalEnumerator_1_t2757161903 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1274050794(__this, method) ((  bool (*) (InternalEnumerator_1_t2757161903 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1274050794_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Net.HttpStatusCode>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m1902563197_gshared (InternalEnumerator_1_t2757161903 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1902563197(__this, method) ((  int32_t (*) (InternalEnumerator_1_t2757161903 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1902563197_gshared)(__this, method)
