﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint
struct RegionEndpoint_t1885241449;
// Amazon.RegionEndpoint/Endpoint
struct Endpoint_t3348692641;
// System.String
struct String_t;
// ThirdParty.Json.LitJson.JsonData
struct JsonData_t4263252052;
// System.IO.Stream
struct Stream_t3255436806;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_IO_Stream3255436806.h"

// Amazon.RegionEndpoint/Endpoint Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint::GetEndpointForService(System.String,System.Boolean)
extern "C"  Endpoint_t3348692641 * RegionEndpoint_GetEndpointForService_m922859906 (RegionEndpoint_t1885241449 * __this, String_t* ___serviceName0, bool ___dualStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ThirdParty.Json.LitJson.JsonData Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint::GetEndpointRule(System.String)
extern "C"  JsonData_t4263252052 * RegionEndpoint_GetEndpointRule_m406016850 (RegionEndpoint_t1885241449 * __this, String_t* ___serviceName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint::NewEndpoint(System.String,System.String)
extern "C"  RegionEndpoint_t1885241449 * RegionEndpoint_NewEndpoint_m796684971 (Il2CppObject * __this /* static, unused */, String_t* ___systemName0, String_t* ___displayName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint::GetBySystemName(System.String)
extern "C"  RegionEndpoint_t1885241449 * RegionEndpoint_GetBySystemName_m1655844089 (Il2CppObject * __this /* static, unused */, String_t* ___systemName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint::LoadEndpointDefinitions()
extern "C"  void RegionEndpoint_LoadEndpointDefinitions_m937278659 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint::LoadEndpointDefinitions(System.String)
extern "C"  void RegionEndpoint_LoadEndpointDefinitions_m974664565 (Il2CppObject * __this /* static, unused */, String_t* ___endpointsPath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint::ReadEndpointFile(System.IO.Stream)
extern "C"  void RegionEndpoint_ReadEndpointFile_m3913970838 (Il2CppObject * __this /* static, unused */, Stream_t3255436806 * ___stream0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint::LoadEndpointDefinitionsFromEmbeddedResource()
extern "C"  void RegionEndpoint_LoadEndpointDefinitionsFromEmbeddedResource_m1013659709 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint::.ctor(System.String,System.String)
extern "C"  void RegionEndpoint__ctor_m3495419658 (RegionEndpoint_t1885241449 * __this, String_t* ___systemName0, String_t* ___displayName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint::get_SystemName()
extern "C"  String_t* RegionEndpoint_get_SystemName_m3314692022 (RegionEndpoint_t1885241449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint::set_SystemName(System.String)
extern "C"  void RegionEndpoint_set_SystemName_m1946720089 (RegionEndpoint_t1885241449 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint::get_DisplayName()
extern "C"  String_t* RegionEndpoint_get_DisplayName_m3632256613 (RegionEndpoint_t1885241449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint::set_DisplayName(System.String)
extern "C"  void RegionEndpoint_set_DisplayName_m3952426120 (RegionEndpoint_t1885241449 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint::ToString()
extern "C"  String_t* RegionEndpoint_ToString_m3915184987 (RegionEndpoint_t1885241449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint::.cctor()
extern "C"  void RegionEndpoint__cctor_m3975846471 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
