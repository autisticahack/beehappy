﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen708647570.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_JsonToken2445581255.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Nullable`1<ThirdParty.Json.LitJson.JsonToken>::.ctor(T)
extern "C"  void Nullable_1__ctor_m1611342024_gshared (Nullable_1_t708647570 * __this, int32_t ___value0, const MethodInfo* method);
#define Nullable_1__ctor_m1611342024(__this, ___value0, method) ((  void (*) (Nullable_1_t708647570 *, int32_t, const MethodInfo*))Nullable_1__ctor_m1611342024_gshared)(__this, ___value0, method)
// System.Boolean System.Nullable`1<ThirdParty.Json.LitJson.JsonToken>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m2925731783_gshared (Nullable_1_t708647570 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m2925731783(__this, method) ((  bool (*) (Nullable_1_t708647570 *, const MethodInfo*))Nullable_1_get_HasValue_m2925731783_gshared)(__this, method)
// T System.Nullable`1<ThirdParty.Json.LitJson.JsonToken>::get_Value()
extern "C"  int32_t Nullable_1_get_Value_m822114079_gshared (Nullable_1_t708647570 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m822114079(__this, method) ((  int32_t (*) (Nullable_1_t708647570 *, const MethodInfo*))Nullable_1_get_Value_m822114079_gshared)(__this, method)
// System.Boolean System.Nullable`1<ThirdParty.Json.LitJson.JsonToken>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m765289849_gshared (Nullable_1_t708647570 * __this, Il2CppObject * ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m765289849(__this, ___other0, method) ((  bool (*) (Nullable_1_t708647570 *, Il2CppObject *, const MethodInfo*))Nullable_1_Equals_m765289849_gshared)(__this, ___other0, method)
// System.Boolean System.Nullable`1<ThirdParty.Json.LitJson.JsonToken>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m2904581110_gshared (Nullable_1_t708647570 * __this, Nullable_1_t708647570  ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m2904581110(__this, ___other0, method) ((  bool (*) (Nullable_1_t708647570 *, Nullable_1_t708647570 , const MethodInfo*))Nullable_1_Equals_m2904581110_gshared)(__this, ___other0, method)
// System.Int32 System.Nullable`1<ThirdParty.Json.LitJson.JsonToken>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m3146422171_gshared (Nullable_1_t708647570 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m3146422171(__this, method) ((  int32_t (*) (Nullable_1_t708647570 *, const MethodInfo*))Nullable_1_GetHashCode_m3146422171_gshared)(__this, method)
// T System.Nullable`1<ThirdParty.Json.LitJson.JsonToken>::GetValueOrDefault()
extern "C"  int32_t Nullable_1_GetValueOrDefault_m23373362_gshared (Nullable_1_t708647570 * __this, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m23373362(__this, method) ((  int32_t (*) (Nullable_1_t708647570 *, const MethodInfo*))Nullable_1_GetValueOrDefault_m23373362_gshared)(__this, method)
// T System.Nullable`1<ThirdParty.Json.LitJson.JsonToken>::GetValueOrDefault(T)
extern "C"  int32_t Nullable_1_GetValueOrDefault_m474156280_gshared (Nullable_1_t708647570 * __this, int32_t ___defaultValue0, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m474156280(__this, ___defaultValue0, method) ((  int32_t (*) (Nullable_1_t708647570 *, int32_t, const MethodInfo*))Nullable_1_GetValueOrDefault_m474156280_gshared)(__this, ___defaultValue0, method)
// System.String System.Nullable`1<ThirdParty.Json.LitJson.JsonToken>::ToString()
extern "C"  String_t* Nullable_1_ToString_m2481691095_gshared (Nullable_1_t708647570 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m2481691095(__this, method) ((  String_t* (*) (Nullable_1_t708647570 *, const MethodInfo*))Nullable_1_ToString_m2481691095_gshared)(__this, method)
