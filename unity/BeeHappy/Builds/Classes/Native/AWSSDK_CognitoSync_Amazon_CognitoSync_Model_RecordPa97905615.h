﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// Amazon.CognitoSync.Operation
struct Operation_t3836154307;

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Nullable_1_gen3251239280.h"
#include "mscorlib_System_Nullable_1_gen3467111648.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.CognitoSync.Model.RecordPatch
struct  RecordPatch_t97905615  : public Il2CppObject
{
public:
	// System.Nullable`1<System.DateTime> Amazon.CognitoSync.Model.RecordPatch::_deviceLastModifiedDate
	Nullable_1_t3251239280  ____deviceLastModifiedDate_0;
	// System.String Amazon.CognitoSync.Model.RecordPatch::_key
	String_t* ____key_1;
	// Amazon.CognitoSync.Operation Amazon.CognitoSync.Model.RecordPatch::_op
	Operation_t3836154307 * ____op_2;
	// System.Nullable`1<System.Int64> Amazon.CognitoSync.Model.RecordPatch::_syncCount
	Nullable_1_t3467111648  ____syncCount_3;
	// System.String Amazon.CognitoSync.Model.RecordPatch::_value
	String_t* ____value_4;

public:
	inline static int32_t get_offset_of__deviceLastModifiedDate_0() { return static_cast<int32_t>(offsetof(RecordPatch_t97905615, ____deviceLastModifiedDate_0)); }
	inline Nullable_1_t3251239280  get__deviceLastModifiedDate_0() const { return ____deviceLastModifiedDate_0; }
	inline Nullable_1_t3251239280 * get_address_of__deviceLastModifiedDate_0() { return &____deviceLastModifiedDate_0; }
	inline void set__deviceLastModifiedDate_0(Nullable_1_t3251239280  value)
	{
		____deviceLastModifiedDate_0 = value;
	}

	inline static int32_t get_offset_of__key_1() { return static_cast<int32_t>(offsetof(RecordPatch_t97905615, ____key_1)); }
	inline String_t* get__key_1() const { return ____key_1; }
	inline String_t** get_address_of__key_1() { return &____key_1; }
	inline void set__key_1(String_t* value)
	{
		____key_1 = value;
		Il2CppCodeGenWriteBarrier(&____key_1, value);
	}

	inline static int32_t get_offset_of__op_2() { return static_cast<int32_t>(offsetof(RecordPatch_t97905615, ____op_2)); }
	inline Operation_t3836154307 * get__op_2() const { return ____op_2; }
	inline Operation_t3836154307 ** get_address_of__op_2() { return &____op_2; }
	inline void set__op_2(Operation_t3836154307 * value)
	{
		____op_2 = value;
		Il2CppCodeGenWriteBarrier(&____op_2, value);
	}

	inline static int32_t get_offset_of__syncCount_3() { return static_cast<int32_t>(offsetof(RecordPatch_t97905615, ____syncCount_3)); }
	inline Nullable_1_t3467111648  get__syncCount_3() const { return ____syncCount_3; }
	inline Nullable_1_t3467111648 * get_address_of__syncCount_3() { return &____syncCount_3; }
	inline void set__syncCount_3(Nullable_1_t3467111648  value)
	{
		____syncCount_3 = value;
	}

	inline static int32_t get_offset_of__value_4() { return static_cast<int32_t>(offsetof(RecordPatch_t97905615, ____value_4)); }
	inline String_t* get__value_4() const { return ____value_4; }
	inline String_t** get_address_of__value_4() { return &____value_4; }
	inline void set__value_4(String_t* value)
	{
		____value_4 = value;
		Il2CppCodeGenWriteBarrier(&____value_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
