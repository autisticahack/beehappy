﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.String,Amazon.RegionEndpoint>
struct Dictionary_2_t2576302067;
// Amazon.RegionEndpoint
struct RegionEndpoint_t661522805;
// Amazon.Internal.IRegionEndpointProvider
struct IRegionEndpointProvider_t2524767261;
// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.RegionEndpoint
struct  RegionEndpoint_t661522805  : public Il2CppObject
{
public:
	// System.String Amazon.RegionEndpoint::<SystemName>k__BackingField
	String_t* ___U3CSystemNameU3Ek__BackingField_16;
	// System.String Amazon.RegionEndpoint::<DisplayName>k__BackingField
	String_t* ___U3CDisplayNameU3Ek__BackingField_17;

public:
	inline static int32_t get_offset_of_U3CSystemNameU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(RegionEndpoint_t661522805, ___U3CSystemNameU3Ek__BackingField_16)); }
	inline String_t* get_U3CSystemNameU3Ek__BackingField_16() const { return ___U3CSystemNameU3Ek__BackingField_16; }
	inline String_t** get_address_of_U3CSystemNameU3Ek__BackingField_16() { return &___U3CSystemNameU3Ek__BackingField_16; }
	inline void set_U3CSystemNameU3Ek__BackingField_16(String_t* value)
	{
		___U3CSystemNameU3Ek__BackingField_16 = value;
		Il2CppCodeGenWriteBarrier(&___U3CSystemNameU3Ek__BackingField_16, value);
	}

	inline static int32_t get_offset_of_U3CDisplayNameU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(RegionEndpoint_t661522805, ___U3CDisplayNameU3Ek__BackingField_17)); }
	inline String_t* get_U3CDisplayNameU3Ek__BackingField_17() const { return ___U3CDisplayNameU3Ek__BackingField_17; }
	inline String_t** get_address_of_U3CDisplayNameU3Ek__BackingField_17() { return &___U3CDisplayNameU3Ek__BackingField_17; }
	inline void set_U3CDisplayNameU3Ek__BackingField_17(String_t* value)
	{
		___U3CDisplayNameU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3CDisplayNameU3Ek__BackingField_17, value);
	}
};

struct RegionEndpoint_t661522805_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,Amazon.RegionEndpoint> Amazon.RegionEndpoint::_hashBySystemName
	Dictionary_2_t2576302067 * ____hashBySystemName_0;
	// Amazon.RegionEndpoint Amazon.RegionEndpoint::USEast1
	RegionEndpoint_t661522805 * ___USEast1_1;
	// Amazon.RegionEndpoint Amazon.RegionEndpoint::USEast2
	RegionEndpoint_t661522805 * ___USEast2_2;
	// Amazon.RegionEndpoint Amazon.RegionEndpoint::USWest1
	RegionEndpoint_t661522805 * ___USWest1_3;
	// Amazon.RegionEndpoint Amazon.RegionEndpoint::USWest2
	RegionEndpoint_t661522805 * ___USWest2_4;
	// Amazon.RegionEndpoint Amazon.RegionEndpoint::EUWest1
	RegionEndpoint_t661522805 * ___EUWest1_5;
	// Amazon.RegionEndpoint Amazon.RegionEndpoint::EUCentral1
	RegionEndpoint_t661522805 * ___EUCentral1_6;
	// Amazon.RegionEndpoint Amazon.RegionEndpoint::APNortheast1
	RegionEndpoint_t661522805 * ___APNortheast1_7;
	// Amazon.RegionEndpoint Amazon.RegionEndpoint::APNortheast2
	RegionEndpoint_t661522805 * ___APNortheast2_8;
	// Amazon.RegionEndpoint Amazon.RegionEndpoint::APSouth1
	RegionEndpoint_t661522805 * ___APSouth1_9;
	// Amazon.RegionEndpoint Amazon.RegionEndpoint::APSoutheast1
	RegionEndpoint_t661522805 * ___APSoutheast1_10;
	// Amazon.RegionEndpoint Amazon.RegionEndpoint::APSoutheast2
	RegionEndpoint_t661522805 * ___APSoutheast2_11;
	// Amazon.RegionEndpoint Amazon.RegionEndpoint::SAEast1
	RegionEndpoint_t661522805 * ___SAEast1_12;
	// Amazon.RegionEndpoint Amazon.RegionEndpoint::USGovCloudWest1
	RegionEndpoint_t661522805 * ___USGovCloudWest1_13;
	// Amazon.RegionEndpoint Amazon.RegionEndpoint::CNNorth1
	RegionEndpoint_t661522805 * ___CNNorth1_14;
	// Amazon.Internal.IRegionEndpointProvider Amazon.RegionEndpoint::_regionEndpointProvider
	Il2CppObject * ____regionEndpointProvider_15;

public:
	inline static int32_t get_offset_of__hashBySystemName_0() { return static_cast<int32_t>(offsetof(RegionEndpoint_t661522805_StaticFields, ____hashBySystemName_0)); }
	inline Dictionary_2_t2576302067 * get__hashBySystemName_0() const { return ____hashBySystemName_0; }
	inline Dictionary_2_t2576302067 ** get_address_of__hashBySystemName_0() { return &____hashBySystemName_0; }
	inline void set__hashBySystemName_0(Dictionary_2_t2576302067 * value)
	{
		____hashBySystemName_0 = value;
		Il2CppCodeGenWriteBarrier(&____hashBySystemName_0, value);
	}

	inline static int32_t get_offset_of_USEast1_1() { return static_cast<int32_t>(offsetof(RegionEndpoint_t661522805_StaticFields, ___USEast1_1)); }
	inline RegionEndpoint_t661522805 * get_USEast1_1() const { return ___USEast1_1; }
	inline RegionEndpoint_t661522805 ** get_address_of_USEast1_1() { return &___USEast1_1; }
	inline void set_USEast1_1(RegionEndpoint_t661522805 * value)
	{
		___USEast1_1 = value;
		Il2CppCodeGenWriteBarrier(&___USEast1_1, value);
	}

	inline static int32_t get_offset_of_USEast2_2() { return static_cast<int32_t>(offsetof(RegionEndpoint_t661522805_StaticFields, ___USEast2_2)); }
	inline RegionEndpoint_t661522805 * get_USEast2_2() const { return ___USEast2_2; }
	inline RegionEndpoint_t661522805 ** get_address_of_USEast2_2() { return &___USEast2_2; }
	inline void set_USEast2_2(RegionEndpoint_t661522805 * value)
	{
		___USEast2_2 = value;
		Il2CppCodeGenWriteBarrier(&___USEast2_2, value);
	}

	inline static int32_t get_offset_of_USWest1_3() { return static_cast<int32_t>(offsetof(RegionEndpoint_t661522805_StaticFields, ___USWest1_3)); }
	inline RegionEndpoint_t661522805 * get_USWest1_3() const { return ___USWest1_3; }
	inline RegionEndpoint_t661522805 ** get_address_of_USWest1_3() { return &___USWest1_3; }
	inline void set_USWest1_3(RegionEndpoint_t661522805 * value)
	{
		___USWest1_3 = value;
		Il2CppCodeGenWriteBarrier(&___USWest1_3, value);
	}

	inline static int32_t get_offset_of_USWest2_4() { return static_cast<int32_t>(offsetof(RegionEndpoint_t661522805_StaticFields, ___USWest2_4)); }
	inline RegionEndpoint_t661522805 * get_USWest2_4() const { return ___USWest2_4; }
	inline RegionEndpoint_t661522805 ** get_address_of_USWest2_4() { return &___USWest2_4; }
	inline void set_USWest2_4(RegionEndpoint_t661522805 * value)
	{
		___USWest2_4 = value;
		Il2CppCodeGenWriteBarrier(&___USWest2_4, value);
	}

	inline static int32_t get_offset_of_EUWest1_5() { return static_cast<int32_t>(offsetof(RegionEndpoint_t661522805_StaticFields, ___EUWest1_5)); }
	inline RegionEndpoint_t661522805 * get_EUWest1_5() const { return ___EUWest1_5; }
	inline RegionEndpoint_t661522805 ** get_address_of_EUWest1_5() { return &___EUWest1_5; }
	inline void set_EUWest1_5(RegionEndpoint_t661522805 * value)
	{
		___EUWest1_5 = value;
		Il2CppCodeGenWriteBarrier(&___EUWest1_5, value);
	}

	inline static int32_t get_offset_of_EUCentral1_6() { return static_cast<int32_t>(offsetof(RegionEndpoint_t661522805_StaticFields, ___EUCentral1_6)); }
	inline RegionEndpoint_t661522805 * get_EUCentral1_6() const { return ___EUCentral1_6; }
	inline RegionEndpoint_t661522805 ** get_address_of_EUCentral1_6() { return &___EUCentral1_6; }
	inline void set_EUCentral1_6(RegionEndpoint_t661522805 * value)
	{
		___EUCentral1_6 = value;
		Il2CppCodeGenWriteBarrier(&___EUCentral1_6, value);
	}

	inline static int32_t get_offset_of_APNortheast1_7() { return static_cast<int32_t>(offsetof(RegionEndpoint_t661522805_StaticFields, ___APNortheast1_7)); }
	inline RegionEndpoint_t661522805 * get_APNortheast1_7() const { return ___APNortheast1_7; }
	inline RegionEndpoint_t661522805 ** get_address_of_APNortheast1_7() { return &___APNortheast1_7; }
	inline void set_APNortheast1_7(RegionEndpoint_t661522805 * value)
	{
		___APNortheast1_7 = value;
		Il2CppCodeGenWriteBarrier(&___APNortheast1_7, value);
	}

	inline static int32_t get_offset_of_APNortheast2_8() { return static_cast<int32_t>(offsetof(RegionEndpoint_t661522805_StaticFields, ___APNortheast2_8)); }
	inline RegionEndpoint_t661522805 * get_APNortheast2_8() const { return ___APNortheast2_8; }
	inline RegionEndpoint_t661522805 ** get_address_of_APNortheast2_8() { return &___APNortheast2_8; }
	inline void set_APNortheast2_8(RegionEndpoint_t661522805 * value)
	{
		___APNortheast2_8 = value;
		Il2CppCodeGenWriteBarrier(&___APNortheast2_8, value);
	}

	inline static int32_t get_offset_of_APSouth1_9() { return static_cast<int32_t>(offsetof(RegionEndpoint_t661522805_StaticFields, ___APSouth1_9)); }
	inline RegionEndpoint_t661522805 * get_APSouth1_9() const { return ___APSouth1_9; }
	inline RegionEndpoint_t661522805 ** get_address_of_APSouth1_9() { return &___APSouth1_9; }
	inline void set_APSouth1_9(RegionEndpoint_t661522805 * value)
	{
		___APSouth1_9 = value;
		Il2CppCodeGenWriteBarrier(&___APSouth1_9, value);
	}

	inline static int32_t get_offset_of_APSoutheast1_10() { return static_cast<int32_t>(offsetof(RegionEndpoint_t661522805_StaticFields, ___APSoutheast1_10)); }
	inline RegionEndpoint_t661522805 * get_APSoutheast1_10() const { return ___APSoutheast1_10; }
	inline RegionEndpoint_t661522805 ** get_address_of_APSoutheast1_10() { return &___APSoutheast1_10; }
	inline void set_APSoutheast1_10(RegionEndpoint_t661522805 * value)
	{
		___APSoutheast1_10 = value;
		Il2CppCodeGenWriteBarrier(&___APSoutheast1_10, value);
	}

	inline static int32_t get_offset_of_APSoutheast2_11() { return static_cast<int32_t>(offsetof(RegionEndpoint_t661522805_StaticFields, ___APSoutheast2_11)); }
	inline RegionEndpoint_t661522805 * get_APSoutheast2_11() const { return ___APSoutheast2_11; }
	inline RegionEndpoint_t661522805 ** get_address_of_APSoutheast2_11() { return &___APSoutheast2_11; }
	inline void set_APSoutheast2_11(RegionEndpoint_t661522805 * value)
	{
		___APSoutheast2_11 = value;
		Il2CppCodeGenWriteBarrier(&___APSoutheast2_11, value);
	}

	inline static int32_t get_offset_of_SAEast1_12() { return static_cast<int32_t>(offsetof(RegionEndpoint_t661522805_StaticFields, ___SAEast1_12)); }
	inline RegionEndpoint_t661522805 * get_SAEast1_12() const { return ___SAEast1_12; }
	inline RegionEndpoint_t661522805 ** get_address_of_SAEast1_12() { return &___SAEast1_12; }
	inline void set_SAEast1_12(RegionEndpoint_t661522805 * value)
	{
		___SAEast1_12 = value;
		Il2CppCodeGenWriteBarrier(&___SAEast1_12, value);
	}

	inline static int32_t get_offset_of_USGovCloudWest1_13() { return static_cast<int32_t>(offsetof(RegionEndpoint_t661522805_StaticFields, ___USGovCloudWest1_13)); }
	inline RegionEndpoint_t661522805 * get_USGovCloudWest1_13() const { return ___USGovCloudWest1_13; }
	inline RegionEndpoint_t661522805 ** get_address_of_USGovCloudWest1_13() { return &___USGovCloudWest1_13; }
	inline void set_USGovCloudWest1_13(RegionEndpoint_t661522805 * value)
	{
		___USGovCloudWest1_13 = value;
		Il2CppCodeGenWriteBarrier(&___USGovCloudWest1_13, value);
	}

	inline static int32_t get_offset_of_CNNorth1_14() { return static_cast<int32_t>(offsetof(RegionEndpoint_t661522805_StaticFields, ___CNNorth1_14)); }
	inline RegionEndpoint_t661522805 * get_CNNorth1_14() const { return ___CNNorth1_14; }
	inline RegionEndpoint_t661522805 ** get_address_of_CNNorth1_14() { return &___CNNorth1_14; }
	inline void set_CNNorth1_14(RegionEndpoint_t661522805 * value)
	{
		___CNNorth1_14 = value;
		Il2CppCodeGenWriteBarrier(&___CNNorth1_14, value);
	}

	inline static int32_t get_offset_of__regionEndpointProvider_15() { return static_cast<int32_t>(offsetof(RegionEndpoint_t661522805_StaticFields, ____regionEndpointProvider_15)); }
	inline Il2CppObject * get__regionEndpointProvider_15() const { return ____regionEndpointProvider_15; }
	inline Il2CppObject ** get_address_of__regionEndpointProvider_15() { return &____regionEndpointProvider_15; }
	inline void set__regionEndpointProvider_15(Il2CppObject * value)
	{
		____regionEndpointProvider_15 = value;
		Il2CppCodeGenWriteBarrier(&____regionEndpointProvider_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
