﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.AsyncOptions
struct  AsyncOptions_t558351272  : public Il2CppObject
{
public:
	// System.Boolean Amazon.Runtime.AsyncOptions::<ExecuteCallbackOnMainThread>k__BackingField
	bool ___U3CExecuteCallbackOnMainThreadU3Ek__BackingField_0;
	// System.Object Amazon.Runtime.AsyncOptions::<State>k__BackingField
	Il2CppObject * ___U3CStateU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CExecuteCallbackOnMainThreadU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AsyncOptions_t558351272, ___U3CExecuteCallbackOnMainThreadU3Ek__BackingField_0)); }
	inline bool get_U3CExecuteCallbackOnMainThreadU3Ek__BackingField_0() const { return ___U3CExecuteCallbackOnMainThreadU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CExecuteCallbackOnMainThreadU3Ek__BackingField_0() { return &___U3CExecuteCallbackOnMainThreadU3Ek__BackingField_0; }
	inline void set_U3CExecuteCallbackOnMainThreadU3Ek__BackingField_0(bool value)
	{
		___U3CExecuteCallbackOnMainThreadU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CStateU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AsyncOptions_t558351272, ___U3CStateU3Ek__BackingField_1)); }
	inline Il2CppObject * get_U3CStateU3Ek__BackingField_1() const { return ___U3CStateU3Ek__BackingField_1; }
	inline Il2CppObject ** get_address_of_U3CStateU3Ek__BackingField_1() { return &___U3CStateU3Ek__BackingField_1; }
	inline void set_U3CStateU3Ek__BackingField_1(Il2CppObject * value)
	{
		___U3CStateU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CStateU3Ek__BackingField_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
