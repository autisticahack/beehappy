﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<ThirdParty.Json.LitJson.JsonToken>
struct DefaultComparer_t4067511416;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_JsonToken2445581255.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<ThirdParty.Json.LitJson.JsonToken>::.ctor()
extern "C"  void DefaultComparer__ctor_m3404924597_gshared (DefaultComparer_t4067511416 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m3404924597(__this, method) ((  void (*) (DefaultComparer_t4067511416 *, const MethodInfo*))DefaultComparer__ctor_m3404924597_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<ThirdParty.Json.LitJson.JsonToken>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m4229977524_gshared (DefaultComparer_t4067511416 * __this, int32_t ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m4229977524(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t4067511416 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m4229977524_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<ThirdParty.Json.LitJson.JsonToken>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m772055804_gshared (DefaultComparer_t4067511416 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m772055804(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t4067511416 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m772055804_gshared)(__this, ___x0, ___y1, method)
