﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// Amazon.CognitoIdentity.CognitoAWSCredentials
struct CognitoAWSCredentials_t2370264792;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Cogn2370264792.h"
#include "mscorlib_System_DateTime693205669.h"

// System.String Amazon.CognitoSync.SyncManager.Internal.DatasetUtils::ValidateDatasetName(System.String)
extern "C"  String_t* DatasetUtils_ValidateDatasetName_m880612806 (Il2CppObject * __this /* static, unused */, String_t* ___datasetName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.CognitoSync.SyncManager.Internal.DatasetUtils::ValidateRecordKey(System.String)
extern "C"  String_t* DatasetUtils_ValidateRecordKey_m1810549771 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.CognitoSync.SyncManager.Internal.DatasetUtils::GetIdentityId(Amazon.CognitoIdentity.CognitoAWSCredentials)
extern "C"  String_t* DatasetUtils_GetIdentityId_m4075555153 (Il2CppObject * __this /* static, unused */, CognitoAWSCredentials_t2370264792 * ___credentials0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime Amazon.CognitoSync.SyncManager.Internal.DatasetUtils::TruncateToSeconds(System.DateTime)
extern "C"  DateTime_t693205669  DatasetUtils_TruncateToSeconds_m1844621093 (Il2CppObject * __this /* static, unused */, DateTime_t693205669  ___dateTime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.SyncManager.Internal.DatasetUtils::.cctor()
extern "C"  void DatasetUtils__cctor_m2623949475 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
