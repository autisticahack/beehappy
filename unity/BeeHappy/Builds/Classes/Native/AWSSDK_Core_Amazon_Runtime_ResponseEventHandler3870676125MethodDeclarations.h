﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.ResponseEventHandler
struct ResponseEventHandler_t3870676125;
// System.Object
struct Il2CppObject;
// Amazon.Runtime.ResponseEventArgs
struct ResponseEventArgs_t4056063878;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AWSSDK_Core_Amazon_Runtime_ResponseEventArgs4056063878.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Amazon.Runtime.ResponseEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void ResponseEventHandler__ctor_m4185686810 (ResponseEventHandler_t3870676125 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.ResponseEventHandler::Invoke(System.Object,Amazon.Runtime.ResponseEventArgs)
extern "C"  void ResponseEventHandler_Invoke_m654503410 (ResponseEventHandler_t3870676125 * __this, Il2CppObject * ___sender0, ResponseEventArgs_t4056063878 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Amazon.Runtime.ResponseEventHandler::BeginInvoke(System.Object,Amazon.Runtime.ResponseEventArgs,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ResponseEventHandler_BeginInvoke_m557444585 (ResponseEventHandler_t3870676125 * __this, Il2CppObject * ___sender0, ResponseEventArgs_t4056063878 * ___e1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.ResponseEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void ResponseEventHandler_EndInvoke_m2089855668 (ResponseEventHandler_t3870676125 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
