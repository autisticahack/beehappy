﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2281509423MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Type,Amazon.Runtime.Internal.Util.Logger>::.ctor()
#define Dictionary_2__ctor_m253536238(__this, method) ((  void (*) (Dictionary_2_t4199855711 *, const MethodInfo*))Dictionary_2__ctor_m584589095_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,Amazon.Runtime.Internal.Util.Logger>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m1178791260(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t4199855711 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m406310120_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,Amazon.Runtime.Internal.Util.Logger>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
#define Dictionary_2__ctor_m2361391259(__this, ___dictionary0, method) ((  void (*) (Dictionary_2_t4199855711 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2602799901_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,Amazon.Runtime.Internal.Util.Logger>::.ctor(System.Int32)
#define Dictionary_2__ctor_m1563486016(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t4199855711 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m206582704_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,Amazon.Runtime.Internal.Util.Logger>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m2633345520(__this, ___dictionary0, ___comparer1, method) ((  void (*) (Dictionary_2_t4199855711 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m3143729840_gshared)(__this, ___dictionary0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,Amazon.Runtime.Internal.Util.Logger>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2__ctor_m1445317930(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t4199855711 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2__ctor_m1206668798_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Type,Amazon.Runtime.Internal.Util.Logger>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1519244757(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t4199855711 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m853262843_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Type,Amazon.Runtime.Internal.Util.Logger>::System.Collections.IDictionary.get_Keys()
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m75399287(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t4199855711 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m673000885_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Type,Amazon.Runtime.Internal.Util.Logger>::System.Collections.IDictionary.get_Values()
#define Dictionary_2_System_Collections_IDictionary_get_Values_m782904959(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t4199855711 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m1552474645_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Type,Amazon.Runtime.Internal.Util.Logger>::System.Collections.IDictionary.get_Item(System.Object)
#define Dictionary_2_System_Collections_IDictionary_get_Item_m1221520165(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t4199855711 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m237963271_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,Amazon.Runtime.Internal.Util.Logger>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_set_Item_m3322616834(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t4199855711 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m3775521570_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,Amazon.Runtime.Internal.Util.Logger>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_Add_m515960467(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t4199855711 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m984276885_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,Amazon.Runtime.Internal.Util.Logger>::System.Collections.IDictionary.Contains(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Contains_m1325950023(__this, ___key0, method) ((  bool (*) (Dictionary_2_t4199855711 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m2868006769_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,Amazon.Runtime.Internal.Util.Logger>::System.Collections.IDictionary.Remove(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Remove_m1792004750(__this, ___key0, method) ((  void (*) (Dictionary_2_t4199855711 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m2017099222_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,Amazon.Runtime.Internal.Util.Logger>::System.Collections.ICollection.get_IsSynchronized()
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m2293945817(__this, method) ((  bool (*) (Dictionary_2_t4199855711 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m960517203_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Type,Amazon.Runtime.Internal.Util.Logger>::System.Collections.ICollection.get_SyncRoot()
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m6426333(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t4199855711 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1900166091_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,Amazon.Runtime.Internal.Util.Logger>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1661150095(__this, method) ((  bool (*) (Dictionary_2_t4199855711 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m4094240197_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,Amazon.Runtime.Internal.Util.Logger>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m129657060(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t4199855711 *, KeyValuePair_2_t1957200933 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m990341268_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,Amazon.Runtime.Internal.Util.Logger>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1818263632(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t4199855711 *, KeyValuePair_2_t1957200933 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1058501024_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,Amazon.Runtime.Internal.Util.Logger>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m3673287992(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t4199855711 *, KeyValuePair_2U5BU5D_t2280289704*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m976354816_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,Amazon.Runtime.Internal.Util.Logger>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1164997653(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t4199855711 *, KeyValuePair_2_t1957200933 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1705959559_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,Amazon.Runtime.Internal.Util.Logger>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Dictionary_2_System_Collections_ICollection_CopyTo_m248949341(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t4199855711 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m3578539931_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Type,Amazon.Runtime.Internal.Util.Logger>::System.Collections.IEnumerable.GetEnumerator()
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m2741101334(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t4199855711 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3100111910_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Type,Amazon.Runtime.Internal.Util.Logger>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3048668471(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t4199855711 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2925090477_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Type,Amazon.Runtime.Internal.Util.Logger>::System.Collections.IDictionary.GetEnumerator()
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1629905340(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t4199855711 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2684932776_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Type,Amazon.Runtime.Internal.Util.Logger>::get_Count()
#define Dictionary_2_get_Count_m3093757305(__this, method) ((  int32_t (*) (Dictionary_2_t4199855711 *, const MethodInfo*))Dictionary_2_get_Count_m3636113691_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Type,Amazon.Runtime.Internal.Util.Logger>::get_Item(TKey)
#define Dictionary_2_get_Item_m621895988(__this, ___key0, method) ((  Logger_t2262497814 * (*) (Dictionary_2_t4199855711 *, Type_t *, const MethodInfo*))Dictionary_2_get_Item_m2413909512_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,Amazon.Runtime.Internal.Util.Logger>::set_Item(TKey,TValue)
#define Dictionary_2_set_Item_m2686690333(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t4199855711 *, Type_t *, Logger_t2262497814 *, const MethodInfo*))Dictionary_2_set_Item_m458653679_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,Amazon.Runtime.Internal.Util.Logger>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2_Init_m4198712537(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t4199855711 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m1045257495_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,Amazon.Runtime.Internal.Util.Logger>::InitArrays(System.Int32)
#define Dictionary_2_InitArrays_m3875869008(__this, ___size0, method) ((  void (*) (Dictionary_2_t4199855711 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m2270022740_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,Amazon.Runtime.Internal.Util.Logger>::CopyToCheck(System.Array,System.Int32)
#define Dictionary_2_CopyToCheck_m1830505894(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t4199855711 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m2147716750_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Type,Amazon.Runtime.Internal.Util.Logger>::make_pair(TKey,TValue)
#define Dictionary_2_make_pair_m566870740(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t1957200933  (*) (Il2CppObject * /* static, unused */, Type_t *, Logger_t2262497814 *, const MethodInfo*))Dictionary_2_make_pair_m2631942124_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<System.Type,Amazon.Runtime.Internal.Util.Logger>::pick_key(TKey,TValue)
#define Dictionary_2_pick_key_m1206862046(__this /* static, unused */, ___key0, ___value1, method) ((  Type_t * (*) (Il2CppObject * /* static, unused */, Type_t *, Logger_t2262497814 *, const MethodInfo*))Dictionary_2_pick_key_m2840829442_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<System.Type,Amazon.Runtime.Internal.Util.Logger>::pick_value(TKey,TValue)
#define Dictionary_2_pick_value_m2447795278(__this /* static, unused */, ___key0, ___value1, method) ((  Logger_t2262497814 * (*) (Il2CppObject * /* static, unused */, Type_t *, Logger_t2262497814 *, const MethodInfo*))Dictionary_2_pick_value_m1872663242_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,Amazon.Runtime.Internal.Util.Logger>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_CopyTo_m423873297(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t4199855711 *, KeyValuePair_2U5BU5D_t2280289704*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m1495142643_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,Amazon.Runtime.Internal.Util.Logger>::Resize()
#define Dictionary_2_Resize_m4056478219(__this, method) ((  void (*) (Dictionary_2_t4199855711 *, const MethodInfo*))Dictionary_2_Resize_m2672264133_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,Amazon.Runtime.Internal.Util.Logger>::Add(TKey,TValue)
#define Dictionary_2_Add_m67990196(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t4199855711 *, Type_t *, Logger_t2262497814 *, const MethodInfo*))Dictionary_2_Add_m1708621268_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,Amazon.Runtime.Internal.Util.Logger>::Clear()
#define Dictionary_2_Clear_m3708362112(__this, method) ((  void (*) (Dictionary_2_t4199855711 *, const MethodInfo*))Dictionary_2_Clear_m2325793156_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,Amazon.Runtime.Internal.Util.Logger>::ContainsKey(TKey)
#define Dictionary_2_ContainsKey_m527878876(__this, ___key0, method) ((  bool (*) (Dictionary_2_t4199855711 *, Type_t *, const MethodInfo*))Dictionary_2_ContainsKey_m3553426152_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,Amazon.Runtime.Internal.Util.Logger>::ContainsValue(TValue)
#define Dictionary_2_ContainsValue_m2102500908(__this, ___value0, method) ((  bool (*) (Dictionary_2_t4199855711 *, Logger_t2262497814 *, const MethodInfo*))Dictionary_2_ContainsValue_m2375979648_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,Amazon.Runtime.Internal.Util.Logger>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2_GetObjectData_m2679951377(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t4199855711 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2_GetObjectData_m2864531407_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,Amazon.Runtime.Internal.Util.Logger>::OnDeserialization(System.Object)
#define Dictionary_2_OnDeserialization_m912593005(__this, ___sender0, method) ((  void (*) (Dictionary_2_t4199855711 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m2160537783_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,Amazon.Runtime.Internal.Util.Logger>::Remove(TKey)
#define Dictionary_2_Remove_m1783339816(__this, ___key0, method) ((  bool (*) (Dictionary_2_t4199855711 *, Type_t *, const MethodInfo*))Dictionary_2_Remove_m1366616528_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,Amazon.Runtime.Internal.Util.Logger>::TryGetValue(TKey,TValue&)
#define Dictionary_2_TryGetValue_m3171806045(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t4199855711 *, Type_t *, Logger_t2262497814 **, const MethodInfo*))Dictionary_2_TryGetValue_m1120370623_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Type,Amazon.Runtime.Internal.Util.Logger>::get_Keys()
#define Dictionary_2_get_Keys_m899103400(__this, method) ((  KeyCollection_t2388386186 * (*) (Dictionary_2_t4199855711 *, const MethodInfo*))Dictionary_2_get_Keys_m1635778172_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Type,Amazon.Runtime.Internal.Util.Logger>::get_Values()
#define Dictionary_2_get_Values_m387792992(__this, method) ((  ValueCollection_t2902915554 * (*) (Dictionary_2_t4199855711 *, const MethodInfo*))Dictionary_2_get_Values_m825860460_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Type,Amazon.Runtime.Internal.Util.Logger>::ToTKey(System.Object)
#define Dictionary_2_ToTKey_m3713800875(__this, ___key0, method) ((  Type_t * (*) (Dictionary_2_t4199855711 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m4209561517_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<System.Type,Amazon.Runtime.Internal.Util.Logger>::ToTValue(System.Object)
#define Dictionary_2_ToTValue_m405586883(__this, ___value0, method) ((  Logger_t2262497814 * (*) (Dictionary_2_t4199855711 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m1381983709_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,Amazon.Runtime.Internal.Util.Logger>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_ContainsKeyValuePair_m1210243701(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t4199855711 *, KeyValuePair_2_t1957200933 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m663697471_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Type,Amazon.Runtime.Internal.Util.Logger>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m3821913852(__this, method) ((  Enumerator_t1224913117  (*) (Dictionary_2_t4199855711 *, const MethodInfo*))Dictionary_2_GetEnumerator_m1752238884_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Type,Amazon.Runtime.Internal.Util.Logger>::<CopyTo>m__0(TKey,TValue)
#define Dictionary_2_U3CCopyToU3Em__0_m945934619(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Il2CppObject * /* static, unused */, Type_t *, Logger_t2262497814 *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m2061238213_gshared)(__this /* static, unused */, ___key0, ___value1, method)
