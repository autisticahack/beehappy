﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.CognitoSync.SyncManager.Record
struct Record_t868799569;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen3251239280.h"
#include "mscorlib_System_String2029220233.h"

// System.String Amazon.CognitoSync.SyncManager.Record::get_Key()
extern "C"  String_t* Record_get_Key_m182125074 (Record_t868799569 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.CognitoSync.SyncManager.Record::get_Value()
extern "C"  String_t* Record_get_Value_m3387724188 (Record_t868799569 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Amazon.CognitoSync.SyncManager.Record::get_SyncCount()
extern "C"  int64_t Record_get_SyncCount_m2492350597 (Record_t868799569 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.DateTime> Amazon.CognitoSync.SyncManager.Record::get_LastModifiedDate()
extern "C"  Nullable_1_t3251239280  Record_get_LastModifiedDate_m2992436823 (Record_t868799569 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.CognitoSync.SyncManager.Record::get_LastModifiedBy()
extern "C"  String_t* Record_get_LastModifiedBy_m1198620169 (Record_t868799569 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.DateTime> Amazon.CognitoSync.SyncManager.Record::get_DeviceLastModifiedDate()
extern "C"  Nullable_1_t3251239280  Record_get_DeviceLastModifiedDate_m2091441875 (Record_t868799569 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.CognitoSync.SyncManager.Record::get_IsModified()
extern "C"  bool Record_get_IsModified_m445098847 (Record_t868799569 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.SyncManager.Record::.ctor(System.String,System.String,System.Int64,System.Nullable`1<System.DateTime>,System.String,System.Nullable`1<System.DateTime>,System.Boolean)
extern "C"  void Record__ctor_m1397471016 (Record_t868799569 * __this, String_t* ___key0, String_t* ___value1, int64_t ___syncCount2, Nullable_1_t3251239280  ___lastModifiedDate3, String_t* ___lastModifiedBy4, Nullable_1_t3251239280  ___deviceLastModifiedDate5, bool ___modified6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.CognitoSync.SyncManager.Record::ToString()
extern "C"  String_t* Record_ToString_m3458188560 (Record_t868799569 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
