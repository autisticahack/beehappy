﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AWSSDK_CognitoSync_Amazon_CognitoSync_SyncManager_4165705311.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.CognitoSync.SyncManager.DataStorageException
struct  DataStorageException_t1234943264  : public SyncManagerException_t4165705311
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
