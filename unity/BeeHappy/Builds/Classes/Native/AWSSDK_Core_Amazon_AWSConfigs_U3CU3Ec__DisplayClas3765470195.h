﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Threading.ManualResetEvent
struct ManualResetEvent_t926074657;
// Amazon.AWSConfigs/<>c__DisplayClass89_0
struct U3CU3Ec__DisplayClass89_0_t3765470194;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.AWSConfigs/<>c__DisplayClass89_1
struct  U3CU3Ec__DisplayClass89_1_t3765470195  : public Il2CppObject
{
public:
	// System.Threading.ManualResetEvent Amazon.AWSConfigs/<>c__DisplayClass89_1::e
	ManualResetEvent_t926074657 * ___e_0;
	// Amazon.AWSConfigs/<>c__DisplayClass89_0 Amazon.AWSConfigs/<>c__DisplayClass89_1::CS$<>8__locals1
	U3CU3Ec__DisplayClass89_0_t3765470194 * ___CSU24U3CU3E8__locals1_1;

public:
	inline static int32_t get_offset_of_e_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass89_1_t3765470195, ___e_0)); }
	inline ManualResetEvent_t926074657 * get_e_0() const { return ___e_0; }
	inline ManualResetEvent_t926074657 ** get_address_of_e_0() { return &___e_0; }
	inline void set_e_0(ManualResetEvent_t926074657 * value)
	{
		___e_0 = value;
		Il2CppCodeGenWriteBarrier(&___e_0, value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E8__locals1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass89_1_t3765470195, ___CSU24U3CU3E8__locals1_1)); }
	inline U3CU3Ec__DisplayClass89_0_t3765470194 * get_CSU24U3CU3E8__locals1_1() const { return ___CSU24U3CU3E8__locals1_1; }
	inline U3CU3Ec__DisplayClass89_0_t3765470194 ** get_address_of_CSU24U3CU3E8__locals1_1() { return &___CSU24U3CU3E8__locals1_1; }
	inline void set_CSU24U3CU3E8__locals1_1(U3CU3Ec__DisplayClass89_0_t3765470194 * value)
	{
		___CSU24U3CU3E8__locals1_1 = value;
		Il2CppCodeGenWriteBarrier(&___CSU24U3CU3E8__locals1_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
