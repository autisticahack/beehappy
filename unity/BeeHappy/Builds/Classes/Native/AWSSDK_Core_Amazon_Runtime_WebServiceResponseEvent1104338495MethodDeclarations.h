﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.WebServiceResponseEventArgs
struct WebServiceResponseEventArgs_t1104338495;
// System.Collections.Generic.IDictionary`2<System.String,System.String>
struct IDictionary_2_t1943082916;
// System.String
struct String_t;
// System.Uri
struct Uri_t19570940;
// Amazon.Runtime.AmazonWebServiceRequest
struct AmazonWebServiceRequest_t3384026212;
// Amazon.Runtime.AmazonWebServiceResponse
struct AmazonWebServiceResponse_t529043356;
// Amazon.Runtime.Internal.IRequest
struct IRequest_t2400804350;
// Amazon.Runtime.Internal.Transform.IWebResponseData
struct IWebResponseData_t3815728244;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "System_System_Uri19570940.h"
#include "AWSSDK_Core_Amazon_Runtime_AmazonWebServiceRequest3384026212.h"
#include "AWSSDK_Core_Amazon_Runtime_AmazonWebServiceResponse529043356.h"

// System.Void Amazon.Runtime.WebServiceResponseEventArgs::.ctor()
extern "C"  void WebServiceResponseEventArgs__ctor_m1946683230 (WebServiceResponseEventArgs_t1104338495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.WebServiceResponseEventArgs::set_RequestHeaders(System.Collections.Generic.IDictionary`2<System.String,System.String>)
extern "C"  void WebServiceResponseEventArgs_set_RequestHeaders_m1291291162 (WebServiceResponseEventArgs_t1104338495 * __this, Il2CppObject* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.Runtime.WebServiceResponseEventArgs::get_ResponseHeaders()
extern "C"  Il2CppObject* WebServiceResponseEventArgs_get_ResponseHeaders_m4030033611 (WebServiceResponseEventArgs_t1104338495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.WebServiceResponseEventArgs::set_ResponseHeaders(System.Collections.Generic.IDictionary`2<System.String,System.String>)
extern "C"  void WebServiceResponseEventArgs_set_ResponseHeaders_m2258862772 (WebServiceResponseEventArgs_t1104338495 * __this, Il2CppObject* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.WebServiceResponseEventArgs::set_Parameters(System.Collections.Generic.IDictionary`2<System.String,System.String>)
extern "C"  void WebServiceResponseEventArgs_set_Parameters_m2854896669 (WebServiceResponseEventArgs_t1104338495 * __this, Il2CppObject* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.WebServiceResponseEventArgs::set_ServiceName(System.String)
extern "C"  void WebServiceResponseEventArgs_set_ServiceName_m2495427281 (WebServiceResponseEventArgs_t1104338495 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.WebServiceResponseEventArgs::set_Endpoint(System.Uri)
extern "C"  void WebServiceResponseEventArgs_set_Endpoint_m300893091 (WebServiceResponseEventArgs_t1104338495 * __this, Uri_t19570940 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.WebServiceResponseEventArgs::set_Request(Amazon.Runtime.AmazonWebServiceRequest)
extern "C"  void WebServiceResponseEventArgs_set_Request_m2367250982 (WebServiceResponseEventArgs_t1104338495 * __this, AmazonWebServiceRequest_t3384026212 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.WebServiceResponseEventArgs::set_Response(Amazon.Runtime.AmazonWebServiceResponse)
extern "C"  void WebServiceResponseEventArgs_set_Response_m514980438 (WebServiceResponseEventArgs_t1104338495 * __this, AmazonWebServiceResponse_t529043356 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.WebServiceResponseEventArgs Amazon.Runtime.WebServiceResponseEventArgs::Create(Amazon.Runtime.AmazonWebServiceResponse,Amazon.Runtime.Internal.IRequest,Amazon.Runtime.Internal.Transform.IWebResponseData)
extern "C"  WebServiceResponseEventArgs_t1104338495 * WebServiceResponseEventArgs_Create_m1718751414 (Il2CppObject * __this /* static, unused */, AmazonWebServiceResponse_t529043356 * ___response0, Il2CppObject * ___request1, Il2CppObject * ___webResponseData2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
