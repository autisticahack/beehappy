﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.SecurityToken.Model.Internal.MarshallTransformations.AssumedRoleUserUnmarshaller
struct AssumedRoleUserUnmarshaller_t2473561683;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.SecurityToken.Model.Internal.MarshallTransformations.AssumedRoleUserUnmarshaller
struct  AssumedRoleUserUnmarshaller_t2473561683  : public Il2CppObject
{
public:

public:
};

struct AssumedRoleUserUnmarshaller_t2473561683_StaticFields
{
public:
	// Amazon.SecurityToken.Model.Internal.MarshallTransformations.AssumedRoleUserUnmarshaller Amazon.SecurityToken.Model.Internal.MarshallTransformations.AssumedRoleUserUnmarshaller::_instance
	AssumedRoleUserUnmarshaller_t2473561683 * ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(AssumedRoleUserUnmarshaller_t2473561683_StaticFields, ____instance_0)); }
	inline AssumedRoleUserUnmarshaller_t2473561683 * get__instance_0() const { return ____instance_0; }
	inline AssumedRoleUserUnmarshaller_t2473561683 ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(AssumedRoleUserUnmarshaller_t2473561683 * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier(&____instance_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
