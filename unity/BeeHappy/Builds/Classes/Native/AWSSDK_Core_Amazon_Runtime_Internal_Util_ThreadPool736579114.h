﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// System.Collections.Generic.Queue`1<Amazon.Runtime.Internal.Util.ThreadPoolThrottler`1/ThreadPoolOptions`1<Amazon.Runtime.IAsyncExecutionContext,Amazon.Runtime.IAsyncExecutionContext>>
struct Queue_1_t3041895050;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Util.ThreadPoolThrottler`1<Amazon.Runtime.IAsyncExecutionContext>
struct  ThreadPoolThrottler_1_t736579114  : public Il2CppObject
{
public:
	// System.Int32 Amazon.Runtime.Internal.Util.ThreadPoolThrottler`1::_requestCount
	int32_t ____requestCount_1;
	// System.Collections.Generic.Queue`1<Amazon.Runtime.Internal.Util.ThreadPoolThrottler`1/ThreadPoolOptions`1<T,T>> Amazon.Runtime.Internal.Util.ThreadPoolThrottler`1::_queuedRequests
	Queue_1_t3041895050 * ____queuedRequests_2;
	// System.Int32 Amazon.Runtime.Internal.Util.ThreadPoolThrottler`1::<MaxConcurentRequest>k__BackingField
	int32_t ___U3CMaxConcurentRequestU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of__requestCount_1() { return static_cast<int32_t>(offsetof(ThreadPoolThrottler_1_t736579114, ____requestCount_1)); }
	inline int32_t get__requestCount_1() const { return ____requestCount_1; }
	inline int32_t* get_address_of__requestCount_1() { return &____requestCount_1; }
	inline void set__requestCount_1(int32_t value)
	{
		____requestCount_1 = value;
	}

	inline static int32_t get_offset_of__queuedRequests_2() { return static_cast<int32_t>(offsetof(ThreadPoolThrottler_1_t736579114, ____queuedRequests_2)); }
	inline Queue_1_t3041895050 * get__queuedRequests_2() const { return ____queuedRequests_2; }
	inline Queue_1_t3041895050 ** get_address_of__queuedRequests_2() { return &____queuedRequests_2; }
	inline void set__queuedRequests_2(Queue_1_t3041895050 * value)
	{
		____queuedRequests_2 = value;
		Il2CppCodeGenWriteBarrier(&____queuedRequests_2, value);
	}

	inline static int32_t get_offset_of_U3CMaxConcurentRequestU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ThreadPoolThrottler_1_t736579114, ___U3CMaxConcurentRequestU3Ek__BackingField_3)); }
	inline int32_t get_U3CMaxConcurentRequestU3Ek__BackingField_3() const { return ___U3CMaxConcurentRequestU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CMaxConcurentRequestU3Ek__BackingField_3() { return &___U3CMaxConcurentRequestU3Ek__BackingField_3; }
	inline void set_U3CMaxConcurentRequestU3Ek__BackingField_3(int32_t value)
	{
		___U3CMaxConcurentRequestU3Ek__BackingField_3 = value;
	}
};

struct ThreadPoolThrottler_1_t736579114_StaticFields
{
public:
	// System.Object Amazon.Runtime.Internal.Util.ThreadPoolThrottler`1::_queueLock
	Il2CppObject * ____queueLock_0;

public:
	inline static int32_t get_offset_of__queueLock_0() { return static_cast<int32_t>(offsetof(ThreadPoolThrottler_1_t736579114_StaticFields, ____queueLock_0)); }
	inline Il2CppObject * get__queueLock_0() const { return ____queueLock_0; }
	inline Il2CppObject ** get_address_of__queueLock_0() { return &____queueLock_0; }
	inline void set__queueLock_0(Il2CppObject * value)
	{
		____queueLock_0 = value;
		Il2CppCodeGenWriteBarrier(&____queueLock_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
