﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.Runtime.Internal.Transform.BoolUnmarshaller
struct BoolUnmarshaller_t2985984772;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Transform.BoolUnmarshaller
struct  BoolUnmarshaller_t2985984772  : public Il2CppObject
{
public:

public:
};

struct BoolUnmarshaller_t2985984772_StaticFields
{
public:
	// Amazon.Runtime.Internal.Transform.BoolUnmarshaller Amazon.Runtime.Internal.Transform.BoolUnmarshaller::_instance
	BoolUnmarshaller_t2985984772 * ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(BoolUnmarshaller_t2985984772_StaticFields, ____instance_0)); }
	inline BoolUnmarshaller_t2985984772 * get__instance_0() const { return ____instance_0; }
	inline BoolUnmarshaller_t2985984772 ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(BoolUnmarshaller_t2985984772 * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier(&____instance_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
