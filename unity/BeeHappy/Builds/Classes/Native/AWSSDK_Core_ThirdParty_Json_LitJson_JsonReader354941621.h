﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Stack`1<ThirdParty.Json.LitJson.JsonToken>
struct Stack_1_t3533309409;
// ThirdParty.Json.LitJson.Lexer
struct Lexer_t954714164;
// System.IO.TextReader
struct TextReader_t1561828458;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_JsonToken2445581255.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.Json.LitJson.JsonReader
struct  JsonReader_t354941621  : public Il2CppObject
{
public:
	// System.Collections.Generic.Stack`1<ThirdParty.Json.LitJson.JsonToken> ThirdParty.Json.LitJson.JsonReader::depth
	Stack_1_t3533309409 * ___depth_0;
	// System.Int32 ThirdParty.Json.LitJson.JsonReader::current_input
	int32_t ___current_input_1;
	// System.Int32 ThirdParty.Json.LitJson.JsonReader::current_symbol
	int32_t ___current_symbol_2;
	// System.Boolean ThirdParty.Json.LitJson.JsonReader::end_of_json
	bool ___end_of_json_3;
	// System.Boolean ThirdParty.Json.LitJson.JsonReader::end_of_input
	bool ___end_of_input_4;
	// ThirdParty.Json.LitJson.Lexer ThirdParty.Json.LitJson.JsonReader::lexer
	Lexer_t954714164 * ___lexer_5;
	// System.Boolean ThirdParty.Json.LitJson.JsonReader::parser_in_string
	bool ___parser_in_string_6;
	// System.Boolean ThirdParty.Json.LitJson.JsonReader::parser_return
	bool ___parser_return_7;
	// System.Boolean ThirdParty.Json.LitJson.JsonReader::read_started
	bool ___read_started_8;
	// System.IO.TextReader ThirdParty.Json.LitJson.JsonReader::reader
	TextReader_t1561828458 * ___reader_9;
	// System.Boolean ThirdParty.Json.LitJson.JsonReader::reader_is_owned
	bool ___reader_is_owned_10;
	// System.Object ThirdParty.Json.LitJson.JsonReader::token_value
	Il2CppObject * ___token_value_11;
	// ThirdParty.Json.LitJson.JsonToken ThirdParty.Json.LitJson.JsonReader::token
	int32_t ___token_12;

public:
	inline static int32_t get_offset_of_depth_0() { return static_cast<int32_t>(offsetof(JsonReader_t354941621, ___depth_0)); }
	inline Stack_1_t3533309409 * get_depth_0() const { return ___depth_0; }
	inline Stack_1_t3533309409 ** get_address_of_depth_0() { return &___depth_0; }
	inline void set_depth_0(Stack_1_t3533309409 * value)
	{
		___depth_0 = value;
		Il2CppCodeGenWriteBarrier(&___depth_0, value);
	}

	inline static int32_t get_offset_of_current_input_1() { return static_cast<int32_t>(offsetof(JsonReader_t354941621, ___current_input_1)); }
	inline int32_t get_current_input_1() const { return ___current_input_1; }
	inline int32_t* get_address_of_current_input_1() { return &___current_input_1; }
	inline void set_current_input_1(int32_t value)
	{
		___current_input_1 = value;
	}

	inline static int32_t get_offset_of_current_symbol_2() { return static_cast<int32_t>(offsetof(JsonReader_t354941621, ___current_symbol_2)); }
	inline int32_t get_current_symbol_2() const { return ___current_symbol_2; }
	inline int32_t* get_address_of_current_symbol_2() { return &___current_symbol_2; }
	inline void set_current_symbol_2(int32_t value)
	{
		___current_symbol_2 = value;
	}

	inline static int32_t get_offset_of_end_of_json_3() { return static_cast<int32_t>(offsetof(JsonReader_t354941621, ___end_of_json_3)); }
	inline bool get_end_of_json_3() const { return ___end_of_json_3; }
	inline bool* get_address_of_end_of_json_3() { return &___end_of_json_3; }
	inline void set_end_of_json_3(bool value)
	{
		___end_of_json_3 = value;
	}

	inline static int32_t get_offset_of_end_of_input_4() { return static_cast<int32_t>(offsetof(JsonReader_t354941621, ___end_of_input_4)); }
	inline bool get_end_of_input_4() const { return ___end_of_input_4; }
	inline bool* get_address_of_end_of_input_4() { return &___end_of_input_4; }
	inline void set_end_of_input_4(bool value)
	{
		___end_of_input_4 = value;
	}

	inline static int32_t get_offset_of_lexer_5() { return static_cast<int32_t>(offsetof(JsonReader_t354941621, ___lexer_5)); }
	inline Lexer_t954714164 * get_lexer_5() const { return ___lexer_5; }
	inline Lexer_t954714164 ** get_address_of_lexer_5() { return &___lexer_5; }
	inline void set_lexer_5(Lexer_t954714164 * value)
	{
		___lexer_5 = value;
		Il2CppCodeGenWriteBarrier(&___lexer_5, value);
	}

	inline static int32_t get_offset_of_parser_in_string_6() { return static_cast<int32_t>(offsetof(JsonReader_t354941621, ___parser_in_string_6)); }
	inline bool get_parser_in_string_6() const { return ___parser_in_string_6; }
	inline bool* get_address_of_parser_in_string_6() { return &___parser_in_string_6; }
	inline void set_parser_in_string_6(bool value)
	{
		___parser_in_string_6 = value;
	}

	inline static int32_t get_offset_of_parser_return_7() { return static_cast<int32_t>(offsetof(JsonReader_t354941621, ___parser_return_7)); }
	inline bool get_parser_return_7() const { return ___parser_return_7; }
	inline bool* get_address_of_parser_return_7() { return &___parser_return_7; }
	inline void set_parser_return_7(bool value)
	{
		___parser_return_7 = value;
	}

	inline static int32_t get_offset_of_read_started_8() { return static_cast<int32_t>(offsetof(JsonReader_t354941621, ___read_started_8)); }
	inline bool get_read_started_8() const { return ___read_started_8; }
	inline bool* get_address_of_read_started_8() { return &___read_started_8; }
	inline void set_read_started_8(bool value)
	{
		___read_started_8 = value;
	}

	inline static int32_t get_offset_of_reader_9() { return static_cast<int32_t>(offsetof(JsonReader_t354941621, ___reader_9)); }
	inline TextReader_t1561828458 * get_reader_9() const { return ___reader_9; }
	inline TextReader_t1561828458 ** get_address_of_reader_9() { return &___reader_9; }
	inline void set_reader_9(TextReader_t1561828458 * value)
	{
		___reader_9 = value;
		Il2CppCodeGenWriteBarrier(&___reader_9, value);
	}

	inline static int32_t get_offset_of_reader_is_owned_10() { return static_cast<int32_t>(offsetof(JsonReader_t354941621, ___reader_is_owned_10)); }
	inline bool get_reader_is_owned_10() const { return ___reader_is_owned_10; }
	inline bool* get_address_of_reader_is_owned_10() { return &___reader_is_owned_10; }
	inline void set_reader_is_owned_10(bool value)
	{
		___reader_is_owned_10 = value;
	}

	inline static int32_t get_offset_of_token_value_11() { return static_cast<int32_t>(offsetof(JsonReader_t354941621, ___token_value_11)); }
	inline Il2CppObject * get_token_value_11() const { return ___token_value_11; }
	inline Il2CppObject ** get_address_of_token_value_11() { return &___token_value_11; }
	inline void set_token_value_11(Il2CppObject * value)
	{
		___token_value_11 = value;
		Il2CppCodeGenWriteBarrier(&___token_value_11, value);
	}

	inline static int32_t get_offset_of_token_12() { return static_cast<int32_t>(offsetof(JsonReader_t354941621, ___token_12)); }
	inline int32_t get_token_12() const { return ___token_12; }
	inline int32_t* get_address_of_token_12() { return &___token_12; }
	inline void set_token_12(int32_t value)
	{
		___token_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
