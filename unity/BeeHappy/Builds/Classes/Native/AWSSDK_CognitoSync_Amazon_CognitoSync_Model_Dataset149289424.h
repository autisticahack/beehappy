﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Nullable_1_gen3251239280.h"
#include "mscorlib_System_Nullable_1_gen3467111648.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.CognitoSync.Model.Dataset
struct  Dataset_t149289424  : public Il2CppObject
{
public:
	// System.Nullable`1<System.DateTime> Amazon.CognitoSync.Model.Dataset::_creationDate
	Nullable_1_t3251239280  ____creationDate_0;
	// System.String Amazon.CognitoSync.Model.Dataset::_datasetName
	String_t* ____datasetName_1;
	// System.Nullable`1<System.Int64> Amazon.CognitoSync.Model.Dataset::_dataStorage
	Nullable_1_t3467111648  ____dataStorage_2;
	// System.String Amazon.CognitoSync.Model.Dataset::_identityId
	String_t* ____identityId_3;
	// System.String Amazon.CognitoSync.Model.Dataset::_lastModifiedBy
	String_t* ____lastModifiedBy_4;
	// System.Nullable`1<System.DateTime> Amazon.CognitoSync.Model.Dataset::_lastModifiedDate
	Nullable_1_t3251239280  ____lastModifiedDate_5;
	// System.Nullable`1<System.Int64> Amazon.CognitoSync.Model.Dataset::_numRecords
	Nullable_1_t3467111648  ____numRecords_6;

public:
	inline static int32_t get_offset_of__creationDate_0() { return static_cast<int32_t>(offsetof(Dataset_t149289424, ____creationDate_0)); }
	inline Nullable_1_t3251239280  get__creationDate_0() const { return ____creationDate_0; }
	inline Nullable_1_t3251239280 * get_address_of__creationDate_0() { return &____creationDate_0; }
	inline void set__creationDate_0(Nullable_1_t3251239280  value)
	{
		____creationDate_0 = value;
	}

	inline static int32_t get_offset_of__datasetName_1() { return static_cast<int32_t>(offsetof(Dataset_t149289424, ____datasetName_1)); }
	inline String_t* get__datasetName_1() const { return ____datasetName_1; }
	inline String_t** get_address_of__datasetName_1() { return &____datasetName_1; }
	inline void set__datasetName_1(String_t* value)
	{
		____datasetName_1 = value;
		Il2CppCodeGenWriteBarrier(&____datasetName_1, value);
	}

	inline static int32_t get_offset_of__dataStorage_2() { return static_cast<int32_t>(offsetof(Dataset_t149289424, ____dataStorage_2)); }
	inline Nullable_1_t3467111648  get__dataStorage_2() const { return ____dataStorage_2; }
	inline Nullable_1_t3467111648 * get_address_of__dataStorage_2() { return &____dataStorage_2; }
	inline void set__dataStorage_2(Nullable_1_t3467111648  value)
	{
		____dataStorage_2 = value;
	}

	inline static int32_t get_offset_of__identityId_3() { return static_cast<int32_t>(offsetof(Dataset_t149289424, ____identityId_3)); }
	inline String_t* get__identityId_3() const { return ____identityId_3; }
	inline String_t** get_address_of__identityId_3() { return &____identityId_3; }
	inline void set__identityId_3(String_t* value)
	{
		____identityId_3 = value;
		Il2CppCodeGenWriteBarrier(&____identityId_3, value);
	}

	inline static int32_t get_offset_of__lastModifiedBy_4() { return static_cast<int32_t>(offsetof(Dataset_t149289424, ____lastModifiedBy_4)); }
	inline String_t* get__lastModifiedBy_4() const { return ____lastModifiedBy_4; }
	inline String_t** get_address_of__lastModifiedBy_4() { return &____lastModifiedBy_4; }
	inline void set__lastModifiedBy_4(String_t* value)
	{
		____lastModifiedBy_4 = value;
		Il2CppCodeGenWriteBarrier(&____lastModifiedBy_4, value);
	}

	inline static int32_t get_offset_of__lastModifiedDate_5() { return static_cast<int32_t>(offsetof(Dataset_t149289424, ____lastModifiedDate_5)); }
	inline Nullable_1_t3251239280  get__lastModifiedDate_5() const { return ____lastModifiedDate_5; }
	inline Nullable_1_t3251239280 * get_address_of__lastModifiedDate_5() { return &____lastModifiedDate_5; }
	inline void set__lastModifiedDate_5(Nullable_1_t3251239280  value)
	{
		____lastModifiedDate_5 = value;
	}

	inline static int32_t get_offset_of__numRecords_6() { return static_cast<int32_t>(offsetof(Dataset_t149289424, ____numRecords_6)); }
	inline Nullable_1_t3467111648  get__numRecords_6() const { return ____numRecords_6; }
	inline Nullable_1_t3467111648 * get_address_of__numRecords_6() { return &____numRecords_6; }
	inline void set__numRecords_6(Nullable_1_t3467111648  value)
	{
		____numRecords_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
