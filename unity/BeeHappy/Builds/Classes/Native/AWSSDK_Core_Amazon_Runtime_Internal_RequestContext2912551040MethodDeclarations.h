﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.RequestContext
struct RequestContext_t2912551040;
// Amazon.Runtime.Internal.IRequest
struct IRequest_t2400804350;
// Amazon.Runtime.Internal.Util.RequestMetrics
struct RequestMetrics_t218029284;
// Amazon.Runtime.Internal.Auth.AbstractAWSSigner
struct AbstractAWSSigner_t2114314031;
// Amazon.Runtime.IClientConfig
struct IClientConfig_t4078933728;
// Amazon.Runtime.AmazonWebServiceRequest
struct AmazonWebServiceRequest_t3384026212;
// Amazon.Runtime.Internal.Transform.IMarshaller`2<Amazon.Runtime.Internal.IRequest,Amazon.Runtime.AmazonWebServiceRequest>
struct IMarshaller_2_t3817076711;
// Amazon.Runtime.Internal.Transform.ResponseUnmarshaller
struct ResponseUnmarshaller_t3934041557;
// Amazon.Runtime.ImmutableCredentials
struct ImmutableCredentials_t282353664;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Util_RequestMet218029284.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Auth_AbstractA2114314031.h"
#include "AWSSDK_Core_Amazon_Runtime_AmazonWebServiceRequest3384026212.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_Resp3934041557.h"
#include "AWSSDK_Core_Amazon_Runtime_ImmutableCredentials282353664.h"

// System.Void Amazon.Runtime.Internal.RequestContext::.ctor(System.Boolean)
extern "C"  void RequestContext__ctor_m1459910601 (RequestContext_t2912551040 * __this, bool ___enableMetrics0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.Internal.IRequest Amazon.Runtime.Internal.RequestContext::get_Request()
extern "C"  Il2CppObject * RequestContext_get_Request_m3094970086 (RequestContext_t2912551040 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.RequestContext::set_Request(Amazon.Runtime.Internal.IRequest)
extern "C"  void RequestContext_set_Request_m132108797 (RequestContext_t2912551040 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.Internal.Util.RequestMetrics Amazon.Runtime.Internal.RequestContext::get_Metrics()
extern "C"  RequestMetrics_t218029284 * RequestContext_get_Metrics_m1012793894 (RequestContext_t2912551040 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.RequestContext::set_Metrics(Amazon.Runtime.Internal.Util.RequestMetrics)
extern "C"  void RequestContext_set_Metrics_m2351442787 (RequestContext_t2912551040 * __this, RequestMetrics_t218029284 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.Internal.Auth.AbstractAWSSigner Amazon.Runtime.Internal.RequestContext::get_Signer()
extern "C"  AbstractAWSSigner_t2114314031 * RequestContext_get_Signer_m548179154 (RequestContext_t2912551040 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.RequestContext::set_Signer(Amazon.Runtime.Internal.Auth.AbstractAWSSigner)
extern "C"  void RequestContext_set_Signer_m1747104019 (RequestContext_t2912551040 * __this, AbstractAWSSigner_t2114314031 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.IClientConfig Amazon.Runtime.Internal.RequestContext::get_ClientConfig()
extern "C"  Il2CppObject * RequestContext_get_ClientConfig_m1895034523 (RequestContext_t2912551040 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.RequestContext::set_ClientConfig(Amazon.Runtime.IClientConfig)
extern "C"  void RequestContext_set_ClientConfig_m1808565616 (RequestContext_t2912551040 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Amazon.Runtime.Internal.RequestContext::get_Retries()
extern "C"  int32_t RequestContext_get_Retries_m204550187 (RequestContext_t2912551040 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.RequestContext::set_Retries(System.Int32)
extern "C"  void RequestContext_set_Retries_m2251774972 (RequestContext_t2912551040 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.RequestContext::get_IsSigned()
extern "C"  bool RequestContext_get_IsSigned_m2944656099 (RequestContext_t2912551040 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.RequestContext::set_IsSigned(System.Boolean)
extern "C"  void RequestContext_set_IsSigned_m3988395306 (RequestContext_t2912551040 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.RequestContext::get_IsAsync()
extern "C"  bool RequestContext_get_IsAsync_m2180920947 (RequestContext_t2912551040 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.RequestContext::set_IsAsync(System.Boolean)
extern "C"  void RequestContext_set_IsAsync_m4210893398 (RequestContext_t2912551040 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.AmazonWebServiceRequest Amazon.Runtime.Internal.RequestContext::get_OriginalRequest()
extern "C"  AmazonWebServiceRequest_t3384026212 * RequestContext_get_OriginalRequest_m3529528404 (RequestContext_t2912551040 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.RequestContext::set_OriginalRequest(Amazon.Runtime.AmazonWebServiceRequest)
extern "C"  void RequestContext_set_OriginalRequest_m922242183 (RequestContext_t2912551040 * __this, AmazonWebServiceRequest_t3384026212 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.Internal.Transform.IMarshaller`2<Amazon.Runtime.Internal.IRequest,Amazon.Runtime.AmazonWebServiceRequest> Amazon.Runtime.Internal.RequestContext::get_Marshaller()
extern "C"  Il2CppObject* RequestContext_get_Marshaller_m1513486575 (RequestContext_t2912551040 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.RequestContext::set_Marshaller(Amazon.Runtime.Internal.Transform.IMarshaller`2<Amazon.Runtime.Internal.IRequest,Amazon.Runtime.AmazonWebServiceRequest>)
extern "C"  void RequestContext_set_Marshaller_m2454868700 (RequestContext_t2912551040 * __this, Il2CppObject* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.Internal.Transform.ResponseUnmarshaller Amazon.Runtime.Internal.RequestContext::get_Unmarshaller()
extern "C"  ResponseUnmarshaller_t3934041557 * RequestContext_get_Unmarshaller_m26613972 (RequestContext_t2912551040 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.RequestContext::set_Unmarshaller(Amazon.Runtime.Internal.Transform.ResponseUnmarshaller)
extern "C"  void RequestContext_set_Unmarshaller_m211072121 (RequestContext_t2912551040 * __this, ResponseUnmarshaller_t3934041557 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.ImmutableCredentials Amazon.Runtime.Internal.RequestContext::get_ImmutableCredentials()
extern "C"  ImmutableCredentials_t282353664 * RequestContext_get_ImmutableCredentials_m2968142606 (RequestContext_t2912551040 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.RequestContext::set_ImmutableCredentials(Amazon.Runtime.ImmutableCredentials)
extern "C"  void RequestContext_set_ImmutableCredentials_m3270350189 (RequestContext_t2912551040 * __this, ImmutableCredentials_t282353664 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Runtime.Internal.RequestContext::get_RequestName()
extern "C"  String_t* RequestContext_get_RequestName_m1219500598 (RequestContext_t2912551040 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
