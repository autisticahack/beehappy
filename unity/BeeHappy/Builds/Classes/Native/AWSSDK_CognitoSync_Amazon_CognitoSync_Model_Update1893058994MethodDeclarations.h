﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.CognitoSync.Model.UpdateRecordsResponse
struct UpdateRecordsResponse_t1893058994;
// System.Collections.Generic.List`1<Amazon.CognitoSync.Model.Record>
struct List_1_t4105675293;

#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<Amazon.CognitoSync.Model.Record> Amazon.CognitoSync.Model.UpdateRecordsResponse::get_Records()
extern "C"  List_1_t4105675293 * UpdateRecordsResponse_get_Records_m3960170895 (UpdateRecordsResponse_t1893058994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Model.UpdateRecordsResponse::set_Records(System.Collections.Generic.List`1<Amazon.CognitoSync.Model.Record>)
extern "C"  void UpdateRecordsResponse_set_Records_m2344576596 (UpdateRecordsResponse_t1893058994 * __this, List_1_t4105675293 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Model.UpdateRecordsResponse::.ctor()
extern "C"  void UpdateRecordsResponse__ctor_m3521180735 (UpdateRecordsResponse_t1893058994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
