﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// System.Collections.Generic.List`1<Amazon.CognitoSync.Model.Record>
struct List_1_t4105675293;

#include "AWSSDK_Core_Amazon_Runtime_AmazonWebServiceResponse529043356.h"
#include "mscorlib_System_Nullable_1_gen334943763.h"
#include "mscorlib_System_Nullable_1_gen2088641033.h"
#include "mscorlib_System_Nullable_1_gen3467111648.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.CognitoSync.Model.ListRecordsResponse
struct  ListRecordsResponse_t1441155223  : public AmazonWebServiceResponse_t529043356
{
public:
	// System.Nullable`1<System.Int32> Amazon.CognitoSync.Model.ListRecordsResponse::_count
	Nullable_1_t334943763  ____count_3;
	// System.Nullable`1<System.Boolean> Amazon.CognitoSync.Model.ListRecordsResponse::_datasetDeletedAfterRequestedSyncCount
	Nullable_1_t2088641033  ____datasetDeletedAfterRequestedSyncCount_4;
	// System.Nullable`1<System.Boolean> Amazon.CognitoSync.Model.ListRecordsResponse::_datasetExists
	Nullable_1_t2088641033  ____datasetExists_5;
	// System.Nullable`1<System.Int64> Amazon.CognitoSync.Model.ListRecordsResponse::_datasetSyncCount
	Nullable_1_t3467111648  ____datasetSyncCount_6;
	// System.String Amazon.CognitoSync.Model.ListRecordsResponse::_lastModifiedBy
	String_t* ____lastModifiedBy_7;
	// System.Collections.Generic.List`1<System.String> Amazon.CognitoSync.Model.ListRecordsResponse::_mergedDatasetNames
	List_1_t1398341365 * ____mergedDatasetNames_8;
	// System.String Amazon.CognitoSync.Model.ListRecordsResponse::_nextToken
	String_t* ____nextToken_9;
	// System.Collections.Generic.List`1<Amazon.CognitoSync.Model.Record> Amazon.CognitoSync.Model.ListRecordsResponse::_records
	List_1_t4105675293 * ____records_10;
	// System.String Amazon.CognitoSync.Model.ListRecordsResponse::_syncSessionToken
	String_t* ____syncSessionToken_11;

public:
	inline static int32_t get_offset_of__count_3() { return static_cast<int32_t>(offsetof(ListRecordsResponse_t1441155223, ____count_3)); }
	inline Nullable_1_t334943763  get__count_3() const { return ____count_3; }
	inline Nullable_1_t334943763 * get_address_of__count_3() { return &____count_3; }
	inline void set__count_3(Nullable_1_t334943763  value)
	{
		____count_3 = value;
	}

	inline static int32_t get_offset_of__datasetDeletedAfterRequestedSyncCount_4() { return static_cast<int32_t>(offsetof(ListRecordsResponse_t1441155223, ____datasetDeletedAfterRequestedSyncCount_4)); }
	inline Nullable_1_t2088641033  get__datasetDeletedAfterRequestedSyncCount_4() const { return ____datasetDeletedAfterRequestedSyncCount_4; }
	inline Nullable_1_t2088641033 * get_address_of__datasetDeletedAfterRequestedSyncCount_4() { return &____datasetDeletedAfterRequestedSyncCount_4; }
	inline void set__datasetDeletedAfterRequestedSyncCount_4(Nullable_1_t2088641033  value)
	{
		____datasetDeletedAfterRequestedSyncCount_4 = value;
	}

	inline static int32_t get_offset_of__datasetExists_5() { return static_cast<int32_t>(offsetof(ListRecordsResponse_t1441155223, ____datasetExists_5)); }
	inline Nullable_1_t2088641033  get__datasetExists_5() const { return ____datasetExists_5; }
	inline Nullable_1_t2088641033 * get_address_of__datasetExists_5() { return &____datasetExists_5; }
	inline void set__datasetExists_5(Nullable_1_t2088641033  value)
	{
		____datasetExists_5 = value;
	}

	inline static int32_t get_offset_of__datasetSyncCount_6() { return static_cast<int32_t>(offsetof(ListRecordsResponse_t1441155223, ____datasetSyncCount_6)); }
	inline Nullable_1_t3467111648  get__datasetSyncCount_6() const { return ____datasetSyncCount_6; }
	inline Nullable_1_t3467111648 * get_address_of__datasetSyncCount_6() { return &____datasetSyncCount_6; }
	inline void set__datasetSyncCount_6(Nullable_1_t3467111648  value)
	{
		____datasetSyncCount_6 = value;
	}

	inline static int32_t get_offset_of__lastModifiedBy_7() { return static_cast<int32_t>(offsetof(ListRecordsResponse_t1441155223, ____lastModifiedBy_7)); }
	inline String_t* get__lastModifiedBy_7() const { return ____lastModifiedBy_7; }
	inline String_t** get_address_of__lastModifiedBy_7() { return &____lastModifiedBy_7; }
	inline void set__lastModifiedBy_7(String_t* value)
	{
		____lastModifiedBy_7 = value;
		Il2CppCodeGenWriteBarrier(&____lastModifiedBy_7, value);
	}

	inline static int32_t get_offset_of__mergedDatasetNames_8() { return static_cast<int32_t>(offsetof(ListRecordsResponse_t1441155223, ____mergedDatasetNames_8)); }
	inline List_1_t1398341365 * get__mergedDatasetNames_8() const { return ____mergedDatasetNames_8; }
	inline List_1_t1398341365 ** get_address_of__mergedDatasetNames_8() { return &____mergedDatasetNames_8; }
	inline void set__mergedDatasetNames_8(List_1_t1398341365 * value)
	{
		____mergedDatasetNames_8 = value;
		Il2CppCodeGenWriteBarrier(&____mergedDatasetNames_8, value);
	}

	inline static int32_t get_offset_of__nextToken_9() { return static_cast<int32_t>(offsetof(ListRecordsResponse_t1441155223, ____nextToken_9)); }
	inline String_t* get__nextToken_9() const { return ____nextToken_9; }
	inline String_t** get_address_of__nextToken_9() { return &____nextToken_9; }
	inline void set__nextToken_9(String_t* value)
	{
		____nextToken_9 = value;
		Il2CppCodeGenWriteBarrier(&____nextToken_9, value);
	}

	inline static int32_t get_offset_of__records_10() { return static_cast<int32_t>(offsetof(ListRecordsResponse_t1441155223, ____records_10)); }
	inline List_1_t4105675293 * get__records_10() const { return ____records_10; }
	inline List_1_t4105675293 ** get_address_of__records_10() { return &____records_10; }
	inline void set__records_10(List_1_t4105675293 * value)
	{
		____records_10 = value;
		Il2CppCodeGenWriteBarrier(&____records_10, value);
	}

	inline static int32_t get_offset_of__syncSessionToken_11() { return static_cast<int32_t>(offsetof(ListRecordsResponse_t1441155223, ____syncSessionToken_11)); }
	inline String_t* get__syncSessionToken_11() const { return ____syncSessionToken_11; }
	inline String_t** get_address_of__syncSessionToken_11() { return &____syncSessionToken_11; }
	inline void set__syncSessionToken_11(String_t* value)
	{
		____syncSessionToken_11 = value;
		Il2CppCodeGenWriteBarrier(&____syncSessionToken_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
