﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.LoggingSection
struct LoggingSection_t905443946;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_LoggingOptions2865640765.h"
#include "AWSSDK_Core_Amazon_ResponseLoggingOption3443611737.h"
#include "mscorlib_System_Nullable_1_gen334943763.h"
#include "mscorlib_System_Nullable_1_gen2088641033.h"
#include "AWSSDK_Core_Amazon_LogMetricsFormatOption97749509.h"

// Amazon.LoggingOptions Amazon.LoggingSection::get_LogTo()
extern "C"  int32_t LoggingSection_get_LogTo_m1198468563 (LoggingSection_t905443946 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.ResponseLoggingOption Amazon.LoggingSection::get_LogResponses()
extern "C"  int32_t LoggingSection_get_LogResponses_m4071164652 (LoggingSection_t905443946 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Int32> Amazon.LoggingSection::get_LogResponsesSizeLimit()
extern "C"  Nullable_1_t334943763  LoggingSection_get_LogResponsesSizeLimit_m4197867419 (LoggingSection_t905443946 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Boolean> Amazon.LoggingSection::get_LogMetrics()
extern "C"  Nullable_1_t2088641033  LoggingSection_get_LogMetrics_m1288443716 (LoggingSection_t905443946 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.LogMetricsFormatOption Amazon.LoggingSection::get_LogMetricsFormat()
extern "C"  int32_t LoggingSection_get_LogMetricsFormat_m2787910168 (LoggingSection_t905443946 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Amazon.LoggingSection::get_LogMetricsCustomFormatter()
extern "C"  Type_t * LoggingSection_get_LogMetricsCustomFormatter_m1952005122 (LoggingSection_t905443946 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.LoggingSection::.ctor()
extern "C"  void LoggingSection__ctor_m2302395249 (LoggingSection_t905443946 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
