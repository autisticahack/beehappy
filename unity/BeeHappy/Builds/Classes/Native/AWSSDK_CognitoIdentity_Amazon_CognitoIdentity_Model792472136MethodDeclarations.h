﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.CognitoIdentity.Model.Credentials
struct Credentials_t792472136;
// Amazon.Runtime.ImmutableCredentials
struct ImmutableCredentials_t282353664;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_DateTime693205669.h"

// Amazon.Runtime.ImmutableCredentials Amazon.CognitoIdentity.Model.Credentials::GetCredentials()
extern "C"  ImmutableCredentials_t282353664 * Credentials_GetCredentials_m104013866 (Credentials_t792472136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.CognitoIdentity.Model.Credentials::get_AccessKeyId()
extern "C"  String_t* Credentials_get_AccessKeyId_m1608339957 (Credentials_t792472136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoIdentity.Model.Credentials::set_AccessKeyId(System.String)
extern "C"  void Credentials_set_AccessKeyId_m3762288358 (Credentials_t792472136 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime Amazon.CognitoIdentity.Model.Credentials::get_Expiration()
extern "C"  DateTime_t693205669  Credentials_get_Expiration_m1990969830 (Credentials_t792472136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoIdentity.Model.Credentials::set_Expiration(System.DateTime)
extern "C"  void Credentials_set_Expiration_m772139935 (Credentials_t792472136 * __this, DateTime_t693205669  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.CognitoIdentity.Model.Credentials::get_SecretKey()
extern "C"  String_t* Credentials_get_SecretKey_m1201331976 (Credentials_t792472136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoIdentity.Model.Credentials::set_SecretKey(System.String)
extern "C"  void Credentials_set_SecretKey_m2468273701 (Credentials_t792472136 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.CognitoIdentity.Model.Credentials::get_SessionToken()
extern "C"  String_t* Credentials_get_SessionToken_m3926496798 (Credentials_t792472136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoIdentity.Model.Credentials::set_SessionToken(System.String)
extern "C"  void Credentials_set_SessionToken_m2768715957 (Credentials_t792472136 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoIdentity.Model.Credentials::.ctor()
extern "C"  void Credentials__ctor_m190010687 (Credentials_t792472136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
