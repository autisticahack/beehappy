﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>
struct Dictionary_2_t3650197868;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1041763336.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_ObjectMetadata4058137740.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3789863417_gshared (Enumerator_t1041763336 * __this, Dictionary_2_t3650197868 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m3789863417(__this, ___host0, method) ((  void (*) (Enumerator_t1041763336 *, Dictionary_2_t3650197868 *, const MethodInfo*))Enumerator__ctor_m3789863417_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m4150435772_gshared (Enumerator_t1041763336 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m4150435772(__this, method) ((  Il2CppObject * (*) (Enumerator_t1041763336 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m4150435772_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m804193410_gshared (Enumerator_t1041763336 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m804193410(__this, method) ((  void (*) (Enumerator_t1041763336 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m804193410_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::Dispose()
extern "C"  void Enumerator_Dispose_m4014171073_gshared (Enumerator_t1041763336 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m4014171073(__this, method) ((  void (*) (Enumerator_t1041763336 *, const MethodInfo*))Enumerator_Dispose_m4014171073_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2332063510_gshared (Enumerator_t1041763336 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2332063510(__this, method) ((  bool (*) (Enumerator_t1041763336 *, const MethodInfo*))Enumerator_MoveNext_m2332063510_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::get_Current()
extern "C"  ObjectMetadata_t4058137740  Enumerator_get_Current_m114914456_gshared (Enumerator_t1041763336 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m114914456(__this, method) ((  ObjectMetadata_t4058137740  (*) (Enumerator_t1041763336 *, const MethodInfo*))Enumerator_get_Current_m114914456_gshared)(__this, method)
