﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.Marshaller
struct Marshaller_t3371889241;
// Amazon.Runtime.IExecutionContext
struct IExecutionContext_t2477130752;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// Amazon.Runtime.IAsyncExecutionContext
struct IAsyncExecutionContext_t3792344986;

#include "codegen/il2cpp-codegen.h"

// System.Void Amazon.Runtime.Internal.Marshaller::InvokeSync(Amazon.Runtime.IExecutionContext)
extern "C"  void Marshaller_InvokeSync_m4206025750 (Marshaller_t3371889241 * __this, Il2CppObject * ___executionContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Amazon.Runtime.Internal.Marshaller::InvokeAsync(Amazon.Runtime.IAsyncExecutionContext)
extern "C"  Il2CppObject * Marshaller_InvokeAsync_m3617950807 (Marshaller_t3371889241 * __this, Il2CppObject * ___executionContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Marshaller::PreInvoke(Amazon.Runtime.IExecutionContext)
extern "C"  void Marshaller_PreInvoke_m4199950084 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___executionContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Marshaller::.ctor()
extern "C"  void Marshaller__ctor_m3003057215 (Marshaller_t3371889241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
