﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.Auth.AWS4SigningResult
struct AWS4SigningResult_t430803065;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t3397334013;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_DateTime693205669.h"

// System.Void Amazon.Runtime.Internal.Auth.AWS4SigningResult::.ctor(System.String,System.DateTime,System.String,System.String,System.Byte[],System.Byte[])
extern "C"  void AWS4SigningResult__ctor_m2531012469 (AWS4SigningResult_t430803065 * __this, String_t* ___awsAccessKeyId0, DateTime_t693205669  ___signedAt1, String_t* ___signedHeaders2, String_t* ___scope3, ByteU5BU5D_t3397334013* ___signingKey4, ByteU5BU5D_t3397334013* ___signature5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Runtime.Internal.Auth.AWS4SigningResult::get_AccessKeyId()
extern "C"  String_t* AWS4SigningResult_get_AccessKeyId_m1057103807 (AWS4SigningResult_t430803065 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Runtime.Internal.Auth.AWS4SigningResult::get_ISO8601DateTime()
extern "C"  String_t* AWS4SigningResult_get_ISO8601DateTime_m3052438120 (AWS4SigningResult_t430803065 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Runtime.Internal.Auth.AWS4SigningResult::get_SignedHeaders()
extern "C"  String_t* AWS4SigningResult_get_SignedHeaders_m1467707683 (AWS4SigningResult_t430803065 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Runtime.Internal.Auth.AWS4SigningResult::get_Scope()
extern "C"  String_t* AWS4SigningResult_get_Scope_m1475019699 (AWS4SigningResult_t430803065 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Amazon.Runtime.Internal.Auth.AWS4SigningResult::get_SigningKey()
extern "C"  ByteU5BU5D_t3397334013* AWS4SigningResult_get_SigningKey_m3825692162 (AWS4SigningResult_t430803065 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Runtime.Internal.Auth.AWS4SigningResult::get_Signature()
extern "C"  String_t* AWS4SigningResult_get_Signature_m1954568821 (AWS4SigningResult_t430803065 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Runtime.Internal.Auth.AWS4SigningResult::get_ForAuthorizationHeader()
extern "C"  String_t* AWS4SigningResult_get_ForAuthorizationHeader_m3786846980 (AWS4SigningResult_t430803065 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
