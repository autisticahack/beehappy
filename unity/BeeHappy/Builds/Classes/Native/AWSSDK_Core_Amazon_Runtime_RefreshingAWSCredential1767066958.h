﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.Runtime.RefreshingAWSCredentials/CredentialsRefreshState
struct CredentialsRefreshState_t3294867821;
// System.Object
struct Il2CppObject;

#include "AWSSDK_Core_Amazon_Runtime_AWSCredentials3583921007.h"
#include "mscorlib_System_TimeSpan3430258949.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.RefreshingAWSCredentials
struct  RefreshingAWSCredentials_t1767066958  : public AWSCredentials_t3583921007
{
public:
	// Amazon.Runtime.RefreshingAWSCredentials/CredentialsRefreshState Amazon.Runtime.RefreshingAWSCredentials::currentState
	CredentialsRefreshState_t3294867821 * ___currentState_0;
	// System.Object Amazon.Runtime.RefreshingAWSCredentials::_refreshLock
	Il2CppObject * ____refreshLock_1;
	// System.TimeSpan Amazon.Runtime.RefreshingAWSCredentials::_preemptExpiryTime
	TimeSpan_t3430258949  ____preemptExpiryTime_2;

public:
	inline static int32_t get_offset_of_currentState_0() { return static_cast<int32_t>(offsetof(RefreshingAWSCredentials_t1767066958, ___currentState_0)); }
	inline CredentialsRefreshState_t3294867821 * get_currentState_0() const { return ___currentState_0; }
	inline CredentialsRefreshState_t3294867821 ** get_address_of_currentState_0() { return &___currentState_0; }
	inline void set_currentState_0(CredentialsRefreshState_t3294867821 * value)
	{
		___currentState_0 = value;
		Il2CppCodeGenWriteBarrier(&___currentState_0, value);
	}

	inline static int32_t get_offset_of__refreshLock_1() { return static_cast<int32_t>(offsetof(RefreshingAWSCredentials_t1767066958, ____refreshLock_1)); }
	inline Il2CppObject * get__refreshLock_1() const { return ____refreshLock_1; }
	inline Il2CppObject ** get_address_of__refreshLock_1() { return &____refreshLock_1; }
	inline void set__refreshLock_1(Il2CppObject * value)
	{
		____refreshLock_1 = value;
		Il2CppCodeGenWriteBarrier(&____refreshLock_1, value);
	}

	inline static int32_t get_offset_of__preemptExpiryTime_2() { return static_cast<int32_t>(offsetof(RefreshingAWSCredentials_t1767066958, ____preemptExpiryTime_2)); }
	inline TimeSpan_t3430258949  get__preemptExpiryTime_2() const { return ____preemptExpiryTime_2; }
	inline TimeSpan_t3430258949 * get_address_of__preemptExpiryTime_2() { return &____preemptExpiryTime_2; }
	inline void set__preemptExpiryTime_2(TimeSpan_t3430258949  value)
	{
		____preemptExpiryTime_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
