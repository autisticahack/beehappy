﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.RefreshingAWSCredentials
struct RefreshingAWSCredentials_t1767066958;
// Amazon.Runtime.ImmutableCredentials
struct ImmutableCredentials_t282353664;
// Amazon.Runtime.RefreshingAWSCredentials/CredentialsRefreshState
struct CredentialsRefreshState_t3294867821;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_TimeSpan3430258949.h"
#include "AWSSDK_Core_Amazon_Runtime_RefreshingAWSCredential3294867821.h"

// System.TimeSpan Amazon.Runtime.RefreshingAWSCredentials::get_PreemptExpiryTime()
extern "C"  TimeSpan_t3430258949  RefreshingAWSCredentials_get_PreemptExpiryTime_m2998598768 (RefreshingAWSCredentials_t1767066958 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.ImmutableCredentials Amazon.Runtime.RefreshingAWSCredentials::GetCredentials()
extern "C"  ImmutableCredentials_t282353664 * RefreshingAWSCredentials_GetCredentials_m2267991724 (RefreshingAWSCredentials_t1767066958 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.RefreshingAWSCredentials::UpdateToGeneratedCredentials(Amazon.Runtime.RefreshingAWSCredentials/CredentialsRefreshState)
extern "C"  void RefreshingAWSCredentials_UpdateToGeneratedCredentials_m2405303855 (RefreshingAWSCredentials_t1767066958 * __this, CredentialsRefreshState_t3294867821 * ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.RefreshingAWSCredentials::get_ShouldUpdate()
extern "C"  bool RefreshingAWSCredentials_get_ShouldUpdate_m1992980814 (RefreshingAWSCredentials_t1767066958 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.RefreshingAWSCredentials/CredentialsRefreshState Amazon.Runtime.RefreshingAWSCredentials::GenerateNewCredentials()
extern "C"  CredentialsRefreshState_t3294867821 * RefreshingAWSCredentials_GenerateNewCredentials_m3237527042 (RefreshingAWSCredentials_t1767066958 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.RefreshingAWSCredentials::ClearCredentials()
extern "C"  void RefreshingAWSCredentials_ClearCredentials_m2294109694 (RefreshingAWSCredentials_t1767066958 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.RefreshingAWSCredentials::.ctor()
extern "C"  void RefreshingAWSCredentials__ctor_m2336079865 (RefreshingAWSCredentials_t1767066958 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
