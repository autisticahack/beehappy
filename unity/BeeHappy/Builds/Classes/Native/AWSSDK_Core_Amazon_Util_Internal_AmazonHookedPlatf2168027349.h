﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.Runtime.Internal.Util.Logger
struct Logger_t2262497814;
// Amazon.Util.Internal.AmazonHookedPlatformInfo
struct AmazonHookedPlatformInfo_t2168027349;
// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Util.Internal.AmazonHookedPlatformInfo
struct  AmazonHookedPlatformInfo_t2168027349  : public Il2CppObject
{
public:
	// System.String Amazon.Util.Internal.AmazonHookedPlatformInfo::device_platform
	String_t* ___device_platform_2;
	// System.String Amazon.Util.Internal.AmazonHookedPlatformInfo::device_model
	String_t* ___device_model_3;
	// System.String Amazon.Util.Internal.AmazonHookedPlatformInfo::device_make
	String_t* ___device_make_4;
	// System.String Amazon.Util.Internal.AmazonHookedPlatformInfo::device_platformVersion
	String_t* ___device_platformVersion_5;
	// System.String Amazon.Util.Internal.AmazonHookedPlatformInfo::device_locale
	String_t* ___device_locale_6;
	// System.String Amazon.Util.Internal.AmazonHookedPlatformInfo::app_version_name
	String_t* ___app_version_name_7;
	// System.String Amazon.Util.Internal.AmazonHookedPlatformInfo::app_version_code
	String_t* ___app_version_code_8;
	// System.String Amazon.Util.Internal.AmazonHookedPlatformInfo::app_package_name
	String_t* ___app_package_name_9;
	// System.String Amazon.Util.Internal.AmazonHookedPlatformInfo::app_title
	String_t* ___app_title_10;
	// System.String Amazon.Util.Internal.AmazonHookedPlatformInfo::<PersistentDataPath>k__BackingField
	String_t* ___U3CPersistentDataPathU3Ek__BackingField_11;
	// System.String Amazon.Util.Internal.AmazonHookedPlatformInfo::<UnityVersion>k__BackingField
	String_t* ___U3CUnityVersionU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_device_platform_2() { return static_cast<int32_t>(offsetof(AmazonHookedPlatformInfo_t2168027349, ___device_platform_2)); }
	inline String_t* get_device_platform_2() const { return ___device_platform_2; }
	inline String_t** get_address_of_device_platform_2() { return &___device_platform_2; }
	inline void set_device_platform_2(String_t* value)
	{
		___device_platform_2 = value;
		Il2CppCodeGenWriteBarrier(&___device_platform_2, value);
	}

	inline static int32_t get_offset_of_device_model_3() { return static_cast<int32_t>(offsetof(AmazonHookedPlatformInfo_t2168027349, ___device_model_3)); }
	inline String_t* get_device_model_3() const { return ___device_model_3; }
	inline String_t** get_address_of_device_model_3() { return &___device_model_3; }
	inline void set_device_model_3(String_t* value)
	{
		___device_model_3 = value;
		Il2CppCodeGenWriteBarrier(&___device_model_3, value);
	}

	inline static int32_t get_offset_of_device_make_4() { return static_cast<int32_t>(offsetof(AmazonHookedPlatformInfo_t2168027349, ___device_make_4)); }
	inline String_t* get_device_make_4() const { return ___device_make_4; }
	inline String_t** get_address_of_device_make_4() { return &___device_make_4; }
	inline void set_device_make_4(String_t* value)
	{
		___device_make_4 = value;
		Il2CppCodeGenWriteBarrier(&___device_make_4, value);
	}

	inline static int32_t get_offset_of_device_platformVersion_5() { return static_cast<int32_t>(offsetof(AmazonHookedPlatformInfo_t2168027349, ___device_platformVersion_5)); }
	inline String_t* get_device_platformVersion_5() const { return ___device_platformVersion_5; }
	inline String_t** get_address_of_device_platformVersion_5() { return &___device_platformVersion_5; }
	inline void set_device_platformVersion_5(String_t* value)
	{
		___device_platformVersion_5 = value;
		Il2CppCodeGenWriteBarrier(&___device_platformVersion_5, value);
	}

	inline static int32_t get_offset_of_device_locale_6() { return static_cast<int32_t>(offsetof(AmazonHookedPlatformInfo_t2168027349, ___device_locale_6)); }
	inline String_t* get_device_locale_6() const { return ___device_locale_6; }
	inline String_t** get_address_of_device_locale_6() { return &___device_locale_6; }
	inline void set_device_locale_6(String_t* value)
	{
		___device_locale_6 = value;
		Il2CppCodeGenWriteBarrier(&___device_locale_6, value);
	}

	inline static int32_t get_offset_of_app_version_name_7() { return static_cast<int32_t>(offsetof(AmazonHookedPlatformInfo_t2168027349, ___app_version_name_7)); }
	inline String_t* get_app_version_name_7() const { return ___app_version_name_7; }
	inline String_t** get_address_of_app_version_name_7() { return &___app_version_name_7; }
	inline void set_app_version_name_7(String_t* value)
	{
		___app_version_name_7 = value;
		Il2CppCodeGenWriteBarrier(&___app_version_name_7, value);
	}

	inline static int32_t get_offset_of_app_version_code_8() { return static_cast<int32_t>(offsetof(AmazonHookedPlatformInfo_t2168027349, ___app_version_code_8)); }
	inline String_t* get_app_version_code_8() const { return ___app_version_code_8; }
	inline String_t** get_address_of_app_version_code_8() { return &___app_version_code_8; }
	inline void set_app_version_code_8(String_t* value)
	{
		___app_version_code_8 = value;
		Il2CppCodeGenWriteBarrier(&___app_version_code_8, value);
	}

	inline static int32_t get_offset_of_app_package_name_9() { return static_cast<int32_t>(offsetof(AmazonHookedPlatformInfo_t2168027349, ___app_package_name_9)); }
	inline String_t* get_app_package_name_9() const { return ___app_package_name_9; }
	inline String_t** get_address_of_app_package_name_9() { return &___app_package_name_9; }
	inline void set_app_package_name_9(String_t* value)
	{
		___app_package_name_9 = value;
		Il2CppCodeGenWriteBarrier(&___app_package_name_9, value);
	}

	inline static int32_t get_offset_of_app_title_10() { return static_cast<int32_t>(offsetof(AmazonHookedPlatformInfo_t2168027349, ___app_title_10)); }
	inline String_t* get_app_title_10() const { return ___app_title_10; }
	inline String_t** get_address_of_app_title_10() { return &___app_title_10; }
	inline void set_app_title_10(String_t* value)
	{
		___app_title_10 = value;
		Il2CppCodeGenWriteBarrier(&___app_title_10, value);
	}

	inline static int32_t get_offset_of_U3CPersistentDataPathU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(AmazonHookedPlatformInfo_t2168027349, ___U3CPersistentDataPathU3Ek__BackingField_11)); }
	inline String_t* get_U3CPersistentDataPathU3Ek__BackingField_11() const { return ___U3CPersistentDataPathU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CPersistentDataPathU3Ek__BackingField_11() { return &___U3CPersistentDataPathU3Ek__BackingField_11; }
	inline void set_U3CPersistentDataPathU3Ek__BackingField_11(String_t* value)
	{
		___U3CPersistentDataPathU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CPersistentDataPathU3Ek__BackingField_11, value);
	}

	inline static int32_t get_offset_of_U3CUnityVersionU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(AmazonHookedPlatformInfo_t2168027349, ___U3CUnityVersionU3Ek__BackingField_12)); }
	inline String_t* get_U3CUnityVersionU3Ek__BackingField_12() const { return ___U3CUnityVersionU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CUnityVersionU3Ek__BackingField_12() { return &___U3CUnityVersionU3Ek__BackingField_12; }
	inline void set_U3CUnityVersionU3Ek__BackingField_12(String_t* value)
	{
		___U3CUnityVersionU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CUnityVersionU3Ek__BackingField_12, value);
	}
};

struct AmazonHookedPlatformInfo_t2168027349_StaticFields
{
public:
	// Amazon.Runtime.Internal.Util.Logger Amazon.Util.Internal.AmazonHookedPlatformInfo::_logger
	Logger_t2262497814 * ____logger_0;
	// Amazon.Util.Internal.AmazonHookedPlatformInfo Amazon.Util.Internal.AmazonHookedPlatformInfo::instance
	AmazonHookedPlatformInfo_t2168027349 * ___instance_1;

public:
	inline static int32_t get_offset_of__logger_0() { return static_cast<int32_t>(offsetof(AmazonHookedPlatformInfo_t2168027349_StaticFields, ____logger_0)); }
	inline Logger_t2262497814 * get__logger_0() const { return ____logger_0; }
	inline Logger_t2262497814 ** get_address_of__logger_0() { return &____logger_0; }
	inline void set__logger_0(Logger_t2262497814 * value)
	{
		____logger_0 = value;
		Il2CppCodeGenWriteBarrier(&____logger_0, value);
	}

	inline static int32_t get_offset_of_instance_1() { return static_cast<int32_t>(offsetof(AmazonHookedPlatformInfo_t2168027349_StaticFields, ___instance_1)); }
	inline AmazonHookedPlatformInfo_t2168027349 * get_instance_1() const { return ___instance_1; }
	inline AmazonHookedPlatformInfo_t2168027349 ** get_address_of_instance_1() { return &___instance_1; }
	inline void set_instance_1(AmazonHookedPlatformInfo_t2168027349 * value)
	{
		___instance_1 = value;
		Il2CppCodeGenWriteBarrier(&___instance_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
