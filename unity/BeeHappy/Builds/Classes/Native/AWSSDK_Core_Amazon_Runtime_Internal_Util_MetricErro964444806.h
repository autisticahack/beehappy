﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Exception
struct Exception_t1927440687;

#include "mscorlib_System_Object2689449295.h"
#include "AWSSDK_Core_Amazon_Runtime_Metric3273440202.h"
#include "mscorlib_System_DateTime693205669.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Util.MetricError
struct  MetricError_t964444806  : public Il2CppObject
{
public:
	// Amazon.Runtime.Metric Amazon.Runtime.Internal.Util.MetricError::<Metric>k__BackingField
	int32_t ___U3CMetricU3Ek__BackingField_0;
	// System.String Amazon.Runtime.Internal.Util.MetricError::<Message>k__BackingField
	String_t* ___U3CMessageU3Ek__BackingField_1;
	// System.Exception Amazon.Runtime.Internal.Util.MetricError::<Exception>k__BackingField
	Exception_t1927440687 * ___U3CExceptionU3Ek__BackingField_2;
	// System.DateTime Amazon.Runtime.Internal.Util.MetricError::<Time>k__BackingField
	DateTime_t693205669  ___U3CTimeU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CMetricU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MetricError_t964444806, ___U3CMetricU3Ek__BackingField_0)); }
	inline int32_t get_U3CMetricU3Ek__BackingField_0() const { return ___U3CMetricU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CMetricU3Ek__BackingField_0() { return &___U3CMetricU3Ek__BackingField_0; }
	inline void set_U3CMetricU3Ek__BackingField_0(int32_t value)
	{
		___U3CMetricU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CMessageU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(MetricError_t964444806, ___U3CMessageU3Ek__BackingField_1)); }
	inline String_t* get_U3CMessageU3Ek__BackingField_1() const { return ___U3CMessageU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CMessageU3Ek__BackingField_1() { return &___U3CMessageU3Ek__BackingField_1; }
	inline void set_U3CMessageU3Ek__BackingField_1(String_t* value)
	{
		___U3CMessageU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CMessageU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CExceptionU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(MetricError_t964444806, ___U3CExceptionU3Ek__BackingField_2)); }
	inline Exception_t1927440687 * get_U3CExceptionU3Ek__BackingField_2() const { return ___U3CExceptionU3Ek__BackingField_2; }
	inline Exception_t1927440687 ** get_address_of_U3CExceptionU3Ek__BackingField_2() { return &___U3CExceptionU3Ek__BackingField_2; }
	inline void set_U3CExceptionU3Ek__BackingField_2(Exception_t1927440687 * value)
	{
		___U3CExceptionU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CExceptionU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CTimeU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(MetricError_t964444806, ___U3CTimeU3Ek__BackingField_3)); }
	inline DateTime_t693205669  get_U3CTimeU3Ek__BackingField_3() const { return ___U3CTimeU3Ek__BackingField_3; }
	inline DateTime_t693205669 * get_address_of_U3CTimeU3Ek__BackingField_3() { return &___U3CTimeU3Ek__BackingField_3; }
	inline void set_U3CTimeU3Ek__BackingField_3(DateTime_t693205669  value)
	{
		___U3CTimeU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
