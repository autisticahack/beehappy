﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.iOS4Unity.ObjC/Libraries
struct  Libraries_t4218932343  : public Il2CppObject
{
public:

public:
};

struct Libraries_t4218932343_StaticFields
{
public:
	// System.IntPtr ThirdParty.iOS4Unity.ObjC/Libraries::Foundation
	IntPtr_t ___Foundation_0;
	// System.IntPtr ThirdParty.iOS4Unity.ObjC/Libraries::UIKit
	IntPtr_t ___UIKit_1;

public:
	inline static int32_t get_offset_of_Foundation_0() { return static_cast<int32_t>(offsetof(Libraries_t4218932343_StaticFields, ___Foundation_0)); }
	inline IntPtr_t get_Foundation_0() const { return ___Foundation_0; }
	inline IntPtr_t* get_address_of_Foundation_0() { return &___Foundation_0; }
	inline void set_Foundation_0(IntPtr_t value)
	{
		___Foundation_0 = value;
	}

	inline static int32_t get_offset_of_UIKit_1() { return static_cast<int32_t>(offsetof(Libraries_t4218932343_StaticFields, ___UIKit_1)); }
	inline IntPtr_t get_UIKit_1() const { return ___UIKit_1; }
	inline IntPtr_t* get_address_of_UIKit_1() { return &___UIKit_1; }
	inline void set_UIKit_1(IntPtr_t value)
	{
		___UIKit_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
