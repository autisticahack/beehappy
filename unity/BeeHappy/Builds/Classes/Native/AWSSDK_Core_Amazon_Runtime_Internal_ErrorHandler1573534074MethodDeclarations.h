﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.ErrorHandler
struct ErrorHandler_t1573534074;
// System.Collections.Generic.IDictionary`2<System.Type,Amazon.Runtime.IExceptionHandler>
struct IDictionary_2_t1347456016;
// Amazon.Runtime.Internal.Util.ILogger
struct ILogger_t676853571;
// Amazon.Runtime.IExecutionContext
struct IExecutionContext_t2477130752;
// Amazon.Runtime.IAsyncExecutionContext
struct IAsyncExecutionContext_t3792344986;
// Amazon.Runtime.IResponseContext
struct IResponseContext_t898521739;
// System.Exception
struct Exception_t1927440687;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1927440687.h"

// System.Collections.Generic.IDictionary`2<System.Type,Amazon.Runtime.IExceptionHandler> Amazon.Runtime.Internal.ErrorHandler::get_ExceptionHandlers()
extern "C"  Il2CppObject* ErrorHandler_get_ExceptionHandlers_m3002075423 (ErrorHandler_t1573534074 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.ErrorHandler::.ctor(Amazon.Runtime.Internal.Util.ILogger)
extern "C"  void ErrorHandler__ctor_m1863315306 (ErrorHandler_t1573534074 * __this, Il2CppObject * ___logger0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.ErrorHandler::InvokeSync(Amazon.Runtime.IExecutionContext)
extern "C"  void ErrorHandler_InvokeSync_m1017472041 (ErrorHandler_t1573534074 * __this, Il2CppObject * ___executionContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.ErrorHandler::InvokeAsyncCallback(Amazon.Runtime.IAsyncExecutionContext)
extern "C"  void ErrorHandler_InvokeAsyncCallback_m1790118191 (ErrorHandler_t1573534074 * __this, Il2CppObject * ___executionContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.ErrorHandler::DisposeReponse(Amazon.Runtime.IResponseContext)
extern "C"  void ErrorHandler_DisposeReponse_m1668439352 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___responseContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.ErrorHandler::ProcessException(Amazon.Runtime.IExecutionContext,System.Exception)
extern "C"  bool ErrorHandler_ProcessException_m1654677070 (ErrorHandler_t1573534074 * __this, Il2CppObject * ___executionContext0, Exception_t1927440687 * ___exception1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
