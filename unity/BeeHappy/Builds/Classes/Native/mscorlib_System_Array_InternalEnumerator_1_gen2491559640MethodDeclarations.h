﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2491559640.h"
#include "mscorlib_System_Array3829468939.h"
#include "AWSSDK_Core_Amazon_Util_Internal_PlatformServices_1632807378.h"

// System.Void System.Array/InternalEnumerator`1<Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2220059369_gshared (InternalEnumerator_1_t2491559640 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m2220059369(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2491559640 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2220059369_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1199800869_gshared (InternalEnumerator_1_t2491559640 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1199800869(__this, method) ((  void (*) (InternalEnumerator_1_t2491559640 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1199800869_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4037583961_gshared (InternalEnumerator_1_t2491559640 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4037583961(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2491559640 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4037583961_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2247344318_gshared (InternalEnumerator_1_t2491559640 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2247344318(__this, method) ((  void (*) (InternalEnumerator_1_t2491559640 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2247344318_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3410927341_gshared (InternalEnumerator_1_t2491559640 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m3410927341(__this, method) ((  bool (*) (InternalEnumerator_1_t2491559640 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3410927341_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m2728670430_gshared (InternalEnumerator_1_t2491559640 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m2728670430(__this, method) ((  int32_t (*) (InternalEnumerator_1_t2491559640 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2728670430_gshared)(__this, method)
