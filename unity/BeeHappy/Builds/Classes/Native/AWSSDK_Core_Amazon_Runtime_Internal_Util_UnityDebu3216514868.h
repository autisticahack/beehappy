﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AWSSDK_Core_Amazon_Runtime_Internal_Util_InternalL2972373343.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Util.UnityDebugLogger
struct  UnityDebugLogger_t3216514868  : public InternalLogger_t2972373343
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
