﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.AsyncOptions
struct AsyncOptions_t558351272;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Amazon.Runtime.AsyncOptions::.ctor()
extern "C"  void AsyncOptions__ctor_m2169674811 (AsyncOptions_t558351272 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.AsyncOptions::get_ExecuteCallbackOnMainThread()
extern "C"  bool AsyncOptions_get_ExecuteCallbackOnMainThread_m4201276568 (AsyncOptions_t558351272 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.AsyncOptions::set_ExecuteCallbackOnMainThread(System.Boolean)
extern "C"  void AsyncOptions_set_ExecuteCallbackOnMainThread_m541727835 (AsyncOptions_t558351272 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Amazon.Runtime.AsyncOptions::get_State()
extern "C"  Il2CppObject * AsyncOptions_get_State_m534435792 (AsyncOptions_t558351272 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
