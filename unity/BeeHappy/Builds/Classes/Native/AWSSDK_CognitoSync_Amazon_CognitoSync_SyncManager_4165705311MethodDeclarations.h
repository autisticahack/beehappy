﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.CognitoSync.SyncManager.SyncManagerException
struct SyncManagerException_t4165705311;
// System.String
struct String_t;
// System.Exception
struct Exception_t1927440687;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Exception1927440687.h"
#include "mscorlib_System_Runtime_Serialization_Serialization228987430.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"

// System.Void Amazon.CognitoSync.SyncManager.SyncManagerException::.ctor(System.String,System.Exception)
extern "C"  void SyncManagerException__ctor_m1331586525 (SyncManagerException_t4165705311 * __this, String_t* ___detailMessage0, Exception_t1927440687 * ___ex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.SyncManager.SyncManagerException::.ctor(System.String)
extern "C"  void SyncManagerException__ctor_m1193603957 (SyncManagerException_t4165705311 * __this, String_t* ___detailMessage0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.SyncManager.SyncManagerException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void SyncManagerException__ctor_m3695379958 (SyncManagerException_t4165705311 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
