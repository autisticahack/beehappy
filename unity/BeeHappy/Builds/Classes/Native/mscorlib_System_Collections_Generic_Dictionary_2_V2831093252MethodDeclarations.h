﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Object>
struct Dictionary_2_t1144560488;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2831093252.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Amazon.Runtime.Metric,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m18447153_gshared (Enumerator_t2831093252 * __this, Dictionary_2_t1144560488 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m18447153(__this, ___host0, method) ((  void (*) (Enumerator_t2831093252 *, Dictionary_2_t1144560488 *, const MethodInfo*))Enumerator__ctor_m18447153_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Amazon.Runtime.Metric,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2846411020_gshared (Enumerator_t2831093252 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2846411020(__this, method) ((  Il2CppObject * (*) (Enumerator_t2831093252 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2846411020_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Amazon.Runtime.Metric,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3089225940_gshared (Enumerator_t2831093252 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3089225940(__this, method) ((  void (*) (Enumerator_t2831093252 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3089225940_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Amazon.Runtime.Metric,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m1070722481_gshared (Enumerator_t2831093252 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1070722481(__this, method) ((  void (*) (Enumerator_t2831093252 *, const MethodInfo*))Enumerator_Dispose_m1070722481_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Amazon.Runtime.Metric,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1585495576_gshared (Enumerator_t2831093252 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1585495576(__this, method) ((  bool (*) (Enumerator_t2831093252 *, const MethodInfo*))Enumerator_MoveNext_m1585495576_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Amazon.Runtime.Metric,System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m1001769806_gshared (Enumerator_t2831093252 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1001769806(__this, method) ((  Il2CppObject * (*) (Enumerator_t2831093252 *, const MethodInfo*))Enumerator_get_Current_m1001769806_gshared)(__this, method)
