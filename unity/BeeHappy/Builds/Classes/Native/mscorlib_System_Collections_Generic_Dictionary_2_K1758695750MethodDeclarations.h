﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3708365277MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m779803571(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t1758695750 *, Dictionary_2_t3570165275 *, const MethodInfo*))KeyCollection__ctor_m944849420_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1141638977(__this, ___item0, method) ((  void (*) (KeyCollection_t1758695750 *, Type_t *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m788825550_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2053167348(__this, method) ((  void (*) (KeyCollection_t1758695750 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2769795483_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2481135615(__this, ___item0, method) ((  bool (*) (KeyCollection_t1758695750 *, Type_t *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m827225850_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3349064616(__this, ___item0, method) ((  bool (*) (KeyCollection_t1758695750 *, Type_t *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2474066187_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1720496714(__this, method) ((  Il2CppObject* (*) (KeyCollection_t1758695750 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3310486225_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m226367250(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1758695750 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m1732125405_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2541010341(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1758695750 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3631903454_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m163335058(__this, method) ((  bool (*) (KeyCollection_t1758695750 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2757042039_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1774204010(__this, method) ((  bool (*) (KeyCollection_t1758695750 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m907397289_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m26615954(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1758695750 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m3998101453_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m571543348(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1758695750 *, TypeU5BU5D_t1664964607*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m1902388931_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::GetEnumerator()
#define KeyCollection_GetEnumerator_m739611759(__this, method) ((  Enumerator_t1964701417  (*) (KeyCollection_t1758695750 *, const MethodInfo*))KeyCollection_GetEnumerator_m131469480_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::get_Count()
#define KeyCollection_get_Count_m1591786730(__this, method) ((  int32_t (*) (KeyCollection_t1758695750 *, const MethodInfo*))KeyCollection_get_Count_m4252672945_gshared)(__this, method)
