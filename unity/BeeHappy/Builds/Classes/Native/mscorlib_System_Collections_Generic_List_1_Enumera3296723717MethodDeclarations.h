﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Amazon.CognitoSync.Model.RecordPatch>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m1509393683(__this, ___l0, method) ((  void (*) (Enumerator_t3296723717 *, List_1_t3761994043 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Amazon.CognitoSync.Model.RecordPatch>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1776206655(__this, method) ((  void (*) (Enumerator_t3296723717 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Amazon.CognitoSync.Model.RecordPatch>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1021259935(__this, method) ((  Il2CppObject * (*) (Enumerator_t3296723717 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Amazon.CognitoSync.Model.RecordPatch>::Dispose()
#define Enumerator_Dispose_m2901938954(__this, method) ((  void (*) (Enumerator_t3296723717 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Amazon.CognitoSync.Model.RecordPatch>::VerifyState()
#define Enumerator_VerifyState_m1640986405(__this, method) ((  void (*) (Enumerator_t3296723717 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Amazon.CognitoSync.Model.RecordPatch>::MoveNext()
#define Enumerator_MoveNext_m3526711239(__this, method) ((  bool (*) (Enumerator_t3296723717 *, const MethodInfo*))Enumerator_MoveNext_m984484162_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Amazon.CognitoSync.Model.RecordPatch>::get_Current()
#define Enumerator_get_Current_m1063608459(__this, method) ((  RecordPatch_t97905615 * (*) (Enumerator_t3296723717 *, const MethodInfo*))Enumerator_get_Current_m2193722336_gshared)(__this, method)
