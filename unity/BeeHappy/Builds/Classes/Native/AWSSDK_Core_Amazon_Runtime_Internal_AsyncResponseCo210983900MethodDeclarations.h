﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.AsyncResponseContext
struct AsyncResponseContext_t210983900;
// Amazon.Runtime.Internal.RuntimeAsyncResult
struct RuntimeAsyncResult_t4279356013;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_RuntimeAsyncRe4279356013.h"

// Amazon.Runtime.Internal.RuntimeAsyncResult Amazon.Runtime.Internal.AsyncResponseContext::get_AsyncResult()
extern "C"  RuntimeAsyncResult_t4279356013 * AsyncResponseContext_get_AsyncResult_m3293650243 (AsyncResponseContext_t210983900 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.AsyncResponseContext::set_AsyncResult(Amazon.Runtime.Internal.RuntimeAsyncResult)
extern "C"  void AsyncResponseContext_set_AsyncResult_m1599880088 (AsyncResponseContext_t210983900 * __this, RuntimeAsyncResult_t4279356013 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.AsyncResponseContext::.ctor()
extern "C"  void AsyncResponseContext__ctor_m432038274 (AsyncResponseContext_t210983900 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
