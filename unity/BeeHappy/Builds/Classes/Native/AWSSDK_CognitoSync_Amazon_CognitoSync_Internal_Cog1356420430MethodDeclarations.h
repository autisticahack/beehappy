﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.CognitoSync.AmazonCognitoSyncRequest
struct AmazonCognitoSyncRequest_t2140463921;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_AmazonCognit2140463921.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Amazon.CognitoSync.Internal.CognitoCredentialsRetriever/CSRequestCache::.cctor()
extern "C"  void CSRequestCache__cctor_m4247985413 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Internal.CognitoCredentialsRetriever/CSRequestCache::PopulateCache()
extern "C"  void CSRequestCache_PopulateCache_m3397142844 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Internal.CognitoCredentialsRetriever/CSRequestCache::SetFields(Amazon.CognitoSync.AmazonCognitoSyncRequest,System.String,System.String)
extern "C"  void CSRequestCache_SetFields_m1057998170 (Il2CppObject * __this /* static, unused */, AmazonCognitoSyncRequest_t2140463921 * ___request0, String_t* ___identityPoolId1, String_t* ___identityId2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
