﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.IDictionary`2<System.String,System.String>
struct IDictionary_2_t1943082916;
// System.String
struct String_t;
// System.Uri
struct Uri_t19570940;
// Amazon.Runtime.AmazonWebServiceRequest
struct AmazonWebServiceRequest_t3384026212;
// Amazon.Runtime.AmazonWebServiceResponse
struct AmazonWebServiceResponse_t529043356;

#include "AWSSDK_Core_Amazon_Runtime_ResponseEventArgs4056063878.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.WebServiceResponseEventArgs
struct  WebServiceResponseEventArgs_t1104338495  : public ResponseEventArgs_t4056063878
{
public:
	// System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.Runtime.WebServiceResponseEventArgs::<RequestHeaders>k__BackingField
	Il2CppObject* ___U3CRequestHeadersU3Ek__BackingField_1;
	// System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.Runtime.WebServiceResponseEventArgs::<ResponseHeaders>k__BackingField
	Il2CppObject* ___U3CResponseHeadersU3Ek__BackingField_2;
	// System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.Runtime.WebServiceResponseEventArgs::<Parameters>k__BackingField
	Il2CppObject* ___U3CParametersU3Ek__BackingField_3;
	// System.String Amazon.Runtime.WebServiceResponseEventArgs::<ServiceName>k__BackingField
	String_t* ___U3CServiceNameU3Ek__BackingField_4;
	// System.Uri Amazon.Runtime.WebServiceResponseEventArgs::<Endpoint>k__BackingField
	Uri_t19570940 * ___U3CEndpointU3Ek__BackingField_5;
	// Amazon.Runtime.AmazonWebServiceRequest Amazon.Runtime.WebServiceResponseEventArgs::<Request>k__BackingField
	AmazonWebServiceRequest_t3384026212 * ___U3CRequestU3Ek__BackingField_6;
	// Amazon.Runtime.AmazonWebServiceResponse Amazon.Runtime.WebServiceResponseEventArgs::<Response>k__BackingField
	AmazonWebServiceResponse_t529043356 * ___U3CResponseU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_U3CRequestHeadersU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(WebServiceResponseEventArgs_t1104338495, ___U3CRequestHeadersU3Ek__BackingField_1)); }
	inline Il2CppObject* get_U3CRequestHeadersU3Ek__BackingField_1() const { return ___U3CRequestHeadersU3Ek__BackingField_1; }
	inline Il2CppObject** get_address_of_U3CRequestHeadersU3Ek__BackingField_1() { return &___U3CRequestHeadersU3Ek__BackingField_1; }
	inline void set_U3CRequestHeadersU3Ek__BackingField_1(Il2CppObject* value)
	{
		___U3CRequestHeadersU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CRequestHeadersU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CResponseHeadersU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(WebServiceResponseEventArgs_t1104338495, ___U3CResponseHeadersU3Ek__BackingField_2)); }
	inline Il2CppObject* get_U3CResponseHeadersU3Ek__BackingField_2() const { return ___U3CResponseHeadersU3Ek__BackingField_2; }
	inline Il2CppObject** get_address_of_U3CResponseHeadersU3Ek__BackingField_2() { return &___U3CResponseHeadersU3Ek__BackingField_2; }
	inline void set_U3CResponseHeadersU3Ek__BackingField_2(Il2CppObject* value)
	{
		___U3CResponseHeadersU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CResponseHeadersU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CParametersU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(WebServiceResponseEventArgs_t1104338495, ___U3CParametersU3Ek__BackingField_3)); }
	inline Il2CppObject* get_U3CParametersU3Ek__BackingField_3() const { return ___U3CParametersU3Ek__BackingField_3; }
	inline Il2CppObject** get_address_of_U3CParametersU3Ek__BackingField_3() { return &___U3CParametersU3Ek__BackingField_3; }
	inline void set_U3CParametersU3Ek__BackingField_3(Il2CppObject* value)
	{
		___U3CParametersU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CParametersU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3CServiceNameU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(WebServiceResponseEventArgs_t1104338495, ___U3CServiceNameU3Ek__BackingField_4)); }
	inline String_t* get_U3CServiceNameU3Ek__BackingField_4() const { return ___U3CServiceNameU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CServiceNameU3Ek__BackingField_4() { return &___U3CServiceNameU3Ek__BackingField_4; }
	inline void set_U3CServiceNameU3Ek__BackingField_4(String_t* value)
	{
		___U3CServiceNameU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CServiceNameU3Ek__BackingField_4, value);
	}

	inline static int32_t get_offset_of_U3CEndpointU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(WebServiceResponseEventArgs_t1104338495, ___U3CEndpointU3Ek__BackingField_5)); }
	inline Uri_t19570940 * get_U3CEndpointU3Ek__BackingField_5() const { return ___U3CEndpointU3Ek__BackingField_5; }
	inline Uri_t19570940 ** get_address_of_U3CEndpointU3Ek__BackingField_5() { return &___U3CEndpointU3Ek__BackingField_5; }
	inline void set_U3CEndpointU3Ek__BackingField_5(Uri_t19570940 * value)
	{
		___U3CEndpointU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CEndpointU3Ek__BackingField_5, value);
	}

	inline static int32_t get_offset_of_U3CRequestU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(WebServiceResponseEventArgs_t1104338495, ___U3CRequestU3Ek__BackingField_6)); }
	inline AmazonWebServiceRequest_t3384026212 * get_U3CRequestU3Ek__BackingField_6() const { return ___U3CRequestU3Ek__BackingField_6; }
	inline AmazonWebServiceRequest_t3384026212 ** get_address_of_U3CRequestU3Ek__BackingField_6() { return &___U3CRequestU3Ek__BackingField_6; }
	inline void set_U3CRequestU3Ek__BackingField_6(AmazonWebServiceRequest_t3384026212 * value)
	{
		___U3CRequestU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CRequestU3Ek__BackingField_6, value);
	}

	inline static int32_t get_offset_of_U3CResponseU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(WebServiceResponseEventArgs_t1104338495, ___U3CResponseU3Ek__BackingField_7)); }
	inline AmazonWebServiceResponse_t529043356 * get_U3CResponseU3Ek__BackingField_7() const { return ___U3CResponseU3Ek__BackingField_7; }
	inline AmazonWebServiceResponse_t529043356 ** get_address_of_U3CResponseU3Ek__BackingField_7() { return &___U3CResponseU3Ek__BackingField_7; }
	inline void set_U3CResponseU3Ek__BackingField_7(AmazonWebServiceResponse_t529043356 * value)
	{
		___U3CResponseU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CResponseU3Ek__BackingField_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
