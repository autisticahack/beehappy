﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Collections.Generic.HashSet`1<System.String>
struct HashSet_1_t362681087;
// System.IO.Stream
struct Stream_t3255436806;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// Amazon.Runtime.Internal.Util.ILogger
struct ILogger_t676853571;
// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"
#include "System_System_Net_HttpStatusCode1898409641.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Transform.UnityWebResponseData
struct  UnityWebResponseData_t3141113362  : public Il2CppObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.String> Amazon.Runtime.Internal.Transform.UnityWebResponseData::_headers
	Dictionary_2_t3943999495 * ____headers_0;
	// System.String[] Amazon.Runtime.Internal.Transform.UnityWebResponseData::_headerNames
	StringU5BU5D_t1642385972* ____headerNames_1;
	// System.Collections.Generic.HashSet`1<System.String> Amazon.Runtime.Internal.Transform.UnityWebResponseData::_headerNamesSet
	HashSet_1_t362681087 * ____headerNamesSet_2;
	// System.IO.Stream Amazon.Runtime.Internal.Transform.UnityWebResponseData::_responseStream
	Stream_t3255436806 * ____responseStream_3;
	// System.Byte[] Amazon.Runtime.Internal.Transform.UnityWebResponseData::_responseBody
	ByteU5BU5D_t3397334013* ____responseBody_4;
	// Amazon.Runtime.Internal.Util.ILogger Amazon.Runtime.Internal.Transform.UnityWebResponseData::_logger
	Il2CppObject * ____logger_5;
	// System.Int64 Amazon.Runtime.Internal.Transform.UnityWebResponseData::<ContentLength>k__BackingField
	int64_t ___U3CContentLengthU3Ek__BackingField_6;
	// System.String Amazon.Runtime.Internal.Transform.UnityWebResponseData::<ContentType>k__BackingField
	String_t* ___U3CContentTypeU3Ek__BackingField_7;
	// System.Net.HttpStatusCode Amazon.Runtime.Internal.Transform.UnityWebResponseData::<StatusCode>k__BackingField
	int32_t ___U3CStatusCodeU3Ek__BackingField_8;
	// System.Boolean Amazon.Runtime.Internal.Transform.UnityWebResponseData::<IsSuccessStatusCode>k__BackingField
	bool ___U3CIsSuccessStatusCodeU3Ek__BackingField_9;

public:
	inline static int32_t get_offset_of__headers_0() { return static_cast<int32_t>(offsetof(UnityWebResponseData_t3141113362, ____headers_0)); }
	inline Dictionary_2_t3943999495 * get__headers_0() const { return ____headers_0; }
	inline Dictionary_2_t3943999495 ** get_address_of__headers_0() { return &____headers_0; }
	inline void set__headers_0(Dictionary_2_t3943999495 * value)
	{
		____headers_0 = value;
		Il2CppCodeGenWriteBarrier(&____headers_0, value);
	}

	inline static int32_t get_offset_of__headerNames_1() { return static_cast<int32_t>(offsetof(UnityWebResponseData_t3141113362, ____headerNames_1)); }
	inline StringU5BU5D_t1642385972* get__headerNames_1() const { return ____headerNames_1; }
	inline StringU5BU5D_t1642385972** get_address_of__headerNames_1() { return &____headerNames_1; }
	inline void set__headerNames_1(StringU5BU5D_t1642385972* value)
	{
		____headerNames_1 = value;
		Il2CppCodeGenWriteBarrier(&____headerNames_1, value);
	}

	inline static int32_t get_offset_of__headerNamesSet_2() { return static_cast<int32_t>(offsetof(UnityWebResponseData_t3141113362, ____headerNamesSet_2)); }
	inline HashSet_1_t362681087 * get__headerNamesSet_2() const { return ____headerNamesSet_2; }
	inline HashSet_1_t362681087 ** get_address_of__headerNamesSet_2() { return &____headerNamesSet_2; }
	inline void set__headerNamesSet_2(HashSet_1_t362681087 * value)
	{
		____headerNamesSet_2 = value;
		Il2CppCodeGenWriteBarrier(&____headerNamesSet_2, value);
	}

	inline static int32_t get_offset_of__responseStream_3() { return static_cast<int32_t>(offsetof(UnityWebResponseData_t3141113362, ____responseStream_3)); }
	inline Stream_t3255436806 * get__responseStream_3() const { return ____responseStream_3; }
	inline Stream_t3255436806 ** get_address_of__responseStream_3() { return &____responseStream_3; }
	inline void set__responseStream_3(Stream_t3255436806 * value)
	{
		____responseStream_3 = value;
		Il2CppCodeGenWriteBarrier(&____responseStream_3, value);
	}

	inline static int32_t get_offset_of__responseBody_4() { return static_cast<int32_t>(offsetof(UnityWebResponseData_t3141113362, ____responseBody_4)); }
	inline ByteU5BU5D_t3397334013* get__responseBody_4() const { return ____responseBody_4; }
	inline ByteU5BU5D_t3397334013** get_address_of__responseBody_4() { return &____responseBody_4; }
	inline void set__responseBody_4(ByteU5BU5D_t3397334013* value)
	{
		____responseBody_4 = value;
		Il2CppCodeGenWriteBarrier(&____responseBody_4, value);
	}

	inline static int32_t get_offset_of__logger_5() { return static_cast<int32_t>(offsetof(UnityWebResponseData_t3141113362, ____logger_5)); }
	inline Il2CppObject * get__logger_5() const { return ____logger_5; }
	inline Il2CppObject ** get_address_of__logger_5() { return &____logger_5; }
	inline void set__logger_5(Il2CppObject * value)
	{
		____logger_5 = value;
		Il2CppCodeGenWriteBarrier(&____logger_5, value);
	}

	inline static int32_t get_offset_of_U3CContentLengthU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(UnityWebResponseData_t3141113362, ___U3CContentLengthU3Ek__BackingField_6)); }
	inline int64_t get_U3CContentLengthU3Ek__BackingField_6() const { return ___U3CContentLengthU3Ek__BackingField_6; }
	inline int64_t* get_address_of_U3CContentLengthU3Ek__BackingField_6() { return &___U3CContentLengthU3Ek__BackingField_6; }
	inline void set_U3CContentLengthU3Ek__BackingField_6(int64_t value)
	{
		___U3CContentLengthU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CContentTypeU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(UnityWebResponseData_t3141113362, ___U3CContentTypeU3Ek__BackingField_7)); }
	inline String_t* get_U3CContentTypeU3Ek__BackingField_7() const { return ___U3CContentTypeU3Ek__BackingField_7; }
	inline String_t** get_address_of_U3CContentTypeU3Ek__BackingField_7() { return &___U3CContentTypeU3Ek__BackingField_7; }
	inline void set_U3CContentTypeU3Ek__BackingField_7(String_t* value)
	{
		___U3CContentTypeU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CContentTypeU3Ek__BackingField_7, value);
	}

	inline static int32_t get_offset_of_U3CStatusCodeU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(UnityWebResponseData_t3141113362, ___U3CStatusCodeU3Ek__BackingField_8)); }
	inline int32_t get_U3CStatusCodeU3Ek__BackingField_8() const { return ___U3CStatusCodeU3Ek__BackingField_8; }
	inline int32_t* get_address_of_U3CStatusCodeU3Ek__BackingField_8() { return &___U3CStatusCodeU3Ek__BackingField_8; }
	inline void set_U3CStatusCodeU3Ek__BackingField_8(int32_t value)
	{
		___U3CStatusCodeU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CIsSuccessStatusCodeU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(UnityWebResponseData_t3141113362, ___U3CIsSuccessStatusCodeU3Ek__BackingField_9)); }
	inline bool get_U3CIsSuccessStatusCodeU3Ek__BackingField_9() const { return ___U3CIsSuccessStatusCodeU3Ek__BackingField_9; }
	inline bool* get_address_of_U3CIsSuccessStatusCodeU3Ek__BackingField_9() { return &___U3CIsSuccessStatusCodeU3Ek__BackingField_9; }
	inline void set_U3CIsSuccessStatusCodeU3Ek__BackingField_9(bool value)
	{
		___U3CIsSuccessStatusCodeU3Ek__BackingField_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
