﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Security.Cryptography.HashAlgorithm
struct HashAlgorithm_t2624936259;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Util.CryptoUtilFactory/CryptoUtil
struct  CryptoUtil_t1025015063  : public Il2CppObject
{
public:

public:
};

struct CryptoUtil_t1025015063_ThreadStaticFields
{
public:
	// System.Security.Cryptography.HashAlgorithm Amazon.Util.CryptoUtilFactory/CryptoUtil::_hashAlgorithm
	HashAlgorithm_t2624936259 * ____hashAlgorithm_0;

public:
	inline static int32_t get_offset_of__hashAlgorithm_0() { return static_cast<int32_t>(offsetof(CryptoUtil_t1025015063_ThreadStaticFields, ____hashAlgorithm_0)); }
	inline HashAlgorithm_t2624936259 * get__hashAlgorithm_0() const { return ____hashAlgorithm_0; }
	inline HashAlgorithm_t2624936259 ** get_address_of__hashAlgorithm_0() { return &____hashAlgorithm_0; }
	inline void set__hashAlgorithm_0(HashAlgorithm_t2624936259 * value)
	{
		____hashAlgorithm_0 = value;
		Il2CppCodeGenWriteBarrier(&____hashAlgorithm_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
