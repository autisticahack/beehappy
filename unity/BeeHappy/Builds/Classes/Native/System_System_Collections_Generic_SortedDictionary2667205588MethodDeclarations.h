﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_SortedDictionary1004715516MethodDeclarations.h"

// System.Void System.Collections.Generic.SortedDictionary`2/Enumerator<System.String,System.String>::.ctor(System.Collections.Generic.SortedDictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1158367813(__this, ___dic0, method) ((  void (*) (Enumerator_t2667205588 *, SortedDictionary_2_t1298644088 *, const MethodInfo*))Enumerator__ctor_m1530705605_gshared)(__this, ___dic0, method)
// System.Collections.DictionaryEntry System.Collections.Generic.SortedDictionary`2/Enumerator<System.String,System.String>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2219137272(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t2667205588 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3759775480_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedDictionary`2/Enumerator<System.String,System.String>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1516599877(__this, method) ((  Il2CppObject * (*) (Enumerator_t2667205588 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2082684101_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedDictionary`2/Enumerator<System.String,System.String>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m786754349(__this, method) ((  Il2CppObject * (*) (Enumerator_t2667205588 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m8986413_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedDictionary`2/Enumerator<System.String,System.String>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2054818575(__this, method) ((  Il2CppObject * (*) (Enumerator_t2667205588 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3256116175_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2/Enumerator<System.String,System.String>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3254740019(__this, method) ((  void (*) (Enumerator_t2667205588 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1941930227_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.SortedDictionary`2/Enumerator<System.String,System.String>::get_Current()
#define Enumerator_get_Current_m4150195463(__this, method) ((  KeyValuePair_2_t1701344717  (*) (Enumerator_t2667205588 *, const MethodInfo*))Enumerator_get_Current_m2922944951_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2/Enumerator<System.String,System.String>::MoveNext()
#define Enumerator_MoveNext_m960175192(__this, method) ((  bool (*) (Enumerator_t2667205588 *, const MethodInfo*))Enumerator_MoveNext_m1169387503_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2/Enumerator<System.String,System.String>::Dispose()
#define Enumerator_Dispose_m1015361250(__this, method) ((  void (*) (Enumerator_t2667205588 *, const MethodInfo*))Enumerator_Dispose_m427679586_gshared)(__this, method)
// System.Collections.Generic.SortedDictionary`2/Node<TKey,TValue> System.Collections.Generic.SortedDictionary`2/Enumerator<System.String,System.String>::get_CurrentNode()
#define Enumerator_get_CurrentNode_m1294785493(__this, method) ((  Node_t4175940316 * (*) (Enumerator_t2667205588 *, const MethodInfo*))Enumerator_get_CurrentNode_m1168849237_gshared)(__this, method)
