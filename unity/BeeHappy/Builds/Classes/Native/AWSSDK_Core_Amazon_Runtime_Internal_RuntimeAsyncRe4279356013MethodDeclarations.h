﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.RuntimeAsyncResult
struct RuntimeAsyncResult_t4279356013;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// System.Object
struct Il2CppObject;
// System.Threading.WaitHandle
struct WaitHandle_t677569169;
// System.Exception
struct Exception_t1927440687;
// Amazon.Runtime.AmazonWebServiceResponse
struct AmazonWebServiceResponse_t529043356;
// Amazon.Runtime.AmazonWebServiceRequest
struct AmazonWebServiceRequest_t3384026212;
// System.Action`4<Amazon.Runtime.AmazonWebServiceRequest,Amazon.Runtime.AmazonWebServiceResponse,System.Exception,Amazon.Runtime.AsyncOptions>
struct Action_4_t897279368;
// Amazon.Runtime.AsyncOptions
struct AsyncOptions_t558351272;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_AsyncCallback163412349.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Exception1927440687.h"
#include "AWSSDK_Core_Amazon_Runtime_AmazonWebServiceResponse529043356.h"
#include "AWSSDK_Core_Amazon_Runtime_AmazonWebServiceRequest3384026212.h"
#include "AWSSDK_Core_Amazon_Runtime_AsyncOptions558351272.h"

// System.Void Amazon.Runtime.Internal.RuntimeAsyncResult::.ctor(System.AsyncCallback,System.Object)
extern "C"  void RuntimeAsyncResult__ctor_m2161030137 (RuntimeAsyncResult_t4279356013 * __this, AsyncCallback_t163412349 * ___asyncCallback0, Il2CppObject * ___asyncState1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.RuntimeAsyncResult::set_AsyncCallback(System.AsyncCallback)
extern "C"  void RuntimeAsyncResult_set_AsyncCallback_m3436157785 (RuntimeAsyncResult_t4279356013 * __this, AsyncCallback_t163412349 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Amazon.Runtime.Internal.RuntimeAsyncResult::get_AsyncState()
extern "C"  Il2CppObject * RuntimeAsyncResult_get_AsyncState_m1094448564 (RuntimeAsyncResult_t4279356013 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.RuntimeAsyncResult::set_AsyncState(System.Object)
extern "C"  void RuntimeAsyncResult_set_AsyncState_m2486274281 (RuntimeAsyncResult_t4279356013 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.WaitHandle Amazon.Runtime.Internal.RuntimeAsyncResult::get_AsyncWaitHandle()
extern "C"  WaitHandle_t677569169 * RuntimeAsyncResult_get_AsyncWaitHandle_m57154924 (RuntimeAsyncResult_t4279356013 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.RuntimeAsyncResult::get_CompletedSynchronously()
extern "C"  bool RuntimeAsyncResult_get_CompletedSynchronously_m2977564163 (RuntimeAsyncResult_t4279356013 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.RuntimeAsyncResult::set_CompletedSynchronously(System.Boolean)
extern "C"  void RuntimeAsyncResult_set_CompletedSynchronously_m1525057850 (RuntimeAsyncResult_t4279356013 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.RuntimeAsyncResult::get_IsCompleted()
extern "C"  bool RuntimeAsyncResult_get_IsCompleted_m4187064349 (RuntimeAsyncResult_t4279356013 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.RuntimeAsyncResult::set_IsCompleted(System.Boolean)
extern "C"  void RuntimeAsyncResult_set_IsCompleted_m890452696 (RuntimeAsyncResult_t4279356013 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception Amazon.Runtime.Internal.RuntimeAsyncResult::get_Exception()
extern "C"  Exception_t1927440687 * RuntimeAsyncResult_get_Exception_m1028366466 (RuntimeAsyncResult_t4279356013 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.RuntimeAsyncResult::set_Exception(System.Exception)
extern "C"  void RuntimeAsyncResult_set_Exception_m3870744969 (RuntimeAsyncResult_t4279356013 * __this, Exception_t1927440687 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.AmazonWebServiceResponse Amazon.Runtime.Internal.RuntimeAsyncResult::get_Response()
extern "C"  AmazonWebServiceResponse_t529043356 * RuntimeAsyncResult_get_Response_m916605326 (RuntimeAsyncResult_t4279356013 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.RuntimeAsyncResult::set_Response(Amazon.Runtime.AmazonWebServiceResponse)
extern "C"  void RuntimeAsyncResult_set_Response_m3209886687 (RuntimeAsyncResult_t4279356013 * __this, AmazonWebServiceResponse_t529043356 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.AmazonWebServiceRequest Amazon.Runtime.Internal.RuntimeAsyncResult::get_Request()
extern "C"  AmazonWebServiceRequest_t3384026212 * RuntimeAsyncResult_get_Request_m267632810 (RuntimeAsyncResult_t4279356013 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.RuntimeAsyncResult::set_Request(Amazon.Runtime.AmazonWebServiceRequest)
extern "C"  void RuntimeAsyncResult_set_Request_m2154165151 (RuntimeAsyncResult_t4279356013 * __this, AmazonWebServiceRequest_t3384026212 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action`4<Amazon.Runtime.AmazonWebServiceRequest,Amazon.Runtime.AmazonWebServiceResponse,System.Exception,Amazon.Runtime.AsyncOptions> Amazon.Runtime.Internal.RuntimeAsyncResult::get_Action()
extern "C"  Action_4_t897279368 * RuntimeAsyncResult_get_Action_m804115394 (RuntimeAsyncResult_t4279356013 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.RuntimeAsyncResult::set_Action(System.Action`4<Amazon.Runtime.AmazonWebServiceRequest,Amazon.Runtime.AmazonWebServiceResponse,System.Exception,Amazon.Runtime.AsyncOptions>)
extern "C"  void RuntimeAsyncResult_set_Action_m4120194101 (RuntimeAsyncResult_t4279356013 * __this, Action_4_t897279368 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.AsyncOptions Amazon.Runtime.Internal.RuntimeAsyncResult::get_AsyncOptions()
extern "C"  AsyncOptions_t558351272 * RuntimeAsyncResult_get_AsyncOptions_m4071321011 (RuntimeAsyncResult_t4279356013 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.RuntimeAsyncResult::set_AsyncOptions(Amazon.Runtime.AsyncOptions)
extern "C"  void RuntimeAsyncResult_set_AsyncOptions_m3285780768 (RuntimeAsyncResult_t4279356013 * __this, AsyncOptions_t558351272 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.RuntimeAsyncResult::SignalWaitHandle()
extern "C"  void RuntimeAsyncResult_SignalWaitHandle_m1367620780 (RuntimeAsyncResult_t4279356013 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.RuntimeAsyncResult::HandleException(System.Exception)
extern "C"  void RuntimeAsyncResult_HandleException_m3594319118 (RuntimeAsyncResult_t4279356013 * __this, Exception_t1927440687 * ___exception0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.RuntimeAsyncResult::InvokeCallback()
extern "C"  void RuntimeAsyncResult_InvokeCallback_m607017372 (RuntimeAsyncResult_t4279356013 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.RuntimeAsyncResult::Dispose(System.Boolean)
extern "C"  void RuntimeAsyncResult_Dispose_m1101804483 (RuntimeAsyncResult_t4279356013 * __this, bool ___disposing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.RuntimeAsyncResult::Dispose()
extern "C"  void RuntimeAsyncResult_Dispose_m4106486402 (RuntimeAsyncResult_t4279356013 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
