﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3601534125MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Func`2<System.IntPtr,System.Object>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m4062853149(__this, ___dictionary0, method) ((  void (*) (Enumerator_t2637884674 *, Dictionary_2_t1317859972 *, const MethodInfo*))Enumerator__ctor_m3742107451_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Func`2<System.IntPtr,System.Object>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1765967312(__this, method) ((  Il2CppObject * (*) (Enumerator_t2637884674 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m229223308_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Func`2<System.IntPtr,System.Object>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3237843750(__this, method) ((  void (*) (Enumerator_t2637884674 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3225937576_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Func`2<System.IntPtr,System.Object>>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3507500651(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t2637884674 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m221119093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Func`2<System.IntPtr,System.Object>>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2853500810(__this, method) ((  Il2CppObject * (*) (Enumerator_t2637884674 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m467957770_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Func`2<System.IntPtr,System.Object>>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m24329252(__this, method) ((  Il2CppObject * (*) (Enumerator_t2637884674 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2325383168_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Func`2<System.IntPtr,System.Object>>::MoveNext()
#define Enumerator_MoveNext_m2962939330(__this, method) ((  bool (*) (Enumerator_t2637884674 *, const MethodInfo*))Enumerator_MoveNext_m3349738440_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Func`2<System.IntPtr,System.Object>>::get_Current()
#define Enumerator_get_Current_m1668535194(__this, method) ((  KeyValuePair_2_t3370172490  (*) (Enumerator_t2637884674 *, const MethodInfo*))Enumerator_get_Current_m25299632_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Func`2<System.IntPtr,System.Object>>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m3396033545(__this, method) ((  Type_t * (*) (Enumerator_t2637884674 *, const MethodInfo*))Enumerator_get_CurrentKey_m3839846791_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Func`2<System.IntPtr,System.Object>>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m4251840809(__this, method) ((  Func_2_t3675469371 * (*) (Enumerator_t2637884674 *, const MethodInfo*))Enumerator_get_CurrentValue_m402763047_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Func`2<System.IntPtr,System.Object>>::Reset()
#define Enumerator_Reset_m1537716435(__this, method) ((  void (*) (Enumerator_t2637884674 *, const MethodInfo*))Enumerator_Reset_m3129803197_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Func`2<System.IntPtr,System.Object>>::VerifyState()
#define Enumerator_VerifyState_m3049712262(__this, method) ((  void (*) (Enumerator_t2637884674 *, const MethodInfo*))Enumerator_VerifyState_m262343092_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Func`2<System.IntPtr,System.Object>>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m333985374(__this, method) ((  void (*) (Enumerator_t2637884674 *, const MethodInfo*))Enumerator_VerifyCurrent_m1702320752_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Func`2<System.IntPtr,System.Object>>::Dispose()
#define Enumerator_Dispose_m764571829(__this, method) ((  void (*) (Enumerator_t2637884674 *, const MethodInfo*))Enumerator_Dispose_m1905011127_gshared)(__this, method)
