﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,System.Func`2<System.IntPtr,System.Object>>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m1595261323(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3370172490 *, Type_t *, Func_2_t3675469371 *, const MethodInfo*))KeyValuePair_2__ctor_m1640124561_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Type,System.Func`2<System.IntPtr,System.Object>>::get_Key()
#define KeyValuePair_2_get_Key_m334911753(__this, method) ((  Type_t * (*) (KeyValuePair_2_t3370172490 *, const MethodInfo*))KeyValuePair_2_get_Key_m2561166459_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,System.Func`2<System.IntPtr,System.Object>>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m3459970496(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3370172490 *, Type_t *, const MethodInfo*))KeyValuePair_2_set_Key_m744486900_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Type,System.Func`2<System.IntPtr,System.Object>>::get_Value()
#define KeyValuePair_2_get_Value_m2185801441(__this, method) ((  Func_2_t3675469371 * (*) (KeyValuePair_2_t3370172490 *, const MethodInfo*))KeyValuePair_2_get_Value_m499643803_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,System.Func`2<System.IntPtr,System.Object>>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m3631035464(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3370172490 *, Func_2_t3675469371 *, const MethodInfo*))KeyValuePair_2_set_Value_m1416408204_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Type,System.Func`2<System.IntPtr,System.Object>>::ToString()
#define KeyValuePair_2_ToString_m1394860574(__this, method) ((  String_t* (*) (KeyValuePair_2_t3370172490 *, const MethodInfo*))KeyValuePair_2_ToString_m2613351884_gshared)(__this, method)
