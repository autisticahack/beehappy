﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext
struct JsonUnmarshallerContext_t456235889;
// System.IO.Stream
struct Stream_t3255436806;
// Amazon.Runtime.Internal.Transform.IWebResponseData
struct IWebResponseData_t3815728244;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_Stream3255436806.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_JsonToken2445581255.h"

// System.Void Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext::.ctor(System.IO.Stream,System.Boolean,Amazon.Runtime.Internal.Transform.IWebResponseData)
extern "C"  void JsonUnmarshallerContext__ctor_m1633836060 (JsonUnmarshallerContext_t456235889 * __this, Stream_t3255436806 * ___responseStream0, bool ___maintainResponseBody1, Il2CppObject * ___responseData2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext::get_IsStartOfDocument()
extern "C"  bool JsonUnmarshallerContext_get_IsStartOfDocument_m1185703448 (JsonUnmarshallerContext_t456235889 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext::get_IsEndElement()
extern "C"  bool JsonUnmarshallerContext_get_IsEndElement_m3037054527 (JsonUnmarshallerContext_t456235889 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext::get_IsStartElement()
extern "C"  bool JsonUnmarshallerContext_get_IsStartElement_m833068986 (JsonUnmarshallerContext_t456235889 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext::get_CurrentDepth()
extern "C"  int32_t JsonUnmarshallerContext_get_CurrentDepth_m1915029944 (JsonUnmarshallerContext_t456235889 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext::get_CurrentPath()
extern "C"  String_t* JsonUnmarshallerContext_get_CurrentPath_m352118129 (JsonUnmarshallerContext_t456235889 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext::Read()
extern "C"  bool JsonUnmarshallerContext_Read_m2348879883 (JsonUnmarshallerContext_t456235889 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext::Peek(ThirdParty.Json.LitJson.JsonToken)
extern "C"  bool JsonUnmarshallerContext_Peek_m2614448275 (JsonUnmarshallerContext_t456235889 * __this, int32_t ___token0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext::ReadText()
extern "C"  String_t* JsonUnmarshallerContext_ReadText_m1061829471 (JsonUnmarshallerContext_t456235889 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ThirdParty.Json.LitJson.JsonToken Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext::get_CurrentTokenType()
extern "C"  int32_t JsonUnmarshallerContext_get_CurrentTokenType_m2873167924 (JsonUnmarshallerContext_t456235889 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext::get_Stream()
extern "C"  Stream_t3255436806 * JsonUnmarshallerContext_get_Stream_m1194173228 (JsonUnmarshallerContext_t456235889 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext::Peek()
extern "C"  int32_t JsonUnmarshallerContext_Peek_m2080680004 (JsonUnmarshallerContext_t456235889 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext::StreamPeek()
extern "C"  int32_t JsonUnmarshallerContext_StreamPeek_m2167724032 (JsonUnmarshallerContext_t456235889 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext::UpdateContext()
extern "C"  void JsonUnmarshallerContext_UpdateContext_m2541327787 (JsonUnmarshallerContext_t456235889 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext::Dispose(System.Boolean)
extern "C"  void JsonUnmarshallerContext_Dispose_m918201241 (JsonUnmarshallerContext_t456235889 * __this, bool ___disposing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
