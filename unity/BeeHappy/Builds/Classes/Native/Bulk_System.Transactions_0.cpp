﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// System.MonoTODOAttribute
struct MonoTODOAttribute_t3487514023;
// System.String
struct String_t;
// System.Transactions.Enlistment
struct Enlistment_t1750074243;
// System.Transactions.PreparingEnlistment
struct PreparingEnlistment_t2112014291;
// System.Transactions.Transaction
struct Transaction_t869361102;
// System.Transactions.IEnlistmentNotification
struct IEnlistmentNotification_t1718399847;
// System.Exception
struct Exception_t1927440687;
// System.Transactions.SinglePhaseEnlistment
struct SinglePhaseEnlistment_t4207375558;
// System.Transactions.ISinglePhaseNotification
struct ISinglePhaseNotification_t242502729;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// System.Transactions.TransactionInformation
struct TransactionInformation_t3811320188;
// System.Object
struct Il2CppObject;
// System.Transactions.TransactionScope
struct TransactionScope_t1681136162;
// System.Transactions.TransactionAbortedException
struct TransactionAbortedException_t1712832570;
// System.Transactions.TransactionException
struct TransactionException_t198021791;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "System_Transactions_U3CModuleU3E3783534214.h"
#include "System_Transactions_U3CModuleU3E3783534214MethodDeclarations.h"
#include "System_Transactions_System_MonoTODOAttribute3487514019.h"
#include "System_Transactions_System_MonoTODOAttribute3487514019MethodDeclarations.h"
#include "mscorlib_System_Void1841601450.h"
#include "mscorlib_System_Attribute542643598MethodDeclarations.h"
#include "mscorlib_System_String2029220233.h"
#include "System_Transactions_System_Transactions_Enlistment1750074243.h"
#include "System_Transactions_System_Transactions_Enlistment1750074243MethodDeclarations.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "System_Transactions_System_Transactions_Enlistment1731762299.h"
#include "System_Transactions_System_Transactions_Enlistment1731762299MethodDeclarations.h"
#include "System_Transactions_System_Transactions_IsolationL4045174252.h"
#include "System_Transactions_System_Transactions_IsolationL4045174252MethodDeclarations.h"
#include "System_Transactions_System_Transactions_PreparingE2112014291.h"
#include "System_Transactions_System_Transactions_PreparingE2112014291MethodDeclarations.h"
#include "System_Transactions_System_Transactions_Transaction869361102.h"
#include "mscorlib_System_Exception1927440687.h"
#include "System_Transactions_System_Transactions_Transaction869361102MethodDeclarations.h"
#include "System_Transactions_System_Transactions_SinglePhas4207375558.h"
#include "System_Transactions_System_Transactions_SinglePhas4207375558MethodDeclarations.h"
#include "mscorlib_System_Collections_ArrayList4252133567MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1087520979MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3906591157MethodDeclarations.h"
#include "System_Transactions_System_Transactions_Transactio3811320188MethodDeclarations.h"
#include "mscorlib_System_Collections_ArrayList4252133567.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1087520979.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3906591157.h"
#include "System_Transactions_System_Transactions_Transactio3811320188.h"
#include "mscorlib_System_Runtime_Serialization_Serialization228987430.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"
#include "mscorlib_System_NotImplementedException2785117854MethodDeclarations.h"
#include "mscorlib_System_NotImplementedException2785117854.h"
#include "System_Transactions_System_Transactions_Transactio4266194108.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Int322071877448.h"
#include "System_Transactions_System_Transactions_Transaction198021791MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat622250653.h"
#include "System_Transactions_System_Transactions_Transaction198021791.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat622250653MethodDeclarations.h"
#include "System_Transactions_System_Transactions_Transactio1681136162.h"
#include "mscorlib_System_InvalidOperationException721527559MethodDeclarations.h"
#include "mscorlib_System_InvalidOperationException721527559.h"
#include "System_Transactions_System_Transactions_Transactio1712832570MethodDeclarations.h"
#include "System_Transactions_System_Transactions_Transactio1712832570.h"
#include "System_Transactions_System_Transactions_Transactio1681136162MethodDeclarations.h"
#include "mscorlib_System_SystemException3877406272MethodDeclarations.h"
#include "mscorlib_System_DateTime693205669MethodDeclarations.h"
#include "mscorlib_System_Guid2533601593MethodDeclarations.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "mscorlib_System_DateTime693205669.h"
#include "mscorlib_System_Guid2533601593.h"
#include "System_Transactions_System_Transactions_Transactio3985316291.h"
#include "System_Transactions_System_Transactions_Transactio3985316291MethodDeclarations.h"
#include "mscorlib_System_TimeSpan3430258949.h"
#include "mscorlib_System_TimeSpan3430258949MethodDeclarations.h"
#include "System_Transactions_System_Transactions_Transactio1588908416.h"
#include "System_Transactions_System_Transactions_Transactio1588908416MethodDeclarations.h"
#include "System_Transactions_System_Transactions_Transactio4266194108MethodDeclarations.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.MonoTODOAttribute::.ctor()
extern "C"  void MonoTODOAttribute__ctor_m174959495 (MonoTODOAttribute_t3487514023 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.MonoTODOAttribute::.ctor(System.String)
extern "C"  void MonoTODOAttribute__ctor_m2684640181 (MonoTODOAttribute_t3487514023 * __this, String_t* ___comment0, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___comment0;
		__this->set_comment_0(L_0);
		return;
	}
}
// System.Void System.Transactions.Enlistment::.ctor()
extern "C"  void Enlistment__ctor_m1730032696 (Enlistment_t1750074243 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		__this->set_done_0((bool)0);
		return;
	}
}
// System.Void System.Transactions.Enlistment::Done()
extern "C"  void Enlistment_Done_m1907240518 (Enlistment_t1750074243 * __this, const MethodInfo* method)
{
	{
		__this->set_done_0((bool)1);
		return;
	}
}
// System.Void System.Transactions.PreparingEnlistment::.ctor(System.Transactions.Transaction,System.Transactions.IEnlistmentNotification)
extern "C"  void PreparingEnlistment__ctor_m842975745 (PreparingEnlistment_t2112014291 * __this, Transaction_t869361102 * ___tx0, Il2CppObject * ___enlisted1, const MethodInfo* method)
{
	{
		Enlistment__ctor_m1730032696(__this, /*hidden argument*/NULL);
		Transaction_t869361102 * L_0 = ___tx0;
		__this->set_tx_2(L_0);
		Il2CppObject * L_1 = ___enlisted1;
		__this->set_enlisted_3(L_1);
		return;
	}
}
// System.Void System.Transactions.PreparingEnlistment::ForceRollback()
extern "C"  void PreparingEnlistment_ForceRollback_m385646317 (PreparingEnlistment_t2112014291 * __this, const MethodInfo* method)
{
	{
		PreparingEnlistment_ForceRollback_m2622594285(__this, (Exception_t1927440687 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Transactions.PreparingEnlistment::ForceRollback(System.Exception)
extern "C"  void PreparingEnlistment_ForceRollback_m2622594285 (PreparingEnlistment_t2112014291 * __this, Exception_t1927440687 * ___ex0, const MethodInfo* method)
{
	{
		Transaction_t869361102 * L_0 = __this->get_tx_2();
		Exception_t1927440687 * L_1 = ___ex0;
		Il2CppObject * L_2 = __this->get_enlisted_3();
		NullCheck(L_0);
		Transaction_Rollback_m601490000(L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Transactions.PreparingEnlistment::Prepared()
extern "C"  void PreparingEnlistment_Prepared_m3911164755 (PreparingEnlistment_t2112014291 * __this, const MethodInfo* method)
{
	{
		__this->set_prepared_1((bool)1);
		return;
	}
}
// System.Boolean System.Transactions.PreparingEnlistment::get_IsPrepared()
extern "C"  bool PreparingEnlistment_get_IsPrepared_m724409356 (PreparingEnlistment_t2112014291 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_prepared_1();
		return L_0;
	}
}
// System.Void System.Transactions.SinglePhaseEnlistment::.ctor(System.Transactions.Transaction,System.Transactions.ISinglePhaseNotification)
extern "C"  void SinglePhaseEnlistment__ctor_m2623225126 (SinglePhaseEnlistment_t4207375558 * __this, Transaction_t869361102 * ___tx0, Il2CppObject * ___enlisted1, const MethodInfo* method)
{
	{
		Enlistment__ctor_m1730032696(__this, /*hidden argument*/NULL);
		Transaction_t869361102 * L_0 = ___tx0;
		__this->set_tx_1(L_0);
		Il2CppObject * L_1 = ___enlisted1;
		__this->set_enlisted_2(L_1);
		return;
	}
}
// System.Void System.Transactions.Transaction::.ctor()
extern Il2CppClass* ArrayList_t4252133567_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1087520979_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t3906591157_il2cpp_TypeInfo_var;
extern Il2CppClass* TransactionInformation_t3811320188_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1725066782_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m591748220_MethodInfo_var;
extern const uint32_t Transaction__ctor_m3765812309_MetadataUsageId;
extern "C"  void Transaction__ctor_m3765812309 (Transaction_t869361102 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transaction__ctor_m3765812309_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ArrayList_t4252133567 * L_0 = (ArrayList_t4252133567 *)il2cpp_codegen_object_new(ArrayList_t4252133567_il2cpp_TypeInfo_var);
		ArrayList__ctor_m4012174379(L_0, /*hidden argument*/NULL);
		__this->set_dependents_3(L_0);
		List_1_t1087520979 * L_1 = (List_1_t1087520979 *)il2cpp_codegen_object_new(List_1_t1087520979_il2cpp_TypeInfo_var);
		List_1__ctor_m1725066782(L_1, /*hidden argument*/List_1__ctor_m1725066782_MethodInfo_var);
		__this->set_volatiles_4(L_1);
		List_1_t3906591157 * L_2 = (List_1_t3906591157 *)il2cpp_codegen_object_new(List_1_t3906591157_il2cpp_TypeInfo_var);
		List_1__ctor_m591748220(L_2, /*hidden argument*/List_1__ctor_m591748220_MethodInfo_var);
		__this->set_durables_5(L_2);
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		TransactionInformation_t3811320188 * L_3 = (TransactionInformation_t3811320188 *)il2cpp_codegen_object_new(TransactionInformation_t3811320188_il2cpp_TypeInfo_var);
		TransactionInformation__ctor_m3664651893(L_3, /*hidden argument*/NULL);
		__this->set_info_2(L_3);
		__this->set_level_1(0);
		return;
	}
}
// System.Void System.Transactions.Transaction::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern Il2CppClass* NotImplementedException_t2785117854_il2cpp_TypeInfo_var;
extern const uint32_t Transaction_System_Runtime_Serialization_ISerializable_GetObjectData_m2362374728_MetadataUsageId;
extern "C"  void Transaction_System_Runtime_Serialization_ISerializable_GetObjectData_m2362374728 (Transaction_t869361102 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transaction_System_Runtime_Serialization_ISerializable_GetObjectData_m2362374728_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t2785117854 * L_0 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m808189835(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Transactions.Transaction System.Transactions.Transaction::get_Current()
extern "C"  Transaction_t869361102 * Transaction_get_Current_m1694135828 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Transaction_EnsureIncompleteCurrentScope_m3481446252(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transaction_t869361102 * L_0 = Transaction_get_CurrentInternal_m3298618427(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Transactions.Transaction System.Transactions.Transaction::get_CurrentInternal()
extern Il2CppClass* Transaction_t869361102_il2cpp_TypeInfo_var;
extern const uint32_t Transaction_get_CurrentInternal_m3298618427_MetadataUsageId;
extern "C"  Transaction_t869361102 * Transaction_get_CurrentInternal_m3298618427 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transaction_get_CurrentInternal_m3298618427_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transaction_t869361102 * L_0 = ((Transaction_t869361102_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(Transaction_t869361102_il2cpp_TypeInfo_var))->get_ambient_0();
		return L_0;
	}
}
// System.Void System.Transactions.Transaction::set_CurrentInternal(System.Transactions.Transaction)
extern Il2CppClass* Transaction_t869361102_il2cpp_TypeInfo_var;
extern const uint32_t Transaction_set_CurrentInternal_m1169074502_MetadataUsageId;
extern "C"  void Transaction_set_CurrentInternal_m1169074502 (Il2CppObject * __this /* static, unused */, Transaction_t869361102 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transaction_set_CurrentInternal_m1169074502_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transaction_t869361102 * L_0 = ___value0;
		((Transaction_t869361102_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(Transaction_t869361102_il2cpp_TypeInfo_var))->set_ambient_0(L_0);
		return;
	}
}
// System.Transactions.TransactionInformation System.Transactions.Transaction::get_TransactionInformation()
extern "C"  TransactionInformation_t3811320188 * Transaction_get_TransactionInformation_m2678919253 (Transaction_t869361102 * __this, const MethodInfo* method)
{
	{
		Transaction_EnsureIncompleteCurrentScope_m3481446252(NULL /*static, unused*/, /*hidden argument*/NULL);
		TransactionInformation_t3811320188 * L_0 = __this->get_info_2();
		return L_0;
	}
}
// System.Void System.Transactions.Transaction::Dispose()
extern "C"  void Transaction_Dispose_m838977436 (Transaction_t869361102 * __this, const MethodInfo* method)
{
	{
		TransactionInformation_t3811320188 * L_0 = Transaction_get_TransactionInformation_m2678919253(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = TransactionInformation_get_Status_m571648349(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0016;
		}
	}
	{
		Transaction_Rollback_m3801978495(__this, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Transactions.Enlistment System.Transactions.Transaction::EnlistVolatile(System.Transactions.IEnlistmentNotification,System.Transactions.EnlistmentOptions)
extern "C"  Enlistment_t1750074243 * Transaction_EnlistVolatile_m3039283060 (Transaction_t869361102 * __this, Il2CppObject * ___notification0, int32_t ___options1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___notification0;
		int32_t L_1 = ___options1;
		Enlistment_t1750074243 * L_2 = Transaction_EnlistVolatileInternal_m2003285957(__this, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Transactions.Enlistment System.Transactions.Transaction::EnlistVolatileInternal(System.Transactions.IEnlistmentNotification,System.Transactions.EnlistmentOptions)
extern Il2CppClass* Enlistment_t1750074243_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Add_m3993395962_MethodInfo_var;
extern const uint32_t Transaction_EnlistVolatileInternal_m2003285957_MetadataUsageId;
extern "C"  Enlistment_t1750074243 * Transaction_EnlistVolatileInternal_m2003285957 (Transaction_t869361102 * __this, Il2CppObject * ___notification0, int32_t ___options1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transaction_EnlistVolatileInternal_m2003285957_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transaction_EnsureIncompleteCurrentScope_m3481446252(NULL /*static, unused*/, /*hidden argument*/NULL);
		List_1_t1087520979 * L_0 = __this->get_volatiles_4();
		Il2CppObject * L_1 = ___notification0;
		NullCheck(L_0);
		List_1_Add_m3993395962(L_0, L_1, /*hidden argument*/List_1_Add_m3993395962_MethodInfo_var);
		Enlistment_t1750074243 * L_2 = (Enlistment_t1750074243 *)il2cpp_codegen_object_new(Enlistment_t1750074243_il2cpp_TypeInfo_var);
		Enlistment__ctor_m1730032696(L_2, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean System.Transactions.Transaction::Equals(System.Object)
extern Il2CppClass* Transaction_t869361102_il2cpp_TypeInfo_var;
extern const uint32_t Transaction_Equals_m1809643446_MetadataUsageId;
extern "C"  bool Transaction_Equals_m1809643446 (Transaction_t869361102 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transaction_Equals_m1809643446_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___obj0;
		bool L_1 = Transaction_Equals_m1743910422(__this, ((Transaction_t869361102 *)IsInstClass(L_0, Transaction_t869361102_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Transactions.Transaction::Equals(System.Transactions.Transaction)
extern "C"  bool Transaction_Equals_m1743910422 (Transaction_t869361102 * __this, Transaction_t869361102 * ___t0, const MethodInfo* method)
{
	int32_t G_B7_0 = 0;
	{
		Transaction_t869361102 * L_0 = ___t0;
		bool L_1 = Object_ReferenceEquals_m3900584722(NULL /*static, unused*/, L_0, __this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000e;
		}
	}
	{
		return (bool)1;
	}

IL_000e:
	{
		Transaction_t869361102 * L_2 = ___t0;
		bool L_3 = Object_ReferenceEquals_m3900584722(NULL /*static, unused*/, L_2, NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001c;
		}
	}
	{
		return (bool)0;
	}

IL_001c:
	{
		int32_t L_4 = __this->get_level_1();
		Transaction_t869361102 * L_5 = ___t0;
		NullCheck(L_5);
		int32_t L_6 = L_5->get_level_1();
		if ((!(((uint32_t)L_4) == ((uint32_t)L_6))))
		{
			goto IL_003d;
		}
	}
	{
		TransactionInformation_t3811320188 * L_7 = __this->get_info_2();
		Transaction_t869361102 * L_8 = ___t0;
		NullCheck(L_8);
		TransactionInformation_t3811320188 * L_9 = L_8->get_info_2();
		G_B7_0 = ((((Il2CppObject*)(TransactionInformation_t3811320188 *)L_7) == ((Il2CppObject*)(TransactionInformation_t3811320188 *)L_9))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B7_0 = 0;
	}

IL_003e:
	{
		return (bool)G_B7_0;
	}
}
// System.Int32 System.Transactions.Transaction::GetHashCode()
extern "C"  int32_t Transaction_GetHashCode_m1043939158 (Transaction_t869361102 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_level_1();
		TransactionInformation_t3811320188 * L_1 = __this->get_info_2();
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_1);
		ArrayList_t4252133567 * L_3 = __this->get_dependents_3();
		NullCheck(L_3);
		int32_t L_4 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_3);
		return ((int32_t)((int32_t)((int32_t)((int32_t)L_0^(int32_t)L_2))^(int32_t)L_4));
	}
}
// System.Void System.Transactions.Transaction::Rollback()
extern "C"  void Transaction_Rollback_m3801978495 (Transaction_t869361102 * __this, const MethodInfo* method)
{
	{
		Transaction_Rollback_m1146972055(__this, (Exception_t1927440687 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Transactions.Transaction::Rollback(System.Exception)
extern "C"  void Transaction_Rollback_m1146972055 (Transaction_t869361102 * __this, Exception_t1927440687 * ___ex0, const MethodInfo* method)
{
	{
		Transaction_EnsureIncompleteCurrentScope_m3481446252(NULL /*static, unused*/, /*hidden argument*/NULL);
		Exception_t1927440687 * L_0 = ___ex0;
		Transaction_Rollback_m601490000(__this, L_0, (Il2CppObject *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Transactions.Transaction::Rollback(System.Exception,System.Transactions.IEnlistmentNotification)
extern Il2CppClass* TransactionException_t198021791_il2cpp_TypeInfo_var;
extern Il2CppClass* Enlistment_t1750074243_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnlistmentNotification_t1718399847_il2cpp_TypeInfo_var;
extern Il2CppClass* Enumerator_t622250653_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m3218413753_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m3277774565_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m684952141_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m2111830438_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m2827452001_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral788012832;
extern const uint32_t Transaction_Rollback_m601490000_MetadataUsageId;
extern "C"  void Transaction_Rollback_m601490000 (Transaction_t869361102 * __this, Exception_t1927440687 * ___ex0, Il2CppObject * ___enlisted1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transaction_Rollback_m601490000_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Enlistment_t1750074243 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Enumerator_t622250653  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = __this->get_aborted_8();
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		TransactionInformation_t3811320188 * L_1 = __this->get_info_2();
		NullCheck(L_1);
		int32_t L_2 = TransactionInformation_get_Status_m571648349(L_1, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_2) == ((uint32_t)1))))
		{
			goto IL_0028;
		}
	}
	{
		TransactionException_t198021791 * L_3 = (TransactionException_t198021791 *)il2cpp_codegen_object_new(TransactionException_t198021791_il2cpp_TypeInfo_var);
		TransactionException__ctor_m840353448(L_3, _stringLiteral788012832, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0028:
	{
		Exception_t1927440687 * L_4 = ___ex0;
		__this->set_innerException_10(L_4);
		Enlistment_t1750074243 * L_5 = (Enlistment_t1750074243 *)il2cpp_codegen_object_new(Enlistment_t1750074243_il2cpp_TypeInfo_var);
		Enlistment__ctor_m1730032696(L_5, /*hidden argument*/NULL);
		V_0 = L_5;
		List_1_t1087520979 * L_6 = __this->get_volatiles_4();
		NullCheck(L_6);
		Enumerator_t622250653  L_7 = List_1_GetEnumerator_m3218413753(L_6, /*hidden argument*/List_1_GetEnumerator_m3218413753_MethodInfo_var);
		V_2 = L_7;
	}

IL_0041:
	try
	{ // begin try (depth: 1)
		{
			goto IL_005c;
		}

IL_0046:
		{
			Il2CppObject * L_8 = Enumerator_get_Current_m3277774565((&V_2), /*hidden argument*/Enumerator_get_Current_m3277774565_MethodInfo_var);
			V_1 = L_8;
			Il2CppObject * L_9 = V_1;
			Il2CppObject * L_10 = ___enlisted1;
			if ((((Il2CppObject*)(Il2CppObject *)L_9) == ((Il2CppObject*)(Il2CppObject *)L_10)))
			{
				goto IL_005c;
			}
		}

IL_0055:
		{
			Il2CppObject * L_11 = V_1;
			Enlistment_t1750074243 * L_12 = V_0;
			NullCheck(L_11);
			InterfaceActionInvoker1< Enlistment_t1750074243 * >::Invoke(2 /* System.Void System.Transactions.IEnlistmentNotification::Rollback(System.Transactions.Enlistment) */, IEnlistmentNotification_t1718399847_il2cpp_TypeInfo_var, L_11, L_12);
		}

IL_005c:
		{
			bool L_13 = Enumerator_MoveNext_m684952141((&V_2), /*hidden argument*/Enumerator_MoveNext_m684952141_MethodInfo_var);
			if (L_13)
			{
				goto IL_0046;
			}
		}

IL_0068:
		{
			IL2CPP_LEAVE(0x79, FINALLY_006d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_006d;
	}

FINALLY_006d:
	{ // begin finally (depth: 1)
		Enumerator_t622250653  L_14 = V_2;
		Enumerator_t622250653  L_15 = L_14;
		Il2CppObject * L_16 = Box(Enumerator_t622250653_il2cpp_TypeInfo_var, &L_15);
		NullCheck((Il2CppObject *)L_16);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_16);
		IL2CPP_END_FINALLY(109)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(109)
	{
		IL2CPP_JUMP_TBL(0x79, IL_0079)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0079:
	{
		List_1_t3906591157 * L_17 = __this->get_durables_5();
		NullCheck(L_17);
		int32_t L_18 = List_1_get_Count_m2111830438(L_17, /*hidden argument*/List_1_get_Count_m2111830438_MethodInfo_var);
		if ((((int32_t)L_18) <= ((int32_t)0)))
		{
			goto IL_00ae;
		}
	}
	{
		List_1_t3906591157 * L_19 = __this->get_durables_5();
		NullCheck(L_19);
		Il2CppObject * L_20 = List_1_get_Item_m2827452001(L_19, 0, /*hidden argument*/List_1_get_Item_m2827452001_MethodInfo_var);
		Il2CppObject * L_21 = ___enlisted1;
		if ((((Il2CppObject*)(Il2CppObject *)L_20) == ((Il2CppObject*)(Il2CppObject *)L_21)))
		{
			goto IL_00ae;
		}
	}
	{
		List_1_t3906591157 * L_22 = __this->get_durables_5();
		NullCheck(L_22);
		Il2CppObject * L_23 = List_1_get_Item_m2827452001(L_22, 0, /*hidden argument*/List_1_get_Item_m2827452001_MethodInfo_var);
		Enlistment_t1750074243 * L_24 = V_0;
		NullCheck(L_23);
		InterfaceActionInvoker1< Enlistment_t1750074243 * >::Invoke(2 /* System.Void System.Transactions.IEnlistmentNotification::Rollback(System.Transactions.Enlistment) */, IEnlistmentNotification_t1718399847_il2cpp_TypeInfo_var, L_23, L_24);
	}

IL_00ae:
	{
		Transaction_set_Aborted_m920233168(__this, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Transactions.Transaction::set_Aborted(System.Boolean)
extern "C"  void Transaction_set_Aborted_m920233168 (Transaction_t869361102 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_aborted_8(L_0);
		bool L_1 = __this->get_aborted_8();
		if (!L_1)
		{
			goto IL_001e;
		}
	}
	{
		TransactionInformation_t3811320188 * L_2 = __this->get_info_2();
		NullCheck(L_2);
		TransactionInformation_set_Status_m946562358(L_2, 2, /*hidden argument*/NULL);
	}

IL_001e:
	{
		return;
	}
}
// System.Transactions.TransactionScope System.Transactions.Transaction::get_Scope()
extern "C"  TransactionScope_t1681136162 * Transaction_get_Scope_m1949711221 (Transaction_t869361102 * __this, const MethodInfo* method)
{
	{
		TransactionScope_t1681136162 * L_0 = __this->get_scope_9();
		return L_0;
	}
}
// System.Void System.Transactions.Transaction::set_Scope(System.Transactions.TransactionScope)
extern "C"  void Transaction_set_Scope_m3247888584 (Transaction_t869361102 * __this, TransactionScope_t1681136162 * ___value0, const MethodInfo* method)
{
	{
		TransactionScope_t1681136162 * L_0 = ___value0;
		__this->set_scope_9(L_0);
		return;
	}
}
// System.Void System.Transactions.Transaction::CommitInternal()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2107546325;
extern const uint32_t Transaction_CommitInternal_m797838023_MetadataUsageId;
extern "C"  void Transaction_CommitInternal_m797838023 (Transaction_t869361102 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transaction_CommitInternal_m797838023_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_committed_7();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		bool L_1 = __this->get_committing_6();
		if (!L_1)
		{
			goto IL_0021;
		}
	}

IL_0016:
	{
		InvalidOperationException_t721527559 * L_2 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_2, _stringLiteral2107546325, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0021:
	{
		__this->set_committing_6((bool)1);
		Transaction_DoCommit_m1072896187(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Transactions.Transaction::DoCommit()
extern Il2CppClass* ISinglePhaseNotification_t242502729_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Count_m1575167876_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m2111830438_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m559185587_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m2827452001_MethodInfo_var;
extern const uint32_t Transaction_DoCommit_m1072896187_MetadataUsageId;
extern "C"  void Transaction_DoCommit_m1072896187 (Transaction_t869361102 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transaction_DoCommit_m1072896187_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		TransactionScope_t1681136162 * L_0 = Transaction_get_Scope_m1949711221(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		Transaction_Rollback_m601490000(__this, (Exception_t1927440687 *)NULL, (Il2CppObject *)NULL, /*hidden argument*/NULL);
		Transaction_CheckAborted_m2786769474(__this, /*hidden argument*/NULL);
	}

IL_0019:
	{
		List_1_t1087520979 * L_1 = __this->get_volatiles_4();
		NullCheck(L_1);
		int32_t L_2 = List_1_get_Count_m1575167876(L_1, /*hidden argument*/List_1_get_Count_m1575167876_MethodInfo_var);
		if ((!(((uint32_t)L_2) == ((uint32_t)1))))
		{
			goto IL_0060;
		}
	}
	{
		List_1_t3906591157 * L_3 = __this->get_durables_5();
		NullCheck(L_3);
		int32_t L_4 = List_1_get_Count_m2111830438(L_3, /*hidden argument*/List_1_get_Count_m2111830438_MethodInfo_var);
		if (L_4)
		{
			goto IL_0060;
		}
	}
	{
		List_1_t1087520979 * L_5 = __this->get_volatiles_4();
		NullCheck(L_5);
		Il2CppObject * L_6 = List_1_get_Item_m559185587(L_5, 0, /*hidden argument*/List_1_get_Item_m559185587_MethodInfo_var);
		V_0 = ((Il2CppObject *)IsInst(L_6, ISinglePhaseNotification_t242502729_il2cpp_TypeInfo_var));
		Il2CppObject * L_7 = V_0;
		if (!L_7)
		{
			goto IL_0060;
		}
	}
	{
		Il2CppObject * L_8 = V_0;
		Transaction_DoSingleCommit_m2979426744(__this, L_8, /*hidden argument*/NULL);
		Transaction_Complete_m2515926970(__this, /*hidden argument*/NULL);
		return;
	}

IL_0060:
	{
		List_1_t1087520979 * L_9 = __this->get_volatiles_4();
		NullCheck(L_9);
		int32_t L_10 = List_1_get_Count_m1575167876(L_9, /*hidden argument*/List_1_get_Count_m1575167876_MethodInfo_var);
		if ((((int32_t)L_10) <= ((int32_t)0)))
		{
			goto IL_0077;
		}
	}
	{
		Transaction_DoPreparePhase_m1200442966(__this, /*hidden argument*/NULL);
	}

IL_0077:
	{
		List_1_t3906591157 * L_11 = __this->get_durables_5();
		NullCheck(L_11);
		int32_t L_12 = List_1_get_Count_m2111830438(L_11, /*hidden argument*/List_1_get_Count_m2111830438_MethodInfo_var);
		if ((((int32_t)L_12) <= ((int32_t)0)))
		{
			goto IL_009a;
		}
	}
	{
		List_1_t3906591157 * L_13 = __this->get_durables_5();
		NullCheck(L_13);
		Il2CppObject * L_14 = List_1_get_Item_m2827452001(L_13, 0, /*hidden argument*/List_1_get_Item_m2827452001_MethodInfo_var);
		Transaction_DoSingleCommit_m2979426744(__this, L_14, /*hidden argument*/NULL);
	}

IL_009a:
	{
		List_1_t1087520979 * L_15 = __this->get_volatiles_4();
		NullCheck(L_15);
		int32_t L_16 = List_1_get_Count_m1575167876(L_15, /*hidden argument*/List_1_get_Count_m1575167876_MethodInfo_var);
		if ((((int32_t)L_16) <= ((int32_t)0)))
		{
			goto IL_00b1;
		}
	}
	{
		Transaction_DoCommitPhase_m4149984918(__this, /*hidden argument*/NULL);
	}

IL_00b1:
	{
		Transaction_Complete_m2515926970(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Transactions.Transaction::Complete()
extern "C"  void Transaction_Complete_m2515926970 (Transaction_t869361102 * __this, const MethodInfo* method)
{
	{
		__this->set_committing_6((bool)0);
		__this->set_committed_7((bool)1);
		bool L_0 = __this->get_aborted_8();
		if (L_0)
		{
			goto IL_0025;
		}
	}
	{
		TransactionInformation_t3811320188 * L_1 = __this->get_info_2();
		NullCheck(L_1);
		TransactionInformation_set_Status_m946562358(L_1, 1, /*hidden argument*/NULL);
	}

IL_0025:
	{
		return;
	}
}
// System.Void System.Transactions.Transaction::DoPreparePhase()
extern Il2CppClass* PreparingEnlistment_t2112014291_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnlistmentNotification_t1718399847_il2cpp_TypeInfo_var;
extern Il2CppClass* Enumerator_t622250653_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m3218413753_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m3277774565_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m684952141_MethodInfo_var;
extern const uint32_t Transaction_DoPreparePhase_m1200442966_MetadataUsageId;
extern "C"  void Transaction_DoPreparePhase_m1200442966 (Transaction_t869361102 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transaction_DoPreparePhase_m1200442966_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PreparingEnlistment_t2112014291 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Enumerator_t622250653  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t1087520979 * L_0 = __this->get_volatiles_4();
		NullCheck(L_0);
		Enumerator_t622250653  L_1 = List_1_GetEnumerator_m3218413753(L_0, /*hidden argument*/List_1_GetEnumerator_m3218413753_MethodInfo_var);
		V_2 = L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_003f;
		}

IL_0011:
		{
			Il2CppObject * L_2 = Enumerator_get_Current_m3277774565((&V_2), /*hidden argument*/Enumerator_get_Current_m3277774565_MethodInfo_var);
			V_1 = L_2;
			Il2CppObject * L_3 = V_1;
			PreparingEnlistment_t2112014291 * L_4 = (PreparingEnlistment_t2112014291 *)il2cpp_codegen_object_new(PreparingEnlistment_t2112014291_il2cpp_TypeInfo_var);
			PreparingEnlistment__ctor_m842975745(L_4, __this, L_3, /*hidden argument*/NULL);
			V_0 = L_4;
			Il2CppObject * L_5 = V_1;
			PreparingEnlistment_t2112014291 * L_6 = V_0;
			NullCheck(L_5);
			InterfaceActionInvoker1< PreparingEnlistment_t2112014291 * >::Invoke(1 /* System.Void System.Transactions.IEnlistmentNotification::Prepare(System.Transactions.PreparingEnlistment) */, IEnlistmentNotification_t1718399847_il2cpp_TypeInfo_var, L_5, L_6);
			PreparingEnlistment_t2112014291 * L_7 = V_0;
			NullCheck(L_7);
			bool L_8 = PreparingEnlistment_get_IsPrepared_m724409356(L_7, /*hidden argument*/NULL);
			if (L_8)
			{
				goto IL_003f;
			}
		}

IL_0033:
		{
			Transaction_set_Aborted_m920233168(__this, (bool)1, /*hidden argument*/NULL);
			goto IL_004b;
		}

IL_003f:
		{
			bool L_9 = Enumerator_MoveNext_m684952141((&V_2), /*hidden argument*/Enumerator_MoveNext_m684952141_MethodInfo_var);
			if (L_9)
			{
				goto IL_0011;
			}
		}

IL_004b:
		{
			IL2CPP_LEAVE(0x5C, FINALLY_0050);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0050;
	}

FINALLY_0050:
	{ // begin finally (depth: 1)
		Enumerator_t622250653  L_10 = V_2;
		Enumerator_t622250653  L_11 = L_10;
		Il2CppObject * L_12 = Box(Enumerator_t622250653_il2cpp_TypeInfo_var, &L_11);
		NullCheck((Il2CppObject *)L_12);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_12);
		IL2CPP_END_FINALLY(80)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(80)
	{
		IL2CPP_JUMP_TBL(0x5C, IL_005c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_005c:
	{
		Transaction_CheckAborted_m2786769474(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Transactions.Transaction::DoCommitPhase()
extern Il2CppClass* Enlistment_t1750074243_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnlistmentNotification_t1718399847_il2cpp_TypeInfo_var;
extern Il2CppClass* Enumerator_t622250653_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m3218413753_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m3277774565_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m684952141_MethodInfo_var;
extern const uint32_t Transaction_DoCommitPhase_m4149984918_MetadataUsageId;
extern "C"  void Transaction_DoCommitPhase_m4149984918 (Transaction_t869361102 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transaction_DoCommitPhase_m4149984918_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Enumerator_t622250653  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Enlistment_t1750074243 * V_2 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t1087520979 * L_0 = __this->get_volatiles_4();
		NullCheck(L_0);
		Enumerator_t622250653  L_1 = List_1_GetEnumerator_m3218413753(L_0, /*hidden argument*/List_1_GetEnumerator_m3218413753_MethodInfo_var);
		V_1 = L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0026;
		}

IL_0011:
		{
			Il2CppObject * L_2 = Enumerator_get_Current_m3277774565((&V_1), /*hidden argument*/Enumerator_get_Current_m3277774565_MethodInfo_var);
			V_0 = L_2;
			Enlistment_t1750074243 * L_3 = (Enlistment_t1750074243 *)il2cpp_codegen_object_new(Enlistment_t1750074243_il2cpp_TypeInfo_var);
			Enlistment__ctor_m1730032696(L_3, /*hidden argument*/NULL);
			V_2 = L_3;
			Il2CppObject * L_4 = V_0;
			Enlistment_t1750074243 * L_5 = V_2;
			NullCheck(L_4);
			InterfaceActionInvoker1< Enlistment_t1750074243 * >::Invoke(0 /* System.Void System.Transactions.IEnlistmentNotification::Commit(System.Transactions.Enlistment) */, IEnlistmentNotification_t1718399847_il2cpp_TypeInfo_var, L_4, L_5);
		}

IL_0026:
		{
			bool L_6 = Enumerator_MoveNext_m684952141((&V_1), /*hidden argument*/Enumerator_MoveNext_m684952141_MethodInfo_var);
			if (L_6)
			{
				goto IL_0011;
			}
		}

IL_0032:
		{
			IL2CPP_LEAVE(0x43, FINALLY_0037);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0037;
	}

FINALLY_0037:
	{ // begin finally (depth: 1)
		Enumerator_t622250653  L_7 = V_1;
		Enumerator_t622250653  L_8 = L_7;
		Il2CppObject * L_9 = Box(Enumerator_t622250653_il2cpp_TypeInfo_var, &L_8);
		NullCheck((Il2CppObject *)L_9);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_9);
		IL2CPP_END_FINALLY(55)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(55)
	{
		IL2CPP_JUMP_TBL(0x43, IL_0043)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0043:
	{
		return;
	}
}
// System.Void System.Transactions.Transaction::DoSingleCommit(System.Transactions.ISinglePhaseNotification)
extern Il2CppClass* SinglePhaseEnlistment_t4207375558_il2cpp_TypeInfo_var;
extern Il2CppClass* ISinglePhaseNotification_t242502729_il2cpp_TypeInfo_var;
extern const uint32_t Transaction_DoSingleCommit_m2979426744_MetadataUsageId;
extern "C"  void Transaction_DoSingleCommit_m2979426744 (Transaction_t869361102 * __this, Il2CppObject * ___single0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transaction_DoSingleCommit_m2979426744_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	SinglePhaseEnlistment_t4207375558 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___single0;
		if (L_0)
		{
			goto IL_0007;
		}
	}
	{
		return;
	}

IL_0007:
	{
		Il2CppObject * L_1 = ___single0;
		SinglePhaseEnlistment_t4207375558 * L_2 = (SinglePhaseEnlistment_t4207375558 *)il2cpp_codegen_object_new(SinglePhaseEnlistment_t4207375558_il2cpp_TypeInfo_var);
		SinglePhaseEnlistment__ctor_m2623225126(L_2, __this, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Il2CppObject * L_3 = ___single0;
		SinglePhaseEnlistment_t4207375558 * L_4 = V_0;
		NullCheck(L_3);
		InterfaceActionInvoker1< SinglePhaseEnlistment_t4207375558 * >::Invoke(0 /* System.Void System.Transactions.ISinglePhaseNotification::SinglePhaseCommit(System.Transactions.SinglePhaseEnlistment) */, ISinglePhaseNotification_t242502729_il2cpp_TypeInfo_var, L_3, L_4);
		Transaction_CheckAborted_m2786769474(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Transactions.Transaction::CheckAborted()
extern Il2CppClass* TransactionAbortedException_t1712832570_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1888987669;
extern const uint32_t Transaction_CheckAborted_m2786769474_MetadataUsageId;
extern "C"  void Transaction_CheckAborted_m2786769474 (Transaction_t869361102 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transaction_CheckAborted_m2786769474_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_aborted_8();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		Exception_t1927440687 * L_1 = __this->get_innerException_10();
		TransactionAbortedException_t1712832570 * L_2 = (TransactionAbortedException_t1712832570 *)il2cpp_codegen_object_new(TransactionAbortedException_t1712832570_il2cpp_TypeInfo_var);
		TransactionAbortedException__ctor_m2129603671(L_2, _stringLiteral1888987669, L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_001c:
	{
		return;
	}
}
// System.Void System.Transactions.Transaction::EnsureIncompleteCurrentScope()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2116253899;
extern const uint32_t Transaction_EnsureIncompleteCurrentScope_m3481446252_MetadataUsageId;
extern "C"  void Transaction_EnsureIncompleteCurrentScope_m3481446252 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transaction_EnsureIncompleteCurrentScope_m3481446252_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transaction_t869361102 * L_0 = Transaction_get_CurrentInternal_m3298618427(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_1 = Transaction_op_Equality_m518739941(NULL /*static, unused*/, L_0, (Transaction_t869361102 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		Transaction_t869361102 * L_2 = Transaction_get_CurrentInternal_m3298618427(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		TransactionScope_t1681136162 * L_3 = Transaction_get_Scope_m1949711221(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_003f;
		}
	}
	{
		Transaction_t869361102 * L_4 = Transaction_get_CurrentInternal_m3298618427(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		TransactionScope_t1681136162 * L_5 = Transaction_get_Scope_m1949711221(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		bool L_6 = TransactionScope_get_IsComplete_m2452316973(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_003f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_7 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_7, _stringLiteral2116253899, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_003f:
	{
		return;
	}
}
// System.Boolean System.Transactions.Transaction::op_Equality(System.Transactions.Transaction,System.Transactions.Transaction)
extern "C"  bool Transaction_op_Equality_m518739941 (Il2CppObject * __this /* static, unused */, Transaction_t869361102 * ___x0, Transaction_t869361102 * ___y1, const MethodInfo* method)
{
	{
		Transaction_t869361102 * L_0 = ___x0;
		bool L_1 = Object_ReferenceEquals_m3900584722(NULL /*static, unused*/, L_0, NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0014;
		}
	}
	{
		Transaction_t869361102 * L_2 = ___y1;
		bool L_3 = Object_ReferenceEquals_m3900584722(NULL /*static, unused*/, L_2, NULL, /*hidden argument*/NULL);
		return L_3;
	}

IL_0014:
	{
		Transaction_t869361102 * L_4 = ___x0;
		Transaction_t869361102 * L_5 = ___y1;
		NullCheck(L_4);
		bool L_6 = Transaction_Equals_m1743910422(L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Boolean System.Transactions.Transaction::op_Inequality(System.Transactions.Transaction,System.Transactions.Transaction)
extern "C"  bool Transaction_op_Inequality_m1585364914 (Il2CppObject * __this /* static, unused */, Transaction_t869361102 * ___x0, Transaction_t869361102 * ___y1, const MethodInfo* method)
{
	{
		Transaction_t869361102 * L_0 = ___x0;
		Transaction_t869361102 * L_1 = ___y1;
		bool L_2 = Transaction_op_Equality_m518739941(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Transactions.TransactionAbortedException::.ctor()
extern "C"  void TransactionAbortedException__ctor_m661778353 (TransactionAbortedException_t1712832570 * __this, const MethodInfo* method)
{
	{
		TransactionException__ctor_m3509008338(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Transactions.TransactionAbortedException::.ctor(System.String,System.Exception)
extern "C"  void TransactionAbortedException__ctor_m2129603671 (TransactionAbortedException_t1712832570 * __this, String_t* ___message0, Exception_t1927440687 * ___innerException1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		Exception_t1927440687 * L_1 = ___innerException1;
		TransactionException__ctor_m639670952(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Transactions.TransactionAbortedException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void TransactionAbortedException__ctor_m627569262 (TransactionAbortedException_t1712832570 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method)
{
	{
		SerializationInfo_t228987430 * L_0 = ___info0;
		StreamingContext_t1417235061  L_1 = ___context1;
		TransactionException__ctor_m421081057(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Transactions.TransactionException::.ctor()
extern "C"  void TransactionException__ctor_m3509008338 (TransactionException_t198021791 * __this, const MethodInfo* method)
{
	{
		SystemException__ctor_m3487796677(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Transactions.TransactionException::.ctor(System.String)
extern "C"  void TransactionException__ctor_m840353448 (TransactionException_t198021791 * __this, String_t* ___message0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		SystemException__ctor_m4001391027(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Transactions.TransactionException::.ctor(System.String,System.Exception)
extern "C"  void TransactionException__ctor_m639670952 (TransactionException_t198021791 * __this, String_t* ___message0, Exception_t1927440687 * ___innerException1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		Exception_t1927440687 * L_1 = ___innerException1;
		SystemException__ctor_m3356086419(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Transactions.TransactionException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void TransactionException__ctor_m421081057 (TransactionException_t198021791 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method)
{
	{
		SerializationInfo_t228987430 * L_0 = ___info0;
		StreamingContext_t1417235061  L_1 = ___context1;
		SystemException__ctor_m2688248668(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Transactions.TransactionInformation::.ctor()
extern Il2CppClass* Guid_t2533601593_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTime_t693205669_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2827728131;
extern const uint32_t TransactionInformation__ctor_m3664651893_MetadataUsageId;
extern "C"  void TransactionInformation__ctor_m3664651893 (TransactionInformation_t3811320188 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TransactionInformation__ctor_m3664651893_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DateTime_t693205669  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Guid_t2533601593  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Guid_t2533601593_il2cpp_TypeInfo_var);
		Guid_t2533601593  L_0 = ((Guid_t2533601593_StaticFields*)Guid_t2533601593_il2cpp_TypeInfo_var->static_fields)->get_Empty_11();
		__this->set_dtcId_1(L_0);
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		__this->set_status_3(0);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t693205669_il2cpp_TypeInfo_var);
		DateTime_t693205669  L_1 = DateTime_get_Now_m24136300(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_1;
		DateTime_t693205669  L_2 = DateTime_ToUniversalTime_m1815024752((&V_0), /*hidden argument*/NULL);
		__this->set_creation_time_2(L_2);
		Guid_t2533601593  L_3 = Guid_NewGuid_m3493657620(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_3;
		String_t* L_4 = Guid_ToString_m3927110175((&V_1), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m2596409543(NULL /*static, unused*/, L_4, _stringLiteral2827728131, /*hidden argument*/NULL);
		__this->set_local_id_0(L_5);
		return;
	}
}
// System.Transactions.TransactionStatus System.Transactions.TransactionInformation::get_Status()
extern "C"  int32_t TransactionInformation_get_Status_m571648349 (TransactionInformation_t3811320188 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_status_3();
		return L_0;
	}
}
// System.Void System.Transactions.TransactionInformation::set_Status(System.Transactions.TransactionStatus)
extern "C"  void TransactionInformation_set_Status_m946562358 (TransactionInformation_t3811320188 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_status_3(L_0);
		return;
	}
}
// System.Void System.Transactions.TransactionManager::.cctor()
extern Il2CppClass* TransactionManager_t3985316291_il2cpp_TypeInfo_var;
extern const uint32_t TransactionManager__cctor_m986801381_MetadataUsageId;
extern "C"  void TransactionManager__cctor_m986801381 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TransactionManager__cctor_m986801381_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TimeSpan_t3430258949  L_0;
		memset(&L_0, 0, sizeof(L_0));
		TimeSpan__ctor_m1888018443(&L_0, 0, 1, 0, /*hidden argument*/NULL);
		((TransactionManager_t3985316291_StaticFields*)TransactionManager_t3985316291_il2cpp_TypeInfo_var->static_fields)->set_defaultTimeout_0(L_0);
		TimeSpan_t3430258949  L_1;
		memset(&L_1, 0, sizeof(L_1));
		TimeSpan__ctor_m1888018443(&L_1, 0, ((int32_t)10), 0, /*hidden argument*/NULL);
		((TransactionManager_t3985316291_StaticFields*)TransactionManager_t3985316291_il2cpp_TypeInfo_var->static_fields)->set_maxTimeout_1(L_1);
		return;
	}
}
// System.TimeSpan System.Transactions.TransactionManager::get_DefaultTimeout()
extern Il2CppClass* TransactionManager_t3985316291_il2cpp_TypeInfo_var;
extern const uint32_t TransactionManager_get_DefaultTimeout_m1509290990_MetadataUsageId;
extern "C"  TimeSpan_t3430258949  TransactionManager_get_DefaultTimeout_m1509290990 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TransactionManager_get_DefaultTimeout_m1509290990_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TransactionManager_t3985316291_il2cpp_TypeInfo_var);
		TimeSpan_t3430258949  L_0 = ((TransactionManager_t3985316291_StaticFields*)TransactionManager_t3985316291_il2cpp_TypeInfo_var->static_fields)->get_defaultTimeout_0();
		return L_0;
	}
}
// System.Void System.Transactions.TransactionOptions::.ctor(System.Transactions.IsolationLevel,System.TimeSpan)
extern "C"  void TransactionOptions__ctor_m3596369679 (TransactionOptions_t1588908416 * __this, int32_t ___level0, TimeSpan_t3430258949  ___timeout1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___level0;
		__this->set_level_0(L_0);
		TimeSpan_t3430258949  L_1 = ___timeout1;
		__this->set_timeout_1(L_1);
		return;
	}
}
extern "C"  void TransactionOptions__ctor_m3596369679_AdjustorThunk (Il2CppObject * __this, int32_t ___level0, TimeSpan_t3430258949  ___timeout1, const MethodInfo* method)
{
	TransactionOptions_t1588908416 * _thisAdjusted = reinterpret_cast<TransactionOptions_t1588908416 *>(__this + 1);
	TransactionOptions__ctor_m3596369679(_thisAdjusted, ___level0, ___timeout1, method);
}
// System.Boolean System.Transactions.TransactionOptions::Equals(System.Object)
extern Il2CppClass* TransactionOptions_t1588908416_il2cpp_TypeInfo_var;
extern const uint32_t TransactionOptions_Equals_m1088096644_MetadataUsageId;
extern "C"  bool TransactionOptions_Equals_m1088096644 (TransactionOptions_t1588908416 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TransactionOptions_Equals_m1088096644_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___obj0;
		if (((Il2CppObject *)IsInstSealed(L_0, TransactionOptions_t1588908416_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = ___obj0;
		bool L_2 = TransactionOptions_op_Equality_m1351512005(NULL /*static, unused*/, (*(TransactionOptions_t1588908416 *)__this), ((*(TransactionOptions_t1588908416 *)((TransactionOptions_t1588908416 *)UnBox (L_1, TransactionOptions_t1588908416_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  bool TransactionOptions_Equals_m1088096644_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	TransactionOptions_t1588908416 * _thisAdjusted = reinterpret_cast<TransactionOptions_t1588908416 *>(__this + 1);
	return TransactionOptions_Equals_m1088096644(_thisAdjusted, ___obj0, method);
}
// System.Int32 System.Transactions.TransactionOptions::GetHashCode()
extern "C"  int32_t TransactionOptions_GetHashCode_m2584502036 (TransactionOptions_t1588908416 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_level_0();
		TimeSpan_t3430258949 * L_1 = __this->get_address_of_timeout_1();
		int32_t L_2 = TimeSpan_GetHashCode_m550404245(L_1, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_0^(int32_t)L_2));
	}
}
extern "C"  int32_t TransactionOptions_GetHashCode_m2584502036_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	TransactionOptions_t1588908416 * _thisAdjusted = reinterpret_cast<TransactionOptions_t1588908416 *>(__this + 1);
	return TransactionOptions_GetHashCode_m2584502036(_thisAdjusted, method);
}
// System.Boolean System.Transactions.TransactionOptions::op_Equality(System.Transactions.TransactionOptions,System.Transactions.TransactionOptions)
extern Il2CppClass* TimeSpan_t3430258949_il2cpp_TypeInfo_var;
extern const uint32_t TransactionOptions_op_Equality_m1351512005_MetadataUsageId;
extern "C"  bool TransactionOptions_op_Equality_m1351512005 (Il2CppObject * __this /* static, unused */, TransactionOptions_t1588908416  ___o10, TransactionOptions_t1588908416  ___o21, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TransactionOptions_op_Equality_m1351512005_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = (&___o10)->get_level_0();
		int32_t L_1 = (&___o21)->get_level_0();
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0028;
		}
	}
	{
		TimeSpan_t3430258949  L_2 = (&___o10)->get_timeout_1();
		TimeSpan_t3430258949  L_3 = (&___o21)->get_timeout_1();
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t3430258949_il2cpp_TypeInfo_var);
		bool L_4 = TimeSpan_op_Equality_m3565136304(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_4));
		goto IL_0029;
	}

IL_0028:
	{
		G_B3_0 = 0;
	}

IL_0029:
	{
		return (bool)G_B3_0;
	}
}
// System.Void System.Transactions.TransactionScope::.cctor()
extern Il2CppClass* TransactionManager_t3985316291_il2cpp_TypeInfo_var;
extern Il2CppClass* TransactionScope_t1681136162_il2cpp_TypeInfo_var;
extern const uint32_t TransactionScope__cctor_m248045484_MetadataUsageId;
extern "C"  void TransactionScope__cctor_m248045484 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TransactionScope__cctor_m248045484_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TransactionManager_t3985316291_il2cpp_TypeInfo_var);
		TimeSpan_t3430258949  L_0 = TransactionManager_get_DefaultTimeout_m1509290990(NULL /*static, unused*/, /*hidden argument*/NULL);
		TransactionOptions_t1588908416  L_1;
		memset(&L_1, 0, sizeof(L_1));
		TransactionOptions__ctor_m3596369679(&L_1, 0, L_0, /*hidden argument*/NULL);
		((TransactionScope_t1681136162_StaticFields*)TransactionScope_t1681136162_il2cpp_TypeInfo_var->static_fields)->set_defaultOptions_0(L_1);
		return;
	}
}
// System.Boolean System.Transactions.TransactionScope::get_IsComplete()
extern "C"  bool TransactionScope_get_IsComplete_m2452316973 (TransactionScope_t1681136162 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_completed_6();
		return L_0;
	}
}
// System.Void System.Transactions.TransactionScope::Dispose()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1660666503;
extern Il2CppCodeGenString* _stringLiteral1423870257;
extern const uint32_t TransactionScope_Dispose_m4231503672_MetadataUsageId;
extern "C"  void TransactionScope_Dispose_m4231503672 (TransactionScope_t1681136162 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TransactionScope_Dispose_m4231503672_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_disposed_5();
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		__this->set_disposed_5((bool)1);
		TransactionScope_t1681136162 * L_1 = __this->get_parentScope_3();
		if (!L_1)
		{
			goto IL_0031;
		}
	}
	{
		TransactionScope_t1681136162 * L_2 = __this->get_parentScope_3();
		TransactionScope_t1681136162 * L_3 = L_2;
		NullCheck(L_3);
		int32_t L_4 = L_3->get_nested_4();
		NullCheck(L_3);
		L_3->set_nested_4(((int32_t)((int32_t)L_4-(int32_t)1)));
	}

IL_0031:
	{
		int32_t L_5 = __this->get_nested_4();
		if ((((int32_t)L_5) <= ((int32_t)0)))
		{
			goto IL_0053;
		}
	}
	{
		Transaction_t869361102 * L_6 = __this->get_transaction_1();
		NullCheck(L_6);
		Transaction_Rollback_m3801978495(L_6, /*hidden argument*/NULL);
		InvalidOperationException_t721527559 * L_7 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_7, _stringLiteral1660666503, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_0053:
	{
		Transaction_t869361102 * L_8 = Transaction_get_CurrentInternal_m3298618427(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transaction_t869361102 * L_9 = __this->get_transaction_1();
		bool L_10 = Transaction_op_Inequality_m1585364914(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_00a9;
		}
	}
	{
		Transaction_t869361102 * L_11 = __this->get_transaction_1();
		bool L_12 = Transaction_op_Inequality_m1585364914(NULL /*static, unused*/, L_11, (Transaction_t869361102 *)NULL, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0084;
		}
	}
	{
		Transaction_t869361102 * L_13 = __this->get_transaction_1();
		NullCheck(L_13);
		Transaction_Rollback_m3801978495(L_13, /*hidden argument*/NULL);
	}

IL_0084:
	{
		Transaction_t869361102 * L_14 = Transaction_get_CurrentInternal_m3298618427(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_15 = Transaction_op_Inequality_m1585364914(NULL /*static, unused*/, L_14, (Transaction_t869361102 *)NULL, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_009e;
		}
	}
	{
		Transaction_t869361102 * L_16 = Transaction_get_CurrentInternal_m3298618427(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_16);
		Transaction_Rollback_m3801978495(L_16, /*hidden argument*/NULL);
	}

IL_009e:
	{
		InvalidOperationException_t721527559 * L_17 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_17, _stringLiteral1423870257, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_17);
	}

IL_00a9:
	{
		Transaction_t869361102 * L_18 = Transaction_get_CurrentInternal_m3298618427(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transaction_t869361102 * L_19 = __this->get_oldTransaction_2();
		bool L_20 = Transaction_op_Equality_m518739941(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_00e0;
		}
	}
	{
		Transaction_t869361102 * L_21 = __this->get_oldTransaction_2();
		bool L_22 = Transaction_op_Inequality_m1585364914(NULL /*static, unused*/, L_21, (Transaction_t869361102 *)NULL, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_00e0;
		}
	}
	{
		Transaction_t869361102 * L_23 = __this->get_oldTransaction_2();
		TransactionScope_t1681136162 * L_24 = __this->get_parentScope_3();
		NullCheck(L_23);
		Transaction_set_Scope_m3247888584(L_23, L_24, /*hidden argument*/NULL);
	}

IL_00e0:
	{
		Transaction_t869361102 * L_25 = __this->get_oldTransaction_2();
		Transaction_set_CurrentInternal_m1169074502(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		Transaction_t869361102 * L_26 = __this->get_transaction_1();
		bool L_27 = Transaction_op_Equality_m518739941(NULL /*static, unused*/, L_26, (Transaction_t869361102 *)NULL, /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_00fd;
		}
	}
	{
		return;
	}

IL_00fd:
	{
		Transaction_t869361102 * L_28 = __this->get_transaction_1();
		NullCheck(L_28);
		Transaction_set_Scope_m3247888584(L_28, (TransactionScope_t1681136162 *)NULL, /*hidden argument*/NULL);
		bool L_29 = TransactionScope_get_IsComplete_m2452316973(__this, /*hidden argument*/NULL);
		if (L_29)
		{
			goto IL_0120;
		}
	}
	{
		Transaction_t869361102 * L_30 = __this->get_transaction_1();
		NullCheck(L_30);
		Transaction_Rollback_m3801978495(L_30, /*hidden argument*/NULL);
		return;
	}

IL_0120:
	{
		bool L_31 = __this->get_isRoot_7();
		if (L_31)
		{
			goto IL_012c;
		}
	}
	{
		return;
	}

IL_012c:
	{
		Transaction_t869361102 * L_32 = __this->get_transaction_1();
		NullCheck(L_32);
		Transaction_CommitInternal_m797838023(L_32, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
