﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.Util.RequestMetrics
struct RequestMetrics_t218029284;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;
// System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Collections.Generic.List`1<System.Object>>
struct Dictionary_2_t513681620;
// System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Collections.Generic.List`1<Amazon.Runtime.IMetricsTiming>>
struct Dictionary_2_t2292610545;
// System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Int64>
struct Dictionary_2_t3659156526;
// Amazon.Runtime.Internal.Util.TimingEvent
struct TimingEvent_t181853090;
// Amazon.Runtime.Internal.Util.Timing
struct Timing_t847194262;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_Runtime_Metric3273440202.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Text_StringBuilder1221177846.h"
#include "mscorlib_System_Object2689449295.h"

// System.Int64 Amazon.Runtime.Internal.Util.RequestMetrics::get_CurrentTime()
extern "C"  int64_t RequestMetrics_get_CurrentTime_m586950274 (RequestMetrics_t218029284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.RequestMetrics::LogError_Locked(Amazon.Runtime.Metric,System.String,System.Object[])
extern "C"  void RequestMetrics_LogError_Locked_m3757452173 (RequestMetrics_t218029284 * __this, int32_t ___metric0, String_t* ___messageFormat1, ObjectU5BU5D_t3614634134* ___args2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.RequestMetrics::Log(System.Text.StringBuilder,Amazon.Runtime.Metric,System.Object)
extern "C"  void RequestMetrics_Log_m770180288 (Il2CppObject * __this /* static, unused */, StringBuilder_t1221177846 * ___builder0, int32_t ___metric1, Il2CppObject * ___metricValue2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.RequestMetrics::Log(System.Text.StringBuilder,Amazon.Runtime.Metric,System.Collections.Generic.List`1<System.Object>)
extern "C"  void RequestMetrics_Log_m3813951102 (Il2CppObject * __this /* static, unused */, StringBuilder_t1221177846 * ___builder0, int32_t ___metric1, List_1_t2058570427 * ___metricValues2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.RequestMetrics::LogHelper(System.Text.StringBuilder,Amazon.Runtime.Metric,System.Object[])
extern "C"  void RequestMetrics_LogHelper_m2353731836 (Il2CppObject * __this /* static, unused */, StringBuilder_t1221177846 * ___builder0, int32_t ___metric1, ObjectU5BU5D_t3614634134* ___metricValues2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Runtime.Internal.Util.RequestMetrics::ObjectToString(System.Object)
extern "C"  String_t* RequestMetrics_ObjectToString_m1897565544 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Collections.Generic.List`1<System.Object>> Amazon.Runtime.Internal.Util.RequestMetrics::get_Properties()
extern "C"  Dictionary_2_t513681620 * RequestMetrics_get_Properties_m1128255080 (RequestMetrics_t218029284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.RequestMetrics::set_Properties(System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Collections.Generic.List`1<System.Object>>)
extern "C"  void RequestMetrics_set_Properties_m3432483013 (RequestMetrics_t218029284 * __this, Dictionary_2_t513681620 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Collections.Generic.List`1<Amazon.Runtime.IMetricsTiming>> Amazon.Runtime.Internal.Util.RequestMetrics::get_Timings()
extern "C"  Dictionary_2_t2292610545 * RequestMetrics_get_Timings_m2029116898 (RequestMetrics_t218029284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.RequestMetrics::set_Timings(System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Collections.Generic.List`1<Amazon.Runtime.IMetricsTiming>>)
extern "C"  void RequestMetrics_set_Timings_m2086951141 (RequestMetrics_t218029284 * __this, Dictionary_2_t2292610545 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Int64> Amazon.Runtime.Internal.Util.RequestMetrics::get_Counters()
extern "C"  Dictionary_2_t3659156526 * RequestMetrics_get_Counters_m4148256070 (RequestMetrics_t218029284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.RequestMetrics::set_Counters(System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Int64>)
extern "C"  void RequestMetrics_set_Counters_m3803050633 (RequestMetrics_t218029284 * __this, Dictionary_2_t3659156526 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.Util.RequestMetrics::get_IsEnabled()
extern "C"  bool RequestMetrics_get_IsEnabled_m2072964854 (RequestMetrics_t218029284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.RequestMetrics::set_IsEnabled(System.Boolean)
extern "C"  void RequestMetrics_set_IsEnabled_m553282881 (RequestMetrics_t218029284 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.RequestMetrics::.ctor()
extern "C"  void RequestMetrics__ctor_m224996418 (RequestMetrics_t218029284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.Internal.Util.TimingEvent Amazon.Runtime.Internal.Util.RequestMetrics::StartEvent(Amazon.Runtime.Metric)
extern "C"  TimingEvent_t181853090 * RequestMetrics_StartEvent_m3309184242 (RequestMetrics_t218029284 * __this, int32_t ___metric0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.Internal.Util.Timing Amazon.Runtime.Internal.Util.RequestMetrics::StopEvent(Amazon.Runtime.Metric)
extern "C"  Timing_t847194262 * RequestMetrics_StopEvent_m1096636000 (RequestMetrics_t218029284 * __this, int32_t ___metric0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.RequestMetrics::AddProperty(Amazon.Runtime.Metric,System.Object)
extern "C"  void RequestMetrics_AddProperty_m2669547640 (RequestMetrics_t218029284 * __this, int32_t ___metric0, Il2CppObject * ___property1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.RequestMetrics::SetCounter(Amazon.Runtime.Metric,System.Int64)
extern "C"  void RequestMetrics_SetCounter_m1947877204 (RequestMetrics_t218029284 * __this, int32_t ___metric0, int64_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Runtime.Internal.Util.RequestMetrics::GetErrors()
extern "C"  String_t* RequestMetrics_GetErrors_m1843733812 (RequestMetrics_t218029284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Runtime.Internal.Util.RequestMetrics::ToString()
extern "C"  String_t* RequestMetrics_ToString_m2325973059 (RequestMetrics_t218029284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Runtime.Internal.Util.RequestMetrics::ToJSON()
extern "C"  String_t* RequestMetrics_ToJSON_m2936573192 (RequestMetrics_t218029284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
