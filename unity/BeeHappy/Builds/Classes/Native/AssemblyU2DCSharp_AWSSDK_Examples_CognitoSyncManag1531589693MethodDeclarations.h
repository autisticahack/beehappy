﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AWSSDK.Examples.CognitoSyncManagerSample
struct CognitoSyncManagerSample_t1531589693;
// Amazon.RegionEndpoint
struct RegionEndpoint_t661522805;
// Amazon.CognitoIdentity.CognitoAWSCredentials
struct CognitoAWSCredentials_t2370264792;
// Amazon.CognitoSync.SyncManager.CognitoSyncManager
struct CognitoSyncManager_t1222571519;
// Amazon.CognitoSync.SyncManager.Dataset
struct Dataset_t3040902086;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.SyncConflict>
struct List_1_t2399431441;
// System.Object
struct Il2CppObject;
// Amazon.CognitoSync.SyncManager.SyncSuccessEventArgs
struct SyncSuccessEventArgs_t4277381347;
// Amazon.CognitoSync.SyncManager.SyncFailureEventArgs
struct SyncFailureEventArgs_t1748345498;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_SyncManager_3040902086.h"
#include "mscorlib_System_Object2689449295.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_SyncManager_4277381347.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_SyncManager_1748345498.h"

// System.Void AWSSDK.Examples.CognitoSyncManagerSample::.ctor()
extern "C"  void CognitoSyncManagerSample__ctor_m4015849482 (CognitoSyncManagerSample_t1531589693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.RegionEndpoint AWSSDK.Examples.CognitoSyncManagerSample::get__Region()
extern "C"  RegionEndpoint_t661522805 * CognitoSyncManagerSample_get__Region_m3741586036 (CognitoSyncManagerSample_t1531589693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.CognitoIdentity.CognitoAWSCredentials AWSSDK.Examples.CognitoSyncManagerSample::get_Credentials()
extern "C"  CognitoAWSCredentials_t2370264792 * CognitoSyncManagerSample_get_Credentials_m4144527307 (CognitoSyncManagerSample_t1531589693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.CognitoSync.SyncManager.CognitoSyncManager AWSSDK.Examples.CognitoSyncManagerSample::get_SyncManager()
extern "C"  CognitoSyncManager_t1222571519 * CognitoSyncManagerSample_get_SyncManager_m2847761629 (CognitoSyncManagerSample_t1531589693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AWSSDK.Examples.CognitoSyncManagerSample::Start()
extern "C"  void CognitoSyncManagerSample_Start_m1779438026 (CognitoSyncManagerSample_t1531589693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AWSSDK.Examples.CognitoSyncManagerSample::OnGUI()
extern "C"  void CognitoSyncManagerSample_OnGUI_m1918323902 (CognitoSyncManagerSample_t1531589693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AWSSDK.Examples.CognitoSyncManagerSample::HandleDatasetDeleted(Amazon.CognitoSync.SyncManager.Dataset)
extern "C"  bool CognitoSyncManagerSample_HandleDatasetDeleted_m1235064095 (CognitoSyncManagerSample_t1531589693 * __this, Dataset_t3040902086 * ___dataset0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AWSSDK.Examples.CognitoSyncManagerSample::HandleDatasetMerged(Amazon.CognitoSync.SyncManager.Dataset,System.Collections.Generic.List`1<System.String>)
extern "C"  bool CognitoSyncManagerSample_HandleDatasetMerged_m2727638088 (CognitoSyncManagerSample_t1531589693 * __this, Dataset_t3040902086 * ___dataset0, List_1_t1398341365 * ___datasetNames1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AWSSDK.Examples.CognitoSyncManagerSample::HandleSyncConflict(Amazon.CognitoSync.SyncManager.Dataset,System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.SyncConflict>)
extern "C"  bool CognitoSyncManagerSample_HandleSyncConflict_m2782273412 (CognitoSyncManagerSample_t1531589693 * __this, Dataset_t3040902086 * ___dataset0, List_1_t2399431441 * ___conflicts1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AWSSDK.Examples.CognitoSyncManagerSample::HandleSyncSuccess(System.Object,Amazon.CognitoSync.SyncManager.SyncSuccessEventArgs)
extern "C"  void CognitoSyncManagerSample_HandleSyncSuccess_m3562789515 (CognitoSyncManagerSample_t1531589693 * __this, Il2CppObject * ___sender0, SyncSuccessEventArgs_t4277381347 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AWSSDK.Examples.CognitoSyncManagerSample::HandleSyncFailure(System.Object,Amazon.CognitoSync.SyncManager.SyncFailureEventArgs)
extern "C"  void CognitoSyncManagerSample_HandleSyncFailure_m116496439 (CognitoSyncManagerSample_t1531589693 * __this, Il2CppObject * ___sender0, SyncFailureEventArgs_t1748345498 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
