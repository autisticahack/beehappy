﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.AmazonServiceResult`2<System.Object,System.Object>
struct AmazonServiceResult_2_t1630799325;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1927440687;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Exception1927440687.h"

// System.Void Amazon.Runtime.AmazonServiceResult`2<System.Object,System.Object>::set_Request(TRequest)
extern "C"  void AmazonServiceResult_2_set_Request_m704866296_gshared (AmazonServiceResult_2_t1630799325 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define AmazonServiceResult_2_set_Request_m704866296(__this, ___value0, method) ((  void (*) (AmazonServiceResult_2_t1630799325 *, Il2CppObject *, const MethodInfo*))AmazonServiceResult_2_set_Request_m704866296_gshared)(__this, ___value0, method)
// TResponse Amazon.Runtime.AmazonServiceResult`2<System.Object,System.Object>::get_Response()
extern "C"  Il2CppObject * AmazonServiceResult_2_get_Response_m2633022317_gshared (AmazonServiceResult_2_t1630799325 * __this, const MethodInfo* method);
#define AmazonServiceResult_2_get_Response_m2633022317(__this, method) ((  Il2CppObject * (*) (AmazonServiceResult_2_t1630799325 *, const MethodInfo*))AmazonServiceResult_2_get_Response_m2633022317_gshared)(__this, method)
// System.Void Amazon.Runtime.AmazonServiceResult`2<System.Object,System.Object>::set_Response(TResponse)
extern "C"  void AmazonServiceResult_2_set_Response_m2527206554_gshared (AmazonServiceResult_2_t1630799325 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define AmazonServiceResult_2_set_Response_m2527206554(__this, ___value0, method) ((  void (*) (AmazonServiceResult_2_t1630799325 *, Il2CppObject *, const MethodInfo*))AmazonServiceResult_2_set_Response_m2527206554_gshared)(__this, ___value0, method)
// System.Exception Amazon.Runtime.AmazonServiceResult`2<System.Object,System.Object>::get_Exception()
extern "C"  Exception_t1927440687 * AmazonServiceResult_2_get_Exception_m3649734926_gshared (AmazonServiceResult_2_t1630799325 * __this, const MethodInfo* method);
#define AmazonServiceResult_2_get_Exception_m3649734926(__this, method) ((  Exception_t1927440687 * (*) (AmazonServiceResult_2_t1630799325 *, const MethodInfo*))AmazonServiceResult_2_get_Exception_m3649734926_gshared)(__this, method)
// System.Void Amazon.Runtime.AmazonServiceResult`2<System.Object,System.Object>::set_Exception(System.Exception)
extern "C"  void AmazonServiceResult_2_set_Exception_m2511823045_gshared (AmazonServiceResult_2_t1630799325 * __this, Exception_t1927440687 * ___value0, const MethodInfo* method);
#define AmazonServiceResult_2_set_Exception_m2511823045(__this, ___value0, method) ((  void (*) (AmazonServiceResult_2_t1630799325 *, Exception_t1927440687 *, const MethodInfo*))AmazonServiceResult_2_set_Exception_m2511823045_gshared)(__this, ___value0, method)
// System.Void Amazon.Runtime.AmazonServiceResult`2<System.Object,System.Object>::set_state(System.Object)
extern "C"  void AmazonServiceResult_2_set_state_m3162568831_gshared (AmazonServiceResult_2_t1630799325 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define AmazonServiceResult_2_set_state_m3162568831(__this, ___value0, method) ((  void (*) (AmazonServiceResult_2_t1630799325 *, Il2CppObject *, const MethodInfo*))AmazonServiceResult_2_set_state_m3162568831_gshared)(__this, ___value0, method)
// System.Void Amazon.Runtime.AmazonServiceResult`2<System.Object,System.Object>::.ctor(TRequest,TResponse,System.Exception,System.Object)
extern "C"  void AmazonServiceResult_2__ctor_m2485307111_gshared (AmazonServiceResult_2_t1630799325 * __this, Il2CppObject * ___request0, Il2CppObject * ___response1, Exception_t1927440687 * ___exception2, Il2CppObject * ___state3, const MethodInfo* method);
#define AmazonServiceResult_2__ctor_m2485307111(__this, ___request0, ___response1, ___exception2, ___state3, method) ((  void (*) (AmazonServiceResult_2_t1630799325 *, Il2CppObject *, Il2CppObject *, Exception_t1927440687 *, Il2CppObject *, const MethodInfo*))AmazonServiceResult_2__ctor_m2485307111_gshared)(__this, ___request0, ___response1, ___exception2, ___state3, method)
