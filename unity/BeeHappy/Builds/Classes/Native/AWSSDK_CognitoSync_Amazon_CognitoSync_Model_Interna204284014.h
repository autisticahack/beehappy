﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.CognitoSync.Model.Internal.MarshallTransformations.DeleteDatasetResponseUnmarshaller
struct DeleteDatasetResponseUnmarshaller_t204284014;

#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_Json2685947887.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.CognitoSync.Model.Internal.MarshallTransformations.DeleteDatasetResponseUnmarshaller
struct  DeleteDatasetResponseUnmarshaller_t204284014  : public JsonResponseUnmarshaller_t2685947887
{
public:

public:
};

struct DeleteDatasetResponseUnmarshaller_t204284014_StaticFields
{
public:
	// Amazon.CognitoSync.Model.Internal.MarshallTransformations.DeleteDatasetResponseUnmarshaller Amazon.CognitoSync.Model.Internal.MarshallTransformations.DeleteDatasetResponseUnmarshaller::_instance
	DeleteDatasetResponseUnmarshaller_t204284014 * ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(DeleteDatasetResponseUnmarshaller_t204284014_StaticFields, ____instance_0)); }
	inline DeleteDatasetResponseUnmarshaller_t204284014 * get__instance_0() const { return ____instance_0; }
	inline DeleteDatasetResponseUnmarshaller_t204284014 ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(DeleteDatasetResponseUnmarshaller_t204284014 * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier(&____instance_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
