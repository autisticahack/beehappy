﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AccelBrain.MonauralBeat
struct MonauralBeat_t928071059;
// System.Single[]
struct SingleU5BU5D_t577127397;

#include "codegen/il2cpp-codegen.h"

// System.Void AccelBrain.MonauralBeat::.ctor()
extern "C"  void MonauralBeat__ctor_m1227362600 (MonauralBeat_t928071059 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single[] AccelBrain.MonauralBeat::UpdatePhase(System.Single[],System.Int32)
extern "C"  SingleU5BU5D_t577127397* MonauralBeat_UpdatePhase_m2347704082 (MonauralBeat_t928071059 * __this, SingleU5BU5D_t577127397* ___data0, int32_t ___channels1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
