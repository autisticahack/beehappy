﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>
struct ReadOnlyCollection_1_t963555367;
// System.Collections.Generic.IList`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>
struct IList_1_t1318710276;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// Mono.Data.Sqlite.SqliteKeyReader/KeyInfo[]
struct KeyInfoU5BU5D_t3526347178;
// System.Collections.Generic.IEnumerator`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>
struct IEnumerator_1_t2548260798;

#include "codegen/il2cpp-codegen.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteKeyReader_K777769675.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m1173446869_gshared (ReadOnlyCollection_1_t963555367 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m1173446869(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t963555367 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m1173446869_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1142895335_gshared (ReadOnlyCollection_1_t963555367 * __this, KeyInfo_t777769675  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1142895335(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t963555367 *, KeyInfo_t777769675 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1142895335_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3633987107_gshared (ReadOnlyCollection_1_t963555367 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3633987107(__this, method) ((  void (*) (ReadOnlyCollection_1_t963555367 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3633987107_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m2729972758_gshared (ReadOnlyCollection_1_t963555367 * __this, int32_t ___index0, KeyInfo_t777769675  ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m2729972758(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t963555367 *, int32_t, KeyInfo_t777769675 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m2729972758_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3828734064_gshared (ReadOnlyCollection_1_t963555367 * __this, KeyInfo_t777769675  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3828734064(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t963555367 *, KeyInfo_t777769675 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3828734064_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3553219594_gshared (ReadOnlyCollection_1_t963555367 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3553219594(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t963555367 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3553219594_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  KeyInfo_t777769675  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2175263142_gshared (ReadOnlyCollection_1_t963555367 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2175263142(__this, ___index0, method) ((  KeyInfo_t777769675  (*) (ReadOnlyCollection_1_t963555367 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2175263142_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m23917755_gshared (ReadOnlyCollection_1_t963555367 * __this, int32_t ___index0, KeyInfo_t777769675  ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m23917755(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t963555367 *, int32_t, KeyInfo_t777769675 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m23917755_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m995444887_gshared (ReadOnlyCollection_1_t963555367 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m995444887(__this, method) ((  bool (*) (ReadOnlyCollection_1_t963555367 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m995444887_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m511208728_gshared (ReadOnlyCollection_1_t963555367 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m511208728(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t963555367 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m511208728_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m724617579_gshared (ReadOnlyCollection_1_t963555367 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m724617579(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t963555367 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m724617579_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m3984682580_gshared (ReadOnlyCollection_1_t963555367 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m3984682580(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t963555367 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m3984682580_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m4019355510_gshared (ReadOnlyCollection_1_t963555367 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m4019355510(__this, method) ((  void (*) (ReadOnlyCollection_1_t963555367 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m4019355510_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m2297008266_gshared (ReadOnlyCollection_1_t963555367 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m2297008266(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t963555367 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m2297008266_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2550137926_gshared (ReadOnlyCollection_1_t963555367 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2550137926(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t963555367 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2550137926_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m2980376691_gshared (ReadOnlyCollection_1_t963555367 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m2980376691(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t963555367 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m2980376691_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m4222760883_gshared (ReadOnlyCollection_1_t963555367 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m4222760883(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t963555367 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m4222760883_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m2531774013_gshared (ReadOnlyCollection_1_t963555367 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m2531774013(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t963555367 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m2531774013_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3081361980_gshared (ReadOnlyCollection_1_t963555367 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3081361980(__this, method) ((  bool (*) (ReadOnlyCollection_1_t963555367 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3081361980_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m3763836290_gshared (ReadOnlyCollection_1_t963555367 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m3763836290(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t963555367 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m3763836290_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1427047099_gshared (ReadOnlyCollection_1_t963555367 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1427047099(__this, method) ((  bool (*) (ReadOnlyCollection_1_t963555367 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1427047099_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2756932536_gshared (ReadOnlyCollection_1_t963555367 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2756932536(__this, method) ((  bool (*) (ReadOnlyCollection_1_t963555367 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2756932536_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m3756600863_gshared (ReadOnlyCollection_1_t963555367 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m3756600863(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t963555367 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m3756600863_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m3668422614_gshared (ReadOnlyCollection_1_t963555367 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m3668422614(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t963555367 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m3668422614_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m1468027641_gshared (ReadOnlyCollection_1_t963555367 * __this, KeyInfo_t777769675  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m1468027641(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t963555367 *, KeyInfo_t777769675 , const MethodInfo*))ReadOnlyCollection_1_Contains_m1468027641_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m3301764035_gshared (ReadOnlyCollection_1_t963555367 * __this, KeyInfoU5BU5D_t3526347178* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m3301764035(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t963555367 *, KeyInfoU5BU5D_t3526347178*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m3301764035_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m2774040800_gshared (ReadOnlyCollection_1_t963555367 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m2774040800(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t963555367 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m2774040800_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m1354916623_gshared (ReadOnlyCollection_1_t963555367 * __this, KeyInfo_t777769675  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m1354916623(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t963555367 *, KeyInfo_t777769675 , const MethodInfo*))ReadOnlyCollection_1_IndexOf_m1354916623_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m743372196_gshared (ReadOnlyCollection_1_t963555367 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m743372196(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t963555367 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m743372196_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::get_Item(System.Int32)
extern "C"  KeyInfo_t777769675  ReadOnlyCollection_1_get_Item_m3989413570_gshared (ReadOnlyCollection_1_t963555367 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m3989413570(__this, ___index0, method) ((  KeyInfo_t777769675  (*) (ReadOnlyCollection_1_t963555367 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m3989413570_gshared)(__this, ___index0, method)
