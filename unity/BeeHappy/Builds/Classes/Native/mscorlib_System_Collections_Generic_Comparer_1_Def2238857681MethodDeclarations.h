﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>
struct DefaultComparer_t2238857681;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23749587448.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::.ctor()
extern "C"  void DefaultComparer__ctor_m2951634824_gshared (DefaultComparer_t2238857681 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m2951634824(__this, method) ((  void (*) (DefaultComparer_t2238857681 *, const MethodInfo*))DefaultComparer__ctor_m2951634824_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m2551961411_gshared (DefaultComparer_t2238857681 * __this, KeyValuePair_2_t3749587448  ___x0, KeyValuePair_2_t3749587448  ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m2551961411(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t2238857681 *, KeyValuePair_2_t3749587448 , KeyValuePair_2_t3749587448 , const MethodInfo*))DefaultComparer_Compare_m2551961411_gshared)(__this, ___x0, ___y1, method)
