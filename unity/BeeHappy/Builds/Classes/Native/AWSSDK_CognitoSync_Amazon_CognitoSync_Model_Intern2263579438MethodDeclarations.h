﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.CognitoSync.Model.Internal.MarshallTransformations.RecordPatchMarshaller
struct RecordPatchMarshaller_t2263579438;
// Amazon.CognitoSync.Model.RecordPatch
struct RecordPatch_t97905615;
// Amazon.Runtime.Internal.Transform.JsonMarshallerContext
struct JsonMarshallerContext_t4063314926;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_Model_RecordPa97905615.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_Json4063314926.h"

// System.Void Amazon.CognitoSync.Model.Internal.MarshallTransformations.RecordPatchMarshaller::Marshall(Amazon.CognitoSync.Model.RecordPatch,Amazon.Runtime.Internal.Transform.JsonMarshallerContext)
extern "C"  void RecordPatchMarshaller_Marshall_m3415846435 (RecordPatchMarshaller_t2263579438 * __this, RecordPatch_t97905615 * ___requestObject0, JsonMarshallerContext_t4063314926 * ___context1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Model.Internal.MarshallTransformations.RecordPatchMarshaller::.ctor()
extern "C"  void RecordPatchMarshaller__ctor_m2160332428 (RecordPatchMarshaller_t2263579438 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Model.Internal.MarshallTransformations.RecordPatchMarshaller::.cctor()
extern "C"  void RecordPatchMarshaller__cctor_m3374933389 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
