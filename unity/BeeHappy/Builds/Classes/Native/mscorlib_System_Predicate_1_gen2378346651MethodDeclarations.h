﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Predicate_1_gen2776792056MethodDeclarations.h"

// System.Void System.Predicate`1<System.Collections.Generic.KeyValuePair`2<System.String,ThirdParty.Json.LitJson.JsonData>>::.ctor(System.Object,System.IntPtr)
#define Predicate_1__ctor_m4264984132(__this, ___object0, ___method1, method) ((  void (*) (Predicate_1_t2378346651 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m2359416570_gshared)(__this, ___object0, ___method1, method)
// System.Boolean System.Predicate`1<System.Collections.Generic.KeyValuePair`2<System.String,ThirdParty.Json.LitJson.JsonData>>::Invoke(T)
#define Predicate_1_Invoke_m3211731308(__this, ___obj0, method) ((  bool (*) (Predicate_1_t2378346651 *, KeyValuePair_2_t3935376536 , const MethodInfo*))Predicate_1_Invoke_m332691618_gshared)(__this, ___obj0, method)
// System.IAsyncResult System.Predicate`1<System.Collections.Generic.KeyValuePair`2<System.String,ThirdParty.Json.LitJson.JsonData>>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m1179331199(__this, ___obj0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Predicate_1_t2378346651 *, KeyValuePair_2_t3935376536 , AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Predicate_1_BeginInvoke_m3157529563_gshared)(__this, ___obj0, ___callback1, ___object2, method)
// System.Boolean System.Predicate`1<System.Collections.Generic.KeyValuePair`2<System.String,ThirdParty.Json.LitJson.JsonData>>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m2619500550(__this, ___result0, method) ((  bool (*) (Predicate_1_t2378346651 *, Il2CppObject *, const MethodInfo*))Predicate_1_EndInvoke_m548603360_gshared)(__this, ___result0, method)
