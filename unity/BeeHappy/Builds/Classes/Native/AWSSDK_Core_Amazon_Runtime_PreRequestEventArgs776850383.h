﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.Runtime.AmazonWebServiceRequest
struct AmazonWebServiceRequest_t3384026212;

#include "mscorlib_System_EventArgs3289624707.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.PreRequestEventArgs
struct  PreRequestEventArgs_t776850383  : public EventArgs_t3289624707
{
public:
	// Amazon.Runtime.AmazonWebServiceRequest Amazon.Runtime.PreRequestEventArgs::<Request>k__BackingField
	AmazonWebServiceRequest_t3384026212 * ___U3CRequestU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CRequestU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PreRequestEventArgs_t776850383, ___U3CRequestU3Ek__BackingField_1)); }
	inline AmazonWebServiceRequest_t3384026212 * get_U3CRequestU3Ek__BackingField_1() const { return ___U3CRequestU3Ek__BackingField_1; }
	inline AmazonWebServiceRequest_t3384026212 ** get_address_of_U3CRequestU3Ek__BackingField_1() { return &___U3CRequestU3Ek__BackingField_1; }
	inline void set_U3CRequestU3Ek__BackingField_1(AmazonWebServiceRequest_t3384026212 * value)
	{
		___U3CRequestU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CRequestU3Ek__BackingField_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
