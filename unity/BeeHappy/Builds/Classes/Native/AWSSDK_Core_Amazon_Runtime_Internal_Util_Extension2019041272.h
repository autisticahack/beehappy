﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Util.Extensions
struct  Extensions_t2019041272  : public Il2CppObject
{
public:

public:
};

struct Extensions_t2019041272_StaticFields
{
public:
	// System.Int64 Amazon.Runtime.Internal.Util.Extensions::ticksPerSecond
	int64_t ___ticksPerSecond_0;
	// System.Double Amazon.Runtime.Internal.Util.Extensions::tickFrequency
	double ___tickFrequency_1;

public:
	inline static int32_t get_offset_of_ticksPerSecond_0() { return static_cast<int32_t>(offsetof(Extensions_t2019041272_StaticFields, ___ticksPerSecond_0)); }
	inline int64_t get_ticksPerSecond_0() const { return ___ticksPerSecond_0; }
	inline int64_t* get_address_of_ticksPerSecond_0() { return &___ticksPerSecond_0; }
	inline void set_ticksPerSecond_0(int64_t value)
	{
		___ticksPerSecond_0 = value;
	}

	inline static int32_t get_offset_of_tickFrequency_1() { return static_cast<int32_t>(offsetof(Extensions_t2019041272_StaticFields, ___tickFrequency_1)); }
	inline double get_tickFrequency_1() const { return ___tickFrequency_1; }
	inline double* get_address_of_tickFrequency_1() { return &___tickFrequency_1; }
	inline void set_tickFrequency_1(double value)
	{
		___tickFrequency_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
