﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Transactions.Transaction
struct Transaction_t869361102;
// System.Transactions.TransactionInformation
struct TransactionInformation_t3811320188;
// System.Collections.ArrayList
struct ArrayList_t4252133567;
// System.Collections.Generic.List`1<System.Transactions.IEnlistmentNotification>
struct List_1_t1087520979;
// System.Collections.Generic.List`1<System.Transactions.ISinglePhaseNotification>
struct List_1_t3906591157;
// System.Transactions.TransactionScope
struct TransactionScope_t1681136162;
// System.Exception
struct Exception_t1927440687;

#include "mscorlib_System_Object2689449295.h"
#include "System_Transactions_System_Transactions_IsolationL4045174252.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Transactions.Transaction
struct  Transaction_t869361102  : public Il2CppObject
{
public:
	// System.Transactions.IsolationLevel System.Transactions.Transaction::level
	int32_t ___level_1;
	// System.Transactions.TransactionInformation System.Transactions.Transaction::info
	TransactionInformation_t3811320188 * ___info_2;
	// System.Collections.ArrayList System.Transactions.Transaction::dependents
	ArrayList_t4252133567 * ___dependents_3;
	// System.Collections.Generic.List`1<System.Transactions.IEnlistmentNotification> System.Transactions.Transaction::volatiles
	List_1_t1087520979 * ___volatiles_4;
	// System.Collections.Generic.List`1<System.Transactions.ISinglePhaseNotification> System.Transactions.Transaction::durables
	List_1_t3906591157 * ___durables_5;
	// System.Boolean System.Transactions.Transaction::committing
	bool ___committing_6;
	// System.Boolean System.Transactions.Transaction::committed
	bool ___committed_7;
	// System.Boolean System.Transactions.Transaction::aborted
	bool ___aborted_8;
	// System.Transactions.TransactionScope System.Transactions.Transaction::scope
	TransactionScope_t1681136162 * ___scope_9;
	// System.Exception System.Transactions.Transaction::innerException
	Exception_t1927440687 * ___innerException_10;

public:
	inline static int32_t get_offset_of_level_1() { return static_cast<int32_t>(offsetof(Transaction_t869361102, ___level_1)); }
	inline int32_t get_level_1() const { return ___level_1; }
	inline int32_t* get_address_of_level_1() { return &___level_1; }
	inline void set_level_1(int32_t value)
	{
		___level_1 = value;
	}

	inline static int32_t get_offset_of_info_2() { return static_cast<int32_t>(offsetof(Transaction_t869361102, ___info_2)); }
	inline TransactionInformation_t3811320188 * get_info_2() const { return ___info_2; }
	inline TransactionInformation_t3811320188 ** get_address_of_info_2() { return &___info_2; }
	inline void set_info_2(TransactionInformation_t3811320188 * value)
	{
		___info_2 = value;
		Il2CppCodeGenWriteBarrier(&___info_2, value);
	}

	inline static int32_t get_offset_of_dependents_3() { return static_cast<int32_t>(offsetof(Transaction_t869361102, ___dependents_3)); }
	inline ArrayList_t4252133567 * get_dependents_3() const { return ___dependents_3; }
	inline ArrayList_t4252133567 ** get_address_of_dependents_3() { return &___dependents_3; }
	inline void set_dependents_3(ArrayList_t4252133567 * value)
	{
		___dependents_3 = value;
		Il2CppCodeGenWriteBarrier(&___dependents_3, value);
	}

	inline static int32_t get_offset_of_volatiles_4() { return static_cast<int32_t>(offsetof(Transaction_t869361102, ___volatiles_4)); }
	inline List_1_t1087520979 * get_volatiles_4() const { return ___volatiles_4; }
	inline List_1_t1087520979 ** get_address_of_volatiles_4() { return &___volatiles_4; }
	inline void set_volatiles_4(List_1_t1087520979 * value)
	{
		___volatiles_4 = value;
		Il2CppCodeGenWriteBarrier(&___volatiles_4, value);
	}

	inline static int32_t get_offset_of_durables_5() { return static_cast<int32_t>(offsetof(Transaction_t869361102, ___durables_5)); }
	inline List_1_t3906591157 * get_durables_5() const { return ___durables_5; }
	inline List_1_t3906591157 ** get_address_of_durables_5() { return &___durables_5; }
	inline void set_durables_5(List_1_t3906591157 * value)
	{
		___durables_5 = value;
		Il2CppCodeGenWriteBarrier(&___durables_5, value);
	}

	inline static int32_t get_offset_of_committing_6() { return static_cast<int32_t>(offsetof(Transaction_t869361102, ___committing_6)); }
	inline bool get_committing_6() const { return ___committing_6; }
	inline bool* get_address_of_committing_6() { return &___committing_6; }
	inline void set_committing_6(bool value)
	{
		___committing_6 = value;
	}

	inline static int32_t get_offset_of_committed_7() { return static_cast<int32_t>(offsetof(Transaction_t869361102, ___committed_7)); }
	inline bool get_committed_7() const { return ___committed_7; }
	inline bool* get_address_of_committed_7() { return &___committed_7; }
	inline void set_committed_7(bool value)
	{
		___committed_7 = value;
	}

	inline static int32_t get_offset_of_aborted_8() { return static_cast<int32_t>(offsetof(Transaction_t869361102, ___aborted_8)); }
	inline bool get_aborted_8() const { return ___aborted_8; }
	inline bool* get_address_of_aborted_8() { return &___aborted_8; }
	inline void set_aborted_8(bool value)
	{
		___aborted_8 = value;
	}

	inline static int32_t get_offset_of_scope_9() { return static_cast<int32_t>(offsetof(Transaction_t869361102, ___scope_9)); }
	inline TransactionScope_t1681136162 * get_scope_9() const { return ___scope_9; }
	inline TransactionScope_t1681136162 ** get_address_of_scope_9() { return &___scope_9; }
	inline void set_scope_9(TransactionScope_t1681136162 * value)
	{
		___scope_9 = value;
		Il2CppCodeGenWriteBarrier(&___scope_9, value);
	}

	inline static int32_t get_offset_of_innerException_10() { return static_cast<int32_t>(offsetof(Transaction_t869361102, ___innerException_10)); }
	inline Exception_t1927440687 * get_innerException_10() const { return ___innerException_10; }
	inline Exception_t1927440687 ** get_address_of_innerException_10() { return &___innerException_10; }
	inline void set_innerException_10(Exception_t1927440687 * value)
	{
		___innerException_10 = value;
		Il2CppCodeGenWriteBarrier(&___innerException_10, value);
	}
};

struct Transaction_t869361102_ThreadStaticFields
{
public:
	// System.Transactions.Transaction System.Transactions.Transaction::ambient
	Transaction_t869361102 * ___ambient_0;

public:
	inline static int32_t get_offset_of_ambient_0() { return static_cast<int32_t>(offsetof(Transaction_t869361102_ThreadStaticFields, ___ambient_0)); }
	inline Transaction_t869361102 * get_ambient_0() const { return ___ambient_0; }
	inline Transaction_t869361102 ** get_address_of_ambient_0() { return &___ambient_0; }
	inline void set_ambient_0(Transaction_t869361102 * value)
	{
		___ambient_0 = value;
		Il2CppCodeGenWriteBarrier(&___ambient_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
