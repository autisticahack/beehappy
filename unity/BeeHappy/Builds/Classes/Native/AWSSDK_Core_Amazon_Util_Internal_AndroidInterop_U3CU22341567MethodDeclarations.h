﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Util.Internal.AndroidInterop/<>c
struct U3CU3Ec_t22341567;
// System.Reflection.MethodInfo
struct MethodInfo_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_MethodInfo3330546337.h"

// System.Void Amazon.Util.Internal.AndroidInterop/<>c::.cctor()
extern "C"  void U3CU3Ec__cctor_m3391386389 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Util.Internal.AndroidInterop/<>c::.ctor()
extern "C"  void U3CU3Ec__ctor_m536194836 (U3CU3Ec_t22341567 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Util.Internal.AndroidInterop/<>c::<GetJavaObjectStatically>b__1_0(System.Reflection.MethodInfo)
extern "C"  bool U3CU3Ec_U3CGetJavaObjectStaticallyU3Eb__1_0_m3869681748 (U3CU3Ec_t22341567 * __this, MethodInfo_t * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Util.Internal.AndroidInterop/<>c::<GetJavaObjectStatically>b__1_1(System.Reflection.MethodInfo)
extern "C"  bool U3CU3Ec_U3CGetJavaObjectStaticallyU3Eb__1_1_m1538876213 (U3CU3Ec_t22341567 * __this, MethodInfo_t * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Util.Internal.AndroidInterop/<>c::<CallMethod>b__3_0(System.Reflection.MethodInfo)
extern "C"  bool U3CU3Ec_U3CCallMethodU3Eb__3_0_m520873682 (U3CU3Ec_t22341567 * __this, MethodInfo_t * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Util.Internal.AndroidInterop/<>c::<CallMethod>b__3_1(System.Reflection.MethodInfo)
extern "C"  bool U3CU3Ec_U3CCallMethodU3Eb__3_1_m2104572045 (U3CU3Ec_t22341567 * __this, MethodInfo_t * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
