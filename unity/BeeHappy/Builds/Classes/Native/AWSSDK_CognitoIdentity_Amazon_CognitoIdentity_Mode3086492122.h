﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.CredentialsUnmarshaller
struct CredentialsUnmarshaller_t3086492122;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.CredentialsUnmarshaller
struct  CredentialsUnmarshaller_t3086492122  : public Il2CppObject
{
public:

public:
};

struct CredentialsUnmarshaller_t3086492122_StaticFields
{
public:
	// Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.CredentialsUnmarshaller Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.CredentialsUnmarshaller::_instance
	CredentialsUnmarshaller_t3086492122 * ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(CredentialsUnmarshaller_t3086492122_StaticFields, ____instance_0)); }
	inline CredentialsUnmarshaller_t3086492122 * get__instance_0() const { return ____instance_0; }
	inline CredentialsUnmarshaller_t3086492122 ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(CredentialsUnmarshaller_t3086492122 * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier(&____instance_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
