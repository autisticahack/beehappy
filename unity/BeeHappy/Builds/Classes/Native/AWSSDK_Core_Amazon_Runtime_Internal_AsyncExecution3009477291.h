﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.Runtime.IAsyncResponseContext
struct IAsyncResponseContext_t340181993;
// Amazon.Runtime.IAsyncRequestContext
struct IAsyncRequestContext_t1452803323;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.AsyncExecutionContext
struct  AsyncExecutionContext_t3009477291  : public Il2CppObject
{
public:
	// Amazon.Runtime.IAsyncResponseContext Amazon.Runtime.Internal.AsyncExecutionContext::<ResponseContext>k__BackingField
	Il2CppObject * ___U3CResponseContextU3Ek__BackingField_0;
	// Amazon.Runtime.IAsyncRequestContext Amazon.Runtime.Internal.AsyncExecutionContext::<RequestContext>k__BackingField
	Il2CppObject * ___U3CRequestContextU3Ek__BackingField_1;
	// System.Object Amazon.Runtime.Internal.AsyncExecutionContext::<RuntimeState>k__BackingField
	Il2CppObject * ___U3CRuntimeStateU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CResponseContextU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AsyncExecutionContext_t3009477291, ___U3CResponseContextU3Ek__BackingField_0)); }
	inline Il2CppObject * get_U3CResponseContextU3Ek__BackingField_0() const { return ___U3CResponseContextU3Ek__BackingField_0; }
	inline Il2CppObject ** get_address_of_U3CResponseContextU3Ek__BackingField_0() { return &___U3CResponseContextU3Ek__BackingField_0; }
	inline void set_U3CResponseContextU3Ek__BackingField_0(Il2CppObject * value)
	{
		___U3CResponseContextU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CResponseContextU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CRequestContextU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AsyncExecutionContext_t3009477291, ___U3CRequestContextU3Ek__BackingField_1)); }
	inline Il2CppObject * get_U3CRequestContextU3Ek__BackingField_1() const { return ___U3CRequestContextU3Ek__BackingField_1; }
	inline Il2CppObject ** get_address_of_U3CRequestContextU3Ek__BackingField_1() { return &___U3CRequestContextU3Ek__BackingField_1; }
	inline void set_U3CRequestContextU3Ek__BackingField_1(Il2CppObject * value)
	{
		___U3CRequestContextU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CRequestContextU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CRuntimeStateU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AsyncExecutionContext_t3009477291, ___U3CRuntimeStateU3Ek__BackingField_2)); }
	inline Il2CppObject * get_U3CRuntimeStateU3Ek__BackingField_2() const { return ___U3CRuntimeStateU3Ek__BackingField_2; }
	inline Il2CppObject ** get_address_of_U3CRuntimeStateU3Ek__BackingField_2() { return &___U3CRuntimeStateU3Ek__BackingField_2; }
	inline void set_U3CRuntimeStateU3Ek__BackingField_2(Il2CppObject * value)
	{
		___U3CRuntimeStateU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CRuntimeStateU3Ek__BackingField_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
