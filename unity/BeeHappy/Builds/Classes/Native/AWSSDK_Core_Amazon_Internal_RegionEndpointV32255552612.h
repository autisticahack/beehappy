﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.Internal.RegionEndpointV3/ServiceMap
struct ServiceMap_t3203095671;
// System.String
struct String_t;
// ThirdParty.Json.LitJson.JsonData
struct JsonData_t4263252052;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Internal.RegionEndpointV3
struct  RegionEndpointV3_t2255552612  : public Il2CppObject
{
public:
	// Amazon.Internal.RegionEndpointV3/ServiceMap Amazon.Internal.RegionEndpointV3::_serviceMap
	ServiceMap_t3203095671 * ____serviceMap_0;
	// System.String Amazon.Internal.RegionEndpointV3::<RegionName>k__BackingField
	String_t* ___U3CRegionNameU3Ek__BackingField_1;
	// System.String Amazon.Internal.RegionEndpointV3::<DisplayName>k__BackingField
	String_t* ___U3CDisplayNameU3Ek__BackingField_2;
	// ThirdParty.Json.LitJson.JsonData Amazon.Internal.RegionEndpointV3::_partitionJsonData
	JsonData_t4263252052 * ____partitionJsonData_3;
	// ThirdParty.Json.LitJson.JsonData Amazon.Internal.RegionEndpointV3::_servicesJsonData
	JsonData_t4263252052 * ____servicesJsonData_4;
	// System.Boolean Amazon.Internal.RegionEndpointV3::_servicesLoaded
	bool ____servicesLoaded_5;

public:
	inline static int32_t get_offset_of__serviceMap_0() { return static_cast<int32_t>(offsetof(RegionEndpointV3_t2255552612, ____serviceMap_0)); }
	inline ServiceMap_t3203095671 * get__serviceMap_0() const { return ____serviceMap_0; }
	inline ServiceMap_t3203095671 ** get_address_of__serviceMap_0() { return &____serviceMap_0; }
	inline void set__serviceMap_0(ServiceMap_t3203095671 * value)
	{
		____serviceMap_0 = value;
		Il2CppCodeGenWriteBarrier(&____serviceMap_0, value);
	}

	inline static int32_t get_offset_of_U3CRegionNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RegionEndpointV3_t2255552612, ___U3CRegionNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CRegionNameU3Ek__BackingField_1() const { return ___U3CRegionNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CRegionNameU3Ek__BackingField_1() { return &___U3CRegionNameU3Ek__BackingField_1; }
	inline void set_U3CRegionNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CRegionNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CRegionNameU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CDisplayNameU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(RegionEndpointV3_t2255552612, ___U3CDisplayNameU3Ek__BackingField_2)); }
	inline String_t* get_U3CDisplayNameU3Ek__BackingField_2() const { return ___U3CDisplayNameU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CDisplayNameU3Ek__BackingField_2() { return &___U3CDisplayNameU3Ek__BackingField_2; }
	inline void set_U3CDisplayNameU3Ek__BackingField_2(String_t* value)
	{
		___U3CDisplayNameU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CDisplayNameU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of__partitionJsonData_3() { return static_cast<int32_t>(offsetof(RegionEndpointV3_t2255552612, ____partitionJsonData_3)); }
	inline JsonData_t4263252052 * get__partitionJsonData_3() const { return ____partitionJsonData_3; }
	inline JsonData_t4263252052 ** get_address_of__partitionJsonData_3() { return &____partitionJsonData_3; }
	inline void set__partitionJsonData_3(JsonData_t4263252052 * value)
	{
		____partitionJsonData_3 = value;
		Il2CppCodeGenWriteBarrier(&____partitionJsonData_3, value);
	}

	inline static int32_t get_offset_of__servicesJsonData_4() { return static_cast<int32_t>(offsetof(RegionEndpointV3_t2255552612, ____servicesJsonData_4)); }
	inline JsonData_t4263252052 * get__servicesJsonData_4() const { return ____servicesJsonData_4; }
	inline JsonData_t4263252052 ** get_address_of__servicesJsonData_4() { return &____servicesJsonData_4; }
	inline void set__servicesJsonData_4(JsonData_t4263252052 * value)
	{
		____servicesJsonData_4 = value;
		Il2CppCodeGenWriteBarrier(&____servicesJsonData_4, value);
	}

	inline static int32_t get_offset_of__servicesLoaded_5() { return static_cast<int32_t>(offsetof(RegionEndpointV3_t2255552612, ____servicesLoaded_5)); }
	inline bool get__servicesLoaded_5() const { return ____servicesLoaded_5; }
	inline bool* get_address_of__servicesLoaded_5() { return &____servicesLoaded_5; }
	inline void set__servicesLoaded_5(bool value)
	{
		____servicesLoaded_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
