﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.Runtime.Internal.Auth.AWS4Signer
struct AWS4Signer_t1011449585;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Auth.AbstractAWSSigner
struct  AbstractAWSSigner_t2114314031  : public Il2CppObject
{
public:
	// Amazon.Runtime.Internal.Auth.AWS4Signer Amazon.Runtime.Internal.Auth.AbstractAWSSigner::_aws4Signer
	AWS4Signer_t1011449585 * ____aws4Signer_0;

public:
	inline static int32_t get_offset_of__aws4Signer_0() { return static_cast<int32_t>(offsetof(AbstractAWSSigner_t2114314031, ____aws4Signer_0)); }
	inline AWS4Signer_t1011449585 * get__aws4Signer_0() const { return ____aws4Signer_0; }
	inline AWS4Signer_t1011449585 ** get_address_of__aws4Signer_0() { return &____aws4Signer_0; }
	inline void set__aws4Signer_0(AWS4Signer_t1011449585 * value)
	{
		____aws4Signer_0 = value;
		Il2CppCodeGenWriteBarrier(&____aws4Signer_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
