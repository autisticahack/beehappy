﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ThirdParty.Json.LitJson.WriterContext
struct WriterContext_t1209007092;

#include "codegen/il2cpp-codegen.h"

// System.Void ThirdParty.Json.LitJson.WriterContext::.ctor()
extern "C"  void WriterContext__ctor_m2481251725 (WriterContext_t1209007092 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
