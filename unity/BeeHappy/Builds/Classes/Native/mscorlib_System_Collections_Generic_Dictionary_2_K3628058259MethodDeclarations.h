﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,System.Object>
struct KeyCollection_t3628058259;
// System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Object>
struct Dictionary_2_t1144560488;
// System.Collections.Generic.IEnumerator`1<Amazon.Runtime.Metric>
struct IEnumerator_1_t748964029;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// Amazon.Runtime.Metric[]
struct MetricU5BU5D_t2477685199;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_Runtime_Metric3273440202.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3834063926.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m2275592418_gshared (KeyCollection_t3628058259 * __this, Dictionary_2_t1144560488 * ___dictionary0, const MethodInfo* method);
#define KeyCollection__ctor_m2275592418(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t3628058259 *, Dictionary_2_t1144560488 *, const MethodInfo*))KeyCollection__ctor_m2275592418_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,System.Object>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m22610452_gshared (KeyCollection_t3628058259 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m22610452(__this, ___item0, method) ((  void (*) (KeyCollection_t3628058259 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m22610452_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,System.Object>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3969664523_gshared (KeyCollection_t3628058259 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3969664523(__this, method) ((  void (*) (KeyCollection_t3628058259 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3969664523_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,System.Object>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2395394556_gshared (KeyCollection_t3628058259 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2395394556(__this, ___item0, method) ((  bool (*) (KeyCollection_t3628058259 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2395394556_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,System.Object>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1815209971_gshared (KeyCollection_t3628058259 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1815209971(__this, ___item0, method) ((  bool (*) (KeyCollection_t3628058259 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1815209971_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,System.Object>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m246573329_gshared (KeyCollection_t3628058259 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m246573329(__this, method) ((  Il2CppObject* (*) (KeyCollection_t3628058259 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m246573329_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m3092510701_gshared (KeyCollection_t3628058259 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m3092510701(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t3628058259 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m3092510701_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1668434068_gshared (KeyCollection_t3628058259 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1668434068(__this, method) ((  Il2CppObject * (*) (KeyCollection_t3628058259 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1668434068_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,System.Object>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2896445255_gshared (KeyCollection_t3628058259 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2896445255(__this, method) ((  bool (*) (KeyCollection_t3628058259 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2896445255_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m451785489_gshared (KeyCollection_t3628058259 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m451785489(__this, method) ((  bool (*) (KeyCollection_t3628058259 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m451785489_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m1284653485_gshared (KeyCollection_t3628058259 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m1284653485(__this, method) ((  Il2CppObject * (*) (KeyCollection_t3628058259 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m1284653485_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,System.Object>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m3857679723_gshared (KeyCollection_t3628058259 * __this, MetricU5BU5D_t2477685199* ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_CopyTo_m3857679723(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t3628058259 *, MetricU5BU5D_t2477685199*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m3857679723_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,System.Object>::GetEnumerator()
extern "C"  Enumerator_t3834063926  KeyCollection_GetEnumerator_m912416230_gshared (KeyCollection_t3628058259 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m912416230(__this, method) ((  Enumerator_t3834063926  (*) (KeyCollection_t3628058259 *, const MethodInfo*))KeyCollection_GetEnumerator_m912416230_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,System.Object>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m3852128017_gshared (KeyCollection_t3628058259 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m3852128017(__this, method) ((  int32_t (*) (KeyCollection_t3628058259 *, const MethodInfo*))KeyCollection_get_Count_m3852128017_gshared)(__this, method)
