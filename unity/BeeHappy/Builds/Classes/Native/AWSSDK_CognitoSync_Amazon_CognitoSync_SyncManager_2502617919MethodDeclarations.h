﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.CognitoSync.SyncManager.Dataset/DatasetDeletedDelegate
struct DatasetDeletedDelegate_t2502617919;
// System.Object
struct Il2CppObject;
// Amazon.CognitoSync.SyncManager.Dataset
struct Dataset_t3040902086;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_SyncManager_3040902086.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Amazon.CognitoSync.SyncManager.Dataset/DatasetDeletedDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void DatasetDeletedDelegate__ctor_m3623647397 (DatasetDeletedDelegate_t2502617919 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.CognitoSync.SyncManager.Dataset/DatasetDeletedDelegate::Invoke(Amazon.CognitoSync.SyncManager.Dataset)
extern "C"  bool DatasetDeletedDelegate_Invoke_m597851131 (DatasetDeletedDelegate_t2502617919 * __this, Dataset_t3040902086 * ___dataset0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Amazon.CognitoSync.SyncManager.Dataset/DatasetDeletedDelegate::BeginInvoke(Amazon.CognitoSync.SyncManager.Dataset,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DatasetDeletedDelegate_BeginInvoke_m3168878446 (DatasetDeletedDelegate_t2502617919 * __this, Dataset_t3040902086 * ___dataset0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.CognitoSync.SyncManager.Dataset/DatasetDeletedDelegate::EndInvoke(System.IAsyncResult)
extern "C"  bool DatasetDeletedDelegate_EndInvoke_m924832495 (DatasetDeletedDelegate_t2502617919 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
