﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Threading.AutoResetEvent
struct AutoResetEvent_t15112628;

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_NetworkReachability1092747145.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Util.Storage.Internal.NetworkInfo/<>c__DisplayClass1_0
struct  U3CU3Ec__DisplayClass1_0_t2945196168  : public Il2CppObject
{
public:
	// UnityEngine.NetworkReachability Amazon.Util.Storage.Internal.NetworkInfo/<>c__DisplayClass1_0::_networkReachability
	int32_t ____networkReachability_0;
	// System.Threading.AutoResetEvent Amazon.Util.Storage.Internal.NetworkInfo/<>c__DisplayClass1_0::asyncEvent
	AutoResetEvent_t15112628 * ___asyncEvent_1;

public:
	inline static int32_t get_offset_of__networkReachability_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1_0_t2945196168, ____networkReachability_0)); }
	inline int32_t get__networkReachability_0() const { return ____networkReachability_0; }
	inline int32_t* get_address_of__networkReachability_0() { return &____networkReachability_0; }
	inline void set__networkReachability_0(int32_t value)
	{
		____networkReachability_0 = value;
	}

	inline static int32_t get_offset_of_asyncEvent_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1_0_t2945196168, ___asyncEvent_1)); }
	inline AutoResetEvent_t15112628 * get_asyncEvent_1() const { return ___asyncEvent_1; }
	inline AutoResetEvent_t15112628 ** get_address_of_asyncEvent_1() { return &___asyncEvent_1; }
	inline void set_asyncEvent_1(AutoResetEvent_t15112628 * value)
	{
		___asyncEvent_1 = value;
		Il2CppCodeGenWriteBarrier(&___asyncEvent_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
