﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Linq.SortSequenceContext`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Object>
struct SortSequenceContext_2_t2301581283;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Object>
struct Func_2_t2065790391;
// System.Collections.Generic.IComparer`1<System.Object>
struct IComparer_1_t643912417;
// System.Linq.SortContext`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct SortContext_1_t3443151100;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
struct KeyValuePair_2U5BU5D_t2854920344;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Linq_SortDirection759359329.h"

// System.Void System.Linq.SortSequenceContext`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Object>::.ctor(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Linq.SortDirection,System.Linq.SortContext`1<TElement>)
extern "C"  void SortSequenceContext_2__ctor_m479639181_gshared (SortSequenceContext_2_t2301581283 * __this, Func_2_t2065790391 * ___selector0, Il2CppObject* ___comparer1, int32_t ___direction2, SortContext_1_t3443151100 * ___child_context3, const MethodInfo* method);
#define SortSequenceContext_2__ctor_m479639181(__this, ___selector0, ___comparer1, ___direction2, ___child_context3, method) ((  void (*) (SortSequenceContext_2_t2301581283 *, Func_2_t2065790391 *, Il2CppObject*, int32_t, SortContext_1_t3443151100 *, const MethodInfo*))SortSequenceContext_2__ctor_m479639181_gshared)(__this, ___selector0, ___comparer1, ___direction2, ___child_context3, method)
// System.Void System.Linq.SortSequenceContext`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Object>::Initialize(TElement[])
extern "C"  void SortSequenceContext_2_Initialize_m2584130701_gshared (SortSequenceContext_2_t2301581283 * __this, KeyValuePair_2U5BU5D_t2854920344* ___elements0, const MethodInfo* method);
#define SortSequenceContext_2_Initialize_m2584130701(__this, ___elements0, method) ((  void (*) (SortSequenceContext_2_t2301581283 *, KeyValuePair_2U5BU5D_t2854920344*, const MethodInfo*))SortSequenceContext_2_Initialize_m2584130701_gshared)(__this, ___elements0, method)
// System.Int32 System.Linq.SortSequenceContext`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Object>::Compare(System.Int32,System.Int32)
extern "C"  int32_t SortSequenceContext_2_Compare_m3372965574_gshared (SortSequenceContext_2_t2301581283 * __this, int32_t ___first_index0, int32_t ___second_index1, const MethodInfo* method);
#define SortSequenceContext_2_Compare_m3372965574(__this, ___first_index0, ___second_index1, method) ((  int32_t (*) (SortSequenceContext_2_t2301581283 *, int32_t, int32_t, const MethodInfo*))SortSequenceContext_2_Compare_m3372965574_gshared)(__this, ___first_index0, ___second_index1, method)
