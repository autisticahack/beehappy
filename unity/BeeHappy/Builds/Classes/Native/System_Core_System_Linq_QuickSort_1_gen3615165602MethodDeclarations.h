﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Linq.QuickSort`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct QuickSort_1_t3615165602;
// System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerable_1_t330981690;
// System.Linq.SortContext`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct SortContext_1_t3443151100;
// System.Int32[]
struct Int32U5BU5D_t3030399641;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Linq.QuickSort`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.IEnumerable`1<TElement>,System.Linq.SortContext`1<TElement>)
extern "C"  void QuickSort_1__ctor_m3487444839_gshared (QuickSort_1_t3615165602 * __this, Il2CppObject* ___source0, SortContext_1_t3443151100 * ___context1, const MethodInfo* method);
#define QuickSort_1__ctor_m3487444839(__this, ___source0, ___context1, method) ((  void (*) (QuickSort_1_t3615165602 *, Il2CppObject*, SortContext_1_t3443151100 *, const MethodInfo*))QuickSort_1__ctor_m3487444839_gshared)(__this, ___source0, ___context1, method)
// System.Int32[] System.Linq.QuickSort`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::CreateIndexes(System.Int32)
extern "C"  Int32U5BU5D_t3030399641* QuickSort_1_CreateIndexes_m1899482816_gshared (Il2CppObject * __this /* static, unused */, int32_t ___length0, const MethodInfo* method);
#define QuickSort_1_CreateIndexes_m1899482816(__this /* static, unused */, ___length0, method) ((  Int32U5BU5D_t3030399641* (*) (Il2CppObject * /* static, unused */, int32_t, const MethodInfo*))QuickSort_1_CreateIndexes_m1899482816_gshared)(__this /* static, unused */, ___length0, method)
// System.Void System.Linq.QuickSort`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::PerformSort()
extern "C"  void QuickSort_1_PerformSort_m2342623710_gshared (QuickSort_1_t3615165602 * __this, const MethodInfo* method);
#define QuickSort_1_PerformSort_m2342623710(__this, method) ((  void (*) (QuickSort_1_t3615165602 *, const MethodInfo*))QuickSort_1_PerformSort_m2342623710_gshared)(__this, method)
// System.Int32 System.Linq.QuickSort`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::CompareItems(System.Int32,System.Int32)
extern "C"  int32_t QuickSort_1_CompareItems_m4284378108_gshared (QuickSort_1_t3615165602 * __this, int32_t ___first_index0, int32_t ___second_index1, const MethodInfo* method);
#define QuickSort_1_CompareItems_m4284378108(__this, ___first_index0, ___second_index1, method) ((  int32_t (*) (QuickSort_1_t3615165602 *, int32_t, int32_t, const MethodInfo*))QuickSort_1_CompareItems_m4284378108_gshared)(__this, ___first_index0, ___second_index1, method)
// System.Int32 System.Linq.QuickSort`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::MedianOfThree(System.Int32,System.Int32)
extern "C"  int32_t QuickSort_1_MedianOfThree_m1493164384_gshared (QuickSort_1_t3615165602 * __this, int32_t ___left0, int32_t ___right1, const MethodInfo* method);
#define QuickSort_1_MedianOfThree_m1493164384(__this, ___left0, ___right1, method) ((  int32_t (*) (QuickSort_1_t3615165602 *, int32_t, int32_t, const MethodInfo*))QuickSort_1_MedianOfThree_m1493164384_gshared)(__this, ___left0, ___right1, method)
// System.Void System.Linq.QuickSort`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Sort(System.Int32,System.Int32)
extern "C"  void QuickSort_1_Sort_m2632548175_gshared (QuickSort_1_t3615165602 * __this, int32_t ___left0, int32_t ___right1, const MethodInfo* method);
#define QuickSort_1_Sort_m2632548175(__this, ___left0, ___right1, method) ((  void (*) (QuickSort_1_t3615165602 *, int32_t, int32_t, const MethodInfo*))QuickSort_1_Sort_m2632548175_gshared)(__this, ___left0, ___right1, method)
// System.Void System.Linq.QuickSort`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::InsertionSort(System.Int32,System.Int32)
extern "C"  void QuickSort_1_InsertionSort_m4121739834_gshared (QuickSort_1_t3615165602 * __this, int32_t ___left0, int32_t ___right1, const MethodInfo* method);
#define QuickSort_1_InsertionSort_m4121739834(__this, ___left0, ___right1, method) ((  void (*) (QuickSort_1_t3615165602 *, int32_t, int32_t, const MethodInfo*))QuickSort_1_InsertionSort_m4121739834_gshared)(__this, ___left0, ___right1, method)
// System.Void System.Linq.QuickSort`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Swap(System.Int32,System.Int32)
extern "C"  void QuickSort_1_Swap_m1849385696_gshared (QuickSort_1_t3615165602 * __this, int32_t ___left0, int32_t ___right1, const MethodInfo* method);
#define QuickSort_1_Swap_m1849385696(__this, ___left0, ___right1, method) ((  void (*) (QuickSort_1_t3615165602 *, int32_t, int32_t, const MethodInfo*))QuickSort_1_Swap_m1849385696_gshared)(__this, ___left0, ___right1, method)
// System.Collections.Generic.IEnumerable`1<TElement> System.Linq.QuickSort`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Sort(System.Collections.Generic.IEnumerable`1<TElement>,System.Linq.SortContext`1<TElement>)
extern "C"  Il2CppObject* QuickSort_1_Sort_m3234428291_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, SortContext_1_t3443151100 * ___context1, const MethodInfo* method);
#define QuickSort_1_Sort_m3234428291(__this /* static, unused */, ___source0, ___context1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, SortContext_1_t3443151100 *, const MethodInfo*))QuickSort_1_Sort_m3234428291_gshared)(__this /* static, unused */, ___source0, ___context1, method)
