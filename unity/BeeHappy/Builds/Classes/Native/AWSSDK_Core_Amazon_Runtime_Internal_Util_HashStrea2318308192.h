﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.Runtime.Internal.Util.IHashingWrapper
struct IHashingWrapper_t2610776002;
// System.Byte[]
struct ByteU5BU5D_t3397334013;

#include "AWSSDK_Core_Amazon_Runtime_Internal_Util_WrapperStr816765491.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Util.HashStream
struct  HashStream_t2318308192  : public WrapperStream_t816765491
{
public:
	// Amazon.Runtime.Internal.Util.IHashingWrapper Amazon.Runtime.Internal.Util.HashStream::<Algorithm>k__BackingField
	Il2CppObject * ___U3CAlgorithmU3Ek__BackingField_2;
	// System.Int64 Amazon.Runtime.Internal.Util.HashStream::<CurrentPosition>k__BackingField
	int64_t ___U3CCurrentPositionU3Ek__BackingField_3;
	// System.Byte[] Amazon.Runtime.Internal.Util.HashStream::<CalculatedHash>k__BackingField
	ByteU5BU5D_t3397334013* ___U3CCalculatedHashU3Ek__BackingField_4;
	// System.Byte[] Amazon.Runtime.Internal.Util.HashStream::<ExpectedHash>k__BackingField
	ByteU5BU5D_t3397334013* ___U3CExpectedHashU3Ek__BackingField_5;
	// System.Int64 Amazon.Runtime.Internal.Util.HashStream::<ExpectedLength>k__BackingField
	int64_t ___U3CExpectedLengthU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CAlgorithmU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(HashStream_t2318308192, ___U3CAlgorithmU3Ek__BackingField_2)); }
	inline Il2CppObject * get_U3CAlgorithmU3Ek__BackingField_2() const { return ___U3CAlgorithmU3Ek__BackingField_2; }
	inline Il2CppObject ** get_address_of_U3CAlgorithmU3Ek__BackingField_2() { return &___U3CAlgorithmU3Ek__BackingField_2; }
	inline void set_U3CAlgorithmU3Ek__BackingField_2(Il2CppObject * value)
	{
		___U3CAlgorithmU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CAlgorithmU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CCurrentPositionU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(HashStream_t2318308192, ___U3CCurrentPositionU3Ek__BackingField_3)); }
	inline int64_t get_U3CCurrentPositionU3Ek__BackingField_3() const { return ___U3CCurrentPositionU3Ek__BackingField_3; }
	inline int64_t* get_address_of_U3CCurrentPositionU3Ek__BackingField_3() { return &___U3CCurrentPositionU3Ek__BackingField_3; }
	inline void set_U3CCurrentPositionU3Ek__BackingField_3(int64_t value)
	{
		___U3CCurrentPositionU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CCalculatedHashU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(HashStream_t2318308192, ___U3CCalculatedHashU3Ek__BackingField_4)); }
	inline ByteU5BU5D_t3397334013* get_U3CCalculatedHashU3Ek__BackingField_4() const { return ___U3CCalculatedHashU3Ek__BackingField_4; }
	inline ByteU5BU5D_t3397334013** get_address_of_U3CCalculatedHashU3Ek__BackingField_4() { return &___U3CCalculatedHashU3Ek__BackingField_4; }
	inline void set_U3CCalculatedHashU3Ek__BackingField_4(ByteU5BU5D_t3397334013* value)
	{
		___U3CCalculatedHashU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCalculatedHashU3Ek__BackingField_4, value);
	}

	inline static int32_t get_offset_of_U3CExpectedHashU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(HashStream_t2318308192, ___U3CExpectedHashU3Ek__BackingField_5)); }
	inline ByteU5BU5D_t3397334013* get_U3CExpectedHashU3Ek__BackingField_5() const { return ___U3CExpectedHashU3Ek__BackingField_5; }
	inline ByteU5BU5D_t3397334013** get_address_of_U3CExpectedHashU3Ek__BackingField_5() { return &___U3CExpectedHashU3Ek__BackingField_5; }
	inline void set_U3CExpectedHashU3Ek__BackingField_5(ByteU5BU5D_t3397334013* value)
	{
		___U3CExpectedHashU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CExpectedHashU3Ek__BackingField_5, value);
	}

	inline static int32_t get_offset_of_U3CExpectedLengthU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(HashStream_t2318308192, ___U3CExpectedLengthU3Ek__BackingField_6)); }
	inline int64_t get_U3CExpectedLengthU3Ek__BackingField_6() const { return ___U3CExpectedLengthU3Ek__BackingField_6; }
	inline int64_t* get_address_of_U3CExpectedLengthU3Ek__BackingField_6() { return &___U3CExpectedLengthU3Ek__BackingField_6; }
	inline void set_U3CExpectedLengthU3Ek__BackingField_6(int64_t value)
	{
		___U3CExpectedLengthU3Ek__BackingField_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
