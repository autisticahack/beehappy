﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Queue_1_Enumerat3019169210MethodDeclarations.h"

// System.Void System.Collections.Generic.Queue`1/Enumerator<Amazon.Runtime.IUnityHttpRequest>::.ctor(System.Collections.Generic.Queue`1<T>)
#define Enumerator__ctor_m3150356652(__this, ___q0, method) ((  void (*) (Enumerator_t2189623312 *, Queue_1_t1679560232 *, const MethodInfo*))Enumerator__ctor_m677001007_gshared)(__this, ___q0, method)
// System.Void System.Collections.Generic.Queue`1/Enumerator<Amazon.Runtime.IUnityHttpRequest>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m211597079(__this, method) ((  void (*) (Enumerator_t2189623312 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m373072478_gshared)(__this, method)
// System.Object System.Collections.Generic.Queue`1/Enumerator<Amazon.Runtime.IUnityHttpRequest>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m817784187(__this, method) ((  Il2CppObject * (*) (Enumerator_t2189623312 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2167685344_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1/Enumerator<Amazon.Runtime.IUnityHttpRequest>::Dispose()
#define Enumerator_Dispose_m1970774296(__this, method) ((  void (*) (Enumerator_t2189623312 *, const MethodInfo*))Enumerator_Dispose_m575349149_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Queue`1/Enumerator<Amazon.Runtime.IUnityHttpRequest>::MoveNext()
#define Enumerator_MoveNext_m2808914699(__this, method) ((  bool (*) (Enumerator_t2189623312 *, const MethodInfo*))Enumerator_MoveNext_m742418190_gshared)(__this, method)
// T System.Collections.Generic.Queue`1/Enumerator<Amazon.Runtime.IUnityHttpRequest>::get_Current()
#define Enumerator_get_Current_m3372282230(__this, method) ((  Il2CppObject * (*) (Enumerator_t2189623312 *, const MethodInfo*))Enumerator_get_Current_m1613610405_gshared)(__this, method)
