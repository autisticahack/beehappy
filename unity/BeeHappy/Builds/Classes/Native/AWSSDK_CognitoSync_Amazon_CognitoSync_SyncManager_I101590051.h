﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// Amazon.CognitoSync.AmazonCognitoSyncClient
struct AmazonCognitoSyncClient_t3423722813;
// Amazon.CognitoIdentity.CognitoAWSCredentials
struct CognitoAWSCredentials_t2370264792;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.CognitoSync.SyncManager.Internal.CognitoSyncStorage
struct  CognitoSyncStorage_t101590051  : public Il2CppObject
{
public:
	// System.String Amazon.CognitoSync.SyncManager.Internal.CognitoSyncStorage::identityPoolId
	String_t* ___identityPoolId_0;
	// Amazon.CognitoSync.AmazonCognitoSyncClient Amazon.CognitoSync.SyncManager.Internal.CognitoSyncStorage::client
	AmazonCognitoSyncClient_t3423722813 * ___client_1;
	// Amazon.CognitoIdentity.CognitoAWSCredentials Amazon.CognitoSync.SyncManager.Internal.CognitoSyncStorage::cognitoCredentials
	CognitoAWSCredentials_t2370264792 * ___cognitoCredentials_2;

public:
	inline static int32_t get_offset_of_identityPoolId_0() { return static_cast<int32_t>(offsetof(CognitoSyncStorage_t101590051, ___identityPoolId_0)); }
	inline String_t* get_identityPoolId_0() const { return ___identityPoolId_0; }
	inline String_t** get_address_of_identityPoolId_0() { return &___identityPoolId_0; }
	inline void set_identityPoolId_0(String_t* value)
	{
		___identityPoolId_0 = value;
		Il2CppCodeGenWriteBarrier(&___identityPoolId_0, value);
	}

	inline static int32_t get_offset_of_client_1() { return static_cast<int32_t>(offsetof(CognitoSyncStorage_t101590051, ___client_1)); }
	inline AmazonCognitoSyncClient_t3423722813 * get_client_1() const { return ___client_1; }
	inline AmazonCognitoSyncClient_t3423722813 ** get_address_of_client_1() { return &___client_1; }
	inline void set_client_1(AmazonCognitoSyncClient_t3423722813 * value)
	{
		___client_1 = value;
		Il2CppCodeGenWriteBarrier(&___client_1, value);
	}

	inline static int32_t get_offset_of_cognitoCredentials_2() { return static_cast<int32_t>(offsetof(CognitoSyncStorage_t101590051, ___cognitoCredentials_2)); }
	inline CognitoAWSCredentials_t2370264792 * get_cognitoCredentials_2() const { return ___cognitoCredentials_2; }
	inline CognitoAWSCredentials_t2370264792 ** get_address_of_cognitoCredentials_2() { return &___cognitoCredentials_2; }
	inline void set_cognitoCredentials_2(CognitoAWSCredentials_t2370264792 * value)
	{
		___cognitoCredentials_2 = value;
		Il2CppCodeGenWriteBarrier(&___cognitoCredentials_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
