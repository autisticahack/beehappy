﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Specialized.StringDictionary
struct StringDictionary_t1070889667;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.Specialized.StringDictionary::.ctor()
extern "C"  void StringDictionary__ctor_m270184480 (StringDictionary_t1070889667 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Collections.Specialized.StringDictionary::GetEnumerator()
extern "C"  Il2CppObject * StringDictionary_GetEnumerator_m2202077700 (StringDictionary_t1070889667 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
