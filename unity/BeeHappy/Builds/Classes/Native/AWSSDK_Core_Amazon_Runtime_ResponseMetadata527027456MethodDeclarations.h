﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.ResponseMetadata
struct ResponseMetadata_t527027456;
// System.String
struct String_t;
// System.Collections.Generic.IDictionary`2<System.String,System.String>
struct IDictionary_2_t1943082916;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.String Amazon.Runtime.ResponseMetadata::get_RequestId()
extern "C"  String_t* ResponseMetadata_get_RequestId_m113207999 (ResponseMetadata_t527027456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.ResponseMetadata::set_RequestId(System.String)
extern "C"  void ResponseMetadata_set_RequestId_m648331128 (ResponseMetadata_t527027456 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.Runtime.ResponseMetadata::get_Metadata()
extern "C"  Il2CppObject* ResponseMetadata_get_Metadata_m297212266 (ResponseMetadata_t527027456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.ResponseMetadata::.ctor()
extern "C"  void ResponseMetadata__ctor_m4104976659 (ResponseMetadata_t527027456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
