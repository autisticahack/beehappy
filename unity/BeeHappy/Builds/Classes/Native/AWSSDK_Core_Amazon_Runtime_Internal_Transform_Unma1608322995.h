﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ThirdParty.Ionic.Zlib.CrcCalculatorStream
struct CrcCalculatorStream_t2228597532;
// Amazon.Runtime.Internal.Transform.IWebResponseData
struct IWebResponseData_t3815728244;
// Amazon.Runtime.Internal.Util.CachingWrapperStream
struct CachingWrapperStream_t380166576;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Transform.UnmarshallerContext
struct  UnmarshallerContext_t1608322995  : public Il2CppObject
{
public:
	// System.Boolean Amazon.Runtime.Internal.Transform.UnmarshallerContext::disposed
	bool ___disposed_0;
	// System.Boolean Amazon.Runtime.Internal.Transform.UnmarshallerContext::<MaintainResponseBody>k__BackingField
	bool ___U3CMaintainResponseBodyU3Ek__BackingField_1;
	// ThirdParty.Ionic.Zlib.CrcCalculatorStream Amazon.Runtime.Internal.Transform.UnmarshallerContext::<CrcStream>k__BackingField
	CrcCalculatorStream_t2228597532 * ___U3CCrcStreamU3Ek__BackingField_2;
	// System.Int32 Amazon.Runtime.Internal.Transform.UnmarshallerContext::<Crc32Result>k__BackingField
	int32_t ___U3CCrc32ResultU3Ek__BackingField_3;
	// Amazon.Runtime.Internal.Transform.IWebResponseData Amazon.Runtime.Internal.Transform.UnmarshallerContext::<WebResponseData>k__BackingField
	Il2CppObject * ___U3CWebResponseDataU3Ek__BackingField_4;
	// Amazon.Runtime.Internal.Util.CachingWrapperStream Amazon.Runtime.Internal.Transform.UnmarshallerContext::<WrappingStream>k__BackingField
	CachingWrapperStream_t380166576 * ___U3CWrappingStreamU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_disposed_0() { return static_cast<int32_t>(offsetof(UnmarshallerContext_t1608322995, ___disposed_0)); }
	inline bool get_disposed_0() const { return ___disposed_0; }
	inline bool* get_address_of_disposed_0() { return &___disposed_0; }
	inline void set_disposed_0(bool value)
	{
		___disposed_0 = value;
	}

	inline static int32_t get_offset_of_U3CMaintainResponseBodyU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(UnmarshallerContext_t1608322995, ___U3CMaintainResponseBodyU3Ek__BackingField_1)); }
	inline bool get_U3CMaintainResponseBodyU3Ek__BackingField_1() const { return ___U3CMaintainResponseBodyU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CMaintainResponseBodyU3Ek__BackingField_1() { return &___U3CMaintainResponseBodyU3Ek__BackingField_1; }
	inline void set_U3CMaintainResponseBodyU3Ek__BackingField_1(bool value)
	{
		___U3CMaintainResponseBodyU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CCrcStreamU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(UnmarshallerContext_t1608322995, ___U3CCrcStreamU3Ek__BackingField_2)); }
	inline CrcCalculatorStream_t2228597532 * get_U3CCrcStreamU3Ek__BackingField_2() const { return ___U3CCrcStreamU3Ek__BackingField_2; }
	inline CrcCalculatorStream_t2228597532 ** get_address_of_U3CCrcStreamU3Ek__BackingField_2() { return &___U3CCrcStreamU3Ek__BackingField_2; }
	inline void set_U3CCrcStreamU3Ek__BackingField_2(CrcCalculatorStream_t2228597532 * value)
	{
		___U3CCrcStreamU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCrcStreamU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CCrc32ResultU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(UnmarshallerContext_t1608322995, ___U3CCrc32ResultU3Ek__BackingField_3)); }
	inline int32_t get_U3CCrc32ResultU3Ek__BackingField_3() const { return ___U3CCrc32ResultU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CCrc32ResultU3Ek__BackingField_3() { return &___U3CCrc32ResultU3Ek__BackingField_3; }
	inline void set_U3CCrc32ResultU3Ek__BackingField_3(int32_t value)
	{
		___U3CCrc32ResultU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CWebResponseDataU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(UnmarshallerContext_t1608322995, ___U3CWebResponseDataU3Ek__BackingField_4)); }
	inline Il2CppObject * get_U3CWebResponseDataU3Ek__BackingField_4() const { return ___U3CWebResponseDataU3Ek__BackingField_4; }
	inline Il2CppObject ** get_address_of_U3CWebResponseDataU3Ek__BackingField_4() { return &___U3CWebResponseDataU3Ek__BackingField_4; }
	inline void set_U3CWebResponseDataU3Ek__BackingField_4(Il2CppObject * value)
	{
		___U3CWebResponseDataU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CWebResponseDataU3Ek__BackingField_4, value);
	}

	inline static int32_t get_offset_of_U3CWrappingStreamU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(UnmarshallerContext_t1608322995, ___U3CWrappingStreamU3Ek__BackingField_5)); }
	inline CachingWrapperStream_t380166576 * get_U3CWrappingStreamU3Ek__BackingField_5() const { return ___U3CWrappingStreamU3Ek__BackingField_5; }
	inline CachingWrapperStream_t380166576 ** get_address_of_U3CWrappingStreamU3Ek__BackingField_5() { return &___U3CWrappingStreamU3Ek__BackingField_5; }
	inline void set_U3CWrappingStreamU3Ek__BackingField_5(CachingWrapperStream_t380166576 * value)
	{
		___U3CWrappingStreamU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CWrappingStreamU3Ek__BackingField_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
