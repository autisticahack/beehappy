﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.UnityWwwRequestFactory
struct UnityWwwRequestFactory_t3206525961;
// Amazon.Runtime.IHttpRequest`1<System.String>
struct IHttpRequest_1_t2649998629;
// System.Uri
struct Uri_t19570940;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Uri19570940.h"

// Amazon.Runtime.IHttpRequest`1<System.String> Amazon.Runtime.Internal.UnityWwwRequestFactory::CreateHttpRequest(System.Uri)
extern "C"  Il2CppObject* UnityWwwRequestFactory_CreateHttpRequest_m3626561057 (UnityWwwRequestFactory_t3206525961 * __this, Uri_t19570940 * ___requestUri0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.UnityWwwRequestFactory::Dispose()
extern "C"  void UnityWwwRequestFactory_Dispose_m3291670278 (UnityWwwRequestFactory_t3206525961 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.UnityWwwRequestFactory::.ctor()
extern "C"  void UnityWwwRequestFactory__ctor_m93609469 (UnityWwwRequestFactory_t3206525961 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
