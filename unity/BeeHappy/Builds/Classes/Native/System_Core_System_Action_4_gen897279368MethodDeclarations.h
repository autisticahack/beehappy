﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action_4_gen2121535467MethodDeclarations.h"

// System.Void System.Action`4<Amazon.Runtime.AmazonWebServiceRequest,Amazon.Runtime.AmazonWebServiceResponse,System.Exception,Amazon.Runtime.AsyncOptions>::.ctor(System.Object,System.IntPtr)
#define Action_4__ctor_m1555803652(__this, ___object0, ___method1, method) ((  void (*) (Action_4_t897279368 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_4__ctor_m3242154536_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`4<Amazon.Runtime.AmazonWebServiceRequest,Amazon.Runtime.AmazonWebServiceResponse,System.Exception,Amazon.Runtime.AsyncOptions>::Invoke(T1,T2,T3,T4)
#define Action_4_Invoke_m1124015734(__this, ___arg10, ___arg21, ___arg32, ___arg43, method) ((  void (*) (Action_4_t897279368 *, AmazonWebServiceRequest_t3384026212 *, AmazonWebServiceResponse_t529043356 *, Exception_t1927440687 *, AsyncOptions_t558351272 *, const MethodInfo*))Action_4_Invoke_m1482157528_gshared)(__this, ___arg10, ___arg21, ___arg32, ___arg43, method)
// System.IAsyncResult System.Action`4<Amazon.Runtime.AmazonWebServiceRequest,Amazon.Runtime.AmazonWebServiceResponse,System.Exception,Amazon.Runtime.AsyncOptions>::BeginInvoke(T1,T2,T3,T4,System.AsyncCallback,System.Object)
#define Action_4_BeginInvoke_m2798777917(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___callback4, ___object5, method) ((  Il2CppObject * (*) (Action_4_t897279368 *, AmazonWebServiceRequest_t3384026212 *, AmazonWebServiceResponse_t529043356 *, Exception_t1927440687 *, AsyncOptions_t558351272 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Action_4_BeginInvoke_m2752165875_gshared)(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___callback4, ___object5, method)
// System.Void System.Action`4<Amazon.Runtime.AmazonWebServiceRequest,Amazon.Runtime.AmazonWebServiceResponse,System.Exception,Amazon.Runtime.AsyncOptions>::EndInvoke(System.IAsyncResult)
#define Action_4_EndInvoke_m441834662(__this, ___result0, method) ((  void (*) (Action_4_t897279368 *, Il2CppObject *, const MethodInfo*))Action_4_EndInvoke_m551759838_gshared)(__this, ___result0, method)
