﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1993830276.h"
#include "mscorlib_System_Array3829468939.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_ArrayMetadata1135078014.h"

// System.Void System.Array/InternalEnumerator`1<ThirdParty.Json.LitJson.ArrayMetadata>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2756170553_gshared (InternalEnumerator_1_t1993830276 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m2756170553(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1993830276 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2756170553_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4280724821_gshared (InternalEnumerator_1_t1993830276 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4280724821(__this, method) ((  void (*) (InternalEnumerator_1_t1993830276 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4280724821_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2232836713_gshared (InternalEnumerator_1_t1993830276 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2232836713(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1993830276 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2232836713_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<ThirdParty.Json.LitJson.ArrayMetadata>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1468042774_gshared (InternalEnumerator_1_t1993830276 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1468042774(__this, method) ((  void (*) (InternalEnumerator_1_t1993830276 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1468042774_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<ThirdParty.Json.LitJson.ArrayMetadata>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m235774285_gshared (InternalEnumerator_1_t1993830276 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m235774285(__this, method) ((  bool (*) (InternalEnumerator_1_t1993830276 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m235774285_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<ThirdParty.Json.LitJson.ArrayMetadata>::get_Current()
extern "C"  ArrayMetadata_t1135078014  InternalEnumerator_1_get_Current_m2815575366_gshared (InternalEnumerator_1_t1993830276 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m2815575366(__this, method) ((  ArrayMetadata_t1135078014  (*) (InternalEnumerator_1_t1993830276 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2815575366_gshared)(__this, method)
