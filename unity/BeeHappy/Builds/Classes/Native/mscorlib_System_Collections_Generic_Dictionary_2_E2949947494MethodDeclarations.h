﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En156532019MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,ThirdParty.iOS4Unity.Callbacks/Methods>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m910313851(__this, ___dictionary0, method) ((  void (*) (Enumerator_t2949947494 *, Dictionary_2_t1629922792 *, const MethodInfo*))Enumerator__ctor_m1897170543_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,ThirdParty.iOS4Unity.Callbacks/Methods>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3278446842(__this, method) ((  Il2CppObject * (*) (Enumerator_t2949947494 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2389891262_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,ThirdParty.iOS4Unity.Callbacks/Methods>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3135445212(__this, method) ((  void (*) (Enumerator_t2949947494 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3302454658_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,ThirdParty.iOS4Unity.Callbacks/Methods>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3345617645(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t2949947494 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m4077548725_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,ThirdParty.iOS4Unity.Callbacks/Methods>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2943446552(__this, method) ((  Il2CppObject * (*) (Enumerator_t2949947494 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2319419488_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,ThirdParty.iOS4Unity.Callbacks/Methods>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2992281322(__this, method) ((  Il2CppObject * (*) (Enumerator_t2949947494 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m120250090_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,ThirdParty.iOS4Unity.Callbacks/Methods>::MoveNext()
#define Enumerator_MoveNext_m4130953076(__this, method) ((  bool (*) (Enumerator_t2949947494 *, const MethodInfo*))Enumerator_MoveNext_m266619810_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,ThirdParty.iOS4Unity.Callbacks/Methods>::get_Current()
#define Enumerator_get_Current_m830153124(__this, method) ((  KeyValuePair_2_t3682235310  (*) (Enumerator_t2949947494 *, const MethodInfo*))Enumerator_get_Current_m3216113338_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,ThirdParty.iOS4Unity.Callbacks/Methods>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m719911871(__this, method) ((  IntPtr_t (*) (Enumerator_t2949947494 *, const MethodInfo*))Enumerator_get_CurrentKey_m2387752419_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,ThirdParty.iOS4Unity.Callbacks/Methods>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m1551931575(__this, method) ((  Methods_t1187897474 * (*) (Enumerator_t2949947494 *, const MethodInfo*))Enumerator_get_CurrentValue_m3147984131_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,ThirdParty.iOS4Unity.Callbacks/Methods>::Reset()
#define Enumerator_Reset_m162115429(__this, method) ((  void (*) (Enumerator_t2949947494 *, const MethodInfo*))Enumerator_Reset_m1147555085_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,ThirdParty.iOS4Unity.Callbacks/Methods>::VerifyState()
#define Enumerator_VerifyState_m686666000(__this, method) ((  void (*) (Enumerator_t2949947494 *, const MethodInfo*))Enumerator_VerifyState_m1547102902_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,ThirdParty.iOS4Unity.Callbacks/Methods>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m1275619340(__this, method) ((  void (*) (Enumerator_t2949947494 *, const MethodInfo*))Enumerator_VerifyCurrent_m213906106_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,ThirdParty.iOS4Unity.Callbacks/Methods>::Dispose()
#define Enumerator_Dispose_m205876951(__this, method) ((  void (*) (Enumerator_t2949947494 *, const MethodInfo*))Enumerator_Dispose_m2117437651_gshared)(__this, method)
