﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2266295352.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21407543090.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3901860260_gshared (InternalEnumerator_1_t2266295352 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m3901860260(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2266295352 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3901860260_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1006458276_gshared (InternalEnumerator_1_t2266295352 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1006458276(__this, method) ((  void (*) (InternalEnumerator_1_t2266295352 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1006458276_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3534381370_gshared (InternalEnumerator_1_t2266295352 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3534381370(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2266295352 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3534381370_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2784844239_gshared (InternalEnumerator_1_t2266295352 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2784844239(__this, method) ((  void (*) (InternalEnumerator_1_t2266295352 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2784844239_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2989667308_gshared (InternalEnumerator_1_t2266295352 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2989667308(__this, method) ((  bool (*) (InternalEnumerator_1_t2266295352 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2989667308_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>>::get_Current()
extern "C"  KeyValuePair_2_t1407543090  InternalEnumerator_1_get_Current_m3673910675_gshared (InternalEnumerator_1_t2266295352 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m3673910675(__this, method) ((  KeyValuePair_2_t1407543090  (*) (InternalEnumerator_1_t2266295352 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3673910675_gshared)(__this, method)
