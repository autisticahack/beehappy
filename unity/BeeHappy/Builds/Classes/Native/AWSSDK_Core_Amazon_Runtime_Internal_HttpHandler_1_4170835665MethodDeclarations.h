﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.HttpHandler`1<System.Object>
struct HttpHandler_1_t4170835665;
// System.Object
struct Il2CppObject;
// Amazon.Runtime.IHttpRequestFactory`1<System.Object>
struct IHttpRequestFactory_1_t126437655;
// Amazon.Runtime.IExecutionContext
struct IExecutionContext_t2477130752;
// Amazon.Runtime.IHttpRequest`1<System.Object>
struct IHttpRequest_1_t3310227691;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// Amazon.Runtime.IAsyncExecutionContext
struct IAsyncExecutionContext_t3792344986;
// Amazon.Runtime.IRequestContext
struct IRequestContext_t1620878341;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Object Amazon.Runtime.Internal.HttpHandler`1<System.Object>::get_CallbackSender()
extern "C"  Il2CppObject * HttpHandler_1_get_CallbackSender_m674323003_gshared (HttpHandler_1_t4170835665 * __this, const MethodInfo* method);
#define HttpHandler_1_get_CallbackSender_m674323003(__this, method) ((  Il2CppObject * (*) (HttpHandler_1_t4170835665 *, const MethodInfo*))HttpHandler_1_get_CallbackSender_m674323003_gshared)(__this, method)
// System.Void Amazon.Runtime.Internal.HttpHandler`1<System.Object>::set_CallbackSender(System.Object)
extern "C"  void HttpHandler_1_set_CallbackSender_m1603440352_gshared (HttpHandler_1_t4170835665 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define HttpHandler_1_set_CallbackSender_m1603440352(__this, ___value0, method) ((  void (*) (HttpHandler_1_t4170835665 *, Il2CppObject *, const MethodInfo*))HttpHandler_1_set_CallbackSender_m1603440352_gshared)(__this, ___value0, method)
// System.Void Amazon.Runtime.Internal.HttpHandler`1<System.Object>::.ctor(Amazon.Runtime.IHttpRequestFactory`1<TRequestContent>,System.Object)
extern "C"  void HttpHandler_1__ctor_m1273353708_gshared (HttpHandler_1_t4170835665 * __this, Il2CppObject* ___requestFactory0, Il2CppObject * ___callbackSender1, const MethodInfo* method);
#define HttpHandler_1__ctor_m1273353708(__this, ___requestFactory0, ___callbackSender1, method) ((  void (*) (HttpHandler_1_t4170835665 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))HttpHandler_1__ctor_m1273353708_gshared)(__this, ___requestFactory0, ___callbackSender1, method)
// System.Void Amazon.Runtime.Internal.HttpHandler`1<System.Object>::InvokeSync(Amazon.Runtime.IExecutionContext)
extern "C"  void HttpHandler_1_InvokeSync_m3925324746_gshared (HttpHandler_1_t4170835665 * __this, Il2CppObject * ___executionContext0, const MethodInfo* method);
#define HttpHandler_1_InvokeSync_m3925324746(__this, ___executionContext0, method) ((  void (*) (HttpHandler_1_t4170835665 *, Il2CppObject *, const MethodInfo*))HttpHandler_1_InvokeSync_m3925324746_gshared)(__this, ___executionContext0, method)
// System.Void Amazon.Runtime.Internal.HttpHandler`1<System.Object>::CompleteFailedRequest(Amazon.Runtime.IHttpRequest`1<TRequestContent>)
extern "C"  void HttpHandler_1_CompleteFailedRequest_m2847384443_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___httpRequest0, const MethodInfo* method);
#define HttpHandler_1_CompleteFailedRequest_m2847384443(__this /* static, unused */, ___httpRequest0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))HttpHandler_1_CompleteFailedRequest_m2847384443_gshared)(__this /* static, unused */, ___httpRequest0, method)
// System.IAsyncResult Amazon.Runtime.Internal.HttpHandler`1<System.Object>::InvokeAsync(Amazon.Runtime.IAsyncExecutionContext)
extern "C"  Il2CppObject * HttpHandler_1_InvokeAsync_m2861142229_gshared (HttpHandler_1_t4170835665 * __this, Il2CppObject * ___executionContext0, const MethodInfo* method);
#define HttpHandler_1_InvokeAsync_m2861142229(__this, ___executionContext0, method) ((  Il2CppObject * (*) (HttpHandler_1_t4170835665 *, Il2CppObject *, const MethodInfo*))HttpHandler_1_InvokeAsync_m2861142229_gshared)(__this, ___executionContext0, method)
// System.Void Amazon.Runtime.Internal.HttpHandler`1<System.Object>::GetRequestStreamCallback(System.IAsyncResult)
extern "C"  void HttpHandler_1_GetRequestStreamCallback_m1249572158_gshared (HttpHandler_1_t4170835665 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define HttpHandler_1_GetRequestStreamCallback_m1249572158(__this, ___result0, method) ((  void (*) (HttpHandler_1_t4170835665 *, Il2CppObject *, const MethodInfo*))HttpHandler_1_GetRequestStreamCallback_m1249572158_gshared)(__this, ___result0, method)
// System.Void Amazon.Runtime.Internal.HttpHandler`1<System.Object>::GetRequestStreamCallbackHelper(System.Object)
extern "C"  void HttpHandler_1_GetRequestStreamCallbackHelper_m4234283863_gshared (HttpHandler_1_t4170835665 * __this, Il2CppObject * ___state0, const MethodInfo* method);
#define HttpHandler_1_GetRequestStreamCallbackHelper_m4234283863(__this, ___state0, method) ((  void (*) (HttpHandler_1_t4170835665 *, Il2CppObject *, const MethodInfo*))HttpHandler_1_GetRequestStreamCallbackHelper_m4234283863_gshared)(__this, ___state0, method)
// System.Void Amazon.Runtime.Internal.HttpHandler`1<System.Object>::GetResponseCallback(System.IAsyncResult)
extern "C"  void HttpHandler_1_GetResponseCallback_m2219318320_gshared (HttpHandler_1_t4170835665 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define HttpHandler_1_GetResponseCallback_m2219318320(__this, ___result0, method) ((  void (*) (HttpHandler_1_t4170835665 *, Il2CppObject *, const MethodInfo*))HttpHandler_1_GetResponseCallback_m2219318320_gshared)(__this, ___result0, method)
// System.Void Amazon.Runtime.Internal.HttpHandler`1<System.Object>::GetResponseCallbackHelper(System.Object)
extern "C"  void HttpHandler_1_GetResponseCallbackHelper_m514406937_gshared (HttpHandler_1_t4170835665 * __this, Il2CppObject * ___state0, const MethodInfo* method);
#define HttpHandler_1_GetResponseCallbackHelper_m514406937(__this, ___state0, method) ((  void (*) (HttpHandler_1_t4170835665 *, Il2CppObject *, const MethodInfo*))HttpHandler_1_GetResponseCallbackHelper_m514406937_gshared)(__this, ___state0, method)
// System.Void Amazon.Runtime.Internal.HttpHandler`1<System.Object>::SetMetrics(Amazon.Runtime.IRequestContext)
extern "C"  void HttpHandler_1_SetMetrics_m2391049017_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___requestContext0, const MethodInfo* method);
#define HttpHandler_1_SetMetrics_m2391049017(__this /* static, unused */, ___requestContext0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))HttpHandler_1_SetMetrics_m2391049017_gshared)(__this /* static, unused */, ___requestContext0, method)
// System.Void Amazon.Runtime.Internal.HttpHandler`1<System.Object>::WriteContentToRequestBody(TRequestContent,Amazon.Runtime.IHttpRequest`1<TRequestContent>,Amazon.Runtime.IRequestContext)
extern "C"  void HttpHandler_1_WriteContentToRequestBody_m1084199443_gshared (HttpHandler_1_t4170835665 * __this, Il2CppObject * ___requestContent0, Il2CppObject* ___httpRequest1, Il2CppObject * ___requestContext2, const MethodInfo* method);
#define HttpHandler_1_WriteContentToRequestBody_m1084199443(__this, ___requestContent0, ___httpRequest1, ___requestContext2, method) ((  void (*) (HttpHandler_1_t4170835665 *, Il2CppObject *, Il2CppObject*, Il2CppObject *, const MethodInfo*))HttpHandler_1_WriteContentToRequestBody_m1084199443_gshared)(__this, ___requestContent0, ___httpRequest1, ___requestContext2, method)
// Amazon.Runtime.IHttpRequest`1<TRequestContent> Amazon.Runtime.Internal.HttpHandler`1<System.Object>::CreateWebRequest(Amazon.Runtime.IRequestContext)
extern "C"  Il2CppObject* HttpHandler_1_CreateWebRequest_m3217971059_gshared (HttpHandler_1_t4170835665 * __this, Il2CppObject * ___requestContext0, const MethodInfo* method);
#define HttpHandler_1_CreateWebRequest_m3217971059(__this, ___requestContext0, method) ((  Il2CppObject* (*) (HttpHandler_1_t4170835665 *, Il2CppObject *, const MethodInfo*))HttpHandler_1_CreateWebRequest_m3217971059_gshared)(__this, ___requestContext0, method)
// System.Void Amazon.Runtime.Internal.HttpHandler`1<System.Object>::Dispose()
extern "C"  void HttpHandler_1_Dispose_m3091244702_gshared (HttpHandler_1_t4170835665 * __this, const MethodInfo* method);
#define HttpHandler_1_Dispose_m3091244702(__this, method) ((  void (*) (HttpHandler_1_t4170835665 *, const MethodInfo*))HttpHandler_1_Dispose_m3091244702_gshared)(__this, method)
// System.Void Amazon.Runtime.Internal.HttpHandler`1<System.Object>::Dispose(System.Boolean)
extern "C"  void HttpHandler_1_Dispose_m2081298693_gshared (HttpHandler_1_t4170835665 * __this, bool ___disposing0, const MethodInfo* method);
#define HttpHandler_1_Dispose_m2081298693(__this, ___disposing0, method) ((  void (*) (HttpHandler_1_t4170835665 *, bool, const MethodInfo*))HttpHandler_1_Dispose_m2081298693_gshared)(__this, ___disposing0, method)
