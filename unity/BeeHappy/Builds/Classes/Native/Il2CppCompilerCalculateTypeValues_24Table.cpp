﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_IntU2174001701.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_Long2567652076.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_Bool2985984772.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_Stri3953260147.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_Date1723598451.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_Resp1207185784.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_Unma2325150308.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_Unma1608322995.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_XmlU1179575220.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_Unit3141113362.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Auth_ClientPro4005895111.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Auth_AbstractA2114314031.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Auth_AWS4Signe1011449585.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Auth_AWS4Signe2266868118.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Auth_AWS4Signin430803065.h"
#include "AWSSDK_Core_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "AWSSDK_Core_U3CPrivateImplementationDetailsU3E___S2631791551.h"
#include "Mono_Data_Tds_U3CModuleU3E3783534214.h"
#include "System_Transactions_U3CModuleU3E3783534214.h"
#include "System_Transactions_System_MonoTODOAttribute3487514019.h"
#include "System_Transactions_System_Transactions_Enlistment1750074243.h"
#include "System_Transactions_System_Transactions_Enlistment1731762299.h"
#include "System_Transactions_System_Transactions_IsolationL4045174252.h"
#include "System_Transactions_System_Transactions_PreparingE2112014291.h"
#include "System_Transactions_System_Transactions_SinglePhas4207375558.h"
#include "System_Transactions_System_Transactions_Transaction869361102.h"
#include "System_Transactions_System_Transactions_Transactio1712832570.h"
#include "System_Transactions_System_Transactions_Transaction198021791.h"
#include "System_Transactions_System_Transactions_Transactio3811320188.h"
#include "System_Transactions_System_Transactions_Transactio3985316291.h"
#include "System_Transactions_System_Transactions_Transactio1588908416.h"
#include "System_Transactions_System_Transactions_Transactio1681136162.h"
#include "System_Transactions_System_Transactions_Transactio4266194108.h"
#include "AWSSDK_SecurityToken_U3CModuleU3E3783534214.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_AmazonSe1241355389.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_AmazonSe2700714712.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_AmazonSe1261559370.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_AmazonSe3771342025.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_AmazonSec265680841.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_Model_Cr3554032640.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_Model_Ass150458319.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_Model_Ass193094215.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_Model_As3931705881.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_Model_Ex1058704965.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_Model_ID2234231496.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_Model_IDP367458406.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_Model_In1321611201.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_Model_Mal157840309.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_Model_Pa4099730702.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_Model_Re1422077785.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_Model_In2473561683.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_Model_In1815493036.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_Model_In3698885375.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_Model_In3599710454.h"
#include "System_Data_U3CModuleU3E3783534214.h"
#include "System_Data_Mono_Data_SqlExpressions_Parser1744254983.h"
#include "System_Data_Mono_Data_SqlExpressions_Parser_YYRule3228736289.h"
#include "System_Data_Mono_Data_SqlExpressions_yydebug_yyDeb3793616829.h"
#include "System_Data_Mono_Data_SqlExpressions_yyParser_yyEx1350108591.h"
#include "System_Data_Mono_Data_SqlExpressions_yyParser_yyUn2784664073.h"
#include "System_Data_Locale4255929014.h"
#include "System_Data_System_MonoTODOAttribute3487514019.h"
#include "System_Data_Mono_Data_SqlExpressions_AggregationFu2691828360.h"
#include "System_Data_Mono_Data_SqlExpressions_Aggregation2849412868.h"
#include "System_Data_Mono_Data_SqlExpressions_Negative721187499.h"
#include "System_Data_Mono_Data_SqlExpressions_ArithmeticOpe3707246961.h"
#include "System_Data_Mono_Data_SqlExpressions_Negation3355962509.h"
#include "System_Data_Mono_Data_SqlExpressions_BoolOperation3116017511.h"
#include "System_Data_Mono_Data_SqlExpressions_ReferencedTab2363768325.h"
#include "System_Data_Mono_Data_SqlExpressions_ColumnReferenc717778461.h"
#include "System_Data_Mono_Data_SqlExpressions_Comparison3554387337.h"
#include "System_Data_Mono_Data_SqlExpressions_BaseExpressio4067875947.h"
#include "System_Data_Mono_Data_SqlExpressions_UnaryExpressi2769986971.h"
#include "System_Data_Mono_Data_SqlExpressions_BinaryExpress4018232945.h"
#include "System_Data_Mono_Data_SqlExpressions_Operation2847614977.h"
#include "System_Data_Mono_Data_SqlExpressions_BinaryOpExpre3781280966.h"
#include "System_Data_Mono_Data_SqlExpressions_IifFunction224081018.h"
#include "System_Data_Mono_Data_SqlExpressions_IsNullFunction156305379.h"
#include "System_Data_Mono_Data_SqlExpressions_ConvertFuncti1592470909.h"
#include "System_Data_Mono_Data_SqlExpressions_In3546431515.h"
#include "System_Data_Mono_Data_SqlExpressions_Like1057370379.h"
#include "System_Data_Mono_Data_SqlExpressions_Literal3735181871.h"
#include "System_Data_Mono_Data_SqlExpressions_Numeric3714709959.h"
#include "System_Data_Mono_Data_SqlExpressions_StringFunctio1721758303.h"
#include "System_Data_Mono_Data_SqlExpressions_ConcatFunction815662470.h"
#include "System_Data_Mono_Data_SqlExpressions_SubstringFunct927862047.h"
#include "System_Data_Mono_Data_SqlExpressions_LenFunction2004154325.h"
#include "System_Data_Mono_Data_SqlExpressions_TrimFunction771403798.h"
#include "System_Data_Mono_Data_SqlExpressions_Tokenizer3681895715.h"
#include "System_Data_System_Data_Common_CatalogLocation1149802810.h"
#include "System_Data_System_Data_Common_DataAdapter668329355.h"
#include "System_Data_System_Data_Common_DataColumnMapping969655534.h"
#include "System_Data_System_Data_Common_DataColumnMappingCo3025014470.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2400 = { sizeof (IntUnmarshaller_t2174001701), -1, sizeof(IntUnmarshaller_t2174001701_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2400[1] = 
{
	IntUnmarshaller_t2174001701_StaticFields::get_offset_of__instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2401 = { sizeof (LongUnmarshaller_t2567652076), -1, sizeof(LongUnmarshaller_t2567652076_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2401[1] = 
{
	LongUnmarshaller_t2567652076_StaticFields::get_offset_of__instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2402 = { sizeof (BoolUnmarshaller_t2985984772), -1, sizeof(BoolUnmarshaller_t2985984772_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2402[1] = 
{
	BoolUnmarshaller_t2985984772_StaticFields::get_offset_of__instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2403 = { sizeof (StringUnmarshaller_t3953260147), -1, sizeof(StringUnmarshaller_t3953260147_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2403[1] = 
{
	StringUnmarshaller_t3953260147_StaticFields::get_offset_of__instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2404 = { sizeof (DateTimeUnmarshaller_t1723598451), -1, sizeof(DateTimeUnmarshaller_t1723598451_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2404[1] = 
{
	DateTimeUnmarshaller_t1723598451_StaticFields::get_offset_of__instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2405 = { sizeof (ResponseMetadataUnmarshaller_t1207185784), -1, sizeof(ResponseMetadataUnmarshaller_t1207185784_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2405[1] = 
{
	ResponseMetadataUnmarshaller_t1207185784_StaticFields::get_offset_of__instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2406 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2406[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2407 = { sizeof (UnmarshallerExtensions_t2325150308), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2408 = { sizeof (UnmarshallerContext_t1608322995), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2408[6] = 
{
	UnmarshallerContext_t1608322995::get_offset_of_disposed_0(),
	UnmarshallerContext_t1608322995::get_offset_of_U3CMaintainResponseBodyU3Ek__BackingField_1(),
	UnmarshallerContext_t1608322995::get_offset_of_U3CCrcStreamU3Ek__BackingField_2(),
	UnmarshallerContext_t1608322995::get_offset_of_U3CCrc32ResultU3Ek__BackingField_3(),
	UnmarshallerContext_t1608322995::get_offset_of_U3CWebResponseDataU3Ek__BackingField_4(),
	UnmarshallerContext_t1608322995::get_offset_of_U3CWrappingStreamU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2409 = { sizeof (XmlUnmarshallerContext_t1179575220), -1, sizeof(XmlUnmarshallerContext_t1179575220_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2409[12] = 
{
	XmlUnmarshallerContext_t1179575220_StaticFields::get_offset_of_READER_SETTINGS_6(),
	XmlUnmarshallerContext_t1179575220_StaticFields::get_offset_of_nodesToSkip_7(),
	XmlUnmarshallerContext_t1179575220::get_offset_of_streamReader_8(),
	XmlUnmarshallerContext_t1179575220::get_offset_of__xmlReader_9(),
	XmlUnmarshallerContext_t1179575220::get_offset_of_stack_10(),
	XmlUnmarshallerContext_t1179575220::get_offset_of_stackString_11(),
	XmlUnmarshallerContext_t1179575220::get_offset_of_attributeValues_12(),
	XmlUnmarshallerContext_t1179575220::get_offset_of_attributeNames_13(),
	XmlUnmarshallerContext_t1179575220::get_offset_of_attributeEnumerator_14(),
	XmlUnmarshallerContext_t1179575220::get_offset_of_nodeType_15(),
	XmlUnmarshallerContext_t1179575220::get_offset_of_nodeContent_16(),
	XmlUnmarshallerContext_t1179575220::get_offset_of_disposed_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2410 = { sizeof (UnityWebResponseData_t3141113362), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2410[10] = 
{
	UnityWebResponseData_t3141113362::get_offset_of__headers_0(),
	UnityWebResponseData_t3141113362::get_offset_of__headerNames_1(),
	UnityWebResponseData_t3141113362::get_offset_of__headerNamesSet_2(),
	UnityWebResponseData_t3141113362::get_offset_of__responseStream_3(),
	UnityWebResponseData_t3141113362::get_offset_of__responseBody_4(),
	UnityWebResponseData_t3141113362::get_offset_of__logger_5(),
	UnityWebResponseData_t3141113362::get_offset_of_U3CContentLengthU3Ek__BackingField_6(),
	UnityWebResponseData_t3141113362::get_offset_of_U3CContentTypeU3Ek__BackingField_7(),
	UnityWebResponseData_t3141113362::get_offset_of_U3CStatusCodeU3Ek__BackingField_8(),
	UnityWebResponseData_t3141113362::get_offset_of_U3CIsSuccessStatusCodeU3Ek__BackingField_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2411 = { sizeof (ClientProtocol_t4005895111)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2411[4] = 
{
	ClientProtocol_t4005895111::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2412 = { sizeof (AbstractAWSSigner_t2114314031), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2412[1] = 
{
	AbstractAWSSigner_t2114314031::get_offset_of__aws4Signer_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2413 = { sizeof (AWS4Signer_t1011449585), -1, sizeof(AWS4Signer_t1011449585_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2413[13] = 
{
	0,
	0,
	0,
	0,
	AWS4Signer_t1011449585_StaticFields::get_offset_of_TerminatorBytes_5(),
	0,
	0,
	0,
	0,
	0,
	0,
	AWS4Signer_t1011449585_StaticFields::get_offset_of_CompressWhitespaceRegex_12(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2414 = { sizeof (U3CU3Ec_t2266868118), -1, sizeof(U3CU3Ec_t2266868118_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2414[4] = 
{
	U3CU3Ec_t2266868118_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t2266868118_StaticFields::get_offset_of_U3CU3E9__36_0_1(),
	U3CU3Ec_t2266868118_StaticFields::get_offset_of_U3CU3E9__37_0_2(),
	U3CU3Ec_t2266868118_StaticFields::get_offset_of_U3CU3E9__38_0_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2415 = { sizeof (AWS4SigningResult_t430803065), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2415[6] = 
{
	AWS4SigningResult_t430803065::get_offset_of__awsAccessKeyId_0(),
	AWS4SigningResult_t430803065::get_offset_of__originalDateTime_1(),
	AWS4SigningResult_t430803065::get_offset_of__signedHeaders_2(),
	AWS4SigningResult_t430803065::get_offset_of__scope_3(),
	AWS4SigningResult_t430803065::get_offset_of__signingKey_4(),
	AWS4SigningResult_t430803065::get_offset_of__signature_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2416 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305143), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2416[1] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U350B1635D1FB2907A171B71751E1A3FA79423CA17_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2417 = { sizeof (__StaticArrayInitTypeSizeU3D112_t2631791551)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D112_t2631791551 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2418 = { sizeof (U3CModuleU3E_t3783534222), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2419 = { sizeof (U3CModuleU3E_t3783534223), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2420 = { sizeof (MonoTODOAttribute_t3487514023), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2420[1] = 
{
	MonoTODOAttribute_t3487514023::get_offset_of_comment_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2421 = { sizeof (Enlistment_t1750074243), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2421[1] = 
{
	Enlistment_t1750074243::get_offset_of_done_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2422 = { sizeof (EnlistmentOptions_t1731762299)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2422[3] = 
{
	EnlistmentOptions_t1731762299::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2423 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2424 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2425 = { sizeof (IsolationLevel_t4045174252)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2425[8] = 
{
	IsolationLevel_t4045174252::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2426 = { sizeof (PreparingEnlistment_t2112014291), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2426[3] = 
{
	PreparingEnlistment_t2112014291::get_offset_of_prepared_1(),
	PreparingEnlistment_t2112014291::get_offset_of_tx_2(),
	PreparingEnlistment_t2112014291::get_offset_of_enlisted_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2427 = { sizeof (SinglePhaseEnlistment_t4207375558), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2427[2] = 
{
	SinglePhaseEnlistment_t4207375558::get_offset_of_tx_1(),
	SinglePhaseEnlistment_t4207375558::get_offset_of_enlisted_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2428 = { sizeof (Transaction_t869361102), -1, 0, sizeof(Transaction_t869361102_ThreadStaticFields) };
extern const int32_t g_FieldOffsetTable2428[11] = 
{
	THREAD_STATIC_FIELD_OFFSET,
	Transaction_t869361102::get_offset_of_level_1(),
	Transaction_t869361102::get_offset_of_info_2(),
	Transaction_t869361102::get_offset_of_dependents_3(),
	Transaction_t869361102::get_offset_of_volatiles_4(),
	Transaction_t869361102::get_offset_of_durables_5(),
	Transaction_t869361102::get_offset_of_committing_6(),
	Transaction_t869361102::get_offset_of_committed_7(),
	Transaction_t869361102::get_offset_of_aborted_8(),
	Transaction_t869361102::get_offset_of_scope_9(),
	Transaction_t869361102::get_offset_of_innerException_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2429 = { sizeof (TransactionAbortedException_t1712832570), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2430 = { sizeof (TransactionException_t198021791), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2431 = { sizeof (TransactionInformation_t3811320188), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2431[4] = 
{
	TransactionInformation_t3811320188::get_offset_of_local_id_0(),
	TransactionInformation_t3811320188::get_offset_of_dtcId_1(),
	TransactionInformation_t3811320188::get_offset_of_creation_time_2(),
	TransactionInformation_t3811320188::get_offset_of_status_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2432 = { sizeof (TransactionManager_t3985316291), -1, sizeof(TransactionManager_t3985316291_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2432[2] = 
{
	TransactionManager_t3985316291_StaticFields::get_offset_of_defaultTimeout_0(),
	TransactionManager_t3985316291_StaticFields::get_offset_of_maxTimeout_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2433 = { sizeof (TransactionOptions_t1588908416)+ sizeof (Il2CppObject), sizeof(TransactionOptions_t1588908416 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2433[2] = 
{
	TransactionOptions_t1588908416::get_offset_of_level_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TransactionOptions_t1588908416::get_offset_of_timeout_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2434 = { sizeof (TransactionScope_t1681136162), -1, sizeof(TransactionScope_t1681136162_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2434[8] = 
{
	TransactionScope_t1681136162_StaticFields::get_offset_of_defaultOptions_0(),
	TransactionScope_t1681136162::get_offset_of_transaction_1(),
	TransactionScope_t1681136162::get_offset_of_oldTransaction_2(),
	TransactionScope_t1681136162::get_offset_of_parentScope_3(),
	TransactionScope_t1681136162::get_offset_of_nested_4(),
	TransactionScope_t1681136162::get_offset_of_disposed_5(),
	TransactionScope_t1681136162::get_offset_of_completed_6(),
	TransactionScope_t1681136162::get_offset_of_isRoot_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2435 = { sizeof (TransactionStatus_t4266194108)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2435[5] = 
{
	TransactionStatus_t4266194108::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2436 = { sizeof (U3CModuleU3E_t3783534224), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2437 = { sizeof (AmazonSecurityTokenServiceClient_t1241355389), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2438 = { sizeof (U3CU3Ec__DisplayClass16_0_t2700714712), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2438[1] = 
{
	U3CU3Ec__DisplayClass16_0_t2700714712::get_offset_of_callback_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2439 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2440 = { sizeof (AmazonSecurityTokenServiceConfig_t1261559370), -1, sizeof(AmazonSecurityTokenServiceConfig_t1261559370_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2440[2] = 
{
	AmazonSecurityTokenServiceConfig_t1261559370_StaticFields::get_offset_of_UserAgentString_21(),
	AmazonSecurityTokenServiceConfig_t1261559370::get_offset_of__userAgent_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2441 = { sizeof (AmazonSecurityTokenServiceException_t3771342025), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2442 = { sizeof (AmazonSecurityTokenServiceRequest_t265680841), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2443 = { sizeof (Credentials_t3554032640), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2443[5] = 
{
	Credentials_t3554032640::get_offset_of__credentials_0(),
	Credentials_t3554032640::get_offset_of__accessKeyId_1(),
	Credentials_t3554032640::get_offset_of__expiration_2(),
	Credentials_t3554032640::get_offset_of__secretAccessKey_3(),
	Credentials_t3554032640::get_offset_of__sessionToken_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2444 = { sizeof (AssumedRoleUser_t150458319), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2444[2] = 
{
	AssumedRoleUser_t150458319::get_offset_of__arn_0(),
	AssumedRoleUser_t150458319::get_offset_of__assumedRoleId_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2445 = { sizeof (AssumeRoleWithWebIdentityRequest_t193094215), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2445[6] = 
{
	AssumeRoleWithWebIdentityRequest_t193094215::get_offset_of__durationSeconds_3(),
	AssumeRoleWithWebIdentityRequest_t193094215::get_offset_of__policy_4(),
	AssumeRoleWithWebIdentityRequest_t193094215::get_offset_of__providerId_5(),
	AssumeRoleWithWebIdentityRequest_t193094215::get_offset_of__roleArn_6(),
	AssumeRoleWithWebIdentityRequest_t193094215::get_offset_of__roleSessionName_7(),
	AssumeRoleWithWebIdentityRequest_t193094215::get_offset_of__webIdentityToken_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2446 = { sizeof (AssumeRoleWithWebIdentityResponse_t3931705881), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2446[6] = 
{
	AssumeRoleWithWebIdentityResponse_t3931705881::get_offset_of__assumedRoleUser_3(),
	AssumeRoleWithWebIdentityResponse_t3931705881::get_offset_of__audience_4(),
	AssumeRoleWithWebIdentityResponse_t3931705881::get_offset_of__credentials_5(),
	AssumeRoleWithWebIdentityResponse_t3931705881::get_offset_of__packedPolicySize_6(),
	AssumeRoleWithWebIdentityResponse_t3931705881::get_offset_of__provider_7(),
	AssumeRoleWithWebIdentityResponse_t3931705881::get_offset_of__subjectFromWebIdentityToken_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2447 = { sizeof (ExpiredTokenException_t1058704965), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2448 = { sizeof (IDPCommunicationErrorException_t2234231496), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2449 = { sizeof (IDPRejectedClaimException_t367458406), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2450 = { sizeof (InvalidIdentityTokenException_t1321611201), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2451 = { sizeof (MalformedPolicyDocumentException_t157840309), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2452 = { sizeof (PackedPolicyTooLargeException_t4099730702), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2453 = { sizeof (RegionDisabledException_t1422077785), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2454 = { sizeof (AssumedRoleUserUnmarshaller_t2473561683), -1, sizeof(AssumedRoleUserUnmarshaller_t2473561683_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2454[1] = 
{
	AssumedRoleUserUnmarshaller_t2473561683_StaticFields::get_offset_of__instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2455 = { sizeof (AssumeRoleWithWebIdentityRequestMarshaller_t1815493036), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2456 = { sizeof (AssumeRoleWithWebIdentityResponseUnmarshaller_t3698885375), -1, sizeof(AssumeRoleWithWebIdentityResponseUnmarshaller_t3698885375_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2456[1] = 
{
	AssumeRoleWithWebIdentityResponseUnmarshaller_t3698885375_StaticFields::get_offset_of__instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2457 = { sizeof (CredentialsUnmarshaller_t3599710454), -1, sizeof(CredentialsUnmarshaller_t3599710454_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2457[1] = 
{
	CredentialsUnmarshaller_t3599710454_StaticFields::get_offset_of__instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2458 = { sizeof (U3CModuleU3E_t3783534225), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2459 = { sizeof (Parser_t1744254983), -1, sizeof(Parser_t1744254983_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2459[19] = 
{
	Parser_t1744254983::get_offset_of_cacheAggregationResults_0(),
	Parser_t1744254983::get_offset_of_aggregationRows_1(),
	Parser_t1744254983_StaticFields::get_offset_of_yacc_verbose_flag_2(),
	Parser_t1744254983::get_offset_of_ErrorOutput_3(),
	Parser_t1744254983::get_offset_of_eof_token_4(),
	Parser_t1744254983::get_offset_of_debug_5(),
	Parser_t1744254983_StaticFields::get_offset_of_yyFinal_6(),
	Parser_t1744254983_StaticFields::get_offset_of_yyNames_7(),
	Parser_t1744254983::get_offset_of_yyExpectingState_8(),
	Parser_t1744254983::get_offset_of_yyMax_9(),
	Parser_t1744254983_StaticFields::get_offset_of_yyLhs_10(),
	Parser_t1744254983_StaticFields::get_offset_of_yyLen_11(),
	Parser_t1744254983_StaticFields::get_offset_of_yyDefRed_12(),
	Parser_t1744254983_StaticFields::get_offset_of_yyDgoto_13(),
	Parser_t1744254983_StaticFields::get_offset_of_yySindex_14(),
	Parser_t1744254983_StaticFields::get_offset_of_yyRindex_15(),
	Parser_t1744254983_StaticFields::get_offset_of_yyGindex_16(),
	Parser_t1744254983_StaticFields::get_offset_of_yyTable_17(),
	Parser_t1744254983_StaticFields::get_offset_of_yyCheck_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2460 = { sizeof (YYRules_t3228736289), -1, sizeof(YYRules_t3228736289_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2460[1] = 
{
	YYRules_t3228736289_StaticFields::get_offset_of_yyRule_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2461 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2462 = { sizeof (yyDebugSimple_t3793616829), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2463 = { sizeof (yyException_t1350108591), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2464 = { sizeof (yyUnexpectedEof_t2784664073), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2465 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2466 = { sizeof (Locale_t4255929018), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2467 = { sizeof (MonoTODOAttribute_t3487514024), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2468 = { sizeof (AggregationFunction_t2691828360)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2468[8] = 
{
	AggregationFunction_t2691828360::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2469 = { sizeof (Aggregation_t2849412868), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2469[8] = 
{
	Aggregation_t2849412868::get_offset_of_cacheResults_0(),
	Aggregation_t2849412868::get_offset_of_rows_1(),
	Aggregation_t2849412868::get_offset_of_column_2(),
	Aggregation_t2849412868::get_offset_of_function_3(),
	Aggregation_t2849412868::get_offset_of_count_4(),
	Aggregation_t2849412868::get_offset_of_result_5(),
	Aggregation_t2849412868::get_offset_of_RowChangeHandler_6(),
	Aggregation_t2849412868::get_offset_of_table_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2470 = { sizeof (Negative_t721187499), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2471 = { sizeof (ArithmeticOperation_t3707246961), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2472 = { sizeof (Negation_t3355962509), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2473 = { sizeof (BoolOperation_t3116017511), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2474 = { sizeof (ReferencedTable_t2363768325)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2474[4] = 
{
	ReferencedTable_t2363768325::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2475 = { sizeof (ColumnReference_t717778461), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2475[5] = 
{
	ColumnReference_t717778461::get_offset_of_refTable_0(),
	ColumnReference_t717778461::get_offset_of_relationName_1(),
	ColumnReference_t717778461::get_offset_of_columnName_2(),
	ColumnReference_t717778461::get_offset_of__cachedColumn_3(),
	ColumnReference_t717778461::get_offset_of__cachedRelation_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2476 = { sizeof (Comparison_t3554387337), -1, sizeof(Comparison_t3554387337_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2476[1] = 
{
	Comparison_t3554387337_StaticFields::get_offset_of_IgnoredTrailingChars_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2477 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2478 = { sizeof (BaseExpression_t4067875947), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2479 = { sizeof (UnaryExpression_t2769986971), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2479[1] = 
{
	UnaryExpression_t2769986971::get_offset_of_expr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2480 = { sizeof (BinaryExpression_t4018232945), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2480[2] = 
{
	BinaryExpression_t4018232945::get_offset_of_expr1_0(),
	BinaryExpression_t4018232945::get_offset_of_expr2_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2481 = { sizeof (Operation_t2847614977)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2481[14] = 
{
	Operation_t2847614977::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2482 = { sizeof (BinaryOpExpression_t3781280966), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2482[1] = 
{
	BinaryOpExpression_t3781280966::get_offset_of_op_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2483 = { sizeof (IifFunction_t224081018), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2483[2] = 
{
	IifFunction_t224081018::get_offset_of_trueExpr_1(),
	IifFunction_t224081018::get_offset_of_falseExpr_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2484 = { sizeof (IsNullFunction_t156305379), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2484[1] = 
{
	IsNullFunction_t156305379::get_offset_of_defaultExpr_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2485 = { sizeof (ConvertFunction_t1592470909), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2485[1] = 
{
	ConvertFunction_t1592470909::get_offset_of_targetType_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2486 = { sizeof (In_t3546431515), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2486[1] = 
{
	In_t3546431515::get_offset_of_set_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2487 = { sizeof (Like_t1057370379), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2487[1] = 
{
	Like_t1057370379::get_offset_of__pattern_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2488 = { sizeof (Literal_t3735181871), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2488[1] = 
{
	Literal_t3735181871::get_offset_of_val_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2489 = { sizeof (Numeric_t3714709959), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2490 = { sizeof (StringFunction_t1721758303), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2491 = { sizeof (ConcatFunction_t815662470), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2491[1] = 
{
	ConcatFunction_t815662470::get_offset_of__add_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2492 = { sizeof (SubstringFunction_t927862047), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2492[2] = 
{
	SubstringFunction_t927862047::get_offset_of_start_1(),
	SubstringFunction_t927862047::get_offset_of_len_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2493 = { sizeof (LenFunction_t2004154325), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2494 = { sizeof (TrimFunction_t771403798), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2495 = { sizeof (Tokenizer_t3681895715), -1, sizeof(Tokenizer_t3681895715_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2495[6] = 
{
	Tokenizer_t3681895715_StaticFields::get_offset_of_tokenMap_0(),
	Tokenizer_t3681895715_StaticFields::get_offset_of_tokens_1(),
	Tokenizer_t3681895715::get_offset_of_input_2(),
	Tokenizer_t3681895715::get_offset_of_pos_3(),
	Tokenizer_t3681895715::get_offset_of_tok_4(),
	Tokenizer_t3681895715::get_offset_of_val_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2496 = { sizeof (CatalogLocation_t1149802810)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2496[3] = 
{
	CatalogLocation_t1149802810::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2497 = { sizeof (DataAdapter_t668329355), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2497[9] = 
{
	DataAdapter_t668329355::get_offset_of_acceptChangesDuringFill_4(),
	DataAdapter_t668329355::get_offset_of_continueUpdateOnError_5(),
	DataAdapter_t668329355::get_offset_of_missingMappingAction_6(),
	DataAdapter_t668329355::get_offset_of_missingSchemaAction_7(),
	DataAdapter_t668329355::get_offset_of_tableMappings_8(),
	DataAdapter_t668329355::get_offset_of_acceptChangesDuringUpdate_9(),
	DataAdapter_t668329355::get_offset_of_fillLoadOption_10(),
	DataAdapter_t668329355::get_offset_of_returnProviderSpecificTypes_11(),
	DataAdapter_t668329355::get_offset_of_FillError_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2498 = { sizeof (DataColumnMapping_t969655534), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2498[2] = 
{
	DataColumnMapping_t969655534::get_offset_of_sourceColumn_1(),
	DataColumnMapping_t969655534::get_offset_of_dataSetColumn_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2499 = { sizeof (DataColumnMappingCollection_t3025014470), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2499[3] = 
{
	DataColumnMappingCollection_t3025014470::get_offset_of_list_1(),
	DataColumnMappingCollection_t3025014470::get_offset_of_sourceColumns_2(),
	DataColumnMappingCollection_t3025014470::get_offset_of_dataSetColumns_3(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
