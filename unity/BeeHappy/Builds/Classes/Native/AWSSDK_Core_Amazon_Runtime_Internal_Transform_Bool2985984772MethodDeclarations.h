﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.Transform.BoolUnmarshaller
struct BoolUnmarshaller_t2985984772;
// Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext
struct XmlUnmarshallerContext_t1179575220;
// Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext
struct JsonUnmarshallerContext_t456235889;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_XmlU1179575220.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_JsonU456235889.h"

// System.Void Amazon.Runtime.Internal.Transform.BoolUnmarshaller::.ctor()
extern "C"  void BoolUnmarshaller__ctor_m1729531124 (BoolUnmarshaller_t2985984772 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.Internal.Transform.BoolUnmarshaller Amazon.Runtime.Internal.Transform.BoolUnmarshaller::get_Instance()
extern "C"  BoolUnmarshaller_t2985984772 * BoolUnmarshaller_get_Instance_m2062304728 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.Transform.BoolUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext)
extern "C"  bool BoolUnmarshaller_Unmarshall_m102142502 (BoolUnmarshaller_t2985984772 * __this, XmlUnmarshallerContext_t1179575220 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.Transform.BoolUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern "C"  bool BoolUnmarshaller_Unmarshall_m3293405337 (BoolUnmarshaller_t2985984772 * __this, JsonUnmarshallerContext_t456235889 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Transform.BoolUnmarshaller::.cctor()
extern "C"  void BoolUnmarshaller__cctor_m1766202823 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
