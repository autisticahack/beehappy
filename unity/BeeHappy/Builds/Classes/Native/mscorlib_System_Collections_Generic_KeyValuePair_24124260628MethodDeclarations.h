﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_888819835MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Collections.Generic.Dictionary`2<System.IntPtr,ThirdParty.iOS4Unity.Callbacks/Methods>>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m2777313720(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t4124260628 *, IntPtr_t, Dictionary_2_t1629922792 *, const MethodInfo*))KeyValuePair_2__ctor_m477426041_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Collections.Generic.Dictionary`2<System.IntPtr,ThirdParty.iOS4Unity.Callbacks/Methods>>::get_Key()
#define KeyValuePair_2_get_Key_m2918444330(__this, method) ((  IntPtr_t (*) (KeyValuePair_2_t4124260628 *, const MethodInfo*))KeyValuePair_2_get_Key_m1574332879_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Collections.Generic.Dictionary`2<System.IntPtr,ThirdParty.iOS4Unity.Callbacks/Methods>>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m842605269(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t4124260628 *, IntPtr_t, const MethodInfo*))KeyValuePair_2_set_Key_m4146602710_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Collections.Generic.Dictionary`2<System.IntPtr,ThirdParty.iOS4Unity.Callbacks/Methods>>::get_Value()
#define KeyValuePair_2_get_Value_m2298983018(__this, method) ((  Dictionary_2_t1629922792 * (*) (KeyValuePair_2_t4124260628 *, const MethodInfo*))KeyValuePair_2_get_Value_m544293807_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Collections.Generic.Dictionary`2<System.IntPtr,ThirdParty.iOS4Unity.Callbacks/Methods>>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m2472042141(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t4124260628 *, Dictionary_2_t1629922792 *, const MethodInfo*))KeyValuePair_2_set_Value_m1938889438_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Collections.Generic.Dictionary`2<System.IntPtr,ThirdParty.iOS4Unity.Callbacks/Methods>>::ToString()
#define KeyValuePair_2_ToString_m358548415(__this, method) ((  String_t* (*) (KeyValuePair_2_t4124260628 *, const MethodInfo*))KeyValuePair_2_ToString_m2882821022_gshared)(__this, method)
