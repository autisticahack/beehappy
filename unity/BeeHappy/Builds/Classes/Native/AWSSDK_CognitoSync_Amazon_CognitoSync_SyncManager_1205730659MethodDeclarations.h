﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.CognitoSync.SyncManager.DatasetMetadata
struct DatasetMetadata_t1205730659;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen3251239280.h"
#include "mscorlib_System_String2029220233.h"

// System.String Amazon.CognitoSync.SyncManager.DatasetMetadata::get_DatasetName()
extern "C"  String_t* DatasetMetadata_get_DatasetName_m245691898 (DatasetMetadata_t1205730659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.DateTime> Amazon.CognitoSync.SyncManager.DatasetMetadata::get_CreationDate()
extern "C"  Nullable_1_t3251239280  DatasetMetadata_get_CreationDate_m1570552555 (DatasetMetadata_t1205730659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.DateTime> Amazon.CognitoSync.SyncManager.DatasetMetadata::get_LastModifiedDate()
extern "C"  Nullable_1_t3251239280  DatasetMetadata_get_LastModifiedDate_m3024651429 (DatasetMetadata_t1205730659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.CognitoSync.SyncManager.DatasetMetadata::get_LastModifiedBy()
extern "C"  String_t* DatasetMetadata_get_LastModifiedBy_m282256147 (DatasetMetadata_t1205730659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Amazon.CognitoSync.SyncManager.DatasetMetadata::get_StorageSizeBytes()
extern "C"  int64_t DatasetMetadata_get_StorageSizeBytes_m2751265398 (DatasetMetadata_t1205730659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Amazon.CognitoSync.SyncManager.DatasetMetadata::get_RecordCount()
extern "C"  int64_t DatasetMetadata_get_RecordCount_m3097763341 (DatasetMetadata_t1205730659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.SyncManager.DatasetMetadata::.ctor(System.String,System.Nullable`1<System.DateTime>,System.Nullable`1<System.DateTime>,System.String,System.Int64,System.Int64)
extern "C"  void DatasetMetadata__ctor_m3022195267 (DatasetMetadata_t1205730659 * __this, String_t* ___datasetName0, Nullable_1_t3251239280  ___creationDate1, Nullable_1_t3251239280  ___lastModifiedDate2, String_t* ___lastModifiedBy3, int64_t ___storageSizeBytes4, int64_t ___recordCount5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.CognitoSync.SyncManager.DatasetMetadata::ToString()
extern "C"  String_t* DatasetMetadata_ToString_m724187280 (DatasetMetadata_t1205730659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
