﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String[]
struct StringU5BU5D_t1642385972;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage/RecordColumns
struct  RecordColumns_t4235253406  : public Il2CppObject
{
public:

public:
};

struct RecordColumns_t4235253406_StaticFields
{
public:
	// System.String[] Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage/RecordColumns::ALL
	StringU5BU5D_t1642385972* ___ALL_0;
	// System.Int32 Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage/RecordColumns::IDENTITY_ID_IDX
	int32_t ___IDENTITY_ID_IDX_1;
	// System.Int32 Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage/RecordColumns::DATASET_NAME_IDX
	int32_t ___DATASET_NAME_IDX_2;
	// System.Int32 Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage/RecordColumns::KEY_IDX
	int32_t ___KEY_IDX_3;
	// System.Int32 Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage/RecordColumns::VALUE_IDX
	int32_t ___VALUE_IDX_4;
	// System.Int32 Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage/RecordColumns::SYNC_COUNT_IDX
	int32_t ___SYNC_COUNT_IDX_5;
	// System.Int32 Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage/RecordColumns::LAST_MODIFIED_TIMESTAMP_IDX
	int32_t ___LAST_MODIFIED_TIMESTAMP_IDX_6;
	// System.Int32 Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage/RecordColumns::LAST_MODIFIED_BY_IDX
	int32_t ___LAST_MODIFIED_BY_IDX_7;
	// System.Int32 Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage/RecordColumns::DEVICE_LAST_MODIFIED_TIMESTAMP_IDX
	int32_t ___DEVICE_LAST_MODIFIED_TIMESTAMP_IDX_8;
	// System.Int32 Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage/RecordColumns::MODIFIED_IDX
	int32_t ___MODIFIED_IDX_9;

public:
	inline static int32_t get_offset_of_ALL_0() { return static_cast<int32_t>(offsetof(RecordColumns_t4235253406_StaticFields, ___ALL_0)); }
	inline StringU5BU5D_t1642385972* get_ALL_0() const { return ___ALL_0; }
	inline StringU5BU5D_t1642385972** get_address_of_ALL_0() { return &___ALL_0; }
	inline void set_ALL_0(StringU5BU5D_t1642385972* value)
	{
		___ALL_0 = value;
		Il2CppCodeGenWriteBarrier(&___ALL_0, value);
	}

	inline static int32_t get_offset_of_IDENTITY_ID_IDX_1() { return static_cast<int32_t>(offsetof(RecordColumns_t4235253406_StaticFields, ___IDENTITY_ID_IDX_1)); }
	inline int32_t get_IDENTITY_ID_IDX_1() const { return ___IDENTITY_ID_IDX_1; }
	inline int32_t* get_address_of_IDENTITY_ID_IDX_1() { return &___IDENTITY_ID_IDX_1; }
	inline void set_IDENTITY_ID_IDX_1(int32_t value)
	{
		___IDENTITY_ID_IDX_1 = value;
	}

	inline static int32_t get_offset_of_DATASET_NAME_IDX_2() { return static_cast<int32_t>(offsetof(RecordColumns_t4235253406_StaticFields, ___DATASET_NAME_IDX_2)); }
	inline int32_t get_DATASET_NAME_IDX_2() const { return ___DATASET_NAME_IDX_2; }
	inline int32_t* get_address_of_DATASET_NAME_IDX_2() { return &___DATASET_NAME_IDX_2; }
	inline void set_DATASET_NAME_IDX_2(int32_t value)
	{
		___DATASET_NAME_IDX_2 = value;
	}

	inline static int32_t get_offset_of_KEY_IDX_3() { return static_cast<int32_t>(offsetof(RecordColumns_t4235253406_StaticFields, ___KEY_IDX_3)); }
	inline int32_t get_KEY_IDX_3() const { return ___KEY_IDX_3; }
	inline int32_t* get_address_of_KEY_IDX_3() { return &___KEY_IDX_3; }
	inline void set_KEY_IDX_3(int32_t value)
	{
		___KEY_IDX_3 = value;
	}

	inline static int32_t get_offset_of_VALUE_IDX_4() { return static_cast<int32_t>(offsetof(RecordColumns_t4235253406_StaticFields, ___VALUE_IDX_4)); }
	inline int32_t get_VALUE_IDX_4() const { return ___VALUE_IDX_4; }
	inline int32_t* get_address_of_VALUE_IDX_4() { return &___VALUE_IDX_4; }
	inline void set_VALUE_IDX_4(int32_t value)
	{
		___VALUE_IDX_4 = value;
	}

	inline static int32_t get_offset_of_SYNC_COUNT_IDX_5() { return static_cast<int32_t>(offsetof(RecordColumns_t4235253406_StaticFields, ___SYNC_COUNT_IDX_5)); }
	inline int32_t get_SYNC_COUNT_IDX_5() const { return ___SYNC_COUNT_IDX_5; }
	inline int32_t* get_address_of_SYNC_COUNT_IDX_5() { return &___SYNC_COUNT_IDX_5; }
	inline void set_SYNC_COUNT_IDX_5(int32_t value)
	{
		___SYNC_COUNT_IDX_5 = value;
	}

	inline static int32_t get_offset_of_LAST_MODIFIED_TIMESTAMP_IDX_6() { return static_cast<int32_t>(offsetof(RecordColumns_t4235253406_StaticFields, ___LAST_MODIFIED_TIMESTAMP_IDX_6)); }
	inline int32_t get_LAST_MODIFIED_TIMESTAMP_IDX_6() const { return ___LAST_MODIFIED_TIMESTAMP_IDX_6; }
	inline int32_t* get_address_of_LAST_MODIFIED_TIMESTAMP_IDX_6() { return &___LAST_MODIFIED_TIMESTAMP_IDX_6; }
	inline void set_LAST_MODIFIED_TIMESTAMP_IDX_6(int32_t value)
	{
		___LAST_MODIFIED_TIMESTAMP_IDX_6 = value;
	}

	inline static int32_t get_offset_of_LAST_MODIFIED_BY_IDX_7() { return static_cast<int32_t>(offsetof(RecordColumns_t4235253406_StaticFields, ___LAST_MODIFIED_BY_IDX_7)); }
	inline int32_t get_LAST_MODIFIED_BY_IDX_7() const { return ___LAST_MODIFIED_BY_IDX_7; }
	inline int32_t* get_address_of_LAST_MODIFIED_BY_IDX_7() { return &___LAST_MODIFIED_BY_IDX_7; }
	inline void set_LAST_MODIFIED_BY_IDX_7(int32_t value)
	{
		___LAST_MODIFIED_BY_IDX_7 = value;
	}

	inline static int32_t get_offset_of_DEVICE_LAST_MODIFIED_TIMESTAMP_IDX_8() { return static_cast<int32_t>(offsetof(RecordColumns_t4235253406_StaticFields, ___DEVICE_LAST_MODIFIED_TIMESTAMP_IDX_8)); }
	inline int32_t get_DEVICE_LAST_MODIFIED_TIMESTAMP_IDX_8() const { return ___DEVICE_LAST_MODIFIED_TIMESTAMP_IDX_8; }
	inline int32_t* get_address_of_DEVICE_LAST_MODIFIED_TIMESTAMP_IDX_8() { return &___DEVICE_LAST_MODIFIED_TIMESTAMP_IDX_8; }
	inline void set_DEVICE_LAST_MODIFIED_TIMESTAMP_IDX_8(int32_t value)
	{
		___DEVICE_LAST_MODIFIED_TIMESTAMP_IDX_8 = value;
	}

	inline static int32_t get_offset_of_MODIFIED_IDX_9() { return static_cast<int32_t>(offsetof(RecordColumns_t4235253406_StaticFields, ___MODIFIED_IDX_9)); }
	inline int32_t get_MODIFIED_IDX_9() const { return ___MODIFIED_IDX_9; }
	inline int32_t* get_address_of_MODIFIED_IDX_9() { return &___MODIFIED_IDX_9; }
	inline void set_MODIFIED_IDX_9(int32_t value)
	{
		___MODIFIED_IDX_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
