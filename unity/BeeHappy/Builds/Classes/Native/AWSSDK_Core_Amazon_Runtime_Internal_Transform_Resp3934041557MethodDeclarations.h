﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.Transform.ResponseUnmarshaller
struct ResponseUnmarshaller_t3934041557;
// Amazon.Runtime.Internal.Transform.UnmarshallerContext
struct UnmarshallerContext_t1608322995;
// Amazon.Runtime.Internal.Transform.IWebResponseData
struct IWebResponseData_t3815728244;
// System.IO.Stream
struct Stream_t3255436806;
// Amazon.Runtime.Internal.Util.RequestMetrics
struct RequestMetrics_t218029284;
// Amazon.Runtime.AmazonServiceException
struct AmazonServiceException_t3748559634;
// System.Exception
struct Exception_t1927440687;
// Amazon.Runtime.AmazonWebServiceResponse
struct AmazonWebServiceResponse_t529043356;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_Stream3255436806.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Util_RequestMet218029284.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_Unma1608322995.h"
#include "mscorlib_System_Exception1927440687.h"
#include "System_System_Net_HttpStatusCode1898409641.h"

// Amazon.Runtime.Internal.Transform.UnmarshallerContext Amazon.Runtime.Internal.Transform.ResponseUnmarshaller::CreateContext(Amazon.Runtime.Internal.Transform.IWebResponseData,System.Boolean,System.IO.Stream,Amazon.Runtime.Internal.Util.RequestMetrics)
extern "C"  UnmarshallerContext_t1608322995 * ResponseUnmarshaller_CreateContext_m70630191 (ResponseUnmarshaller_t3934041557 * __this, Il2CppObject * ___response0, bool ___readEntireResponse1, Stream_t3255436806 * ___stream2, RequestMetrics_t218029284 * ___metrics3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.Transform.ResponseUnmarshaller::get_HasStreamingProperty()
extern "C"  bool ResponseUnmarshaller_get_HasStreamingProperty_m274588065 (ResponseUnmarshaller_t3934041557 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.AmazonServiceException Amazon.Runtime.Internal.Transform.ResponseUnmarshaller::UnmarshallException(Amazon.Runtime.Internal.Transform.UnmarshallerContext,System.Exception,System.Net.HttpStatusCode)
extern "C"  AmazonServiceException_t3748559634 * ResponseUnmarshaller_UnmarshallException_m4151636653 (ResponseUnmarshaller_t3934041557 * __this, UnmarshallerContext_t1608322995 * ___input0, Exception_t1927440687 * ___innerException1, int32_t ___statusCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.AmazonWebServiceResponse Amazon.Runtime.Internal.Transform.ResponseUnmarshaller::UnmarshallResponse(Amazon.Runtime.Internal.Transform.UnmarshallerContext)
extern "C"  AmazonWebServiceResponse_t529043356 * ResponseUnmarshaller_UnmarshallResponse_m793535050 (ResponseUnmarshaller_t3934041557 * __this, UnmarshallerContext_t1608322995 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.Transform.ResponseUnmarshaller::ShouldReadEntireResponse(Amazon.Runtime.Internal.Transform.IWebResponseData,System.Boolean)
extern "C"  bool ResponseUnmarshaller_ShouldReadEntireResponse_m1717610590 (ResponseUnmarshaller_t3934041557 * __this, Il2CppObject * ___response0, bool ___readEntireResponse1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Transform.ResponseUnmarshaller::.ctor()
extern "C"  void ResponseUnmarshaller__ctor_m273145413 (ResponseUnmarshaller_t3934041557 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
