﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.AWSSection
struct AWSSection_t3096528870;
// Amazon.LoggingSection
struct LoggingSection_t905443946;
// System.String
struct String_t;
// Amazon.ProxySection
struct ProxySection_t4280332351;
// System.Collections.Generic.IDictionary`2<System.String,System.Xml.Linq.XElement>
struct IDictionary_2_t467683733;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_LoggingSection905443946.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Nullable_1_gen2088641033.h"

// Amazon.LoggingSection Amazon.AWSSection::get_Logging()
extern "C"  LoggingSection_t905443946 * AWSSection_get_Logging_m1939664036 (AWSSection_t3096528870 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.AWSSection::set_Logging(Amazon.LoggingSection)
extern "C"  void AWSSection_set_Logging_m110835439 (AWSSection_t3096528870 * __this, LoggingSection_t905443946 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.AWSSection::get_EndpointDefinition()
extern "C"  String_t* AWSSection_get_EndpointDefinition_m1713426137 (AWSSection_t3096528870 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.AWSSection::get_Region()
extern "C"  String_t* AWSSection_get_Region_m3319904391 (AWSSection_t3096528870 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.AWSSection::set_Region(System.String)
extern "C"  void AWSSection_set_Region_m40787942 (AWSSection_t3096528870 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Boolean> Amazon.AWSSection::get_UseSdkCache()
extern "C"  Nullable_1_t2088641033  AWSSection_get_UseSdkCache_m1027853530 (AWSSection_t3096528870 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Boolean> Amazon.AWSSection::get_CorrectForClockSkew()
extern "C"  Nullable_1_t2088641033  AWSSection_get_CorrectForClockSkew_m3426173200 (AWSSection_t3096528870 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.AWSSection::set_CorrectForClockSkew(System.Nullable`1<System.Boolean>)
extern "C"  void AWSSection_set_CorrectForClockSkew_m3762904071 (AWSSection_t3096528870 * __this, Nullable_1_t2088641033  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.ProxySection Amazon.AWSSection::get_Proxy()
extern "C"  ProxySection_t4280332351 * AWSSection_get_Proxy_m1445462404 (AWSSection_t3096528870 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.AWSSection::get_ProfileName()
extern "C"  String_t* AWSSection_get_ProfileName_m322797395 (AWSSection_t3096528870 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.AWSSection::get_ProfilesLocation()
extern "C"  String_t* AWSSection_get_ProfilesLocation_m3780004198 (AWSSection_t3096528870 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.AWSSection::get_ApplicationName()
extern "C"  String_t* AWSSection_get_ApplicationName_m235275806 (AWSSection_t3096528870 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IDictionary`2<System.String,System.Xml.Linq.XElement> Amazon.AWSSection::get_ServiceSections()
extern "C"  Il2CppObject* AWSSection_get_ServiceSections_m1743280002 (AWSSection_t3096528870 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.AWSSection::set_ServiceSections(System.Collections.Generic.IDictionary`2<System.String,System.Xml.Linq.XElement>)
extern "C"  void AWSSection_set_ServiceSections_m546786511 (AWSSection_t3096528870 * __this, Il2CppObject* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.AWSSection::.ctor()
extern "C"  void AWSSection__ctor_m2511126035 (AWSSection_t3096528870 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
