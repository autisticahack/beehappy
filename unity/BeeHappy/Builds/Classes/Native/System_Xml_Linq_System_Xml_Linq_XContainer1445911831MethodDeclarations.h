﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.Linq.XContainer
struct XContainer_t1445911831;
// System.Xml.Linq.XNode
struct XNode_t2707504214;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerable`1<System.Xml.Linq.XNode>
struct IEnumerable_1_t2999631259;
// System.Collections.Generic.IEnumerable`1<System.Xml.Linq.XElement>
struct IEnumerable_1_t845948095;
// System.Xml.Linq.XName
struct XName_t785190363;
// System.Xml.Linq.XElement
struct XElement_t553821050;
// System.Xml.XmlReader
struct XmlReader_t3675626668;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "System_Xml_Linq_System_Xml_Linq_XNode2707504214.h"
#include "System_Xml_Linq_System_Xml_Linq_XName785190363.h"
#include "System_Xml_System_Xml_XmlReader3675626668.h"
#include "System_Xml_Linq_System_Xml_Linq_LoadOptions53198144.h"

// System.Void System.Xml.Linq.XContainer::.ctor()
extern "C"  void XContainer__ctor_m3187361404 (XContainer_t1445911831 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Linq.XNode System.Xml.Linq.XContainer::get_FirstNode()
extern "C"  XNode_t2707504214 * XContainer_get_FirstNode_m3344509478 (XContainer_t1445911831 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Linq.XContainer::CheckChildType(System.Object,System.Boolean)
extern "C"  void XContainer_CheckChildType_m2935453595 (XContainer_t1445911831 * __this, Il2CppObject * ___o0, bool ___addFirst1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Linq.XContainer::Add(System.Object)
extern "C"  void XContainer_Add_m3035239173 (XContainer_t1445911831 * __this, Il2CppObject * ___content0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Linq.XContainer::AddNode(System.Xml.Linq.XNode)
extern "C"  void XContainer_AddNode_m3593198507 (XContainer_t1445911831 * __this, XNode_t2707504214 * ___n0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.Linq.XContainer::OnAddingObject(System.Object,System.Boolean,System.Xml.Linq.XNode,System.Boolean)
extern "C"  bool XContainer_OnAddingObject_m4085827393 (XContainer_t1445911831 * __this, Il2CppObject * ___o0, bool ___rejectAttribute1, XNode_t2707504214 * ___refNode2, bool ___addFirst3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<System.Xml.Linq.XNode> System.Xml.Linq.XContainer::Nodes()
extern "C"  Il2CppObject* XContainer_Nodes_m1386197549 (XContainer_t1445911831 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<System.Xml.Linq.XElement> System.Xml.Linq.XContainer::Elements()
extern "C"  Il2CppObject* XContainer_Elements_m3349403213 (XContainer_t1445911831 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<System.Xml.Linq.XElement> System.Xml.Linq.XContainer::Elements(System.Xml.Linq.XName)
extern "C"  Il2CppObject* XContainer_Elements_m2291783576 (XContainer_t1445911831 * __this, XName_t785190363 * ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Linq.XElement System.Xml.Linq.XContainer::Element(System.Xml.Linq.XName)
extern "C"  XElement_t553821050 * XContainer_Element_m2629677368 (XContainer_t1445911831 * __this, XName_t785190363 * ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Linq.XContainer::ReadContentFrom(System.Xml.XmlReader,System.Xml.Linq.LoadOptions)
extern "C"  void XContainer_ReadContentFrom_m14040437 (XContainer_t1445911831 * __this, XmlReader_t3675626668 * ___reader0, int32_t ___options1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
