﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Text.RegularExpressions.Regex
struct Regex_t1803876613;

#include "AWSSDK_Core_Amazon_Runtime_Internal_Auth_AbstractA2114314031.h"
#include "AWSSDK_Core_Amazon_Runtime_SigningAlgorithm3740229458.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Auth.AWS4Signer
struct  AWS4Signer_t1011449585  : public AbstractAWSSigner_t2114314031
{
public:

public:
};

struct AWS4Signer_t1011449585_StaticFields
{
public:
	// System.Byte[] Amazon.Runtime.Internal.Auth.AWS4Signer::TerminatorBytes
	ByteU5BU5D_t3397334013* ___TerminatorBytes_5;
	// System.Text.RegularExpressions.Regex Amazon.Runtime.Internal.Auth.AWS4Signer::CompressWhitespaceRegex
	Regex_t1803876613 * ___CompressWhitespaceRegex_12;

public:
	inline static int32_t get_offset_of_TerminatorBytes_5() { return static_cast<int32_t>(offsetof(AWS4Signer_t1011449585_StaticFields, ___TerminatorBytes_5)); }
	inline ByteU5BU5D_t3397334013* get_TerminatorBytes_5() const { return ___TerminatorBytes_5; }
	inline ByteU5BU5D_t3397334013** get_address_of_TerminatorBytes_5() { return &___TerminatorBytes_5; }
	inline void set_TerminatorBytes_5(ByteU5BU5D_t3397334013* value)
	{
		___TerminatorBytes_5 = value;
		Il2CppCodeGenWriteBarrier(&___TerminatorBytes_5, value);
	}

	inline static int32_t get_offset_of_CompressWhitespaceRegex_12() { return static_cast<int32_t>(offsetof(AWS4Signer_t1011449585_StaticFields, ___CompressWhitespaceRegex_12)); }
	inline Regex_t1803876613 * get_CompressWhitespaceRegex_12() const { return ___CompressWhitespaceRegex_12; }
	inline Regex_t1803876613 ** get_address_of_CompressWhitespaceRegex_12() { return &___CompressWhitespaceRegex_12; }
	inline void set_CompressWhitespaceRegex_12(Regex_t1803876613 * value)
	{
		___CompressWhitespaceRegex_12 = value;
		Il2CppCodeGenWriteBarrier(&___CompressWhitespaceRegex_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
