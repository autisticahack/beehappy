﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.SecurityToken.Model.AssumedRoleUser
struct  AssumedRoleUser_t150458319  : public Il2CppObject
{
public:
	// System.String Amazon.SecurityToken.Model.AssumedRoleUser::_arn
	String_t* ____arn_0;
	// System.String Amazon.SecurityToken.Model.AssumedRoleUser::_assumedRoleId
	String_t* ____assumedRoleId_1;

public:
	inline static int32_t get_offset_of__arn_0() { return static_cast<int32_t>(offsetof(AssumedRoleUser_t150458319, ____arn_0)); }
	inline String_t* get__arn_0() const { return ____arn_0; }
	inline String_t** get_address_of__arn_0() { return &____arn_0; }
	inline void set__arn_0(String_t* value)
	{
		____arn_0 = value;
		Il2CppCodeGenWriteBarrier(&____arn_0, value);
	}

	inline static int32_t get_offset_of__assumedRoleId_1() { return static_cast<int32_t>(offsetof(AssumedRoleUser_t150458319, ____assumedRoleId_1)); }
	inline String_t* get__assumedRoleId_1() const { return ____assumedRoleId_1; }
	inline String_t** get_address_of__assumedRoleId_1() { return &____assumedRoleId_1; }
	inline void set__assumedRoleId_1(String_t* value)
	{
		____assumedRoleId_1 = value;
		Il2CppCodeGenWriteBarrier(&____assumedRoleId_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
