﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.SecurityToken.AmazonSecurityTokenServiceRequest
struct AmazonSecurityTokenServiceRequest_t265680841;

#include "codegen/il2cpp-codegen.h"

// System.Void Amazon.SecurityToken.AmazonSecurityTokenServiceRequest::.ctor()
extern "C"  void AmazonSecurityTokenServiceRequest__ctor_m2827735933 (AmazonSecurityTokenServiceRequest_t265680841 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
