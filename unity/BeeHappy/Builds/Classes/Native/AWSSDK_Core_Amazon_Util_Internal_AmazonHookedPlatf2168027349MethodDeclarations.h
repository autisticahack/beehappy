﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Util.Internal.AmazonHookedPlatformInfo
struct AmazonHookedPlatformInfo_t2168027349;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Amazon.Util.Internal.AmazonHookedPlatformInfo::.ctor()
extern "C"  void AmazonHookedPlatformInfo__ctor_m942411997 (AmazonHookedPlatformInfo_t2168027349 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Util.Internal.AmazonHookedPlatformInfo::set_Platform(System.String)
extern "C"  void AmazonHookedPlatformInfo_set_Platform_m3689878689 (AmazonHookedPlatformInfo_t2168027349 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Util.Internal.AmazonHookedPlatformInfo::set_Model(System.String)
extern "C"  void AmazonHookedPlatformInfo_set_Model_m1475274153 (AmazonHookedPlatformInfo_t2168027349 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Util.Internal.AmazonHookedPlatformInfo::set_Make(System.String)
extern "C"  void AmazonHookedPlatformInfo_set_Make_m2027500126 (AmazonHookedPlatformInfo_t2168027349 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Util.Internal.AmazonHookedPlatformInfo::set_PlatformVersion(System.String)
extern "C"  void AmazonHookedPlatformInfo_set_PlatformVersion_m3726486327 (AmazonHookedPlatformInfo_t2168027349 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Util.Internal.AmazonHookedPlatformInfo::get_PersistentDataPath()
extern "C"  String_t* AmazonHookedPlatformInfo_get_PersistentDataPath_m486582889 (AmazonHookedPlatformInfo_t2168027349 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Util.Internal.AmazonHookedPlatformInfo::set_PersistentDataPath(System.String)
extern "C"  void AmazonHookedPlatformInfo_set_PersistentDataPath_m734238394 (AmazonHookedPlatformInfo_t2168027349 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Util.Internal.AmazonHookedPlatformInfo::set_UnityVersion(System.String)
extern "C"  void AmazonHookedPlatformInfo_set_UnityVersion_m967183819 (AmazonHookedPlatformInfo_t2168027349 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Util.Internal.AmazonHookedPlatformInfo::set_Locale(System.String)
extern "C"  void AmazonHookedPlatformInfo_set_Locale_m402209818 (AmazonHookedPlatformInfo_t2168027349 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Util.Internal.AmazonHookedPlatformInfo Amazon.Util.Internal.AmazonHookedPlatformInfo::get_Instance()
extern "C"  AmazonHookedPlatformInfo_t2168027349 * AmazonHookedPlatformInfo_get_Instance_m3721437602 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Util.Internal.AmazonHookedPlatformInfo::get_PackageName()
extern "C"  String_t* AmazonHookedPlatformInfo_get_PackageName_m1628115118 (AmazonHookedPlatformInfo_t2168027349 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Util.Internal.AmazonHookedPlatformInfo::set_PackageName(System.String)
extern "C"  void AmazonHookedPlatformInfo_set_PackageName_m1736745299 (AmazonHookedPlatformInfo_t2168027349 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Util.Internal.AmazonHookedPlatformInfo::set_VersionName(System.String)
extern "C"  void AmazonHookedPlatformInfo_set_VersionName_m978811459 (AmazonHookedPlatformInfo_t2168027349 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Util.Internal.AmazonHookedPlatformInfo::set_VersionCode(System.String)
extern "C"  void AmazonHookedPlatformInfo_set_VersionCode_m440848497 (AmazonHookedPlatformInfo_t2168027349 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Util.Internal.AmazonHookedPlatformInfo::set_Title(System.String)
extern "C"  void AmazonHookedPlatformInfo_set_Title_m3688668152 (AmazonHookedPlatformInfo_t2168027349 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Util.Internal.AmazonHookedPlatformInfo::Init()
extern "C"  void AmazonHookedPlatformInfo_Init_m2352932627 (AmazonHookedPlatformInfo_t2168027349 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Util.Internal.AmazonHookedPlatformInfo::.cctor()
extern "C"  void AmazonHookedPlatformInfo__cctor_m394902158 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
