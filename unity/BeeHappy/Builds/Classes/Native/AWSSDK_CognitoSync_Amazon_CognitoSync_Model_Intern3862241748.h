﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.CognitoSync.Model.Internal.MarshallTransformations.DatasetUnmarshaller
struct DatasetUnmarshaller_t3862241748;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.CognitoSync.Model.Internal.MarshallTransformations.DatasetUnmarshaller
struct  DatasetUnmarshaller_t3862241748  : public Il2CppObject
{
public:

public:
};

struct DatasetUnmarshaller_t3862241748_StaticFields
{
public:
	// Amazon.CognitoSync.Model.Internal.MarshallTransformations.DatasetUnmarshaller Amazon.CognitoSync.Model.Internal.MarshallTransformations.DatasetUnmarshaller::_instance
	DatasetUnmarshaller_t3862241748 * ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(DatasetUnmarshaller_t3862241748_StaticFields, ____instance_0)); }
	inline DatasetUnmarshaller_t3862241748 * get__instance_0() const { return ____instance_0; }
	inline DatasetUnmarshaller_t3862241748 ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(DatasetUnmarshaller_t3862241748 * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier(&____instance_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
