﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.SecurityToken.AmazonSecurityTokenServiceClient/<>c__DisplayClass16_0
struct U3CU3Ec__DisplayClass16_0_t2700714712;
// Amazon.Runtime.AmazonWebServiceRequest
struct AmazonWebServiceRequest_t3384026212;
// Amazon.Runtime.AmazonWebServiceResponse
struct AmazonWebServiceResponse_t529043356;
// System.Exception
struct Exception_t1927440687;
// Amazon.Runtime.AsyncOptions
struct AsyncOptions_t558351272;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_Runtime_AmazonWebServiceRequest3384026212.h"
#include "AWSSDK_Core_Amazon_Runtime_AmazonWebServiceResponse529043356.h"
#include "mscorlib_System_Exception1927440687.h"
#include "AWSSDK_Core_Amazon_Runtime_AsyncOptions558351272.h"

// System.Void Amazon.SecurityToken.AmazonSecurityTokenServiceClient/<>c__DisplayClass16_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass16_0__ctor_m349328331 (U3CU3Ec__DisplayClass16_0_t2700714712 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.SecurityToken.AmazonSecurityTokenServiceClient/<>c__DisplayClass16_0::<AssumeRoleWithWebIdentityAsync>b__0(Amazon.Runtime.AmazonWebServiceRequest,Amazon.Runtime.AmazonWebServiceResponse,System.Exception,Amazon.Runtime.AsyncOptions)
extern "C"  void U3CU3Ec__DisplayClass16_0_U3CAssumeRoleWithWebIdentityAsyncU3Eb__0_m832857059 (U3CU3Ec__DisplayClass16_0_t2700714712 * __this, AmazonWebServiceRequest_t3384026212 * ___req0, AmazonWebServiceResponse_t529043356 * ___res1, Exception_t1927440687 * ___ex2, AsyncOptions_t558351272 * ___ao3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
