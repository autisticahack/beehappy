﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.SecurityToken.Model.AssumedRoleUser
struct AssumedRoleUser_t150458319;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Amazon.SecurityToken.Model.AssumedRoleUser::set_Arn(System.String)
extern "C"  void AssumedRoleUser_set_Arn_m1960284340 (AssumedRoleUser_t150458319 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.SecurityToken.Model.AssumedRoleUser::set_AssumedRoleId(System.String)
extern "C"  void AssumedRoleUser_set_AssumedRoleId_m2821507572 (AssumedRoleUser_t150458319 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.SecurityToken.Model.AssumedRoleUser::.ctor()
extern "C"  void AssumedRoleUser__ctor_m2660329274 (AssumedRoleUser_t150458319 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
