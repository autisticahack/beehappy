﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t3614634134;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage/Statement
struct  Statement_t3708524595  : public Il2CppObject
{
public:
	// System.String Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage/Statement::<Query>k__BackingField
	String_t* ___U3CQueryU3Ek__BackingField_0;
	// System.Object[] Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage/Statement::<Parameters>k__BackingField
	ObjectU5BU5D_t3614634134* ___U3CParametersU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CQueryU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Statement_t3708524595, ___U3CQueryU3Ek__BackingField_0)); }
	inline String_t* get_U3CQueryU3Ek__BackingField_0() const { return ___U3CQueryU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CQueryU3Ek__BackingField_0() { return &___U3CQueryU3Ek__BackingField_0; }
	inline void set_U3CQueryU3Ek__BackingField_0(String_t* value)
	{
		___U3CQueryU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CQueryU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CParametersU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Statement_t3708524595, ___U3CParametersU3Ek__BackingField_1)); }
	inline ObjectU5BU5D_t3614634134* get_U3CParametersU3Ek__BackingField_1() const { return ___U3CParametersU3Ek__BackingField_1; }
	inline ObjectU5BU5D_t3614634134** get_address_of_U3CParametersU3Ek__BackingField_1() { return &___U3CParametersU3Ek__BackingField_1; }
	inline void set_U3CParametersU3Ek__BackingField_1(ObjectU5BU5D_t3614634134* value)
	{
		___U3CParametersU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CParametersU3Ek__BackingField_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
