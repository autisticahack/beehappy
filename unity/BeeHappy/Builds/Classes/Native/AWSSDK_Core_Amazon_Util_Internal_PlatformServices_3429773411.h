﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.Util.Storage.KVStore
struct KVStore_t2216900448;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Util.Internal.PlatformServices.ApplicationSettings
struct  ApplicationSettings_t3429773411  : public Il2CppObject
{
public:
	// Amazon.Util.Storage.KVStore Amazon.Util.Internal.PlatformServices.ApplicationSettings::kvStore
	KVStore_t2216900448 * ___kvStore_0;

public:
	inline static int32_t get_offset_of_kvStore_0() { return static_cast<int32_t>(offsetof(ApplicationSettings_t3429773411, ___kvStore_0)); }
	inline KVStore_t2216900448 * get_kvStore_0() const { return ___kvStore_0; }
	inline KVStore_t2216900448 ** get_address_of_kvStore_0() { return &___kvStore_0; }
	inline void set_kvStore_0(KVStore_t2216900448 * value)
	{
		___kvStore_0 = value;
		Il2CppCodeGenWriteBarrier(&___kvStore_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
