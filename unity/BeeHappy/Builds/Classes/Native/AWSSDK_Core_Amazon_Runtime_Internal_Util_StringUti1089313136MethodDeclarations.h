﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.String Amazon.Runtime.Internal.Util.StringUtils::FromString(System.String)
extern "C"  String_t* StringUtils_FromString_m1100270676 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Runtime.Internal.Util.StringUtils::FromInt(System.Int32)
extern "C"  String_t* StringUtils_FromInt_m146443913 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Runtime.Internal.Util.StringUtils::FromLong(System.Int64)
extern "C"  String_t* StringUtils_FromLong_m422878927 (Il2CppObject * __this /* static, unused */, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.StringUtils::.cctor()
extern "C"  void StringUtils__cctor_m4229270571 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
