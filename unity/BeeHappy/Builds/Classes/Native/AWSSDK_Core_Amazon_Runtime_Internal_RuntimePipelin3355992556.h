﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.Runtime.Internal.Util.ILogger
struct ILogger_t676853571;
// Amazon.Runtime.IPipelineHandler
struct IPipelineHandler_t2320756001;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.RuntimePipeline
struct  RuntimePipeline_t3355992556  : public Il2CppObject
{
public:
	// System.Boolean Amazon.Runtime.Internal.RuntimePipeline::_disposed
	bool ____disposed_0;
	// Amazon.Runtime.Internal.Util.ILogger Amazon.Runtime.Internal.RuntimePipeline::_logger
	Il2CppObject * ____logger_1;
	// Amazon.Runtime.IPipelineHandler Amazon.Runtime.Internal.RuntimePipeline::_handler
	Il2CppObject * ____handler_2;

public:
	inline static int32_t get_offset_of__disposed_0() { return static_cast<int32_t>(offsetof(RuntimePipeline_t3355992556, ____disposed_0)); }
	inline bool get__disposed_0() const { return ____disposed_0; }
	inline bool* get_address_of__disposed_0() { return &____disposed_0; }
	inline void set__disposed_0(bool value)
	{
		____disposed_0 = value;
	}

	inline static int32_t get_offset_of__logger_1() { return static_cast<int32_t>(offsetof(RuntimePipeline_t3355992556, ____logger_1)); }
	inline Il2CppObject * get__logger_1() const { return ____logger_1; }
	inline Il2CppObject ** get_address_of__logger_1() { return &____logger_1; }
	inline void set__logger_1(Il2CppObject * value)
	{
		____logger_1 = value;
		Il2CppCodeGenWriteBarrier(&____logger_1, value);
	}

	inline static int32_t get_offset_of__handler_2() { return static_cast<int32_t>(offsetof(RuntimePipeline_t3355992556, ____handler_2)); }
	inline Il2CppObject * get__handler_2() const { return ____handler_2; }
	inline Il2CppObject ** get_address_of__handler_2() { return &____handler_2; }
	inline void set__handler_2(Il2CppObject * value)
	{
		____handler_2 = value;
		Il2CppCodeGenWriteBarrier(&____handler_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
