﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.DefaultRequest/<>c
struct U3CU3Ec_t12014549;
// System.IO.Stream
struct Stream_t3255436806;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_Stream3255436806.h"

// System.Void Amazon.Runtime.Internal.DefaultRequest/<>c::.cctor()
extern "C"  void U3CU3Ec__cctor_m1065603523 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.DefaultRequest/<>c::.ctor()
extern "C"  void U3CU3Ec__ctor_m2903729494 (U3CU3Ec_t12014549 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.DefaultRequest/<>c::<ComputeContentStreamHash>b__57_0(System.IO.Stream)
extern "C"  bool U3CU3Ec_U3CComputeContentStreamHashU3Eb__57_0_m2241776848 (U3CU3Ec_t12014549 * __this, Stream_t3255436806 * ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
