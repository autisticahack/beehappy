﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>
struct DefaultComparer_t1076550313;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23749587448.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::.ctor()
extern "C"  void DefaultComparer__ctor_m2484660672_gshared (DefaultComparer_t1076550313 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m2484660672(__this, method) ((  void (*) (DefaultComparer_t1076550313 *, const MethodInfo*))DefaultComparer__ctor_m2484660672_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m1697897969_gshared (DefaultComparer_t1076550313 * __this, KeyValuePair_2_t3749587448  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m1697897969(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t1076550313 *, KeyValuePair_2_t3749587448 , const MethodInfo*))DefaultComparer_GetHashCode_m1697897969_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m3058908121_gshared (DefaultComparer_t1076550313 * __this, KeyValuePair_2_t3749587448  ___x0, KeyValuePair_2_t3749587448  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m3058908121(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t1076550313 *, KeyValuePair_2_t3749587448 , KeyValuePair_2_t3749587448 , const MethodInfo*))DefaultComparer_Equals_m3058908121_gshared)(__this, ___x0, ___y1, method)
