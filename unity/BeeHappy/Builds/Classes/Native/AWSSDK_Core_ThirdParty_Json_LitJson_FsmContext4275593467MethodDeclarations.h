﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ThirdParty.Json.LitJson.FsmContext
struct FsmContext_t4275593467;

#include "codegen/il2cpp-codegen.h"

// System.Void ThirdParty.Json.LitJson.FsmContext::.ctor()
extern "C"  void FsmContext__ctor_m1039323702 (FsmContext_t4275593467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
