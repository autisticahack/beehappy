﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AWSSDK_Core_Amazon_Runtime_Internal_CredentialsRetr208022274.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.CognitoSync.Internal.CognitoCredentialsRetriever
struct  CognitoCredentialsRetriever_t1718611005  : public CredentialsRetriever_t208022274
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
