﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.SecurityToken.AmazonSecurityTokenServiceConfig
struct AmazonSecurityTokenServiceConfig_t1261559370;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void Amazon.SecurityToken.AmazonSecurityTokenServiceConfig::.ctor()
extern "C"  void AmazonSecurityTokenServiceConfig__ctor_m2978057874 (AmazonSecurityTokenServiceConfig_t1261559370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.SecurityToken.AmazonSecurityTokenServiceConfig::get_RegionEndpointServiceName()
extern "C"  String_t* AmazonSecurityTokenServiceConfig_get_RegionEndpointServiceName_m2944299163 (AmazonSecurityTokenServiceConfig_t1261559370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.SecurityToken.AmazonSecurityTokenServiceConfig::get_UserAgent()
extern "C"  String_t* AmazonSecurityTokenServiceConfig_get_UserAgent_m3797224716 (AmazonSecurityTokenServiceConfig_t1261559370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.SecurityToken.AmazonSecurityTokenServiceConfig::.cctor()
extern "C"  void AmazonSecurityTokenServiceConfig__cctor_m1950723557 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
