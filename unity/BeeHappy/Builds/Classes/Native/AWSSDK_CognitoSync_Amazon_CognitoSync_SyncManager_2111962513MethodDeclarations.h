﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.CognitoSync.SyncManager.Dataset/<>c__DisplayClass73_0
struct U3CU3Ec__DisplayClass73_0_t2111962513;

#include "codegen/il2cpp-codegen.h"

// System.Void Amazon.CognitoSync.SyncManager.Dataset/<>c__DisplayClass73_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass73_0__ctor_m2783133163 (U3CU3Ec__DisplayClass73_0_t2111962513 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.SyncManager.Dataset/<>c__DisplayClass73_0::<FireSyncFailureEvent>b__0()
extern "C"  void U3CU3Ec__DisplayClass73_0_U3CFireSyncFailureEventU3Eb__0_m2071295320 (U3CU3Ec__DisplayClass73_0_t2111962513 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
