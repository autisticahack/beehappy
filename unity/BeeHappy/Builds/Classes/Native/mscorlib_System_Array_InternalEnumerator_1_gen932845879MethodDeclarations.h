﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen932845879.h"
#include "mscorlib_System_Array3829468939.h"
#include "System_Core_System_Collections_Generic_HashSet_1_Lin74093617.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Net.HttpStatusCode>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1459878407_gshared (InternalEnumerator_1_t932845879 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1459878407(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t932845879 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1459878407_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Net.HttpStatusCode>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2095617287_gshared (InternalEnumerator_1_t932845879 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2095617287(__this, method) ((  void (*) (InternalEnumerator_1_t932845879 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2095617287_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Net.HttpStatusCode>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2457494723_gshared (InternalEnumerator_1_t932845879 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2457494723(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t932845879 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2457494723_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Net.HttpStatusCode>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1176044390_gshared (InternalEnumerator_1_t932845879 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1176044390(__this, method) ((  void (*) (InternalEnumerator_1_t932845879 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1176044390_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Net.HttpStatusCode>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3206522979_gshared (InternalEnumerator_1_t932845879 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m3206522979(__this, method) ((  bool (*) (InternalEnumerator_1_t932845879 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3206522979_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Net.HttpStatusCode>>::get_Current()
extern "C"  Link_t74093617  InternalEnumerator_1_get_Current_m3187096104_gshared (InternalEnumerator_1_t932845879 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m3187096104(__this, method) ((  Link_t74093617  (*) (InternalEnumerator_1_t932845879 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3187096104_gshared)(__this, method)
