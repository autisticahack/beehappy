﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.Util.AlwaysSendList`1<System.Object>
struct AlwaysSendList_1_t3890205612;

#include "codegen/il2cpp-codegen.h"

// System.Void Amazon.Runtime.Internal.Util.AlwaysSendList`1<System.Object>::.ctor()
extern "C"  void AlwaysSendList_1__ctor_m2594435730_gshared (AlwaysSendList_1_t3890205612 * __this, const MethodInfo* method);
#define AlwaysSendList_1__ctor_m2594435730(__this, method) ((  void (*) (AlwaysSendList_1_t3890205612 *, const MethodInfo*))AlwaysSendList_1__ctor_m2594435730_gshared)(__this, method)
