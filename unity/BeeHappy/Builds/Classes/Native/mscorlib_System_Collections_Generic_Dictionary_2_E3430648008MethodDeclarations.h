﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3601534125MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,ThirdParty.Json.LitJson.ExporterFunc>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m2906856635(__this, ___dictionary0, method) ((  void (*) (Enumerator_t3430648008 *, Dictionary_2_t2110623306 *, const MethodInfo*))Enumerator__ctor_m3742107451_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,ThirdParty.Json.LitJson.ExporterFunc>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2086787578(__this, method) ((  Il2CppObject * (*) (Enumerator_t3430648008 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m229223308_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,ThirdParty.Json.LitJson.ExporterFunc>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m593130244(__this, method) ((  void (*) (Enumerator_t3430648008 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3225937576_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Type,ThirdParty.Json.LitJson.ExporterFunc>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3768947589(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t3430648008 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m221119093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,ThirdParty.Json.LitJson.ExporterFunc>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m713701984(__this, method) ((  Il2CppObject * (*) (Enumerator_t3430648008 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m467957770_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,ThirdParty.Json.LitJson.ExporterFunc>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m464504314(__this, method) ((  Il2CppObject * (*) (Enumerator_t3430648008 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2325383168_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Type,ThirdParty.Json.LitJson.ExporterFunc>::MoveNext()
#define Enumerator_MoveNext_m1543987644(__this, method) ((  bool (*) (Enumerator_t3430648008 *, const MethodInfo*))Enumerator_MoveNext_m3349738440_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Type,ThirdParty.Json.LitJson.ExporterFunc>::get_Current()
#define Enumerator_get_Current_m2683294492(__this, method) ((  KeyValuePair_2_t4162935824  (*) (Enumerator_t3430648008 *, const MethodInfo*))Enumerator_get_Current_m25299632_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Type,ThirdParty.Json.LitJson.ExporterFunc>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m129995343(__this, method) ((  Type_t * (*) (Enumerator_t3430648008 *, const MethodInfo*))Enumerator_get_CurrentKey_m3839846791_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Type,ThirdParty.Json.LitJson.ExporterFunc>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m2250953255(__this, method) ((  ExporterFunc_t173265409 * (*) (Enumerator_t3430648008 *, const MethodInfo*))Enumerator_get_CurrentValue_m402763047_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,ThirdParty.Json.LitJson.ExporterFunc>::Reset()
#define Enumerator_Reset_m138647837(__this, method) ((  void (*) (Enumerator_t3430648008 *, const MethodInfo*))Enumerator_Reset_m3129803197_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,ThirdParty.Json.LitJson.ExporterFunc>::VerifyState()
#define Enumerator_VerifyState_m2482327376(__this, method) ((  void (*) (Enumerator_t3430648008 *, const MethodInfo*))Enumerator_VerifyState_m262343092_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,ThirdParty.Json.LitJson.ExporterFunc>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m718443988(__this, method) ((  void (*) (Enumerator_t3430648008 *, const MethodInfo*))Enumerator_VerifyCurrent_m1702320752_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,ThirdParty.Json.LitJson.ExporterFunc>::Dispose()
#define Enumerator_Dispose_m2773083711(__this, method) ((  void (*) (Enumerator_t3430648008 *, const MethodInfo*))Enumerator_Dispose_m1905011127_gshared)(__this, method)
