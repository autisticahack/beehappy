﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.Util.InternalConsoleLogger
struct InternalConsoleLogger_t4018195642;
// System.Type
struct Type_t;
// System.Exception
struct Exception_t1927440687;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t3614634134;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_Exception1927440687.h"
#include "mscorlib_System_String2029220233.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Util_InternalCo527408354.h"

// System.Void Amazon.Runtime.Internal.Util.InternalConsoleLogger::.ctor(System.Type)
extern "C"  void InternalConsoleLogger__ctor_m1982387485 (InternalConsoleLogger_t4018195642 * __this, Type_t * ___declaringType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.InternalConsoleLogger::Error(System.Exception,System.String,System.Object[])
extern "C"  void InternalConsoleLogger_Error_m258237678 (InternalConsoleLogger_t4018195642 * __this, Exception_t1927440687 * ___exception0, String_t* ___messageFormat1, ObjectU5BU5D_t3614634134* ___args2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.InternalConsoleLogger::Debug(System.Exception,System.String,System.Object[])
extern "C"  void InternalConsoleLogger_Debug_m3121955689 (InternalConsoleLogger_t4018195642 * __this, Exception_t1927440687 * ___exception0, String_t* ___messageFormat1, ObjectU5BU5D_t3614634134* ___args2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.InternalConsoleLogger::DebugFormat(System.String,System.Object[])
extern "C"  void InternalConsoleLogger_DebugFormat_m2388185488 (InternalConsoleLogger_t4018195642 * __this, String_t* ___message0, ObjectU5BU5D_t3614634134* ___arguments1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.InternalConsoleLogger::InfoFormat(System.String,System.Object[])
extern "C"  void InternalConsoleLogger_InfoFormat_m569183193 (InternalConsoleLogger_t4018195642 * __this, String_t* ___message0, ObjectU5BU5D_t3614634134* ___arguments1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.InternalConsoleLogger::Log(Amazon.Runtime.Internal.Util.InternalConsoleLogger/LogLevel,System.String,System.Exception)
extern "C"  void InternalConsoleLogger_Log_m1794810306 (InternalConsoleLogger_t4018195642 * __this, int32_t ___logLevel0, String_t* ___message1, Exception_t1927440687 * ___ex2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
