﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Util.Internal.PlatformServices.NetworkStatusEventArgs
struct NetworkStatusEventArgs_t1607167653;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_Util_Internal_PlatformServices_N879095062.h"

// Amazon.Util.Internal.PlatformServices.NetworkStatus Amazon.Util.Internal.PlatformServices.NetworkStatusEventArgs::get_Status()
extern "C"  int32_t NetworkStatusEventArgs_get_Status_m3693116788 (NetworkStatusEventArgs_t1607167653 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Util.Internal.PlatformServices.NetworkStatusEventArgs::set_Status(Amazon.Util.Internal.PlatformServices.NetworkStatus)
extern "C"  void NetworkStatusEventArgs_set_Status_m3766613865 (NetworkStatusEventArgs_t1607167653 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Util.Internal.PlatformServices.NetworkStatusEventArgs::.ctor(Amazon.Util.Internal.PlatformServices.NetworkStatus)
extern "C"  void NetworkStatusEventArgs__ctor_m807563994 (NetworkStatusEventArgs_t1607167653 * __this, int32_t ___status0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
