﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Util.Storage.Internal.NetworkInfo/<>c__DisplayClass1_0
struct U3CU3Ec__DisplayClass1_0_t2945196168;

#include "codegen/il2cpp-codegen.h"

// System.Void Amazon.Util.Storage.Internal.NetworkInfo/<>c__DisplayClass1_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass1_0__ctor_m2472666587 (U3CU3Ec__DisplayClass1_0_t2945196168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Util.Storage.Internal.NetworkInfo/<>c__DisplayClass1_0::<get_Reachability>b__0()
extern "C"  void U3CU3Ec__DisplayClass1_0_U3Cget_ReachabilityU3Eb__0_m2955861445 (U3CU3Ec__DisplayClass1_0_t2945196168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
