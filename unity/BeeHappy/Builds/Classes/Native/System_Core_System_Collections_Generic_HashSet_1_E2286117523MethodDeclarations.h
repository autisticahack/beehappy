﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.HashSet`1<System.Net.WebExceptionStatus>
struct HashSet_1_t3797801681;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Collections_Generic_HashSet_1_E2286117523.h"
#include "System_System_Net_WebExceptionStatus1169373531.h"

// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Net.WebExceptionStatus>::.ctor(System.Collections.Generic.HashSet`1<T>)
extern "C"  void Enumerator__ctor_m2443584685_gshared (Enumerator_t2286117523 * __this, HashSet_1_t3797801681 * ___hashset0, const MethodInfo* method);
#define Enumerator__ctor_m2443584685(__this, ___hashset0, method) ((  void (*) (Enumerator_t2286117523 *, HashSet_1_t3797801681 *, const MethodInfo*))Enumerator__ctor_m2443584685_gshared)(__this, ___hashset0, method)
// System.Object System.Collections.Generic.HashSet`1/Enumerator<System.Net.WebExceptionStatus>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m661749011_gshared (Enumerator_t2286117523 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m661749011(__this, method) ((  Il2CppObject * (*) (Enumerator_t2286117523 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m661749011_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Net.WebExceptionStatus>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1901122971_gshared (Enumerator_t2286117523 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1901122971(__this, method) ((  void (*) (Enumerator_t2286117523 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1901122971_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1/Enumerator<System.Net.WebExceptionStatus>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3889495051_gshared (Enumerator_t2286117523 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3889495051(__this, method) ((  bool (*) (Enumerator_t2286117523 *, const MethodInfo*))Enumerator_MoveNext_m3889495051_gshared)(__this, method)
// T System.Collections.Generic.HashSet`1/Enumerator<System.Net.WebExceptionStatus>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m3395339702_gshared (Enumerator_t2286117523 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3395339702(__this, method) ((  int32_t (*) (Enumerator_t2286117523 *, const MethodInfo*))Enumerator_get_Current_m3395339702_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Net.WebExceptionStatus>::Dispose()
extern "C"  void Enumerator_Dispose_m2377534074_gshared (Enumerator_t2286117523 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2377534074(__this, method) ((  void (*) (Enumerator_t2286117523 *, const MethodInfo*))Enumerator_Dispose_m2377534074_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Net.WebExceptionStatus>::CheckState()
extern "C"  void Enumerator_CheckState_m1105513922_gshared (Enumerator_t2286117523 * __this, const MethodInfo* method);
#define Enumerator_CheckState_m1105513922(__this, method) ((  void (*) (Enumerator_t2286117523 *, const MethodInfo*))Enumerator_CheckState_m1105513922_gshared)(__this, method)
