﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.Runtime.IMetricsFormatter
struct IMetricsFormatter_t495884108;

#include "mscorlib_System_Object2689449295.h"
#include "AWSSDK_Core_Amazon_LoggingOptions2865640765.h"
#include "AWSSDK_Core_Amazon_ResponseLoggingOption3443611737.h"
#include "AWSSDK_Core_Amazon_LogMetricsFormatOption97749509.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Util.LoggingConfig
struct  LoggingConfig_t4162907495  : public Il2CppObject
{
public:
	// Amazon.LoggingOptions Amazon.Util.LoggingConfig::_logTo
	int32_t ____logTo_1;
	// Amazon.ResponseLoggingOption Amazon.Util.LoggingConfig::<LogResponses>k__BackingField
	int32_t ___U3CLogResponsesU3Ek__BackingField_2;
	// System.Int32 Amazon.Util.LoggingConfig::<LogResponsesSizeLimit>k__BackingField
	int32_t ___U3CLogResponsesSizeLimitU3Ek__BackingField_3;
	// System.Boolean Amazon.Util.LoggingConfig::<LogMetrics>k__BackingField
	bool ___U3CLogMetricsU3Ek__BackingField_4;
	// Amazon.LogMetricsFormatOption Amazon.Util.LoggingConfig::<LogMetricsFormat>k__BackingField
	int32_t ___U3CLogMetricsFormatU3Ek__BackingField_5;
	// Amazon.Runtime.IMetricsFormatter Amazon.Util.LoggingConfig::<LogMetricsCustomFormatter>k__BackingField
	Il2CppObject * ___U3CLogMetricsCustomFormatterU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of__logTo_1() { return static_cast<int32_t>(offsetof(LoggingConfig_t4162907495, ____logTo_1)); }
	inline int32_t get__logTo_1() const { return ____logTo_1; }
	inline int32_t* get_address_of__logTo_1() { return &____logTo_1; }
	inline void set__logTo_1(int32_t value)
	{
		____logTo_1 = value;
	}

	inline static int32_t get_offset_of_U3CLogResponsesU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(LoggingConfig_t4162907495, ___U3CLogResponsesU3Ek__BackingField_2)); }
	inline int32_t get_U3CLogResponsesU3Ek__BackingField_2() const { return ___U3CLogResponsesU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CLogResponsesU3Ek__BackingField_2() { return &___U3CLogResponsesU3Ek__BackingField_2; }
	inline void set_U3CLogResponsesU3Ek__BackingField_2(int32_t value)
	{
		___U3CLogResponsesU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CLogResponsesSizeLimitU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(LoggingConfig_t4162907495, ___U3CLogResponsesSizeLimitU3Ek__BackingField_3)); }
	inline int32_t get_U3CLogResponsesSizeLimitU3Ek__BackingField_3() const { return ___U3CLogResponsesSizeLimitU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CLogResponsesSizeLimitU3Ek__BackingField_3() { return &___U3CLogResponsesSizeLimitU3Ek__BackingField_3; }
	inline void set_U3CLogResponsesSizeLimitU3Ek__BackingField_3(int32_t value)
	{
		___U3CLogResponsesSizeLimitU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CLogMetricsU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(LoggingConfig_t4162907495, ___U3CLogMetricsU3Ek__BackingField_4)); }
	inline bool get_U3CLogMetricsU3Ek__BackingField_4() const { return ___U3CLogMetricsU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CLogMetricsU3Ek__BackingField_4() { return &___U3CLogMetricsU3Ek__BackingField_4; }
	inline void set_U3CLogMetricsU3Ek__BackingField_4(bool value)
	{
		___U3CLogMetricsU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CLogMetricsFormatU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(LoggingConfig_t4162907495, ___U3CLogMetricsFormatU3Ek__BackingField_5)); }
	inline int32_t get_U3CLogMetricsFormatU3Ek__BackingField_5() const { return ___U3CLogMetricsFormatU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CLogMetricsFormatU3Ek__BackingField_5() { return &___U3CLogMetricsFormatU3Ek__BackingField_5; }
	inline void set_U3CLogMetricsFormatU3Ek__BackingField_5(int32_t value)
	{
		___U3CLogMetricsFormatU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CLogMetricsCustomFormatterU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(LoggingConfig_t4162907495, ___U3CLogMetricsCustomFormatterU3Ek__BackingField_6)); }
	inline Il2CppObject * get_U3CLogMetricsCustomFormatterU3Ek__BackingField_6() const { return ___U3CLogMetricsCustomFormatterU3Ek__BackingField_6; }
	inline Il2CppObject ** get_address_of_U3CLogMetricsCustomFormatterU3Ek__BackingField_6() { return &___U3CLogMetricsCustomFormatterU3Ek__BackingField_6; }
	inline void set_U3CLogMetricsCustomFormatterU3Ek__BackingField_6(Il2CppObject * value)
	{
		___U3CLogMetricsCustomFormatterU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CLogMetricsCustomFormatterU3Ek__BackingField_6, value);
	}
};

struct LoggingConfig_t4162907495_StaticFields
{
public:
	// System.Int32 Amazon.Util.LoggingConfig::DefaultLogResponsesSizeLimit
	int32_t ___DefaultLogResponsesSizeLimit_0;

public:
	inline static int32_t get_offset_of_DefaultLogResponsesSizeLimit_0() { return static_cast<int32_t>(offsetof(LoggingConfig_t4162907495_StaticFields, ___DefaultLogResponsesSizeLimit_0)); }
	inline int32_t get_DefaultLogResponsesSizeLimit_0() const { return ___DefaultLogResponsesSizeLimit_0; }
	inline int32_t* get_address_of_DefaultLogResponsesSizeLimit_0() { return &___DefaultLogResponsesSizeLimit_0; }
	inline void set_DefaultLogResponsesSizeLimit_0(int32_t value)
	{
		___DefaultLogResponsesSizeLimit_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
