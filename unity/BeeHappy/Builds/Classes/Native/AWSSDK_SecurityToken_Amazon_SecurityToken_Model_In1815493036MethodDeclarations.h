﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.SecurityToken.Model.Internal.MarshallTransformations.AssumeRoleWithWebIdentityRequestMarshaller
struct AssumeRoleWithWebIdentityRequestMarshaller_t1815493036;
// Amazon.Runtime.Internal.IRequest
struct IRequest_t2400804350;
// Amazon.Runtime.AmazonWebServiceRequest
struct AmazonWebServiceRequest_t3384026212;
// Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest
struct AssumeRoleWithWebIdentityRequest_t193094215;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_Runtime_AmazonWebServiceRequest3384026212.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_Model_Ass193094215.h"

// Amazon.Runtime.Internal.IRequest Amazon.SecurityToken.Model.Internal.MarshallTransformations.AssumeRoleWithWebIdentityRequestMarshaller::Marshall(Amazon.Runtime.AmazonWebServiceRequest)
extern "C"  Il2CppObject * AssumeRoleWithWebIdentityRequestMarshaller_Marshall_m404585616 (AssumeRoleWithWebIdentityRequestMarshaller_t1815493036 * __this, AmazonWebServiceRequest_t3384026212 * ___input0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.Internal.IRequest Amazon.SecurityToken.Model.Internal.MarshallTransformations.AssumeRoleWithWebIdentityRequestMarshaller::Marshall(Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest)
extern "C"  Il2CppObject * AssumeRoleWithWebIdentityRequestMarshaller_Marshall_m1111591347 (AssumeRoleWithWebIdentityRequestMarshaller_t1815493036 * __this, AssumeRoleWithWebIdentityRequest_t193094215 * ___publicRequest0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.SecurityToken.Model.Internal.MarshallTransformations.AssumeRoleWithWebIdentityRequestMarshaller::.ctor()
extern "C"  void AssumeRoleWithWebIdentityRequestMarshaller__ctor_m3147474726 (AssumeRoleWithWebIdentityRequestMarshaller_t1815493036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
