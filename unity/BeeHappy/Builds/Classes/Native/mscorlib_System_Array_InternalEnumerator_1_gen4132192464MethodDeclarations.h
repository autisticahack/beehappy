﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4132192464.h"
#include "mscorlib_System_Array3829468939.h"
#include "AWSSDK_Core_Amazon_Runtime_Metric3273440202.h"

// System.Void System.Array/InternalEnumerator`1<Amazon.Runtime.Metric>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3736486177_gshared (InternalEnumerator_1_t4132192464 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m3736486177(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t4132192464 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3736486177_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<Amazon.Runtime.Metric>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m124996245_gshared (InternalEnumerator_1_t4132192464 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m124996245(__this, method) ((  void (*) (InternalEnumerator_1_t4132192464 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m124996245_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Amazon.Runtime.Metric>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1804989353_gshared (InternalEnumerator_1_t4132192464 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1804989353(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t4132192464 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1804989353_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Amazon.Runtime.Metric>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2174254852_gshared (InternalEnumerator_1_t4132192464 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2174254852(__this, method) ((  void (*) (InternalEnumerator_1_t4132192464 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2174254852_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Amazon.Runtime.Metric>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1267736189_gshared (InternalEnumerator_1_t4132192464 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1267736189(__this, method) ((  bool (*) (InternalEnumerator_1_t4132192464 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1267736189_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Amazon.Runtime.Metric>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m1054546204_gshared (InternalEnumerator_1_t4132192464 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1054546204(__this, method) ((  int32_t (*) (InternalEnumerator_1_t4132192464 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1054546204_gshared)(__this, method)
