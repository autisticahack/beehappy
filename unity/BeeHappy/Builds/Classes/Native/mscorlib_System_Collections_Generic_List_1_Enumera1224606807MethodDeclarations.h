﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Amazon.Runtime.IPipelineHandler>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m1468050878(__this, ___l0, method) ((  void (*) (Enumerator_t1224606807 *, List_1_t1689877133 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Amazon.Runtime.IPipelineHandler>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3100236700(__this, method) ((  void (*) (Enumerator_t1224606807 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Amazon.Runtime.IPipelineHandler>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m768653584(__this, method) ((  Il2CppObject * (*) (Enumerator_t1224606807 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Amazon.Runtime.IPipelineHandler>::Dispose()
#define Enumerator_Dispose_m447349547(__this, method) ((  void (*) (Enumerator_t1224606807 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Amazon.Runtime.IPipelineHandler>::VerifyState()
#define Enumerator_VerifyState_m2154815448(__this, method) ((  void (*) (Enumerator_t1224606807 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Amazon.Runtime.IPipelineHandler>::MoveNext()
#define Enumerator_MoveNext_m1578874324(__this, method) ((  bool (*) (Enumerator_t1224606807 *, const MethodInfo*))Enumerator_MoveNext_m984484162_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Amazon.Runtime.IPipelineHandler>::get_Current()
#define Enumerator_get_Current_m3331479755(__this, method) ((  Il2CppObject * (*) (Enumerator_t1224606807 *, const MethodInfo*))Enumerator_get_Current_m2193722336_gshared)(__this, method)
