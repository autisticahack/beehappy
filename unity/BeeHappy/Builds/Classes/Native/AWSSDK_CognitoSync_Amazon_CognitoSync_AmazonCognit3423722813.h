﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AWSSDK_Core_Amazon_Runtime_AmazonServiceClient3583134838.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.CognitoSync.AmazonCognitoSyncClient
struct  AmazonCognitoSyncClient_t3423722813  : public AmazonServiceClient_t3583134838
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
