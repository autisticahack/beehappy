﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.Linq.XNodeDocumentOrderComparer
struct XNodeDocumentOrderComparer_t1208684534;
// System.Object
struct Il2CppObject;
// System.Xml.Linq.XNode
struct XNode_t2707504214;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "System_Xml_Linq_System_Xml_Linq_XNode2707504214.h"
#include "System_Xml_Linq_System_Xml_Linq_XNodeDocumentOrder4155354831.h"

// System.Void System.Xml.Linq.XNodeDocumentOrderComparer::.ctor()
extern "C"  void XNodeDocumentOrderComparer__ctor_m2785062413 (XNodeDocumentOrderComparer_t1208684534 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Xml.Linq.XNodeDocumentOrderComparer::System.Collections.IComparer.Compare(System.Object,System.Object)
extern "C"  int32_t XNodeDocumentOrderComparer_System_Collections_IComparer_Compare_m2457853580 (XNodeDocumentOrderComparer_t1208684534 * __this, Il2CppObject * ___n10, Il2CppObject * ___n21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Xml.Linq.XNodeDocumentOrderComparer::Compare(System.Xml.Linq.XNode,System.Xml.Linq.XNode)
extern "C"  int32_t XNodeDocumentOrderComparer_Compare_m2082190298 (XNodeDocumentOrderComparer_t1208684534 * __this, XNode_t2707504214 * ___n10, XNode_t2707504214 * ___n21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Linq.XNodeDocumentOrderComparer/CompareResult System.Xml.Linq.XNodeDocumentOrderComparer::CompareCore(System.Xml.Linq.XNode,System.Xml.Linq.XNode)
extern "C"  int32_t XNodeDocumentOrderComparer_CompareCore_m1096164403 (XNodeDocumentOrderComparer_t1208684534 * __this, XNode_t2707504214 * ___n10, XNode_t2707504214 * ___n21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Linq.XNodeDocumentOrderComparer/CompareResult System.Xml.Linq.XNodeDocumentOrderComparer::CompareSibling(System.Xml.Linq.XNode,System.Xml.Linq.XNode,System.Xml.Linq.XNodeDocumentOrderComparer/CompareResult)
extern "C"  int32_t XNodeDocumentOrderComparer_CompareSibling_m1852759449 (XNodeDocumentOrderComparer_t1208684534 * __this, XNode_t2707504214 * ___n10, XNode_t2707504214 * ___n21, int32_t ___forSameValue2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
