﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_EventArgs3289624707.h"
#include "AWSSDK_Core_Amazon_Util_Internal_PlatformServices_N879095062.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Util.Internal.PlatformServices.NetworkStatusEventArgs
struct  NetworkStatusEventArgs_t1607167653  : public EventArgs_t3289624707
{
public:
	// Amazon.Util.Internal.PlatformServices.NetworkStatus Amazon.Util.Internal.PlatformServices.NetworkStatusEventArgs::<Status>k__BackingField
	int32_t ___U3CStatusU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CStatusU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(NetworkStatusEventArgs_t1607167653, ___U3CStatusU3Ek__BackingField_1)); }
	inline int32_t get_U3CStatusU3Ek__BackingField_1() const { return ___U3CStatusU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CStatusU3Ek__BackingField_1() { return &___U3CStatusU3Ek__BackingField_1; }
	inline void set_U3CStatusU3Ek__BackingField_1(int32_t value)
	{
		___U3CStatusU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
