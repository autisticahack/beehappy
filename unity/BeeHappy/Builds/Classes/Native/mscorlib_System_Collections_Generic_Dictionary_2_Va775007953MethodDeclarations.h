﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1834534456MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.IntPtr,System.Collections.Generic.Dictionary`2<System.IntPtr,ThirdParty.iOS4Unity.Callbacks/Methods>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m2925981467(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t775007953 *, Dictionary_2_t2071948110 *, const MethodInfo*))ValueCollection__ctor_m1760151388_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.IntPtr,System.Collections.Generic.Dictionary`2<System.IntPtr,ThirdParty.iOS4Unity.Callbacks/Methods>>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1301347957(__this, ___item0, method) ((  void (*) (ValueCollection_t775007953 *, Dictionary_2_t1629922792 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m4281311350_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.IntPtr,System.Collections.Generic.Dictionary`2<System.IntPtr,ThirdParty.iOS4Unity.Callbacks/Methods>>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3030939116(__this, method) ((  void (*) (ValueCollection_t775007953 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3471385195_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.IntPtr,System.Collections.Generic.Dictionary`2<System.IntPtr,ThirdParty.iOS4Unity.Callbacks/Methods>>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1478790191(__this, ___item0, method) ((  bool (*) (ValueCollection_t775007953 *, Dictionary_2_t1629922792 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3650367146_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.IntPtr,System.Collections.Generic.Dictionary`2<System.IntPtr,ThirdParty.iOS4Unity.Callbacks/Methods>>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m578598454(__this, ___item0, method) ((  bool (*) (ValueCollection_t775007953 *, Dictionary_2_t1629922792 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m4188992987_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.IntPtr,System.Collections.Generic.Dictionary`2<System.IntPtr,ThirdParty.iOS4Unity.Callbacks/Methods>>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3730244380(__this, method) ((  Il2CppObject* (*) (ValueCollection_t775007953 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3930535201_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.IntPtr,System.Collections.Generic.Dictionary`2<System.IntPtr,ThirdParty.iOS4Unity.Callbacks/Methods>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m928965148(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t775007953 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m2212134845_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.IntPtr,System.Collections.Generic.Dictionary`2<System.IntPtr,ThirdParty.iOS4Unity.Callbacks/Methods>>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2139087961(__this, method) ((  Il2CppObject * (*) (ValueCollection_t775007953 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2109355710_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.IntPtr,System.Collections.Generic.Dictionary`2<System.IntPtr,ThirdParty.iOS4Unity.Callbacks/Methods>>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m698423554(__this, method) ((  bool (*) (ValueCollection_t775007953 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3022741191_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.IntPtr,System.Collections.Generic.Dictionary`2<System.IntPtr,ThirdParty.iOS4Unity.Callbacks/Methods>>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1527146316(__this, method) ((  bool (*) (ValueCollection_t775007953 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2217081553_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.IntPtr,System.Collections.Generic.Dictionary`2<System.IntPtr,ThirdParty.iOS4Unity.Callbacks/Methods>>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m4216838220(__this, method) ((  Il2CppObject * (*) (ValueCollection_t775007953 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m3253749389_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.IntPtr,System.Collections.Generic.Dictionary`2<System.IntPtr,ThirdParty.iOS4Unity.Callbacks/Methods>>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m3908988460(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t775007953 *, Dictionary_2U5BU5D_t4120690041*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m254858027_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.IntPtr,System.Collections.Generic.Dictionary`2<System.IntPtr,ThirdParty.iOS4Unity.Callbacks/Methods>>::GetEnumerator()
#define ValueCollection_GetEnumerator_m576806631(__this, method) ((  Enumerator_t3758480874  (*) (ValueCollection_t775007953 *, const MethodInfo*))ValueCollection_GetEnumerator_m2859673798_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.IntPtr,System.Collections.Generic.Dictionary`2<System.IntPtr,ThirdParty.iOS4Unity.Callbacks/Methods>>::get_Count()
#define ValueCollection_get_Count_m1228040004(__this, method) ((  int32_t (*) (ValueCollection_t775007953 *, const MethodInfo*))ValueCollection_get_Count_m3277366953_gshared)(__this, method)
