﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Util.Internal.AndroidInterop/<>c__6`1<System.Int32>
struct U3CU3Ec__6_1_t1600678017;
// System.Reflection.MethodInfo
struct MethodInfo_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_MethodInfo3330546337.h"

// System.Void Amazon.Util.Internal.AndroidInterop/<>c__6`1<System.Int32>::.cctor()
extern "C"  void U3CU3Ec__6_1__cctor_m1113788077_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define U3CU3Ec__6_1__cctor_m1113788077(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))U3CU3Ec__6_1__cctor_m1113788077_gshared)(__this /* static, unused */, method)
// System.Void Amazon.Util.Internal.AndroidInterop/<>c__6`1<System.Int32>::.ctor()
extern "C"  void U3CU3Ec__6_1__ctor_m300635530_gshared (U3CU3Ec__6_1_t1600678017 * __this, const MethodInfo* method);
#define U3CU3Ec__6_1__ctor_m300635530(__this, method) ((  void (*) (U3CU3Ec__6_1_t1600678017 *, const MethodInfo*))U3CU3Ec__6_1__ctor_m300635530_gshared)(__this, method)
// System.Boolean Amazon.Util.Internal.AndroidInterop/<>c__6`1<System.Int32>::<GetJavaField>b__6_0(System.Reflection.MethodInfo)
extern "C"  bool U3CU3Ec__6_1_U3CGetJavaFieldU3Eb__6_0_m373750616_gshared (U3CU3Ec__6_1_t1600678017 * __this, MethodInfo_t * ___x0, const MethodInfo* method);
#define U3CU3Ec__6_1_U3CGetJavaFieldU3Eb__6_0_m373750616(__this, ___x0, method) ((  bool (*) (U3CU3Ec__6_1_t1600678017 *, MethodInfo_t *, const MethodInfo*))U3CU3Ec__6_1_U3CGetJavaFieldU3Eb__6_0_m373750616_gshared)(__this, ___x0, method)
// System.Boolean Amazon.Util.Internal.AndroidInterop/<>c__6`1<System.Int32>::<GetJavaField>b__6_1(System.Reflection.MethodInfo)
extern "C"  bool U3CU3Ec__6_1_U3CGetJavaFieldU3Eb__6_1_m2974693433_gshared (U3CU3Ec__6_1_t1600678017 * __this, MethodInfo_t * ___x0, const MethodInfo* method);
#define U3CU3Ec__6_1_U3CGetJavaFieldU3Eb__6_1_m2974693433(__this, ___x0, method) ((  bool (*) (U3CU3Ec__6_1_t1600678017 *, MethodInfo_t *, const MethodInfo*))U3CU3Ec__6_1_U3CGetJavaFieldU3Eb__6_1_m2974693433_gshared)(__this, ___x0, method)
