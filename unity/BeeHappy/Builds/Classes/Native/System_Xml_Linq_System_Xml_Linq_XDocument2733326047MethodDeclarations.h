﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.Linq.XDocument
struct XDocument_t2733326047;
// System.Xml.Linq.XDeclaration
struct XDeclaration_t3367285402;
// System.Xml.Linq.XDocumentType
struct XDocumentType_t738990919;
// System.Xml.Linq.XElement
struct XElement_t553821050;
// System.IO.TextReader
struct TextReader_t1561828458;
// System.Xml.XmlReader
struct XmlReader_t3675626668;
// System.String
struct String_t;
// System.Xml.XmlWriter
struct XmlWriter_t1048088568;
// System.Object
struct Il2CppObject;
// System.Xml.Linq.XNode
struct XNode_t2707504214;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_Linq_System_Xml_Linq_XDeclaration3367285402.h"
#include "System_Xml_System_Xml_XmlNodeType739504597.h"
#include "mscorlib_System_IO_TextReader1561828458.h"
#include "System_Xml_Linq_System_Xml_Linq_LoadOptions53198144.h"
#include "System_Xml_System_Xml_XmlReader3675626668.h"
#include "mscorlib_System_String2029220233.h"
#include "System_Xml_System_Xml_XmlWriter1048088568.h"
#include "mscorlib_System_Object2689449295.h"
#include "System_Xml_Linq_System_Xml_Linq_XNode2707504214.h"

// System.Void System.Xml.Linq.XDocument::.ctor()
extern "C"  void XDocument__ctor_m151629198 (XDocument_t2733326047 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Linq.XDeclaration System.Xml.Linq.XDocument::get_Declaration()
extern "C"  XDeclaration_t3367285402 * XDocument_get_Declaration_m2764232986 (XDocument_t2733326047 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Linq.XDocument::set_Declaration(System.Xml.Linq.XDeclaration)
extern "C"  void XDocument_set_Declaration_m575128867 (XDocument_t2733326047 * __this, XDeclaration_t3367285402 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Linq.XDocumentType System.Xml.Linq.XDocument::get_DocumentType()
extern "C"  XDocumentType_t738990919 * XDocument_get_DocumentType_m2810920598 (XDocument_t2733326047 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlNodeType System.Xml.Linq.XDocument::get_NodeType()
extern "C"  int32_t XDocument_get_NodeType_m3730006457 (XDocument_t2733326047 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Linq.XElement System.Xml.Linq.XDocument::get_Root()
extern "C"  XElement_t553821050 * XDocument_get_Root_m946627048 (XDocument_t2733326047 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Linq.XDocument System.Xml.Linq.XDocument::Load(System.IO.TextReader)
extern "C"  XDocument_t2733326047 * XDocument_Load_m2533057503 (Il2CppObject * __this /* static, unused */, TextReader_t1561828458 * ___reader0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Linq.XDocument System.Xml.Linq.XDocument::Load(System.IO.TextReader,System.Xml.Linq.LoadOptions)
extern "C"  XDocument_t2733326047 * XDocument_Load_m479322937 (Il2CppObject * __this /* static, unused */, TextReader_t1561828458 * ___reader0, int32_t ___options1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Linq.XDocument System.Xml.Linq.XDocument::LoadCore(System.Xml.XmlReader,System.Xml.Linq.LoadOptions)
extern "C"  XDocument_t2733326047 * XDocument_LoadCore_m1398431905 (Il2CppObject * __this /* static, unused */, XmlReader_t3675626668 * ___reader0, int32_t ___options1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Linq.XDocument::ReadContent(System.Xml.XmlReader,System.Xml.Linq.LoadOptions)
extern "C"  void XDocument_ReadContent_m555950019 (XDocument_t2733326047 * __this, XmlReader_t3675626668 * ___reader0, int32_t ___options1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Linq.XDocument::ValidateWhitespace(System.String)
extern "C"  void XDocument_ValidateWhitespace_m3463530265 (Il2CppObject * __this /* static, unused */, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Linq.XDocument::WriteTo(System.Xml.XmlWriter)
extern "C"  void XDocument_WriteTo_m4076631342 (XDocument_t2733326047 * __this, XmlWriter_t1048088568 * ___w0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.Linq.XDocument::OnAddingObject(System.Object,System.Boolean,System.Xml.Linq.XNode,System.Boolean)
extern "C"  bool XDocument_OnAddingObject_m2423584837 (XDocument_t2733326047 * __this, Il2CppObject * ___obj0, bool ___rejectAttribute1, XNode_t2707504214 * ___refNode2, bool ___addFirst3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Linq.XDocument::VerifyAddedNode(System.Object,System.Boolean)
extern "C"  void XDocument_VerifyAddedNode_m3124949756 (XDocument_t2733326047 * __this, Il2CppObject * ___node0, bool ___addFirst1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
