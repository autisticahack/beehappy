﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3237672747MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.String,ThirdParty.Json.LitJson.JsonData>>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m338194164(__this, ___l0, method) ((  void (*) (Enumerator_t2839227342 *, List_1_t3304497668 *, const MethodInfo*))Enumerator__ctor_m1246468418_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.String,ThirdParty.Json.LitJson.JsonData>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m172419638(__this, method) ((  void (*) (Enumerator_t2839227342 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m372155760_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.String,ThirdParty.Json.LitJson.JsonData>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1693666386(__this, method) ((  Il2CppObject * (*) (Enumerator_t2839227342 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1324042990_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.String,ThirdParty.Json.LitJson.JsonData>>::Dispose()
#define Enumerator_Dispose_m1448430167(__this, method) ((  void (*) (Enumerator_t2839227342 *, const MethodInfo*))Enumerator_Dispose_m2341522715_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.String,ThirdParty.Json.LitJson.JsonData>>::VerifyState()
#define Enumerator_VerifyState_m2642394746(__this, method) ((  void (*) (Enumerator_t2839227342 *, const MethodInfo*))Enumerator_VerifyState_m1266371732_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.String,ThirdParty.Json.LitJson.JsonData>>::MoveNext()
#define Enumerator_MoveNext_m2425010510(__this, method) ((  bool (*) (Enumerator_t2839227342 *, const MethodInfo*))Enumerator_MoveNext_m3337940480_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.String,ThirdParty.Json.LitJson.JsonData>>::get_Current()
#define Enumerator_get_Current_m3929657847(__this, method) ((  KeyValuePair_2_t3935376536  (*) (Enumerator_t2839227342 *, const MethodInfo*))Enumerator_get_Current_m305664535_gshared)(__this, method)
