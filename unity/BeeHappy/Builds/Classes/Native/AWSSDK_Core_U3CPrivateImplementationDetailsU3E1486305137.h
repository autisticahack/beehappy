﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object2689449295.h"
#include "AWSSDK_Core_U3CPrivateImplementationDetailsU3E___S2631791551.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t1486305143  : public Il2CppObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields
{
public:
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=112 <PrivateImplementationDetails>::50B1635D1FB2907A171B71751E1A3FA79423CA17
	__StaticArrayInitTypeSizeU3D112_t2631791551  ___50B1635D1FB2907A171B71751E1A3FA79423CA17_0;

public:
	inline static int32_t get_offset_of_U350B1635D1FB2907A171B71751E1A3FA79423CA17_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields, ___50B1635D1FB2907A171B71751E1A3FA79423CA17_0)); }
	inline __StaticArrayInitTypeSizeU3D112_t2631791551  get_U350B1635D1FB2907A171B71751E1A3FA79423CA17_0() const { return ___50B1635D1FB2907A171B71751E1A3FA79423CA17_0; }
	inline __StaticArrayInitTypeSizeU3D112_t2631791551 * get_address_of_U350B1635D1FB2907A171B71751E1A3FA79423CA17_0() { return &___50B1635D1FB2907A171B71751E1A3FA79423CA17_0; }
	inline void set_U350B1635D1FB2907A171B71751E1A3FA79423CA17_0(__StaticArrayInitTypeSizeU3D112_t2631791551  value)
	{
		___50B1635D1FB2907A171B71751E1A3FA79423CA17_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
