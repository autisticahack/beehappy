﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Char[]
struct CharU5BU5D_t1328083999;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// Amazon.Util.Internal.RootConfig
struct RootConfig_t4046630774;
// System.ComponentModel.PropertyChangedEventHandler
struct PropertyChangedEventHandler_t3042952059;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.Diagnostics.TraceListener>>
struct Dictionary_2_t403882377;
// System.Xml.Linq.XDocument
struct XDocument_t2733326047;

#include "mscorlib_System_Object2689449295.h"
#include "AWSSDK_Core_Amazon_LoggingOptions2865640765.h"
#include "AWSSDK_Core_Amazon_ResponseLoggingOption3443611737.h"
#include "mscorlib_System_TimeSpan3430258949.h"
#include "AWSSDK_Core_Amazon_AWSConfigs_HttpClientOption4250830711.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.AWSConfigs
struct  AWSConfigs_t909839638  : public Il2CppObject
{
public:

public:
};

struct AWSConfigs_t909839638_StaticFields
{
public:
	// System.Char[] Amazon.AWSConfigs::validSeparators
	CharU5BU5D_t1328083999* ___validSeparators_0;
	// System.String Amazon.AWSConfigs::_awsRegion
	String_t* ____awsRegion_1;
	// Amazon.LoggingOptions Amazon.AWSConfigs::_logging
	int32_t ____logging_2;
	// Amazon.ResponseLoggingOption Amazon.AWSConfigs::_responseLogging
	int32_t ____responseLogging_3;
	// System.Boolean Amazon.AWSConfigs::_logMetrics
	bool ____logMetrics_4;
	// System.String Amazon.AWSConfigs::_endpointDefinition
	String_t* ____endpointDefinition_5;
	// System.String Amazon.AWSConfigs::_awsProfileName
	String_t* ____awsProfileName_6;
	// System.String Amazon.AWSConfigs::_awsAccountsLocation
	String_t* ____awsAccountsLocation_7;
	// System.Boolean Amazon.AWSConfigs::_useSdkCache
	bool ____useSdkCache_8;
	// System.Object Amazon.AWSConfigs::_lock
	Il2CppObject * ____lock_9;
	// System.Collections.Generic.List`1<System.String> Amazon.AWSConfigs::standardConfigs
	List_1_t1398341365 * ___standardConfigs_10;
	// System.Boolean Amazon.AWSConfigs::configPresent
	bool ___configPresent_11;
	// Amazon.Util.Internal.RootConfig Amazon.AWSConfigs::_rootConfig
	RootConfig_t4046630774 * ____rootConfig_12;
	// System.TimeSpan Amazon.AWSConfigs::<ClockOffset>k__BackingField
	TimeSpan_t3430258949  ___U3CClockOffsetU3Ek__BackingField_13;
	// System.ComponentModel.PropertyChangedEventHandler Amazon.AWSConfigs::mPropertyChanged
	PropertyChangedEventHandler_t3042952059 * ___mPropertyChanged_14;
	// System.Object Amazon.AWSConfigs::propertyChangedLock
	Il2CppObject * ___propertyChangedLock_15;
	// Amazon.AWSConfigs/HttpClientOption Amazon.AWSConfigs::_httpClient
	int32_t ____httpClient_16;
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.Diagnostics.TraceListener>> Amazon.AWSConfigs::_traceListeners
	Dictionary_2_t403882377 * ____traceListeners_17;
	// System.Xml.Linq.XDocument Amazon.AWSConfigs::xmlDoc
	XDocument_t2733326047 * ___xmlDoc_18;

public:
	inline static int32_t get_offset_of_validSeparators_0() { return static_cast<int32_t>(offsetof(AWSConfigs_t909839638_StaticFields, ___validSeparators_0)); }
	inline CharU5BU5D_t1328083999* get_validSeparators_0() const { return ___validSeparators_0; }
	inline CharU5BU5D_t1328083999** get_address_of_validSeparators_0() { return &___validSeparators_0; }
	inline void set_validSeparators_0(CharU5BU5D_t1328083999* value)
	{
		___validSeparators_0 = value;
		Il2CppCodeGenWriteBarrier(&___validSeparators_0, value);
	}

	inline static int32_t get_offset_of__awsRegion_1() { return static_cast<int32_t>(offsetof(AWSConfigs_t909839638_StaticFields, ____awsRegion_1)); }
	inline String_t* get__awsRegion_1() const { return ____awsRegion_1; }
	inline String_t** get_address_of__awsRegion_1() { return &____awsRegion_1; }
	inline void set__awsRegion_1(String_t* value)
	{
		____awsRegion_1 = value;
		Il2CppCodeGenWriteBarrier(&____awsRegion_1, value);
	}

	inline static int32_t get_offset_of__logging_2() { return static_cast<int32_t>(offsetof(AWSConfigs_t909839638_StaticFields, ____logging_2)); }
	inline int32_t get__logging_2() const { return ____logging_2; }
	inline int32_t* get_address_of__logging_2() { return &____logging_2; }
	inline void set__logging_2(int32_t value)
	{
		____logging_2 = value;
	}

	inline static int32_t get_offset_of__responseLogging_3() { return static_cast<int32_t>(offsetof(AWSConfigs_t909839638_StaticFields, ____responseLogging_3)); }
	inline int32_t get__responseLogging_3() const { return ____responseLogging_3; }
	inline int32_t* get_address_of__responseLogging_3() { return &____responseLogging_3; }
	inline void set__responseLogging_3(int32_t value)
	{
		____responseLogging_3 = value;
	}

	inline static int32_t get_offset_of__logMetrics_4() { return static_cast<int32_t>(offsetof(AWSConfigs_t909839638_StaticFields, ____logMetrics_4)); }
	inline bool get__logMetrics_4() const { return ____logMetrics_4; }
	inline bool* get_address_of__logMetrics_4() { return &____logMetrics_4; }
	inline void set__logMetrics_4(bool value)
	{
		____logMetrics_4 = value;
	}

	inline static int32_t get_offset_of__endpointDefinition_5() { return static_cast<int32_t>(offsetof(AWSConfigs_t909839638_StaticFields, ____endpointDefinition_5)); }
	inline String_t* get__endpointDefinition_5() const { return ____endpointDefinition_5; }
	inline String_t** get_address_of__endpointDefinition_5() { return &____endpointDefinition_5; }
	inline void set__endpointDefinition_5(String_t* value)
	{
		____endpointDefinition_5 = value;
		Il2CppCodeGenWriteBarrier(&____endpointDefinition_5, value);
	}

	inline static int32_t get_offset_of__awsProfileName_6() { return static_cast<int32_t>(offsetof(AWSConfigs_t909839638_StaticFields, ____awsProfileName_6)); }
	inline String_t* get__awsProfileName_6() const { return ____awsProfileName_6; }
	inline String_t** get_address_of__awsProfileName_6() { return &____awsProfileName_6; }
	inline void set__awsProfileName_6(String_t* value)
	{
		____awsProfileName_6 = value;
		Il2CppCodeGenWriteBarrier(&____awsProfileName_6, value);
	}

	inline static int32_t get_offset_of__awsAccountsLocation_7() { return static_cast<int32_t>(offsetof(AWSConfigs_t909839638_StaticFields, ____awsAccountsLocation_7)); }
	inline String_t* get__awsAccountsLocation_7() const { return ____awsAccountsLocation_7; }
	inline String_t** get_address_of__awsAccountsLocation_7() { return &____awsAccountsLocation_7; }
	inline void set__awsAccountsLocation_7(String_t* value)
	{
		____awsAccountsLocation_7 = value;
		Il2CppCodeGenWriteBarrier(&____awsAccountsLocation_7, value);
	}

	inline static int32_t get_offset_of__useSdkCache_8() { return static_cast<int32_t>(offsetof(AWSConfigs_t909839638_StaticFields, ____useSdkCache_8)); }
	inline bool get__useSdkCache_8() const { return ____useSdkCache_8; }
	inline bool* get_address_of__useSdkCache_8() { return &____useSdkCache_8; }
	inline void set__useSdkCache_8(bool value)
	{
		____useSdkCache_8 = value;
	}

	inline static int32_t get_offset_of__lock_9() { return static_cast<int32_t>(offsetof(AWSConfigs_t909839638_StaticFields, ____lock_9)); }
	inline Il2CppObject * get__lock_9() const { return ____lock_9; }
	inline Il2CppObject ** get_address_of__lock_9() { return &____lock_9; }
	inline void set__lock_9(Il2CppObject * value)
	{
		____lock_9 = value;
		Il2CppCodeGenWriteBarrier(&____lock_9, value);
	}

	inline static int32_t get_offset_of_standardConfigs_10() { return static_cast<int32_t>(offsetof(AWSConfigs_t909839638_StaticFields, ___standardConfigs_10)); }
	inline List_1_t1398341365 * get_standardConfigs_10() const { return ___standardConfigs_10; }
	inline List_1_t1398341365 ** get_address_of_standardConfigs_10() { return &___standardConfigs_10; }
	inline void set_standardConfigs_10(List_1_t1398341365 * value)
	{
		___standardConfigs_10 = value;
		Il2CppCodeGenWriteBarrier(&___standardConfigs_10, value);
	}

	inline static int32_t get_offset_of_configPresent_11() { return static_cast<int32_t>(offsetof(AWSConfigs_t909839638_StaticFields, ___configPresent_11)); }
	inline bool get_configPresent_11() const { return ___configPresent_11; }
	inline bool* get_address_of_configPresent_11() { return &___configPresent_11; }
	inline void set_configPresent_11(bool value)
	{
		___configPresent_11 = value;
	}

	inline static int32_t get_offset_of__rootConfig_12() { return static_cast<int32_t>(offsetof(AWSConfigs_t909839638_StaticFields, ____rootConfig_12)); }
	inline RootConfig_t4046630774 * get__rootConfig_12() const { return ____rootConfig_12; }
	inline RootConfig_t4046630774 ** get_address_of__rootConfig_12() { return &____rootConfig_12; }
	inline void set__rootConfig_12(RootConfig_t4046630774 * value)
	{
		____rootConfig_12 = value;
		Il2CppCodeGenWriteBarrier(&____rootConfig_12, value);
	}

	inline static int32_t get_offset_of_U3CClockOffsetU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(AWSConfigs_t909839638_StaticFields, ___U3CClockOffsetU3Ek__BackingField_13)); }
	inline TimeSpan_t3430258949  get_U3CClockOffsetU3Ek__BackingField_13() const { return ___U3CClockOffsetU3Ek__BackingField_13; }
	inline TimeSpan_t3430258949 * get_address_of_U3CClockOffsetU3Ek__BackingField_13() { return &___U3CClockOffsetU3Ek__BackingField_13; }
	inline void set_U3CClockOffsetU3Ek__BackingField_13(TimeSpan_t3430258949  value)
	{
		___U3CClockOffsetU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_mPropertyChanged_14() { return static_cast<int32_t>(offsetof(AWSConfigs_t909839638_StaticFields, ___mPropertyChanged_14)); }
	inline PropertyChangedEventHandler_t3042952059 * get_mPropertyChanged_14() const { return ___mPropertyChanged_14; }
	inline PropertyChangedEventHandler_t3042952059 ** get_address_of_mPropertyChanged_14() { return &___mPropertyChanged_14; }
	inline void set_mPropertyChanged_14(PropertyChangedEventHandler_t3042952059 * value)
	{
		___mPropertyChanged_14 = value;
		Il2CppCodeGenWriteBarrier(&___mPropertyChanged_14, value);
	}

	inline static int32_t get_offset_of_propertyChangedLock_15() { return static_cast<int32_t>(offsetof(AWSConfigs_t909839638_StaticFields, ___propertyChangedLock_15)); }
	inline Il2CppObject * get_propertyChangedLock_15() const { return ___propertyChangedLock_15; }
	inline Il2CppObject ** get_address_of_propertyChangedLock_15() { return &___propertyChangedLock_15; }
	inline void set_propertyChangedLock_15(Il2CppObject * value)
	{
		___propertyChangedLock_15 = value;
		Il2CppCodeGenWriteBarrier(&___propertyChangedLock_15, value);
	}

	inline static int32_t get_offset_of__httpClient_16() { return static_cast<int32_t>(offsetof(AWSConfigs_t909839638_StaticFields, ____httpClient_16)); }
	inline int32_t get__httpClient_16() const { return ____httpClient_16; }
	inline int32_t* get_address_of__httpClient_16() { return &____httpClient_16; }
	inline void set__httpClient_16(int32_t value)
	{
		____httpClient_16 = value;
	}

	inline static int32_t get_offset_of__traceListeners_17() { return static_cast<int32_t>(offsetof(AWSConfigs_t909839638_StaticFields, ____traceListeners_17)); }
	inline Dictionary_2_t403882377 * get__traceListeners_17() const { return ____traceListeners_17; }
	inline Dictionary_2_t403882377 ** get_address_of__traceListeners_17() { return &____traceListeners_17; }
	inline void set__traceListeners_17(Dictionary_2_t403882377 * value)
	{
		____traceListeners_17 = value;
		Il2CppCodeGenWriteBarrier(&____traceListeners_17, value);
	}

	inline static int32_t get_offset_of_xmlDoc_18() { return static_cast<int32_t>(offsetof(AWSConfigs_t909839638_StaticFields, ___xmlDoc_18)); }
	inline XDocument_t2733326047 * get_xmlDoc_18() const { return ___xmlDoc_18; }
	inline XDocument_t2733326047 ** get_address_of_xmlDoc_18() { return &___xmlDoc_18; }
	inline void set_xmlDoc_18(XDocument_t2733326047 * value)
	{
		___xmlDoc_18 = value;
		Il2CppCodeGenWriteBarrier(&___xmlDoc_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
