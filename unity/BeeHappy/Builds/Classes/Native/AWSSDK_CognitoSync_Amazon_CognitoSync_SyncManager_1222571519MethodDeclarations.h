﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.CognitoSync.SyncManager.CognitoSyncManager
struct CognitoSyncManager_t1222571519;
// Amazon.CognitoIdentity.CognitoAWSCredentials
struct CognitoAWSCredentials_t2370264792;
// Amazon.CognitoSync.AmazonCognitoSyncConfig
struct AmazonCognitoSyncConfig_t1479139240;
// Amazon.CognitoSync.SyncManager.Dataset
struct Dataset_t3040902086;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// System.EventArgs
struct EventArgs_t3289624707;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Cogn2370264792.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_AmazonCognit1479139240.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_EventArgs3289624707.h"

// System.Void Amazon.CognitoSync.SyncManager.CognitoSyncManager::.ctor(Amazon.CognitoIdentity.CognitoAWSCredentials,Amazon.CognitoSync.AmazonCognitoSyncConfig)
extern "C"  void CognitoSyncManager__ctor_m1989149288 (CognitoSyncManager_t1222571519 * __this, CognitoAWSCredentials_t2370264792 * ___cognitoCredentials0, AmazonCognitoSyncConfig_t1479139240 * ___config1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.SyncManager.CognitoSyncManager::Dispose()
extern "C"  void CognitoSyncManager_Dispose_m3794346976 (CognitoSyncManager_t1222571519 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.SyncManager.CognitoSyncManager::Dispose(System.Boolean)
extern "C"  void CognitoSyncManager_Dispose_m2687962211 (CognitoSyncManager_t1222571519 * __this, bool ___disposing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.CognitoSync.SyncManager.Dataset Amazon.CognitoSync.SyncManager.CognitoSyncManager::OpenOrCreateDataset(System.String)
extern "C"  Dataset_t3040902086 * CognitoSyncManager_OpenOrCreateDataset_m4095815935 (CognitoSyncManager_t1222571519 * __this, String_t* ___datasetName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.SyncManager.CognitoSyncManager::WipeData(System.Boolean)
extern "C"  void CognitoSyncManager_WipeData_m4156854839 (CognitoSyncManager_t1222571519 * __this, bool ___wipeCredentialsAndID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.SyncManager.CognitoSyncManager::IdentityChanged(System.Object,System.EventArgs)
extern "C"  void CognitoSyncManager_IdentityChanged_m2682477169 (CognitoSyncManager_t1222571519 * __this, Il2CppObject * ___sender0, EventArgs_t3289624707 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.CognitoSync.SyncManager.CognitoSyncManager::get_IdentityId()
extern "C"  String_t* CognitoSyncManager_get_IdentityId_m810795640 (CognitoSyncManager_t1222571519 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
