﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3601534125MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m2221533338(__this, ___dictionary0, method) ((  void (*) (Enumerator_t825078117 *, Dictionary_2_t3800020711 *, const MethodInfo*))Enumerator__ctor_m3742107451_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2892831643(__this, method) ((  Il2CppObject * (*) (Enumerator_t825078117 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m229223308_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3181855887(__this, method) ((  void (*) (Enumerator_t825078117 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3225937576_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2651609624(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t825078117 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m221119093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2219397625(__this, method) ((  Il2CppObject * (*) (Enumerator_t825078117 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m467957770_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2504246273(__this, method) ((  Il2CppObject * (*) (Enumerator_t825078117 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2325383168_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint>::MoveNext()
#define Enumerator_MoveNext_m501592667(__this, method) ((  bool (*) (Enumerator_t825078117 *, const MethodInfo*))Enumerator_MoveNext_m3349738440_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint>::get_Current()
#define Enumerator_get_Current_m1971948931(__this, method) ((  KeyValuePair_2_t1557365933  (*) (Enumerator_t825078117 *, const MethodInfo*))Enumerator_get_Current_m25299632_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m49688514(__this, method) ((  String_t* (*) (Enumerator_t825078117 *, const MethodInfo*))Enumerator_get_CurrentKey_m3839846791_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m4292204802(__this, method) ((  RegionEndpoint_t1885241449 * (*) (Enumerator_t825078117 *, const MethodInfo*))Enumerator_get_CurrentValue_m402763047_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint>::Reset()
#define Enumerator_Reset_m1577454068(__this, method) ((  void (*) (Enumerator_t825078117 *, const MethodInfo*))Enumerator_Reset_m3129803197_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint>::VerifyState()
#define Enumerator_VerifyState_m276047741(__this, method) ((  void (*) (Enumerator_t825078117 *, const MethodInfo*))Enumerator_VerifyState_m262343092_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m1270889343(__this, method) ((  void (*) (Enumerator_t825078117 *, const MethodInfo*))Enumerator_VerifyCurrent_m1702320752_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint>::Dispose()
#define Enumerator_Dispose_m276562030(__this, method) ((  void (*) (Enumerator_t825078117 *, const MethodInfo*))Enumerator_Dispose_m1905011127_gshared)(__this, method)
