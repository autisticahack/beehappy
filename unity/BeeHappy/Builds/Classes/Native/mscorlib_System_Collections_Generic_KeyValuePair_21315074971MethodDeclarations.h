﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,System.Collections.Generic.Dictionary`2<System.String,Amazon.Runtime.ConstantClass>>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m3445795077(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t1315074971 *, Type_t *, Dictionary_2_t1620371852 *, const MethodInfo*))KeyValuePair_2__ctor_m1640124561_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Type,System.Collections.Generic.Dictionary`2<System.String,Amazon.Runtime.ConstantClass>>::get_Key()
#define KeyValuePair_2_get_Key_m2744328071(__this, method) ((  Type_t * (*) (KeyValuePair_2_t1315074971 *, const MethodInfo*))KeyValuePair_2_get_Key_m2561166459_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,System.Collections.Generic.Dictionary`2<System.String,Amazon.Runtime.ConstantClass>>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m726761944(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1315074971 *, Type_t *, const MethodInfo*))KeyValuePair_2_set_Key_m744486900_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Type,System.Collections.Generic.Dictionary`2<System.String,Amazon.Runtime.ConstantClass>>::get_Value()
#define KeyValuePair_2_get_Value_m2442461095(__this, method) ((  Dictionary_2_t1620371852 * (*) (KeyValuePair_2_t1315074971 *, const MethodInfo*))KeyValuePair_2_get_Value_m499643803_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,System.Collections.Generic.Dictionary`2<System.String,Amazon.Runtime.ConstantClass>>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m101876840(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1315074971 *, Dictionary_2_t1620371852 *, const MethodInfo*))KeyValuePair_2_set_Value_m1416408204_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Type,System.Collections.Generic.Dictionary`2<System.String,Amazon.Runtime.ConstantClass>>::ToString()
#define KeyValuePair_2_ToString_m3724169312(__this, method) ((  String_t* (*) (KeyValuePair_2_t1315074971 *, const MethodInfo*))KeyValuePair_2_ToString_m2613351884_gshared)(__this, method)
