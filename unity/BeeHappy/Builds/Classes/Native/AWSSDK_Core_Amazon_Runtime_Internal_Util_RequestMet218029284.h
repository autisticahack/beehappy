﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// System.Diagnostics.Stopwatch
struct Stopwatch_t1380178105;
// System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,Amazon.Runtime.Internal.Util.Timing>
struct Dictionary_2_t3597272751;
// System.Collections.Generic.List`1<Amazon.Runtime.Internal.Util.MetricError>
struct List_1_t333565938;
// System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Collections.Generic.List`1<System.Object>>
struct Dictionary_2_t513681620;
// System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Collections.Generic.List`1<Amazon.Runtime.IMetricsTiming>>
struct Dictionary_2_t2292610545;
// System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Int64>
struct Dictionary_2_t3659156526;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Util.RequestMetrics
struct  RequestMetrics_t218029284  : public Il2CppObject
{
public:
	// System.Object Amazon.Runtime.Internal.Util.RequestMetrics::metricsLock
	Il2CppObject * ___metricsLock_0;
	// System.Diagnostics.Stopwatch Amazon.Runtime.Internal.Util.RequestMetrics::stopWatch
	Stopwatch_t1380178105 * ___stopWatch_1;
	// System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,Amazon.Runtime.Internal.Util.Timing> Amazon.Runtime.Internal.Util.RequestMetrics::inFlightTimings
	Dictionary_2_t3597272751 * ___inFlightTimings_2;
	// System.Collections.Generic.List`1<Amazon.Runtime.Internal.Util.MetricError> Amazon.Runtime.Internal.Util.RequestMetrics::errors
	List_1_t333565938 * ___errors_3;
	// System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Collections.Generic.List`1<System.Object>> Amazon.Runtime.Internal.Util.RequestMetrics::<Properties>k__BackingField
	Dictionary_2_t513681620 * ___U3CPropertiesU3Ek__BackingField_4;
	// System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Collections.Generic.List`1<Amazon.Runtime.IMetricsTiming>> Amazon.Runtime.Internal.Util.RequestMetrics::<Timings>k__BackingField
	Dictionary_2_t2292610545 * ___U3CTimingsU3Ek__BackingField_5;
	// System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Int64> Amazon.Runtime.Internal.Util.RequestMetrics::<Counters>k__BackingField
	Dictionary_2_t3659156526 * ___U3CCountersU3Ek__BackingField_6;
	// System.Boolean Amazon.Runtime.Internal.Util.RequestMetrics::<IsEnabled>k__BackingField
	bool ___U3CIsEnabledU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_metricsLock_0() { return static_cast<int32_t>(offsetof(RequestMetrics_t218029284, ___metricsLock_0)); }
	inline Il2CppObject * get_metricsLock_0() const { return ___metricsLock_0; }
	inline Il2CppObject ** get_address_of_metricsLock_0() { return &___metricsLock_0; }
	inline void set_metricsLock_0(Il2CppObject * value)
	{
		___metricsLock_0 = value;
		Il2CppCodeGenWriteBarrier(&___metricsLock_0, value);
	}

	inline static int32_t get_offset_of_stopWatch_1() { return static_cast<int32_t>(offsetof(RequestMetrics_t218029284, ___stopWatch_1)); }
	inline Stopwatch_t1380178105 * get_stopWatch_1() const { return ___stopWatch_1; }
	inline Stopwatch_t1380178105 ** get_address_of_stopWatch_1() { return &___stopWatch_1; }
	inline void set_stopWatch_1(Stopwatch_t1380178105 * value)
	{
		___stopWatch_1 = value;
		Il2CppCodeGenWriteBarrier(&___stopWatch_1, value);
	}

	inline static int32_t get_offset_of_inFlightTimings_2() { return static_cast<int32_t>(offsetof(RequestMetrics_t218029284, ___inFlightTimings_2)); }
	inline Dictionary_2_t3597272751 * get_inFlightTimings_2() const { return ___inFlightTimings_2; }
	inline Dictionary_2_t3597272751 ** get_address_of_inFlightTimings_2() { return &___inFlightTimings_2; }
	inline void set_inFlightTimings_2(Dictionary_2_t3597272751 * value)
	{
		___inFlightTimings_2 = value;
		Il2CppCodeGenWriteBarrier(&___inFlightTimings_2, value);
	}

	inline static int32_t get_offset_of_errors_3() { return static_cast<int32_t>(offsetof(RequestMetrics_t218029284, ___errors_3)); }
	inline List_1_t333565938 * get_errors_3() const { return ___errors_3; }
	inline List_1_t333565938 ** get_address_of_errors_3() { return &___errors_3; }
	inline void set_errors_3(List_1_t333565938 * value)
	{
		___errors_3 = value;
		Il2CppCodeGenWriteBarrier(&___errors_3, value);
	}

	inline static int32_t get_offset_of_U3CPropertiesU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(RequestMetrics_t218029284, ___U3CPropertiesU3Ek__BackingField_4)); }
	inline Dictionary_2_t513681620 * get_U3CPropertiesU3Ek__BackingField_4() const { return ___U3CPropertiesU3Ek__BackingField_4; }
	inline Dictionary_2_t513681620 ** get_address_of_U3CPropertiesU3Ek__BackingField_4() { return &___U3CPropertiesU3Ek__BackingField_4; }
	inline void set_U3CPropertiesU3Ek__BackingField_4(Dictionary_2_t513681620 * value)
	{
		___U3CPropertiesU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CPropertiesU3Ek__BackingField_4, value);
	}

	inline static int32_t get_offset_of_U3CTimingsU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(RequestMetrics_t218029284, ___U3CTimingsU3Ek__BackingField_5)); }
	inline Dictionary_2_t2292610545 * get_U3CTimingsU3Ek__BackingField_5() const { return ___U3CTimingsU3Ek__BackingField_5; }
	inline Dictionary_2_t2292610545 ** get_address_of_U3CTimingsU3Ek__BackingField_5() { return &___U3CTimingsU3Ek__BackingField_5; }
	inline void set_U3CTimingsU3Ek__BackingField_5(Dictionary_2_t2292610545 * value)
	{
		___U3CTimingsU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CTimingsU3Ek__BackingField_5, value);
	}

	inline static int32_t get_offset_of_U3CCountersU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(RequestMetrics_t218029284, ___U3CCountersU3Ek__BackingField_6)); }
	inline Dictionary_2_t3659156526 * get_U3CCountersU3Ek__BackingField_6() const { return ___U3CCountersU3Ek__BackingField_6; }
	inline Dictionary_2_t3659156526 ** get_address_of_U3CCountersU3Ek__BackingField_6() { return &___U3CCountersU3Ek__BackingField_6; }
	inline void set_U3CCountersU3Ek__BackingField_6(Dictionary_2_t3659156526 * value)
	{
		___U3CCountersU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCountersU3Ek__BackingField_6, value);
	}

	inline static int32_t get_offset_of_U3CIsEnabledU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(RequestMetrics_t218029284, ___U3CIsEnabledU3Ek__BackingField_7)); }
	inline bool get_U3CIsEnabledU3Ek__BackingField_7() const { return ___U3CIsEnabledU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CIsEnabledU3Ek__BackingField_7() { return &___U3CIsEnabledU3Ek__BackingField_7; }
	inline void set_U3CIsEnabledU3Ek__BackingField_7(bool value)
	{
		___U3CIsEnabledU3Ek__BackingField_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
