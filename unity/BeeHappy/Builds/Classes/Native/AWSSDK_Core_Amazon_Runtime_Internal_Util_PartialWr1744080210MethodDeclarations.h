﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.Util.PartialWrapperStream
struct PartialWrapperStream_t1744080210;
// System.Byte[]
struct ByteU5BU5D_t3397334013;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_SeekOrigin2475945306.h"

// System.Int64 Amazon.Runtime.Internal.Util.PartialWrapperStream::get_RemainingPartSize()
extern "C"  int64_t PartialWrapperStream_get_RemainingPartSize_m1237467480 (PartialWrapperStream_t1744080210 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Amazon.Runtime.Internal.Util.PartialWrapperStream::get_Length()
extern "C"  int64_t PartialWrapperStream_get_Length_m3507171474 (PartialWrapperStream_t1744080210 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Amazon.Runtime.Internal.Util.PartialWrapperStream::get_Position()
extern "C"  int64_t PartialWrapperStream_get_Position_m1764825715 (PartialWrapperStream_t1744080210 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.PartialWrapperStream::set_Position(System.Int64)
extern "C"  void PartialWrapperStream_set_Position_m3491839804 (PartialWrapperStream_t1744080210 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Amazon.Runtime.Internal.Util.PartialWrapperStream::Read(System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t PartialWrapperStream_Read_m4062620725 (PartialWrapperStream_t1744080210 * __this, ByteU5BU5D_t3397334013* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Amazon.Runtime.Internal.Util.PartialWrapperStream::Seek(System.Int64,System.IO.SeekOrigin)
extern "C"  int64_t PartialWrapperStream_Seek_m3669596958 (PartialWrapperStream_t1744080210 * __this, int64_t ___offset0, int32_t ___origin1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.PartialWrapperStream::SetLength(System.Int64)
extern "C"  void PartialWrapperStream_SetLength_m2405729260 (PartialWrapperStream_t1744080210 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.PartialWrapperStream::Write(System.Byte[],System.Int32,System.Int32)
extern "C"  void PartialWrapperStream_Write_m408065164 (PartialWrapperStream_t1744080210 * __this, ByteU5BU5D_t3397334013* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.PartialWrapperStream::WriteByte(System.Byte)
extern "C"  void PartialWrapperStream_WriteByte_m2086179088 (PartialWrapperStream_t1744080210 * __this, uint8_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
