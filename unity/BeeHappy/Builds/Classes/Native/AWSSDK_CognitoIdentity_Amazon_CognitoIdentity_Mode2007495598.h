﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetCredentialsForIdentityResponseUnmarshaller
struct GetCredentialsForIdentityResponseUnmarshaller_t2007495598;

#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_Json2685947887.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetCredentialsForIdentityResponseUnmarshaller
struct  GetCredentialsForIdentityResponseUnmarshaller_t2007495598  : public JsonResponseUnmarshaller_t2685947887
{
public:

public:
};

struct GetCredentialsForIdentityResponseUnmarshaller_t2007495598_StaticFields
{
public:
	// Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetCredentialsForIdentityResponseUnmarshaller Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetCredentialsForIdentityResponseUnmarshaller::_instance
	GetCredentialsForIdentityResponseUnmarshaller_t2007495598 * ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(GetCredentialsForIdentityResponseUnmarshaller_t2007495598_StaticFields, ____instance_0)); }
	inline GetCredentialsForIdentityResponseUnmarshaller_t2007495598 * get__instance_0() const { return ____instance_0; }
	inline GetCredentialsForIdentityResponseUnmarshaller_t2007495598 ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(GetCredentialsForIdentityResponseUnmarshaller_t2007495598 * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier(&____instance_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
