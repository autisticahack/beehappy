﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Data.DataTable
struct DataTable_t3267612424;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Exception
struct Exception_t1927440687;

#include "mscorlib_System_EventArgs3289624707.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.FillErrorEventArgs
struct  FillErrorEventArgs_t1074279162  : public EventArgs_t3289624707
{
public:
	// System.Data.DataTable System.Data.FillErrorEventArgs::data_table
	DataTable_t3267612424 * ___data_table_1;
	// System.Object[] System.Data.FillErrorEventArgs::values
	ObjectU5BU5D_t3614634134* ___values_2;
	// System.Exception System.Data.FillErrorEventArgs::errors
	Exception_t1927440687 * ___errors_3;
	// System.Boolean System.Data.FillErrorEventArgs::f_continue
	bool ___f_continue_4;

public:
	inline static int32_t get_offset_of_data_table_1() { return static_cast<int32_t>(offsetof(FillErrorEventArgs_t1074279162, ___data_table_1)); }
	inline DataTable_t3267612424 * get_data_table_1() const { return ___data_table_1; }
	inline DataTable_t3267612424 ** get_address_of_data_table_1() { return &___data_table_1; }
	inline void set_data_table_1(DataTable_t3267612424 * value)
	{
		___data_table_1 = value;
		Il2CppCodeGenWriteBarrier(&___data_table_1, value);
	}

	inline static int32_t get_offset_of_values_2() { return static_cast<int32_t>(offsetof(FillErrorEventArgs_t1074279162, ___values_2)); }
	inline ObjectU5BU5D_t3614634134* get_values_2() const { return ___values_2; }
	inline ObjectU5BU5D_t3614634134** get_address_of_values_2() { return &___values_2; }
	inline void set_values_2(ObjectU5BU5D_t3614634134* value)
	{
		___values_2 = value;
		Il2CppCodeGenWriteBarrier(&___values_2, value);
	}

	inline static int32_t get_offset_of_errors_3() { return static_cast<int32_t>(offsetof(FillErrorEventArgs_t1074279162, ___errors_3)); }
	inline Exception_t1927440687 * get_errors_3() const { return ___errors_3; }
	inline Exception_t1927440687 ** get_address_of_errors_3() { return &___errors_3; }
	inline void set_errors_3(Exception_t1927440687 * value)
	{
		___errors_3 = value;
		Il2CppCodeGenWriteBarrier(&___errors_3, value);
	}

	inline static int32_t get_offset_of_f_continue_4() { return static_cast<int32_t>(offsetof(FillErrorEventArgs_t1074279162, ___f_continue_4)); }
	inline bool get_f_continue_4() const { return ___f_continue_4; }
	inline bool* get_address_of_f_continue_4() { return &___f_continue_4; }
	inline void set_f_continue_4(bool value)
	{
		___f_continue_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
