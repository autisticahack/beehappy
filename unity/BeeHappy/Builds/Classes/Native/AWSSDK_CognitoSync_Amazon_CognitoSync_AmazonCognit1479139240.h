﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AWSSDK_Core_Amazon_Runtime_ClientConfig3664713661.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.CognitoSync.AmazonCognitoSyncConfig
struct  AmazonCognitoSyncConfig_t1479139240  : public ClientConfig_t3664713661
{
public:
	// System.String Amazon.CognitoSync.AmazonCognitoSyncConfig::_userAgent
	String_t* ____userAgent_22;

public:
	inline static int32_t get_offset_of__userAgent_22() { return static_cast<int32_t>(offsetof(AmazonCognitoSyncConfig_t1479139240, ____userAgent_22)); }
	inline String_t* get__userAgent_22() const { return ____userAgent_22; }
	inline String_t** get_address_of__userAgent_22() { return &____userAgent_22; }
	inline void set__userAgent_22(String_t* value)
	{
		____userAgent_22 = value;
		Il2CppCodeGenWriteBarrier(&____userAgent_22, value);
	}
};

struct AmazonCognitoSyncConfig_t1479139240_StaticFields
{
public:
	// System.String Amazon.CognitoSync.AmazonCognitoSyncConfig::UserAgentString
	String_t* ___UserAgentString_21;

public:
	inline static int32_t get_offset_of_UserAgentString_21() { return static_cast<int32_t>(offsetof(AmazonCognitoSyncConfig_t1479139240_StaticFields, ___UserAgentString_21)); }
	inline String_t* get_UserAgentString_21() const { return ___UserAgentString_21; }
	inline String_t** get_address_of_UserAgentString_21() { return &___UserAgentString_21; }
	inline void set_UserAgentString_21(String_t* value)
	{
		___UserAgentString_21 = value;
		Il2CppCodeGenWriteBarrier(&___UserAgentString_21, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
