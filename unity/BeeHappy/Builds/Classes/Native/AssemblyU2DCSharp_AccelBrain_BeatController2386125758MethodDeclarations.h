﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AccelBrain.BeatController
struct BeatController_t2386125758;
// System.Single[]
struct SingleU5BU5D_t577127397;

#include "codegen/il2cpp-codegen.h"

// System.Void AccelBrain.BeatController::.ctor()
extern "C"  void BeatController__ctor_m4288157117 (BeatController_t2386125758 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AccelBrain.BeatController::Play()
extern "C"  void BeatController_Play_m363766557 (BeatController_t2386125758 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AccelBrain.BeatController::Stop()
extern "C"  void BeatController_Stop_m2448762773 (BeatController_t2386125758 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AccelBrain.BeatController::OnAudioFilterRead(System.Single[],System.Int32)
extern "C"  void BeatController_OnAudioFilterRead_m2203384794 (BeatController_t2386125758 * __this, SingleU5BU5D_t577127397* ___data0, int32_t ___channels1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
