﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.StreamTransferProgressArgs
struct StreamTransferProgressArgs_t2639638063;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void Amazon.Runtime.StreamTransferProgressArgs::.ctor(System.Int64,System.Int64,System.Int64)
extern "C"  void StreamTransferProgressArgs__ctor_m3656491188 (StreamTransferProgressArgs_t2639638063 * __this, int64_t ___incrementTransferred0, int64_t ___transferred1, int64_t ___total2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Amazon.Runtime.StreamTransferProgressArgs::get_PercentDone()
extern "C"  int32_t StreamTransferProgressArgs_get_PercentDone_m299997552 (StreamTransferProgressArgs_t2639638063 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Runtime.StreamTransferProgressArgs::ToString()
extern "C"  String_t* StreamTransferProgressArgs_ToString_m2221933111 (StreamTransferProgressArgs_t2639638063 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
