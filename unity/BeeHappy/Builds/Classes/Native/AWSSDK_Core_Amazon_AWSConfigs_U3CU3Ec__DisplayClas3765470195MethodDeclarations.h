﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.AWSConfigs/<>c__DisplayClass89_1
struct U3CU3Ec__DisplayClass89_1_t3765470195;

#include "codegen/il2cpp-codegen.h"

// System.Void Amazon.AWSConfigs/<>c__DisplayClass89_1::.ctor()
extern "C"  void U3CU3Ec__DisplayClass89_1__ctor_m3733814784 (U3CU3Ec__DisplayClass89_1_t3765470195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.AWSConfigs/<>c__DisplayClass89_1::<LoadConfigFromResource>b__1()
extern "C"  void U3CU3Ec__DisplayClass89_1_U3CLoadConfigFromResourceU3Eb__1_m1992939415 (U3CU3Ec__DisplayClass89_1_t3765470195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
