﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.IntPtr[]
struct IntPtrU5BU5D_t169632028;
// Mono.Security.X509.X509CertificateCollection
struct X509CertificateCollection_t3592472866;
// System.Collections.Generic.RBTree
struct RBTree_t1544615604;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.RBTree/Node>
struct IEnumerator_1_t4269627449;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.List`1<System.Collections.Generic.RBTree/Node>
struct List_1_t1868257458;
// System.Collections.Generic.RBTree/Node
struct Node_t2499136326;
// System.Collections.Specialized.HybridDictionary
struct HybridDictionary_t290043810;
// System.Collections.IDictionary
struct IDictionary_t596158605;
// System.Collections.ICollection
struct ICollection_t91669223;
// System.Array
struct Il2CppArray;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t259680273;
// System.Collections.Specialized.ListDictionary
struct ListDictionary_t3458713452;
// System.Collections.IComparer
struct IComparer_t3952557350;
// System.Collections.Specialized.ListDictionary/DictionaryNode
struct DictionaryNode_t2725637098;
// System.Collections.Specialized.ListDictionary/DictionaryNodeCollection
struct DictionaryNodeCollection_t528898270;
// System.Collections.Specialized.ListDictionary/DictionaryNodeCollection/DictionaryNodeCollectionEnumerator
struct DictionaryNodeCollectionEnumerator_t2037848305;
// System.Collections.Specialized.ListDictionary/DictionaryNodeEnumerator
struct DictionaryNodeEnumerator_t1923170152;
// System.Collections.Specialized.NameObjectCollectionBase
struct NameObjectCollectionBase_t2034248631;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// System.Collections.Specialized.NameObjectCollectionBase/KeysCollection
struct KeysCollection_t633582367;
// System.Collections.Specialized.NameObjectCollectionBase/_Item
struct _Item_t3244489099;
// System.Collections.Specialized.NameObjectCollectionBase/_KeysEnumerator
struct _KeysEnumerator_t1718269396;
// System.Collections.Specialized.NameValueCollection
struct NameValueCollection_t3047564564;
// System.Collections.ArrayList
struct ArrayList_t4252133567;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Collections.Specialized.StringCollection
struct StringCollection_t352985975;
// System.Collections.Specialized.StringEnumerator
struct StringEnumerator_t441637433;
// System.Collections.Specialized.StringDictionary
struct StringDictionary_t1070889667;
// System.ComponentModel.BrowsableAttribute
struct BrowsableAttribute_t2487167291;
// System.ComponentModel.CategoryAttribute
struct CategoryAttribute_t540457070;
// System.ComponentModel.CollectionChangeEventArgs
struct CollectionChangeEventArgs_t1734749345;
// System.ComponentModel.CollectionChangeEventHandler
struct CollectionChangeEventHandler_t790626706;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// System.ComponentModel.Component
struct Component_t2826673791;
// System.ComponentModel.EventHandlerList
struct EventHandlerList_t1298116880;
// System.ComponentModel.DefaultEventAttribute
struct DefaultEventAttribute_t1079704873;
// System.ComponentModel.DefaultPropertyAttribute
struct DefaultPropertyAttribute_t1962767338;
// System.ComponentModel.DefaultValueAttribute
struct DefaultValueAttribute_t1302720498;
// System.ComponentModel.DescriptionAttribute
struct DescriptionAttribute_t3207779672;
// System.ComponentModel.DesignerAttribute
struct DesignerAttribute_t2778719479;
// System.Type
struct Type_t;
// System.ComponentModel.DesignerCategoryAttribute
struct DesignerCategoryAttribute_t1270090451;
// System.ComponentModel.DesignerSerializationVisibilityAttribute
struct DesignerSerializationVisibilityAttribute_t2980019899;
// System.ComponentModel.DesignOnlyAttribute
struct DesignOnlyAttribute_t2394309572;
// System.ComponentModel.DesignTimeVisibleAttribute
struct DesignTimeVisibleAttribute_t2120749151;
// System.ComponentModel.EditorAttribute
struct EditorAttribute_t3559776959;
// System.ComponentModel.EditorBrowsableAttribute
struct EditorBrowsableAttribute_t1050682502;
// System.Delegate
struct Delegate_t3022476291;
// System.ComponentModel.ListEntry
struct ListEntry_t935815304;
// System.ComponentModel.InvalidEnumArgumentException
struct InvalidEnumArgumentException_t3709744516;
// System.ComponentModel.ListBindableAttribute
struct ListBindableAttribute_t1092011273;
// System.ComponentModel.ListChangedEventArgs
struct ListChangedEventArgs_t3132270315;
// System.ComponentModel.ListChangedEventHandler
struct ListChangedEventHandler_t2276411942;
// System.ComponentModel.MarshalByValueComponent
struct MarshalByValueComponent_t3997823175;
// System.ComponentModel.ISite
struct ISite_t1774720436;
// System.ComponentModel.MemberDescriptor
struct MemberDescriptor_t3749827553;
// System.ComponentModel.MergablePropertyAttribute
struct MergablePropertyAttribute_t1597820506;
// System.ComponentModel.PropertyChangedEventArgs
struct PropertyChangedEventArgs_t1689446432;
// System.ComponentModel.PropertyChangedEventHandler
struct PropertyChangedEventHandler_t3042952059;
// System.ComponentModel.PropertyDescriptor
struct PropertyDescriptor_t4250402154;
// System.ComponentModel.PropertyDescriptorCollection
struct PropertyDescriptorCollection_t3166009492;
// System.ComponentModel.PropertyDescriptor[]
struct PropertyDescriptorU5BU5D_t2049367471;
// System.ComponentModel.ReadOnlyAttribute
struct ReadOnlyAttribute_t4102148880;
// System.ComponentModel.RecommendedAsConfigurableAttribute
struct RecommendedAsConfigurableAttribute_t420947846;
// System.ComponentModel.RefreshEventArgs
struct RefreshEventArgs_t2077477224;
// System.ComponentModel.RefreshEventHandler
struct RefreshEventHandler_t456069287;
// System.ComponentModel.RefreshPropertiesAttribute
struct RefreshPropertiesAttribute_t2234294918;
// System.ComponentModel.ToolboxItemAttribute
struct ToolboxItemAttribute_t3063187714;
// System.ComponentModel.TypeConverterAttribute
struct TypeConverterAttribute_t252469870;
// System.ComponentModel.WeakObjectWrapper
struct WeakObjectWrapper_t2012978780;
// System.WeakReference
struct WeakReference_t1077405567;
// System.ComponentModel.WeakObjectWrapperComparer
struct WeakObjectWrapperComparer_t3891611113;
// System.ComponentModel.Win32Exception
struct Win32Exception_t1708275760;
// System.DefaultUriParser
struct DefaultUriParser_t1591960796;
// System.Diagnostics.Stopwatch
struct Stopwatch_t1380178105;
// System.Diagnostics.TraceListener
struct TraceListener_t3414949279;
// System.IO.Compression.DeflateStream
struct DeflateStream_t3198596725;
// System.IO.Stream
struct Stream_t3255436806;
// System.IO.Compression.DeflateStream/UnmanagedReadOrWrite
struct UnmanagedReadOrWrite_t1990215745;
// System.IO.Compression.DeflateStream/ReadMethod
struct ReadMethod_t3362229488;
// System.IO.Compression.DeflateStream/WriteMethod
struct WriteMethod_t1894833619;
// System.IO.Compression.GZipStream
struct GZipStream_t2274754946;
// System.MonoTODOAttribute
struct MonoTODOAttribute_t3487514020;
// System.Net.Authorization
struct Authorization_t1602399;
// System.Net.WebRequest
struct WebRequest_t1365124353;
// System.Net.ICredentials
struct ICredentials_t3855617113;
// System.Net.IAuthenticationModule
struct IAuthenticationModule_t3093891015;
// System.Net.BasicClient
struct BasicClient_t3996961659;
// System.Net.BindIPEndPoint
struct BindIPEndPoint_t635820671;
// System.Net.IPEndPoint
struct IPEndPoint_t2615413766;
// System.Net.ServicePoint
struct ServicePoint_t2765344313;
// System.Net.ChunkStream
struct ChunkStream_t91719323;
// System.Net.WebHeaderCollection
struct WebHeaderCollection_t3028142837;
// System.Net.ChunkStream/Chunk
struct Chunk_t3860501603;
// System.Net.Cookie
struct Cookie_t3154017544;
// System.Uri
struct Uri_t19570940;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.Net.CookieCollection
struct CookieCollection_t521422364;
// System.Collections.Generic.IList`1<System.Net.Cookie>
struct IList_1_t3694958145;
// System.Net.CookieCollection/CookieCollectionComparer
struct CookieCollectionComparer_t3570802680;
// System.Net.CookieContainer
struct CookieContainer_t2808809223;
// System.Net.CookieException
struct CookieException_t1505724635;
// System.Exception
struct Exception_t1927440687;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "System_U3CModuleU3E3783534214.h"
#include "System_U3CModuleU3E3783534214MethodDeclarations.h"
#include "System_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "System_U3CPrivateImplementationDetailsU3E1486305137MethodDeclarations.h"
#include "System_U3CPrivateImplementationDetailsU3E_U24Array3672778802.h"
#include "System_U3CPrivateImplementationDetailsU3E_U24Array3672778802MethodDeclarations.h"
#include "System_U3CPrivateImplementationDetailsU3E_U24ArrayT116038554.h"
#include "System_U3CPrivateImplementationDetailsU3E_U24ArrayT116038554MethodDeclarations.h"
#include "System_U3CPrivateImplementationDetailsU3E_U24Array1703410334.h"
#include "System_U3CPrivateImplementationDetailsU3E_U24Array1703410334MethodDeclarations.h"
#include "System_Locale4255929014.h"
#include "System_Locale4255929014MethodDeclarations.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "System_Mono_Security_X509_OSX509Certificates3584809896.h"
#include "System_Mono_Security_X509_OSX509Certificates3584809896MethodDeclarations.h"
#include "mscorlib_System_Void1841601450.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_IntPtr2504060609MethodDeclarations.h"
#include "mscorlib_System_Int322071877448.h"
#include "System_Mono_Security_X509_OSX509Certificates_SecTr1984565408.h"
#include "mscorlib_System_Byte3683104436.h"
#include "Mono_Security_Mono_Security_X509_X509CertificateCo3592472865.h"
#include "mscorlib_System_ArgumentNullException628810857MethodDeclarations.h"
#include "mscorlib_System_Collections_CollectionBase1101587467MethodDeclarations.h"
#include "Mono_Security_Mono_Security_X509_X509CertificateCo3592472865MethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException628810857.h"
#include "mscorlib_System_Collections_CollectionBase1101587467.h"
#include "Mono_Security_Mono_Security_X509_X509Certificate324051957.h"
#include "Mono_Security_Mono_Security_X509_X509Certificate324051957MethodDeclarations.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "System_Mono_Security_X509_OSX509Certificates_SecTr1984565408MethodDeclarations.h"
#include "System_System_Collections_Generic_RBTree1544615604.h"
#include "System_System_Collections_Generic_RBTree1544615604MethodDeclarations.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "System_System_Collections_Generic_RBTree_NodeEnumer648190100.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1868257458.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1868257458MethodDeclarations.h"
#include "System_System_Collections_Generic_RBTree_Node2499136326.h"
#include "mscorlib_System_UInt322149682021.h"
#include "System_System_Collections_Generic_RBTree_Node2499136326MethodDeclarations.h"
#include "System_System_Collections_Generic_RBTree_NodeEnumer648190100MethodDeclarations.h"
#include "mscorlib_System_SystemException3877406272MethodDeclarations.h"
#include "mscorlib_System_SystemException3877406272.h"
#include "System_System_Collections_Generic_Stack_1_gen3586864480.h"
#include "System_System_Collections_Generic_Stack_1_gen3586864480MethodDeclarations.h"
#include "mscorlib_System_ObjectDisposedException2695136451MethodDeclarations.h"
#include "mscorlib_System_InvalidOperationException721527559MethodDeclarations.h"
#include "mscorlib_System_ObjectDisposedException2695136451.h"
#include "mscorlib_System_InvalidOperationException721527559.h"
#include "System_System_Collections_Specialized_HybridDiction290043810.h"
#include "System_System_Collections_Specialized_HybridDiction290043810MethodDeclarations.h"
#include "mscorlib_System_Collections_CaseInsensitiveComparer157661140MethodDeclarations.h"
#include "mscorlib_System_Collections_CaseInsensitiveHashCod2307530285MethodDeclarations.h"
#include "System_System_Collections_Specialized_ListDictiona3458713452MethodDeclarations.h"
#include "mscorlib_System_Collections_Hashtable909839986MethodDeclarations.h"
#include "mscorlib_System_Collections_CaseInsensitiveComparer157661140.h"
#include "mscorlib_System_Collections_CaseInsensitiveHashCod2307530285.h"
#include "System_System_Collections_Specialized_ListDictiona3458713452.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"
#include "System_System_Collections_Specialized_ListDictiona2725637098.h"
#include "System_System_Collections_Specialized_ListDictiona1923170152MethodDeclarations.h"
#include "System_System_Collections_Specialized_ListDictiona1923170152.h"
#include "System_System_Collections_Specialized_ListDictiona2725637098MethodDeclarations.h"
#include "mscorlib_System_ArgumentOutOfRangeException279959794MethodDeclarations.h"
#include "mscorlib_System_Array3829468939MethodDeclarations.h"
#include "mscorlib_System_IndexOutOfRangeException3527622107MethodDeclarations.h"
#include "mscorlib_System_ArgumentException3259014390MethodDeclarations.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"
#include "mscorlib_System_ArgumentOutOfRangeException279959794.h"
#include "mscorlib_System_IndexOutOfRangeException3527622107.h"
#include "mscorlib_System_ArgumentException3259014390.h"
#include "System_System_Collections_Specialized_ListDictionar528898270MethodDeclarations.h"
#include "System_System_Collections_Specialized_ListDictionar528898270.h"
#include "System_System_Collections_Specialized_ListDictiona2037848305MethodDeclarations.h"
#include "System_System_Collections_Specialized_ListDictiona2037848305.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398MethodDeclarations.h"
#include "System_System_Collections_Specialized_NameObjectCo2034248631.h"
#include "System_System_Collections_Specialized_NameObjectCo2034248631MethodDeclarations.h"
#include "mscorlib_System_Runtime_Serialization_Serialization228987430.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"
#include "System_System_Collections_Specialized_NameObjectCol633582367.h"
#include "mscorlib_System_Collections_ArrayList4252133567MethodDeclarations.h"
#include "mscorlib_System_Collections_ArrayList4252133567.h"
#include "System_System_Collections_Specialized_NameObjectCo3244489099.h"
#include "System_System_Collections_Specialized_NameObjectCol633582367MethodDeclarations.h"
#include "System_System_Collections_Specialized_NameObjectCo1718269396MethodDeclarations.h"
#include "System_System_Collections_Specialized_NameObjectCo1718269396.h"
#include "mscorlib_System_Type1303803226MethodDeclarations.h"
#include "mscorlib_System_Runtime_Serialization_Serialization228987430MethodDeclarations.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_RuntimeTypeHandle2330101084.h"
#include "mscorlib_System_Runtime_Serialization_Serialization753258759MethodDeclarations.h"
#include "mscorlib_System_Runtime_Serialization_Serialization753258759.h"
#include "mscorlib_System_NotSupportedException1793819818MethodDeclarations.h"
#include "System_System_Collections_Specialized_NameObjectCo3244489099MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1793819818.h"
#include "System_System_Collections_Specialized_NameValueCol3047564564.h"
#include "System_System_Collections_Specialized_NameValueCol3047564564MethodDeclarations.h"
#include "mscorlib_System_Text_StringBuilder1221177846MethodDeclarations.h"
#include "mscorlib_System_Char3454481338.h"
#include "mscorlib_System_Text_StringBuilder1221177846.h"
#include "System_System_Collections_Specialized_StringCollect352985975.h"
#include "System_System_Collections_Specialized_StringCollect352985975MethodDeclarations.h"
#include "System_System_Collections_Specialized_StringEnumera441637433.h"
#include "System_System_Collections_Specialized_StringEnumera441637433MethodDeclarations.h"
#include "System_System_Collections_Specialized_StringDictio1070889667.h"
#include "System_System_Collections_Specialized_StringDictio1070889667MethodDeclarations.h"
#include "System_System_ComponentModel_BrowsableAttribute2487167291.h"
#include "System_System_ComponentModel_BrowsableAttribute2487167291MethodDeclarations.h"
#include "mscorlib_System_Attribute542643598MethodDeclarations.h"
#include "mscorlib_System_Boolean3825574718MethodDeclarations.h"
#include "System_System_ComponentModel_CategoryAttribute540457070.h"
#include "System_System_ComponentModel_CategoryAttribute540457070MethodDeclarations.h"
#include "System_System_ComponentModel_CollectionChangeActio3495844100.h"
#include "System_System_ComponentModel_CollectionChangeActio3495844100MethodDeclarations.h"
#include "System_System_ComponentModel_CollectionChangeEvent1734749345.h"
#include "System_System_ComponentModel_CollectionChangeEvent1734749345MethodDeclarations.h"
#include "mscorlib_System_EventArgs3289624707MethodDeclarations.h"
#include "System_System_ComponentModel_CollectionChangeEventH790626706.h"
#include "System_System_ComponentModel_CollectionChangeEventH790626706MethodDeclarations.h"
#include "mscorlib_System_AsyncCallback163412349.h"
#include "System_System_ComponentModel_Component2826673791.h"
#include "System_System_ComponentModel_Component2826673791MethodDeclarations.h"
#include "mscorlib_System_MarshalByRefObject1285298191MethodDeclarations.h"
#include "System_System_ComponentModel_EventHandlerList1298116880.h"
#include "System_System_ComponentModel_EventHandlerList1298116880MethodDeclarations.h"
#include "mscorlib_System_GC2902933594MethodDeclarations.h"
#include "mscorlib_System_EventHandler277755526MethodDeclarations.h"
#include "mscorlib_System_EventHandler277755526.h"
#include "mscorlib_System_Delegate3022476291.h"
#include "mscorlib_System_EventArgs3289624707.h"
#include "System_System_ComponentModel_ComponentConverter3121608223.h"
#include "System_System_ComponentModel_ComponentConverter3121608223MethodDeclarations.h"
#include "System_System_ComponentModel_DefaultEventAttribute1079704873.h"
#include "System_System_ComponentModel_DefaultEventAttribute1079704873MethodDeclarations.h"
#include "mscorlib_System_Attribute542643598.h"
#include "System_System_ComponentModel_DefaultPropertyAttrib1962767338.h"
#include "System_System_ComponentModel_DefaultPropertyAttrib1962767338MethodDeclarations.h"
#include "System_System_ComponentModel_DefaultValueAttribute1302720498.h"
#include "System_System_ComponentModel_DefaultValueAttribute1302720498MethodDeclarations.h"
#include "System_System_ComponentModel_DescriptionAttribute3207779672.h"
#include "System_System_ComponentModel_DescriptionAttribute3207779672MethodDeclarations.h"
#include "System_System_ComponentModel_DesignerAttribute2778719479.h"
#include "System_System_ComponentModel_DesignerAttribute2778719479MethodDeclarations.h"
#include "mscorlib_System_NullReferenceException3156209119MethodDeclarations.h"
#include "mscorlib_System_NullReferenceException3156209119.h"
#include "System_System_ComponentModel_DesignerCategoryAttri1270090451.h"
#include "System_System_ComponentModel_DesignerCategoryAttri1270090451MethodDeclarations.h"
#include "System_System_ComponentModel_DesignerSerialization3751360903.h"
#include "System_System_ComponentModel_DesignerSerialization3751360903MethodDeclarations.h"
#include "System_System_ComponentModel_DesignerSerialization2980019899.h"
#include "System_System_ComponentModel_DesignerSerialization2980019899MethodDeclarations.h"
#include "mscorlib_System_Enum2459695545MethodDeclarations.h"
#include "mscorlib_System_Enum2459695545.h"
#include "System_System_ComponentModel_DesignOnlyAttribute2394309572.h"
#include "System_System_ComponentModel_DesignOnlyAttribute2394309572MethodDeclarations.h"
#include "System_System_ComponentModel_DesignTimeVisibleAttr2120749151.h"
#include "System_System_ComponentModel_DesignTimeVisibleAttr2120749151MethodDeclarations.h"
#include "System_System_ComponentModel_EditorAttribute3559776959.h"
#include "System_System_ComponentModel_EditorAttribute3559776959MethodDeclarations.h"
#include "System_System_ComponentModel_EditorBrowsableAttrib1050682502.h"
#include "System_System_ComponentModel_EditorBrowsableAttrib1050682502MethodDeclarations.h"
#include "System_System_ComponentModel_EditorBrowsableState373498655.h"
#include "System_System_ComponentModel_EditorBrowsableState373498655MethodDeclarations.h"
#include "System_System_ComponentModel_ListEntry935815304.h"
#include "mscorlib_System_Delegate3022476291MethodDeclarations.h"
#include "System_System_ComponentModel_ListEntry935815304MethodDeclarations.h"
#include "System_System_ComponentModel_ExpandableObjectConve1139197709.h"
#include "System_System_ComponentModel_ExpandableObjectConve1139197709MethodDeclarations.h"
#include "System_System_ComponentModel_InvalidEnumArgumentEx3709744516.h"
#include "System_System_ComponentModel_InvalidEnumArgumentEx3709744516MethodDeclarations.h"
#include "System_System_ComponentModel_ListBindableAttribute1092011273.h"
#include "System_System_ComponentModel_ListBindableAttribute1092011273MethodDeclarations.h"
#include "System_System_ComponentModel_ListChangedEventArgs3132270315.h"
#include "System_System_ComponentModel_ListChangedEventArgs3132270315MethodDeclarations.h"
#include "System_System_ComponentModel_ListChangedType3463990274.h"
#include "System_System_ComponentModel_ListChangedEventHandl2276411942.h"
#include "System_System_ComponentModel_ListChangedEventHandl2276411942MethodDeclarations.h"
#include "System_System_ComponentModel_ListChangedType3463990274MethodDeclarations.h"
#include "System_System_ComponentModel_ListSortDirection4186912589.h"
#include "System_System_ComponentModel_ListSortDirection4186912589MethodDeclarations.h"
#include "System_System_ComponentModel_MarshalByValueCompone3997823175.h"
#include "System_System_ComponentModel_MarshalByValueCompone3997823175MethodDeclarations.h"
#include "System_System_ComponentModel_MemberDescriptor3749827553.h"
#include "System_System_ComponentModel_MemberDescriptor3749827553MethodDeclarations.h"
#include "System_System_ComponentModel_MergablePropertyAttri1597820506.h"
#include "System_System_ComponentModel_MergablePropertyAttri1597820506MethodDeclarations.h"
#include "System_System_ComponentModel_PropertyChangedEventA1689446432.h"
#include "System_System_ComponentModel_PropertyChangedEventA1689446432MethodDeclarations.h"
#include "System_System_ComponentModel_PropertyChangedEventH3042952059.h"
#include "System_System_ComponentModel_PropertyChangedEventH3042952059MethodDeclarations.h"
#include "System_System_ComponentModel_PropertyDescriptor4250402154.h"
#include "System_System_ComponentModel_PropertyDescriptor4250402154MethodDeclarations.h"
#include "System_System_ComponentModel_PropertyDescriptorCol3166009492.h"
#include "System_System_ComponentModel_PropertyDescriptorCol3166009492MethodDeclarations.h"
#include "System_ArrayTypes.h"
#include "mscorlib_System_NotImplementedException2785117854MethodDeclarations.h"
#include "mscorlib_System_NotImplementedException2785117854.h"
#include "mscorlib_System_StringComparison2376310518.h"
#include "System_System_ComponentModel_ReadOnlyAttribute4102148880.h"
#include "System_System_ComponentModel_ReadOnlyAttribute4102148880MethodDeclarations.h"
#include "System_System_ComponentModel_RecommendedAsConfigura420947846.h"
#include "System_System_ComponentModel_RecommendedAsConfigura420947846MethodDeclarations.h"
#include "System_System_ComponentModel_ReferenceConverter3131270729.h"
#include "System_System_ComponentModel_ReferenceConverter3131270729MethodDeclarations.h"
#include "System_System_ComponentModel_RefreshEventArgs2077477224.h"
#include "System_System_ComponentModel_RefreshEventArgs2077477224MethodDeclarations.h"
#include "System_System_ComponentModel_RefreshEventHandler456069287.h"
#include "System_System_ComponentModel_RefreshEventHandler456069287MethodDeclarations.h"
#include "System_System_ComponentModel_RefreshProperties2240171922.h"
#include "System_System_ComponentModel_RefreshProperties2240171922MethodDeclarations.h"
#include "System_System_ComponentModel_RefreshPropertiesAttr2234294918.h"
#include "System_System_ComponentModel_RefreshPropertiesAttr2234294918MethodDeclarations.h"
#include "System_System_ComponentModel_StringConverter3749524419.h"
#include "System_System_ComponentModel_StringConverter3749524419MethodDeclarations.h"
#include "System_System_ComponentModel_ToolboxItemAttribute3063187714.h"
#include "System_System_ComponentModel_ToolboxItemAttribute3063187714MethodDeclarations.h"
#include "System_System_ComponentModel_TypeConverter745995970.h"
#include "System_System_ComponentModel_TypeConverter745995970MethodDeclarations.h"
#include "System_System_ComponentModel_TypeConverterAttribute252469870.h"
#include "System_System_ComponentModel_TypeConverterAttribute252469870MethodDeclarations.h"
#include "System_System_ComponentModel_TypeDescriptionProvid2438624375.h"
#include "System_System_ComponentModel_TypeDescriptionProvid2438624375MethodDeclarations.h"
#include "System_System_ComponentModel_TypeDescriptor3595688691.h"
#include "System_System_ComponentModel_TypeDescriptor3595688691MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge385723205MethodDeclarations.h"
#include "System_System_ComponentModel_WeakObjectWrapperComp3891611113MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1208943611MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge385723205.h"
#include "System_System_ComponentModel_WeakObjectWrapperComp3891611113.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1208943611.h"
#include "mscorlib_System_Threading_Monitor3228523394MethodDeclarations.h"
#include "System_System_ComponentModel_WeakObjectWrapper2012978780.h"
#include "System_System_ComponentModel_WeakObjectWrapper2012978780MethodDeclarations.h"
#include "mscorlib_System_WeakReference1077405567.h"
#include "mscorlib_System_Collections_Generic_EqualityCompare586614051MethodDeclarations.h"
#include "mscorlib_System_WeakReference1077405567MethodDeclarations.h"
#include "System_System_ComponentModel_Win32Exception1708275760.h"
#include "System_System_ComponentModel_Win32Exception1708275760MethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_Marshal785896760MethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_ExternalEx1252662682MethodDeclarations.h"
#include "mscorlib_System_Exception1927440687MethodDeclarations.h"
#include "mscorlib_System_Exception1927440687.h"
#include "System_System_DefaultUriParser1591960796.h"
#include "System_System_DefaultUriParser1591960796MethodDeclarations.h"
#include "System_System_UriParser1012511323MethodDeclarations.h"
#include "System_System_UriParser1012511323.h"
#include "System_System_Diagnostics_Stopwatch1380178105.h"
#include "System_System_Diagnostics_Stopwatch1380178105MethodDeclarations.h"
#include "mscorlib_System_Int64909078037.h"
#include "System_System_Diagnostics_TraceListener3414949279.h"
#include "System_System_Diagnostics_TraceListener3414949279MethodDeclarations.h"
#include "System_System_GenericUriParser2599285286.h"
#include "System_System_GenericUriParser2599285286MethodDeclarations.h"
#include "System_System_IO_Compression_CompressionMode1471062003.h"
#include "System_System_IO_Compression_CompressionMode1471062003MethodDeclarations.h"
#include "System_System_IO_Compression_DeflateStream3198596725.h"
#include "System_System_IO_Compression_DeflateStream3198596725MethodDeclarations.h"
#include "mscorlib_System_IO_Stream3255436806.h"
#include "mscorlib_System_IO_Stream3255436806MethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_GCHandle3409268066MethodDeclarations.h"
#include "System_System_IO_Compression_DeflateStream_Unmanag1990215745MethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_GCHandle3409268066.h"
#include "System_System_IO_Compression_DeflateStream_Unmanag1990215745.h"
#include "mscorlib_System_Math2022911894MethodDeclarations.h"
#include "mscorlib_System_IO_IOException2458421087MethodDeclarations.h"
#include "mscorlib_System_IO_IOException2458421087.h"
#include "System_System_IO_Compression_DeflateStream_ReadMet3362229488MethodDeclarations.h"
#include "System_System_IO_Compression_DeflateStream_ReadMet3362229488.h"
#include "System_System_IO_Compression_DeflateStream_WriteMe1894833619MethodDeclarations.h"
#include "System_System_IO_Compression_DeflateStream_WriteMe1894833619.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_AsyncRe2232356043.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_AsyncRe2232356043MethodDeclarations.h"
#include "mscorlib_System_IO_SeekOrigin2475945306.h"
#include "System_System_IO_Compression_GZipStream2274754946.h"
#include "System_System_IO_Compression_GZipStream2274754946MethodDeclarations.h"
#include "System_System_MonoTODOAttribute3487514019.h"
#include "System_System_MonoTODOAttribute3487514019MethodDeclarations.h"
#include "System_System_Net_AuthenticationManager3410876775.h"
#include "System_System_Net_AuthenticationManager3410876775MethodDeclarations.h"
#include "System_System_Net_BasicClient3996961659MethodDeclarations.h"
#include "System_System_Net_DigestClient4126467897MethodDeclarations.h"
#include "System_System_Net_BasicClient3996961659.h"
#include "System_System_Net_DigestClient4126467897.h"
#include "System_System_Net_WebRequest1365124353.h"
#include "System_System_Net_Authorization1602399.h"
#include "System_System_Net_Authorization1602399MethodDeclarations.h"
#include "System_System_Net_HttpWebRequest1951404513MethodDeclarations.h"
#include "System_System_Net_NetworkCredential1714133953MethodDeclarations.h"
#include "mscorlib_System_Convert2607082565MethodDeclarations.h"
#include "System_System_Net_HttpWebRequest1951404513.h"
#include "System_System_Net_NetworkCredential1714133953.h"
#include "System_System_Uri19570940.h"
#include "System_System_Net_BindIPEndPoint635820671.h"
#include "System_System_Net_BindIPEndPoint635820671MethodDeclarations.h"
#include "System_System_Net_ServicePoint2765344313.h"
#include "System_System_Net_IPEndPoint2615413766.h"
#include "System_System_Net_ChunkStream91719323.h"
#include "System_System_Net_ChunkStream91719323MethodDeclarations.h"
#include "System_System_Net_WebHeaderCollection3028142837.h"
#include "System_System_Net_ChunkStream_Chunk3860501603MethodDeclarations.h"
#include "System_System_Net_ChunkStream_Chunk3860501603.h"
#include "System_System_Net_ChunkStream_State4001596355.h"
#include "mscorlib_System_Buffer3497320070MethodDeclarations.h"
#include "mscorlib_System_Int322071877448MethodDeclarations.h"
#include "mscorlib_System_Globalization_NumberStyles3408984435.h"
#include "mscorlib_System_IO_StringReader1480123486MethodDeclarations.h"
#include "System_System_Net_WebHeaderCollection3028142837MethodDeclarations.h"
#include "mscorlib_System_IO_StringReader1480123486.h"
#include "System_System_Net_WebException3368933679MethodDeclarations.h"
#include "System_System_Net_WebException3368933679.h"
#include "System_System_Net_WebExceptionStatus1169373531.h"
#include "System_System_Net_WebResponse1895226051.h"
#include "System_System_Net_ChunkStream_State4001596355MethodDeclarations.h"
#include "System_System_Net_Cookie3154017544.h"
#include "System_System_Net_Cookie3154017544MethodDeclarations.h"
#include "mscorlib_System_DateTime693205669MethodDeclarations.h"
#include "mscorlib_System_DateTime693205669.h"
#include "mscorlib_System_Runtime_CompilerServices_RuntimeHel266230107MethodDeclarations.h"
#include "mscorlib_System_RuntimeFieldHandle2331729674.h"
#include "System_System_Net_CookieException1505724635MethodDeclarations.h"
#include "System_System_Net_CookieException1505724635.h"
#include "mscorlib_System_Globalization_CultureInfo3500843524MethodDeclarations.h"
#include "mscorlib_System_Globalization_CultureInfo3500843524.h"
#include "System_System_Uri19570940MethodDeclarations.h"
#include "System_System_Net_CookieCollection521422364.h"
#include "System_System_Net_CookieCollection521422364MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2523138676MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2523138676.h"
#include "System_System_Net_CookieCollection_CookieCollectio3570802680MethodDeclarations.h"
#include "System_System_Net_CookieCollection_CookieCollectio3570802680.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2057868350.h"
#include "System_System_Net_CookieContainer2808809223.h"
#include "System_System_Net_CookieContainer2808809223MethodDeclarations.h"
#include "mscorlib_System_Globalization_CompareInfo2310920157.h"
#include "mscorlib_System_Globalization_CompareInfo2310920157MethodDeclarations.h"
#include "mscorlib_System_Globalization_CompareOptions2829943955.h"
#include "mscorlib_System_FormatException2948921286MethodDeclarations.h"

// System.Int32 System.Array::IndexOf<System.Int32>(!!0[],!!0)
extern "C"  int32_t Array_IndexOf_TisInt32_t2071877448_m1686802248_gshared (Il2CppObject * __this /* static, unused */, Int32U5BU5D_t3030399641* p0, int32_t p1, const MethodInfo* method);
#define Array_IndexOf_TisInt32_t2071877448_m1686802248(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, Int32U5BU5D_t3030399641*, int32_t, const MethodInfo*))Array_IndexOf_TisInt32_t2071877448_m1686802248_gshared)(__this /* static, unused */, p0, p1, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String Locale::GetText(System.String)
extern "C"  String_t* Locale_GetText_m4034107474 (Il2CppObject * __this /* static, unused */, String_t* ___msg0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___msg0;
		return L_0;
	}
}
// System.String Locale::GetText(System.String,System.Object[])
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Locale_GetText_m1445803604_MetadataUsageId;
extern "C"  String_t* Locale_GetText_m1445803604 (Il2CppObject * __this /* static, unused */, String_t* ___fmt0, ObjectU5BU5D_t3614634134* ___args1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Locale_GetText_m1445803604_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___fmt0;
		ObjectU5BU5D_t3614634134* L_1 = ___args1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Format_m1263743648(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void Mono.Security.X509.OSX509Certificates::.cctor()
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern Il2CppClass* OSX509Certificates_t3584809896_il2cpp_TypeInfo_var;
extern const uint32_t OSX509Certificates__cctor_m1823235584_MetadataUsageId;
extern "C"  void OSX509Certificates__cctor_m1823235584 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OSX509Certificates__cctor_m1823235584_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		IntPtr_t L_1 = OSX509Certificates_SecPolicyCreateSSL_m1432818148(NULL /*static, unused*/, 0, L_0, /*hidden argument*/NULL);
		((OSX509Certificates_t3584809896_StaticFields*)OSX509Certificates_t3584809896_il2cpp_TypeInfo_var->static_fields)->set_sslsecpolicy_0(L_1);
		return;
	}
}
// System.IntPtr Mono.Security.X509.OSX509Certificates::SecCertificateCreateWithData(System.IntPtr,System.IntPtr)
extern "C"  IntPtr_t OSX509Certificates_SecCertificateCreateWithData_m2926687940 (Il2CppObject * __this /* static, unused */, IntPtr_t ___allocator0, IntPtr_t ___nsdataRef1, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, intptr_t);
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(intptr_t) + sizeof(intptr_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("/System/Library/Frameworks/Security.framework/Security"), "SecCertificateCreateWithData", IL2CPP_CALL_DEFAULT, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'SecCertificateCreateWithData'"));
		}
	}

	// Native function invocation
	intptr_t returnValue = il2cppPInvokeFunc(reinterpret_cast<intptr_t>((___allocator0).get_m_value_0()), reinterpret_cast<intptr_t>((___nsdataRef1).get_m_value_0()));

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)(returnValue)));

	return _returnValue_unmarshaled;
}
// System.Int32 Mono.Security.X509.OSX509Certificates::SecTrustCreateWithCertificates(System.IntPtr,System.IntPtr,System.IntPtr&)
extern "C"  int32_t OSX509Certificates_SecTrustCreateWithCertificates_m525119422 (Il2CppObject * __this /* static, unused */, IntPtr_t ___certOrCertArray0, IntPtr_t ___policies1, IntPtr_t* ___sectrustref2, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, intptr_t, intptr_t*);
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(intptr_t) + sizeof(intptr_t) + sizeof(intptr_t*);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("/System/Library/Frameworks/Security.framework/Security"), "SecTrustCreateWithCertificates", IL2CPP_CALL_DEFAULT, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'SecTrustCreateWithCertificates'"));
		}
	}

	// Marshaling of parameter '___sectrustref2' to native representation
	intptr_t ____sectrustref2_empty = 0;

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc(reinterpret_cast<intptr_t>((___certOrCertArray0).get_m_value_0()), reinterpret_cast<intptr_t>((___policies1).get_m_value_0()), &____sectrustref2_empty);

	// Marshaling of parameter '___sectrustref2' back from native representation
	IntPtr_t ___sectrustref2_temp;
	___sectrustref2_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)(____sectrustref2_empty)));
	*___sectrustref2 = ___sectrustref2_temp;

	return returnValue;
}
// System.IntPtr Mono.Security.X509.OSX509Certificates::SecPolicyCreateSSL(System.Int32,System.IntPtr)
extern "C"  IntPtr_t OSX509Certificates_SecPolicyCreateSSL_m1432818148 (Il2CppObject * __this /* static, unused */, int32_t ___server0, IntPtr_t ___cfStringHostname1, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (int32_t, intptr_t);
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(int32_t) + sizeof(intptr_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("/System/Library/Frameworks/Security.framework/Security"), "SecPolicyCreateSSL", IL2CPP_CALL_DEFAULT, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'SecPolicyCreateSSL'"));
		}
	}

	// Native function invocation
	intptr_t returnValue = il2cppPInvokeFunc(___server0, reinterpret_cast<intptr_t>((___cfStringHostname1).get_m_value_0()));

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)(returnValue)));

	return _returnValue_unmarshaled;
}
// System.Int32 Mono.Security.X509.OSX509Certificates::SecTrustEvaluate(System.IntPtr,Mono.Security.X509.OSX509Certificates/SecTrustResult&)
extern "C"  int32_t OSX509Certificates_SecTrustEvaluate_m1792892941 (Il2CppObject * __this /* static, unused */, IntPtr_t ___secTrustRef0, int32_t* ___secTrustResultTime1, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, int32_t*);
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(intptr_t) + sizeof(int32_t*);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("/System/Library/Frameworks/Security.framework/Security"), "SecTrustEvaluate", IL2CPP_CALL_DEFAULT, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'SecTrustEvaluate'"));
		}
	}

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc(reinterpret_cast<intptr_t>((___secTrustRef0).get_m_value_0()), ___secTrustResultTime1);

	return returnValue;
}
// System.IntPtr Mono.Security.X509.OSX509Certificates::CFDataCreate(System.IntPtr,System.Byte*,System.IntPtr)
extern "C"  IntPtr_t OSX509Certificates_CFDataCreate_m216413830 (Il2CppObject * __this /* static, unused */, IntPtr_t ___allocator0, uint8_t* ___bytes1, IntPtr_t ___length2, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, uint8_t*, intptr_t);
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(intptr_t) + sizeof(uint8_t*) + sizeof(intptr_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("/System/Library/Frameworks/CoreFoundation.framework/CoreFoundation"), "CFDataCreate", IL2CPP_CALL_DEFAULT, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'CFDataCreate'"));
		}
	}

	// Native function invocation
	intptr_t returnValue = il2cppPInvokeFunc(reinterpret_cast<intptr_t>((___allocator0).get_m_value_0()), ___bytes1, reinterpret_cast<intptr_t>((___length2).get_m_value_0()));

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)(returnValue)));

	return _returnValue_unmarshaled;
}
// System.Void Mono.Security.X509.OSX509Certificates::CFRelease(System.IntPtr)
extern "C"  void OSX509Certificates_CFRelease_m2060304737 (Il2CppObject * __this /* static, unused */, IntPtr_t ___handle0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t);
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(intptr_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("/System/Library/Frameworks/CoreFoundation.framework/CoreFoundation"), "CFRelease", IL2CPP_CALL_DEFAULT, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'CFRelease'"));
		}
	}

	// Native function invocation
	il2cppPInvokeFunc(reinterpret_cast<intptr_t>((___handle0).get_m_value_0()));

}
// System.IntPtr Mono.Security.X509.OSX509Certificates::CFArrayCreate(System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr)
extern "C"  IntPtr_t OSX509Certificates_CFArrayCreate_m2541019176 (Il2CppObject * __this /* static, unused */, IntPtr_t ___allocator0, IntPtr_t ___values1, IntPtr_t ___numValues2, IntPtr_t ___callbacks3, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, intptr_t, intptr_t, intptr_t);
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(intptr_t) + sizeof(intptr_t) + sizeof(intptr_t) + sizeof(intptr_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("/System/Library/Frameworks/CoreFoundation.framework/CoreFoundation"), "CFArrayCreate", IL2CPP_CALL_DEFAULT, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'CFArrayCreate'"));
		}
	}

	// Native function invocation
	intptr_t returnValue = il2cppPInvokeFunc(reinterpret_cast<intptr_t>((___allocator0).get_m_value_0()), reinterpret_cast<intptr_t>((___values1).get_m_value_0()), reinterpret_cast<intptr_t>((___numValues2).get_m_value_0()), reinterpret_cast<intptr_t>((___callbacks3).get_m_value_0()));

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)(returnValue)));

	return _returnValue_unmarshaled;
}
// System.IntPtr Mono.Security.X509.OSX509Certificates::MakeCFData(System.Byte[])
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern Il2CppClass* OSX509Certificates_t3584809896_il2cpp_TypeInfo_var;
extern const uint32_t OSX509Certificates_MakeCFData_m3157530988_MetadataUsageId;
extern "C"  IntPtr_t OSX509Certificates_MakeCFData_m3157530988 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OSX509Certificates_MakeCFData_m3157530988_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint8_t* V_0 = NULL;
	{
		ByteU5BU5D_t3397334013* L_0 = ___data0;
		NullCheck(L_0);
		V_0 = (uint8_t*)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)));
		IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		uint8_t* L_2 = V_0;
		ByteU5BU5D_t3397334013* L_3 = ___data0;
		NullCheck(L_3);
		IntPtr_t L_4 = IntPtr_op_Explicit_m3896766622(NULL /*static, unused*/, (((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length)))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(OSX509Certificates_t3584809896_il2cpp_TypeInfo_var);
		IntPtr_t L_5 = OSX509Certificates_CFDataCreate_m216413830(NULL /*static, unused*/, L_1, (uint8_t*)(uint8_t*)L_2, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.IntPtr Mono.Security.X509.OSX509Certificates::FromIntPtrs(System.IntPtr[])
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern Il2CppClass* OSX509Certificates_t3584809896_il2cpp_TypeInfo_var;
extern const uint32_t OSX509Certificates_FromIntPtrs_m1498809044_MetadataUsageId;
extern "C"  IntPtr_t OSX509Certificates_FromIntPtrs_m1498809044 (Il2CppObject * __this /* static, unused */, IntPtrU5BU5D_t169632028* ___values0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OSX509Certificates_FromIntPtrs_m1498809044_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IntPtr_t* V_0 = NULL;
	uintptr_t G_B4_0 = 0;
	{
		IntPtrU5BU5D_t169632028* L_0 = ___values0;
		if (!L_0)
		{
			goto IL_000e;
		}
	}
	{
		IntPtrU5BU5D_t169632028* L_1 = ___values0;
		NullCheck(L_1);
		if ((((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length)))))
		{
			goto IL_0015;
		}
	}

IL_000e:
	{
		G_B4_0 = (((uintptr_t)0));
		goto IL_001c;
	}

IL_0015:
	{
		IntPtrU5BU5D_t169632028* L_2 = ___values0;
		NullCheck(L_2);
		G_B4_0 = ((uintptr_t)(((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))));
	}

IL_001c:
	{
		V_0 = (IntPtr_t*)G_B4_0;
		IntPtr_t L_3 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		IntPtr_t* L_4 = V_0;
		IntPtr_t L_5 = IntPtr_op_Explicit_m1401226878(NULL /*static, unused*/, (void*)(void*)L_4, /*hidden argument*/NULL);
		IntPtrU5BU5D_t169632028* L_6 = ___values0;
		NullCheck(L_6);
		IntPtr_t L_7 = IntPtr_op_Explicit_m3896766622(NULL /*static, unused*/, (((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length)))), /*hidden argument*/NULL);
		IntPtr_t L_8 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		IL2CPP_RUNTIME_CLASS_INIT(OSX509Certificates_t3584809896_il2cpp_TypeInfo_var);
		IntPtr_t L_9 = OSX509Certificates_CFArrayCreate_m2541019176(NULL /*static, unused*/, L_3, L_5, L_7, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// Mono.Security.X509.OSX509Certificates/SecTrustResult Mono.Security.X509.OSX509Certificates::TrustEvaluateSsl(Mono.Security.X509.X509CertificateCollection)
extern Il2CppClass* OSX509Certificates_t3584809896_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t OSX509Certificates_TrustEvaluateSsl_m3202803325_MetadataUsageId;
extern "C"  int32_t OSX509Certificates_TrustEvaluateSsl_m3202803325 (Il2CppObject * __this /* static, unused */, X509CertificateCollection_t3592472866 * ___certificates0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OSX509Certificates_TrustEvaluateSsl_m3202803325_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			X509CertificateCollection_t3592472866 * L_0 = ___certificates0;
			IL2CPP_RUNTIME_CLASS_INIT(OSX509Certificates_t3584809896_il2cpp_TypeInfo_var);
			int32_t L_1 = OSX509Certificates__TrustEvaluateSsl_m3372339288(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
			V_0 = L_1;
			goto IL_001e;
		}

IL_000c:
		{
			; // IL_000c: leave IL_001e
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0011;
		throw e;
	}

CATCH_0011:
	{ // begin catch(System.Object)
		{
			V_0 = 3;
			goto IL_001e;
		}

IL_0019:
		{
			; // IL_0019: leave IL_001e
		}
	} // end catch (depth: 1)

IL_001e:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// Mono.Security.X509.OSX509Certificates/SecTrustResult Mono.Security.X509.OSX509Certificates::_TrustEvaluateSsl(Mono.Security.X509.X509CertificateCollection)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppClass* IntPtrU5BU5D_t169632028_il2cpp_TypeInfo_var;
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern Il2CppClass* OSX509Certificates_t3584809896_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1767150654;
extern const uint32_t OSX509Certificates__TrustEvaluateSsl_m3372339288_MetadataUsageId;
extern "C"  int32_t OSX509Certificates__TrustEvaluateSsl_m3372339288 (Il2CppObject * __this /* static, unused */, X509CertificateCollection_t3592472866 * ___certificates0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OSX509Certificates__TrustEvaluateSsl_m3372339288_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	IntPtrU5BU5D_t169632028* V_1 = NULL;
	IntPtrU5BU5D_t169632028* V_2 = NULL;
	IntPtr_t V_3;
	memset(&V_3, 0, sizeof(V_3));
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	IntPtr_t V_6;
	memset(&V_6, 0, sizeof(V_6));
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	int32_t V_10 = 0;
	int32_t V_11 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		X509CertificateCollection_t3592472866 * L_0 = ___certificates0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, _stringLiteral1767150654, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		X509CertificateCollection_t3592472866 * L_2 = ___certificates0;
		NullCheck(L_2);
		int32_t L_3 = CollectionBase_get_Count_m740218359(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = V_0;
		V_1 = ((IntPtrU5BU5D_t169632028*)SZArrayNew(IntPtrU5BU5D_t169632028_il2cpp_TypeInfo_var, (uint32_t)L_4));
		int32_t L_5 = V_0;
		V_2 = ((IntPtrU5BU5D_t169632028*)SZArrayNew(IntPtrU5BU5D_t169632028_il2cpp_TypeInfo_var, (uint32_t)L_5));
		IntPtr_t L_6 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		V_3 = L_6;
	}

IL_002c:
	try
	{ // begin try (depth: 1)
		{
			V_4 = 0;
			goto IL_0059;
		}

IL_0034:
		{
			IntPtrU5BU5D_t169632028* L_7 = V_1;
			int32_t L_8 = V_4;
			NullCheck(L_7);
			X509CertificateCollection_t3592472866 * L_9 = ___certificates0;
			int32_t L_10 = V_4;
			NullCheck(L_9);
			X509Certificate_t324051958 * L_11 = X509CertificateCollection_get_Item_m2115598414(L_9, L_10, /*hidden argument*/NULL);
			NullCheck(L_11);
			ByteU5BU5D_t3397334013* L_12 = VirtFuncInvoker0< ByteU5BU5D_t3397334013* >::Invoke(12 /* System.Byte[] Mono.Security.X509.X509Certificate::get_RawData() */, L_11);
			IL2CPP_RUNTIME_CLASS_INIT(OSX509Certificates_t3584809896_il2cpp_TypeInfo_var);
			IntPtr_t L_13 = OSX509Certificates_MakeCFData_m3157530988(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
			(*(IntPtr_t*)((L_7)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_8)))) = L_13;
			int32_t L_14 = V_4;
			V_4 = ((int32_t)((int32_t)L_14+(int32_t)1));
		}

IL_0059:
		{
			int32_t L_15 = V_4;
			int32_t L_16 = V_0;
			if ((((int32_t)L_15) < ((int32_t)L_16)))
			{
				goto IL_0034;
			}
		}

IL_0061:
		{
			V_5 = 0;
			goto IL_00a5;
		}

IL_0069:
		{
			IntPtrU5BU5D_t169632028* L_17 = V_2;
			int32_t L_18 = V_5;
			NullCheck(L_17);
			IntPtr_t L_19 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
			IntPtrU5BU5D_t169632028* L_20 = V_1;
			int32_t L_21 = V_5;
			NullCheck(L_20);
			int32_t L_22 = L_21;
			IntPtr_t L_23 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_22));
			IL2CPP_RUNTIME_CLASS_INIT(OSX509Certificates_t3584809896_il2cpp_TypeInfo_var);
			IntPtr_t L_24 = OSX509Certificates_SecCertificateCreateWithData_m2926687940(NULL /*static, unused*/, L_19, L_23, /*hidden argument*/NULL);
			(*(IntPtr_t*)((L_17)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_18)))) = L_24;
			IntPtrU5BU5D_t169632028* L_25 = V_2;
			int32_t L_26 = V_5;
			NullCheck(L_25);
			int32_t L_27 = L_26;
			IntPtr_t L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
			IntPtr_t L_29 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
			bool L_30 = IntPtr_op_Equality_m1573482188(NULL /*static, unused*/, L_28, L_29, /*hidden argument*/NULL);
			if (!L_30)
			{
				goto IL_009f;
			}
		}

IL_0097:
		{
			V_11 = 3;
			IL2CPP_LEAVE(0x181, FINALLY_0101);
		}

IL_009f:
		{
			int32_t L_31 = V_5;
			V_5 = ((int32_t)((int32_t)L_31+(int32_t)1));
		}

IL_00a5:
		{
			int32_t L_32 = V_5;
			int32_t L_33 = V_0;
			if ((((int32_t)L_32) < ((int32_t)L_33)))
			{
				goto IL_0069;
			}
		}

IL_00ad:
		{
			IntPtrU5BU5D_t169632028* L_34 = V_2;
			IL2CPP_RUNTIME_CLASS_INIT(OSX509Certificates_t3584809896_il2cpp_TypeInfo_var);
			IntPtr_t L_35 = OSX509Certificates_FromIntPtrs_m1498809044(NULL /*static, unused*/, L_34, /*hidden argument*/NULL);
			V_3 = L_35;
			IntPtr_t L_36 = V_3;
			IntPtr_t L_37 = ((OSX509Certificates_t3584809896_StaticFields*)OSX509Certificates_t3584809896_il2cpp_TypeInfo_var->static_fields)->get_sslsecpolicy_0();
			int32_t L_38 = OSX509Certificates_SecTrustCreateWithCertificates_m525119422(NULL /*static, unused*/, L_36, L_37, (&V_6), /*hidden argument*/NULL);
			V_7 = L_38;
			int32_t L_39 = V_7;
			if (L_39)
			{
				goto IL_00f4;
			}
		}

IL_00ca:
		{
			IntPtr_t L_40 = V_6;
			IL2CPP_RUNTIME_CLASS_INIT(OSX509Certificates_t3584809896_il2cpp_TypeInfo_var);
			int32_t L_41 = OSX509Certificates_SecTrustEvaluate_m1792892941(NULL /*static, unused*/, L_40, (&V_8), /*hidden argument*/NULL);
			V_7 = L_41;
			int32_t L_42 = V_7;
			if (!L_42)
			{
				goto IL_00e4;
			}
		}

IL_00dc:
		{
			V_11 = 3;
			IL2CPP_LEAVE(0x181, FINALLY_0101);
		}

IL_00e4:
		{
			IntPtr_t L_43 = V_6;
			IL2CPP_RUNTIME_CLASS_INIT(OSX509Certificates_t3584809896_il2cpp_TypeInfo_var);
			OSX509Certificates_CFRelease_m2060304737(NULL /*static, unused*/, L_43, /*hidden argument*/NULL);
			int32_t L_44 = V_8;
			V_11 = L_44;
			IL2CPP_LEAVE(0x181, FINALLY_0101);
		}

IL_00f4:
		{
			V_11 = 3;
			IL2CPP_LEAVE(0x181, FINALLY_0101);
		}

IL_00fc:
		{
			; // IL_00fc: leave IL_0181
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0101;
	}

FINALLY_0101:
	{ // begin finally (depth: 1)
		{
			V_9 = 0;
			goto IL_012b;
		}

IL_0109:
		{
			IntPtrU5BU5D_t169632028* L_45 = V_1;
			int32_t L_46 = V_9;
			NullCheck(L_45);
			int32_t L_47 = L_46;
			IntPtr_t L_48 = (L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_47));
			IntPtr_t L_49 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
			bool L_50 = IntPtr_op_Inequality_m3044532593(NULL /*static, unused*/, L_48, L_49, /*hidden argument*/NULL);
			if (!L_50)
			{
				goto IL_0125;
			}
		}

IL_011c:
		{
			IntPtrU5BU5D_t169632028* L_51 = V_1;
			int32_t L_52 = V_9;
			NullCheck(L_51);
			int32_t L_53 = L_52;
			IntPtr_t L_54 = (L_51)->GetAt(static_cast<il2cpp_array_size_t>(L_53));
			IL2CPP_RUNTIME_CLASS_INIT(OSX509Certificates_t3584809896_il2cpp_TypeInfo_var);
			OSX509Certificates_CFRelease_m2060304737(NULL /*static, unused*/, L_54, /*hidden argument*/NULL);
		}

IL_0125:
		{
			int32_t L_55 = V_9;
			V_9 = ((int32_t)((int32_t)L_55+(int32_t)1));
		}

IL_012b:
		{
			int32_t L_56 = V_9;
			int32_t L_57 = V_0;
			if ((((int32_t)L_56) < ((int32_t)L_57)))
			{
				goto IL_0109;
			}
		}

IL_0133:
		{
			IntPtr_t L_58 = V_3;
			IntPtr_t L_59 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
			bool L_60 = IntPtr_op_Inequality_m3044532593(NULL /*static, unused*/, L_58, L_59, /*hidden argument*/NULL);
			if (!L_60)
			{
				goto IL_014e;
			}
		}

IL_0143:
		{
			IntPtr_t L_61 = V_3;
			IL2CPP_RUNTIME_CLASS_INIT(OSX509Certificates_t3584809896_il2cpp_TypeInfo_var);
			OSX509Certificates_CFRelease_m2060304737(NULL /*static, unused*/, L_61, /*hidden argument*/NULL);
			goto IL_0180;
		}

IL_014e:
		{
			V_10 = 0;
			goto IL_0178;
		}

IL_0156:
		{
			IntPtrU5BU5D_t169632028* L_62 = V_2;
			int32_t L_63 = V_10;
			NullCheck(L_62);
			int32_t L_64 = L_63;
			IntPtr_t L_65 = (L_62)->GetAt(static_cast<il2cpp_array_size_t>(L_64));
			IntPtr_t L_66 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
			bool L_67 = IntPtr_op_Inequality_m3044532593(NULL /*static, unused*/, L_65, L_66, /*hidden argument*/NULL);
			if (!L_67)
			{
				goto IL_0172;
			}
		}

IL_0169:
		{
			IntPtrU5BU5D_t169632028* L_68 = V_2;
			int32_t L_69 = V_10;
			NullCheck(L_68);
			int32_t L_70 = L_69;
			IntPtr_t L_71 = (L_68)->GetAt(static_cast<il2cpp_array_size_t>(L_70));
			IL2CPP_RUNTIME_CLASS_INIT(OSX509Certificates_t3584809896_il2cpp_TypeInfo_var);
			OSX509Certificates_CFRelease_m2060304737(NULL /*static, unused*/, L_71, /*hidden argument*/NULL);
		}

IL_0172:
		{
			int32_t L_72 = V_10;
			V_10 = ((int32_t)((int32_t)L_72+(int32_t)1));
		}

IL_0178:
		{
			int32_t L_73 = V_10;
			int32_t L_74 = V_0;
			if ((((int32_t)L_73) < ((int32_t)L_74)))
			{
				goto IL_0156;
			}
		}

IL_0180:
		{
			IL2CPP_END_FINALLY(257)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(257)
	{
		IL2CPP_JUMP_TBL(0x181, IL_0181)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0181:
	{
		int32_t L_75 = V_11;
		return L_75;
	}
}
// System.Void System.Collections.Generic.RBTree::.ctor(System.Object)
extern "C"  void RBTree__ctor_m2535208397 (RBTree_t1544615604 * __this, Il2CppObject * ___hlp0, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___hlp0;
		__this->set_hlp_1(L_0);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.RBTree/Node> System.Collections.Generic.RBTree::System.Collections.Generic.IEnumerable<System.Collections.Generic.RBTree.Node>.GetEnumerator()
extern Il2CppClass* NodeEnumerator_t648190100_il2cpp_TypeInfo_var;
extern const uint32_t RBTree_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_RBTree_NodeU3E_GetEnumerator_m3412557486_MetadataUsageId;
extern "C"  Il2CppObject* RBTree_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_RBTree_NodeU3E_GetEnumerator_m3412557486 (RBTree_t1544615604 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RBTree_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_RBTree_NodeU3E_GetEnumerator_m3412557486_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NodeEnumerator_t648190100  L_0 = RBTree_GetEnumerator_m3973894836(__this, /*hidden argument*/NULL);
		NodeEnumerator_t648190100  L_1 = L_0;
		Il2CppObject * L_2 = Box(NodeEnumerator_t648190100_il2cpp_TypeInfo_var, &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.RBTree::System.Collections.IEnumerable.GetEnumerator()
extern Il2CppClass* NodeEnumerator_t648190100_il2cpp_TypeInfo_var;
extern const uint32_t RBTree_System_Collections_IEnumerable_GetEnumerator_m1477090404_MetadataUsageId;
extern "C"  Il2CppObject * RBTree_System_Collections_IEnumerable_GetEnumerator_m1477090404 (RBTree_t1544615604 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RBTree_System_Collections_IEnumerable_GetEnumerator_m1477090404_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NodeEnumerator_t648190100  L_0 = RBTree_GetEnumerator_m3973894836(__this, /*hidden argument*/NULL);
		NodeEnumerator_t648190100  L_1 = L_0;
		Il2CppObject * L_2 = Box(NodeEnumerator_t648190100_il2cpp_TypeInfo_var, &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Collections.Generic.List`1<System.Collections.Generic.RBTree/Node> System.Collections.Generic.RBTree::alloc_path()
extern Il2CppClass* RBTree_t1544615604_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1868257458_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3050112466_MethodInfo_var;
extern const uint32_t RBTree_alloc_path_m3500173063_MetadataUsageId;
extern "C"  List_1_t1868257458 * RBTree_alloc_path_m3500173063 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RBTree_alloc_path_m3500173063_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t1868257458 * V_0 = NULL;
	{
		List_1_t1868257458 * L_0 = ((RBTree_t1544615604_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(RBTree_t1544615604_il2cpp_TypeInfo_var))->get_cached_path_3();
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		List_1_t1868257458 * L_1 = (List_1_t1868257458 *)il2cpp_codegen_object_new(List_1_t1868257458_il2cpp_TypeInfo_var);
		List_1__ctor_m3050112466(L_1, /*hidden argument*/List_1__ctor_m3050112466_MethodInfo_var);
		return L_1;
	}

IL_0010:
	{
		List_1_t1868257458 * L_2 = ((RBTree_t1544615604_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(RBTree_t1544615604_il2cpp_TypeInfo_var))->get_cached_path_3();
		V_0 = L_2;
		((RBTree_t1544615604_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(RBTree_t1544615604_il2cpp_TypeInfo_var))->set_cached_path_3((List_1_t1868257458 *)NULL);
		List_1_t1868257458 * L_3 = V_0;
		return L_3;
	}
}
// System.Void System.Collections.Generic.RBTree::release_path(System.Collections.Generic.List`1<System.Collections.Generic.RBTree/Node>)
extern Il2CppClass* RBTree_t1544615604_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Capacity_m1007087661_MethodInfo_var;
extern const MethodInfo* List_1_Clear_m2079221723_MethodInfo_var;
extern const uint32_t RBTree_release_path_m145089350_MetadataUsageId;
extern "C"  void RBTree_release_path_m145089350 (Il2CppObject * __this /* static, unused */, List_1_t1868257458 * ___path0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RBTree_release_path_m145089350_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1868257458 * L_0 = ((RBTree_t1544615604_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(RBTree_t1544615604_il2cpp_TypeInfo_var))->get_cached_path_3();
		if (!L_0)
		{
			goto IL_001f;
		}
	}
	{
		List_1_t1868257458 * L_1 = ((RBTree_t1544615604_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(RBTree_t1544615604_il2cpp_TypeInfo_var))->get_cached_path_3();
		NullCheck(L_1);
		int32_t L_2 = List_1_get_Capacity_m1007087661(L_1, /*hidden argument*/List_1_get_Capacity_m1007087661_MethodInfo_var);
		List_1_t1868257458 * L_3 = ___path0;
		NullCheck(L_3);
		int32_t L_4 = List_1_get_Capacity_m1007087661(L_3, /*hidden argument*/List_1_get_Capacity_m1007087661_MethodInfo_var);
		if ((((int32_t)L_2) >= ((int32_t)L_4)))
		{
			goto IL_002b;
		}
	}

IL_001f:
	{
		List_1_t1868257458 * L_5 = ___path0;
		NullCheck(L_5);
		List_1_Clear_m2079221723(L_5, /*hidden argument*/List_1_Clear_m2079221723_MethodInfo_var);
		List_1_t1868257458 * L_6 = ___path0;
		((RBTree_t1544615604_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(RBTree_t1544615604_il2cpp_TypeInfo_var))->set_cached_path_3(L_6);
	}

IL_002b:
	{
		return;
	}
}
// System.Void System.Collections.Generic.RBTree::Clear()
extern "C"  void RBTree_Clear_m452713382 (RBTree_t1544615604 * __this, const MethodInfo* method)
{
	{
		__this->set_root_0((Node_t2499136326 *)NULL);
		uint32_t L_0 = __this->get_version_2();
		__this->set_version_2(((int32_t)((int32_t)L_0+(int32_t)1)));
		return;
	}
}
// System.Int32 System.Collections.Generic.RBTree::get_Count()
extern "C"  int32_t RBTree_get_Count_m4248632779 (RBTree_t1544615604 * __this, const MethodInfo* method)
{
	uint32_t G_B3_0 = 0;
	{
		Node_t2499136326 * L_0 = __this->get_root_0();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		G_B3_0 = ((uint32_t)(0));
		goto IL_001c;
	}

IL_0011:
	{
		Node_t2499136326 * L_1 = __this->get_root_0();
		NullCheck(L_1);
		uint32_t L_2 = Node_get_Size_m3884002931(L_1, /*hidden argument*/NULL);
		G_B3_0 = L_2;
	}

IL_001c:
	{
		return G_B3_0;
	}
}
// System.Collections.Generic.RBTree/NodeEnumerator System.Collections.Generic.RBTree::GetEnumerator()
extern "C"  NodeEnumerator_t648190100  RBTree_GetEnumerator_m3973894836 (RBTree_t1544615604 * __this, const MethodInfo* method)
{
	{
		NodeEnumerator_t648190100  L_0;
		memset(&L_0, 0, sizeof(L_0));
		NodeEnumerator__ctor_m3411227341(&L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Collections.Generic.RBTree/Node System.Collections.Generic.RBTree::do_insert(System.Int32,System.Collections.Generic.RBTree/Node,System.Collections.Generic.List`1<System.Collections.Generic.RBTree/Node>)
extern Il2CppClass* SystemException_t3877406272_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Count_m1995942658_MethodInfo_var;
extern const MethodInfo* List_1_set_Item_m3689121816_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m2408083997_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1141062551;
extern const uint32_t RBTree_do_insert_m3330241496_MetadataUsageId;
extern "C"  Node_t2499136326 * RBTree_do_insert_m3330241496 (RBTree_t1544615604 * __this, int32_t ___in_tree_cmp0, Node_t2499136326 * ___current1, List_1_t1868257458 * ___path2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RBTree_do_insert_m3330241496_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Node_t2499136326 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		List_1_t1868257458 * L_0 = ___path2;
		List_1_t1868257458 * L_1 = ___path2;
		NullCheck(L_1);
		int32_t L_2 = List_1_get_Count_m1995942658(L_1, /*hidden argument*/List_1_get_Count_m1995942658_MethodInfo_var);
		Node_t2499136326 * L_3 = ___current1;
		NullCheck(L_0);
		List_1_set_Item_m3689121816(L_0, ((int32_t)((int32_t)L_2-(int32_t)1)), L_3, /*hidden argument*/List_1_set_Item_m3689121816_MethodInfo_var);
		List_1_t1868257458 * L_4 = ___path2;
		List_1_t1868257458 * L_5 = ___path2;
		NullCheck(L_5);
		int32_t L_6 = List_1_get_Count_m1995942658(L_5, /*hidden argument*/List_1_get_Count_m1995942658_MethodInfo_var);
		NullCheck(L_4);
		Node_t2499136326 * L_7 = List_1_get_Item_m2408083997(L_4, ((int32_t)((int32_t)L_6-(int32_t)3)), /*hidden argument*/List_1_get_Item_m2408083997_MethodInfo_var);
		V_0 = L_7;
		int32_t L_8 = ___in_tree_cmp0;
		if ((((int32_t)L_8) >= ((int32_t)0)))
		{
			goto IL_0031;
		}
	}
	{
		Node_t2499136326 * L_9 = V_0;
		Node_t2499136326 * L_10 = ___current1;
		NullCheck(L_9);
		L_9->set_left_0(L_10);
		goto IL_0038;
	}

IL_0031:
	{
		Node_t2499136326 * L_11 = V_0;
		Node_t2499136326 * L_12 = ___current1;
		NullCheck(L_11);
		L_11->set_right_1(L_12);
	}

IL_0038:
	{
		V_1 = 0;
		goto IL_0057;
	}

IL_003f:
	{
		List_1_t1868257458 * L_13 = ___path2;
		int32_t L_14 = V_1;
		NullCheck(L_13);
		Node_t2499136326 * L_15 = List_1_get_Item_m2408083997(L_13, L_14, /*hidden argument*/List_1_get_Item_m2408083997_MethodInfo_var);
		Node_t2499136326 * L_16 = L_15;
		NullCheck(L_16);
		uint32_t L_17 = Node_get_Size_m3884002931(L_16, /*hidden argument*/NULL);
		NullCheck(L_16);
		Node_set_Size_m2428994664(L_16, ((int32_t)((int32_t)L_17+(int32_t)1)), /*hidden argument*/NULL);
		int32_t L_18 = V_1;
		V_1 = ((int32_t)((int32_t)L_18+(int32_t)2));
	}

IL_0057:
	{
		int32_t L_19 = V_1;
		List_1_t1868257458 * L_20 = ___path2;
		NullCheck(L_20);
		int32_t L_21 = List_1_get_Count_m1995942658(L_20, /*hidden argument*/List_1_get_Count_m1995942658_MethodInfo_var);
		if ((((int32_t)L_19) < ((int32_t)((int32_t)((int32_t)L_21-(int32_t)2)))))
		{
			goto IL_003f;
		}
	}
	{
		Node_t2499136326 * L_22 = V_0;
		NullCheck(L_22);
		bool L_23 = Node_get_IsBlack_m2486375420(L_22, /*hidden argument*/NULL);
		if (L_23)
		{
			goto IL_0077;
		}
	}
	{
		List_1_t1868257458 * L_24 = ___path2;
		RBTree_rebalance_insert_m779337910(__this, L_24, /*hidden argument*/NULL);
	}

IL_0077:
	{
		Node_t2499136326 * L_25 = __this->get_root_0();
		NullCheck(L_25);
		bool L_26 = Node_get_IsBlack_m2486375420(L_25, /*hidden argument*/NULL);
		if (L_26)
		{
			goto IL_0092;
		}
	}
	{
		SystemException_t3877406272 * L_27 = (SystemException_t3877406272 *)il2cpp_codegen_object_new(SystemException_t3877406272_il2cpp_TypeInfo_var);
		SystemException__ctor_m4001391027(L_27, _stringLiteral1141062551, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_27);
	}

IL_0092:
	{
		uint32_t L_28 = __this->get_version_2();
		__this->set_version_2(((int32_t)((int32_t)L_28+(int32_t)1)));
		Node_t2499136326 * L_29 = ___current1;
		return L_29;
	}
}
// System.Collections.Generic.RBTree/Node System.Collections.Generic.RBTree::do_remove(System.Collections.Generic.List`1<System.Collections.Generic.RBTree/Node>)
extern Il2CppClass* SystemException_t3877406272_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Count_m1995942658_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m2408083997_MethodInfo_var;
extern const MethodInfo* List_1_Add_m3784024590_MethodInfo_var;
extern const MethodInfo* List_1_set_Item_m3689121816_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2110731492;
extern Il2CppCodeGenString* _stringLiteral2246167095;
extern const uint32_t RBTree_do_remove_m1300011114_MetadataUsageId;
extern "C"  Node_t2499136326 * RBTree_do_remove_m1300011114 (RBTree_t1544615604 * __this, List_1_t1868257458 * ___path0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RBTree_do_remove_m1300011114_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Node_t2499136326 * V_1 = NULL;
	Node_t2499136326 * V_2 = NULL;
	Node_t2499136326 * V_3 = NULL;
	Node_t2499136326 * V_4 = NULL;
	int32_t V_5 = 0;
	RBTree_t1544615604 * G_B10_0 = NULL;
	RBTree_t1544615604 * G_B9_0 = NULL;
	Node_t2499136326 * G_B11_0 = NULL;
	RBTree_t1544615604 * G_B11_1 = NULL;
	{
		List_1_t1868257458 * L_0 = ___path0;
		NullCheck(L_0);
		int32_t L_1 = List_1_get_Count_m1995942658(L_0, /*hidden argument*/List_1_get_Count_m1995942658_MethodInfo_var);
		V_0 = ((int32_t)((int32_t)L_1-(int32_t)1));
		List_1_t1868257458 * L_2 = ___path0;
		int32_t L_3 = V_0;
		NullCheck(L_2);
		Node_t2499136326 * L_4 = List_1_get_Item_m2408083997(L_2, L_3, /*hidden argument*/List_1_get_Item_m2408083997_MethodInfo_var);
		V_1 = L_4;
		Node_t2499136326 * L_5 = V_1;
		NullCheck(L_5);
		Node_t2499136326 * L_6 = L_5->get_left_0();
		if (!L_6)
		{
			goto IL_0062;
		}
	}
	{
		Node_t2499136326 * L_7 = V_1;
		NullCheck(L_7);
		Node_t2499136326 * L_8 = L_7->get_left_0();
		Node_t2499136326 * L_9 = V_1;
		NullCheck(L_9);
		Node_t2499136326 * L_10 = L_9->get_right_1();
		List_1_t1868257458 * L_11 = ___path0;
		Node_t2499136326 * L_12 = RBTree_right_most_m129601026(NULL /*static, unused*/, L_8, L_10, L_11, /*hidden argument*/NULL);
		V_2 = L_12;
		Node_t2499136326 * L_13 = V_1;
		Node_t2499136326 * L_14 = V_2;
		NullCheck(L_13);
		VirtActionInvoker1< Node_t2499136326 * >::Invoke(4 /* System.Void System.Collections.Generic.RBTree/Node::SwapValue(System.Collections.Generic.RBTree/Node) */, L_13, L_14);
		Node_t2499136326 * L_15 = V_2;
		NullCheck(L_15);
		Node_t2499136326 * L_16 = L_15->get_left_0();
		if (!L_16)
		{
			goto IL_005d;
		}
	}
	{
		Node_t2499136326 * L_17 = V_2;
		NullCheck(L_17);
		Node_t2499136326 * L_18 = L_17->get_left_0();
		V_3 = L_18;
		List_1_t1868257458 * L_19 = ___path0;
		NullCheck(L_19);
		List_1_Add_m3784024590(L_19, (Node_t2499136326 *)NULL, /*hidden argument*/List_1_Add_m3784024590_MethodInfo_var);
		List_1_t1868257458 * L_20 = ___path0;
		Node_t2499136326 * L_21 = V_3;
		NullCheck(L_20);
		List_1_Add_m3784024590(L_20, L_21, /*hidden argument*/List_1_Add_m3784024590_MethodInfo_var);
		Node_t2499136326 * L_22 = V_2;
		Node_t2499136326 * L_23 = V_3;
		NullCheck(L_22);
		VirtActionInvoker1< Node_t2499136326 * >::Invoke(4 /* System.Void System.Collections.Generic.RBTree/Node::SwapValue(System.Collections.Generic.RBTree/Node) */, L_22, L_23);
	}

IL_005d:
	{
		goto IL_008c;
	}

IL_0062:
	{
		Node_t2499136326 * L_24 = V_1;
		NullCheck(L_24);
		Node_t2499136326 * L_25 = L_24->get_right_1();
		if (!L_25)
		{
			goto IL_008c;
		}
	}
	{
		Node_t2499136326 * L_26 = V_1;
		NullCheck(L_26);
		Node_t2499136326 * L_27 = L_26->get_right_1();
		V_4 = L_27;
		List_1_t1868257458 * L_28 = ___path0;
		NullCheck(L_28);
		List_1_Add_m3784024590(L_28, (Node_t2499136326 *)NULL, /*hidden argument*/List_1_Add_m3784024590_MethodInfo_var);
		List_1_t1868257458 * L_29 = ___path0;
		Node_t2499136326 * L_30 = V_4;
		NullCheck(L_29);
		List_1_Add_m3784024590(L_29, L_30, /*hidden argument*/List_1_Add_m3784024590_MethodInfo_var);
		Node_t2499136326 * L_31 = V_1;
		Node_t2499136326 * L_32 = V_4;
		NullCheck(L_31);
		VirtActionInvoker1< Node_t2499136326 * >::Invoke(4 /* System.Void System.Collections.Generic.RBTree/Node::SwapValue(System.Collections.Generic.RBTree/Node) */, L_31, L_32);
	}

IL_008c:
	{
		List_1_t1868257458 * L_33 = ___path0;
		NullCheck(L_33);
		int32_t L_34 = List_1_get_Count_m1995942658(L_33, /*hidden argument*/List_1_get_Count_m1995942658_MethodInfo_var);
		V_0 = ((int32_t)((int32_t)L_34-(int32_t)1));
		List_1_t1868257458 * L_35 = ___path0;
		int32_t L_36 = V_0;
		NullCheck(L_35);
		Node_t2499136326 * L_37 = List_1_get_Item_m2408083997(L_35, L_36, /*hidden argument*/List_1_get_Item_m2408083997_MethodInfo_var);
		V_1 = L_37;
		Node_t2499136326 * L_38 = V_1;
		NullCheck(L_38);
		uint32_t L_39 = Node_get_Size_m3884002931(L_38, /*hidden argument*/NULL);
		if ((((int32_t)L_39) == ((int32_t)1)))
		{
			goto IL_00b4;
		}
	}
	{
		SystemException_t3877406272 * L_40 = (SystemException_t3877406272 *)il2cpp_codegen_object_new(SystemException_t3877406272_il2cpp_TypeInfo_var);
		SystemException__ctor_m4001391027(L_40, _stringLiteral2110731492, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_40);
	}

IL_00b4:
	{
		List_1_t1868257458 * L_41 = ___path0;
		int32_t L_42 = V_0;
		NullCheck(L_41);
		List_1_set_Item_m3689121816(L_41, L_42, (Node_t2499136326 *)NULL, /*hidden argument*/List_1_set_Item_m3689121816_MethodInfo_var);
		int32_t L_43 = V_0;
		G_B9_0 = __this;
		if (L_43)
		{
			G_B10_0 = __this;
			goto IL_00c9;
		}
	}
	{
		G_B11_0 = ((Node_t2499136326 *)(NULL));
		G_B11_1 = G_B9_0;
		goto IL_00d2;
	}

IL_00c9:
	{
		List_1_t1868257458 * L_44 = ___path0;
		int32_t L_45 = V_0;
		NullCheck(L_44);
		Node_t2499136326 * L_46 = List_1_get_Item_m2408083997(L_44, ((int32_t)((int32_t)L_45-(int32_t)2)), /*hidden argument*/List_1_get_Item_m2408083997_MethodInfo_var);
		G_B11_0 = L_46;
		G_B11_1 = G_B10_0;
	}

IL_00d2:
	{
		Node_t2499136326 * L_47 = V_1;
		NullCheck(G_B11_1);
		RBTree_node_reparent_m2851909385(G_B11_1, G_B11_0, L_47, 0, (Node_t2499136326 *)NULL, /*hidden argument*/NULL);
		V_5 = 0;
		goto IL_00fd;
	}

IL_00e2:
	{
		List_1_t1868257458 * L_48 = ___path0;
		int32_t L_49 = V_5;
		NullCheck(L_48);
		Node_t2499136326 * L_50 = List_1_get_Item_m2408083997(L_48, L_49, /*hidden argument*/List_1_get_Item_m2408083997_MethodInfo_var);
		Node_t2499136326 * L_51 = L_50;
		NullCheck(L_51);
		uint32_t L_52 = Node_get_Size_m3884002931(L_51, /*hidden argument*/NULL);
		NullCheck(L_51);
		Node_set_Size_m2428994664(L_51, ((int32_t)((int32_t)L_52-(int32_t)1)), /*hidden argument*/NULL);
		int32_t L_53 = V_5;
		V_5 = ((int32_t)((int32_t)L_53+(int32_t)2));
	}

IL_00fd:
	{
		int32_t L_54 = V_5;
		List_1_t1868257458 * L_55 = ___path0;
		NullCheck(L_55);
		int32_t L_56 = List_1_get_Count_m1995942658(L_55, /*hidden argument*/List_1_get_Count_m1995942658_MethodInfo_var);
		if ((((int32_t)L_54) < ((int32_t)((int32_t)((int32_t)L_56-(int32_t)2)))))
		{
			goto IL_00e2;
		}
	}
	{
		int32_t L_57 = V_0;
		if (!L_57)
		{
			goto IL_0124;
		}
	}
	{
		Node_t2499136326 * L_58 = V_1;
		NullCheck(L_58);
		bool L_59 = Node_get_IsBlack_m2486375420(L_58, /*hidden argument*/NULL);
		if (!L_59)
		{
			goto IL_0124;
		}
	}
	{
		List_1_t1868257458 * L_60 = ___path0;
		RBTree_rebalance_delete_m1773806460(__this, L_60, /*hidden argument*/NULL);
	}

IL_0124:
	{
		Node_t2499136326 * L_61 = __this->get_root_0();
		if (!L_61)
		{
			goto IL_014a;
		}
	}
	{
		Node_t2499136326 * L_62 = __this->get_root_0();
		NullCheck(L_62);
		bool L_63 = Node_get_IsBlack_m2486375420(L_62, /*hidden argument*/NULL);
		if (L_63)
		{
			goto IL_014a;
		}
	}
	{
		SystemException_t3877406272 * L_64 = (SystemException_t3877406272 *)il2cpp_codegen_object_new(SystemException_t3877406272_il2cpp_TypeInfo_var);
		SystemException__ctor_m4001391027(L_64, _stringLiteral2246167095, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_64);
	}

IL_014a:
	{
		uint32_t L_65 = __this->get_version_2();
		__this->set_version_2(((int32_t)((int32_t)L_65+(int32_t)1)));
		Node_t2499136326 * L_66 = V_1;
		return L_66;
	}
}
// System.Void System.Collections.Generic.RBTree::rebalance_insert(System.Collections.Generic.List`1<System.Collections.Generic.RBTree/Node>)
extern const MethodInfo* List_1_get_Count_m1995942658_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m2408083997_MethodInfo_var;
extern const uint32_t RBTree_rebalance_insert_m779337910_MetadataUsageId;
extern "C"  void RBTree_rebalance_insert_m779337910 (RBTree_t1544615604 * __this, List_1_t1868257458 * ___path0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RBTree_rebalance_insert_m779337910_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	bool V_1 = false;
	{
		List_1_t1868257458 * L_0 = ___path0;
		NullCheck(L_0);
		int32_t L_1 = List_1_get_Count_m1995942658(L_0, /*hidden argument*/List_1_get_Count_m1995942658_MethodInfo_var);
		V_0 = ((int32_t)((int32_t)L_1-(int32_t)1));
	}

IL_0009:
	{
		List_1_t1868257458 * L_2 = ___path0;
		int32_t L_3 = V_0;
		NullCheck(L_2);
		Node_t2499136326 * L_4 = List_1_get_Item_m2408083997(L_2, ((int32_t)((int32_t)L_3-(int32_t)3)), /*hidden argument*/List_1_get_Item_m2408083997_MethodInfo_var);
		if (!L_4)
		{
			goto IL_002a;
		}
	}
	{
		List_1_t1868257458 * L_5 = ___path0;
		int32_t L_6 = V_0;
		NullCheck(L_5);
		Node_t2499136326 * L_7 = List_1_get_Item_m2408083997(L_5, ((int32_t)((int32_t)L_6-(int32_t)3)), /*hidden argument*/List_1_get_Item_m2408083997_MethodInfo_var);
		NullCheck(L_7);
		bool L_8 = Node_get_IsBlack_m2486375420(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0033;
		}
	}

IL_002a:
	{
		int32_t L_9 = V_0;
		List_1_t1868257458 * L_10 = ___path0;
		RBTree_rebalance_insert__rotate_final_m3178362865(__this, L_9, L_10, /*hidden argument*/NULL);
		return;
	}

IL_0033:
	{
		List_1_t1868257458 * L_11 = ___path0;
		int32_t L_12 = V_0;
		NullCheck(L_11);
		Node_t2499136326 * L_13 = List_1_get_Item_m2408083997(L_11, ((int32_t)((int32_t)L_12-(int32_t)2)), /*hidden argument*/List_1_get_Item_m2408083997_MethodInfo_var);
		V_1 = (bool)1;
		List_1_t1868257458 * L_14 = ___path0;
		int32_t L_15 = V_0;
		NullCheck(L_14);
		Node_t2499136326 * L_16 = List_1_get_Item_m2408083997(L_14, ((int32_t)((int32_t)L_15-(int32_t)3)), /*hidden argument*/List_1_get_Item_m2408083997_MethodInfo_var);
		bool L_17 = V_1;
		NullCheck(L_16);
		Node_set_IsBlack_m3482698563(L_16, L_17, /*hidden argument*/NULL);
		bool L_18 = V_1;
		NullCheck(L_13);
		Node_set_IsBlack_m3482698563(L_13, L_18, /*hidden argument*/NULL);
		int32_t L_19 = V_0;
		V_0 = ((int32_t)((int32_t)L_19-(int32_t)4));
		int32_t L_20 = V_0;
		if (L_20)
		{
			goto IL_005e;
		}
	}
	{
		return;
	}

IL_005e:
	{
		List_1_t1868257458 * L_21 = ___path0;
		int32_t L_22 = V_0;
		NullCheck(L_21);
		Node_t2499136326 * L_23 = List_1_get_Item_m2408083997(L_21, L_22, /*hidden argument*/List_1_get_Item_m2408083997_MethodInfo_var);
		NullCheck(L_23);
		Node_set_IsBlack_m3482698563(L_23, (bool)0, /*hidden argument*/NULL);
		List_1_t1868257458 * L_24 = ___path0;
		int32_t L_25 = V_0;
		NullCheck(L_24);
		Node_t2499136326 * L_26 = List_1_get_Item_m2408083997(L_24, ((int32_t)((int32_t)L_25-(int32_t)2)), /*hidden argument*/List_1_get_Item_m2408083997_MethodInfo_var);
		NullCheck(L_26);
		bool L_27 = Node_get_IsBlack_m2486375420(L_26, /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_0009;
		}
	}
	{
		return;
	}
}
// System.Void System.Collections.Generic.RBTree::rebalance_delete(System.Collections.Generic.List`1<System.Collections.Generic.RBTree/Node>)
extern const MethodInfo* List_1_get_Count_m1995942658_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m2408083997_MethodInfo_var;
extern const uint32_t RBTree_rebalance_delete_m1773806460_MetadataUsageId;
extern "C"  void RBTree_rebalance_delete_m1773806460 (RBTree_t1544615604 * __this, List_1_t1868257458 * ___path0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RBTree_rebalance_delete_m1773806460_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Node_t2499136326 * V_1 = NULL;
	{
		List_1_t1868257458 * L_0 = ___path0;
		NullCheck(L_0);
		int32_t L_1 = List_1_get_Count_m1995942658(L_0, /*hidden argument*/List_1_get_Count_m1995942658_MethodInfo_var);
		V_0 = ((int32_t)((int32_t)L_1-(int32_t)1));
	}

IL_0009:
	{
		List_1_t1868257458 * L_2 = ___path0;
		int32_t L_3 = V_0;
		NullCheck(L_2);
		Node_t2499136326 * L_4 = List_1_get_Item_m2408083997(L_2, ((int32_t)((int32_t)L_3-(int32_t)1)), /*hidden argument*/List_1_get_Item_m2408083997_MethodInfo_var);
		V_1 = L_4;
		Node_t2499136326 * L_5 = V_1;
		NullCheck(L_5);
		bool L_6 = Node_get_IsBlack_m2486375420(L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0031;
		}
	}
	{
		int32_t L_7 = V_0;
		List_1_t1868257458 * L_8 = ___path0;
		int32_t L_9 = RBTree_ensure_sibling_black_m373665857(__this, L_7, L_8, /*hidden argument*/NULL);
		V_0 = L_9;
		List_1_t1868257458 * L_10 = ___path0;
		int32_t L_11 = V_0;
		NullCheck(L_10);
		Node_t2499136326 * L_12 = List_1_get_Item_m2408083997(L_10, ((int32_t)((int32_t)L_11-(int32_t)1)), /*hidden argument*/List_1_get_Item_m2408083997_MethodInfo_var);
		V_1 = L_12;
	}

IL_0031:
	{
		Node_t2499136326 * L_13 = V_1;
		NullCheck(L_13);
		Node_t2499136326 * L_14 = L_13->get_left_0();
		if (!L_14)
		{
			goto IL_004c;
		}
	}
	{
		Node_t2499136326 * L_15 = V_1;
		NullCheck(L_15);
		Node_t2499136326 * L_16 = L_15->get_left_0();
		NullCheck(L_16);
		bool L_17 = Node_get_IsBlack_m2486375420(L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_0067;
		}
	}

IL_004c:
	{
		Node_t2499136326 * L_18 = V_1;
		NullCheck(L_18);
		Node_t2499136326 * L_19 = L_18->get_right_1();
		if (!L_19)
		{
			goto IL_0070;
		}
	}
	{
		Node_t2499136326 * L_20 = V_1;
		NullCheck(L_20);
		Node_t2499136326 * L_21 = L_20->get_right_1();
		NullCheck(L_21);
		bool L_22 = Node_get_IsBlack_m2486375420(L_21, /*hidden argument*/NULL);
		if (L_22)
		{
			goto IL_0070;
		}
	}

IL_0067:
	{
		int32_t L_23 = V_0;
		List_1_t1868257458 * L_24 = ___path0;
		RBTree_rebalance_delete__rotate_final_m2368895051(__this, L_23, L_24, /*hidden argument*/NULL);
		return;
	}

IL_0070:
	{
		Node_t2499136326 * L_25 = V_1;
		NullCheck(L_25);
		Node_set_IsBlack_m3482698563(L_25, (bool)0, /*hidden argument*/NULL);
		int32_t L_26 = V_0;
		V_0 = ((int32_t)((int32_t)L_26-(int32_t)2));
		int32_t L_27 = V_0;
		if (L_27)
		{
			goto IL_0082;
		}
	}
	{
		return;
	}

IL_0082:
	{
		List_1_t1868257458 * L_28 = ___path0;
		int32_t L_29 = V_0;
		NullCheck(L_28);
		Node_t2499136326 * L_30 = List_1_get_Item_m2408083997(L_28, L_29, /*hidden argument*/List_1_get_Item_m2408083997_MethodInfo_var);
		NullCheck(L_30);
		bool L_31 = Node_get_IsBlack_m2486375420(L_30, /*hidden argument*/NULL);
		if (L_31)
		{
			goto IL_0009;
		}
	}
	{
		List_1_t1868257458 * L_32 = ___path0;
		int32_t L_33 = V_0;
		NullCheck(L_32);
		Node_t2499136326 * L_34 = List_1_get_Item_m2408083997(L_32, L_33, /*hidden argument*/List_1_get_Item_m2408083997_MethodInfo_var);
		NullCheck(L_34);
		Node_set_IsBlack_m3482698563(L_34, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.RBTree::rebalance_insert__rotate_final(System.Int32,System.Collections.Generic.List`1<System.Collections.Generic.RBTree/Node>)
extern const MethodInfo* List_1_get_Item_m2408083997_MethodInfo_var;
extern const uint32_t RBTree_rebalance_insert__rotate_final_m3178362865_MetadataUsageId;
extern "C"  void RBTree_rebalance_insert__rotate_final_m3178362865 (RBTree_t1544615604 * __this, int32_t ___curpos0, List_1_t1868257458 * ___path1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RBTree_rebalance_insert__rotate_final_m3178362865_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Node_t2499136326 * V_0 = NULL;
	Node_t2499136326 * V_1 = NULL;
	Node_t2499136326 * V_2 = NULL;
	uint32_t V_3 = 0;
	Node_t2499136326 * V_4 = NULL;
	bool V_5 = false;
	bool V_6 = false;
	RBTree_t1544615604 * G_B14_0 = NULL;
	RBTree_t1544615604 * G_B13_0 = NULL;
	Node_t2499136326 * G_B15_0 = NULL;
	RBTree_t1544615604 * G_B15_1 = NULL;
	{
		List_1_t1868257458 * L_0 = ___path1;
		int32_t L_1 = ___curpos0;
		NullCheck(L_0);
		Node_t2499136326 * L_2 = List_1_get_Item_m2408083997(L_0, L_1, /*hidden argument*/List_1_get_Item_m2408083997_MethodInfo_var);
		V_0 = L_2;
		List_1_t1868257458 * L_3 = ___path1;
		int32_t L_4 = ___curpos0;
		NullCheck(L_3);
		Node_t2499136326 * L_5 = List_1_get_Item_m2408083997(L_3, ((int32_t)((int32_t)L_4-(int32_t)2)), /*hidden argument*/List_1_get_Item_m2408083997_MethodInfo_var);
		V_1 = L_5;
		List_1_t1868257458 * L_6 = ___path1;
		int32_t L_7 = ___curpos0;
		NullCheck(L_6);
		Node_t2499136326 * L_8 = List_1_get_Item_m2408083997(L_6, ((int32_t)((int32_t)L_7-(int32_t)4)), /*hidden argument*/List_1_get_Item_m2408083997_MethodInfo_var);
		V_2 = L_8;
		Node_t2499136326 * L_9 = V_2;
		NullCheck(L_9);
		uint32_t L_10 = Node_get_Size_m3884002931(L_9, /*hidden argument*/NULL);
		V_3 = L_10;
		Node_t2499136326 * L_11 = V_1;
		Node_t2499136326 * L_12 = V_2;
		NullCheck(L_12);
		Node_t2499136326 * L_13 = L_12->get_left_0();
		V_5 = (bool)((((Il2CppObject*)(Node_t2499136326 *)L_11) == ((Il2CppObject*)(Node_t2499136326 *)L_13))? 1 : 0);
		Node_t2499136326 * L_14 = V_0;
		Node_t2499136326 * L_15 = V_1;
		NullCheck(L_15);
		Node_t2499136326 * L_16 = L_15->get_left_0();
		V_6 = (bool)((((Il2CppObject*)(Node_t2499136326 *)L_14) == ((Il2CppObject*)(Node_t2499136326 *)L_16))? 1 : 0);
		bool L_17 = V_5;
		if (!L_17)
		{
			goto IL_0062;
		}
	}
	{
		bool L_18 = V_6;
		if (!L_18)
		{
			goto IL_0062;
		}
	}
	{
		Node_t2499136326 * L_19 = V_2;
		Node_t2499136326 * L_20 = V_1;
		NullCheck(L_20);
		Node_t2499136326 * L_21 = L_20->get_right_1();
		NullCheck(L_19);
		L_19->set_left_0(L_21);
		Node_t2499136326 * L_22 = V_1;
		Node_t2499136326 * L_23 = V_2;
		NullCheck(L_22);
		L_22->set_right_1(L_23);
		Node_t2499136326 * L_24 = V_1;
		V_4 = L_24;
		goto IL_00f0;
	}

IL_0062:
	{
		bool L_25 = V_5;
		if (!L_25)
		{
			goto IL_009e;
		}
	}
	{
		bool L_26 = V_6;
		if (L_26)
		{
			goto IL_009e;
		}
	}
	{
		Node_t2499136326 * L_27 = V_2;
		Node_t2499136326 * L_28 = V_0;
		NullCheck(L_28);
		Node_t2499136326 * L_29 = L_28->get_right_1();
		NullCheck(L_27);
		L_27->set_left_0(L_29);
		Node_t2499136326 * L_30 = V_0;
		Node_t2499136326 * L_31 = V_2;
		NullCheck(L_30);
		L_30->set_right_1(L_31);
		Node_t2499136326 * L_32 = V_1;
		Node_t2499136326 * L_33 = V_0;
		NullCheck(L_33);
		Node_t2499136326 * L_34 = L_33->get_left_0();
		NullCheck(L_32);
		L_32->set_right_1(L_34);
		Node_t2499136326 * L_35 = V_0;
		Node_t2499136326 * L_36 = V_1;
		NullCheck(L_35);
		L_35->set_left_0(L_36);
		Node_t2499136326 * L_37 = V_0;
		V_4 = L_37;
		goto IL_00f0;
	}

IL_009e:
	{
		bool L_38 = V_5;
		if (L_38)
		{
			goto IL_00da;
		}
	}
	{
		bool L_39 = V_6;
		if (!L_39)
		{
			goto IL_00da;
		}
	}
	{
		Node_t2499136326 * L_40 = V_2;
		Node_t2499136326 * L_41 = V_0;
		NullCheck(L_41);
		Node_t2499136326 * L_42 = L_41->get_left_0();
		NullCheck(L_40);
		L_40->set_right_1(L_42);
		Node_t2499136326 * L_43 = V_0;
		Node_t2499136326 * L_44 = V_2;
		NullCheck(L_43);
		L_43->set_left_0(L_44);
		Node_t2499136326 * L_45 = V_1;
		Node_t2499136326 * L_46 = V_0;
		NullCheck(L_46);
		Node_t2499136326 * L_47 = L_46->get_right_1();
		NullCheck(L_45);
		L_45->set_left_0(L_47);
		Node_t2499136326 * L_48 = V_0;
		Node_t2499136326 * L_49 = V_1;
		NullCheck(L_48);
		L_48->set_right_1(L_49);
		Node_t2499136326 * L_50 = V_0;
		V_4 = L_50;
		goto IL_00f0;
	}

IL_00da:
	{
		Node_t2499136326 * L_51 = V_2;
		Node_t2499136326 * L_52 = V_1;
		NullCheck(L_52);
		Node_t2499136326 * L_53 = L_52->get_left_0();
		NullCheck(L_51);
		L_51->set_right_1(L_53);
		Node_t2499136326 * L_54 = V_1;
		Node_t2499136326 * L_55 = V_2;
		NullCheck(L_54);
		L_54->set_left_0(L_55);
		Node_t2499136326 * L_56 = V_1;
		V_4 = L_56;
	}

IL_00f0:
	{
		Node_t2499136326 * L_57 = V_2;
		NullCheck(L_57);
		Node_FixSize_m4010501621(L_57, /*hidden argument*/NULL);
		Node_t2499136326 * L_58 = V_2;
		NullCheck(L_58);
		Node_set_IsBlack_m3482698563(L_58, (bool)0, /*hidden argument*/NULL);
		Node_t2499136326 * L_59 = V_4;
		Node_t2499136326 * L_60 = V_1;
		if ((((Il2CppObject*)(Node_t2499136326 *)L_59) == ((Il2CppObject*)(Node_t2499136326 *)L_60)))
		{
			goto IL_010d;
		}
	}
	{
		Node_t2499136326 * L_61 = V_1;
		NullCheck(L_61);
		Node_FixSize_m4010501621(L_61, /*hidden argument*/NULL);
	}

IL_010d:
	{
		Node_t2499136326 * L_62 = V_4;
		NullCheck(L_62);
		Node_set_IsBlack_m3482698563(L_62, (bool)1, /*hidden argument*/NULL);
		int32_t L_63 = ___curpos0;
		G_B13_0 = __this;
		if ((!(((uint32_t)L_63) == ((uint32_t)4))))
		{
			G_B14_0 = __this;
			goto IL_0123;
		}
	}
	{
		G_B15_0 = ((Node_t2499136326 *)(NULL));
		G_B15_1 = G_B13_0;
		goto IL_012c;
	}

IL_0123:
	{
		List_1_t1868257458 * L_64 = ___path1;
		int32_t L_65 = ___curpos0;
		NullCheck(L_64);
		Node_t2499136326 * L_66 = List_1_get_Item_m2408083997(L_64, ((int32_t)((int32_t)L_65-(int32_t)6)), /*hidden argument*/List_1_get_Item_m2408083997_MethodInfo_var);
		G_B15_0 = L_66;
		G_B15_1 = G_B14_0;
	}

IL_012c:
	{
		Node_t2499136326 * L_67 = V_2;
		uint32_t L_68 = V_3;
		Node_t2499136326 * L_69 = V_4;
		NullCheck(G_B15_1);
		RBTree_node_reparent_m2851909385(G_B15_1, G_B15_0, L_67, L_68, L_69, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.RBTree::rebalance_delete__rotate_final(System.Int32,System.Collections.Generic.List`1<System.Collections.Generic.RBTree/Node>)
extern const MethodInfo* List_1_get_Item_m2408083997_MethodInfo_var;
extern const uint32_t RBTree_rebalance_delete__rotate_final_m2368895051_MetadataUsageId;
extern "C"  void RBTree_rebalance_delete__rotate_final_m2368895051 (RBTree_t1544615604 * __this, int32_t ___curpos0, List_1_t1868257458 * ___path1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RBTree_rebalance_delete__rotate_final_m2368895051_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Node_t2499136326 * V_0 = NULL;
	Node_t2499136326 * V_1 = NULL;
	uint32_t V_2 = 0;
	bool V_3 = false;
	Node_t2499136326 * V_4 = NULL;
	Node_t2499136326 * V_5 = NULL;
	Node_t2499136326 * V_6 = NULL;
	RBTree_t1544615604 * G_B14_0 = NULL;
	RBTree_t1544615604 * G_B13_0 = NULL;
	Node_t2499136326 * G_B15_0 = NULL;
	RBTree_t1544615604 * G_B15_1 = NULL;
	{
		List_1_t1868257458 * L_0 = ___path1;
		int32_t L_1 = ___curpos0;
		NullCheck(L_0);
		Node_t2499136326 * L_2 = List_1_get_Item_m2408083997(L_0, ((int32_t)((int32_t)L_1-(int32_t)1)), /*hidden argument*/List_1_get_Item_m2408083997_MethodInfo_var);
		V_0 = L_2;
		List_1_t1868257458 * L_3 = ___path1;
		int32_t L_4 = ___curpos0;
		NullCheck(L_3);
		Node_t2499136326 * L_5 = List_1_get_Item_m2408083997(L_3, ((int32_t)((int32_t)L_4-(int32_t)2)), /*hidden argument*/List_1_get_Item_m2408083997_MethodInfo_var);
		V_1 = L_5;
		Node_t2499136326 * L_6 = V_1;
		NullCheck(L_6);
		uint32_t L_7 = Node_get_Size_m3884002931(L_6, /*hidden argument*/NULL);
		V_2 = L_7;
		Node_t2499136326 * L_8 = V_1;
		NullCheck(L_8);
		bool L_9 = Node_get_IsBlack_m2486375420(L_8, /*hidden argument*/NULL);
		V_3 = L_9;
		Node_t2499136326 * L_10 = V_1;
		NullCheck(L_10);
		Node_t2499136326 * L_11 = L_10->get_right_1();
		Node_t2499136326 * L_12 = V_0;
		if ((!(((Il2CppObject*)(Node_t2499136326 *)L_11) == ((Il2CppObject*)(Node_t2499136326 *)L_12))))
		{
			goto IL_00ab;
		}
	}
	{
		Node_t2499136326 * L_13 = V_0;
		NullCheck(L_13);
		Node_t2499136326 * L_14 = L_13->get_right_1();
		if (!L_14)
		{
			goto IL_0049;
		}
	}
	{
		Node_t2499136326 * L_15 = V_0;
		NullCheck(L_15);
		Node_t2499136326 * L_16 = L_15->get_right_1();
		NullCheck(L_16);
		bool L_17 = Node_get_IsBlack_m2486375420(L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_0084;
		}
	}

IL_0049:
	{
		Node_t2499136326 * L_18 = V_0;
		NullCheck(L_18);
		Node_t2499136326 * L_19 = L_18->get_left_0();
		V_5 = L_19;
		Node_t2499136326 * L_20 = V_1;
		Node_t2499136326 * L_21 = V_5;
		NullCheck(L_21);
		Node_t2499136326 * L_22 = L_21->get_left_0();
		NullCheck(L_20);
		L_20->set_right_1(L_22);
		Node_t2499136326 * L_23 = V_5;
		Node_t2499136326 * L_24 = V_1;
		NullCheck(L_23);
		L_23->set_left_0(L_24);
		Node_t2499136326 * L_25 = V_0;
		Node_t2499136326 * L_26 = V_5;
		NullCheck(L_26);
		Node_t2499136326 * L_27 = L_26->get_right_1();
		NullCheck(L_25);
		L_25->set_left_0(L_27);
		Node_t2499136326 * L_28 = V_5;
		Node_t2499136326 * L_29 = V_0;
		NullCheck(L_28);
		L_28->set_right_1(L_29);
		Node_t2499136326 * L_30 = V_5;
		V_4 = L_30;
		goto IL_00a6;
	}

IL_0084:
	{
		Node_t2499136326 * L_31 = V_1;
		Node_t2499136326 * L_32 = V_0;
		NullCheck(L_32);
		Node_t2499136326 * L_33 = L_32->get_left_0();
		NullCheck(L_31);
		L_31->set_right_1(L_33);
		Node_t2499136326 * L_34 = V_0;
		Node_t2499136326 * L_35 = V_1;
		NullCheck(L_34);
		L_34->set_left_0(L_35);
		Node_t2499136326 * L_36 = V_0;
		NullCheck(L_36);
		Node_t2499136326 * L_37 = L_36->get_right_1();
		NullCheck(L_37);
		Node_set_IsBlack_m3482698563(L_37, (bool)1, /*hidden argument*/NULL);
		Node_t2499136326 * L_38 = V_0;
		V_4 = L_38;
	}

IL_00a6:
	{
		goto IL_0123;
	}

IL_00ab:
	{
		Node_t2499136326 * L_39 = V_0;
		NullCheck(L_39);
		Node_t2499136326 * L_40 = L_39->get_left_0();
		if (!L_40)
		{
			goto IL_00c6;
		}
	}
	{
		Node_t2499136326 * L_41 = V_0;
		NullCheck(L_41);
		Node_t2499136326 * L_42 = L_41->get_left_0();
		NullCheck(L_42);
		bool L_43 = Node_get_IsBlack_m2486375420(L_42, /*hidden argument*/NULL);
		if (!L_43)
		{
			goto IL_0101;
		}
	}

IL_00c6:
	{
		Node_t2499136326 * L_44 = V_0;
		NullCheck(L_44);
		Node_t2499136326 * L_45 = L_44->get_right_1();
		V_6 = L_45;
		Node_t2499136326 * L_46 = V_1;
		Node_t2499136326 * L_47 = V_6;
		NullCheck(L_47);
		Node_t2499136326 * L_48 = L_47->get_right_1();
		NullCheck(L_46);
		L_46->set_left_0(L_48);
		Node_t2499136326 * L_49 = V_6;
		Node_t2499136326 * L_50 = V_1;
		NullCheck(L_49);
		L_49->set_right_1(L_50);
		Node_t2499136326 * L_51 = V_0;
		Node_t2499136326 * L_52 = V_6;
		NullCheck(L_52);
		Node_t2499136326 * L_53 = L_52->get_left_0();
		NullCheck(L_51);
		L_51->set_right_1(L_53);
		Node_t2499136326 * L_54 = V_6;
		Node_t2499136326 * L_55 = V_0;
		NullCheck(L_54);
		L_54->set_left_0(L_55);
		Node_t2499136326 * L_56 = V_6;
		V_4 = L_56;
		goto IL_0123;
	}

IL_0101:
	{
		Node_t2499136326 * L_57 = V_1;
		Node_t2499136326 * L_58 = V_0;
		NullCheck(L_58);
		Node_t2499136326 * L_59 = L_58->get_right_1();
		NullCheck(L_57);
		L_57->set_left_0(L_59);
		Node_t2499136326 * L_60 = V_0;
		Node_t2499136326 * L_61 = V_1;
		NullCheck(L_60);
		L_60->set_right_1(L_61);
		Node_t2499136326 * L_62 = V_0;
		NullCheck(L_62);
		Node_t2499136326 * L_63 = L_62->get_left_0();
		NullCheck(L_63);
		Node_set_IsBlack_m3482698563(L_63, (bool)1, /*hidden argument*/NULL);
		Node_t2499136326 * L_64 = V_0;
		V_4 = L_64;
	}

IL_0123:
	{
		Node_t2499136326 * L_65 = V_1;
		NullCheck(L_65);
		Node_FixSize_m4010501621(L_65, /*hidden argument*/NULL);
		Node_t2499136326 * L_66 = V_1;
		NullCheck(L_66);
		Node_set_IsBlack_m3482698563(L_66, (bool)1, /*hidden argument*/NULL);
		Node_t2499136326 * L_67 = V_4;
		Node_t2499136326 * L_68 = V_0;
		if ((((Il2CppObject*)(Node_t2499136326 *)L_67) == ((Il2CppObject*)(Node_t2499136326 *)L_68)))
		{
			goto IL_0140;
		}
	}
	{
		Node_t2499136326 * L_69 = V_0;
		NullCheck(L_69);
		Node_FixSize_m4010501621(L_69, /*hidden argument*/NULL);
	}

IL_0140:
	{
		Node_t2499136326 * L_70 = V_4;
		bool L_71 = V_3;
		NullCheck(L_70);
		Node_set_IsBlack_m3482698563(L_70, L_71, /*hidden argument*/NULL);
		int32_t L_72 = ___curpos0;
		G_B13_0 = __this;
		if ((!(((uint32_t)L_72) == ((uint32_t)2))))
		{
			G_B14_0 = __this;
			goto IL_0156;
		}
	}
	{
		G_B15_0 = ((Node_t2499136326 *)(NULL));
		G_B15_1 = G_B13_0;
		goto IL_015f;
	}

IL_0156:
	{
		List_1_t1868257458 * L_73 = ___path1;
		int32_t L_74 = ___curpos0;
		NullCheck(L_73);
		Node_t2499136326 * L_75 = List_1_get_Item_m2408083997(L_73, ((int32_t)((int32_t)L_74-(int32_t)4)), /*hidden argument*/List_1_get_Item_m2408083997_MethodInfo_var);
		G_B15_0 = L_75;
		G_B15_1 = G_B14_0;
	}

IL_015f:
	{
		Node_t2499136326 * L_76 = V_1;
		uint32_t L_77 = V_2;
		Node_t2499136326 * L_78 = V_4;
		NullCheck(G_B15_1);
		RBTree_node_reparent_m2851909385(G_B15_1, G_B15_0, L_76, L_77, L_78, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 System.Collections.Generic.RBTree::ensure_sibling_black(System.Int32,System.Collections.Generic.List`1<System.Collections.Generic.RBTree/Node>)
extern const MethodInfo* List_1_get_Item_m2408083997_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m1995942658_MethodInfo_var;
extern const MethodInfo* List_1_Add_m3784024590_MethodInfo_var;
extern const MethodInfo* List_1_set_Item_m3689121816_MethodInfo_var;
extern const uint32_t RBTree_ensure_sibling_black_m373665857_MetadataUsageId;
extern "C"  int32_t RBTree_ensure_sibling_black_m373665857 (RBTree_t1544615604 * __this, int32_t ___curpos0, List_1_t1868257458 * ___path1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RBTree_ensure_sibling_black_m373665857_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Node_t2499136326 * V_0 = NULL;
	Node_t2499136326 * V_1 = NULL;
	Node_t2499136326 * V_2 = NULL;
	bool V_3 = false;
	uint32_t V_4 = 0;
	RBTree_t1544615604 * G_B5_0 = NULL;
	RBTree_t1544615604 * G_B4_0 = NULL;
	Node_t2499136326 * G_B6_0 = NULL;
	RBTree_t1544615604 * G_B6_1 = NULL;
	int32_t G_B10_0 = 0;
	List_1_t1868257458 * G_B10_1 = NULL;
	int32_t G_B9_0 = 0;
	List_1_t1868257458 * G_B9_1 = NULL;
	Node_t2499136326 * G_B11_0 = NULL;
	int32_t G_B11_1 = 0;
	List_1_t1868257458 * G_B11_2 = NULL;
	int32_t G_B13_0 = 0;
	List_1_t1868257458 * G_B13_1 = NULL;
	int32_t G_B12_0 = 0;
	List_1_t1868257458 * G_B12_1 = NULL;
	Node_t2499136326 * G_B14_0 = NULL;
	int32_t G_B14_1 = 0;
	List_1_t1868257458 * G_B14_2 = NULL;
	{
		List_1_t1868257458 * L_0 = ___path1;
		int32_t L_1 = ___curpos0;
		NullCheck(L_0);
		Node_t2499136326 * L_2 = List_1_get_Item_m2408083997(L_0, L_1, /*hidden argument*/List_1_get_Item_m2408083997_MethodInfo_var);
		V_0 = L_2;
		List_1_t1868257458 * L_3 = ___path1;
		int32_t L_4 = ___curpos0;
		NullCheck(L_3);
		Node_t2499136326 * L_5 = List_1_get_Item_m2408083997(L_3, ((int32_t)((int32_t)L_4-(int32_t)1)), /*hidden argument*/List_1_get_Item_m2408083997_MethodInfo_var);
		V_1 = L_5;
		List_1_t1868257458 * L_6 = ___path1;
		int32_t L_7 = ___curpos0;
		NullCheck(L_6);
		Node_t2499136326 * L_8 = List_1_get_Item_m2408083997(L_6, ((int32_t)((int32_t)L_7-(int32_t)2)), /*hidden argument*/List_1_get_Item_m2408083997_MethodInfo_var);
		V_2 = L_8;
		Node_t2499136326 * L_9 = V_2;
		NullCheck(L_9);
		uint32_t L_10 = Node_get_Size_m3884002931(L_9, /*hidden argument*/NULL);
		V_4 = L_10;
		Node_t2499136326 * L_11 = V_2;
		NullCheck(L_11);
		Node_t2499136326 * L_12 = L_11->get_right_1();
		Node_t2499136326 * L_13 = V_1;
		if ((!(((Il2CppObject*)(Node_t2499136326 *)L_12) == ((Il2CppObject*)(Node_t2499136326 *)L_13))))
		{
			goto IL_004a;
		}
	}
	{
		Node_t2499136326 * L_14 = V_2;
		Node_t2499136326 * L_15 = V_1;
		NullCheck(L_15);
		Node_t2499136326 * L_16 = L_15->get_left_0();
		NullCheck(L_14);
		L_14->set_right_1(L_16);
		Node_t2499136326 * L_17 = V_1;
		Node_t2499136326 * L_18 = V_2;
		NullCheck(L_17);
		L_17->set_left_0(L_18);
		V_3 = (bool)1;
		goto IL_005f;
	}

IL_004a:
	{
		Node_t2499136326 * L_19 = V_2;
		Node_t2499136326 * L_20 = V_1;
		NullCheck(L_20);
		Node_t2499136326 * L_21 = L_20->get_right_1();
		NullCheck(L_19);
		L_19->set_left_0(L_21);
		Node_t2499136326 * L_22 = V_1;
		Node_t2499136326 * L_23 = V_2;
		NullCheck(L_22);
		L_22->set_right_1(L_23);
		V_3 = (bool)0;
	}

IL_005f:
	{
		Node_t2499136326 * L_24 = V_2;
		NullCheck(L_24);
		Node_FixSize_m4010501621(L_24, /*hidden argument*/NULL);
		Node_t2499136326 * L_25 = V_2;
		NullCheck(L_25);
		Node_set_IsBlack_m3482698563(L_25, (bool)0, /*hidden argument*/NULL);
		Node_t2499136326 * L_26 = V_1;
		NullCheck(L_26);
		Node_set_IsBlack_m3482698563(L_26, (bool)1, /*hidden argument*/NULL);
		int32_t L_27 = ___curpos0;
		G_B4_0 = __this;
		if ((!(((uint32_t)L_27) == ((uint32_t)2))))
		{
			G_B5_0 = __this;
			goto IL_0082;
		}
	}
	{
		G_B6_0 = ((Node_t2499136326 *)(NULL));
		G_B6_1 = G_B4_0;
		goto IL_008b;
	}

IL_0082:
	{
		List_1_t1868257458 * L_28 = ___path1;
		int32_t L_29 = ___curpos0;
		NullCheck(L_28);
		Node_t2499136326 * L_30 = List_1_get_Item_m2408083997(L_28, ((int32_t)((int32_t)L_29-(int32_t)4)), /*hidden argument*/List_1_get_Item_m2408083997_MethodInfo_var);
		G_B6_0 = L_30;
		G_B6_1 = G_B5_0;
	}

IL_008b:
	{
		Node_t2499136326 * L_31 = V_2;
		uint32_t L_32 = V_4;
		Node_t2499136326 * L_33 = V_1;
		NullCheck(G_B6_1);
		RBTree_node_reparent_m2851909385(G_B6_1, G_B6_0, L_31, L_32, L_33, /*hidden argument*/NULL);
		int32_t L_34 = ___curpos0;
		List_1_t1868257458 * L_35 = ___path1;
		NullCheck(L_35);
		int32_t L_36 = List_1_get_Count_m1995942658(L_35, /*hidden argument*/List_1_get_Count_m1995942658_MethodInfo_var);
		if ((!(((uint32_t)((int32_t)((int32_t)L_34+(int32_t)1))) == ((uint32_t)L_36))))
		{
			goto IL_00b0;
		}
	}
	{
		List_1_t1868257458 * L_37 = ___path1;
		NullCheck(L_37);
		List_1_Add_m3784024590(L_37, (Node_t2499136326 *)NULL, /*hidden argument*/List_1_Add_m3784024590_MethodInfo_var);
		List_1_t1868257458 * L_38 = ___path1;
		NullCheck(L_38);
		List_1_Add_m3784024590(L_38, (Node_t2499136326 *)NULL, /*hidden argument*/List_1_Add_m3784024590_MethodInfo_var);
	}

IL_00b0:
	{
		List_1_t1868257458 * L_39 = ___path1;
		int32_t L_40 = ___curpos0;
		Node_t2499136326 * L_41 = V_1;
		NullCheck(L_39);
		List_1_set_Item_m3689121816(L_39, ((int32_t)((int32_t)L_40-(int32_t)2)), L_41, /*hidden argument*/List_1_set_Item_m3689121816_MethodInfo_var);
		List_1_t1868257458 * L_42 = ___path1;
		int32_t L_43 = ___curpos0;
		bool L_44 = V_3;
		G_B9_0 = ((int32_t)((int32_t)L_43-(int32_t)1));
		G_B9_1 = L_42;
		if (!L_44)
		{
			G_B10_0 = ((int32_t)((int32_t)L_43-(int32_t)1));
			G_B10_1 = L_42;
			goto IL_00cf;
		}
	}
	{
		Node_t2499136326 * L_45 = V_1;
		NullCheck(L_45);
		Node_t2499136326 * L_46 = L_45->get_right_1();
		G_B11_0 = L_46;
		G_B11_1 = G_B9_0;
		G_B11_2 = G_B9_1;
		goto IL_00d5;
	}

IL_00cf:
	{
		Node_t2499136326 * L_47 = V_1;
		NullCheck(L_47);
		Node_t2499136326 * L_48 = L_47->get_left_0();
		G_B11_0 = L_48;
		G_B11_1 = G_B10_0;
		G_B11_2 = G_B10_1;
	}

IL_00d5:
	{
		NullCheck(G_B11_2);
		List_1_set_Item_m3689121816(G_B11_2, G_B11_1, G_B11_0, /*hidden argument*/List_1_set_Item_m3689121816_MethodInfo_var);
		List_1_t1868257458 * L_49 = ___path1;
		int32_t L_50 = ___curpos0;
		Node_t2499136326 * L_51 = V_2;
		NullCheck(L_49);
		List_1_set_Item_m3689121816(L_49, L_50, L_51, /*hidden argument*/List_1_set_Item_m3689121816_MethodInfo_var);
		List_1_t1868257458 * L_52 = ___path1;
		int32_t L_53 = ___curpos0;
		bool L_54 = V_3;
		G_B12_0 = ((int32_t)((int32_t)L_53+(int32_t)1));
		G_B12_1 = L_52;
		if (!L_54)
		{
			G_B13_0 = ((int32_t)((int32_t)L_53+(int32_t)1));
			G_B13_1 = L_52;
			goto IL_00f7;
		}
	}
	{
		Node_t2499136326 * L_55 = V_2;
		NullCheck(L_55);
		Node_t2499136326 * L_56 = L_55->get_right_1();
		G_B14_0 = L_56;
		G_B14_1 = G_B12_0;
		G_B14_2 = G_B12_1;
		goto IL_00fd;
	}

IL_00f7:
	{
		Node_t2499136326 * L_57 = V_2;
		NullCheck(L_57);
		Node_t2499136326 * L_58 = L_57->get_left_0();
		G_B14_0 = L_58;
		G_B14_1 = G_B13_0;
		G_B14_2 = G_B13_1;
	}

IL_00fd:
	{
		NullCheck(G_B14_2);
		List_1_set_Item_m3689121816(G_B14_2, G_B14_1, G_B14_0, /*hidden argument*/List_1_set_Item_m3689121816_MethodInfo_var);
		List_1_t1868257458 * L_59 = ___path1;
		int32_t L_60 = ___curpos0;
		Node_t2499136326 * L_61 = V_0;
		NullCheck(L_59);
		List_1_set_Item_m3689121816(L_59, ((int32_t)((int32_t)L_60+(int32_t)2)), L_61, /*hidden argument*/List_1_set_Item_m3689121816_MethodInfo_var);
		int32_t L_62 = ___curpos0;
		return ((int32_t)((int32_t)L_62+(int32_t)2));
	}
}
// System.Void System.Collections.Generic.RBTree::node_reparent(System.Collections.Generic.RBTree/Node,System.Collections.Generic.RBTree/Node,System.UInt32,System.Collections.Generic.RBTree/Node)
extern Il2CppClass* SystemException_t3877406272_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1595186001;
extern Il2CppCodeGenString* _stringLiteral346067822;
extern const uint32_t RBTree_node_reparent_m2851909385_MetadataUsageId;
extern "C"  void RBTree_node_reparent_m2851909385 (RBTree_t1544615604 * __this, Node_t2499136326 * ___orig_parent0, Node_t2499136326 * ___orig1, uint32_t ___orig_size2, Node_t2499136326 * ___updated3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RBTree_node_reparent_m2851909385_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Node_t2499136326 * L_0 = ___updated3;
		if (!L_0)
		{
			goto IL_001f;
		}
	}
	{
		Node_t2499136326 * L_1 = ___updated3;
		NullCheck(L_1);
		uint32_t L_2 = Node_FixSize_m4010501621(L_1, /*hidden argument*/NULL);
		uint32_t L_3 = ___orig_size2;
		if ((((int32_t)L_2) == ((int32_t)L_3)))
		{
			goto IL_001f;
		}
	}
	{
		SystemException_t3877406272 * L_4 = (SystemException_t3877406272 *)il2cpp_codegen_object_new(SystemException_t3877406272_il2cpp_TypeInfo_var);
		SystemException__ctor_m4001391027(L_4, _stringLiteral1595186001, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_001f:
	{
		Node_t2499136326 * L_5 = ___orig1;
		Node_t2499136326 * L_6 = __this->get_root_0();
		if ((!(((Il2CppObject*)(Node_t2499136326 *)L_5) == ((Il2CppObject*)(Node_t2499136326 *)L_6))))
		{
			goto IL_0038;
		}
	}
	{
		Node_t2499136326 * L_7 = ___updated3;
		__this->set_root_0(L_7);
		goto IL_0075;
	}

IL_0038:
	{
		Node_t2499136326 * L_8 = ___orig1;
		Node_t2499136326 * L_9 = ___orig_parent0;
		NullCheck(L_9);
		Node_t2499136326 * L_10 = L_9->get_left_0();
		if ((!(((Il2CppObject*)(Node_t2499136326 *)L_8) == ((Il2CppObject*)(Node_t2499136326 *)L_10))))
		{
			goto IL_0051;
		}
	}
	{
		Node_t2499136326 * L_11 = ___orig_parent0;
		Node_t2499136326 * L_12 = ___updated3;
		NullCheck(L_11);
		L_11->set_left_0(L_12);
		goto IL_0075;
	}

IL_0051:
	{
		Node_t2499136326 * L_13 = ___orig1;
		Node_t2499136326 * L_14 = ___orig_parent0;
		NullCheck(L_14);
		Node_t2499136326 * L_15 = L_14->get_right_1();
		if ((!(((Il2CppObject*)(Node_t2499136326 *)L_13) == ((Il2CppObject*)(Node_t2499136326 *)L_15))))
		{
			goto IL_006a;
		}
	}
	{
		Node_t2499136326 * L_16 = ___orig_parent0;
		Node_t2499136326 * L_17 = ___updated3;
		NullCheck(L_16);
		L_16->set_right_1(L_17);
		goto IL_0075;
	}

IL_006a:
	{
		SystemException_t3877406272 * L_18 = (SystemException_t3877406272 *)il2cpp_codegen_object_new(SystemException_t3877406272_il2cpp_TypeInfo_var);
		SystemException__ctor_m4001391027(L_18, _stringLiteral346067822, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_18);
	}

IL_0075:
	{
		return;
	}
}
// System.Collections.Generic.RBTree/Node System.Collections.Generic.RBTree::right_most(System.Collections.Generic.RBTree/Node,System.Collections.Generic.RBTree/Node,System.Collections.Generic.List`1<System.Collections.Generic.RBTree/Node>)
extern const MethodInfo* List_1_Add_m3784024590_MethodInfo_var;
extern const uint32_t RBTree_right_most_m129601026_MetadataUsageId;
extern "C"  Node_t2499136326 * RBTree_right_most_m129601026 (Il2CppObject * __this /* static, unused */, Node_t2499136326 * ___current0, Node_t2499136326 * ___sibling1, List_1_t1868257458 * ___path2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RBTree_right_most_m129601026_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		goto IL_0030;
	}

IL_0005:
	{
		List_1_t1868257458 * L_0 = ___path2;
		Node_t2499136326 * L_1 = ___sibling1;
		NullCheck(L_0);
		List_1_Add_m3784024590(L_0, L_1, /*hidden argument*/List_1_Add_m3784024590_MethodInfo_var);
		List_1_t1868257458 * L_2 = ___path2;
		Node_t2499136326 * L_3 = ___current0;
		NullCheck(L_2);
		List_1_Add_m3784024590(L_2, L_3, /*hidden argument*/List_1_Add_m3784024590_MethodInfo_var);
		Node_t2499136326 * L_4 = ___current0;
		NullCheck(L_4);
		Node_t2499136326 * L_5 = L_4->get_right_1();
		if (L_5)
		{
			goto IL_0020;
		}
	}
	{
		Node_t2499136326 * L_6 = ___current0;
		return L_6;
	}

IL_0020:
	{
		Node_t2499136326 * L_7 = ___current0;
		NullCheck(L_7);
		Node_t2499136326 * L_8 = L_7->get_left_0();
		___sibling1 = L_8;
		Node_t2499136326 * L_9 = ___current0;
		NullCheck(L_9);
		Node_t2499136326 * L_10 = L_9->get_right_1();
		___current0 = L_10;
	}

IL_0030:
	{
		goto IL_0005;
	}
}
// System.Void System.Collections.Generic.RBTree/Node::.ctor()
extern "C"  void Node__ctor_m3246611644 (Node_t2499136326 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		__this->set_size_black_2(2);
		return;
	}
}
// System.Boolean System.Collections.Generic.RBTree/Node::get_IsBlack()
extern "C"  bool Node_get_IsBlack_m2486375420 (Node_t2499136326 * __this, const MethodInfo* method)
{
	{
		uint32_t L_0 = __this->get_size_black_2();
		return (bool)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)1))) == ((int32_t)1))? 1 : 0);
	}
}
// System.Void System.Collections.Generic.RBTree/Node::set_IsBlack(System.Boolean)
extern "C"  void Node_set_IsBlack_m3482698563 (Node_t2499136326 * __this, bool ___value0, const MethodInfo* method)
{
	Node_t2499136326 * G_B2_0 = NULL;
	Node_t2499136326 * G_B1_0 = NULL;
	uint32_t G_B3_0 = 0;
	Node_t2499136326 * G_B3_1 = NULL;
	{
		bool L_0 = ___value0;
		G_B1_0 = __this;
		if (!L_0)
		{
			G_B2_0 = __this;
			goto IL_0014;
		}
	}
	{
		uint32_t L_1 = __this->get_size_black_2();
		G_B3_0 = ((uint32_t)(((int32_t)((int32_t)L_1|(int32_t)1))));
		G_B3_1 = G_B1_0;
		goto IL_001d;
	}

IL_0014:
	{
		uint32_t L_2 = __this->get_size_black_2();
		G_B3_0 = ((uint32_t)(((int32_t)((int32_t)L_2&(int32_t)((int32_t)-2)))));
		G_B3_1 = G_B2_0;
	}

IL_001d:
	{
		NullCheck(G_B3_1);
		G_B3_1->set_size_black_2(G_B3_0);
		return;
	}
}
// System.UInt32 System.Collections.Generic.RBTree/Node::get_Size()
extern "C"  uint32_t Node_get_Size_m3884002931 (Node_t2499136326 * __this, const MethodInfo* method)
{
	{
		uint32_t L_0 = __this->get_size_black_2();
		return ((int32_t)((uint32_t)L_0>>1));
	}
}
// System.Void System.Collections.Generic.RBTree/Node::set_Size(System.UInt32)
extern "C"  void Node_set_Size_m2428994664 (Node_t2499136326 * __this, uint32_t ___value0, const MethodInfo* method)
{
	{
		uint32_t L_0 = ___value0;
		uint32_t L_1 = __this->get_size_black_2();
		__this->set_size_black_2(((int32_t)((int32_t)((int32_t)((int32_t)L_0<<(int32_t)1))|(int32_t)((int32_t)((int32_t)L_1&(int32_t)1)))));
		return;
	}
}
// System.UInt32 System.Collections.Generic.RBTree/Node::FixSize()
extern "C"  uint32_t Node_FixSize_m4010501621 (Node_t2499136326 * __this, const MethodInfo* method)
{
	{
		Node_set_Size_m2428994664(__this, 1, /*hidden argument*/NULL);
		Node_t2499136326 * L_0 = __this->get_left_0();
		if (!L_0)
		{
			goto IL_002a;
		}
	}
	{
		uint32_t L_1 = Node_get_Size_m3884002931(__this, /*hidden argument*/NULL);
		Node_t2499136326 * L_2 = __this->get_left_0();
		NullCheck(L_2);
		uint32_t L_3 = Node_get_Size_m3884002931(L_2, /*hidden argument*/NULL);
		Node_set_Size_m2428994664(__this, ((int32_t)((int32_t)L_1+(int32_t)L_3)), /*hidden argument*/NULL);
	}

IL_002a:
	{
		Node_t2499136326 * L_4 = __this->get_right_1();
		if (!L_4)
		{
			goto IL_004d;
		}
	}
	{
		uint32_t L_5 = Node_get_Size_m3884002931(__this, /*hidden argument*/NULL);
		Node_t2499136326 * L_6 = __this->get_right_1();
		NullCheck(L_6);
		uint32_t L_7 = Node_get_Size_m3884002931(L_6, /*hidden argument*/NULL);
		Node_set_Size_m2428994664(__this, ((int32_t)((int32_t)L_5+(int32_t)L_7)), /*hidden argument*/NULL);
	}

IL_004d:
	{
		uint32_t L_8 = Node_get_Size_m3884002931(__this, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Void System.Collections.Generic.RBTree/NodeEnumerator::.ctor(System.Collections.Generic.RBTree)
extern "C"  void NodeEnumerator__ctor_m3411227341 (NodeEnumerator_t648190100 * __this, RBTree_t1544615604 * ___tree0, const MethodInfo* method)
{
	{
		RBTree_t1544615604 * L_0 = ___tree0;
		__this->set_tree_0(L_0);
		RBTree_t1544615604 * L_1 = ___tree0;
		NullCheck(L_1);
		uint32_t L_2 = L_1->get_version_2();
		__this->set_version_1(L_2);
		__this->set_pennants_2((Stack_1_t3586864480 *)NULL);
		return;
	}
}
extern "C"  void NodeEnumerator__ctor_m3411227341_AdjustorThunk (Il2CppObject * __this, RBTree_t1544615604 * ___tree0, const MethodInfo* method)
{
	NodeEnumerator_t648190100 * _thisAdjusted = reinterpret_cast<NodeEnumerator_t648190100 *>(__this + 1);
	NodeEnumerator__ctor_m3411227341(_thisAdjusted, ___tree0, method);
}
// System.Object System.Collections.Generic.RBTree/NodeEnumerator::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * NodeEnumerator_System_Collections_IEnumerator_get_Current_m4244846678 (NodeEnumerator_t648190100 * __this, const MethodInfo* method)
{
	{
		NodeEnumerator_check_current_m135648288(__this, /*hidden argument*/NULL);
		Node_t2499136326 * L_0 = NodeEnumerator_get_Current_m300427921(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
extern "C"  Il2CppObject * NodeEnumerator_System_Collections_IEnumerator_get_Current_m4244846678_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NodeEnumerator_t648190100 * _thisAdjusted = reinterpret_cast<NodeEnumerator_t648190100 *>(__this + 1);
	return NodeEnumerator_System_Collections_IEnumerator_get_Current_m4244846678(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.RBTree/NodeEnumerator::Reset()
extern "C"  void NodeEnumerator_Reset_m1401118445 (NodeEnumerator_t648190100 * __this, const MethodInfo* method)
{
	{
		NodeEnumerator_check_version_m1999562667(__this, /*hidden argument*/NULL);
		__this->set_pennants_2((Stack_1_t3586864480 *)NULL);
		return;
	}
}
extern "C"  void NodeEnumerator_Reset_m1401118445_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NodeEnumerator_t648190100 * _thisAdjusted = reinterpret_cast<NodeEnumerator_t648190100 *>(__this + 1);
	NodeEnumerator_Reset_m1401118445(_thisAdjusted, method);
}
// System.Collections.Generic.RBTree/Node System.Collections.Generic.RBTree/NodeEnumerator::get_Current()
extern const MethodInfo* Stack_1_Peek_m4196035282_MethodInfo_var;
extern const uint32_t NodeEnumerator_get_Current_m300427921_MetadataUsageId;
extern "C"  Node_t2499136326 * NodeEnumerator_get_Current_m300427921 (NodeEnumerator_t648190100 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NodeEnumerator_get_Current_m300427921_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Stack_1_t3586864480 * L_0 = __this->get_pennants_2();
		NullCheck(L_0);
		Node_t2499136326 * L_1 = Stack_1_Peek_m4196035282(L_0, /*hidden argument*/Stack_1_Peek_m4196035282_MethodInfo_var);
		return L_1;
	}
}
extern "C"  Node_t2499136326 * NodeEnumerator_get_Current_m300427921_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NodeEnumerator_t648190100 * _thisAdjusted = reinterpret_cast<NodeEnumerator_t648190100 *>(__this + 1);
	return NodeEnumerator_get_Current_m300427921(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.RBTree/NodeEnumerator::MoveNext()
extern Il2CppClass* Stack_1_t3586864480_il2cpp_TypeInfo_var;
extern const MethodInfo* Stack_1__ctor_m2667244830_MethodInfo_var;
extern const MethodInfo* Stack_1_get_Count_m4266702390_MethodInfo_var;
extern const MethodInfo* Stack_1_Pop_m3059123758_MethodInfo_var;
extern const MethodInfo* Stack_1_Push_m147339882_MethodInfo_var;
extern const uint32_t NodeEnumerator_MoveNext_m3617254344_MetadataUsageId;
extern "C"  bool NodeEnumerator_MoveNext_m3617254344 (NodeEnumerator_t648190100 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NodeEnumerator_MoveNext_m3617254344_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Node_t2499136326 * V_0 = NULL;
	Node_t2499136326 * V_1 = NULL;
	{
		NodeEnumerator_check_version_m1999562667(__this, /*hidden argument*/NULL);
		Stack_1_t3586864480 * L_0 = __this->get_pennants_2();
		if (L_0)
		{
			goto IL_003f;
		}
	}
	{
		RBTree_t1544615604 * L_1 = __this->get_tree_0();
		NullCheck(L_1);
		Node_t2499136326 * L_2 = L_1->get_root_0();
		if (L_2)
		{
			goto IL_0023;
		}
	}
	{
		return (bool)0;
	}

IL_0023:
	{
		Stack_1_t3586864480 * L_3 = (Stack_1_t3586864480 *)il2cpp_codegen_object_new(Stack_1_t3586864480_il2cpp_TypeInfo_var);
		Stack_1__ctor_m2667244830(L_3, /*hidden argument*/Stack_1__ctor_m2667244830_MethodInfo_var);
		__this->set_pennants_2(L_3);
		RBTree_t1544615604 * L_4 = __this->get_tree_0();
		NullCheck(L_4);
		Node_t2499136326 * L_5 = L_4->get_root_0();
		V_0 = L_5;
		goto IL_0064;
	}

IL_003f:
	{
		Stack_1_t3586864480 * L_6 = __this->get_pennants_2();
		NullCheck(L_6);
		int32_t L_7 = Stack_1_get_Count_m4266702390(L_6, /*hidden argument*/Stack_1_get_Count_m4266702390_MethodInfo_var);
		if (L_7)
		{
			goto IL_0051;
		}
	}
	{
		return (bool)0;
	}

IL_0051:
	{
		Stack_1_t3586864480 * L_8 = __this->get_pennants_2();
		NullCheck(L_8);
		Node_t2499136326 * L_9 = Stack_1_Pop_m3059123758(L_8, /*hidden argument*/Stack_1_Pop_m3059123758_MethodInfo_var);
		V_1 = L_9;
		Node_t2499136326 * L_10 = V_1;
		NullCheck(L_10);
		Node_t2499136326 * L_11 = L_10->get_right_1();
		V_0 = L_11;
	}

IL_0064:
	{
		goto IL_007c;
	}

IL_0069:
	{
		Stack_1_t3586864480 * L_12 = __this->get_pennants_2();
		Node_t2499136326 * L_13 = V_0;
		NullCheck(L_12);
		Stack_1_Push_m147339882(L_12, L_13, /*hidden argument*/Stack_1_Push_m147339882_MethodInfo_var);
		Node_t2499136326 * L_14 = V_0;
		NullCheck(L_14);
		Node_t2499136326 * L_15 = L_14->get_left_0();
		V_0 = L_15;
	}

IL_007c:
	{
		Node_t2499136326 * L_16 = V_0;
		if (L_16)
		{
			goto IL_0069;
		}
	}
	{
		Stack_1_t3586864480 * L_17 = __this->get_pennants_2();
		NullCheck(L_17);
		int32_t L_18 = Stack_1_get_Count_m4266702390(L_17, /*hidden argument*/Stack_1_get_Count_m4266702390_MethodInfo_var);
		return (bool)((((int32_t)((((int32_t)L_18) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
extern "C"  bool NodeEnumerator_MoveNext_m3617254344_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NodeEnumerator_t648190100 * _thisAdjusted = reinterpret_cast<NodeEnumerator_t648190100 *>(__this + 1);
	return NodeEnumerator_MoveNext_m3617254344(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.RBTree/NodeEnumerator::Dispose()
extern "C"  void NodeEnumerator_Dispose_m475269499 (NodeEnumerator_t648190100 * __this, const MethodInfo* method)
{
	{
		__this->set_tree_0((RBTree_t1544615604 *)NULL);
		__this->set_pennants_2((Stack_1_t3586864480 *)NULL);
		return;
	}
}
extern "C"  void NodeEnumerator_Dispose_m475269499_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NodeEnumerator_t648190100 * _thisAdjusted = reinterpret_cast<NodeEnumerator_t648190100 *>(__this + 1);
	NodeEnumerator_Dispose_m475269499(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.RBTree/NodeEnumerator::check_version()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3881240472;
extern Il2CppCodeGenString* _stringLiteral4064854551;
extern const uint32_t NodeEnumerator_check_version_m1999562667_MetadataUsageId;
extern "C"  void NodeEnumerator_check_version_m1999562667 (NodeEnumerator_t648190100 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NodeEnumerator_check_version_m1999562667_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RBTree_t1544615604 * L_0 = __this->get_tree_0();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, _stringLiteral3881240472, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		uint32_t L_2 = __this->get_version_1();
		RBTree_t1544615604 * L_3 = __this->get_tree_0();
		NullCheck(L_3);
		uint32_t L_4 = L_3->get_version_2();
		if ((((int32_t)L_2) == ((int32_t)L_4)))
		{
			goto IL_0037;
		}
	}
	{
		InvalidOperationException_t721527559 * L_5 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_5, _stringLiteral4064854551, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0037:
	{
		return;
	}
}
extern "C"  void NodeEnumerator_check_version_m1999562667_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NodeEnumerator_t648190100 * _thisAdjusted = reinterpret_cast<NodeEnumerator_t648190100 *>(__this + 1);
	NodeEnumerator_check_version_m1999562667(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.RBTree/NodeEnumerator::check_current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1373650187;
extern const uint32_t NodeEnumerator_check_current_m135648288_MetadataUsageId;
extern "C"  void NodeEnumerator_check_current_m135648288 (NodeEnumerator_t648190100 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NodeEnumerator_check_current_m135648288_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NodeEnumerator_check_version_m1999562667(__this, /*hidden argument*/NULL);
		Stack_1_t3586864480 * L_0 = __this->get_pennants_2();
		if (L_0)
		{
			goto IL_001c;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, _stringLiteral1373650187, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001c:
	{
		return;
	}
}
extern "C"  void NodeEnumerator_check_current_m135648288_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NodeEnumerator_t648190100 * _thisAdjusted = reinterpret_cast<NodeEnumerator_t648190100 *>(__this + 1);
	NodeEnumerator_check_current_m135648288(_thisAdjusted, method);
}
// Conversion methods for marshalling of: System.Collections.Generic.RBTree/NodeEnumerator
extern "C" void NodeEnumerator_t648190100_marshal_pinvoke(const NodeEnumerator_t648190100& unmarshaled, NodeEnumerator_t648190100_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___tree_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'tree' of type 'NodeEnumerator': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___tree_0Exception);
}
extern "C" void NodeEnumerator_t648190100_marshal_pinvoke_back(const NodeEnumerator_t648190100_marshaled_pinvoke& marshaled, NodeEnumerator_t648190100& unmarshaled)
{
	Il2CppCodeGenException* ___tree_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'tree' of type 'NodeEnumerator': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___tree_0Exception);
}
// Conversion method for clean up from marshalling of: System.Collections.Generic.RBTree/NodeEnumerator
extern "C" void NodeEnumerator_t648190100_marshal_pinvoke_cleanup(NodeEnumerator_t648190100_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: System.Collections.Generic.RBTree/NodeEnumerator
extern "C" void NodeEnumerator_t648190100_marshal_com(const NodeEnumerator_t648190100& unmarshaled, NodeEnumerator_t648190100_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___tree_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'tree' of type 'NodeEnumerator': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___tree_0Exception);
}
extern "C" void NodeEnumerator_t648190100_marshal_com_back(const NodeEnumerator_t648190100_marshaled_com& marshaled, NodeEnumerator_t648190100& unmarshaled)
{
	Il2CppCodeGenException* ___tree_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'tree' of type 'NodeEnumerator': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___tree_0Exception);
}
// Conversion method for clean up from marshalling of: System.Collections.Generic.RBTree/NodeEnumerator
extern "C" void NodeEnumerator_t648190100_marshal_com_cleanup(NodeEnumerator_t648190100_marshaled_com& marshaled)
{
}
// System.Void System.Collections.Specialized.HybridDictionary::.ctor()
extern "C"  void HybridDictionary__ctor_m884012539 (HybridDictionary_t290043810 * __this, const MethodInfo* method)
{
	{
		HybridDictionary__ctor_m890422665(__this, 0, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Specialized.HybridDictionary::.ctor(System.Int32,System.Boolean)
extern Il2CppClass* CaseInsensitiveComparer_t157661140_il2cpp_TypeInfo_var;
extern Il2CppClass* CaseInsensitiveHashCodeProvider_t2307530285_il2cpp_TypeInfo_var;
extern Il2CppClass* ListDictionary_t3458713452_il2cpp_TypeInfo_var;
extern Il2CppClass* Hashtable_t909839986_il2cpp_TypeInfo_var;
extern const uint32_t HybridDictionary__ctor_m890422665_MetadataUsageId;
extern "C"  void HybridDictionary__ctor_m890422665 (HybridDictionary_t290043810 * __this, int32_t ___initialSize0, bool ___caseInsensitive1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HybridDictionary__ctor_m890422665_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	CaseInsensitiveComparer_t157661140 * G_B3_0 = NULL;
	CaseInsensitiveHashCodeProvider_t2307530285 * G_B6_0 = NULL;
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		bool L_0 = ___caseInsensitive1;
		__this->set_caseInsensitive_0(L_0);
		bool L_1 = ___caseInsensitive1;
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CaseInsensitiveComparer_t157661140_il2cpp_TypeInfo_var);
		CaseInsensitiveComparer_t157661140 * L_2 = CaseInsensitiveComparer_get_DefaultInvariant_m93592233(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		goto IL_001e;
	}

IL_001d:
	{
		G_B3_0 = ((CaseInsensitiveComparer_t157661140 *)(NULL));
	}

IL_001e:
	{
		V_0 = G_B3_0;
		bool L_3 = ___caseInsensitive1;
		if (!L_3)
		{
			goto IL_002f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CaseInsensitiveHashCodeProvider_t2307530285_il2cpp_TypeInfo_var);
		CaseInsensitiveHashCodeProvider_t2307530285 * L_4 = CaseInsensitiveHashCodeProvider_get_DefaultInvariant_m1293455465(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B6_0 = L_4;
		goto IL_0030;
	}

IL_002f:
	{
		G_B6_0 = ((CaseInsensitiveHashCodeProvider_t2307530285 *)(NULL));
	}

IL_0030:
	{
		V_1 = G_B6_0;
		int32_t L_5 = ___initialSize0;
		if ((((int32_t)L_5) > ((int32_t)((int32_t)10))))
		{
			goto IL_004a;
		}
	}
	{
		Il2CppObject * L_6 = V_0;
		ListDictionary_t3458713452 * L_7 = (ListDictionary_t3458713452 *)il2cpp_codegen_object_new(ListDictionary_t3458713452_il2cpp_TypeInfo_var);
		ListDictionary__ctor_m319558045(L_7, L_6, /*hidden argument*/NULL);
		__this->set_list_2(L_7);
		goto IL_0058;
	}

IL_004a:
	{
		int32_t L_8 = ___initialSize0;
		Il2CppObject * L_9 = V_1;
		Il2CppObject * L_10 = V_0;
		Hashtable_t909839986 * L_11 = (Hashtable_t909839986 *)il2cpp_codegen_object_new(Hashtable_t909839986_il2cpp_TypeInfo_var);
		Hashtable__ctor_m4106078798(L_11, L_8, L_9, L_10, /*hidden argument*/NULL);
		__this->set_hashtable_1(L_11);
	}

IL_0058:
	{
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Specialized.HybridDictionary::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * HybridDictionary_System_Collections_IEnumerable_GetEnumerator_m1536454198 (HybridDictionary_t290043810 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = HybridDictionary_GetEnumerator_m3570171579(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Collections.IDictionary System.Collections.Specialized.HybridDictionary::get_inner()
extern "C"  Il2CppObject * HybridDictionary_get_inner_m3223590742 (HybridDictionary_t290043810 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * G_B3_0 = NULL;
	{
		ListDictionary_t3458713452 * L_0 = __this->get_list_2();
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		Hashtable_t909839986 * L_1 = __this->get_hashtable_1();
		V_0 = L_1;
		Il2CppObject * L_2 = V_0;
		G_B3_0 = L_2;
		goto IL_001e;
	}

IL_0018:
	{
		ListDictionary_t3458713452 * L_3 = __this->get_list_2();
		G_B3_0 = ((Il2CppObject *)(L_3));
	}

IL_001e:
	{
		return G_B3_0;
	}
}
// System.Int32 System.Collections.Specialized.HybridDictionary::get_Count()
extern Il2CppClass* ICollection_t91669223_il2cpp_TypeInfo_var;
extern const uint32_t HybridDictionary_get_Count_m1347303215_MetadataUsageId;
extern "C"  int32_t HybridDictionary_get_Count_m1347303215 (HybridDictionary_t290043810 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HybridDictionary_get_Count_m1347303215_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = HybridDictionary_get_inner_m3223590742(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.ICollection::get_Count() */, ICollection_t91669223_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Boolean System.Collections.Specialized.HybridDictionary::get_IsSynchronized()
extern "C"  bool HybridDictionary_get_IsSynchronized_m3763975586 (HybridDictionary_t290043810 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Specialized.HybridDictionary::get_Item(System.Object)
extern Il2CppClass* IDictionary_t596158605_il2cpp_TypeInfo_var;
extern const uint32_t HybridDictionary_get_Item_m1077394870_MetadataUsageId;
extern "C"  Il2CppObject * HybridDictionary_get_Item_m1077394870 (HybridDictionary_t290043810 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HybridDictionary_get_Item_m1077394870_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = HybridDictionary_get_inner_m3223590742(__this, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___key0;
		NullCheck(L_0);
		Il2CppObject * L_2 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t596158605_il2cpp_TypeInfo_var, L_0, L_1);
		return L_2;
	}
}
// System.Void System.Collections.Specialized.HybridDictionary::set_Item(System.Object,System.Object)
extern Il2CppClass* IDictionary_t596158605_il2cpp_TypeInfo_var;
extern const uint32_t HybridDictionary_set_Item_m4049906479_MetadataUsageId;
extern "C"  void HybridDictionary_set_Item_m4049906479 (HybridDictionary_t290043810 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HybridDictionary_set_Item_m4049906479_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = HybridDictionary_get_inner_m3223590742(__this, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___key0;
		Il2CppObject * L_2 = ___value1;
		NullCheck(L_0);
		InterfaceActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(1 /* System.Void System.Collections.IDictionary::set_Item(System.Object,System.Object) */, IDictionary_t596158605_il2cpp_TypeInfo_var, L_0, L_1, L_2);
		ListDictionary_t3458713452 * L_3 = __this->get_list_2();
		if (!L_3)
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_4 = HybridDictionary_get_Count_m1347303215(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_4) <= ((int32_t)((int32_t)10))))
		{
			goto IL_002b;
		}
	}
	{
		HybridDictionary_Switch_m146896129(__this, /*hidden argument*/NULL);
	}

IL_002b:
	{
		return;
	}
}
// System.Collections.ICollection System.Collections.Specialized.HybridDictionary::get_Keys()
extern Il2CppClass* IDictionary_t596158605_il2cpp_TypeInfo_var;
extern const uint32_t HybridDictionary_get_Keys_m4114678936_MetadataUsageId;
extern "C"  Il2CppObject * HybridDictionary_get_Keys_m4114678936 (HybridDictionary_t290043810 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HybridDictionary_get_Keys_m4114678936_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = HybridDictionary_get_inner_m3223590742(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Collections.ICollection System.Collections.IDictionary::get_Keys() */, IDictionary_t596158605_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Object System.Collections.Specialized.HybridDictionary::get_SyncRoot()
extern "C"  Il2CppObject * HybridDictionary_get_SyncRoot_m1170821686 (HybridDictionary_t290043810 * __this, const MethodInfo* method)
{
	{
		return __this;
	}
}
// System.Collections.ICollection System.Collections.Specialized.HybridDictionary::get_Values()
extern Il2CppClass* IDictionary_t596158605_il2cpp_TypeInfo_var;
extern const uint32_t HybridDictionary_get_Values_m3351943902_MetadataUsageId;
extern "C"  Il2CppObject * HybridDictionary_get_Values_m3351943902 (HybridDictionary_t290043810 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HybridDictionary_get_Values_m3351943902_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = HybridDictionary_get_inner_m3223590742(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(3 /* System.Collections.ICollection System.Collections.IDictionary::get_Values() */, IDictionary_t596158605_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Void System.Collections.Specialized.HybridDictionary::Add(System.Object,System.Object)
extern Il2CppClass* IDictionary_t596158605_il2cpp_TypeInfo_var;
extern const uint32_t HybridDictionary_Add_m1037587952_MetadataUsageId;
extern "C"  void HybridDictionary_Add_m1037587952 (HybridDictionary_t290043810 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HybridDictionary_Add_m1037587952_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = HybridDictionary_get_inner_m3223590742(__this, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___key0;
		Il2CppObject * L_2 = ___value1;
		NullCheck(L_0);
		InterfaceActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(4 /* System.Void System.Collections.IDictionary::Add(System.Object,System.Object) */, IDictionary_t596158605_il2cpp_TypeInfo_var, L_0, L_1, L_2);
		ListDictionary_t3458713452 * L_3 = __this->get_list_2();
		if (!L_3)
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_4 = HybridDictionary_get_Count_m1347303215(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_4) <= ((int32_t)((int32_t)10))))
		{
			goto IL_002b;
		}
	}
	{
		HybridDictionary_Switch_m146896129(__this, /*hidden argument*/NULL);
	}

IL_002b:
	{
		return;
	}
}
// System.Void System.Collections.Specialized.HybridDictionary::Clear()
extern Il2CppClass* IDictionary_t596158605_il2cpp_TypeInfo_var;
extern const uint32_t HybridDictionary_Clear_m2579520244_MetadataUsageId;
extern "C"  void HybridDictionary_Clear_m2579520244 (HybridDictionary_t290043810 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HybridDictionary_Clear_m2579520244_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = HybridDictionary_get_inner_m3223590742(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		InterfaceActionInvoker0::Invoke(5 /* System.Void System.Collections.IDictionary::Clear() */, IDictionary_t596158605_il2cpp_TypeInfo_var, L_0);
		return;
	}
}
// System.Boolean System.Collections.Specialized.HybridDictionary::Contains(System.Object)
extern Il2CppClass* IDictionary_t596158605_il2cpp_TypeInfo_var;
extern const uint32_t HybridDictionary_Contains_m1057090920_MetadataUsageId;
extern "C"  bool HybridDictionary_Contains_m1057090920 (HybridDictionary_t290043810 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HybridDictionary_Contains_m1057090920_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = HybridDictionary_get_inner_m3223590742(__this, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___key0;
		NullCheck(L_0);
		bool L_2 = InterfaceFuncInvoker1< bool, Il2CppObject * >::Invoke(6 /* System.Boolean System.Collections.IDictionary::Contains(System.Object) */, IDictionary_t596158605_il2cpp_TypeInfo_var, L_0, L_1);
		return L_2;
	}
}
// System.Void System.Collections.Specialized.HybridDictionary::CopyTo(System.Array,System.Int32)
extern Il2CppClass* ICollection_t91669223_il2cpp_TypeInfo_var;
extern const uint32_t HybridDictionary_CopyTo_m2422525990_MetadataUsageId;
extern "C"  void HybridDictionary_CopyTo_m2422525990 (HybridDictionary_t290043810 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HybridDictionary_CopyTo_m2422525990_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = HybridDictionary_get_inner_m3223590742(__this, /*hidden argument*/NULL);
		Il2CppArray * L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck(L_0);
		InterfaceActionInvoker2< Il2CppArray *, int32_t >::Invoke(3 /* System.Void System.Collections.ICollection::CopyTo(System.Array,System.Int32) */, ICollection_t91669223_il2cpp_TypeInfo_var, L_0, L_1, L_2);
		return;
	}
}
// System.Collections.IDictionaryEnumerator System.Collections.Specialized.HybridDictionary::GetEnumerator()
extern Il2CppClass* IDictionary_t596158605_il2cpp_TypeInfo_var;
extern const uint32_t HybridDictionary_GetEnumerator_m3570171579_MetadataUsageId;
extern "C"  Il2CppObject * HybridDictionary_GetEnumerator_m3570171579 (HybridDictionary_t290043810 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HybridDictionary_GetEnumerator_m3570171579_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = HybridDictionary_get_inner_m3223590742(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(7 /* System.Collections.IDictionaryEnumerator System.Collections.IDictionary::GetEnumerator() */, IDictionary_t596158605_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Void System.Collections.Specialized.HybridDictionary::Remove(System.Object)
extern Il2CppClass* IDictionary_t596158605_il2cpp_TypeInfo_var;
extern const uint32_t HybridDictionary_Remove_m2059808027_MetadataUsageId;
extern "C"  void HybridDictionary_Remove_m2059808027 (HybridDictionary_t290043810 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HybridDictionary_Remove_m2059808027_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = HybridDictionary_get_inner_m3223590742(__this, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___key0;
		NullCheck(L_0);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(8 /* System.Void System.Collections.IDictionary::Remove(System.Object) */, IDictionary_t596158605_il2cpp_TypeInfo_var, L_0, L_1);
		return;
	}
}
// System.Void System.Collections.Specialized.HybridDictionary::Switch()
extern Il2CppClass* CaseInsensitiveComparer_t157661140_il2cpp_TypeInfo_var;
extern Il2CppClass* CaseInsensitiveHashCodeProvider_t2307530285_il2cpp_TypeInfo_var;
extern Il2CppClass* Hashtable_t909839986_il2cpp_TypeInfo_var;
extern const uint32_t HybridDictionary_Switch_m146896129_MetadataUsageId;
extern "C"  void HybridDictionary_Switch_m146896129 (HybridDictionary_t290043810 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HybridDictionary_Switch_m146896129_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	CaseInsensitiveComparer_t157661140 * G_B3_0 = NULL;
	CaseInsensitiveHashCodeProvider_t2307530285 * G_B6_0 = NULL;
	{
		bool L_0 = __this->get_caseInsensitive_0();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CaseInsensitiveComparer_t157661140_il2cpp_TypeInfo_var);
		CaseInsensitiveComparer_t157661140 * L_1 = CaseInsensitiveComparer_get_DefaultInvariant_m93592233(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B3_0 = L_1;
		goto IL_0016;
	}

IL_0015:
	{
		G_B3_0 = ((CaseInsensitiveComparer_t157661140 *)(NULL));
	}

IL_0016:
	{
		V_0 = G_B3_0;
		bool L_2 = __this->get_caseInsensitive_0();
		if (!L_2)
		{
			goto IL_002c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CaseInsensitiveHashCodeProvider_t2307530285_il2cpp_TypeInfo_var);
		CaseInsensitiveHashCodeProvider_t2307530285 * L_3 = CaseInsensitiveHashCodeProvider_get_DefaultInvariant_m1293455465(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B6_0 = L_3;
		goto IL_002d;
	}

IL_002c:
	{
		G_B6_0 = ((CaseInsensitiveHashCodeProvider_t2307530285 *)(NULL));
	}

IL_002d:
	{
		V_1 = G_B6_0;
		ListDictionary_t3458713452 * L_4 = __this->get_list_2();
		Il2CppObject * L_5 = V_1;
		Il2CppObject * L_6 = V_0;
		Hashtable_t909839986 * L_7 = (Hashtable_t909839986 *)il2cpp_codegen_object_new(Hashtable_t909839986_il2cpp_TypeInfo_var);
		Hashtable__ctor_m3742489710(L_7, L_4, L_5, L_6, /*hidden argument*/NULL);
		__this->set_hashtable_1(L_7);
		ListDictionary_t3458713452 * L_8 = __this->get_list_2();
		NullCheck(L_8);
		ListDictionary_Clear_m3646179034(L_8, /*hidden argument*/NULL);
		__this->set_list_2((ListDictionary_t3458713452 *)NULL);
		return;
	}
}
// System.Void System.Collections.Specialized.ListDictionary::.ctor()
extern "C"  void ListDictionary__ctor_m3573908233 (ListDictionary_t3458713452 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		__this->set_count_0(0);
		__this->set_version_1(0);
		__this->set_comparer_3((Il2CppObject *)NULL);
		__this->set_head_2((DictionaryNode_t2725637098 *)NULL);
		return;
	}
}
// System.Void System.Collections.Specialized.ListDictionary::.ctor(System.Collections.IComparer)
extern "C"  void ListDictionary__ctor_m319558045 (ListDictionary_t3458713452 * __this, Il2CppObject * ___comparer0, const MethodInfo* method)
{
	{
		ListDictionary__ctor_m3573908233(__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___comparer0;
		__this->set_comparer_3(L_0);
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Specialized.ListDictionary::System.Collections.IEnumerable.GetEnumerator()
extern Il2CppClass* DictionaryNodeEnumerator_t1923170152_il2cpp_TypeInfo_var;
extern const uint32_t ListDictionary_System_Collections_IEnumerable_GetEnumerator_m2244736244_MetadataUsageId;
extern "C"  Il2CppObject * ListDictionary_System_Collections_IEnumerable_GetEnumerator_m2244736244 (ListDictionary_t3458713452 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ListDictionary_System_Collections_IEnumerable_GetEnumerator_m2244736244_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		DictionaryNodeEnumerator_t1923170152 * L_0 = (DictionaryNodeEnumerator_t1923170152 *)il2cpp_codegen_object_new(DictionaryNodeEnumerator_t1923170152_il2cpp_TypeInfo_var);
		DictionaryNodeEnumerator__ctor_m3657252825(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Collections.Specialized.ListDictionary/DictionaryNode System.Collections.Specialized.ListDictionary::FindEntry(System.Object)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppClass* IComparer_t3952557350_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3021628599;
extern Il2CppCodeGenString* _stringLiteral2250706828;
extern const uint32_t ListDictionary_FindEntry_m1690278153_MetadataUsageId;
extern "C"  DictionaryNode_t2725637098 * ListDictionary_FindEntry_m1690278153 (ListDictionary_t3458713452 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ListDictionary_FindEntry_m1690278153_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DictionaryNode_t2725637098 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___key0;
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2624491786(L_1, _stringLiteral3021628599, _stringLiteral2250706828, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		DictionaryNode_t2725637098 * L_2 = __this->get_head_2();
		V_0 = L_2;
		Il2CppObject * L_3 = __this->get_comparer_3();
		if (L_3)
		{
			goto IL_0055;
		}
	}
	{
		goto IL_004a;
	}

IL_002d:
	{
		Il2CppObject * L_4 = ___key0;
		DictionaryNode_t2725637098 * L_5 = V_0;
		NullCheck(L_5);
		Il2CppObject * L_6 = L_5->get_key_0();
		NullCheck(L_4);
		bool L_7 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, L_4, L_6);
		if (!L_7)
		{
			goto IL_0043;
		}
	}
	{
		goto IL_0050;
	}

IL_0043:
	{
		DictionaryNode_t2725637098 * L_8 = V_0;
		NullCheck(L_8);
		DictionaryNode_t2725637098 * L_9 = L_8->get_next_2();
		V_0 = L_9;
	}

IL_004a:
	{
		DictionaryNode_t2725637098 * L_10 = V_0;
		if (L_10)
		{
			goto IL_002d;
		}
	}

IL_0050:
	{
		goto IL_0083;
	}

IL_0055:
	{
		goto IL_007d;
	}

IL_005a:
	{
		Il2CppObject * L_11 = __this->get_comparer_3();
		Il2CppObject * L_12 = ___key0;
		DictionaryNode_t2725637098 * L_13 = V_0;
		NullCheck(L_13);
		Il2CppObject * L_14 = L_13->get_key_0();
		NullCheck(L_11);
		int32_t L_15 = InterfaceFuncInvoker2< int32_t, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Int32 System.Collections.IComparer::Compare(System.Object,System.Object) */, IComparer_t3952557350_il2cpp_TypeInfo_var, L_11, L_12, L_14);
		if (L_15)
		{
			goto IL_0076;
		}
	}
	{
		goto IL_0083;
	}

IL_0076:
	{
		DictionaryNode_t2725637098 * L_16 = V_0;
		NullCheck(L_16);
		DictionaryNode_t2725637098 * L_17 = L_16->get_next_2();
		V_0 = L_17;
	}

IL_007d:
	{
		DictionaryNode_t2725637098 * L_18 = V_0;
		if (L_18)
		{
			goto IL_005a;
		}
	}

IL_0083:
	{
		DictionaryNode_t2725637098 * L_19 = V_0;
		return L_19;
	}
}
// System.Collections.Specialized.ListDictionary/DictionaryNode System.Collections.Specialized.ListDictionary::FindEntry(System.Object,System.Collections.Specialized.ListDictionary/DictionaryNode&)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppClass* IComparer_t3952557350_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3021628599;
extern Il2CppCodeGenString* _stringLiteral2250706828;
extern const uint32_t ListDictionary_FindEntry_m432748847_MetadataUsageId;
extern "C"  DictionaryNode_t2725637098 * ListDictionary_FindEntry_m432748847 (ListDictionary_t3458713452 * __this, Il2CppObject * ___key0, DictionaryNode_t2725637098 ** ___prev1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ListDictionary_FindEntry_m432748847_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DictionaryNode_t2725637098 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___key0;
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2624491786(L_1, _stringLiteral3021628599, _stringLiteral2250706828, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		DictionaryNode_t2725637098 * L_2 = __this->get_head_2();
		V_0 = L_2;
		DictionaryNode_t2725637098 ** L_3 = ___prev1;
		*((Il2CppObject **)(L_3)) = (Il2CppObject *)NULL;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_3), (Il2CppObject *)NULL);
		Il2CppObject * L_4 = __this->get_comparer_3();
		if (L_4)
		{
			goto IL_005b;
		}
	}
	{
		goto IL_0050;
	}

IL_0030:
	{
		Il2CppObject * L_5 = ___key0;
		DictionaryNode_t2725637098 * L_6 = V_0;
		NullCheck(L_6);
		Il2CppObject * L_7 = L_6->get_key_0();
		NullCheck(L_5);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, L_5, L_7);
		if (!L_8)
		{
			goto IL_0046;
		}
	}
	{
		goto IL_0056;
	}

IL_0046:
	{
		DictionaryNode_t2725637098 ** L_9 = ___prev1;
		DictionaryNode_t2725637098 * L_10 = V_0;
		*((Il2CppObject **)(L_9)) = (Il2CppObject *)L_10;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_9), (Il2CppObject *)L_10);
		DictionaryNode_t2725637098 * L_11 = V_0;
		NullCheck(L_11);
		DictionaryNode_t2725637098 * L_12 = L_11->get_next_2();
		V_0 = L_12;
	}

IL_0050:
	{
		DictionaryNode_t2725637098 * L_13 = V_0;
		if (L_13)
		{
			goto IL_0030;
		}
	}

IL_0056:
	{
		goto IL_008c;
	}

IL_005b:
	{
		goto IL_0086;
	}

IL_0060:
	{
		Il2CppObject * L_14 = __this->get_comparer_3();
		Il2CppObject * L_15 = ___key0;
		DictionaryNode_t2725637098 * L_16 = V_0;
		NullCheck(L_16);
		Il2CppObject * L_17 = L_16->get_key_0();
		NullCheck(L_14);
		int32_t L_18 = InterfaceFuncInvoker2< int32_t, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Int32 System.Collections.IComparer::Compare(System.Object,System.Object) */, IComparer_t3952557350_il2cpp_TypeInfo_var, L_14, L_15, L_17);
		if (L_18)
		{
			goto IL_007c;
		}
	}
	{
		goto IL_008c;
	}

IL_007c:
	{
		DictionaryNode_t2725637098 ** L_19 = ___prev1;
		DictionaryNode_t2725637098 * L_20 = V_0;
		*((Il2CppObject **)(L_19)) = (Il2CppObject *)L_20;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_19), (Il2CppObject *)L_20);
		DictionaryNode_t2725637098 * L_21 = V_0;
		NullCheck(L_21);
		DictionaryNode_t2725637098 * L_22 = L_21->get_next_2();
		V_0 = L_22;
	}

IL_0086:
	{
		DictionaryNode_t2725637098 * L_23 = V_0;
		if (L_23)
		{
			goto IL_0060;
		}
	}

IL_008c:
	{
		DictionaryNode_t2725637098 * L_24 = V_0;
		return L_24;
	}
}
// System.Void System.Collections.Specialized.ListDictionary::AddImpl(System.Object,System.Object,System.Collections.Specialized.ListDictionary/DictionaryNode)
extern Il2CppClass* DictionaryNode_t2725637098_il2cpp_TypeInfo_var;
extern const uint32_t ListDictionary_AddImpl_m2604632092_MetadataUsageId;
extern "C"  void ListDictionary_AddImpl_m2604632092 (ListDictionary_t3458713452 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, DictionaryNode_t2725637098 * ___prev2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ListDictionary_AddImpl_m2604632092_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		DictionaryNode_t2725637098 * L_0 = ___prev2;
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		Il2CppObject * L_1 = ___key0;
		Il2CppObject * L_2 = ___value1;
		DictionaryNode_t2725637098 * L_3 = __this->get_head_2();
		DictionaryNode_t2725637098 * L_4 = (DictionaryNode_t2725637098 *)il2cpp_codegen_object_new(DictionaryNode_t2725637098_il2cpp_TypeInfo_var);
		DictionaryNode__ctor_m2839151850(L_4, L_1, L_2, L_3, /*hidden argument*/NULL);
		__this->set_head_2(L_4);
		goto IL_0031;
	}

IL_001e:
	{
		DictionaryNode_t2725637098 * L_5 = ___prev2;
		Il2CppObject * L_6 = ___key0;
		Il2CppObject * L_7 = ___value1;
		DictionaryNode_t2725637098 * L_8 = ___prev2;
		NullCheck(L_8);
		DictionaryNode_t2725637098 * L_9 = L_8->get_next_2();
		DictionaryNode_t2725637098 * L_10 = (DictionaryNode_t2725637098 *)il2cpp_codegen_object_new(DictionaryNode_t2725637098_il2cpp_TypeInfo_var);
		DictionaryNode__ctor_m2839151850(L_10, L_6, L_7, L_9, /*hidden argument*/NULL);
		NullCheck(L_5);
		L_5->set_next_2(L_10);
	}

IL_0031:
	{
		int32_t L_11 = __this->get_count_0();
		__this->set_count_0(((int32_t)((int32_t)L_11+(int32_t)1)));
		int32_t L_12 = __this->get_version_1();
		__this->set_version_1(((int32_t)((int32_t)L_12+(int32_t)1)));
		return;
	}
}
// System.Int32 System.Collections.Specialized.ListDictionary::get_Count()
extern "C"  int32_t ListDictionary_get_Count_m2397535557 (ListDictionary_t3458713452 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_count_0();
		return L_0;
	}
}
// System.Boolean System.Collections.Specialized.ListDictionary::get_IsSynchronized()
extern "C"  bool ListDictionary_get_IsSynchronized_m2961717108 (ListDictionary_t3458713452 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Specialized.ListDictionary::get_SyncRoot()
extern "C"  Il2CppObject * ListDictionary_get_SyncRoot_m1212356752 (ListDictionary_t3458713452 * __this, const MethodInfo* method)
{
	{
		return __this;
	}
}
// System.Void System.Collections.Specialized.ListDictionary::CopyTo(System.Array,System.Int32)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppClass* IndexOutOfRangeException_t3527622107_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* DictionaryEntry_t3048875398_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1185213181;
extern Il2CppCodeGenString* _stringLiteral1003153106;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern Il2CppCodeGenString* _stringLiteral3863425438;
extern Il2CppCodeGenString* _stringLiteral1043308353;
extern Il2CppCodeGenString* _stringLiteral1406820407;
extern const uint32_t ListDictionary_CopyTo_m1124479600_MetadataUsageId;
extern "C"  void ListDictionary_CopyTo_m1124479600 (ListDictionary_t3458713452 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ListDictionary_CopyTo_m1124479600_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DictionaryEntry_t3048875398  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppArray * L_0 = ___array0;
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2624491786(L_1, _stringLiteral1185213181, _stringLiteral1003153106, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		int32_t L_2 = ___index1;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_002d;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_3 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m4234257711(L_3, _stringLiteral1460639766, _stringLiteral3863425438, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002d:
	{
		int32_t L_4 = ___index1;
		Il2CppArray * L_5 = ___array0;
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m1498215565(L_5, /*hidden argument*/NULL);
		if ((((int32_t)L_4) <= ((int32_t)L_6)))
		{
			goto IL_0044;
		}
	}
	{
		IndexOutOfRangeException_t3527622107 * L_7 = (IndexOutOfRangeException_t3527622107 *)il2cpp_codegen_object_new(IndexOutOfRangeException_t3527622107_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m1847153122(L_7, _stringLiteral1043308353, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_0044:
	{
		int32_t L_8 = ListDictionary_get_Count_m2397535557(__this, /*hidden argument*/NULL);
		Il2CppArray * L_9 = ___array0;
		NullCheck(L_9);
		int32_t L_10 = Array_get_Length_m1498215565(L_9, /*hidden argument*/NULL);
		int32_t L_11 = ___index1;
		if ((((int32_t)L_8) <= ((int32_t)((int32_t)((int32_t)L_10-(int32_t)L_11)))))
		{
			goto IL_0062;
		}
	}
	{
		ArgumentException_t3259014390 * L_12 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_12, _stringLiteral1406820407, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_12);
	}

IL_0062:
	{
		Il2CppObject * L_13 = ListDictionary_GetEnumerator_m1511032609(__this, /*hidden argument*/NULL);
		V_1 = L_13;
	}

IL_0069:
	try
	{ // begin try (depth: 1)
		{
			goto IL_008c;
		}

IL_006e:
		{
			Il2CppObject * L_14 = V_1;
			NullCheck(L_14);
			Il2CppObject * L_15 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_14);
			V_0 = ((*(DictionaryEntry_t3048875398 *)((DictionaryEntry_t3048875398 *)UnBox (L_15, DictionaryEntry_t3048875398_il2cpp_TypeInfo_var))));
			Il2CppArray * L_16 = ___array0;
			DictionaryEntry_t3048875398  L_17 = V_0;
			DictionaryEntry_t3048875398  L_18 = L_17;
			Il2CppObject * L_19 = Box(DictionaryEntry_t3048875398_il2cpp_TypeInfo_var, &L_18);
			int32_t L_20 = ___index1;
			int32_t L_21 = L_20;
			___index1 = ((int32_t)((int32_t)L_21+(int32_t)1));
			NullCheck(L_16);
			Array_SetValue_m2652759566(L_16, L_19, L_21, /*hidden argument*/NULL);
		}

IL_008c:
		{
			Il2CppObject * L_22 = V_1;
			NullCheck(L_22);
			bool L_23 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_22);
			if (L_23)
			{
				goto IL_006e;
			}
		}

IL_0097:
		{
			IL2CPP_LEAVE(0xAE, FINALLY_009c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_009c;
	}

FINALLY_009c:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_24 = V_1;
			V_2 = ((Il2CppObject *)IsInst(L_24, IDisposable_t2427283555_il2cpp_TypeInfo_var));
			Il2CppObject * L_25 = V_2;
			if (L_25)
			{
				goto IL_00a7;
			}
		}

IL_00a6:
		{
			IL2CPP_END_FINALLY(156)
		}

IL_00a7:
		{
			Il2CppObject * L_26 = V_2;
			NullCheck(L_26);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_26);
			IL2CPP_END_FINALLY(156)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(156)
	{
		IL2CPP_JUMP_TBL(0xAE, IL_00ae)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00ae:
	{
		return;
	}
}
// System.Object System.Collections.Specialized.ListDictionary::get_Item(System.Object)
extern "C"  Il2CppObject * ListDictionary_get_Item_m3967158800 (ListDictionary_t3458713452 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	DictionaryNode_t2725637098 * V_0 = NULL;
	Il2CppObject * G_B3_0 = NULL;
	{
		Il2CppObject * L_0 = ___key0;
		DictionaryNode_t2725637098 * L_1 = ListDictionary_FindEntry_m1690278153(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		DictionaryNode_t2725637098 * L_2 = V_0;
		if (L_2)
		{
			goto IL_0014;
		}
	}
	{
		G_B3_0 = NULL;
		goto IL_001a;
	}

IL_0014:
	{
		DictionaryNode_t2725637098 * L_3 = V_0;
		NullCheck(L_3);
		Il2CppObject * L_4 = L_3->get_value_1();
		G_B3_0 = L_4;
	}

IL_001a:
	{
		return G_B3_0;
	}
}
// System.Void System.Collections.Specialized.ListDictionary::set_Item(System.Object,System.Object)
extern "C"  void ListDictionary_set_Item_m972551921 (ListDictionary_t3458713452 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	DictionaryNode_t2725637098 * V_0 = NULL;
	DictionaryNode_t2725637098 * V_1 = NULL;
	{
		Il2CppObject * L_0 = ___key0;
		DictionaryNode_t2725637098 * L_1 = ListDictionary_FindEntry_m432748847(__this, L_0, (&V_0), /*hidden argument*/NULL);
		V_1 = L_1;
		DictionaryNode_t2725637098 * L_2 = V_1;
		if (!L_2)
		{
			goto IL_001c;
		}
	}
	{
		DictionaryNode_t2725637098 * L_3 = V_1;
		Il2CppObject * L_4 = ___value1;
		NullCheck(L_3);
		L_3->set_value_1(L_4);
		goto IL_0025;
	}

IL_001c:
	{
		Il2CppObject * L_5 = ___key0;
		Il2CppObject * L_6 = ___value1;
		DictionaryNode_t2725637098 * L_7 = V_0;
		ListDictionary_AddImpl_m2604632092(__this, L_5, L_6, L_7, /*hidden argument*/NULL);
	}

IL_0025:
	{
		return;
	}
}
// System.Collections.ICollection System.Collections.Specialized.ListDictionary::get_Keys()
extern Il2CppClass* DictionaryNodeCollection_t528898270_il2cpp_TypeInfo_var;
extern const uint32_t ListDictionary_get_Keys_m3344920214_MetadataUsageId;
extern "C"  Il2CppObject * ListDictionary_get_Keys_m3344920214 (ListDictionary_t3458713452 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ListDictionary_get_Keys_m3344920214_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		DictionaryNodeCollection_t528898270 * L_0 = (DictionaryNodeCollection_t528898270 *)il2cpp_codegen_object_new(DictionaryNodeCollection_t528898270_il2cpp_TypeInfo_var);
		DictionaryNodeCollection__ctor_m445138956(L_0, __this, (bool)1, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Collections.ICollection System.Collections.Specialized.ListDictionary::get_Values()
extern Il2CppClass* DictionaryNodeCollection_t528898270_il2cpp_TypeInfo_var;
extern const uint32_t ListDictionary_get_Values_m4194383876_MetadataUsageId;
extern "C"  Il2CppObject * ListDictionary_get_Values_m4194383876 (ListDictionary_t3458713452 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ListDictionary_get_Values_m4194383876_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		DictionaryNodeCollection_t528898270 * L_0 = (DictionaryNodeCollection_t528898270 *)il2cpp_codegen_object_new(DictionaryNodeCollection_t528898270_il2cpp_TypeInfo_var);
		DictionaryNodeCollection__ctor_m445138956(L_0, __this, (bool)0, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void System.Collections.Specialized.ListDictionary::Add(System.Object,System.Object)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3021628599;
extern Il2CppCodeGenString* _stringLiteral1659354982;
extern const uint32_t ListDictionary_Add_m4276262662_MetadataUsageId;
extern "C"  void ListDictionary_Add_m4276262662 (ListDictionary_t3458713452 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ListDictionary_Add_m4276262662_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DictionaryNode_t2725637098 * V_0 = NULL;
	DictionaryNode_t2725637098 * V_1 = NULL;
	{
		Il2CppObject * L_0 = ___key0;
		DictionaryNode_t2725637098 * L_1 = ListDictionary_FindEntry_m432748847(__this, L_0, (&V_0), /*hidden argument*/NULL);
		V_1 = L_1;
		DictionaryNode_t2725637098 * L_2 = V_1;
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		ArgumentException_t3259014390 * L_3 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m544251339(L_3, _stringLiteral3021628599, _stringLiteral1659354982, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0020:
	{
		Il2CppObject * L_4 = ___key0;
		Il2CppObject * L_5 = ___value1;
		DictionaryNode_t2725637098 * L_6 = V_0;
		ListDictionary_AddImpl_m2604632092(__this, L_4, L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Specialized.ListDictionary::Clear()
extern "C"  void ListDictionary_Clear_m3646179034 (ListDictionary_t3458713452 * __this, const MethodInfo* method)
{
	{
		__this->set_head_2((DictionaryNode_t2725637098 *)NULL);
		__this->set_count_0(0);
		int32_t L_0 = __this->get_version_1();
		__this->set_version_1(((int32_t)((int32_t)L_0+(int32_t)1)));
		return;
	}
}
// System.Boolean System.Collections.Specialized.ListDictionary::Contains(System.Object)
extern "C"  bool ListDictionary_Contains_m2267260730 (ListDictionary_t3458713452 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		DictionaryNode_t2725637098 * L_1 = ListDictionary_FindEntry_m1690278153(__this, L_0, /*hidden argument*/NULL);
		return (bool)((((int32_t)((((Il2CppObject*)(DictionaryNode_t2725637098 *)L_1) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Collections.IDictionaryEnumerator System.Collections.Specialized.ListDictionary::GetEnumerator()
extern Il2CppClass* DictionaryNodeEnumerator_t1923170152_il2cpp_TypeInfo_var;
extern const uint32_t ListDictionary_GetEnumerator_m1511032609_MetadataUsageId;
extern "C"  Il2CppObject * ListDictionary_GetEnumerator_m1511032609 (ListDictionary_t3458713452 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ListDictionary_GetEnumerator_m1511032609_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		DictionaryNodeEnumerator_t1923170152 * L_0 = (DictionaryNodeEnumerator_t1923170152 *)il2cpp_codegen_object_new(DictionaryNodeEnumerator_t1923170152_il2cpp_TypeInfo_var);
		DictionaryNodeEnumerator__ctor_m3657252825(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void System.Collections.Specialized.ListDictionary::Remove(System.Object)
extern "C"  void ListDictionary_Remove_m2294694061 (ListDictionary_t3458713452 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	DictionaryNode_t2725637098 * V_0 = NULL;
	DictionaryNode_t2725637098 * V_1 = NULL;
	{
		Il2CppObject * L_0 = ___key0;
		DictionaryNode_t2725637098 * L_1 = ListDictionary_FindEntry_m432748847(__this, L_0, (&V_0), /*hidden argument*/NULL);
		V_1 = L_1;
		DictionaryNode_t2725637098 * L_2 = V_1;
		if (L_2)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		DictionaryNode_t2725637098 * L_3 = V_0;
		if (L_3)
		{
			goto IL_0028;
		}
	}
	{
		DictionaryNode_t2725637098 * L_4 = V_1;
		NullCheck(L_4);
		DictionaryNode_t2725637098 * L_5 = L_4->get_next_2();
		__this->set_head_2(L_5);
		goto IL_0034;
	}

IL_0028:
	{
		DictionaryNode_t2725637098 * L_6 = V_0;
		DictionaryNode_t2725637098 * L_7 = V_1;
		NullCheck(L_7);
		DictionaryNode_t2725637098 * L_8 = L_7->get_next_2();
		NullCheck(L_6);
		L_6->set_next_2(L_8);
	}

IL_0034:
	{
		DictionaryNode_t2725637098 * L_9 = V_1;
		NullCheck(L_9);
		L_9->set_value_1(NULL);
		int32_t L_10 = __this->get_count_0();
		__this->set_count_0(((int32_t)((int32_t)L_10-(int32_t)1)));
		int32_t L_11 = __this->get_version_1();
		__this->set_version_1(((int32_t)((int32_t)L_11+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Specialized.ListDictionary/DictionaryNode::.ctor(System.Object,System.Object,System.Collections.Specialized.ListDictionary/DictionaryNode)
extern "C"  void DictionaryNode__ctor_m2839151850 (DictionaryNode_t2725637098 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, DictionaryNode_t2725637098 * ___next2, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___key0;
		__this->set_key_0(L_0);
		Il2CppObject * L_1 = ___value1;
		__this->set_value_1(L_1);
		DictionaryNode_t2725637098 * L_2 = ___next2;
		__this->set_next_2(L_2);
		return;
	}
}
// System.Void System.Collections.Specialized.ListDictionary/DictionaryNodeCollection::.ctor(System.Collections.Specialized.ListDictionary,System.Boolean)
extern "C"  void DictionaryNodeCollection__ctor_m445138956 (DictionaryNodeCollection_t528898270 * __this, ListDictionary_t3458713452 * ___dict0, bool ___isKeyList1, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		ListDictionary_t3458713452 * L_0 = ___dict0;
		__this->set_dict_0(L_0);
		bool L_1 = ___isKeyList1;
		__this->set_isKeyList_1(L_1);
		return;
	}
}
// System.Int32 System.Collections.Specialized.ListDictionary/DictionaryNodeCollection::get_Count()
extern "C"  int32_t DictionaryNodeCollection_get_Count_m3891757076 (DictionaryNodeCollection_t528898270 * __this, const MethodInfo* method)
{
	{
		ListDictionary_t3458713452 * L_0 = __this->get_dict_0();
		NullCheck(L_0);
		int32_t L_1 = ListDictionary_get_Count_m2397535557(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Collections.Specialized.ListDictionary/DictionaryNodeCollection::get_IsSynchronized()
extern "C"  bool DictionaryNodeCollection_get_IsSynchronized_m4126252101 (DictionaryNodeCollection_t528898270 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Specialized.ListDictionary/DictionaryNodeCollection::get_SyncRoot()
extern "C"  Il2CppObject * DictionaryNodeCollection_get_SyncRoot_m4292609649 (DictionaryNodeCollection_t528898270 * __this, const MethodInfo* method)
{
	{
		ListDictionary_t3458713452 * L_0 = __this->get_dict_0();
		NullCheck(L_0);
		Il2CppObject * L_1 = ListDictionary_get_SyncRoot_m1212356752(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void System.Collections.Specialized.ListDictionary/DictionaryNodeCollection::CopyTo(System.Array,System.Int32)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppClass* IndexOutOfRangeException_t3527622107_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1185213181;
extern Il2CppCodeGenString* _stringLiteral1003153106;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern Il2CppCodeGenString* _stringLiteral3863425438;
extern Il2CppCodeGenString* _stringLiteral1043308353;
extern Il2CppCodeGenString* _stringLiteral1406820407;
extern const uint32_t DictionaryNodeCollection_CopyTo_m3104713877_MetadataUsageId;
extern "C"  void DictionaryNodeCollection_CopyTo_m3104713877 (DictionaryNodeCollection_t528898270 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DictionaryNodeCollection_CopyTo_m3104713877_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppArray * L_0 = ___array0;
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2624491786(L_1, _stringLiteral1185213181, _stringLiteral1003153106, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		int32_t L_2 = ___index1;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_002d;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_3 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m4234257711(L_3, _stringLiteral1460639766, _stringLiteral3863425438, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002d:
	{
		int32_t L_4 = ___index1;
		Il2CppArray * L_5 = ___array0;
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m1498215565(L_5, /*hidden argument*/NULL);
		if ((((int32_t)L_4) <= ((int32_t)L_6)))
		{
			goto IL_0044;
		}
	}
	{
		IndexOutOfRangeException_t3527622107 * L_7 = (IndexOutOfRangeException_t3527622107 *)il2cpp_codegen_object_new(IndexOutOfRangeException_t3527622107_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m1847153122(L_7, _stringLiteral1043308353, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_0044:
	{
		int32_t L_8 = DictionaryNodeCollection_get_Count_m3891757076(__this, /*hidden argument*/NULL);
		Il2CppArray * L_9 = ___array0;
		NullCheck(L_9);
		int32_t L_10 = Array_get_Length_m1498215565(L_9, /*hidden argument*/NULL);
		int32_t L_11 = ___index1;
		if ((((int32_t)L_8) <= ((int32_t)((int32_t)((int32_t)L_10-(int32_t)L_11)))))
		{
			goto IL_0062;
		}
	}
	{
		ArgumentException_t3259014390 * L_12 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_12, _stringLiteral1406820407, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_12);
	}

IL_0062:
	{
		Il2CppObject * L_13 = DictionaryNodeCollection_GetEnumerator_m4231738996(__this, /*hidden argument*/NULL);
		V_1 = L_13;
	}

IL_0069:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0082;
		}

IL_006e:
		{
			Il2CppObject * L_14 = V_1;
			NullCheck(L_14);
			Il2CppObject * L_15 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_14);
			V_0 = L_15;
			Il2CppArray * L_16 = ___array0;
			Il2CppObject * L_17 = V_0;
			int32_t L_18 = ___index1;
			int32_t L_19 = L_18;
			___index1 = ((int32_t)((int32_t)L_19+(int32_t)1));
			NullCheck(L_16);
			Array_SetValue_m2652759566(L_16, L_17, L_19, /*hidden argument*/NULL);
		}

IL_0082:
		{
			Il2CppObject * L_20 = V_1;
			NullCheck(L_20);
			bool L_21 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_20);
			if (L_21)
			{
				goto IL_006e;
			}
		}

IL_008d:
		{
			IL2CPP_LEAVE(0xA4, FINALLY_0092);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0092;
	}

FINALLY_0092:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_22 = V_1;
			V_2 = ((Il2CppObject *)IsInst(L_22, IDisposable_t2427283555_il2cpp_TypeInfo_var));
			Il2CppObject * L_23 = V_2;
			if (L_23)
			{
				goto IL_009d;
			}
		}

IL_009c:
		{
			IL2CPP_END_FINALLY(146)
		}

IL_009d:
		{
			Il2CppObject * L_24 = V_2;
			NullCheck(L_24);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_24);
			IL2CPP_END_FINALLY(146)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(146)
	{
		IL2CPP_JUMP_TBL(0xA4, IL_00a4)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00a4:
	{
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Specialized.ListDictionary/DictionaryNodeCollection::GetEnumerator()
extern Il2CppClass* DictionaryNodeCollectionEnumerator_t2037848305_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryNodeCollection_GetEnumerator_m4231738996_MetadataUsageId;
extern "C"  Il2CppObject * DictionaryNodeCollection_GetEnumerator_m4231738996 (DictionaryNodeCollection_t528898270 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DictionaryNodeCollection_GetEnumerator_m4231738996_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ListDictionary_t3458713452 * L_0 = __this->get_dict_0();
		NullCheck(L_0);
		Il2CppObject * L_1 = ListDictionary_GetEnumerator_m1511032609(L_0, /*hidden argument*/NULL);
		bool L_2 = __this->get_isKeyList_1();
		DictionaryNodeCollectionEnumerator_t2037848305 * L_3 = (DictionaryNodeCollectionEnumerator_t2037848305 *)il2cpp_codegen_object_new(DictionaryNodeCollectionEnumerator_t2037848305_il2cpp_TypeInfo_var);
		DictionaryNodeCollectionEnumerator__ctor_m84810679(L_3, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void System.Collections.Specialized.ListDictionary/DictionaryNodeCollection/DictionaryNodeCollectionEnumerator::.ctor(System.Collections.IDictionaryEnumerator,System.Boolean)
extern "C"  void DictionaryNodeCollectionEnumerator__ctor_m84810679 (DictionaryNodeCollectionEnumerator_t2037848305 * __this, Il2CppObject * ___inner0, bool ___isKeyList1, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___inner0;
		__this->set_inner_0(L_0);
		bool L_1 = ___isKeyList1;
		__this->set_isKeyList_1(L_1);
		return;
	}
}
// System.Object System.Collections.Specialized.ListDictionary/DictionaryNodeCollection/DictionaryNodeCollectionEnumerator::get_Current()
extern Il2CppClass* IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryNodeCollectionEnumerator_get_Current_m3220527494_MetadataUsageId;
extern "C"  Il2CppObject * DictionaryNodeCollectionEnumerator_get_Current_m3220527494 (DictionaryNodeCollectionEnumerator_t2037848305 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DictionaryNodeCollectionEnumerator_get_Current_m3220527494_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * G_B3_0 = NULL;
	{
		bool L_0 = __this->get_isKeyList_1();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		Il2CppObject * L_1 = __this->get_inner_0();
		NullCheck(L_1);
		Il2CppObject * L_2 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(1 /* System.Object System.Collections.IDictionaryEnumerator::get_Key() */, IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var, L_1);
		G_B3_0 = L_2;
		goto IL_0026;
	}

IL_001b:
	{
		Il2CppObject * L_3 = __this->get_inner_0();
		NullCheck(L_3);
		Il2CppObject * L_4 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Object System.Collections.IDictionaryEnumerator::get_Value() */, IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var, L_3);
		G_B3_0 = L_4;
	}

IL_0026:
	{
		return G_B3_0;
	}
}
// System.Boolean System.Collections.Specialized.ListDictionary/DictionaryNodeCollection/DictionaryNodeCollectionEnumerator::MoveNext()
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryNodeCollectionEnumerator_MoveNext_m848269647_MetadataUsageId;
extern "C"  bool DictionaryNodeCollectionEnumerator_MoveNext_m848269647 (DictionaryNodeCollectionEnumerator_t2037848305 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DictionaryNodeCollectionEnumerator_MoveNext_m848269647_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get_inner_0();
		NullCheck(L_0);
		bool L_1 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Void System.Collections.Specialized.ListDictionary/DictionaryNodeCollection/DictionaryNodeCollectionEnumerator::Reset()
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryNodeCollectionEnumerator_Reset_m2279681240_MetadataUsageId;
extern "C"  void DictionaryNodeCollectionEnumerator_Reset_m2279681240 (DictionaryNodeCollectionEnumerator_t2037848305 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DictionaryNodeCollectionEnumerator_Reset_m2279681240_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get_inner_0();
		NullCheck(L_0);
		InterfaceActionInvoker0::Invoke(2 /* System.Void System.Collections.IEnumerator::Reset() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_0);
		return;
	}
}
// System.Void System.Collections.Specialized.ListDictionary/DictionaryNodeEnumerator::.ctor(System.Collections.Specialized.ListDictionary)
extern "C"  void DictionaryNodeEnumerator__ctor_m3657252825 (DictionaryNodeEnumerator_t1923170152 * __this, ListDictionary_t3458713452 * ___dict0, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		ListDictionary_t3458713452 * L_0 = ___dict0;
		__this->set_dict_0(L_0);
		ListDictionary_t3458713452 * L_1 = ___dict0;
		NullCheck(L_1);
		int32_t L_2 = L_1->get_version_1();
		__this->set_version_3(L_2);
		DictionaryNodeEnumerator_Reset_m1131304747(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Specialized.ListDictionary/DictionaryNodeEnumerator::FailFast()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral126210674;
extern const uint32_t DictionaryNodeEnumerator_FailFast_m3568492354_MetadataUsageId;
extern "C"  void DictionaryNodeEnumerator_FailFast_m3568492354 (DictionaryNodeEnumerator_t1923170152 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DictionaryNodeEnumerator_FailFast_m3568492354_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_version_3();
		ListDictionary_t3458713452 * L_1 = __this->get_dict_0();
		NullCheck(L_1);
		int32_t L_2 = L_1->get_version_1();
		if ((((int32_t)L_0) == ((int32_t)L_2)))
		{
			goto IL_0021;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, _stringLiteral126210674, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0021:
	{
		return;
	}
}
// System.Boolean System.Collections.Specialized.ListDictionary/DictionaryNodeEnumerator::MoveNext()
extern "C"  bool DictionaryNodeEnumerator_MoveNext_m3110756080 (DictionaryNodeEnumerator_t1923170152 * __this, const MethodInfo* method)
{
	DictionaryNodeEnumerator_t1923170152 * G_B5_0 = NULL;
	DictionaryNodeEnumerator_t1923170152 * G_B4_0 = NULL;
	DictionaryNode_t2725637098 * G_B6_0 = NULL;
	DictionaryNodeEnumerator_t1923170152 * G_B6_1 = NULL;
	{
		DictionaryNodeEnumerator_FailFast_m3568492354(__this, /*hidden argument*/NULL);
		DictionaryNode_t2725637098 * L_0 = __this->get_current_2();
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		bool L_1 = __this->get_isAtStart_1();
		if (L_1)
		{
			goto IL_001e;
		}
	}
	{
		return (bool)0;
	}

IL_001e:
	{
		bool L_2 = __this->get_isAtStart_1();
		G_B4_0 = __this;
		if (!L_2)
		{
			G_B5_0 = __this;
			goto IL_003a;
		}
	}
	{
		ListDictionary_t3458713452 * L_3 = __this->get_dict_0();
		NullCheck(L_3);
		DictionaryNode_t2725637098 * L_4 = L_3->get_head_2();
		G_B6_0 = L_4;
		G_B6_1 = G_B4_0;
		goto IL_0045;
	}

IL_003a:
	{
		DictionaryNode_t2725637098 * L_5 = __this->get_current_2();
		NullCheck(L_5);
		DictionaryNode_t2725637098 * L_6 = L_5->get_next_2();
		G_B6_0 = L_6;
		G_B6_1 = G_B5_0;
	}

IL_0045:
	{
		NullCheck(G_B6_1);
		G_B6_1->set_current_2(G_B6_0);
		__this->set_isAtStart_1((bool)0);
		DictionaryNode_t2725637098 * L_7 = __this->get_current_2();
		return (bool)((((int32_t)((((Il2CppObject*)(DictionaryNode_t2725637098 *)L_7) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Collections.Specialized.ListDictionary/DictionaryNodeEnumerator::Reset()
extern "C"  void DictionaryNodeEnumerator_Reset_m1131304747 (DictionaryNodeEnumerator_t1923170152 * __this, const MethodInfo* method)
{
	{
		DictionaryNodeEnumerator_FailFast_m3568492354(__this, /*hidden argument*/NULL);
		__this->set_isAtStart_1((bool)1);
		__this->set_current_2((DictionaryNode_t2725637098 *)NULL);
		return;
	}
}
// System.Object System.Collections.Specialized.ListDictionary/DictionaryNodeEnumerator::get_Current()
extern Il2CppClass* DictionaryEntry_t3048875398_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryNodeEnumerator_get_Current_m980909215_MetadataUsageId;
extern "C"  Il2CppObject * DictionaryNodeEnumerator_get_Current_m980909215 (DictionaryNodeEnumerator_t1923170152 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DictionaryNodeEnumerator_get_Current_m980909215_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		DictionaryEntry_t3048875398  L_0 = DictionaryNodeEnumerator_get_Entry_m2402517470(__this, /*hidden argument*/NULL);
		DictionaryEntry_t3048875398  L_1 = L_0;
		Il2CppObject * L_2 = Box(DictionaryEntry_t3048875398_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Collections.Specialized.ListDictionary/DictionaryNode System.Collections.Specialized.ListDictionary/DictionaryNodeEnumerator::get_DictionaryNode()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2956752624;
extern const uint32_t DictionaryNodeEnumerator_get_DictionaryNode_m3521889822_MetadataUsageId;
extern "C"  DictionaryNode_t2725637098 * DictionaryNodeEnumerator_get_DictionaryNode_m3521889822 (DictionaryNodeEnumerator_t1923170152 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DictionaryNodeEnumerator_get_DictionaryNode_m3521889822_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		DictionaryNodeEnumerator_FailFast_m3568492354(__this, /*hidden argument*/NULL);
		DictionaryNode_t2725637098 * L_0 = __this->get_current_2();
		if (L_0)
		{
			goto IL_001c;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, _stringLiteral2956752624, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001c:
	{
		DictionaryNode_t2725637098 * L_2 = __this->get_current_2();
		return L_2;
	}
}
// System.Collections.DictionaryEntry System.Collections.Specialized.ListDictionary/DictionaryNodeEnumerator::get_Entry()
extern "C"  DictionaryEntry_t3048875398  DictionaryNodeEnumerator_get_Entry_m2402517470 (DictionaryNodeEnumerator_t1923170152 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		DictionaryNode_t2725637098 * L_0 = DictionaryNodeEnumerator_get_DictionaryNode_m3521889822(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Il2CppObject * L_1 = L_0->get_key_0();
		V_0 = L_1;
		Il2CppObject * L_2 = V_0;
		DictionaryNode_t2725637098 * L_3 = __this->get_current_2();
		NullCheck(L_3);
		Il2CppObject * L_4 = L_3->get_value_1();
		DictionaryEntry_t3048875398  L_5;
		memset(&L_5, 0, sizeof(L_5));
		DictionaryEntry__ctor_m2901884110(&L_5, L_2, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Object System.Collections.Specialized.ListDictionary/DictionaryNodeEnumerator::get_Key()
extern "C"  Il2CppObject * DictionaryNodeEnumerator_get_Key_m4038343473 (DictionaryNodeEnumerator_t1923170152 * __this, const MethodInfo* method)
{
	{
		DictionaryNode_t2725637098 * L_0 = DictionaryNodeEnumerator_get_DictionaryNode_m3521889822(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Il2CppObject * L_1 = L_0->get_key_0();
		return L_1;
	}
}
// System.Object System.Collections.Specialized.ListDictionary/DictionaryNodeEnumerator::get_Value()
extern "C"  Il2CppObject * DictionaryNodeEnumerator_get_Value_m2179504825 (DictionaryNodeEnumerator_t1923170152 * __this, const MethodInfo* method)
{
	{
		DictionaryNode_t2725637098 * L_0 = DictionaryNodeEnumerator_get_DictionaryNode_m3521889822(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Il2CppObject * L_1 = L_0->get_value_1();
		return L_1;
	}
}
// System.Void System.Collections.Specialized.NameObjectCollectionBase::.ctor()
extern Il2CppClass* CaseInsensitiveHashCodeProvider_t2307530285_il2cpp_TypeInfo_var;
extern Il2CppClass* CaseInsensitiveComparer_t157661140_il2cpp_TypeInfo_var;
extern const uint32_t NameObjectCollectionBase__ctor_m2433753948_MetadataUsageId;
extern "C"  void NameObjectCollectionBase__ctor_m2433753948 (NameObjectCollectionBase_t2034248631 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NameObjectCollectionBase__ctor_m2433753948_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		__this->set_m_readonly_6((bool)0);
		IL2CPP_RUNTIME_CLASS_INIT(CaseInsensitiveHashCodeProvider_t2307530285_il2cpp_TypeInfo_var);
		CaseInsensitiveHashCodeProvider_t2307530285 * L_0 = CaseInsensitiveHashCodeProvider_get_DefaultInvariant_m1293455465(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_m_hashprovider_3(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(CaseInsensitiveComparer_t157661140_il2cpp_TypeInfo_var);
		CaseInsensitiveComparer_t157661140 * L_1 = CaseInsensitiveComparer_get_DefaultInvariant_m93592233(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_m_comparer_4(L_1);
		__this->set_m_defCapacity_5(0);
		NameObjectCollectionBase_Init_m1271766820(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Specialized.NameObjectCollectionBase::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void NameObjectCollectionBase__ctor_m241668161 (NameObjectCollectionBase_t2034248631 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		SerializationInfo_t228987430 * L_0 = ___info0;
		__this->set_infoCopy_7(L_0);
		return;
	}
}
// System.Boolean System.Collections.Specialized.NameObjectCollectionBase::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool NameObjectCollectionBase_System_Collections_ICollection_get_IsSynchronized_m4271190852 (NameObjectCollectionBase_t2034248631 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Specialized.NameObjectCollectionBase::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * NameObjectCollectionBase_System_Collections_ICollection_get_SyncRoot_m1491738 (NameObjectCollectionBase_t2034248631 * __this, const MethodInfo* method)
{
	{
		return __this;
	}
}
// System.Void System.Collections.Specialized.NameObjectCollectionBase::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern Il2CppClass* ICollection_t91669223_il2cpp_TypeInfo_var;
extern const uint32_t NameObjectCollectionBase_System_Collections_ICollection_CopyTo_m810189168_MetadataUsageId;
extern "C"  void NameObjectCollectionBase_System_Collections_ICollection_CopyTo_m810189168 (NameObjectCollectionBase_t2034248631 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NameObjectCollectionBase_System_Collections_ICollection_CopyTo_m810189168_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		KeysCollection_t633582367 * L_0 = VirtFuncInvoker0< KeysCollection_t633582367 * >::Invoke(11 /* System.Collections.Specialized.NameObjectCollectionBase/KeysCollection System.Collections.Specialized.NameObjectCollectionBase::get_Keys() */, __this);
		Il2CppArray * L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck(L_0);
		InterfaceActionInvoker2< Il2CppArray *, int32_t >::Invoke(3 /* System.Void System.Collections.ICollection::CopyTo(System.Array,System.Int32) */, ICollection_t91669223_il2cpp_TypeInfo_var, L_0, L_1, L_2);
		return;
	}
}
// System.Void System.Collections.Specialized.NameObjectCollectionBase::Init()
extern Il2CppClass* Hashtable_t909839986_il2cpp_TypeInfo_var;
extern Il2CppClass* ArrayList_t4252133567_il2cpp_TypeInfo_var;
extern const uint32_t NameObjectCollectionBase_Init_m1271766820_MetadataUsageId;
extern "C"  void NameObjectCollectionBase_Init_m1271766820 (NameObjectCollectionBase_t2034248631 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NameObjectCollectionBase_Init_m1271766820_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get_equality_comparer_9();
		if (!L_0)
		{
			goto IL_0027;
		}
	}
	{
		int32_t L_1 = __this->get_m_defCapacity_5();
		Il2CppObject * L_2 = __this->get_equality_comparer_9();
		Hashtable_t909839986 * L_3 = (Hashtable_t909839986 *)il2cpp_codegen_object_new(Hashtable_t909839986_il2cpp_TypeInfo_var);
		Hashtable__ctor_m622168569(L_3, L_1, L_2, /*hidden argument*/NULL);
		__this->set_m_ItemsContainer_0(L_3);
		goto IL_0044;
	}

IL_0027:
	{
		int32_t L_4 = __this->get_m_defCapacity_5();
		Il2CppObject * L_5 = __this->get_m_hashprovider_3();
		Il2CppObject * L_6 = __this->get_m_comparer_4();
		Hashtable_t909839986 * L_7 = (Hashtable_t909839986 *)il2cpp_codegen_object_new(Hashtable_t909839986_il2cpp_TypeInfo_var);
		Hashtable__ctor_m4106078798(L_7, L_4, L_5, L_6, /*hidden argument*/NULL);
		__this->set_m_ItemsContainer_0(L_7);
	}

IL_0044:
	{
		ArrayList_t4252133567 * L_8 = (ArrayList_t4252133567 *)il2cpp_codegen_object_new(ArrayList_t4252133567_il2cpp_TypeInfo_var);
		ArrayList__ctor_m4012174379(L_8, /*hidden argument*/NULL);
		__this->set_m_ItemsArray_2(L_8);
		__this->set_m_NullKeyItem_1((_Item_t3244489099 *)NULL);
		return;
	}
}
// System.Collections.Specialized.NameObjectCollectionBase/KeysCollection System.Collections.Specialized.NameObjectCollectionBase::get_Keys()
extern Il2CppClass* KeysCollection_t633582367_il2cpp_TypeInfo_var;
extern const uint32_t NameObjectCollectionBase_get_Keys_m446053925_MetadataUsageId;
extern "C"  KeysCollection_t633582367 * NameObjectCollectionBase_get_Keys_m446053925 (NameObjectCollectionBase_t2034248631 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NameObjectCollectionBase_get_Keys_m446053925_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		KeysCollection_t633582367 * L_0 = __this->get_keyscoll_8();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		KeysCollection_t633582367 * L_1 = (KeysCollection_t633582367 *)il2cpp_codegen_object_new(KeysCollection_t633582367_il2cpp_TypeInfo_var);
		KeysCollection__ctor_m847580183(L_1, __this, /*hidden argument*/NULL);
		__this->set_keyscoll_8(L_1);
	}

IL_0017:
	{
		KeysCollection_t633582367 * L_2 = __this->get_keyscoll_8();
		return L_2;
	}
}
// System.Collections.IEnumerator System.Collections.Specialized.NameObjectCollectionBase::GetEnumerator()
extern Il2CppClass* _KeysEnumerator_t1718269396_il2cpp_TypeInfo_var;
extern const uint32_t NameObjectCollectionBase_GetEnumerator_m646294968_MetadataUsageId;
extern "C"  Il2CppObject * NameObjectCollectionBase_GetEnumerator_m646294968 (NameObjectCollectionBase_t2034248631 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NameObjectCollectionBase_GetEnumerator_m646294968_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		_KeysEnumerator_t1718269396 * L_0 = (_KeysEnumerator_t1718269396 *)il2cpp_codegen_object_new(_KeysEnumerator_t1718269396_il2cpp_TypeInfo_var);
		_KeysEnumerator__ctor_m995107394(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void System.Collections.Specialized.NameObjectCollectionBase::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const Il2CppType* IEqualityComparer_t2716208158_0_0_0_var;
extern const Il2CppType* Int32_t2071877448_0_0_0_var;
extern const Il2CppType* IHashCodeProvider_t1980576455_0_0_0_var;
extern const Il2CppType* IComparer_t3952557350_0_0_0_var;
extern const Il2CppType* StringU5BU5D_t1642385972_0_0_0_var;
extern const Il2CppType* ObjectU5BU5D_t3614634134_0_0_0_var;
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* _Item_t3244489099_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2792112382;
extern Il2CppCodeGenString* _stringLiteral1017838394;
extern Il2CppCodeGenString* _stringLiteral4699698;
extern Il2CppCodeGenString* _stringLiteral815656099;
extern Il2CppCodeGenString* _stringLiteral3743961169;
extern Il2CppCodeGenString* _stringLiteral1225783828;
extern Il2CppCodeGenString* _stringLiteral1554779067;
extern Il2CppCodeGenString* _stringLiteral1857678454;
extern Il2CppCodeGenString* _stringLiteral1188955108;
extern const uint32_t NameObjectCollectionBase_GetObjectData_m699981132_MetadataUsageId;
extern "C"  void NameObjectCollectionBase_GetObjectData_m699981132 (NameObjectCollectionBase_t2034248631 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NameObjectCollectionBase_GetObjectData_m699981132_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	StringU5BU5D_t1642385972* V_1 = NULL;
	ObjectU5BU5D_t3614634134* V_2 = NULL;
	int32_t V_3 = 0;
	_Item_t3244489099 * V_4 = NULL;
	Il2CppObject * V_5 = NULL;
	Il2CppObject * V_6 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		SerializationInfo_t228987430 * L_0 = ___info0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, _stringLiteral2792112382, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(14 /* System.Int32 System.Collections.Specialized.NameObjectCollectionBase::get_Count() */, __this);
		V_0 = L_2;
		int32_t L_3 = V_0;
		V_1 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)L_3));
		int32_t L_4 = V_0;
		V_2 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)L_4));
		V_3 = 0;
		ArrayList_t4252133567 * L_5 = __this->get_m_ItemsArray_2();
		NullCheck(L_5);
		Il2CppObject * L_6 = VirtFuncInvoker0< Il2CppObject * >::Invoke(43 /* System.Collections.IEnumerator System.Collections.ArrayList::GetEnumerator() */, L_5);
		V_5 = L_6;
	}

IL_0035:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0060;
		}

IL_003a:
		{
			Il2CppObject * L_7 = V_5;
			NullCheck(L_7);
			Il2CppObject * L_8 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_7);
			V_4 = ((_Item_t3244489099 *)CastclassClass(L_8, _Item_t3244489099_il2cpp_TypeInfo_var));
			StringU5BU5D_t1642385972* L_9 = V_1;
			int32_t L_10 = V_3;
			_Item_t3244489099 * L_11 = V_4;
			NullCheck(L_11);
			String_t* L_12 = L_11->get_key_0();
			NullCheck(L_9);
			ArrayElementTypeCheck (L_9, L_12);
			(L_9)->SetAt(static_cast<il2cpp_array_size_t>(L_10), (String_t*)L_12);
			ObjectU5BU5D_t3614634134* L_13 = V_2;
			int32_t L_14 = V_3;
			_Item_t3244489099 * L_15 = V_4;
			NullCheck(L_15);
			Il2CppObject * L_16 = L_15->get_value_1();
			NullCheck(L_13);
			ArrayElementTypeCheck (L_13, L_16);
			(L_13)->SetAt(static_cast<il2cpp_array_size_t>(L_14), (Il2CppObject *)L_16);
			int32_t L_17 = V_3;
			V_3 = ((int32_t)((int32_t)L_17+(int32_t)1));
		}

IL_0060:
		{
			Il2CppObject * L_18 = V_5;
			NullCheck(L_18);
			bool L_19 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_18);
			if (L_19)
			{
				goto IL_003a;
			}
		}

IL_006c:
		{
			IL2CPP_LEAVE(0x87, FINALLY_0071);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0071;
	}

FINALLY_0071:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_20 = V_5;
			V_6 = ((Il2CppObject *)IsInst(L_20, IDisposable_t2427283555_il2cpp_TypeInfo_var));
			Il2CppObject * L_21 = V_6;
			if (L_21)
			{
				goto IL_007f;
			}
		}

IL_007e:
		{
			IL2CPP_END_FINALLY(113)
		}

IL_007f:
		{
			Il2CppObject * L_22 = V_6;
			NullCheck(L_22);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_22);
			IL2CPP_END_FINALLY(113)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(113)
	{
		IL2CPP_JUMP_TBL(0x87, IL_0087)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0087:
	{
		Il2CppObject * L_23 = __this->get_equality_comparer_9();
		if (!L_23)
		{
			goto IL_00cd;
		}
	}
	{
		SerializationInfo_t228987430 * L_24 = ___info0;
		Il2CppObject * L_25 = __this->get_equality_comparer_9();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_26 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(IEqualityComparer_t2716208158_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_24);
		SerializationInfo_AddValue_m1781549036(L_24, _stringLiteral1017838394, L_25, L_26, /*hidden argument*/NULL);
		SerializationInfo_t228987430 * L_27 = ___info0;
		int32_t L_28 = 4;
		Il2CppObject * L_29 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_28);
		Type_t * L_30 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Int32_t2071877448_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_27);
		SerializationInfo_AddValue_m1781549036(L_27, _stringLiteral4699698, L_29, L_30, /*hidden argument*/NULL);
		goto IL_011e;
	}

IL_00cd:
	{
		SerializationInfo_t228987430 * L_31 = ___info0;
		Il2CppObject * L_32 = __this->get_m_hashprovider_3();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_33 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(IHashCodeProvider_t1980576455_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_31);
		SerializationInfo_AddValue_m1781549036(L_31, _stringLiteral815656099, L_32, L_33, /*hidden argument*/NULL);
		SerializationInfo_t228987430 * L_34 = ___info0;
		Il2CppObject * L_35 = __this->get_m_comparer_4();
		Type_t * L_36 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(IComparer_t3952557350_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_34);
		SerializationInfo_AddValue_m1781549036(L_34, _stringLiteral3743961169, L_35, L_36, /*hidden argument*/NULL);
		SerializationInfo_t228987430 * L_37 = ___info0;
		int32_t L_38 = 2;
		Il2CppObject * L_39 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_38);
		Type_t * L_40 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Int32_t2071877448_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_37);
		SerializationInfo_AddValue_m1781549036(L_37, _stringLiteral4699698, L_39, L_40, /*hidden argument*/NULL);
	}

IL_011e:
	{
		SerializationInfo_t228987430 * L_41 = ___info0;
		bool L_42 = __this->get_m_readonly_6();
		NullCheck(L_41);
		SerializationInfo_AddValue_m1192926088(L_41, _stringLiteral1225783828, L_42, /*hidden argument*/NULL);
		SerializationInfo_t228987430 * L_43 = ___info0;
		int32_t L_44 = V_0;
		NullCheck(L_43);
		SerializationInfo_AddValue_m902275108(L_43, _stringLiteral1554779067, L_44, /*hidden argument*/NULL);
		SerializationInfo_t228987430 * L_45 = ___info0;
		StringU5BU5D_t1642385972* L_46 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_47 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(StringU5BU5D_t1642385972_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_45);
		SerializationInfo_AddValue_m1781549036(L_45, _stringLiteral1857678454, (Il2CppObject *)(Il2CppObject *)L_46, L_47, /*hidden argument*/NULL);
		SerializationInfo_t228987430 * L_48 = ___info0;
		ObjectU5BU5D_t3614634134* L_49 = V_2;
		Type_t * L_50 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(ObjectU5BU5D_t3614634134_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_48);
		SerializationInfo_AddValue_m1781549036(L_48, _stringLiteral1188955108, (Il2CppObject *)(Il2CppObject *)L_49, L_50, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 System.Collections.Specialized.NameObjectCollectionBase::get_Count()
extern "C"  int32_t NameObjectCollectionBase_get_Count_m2353593692 (NameObjectCollectionBase_t2034248631 * __this, const MethodInfo* method)
{
	{
		ArrayList_t4252133567 * L_0 = __this->get_m_ItemsArray_2();
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_0);
		return L_1;
	}
}
// System.Void System.Collections.Specialized.NameObjectCollectionBase::OnDeserialization(System.Object)
extern const Il2CppType* IHashCodeProvider_t1980576455_0_0_0_var;
extern const Il2CppType* IEqualityComparer_t2716208158_0_0_0_var;
extern const Il2CppType* IComparer_t3952557350_0_0_0_var;
extern const Il2CppType* StringU5BU5D_t1642385972_0_0_0_var;
extern const Il2CppType* ObjectU5BU5D_t3614634134_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* IHashCodeProvider_t1980576455_il2cpp_TypeInfo_var;
extern Il2CppClass* IEqualityComparer_t2716208158_il2cpp_TypeInfo_var;
extern Il2CppClass* IComparer_t3952557350_il2cpp_TypeInfo_var;
extern Il2CppClass* SerializationException_t753258759_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral815656099;
extern Il2CppCodeGenString* _stringLiteral1017838394;
extern Il2CppCodeGenString* _stringLiteral3743961169;
extern Il2CppCodeGenString* _stringLiteral3639910873;
extern Il2CppCodeGenString* _stringLiteral1225783828;
extern Il2CppCodeGenString* _stringLiteral1857678454;
extern Il2CppCodeGenString* _stringLiteral2897760885;
extern Il2CppCodeGenString* _stringLiteral1188955108;
extern Il2CppCodeGenString* _stringLiteral1826620485;
extern const uint32_t NameObjectCollectionBase_OnDeserialization_m3478980442_MetadataUsageId;
extern "C"  void NameObjectCollectionBase_OnDeserialization_m3478980442 (NameObjectCollectionBase_t2034248631 * __this, Il2CppObject * ___sender0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NameObjectCollectionBase_OnDeserialization_m3478980442_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	SerializationInfo_t228987430 * V_0 = NULL;
	StringU5BU5D_t1642385972* V_1 = NULL;
	ObjectU5BU5D_t3614634134* V_2 = NULL;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		SerializationInfo_t228987430 * L_0 = __this->get_infoCopy_7();
		V_0 = L_0;
		SerializationInfo_t228987430 * L_1 = V_0;
		if (L_1)
		{
			goto IL_000e;
		}
	}
	{
		return;
	}

IL_000e:
	{
		__this->set_infoCopy_7((SerializationInfo_t228987430 *)NULL);
		SerializationInfo_t228987430 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(IHashCodeProvider_t1980576455_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_2);
		Il2CppObject * L_4 = SerializationInfo_GetValue_m1127314592(L_2, _stringLiteral815656099, L_3, /*hidden argument*/NULL);
		__this->set_m_hashprovider_3(((Il2CppObject *)Castclass(L_4, IHashCodeProvider_t1980576455_il2cpp_TypeInfo_var)));
		Il2CppObject * L_5 = __this->get_m_hashprovider_3();
		if (L_5)
		{
			goto IL_0065;
		}
	}
	{
		SerializationInfo_t228987430 * L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_7 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(IEqualityComparer_t2716208158_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_6);
		Il2CppObject * L_8 = SerializationInfo_GetValue_m1127314592(L_6, _stringLiteral1017838394, L_7, /*hidden argument*/NULL);
		__this->set_equality_comparer_9(((Il2CppObject *)Castclass(L_8, IEqualityComparer_t2716208158_il2cpp_TypeInfo_var)));
		goto IL_009b;
	}

IL_0065:
	{
		SerializationInfo_t228987430 * L_9 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_10 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(IComparer_t3952557350_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_9);
		Il2CppObject * L_11 = SerializationInfo_GetValue_m1127314592(L_9, _stringLiteral3743961169, L_10, /*hidden argument*/NULL);
		__this->set_m_comparer_4(((Il2CppObject *)Castclass(L_11, IComparer_t3952557350_il2cpp_TypeInfo_var)));
		Il2CppObject * L_12 = __this->get_m_comparer_4();
		if (L_12)
		{
			goto IL_009b;
		}
	}
	{
		SerializationException_t753258759 * L_13 = (SerializationException_t753258759 *)il2cpp_codegen_object_new(SerializationException_t753258759_il2cpp_TypeInfo_var);
		SerializationException__ctor_m1019897788(L_13, _stringLiteral3639910873, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_13);
	}

IL_009b:
	{
		SerializationInfo_t228987430 * L_14 = V_0;
		NullCheck(L_14);
		bool L_15 = SerializationInfo_GetBoolean_m3573708305(L_14, _stringLiteral1225783828, /*hidden argument*/NULL);
		__this->set_m_readonly_6(L_15);
		SerializationInfo_t228987430 * L_16 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_17 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(StringU5BU5D_t1642385972_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_16);
		Il2CppObject * L_18 = SerializationInfo_GetValue_m1127314592(L_16, _stringLiteral1857678454, L_17, /*hidden argument*/NULL);
		V_1 = ((StringU5BU5D_t1642385972*)Castclass(L_18, StringU5BU5D_t1642385972_il2cpp_TypeInfo_var));
		StringU5BU5D_t1642385972* L_19 = V_1;
		if (L_19)
		{
			goto IL_00d8;
		}
	}
	{
		SerializationException_t753258759 * L_20 = (SerializationException_t753258759 *)il2cpp_codegen_object_new(SerializationException_t753258759_il2cpp_TypeInfo_var);
		SerializationException__ctor_m1019897788(L_20, _stringLiteral2897760885, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_20);
	}

IL_00d8:
	{
		SerializationInfo_t228987430 * L_21 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_22 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(ObjectU5BU5D_t3614634134_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_21);
		Il2CppObject * L_23 = SerializationInfo_GetValue_m1127314592(L_21, _stringLiteral1188955108, L_22, /*hidden argument*/NULL);
		V_2 = ((ObjectU5BU5D_t3614634134*)Castclass(L_23, ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var));
		ObjectU5BU5D_t3614634134* L_24 = V_2;
		if (L_24)
		{
			goto IL_0104;
		}
	}
	{
		SerializationException_t753258759 * L_25 = (SerializationException_t753258759 *)il2cpp_codegen_object_new(SerializationException_t753258759_il2cpp_TypeInfo_var);
		SerializationException__ctor_m1019897788(L_25, _stringLiteral1826620485, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_25);
	}

IL_0104:
	{
		NameObjectCollectionBase_Init_m1271766820(__this, /*hidden argument*/NULL);
		StringU5BU5D_t1642385972* L_26 = V_1;
		NullCheck(L_26);
		V_3 = (((int32_t)((int32_t)(((Il2CppArray *)L_26)->max_length))));
		V_4 = 0;
		goto IL_012a;
	}

IL_0116:
	{
		StringU5BU5D_t1642385972* L_27 = V_1;
		int32_t L_28 = V_4;
		NullCheck(L_27);
		int32_t L_29 = L_28;
		String_t* L_30 = (L_27)->GetAt(static_cast<il2cpp_array_size_t>(L_29));
		ObjectU5BU5D_t3614634134* L_31 = V_2;
		int32_t L_32 = V_4;
		NullCheck(L_31);
		int32_t L_33 = L_32;
		Il2CppObject * L_34 = (L_31)->GetAt(static_cast<il2cpp_array_size_t>(L_33));
		NameObjectCollectionBase_BaseAdd_m964300824(__this, L_30, L_34, /*hidden argument*/NULL);
		int32_t L_35 = V_4;
		V_4 = ((int32_t)((int32_t)L_35+(int32_t)1));
	}

IL_012a:
	{
		int32_t L_36 = V_4;
		int32_t L_37 = V_3;
		if ((((int32_t)L_36) < ((int32_t)L_37)))
		{
			goto IL_0116;
		}
	}
	{
		return;
	}
}
// System.Boolean System.Collections.Specialized.NameObjectCollectionBase::get_IsReadOnly()
extern "C"  bool NameObjectCollectionBase_get_IsReadOnly_m3287350911 (NameObjectCollectionBase_t2034248631 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_m_readonly_6();
		return L_0;
	}
}
// System.Void System.Collections.Specialized.NameObjectCollectionBase::BaseAdd(System.String,System.Object)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppClass* _Item_t3244489099_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1461460571;
extern const uint32_t NameObjectCollectionBase_BaseAdd_m964300824_MetadataUsageId;
extern "C"  void NameObjectCollectionBase_BaseAdd_m964300824 (NameObjectCollectionBase_t2034248631 * __this, String_t* ___name0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NameObjectCollectionBase_BaseAdd_m964300824_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	_Item_t3244489099 * V_0 = NULL;
	{
		bool L_0 = NameObjectCollectionBase_get_IsReadOnly_m3287350911(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		NotSupportedException_t1793819818 * L_1 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_1, _stringLiteral1461460571, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		String_t* L_2 = ___name0;
		Il2CppObject * L_3 = ___value1;
		_Item_t3244489099 * L_4 = (_Item_t3244489099 *)il2cpp_codegen_object_new(_Item_t3244489099_il2cpp_TypeInfo_var);
		_Item__ctor_m1496904011(L_4, L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		String_t* L_5 = ___name0;
		if (L_5)
		{
			goto IL_003b;
		}
	}
	{
		_Item_t3244489099 * L_6 = __this->get_m_NullKeyItem_1();
		if (L_6)
		{
			goto IL_0036;
		}
	}
	{
		_Item_t3244489099 * L_7 = V_0;
		__this->set_m_NullKeyItem_1(L_7);
	}

IL_0036:
	{
		goto IL_0059;
	}

IL_003b:
	{
		Hashtable_t909839986 * L_8 = __this->get_m_ItemsContainer_0();
		String_t* L_9 = ___name0;
		NullCheck(L_8);
		Il2CppObject * L_10 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(26 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_8, L_9);
		if (L_10)
		{
			goto IL_0059;
		}
	}
	{
		Hashtable_t909839986 * L_11 = __this->get_m_ItemsContainer_0();
		String_t* L_12 = ___name0;
		_Item_t3244489099 * L_13 = V_0;
		NullCheck(L_11);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(29 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_11, L_12, L_13);
	}

IL_0059:
	{
		ArrayList_t4252133567 * L_14 = __this->get_m_ItemsArray_2();
		_Item_t3244489099 * L_15 = V_0;
		NullCheck(L_14);
		VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_14, L_15);
		return;
	}
}
// System.Object System.Collections.Specialized.NameObjectCollectionBase::BaseGet(System.Int32)
extern Il2CppClass* _Item_t3244489099_il2cpp_TypeInfo_var;
extern const uint32_t NameObjectCollectionBase_BaseGet_m667273025_MetadataUsageId;
extern "C"  Il2CppObject * NameObjectCollectionBase_BaseGet_m667273025 (NameObjectCollectionBase_t2034248631 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NameObjectCollectionBase_BaseGet_m667273025_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ArrayList_t4252133567 * L_0 = __this->get_m_ItemsArray_2();
		int32_t L_1 = ___index0;
		NullCheck(L_0);
		Il2CppObject * L_2 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_0, L_1);
		NullCheck(((_Item_t3244489099 *)CastclassClass(L_2, _Item_t3244489099_il2cpp_TypeInfo_var)));
		Il2CppObject * L_3 = ((_Item_t3244489099 *)CastclassClass(L_2, _Item_t3244489099_il2cpp_TypeInfo_var))->get_value_1();
		return L_3;
	}
}
// System.Object System.Collections.Specialized.NameObjectCollectionBase::BaseGet(System.String)
extern "C"  Il2CppObject * NameObjectCollectionBase_BaseGet_m3772026934 (NameObjectCollectionBase_t2034248631 * __this, String_t* ___name0, const MethodInfo* method)
{
	_Item_t3244489099 * V_0 = NULL;
	{
		String_t* L_0 = ___name0;
		_Item_t3244489099 * L_1 = NameObjectCollectionBase_FindFirstMatchedItem_m2460648656(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		_Item_t3244489099 * L_2 = V_0;
		if (L_2)
		{
			goto IL_0010;
		}
	}
	{
		return NULL;
	}

IL_0010:
	{
		_Item_t3244489099 * L_3 = V_0;
		NullCheck(L_3);
		Il2CppObject * L_4 = L_3->get_value_1();
		return L_4;
	}
}
// System.String System.Collections.Specialized.NameObjectCollectionBase::BaseGetKey(System.Int32)
extern Il2CppClass* _Item_t3244489099_il2cpp_TypeInfo_var;
extern const uint32_t NameObjectCollectionBase_BaseGetKey_m1766059806_MetadataUsageId;
extern "C"  String_t* NameObjectCollectionBase_BaseGetKey_m1766059806 (NameObjectCollectionBase_t2034248631 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NameObjectCollectionBase_BaseGetKey_m1766059806_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ArrayList_t4252133567 * L_0 = __this->get_m_ItemsArray_2();
		int32_t L_1 = ___index0;
		NullCheck(L_0);
		Il2CppObject * L_2 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_0, L_1);
		NullCheck(((_Item_t3244489099 *)CastclassClass(L_2, _Item_t3244489099_il2cpp_TypeInfo_var)));
		String_t* L_3 = ((_Item_t3244489099 *)CastclassClass(L_2, _Item_t3244489099_il2cpp_TypeInfo_var))->get_key_0();
		return L_3;
	}
}
// System.Void System.Collections.Specialized.NameObjectCollectionBase::BaseRemove(System.String)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1461460571;
extern const uint32_t NameObjectCollectionBase_BaseRemove_m3247708763_MetadataUsageId;
extern "C"  void NameObjectCollectionBase_BaseRemove_m3247708763 (NameObjectCollectionBase_t2034248631 * __this, String_t* ___name0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NameObjectCollectionBase_BaseRemove_m3247708763_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	String_t* V_1 = NULL;
	int32_t V_2 = 0;
	{
		V_0 = 0;
		bool L_0 = NameObjectCollectionBase_get_IsReadOnly_m3287350911(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		NotSupportedException_t1793819818 * L_1 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_1, _stringLiteral1461460571, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		String_t* L_2 = ___name0;
		if (!L_2)
		{
			goto IL_002f;
		}
	}
	{
		Hashtable_t909839986 * L_3 = __this->get_m_ItemsContainer_0();
		String_t* L_4 = ___name0;
		NullCheck(L_3);
		VirtActionInvoker1< Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Remove(System.Object) */, L_3, L_4);
		goto IL_0036;
	}

IL_002f:
	{
		__this->set_m_NullKeyItem_1((_Item_t3244489099 *)NULL);
	}

IL_0036:
	{
		ArrayList_t4252133567 * L_5 = __this->get_m_ItemsArray_2();
		NullCheck(L_5);
		int32_t L_6 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_5);
		V_0 = L_6;
		V_2 = 0;
		goto IL_0077;
	}

IL_0049:
	{
		int32_t L_7 = V_2;
		String_t* L_8 = NameObjectCollectionBase_BaseGetKey_m1766059806(__this, L_7, /*hidden argument*/NULL);
		V_1 = L_8;
		String_t* L_9 = V_1;
		String_t* L_10 = ___name0;
		bool L_11 = NameObjectCollectionBase_Equals_m1130527645(__this, L_9, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0073;
		}
	}
	{
		ArrayList_t4252133567 * L_12 = __this->get_m_ItemsArray_2();
		int32_t L_13 = V_2;
		NullCheck(L_12);
		VirtActionInvoker1< int32_t >::Invoke(39 /* System.Void System.Collections.ArrayList::RemoveAt(System.Int32) */, L_12, L_13);
		int32_t L_14 = V_0;
		V_0 = ((int32_t)((int32_t)L_14-(int32_t)1));
		goto IL_0077;
	}

IL_0073:
	{
		int32_t L_15 = V_2;
		V_2 = ((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_0077:
	{
		int32_t L_16 = V_2;
		int32_t L_17 = V_0;
		if ((((int32_t)L_16) < ((int32_t)L_17)))
		{
			goto IL_0049;
		}
	}
	{
		return;
	}
}
// System.Void System.Collections.Specialized.NameObjectCollectionBase::BaseSet(System.String,System.Object)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1461460571;
extern const uint32_t NameObjectCollectionBase_BaseSet_m1294211025_MetadataUsageId;
extern "C"  void NameObjectCollectionBase_BaseSet_m1294211025 (NameObjectCollectionBase_t2034248631 * __this, String_t* ___name0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NameObjectCollectionBase_BaseSet_m1294211025_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	_Item_t3244489099 * V_0 = NULL;
	{
		bool L_0 = NameObjectCollectionBase_get_IsReadOnly_m3287350911(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		NotSupportedException_t1793819818 * L_1 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_1, _stringLiteral1461460571, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		String_t* L_2 = ___name0;
		_Item_t3244489099 * L_3 = NameObjectCollectionBase_FindFirstMatchedItem_m2460648656(__this, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		_Item_t3244489099 * L_4 = V_0;
		if (!L_4)
		{
			goto IL_0030;
		}
	}
	{
		_Item_t3244489099 * L_5 = V_0;
		Il2CppObject * L_6 = ___value1;
		NullCheck(L_5);
		L_5->set_value_1(L_6);
		goto IL_0038;
	}

IL_0030:
	{
		String_t* L_7 = ___name0;
		Il2CppObject * L_8 = ___value1;
		NameObjectCollectionBase_BaseAdd_m964300824(__this, L_7, L_8, /*hidden argument*/NULL);
	}

IL_0038:
	{
		return;
	}
}
// System.Collections.Specialized.NameObjectCollectionBase/_Item System.Collections.Specialized.NameObjectCollectionBase::FindFirstMatchedItem(System.String)
extern Il2CppClass* _Item_t3244489099_il2cpp_TypeInfo_var;
extern const uint32_t NameObjectCollectionBase_FindFirstMatchedItem_m2460648656_MetadataUsageId;
extern "C"  _Item_t3244489099 * NameObjectCollectionBase_FindFirstMatchedItem_m2460648656 (NameObjectCollectionBase_t2034248631 * __this, String_t* ___name0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NameObjectCollectionBase_FindFirstMatchedItem_m2460648656_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___name0;
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		Hashtable_t909839986 * L_1 = __this->get_m_ItemsContainer_0();
		String_t* L_2 = ___name0;
		NullCheck(L_1);
		Il2CppObject * L_3 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(26 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_1, L_2);
		return ((_Item_t3244489099 *)CastclassClass(L_3, _Item_t3244489099_il2cpp_TypeInfo_var));
	}

IL_0018:
	{
		_Item_t3244489099 * L_4 = __this->get_m_NullKeyItem_1();
		return L_4;
	}
}
// System.Boolean System.Collections.Specialized.NameObjectCollectionBase::Equals(System.String,System.String)
extern Il2CppClass* IComparer_t3952557350_il2cpp_TypeInfo_var;
extern Il2CppClass* IEqualityComparer_t2716208158_il2cpp_TypeInfo_var;
extern const uint32_t NameObjectCollectionBase_Equals_m1130527645_MetadataUsageId;
extern "C"  bool NameObjectCollectionBase_Equals_m1130527645 (NameObjectCollectionBase_t2034248631 * __this, String_t* ___s10, String_t* ___s21, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NameObjectCollectionBase_Equals_m1130527645_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get_m_comparer_4();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		Il2CppObject * L_1 = __this->get_m_comparer_4();
		String_t* L_2 = ___s10;
		String_t* L_3 = ___s21;
		NullCheck(L_1);
		int32_t L_4 = InterfaceFuncInvoker2< int32_t, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Int32 System.Collections.IComparer::Compare(System.Object,System.Object) */, IComparer_t3952557350_il2cpp_TypeInfo_var, L_1, L_2, L_3);
		return (bool)((((int32_t)L_4) == ((int32_t)0))? 1 : 0);
	}

IL_001c:
	{
		Il2CppObject * L_5 = __this->get_equality_comparer_9();
		String_t* L_6 = ___s10;
		String_t* L_7 = ___s21;
		NullCheck(L_5);
		bool L_8 = InterfaceFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Boolean System.Collections.IEqualityComparer::Equals(System.Object,System.Object) */, IEqualityComparer_t2716208158_il2cpp_TypeInfo_var, L_5, L_6, L_7);
		return L_8;
	}
}
// System.Void System.Collections.Specialized.NameObjectCollectionBase/_Item::.ctor(System.String,System.Object)
extern "C"  void _Item__ctor_m1496904011 (_Item_t3244489099 * __this, String_t* ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___key0;
		__this->set_key_0(L_0);
		Il2CppObject * L_1 = ___value1;
		__this->set_value_1(L_1);
		return;
	}
}
// System.Void System.Collections.Specialized.NameObjectCollectionBase/_KeysEnumerator::.ctor(System.Collections.Specialized.NameObjectCollectionBase)
extern "C"  void _KeysEnumerator__ctor_m995107394 (_KeysEnumerator_t1718269396 * __this, NameObjectCollectionBase_t2034248631 * ___collection0, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		NameObjectCollectionBase_t2034248631 * L_0 = ___collection0;
		__this->set_m_collection_0(L_0);
		_KeysEnumerator_Reset_m3709233259(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object System.Collections.Specialized.NameObjectCollectionBase/_KeysEnumerator::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern const uint32_t _KeysEnumerator_get_Current_m2426335091_MetadataUsageId;
extern "C"  Il2CppObject * _KeysEnumerator_get_Current_m2426335091 (_KeysEnumerator_t1718269396 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_KeysEnumerator_get_Current_m2426335091_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_m_position_1();
		NameObjectCollectionBase_t2034248631 * L_1 = __this->get_m_collection_0();
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(14 /* System.Int32 System.Collections.Specialized.NameObjectCollectionBase::get_Count() */, L_1);
		if ((((int32_t)L_0) < ((int32_t)L_2)))
		{
			goto IL_0022;
		}
	}
	{
		int32_t L_3 = __this->get_m_position_1();
		if ((((int32_t)L_3) >= ((int32_t)0)))
		{
			goto IL_0034;
		}
	}

IL_0022:
	{
		NameObjectCollectionBase_t2034248631 * L_4 = __this->get_m_collection_0();
		int32_t L_5 = __this->get_m_position_1();
		NullCheck(L_4);
		String_t* L_6 = NameObjectCollectionBase_BaseGetKey_m1766059806(L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}

IL_0034:
	{
		InvalidOperationException_t721527559 * L_7 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_7, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}
}
// System.Boolean System.Collections.Specialized.NameObjectCollectionBase/_KeysEnumerator::MoveNext()
extern "C"  bool _KeysEnumerator_MoveNext_m2119083934 (_KeysEnumerator_t1718269396 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_position_1();
		int32_t L_1 = ((int32_t)((int32_t)L_0+(int32_t)1));
		V_0 = L_1;
		__this->set_m_position_1(L_1);
		int32_t L_2 = V_0;
		NameObjectCollectionBase_t2034248631 * L_3 = __this->get_m_collection_0();
		NullCheck(L_3);
		int32_t L_4 = VirtFuncInvoker0< int32_t >::Invoke(14 /* System.Int32 System.Collections.Specialized.NameObjectCollectionBase::get_Count() */, L_3);
		return (bool)((((int32_t)L_2) < ((int32_t)L_4))? 1 : 0);
	}
}
// System.Void System.Collections.Specialized.NameObjectCollectionBase/_KeysEnumerator::Reset()
extern "C"  void _KeysEnumerator_Reset_m3709233259 (_KeysEnumerator_t1718269396 * __this, const MethodInfo* method)
{
	{
		__this->set_m_position_1((-1));
		return;
	}
}
// System.Void System.Collections.Specialized.NameObjectCollectionBase/KeysCollection::.ctor(System.Collections.Specialized.NameObjectCollectionBase)
extern "C"  void KeysCollection__ctor_m847580183 (KeysCollection_t633582367 * __this, NameObjectCollectionBase_t2034248631 * ___collection0, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		NameObjectCollectionBase_t2034248631 * L_0 = ___collection0;
		__this->set_m_collection_0(L_0);
		return;
	}
}
// System.Void System.Collections.Specialized.NameObjectCollectionBase/KeysCollection::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* _Item_t3244489099_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1185213181;
extern Il2CppCodeGenString* _stringLiteral3015127219;
extern Il2CppCodeGenString* _stringLiteral3834829273;
extern Il2CppCodeGenString* _stringLiteral3056724556;
extern Il2CppCodeGenString* _stringLiteral2316951895;
extern const uint32_t KeysCollection_System_Collections_ICollection_CopyTo_m1079819835_MetadataUsageId;
extern "C"  void KeysCollection_System_Collections_ICollection_CopyTo_m1079819835 (KeysCollection_t633582367 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeysCollection_System_Collections_ICollection_CopyTo_m1079819835_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ArrayList_t4252133567 * V_0 = NULL;
	ObjectU5BU5D_t3614634134* V_1 = NULL;
	int32_t V_2 = 0;
	{
		NameObjectCollectionBase_t2034248631 * L_0 = __this->get_m_collection_0();
		NullCheck(L_0);
		ArrayList_t4252133567 * L_1 = L_0->get_m_ItemsArray_2();
		V_0 = L_1;
		Il2CppArray * L_2 = ___array0;
		if (L_2)
		{
			goto IL_001d;
		}
	}
	{
		ArgumentNullException_t628810857 * L_3 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_3, _stringLiteral1185213181, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001d:
	{
		int32_t L_4 = ___arrayIndex1;
		if ((((int32_t)L_4) >= ((int32_t)0)))
		{
			goto IL_002f;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_5 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_5, _stringLiteral3015127219, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_002f:
	{
		Il2CppArray * L_6 = ___array0;
		NullCheck(L_6);
		int32_t L_7 = Array_get_Length_m1498215565(L_6, /*hidden argument*/NULL);
		if ((((int32_t)L_7) <= ((int32_t)0)))
		{
			goto IL_0052;
		}
	}
	{
		int32_t L_8 = ___arrayIndex1;
		Il2CppArray * L_9 = ___array0;
		NullCheck(L_9);
		int32_t L_10 = Array_get_Length_m1498215565(L_9, /*hidden argument*/NULL);
		if ((((int32_t)L_8) < ((int32_t)L_10)))
		{
			goto IL_0052;
		}
	}
	{
		ArgumentException_t3259014390 * L_11 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_11, _stringLiteral3834829273, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_11);
	}

IL_0052:
	{
		int32_t L_12 = ___arrayIndex1;
		ArrayList_t4252133567 * L_13 = V_0;
		NullCheck(L_13);
		int32_t L_14 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_13);
		Il2CppArray * L_15 = ___array0;
		NullCheck(L_15);
		int32_t L_16 = Array_get_Length_m1498215565(L_15, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)L_12+(int32_t)L_14))) <= ((int32_t)L_16)))
		{
			goto IL_0070;
		}
	}
	{
		ArgumentException_t3259014390 * L_17 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_17, _stringLiteral3056724556, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_17);
	}

IL_0070:
	{
		Il2CppArray * L_18 = ___array0;
		if (!L_18)
		{
			goto IL_008d;
		}
	}
	{
		Il2CppArray * L_19 = ___array0;
		NullCheck(L_19);
		int32_t L_20 = Array_get_Rank_m3837250695(L_19, /*hidden argument*/NULL);
		if ((((int32_t)L_20) <= ((int32_t)1)))
		{
			goto IL_008d;
		}
	}
	{
		ArgumentException_t3259014390 * L_21 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_21, _stringLiteral2316951895, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}

IL_008d:
	{
		Il2CppArray * L_22 = ___array0;
		V_1 = ((ObjectU5BU5D_t3614634134*)Castclass(L_22, ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var));
		V_2 = 0;
		goto IL_00b8;
	}

IL_009b:
	{
		ObjectU5BU5D_t3614634134* L_23 = V_1;
		int32_t L_24 = ___arrayIndex1;
		ArrayList_t4252133567 * L_25 = V_0;
		int32_t L_26 = V_2;
		NullCheck(L_25);
		Il2CppObject * L_27 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_25, L_26);
		NullCheck(((_Item_t3244489099 *)CastclassClass(L_27, _Item_t3244489099_il2cpp_TypeInfo_var)));
		String_t* L_28 = ((_Item_t3244489099 *)CastclassClass(L_27, _Item_t3244489099_il2cpp_TypeInfo_var))->get_key_0();
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, L_28);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(L_24), (Il2CppObject *)L_28);
		int32_t L_29 = V_2;
		V_2 = ((int32_t)((int32_t)L_29+(int32_t)1));
		int32_t L_30 = ___arrayIndex1;
		___arrayIndex1 = ((int32_t)((int32_t)L_30+(int32_t)1));
	}

IL_00b8:
	{
		int32_t L_31 = V_2;
		ArrayList_t4252133567 * L_32 = V_0;
		NullCheck(L_32);
		int32_t L_33 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_32);
		if ((((int32_t)L_31) < ((int32_t)L_33)))
		{
			goto IL_009b;
		}
	}
	{
		return;
	}
}
// System.Boolean System.Collections.Specialized.NameObjectCollectionBase/KeysCollection::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeysCollection_System_Collections_ICollection_get_IsSynchronized_m2298576971 (KeysCollection_t633582367 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Specialized.NameObjectCollectionBase/KeysCollection::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeysCollection_System_Collections_ICollection_get_SyncRoot_m2289369147 (KeysCollection_t633582367 * __this, const MethodInfo* method)
{
	{
		NameObjectCollectionBase_t2034248631 * L_0 = __this->get_m_collection_0();
		return L_0;
	}
}
// System.Int32 System.Collections.Specialized.NameObjectCollectionBase/KeysCollection::get_Count()
extern "C"  int32_t KeysCollection_get_Count_m3650400467 (KeysCollection_t633582367 * __this, const MethodInfo* method)
{
	{
		NameObjectCollectionBase_t2034248631 * L_0 = __this->get_m_collection_0();
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(14 /* System.Int32 System.Collections.Specialized.NameObjectCollectionBase::get_Count() */, L_0);
		return L_1;
	}
}
// System.Collections.IEnumerator System.Collections.Specialized.NameObjectCollectionBase/KeysCollection::GetEnumerator()
extern Il2CppClass* _KeysEnumerator_t1718269396_il2cpp_TypeInfo_var;
extern const uint32_t KeysCollection_GetEnumerator_m3831125359_MetadataUsageId;
extern "C"  Il2CppObject * KeysCollection_GetEnumerator_m3831125359 (KeysCollection_t633582367 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeysCollection_GetEnumerator_m3831125359_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NameObjectCollectionBase_t2034248631 * L_0 = __this->get_m_collection_0();
		_KeysEnumerator_t1718269396 * L_1 = (_KeysEnumerator_t1718269396 *)il2cpp_codegen_object_new(_KeysEnumerator_t1718269396_il2cpp_TypeInfo_var);
		_KeysEnumerator__ctor_m995107394(L_1, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void System.Collections.Specialized.NameValueCollection::.ctor()
extern "C"  void NameValueCollection__ctor_m1767369537 (NameValueCollection_t3047564564 * __this, const MethodInfo* method)
{
	{
		NameObjectCollectionBase__ctor_m2433753948(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Specialized.NameValueCollection::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void NameValueCollection__ctor_m3660918416 (NameValueCollection_t3047564564 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method)
{
	{
		SerializationInfo_t228987430 * L_0 = ___info0;
		StreamingContext_t1417235061  L_1 = ___context1;
		NameObjectCollectionBase__ctor_m241668161(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String System.Collections.Specialized.NameValueCollection::get_Item(System.String)
extern "C"  String_t* NameValueCollection_get_Item_m2776418562 (NameValueCollection_t3047564564 * __this, String_t* ___name0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name0;
		String_t* L_1 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(18 /* System.String System.Collections.Specialized.NameValueCollection::Get(System.String) */, __this, L_0);
		return L_1;
	}
}
// System.Void System.Collections.Specialized.NameValueCollection::set_Item(System.String,System.String)
extern "C"  void NameValueCollection_set_Item_m3775607929 (NameValueCollection_t3047564564 * __this, String_t* ___name0, String_t* ___value1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name0;
		String_t* L_1 = ___value1;
		VirtActionInvoker2< String_t*, String_t* >::Invoke(22 /* System.Void System.Collections.Specialized.NameValueCollection::Set(System.String,System.String) */, __this, L_0, L_1);
		return;
	}
}
// System.Void System.Collections.Specialized.NameValueCollection::Add(System.String,System.String)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppClass* ArrayList_t4252133567_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1461460571;
extern const uint32_t NameValueCollection_Add_m263445674_MetadataUsageId;
extern "C"  void NameValueCollection_Add_m263445674 (NameValueCollection_t3047564564 * __this, String_t* ___name0, String_t* ___val1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NameValueCollection_Add_m263445674_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ArrayList_t4252133567 * V_0 = NULL;
	{
		bool L_0 = NameObjectCollectionBase_get_IsReadOnly_m3287350911(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		NotSupportedException_t1793819818 * L_1 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_1, _stringLiteral1461460571, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		NameValueCollection_InvalidateCachedArrays_m1142700554(__this, /*hidden argument*/NULL);
		String_t* L_2 = ___name0;
		Il2CppObject * L_3 = NameObjectCollectionBase_BaseGet_m3772026934(__this, L_2, /*hidden argument*/NULL);
		V_0 = ((ArrayList_t4252133567 *)CastclassClass(L_3, ArrayList_t4252133567_il2cpp_TypeInfo_var));
		ArrayList_t4252133567 * L_4 = V_0;
		if (L_4)
		{
			goto IL_0050;
		}
	}
	{
		ArrayList_t4252133567 * L_5 = (ArrayList_t4252133567 *)il2cpp_codegen_object_new(ArrayList_t4252133567_il2cpp_TypeInfo_var);
		ArrayList__ctor_m4012174379(L_5, /*hidden argument*/NULL);
		V_0 = L_5;
		String_t* L_6 = ___val1;
		if (!L_6)
		{
			goto IL_0043;
		}
	}
	{
		ArrayList_t4252133567 * L_7 = V_0;
		String_t* L_8 = ___val1;
		NullCheck(L_7);
		VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_7, L_8);
	}

IL_0043:
	{
		String_t* L_9 = ___name0;
		ArrayList_t4252133567 * L_10 = V_0;
		NameObjectCollectionBase_BaseAdd_m964300824(__this, L_9, L_10, /*hidden argument*/NULL);
		goto IL_005e;
	}

IL_0050:
	{
		String_t* L_11 = ___val1;
		if (!L_11)
		{
			goto IL_005e;
		}
	}
	{
		ArrayList_t4252133567 * L_12 = V_0;
		String_t* L_13 = ___val1;
		NullCheck(L_12);
		VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_12, L_13);
	}

IL_005e:
	{
		return;
	}
}
// System.String System.Collections.Specialized.NameValueCollection::Get(System.Int32)
extern Il2CppClass* ArrayList_t4252133567_il2cpp_TypeInfo_var;
extern const uint32_t NameValueCollection_Get_m861415899_MetadataUsageId;
extern "C"  String_t* NameValueCollection_Get_m861415899 (NameValueCollection_t3047564564 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NameValueCollection_Get_m861415899_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ArrayList_t4252133567 * V_0 = NULL;
	{
		int32_t L_0 = ___index0;
		Il2CppObject * L_1 = NameObjectCollectionBase_BaseGet_m667273025(__this, L_0, /*hidden argument*/NULL);
		V_0 = ((ArrayList_t4252133567 *)CastclassClass(L_1, ArrayList_t4252133567_il2cpp_TypeInfo_var));
		ArrayList_t4252133567 * L_2 = V_0;
		String_t* L_3 = NameValueCollection_AsSingleString_m10599052(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.String System.Collections.Specialized.NameValueCollection::Get(System.String)
extern Il2CppClass* ArrayList_t4252133567_il2cpp_TypeInfo_var;
extern const uint32_t NameValueCollection_Get_m2509739626_MetadataUsageId;
extern "C"  String_t* NameValueCollection_Get_m2509739626 (NameValueCollection_t3047564564 * __this, String_t* ___name0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NameValueCollection_Get_m2509739626_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ArrayList_t4252133567 * V_0 = NULL;
	{
		String_t* L_0 = ___name0;
		Il2CppObject * L_1 = NameObjectCollectionBase_BaseGet_m3772026934(__this, L_0, /*hidden argument*/NULL);
		V_0 = ((ArrayList_t4252133567 *)CastclassClass(L_1, ArrayList_t4252133567_il2cpp_TypeInfo_var));
		ArrayList_t4252133567 * L_2 = V_0;
		String_t* L_3 = NameValueCollection_AsSingleString_m10599052(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.String System.Collections.Specialized.NameValueCollection::AsSingleString(System.Collections.ArrayList)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Char_t3454481338_il2cpp_TypeInfo_var;
extern Il2CppClass* StringBuilder_t1221177846_il2cpp_TypeInfo_var;
extern const uint32_t NameValueCollection_AsSingleString_m10599052_MetadataUsageId;
extern "C"  String_t* NameValueCollection_AsSingleString_m10599052 (Il2CppObject * __this /* static, unused */, ArrayList_t4252133567 * ___values0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NameValueCollection_AsSingleString_m10599052_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppChar V_0 = 0x0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	StringBuilder_t1221177846 * V_4 = NULL;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	{
		ArrayList_t4252133567 * L_0 = ___values0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return (String_t*)NULL;
	}

IL_0008:
	{
		ArrayList_t4252133567 * L_1 = ___values0;
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_1);
		V_1 = L_2;
		int32_t L_3 = V_1;
		V_6 = L_3;
		int32_t L_4 = V_6;
		if (L_4 == 0)
		{
			goto IL_002a;
		}
		if (L_4 == 1)
		{
			goto IL_002c;
		}
		if (L_4 == 2)
		{
			goto IL_0039;
		}
	}
	{
		goto IL_005e;
	}

IL_002a:
	{
		return (String_t*)NULL;
	}

IL_002c:
	{
		ArrayList_t4252133567 * L_5 = ___values0;
		NullCheck(L_5);
		Il2CppObject * L_6 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_5, 0);
		return ((String_t*)CastclassSealed(L_6, String_t_il2cpp_TypeInfo_var));
	}

IL_0039:
	{
		ArrayList_t4252133567 * L_7 = ___values0;
		NullCheck(L_7);
		Il2CppObject * L_8 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_7, 0);
		Il2CppChar L_9 = ((Il2CppChar)((int32_t)44));
		Il2CppObject * L_10 = Box(Char_t3454481338_il2cpp_TypeInfo_var, &L_9);
		ArrayList_t4252133567 * L_11 = ___values0;
		NullCheck(L_11);
		Il2CppObject * L_12 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_11, 1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m2000667605(NULL /*static, unused*/, ((String_t*)CastclassSealed(L_8, String_t_il2cpp_TypeInfo_var)), L_10, ((String_t*)CastclassSealed(L_12, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return L_13;
	}

IL_005e:
	{
		int32_t L_14 = V_1;
		V_2 = L_14;
		V_3 = 0;
		goto IL_007f;
	}

IL_0067:
	{
		int32_t L_15 = V_2;
		ArrayList_t4252133567 * L_16 = ___values0;
		int32_t L_17 = V_3;
		NullCheck(L_16);
		Il2CppObject * L_18 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_16, L_17);
		NullCheck(((String_t*)CastclassSealed(L_18, String_t_il2cpp_TypeInfo_var)));
		int32_t L_19 = String_get_Length_m1606060069(((String_t*)CastclassSealed(L_18, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		V_2 = ((int32_t)((int32_t)L_15+(int32_t)L_19));
		int32_t L_20 = V_3;
		V_3 = ((int32_t)((int32_t)L_20+(int32_t)1));
	}

IL_007f:
	{
		int32_t L_21 = V_3;
		int32_t L_22 = V_1;
		if ((((int32_t)L_21) < ((int32_t)L_22)))
		{
			goto IL_0067;
		}
	}
	{
		ArrayList_t4252133567 * L_23 = ___values0;
		NullCheck(L_23);
		Il2CppObject * L_24 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_23, 0);
		int32_t L_25 = V_2;
		StringBuilder_t1221177846 * L_26 = (StringBuilder_t1221177846 *)il2cpp_codegen_object_new(StringBuilder_t1221177846_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m1456828835(L_26, ((String_t*)CastclassSealed(L_24, String_t_il2cpp_TypeInfo_var)), L_25, /*hidden argument*/NULL);
		V_4 = L_26;
		V_5 = 1;
		goto IL_00c2;
	}

IL_00a2:
	{
		StringBuilder_t1221177846 * L_27 = V_4;
		NullCheck(L_27);
		StringBuilder_Append_m3618697540(L_27, ((int32_t)44), /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_28 = V_4;
		ArrayList_t4252133567 * L_29 = ___values0;
		int32_t L_30 = V_5;
		NullCheck(L_29);
		Il2CppObject * L_31 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_29, L_30);
		NullCheck(L_28);
		StringBuilder_Append_m3541816491(L_28, L_31, /*hidden argument*/NULL);
		int32_t L_32 = V_5;
		V_5 = ((int32_t)((int32_t)L_32+(int32_t)1));
	}

IL_00c2:
	{
		int32_t L_33 = V_5;
		int32_t L_34 = V_1;
		if ((((int32_t)L_33) < ((int32_t)L_34)))
		{
			goto IL_00a2;
		}
	}
	{
		StringBuilder_t1221177846 * L_35 = V_4;
		NullCheck(L_35);
		String_t* L_36 = StringBuilder_ToString_m1507807375(L_35, /*hidden argument*/NULL);
		return L_36;
	}
}
// System.String System.Collections.Specialized.NameValueCollection::GetKey(System.Int32)
extern "C"  String_t* NameValueCollection_GetKey_m3871624648 (NameValueCollection_t3047564564 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		String_t* L_1 = NameObjectCollectionBase_BaseGetKey_m1766059806(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String[] System.Collections.Specialized.NameValueCollection::GetValues(System.String)
extern Il2CppClass* ArrayList_t4252133567_il2cpp_TypeInfo_var;
extern const uint32_t NameValueCollection_GetValues_m1275908180_MetadataUsageId;
extern "C"  StringU5BU5D_t1642385972* NameValueCollection_GetValues_m1275908180 (NameValueCollection_t3047564564 * __this, String_t* ___name0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NameValueCollection_GetValues_m1275908180_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ArrayList_t4252133567 * V_0 = NULL;
	{
		String_t* L_0 = ___name0;
		Il2CppObject * L_1 = NameObjectCollectionBase_BaseGet_m3772026934(__this, L_0, /*hidden argument*/NULL);
		V_0 = ((ArrayList_t4252133567 *)CastclassClass(L_1, ArrayList_t4252133567_il2cpp_TypeInfo_var));
		ArrayList_t4252133567 * L_2 = V_0;
		StringU5BU5D_t1642385972* L_3 = NameValueCollection_AsStringArray_m1681029335(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.String[] System.Collections.Specialized.NameValueCollection::AsStringArray(System.Collections.ArrayList)
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern const uint32_t NameValueCollection_AsStringArray_m1681029335_MetadataUsageId;
extern "C"  StringU5BU5D_t1642385972* NameValueCollection_AsStringArray_m1681029335 (Il2CppObject * __this /* static, unused */, ArrayList_t4252133567 * ___values0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NameValueCollection_AsStringArray_m1681029335_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	StringU5BU5D_t1642385972* V_1 = NULL;
	{
		ArrayList_t4252133567 * L_0 = ___values0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return (StringU5BU5D_t1642385972*)NULL;
	}

IL_0008:
	{
		ArrayList_t4252133567 * L_1 = ___values0;
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_1);
		V_0 = L_2;
		int32_t L_3 = V_0;
		if (L_3)
		{
			goto IL_0017;
		}
	}
	{
		return (StringU5BU5D_t1642385972*)NULL;
	}

IL_0017:
	{
		int32_t L_4 = V_0;
		V_1 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)L_4));
		ArrayList_t4252133567 * L_5 = ___values0;
		StringU5BU5D_t1642385972* L_6 = V_1;
		NullCheck(L_5);
		VirtActionInvoker1< Il2CppArray * >::Invoke(40 /* System.Void System.Collections.ArrayList::CopyTo(System.Array) */, L_5, (Il2CppArray *)(Il2CppArray *)L_6);
		StringU5BU5D_t1642385972* L_7 = V_1;
		return L_7;
	}
}
// System.Void System.Collections.Specialized.NameValueCollection::Remove(System.String)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1461460571;
extern const uint32_t NameValueCollection_Remove_m629217025_MetadataUsageId;
extern "C"  void NameValueCollection_Remove_m629217025 (NameValueCollection_t3047564564 * __this, String_t* ___name0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NameValueCollection_Remove_m629217025_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = NameObjectCollectionBase_get_IsReadOnly_m3287350911(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		NotSupportedException_t1793819818 * L_1 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_1, _stringLiteral1461460571, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		NameValueCollection_InvalidateCachedArrays_m1142700554(__this, /*hidden argument*/NULL);
		String_t* L_2 = ___name0;
		NameObjectCollectionBase_BaseRemove_m3247708763(__this, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Specialized.NameValueCollection::Set(System.String,System.String)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppClass* ArrayList_t4252133567_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1461460571;
extern const uint32_t NameValueCollection_Set_m2969274643_MetadataUsageId;
extern "C"  void NameValueCollection_Set_m2969274643 (NameValueCollection_t3047564564 * __this, String_t* ___name0, String_t* ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NameValueCollection_Set_m2969274643_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ArrayList_t4252133567 * V_0 = NULL;
	{
		bool L_0 = NameObjectCollectionBase_get_IsReadOnly_m3287350911(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		NotSupportedException_t1793819818 * L_1 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_1, _stringLiteral1461460571, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		NameValueCollection_InvalidateCachedArrays_m1142700554(__this, /*hidden argument*/NULL);
		ArrayList_t4252133567 * L_2 = (ArrayList_t4252133567 *)il2cpp_codegen_object_new(ArrayList_t4252133567_il2cpp_TypeInfo_var);
		ArrayList__ctor_m4012174379(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		String_t* L_3 = ___value1;
		if (!L_3)
		{
			goto IL_003d;
		}
	}
	{
		ArrayList_t4252133567 * L_4 = V_0;
		String_t* L_5 = ___value1;
		NullCheck(L_4);
		VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_4, L_5);
		String_t* L_6 = ___name0;
		ArrayList_t4252133567 * L_7 = V_0;
		NameObjectCollectionBase_BaseSet_m1294211025(__this, L_6, L_7, /*hidden argument*/NULL);
		goto IL_0045;
	}

IL_003d:
	{
		String_t* L_8 = ___name0;
		NameObjectCollectionBase_BaseSet_m1294211025(__this, L_8, NULL, /*hidden argument*/NULL);
	}

IL_0045:
	{
		return;
	}
}
// System.Void System.Collections.Specialized.NameValueCollection::InvalidateCachedArrays()
extern "C"  void NameValueCollection_InvalidateCachedArrays_m1142700554 (NameValueCollection_t3047564564 * __this, const MethodInfo* method)
{
	{
		__this->set_cachedAllKeys_10((StringU5BU5D_t1642385972*)NULL);
		__this->set_cachedAll_11((StringU5BU5D_t1642385972*)NULL);
		return;
	}
}
// System.Void System.Collections.Specialized.StringCollection::.ctor()
extern Il2CppClass* ArrayList_t4252133567_il2cpp_TypeInfo_var;
extern const uint32_t StringCollection__ctor_m467153844_MetadataUsageId;
extern "C"  void StringCollection__ctor_m467153844 (StringCollection_t352985975 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StringCollection__ctor_m467153844_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ArrayList_t4252133567 * L_0 = (ArrayList_t4252133567 *)il2cpp_codegen_object_new(ArrayList_t4252133567_il2cpp_TypeInfo_var);
		ArrayList__ctor_m4012174379(L_0, /*hidden argument*/NULL);
		__this->set_data_0(L_0);
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean System.Collections.Specialized.StringCollection::System.Collections.IList.get_IsReadOnly()
extern "C"  bool StringCollection_System_Collections_IList_get_IsReadOnly_m3716594668 (StringCollection_t352985975 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean System.Collections.Specialized.StringCollection::System.Collections.IList.get_IsFixedSize()
extern "C"  bool StringCollection_System_Collections_IList_get_IsFixedSize_m1043931093 (StringCollection_t352985975 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Specialized.StringCollection::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * StringCollection_System_Collections_IList_get_Item_m2290212549 (StringCollection_t352985975 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		String_t* L_1 = StringCollection_get_Item_m4283262666(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void System.Collections.Specialized.StringCollection::System.Collections.IList.set_Item(System.Int32,System.Object)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t StringCollection_System_Collections_IList_set_Item_m2482717326_MetadataUsageId;
extern "C"  void StringCollection_System_Collections_IList_set_Item_m2482717326 (StringCollection_t352985975 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StringCollection_System_Collections_IList_set_Item_m2482717326_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		Il2CppObject * L_1 = ___value1;
		StringCollection_set_Item_m2522937815(__this, L_0, ((String_t*)CastclassSealed(L_1, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 System.Collections.Specialized.StringCollection::System.Collections.IList.Add(System.Object)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t StringCollection_System_Collections_IList_Add_m941275116_MetadataUsageId;
extern "C"  int32_t StringCollection_System_Collections_IList_Add_m941275116 (StringCollection_t352985975 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StringCollection_System_Collections_IList_Add_m941275116_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___value0;
		int32_t L_1 = StringCollection_Add_m2506333889(__this, ((String_t*)CastclassSealed(L_0, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Collections.Specialized.StringCollection::System.Collections.IList.Contains(System.Object)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t StringCollection_System_Collections_IList_Contains_m1706640982_MetadataUsageId;
extern "C"  bool StringCollection_System_Collections_IList_Contains_m1706640982 (StringCollection_t352985975 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StringCollection_System_Collections_IList_Contains_m1706640982_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___value0;
		bool L_1 = StringCollection_Contains_m870435597(__this, ((String_t*)CastclassSealed(L_0, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 System.Collections.Specialized.StringCollection::System.Collections.IList.IndexOf(System.Object)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t StringCollection_System_Collections_IList_IndexOf_m13314746_MetadataUsageId;
extern "C"  int32_t StringCollection_System_Collections_IList_IndexOf_m13314746 (StringCollection_t352985975 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StringCollection_System_Collections_IList_IndexOf_m13314746_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___value0;
		int32_t L_1 = StringCollection_IndexOf_m1014856711(__this, ((String_t*)CastclassSealed(L_0, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void System.Collections.Specialized.StringCollection::System.Collections.IList.Insert(System.Int32,System.Object)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t StringCollection_System_Collections_IList_Insert_m1915566445_MetadataUsageId;
extern "C"  void StringCollection_System_Collections_IList_Insert_m1915566445 (StringCollection_t352985975 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StringCollection_System_Collections_IList_Insert_m1915566445_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		Il2CppObject * L_1 = ___value1;
		StringCollection_Insert_m1467620830(__this, L_0, ((String_t*)CastclassSealed(L_1, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Specialized.StringCollection::System.Collections.IList.Remove(System.Object)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t StringCollection_System_Collections_IList_Remove_m3244543085_MetadataUsageId;
extern "C"  void StringCollection_System_Collections_IList_Remove_m3244543085 (StringCollection_t352985975 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StringCollection_System_Collections_IList_Remove_m3244543085_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___value0;
		StringCollection_Remove_m4131969964(__this, ((String_t*)CastclassSealed(L_0, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Specialized.StringCollection::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void StringCollection_System_Collections_ICollection_CopyTo_m2882084540 (StringCollection_t352985975 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		ArrayList_t4252133567 * L_0 = __this->get_data_0();
		Il2CppArray * L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck(L_0);
		VirtActionInvoker2< Il2CppArray *, int32_t >::Invoke(41 /* System.Void System.Collections.ArrayList::CopyTo(System.Array,System.Int32) */, L_0, L_1, L_2);
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Specialized.StringCollection::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * StringCollection_System_Collections_IEnumerable_GetEnumerator_m2032583317 (StringCollection_t352985975 * __this, const MethodInfo* method)
{
	{
		ArrayList_t4252133567 * L_0 = __this->get_data_0();
		NullCheck(L_0);
		Il2CppObject * L_1 = VirtFuncInvoker0< Il2CppObject * >::Invoke(43 /* System.Collections.IEnumerator System.Collections.ArrayList::GetEnumerator() */, L_0);
		return L_1;
	}
}
// System.String System.Collections.Specialized.StringCollection::get_Item(System.Int32)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t StringCollection_get_Item_m4283262666_MetadataUsageId;
extern "C"  String_t* StringCollection_get_Item_m4283262666 (StringCollection_t352985975 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StringCollection_get_Item_m4283262666_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ArrayList_t4252133567 * L_0 = __this->get_data_0();
		int32_t L_1 = ___index0;
		NullCheck(L_0);
		Il2CppObject * L_2 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_0, L_1);
		return ((String_t*)CastclassSealed(L_2, String_t_il2cpp_TypeInfo_var));
	}
}
// System.Void System.Collections.Specialized.StringCollection::set_Item(System.Int32,System.String)
extern "C"  void StringCollection_set_Item_m2522937815 (StringCollection_t352985975 * __this, int32_t ___index0, String_t* ___value1, const MethodInfo* method)
{
	{
		ArrayList_t4252133567 * L_0 = __this->get_data_0();
		int32_t L_1 = ___index0;
		String_t* L_2 = ___value1;
		NullCheck(L_0);
		VirtActionInvoker2< int32_t, Il2CppObject * >::Invoke(22 /* System.Void System.Collections.ArrayList::set_Item(System.Int32,System.Object) */, L_0, L_1, L_2);
		return;
	}
}
// System.Int32 System.Collections.Specialized.StringCollection::get_Count()
extern "C"  int32_t StringCollection_get_Count_m1458092700 (StringCollection_t352985975 * __this, const MethodInfo* method)
{
	{
		ArrayList_t4252133567 * L_0 = __this->get_data_0();
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_0);
		return L_1;
	}
}
// System.Int32 System.Collections.Specialized.StringCollection::Add(System.String)
extern "C"  int32_t StringCollection_Add_m2506333889 (StringCollection_t352985975 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		ArrayList_t4252133567 * L_0 = __this->get_data_0();
		String_t* L_1 = ___value0;
		NullCheck(L_0);
		int32_t L_2 = VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_0, L_1);
		return L_2;
	}
}
// System.Void System.Collections.Specialized.StringCollection::Clear()
extern "C"  void StringCollection_Clear_m2899840293 (StringCollection_t352985975 * __this, const MethodInfo* method)
{
	{
		ArrayList_t4252133567 * L_0 = __this->get_data_0();
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(31 /* System.Void System.Collections.ArrayList::Clear() */, L_0);
		return;
	}
}
// System.Boolean System.Collections.Specialized.StringCollection::Contains(System.String)
extern "C"  bool StringCollection_Contains_m870435597 (StringCollection_t352985975 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		ArrayList_t4252133567 * L_0 = __this->get_data_0();
		String_t* L_1 = ___value0;
		NullCheck(L_0);
		bool L_2 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(32 /* System.Boolean System.Collections.ArrayList::Contains(System.Object) */, L_0, L_1);
		return L_2;
	}
}
// System.Collections.Specialized.StringEnumerator System.Collections.Specialized.StringCollection::GetEnumerator()
extern Il2CppClass* StringEnumerator_t441637433_il2cpp_TypeInfo_var;
extern const uint32_t StringCollection_GetEnumerator_m3207777781_MetadataUsageId;
extern "C"  StringEnumerator_t441637433 * StringCollection_GetEnumerator_m3207777781 (StringCollection_t352985975 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StringCollection_GetEnumerator_m3207777781_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		StringEnumerator_t441637433 * L_0 = (StringEnumerator_t441637433 *)il2cpp_codegen_object_new(StringEnumerator_t441637433_il2cpp_TypeInfo_var);
		StringEnumerator__ctor_m1312483144(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Int32 System.Collections.Specialized.StringCollection::IndexOf(System.String)
extern "C"  int32_t StringCollection_IndexOf_m1014856711 (StringCollection_t352985975 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		ArrayList_t4252133567 * L_0 = __this->get_data_0();
		String_t* L_1 = ___value0;
		NullCheck(L_0);
		int32_t L_2 = VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(33 /* System.Int32 System.Collections.ArrayList::IndexOf(System.Object) */, L_0, L_1);
		return L_2;
	}
}
// System.Void System.Collections.Specialized.StringCollection::Insert(System.Int32,System.String)
extern "C"  void StringCollection_Insert_m1467620830 (StringCollection_t352985975 * __this, int32_t ___index0, String_t* ___value1, const MethodInfo* method)
{
	{
		ArrayList_t4252133567 * L_0 = __this->get_data_0();
		int32_t L_1 = ___index0;
		String_t* L_2 = ___value1;
		NullCheck(L_0);
		VirtActionInvoker2< int32_t, Il2CppObject * >::Invoke(36 /* System.Void System.Collections.ArrayList::Insert(System.Int32,System.Object) */, L_0, L_1, L_2);
		return;
	}
}
// System.Boolean System.Collections.Specialized.StringCollection::get_IsReadOnly()
extern "C"  bool StringCollection_get_IsReadOnly_m3129120249 (StringCollection_t352985975 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean System.Collections.Specialized.StringCollection::get_IsSynchronized()
extern "C"  bool StringCollection_get_IsSynchronized_m1699466861 (StringCollection_t352985975 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Void System.Collections.Specialized.StringCollection::Remove(System.String)
extern "C"  void StringCollection_Remove_m4131969964 (StringCollection_t352985975 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		ArrayList_t4252133567 * L_0 = __this->get_data_0();
		String_t* L_1 = ___value0;
		NullCheck(L_0);
		VirtActionInvoker1< Il2CppObject * >::Invoke(38 /* System.Void System.Collections.ArrayList::Remove(System.Object) */, L_0, L_1);
		return;
	}
}
// System.Void System.Collections.Specialized.StringCollection::RemoveAt(System.Int32)
extern "C"  void StringCollection_RemoveAt_m1013080178 (StringCollection_t352985975 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		ArrayList_t4252133567 * L_0 = __this->get_data_0();
		int32_t L_1 = ___index0;
		NullCheck(L_0);
		VirtActionInvoker1< int32_t >::Invoke(39 /* System.Void System.Collections.ArrayList::RemoveAt(System.Int32) */, L_0, L_1);
		return;
	}
}
// System.Object System.Collections.Specialized.StringCollection::get_SyncRoot()
extern "C"  Il2CppObject * StringCollection_get_SyncRoot_m1322750277 (StringCollection_t352985975 * __this, const MethodInfo* method)
{
	{
		return __this;
	}
}
// System.Void System.Collections.Specialized.StringDictionary::.ctor()
extern Il2CppClass* Hashtable_t909839986_il2cpp_TypeInfo_var;
extern const uint32_t StringDictionary__ctor_m270184480_MetadataUsageId;
extern "C"  void StringDictionary__ctor_m270184480 (StringDictionary_t1070889667 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StringDictionary__ctor_m270184480_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		Hashtable_t909839986 * L_0 = (Hashtable_t909839986 *)il2cpp_codegen_object_new(Hashtable_t909839986_il2cpp_TypeInfo_var);
		Hashtable__ctor_m1884964176(L_0, /*hidden argument*/NULL);
		__this->set_contents_0(L_0);
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Specialized.StringDictionary::GetEnumerator()
extern "C"  Il2CppObject * StringDictionary_GetEnumerator_m2202077700 (StringDictionary_t1070889667 * __this, const MethodInfo* method)
{
	{
		Hashtable_t909839986 * L_0 = __this->get_contents_0();
		NullCheck(L_0);
		Il2CppObject * L_1 = VirtFuncInvoker0< Il2CppObject * >::Invoke(32 /* System.Collections.IDictionaryEnumerator System.Collections.Hashtable::GetEnumerator() */, L_0);
		return L_1;
	}
}
// System.Void System.Collections.Specialized.StringEnumerator::.ctor(System.Collections.Specialized.StringCollection)
extern Il2CppClass* IEnumerable_t2911409499_il2cpp_TypeInfo_var;
extern const uint32_t StringEnumerator__ctor_m1312483144_MetadataUsageId;
extern "C"  void StringEnumerator__ctor_m1312483144 (StringEnumerator_t441637433 * __this, StringCollection_t352985975 * ___coll0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StringEnumerator__ctor_m1312483144_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		StringCollection_t352985975 * L_0 = ___coll0;
		NullCheck(L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t2911409499_il2cpp_TypeInfo_var, L_0);
		__this->set_enumerable_0(L_1);
		return;
	}
}
// System.String System.Collections.Specialized.StringEnumerator::get_Current()
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t StringEnumerator_get_Current_m2945155571_MetadataUsageId;
extern "C"  String_t* StringEnumerator_get_Current_m2945155571 (StringEnumerator_t441637433 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StringEnumerator_get_Current_m2945155571_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get_enumerable_0();
		NullCheck(L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_0);
		return ((String_t*)CastclassSealed(L_1, String_t_il2cpp_TypeInfo_var));
	}
}
// System.Boolean System.Collections.Specialized.StringEnumerator::MoveNext()
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern const uint32_t StringEnumerator_MoveNext_m78691276_MetadataUsageId;
extern "C"  bool StringEnumerator_MoveNext_m78691276 (StringEnumerator_t441637433 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StringEnumerator_MoveNext_m78691276_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get_enumerable_0();
		NullCheck(L_0);
		bool L_1 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Void System.ComponentModel.BrowsableAttribute::.ctor(System.Boolean)
extern "C"  void BrowsableAttribute__ctor_m1194948749 (BrowsableAttribute_t2487167291 * __this, bool ___browsable0, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		bool L_0 = ___browsable0;
		__this->set_browsable_0(L_0);
		return;
	}
}
// System.Void System.ComponentModel.BrowsableAttribute::.cctor()
extern Il2CppClass* BrowsableAttribute_t2487167291_il2cpp_TypeInfo_var;
extern const uint32_t BrowsableAttribute__cctor_m3377205821_MetadataUsageId;
extern "C"  void BrowsableAttribute__cctor_m3377205821 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BrowsableAttribute__cctor_m3377205821_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		BrowsableAttribute_t2487167291 * L_0 = (BrowsableAttribute_t2487167291 *)il2cpp_codegen_object_new(BrowsableAttribute_t2487167291_il2cpp_TypeInfo_var);
		BrowsableAttribute__ctor_m1194948749(L_0, (bool)1, /*hidden argument*/NULL);
		((BrowsableAttribute_t2487167291_StaticFields*)BrowsableAttribute_t2487167291_il2cpp_TypeInfo_var->static_fields)->set_Default_1(L_0);
		BrowsableAttribute_t2487167291 * L_1 = (BrowsableAttribute_t2487167291 *)il2cpp_codegen_object_new(BrowsableAttribute_t2487167291_il2cpp_TypeInfo_var);
		BrowsableAttribute__ctor_m1194948749(L_1, (bool)0, /*hidden argument*/NULL);
		((BrowsableAttribute_t2487167291_StaticFields*)BrowsableAttribute_t2487167291_il2cpp_TypeInfo_var->static_fields)->set_No_2(L_1);
		BrowsableAttribute_t2487167291 * L_2 = (BrowsableAttribute_t2487167291 *)il2cpp_codegen_object_new(BrowsableAttribute_t2487167291_il2cpp_TypeInfo_var);
		BrowsableAttribute__ctor_m1194948749(L_2, (bool)1, /*hidden argument*/NULL);
		((BrowsableAttribute_t2487167291_StaticFields*)BrowsableAttribute_t2487167291_il2cpp_TypeInfo_var->static_fields)->set_Yes_3(L_2);
		return;
	}
}
// System.Boolean System.ComponentModel.BrowsableAttribute::get_Browsable()
extern "C"  bool BrowsableAttribute_get_Browsable_m1697659806 (BrowsableAttribute_t2487167291 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_browsable_0();
		return L_0;
	}
}
// System.Boolean System.ComponentModel.BrowsableAttribute::Equals(System.Object)
extern Il2CppClass* BrowsableAttribute_t2487167291_il2cpp_TypeInfo_var;
extern const uint32_t BrowsableAttribute_Equals_m1726470763_MetadataUsageId;
extern "C"  bool BrowsableAttribute_Equals_m1726470763 (BrowsableAttribute_t2487167291 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BrowsableAttribute_Equals_m1726470763_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___obj0;
		if (((BrowsableAttribute_t2487167291 *)IsInstSealed(L_0, BrowsableAttribute_t2487167291_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = ___obj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(BrowsableAttribute_t2487167291 *)__this))))
		{
			goto IL_0016;
		}
	}
	{
		return (bool)1;
	}

IL_0016:
	{
		Il2CppObject * L_2 = ___obj0;
		NullCheck(((BrowsableAttribute_t2487167291 *)CastclassSealed(L_2, BrowsableAttribute_t2487167291_il2cpp_TypeInfo_var)));
		bool L_3 = BrowsableAttribute_get_Browsable_m1697659806(((BrowsableAttribute_t2487167291 *)CastclassSealed(L_2, BrowsableAttribute_t2487167291_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		bool L_4 = __this->get_browsable_0();
		return (bool)((((int32_t)L_3) == ((int32_t)L_4))? 1 : 0);
	}
}
// System.Int32 System.ComponentModel.BrowsableAttribute::GetHashCode()
extern "C"  int32_t BrowsableAttribute_GetHashCode_m2898709353 (BrowsableAttribute_t2487167291 * __this, const MethodInfo* method)
{
	{
		bool* L_0 = __this->get_address_of_browsable_0();
		int32_t L_1 = Boolean_GetHashCode_m1894638460(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void System.ComponentModel.CategoryAttribute::.ctor()
extern Il2CppCodeGenString* _stringLiteral4264088870;
extern const uint32_t CategoryAttribute__ctor_m897108299_MetadataUsageId;
extern "C"  void CategoryAttribute__ctor_m897108299 (CategoryAttribute_t540457070 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CategoryAttribute__ctor_m897108299_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		__this->set_category_0(_stringLiteral4264088870);
		return;
	}
}
// System.Void System.ComponentModel.CategoryAttribute::.cctor()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* CategoryAttribute_t540457070_il2cpp_TypeInfo_var;
extern const uint32_t CategoryAttribute__cctor_m3743202008_MetadataUsageId;
extern "C"  void CategoryAttribute__cctor_m3743202008 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CategoryAttribute__cctor_m3743202008_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m2551263788(L_0, /*hidden argument*/NULL);
		((CategoryAttribute_t540457070_StaticFields*)CategoryAttribute_t540457070_il2cpp_TypeInfo_var->static_fields)->set_lockobj_2(L_0);
		return;
	}
}
// System.String System.ComponentModel.CategoryAttribute::GetLocalizedString(System.String)
extern "C"  String_t* CategoryAttribute_GetLocalizedString_m791363968 (CategoryAttribute_t540457070 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		String_t* L_1 = Locale_GetText_m4034107474(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String System.ComponentModel.CategoryAttribute::get_Category()
extern "C"  String_t* CategoryAttribute_get_Category_m1674851711 (CategoryAttribute_t540457070 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		bool L_0 = __this->get_IsLocalized_1();
		if (L_0)
		{
			goto IL_002c;
		}
	}
	{
		__this->set_IsLocalized_1((bool)1);
		String_t* L_1 = __this->get_category_0();
		String_t* L_2 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(4 /* System.String System.ComponentModel.CategoryAttribute::GetLocalizedString(System.String) */, __this, L_1);
		V_0 = L_2;
		String_t* L_3 = V_0;
		if (!L_3)
		{
			goto IL_002c;
		}
	}
	{
		String_t* L_4 = V_0;
		__this->set_category_0(L_4);
	}

IL_002c:
	{
		String_t* L_5 = __this->get_category_0();
		return L_5;
	}
}
// System.Boolean System.ComponentModel.CategoryAttribute::Equals(System.Object)
extern Il2CppClass* CategoryAttribute_t540457070_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t CategoryAttribute_Equals_m3997171012_MetadataUsageId;
extern "C"  bool CategoryAttribute_Equals_m3997171012 (CategoryAttribute_t540457070 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CategoryAttribute_Equals_m3997171012_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___obj0;
		if (((CategoryAttribute_t540457070 *)IsInstClass(L_0, CategoryAttribute_t540457070_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = ___obj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(CategoryAttribute_t540457070 *)__this))))
		{
			goto IL_0016;
		}
	}
	{
		return (bool)1;
	}

IL_0016:
	{
		Il2CppObject * L_2 = ___obj0;
		NullCheck(((CategoryAttribute_t540457070 *)CastclassClass(L_2, CategoryAttribute_t540457070_il2cpp_TypeInfo_var)));
		String_t* L_3 = CategoryAttribute_get_Category_m1674851711(((CategoryAttribute_t540457070 *)CastclassClass(L_2, CategoryAttribute_t540457070_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		String_t* L_4 = __this->get_category_0();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Int32 System.ComponentModel.CategoryAttribute::GetHashCode()
extern "C"  int32_t CategoryAttribute_GetHashCode_m524908012 (CategoryAttribute_t540457070 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_category_0();
		NullCheck(L_0);
		int32_t L_1 = String_GetHashCode_m931956593(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void System.ComponentModel.CollectionChangeEventArgs::.ctor(System.ComponentModel.CollectionChangeAction,System.Object)
extern Il2CppClass* EventArgs_t3289624707_il2cpp_TypeInfo_var;
extern const uint32_t CollectionChangeEventArgs__ctor_m2428891309_MetadataUsageId;
extern "C"  void CollectionChangeEventArgs__ctor_m2428891309 (CollectionChangeEventArgs_t1734749345 * __this, int32_t ___action0, Il2CppObject * ___element1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CollectionChangeEventArgs__ctor_m2428891309_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t3289624707_il2cpp_TypeInfo_var);
		EventArgs__ctor_m3696060910(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___action0;
		__this->set_changeAction_1(L_0);
		Il2CppObject * L_1 = ___element1;
		__this->set_theElement_2(L_1);
		return;
	}
}
// System.ComponentModel.CollectionChangeAction System.ComponentModel.CollectionChangeEventArgs::get_Action()
extern "C"  int32_t CollectionChangeEventArgs_get_Action_m4191935319 (CollectionChangeEventArgs_t1734749345 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_changeAction_1();
		return L_0;
	}
}
// System.Object System.ComponentModel.CollectionChangeEventArgs::get_Element()
extern "C"  Il2CppObject * CollectionChangeEventArgs_get_Element_m3099257486 (CollectionChangeEventArgs_t1734749345 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_theElement_2();
		return L_0;
	}
}
// System.Void System.ComponentModel.CollectionChangeEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void CollectionChangeEventHandler__ctor_m471418715 (CollectionChangeEventHandler_t790626706 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.ComponentModel.CollectionChangeEventHandler::Invoke(System.Object,System.ComponentModel.CollectionChangeEventArgs)
extern "C"  void CollectionChangeEventHandler_Invoke_m1100007127 (CollectionChangeEventHandler_t790626706 * __this, Il2CppObject * ___sender0, CollectionChangeEventArgs_t1734749345 * ___e1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		CollectionChangeEventHandler_Invoke_m1100007127((CollectionChangeEventHandler_t790626706 *)__this->get_prev_9(),___sender0, ___e1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___sender0, CollectionChangeEventArgs_t1734749345 * ___e1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___sender0, ___e1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___sender0, CollectionChangeEventArgs_t1734749345 * ___e1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___sender0, ___e1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, CollectionChangeEventArgs_t1734749345 * ___e1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___sender0, ___e1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.ComponentModel.CollectionChangeEventHandler::BeginInvoke(System.Object,System.ComponentModel.CollectionChangeEventArgs,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * CollectionChangeEventHandler_BeginInvoke_m1462915896 (CollectionChangeEventHandler_t790626706 * __this, Il2CppObject * ___sender0, CollectionChangeEventArgs_t1734749345 * ___e1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___sender0;
	__d_args[1] = ___e1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void System.ComponentModel.CollectionChangeEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void CollectionChangeEventHandler_EndInvoke_m3482772445 (CollectionChangeEventHandler_t790626706 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.ComponentModel.Component::.ctor()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Component__ctor_m685180076_MetadataUsageId;
extern "C"  void Component__ctor_m685180076 (Component_t2826673791 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Component__ctor_m685180076_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m2551263788(L_0, /*hidden argument*/NULL);
		__this->set_disposedEvent_3(L_0);
		MarshalByRefObject__ctor_m529577364(__this, /*hidden argument*/NULL);
		__this->set_event_handlers_1((EventHandlerList_t1298116880 *)NULL);
		return;
	}
}
// System.ComponentModel.EventHandlerList System.ComponentModel.Component::get_Events()
extern Il2CppClass* EventHandlerList_t1298116880_il2cpp_TypeInfo_var;
extern const uint32_t Component_get_Events_m1498771332_MetadataUsageId;
extern "C"  EventHandlerList_t1298116880 * Component_get_Events_m1498771332 (Component_t2826673791 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Component_get_Events_m1498771332_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		EventHandlerList_t1298116880 * L_0 = __this->get_event_handlers_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		EventHandlerList_t1298116880 * L_1 = (EventHandlerList_t1298116880 *)il2cpp_codegen_object_new(EventHandlerList_t1298116880_il2cpp_TypeInfo_var);
		EventHandlerList__ctor_m125822993(L_1, /*hidden argument*/NULL);
		__this->set_event_handlers_1(L_1);
	}

IL_0016:
	{
		EventHandlerList_t1298116880 * L_2 = __this->get_event_handlers_1();
		return L_2;
	}
}
// System.Void System.ComponentModel.Component::Finalize()
extern "C"  void Component_Finalize_m3915583340 (Component_t2826673791 * __this, const MethodInfo* method)
{
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		VirtActionInvoker1< bool >::Invoke(7 /* System.Void System.ComponentModel.Component::Dispose(System.Boolean) */, __this, (bool)0);
		IL2CPP_LEAVE(0x13, FINALLY_000c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_000c;
	}

FINALLY_000c:
	{ // begin finally (depth: 1)
		Object_Finalize_m4087144328(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(12)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(12)
	{
		IL2CPP_JUMP_TBL(0x13, IL_0013)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0013:
	{
		return;
	}
}
// System.Void System.ComponentModel.Component::Dispose()
extern "C"  void Component_Dispose_m3787380235 (Component_t2826673791 * __this, const MethodInfo* method)
{
	{
		VirtActionInvoker1< bool >::Invoke(7 /* System.Void System.ComponentModel.Component::Dispose(System.Boolean) */, __this, (bool)1);
		GC_SuppressFinalize_m953228702(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.ComponentModel.Component::Dispose(System.Boolean)
extern Il2CppClass* ISite_t1774720436_il2cpp_TypeInfo_var;
extern Il2CppClass* IContainer_t3025744548_il2cpp_TypeInfo_var;
extern Il2CppClass* EventHandler_t277755526_il2cpp_TypeInfo_var;
extern Il2CppClass* EventArgs_t3289624707_il2cpp_TypeInfo_var;
extern const uint32_t Component_Dispose_m1758989928_MetadataUsageId;
extern "C"  void Component_Dispose_m1758989928 (Component_t2826673791 * __this, bool ___release_all0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Component_Dispose_m1758989928_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_t277755526 * V_0 = NULL;
	{
		bool L_0 = ___release_all0;
		if (!L_0)
		{
			goto IL_005b;
		}
	}
	{
		Il2CppObject * L_1 = __this->get_mySite_2();
		if (!L_1)
		{
			goto IL_0032;
		}
	}
	{
		Il2CppObject * L_2 = __this->get_mySite_2();
		NullCheck(L_2);
		Il2CppObject * L_3 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.ComponentModel.IContainer System.ComponentModel.ISite::get_Container() */, ISite_t1774720436_il2cpp_TypeInfo_var, L_2);
		if (!L_3)
		{
			goto IL_0032;
		}
	}
	{
		Il2CppObject * L_4 = __this->get_mySite_2();
		NullCheck(L_4);
		Il2CppObject * L_5 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.ComponentModel.IContainer System.ComponentModel.ISite::get_Container() */, ISite_t1774720436_il2cpp_TypeInfo_var, L_4);
		NullCheck(L_5);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(0 /* System.Void System.ComponentModel.IContainer::Remove(System.ComponentModel.IComponent) */, IContainer_t3025744548_il2cpp_TypeInfo_var, L_5, __this);
	}

IL_0032:
	{
		EventHandlerList_t1298116880 * L_6 = Component_get_Events_m1498771332(__this, /*hidden argument*/NULL);
		Il2CppObject * L_7 = __this->get_disposedEvent_3();
		NullCheck(L_6);
		Delegate_t3022476291 * L_8 = EventHandlerList_get_Item_m533246000(L_6, L_7, /*hidden argument*/NULL);
		V_0 = ((EventHandler_t277755526 *)CastclassSealed(L_8, EventHandler_t277755526_il2cpp_TypeInfo_var));
		EventHandler_t277755526 * L_9 = V_0;
		if (!L_9)
		{
			goto IL_005b;
		}
	}
	{
		EventHandler_t277755526 * L_10 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t3289624707_il2cpp_TypeInfo_var);
		EventArgs_t3289624707 * L_11 = ((EventArgs_t3289624707_StaticFields*)EventArgs_t3289624707_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		NullCheck(L_10);
		EventHandler_Invoke_m1137722757(L_10, __this, L_11, /*hidden argument*/NULL);
	}

IL_005b:
	{
		return;
	}
}
// System.String System.ComponentModel.Component::ToString()
extern Il2CppClass* ISite_t1774720436_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2410691315;
extern const uint32_t Component_ToString_m1082195383_MetadataUsageId;
extern "C"  String_t* Component_ToString_m1082195383 (Component_t2826673791 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Component_ToString_m1082195383_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get_mySite_2();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		Type_t * L_1 = Object_GetType_m191970594(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Type::ToString() */, L_1);
		return L_2;
	}

IL_0017:
	{
		Il2CppObject * L_3 = __this->get_mySite_2();
		NullCheck(L_3);
		String_t* L_4 = InterfaceFuncInvoker0< String_t* >::Invoke(1 /* System.String System.ComponentModel.ISite::get_Name() */, ISite_t1774720436_il2cpp_TypeInfo_var, L_3);
		Type_t * L_5 = Object_GetType_m191970594(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Type::ToString() */, L_5);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Format_m1811873526(NULL /*static, unused*/, _stringLiteral2410691315, L_4, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void System.ComponentModel.DefaultEventAttribute::.ctor(System.String)
extern "C"  void DefaultEventAttribute__ctor_m2255997336 (DefaultEventAttribute_t1079704873 * __this, String_t* ___name0, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name0;
		__this->set_eventName_0(L_0);
		return;
	}
}
// System.Void System.ComponentModel.DefaultEventAttribute::.cctor()
extern Il2CppClass* DefaultEventAttribute_t1079704873_il2cpp_TypeInfo_var;
extern const uint32_t DefaultEventAttribute__cctor_m1343114747_MetadataUsageId;
extern "C"  void DefaultEventAttribute__cctor_m1343114747 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultEventAttribute__cctor_m1343114747_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		DefaultEventAttribute_t1079704873 * L_0 = (DefaultEventAttribute_t1079704873 *)il2cpp_codegen_object_new(DefaultEventAttribute_t1079704873_il2cpp_TypeInfo_var);
		DefaultEventAttribute__ctor_m2255997336(L_0, (String_t*)NULL, /*hidden argument*/NULL);
		((DefaultEventAttribute_t1079704873_StaticFields*)DefaultEventAttribute_t1079704873_il2cpp_TypeInfo_var->static_fields)->set_Default_1(L_0);
		return;
	}
}
// System.Boolean System.ComponentModel.DefaultEventAttribute::Equals(System.Object)
extern Il2CppClass* DefaultEventAttribute_t1079704873_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t DefaultEventAttribute_Equals_m2591923917_MetadataUsageId;
extern "C"  bool DefaultEventAttribute_Equals_m2591923917 (DefaultEventAttribute_t1079704873 * __this, Il2CppObject * ___o0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultEventAttribute_Equals_m2591923917_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___o0;
		if (((DefaultEventAttribute_t1079704873 *)IsInstSealed(L_0, DefaultEventAttribute_t1079704873_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = ___o0;
		NullCheck(((DefaultEventAttribute_t1079704873 *)CastclassSealed(L_1, DefaultEventAttribute_t1079704873_il2cpp_TypeInfo_var)));
		String_t* L_2 = ((DefaultEventAttribute_t1079704873 *)CastclassSealed(L_1, DefaultEventAttribute_t1079704873_il2cpp_TypeInfo_var))->get_eventName_0();
		String_t* L_3 = __this->get_eventName_0();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_4 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Int32 System.ComponentModel.DefaultEventAttribute::GetHashCode()
extern "C"  int32_t DefaultEventAttribute_GetHashCode_m2295971583 (DefaultEventAttribute_t1079704873 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Attribute_GetHashCode_m2653962112(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void System.ComponentModel.DefaultPropertyAttribute::.ctor(System.String)
extern "C"  void DefaultPropertyAttribute__ctor_m3079322845 (DefaultPropertyAttribute_t1962767338 * __this, String_t* ___name0, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name0;
		__this->set_property_name_0(L_0);
		return;
	}
}
// System.Void System.ComponentModel.DefaultPropertyAttribute::.cctor()
extern Il2CppClass* DefaultPropertyAttribute_t1962767338_il2cpp_TypeInfo_var;
extern const uint32_t DefaultPropertyAttribute__cctor_m2808816216_MetadataUsageId;
extern "C"  void DefaultPropertyAttribute__cctor_m2808816216 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultPropertyAttribute__cctor_m2808816216_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		DefaultPropertyAttribute_t1962767338 * L_0 = (DefaultPropertyAttribute_t1962767338 *)il2cpp_codegen_object_new(DefaultPropertyAttribute_t1962767338_il2cpp_TypeInfo_var);
		DefaultPropertyAttribute__ctor_m3079322845(L_0, (String_t*)NULL, /*hidden argument*/NULL);
		((DefaultPropertyAttribute_t1962767338_StaticFields*)DefaultPropertyAttribute_t1962767338_il2cpp_TypeInfo_var->static_fields)->set_Default_1(L_0);
		return;
	}
}
// System.String System.ComponentModel.DefaultPropertyAttribute::get_Name()
extern "C"  String_t* DefaultPropertyAttribute_get_Name_m3629679946 (DefaultPropertyAttribute_t1962767338 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_property_name_0();
		return L_0;
	}
}
// System.Boolean System.ComponentModel.DefaultPropertyAttribute::Equals(System.Object)
extern Il2CppClass* DefaultPropertyAttribute_t1962767338_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t DefaultPropertyAttribute_Equals_m4294496664_MetadataUsageId;
extern "C"  bool DefaultPropertyAttribute_Equals_m4294496664 (DefaultPropertyAttribute_t1962767338 * __this, Il2CppObject * ___o0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultPropertyAttribute_Equals_m4294496664_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___o0;
		if (((DefaultPropertyAttribute_t1962767338 *)IsInstSealed(L_0, DefaultPropertyAttribute_t1962767338_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = ___o0;
		NullCheck(((DefaultPropertyAttribute_t1962767338 *)CastclassSealed(L_1, DefaultPropertyAttribute_t1962767338_il2cpp_TypeInfo_var)));
		String_t* L_2 = DefaultPropertyAttribute_get_Name_m3629679946(((DefaultPropertyAttribute_t1962767338 *)CastclassSealed(L_1, DefaultPropertyAttribute_t1962767338_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		String_t* L_3 = __this->get_property_name_0();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_4 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Int32 System.ComponentModel.DefaultPropertyAttribute::GetHashCode()
extern "C"  int32_t DefaultPropertyAttribute_GetHashCode_m3502177152 (DefaultPropertyAttribute_t1962767338 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Attribute_GetHashCode_m2653962112(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void System.ComponentModel.DefaultValueAttribute::.ctor(System.Boolean)
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern const uint32_t DefaultValueAttribute__ctor_m2050964062_MetadataUsageId;
extern "C"  void DefaultValueAttribute__ctor_m2050964062 (DefaultValueAttribute_t1302720498 * __this, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultValueAttribute__ctor_m2050964062_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		bool L_0 = ___value0;
		bool L_1 = L_0;
		Il2CppObject * L_2 = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &L_1);
		__this->set_DefaultValue_0(L_2);
		return;
	}
}
// System.Void System.ComponentModel.DefaultValueAttribute::.ctor(System.Int32)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t DefaultValueAttribute__ctor_m2366108490_MetadataUsageId;
extern "C"  void DefaultValueAttribute__ctor_m2366108490 (DefaultValueAttribute_t1302720498 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultValueAttribute__ctor_m2366108490_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___value0;
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_1);
		__this->set_DefaultValue_0(L_2);
		return;
	}
}
// System.Void System.ComponentModel.DefaultValueAttribute::.ctor(System.Object)
extern "C"  void DefaultValueAttribute__ctor_m823714377 (DefaultValueAttribute_t1302720498 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___value0;
		__this->set_DefaultValue_0(L_0);
		return;
	}
}
// System.Void System.ComponentModel.DefaultValueAttribute::.ctor(System.String)
extern "C"  void DefaultValueAttribute__ctor_m424349933 (DefaultValueAttribute_t1302720498 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___value0;
		__this->set_DefaultValue_0(L_0);
		return;
	}
}
// System.Object System.ComponentModel.DefaultValueAttribute::get_Value()
extern "C"  Il2CppObject * DefaultValueAttribute_get_Value_m3806689510 (DefaultValueAttribute_t1302720498 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_DefaultValue_0();
		return L_0;
	}
}
// System.Boolean System.ComponentModel.DefaultValueAttribute::Equals(System.Object)
extern Il2CppClass* DefaultValueAttribute_t1302720498_il2cpp_TypeInfo_var;
extern const uint32_t DefaultValueAttribute_Equals_m695787710_MetadataUsageId;
extern "C"  bool DefaultValueAttribute_Equals_m695787710 (DefaultValueAttribute_t1302720498 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultValueAttribute_Equals_m695787710_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DefaultValueAttribute_t1302720498 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___obj0;
		V_0 = ((DefaultValueAttribute_t1302720498 *)IsInstClass(L_0, DefaultValueAttribute_t1302720498_il2cpp_TypeInfo_var));
		DefaultValueAttribute_t1302720498 * L_1 = V_0;
		if (L_1)
		{
			goto IL_000f;
		}
	}
	{
		return (bool)0;
	}

IL_000f:
	{
		Il2CppObject * L_2 = __this->get_DefaultValue_0();
		if (L_2)
		{
			goto IL_0024;
		}
	}
	{
		DefaultValueAttribute_t1302720498 * L_3 = V_0;
		NullCheck(L_3);
		Il2CppObject * L_4 = VirtFuncInvoker0< Il2CppObject * >::Invoke(4 /* System.Object System.ComponentModel.DefaultValueAttribute::get_Value() */, L_3);
		return (bool)((((Il2CppObject*)(Il2CppObject *)L_4) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
	}

IL_0024:
	{
		Il2CppObject * L_5 = __this->get_DefaultValue_0();
		DefaultValueAttribute_t1302720498 * L_6 = V_0;
		NullCheck(L_6);
		Il2CppObject * L_7 = VirtFuncInvoker0< Il2CppObject * >::Invoke(4 /* System.Object System.ComponentModel.DefaultValueAttribute::get_Value() */, L_6);
		NullCheck(L_5);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, L_5, L_7);
		return L_8;
	}
}
// System.Int32 System.ComponentModel.DefaultValueAttribute::GetHashCode()
extern "C"  int32_t DefaultValueAttribute_GetHashCode_m1764277706 (DefaultValueAttribute_t1302720498 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_DefaultValue_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		int32_t L_1 = Attribute_GetHashCode_m2653962112(__this, /*hidden argument*/NULL);
		return L_1;
	}

IL_0012:
	{
		Il2CppObject * L_2 = __this->get_DefaultValue_0();
		NullCheck(L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_2);
		return L_3;
	}
}
// System.Void System.ComponentModel.DescriptionAttribute::.ctor()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t DescriptionAttribute__ctor_m3178533097_MetadataUsageId;
extern "C"  void DescriptionAttribute__ctor_m3178533097 (DescriptionAttribute_t3207779672 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DescriptionAttribute__ctor_m3178533097_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_desc_0(L_0);
		return;
	}
}
// System.Void System.ComponentModel.DescriptionAttribute::.ctor(System.String)
extern "C"  void DescriptionAttribute__ctor_m4290862631 (DescriptionAttribute_t3207779672 * __this, String_t* ___name0, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name0;
		__this->set_desc_0(L_0);
		return;
	}
}
// System.Void System.ComponentModel.DescriptionAttribute::.cctor()
extern Il2CppClass* DescriptionAttribute_t3207779672_il2cpp_TypeInfo_var;
extern const uint32_t DescriptionAttribute__cctor_m1546248318_MetadataUsageId;
extern "C"  void DescriptionAttribute__cctor_m1546248318 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DescriptionAttribute__cctor_m1546248318_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		DescriptionAttribute_t3207779672 * L_0 = (DescriptionAttribute_t3207779672 *)il2cpp_codegen_object_new(DescriptionAttribute_t3207779672_il2cpp_TypeInfo_var);
		DescriptionAttribute__ctor_m3178533097(L_0, /*hidden argument*/NULL);
		((DescriptionAttribute_t3207779672_StaticFields*)DescriptionAttribute_t3207779672_il2cpp_TypeInfo_var->static_fields)->set_Default_1(L_0);
		return;
	}
}
// System.String System.ComponentModel.DescriptionAttribute::get_Description()
extern "C"  String_t* DescriptionAttribute_get_Description_m1872479123 (DescriptionAttribute_t3207779672 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = DescriptionAttribute_get_DescriptionValue_m387684516(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.String System.ComponentModel.DescriptionAttribute::get_DescriptionValue()
extern "C"  String_t* DescriptionAttribute_get_DescriptionValue_m387684516 (DescriptionAttribute_t3207779672 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_desc_0();
		return L_0;
	}
}
// System.Boolean System.ComponentModel.DescriptionAttribute::Equals(System.Object)
extern Il2CppClass* DescriptionAttribute_t3207779672_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t DescriptionAttribute_Equals_m1045561206_MetadataUsageId;
extern "C"  bool DescriptionAttribute_Equals_m1045561206 (DescriptionAttribute_t3207779672 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DescriptionAttribute_Equals_m1045561206_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___obj0;
		if (((DescriptionAttribute_t3207779672 *)IsInstClass(L_0, DescriptionAttribute_t3207779672_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = ___obj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(DescriptionAttribute_t3207779672 *)__this))))
		{
			goto IL_0016;
		}
	}
	{
		return (bool)1;
	}

IL_0016:
	{
		Il2CppObject * L_2 = ___obj0;
		NullCheck(((DescriptionAttribute_t3207779672 *)CastclassClass(L_2, DescriptionAttribute_t3207779672_il2cpp_TypeInfo_var)));
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String System.ComponentModel.DescriptionAttribute::get_Description() */, ((DescriptionAttribute_t3207779672 *)CastclassClass(L_2, DescriptionAttribute_t3207779672_il2cpp_TypeInfo_var)));
		String_t* L_4 = __this->get_desc_0();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Int32 System.ComponentModel.DescriptionAttribute::GetHashCode()
extern "C"  int32_t DescriptionAttribute_GetHashCode_m4180319982 (DescriptionAttribute_t3207779672 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_desc_0();
		NullCheck(L_0);
		int32_t L_1 = String_GetHashCode_m931956593(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void System.ComponentModel.DesignerAttribute::.ctor(System.String)
extern const Il2CppType* IDesigner_t587069890_0_0_0_var;
extern Il2CppClass* NullReferenceException_t3156209119_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t DesignerAttribute__ctor_m2876366278_MetadataUsageId;
extern "C"  void DesignerAttribute__ctor_m2876366278 (DesignerAttribute_t2778719479 * __this, String_t* ___designerTypeName0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DesignerAttribute__ctor_m2876366278_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___designerTypeName0;
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		NullReferenceException_t3156209119 * L_1 = (NullReferenceException_t3156209119 *)il2cpp_codegen_object_new(NullReferenceException_t3156209119_il2cpp_TypeInfo_var);
		NullReferenceException__ctor_m2202599572(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		String_t* L_2 = ___designerTypeName0;
		__this->set_name_0(L_2);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(IDesigner_t587069890_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, L_3);
		__this->set_basetypename_1(L_4);
		return;
	}
}
// System.Void System.ComponentModel.DesignerAttribute::.ctor(System.String,System.Type)
extern "C"  void DesignerAttribute__ctor_m301030763 (DesignerAttribute_t2778719479 * __this, String_t* ___designerTypeName0, Type_t * ___designerBaseType1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___designerTypeName0;
		Type_t * L_1 = ___designerBaseType1;
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(15 /* System.String System.Type::get_AssemblyQualifiedName() */, L_1);
		DesignerAttribute__ctor_m206724570(__this, L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.ComponentModel.DesignerAttribute::.ctor(System.String,System.String)
extern Il2CppClass* NullReferenceException_t3156209119_il2cpp_TypeInfo_var;
extern const uint32_t DesignerAttribute__ctor_m206724570_MetadataUsageId;
extern "C"  void DesignerAttribute__ctor_m206724570 (DesignerAttribute_t2778719479 * __this, String_t* ___designerTypeName0, String_t* ___designerBaseTypeName1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DesignerAttribute__ctor_m206724570_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___designerTypeName0;
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		NullReferenceException_t3156209119 * L_1 = (NullReferenceException_t3156209119 *)il2cpp_codegen_object_new(NullReferenceException_t3156209119_il2cpp_TypeInfo_var);
		NullReferenceException__ctor_m2202599572(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		String_t* L_2 = ___designerTypeName0;
		__this->set_name_0(L_2);
		String_t* L_3 = ___designerBaseTypeName1;
		__this->set_basetypename_1(L_3);
		return;
	}
}
// System.String System.ComponentModel.DesignerAttribute::get_DesignerBaseTypeName()
extern "C"  String_t* DesignerAttribute_get_DesignerBaseTypeName_m2226214009 (DesignerAttribute_t2778719479 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_basetypename_1();
		return L_0;
	}
}
// System.String System.ComponentModel.DesignerAttribute::get_DesignerTypeName()
extern "C"  String_t* DesignerAttribute_get_DesignerTypeName_m2291864638 (DesignerAttribute_t2778719479 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_name_0();
		return L_0;
	}
}
// System.Boolean System.ComponentModel.DesignerAttribute::Equals(System.Object)
extern Il2CppClass* DesignerAttribute_t2778719479_il2cpp_TypeInfo_var;
extern const uint32_t DesignerAttribute_Equals_m4260278407_MetadataUsageId;
extern "C"  bool DesignerAttribute_Equals_m4260278407 (DesignerAttribute_t2778719479 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DesignerAttribute_Equals_m4260278407_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B5_0 = 0;
	{
		Il2CppObject * L_0 = ___obj0;
		if (((DesignerAttribute_t2778719479 *)IsInstSealed(L_0, DesignerAttribute_t2778719479_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = ___obj0;
		NullCheck(((DesignerAttribute_t2778719479 *)CastclassSealed(L_1, DesignerAttribute_t2778719479_il2cpp_TypeInfo_var)));
		String_t* L_2 = DesignerAttribute_get_DesignerBaseTypeName_m2226214009(((DesignerAttribute_t2778719479 *)CastclassSealed(L_1, DesignerAttribute_t2778719479_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		String_t* L_3 = __this->get_basetypename_1();
		NullCheck(L_2);
		bool L_4 = String_Equals_m2633592423(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0040;
		}
	}
	{
		Il2CppObject * L_5 = ___obj0;
		NullCheck(((DesignerAttribute_t2778719479 *)CastclassSealed(L_5, DesignerAttribute_t2778719479_il2cpp_TypeInfo_var)));
		String_t* L_6 = DesignerAttribute_get_DesignerTypeName_m2291864638(((DesignerAttribute_t2778719479 *)CastclassSealed(L_5, DesignerAttribute_t2778719479_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		String_t* L_7 = __this->get_name_0();
		NullCheck(L_6);
		bool L_8 = String_Equals_m2633592423(L_6, L_7, /*hidden argument*/NULL);
		G_B5_0 = ((int32_t)(L_8));
		goto IL_0041;
	}

IL_0040:
	{
		G_B5_0 = 0;
	}

IL_0041:
	{
		return (bool)G_B5_0;
	}
}
// System.Int32 System.ComponentModel.DesignerAttribute::GetHashCode()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t DesignerAttribute_GetHashCode_m1478989101_MetadataUsageId;
extern "C"  int32_t DesignerAttribute_GetHashCode_m1478989101 (DesignerAttribute_t2778719479 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DesignerAttribute_GetHashCode_m1478989101_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = __this->get_name_0();
		String_t* L_1 = __this->get_basetypename_1();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m2596409543(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = String_GetHashCode_m931956593(L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void System.ComponentModel.DesignerCategoryAttribute::.ctor(System.String)
extern "C"  void DesignerCategoryAttribute__ctor_m2883200354 (DesignerCategoryAttribute_t1270090451 * __this, String_t* ___category0, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___category0;
		__this->set_category_0(L_0);
		return;
	}
}
// System.Void System.ComponentModel.DesignerCategoryAttribute::.cctor()
extern Il2CppClass* DesignerCategoryAttribute_t1270090451_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1948514967;
extern Il2CppCodeGenString* _stringLiteral1770476244;
extern Il2CppCodeGenString* _stringLiteral708851129;
extern const uint32_t DesignerCategoryAttribute__cctor_m3553693865_MetadataUsageId;
extern "C"  void DesignerCategoryAttribute__cctor_m3553693865 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DesignerCategoryAttribute__cctor_m3553693865_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		DesignerCategoryAttribute_t1270090451 * L_0 = (DesignerCategoryAttribute_t1270090451 *)il2cpp_codegen_object_new(DesignerCategoryAttribute_t1270090451_il2cpp_TypeInfo_var);
		DesignerCategoryAttribute__ctor_m2883200354(L_0, _stringLiteral1948514967, /*hidden argument*/NULL);
		((DesignerCategoryAttribute_t1270090451_StaticFields*)DesignerCategoryAttribute_t1270090451_il2cpp_TypeInfo_var->static_fields)->set_Component_1(L_0);
		DesignerCategoryAttribute_t1270090451 * L_1 = (DesignerCategoryAttribute_t1270090451 *)il2cpp_codegen_object_new(DesignerCategoryAttribute_t1270090451_il2cpp_TypeInfo_var);
		DesignerCategoryAttribute__ctor_m2883200354(L_1, _stringLiteral1770476244, /*hidden argument*/NULL);
		((DesignerCategoryAttribute_t1270090451_StaticFields*)DesignerCategoryAttribute_t1270090451_il2cpp_TypeInfo_var->static_fields)->set_Form_2(L_1);
		DesignerCategoryAttribute_t1270090451 * L_2 = (DesignerCategoryAttribute_t1270090451 *)il2cpp_codegen_object_new(DesignerCategoryAttribute_t1270090451_il2cpp_TypeInfo_var);
		DesignerCategoryAttribute__ctor_m2883200354(L_2, _stringLiteral708851129, /*hidden argument*/NULL);
		((DesignerCategoryAttribute_t1270090451_StaticFields*)DesignerCategoryAttribute_t1270090451_il2cpp_TypeInfo_var->static_fields)->set_Generic_3(L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		DesignerCategoryAttribute_t1270090451 * L_4 = (DesignerCategoryAttribute_t1270090451 *)il2cpp_codegen_object_new(DesignerCategoryAttribute_t1270090451_il2cpp_TypeInfo_var);
		DesignerCategoryAttribute__ctor_m2883200354(L_4, L_3, /*hidden argument*/NULL);
		((DesignerCategoryAttribute_t1270090451_StaticFields*)DesignerCategoryAttribute_t1270090451_il2cpp_TypeInfo_var->static_fields)->set_Default_4(L_4);
		return;
	}
}
// System.String System.ComponentModel.DesignerCategoryAttribute::get_Category()
extern "C"  String_t* DesignerCategoryAttribute_get_Category_m2847651960 (DesignerCategoryAttribute_t1270090451 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_category_0();
		return L_0;
	}
}
// System.Boolean System.ComponentModel.DesignerCategoryAttribute::Equals(System.Object)
extern Il2CppClass* DesignerCategoryAttribute_t1270090451_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t DesignerCategoryAttribute_Equals_m1268982431_MetadataUsageId;
extern "C"  bool DesignerCategoryAttribute_Equals_m1268982431 (DesignerCategoryAttribute_t1270090451 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DesignerCategoryAttribute_Equals_m1268982431_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___obj0;
		if (((DesignerCategoryAttribute_t1270090451 *)IsInstSealed(L_0, DesignerCategoryAttribute_t1270090451_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = ___obj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(DesignerCategoryAttribute_t1270090451 *)__this))))
		{
			goto IL_0016;
		}
	}
	{
		return (bool)1;
	}

IL_0016:
	{
		Il2CppObject * L_2 = ___obj0;
		NullCheck(((DesignerCategoryAttribute_t1270090451 *)CastclassSealed(L_2, DesignerCategoryAttribute_t1270090451_il2cpp_TypeInfo_var)));
		String_t* L_3 = DesignerCategoryAttribute_get_Category_m2847651960(((DesignerCategoryAttribute_t1270090451 *)CastclassSealed(L_2, DesignerCategoryAttribute_t1270090451_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		String_t* L_4 = __this->get_category_0();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Int32 System.ComponentModel.DesignerCategoryAttribute::GetHashCode()
extern "C"  int32_t DesignerCategoryAttribute_GetHashCode_m2923172713 (DesignerCategoryAttribute_t1270090451 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_category_0();
		NullCheck(L_0);
		int32_t L_1 = String_GetHashCode_m931956593(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void System.ComponentModel.DesignerSerializationVisibilityAttribute::.ctor(System.ComponentModel.DesignerSerializationVisibility)
extern "C"  void DesignerSerializationVisibilityAttribute__ctor_m1400474148 (DesignerSerializationVisibilityAttribute_t2980019899 * __this, int32_t ___vis0, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___vis0;
		__this->set_visibility_0(L_0);
		return;
	}
}
// System.Void System.ComponentModel.DesignerSerializationVisibilityAttribute::.cctor()
extern Il2CppClass* DesignerSerializationVisibilityAttribute_t2980019899_il2cpp_TypeInfo_var;
extern const uint32_t DesignerSerializationVisibilityAttribute__cctor_m3963815613_MetadataUsageId;
extern "C"  void DesignerSerializationVisibilityAttribute__cctor_m3963815613 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DesignerSerializationVisibilityAttribute__cctor_m3963815613_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		DesignerSerializationVisibilityAttribute_t2980019899 * L_0 = (DesignerSerializationVisibilityAttribute_t2980019899 *)il2cpp_codegen_object_new(DesignerSerializationVisibilityAttribute_t2980019899_il2cpp_TypeInfo_var);
		DesignerSerializationVisibilityAttribute__ctor_m1400474148(L_0, 1, /*hidden argument*/NULL);
		((DesignerSerializationVisibilityAttribute_t2980019899_StaticFields*)DesignerSerializationVisibilityAttribute_t2980019899_il2cpp_TypeInfo_var->static_fields)->set_Default_1(L_0);
		DesignerSerializationVisibilityAttribute_t2980019899 * L_1 = (DesignerSerializationVisibilityAttribute_t2980019899 *)il2cpp_codegen_object_new(DesignerSerializationVisibilityAttribute_t2980019899_il2cpp_TypeInfo_var);
		DesignerSerializationVisibilityAttribute__ctor_m1400474148(L_1, 2, /*hidden argument*/NULL);
		((DesignerSerializationVisibilityAttribute_t2980019899_StaticFields*)DesignerSerializationVisibilityAttribute_t2980019899_il2cpp_TypeInfo_var->static_fields)->set_Content_2(L_1);
		DesignerSerializationVisibilityAttribute_t2980019899 * L_2 = (DesignerSerializationVisibilityAttribute_t2980019899 *)il2cpp_codegen_object_new(DesignerSerializationVisibilityAttribute_t2980019899_il2cpp_TypeInfo_var);
		DesignerSerializationVisibilityAttribute__ctor_m1400474148(L_2, 0, /*hidden argument*/NULL);
		((DesignerSerializationVisibilityAttribute_t2980019899_StaticFields*)DesignerSerializationVisibilityAttribute_t2980019899_il2cpp_TypeInfo_var->static_fields)->set_Hidden_3(L_2);
		DesignerSerializationVisibilityAttribute_t2980019899 * L_3 = (DesignerSerializationVisibilityAttribute_t2980019899 *)il2cpp_codegen_object_new(DesignerSerializationVisibilityAttribute_t2980019899_il2cpp_TypeInfo_var);
		DesignerSerializationVisibilityAttribute__ctor_m1400474148(L_3, 1, /*hidden argument*/NULL);
		((DesignerSerializationVisibilityAttribute_t2980019899_StaticFields*)DesignerSerializationVisibilityAttribute_t2980019899_il2cpp_TypeInfo_var->static_fields)->set_Visible_4(L_3);
		return;
	}
}
// System.ComponentModel.DesignerSerializationVisibility System.ComponentModel.DesignerSerializationVisibilityAttribute::get_Visibility()
extern "C"  int32_t DesignerSerializationVisibilityAttribute_get_Visibility_m1322167120 (DesignerSerializationVisibilityAttribute_t2980019899 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_visibility_0();
		return L_0;
	}
}
// System.Boolean System.ComponentModel.DesignerSerializationVisibilityAttribute::Equals(System.Object)
extern Il2CppClass* DesignerSerializationVisibilityAttribute_t2980019899_il2cpp_TypeInfo_var;
extern const uint32_t DesignerSerializationVisibilityAttribute_Equals_m810877623_MetadataUsageId;
extern "C"  bool DesignerSerializationVisibilityAttribute_Equals_m810877623 (DesignerSerializationVisibilityAttribute_t2980019899 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DesignerSerializationVisibilityAttribute_Equals_m810877623_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___obj0;
		if (((DesignerSerializationVisibilityAttribute_t2980019899 *)IsInstSealed(L_0, DesignerSerializationVisibilityAttribute_t2980019899_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = ___obj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(DesignerSerializationVisibilityAttribute_t2980019899 *)__this))))
		{
			goto IL_0016;
		}
	}
	{
		return (bool)1;
	}

IL_0016:
	{
		Il2CppObject * L_2 = ___obj0;
		NullCheck(((DesignerSerializationVisibilityAttribute_t2980019899 *)CastclassSealed(L_2, DesignerSerializationVisibilityAttribute_t2980019899_il2cpp_TypeInfo_var)));
		int32_t L_3 = DesignerSerializationVisibilityAttribute_get_Visibility_m1322167120(((DesignerSerializationVisibilityAttribute_t2980019899 *)CastclassSealed(L_2, DesignerSerializationVisibilityAttribute_t2980019899_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		int32_t L_4 = __this->get_visibility_0();
		return (bool)((((int32_t)L_3) == ((int32_t)L_4))? 1 : 0);
	}
}
// System.Int32 System.ComponentModel.DesignerSerializationVisibilityAttribute::GetHashCode()
extern Il2CppClass* DesignerSerializationVisibility_t3751360903_il2cpp_TypeInfo_var;
extern const uint32_t DesignerSerializationVisibilityAttribute_GetHashCode_m4243283729_MetadataUsageId;
extern "C"  int32_t DesignerSerializationVisibilityAttribute_GetHashCode_m4243283729 (DesignerSerializationVisibilityAttribute_t2980019899 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DesignerSerializationVisibilityAttribute_GetHashCode_m4243283729_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_visibility_0();
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(DesignerSerializationVisibility_t3751360903_il2cpp_TypeInfo_var, &L_1);
		NullCheck((Enum_t2459695545 *)L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Enum::GetHashCode() */, (Enum_t2459695545 *)L_2);
		return L_3;
	}
}
// System.Void System.ComponentModel.DesignOnlyAttribute::.ctor(System.Boolean)
extern "C"  void DesignOnlyAttribute__ctor_m3444989896 (DesignOnlyAttribute_t2394309572 * __this, bool ___design_only0, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		bool L_0 = ___design_only0;
		__this->set_design_only_0(L_0);
		return;
	}
}
// System.Void System.ComponentModel.DesignOnlyAttribute::.cctor()
extern Il2CppClass* DesignOnlyAttribute_t2394309572_il2cpp_TypeInfo_var;
extern const uint32_t DesignOnlyAttribute__cctor_m1220910124_MetadataUsageId;
extern "C"  void DesignOnlyAttribute__cctor_m1220910124 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DesignOnlyAttribute__cctor_m1220910124_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		DesignOnlyAttribute_t2394309572 * L_0 = (DesignOnlyAttribute_t2394309572 *)il2cpp_codegen_object_new(DesignOnlyAttribute_t2394309572_il2cpp_TypeInfo_var);
		DesignOnlyAttribute__ctor_m3444989896(L_0, (bool)0, /*hidden argument*/NULL);
		((DesignOnlyAttribute_t2394309572_StaticFields*)DesignOnlyAttribute_t2394309572_il2cpp_TypeInfo_var->static_fields)->set_Default_1(L_0);
		DesignOnlyAttribute_t2394309572 * L_1 = (DesignOnlyAttribute_t2394309572 *)il2cpp_codegen_object_new(DesignOnlyAttribute_t2394309572_il2cpp_TypeInfo_var);
		DesignOnlyAttribute__ctor_m3444989896(L_1, (bool)0, /*hidden argument*/NULL);
		((DesignOnlyAttribute_t2394309572_StaticFields*)DesignOnlyAttribute_t2394309572_il2cpp_TypeInfo_var->static_fields)->set_No_2(L_1);
		DesignOnlyAttribute_t2394309572 * L_2 = (DesignOnlyAttribute_t2394309572 *)il2cpp_codegen_object_new(DesignOnlyAttribute_t2394309572_il2cpp_TypeInfo_var);
		DesignOnlyAttribute__ctor_m3444989896(L_2, (bool)1, /*hidden argument*/NULL);
		((DesignOnlyAttribute_t2394309572_StaticFields*)DesignOnlyAttribute_t2394309572_il2cpp_TypeInfo_var->static_fields)->set_Yes_3(L_2);
		return;
	}
}
// System.Boolean System.ComponentModel.DesignOnlyAttribute::get_IsDesignOnly()
extern "C"  bool DesignOnlyAttribute_get_IsDesignOnly_m1559833632 (DesignOnlyAttribute_t2394309572 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_design_only_0();
		return L_0;
	}
}
// System.Boolean System.ComponentModel.DesignOnlyAttribute::Equals(System.Object)
extern Il2CppClass* DesignOnlyAttribute_t2394309572_il2cpp_TypeInfo_var;
extern const uint32_t DesignOnlyAttribute_Equals_m283299064_MetadataUsageId;
extern "C"  bool DesignOnlyAttribute_Equals_m283299064 (DesignOnlyAttribute_t2394309572 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DesignOnlyAttribute_Equals_m283299064_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___obj0;
		if (((DesignOnlyAttribute_t2394309572 *)IsInstSealed(L_0, DesignOnlyAttribute_t2394309572_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = ___obj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(DesignOnlyAttribute_t2394309572 *)__this))))
		{
			goto IL_0016;
		}
	}
	{
		return (bool)1;
	}

IL_0016:
	{
		Il2CppObject * L_2 = ___obj0;
		NullCheck(((DesignOnlyAttribute_t2394309572 *)CastclassSealed(L_2, DesignOnlyAttribute_t2394309572_il2cpp_TypeInfo_var)));
		bool L_3 = DesignOnlyAttribute_get_IsDesignOnly_m1559833632(((DesignOnlyAttribute_t2394309572 *)CastclassSealed(L_2, DesignOnlyAttribute_t2394309572_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		bool L_4 = __this->get_design_only_0();
		return (bool)((((int32_t)L_3) == ((int32_t)L_4))? 1 : 0);
	}
}
// System.Int32 System.ComponentModel.DesignOnlyAttribute::GetHashCode()
extern "C"  int32_t DesignOnlyAttribute_GetHashCode_m2438392964 (DesignOnlyAttribute_t2394309572 * __this, const MethodInfo* method)
{
	{
		bool* L_0 = __this->get_address_of_design_only_0();
		int32_t L_1 = Boolean_GetHashCode_m1894638460(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void System.ComponentModel.DesignTimeVisibleAttribute::.ctor(System.Boolean)
extern "C"  void DesignTimeVisibleAttribute__ctor_m1286837001 (DesignTimeVisibleAttribute_t2120749151 * __this, bool ___visible0, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		bool L_0 = ___visible0;
		__this->set_visible_0(L_0);
		return;
	}
}
// System.Void System.ComponentModel.DesignTimeVisibleAttribute::.cctor()
extern Il2CppClass* DesignTimeVisibleAttribute_t2120749151_il2cpp_TypeInfo_var;
extern const uint32_t DesignTimeVisibleAttribute__cctor_m2091212705_MetadataUsageId;
extern "C"  void DesignTimeVisibleAttribute__cctor_m2091212705 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DesignTimeVisibleAttribute__cctor_m2091212705_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		DesignTimeVisibleAttribute_t2120749151 * L_0 = (DesignTimeVisibleAttribute_t2120749151 *)il2cpp_codegen_object_new(DesignTimeVisibleAttribute_t2120749151_il2cpp_TypeInfo_var);
		DesignTimeVisibleAttribute__ctor_m1286837001(L_0, (bool)1, /*hidden argument*/NULL);
		((DesignTimeVisibleAttribute_t2120749151_StaticFields*)DesignTimeVisibleAttribute_t2120749151_il2cpp_TypeInfo_var->static_fields)->set_Default_1(L_0);
		DesignTimeVisibleAttribute_t2120749151 * L_1 = (DesignTimeVisibleAttribute_t2120749151 *)il2cpp_codegen_object_new(DesignTimeVisibleAttribute_t2120749151_il2cpp_TypeInfo_var);
		DesignTimeVisibleAttribute__ctor_m1286837001(L_1, (bool)0, /*hidden argument*/NULL);
		((DesignTimeVisibleAttribute_t2120749151_StaticFields*)DesignTimeVisibleAttribute_t2120749151_il2cpp_TypeInfo_var->static_fields)->set_No_2(L_1);
		DesignTimeVisibleAttribute_t2120749151 * L_2 = (DesignTimeVisibleAttribute_t2120749151 *)il2cpp_codegen_object_new(DesignTimeVisibleAttribute_t2120749151_il2cpp_TypeInfo_var);
		DesignTimeVisibleAttribute__ctor_m1286837001(L_2, (bool)1, /*hidden argument*/NULL);
		((DesignTimeVisibleAttribute_t2120749151_StaticFields*)DesignTimeVisibleAttribute_t2120749151_il2cpp_TypeInfo_var->static_fields)->set_Yes_3(L_2);
		return;
	}
}
// System.Boolean System.ComponentModel.DesignTimeVisibleAttribute::get_Visible()
extern "C"  bool DesignTimeVisibleAttribute_get_Visible_m4200839797 (DesignTimeVisibleAttribute_t2120749151 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_visible_0();
		return L_0;
	}
}
// System.Boolean System.ComponentModel.DesignTimeVisibleAttribute::Equals(System.Object)
extern Il2CppClass* DesignTimeVisibleAttribute_t2120749151_il2cpp_TypeInfo_var;
extern const uint32_t DesignTimeVisibleAttribute_Equals_m1143449067_MetadataUsageId;
extern "C"  bool DesignTimeVisibleAttribute_Equals_m1143449067 (DesignTimeVisibleAttribute_t2120749151 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DesignTimeVisibleAttribute_Equals_m1143449067_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___obj0;
		if (((DesignTimeVisibleAttribute_t2120749151 *)IsInstSealed(L_0, DesignTimeVisibleAttribute_t2120749151_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = ___obj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(DesignTimeVisibleAttribute_t2120749151 *)__this))))
		{
			goto IL_0016;
		}
	}
	{
		return (bool)1;
	}

IL_0016:
	{
		Il2CppObject * L_2 = ___obj0;
		NullCheck(((DesignTimeVisibleAttribute_t2120749151 *)CastclassSealed(L_2, DesignTimeVisibleAttribute_t2120749151_il2cpp_TypeInfo_var)));
		bool L_3 = DesignTimeVisibleAttribute_get_Visible_m4200839797(((DesignTimeVisibleAttribute_t2120749151 *)CastclassSealed(L_2, DesignTimeVisibleAttribute_t2120749151_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		bool L_4 = __this->get_visible_0();
		return (bool)((((int32_t)L_3) == ((int32_t)L_4))? 1 : 0);
	}
}
// System.Int32 System.ComponentModel.DesignTimeVisibleAttribute::GetHashCode()
extern "C"  int32_t DesignTimeVisibleAttribute_GetHashCode_m2160740589 (DesignTimeVisibleAttribute_t2120749151 * __this, const MethodInfo* method)
{
	{
		bool* L_0 = __this->get_address_of_visible_0();
		int32_t L_1 = Boolean_GetHashCode_m1894638460(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void System.ComponentModel.EditorAttribute::.ctor(System.String,System.String)
extern "C"  void EditorAttribute__ctor_m3488466142 (EditorAttribute_t3559776959 * __this, String_t* ___typeName0, String_t* ___baseTypeName1, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___typeName0;
		__this->set_name_0(L_0);
		String_t* L_1 = ___baseTypeName1;
		__this->set_basename_1(L_1);
		return;
	}
}
// System.String System.ComponentModel.EditorAttribute::get_EditorBaseTypeName()
extern "C"  String_t* EditorAttribute_get_EditorBaseTypeName_m2498230921 (EditorAttribute_t3559776959 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_basename_1();
		return L_0;
	}
}
// System.String System.ComponentModel.EditorAttribute::get_EditorTypeName()
extern "C"  String_t* EditorAttribute_get_EditorTypeName_m961831462 (EditorAttribute_t3559776959 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_name_0();
		return L_0;
	}
}
// System.Boolean System.ComponentModel.EditorAttribute::Equals(System.Object)
extern Il2CppClass* EditorAttribute_t3559776959_il2cpp_TypeInfo_var;
extern const uint32_t EditorAttribute_Equals_m1798416147_MetadataUsageId;
extern "C"  bool EditorAttribute_Equals_m1798416147 (EditorAttribute_t3559776959 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EditorAttribute_Equals_m1798416147_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B5_0 = 0;
	{
		Il2CppObject * L_0 = ___obj0;
		if (((EditorAttribute_t3559776959 *)IsInstSealed(L_0, EditorAttribute_t3559776959_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = ___obj0;
		NullCheck(((EditorAttribute_t3559776959 *)CastclassSealed(L_1, EditorAttribute_t3559776959_il2cpp_TypeInfo_var)));
		String_t* L_2 = EditorAttribute_get_EditorBaseTypeName_m2498230921(((EditorAttribute_t3559776959 *)CastclassSealed(L_1, EditorAttribute_t3559776959_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		String_t* L_3 = __this->get_basename_1();
		NullCheck(L_2);
		bool L_4 = String_Equals_m2633592423(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0040;
		}
	}
	{
		Il2CppObject * L_5 = ___obj0;
		NullCheck(((EditorAttribute_t3559776959 *)CastclassSealed(L_5, EditorAttribute_t3559776959_il2cpp_TypeInfo_var)));
		String_t* L_6 = EditorAttribute_get_EditorTypeName_m961831462(((EditorAttribute_t3559776959 *)CastclassSealed(L_5, EditorAttribute_t3559776959_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		String_t* L_7 = __this->get_name_0();
		NullCheck(L_6);
		bool L_8 = String_Equals_m2633592423(L_6, L_7, /*hidden argument*/NULL);
		G_B5_0 = ((int32_t)(L_8));
		goto IL_0041;
	}

IL_0040:
	{
		G_B5_0 = 0;
	}

IL_0041:
	{
		return (bool)G_B5_0;
	}
}
// System.Int32 System.ComponentModel.EditorAttribute::GetHashCode()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t EditorAttribute_GetHashCode_m2621716125_MetadataUsageId;
extern "C"  int32_t EditorAttribute_GetHashCode_m2621716125 (EditorAttribute_t3559776959 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EditorAttribute_GetHashCode_m2621716125_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = __this->get_name_0();
		String_t* L_1 = __this->get_basename_1();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m2596409543(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = String_GetHashCode_m931956593(L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void System.ComponentModel.EditorBrowsableAttribute::.ctor(System.ComponentModel.EditorBrowsableState)
extern "C"  void EditorBrowsableAttribute__ctor_m2635501285 (EditorBrowsableAttribute_t1050682502 * __this, int32_t ___state0, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___state0;
		__this->set_state_0(L_0);
		return;
	}
}
// System.ComponentModel.EditorBrowsableState System.ComponentModel.EditorBrowsableAttribute::get_State()
extern "C"  int32_t EditorBrowsableAttribute_get_State_m3897062724 (EditorBrowsableAttribute_t1050682502 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_state_0();
		return L_0;
	}
}
// System.Boolean System.ComponentModel.EditorBrowsableAttribute::Equals(System.Object)
extern Il2CppClass* EditorBrowsableAttribute_t1050682502_il2cpp_TypeInfo_var;
extern const uint32_t EditorBrowsableAttribute_Equals_m345895380_MetadataUsageId;
extern "C"  bool EditorBrowsableAttribute_Equals_m345895380 (EditorBrowsableAttribute_t1050682502 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EditorBrowsableAttribute_Equals_m345895380_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___obj0;
		if (((EditorBrowsableAttribute_t1050682502 *)IsInstSealed(L_0, EditorBrowsableAttribute_t1050682502_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = ___obj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(EditorBrowsableAttribute_t1050682502 *)__this))))
		{
			goto IL_0016;
		}
	}
	{
		return (bool)1;
	}

IL_0016:
	{
		Il2CppObject * L_2 = ___obj0;
		NullCheck(((EditorBrowsableAttribute_t1050682502 *)CastclassSealed(L_2, EditorBrowsableAttribute_t1050682502_il2cpp_TypeInfo_var)));
		int32_t L_3 = EditorBrowsableAttribute_get_State_m3897062724(((EditorBrowsableAttribute_t1050682502 *)CastclassSealed(L_2, EditorBrowsableAttribute_t1050682502_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		int32_t L_4 = __this->get_state_0();
		return (bool)((((int32_t)L_3) == ((int32_t)L_4))? 1 : 0);
	}
}
// System.Int32 System.ComponentModel.EditorBrowsableAttribute::GetHashCode()
extern Il2CppClass* EditorBrowsableState_t373498655_il2cpp_TypeInfo_var;
extern const uint32_t EditorBrowsableAttribute_GetHashCode_m169893972_MetadataUsageId;
extern "C"  int32_t EditorBrowsableAttribute_GetHashCode_m169893972 (EditorBrowsableAttribute_t1050682502 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EditorBrowsableAttribute_GetHashCode_m169893972_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_state_0();
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(EditorBrowsableState_t373498655_il2cpp_TypeInfo_var, &L_1);
		NullCheck((Enum_t2459695545 *)L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Enum::GetHashCode() */, (Enum_t2459695545 *)L_2);
		return L_3;
	}
}
// System.Void System.ComponentModel.EventHandlerList::.ctor()
extern "C"  void EventHandlerList__ctor_m125822993 (EventHandlerList_t1298116880 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Delegate System.ComponentModel.EventHandlerList::get_Item(System.Object)
extern "C"  Delegate_t3022476291 * EventHandlerList_get_Item_m533246000 (EventHandlerList_t1298116880 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	ListEntry_t935815304 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___key0;
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		Delegate_t3022476291 * L_1 = __this->get_null_entry_1();
		return L_1;
	}

IL_000d:
	{
		Il2CppObject * L_2 = ___key0;
		ListEntry_t935815304 * L_3 = EventHandlerList_FindEntry_m2326093420(__this, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		ListEntry_t935815304 * L_4 = V_0;
		if (!L_4)
		{
			goto IL_0022;
		}
	}
	{
		ListEntry_t935815304 * L_5 = V_0;
		NullCheck(L_5);
		Delegate_t3022476291 * L_6 = L_5->get_value_1();
		return L_6;
	}

IL_0022:
	{
		return (Delegate_t3022476291 *)NULL;
	}
}
// System.Void System.ComponentModel.EventHandlerList::AddHandler(System.Object,System.Delegate)
extern Il2CppClass* ListEntry_t935815304_il2cpp_TypeInfo_var;
extern const uint32_t EventHandlerList_AddHandler_m3545570610_MetadataUsageId;
extern "C"  void EventHandlerList_AddHandler_m3545570610 (EventHandlerList_t1298116880 * __this, Il2CppObject * ___key0, Delegate_t3022476291 * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EventHandlerList_AddHandler_m3545570610_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ListEntry_t935815304 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___key0;
		if (L_0)
		{
			goto IL_0019;
		}
	}
	{
		Delegate_t3022476291 * L_1 = __this->get_null_entry_1();
		Delegate_t3022476291 * L_2 = ___value1;
		Delegate_t3022476291 * L_3 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		__this->set_null_entry_1(L_3);
		return;
	}

IL_0019:
	{
		Il2CppObject * L_4 = ___key0;
		ListEntry_t935815304 * L_5 = EventHandlerList_FindEntry_m2326093420(__this, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		ListEntry_t935815304 * L_6 = V_0;
		if (L_6)
		{
			goto IL_004e;
		}
	}
	{
		ListEntry_t935815304 * L_7 = (ListEntry_t935815304 *)il2cpp_codegen_object_new(ListEntry_t935815304_il2cpp_TypeInfo_var);
		ListEntry__ctor_m2506112277(L_7, /*hidden argument*/NULL);
		V_0 = L_7;
		ListEntry_t935815304 * L_8 = V_0;
		Il2CppObject * L_9 = ___key0;
		NullCheck(L_8);
		L_8->set_key_0(L_9);
		ListEntry_t935815304 * L_10 = V_0;
		NullCheck(L_10);
		L_10->set_value_1((Delegate_t3022476291 *)NULL);
		ListEntry_t935815304 * L_11 = V_0;
		ListEntry_t935815304 * L_12 = __this->get_entries_0();
		NullCheck(L_11);
		L_11->set_next_2(L_12);
		ListEntry_t935815304 * L_13 = V_0;
		__this->set_entries_0(L_13);
	}

IL_004e:
	{
		ListEntry_t935815304 * L_14 = V_0;
		ListEntry_t935815304 * L_15 = V_0;
		NullCheck(L_15);
		Delegate_t3022476291 * L_16 = L_15->get_value_1();
		Delegate_t3022476291 * L_17 = ___value1;
		Delegate_t3022476291 * L_18 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_16, L_17, /*hidden argument*/NULL);
		NullCheck(L_14);
		L_14->set_value_1(L_18);
		return;
	}
}
// System.Void System.ComponentModel.EventHandlerList::RemoveHandler(System.Object,System.Delegate)
extern "C"  void EventHandlerList_RemoveHandler_m874666245 (EventHandlerList_t1298116880 * __this, Il2CppObject * ___key0, Delegate_t3022476291 * ___value1, const MethodInfo* method)
{
	ListEntry_t935815304 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___key0;
		if (L_0)
		{
			goto IL_0019;
		}
	}
	{
		Delegate_t3022476291 * L_1 = __this->get_null_entry_1();
		Delegate_t3022476291 * L_2 = ___value1;
		Delegate_t3022476291 * L_3 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		__this->set_null_entry_1(L_3);
		return;
	}

IL_0019:
	{
		Il2CppObject * L_4 = ___key0;
		ListEntry_t935815304 * L_5 = EventHandlerList_FindEntry_m2326093420(__this, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		ListEntry_t935815304 * L_6 = V_0;
		if (L_6)
		{
			goto IL_0028;
		}
	}
	{
		return;
	}

IL_0028:
	{
		ListEntry_t935815304 * L_7 = V_0;
		ListEntry_t935815304 * L_8 = V_0;
		NullCheck(L_8);
		Delegate_t3022476291 * L_9 = L_8->get_value_1();
		Delegate_t3022476291 * L_10 = ___value1;
		Delegate_t3022476291 * L_11 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		NullCheck(L_7);
		L_7->set_value_1(L_11);
		return;
	}
}
// System.Void System.ComponentModel.EventHandlerList::Dispose()
extern "C"  void EventHandlerList_Dispose_m813102448 (EventHandlerList_t1298116880 * __this, const MethodInfo* method)
{
	{
		__this->set_entries_0((ListEntry_t935815304 *)NULL);
		return;
	}
}
// System.ComponentModel.ListEntry System.ComponentModel.EventHandlerList::FindEntry(System.Object)
extern "C"  ListEntry_t935815304 * EventHandlerList_FindEntry_m2326093420 (EventHandlerList_t1298116880 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	ListEntry_t935815304 * V_0 = NULL;
	{
		ListEntry_t935815304 * L_0 = __this->get_entries_0();
		V_0 = L_0;
		goto IL_0021;
	}

IL_000c:
	{
		ListEntry_t935815304 * L_1 = V_0;
		NullCheck(L_1);
		Il2CppObject * L_2 = L_1->get_key_0();
		Il2CppObject * L_3 = ___key0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_2) == ((Il2CppObject*)(Il2CppObject *)L_3))))
		{
			goto IL_001a;
		}
	}
	{
		ListEntry_t935815304 * L_4 = V_0;
		return L_4;
	}

IL_001a:
	{
		ListEntry_t935815304 * L_5 = V_0;
		NullCheck(L_5);
		ListEntry_t935815304 * L_6 = L_5->get_next_2();
		V_0 = L_6;
	}

IL_0021:
	{
		ListEntry_t935815304 * L_7 = V_0;
		if (L_7)
		{
			goto IL_000c;
		}
	}
	{
		return (ListEntry_t935815304 *)NULL;
	}
}
// System.Void System.ComponentModel.InvalidEnumArgumentException::.ctor()
extern "C"  void InvalidEnumArgumentException__ctor_m2412961433 (InvalidEnumArgumentException_t3709744516 * __this, const MethodInfo* method)
{
	{
		InvalidEnumArgumentException__ctor_m3345705171(__this, (String_t*)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.ComponentModel.InvalidEnumArgumentException::.ctor(System.String)
extern "C"  void InvalidEnumArgumentException__ctor_m3345705171 (InvalidEnumArgumentException_t3709744516 * __this, String_t* ___message0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		ArgumentException__ctor_m3739475201(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.ComponentModel.InvalidEnumArgumentException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void InvalidEnumArgumentException__ctor_m1156511804 (InvalidEnumArgumentException_t3709744516 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method)
{
	{
		SerializationInfo_t228987430 * L_0 = ___info0;
		StreamingContext_t1417235061  L_1 = ___context1;
		ArgumentException__ctor_m158205422(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.ComponentModel.ListBindableAttribute::.ctor(System.Boolean)
extern "C"  void ListBindableAttribute__ctor_m200641937 (ListBindableAttribute_t1092011273 * __this, bool ___listBindable0, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		bool L_0 = ___listBindable0;
		__this->set_bindable_3(L_0);
		return;
	}
}
// System.Void System.ComponentModel.ListBindableAttribute::.cctor()
extern Il2CppClass* ListBindableAttribute_t1092011273_il2cpp_TypeInfo_var;
extern const uint32_t ListBindableAttribute__cctor_m867040729_MetadataUsageId;
extern "C"  void ListBindableAttribute__cctor_m867040729 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ListBindableAttribute__cctor_m867040729_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ListBindableAttribute_t1092011273 * L_0 = (ListBindableAttribute_t1092011273 *)il2cpp_codegen_object_new(ListBindableAttribute_t1092011273_il2cpp_TypeInfo_var);
		ListBindableAttribute__ctor_m200641937(L_0, (bool)1, /*hidden argument*/NULL);
		((ListBindableAttribute_t1092011273_StaticFields*)ListBindableAttribute_t1092011273_il2cpp_TypeInfo_var->static_fields)->set_Default_0(L_0);
		ListBindableAttribute_t1092011273 * L_1 = (ListBindableAttribute_t1092011273 *)il2cpp_codegen_object_new(ListBindableAttribute_t1092011273_il2cpp_TypeInfo_var);
		ListBindableAttribute__ctor_m200641937(L_1, (bool)0, /*hidden argument*/NULL);
		((ListBindableAttribute_t1092011273_StaticFields*)ListBindableAttribute_t1092011273_il2cpp_TypeInfo_var->static_fields)->set_No_1(L_1);
		ListBindableAttribute_t1092011273 * L_2 = (ListBindableAttribute_t1092011273 *)il2cpp_codegen_object_new(ListBindableAttribute_t1092011273_il2cpp_TypeInfo_var);
		ListBindableAttribute__ctor_m200641937(L_2, (bool)1, /*hidden argument*/NULL);
		((ListBindableAttribute_t1092011273_StaticFields*)ListBindableAttribute_t1092011273_il2cpp_TypeInfo_var->static_fields)->set_Yes_2(L_2);
		return;
	}
}
// System.Boolean System.ComponentModel.ListBindableAttribute::Equals(System.Object)
extern Il2CppClass* ListBindableAttribute_t1092011273_il2cpp_TypeInfo_var;
extern const uint32_t ListBindableAttribute_Equals_m2614154631_MetadataUsageId;
extern "C"  bool ListBindableAttribute_Equals_m2614154631 (ListBindableAttribute_t1092011273 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ListBindableAttribute_Equals_m2614154631_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		Il2CppObject * L_0 = ___obj0;
		if (((ListBindableAttribute_t1092011273 *)IsInstSealed(L_0, ListBindableAttribute_t1092011273_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = ___obj0;
		NullCheck(((ListBindableAttribute_t1092011273 *)CastclassSealed(L_1, ListBindableAttribute_t1092011273_il2cpp_TypeInfo_var)));
		bool L_2 = ListBindableAttribute_get_ListBindable_m851508878(((ListBindableAttribute_t1092011273 *)CastclassSealed(L_1, ListBindableAttribute_t1092011273_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		V_0 = L_2;
		bool L_3 = __this->get_bindable_3();
		bool L_4 = Boolean_Equals_m294106711((&V_0), L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Int32 System.ComponentModel.ListBindableAttribute::GetHashCode()
extern "C"  int32_t ListBindableAttribute_GetHashCode_m1006781969 (ListBindableAttribute_t1092011273 * __this, const MethodInfo* method)
{
	{
		bool* L_0 = __this->get_address_of_bindable_3();
		int32_t L_1 = Boolean_GetHashCode_m1894638460(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.ComponentModel.ListBindableAttribute::get_ListBindable()
extern "C"  bool ListBindableAttribute_get_ListBindable_m851508878 (ListBindableAttribute_t1092011273 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_bindable_3();
		return L_0;
	}
}
// System.Void System.ComponentModel.ListChangedEventArgs::.ctor(System.ComponentModel.ListChangedType,System.Int32,System.Int32)
extern Il2CppClass* EventArgs_t3289624707_il2cpp_TypeInfo_var;
extern const uint32_t ListChangedEventArgs__ctor_m2492526393_MetadataUsageId;
extern "C"  void ListChangedEventArgs__ctor_m2492526393 (ListChangedEventArgs_t3132270315 * __this, int32_t ___listChangedType0, int32_t ___newIndex1, int32_t ___oldIndex2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ListChangedEventArgs__ctor_m2492526393_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t3289624707_il2cpp_TypeInfo_var);
		EventArgs__ctor_m3696060910(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___listChangedType0;
		__this->set_changedType_1(L_0);
		int32_t L_1 = ___newIndex1;
		__this->set_newIndex_3(L_1);
		int32_t L_2 = ___oldIndex2;
		__this->set_oldIndex_2(L_2);
		return;
	}
}
// System.Void System.ComponentModel.ListChangedEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void ListChangedEventHandler__ctor_m1609476863 (ListChangedEventHandler_t2276411942 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.ComponentModel.ListChangedEventHandler::Invoke(System.Object,System.ComponentModel.ListChangedEventArgs)
extern "C"  void ListChangedEventHandler_Invoke_m2113751515 (ListChangedEventHandler_t2276411942 * __this, Il2CppObject * ___sender0, ListChangedEventArgs_t3132270315 * ___e1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ListChangedEventHandler_Invoke_m2113751515((ListChangedEventHandler_t2276411942 *)__this->get_prev_9(),___sender0, ___e1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___sender0, ListChangedEventArgs_t3132270315 * ___e1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___sender0, ___e1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___sender0, ListChangedEventArgs_t3132270315 * ___e1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___sender0, ___e1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, ListChangedEventArgs_t3132270315 * ___e1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___sender0, ___e1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.ComponentModel.ListChangedEventHandler::BeginInvoke(System.Object,System.ComponentModel.ListChangedEventArgs,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ListChangedEventHandler_BeginInvoke_m865365000 (ListChangedEventHandler_t2276411942 * __this, Il2CppObject * ___sender0, ListChangedEventArgs_t3132270315 * ___e1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___sender0;
	__d_args[1] = ___e1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void System.ComponentModel.ListChangedEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void ListChangedEventHandler_EndInvoke_m2224075705 (ListChangedEventHandler_t2276411942 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.ComponentModel.ListEntry::.ctor()
extern "C"  void ListEntry__ctor_m2506112277 (ListEntry_t935815304 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.ComponentModel.MarshalByValueComponent::.ctor()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t MarshalByValueComponent__ctor_m1750517712_MetadataUsageId;
extern "C"  void MarshalByValueComponent__ctor_m1750517712 (MarshalByValueComponent_t3997823175 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MarshalByValueComponent__ctor_m1750517712_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m2551263788(L_0, /*hidden argument*/NULL);
		__this->set_disposedEvent_1(L_0);
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.ComponentModel.MarshalByValueComponent::Dispose()
extern "C"  void MarshalByValueComponent_Dispose_m4161119315 (MarshalByValueComponent_t3997823175 * __this, const MethodInfo* method)
{
	{
		VirtActionInvoker1< bool >::Invoke(6 /* System.Void System.ComponentModel.MarshalByValueComponent::Dispose(System.Boolean) */, __this, (bool)1);
		GC_SuppressFinalize_m953228702(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.ComponentModel.MarshalByValueComponent::Dispose(System.Boolean)
extern "C"  void MarshalByValueComponent_Dispose_m4268598196 (MarshalByValueComponent_t3997823175 * __this, bool ___disposing0, const MethodInfo* method)
{
	{
		bool L_0 = ___disposing0;
		if (!L_0)
		{
			goto IL_0006;
		}
	}

IL_0006:
	{
		return;
	}
}
// System.Void System.ComponentModel.MarshalByValueComponent::Finalize()
extern "C"  void MarshalByValueComponent_Finalize_m3972633420 (MarshalByValueComponent_t3997823175 * __this, const MethodInfo* method)
{
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		VirtActionInvoker1< bool >::Invoke(6 /* System.Void System.ComponentModel.MarshalByValueComponent::Dispose(System.Boolean) */, __this, (bool)0);
		IL2CPP_LEAVE(0x13, FINALLY_000c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_000c;
	}

FINALLY_000c:
	{ // begin finally (depth: 1)
		Object_Finalize_m4087144328(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(12)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(12)
	{
		IL2CPP_JUMP_TBL(0x13, IL_0013)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0013:
	{
		return;
	}
}
// System.Object System.ComponentModel.MarshalByValueComponent::GetService(System.Type)
extern Il2CppClass* IServiceProvider_t2397848447_il2cpp_TypeInfo_var;
extern const uint32_t MarshalByValueComponent_GetService_m816478501_MetadataUsageId;
extern "C"  Il2CppObject * MarshalByValueComponent_GetService_m816478501 (MarshalByValueComponent_t3997823175 * __this, Type_t * ___service0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MarshalByValueComponent_GetService_m816478501_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get_mySite_0();
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		Il2CppObject * L_1 = __this->get_mySite_0();
		Type_t * L_2 = ___service0;
		NullCheck(L_1);
		Il2CppObject * L_3 = InterfaceFuncInvoker1< Il2CppObject *, Type_t * >::Invoke(0 /* System.Object System.IServiceProvider::GetService(System.Type) */, IServiceProvider_t2397848447_il2cpp_TypeInfo_var, L_1, L_2);
		return L_3;
	}

IL_0018:
	{
		return NULL;
	}
}
// System.ComponentModel.ISite System.ComponentModel.MarshalByValueComponent::get_Site()
extern "C"  Il2CppObject * MarshalByValueComponent_get_Site_m137619012 (MarshalByValueComponent_t3997823175 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_mySite_0();
		return L_0;
	}
}
// System.Void System.ComponentModel.MarshalByValueComponent::set_Site(System.ComponentModel.ISite)
extern "C"  void MarshalByValueComponent_set_Site_m1013938265 (MarshalByValueComponent_t3997823175 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_mySite_0(L_0);
		return;
	}
}
// System.String System.ComponentModel.MarshalByValueComponent::ToString()
extern Il2CppClass* ISite_t1774720436_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2410691315;
extern const uint32_t MarshalByValueComponent_ToString_m868476799_MetadataUsageId;
extern "C"  String_t* MarshalByValueComponent_ToString_m868476799 (MarshalByValueComponent_t3997823175 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MarshalByValueComponent_ToString_m868476799_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get_mySite_0();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		Type_t * L_1 = Object_GetType_m191970594(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Type::ToString() */, L_1);
		return L_2;
	}

IL_0017:
	{
		Il2CppObject * L_3 = __this->get_mySite_0();
		NullCheck(L_3);
		String_t* L_4 = InterfaceFuncInvoker0< String_t* >::Invoke(1 /* System.String System.ComponentModel.ISite::get_Name() */, ISite_t1774720436_il2cpp_TypeInfo_var, L_3);
		Type_t * L_5 = Object_GetType_m191970594(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Type::ToString() */, L_5);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Format_m1811873526(NULL /*static, unused*/, _stringLiteral2410691315, L_4, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.String System.ComponentModel.MemberDescriptor::get_Name()
extern "C"  String_t* MemberDescriptor_get_Name_m1191562727 (MemberDescriptor_t3749827553 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_name_0();
		return L_0;
	}
}
// System.Int32 System.ComponentModel.MemberDescriptor::GetHashCode()
extern "C"  int32_t MemberDescriptor_GetHashCode_m573108599 (MemberDescriptor_t3749827553 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Object_GetHashCode_m1715190285(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean System.ComponentModel.MemberDescriptor::Equals(System.Object)
extern Il2CppClass* MemberDescriptor_t3749827553_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t MemberDescriptor_Equals_m2946310549_MetadataUsageId;
extern "C"  bool MemberDescriptor_Equals_m2946310549 (MemberDescriptor_t3749827553 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MemberDescriptor_Equals_m2946310549_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MemberDescriptor_t3749827553 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___obj0;
		V_0 = ((MemberDescriptor_t3749827553 *)IsInstClass(L_0, MemberDescriptor_t3749827553_il2cpp_TypeInfo_var));
		MemberDescriptor_t3749827553 * L_1 = V_0;
		if (L_1)
		{
			goto IL_000f;
		}
	}
	{
		return (bool)0;
	}

IL_000f:
	{
		MemberDescriptor_t3749827553 * L_2 = V_0;
		NullCheck(L_2);
		String_t* L_3 = L_2->get_name_0();
		String_t* L_4 = __this->get_name_0();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void System.ComponentModel.MergablePropertyAttribute::.ctor(System.Boolean)
extern "C"  void MergablePropertyAttribute__ctor_m34960188 (MergablePropertyAttribute_t1597820506 * __this, bool ___allowMerge0, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		bool L_0 = ___allowMerge0;
		__this->set_mergable_0(L_0);
		return;
	}
}
// System.Void System.ComponentModel.MergablePropertyAttribute::.cctor()
extern Il2CppClass* MergablePropertyAttribute_t1597820506_il2cpp_TypeInfo_var;
extern const uint32_t MergablePropertyAttribute__cctor_m45331836_MetadataUsageId;
extern "C"  void MergablePropertyAttribute__cctor_m45331836 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MergablePropertyAttribute__cctor_m45331836_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MergablePropertyAttribute_t1597820506 * L_0 = (MergablePropertyAttribute_t1597820506 *)il2cpp_codegen_object_new(MergablePropertyAttribute_t1597820506_il2cpp_TypeInfo_var);
		MergablePropertyAttribute__ctor_m34960188(L_0, (bool)1, /*hidden argument*/NULL);
		((MergablePropertyAttribute_t1597820506_StaticFields*)MergablePropertyAttribute_t1597820506_il2cpp_TypeInfo_var->static_fields)->set_Default_1(L_0);
		MergablePropertyAttribute_t1597820506 * L_1 = (MergablePropertyAttribute_t1597820506 *)il2cpp_codegen_object_new(MergablePropertyAttribute_t1597820506_il2cpp_TypeInfo_var);
		MergablePropertyAttribute__ctor_m34960188(L_1, (bool)0, /*hidden argument*/NULL);
		((MergablePropertyAttribute_t1597820506_StaticFields*)MergablePropertyAttribute_t1597820506_il2cpp_TypeInfo_var->static_fields)->set_No_2(L_1);
		MergablePropertyAttribute_t1597820506 * L_2 = (MergablePropertyAttribute_t1597820506 *)il2cpp_codegen_object_new(MergablePropertyAttribute_t1597820506_il2cpp_TypeInfo_var);
		MergablePropertyAttribute__ctor_m34960188(L_2, (bool)1, /*hidden argument*/NULL);
		((MergablePropertyAttribute_t1597820506_StaticFields*)MergablePropertyAttribute_t1597820506_il2cpp_TypeInfo_var->static_fields)->set_Yes_3(L_2);
		return;
	}
}
// System.Boolean System.ComponentModel.MergablePropertyAttribute::get_AllowMerge()
extern "C"  bool MergablePropertyAttribute_get_AllowMerge_m1023204627 (MergablePropertyAttribute_t1597820506 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_mergable_0();
		return L_0;
	}
}
// System.Boolean System.ComponentModel.MergablePropertyAttribute::Equals(System.Object)
extern Il2CppClass* MergablePropertyAttribute_t1597820506_il2cpp_TypeInfo_var;
extern const uint32_t MergablePropertyAttribute_Equals_m3543619420_MetadataUsageId;
extern "C"  bool MergablePropertyAttribute_Equals_m3543619420 (MergablePropertyAttribute_t1597820506 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MergablePropertyAttribute_Equals_m3543619420_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___obj0;
		if (((MergablePropertyAttribute_t1597820506 *)IsInstSealed(L_0, MergablePropertyAttribute_t1597820506_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = ___obj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(MergablePropertyAttribute_t1597820506 *)__this))))
		{
			goto IL_0016;
		}
	}
	{
		return (bool)1;
	}

IL_0016:
	{
		Il2CppObject * L_2 = ___obj0;
		NullCheck(((MergablePropertyAttribute_t1597820506 *)CastclassSealed(L_2, MergablePropertyAttribute_t1597820506_il2cpp_TypeInfo_var)));
		bool L_3 = MergablePropertyAttribute_get_AllowMerge_m1023204627(((MergablePropertyAttribute_t1597820506 *)CastclassSealed(L_2, MergablePropertyAttribute_t1597820506_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		bool L_4 = __this->get_mergable_0();
		return (bool)((((int32_t)L_3) == ((int32_t)L_4))? 1 : 0);
	}
}
// System.Int32 System.ComponentModel.MergablePropertyAttribute::GetHashCode()
extern "C"  int32_t MergablePropertyAttribute_GetHashCode_m2482033448 (MergablePropertyAttribute_t1597820506 * __this, const MethodInfo* method)
{
	{
		bool* L_0 = __this->get_address_of_mergable_0();
		int32_t L_1 = Boolean_GetHashCode_m1894638460(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void System.ComponentModel.PropertyChangedEventArgs::.ctor(System.String)
extern Il2CppClass* EventArgs_t3289624707_il2cpp_TypeInfo_var;
extern const uint32_t PropertyChangedEventArgs__ctor_m3472995223_MetadataUsageId;
extern "C"  void PropertyChangedEventArgs__ctor_m3472995223 (PropertyChangedEventArgs_t1689446432 * __this, String_t* ___name0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PropertyChangedEventArgs__ctor_m3472995223_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t3289624707_il2cpp_TypeInfo_var);
		EventArgs__ctor_m3696060910(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name0;
		__this->set_propertyName_1(L_0);
		return;
	}
}
// System.String System.ComponentModel.PropertyChangedEventArgs::get_PropertyName()
extern "C"  String_t* PropertyChangedEventArgs_get_PropertyName_m3631147821 (PropertyChangedEventArgs_t1689446432 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_propertyName_1();
		return L_0;
	}
}
// System.Void System.ComponentModel.PropertyChangedEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void PropertyChangedEventHandler__ctor_m3283190984 (PropertyChangedEventHandler_t3042952059 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.ComponentModel.PropertyChangedEventHandler::Invoke(System.Object,System.ComponentModel.PropertyChangedEventArgs)
extern "C"  void PropertyChangedEventHandler_Invoke_m3733684827 (PropertyChangedEventHandler_t3042952059 * __this, Il2CppObject * ___sender0, PropertyChangedEventArgs_t1689446432 * ___e1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		PropertyChangedEventHandler_Invoke_m3733684827((PropertyChangedEventHandler_t3042952059 *)__this->get_prev_9(),___sender0, ___e1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___sender0, PropertyChangedEventArgs_t1689446432 * ___e1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___sender0, ___e1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___sender0, PropertyChangedEventArgs_t1689446432 * ___e1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___sender0, ___e1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, PropertyChangedEventArgs_t1689446432 * ___e1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___sender0, ___e1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.ComponentModel.PropertyChangedEventHandler::BeginInvoke(System.Object,System.ComponentModel.PropertyChangedEventArgs,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * PropertyChangedEventHandler_BeginInvoke_m3150012958 (PropertyChangedEventHandler_t3042952059 * __this, Il2CppObject * ___sender0, PropertyChangedEventArgs_t1689446432 * ___e1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___sender0;
	__d_args[1] = ___e1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void System.ComponentModel.PropertyChangedEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void PropertyChangedEventHandler_EndInvoke_m109495738 (PropertyChangedEventHandler_t3042952059 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Boolean System.ComponentModel.PropertyDescriptor::Equals(System.Object)
extern Il2CppClass* PropertyDescriptor_t4250402154_il2cpp_TypeInfo_var;
extern const uint32_t PropertyDescriptor_Equals_m4086017300_MetadataUsageId;
extern "C"  bool PropertyDescriptor_Equals_m4086017300 (PropertyDescriptor_t4250402154 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PropertyDescriptor_Equals_m4086017300_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PropertyDescriptor_t4250402154 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___obj0;
		bool L_1 = MemberDescriptor_Equals_m2946310549(__this, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_000e;
		}
	}
	{
		return (bool)0;
	}

IL_000e:
	{
		Il2CppObject * L_2 = ___obj0;
		V_0 = ((PropertyDescriptor_t4250402154 *)IsInstClass(L_2, PropertyDescriptor_t4250402154_il2cpp_TypeInfo_var));
		PropertyDescriptor_t4250402154 * L_3 = V_0;
		if (L_3)
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		PropertyDescriptor_t4250402154 * L_4 = V_0;
		NullCheck(L_4);
		Type_t * L_5 = VirtFuncInvoker0< Type_t * >::Invoke(5 /* System.Type System.ComponentModel.PropertyDescriptor::get_PropertyType() */, L_4);
		Type_t * L_6 = VirtFuncInvoker0< Type_t * >::Invoke(5 /* System.Type System.ComponentModel.PropertyDescriptor::get_PropertyType() */, __this);
		return (bool)((((Il2CppObject*)(Type_t *)L_5) == ((Il2CppObject*)(Type_t *)L_6))? 1 : 0);
	}
}
// System.Int32 System.ComponentModel.PropertyDescriptor::GetHashCode()
extern "C"  int32_t PropertyDescriptor_GetHashCode_m3169280040 (PropertyDescriptor_t4250402154 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = MemberDescriptor_GetHashCode_m573108599(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void System.ComponentModel.PropertyDescriptorCollection::.ctor(System.ComponentModel.PropertyDescriptor[])
extern Il2CppClass* ArrayList_t4252133567_il2cpp_TypeInfo_var;
extern const uint32_t PropertyDescriptorCollection__ctor_m2487959314_MetadataUsageId;
extern "C"  void PropertyDescriptorCollection__ctor_m2487959314 (PropertyDescriptorCollection_t3166009492 * __this, PropertyDescriptorU5BU5D_t2049367471* ___properties0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PropertyDescriptorCollection__ctor_m2487959314_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		ArrayList_t4252133567 * L_0 = (ArrayList_t4252133567 *)il2cpp_codegen_object_new(ArrayList_t4252133567_il2cpp_TypeInfo_var);
		ArrayList__ctor_m4012174379(L_0, /*hidden argument*/NULL);
		__this->set_properties_1(L_0);
		PropertyDescriptorU5BU5D_t2049367471* L_1 = ___properties0;
		if (L_1)
		{
			goto IL_0018;
		}
	}
	{
		return;
	}

IL_0018:
	{
		ArrayList_t4252133567 * L_2 = __this->get_properties_1();
		PropertyDescriptorU5BU5D_t2049367471* L_3 = ___properties0;
		NullCheck(L_2);
		VirtActionInvoker1< Il2CppObject * >::Invoke(44 /* System.Void System.Collections.ArrayList::AddRange(System.Collections.ICollection) */, L_2, (Il2CppObject *)(Il2CppObject *)L_3);
		return;
	}
}
// System.Void System.ComponentModel.PropertyDescriptorCollection::.ctor(System.ComponentModel.PropertyDescriptor[],System.Boolean)
extern "C"  void PropertyDescriptorCollection__ctor_m3463703649 (PropertyDescriptorCollection_t3166009492 * __this, PropertyDescriptorU5BU5D_t2049367471* ___properties0, bool ___readOnly1, const MethodInfo* method)
{
	{
		PropertyDescriptorU5BU5D_t2049367471* L_0 = ___properties0;
		PropertyDescriptorCollection__ctor_m2487959314(__this, L_0, /*hidden argument*/NULL);
		bool L_1 = ___readOnly1;
		__this->set_readOnly_2(L_1);
		return;
	}
}
// System.Void System.ComponentModel.PropertyDescriptorCollection::.cctor()
extern Il2CppClass* PropertyDescriptorCollection_t3166009492_il2cpp_TypeInfo_var;
extern const uint32_t PropertyDescriptorCollection__cctor_m299292564_MetadataUsageId;
extern "C"  void PropertyDescriptorCollection__cctor_m299292564 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PropertyDescriptorCollection__cctor_m299292564_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PropertyDescriptorCollection_t3166009492 * L_0 = (PropertyDescriptorCollection_t3166009492 *)il2cpp_codegen_object_new(PropertyDescriptorCollection_t3166009492_il2cpp_TypeInfo_var);
		PropertyDescriptorCollection__ctor_m3463703649(L_0, (PropertyDescriptorU5BU5D_t2049367471*)(PropertyDescriptorU5BU5D_t2049367471*)NULL, (bool)1, /*hidden argument*/NULL);
		((PropertyDescriptorCollection_t3166009492_StaticFields*)PropertyDescriptorCollection_t3166009492_il2cpp_TypeInfo_var->static_fields)->set_Empty_0(L_0);
		return;
	}
}
// System.Int32 System.ComponentModel.PropertyDescriptorCollection::System.Collections.IList.Add(System.Object)
extern Il2CppClass* PropertyDescriptor_t4250402154_il2cpp_TypeInfo_var;
extern const uint32_t PropertyDescriptorCollection_System_Collections_IList_Add_m2545037033_MetadataUsageId;
extern "C"  int32_t PropertyDescriptorCollection_System_Collections_IList_Add_m2545037033 (PropertyDescriptorCollection_t3166009492 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PropertyDescriptorCollection_System_Collections_IList_Add_m2545037033_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___value0;
		int32_t L_1 = PropertyDescriptorCollection_Add_m1444404967(__this, ((PropertyDescriptor_t4250402154 *)CastclassClass(L_0, PropertyDescriptor_t4250402154_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void System.ComponentModel.PropertyDescriptorCollection::System.Collections.IDictionary.Add(System.Object,System.Object)
extern Il2CppClass* PropertyDescriptor_t4250402154_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1803325615;
extern const uint32_t PropertyDescriptorCollection_System_Collections_IDictionary_Add_m1349483251_MetadataUsageId;
extern "C"  void PropertyDescriptorCollection_System_Collections_IDictionary_Add_m1349483251 (PropertyDescriptorCollection_t3166009492 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PropertyDescriptorCollection_System_Collections_IDictionary_Add_m1349483251_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___value1;
		if (((PropertyDescriptor_t4250402154 *)IsInstClass(L_0, PropertyDescriptor_t4250402154_il2cpp_TypeInfo_var)))
		{
			goto IL_0016;
		}
	}
	{
		ArgumentException_t3259014390 * L_1 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_1, _stringLiteral1803325615, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		Il2CppObject * L_2 = ___value1;
		PropertyDescriptorCollection_Add_m1444404967(__this, ((PropertyDescriptor_t4250402154 *)CastclassClass(L_2, PropertyDescriptor_t4250402154_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.ComponentModel.PropertyDescriptorCollection::System.Collections.IList.Clear()
extern "C"  void PropertyDescriptorCollection_System_Collections_IList_Clear_m1762837225 (PropertyDescriptorCollection_t3166009492 * __this, const MethodInfo* method)
{
	{
		PropertyDescriptorCollection_Clear_m381761220(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.ComponentModel.PropertyDescriptorCollection::System.Collections.IDictionary.Clear()
extern "C"  void PropertyDescriptorCollection_System_Collections_IDictionary_Clear_m3687224111 (PropertyDescriptorCollection_t3166009492 * __this, const MethodInfo* method)
{
	{
		PropertyDescriptorCollection_Clear_m381761220(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean System.ComponentModel.PropertyDescriptorCollection::System.Collections.IList.Contains(System.Object)
extern Il2CppClass* PropertyDescriptor_t4250402154_il2cpp_TypeInfo_var;
extern const uint32_t PropertyDescriptorCollection_System_Collections_IList_Contains_m3192600021_MetadataUsageId;
extern "C"  bool PropertyDescriptorCollection_System_Collections_IList_Contains_m3192600021 (PropertyDescriptorCollection_t3166009492 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PropertyDescriptorCollection_System_Collections_IList_Contains_m3192600021_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___value0;
		bool L_1 = PropertyDescriptorCollection_Contains_m1112441547(__this, ((PropertyDescriptor_t4250402154 *)CastclassClass(L_0, PropertyDescriptor_t4250402154_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.ComponentModel.PropertyDescriptorCollection::System.Collections.IDictionary.Contains(System.Object)
extern Il2CppClass* PropertyDescriptor_t4250402154_il2cpp_TypeInfo_var;
extern const uint32_t PropertyDescriptorCollection_System_Collections_IDictionary_Contains_m2607003711_MetadataUsageId;
extern "C"  bool PropertyDescriptorCollection_System_Collections_IDictionary_Contains_m2607003711 (PropertyDescriptorCollection_t3166009492 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PropertyDescriptorCollection_System_Collections_IDictionary_Contains_m2607003711_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___value0;
		bool L_1 = PropertyDescriptorCollection_Contains_m1112441547(__this, ((PropertyDescriptor_t4250402154 *)CastclassClass(L_0, PropertyDescriptor_t4250402154_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Collections.IEnumerator System.ComponentModel.PropertyDescriptorCollection::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * PropertyDescriptorCollection_System_Collections_IEnumerable_GetEnumerator_m1233007566 (PropertyDescriptorCollection_t3166009492 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = VirtFuncInvoker0< Il2CppObject * >::Invoke(31 /* System.Collections.IEnumerator System.ComponentModel.PropertyDescriptorCollection::GetEnumerator() */, __this);
		return L_0;
	}
}
// System.Collections.IDictionaryEnumerator System.ComponentModel.PropertyDescriptorCollection::System.Collections.IDictionary.GetEnumerator()
extern Il2CppClass* NotImplementedException_t2785117854_il2cpp_TypeInfo_var;
extern const uint32_t PropertyDescriptorCollection_System_Collections_IDictionary_GetEnumerator_m4065956348_MetadataUsageId;
extern "C"  Il2CppObject * PropertyDescriptorCollection_System_Collections_IDictionary_GetEnumerator_m4065956348 (PropertyDescriptorCollection_t3166009492 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PropertyDescriptorCollection_System_Collections_IDictionary_GetEnumerator_m4065956348_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t2785117854 * L_0 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m808189835(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Int32 System.ComponentModel.PropertyDescriptorCollection::System.Collections.IList.IndexOf(System.Object)
extern Il2CppClass* PropertyDescriptor_t4250402154_il2cpp_TypeInfo_var;
extern const uint32_t PropertyDescriptorCollection_System_Collections_IList_IndexOf_m3055093751_MetadataUsageId;
extern "C"  int32_t PropertyDescriptorCollection_System_Collections_IList_IndexOf_m3055093751 (PropertyDescriptorCollection_t3166009492 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PropertyDescriptorCollection_System_Collections_IList_IndexOf_m3055093751_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___value0;
		int32_t L_1 = PropertyDescriptorCollection_IndexOf_m2940684757(__this, ((PropertyDescriptor_t4250402154 *)CastclassClass(L_0, PropertyDescriptor_t4250402154_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void System.ComponentModel.PropertyDescriptorCollection::System.Collections.IList.Insert(System.Int32,System.Object)
extern Il2CppClass* PropertyDescriptor_t4250402154_il2cpp_TypeInfo_var;
extern const uint32_t PropertyDescriptorCollection_System_Collections_IList_Insert_m2641798284_MetadataUsageId;
extern "C"  void PropertyDescriptorCollection_System_Collections_IList_Insert_m2641798284 (PropertyDescriptorCollection_t3166009492 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PropertyDescriptorCollection_System_Collections_IList_Insert_m2641798284_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		Il2CppObject * L_1 = ___value1;
		PropertyDescriptorCollection_Insert_m2741675098(__this, L_0, ((PropertyDescriptor_t4250402154 *)CastclassClass(L_1, PropertyDescriptor_t4250402154_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.ComponentModel.PropertyDescriptorCollection::System.Collections.IDictionary.Remove(System.Object)
extern Il2CppClass* PropertyDescriptor_t4250402154_il2cpp_TypeInfo_var;
extern const uint32_t PropertyDescriptorCollection_System_Collections_IDictionary_Remove_m3593805206_MetadataUsageId;
extern "C"  void PropertyDescriptorCollection_System_Collections_IDictionary_Remove_m3593805206 (PropertyDescriptorCollection_t3166009492 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PropertyDescriptorCollection_System_Collections_IDictionary_Remove_m3593805206_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___value0;
		PropertyDescriptorCollection_Remove_m15982774(__this, ((PropertyDescriptor_t4250402154 *)CastclassClass(L_0, PropertyDescriptor_t4250402154_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.ComponentModel.PropertyDescriptorCollection::System.Collections.IList.Remove(System.Object)
extern Il2CppClass* PropertyDescriptor_t4250402154_il2cpp_TypeInfo_var;
extern const uint32_t PropertyDescriptorCollection_System_Collections_IList_Remove_m269098166_MetadataUsageId;
extern "C"  void PropertyDescriptorCollection_System_Collections_IList_Remove_m269098166 (PropertyDescriptorCollection_t3166009492 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PropertyDescriptorCollection_System_Collections_IList_Remove_m269098166_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___value0;
		PropertyDescriptorCollection_Remove_m15982774(__this, ((PropertyDescriptor_t4250402154 *)CastclassClass(L_0, PropertyDescriptor_t4250402154_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.ComponentModel.PropertyDescriptorCollection::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void PropertyDescriptorCollection_System_Collections_IList_RemoveAt_m4243254102 (PropertyDescriptorCollection_t3166009492 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		PropertyDescriptorCollection_RemoveAt_m3024774059(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean System.ComponentModel.PropertyDescriptorCollection::System.Collections.IList.get_IsFixedSize()
extern "C"  bool PropertyDescriptorCollection_System_Collections_IList_get_IsFixedSize_m2356655192 (PropertyDescriptorCollection_t3166009492 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_readOnly_2();
		return L_0;
	}
}
// System.Boolean System.ComponentModel.PropertyDescriptorCollection::System.Collections.IList.get_IsReadOnly()
extern "C"  bool PropertyDescriptorCollection_System_Collections_IList_get_IsReadOnly_m3720213229 (PropertyDescriptorCollection_t3166009492 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_readOnly_2();
		return L_0;
	}
}
// System.Boolean System.ComponentModel.PropertyDescriptorCollection::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool PropertyDescriptorCollection_System_Collections_ICollection_get_IsSynchronized_m3263756009 (PropertyDescriptorCollection_t3166009492 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Int32 System.ComponentModel.PropertyDescriptorCollection::System.Collections.ICollection.get_Count()
extern "C"  int32_t PropertyDescriptorCollection_System_Collections_ICollection_get_Count_m417392944 (PropertyDescriptorCollection_t3166009492 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = PropertyDescriptorCollection_get_Count_m3465428969(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Object System.ComponentModel.PropertyDescriptorCollection::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * PropertyDescriptorCollection_System_Collections_ICollection_get_SyncRoot_m2933402441 (PropertyDescriptorCollection_t3166009492 * __this, const MethodInfo* method)
{
	{
		return NULL;
	}
}
// System.Collections.ICollection System.ComponentModel.PropertyDescriptorCollection::System.Collections.IDictionary.get_Keys()
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* PropertyDescriptor_t4250402154_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t PropertyDescriptorCollection_System_Collections_IDictionary_get_Keys_m3800665023_MetadataUsageId;
extern "C"  Il2CppObject * PropertyDescriptorCollection_System_Collections_IDictionary_get_Keys_m3800665023 (PropertyDescriptorCollection_t3166009492 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PropertyDescriptorCollection_System_Collections_IDictionary_get_Keys_m3800665023_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringU5BU5D_t1642385972* V_0 = NULL;
	int32_t V_1 = 0;
	PropertyDescriptor_t4250402154 * V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	Il2CppObject * V_4 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		ArrayList_t4252133567 * L_0 = __this->get_properties_1();
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_0);
		V_0 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)L_1));
		V_1 = 0;
		ArrayList_t4252133567 * L_2 = __this->get_properties_1();
		NullCheck(L_2);
		Il2CppObject * L_3 = VirtFuncInvoker0< Il2CppObject * >::Invoke(43 /* System.Collections.IEnumerator System.Collections.ArrayList::GetEnumerator() */, L_2);
		V_3 = L_3;
	}

IL_001f:
	try
	{ // begin try (depth: 1)
		{
			goto IL_003d;
		}

IL_0024:
		{
			Il2CppObject * L_4 = V_3;
			NullCheck(L_4);
			Il2CppObject * L_5 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_4);
			V_2 = ((PropertyDescriptor_t4250402154 *)CastclassClass(L_5, PropertyDescriptor_t4250402154_il2cpp_TypeInfo_var));
			StringU5BU5D_t1642385972* L_6 = V_0;
			int32_t L_7 = V_1;
			int32_t L_8 = L_7;
			V_1 = ((int32_t)((int32_t)L_8+(int32_t)1));
			PropertyDescriptor_t4250402154 * L_9 = V_2;
			NullCheck(L_9);
			String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String System.ComponentModel.MemberDescriptor::get_Name() */, L_9);
			NullCheck(L_6);
			ArrayElementTypeCheck (L_6, L_10);
			(L_6)->SetAt(static_cast<il2cpp_array_size_t>(L_8), (String_t*)L_10);
		}

IL_003d:
		{
			Il2CppObject * L_11 = V_3;
			NullCheck(L_11);
			bool L_12 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_11);
			if (L_12)
			{
				goto IL_0024;
			}
		}

IL_0048:
		{
			IL2CPP_LEAVE(0x62, FINALLY_004d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_004d;
	}

FINALLY_004d:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_13 = V_3;
			V_4 = ((Il2CppObject *)IsInst(L_13, IDisposable_t2427283555_il2cpp_TypeInfo_var));
			Il2CppObject * L_14 = V_4;
			if (L_14)
			{
				goto IL_005a;
			}
		}

IL_0059:
		{
			IL2CPP_END_FINALLY(77)
		}

IL_005a:
		{
			Il2CppObject * L_15 = V_4;
			NullCheck(L_15);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_15);
			IL2CPP_END_FINALLY(77)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(77)
	{
		IL2CPP_JUMP_TBL(0x62, IL_0062)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0062:
	{
		StringU5BU5D_t1642385972* L_16 = V_0;
		return (Il2CppObject *)L_16;
	}
}
// System.Collections.ICollection System.ComponentModel.PropertyDescriptorCollection::System.Collections.IDictionary.get_Values()
extern Il2CppClass* ICollection_t91669223_il2cpp_TypeInfo_var;
extern const uint32_t PropertyDescriptorCollection_System_Collections_IDictionary_get_Values_m1397318223_MetadataUsageId;
extern "C"  Il2CppObject * PropertyDescriptorCollection_System_Collections_IDictionary_get_Values_m1397318223 (PropertyDescriptorCollection_t3166009492 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PropertyDescriptorCollection_System_Collections_IDictionary_get_Values_m1397318223_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ArrayList_t4252133567 * L_0 = __this->get_properties_1();
		NullCheck(L_0);
		Il2CppObject * L_1 = VirtFuncInvoker0< Il2CppObject * >::Invoke(49 /* System.Object System.Collections.ArrayList::Clone() */, L_0);
		return ((Il2CppObject *)Castclass(L_1, ICollection_t91669223_il2cpp_TypeInfo_var));
	}
}
// System.Object System.ComponentModel.PropertyDescriptorCollection::System.Collections.IDictionary.get_Item(System.Object)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t PropertyDescriptorCollection_System_Collections_IDictionary_get_Item_m3701519105_MetadataUsageId;
extern "C"  Il2CppObject * PropertyDescriptorCollection_System_Collections_IDictionary_get_Item_m3701519105 (PropertyDescriptorCollection_t3166009492 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PropertyDescriptorCollection_System_Collections_IDictionary_get_Item_m3701519105_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___key0;
		if (((String_t*)IsInstSealed(L_0, String_t_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return NULL;
	}

IL_000d:
	{
		Il2CppObject * L_1 = ___key0;
		PropertyDescriptor_t4250402154 * L_2 = VirtFuncInvoker1< PropertyDescriptor_t4250402154 *, String_t* >::Invoke(34 /* System.ComponentModel.PropertyDescriptor System.ComponentModel.PropertyDescriptorCollection::get_Item(System.String) */, __this, ((String_t*)CastclassSealed(L_1, String_t_il2cpp_TypeInfo_var)));
		return L_2;
	}
}
// System.Void System.ComponentModel.PropertyDescriptorCollection::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* PropertyDescriptor_t4250402154_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern const uint32_t PropertyDescriptorCollection_System_Collections_IDictionary_set_Item_m2873835906_MetadataUsageId;
extern "C"  void PropertyDescriptorCollection_System_Collections_IDictionary_set_Item_m2873835906 (PropertyDescriptorCollection_t3166009492 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PropertyDescriptorCollection_System_Collections_IDictionary_set_Item_m2873835906_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		bool L_0 = __this->get_readOnly_2();
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		NotSupportedException_t1793819818 * L_1 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Il2CppObject * L_2 = ___key0;
		if (!((String_t*)IsInstSealed(L_2, String_t_il2cpp_TypeInfo_var)))
		{
			goto IL_0027;
		}
	}
	{
		Il2CppObject * L_3 = ___value1;
		if (((PropertyDescriptor_t4250402154 *)IsInstClass(L_3, PropertyDescriptor_t4250402154_il2cpp_TypeInfo_var)))
		{
			goto IL_002d;
		}
	}

IL_0027:
	{
		ArgumentException_t3259014390 * L_4 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2105824819(L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_002d:
	{
		ArrayList_t4252133567 * L_5 = __this->get_properties_1();
		Il2CppObject * L_6 = ___value1;
		NullCheck(L_5);
		int32_t L_7 = VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(33 /* System.Int32 System.Collections.ArrayList::IndexOf(System.Object) */, L_5, L_6);
		V_0 = L_7;
		int32_t L_8 = V_0;
		if ((!(((uint32_t)L_8) == ((uint32_t)(-1)))))
		{
			goto IL_0053;
		}
	}
	{
		Il2CppObject * L_9 = ___value1;
		PropertyDescriptorCollection_Add_m1444404967(__this, ((PropertyDescriptor_t4250402154 *)CastclassClass(L_9, PropertyDescriptor_t4250402154_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		goto IL_0060;
	}

IL_0053:
	{
		ArrayList_t4252133567 * L_10 = __this->get_properties_1();
		int32_t L_11 = V_0;
		Il2CppObject * L_12 = ___value1;
		NullCheck(L_10);
		VirtActionInvoker2< int32_t, Il2CppObject * >::Invoke(22 /* System.Void System.Collections.ArrayList::set_Item(System.Int32,System.Object) */, L_10, L_11, L_12);
	}

IL_0060:
	{
		return;
	}
}
// System.Object System.ComponentModel.PropertyDescriptorCollection::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * PropertyDescriptorCollection_System_Collections_IList_get_Item_m1694300544 (PropertyDescriptorCollection_t3166009492 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		ArrayList_t4252133567 * L_0 = __this->get_properties_1();
		int32_t L_1 = ___index0;
		NullCheck(L_0);
		Il2CppObject * L_2 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_0, L_1);
		return L_2;
	}
}
// System.Void System.ComponentModel.PropertyDescriptorCollection::System.Collections.IList.set_Item(System.Int32,System.Object)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t PropertyDescriptorCollection_System_Collections_IList_set_Item_m2158609863_MetadataUsageId;
extern "C"  void PropertyDescriptorCollection_System_Collections_IList_set_Item_m2158609863 (PropertyDescriptorCollection_t3166009492 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PropertyDescriptorCollection_System_Collections_IList_set_Item_m2158609863_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_readOnly_2();
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		NotSupportedException_t1793819818 * L_1 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		ArrayList_t4252133567 * L_2 = __this->get_properties_1();
		int32_t L_3 = ___index0;
		Il2CppObject * L_4 = ___value1;
		NullCheck(L_2);
		VirtActionInvoker2< int32_t, Il2CppObject * >::Invoke(22 /* System.Void System.Collections.ArrayList::set_Item(System.Int32,System.Object) */, L_2, L_3, L_4);
		return;
	}
}
// System.Int32 System.ComponentModel.PropertyDescriptorCollection::Add(System.ComponentModel.PropertyDescriptor)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t PropertyDescriptorCollection_Add_m1444404967_MetadataUsageId;
extern "C"  int32_t PropertyDescriptorCollection_Add_m1444404967 (PropertyDescriptorCollection_t3166009492 * __this, PropertyDescriptor_t4250402154 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PropertyDescriptorCollection_Add_m1444404967_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_readOnly_2();
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		NotSupportedException_t1793819818 * L_1 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		ArrayList_t4252133567 * L_2 = __this->get_properties_1();
		PropertyDescriptor_t4250402154 * L_3 = ___value0;
		NullCheck(L_2);
		VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_2, L_3);
		ArrayList_t4252133567 * L_4 = __this->get_properties_1();
		NullCheck(L_4);
		int32_t L_5 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_4);
		return ((int32_t)((int32_t)L_5-(int32_t)1));
	}
}
// System.Void System.ComponentModel.PropertyDescriptorCollection::Clear()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t PropertyDescriptorCollection_Clear_m381761220_MetadataUsageId;
extern "C"  void PropertyDescriptorCollection_Clear_m381761220 (PropertyDescriptorCollection_t3166009492 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PropertyDescriptorCollection_Clear_m381761220_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_readOnly_2();
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		NotSupportedException_t1793819818 * L_1 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		ArrayList_t4252133567 * L_2 = __this->get_properties_1();
		NullCheck(L_2);
		VirtActionInvoker0::Invoke(31 /* System.Void System.Collections.ArrayList::Clear() */, L_2);
		return;
	}
}
// System.Boolean System.ComponentModel.PropertyDescriptorCollection::Contains(System.ComponentModel.PropertyDescriptor)
extern "C"  bool PropertyDescriptorCollection_Contains_m1112441547 (PropertyDescriptorCollection_t3166009492 * __this, PropertyDescriptor_t4250402154 * ___value0, const MethodInfo* method)
{
	{
		ArrayList_t4252133567 * L_0 = __this->get_properties_1();
		PropertyDescriptor_t4250402154 * L_1 = ___value0;
		NullCheck(L_0);
		bool L_2 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(32 /* System.Boolean System.Collections.ArrayList::Contains(System.Object) */, L_0, L_1);
		return L_2;
	}
}
// System.Void System.ComponentModel.PropertyDescriptorCollection::CopyTo(System.Array,System.Int32)
extern "C"  void PropertyDescriptorCollection_CopyTo_m4285022054 (PropertyDescriptorCollection_t3166009492 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		ArrayList_t4252133567 * L_0 = __this->get_properties_1();
		Il2CppArray * L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck(L_0);
		VirtActionInvoker2< Il2CppArray *, int32_t >::Invoke(41 /* System.Void System.Collections.ArrayList::CopyTo(System.Array,System.Int32) */, L_0, L_1, L_2);
		return;
	}
}
// System.ComponentModel.PropertyDescriptor System.ComponentModel.PropertyDescriptorCollection::Find(System.String,System.Boolean)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppClass* PropertyDescriptor_t4250402154_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2328218955;
extern const uint32_t PropertyDescriptorCollection_Find_m3781931283_MetadataUsageId;
extern "C"  PropertyDescriptor_t4250402154 * PropertyDescriptorCollection_Find_m3781931283 (PropertyDescriptorCollection_t3166009492 * __this, String_t* ___name0, bool ___ignoreCase1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PropertyDescriptorCollection_Find_m3781931283_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	PropertyDescriptor_t4250402154 * V_1 = NULL;
	{
		String_t* L_0 = ___name0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, _stringLiteral2328218955, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		V_0 = 0;
		goto IL_0061;
	}

IL_0018:
	{
		ArrayList_t4252133567 * L_2 = __this->get_properties_1();
		int32_t L_3 = V_0;
		NullCheck(L_2);
		Il2CppObject * L_4 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_2, L_3);
		V_1 = ((PropertyDescriptor_t4250402154 *)CastclassClass(L_4, PropertyDescriptor_t4250402154_il2cpp_TypeInfo_var));
		bool L_5 = ___ignoreCase1;
		if (!L_5)
		{
			goto IL_0049;
		}
	}
	{
		String_t* L_6 = ___name0;
		PropertyDescriptor_t4250402154 * L_7 = V_1;
		NullCheck(L_7);
		String_t* L_8 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String System.ComponentModel.MemberDescriptor::get_Name() */, L_7);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		int32_t L_9 = String_Compare_m3288062998(NULL /*static, unused*/, L_6, L_8, 5, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_0044;
		}
	}
	{
		PropertyDescriptor_t4250402154 * L_10 = V_1;
		return L_10;
	}

IL_0044:
	{
		goto IL_005d;
	}

IL_0049:
	{
		String_t* L_11 = ___name0;
		PropertyDescriptor_t4250402154 * L_12 = V_1;
		NullCheck(L_12);
		String_t* L_13 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String System.ComponentModel.MemberDescriptor::get_Name() */, L_12);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		int32_t L_14 = String_Compare_m3288062998(NULL /*static, unused*/, L_11, L_13, 4, /*hidden argument*/NULL);
		if (L_14)
		{
			goto IL_005d;
		}
	}
	{
		PropertyDescriptor_t4250402154 * L_15 = V_1;
		return L_15;
	}

IL_005d:
	{
		int32_t L_16 = V_0;
		V_0 = ((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0061:
	{
		int32_t L_17 = V_0;
		ArrayList_t4252133567 * L_18 = __this->get_properties_1();
		NullCheck(L_18);
		int32_t L_19 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_18);
		if ((((int32_t)L_17) < ((int32_t)L_19)))
		{
			goto IL_0018;
		}
	}
	{
		return (PropertyDescriptor_t4250402154 *)NULL;
	}
}
// System.Collections.IEnumerator System.ComponentModel.PropertyDescriptorCollection::GetEnumerator()
extern "C"  Il2CppObject * PropertyDescriptorCollection_GetEnumerator_m4151070489 (PropertyDescriptorCollection_t3166009492 * __this, const MethodInfo* method)
{
	{
		ArrayList_t4252133567 * L_0 = __this->get_properties_1();
		NullCheck(L_0);
		Il2CppObject * L_1 = VirtFuncInvoker0< Il2CppObject * >::Invoke(43 /* System.Collections.IEnumerator System.Collections.ArrayList::GetEnumerator() */, L_0);
		return L_1;
	}
}
// System.Int32 System.ComponentModel.PropertyDescriptorCollection::IndexOf(System.ComponentModel.PropertyDescriptor)
extern "C"  int32_t PropertyDescriptorCollection_IndexOf_m2940684757 (PropertyDescriptorCollection_t3166009492 * __this, PropertyDescriptor_t4250402154 * ___value0, const MethodInfo* method)
{
	{
		ArrayList_t4252133567 * L_0 = __this->get_properties_1();
		PropertyDescriptor_t4250402154 * L_1 = ___value0;
		NullCheck(L_0);
		int32_t L_2 = VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(33 /* System.Int32 System.Collections.ArrayList::IndexOf(System.Object) */, L_0, L_1);
		return L_2;
	}
}
// System.Void System.ComponentModel.PropertyDescriptorCollection::Insert(System.Int32,System.ComponentModel.PropertyDescriptor)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t PropertyDescriptorCollection_Insert_m2741675098_MetadataUsageId;
extern "C"  void PropertyDescriptorCollection_Insert_m2741675098 (PropertyDescriptorCollection_t3166009492 * __this, int32_t ___index0, PropertyDescriptor_t4250402154 * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PropertyDescriptorCollection_Insert_m2741675098_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_readOnly_2();
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		NotSupportedException_t1793819818 * L_1 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		ArrayList_t4252133567 * L_2 = __this->get_properties_1();
		int32_t L_3 = ___index0;
		PropertyDescriptor_t4250402154 * L_4 = ___value1;
		NullCheck(L_2);
		VirtActionInvoker2< int32_t, Il2CppObject * >::Invoke(36 /* System.Void System.Collections.ArrayList::Insert(System.Int32,System.Object) */, L_2, L_3, L_4);
		return;
	}
}
// System.Void System.ComponentModel.PropertyDescriptorCollection::Remove(System.ComponentModel.PropertyDescriptor)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t PropertyDescriptorCollection_Remove_m15982774_MetadataUsageId;
extern "C"  void PropertyDescriptorCollection_Remove_m15982774 (PropertyDescriptorCollection_t3166009492 * __this, PropertyDescriptor_t4250402154 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PropertyDescriptorCollection_Remove_m15982774_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_readOnly_2();
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		NotSupportedException_t1793819818 * L_1 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		ArrayList_t4252133567 * L_2 = __this->get_properties_1();
		PropertyDescriptor_t4250402154 * L_3 = ___value0;
		NullCheck(L_2);
		VirtActionInvoker1< Il2CppObject * >::Invoke(38 /* System.Void System.Collections.ArrayList::Remove(System.Object) */, L_2, L_3);
		return;
	}
}
// System.Void System.ComponentModel.PropertyDescriptorCollection::RemoveAt(System.Int32)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t PropertyDescriptorCollection_RemoveAt_m3024774059_MetadataUsageId;
extern "C"  void PropertyDescriptorCollection_RemoveAt_m3024774059 (PropertyDescriptorCollection_t3166009492 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PropertyDescriptorCollection_RemoveAt_m3024774059_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_readOnly_2();
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		NotSupportedException_t1793819818 * L_1 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		ArrayList_t4252133567 * L_2 = __this->get_properties_1();
		int32_t L_3 = ___index0;
		NullCheck(L_2);
		VirtActionInvoker1< int32_t >::Invoke(39 /* System.Void System.Collections.ArrayList::RemoveAt(System.Int32) */, L_2, L_3);
		return;
	}
}
// System.Int32 System.ComponentModel.PropertyDescriptorCollection::get_Count()
extern "C"  int32_t PropertyDescriptorCollection_get_Count_m3465428969 (PropertyDescriptorCollection_t3166009492 * __this, const MethodInfo* method)
{
	{
		ArrayList_t4252133567 * L_0 = __this->get_properties_1();
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_0);
		return L_1;
	}
}
// System.ComponentModel.PropertyDescriptor System.ComponentModel.PropertyDescriptorCollection::get_Item(System.String)
extern "C"  PropertyDescriptor_t4250402154 * PropertyDescriptorCollection_get_Item_m3128918101 (PropertyDescriptorCollection_t3166009492 * __this, String_t* ___s0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___s0;
		PropertyDescriptor_t4250402154 * L_1 = VirtFuncInvoker2< PropertyDescriptor_t4250402154 *, String_t*, bool >::Invoke(30 /* System.ComponentModel.PropertyDescriptor System.ComponentModel.PropertyDescriptorCollection::Find(System.String,System.Boolean) */, __this, L_0, (bool)0);
		return L_1;
	}
}
// System.Void System.ComponentModel.ReadOnlyAttribute::.ctor(System.Boolean)
extern "C"  void ReadOnlyAttribute__ctor_m4293741434 (ReadOnlyAttribute_t4102148880 * __this, bool ___read_only0, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		bool L_0 = ___read_only0;
		__this->set_read_only_0(L_0);
		return;
	}
}
// System.Void System.ComponentModel.ReadOnlyAttribute::.cctor()
extern Il2CppClass* ReadOnlyAttribute_t4102148880_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyAttribute__cctor_m2518358734_MetadataUsageId;
extern "C"  void ReadOnlyAttribute__cctor_m2518358734 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyAttribute__cctor_m2518358734_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ReadOnlyAttribute_t4102148880 * L_0 = (ReadOnlyAttribute_t4102148880 *)il2cpp_codegen_object_new(ReadOnlyAttribute_t4102148880_il2cpp_TypeInfo_var);
		ReadOnlyAttribute__ctor_m4293741434(L_0, (bool)0, /*hidden argument*/NULL);
		((ReadOnlyAttribute_t4102148880_StaticFields*)ReadOnlyAttribute_t4102148880_il2cpp_TypeInfo_var->static_fields)->set_No_1(L_0);
		ReadOnlyAttribute_t4102148880 * L_1 = (ReadOnlyAttribute_t4102148880 *)il2cpp_codegen_object_new(ReadOnlyAttribute_t4102148880_il2cpp_TypeInfo_var);
		ReadOnlyAttribute__ctor_m4293741434(L_1, (bool)1, /*hidden argument*/NULL);
		((ReadOnlyAttribute_t4102148880_StaticFields*)ReadOnlyAttribute_t4102148880_il2cpp_TypeInfo_var->static_fields)->set_Yes_2(L_1);
		ReadOnlyAttribute_t4102148880 * L_2 = (ReadOnlyAttribute_t4102148880 *)il2cpp_codegen_object_new(ReadOnlyAttribute_t4102148880_il2cpp_TypeInfo_var);
		ReadOnlyAttribute__ctor_m4293741434(L_2, (bool)0, /*hidden argument*/NULL);
		((ReadOnlyAttribute_t4102148880_StaticFields*)ReadOnlyAttribute_t4102148880_il2cpp_TypeInfo_var->static_fields)->set_Default_3(L_2);
		return;
	}
}
// System.Boolean System.ComponentModel.ReadOnlyAttribute::get_IsReadOnly()
extern "C"  bool ReadOnlyAttribute_get_IsReadOnly_m1257786336 (ReadOnlyAttribute_t4102148880 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_read_only_0();
		return L_0;
	}
}
// System.Int32 System.ComponentModel.ReadOnlyAttribute::GetHashCode()
extern "C"  int32_t ReadOnlyAttribute_GetHashCode_m1429716742 (ReadOnlyAttribute_t4102148880 * __this, const MethodInfo* method)
{
	{
		bool* L_0 = __this->get_address_of_read_only_0();
		int32_t L_1 = Boolean_GetHashCode_m1894638460(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.ComponentModel.ReadOnlyAttribute::Equals(System.Object)
extern Il2CppClass* ReadOnlyAttribute_t4102148880_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyAttribute_Equals_m1737827978_MetadataUsageId;
extern "C"  bool ReadOnlyAttribute_Equals_m1737827978 (ReadOnlyAttribute_t4102148880 * __this, Il2CppObject * ___o0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyAttribute_Equals_m1737827978_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		Il2CppObject * L_0 = ___o0;
		if (((ReadOnlyAttribute_t4102148880 *)IsInstSealed(L_0, ReadOnlyAttribute_t4102148880_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = ___o0;
		NullCheck(((ReadOnlyAttribute_t4102148880 *)CastclassSealed(L_1, ReadOnlyAttribute_t4102148880_il2cpp_TypeInfo_var)));
		bool L_2 = ReadOnlyAttribute_get_IsReadOnly_m1257786336(((ReadOnlyAttribute_t4102148880 *)CastclassSealed(L_1, ReadOnlyAttribute_t4102148880_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		V_0 = L_2;
		bool L_3 = __this->get_read_only_0();
		bool L_4 = Boolean_Equals_m294106711((&V_0), L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void System.ComponentModel.RecommendedAsConfigurableAttribute::.ctor(System.Boolean)
extern "C"  void RecommendedAsConfigurableAttribute__ctor_m3261060350 (RecommendedAsConfigurableAttribute_t420947846 * __this, bool ___recommendedAsConfigurable0, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		bool L_0 = ___recommendedAsConfigurable0;
		__this->set_recommendedAsConfigurable_0(L_0);
		return;
	}
}
// System.Void System.ComponentModel.RecommendedAsConfigurableAttribute::.cctor()
extern Il2CppClass* RecommendedAsConfigurableAttribute_t420947846_il2cpp_TypeInfo_var;
extern const uint32_t RecommendedAsConfigurableAttribute__cctor_m4287333502_MetadataUsageId;
extern "C"  void RecommendedAsConfigurableAttribute__cctor_m4287333502 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RecommendedAsConfigurableAttribute__cctor_m4287333502_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RecommendedAsConfigurableAttribute_t420947846 * L_0 = (RecommendedAsConfigurableAttribute_t420947846 *)il2cpp_codegen_object_new(RecommendedAsConfigurableAttribute_t420947846_il2cpp_TypeInfo_var);
		RecommendedAsConfigurableAttribute__ctor_m3261060350(L_0, (bool)0, /*hidden argument*/NULL);
		((RecommendedAsConfigurableAttribute_t420947846_StaticFields*)RecommendedAsConfigurableAttribute_t420947846_il2cpp_TypeInfo_var->static_fields)->set_Default_1(L_0);
		RecommendedAsConfigurableAttribute_t420947846 * L_1 = (RecommendedAsConfigurableAttribute_t420947846 *)il2cpp_codegen_object_new(RecommendedAsConfigurableAttribute_t420947846_il2cpp_TypeInfo_var);
		RecommendedAsConfigurableAttribute__ctor_m3261060350(L_1, (bool)0, /*hidden argument*/NULL);
		((RecommendedAsConfigurableAttribute_t420947846_StaticFields*)RecommendedAsConfigurableAttribute_t420947846_il2cpp_TypeInfo_var->static_fields)->set_No_2(L_1);
		RecommendedAsConfigurableAttribute_t420947846 * L_2 = (RecommendedAsConfigurableAttribute_t420947846 *)il2cpp_codegen_object_new(RecommendedAsConfigurableAttribute_t420947846_il2cpp_TypeInfo_var);
		RecommendedAsConfigurableAttribute__ctor_m3261060350(L_2, (bool)1, /*hidden argument*/NULL);
		((RecommendedAsConfigurableAttribute_t420947846_StaticFields*)RecommendedAsConfigurableAttribute_t420947846_il2cpp_TypeInfo_var->static_fields)->set_Yes_3(L_2);
		return;
	}
}
// System.Boolean System.ComponentModel.RecommendedAsConfigurableAttribute::get_RecommendedAsConfigurable()
extern "C"  bool RecommendedAsConfigurableAttribute_get_RecommendedAsConfigurable_m1117204446 (RecommendedAsConfigurableAttribute_t420947846 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_recommendedAsConfigurable_0();
		return L_0;
	}
}
// System.Boolean System.ComponentModel.RecommendedAsConfigurableAttribute::Equals(System.Object)
extern Il2CppClass* RecommendedAsConfigurableAttribute_t420947846_il2cpp_TypeInfo_var;
extern const uint32_t RecommendedAsConfigurableAttribute_Equals_m1643996838_MetadataUsageId;
extern "C"  bool RecommendedAsConfigurableAttribute_Equals_m1643996838 (RecommendedAsConfigurableAttribute_t420947846 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RecommendedAsConfigurableAttribute_Equals_m1643996838_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___obj0;
		if (((RecommendedAsConfigurableAttribute_t420947846 *)IsInstClass(L_0, RecommendedAsConfigurableAttribute_t420947846_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = ___obj0;
		NullCheck(((RecommendedAsConfigurableAttribute_t420947846 *)CastclassClass(L_1, RecommendedAsConfigurableAttribute_t420947846_il2cpp_TypeInfo_var)));
		bool L_2 = RecommendedAsConfigurableAttribute_get_RecommendedAsConfigurable_m1117204446(((RecommendedAsConfigurableAttribute_t420947846 *)CastclassClass(L_1, RecommendedAsConfigurableAttribute_t420947846_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		bool L_3 = __this->get_recommendedAsConfigurable_0();
		return (bool)((((int32_t)L_2) == ((int32_t)L_3))? 1 : 0);
	}
}
// System.Int32 System.ComponentModel.RecommendedAsConfigurableAttribute::GetHashCode()
extern "C"  int32_t RecommendedAsConfigurableAttribute_GetHashCode_m4005322302 (RecommendedAsConfigurableAttribute_t420947846 * __this, const MethodInfo* method)
{
	{
		bool* L_0 = __this->get_address_of_recommendedAsConfigurable_0();
		int32_t L_1 = Boolean_GetHashCode_m1894638460(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void System.ComponentModel.RefreshEventArgs::.ctor(System.Object)
extern Il2CppClass* EventArgs_t3289624707_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3542550397;
extern const uint32_t RefreshEventArgs__ctor_m3407787435_MetadataUsageId;
extern "C"  void RefreshEventArgs__ctor_m3407787435 (RefreshEventArgs_t2077477224 * __this, Il2CppObject * ___componentChanged0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RefreshEventArgs__ctor_m3407787435_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t3289624707_il2cpp_TypeInfo_var);
		EventArgs__ctor_m3696060910(__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___componentChanged0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, _stringLiteral3542550397, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		Il2CppObject * L_2 = ___componentChanged0;
		__this->set_component_1(L_2);
		Il2CppObject * L_3 = __this->get_component_1();
		NullCheck(L_3);
		Type_t * L_4 = Object_GetType_m191970594(L_3, /*hidden argument*/NULL);
		__this->set_type_2(L_4);
		return;
	}
}
// System.Void System.ComponentModel.RefreshEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void RefreshEventHandler__ctor_m364042240 (RefreshEventHandler_t456069287 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.ComponentModel.RefreshEventHandler::Invoke(System.ComponentModel.RefreshEventArgs)
extern "C"  void RefreshEventHandler_Invoke_m2415338745 (RefreshEventHandler_t456069287 * __this, RefreshEventArgs_t2077477224 * ___e0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		RefreshEventHandler_Invoke_m2415338745((RefreshEventHandler_t456069287 *)__this->get_prev_9(),___e0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, RefreshEventArgs_t2077477224 * ___e0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___e0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, RefreshEventArgs_t2077477224 * ___e0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___e0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___e0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.ComponentModel.RefreshEventHandler::BeginInvoke(System.ComponentModel.RefreshEventArgs,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * RefreshEventHandler_BeginInvoke_m195317252 (RefreshEventHandler_t456069287 * __this, RefreshEventArgs_t2077477224 * ___e0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___e0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.ComponentModel.RefreshEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void RefreshEventHandler_EndInvoke_m439483870 (RefreshEventHandler_t456069287 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.ComponentModel.RefreshPropertiesAttribute::.ctor(System.ComponentModel.RefreshProperties)
extern "C"  void RefreshPropertiesAttribute__ctor_m3169953508 (RefreshPropertiesAttribute_t2234294918 * __this, int32_t ___refresh0, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___refresh0;
		__this->set_refresh_0(L_0);
		return;
	}
}
// System.Void System.ComponentModel.RefreshPropertiesAttribute::.cctor()
extern Il2CppClass* RefreshPropertiesAttribute_t2234294918_il2cpp_TypeInfo_var;
extern const uint32_t RefreshPropertiesAttribute__cctor_m930577998_MetadataUsageId;
extern "C"  void RefreshPropertiesAttribute__cctor_m930577998 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RefreshPropertiesAttribute__cctor_m930577998_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RefreshPropertiesAttribute_t2234294918 * L_0 = (RefreshPropertiesAttribute_t2234294918 *)il2cpp_codegen_object_new(RefreshPropertiesAttribute_t2234294918_il2cpp_TypeInfo_var);
		RefreshPropertiesAttribute__ctor_m3169953508(L_0, 1, /*hidden argument*/NULL);
		((RefreshPropertiesAttribute_t2234294918_StaticFields*)RefreshPropertiesAttribute_t2234294918_il2cpp_TypeInfo_var->static_fields)->set_All_1(L_0);
		RefreshPropertiesAttribute_t2234294918 * L_1 = (RefreshPropertiesAttribute_t2234294918 *)il2cpp_codegen_object_new(RefreshPropertiesAttribute_t2234294918_il2cpp_TypeInfo_var);
		RefreshPropertiesAttribute__ctor_m3169953508(L_1, 0, /*hidden argument*/NULL);
		((RefreshPropertiesAttribute_t2234294918_StaticFields*)RefreshPropertiesAttribute_t2234294918_il2cpp_TypeInfo_var->static_fields)->set_Default_2(L_1);
		RefreshPropertiesAttribute_t2234294918 * L_2 = (RefreshPropertiesAttribute_t2234294918 *)il2cpp_codegen_object_new(RefreshPropertiesAttribute_t2234294918_il2cpp_TypeInfo_var);
		RefreshPropertiesAttribute__ctor_m3169953508(L_2, 2, /*hidden argument*/NULL);
		((RefreshPropertiesAttribute_t2234294918_StaticFields*)RefreshPropertiesAttribute_t2234294918_il2cpp_TypeInfo_var->static_fields)->set_Repaint_3(L_2);
		return;
	}
}
// System.ComponentModel.RefreshProperties System.ComponentModel.RefreshPropertiesAttribute::get_RefreshProperties()
extern "C"  int32_t RefreshPropertiesAttribute_get_RefreshProperties_m1785512380 (RefreshPropertiesAttribute_t2234294918 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_refresh_0();
		return L_0;
	}
}
// System.Boolean System.ComponentModel.RefreshPropertiesAttribute::Equals(System.Object)
extern Il2CppClass* RefreshPropertiesAttribute_t2234294918_il2cpp_TypeInfo_var;
extern const uint32_t RefreshPropertiesAttribute_Equals_m1211762146_MetadataUsageId;
extern "C"  bool RefreshPropertiesAttribute_Equals_m1211762146 (RefreshPropertiesAttribute_t2234294918 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RefreshPropertiesAttribute_Equals_m1211762146_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___obj0;
		if (((RefreshPropertiesAttribute_t2234294918 *)IsInstSealed(L_0, RefreshPropertiesAttribute_t2234294918_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = ___obj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(RefreshPropertiesAttribute_t2234294918 *)__this))))
		{
			goto IL_0016;
		}
	}
	{
		return (bool)1;
	}

IL_0016:
	{
		Il2CppObject * L_2 = ___obj0;
		NullCheck(((RefreshPropertiesAttribute_t2234294918 *)CastclassSealed(L_2, RefreshPropertiesAttribute_t2234294918_il2cpp_TypeInfo_var)));
		int32_t L_3 = RefreshPropertiesAttribute_get_RefreshProperties_m1785512380(((RefreshPropertiesAttribute_t2234294918 *)CastclassSealed(L_2, RefreshPropertiesAttribute_t2234294918_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		int32_t L_4 = __this->get_refresh_0();
		return (bool)((((int32_t)L_3) == ((int32_t)L_4))? 1 : 0);
	}
}
// System.Int32 System.ComponentModel.RefreshPropertiesAttribute::GetHashCode()
extern Il2CppClass* RefreshProperties_t2240171922_il2cpp_TypeInfo_var;
extern const uint32_t RefreshPropertiesAttribute_GetHashCode_m3132688070_MetadataUsageId;
extern "C"  int32_t RefreshPropertiesAttribute_GetHashCode_m3132688070 (RefreshPropertiesAttribute_t2234294918 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RefreshPropertiesAttribute_GetHashCode_m3132688070_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_refresh_0();
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(RefreshProperties_t2240171922_il2cpp_TypeInfo_var, &L_1);
		NullCheck((Enum_t2459695545 *)L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Enum::GetHashCode() */, (Enum_t2459695545 *)L_2);
		return L_3;
	}
}
// System.Void System.ComponentModel.ToolboxItemAttribute::.ctor(System.Boolean)
extern Il2CppCodeGenString* _stringLiteral415756191;
extern const uint32_t ToolboxItemAttribute__ctor_m2333507424_MetadataUsageId;
extern "C"  void ToolboxItemAttribute__ctor_m2333507424 (ToolboxItemAttribute_t3063187714 * __this, bool ___defaultType0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ToolboxItemAttribute__ctor_m2333507424_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		bool L_0 = ___defaultType0;
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		__this->set_itemTypeName_3(_stringLiteral415756191);
	}

IL_0017:
	{
		return;
	}
}
// System.Void System.ComponentModel.ToolboxItemAttribute::.ctor(System.String)
extern "C"  void ToolboxItemAttribute__ctor_m3243023093 (ToolboxItemAttribute_t3063187714 * __this, String_t* ___toolboxItemName0, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___toolboxItemName0;
		__this->set_itemTypeName_3(L_0);
		return;
	}
}
// System.Void System.ComponentModel.ToolboxItemAttribute::.cctor()
extern Il2CppClass* ToolboxItemAttribute_t3063187714_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral415756191;
extern const uint32_t ToolboxItemAttribute__cctor_m127312448_MetadataUsageId;
extern "C"  void ToolboxItemAttribute__cctor_m127312448 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ToolboxItemAttribute__cctor_m127312448_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ToolboxItemAttribute_t3063187714 * L_0 = (ToolboxItemAttribute_t3063187714 *)il2cpp_codegen_object_new(ToolboxItemAttribute_t3063187714_il2cpp_TypeInfo_var);
		ToolboxItemAttribute__ctor_m3243023093(L_0, _stringLiteral415756191, /*hidden argument*/NULL);
		((ToolboxItemAttribute_t3063187714_StaticFields*)ToolboxItemAttribute_t3063187714_il2cpp_TypeInfo_var->static_fields)->set_Default_0(L_0);
		ToolboxItemAttribute_t3063187714 * L_1 = (ToolboxItemAttribute_t3063187714 *)il2cpp_codegen_object_new(ToolboxItemAttribute_t3063187714_il2cpp_TypeInfo_var);
		ToolboxItemAttribute__ctor_m2333507424(L_1, (bool)0, /*hidden argument*/NULL);
		((ToolboxItemAttribute_t3063187714_StaticFields*)ToolboxItemAttribute_t3063187714_il2cpp_TypeInfo_var->static_fields)->set_None_1(L_1);
		return;
	}
}
// System.String System.ComponentModel.ToolboxItemAttribute::get_ToolboxItemTypeName()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t ToolboxItemAttribute_get_ToolboxItemTypeName_m2264175550_MetadataUsageId;
extern "C"  String_t* ToolboxItemAttribute_get_ToolboxItemTypeName_m2264175550 (ToolboxItemAttribute_t3063187714 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ToolboxItemAttribute_get_ToolboxItemTypeName_m2264175550_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = __this->get_itemTypeName_3();
		if (L_0)
		{
			goto IL_002d;
		}
	}
	{
		Type_t * L_1 = __this->get_itemType_2();
		if (L_1)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_2;
	}

IL_001c:
	{
		Type_t * L_3 = __this->get_itemType_2();
		NullCheck(L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(15 /* System.String System.Type::get_AssemblyQualifiedName() */, L_3);
		__this->set_itemTypeName_3(L_4);
	}

IL_002d:
	{
		String_t* L_5 = __this->get_itemTypeName_3();
		return L_5;
	}
}
// System.Boolean System.ComponentModel.ToolboxItemAttribute::Equals(System.Object)
extern Il2CppClass* ToolboxItemAttribute_t3063187714_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t ToolboxItemAttribute_Equals_m49160084_MetadataUsageId;
extern "C"  bool ToolboxItemAttribute_Equals_m49160084 (ToolboxItemAttribute_t3063187714 * __this, Il2CppObject * ___o0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ToolboxItemAttribute_Equals_m49160084_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ToolboxItemAttribute_t3063187714 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___o0;
		V_0 = ((ToolboxItemAttribute_t3063187714 *)IsInstClass(L_0, ToolboxItemAttribute_t3063187714_il2cpp_TypeInfo_var));
		ToolboxItemAttribute_t3063187714 * L_1 = V_0;
		if (L_1)
		{
			goto IL_000f;
		}
	}
	{
		return (bool)0;
	}

IL_000f:
	{
		ToolboxItemAttribute_t3063187714 * L_2 = V_0;
		NullCheck(L_2);
		String_t* L_3 = ToolboxItemAttribute_get_ToolboxItemTypeName_m2264175550(L_2, /*hidden argument*/NULL);
		String_t* L_4 = ToolboxItemAttribute_get_ToolboxItemTypeName_m2264175550(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Int32 System.ComponentModel.ToolboxItemAttribute::GetHashCode()
extern "C"  int32_t ToolboxItemAttribute_GetHashCode_m2851631312 (ToolboxItemAttribute_t3063187714 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_itemTypeName_3();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		String_t* L_1 = __this->get_itemTypeName_3();
		NullCheck(L_1);
		int32_t L_2 = String_GetHashCode_m931956593(L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_0017:
	{
		int32_t L_3 = Attribute_GetHashCode_m2653962112(__this, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void System.ComponentModel.TypeConverterAttribute::.ctor()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t TypeConverterAttribute__ctor_m3322338951_MetadataUsageId;
extern "C"  void TypeConverterAttribute__ctor_m3322338951 (TypeConverterAttribute_t252469870 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypeConverterAttribute__ctor_m3322338951_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_converter_type_1(L_0);
		return;
	}
}
// System.Void System.ComponentModel.TypeConverterAttribute::.ctor(System.String)
extern "C"  void TypeConverterAttribute__ctor_m1783325257 (TypeConverterAttribute_t252469870 * __this, String_t* ___typeName0, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___typeName0;
		__this->set_converter_type_1(L_0);
		return;
	}
}
// System.Void System.ComponentModel.TypeConverterAttribute::.ctor(System.Type)
extern "C"  void TypeConverterAttribute__ctor_m4061167050 (TypeConverterAttribute_t252469870 * __this, Type_t * ___type0, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		Type_t * L_0 = ___type0;
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(15 /* System.String System.Type::get_AssemblyQualifiedName() */, L_0);
		__this->set_converter_type_1(L_1);
		return;
	}
}
// System.Void System.ComponentModel.TypeConverterAttribute::.cctor()
extern Il2CppClass* TypeConverterAttribute_t252469870_il2cpp_TypeInfo_var;
extern const uint32_t TypeConverterAttribute__cctor_m506796812_MetadataUsageId;
extern "C"  void TypeConverterAttribute__cctor_m506796812 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypeConverterAttribute__cctor_m506796812_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TypeConverterAttribute_t252469870 * L_0 = (TypeConverterAttribute_t252469870 *)il2cpp_codegen_object_new(TypeConverterAttribute_t252469870_il2cpp_TypeInfo_var);
		TypeConverterAttribute__ctor_m3322338951(L_0, /*hidden argument*/NULL);
		((TypeConverterAttribute_t252469870_StaticFields*)TypeConverterAttribute_t252469870_il2cpp_TypeInfo_var->static_fields)->set_Default_0(L_0);
		return;
	}
}
// System.Boolean System.ComponentModel.TypeConverterAttribute::Equals(System.Object)
extern Il2CppClass* TypeConverterAttribute_t252469870_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t TypeConverterAttribute_Equals_m2330345916_MetadataUsageId;
extern "C"  bool TypeConverterAttribute_Equals_m2330345916 (TypeConverterAttribute_t252469870 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypeConverterAttribute_Equals_m2330345916_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___obj0;
		if (((TypeConverterAttribute_t252469870 *)IsInstSealed(L_0, TypeConverterAttribute_t252469870_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = ___obj0;
		NullCheck(((TypeConverterAttribute_t252469870 *)CastclassSealed(L_1, TypeConverterAttribute_t252469870_il2cpp_TypeInfo_var)));
		String_t* L_2 = TypeConverterAttribute_get_ConverterTypeName_m2296240606(((TypeConverterAttribute_t252469870 *)CastclassSealed(L_1, TypeConverterAttribute_t252469870_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		String_t* L_3 = __this->get_converter_type_1();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_4 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Int32 System.ComponentModel.TypeConverterAttribute::GetHashCode()
extern "C"  int32_t TypeConverterAttribute_GetHashCode_m131437980 (TypeConverterAttribute_t252469870 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_converter_type_1();
		NullCheck(L_0);
		int32_t L_1 = String_GetHashCode_m931956593(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String System.ComponentModel.TypeConverterAttribute::get_ConverterTypeName()
extern "C"  String_t* TypeConverterAttribute_get_ConverterTypeName_m2296240606 (TypeConverterAttribute_t252469870 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_converter_type_1();
		return L_0;
	}
}
// System.Void System.ComponentModel.TypeDescriptor::.cctor()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeDescriptor_t3595688691_il2cpp_TypeInfo_var;
extern Il2CppClass* Hashtable_t909839986_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t385723205_il2cpp_TypeInfo_var;
extern Il2CppClass* WeakObjectWrapperComparer_t3891611113_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t1208943611_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m3593752731_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m2804069212_MethodInfo_var;
extern const uint32_t TypeDescriptor__cctor_m203489455_MetadataUsageId;
extern "C"  void TypeDescriptor__cctor_m203489455 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypeDescriptor__cctor_m203489455_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m2551263788(L_0, /*hidden argument*/NULL);
		((TypeDescriptor_t3595688691_StaticFields*)TypeDescriptor_t3595688691_il2cpp_TypeInfo_var->static_fields)->set_creatingDefaultConverters_0(L_0);
		Hashtable_t909839986 * L_1 = (Hashtable_t909839986 *)il2cpp_codegen_object_new(Hashtable_t909839986_il2cpp_TypeInfo_var);
		Hashtable__ctor_m1884964176(L_1, /*hidden argument*/NULL);
		((TypeDescriptor_t3595688691_StaticFields*)TypeDescriptor_t3595688691_il2cpp_TypeInfo_var->static_fields)->set_componentTable_1(L_1);
		Hashtable_t909839986 * L_2 = (Hashtable_t909839986 *)il2cpp_codegen_object_new(Hashtable_t909839986_il2cpp_TypeInfo_var);
		Hashtable__ctor_m1884964176(L_2, /*hidden argument*/NULL);
		((TypeDescriptor_t3595688691_StaticFields*)TypeDescriptor_t3595688691_il2cpp_TypeInfo_var->static_fields)->set_typeTable_2(L_2);
		Il2CppObject * L_3 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m2551263788(L_3, /*hidden argument*/NULL);
		((TypeDescriptor_t3595688691_StaticFields*)TypeDescriptor_t3595688691_il2cpp_TypeInfo_var->static_fields)->set_typeDescriptionProvidersLock_3(L_3);
		Il2CppObject * L_4 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m2551263788(L_4, /*hidden argument*/NULL);
		((TypeDescriptor_t3595688691_StaticFields*)TypeDescriptor_t3595688691_il2cpp_TypeInfo_var->static_fields)->set_componentDescriptionProvidersLock_5(L_4);
		Dictionary_2_t385723205 * L_5 = (Dictionary_2_t385723205 *)il2cpp_codegen_object_new(Dictionary_2_t385723205_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m3593752731(L_5, /*hidden argument*/Dictionary_2__ctor_m3593752731_MethodInfo_var);
		((TypeDescriptor_t3595688691_StaticFields*)TypeDescriptor_t3595688691_il2cpp_TypeInfo_var->static_fields)->set_typeDescriptionProviders_4(L_5);
		WeakObjectWrapperComparer_t3891611113 * L_6 = (WeakObjectWrapperComparer_t3891611113 *)il2cpp_codegen_object_new(WeakObjectWrapperComparer_t3891611113_il2cpp_TypeInfo_var);
		WeakObjectWrapperComparer__ctor_m2523985882(L_6, /*hidden argument*/NULL);
		Dictionary_2_t1208943611 * L_7 = (Dictionary_2_t1208943611 *)il2cpp_codegen_object_new(Dictionary_2_t1208943611_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2804069212(L_7, L_6, /*hidden argument*/Dictionary_2__ctor_m2804069212_MethodInfo_var);
		((TypeDescriptor_t3595688691_StaticFields*)TypeDescriptor_t3595688691_il2cpp_TypeInfo_var->static_fields)->set_componentDescriptionProviders_6(L_7);
		return;
	}
}
// System.Void System.ComponentModel.TypeDescriptor::Refresh(System.Object)
extern Il2CppClass* TypeDescriptor_t3595688691_il2cpp_TypeInfo_var;
extern Il2CppClass* RefreshEventArgs_t2077477224_il2cpp_TypeInfo_var;
extern const uint32_t TypeDescriptor_Refresh_m1287836919_MetadataUsageId;
extern "C"  void TypeDescriptor_Refresh_m1287836919 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___component0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypeDescriptor_Refresh_m1287836919_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Hashtable_t909839986 * V_0 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(TypeDescriptor_t3595688691_il2cpp_TypeInfo_var);
		Hashtable_t909839986 * L_0 = ((TypeDescriptor_t3595688691_StaticFields*)TypeDescriptor_t3595688691_il2cpp_TypeInfo_var->static_fields)->get_componentTable_1();
		V_0 = L_0;
		Hashtable_t909839986 * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		IL2CPP_RUNTIME_CLASS_INIT(TypeDescriptor_t3595688691_il2cpp_TypeInfo_var);
		Hashtable_t909839986 * L_2 = ((TypeDescriptor_t3595688691_StaticFields*)TypeDescriptor_t3595688691_il2cpp_TypeInfo_var->static_fields)->get_componentTable_1();
		Il2CppObject * L_3 = ___component0;
		NullCheck(L_2);
		VirtActionInvoker1< Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Remove(System.Object) */, L_2, L_3);
		IL2CPP_LEAVE(0x23, FINALLY_001c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_001c;
	}

FINALLY_001c:
	{ // begin finally (depth: 1)
		Hashtable_t909839986 * L_4 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(28)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(28)
	{
		IL2CPP_JUMP_TBL(0x23, IL_0023)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0023:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TypeDescriptor_t3595688691_il2cpp_TypeInfo_var);
		RefreshEventHandler_t456069287 * L_5 = ((TypeDescriptor_t3595688691_StaticFields*)TypeDescriptor_t3595688691_il2cpp_TypeInfo_var->static_fields)->get_Refreshed_7();
		if (!L_5)
		{
			goto IL_003d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TypeDescriptor_t3595688691_il2cpp_TypeInfo_var);
		RefreshEventHandler_t456069287 * L_6 = ((TypeDescriptor_t3595688691_StaticFields*)TypeDescriptor_t3595688691_il2cpp_TypeInfo_var->static_fields)->get_Refreshed_7();
		Il2CppObject * L_7 = ___component0;
		RefreshEventArgs_t2077477224 * L_8 = (RefreshEventArgs_t2077477224 *)il2cpp_codegen_object_new(RefreshEventArgs_t2077477224_il2cpp_TypeInfo_var);
		RefreshEventArgs__ctor_m3407787435(L_8, L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		RefreshEventHandler_Invoke_m2415338745(L_6, L_8, /*hidden argument*/NULL);
	}

IL_003d:
	{
		return;
	}
}
// System.Int32 System.ComponentModel.WeakObjectWrapper::get_TargetHashCode()
extern "C"  int32_t WeakObjectWrapper_get_TargetHashCode_m1269470006 (WeakObjectWrapper_t2012978780 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_U3CTargetHashCodeU3Ek__BackingField_0();
		return L_0;
	}
}
// System.WeakReference System.ComponentModel.WeakObjectWrapper::get_Weak()
extern "C"  WeakReference_t1077405567 * WeakObjectWrapper_get_Weak_m2333879335 (WeakObjectWrapper_t2012978780 * __this, const MethodInfo* method)
{
	{
		WeakReference_t1077405567 * L_0 = __this->get_U3CWeakU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void System.ComponentModel.WeakObjectWrapperComparer::.ctor()
extern Il2CppClass* EqualityComparer_1_t586614051_il2cpp_TypeInfo_var;
extern const MethodInfo* EqualityComparer_1__ctor_m2203349008_MethodInfo_var;
extern const uint32_t WeakObjectWrapperComparer__ctor_m2523985882_MetadataUsageId;
extern "C"  void WeakObjectWrapperComparer__ctor_m2523985882 (WeakObjectWrapperComparer_t3891611113 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WeakObjectWrapperComparer__ctor_m2523985882_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(EqualityComparer_1_t586614051_il2cpp_TypeInfo_var);
		EqualityComparer_1__ctor_m2203349008(__this, /*hidden argument*/EqualityComparer_1__ctor_m2203349008_MethodInfo_var);
		return;
	}
}
// System.Boolean System.ComponentModel.WeakObjectWrapperComparer::Equals(System.ComponentModel.WeakObjectWrapper,System.ComponentModel.WeakObjectWrapper)
extern "C"  bool WeakObjectWrapperComparer_Equals_m2417342495 (WeakObjectWrapperComparer_t3891611113 * __this, WeakObjectWrapper_t2012978780 * ___x0, WeakObjectWrapper_t2012978780 * ___y1, const MethodInfo* method)
{
	WeakReference_t1077405567 * V_0 = NULL;
	WeakReference_t1077405567 * V_1 = NULL;
	{
		WeakObjectWrapper_t2012978780 * L_0 = ___x0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		WeakObjectWrapper_t2012978780 * L_1 = ___y1;
		if (L_1)
		{
			goto IL_000e;
		}
	}
	{
		return (bool)0;
	}

IL_000e:
	{
		WeakObjectWrapper_t2012978780 * L_2 = ___x0;
		if (!L_2)
		{
			goto IL_001a;
		}
	}
	{
		WeakObjectWrapper_t2012978780 * L_3 = ___y1;
		if (L_3)
		{
			goto IL_001c;
		}
	}

IL_001a:
	{
		return (bool)0;
	}

IL_001c:
	{
		WeakObjectWrapper_t2012978780 * L_4 = ___x0;
		NullCheck(L_4);
		WeakReference_t1077405567 * L_5 = WeakObjectWrapper_get_Weak_m2333879335(L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		WeakObjectWrapper_t2012978780 * L_6 = ___y1;
		NullCheck(L_6);
		WeakReference_t1077405567 * L_7 = WeakObjectWrapper_get_Weak_m2333879335(L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		WeakReference_t1077405567 * L_8 = V_0;
		NullCheck(L_8);
		bool L_9 = VirtFuncInvoker0< bool >::Invoke(5 /* System.Boolean System.WeakReference::get_IsAlive() */, L_8);
		if (L_9)
		{
			goto IL_0042;
		}
	}
	{
		WeakReference_t1077405567 * L_10 = V_1;
		NullCheck(L_10);
		bool L_11 = VirtFuncInvoker0< bool >::Invoke(5 /* System.Boolean System.WeakReference::get_IsAlive() */, L_10);
		if (L_11)
		{
			goto IL_0042;
		}
	}
	{
		return (bool)0;
	}

IL_0042:
	{
		WeakReference_t1077405567 * L_12 = V_0;
		NullCheck(L_12);
		Il2CppObject * L_13 = VirtFuncInvoker0< Il2CppObject * >::Invoke(6 /* System.Object System.WeakReference::get_Target() */, L_12);
		WeakReference_t1077405567 * L_14 = V_1;
		NullCheck(L_14);
		Il2CppObject * L_15 = VirtFuncInvoker0< Il2CppObject * >::Invoke(6 /* System.Object System.WeakReference::get_Target() */, L_14);
		return (bool)((((Il2CppObject*)(Il2CppObject *)L_13) == ((Il2CppObject*)(Il2CppObject *)L_15))? 1 : 0);
	}
}
// System.Int32 System.ComponentModel.WeakObjectWrapperComparer::GetHashCode(System.ComponentModel.WeakObjectWrapper)
extern "C"  int32_t WeakObjectWrapperComparer_GetHashCode_m3041359540 (WeakObjectWrapperComparer_t3891611113 * __this, WeakObjectWrapper_t2012978780 * ___obj0, const MethodInfo* method)
{
	{
		WeakObjectWrapper_t2012978780 * L_0 = ___obj0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return 0;
	}

IL_0008:
	{
		WeakObjectWrapper_t2012978780 * L_1 = ___obj0;
		NullCheck(L_1);
		int32_t L_2 = WeakObjectWrapper_get_TargetHashCode_m1269470006(L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void System.ComponentModel.Win32Exception::.ctor()
extern Il2CppClass* Marshal_t785896760_il2cpp_TypeInfo_var;
extern const uint32_t Win32Exception__ctor_m1919801717_MetadataUsageId;
extern "C"  void Win32Exception__ctor_m1919801717 (Win32Exception_t1708275760 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Win32Exception__ctor_m1919801717_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t785896760_il2cpp_TypeInfo_var);
		int32_t L_0 = Marshal_GetLastWin32Error_m4162683157(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_1 = Win32Exception_W32ErrorMessage_m916044474(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		ExternalException__ctor_m2637668460(__this, L_1, /*hidden argument*/NULL);
		int32_t L_2 = Marshal_GetLastWin32Error_m4162683157(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_native_error_code_11(L_2);
		return;
	}
}
// System.Void System.ComponentModel.Win32Exception::.ctor(System.Int32)
extern "C"  void Win32Exception__ctor_m1153504280 (Win32Exception_t1708275760 * __this, int32_t ___error0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___error0;
		String_t* L_1 = Win32Exception_W32ErrorMessage_m916044474(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		ExternalException__ctor_m2637668460(__this, L_1, /*hidden argument*/NULL);
		int32_t L_2 = ___error0;
		__this->set_native_error_code_11(L_2);
		return;
	}
}
// System.Void System.ComponentModel.Win32Exception::.ctor(System.Int32,System.String)
extern "C"  void Win32Exception__ctor_m625346072 (Win32Exception_t1708275760 * __this, int32_t ___error0, String_t* ___message1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message1;
		ExternalException__ctor_m2637668460(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = ___error0;
		__this->set_native_error_code_11(L_1);
		return;
	}
}
// System.Void System.ComponentModel.Win32Exception::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern Il2CppCodeGenString* _stringLiteral2525319328;
extern const uint32_t Win32Exception__ctor_m3886439846_MetadataUsageId;
extern "C"  void Win32Exception__ctor_m3886439846 (Win32Exception_t1708275760 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Win32Exception__ctor_m3886439846_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SerializationInfo_t228987430 * L_0 = ___info0;
		StreamingContext_t1417235061  L_1 = ___context1;
		ExternalException__ctor_m4181288867(__this, L_0, L_1, /*hidden argument*/NULL);
		SerializationInfo_t228987430 * L_2 = ___info0;
		NullCheck(L_2);
		int32_t L_3 = SerializationInfo_GetInt32_m4039439501(L_2, _stringLiteral2525319328, /*hidden argument*/NULL);
		__this->set_native_error_code_11(L_3);
		return;
	}
}
// System.Int32 System.ComponentModel.Win32Exception::get_NativeErrorCode()
extern "C"  int32_t Win32Exception_get_NativeErrorCode_m2974494452 (Win32Exception_t1708275760 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_native_error_code_11();
		return L_0;
	}
}
// System.Void System.ComponentModel.Win32Exception::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2792112382;
extern Il2CppCodeGenString* _stringLiteral2525319328;
extern const uint32_t Win32Exception_GetObjectData_m2648429033_MetadataUsageId;
extern "C"  void Win32Exception_GetObjectData_m2648429033 (Win32Exception_t1708275760 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Win32Exception_GetObjectData_m2648429033_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SerializationInfo_t228987430 * L_0 = ___info0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, _stringLiteral2792112382, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		SerializationInfo_t228987430 * L_2 = ___info0;
		int32_t L_3 = __this->get_native_error_code_11();
		NullCheck(L_2);
		SerializationInfo_AddValue_m902275108(L_2, _stringLiteral2525319328, L_3, /*hidden argument*/NULL);
		SerializationInfo_t228987430 * L_4 = ___info0;
		StreamingContext_t1417235061  L_5 = ___context1;
		Exception_GetObjectData_m2653827630(__this, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.String System.ComponentModel.Win32Exception::W32ErrorMessage(System.Int32)
extern "C"  String_t* Win32Exception_W32ErrorMessage_m916044474 (Il2CppObject * __this /* static, unused */, int32_t ___error_code0, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef String_t* (*Win32Exception_W32ErrorMessage_m916044474_ftn) (int32_t);
	return  ((Win32Exception_W32ErrorMessage_m916044474_ftn)System::System::ComponentModel::Win32Exception::W32ErrorMessage) (___error_code0);
}
// System.Void System.DefaultUriParser::.ctor()
extern Il2CppClass* UriParser_t1012511323_il2cpp_TypeInfo_var;
extern const uint32_t DefaultUriParser__ctor_m4218024811_MetadataUsageId;
extern "C"  void DefaultUriParser__ctor_m4218024811 (DefaultUriParser_t1591960796 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultUriParser__ctor_m4218024811_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UriParser_t1012511323_il2cpp_TypeInfo_var);
		UriParser__ctor_m1282308392(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.DefaultUriParser::.ctor(System.String)
extern Il2CppClass* UriParser_t1012511323_il2cpp_TypeInfo_var;
extern const uint32_t DefaultUriParser__ctor_m1180095657_MetadataUsageId;
extern "C"  void DefaultUriParser__ctor_m1180095657 (DefaultUriParser_t1591960796 * __this, String_t* ___scheme0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultUriParser__ctor_m1180095657_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UriParser_t1012511323_il2cpp_TypeInfo_var);
		UriParser__ctor_m1282308392(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___scheme0;
		((UriParser_t1012511323 *)__this)->set_scheme_name_2(L_0);
		return;
	}
}
// System.Void System.Diagnostics.Stopwatch::.ctor()
extern "C"  void Stopwatch__ctor_m589309528 (Stopwatch_t1380178105 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Diagnostics.Stopwatch::.cctor()
extern Il2CppClass* Stopwatch_t1380178105_il2cpp_TypeInfo_var;
extern const uint32_t Stopwatch__cctor_m1036688681_MetadataUsageId;
extern "C"  void Stopwatch__cctor_m1036688681 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Stopwatch__cctor_m1036688681_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((Stopwatch_t1380178105_StaticFields*)Stopwatch_t1380178105_il2cpp_TypeInfo_var->static_fields)->set_Frequency_0((((int64_t)((int64_t)((int32_t)10000000)))));
		((Stopwatch_t1380178105_StaticFields*)Stopwatch_t1380178105_il2cpp_TypeInfo_var->static_fields)->set_IsHighResolution_1((bool)1);
		return;
	}
}
// System.Int64 System.Diagnostics.Stopwatch::GetTimestamp()
extern "C"  int64_t Stopwatch_GetTimestamp_m4156329811 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef int64_t (*Stopwatch_GetTimestamp_m4156329811_ftn) ();
	return  ((Stopwatch_GetTimestamp_m4156329811_ftn)System::System::Diagnostics::Stopwatch::GetTimestamp) ();
}
// System.Diagnostics.Stopwatch System.Diagnostics.Stopwatch::StartNew()
extern Il2CppClass* Stopwatch_t1380178105_il2cpp_TypeInfo_var;
extern const uint32_t Stopwatch_StartNew_m2200430001_MetadataUsageId;
extern "C"  Stopwatch_t1380178105 * Stopwatch_StartNew_m2200430001 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Stopwatch_StartNew_m2200430001_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Stopwatch_t1380178105 * V_0 = NULL;
	{
		Stopwatch_t1380178105 * L_0 = (Stopwatch_t1380178105 *)il2cpp_codegen_object_new(Stopwatch_t1380178105_il2cpp_TypeInfo_var);
		Stopwatch__ctor_m589309528(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Stopwatch_t1380178105 * L_1 = V_0;
		NullCheck(L_1);
		Stopwatch_Start_m2051791460(L_1, /*hidden argument*/NULL);
		Stopwatch_t1380178105 * L_2 = V_0;
		return L_2;
	}
}
// System.Int64 System.Diagnostics.Stopwatch::get_ElapsedTicks()
extern Il2CppClass* Stopwatch_t1380178105_il2cpp_TypeInfo_var;
extern const uint32_t Stopwatch_get_ElapsedTicks_m3689589548_MetadataUsageId;
extern "C"  int64_t Stopwatch_get_ElapsedTicks_m3689589548 (Stopwatch_t1380178105 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Stopwatch_get_ElapsedTicks_m3689589548_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int64_t G_B3_0 = 0;
	{
		bool L_0 = __this->get_is_running_4();
		if (!L_0)
		{
			goto IL_0023;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Stopwatch_t1380178105_il2cpp_TypeInfo_var);
		int64_t L_1 = Stopwatch_GetTimestamp_m4156329811(NULL /*static, unused*/, /*hidden argument*/NULL);
		int64_t L_2 = __this->get_started_3();
		int64_t L_3 = __this->get_elapsed_2();
		G_B3_0 = ((int64_t)((int64_t)((int64_t)((int64_t)L_1-(int64_t)L_2))+(int64_t)L_3));
		goto IL_0029;
	}

IL_0023:
	{
		int64_t L_4 = __this->get_elapsed_2();
		G_B3_0 = L_4;
	}

IL_0029:
	{
		return G_B3_0;
	}
}
// System.Void System.Diagnostics.Stopwatch::Start()
extern Il2CppClass* Stopwatch_t1380178105_il2cpp_TypeInfo_var;
extern const uint32_t Stopwatch_Start_m2051791460_MetadataUsageId;
extern "C"  void Stopwatch_Start_m2051791460 (Stopwatch_t1380178105 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Stopwatch_Start_m2051791460_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_is_running_4();
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Stopwatch_t1380178105_il2cpp_TypeInfo_var);
		int64_t L_1 = Stopwatch_GetTimestamp_m4156329811(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_started_3(L_1);
		__this->set_is_running_4((bool)1);
		return;
	}
}
// System.Void System.Diagnostics.TraceListener::.ctor(System.String)
extern Il2CppClass* StringDictionary_t1070889667_il2cpp_TypeInfo_var;
extern const uint32_t TraceListener__ctor_m1767898120_MetadataUsageId;
extern "C"  void TraceListener__ctor_m1767898120 (TraceListener_t3414949279 * __this, String_t* ___name0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TraceListener__ctor_m1767898120_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_indentSize_1(4);
		StringDictionary_t1070889667 * L_0 = (StringDictionary_t1070889667 *)il2cpp_codegen_object_new(StringDictionary_t1070889667_il2cpp_TypeInfo_var);
		StringDictionary__ctor_m270184480(L_0, /*hidden argument*/NULL);
		__this->set_attributes_2(L_0);
		__this->set_needIndent_4((bool)1);
		MarshalByRefObject__ctor_m529577364(__this, /*hidden argument*/NULL);
		String_t* L_1 = ___name0;
		VirtActionInvoker1< String_t* >::Invoke(7 /* System.Void System.Diagnostics.TraceListener::set_Name(System.String) */, __this, L_1);
		return;
	}
}
// System.Void System.Diagnostics.TraceListener::set_Name(System.String)
extern "C"  void TraceListener_set_Name_m2158517316 (TraceListener_t3414949279 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_name_3(L_0);
		return;
	}
}
// System.Void System.Diagnostics.TraceListener::Dispose()
extern "C"  void TraceListener_Dispose_m3683438111 (TraceListener_t3414949279 * __this, const MethodInfo* method)
{
	{
		VirtActionInvoker1< bool >::Invoke(8 /* System.Void System.Diagnostics.TraceListener::Dispose(System.Boolean) */, __this, (bool)1);
		GC_SuppressFinalize_m953228702(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Diagnostics.TraceListener::Dispose(System.Boolean)
extern "C"  void TraceListener_Dispose_m513396866 (TraceListener_t3414949279 * __this, bool ___disposing0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void System.IO.Compression.DeflateStream::.ctor(System.IO.Stream,System.IO.Compression.CompressionMode)
extern "C"  void DeflateStream__ctor_m3627775123 (DeflateStream_t3198596725 * __this, Stream_t3255436806 * ___compressedStream0, int32_t ___mode1, const MethodInfo* method)
{
	{
		Stream_t3255436806 * L_0 = ___compressedStream0;
		int32_t L_1 = ___mode1;
		DeflateStream__ctor_m2642899039(__this, L_0, L_1, (bool)0, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.IO.Compression.DeflateStream::.ctor(System.IO.Stream,System.IO.Compression.CompressionMode,System.Boolean,System.Boolean)
extern Il2CppClass* Stream_t3255436806_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppClass* UnmanagedReadOrWrite_t1990215745_il2cpp_TypeInfo_var;
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern Il2CppClass* NotImplementedException_t2785117854_il2cpp_TypeInfo_var;
extern const MethodInfo* DeflateStream_UnmanagedWrite_m2053312812_MethodInfo_var;
extern const MethodInfo* DeflateStream_UnmanagedRead_m3972503109_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral958465395;
extern Il2CppCodeGenString* _stringLiteral1414244983;
extern Il2CppCodeGenString* _stringLiteral1338656661;
extern const uint32_t DeflateStream__ctor_m2642899039_MetadataUsageId;
extern "C"  void DeflateStream__ctor_m2642899039 (DeflateStream_t3198596725 * __this, Stream_t3255436806 * ___compressedStream0, int32_t ___mode1, bool ___leaveOpen2, bool ___gzip3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DeflateStream__ctor_m2642899039_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DeflateStream_t3198596725 * G_B7_0 = NULL;
	DeflateStream_t3198596725 * G_B6_0 = NULL;
	UnmanagedReadOrWrite_t1990215745 * G_B8_0 = NULL;
	DeflateStream_t3198596725 * G_B8_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Stream_t3255436806_il2cpp_TypeInfo_var);
		Stream__ctor_m1531324023(__this, /*hidden argument*/NULL);
		Stream_t3255436806 * L_0 = ___compressedStream0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, _stringLiteral958465395, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		int32_t L_2 = ___mode1;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_002f;
		}
	}
	{
		int32_t L_3 = ___mode1;
		if (!L_3)
		{
			goto IL_002f;
		}
	}
	{
		ArgumentException_t3259014390 * L_4 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_4, _stringLiteral1414244983, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_002f:
	{
		GCHandle_t3409268066  L_5 = GCHandle_Alloc_m3171748614(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		__this->set_data_8(L_5);
		Stream_t3255436806 * L_6 = ___compressedStream0;
		__this->set_base_stream_1(L_6);
		int32_t L_7 = ___mode1;
		G_B6_0 = __this;
		if ((!(((uint32_t)L_7) == ((uint32_t)1))))
		{
			G_B7_0 = __this;
			goto IL_005b;
		}
	}
	{
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)(void*)DeflateStream_UnmanagedWrite_m2053312812_MethodInfo_var);
		UnmanagedReadOrWrite_t1990215745 * L_9 = (UnmanagedReadOrWrite_t1990215745 *)il2cpp_codegen_object_new(UnmanagedReadOrWrite_t1990215745_il2cpp_TypeInfo_var);
		UnmanagedReadOrWrite__ctor_m482274997(L_9, NULL, L_8, /*hidden argument*/NULL);
		G_B8_0 = L_9;
		G_B8_1 = G_B6_0;
		goto IL_0067;
	}

IL_005b:
	{
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)DeflateStream_UnmanagedRead_m3972503109_MethodInfo_var);
		UnmanagedReadOrWrite_t1990215745 * L_11 = (UnmanagedReadOrWrite_t1990215745 *)il2cpp_codegen_object_new(UnmanagedReadOrWrite_t1990215745_il2cpp_TypeInfo_var);
		UnmanagedReadOrWrite__ctor_m482274997(L_11, NULL, L_10, /*hidden argument*/NULL);
		G_B8_0 = L_11;
		G_B8_1 = G_B7_0;
	}

IL_0067:
	{
		NullCheck(G_B8_1);
		G_B8_1->set_feeder_5(G_B8_0);
		int32_t L_12 = ___mode1;
		bool L_13 = ___gzip3;
		UnmanagedReadOrWrite_t1990215745 * L_14 = __this->get_feeder_5();
		GCHandle_t3409268066  L_15 = __this->get_data_8();
		IntPtr_t L_16 = GCHandle_ToIntPtr_m290813543(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		IntPtr_t L_17 = DeflateStream_CreateZStream_m2111962461(NULL /*static, unused*/, L_12, L_13, L_14, L_16, /*hidden argument*/NULL);
		__this->set_z_stream_6(L_17);
		IntPtr_t L_18 = __this->get_z_stream_6();
		IntPtr_t L_19 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		bool L_20 = IntPtr_op_Equality_m1573482188(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_00b9;
		}
	}
	{
		__this->set_base_stream_1((Stream_t3255436806 *)NULL);
		__this->set_feeder_5((UnmanagedReadOrWrite_t1990215745 *)NULL);
		NotImplementedException_t2785117854 * L_21 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1795163961(L_21, _stringLiteral1338656661, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}

IL_00b9:
	{
		int32_t L_22 = ___mode1;
		__this->set_mode_2(L_22);
		bool L_23 = ___leaveOpen2;
		__this->set_leaveOpen_3(L_23);
		return;
	}
}
// System.Void System.IO.Compression.DeflateStream::Dispose(System.Boolean)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern Il2CppClass* GCHandle_t3409268066_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2402255043;
extern const uint32_t DeflateStream_Dispose_m290108336_MetadataUsageId;
extern "C"  void DeflateStream_Dispose_m290108336 (DeflateStream_t3198596725 * __this, bool ___disposing0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DeflateStream_Dispose_m290108336_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IntPtr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	Stream_t3255436806 * V_2 = NULL;
	GCHandle_t3409268066  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		bool L_0 = ___disposing0;
		if (!L_0)
		{
			goto IL_007a;
		}
	}
	{
		bool L_1 = __this->get_disposed_4();
		if (L_1)
		{
			goto IL_007a;
		}
	}
	{
		__this->set_disposed_4((bool)1);
		IntPtr_t L_2 = __this->get_z_stream_6();
		V_0 = L_2;
		IntPtr_t L_3 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		__this->set_z_stream_6(L_3);
		V_1 = 0;
		IntPtr_t L_4 = V_0;
		IntPtr_t L_5 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		bool L_6 = IntPtr_op_Inequality_m3044532593(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0043;
		}
	}
	{
		IntPtr_t L_7 = V_0;
		int32_t L_8 = DeflateStream_CloseZStream_m1553633498(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		V_1 = L_8;
	}

IL_0043:
	{
		__this->set_io_buffer_7((ByteU5BU5D_t3397334013*)NULL);
		bool L_9 = __this->get_leaveOpen_3();
		if (L_9)
		{
			goto IL_006f;
		}
	}
	{
		Stream_t3255436806 * L_10 = __this->get_base_stream_1();
		V_2 = L_10;
		Stream_t3255436806 * L_11 = V_2;
		if (!L_11)
		{
			goto IL_0068;
		}
	}
	{
		Stream_t3255436806 * L_12 = V_2;
		NullCheck(L_12);
		VirtActionInvoker0::Invoke(12 /* System.Void System.IO.Stream::Close() */, L_12);
	}

IL_0068:
	{
		__this->set_base_stream_1((Stream_t3255436806 *)NULL);
	}

IL_006f:
	{
		int32_t L_13 = V_1;
		DeflateStream_CheckResult_m390537652(NULL /*static, unused*/, L_13, _stringLiteral2402255043, /*hidden argument*/NULL);
	}

IL_007a:
	{
		GCHandle_t3409268066 * L_14 = __this->get_address_of_data_8();
		bool L_15 = GCHandle_get_IsAllocated_m2246567034(L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_00a4;
		}
	}
	{
		GCHandle_t3409268066 * L_16 = __this->get_address_of_data_8();
		GCHandle_Free_m1639542352(L_16, /*hidden argument*/NULL);
		Initobj (GCHandle_t3409268066_il2cpp_TypeInfo_var, (&V_3));
		GCHandle_t3409268066  L_17 = V_3;
		__this->set_data_8(L_17);
	}

IL_00a4:
	{
		bool L_18 = ___disposing0;
		Stream_Dispose_m440254723(__this, L_18, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 System.IO.Compression.DeflateStream::UnmanagedRead(System.IntPtr,System.Int32,System.IntPtr)
extern Il2CppClass* DeflateStream_t3198596725_il2cpp_TypeInfo_var;
extern const uint32_t DeflateStream_UnmanagedRead_m3972503109_MetadataUsageId;
extern "C"  int32_t DeflateStream_UnmanagedRead_m3972503109 (Il2CppObject * __this /* static, unused */, IntPtr_t ___buffer0, int32_t ___length1, IntPtr_t ___data2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DeflateStream_UnmanagedRead_m3972503109_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GCHandle_t3409268066  V_0;
	memset(&V_0, 0, sizeof(V_0));
	DeflateStream_t3198596725 * V_1 = NULL;
	{
		IntPtr_t L_0 = ___data2;
		GCHandle_t3409268066  L_1 = GCHandle_FromIntPtr_m390042100(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Il2CppObject * L_2 = GCHandle_get_Target_m2327042781((&V_0), /*hidden argument*/NULL);
		V_1 = ((DeflateStream_t3198596725 *)IsInstClass(L_2, DeflateStream_t3198596725_il2cpp_TypeInfo_var));
		DeflateStream_t3198596725 * L_3 = V_1;
		if (L_3)
		{
			goto IL_001c;
		}
	}
	{
		return (-1);
	}

IL_001c:
	{
		DeflateStream_t3198596725 * L_4 = V_1;
		IntPtr_t L_5 = ___buffer0;
		int32_t L_6 = ___length1;
		NullCheck(L_4);
		int32_t L_7 = DeflateStream_UnmanagedRead_m1789526367(L_4, L_5, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
extern "C" int32_t DEFAULT_CALL ReversePInvokeWrapper_DeflateStream_UnmanagedRead_m3972503109(intptr_t ___buffer0, int32_t ___length1, intptr_t ___data2)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___buffer0' to managed representation
	IntPtr_t ____buffer0_unmarshaled;
	____buffer0_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)(___buffer0)));

	// Marshaling of parameter '___data2' to managed representation
	IntPtr_t ____data2_unmarshaled;
	____data2_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)(___data2)));

	// Managed method invocation
	int32_t returnValue = ::DeflateStream_UnmanagedRead_m3972503109(NULL, ____buffer0_unmarshaled, ___length1, ____data2_unmarshaled, NULL);

	return returnValue;
}
// System.Int32 System.IO.Compression.DeflateStream::UnmanagedRead(System.IntPtr,System.Int32)
extern Il2CppClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern Il2CppClass* Marshal_t785896760_il2cpp_TypeInfo_var;
extern const uint32_t DeflateStream_UnmanagedRead_m1789526367_MetadataUsageId;
extern "C"  int32_t DeflateStream_UnmanagedRead_m1789526367 (DeflateStream_t3198596725 * __this, IntPtr_t ___buffer0, int32_t ___length1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DeflateStream_UnmanagedRead_m1789526367_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		V_0 = 0;
		V_1 = 1;
		goto IL_0075;
	}

IL_0009:
	{
		ByteU5BU5D_t3397334013* L_0 = __this->get_io_buffer_7();
		if (L_0)
		{
			goto IL_0024;
		}
	}
	{
		__this->set_io_buffer_7(((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)((int32_t)4096))));
	}

IL_0024:
	{
		int32_t L_1 = ___length1;
		ByteU5BU5D_t3397334013* L_2 = __this->get_io_buffer_7();
		NullCheck(L_2);
		int32_t L_3 = Math_Min_m4290821911(NULL /*static, unused*/, L_1, (((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length)))), /*hidden argument*/NULL);
		V_2 = L_3;
		Stream_t3255436806 * L_4 = __this->get_base_stream_1();
		ByteU5BU5D_t3397334013* L_5 = __this->get_io_buffer_7();
		int32_t L_6 = V_2;
		NullCheck(L_4);
		int32_t L_7 = VirtFuncInvoker3< int32_t, ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(16 /* System.Int32 System.IO.Stream::Read(System.Byte[],System.Int32,System.Int32) */, L_4, L_5, 0, L_6);
		V_1 = L_7;
		int32_t L_8 = V_1;
		if ((((int32_t)L_8) <= ((int32_t)0)))
		{
			goto IL_0075;
		}
	}
	{
		ByteU5BU5D_t3397334013* L_9 = __this->get_io_buffer_7();
		IntPtr_t L_10 = ___buffer0;
		int32_t L_11 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t785896760_il2cpp_TypeInfo_var);
		Marshal_Copy_m3575923452(NULL /*static, unused*/, L_9, 0, L_10, L_11, /*hidden argument*/NULL);
		void* L_12 = IntPtr_ToPointer_m1888290092((&___buffer0), /*hidden argument*/NULL);
		int32_t L_13 = V_1;
		IntPtr__ctor_m3033286303((&___buffer0), (void*)(void*)((void*)((intptr_t)L_12+(int32_t)L_13)), /*hidden argument*/NULL);
		int32_t L_14 = ___length1;
		int32_t L_15 = V_1;
		___length1 = ((int32_t)((int32_t)L_14-(int32_t)L_15));
		int32_t L_16 = V_0;
		int32_t L_17 = V_1;
		V_0 = ((int32_t)((int32_t)L_16+(int32_t)L_17));
	}

IL_0075:
	{
		int32_t L_18 = ___length1;
		if ((((int32_t)L_18) <= ((int32_t)0)))
		{
			goto IL_0083;
		}
	}
	{
		int32_t L_19 = V_1;
		if ((((int32_t)L_19) > ((int32_t)0)))
		{
			goto IL_0009;
		}
	}

IL_0083:
	{
		int32_t L_20 = V_0;
		return L_20;
	}
}
// System.Int32 System.IO.Compression.DeflateStream::UnmanagedWrite(System.IntPtr,System.Int32,System.IntPtr)
extern Il2CppClass* DeflateStream_t3198596725_il2cpp_TypeInfo_var;
extern const uint32_t DeflateStream_UnmanagedWrite_m2053312812_MetadataUsageId;
extern "C"  int32_t DeflateStream_UnmanagedWrite_m2053312812 (Il2CppObject * __this /* static, unused */, IntPtr_t ___buffer0, int32_t ___length1, IntPtr_t ___data2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DeflateStream_UnmanagedWrite_m2053312812_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GCHandle_t3409268066  V_0;
	memset(&V_0, 0, sizeof(V_0));
	DeflateStream_t3198596725 * V_1 = NULL;
	{
		IntPtr_t L_0 = ___data2;
		GCHandle_t3409268066  L_1 = GCHandle_FromIntPtr_m390042100(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Il2CppObject * L_2 = GCHandle_get_Target_m2327042781((&V_0), /*hidden argument*/NULL);
		V_1 = ((DeflateStream_t3198596725 *)IsInstClass(L_2, DeflateStream_t3198596725_il2cpp_TypeInfo_var));
		DeflateStream_t3198596725 * L_3 = V_1;
		if (L_3)
		{
			goto IL_001c;
		}
	}
	{
		return (-1);
	}

IL_001c:
	{
		DeflateStream_t3198596725 * L_4 = V_1;
		IntPtr_t L_5 = ___buffer0;
		int32_t L_6 = ___length1;
		NullCheck(L_4);
		int32_t L_7 = DeflateStream_UnmanagedWrite_m3254658624(L_4, L_5, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
extern "C" int32_t DEFAULT_CALL ReversePInvokeWrapper_DeflateStream_UnmanagedWrite_m2053312812(intptr_t ___buffer0, int32_t ___length1, intptr_t ___data2)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___buffer0' to managed representation
	IntPtr_t ____buffer0_unmarshaled;
	____buffer0_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)(___buffer0)));

	// Marshaling of parameter '___data2' to managed representation
	IntPtr_t ____data2_unmarshaled;
	____data2_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)(___data2)));

	// Managed method invocation
	int32_t returnValue = ::DeflateStream_UnmanagedWrite_m2053312812(NULL, ____buffer0_unmarshaled, ___length1, ____data2_unmarshaled, NULL);

	return returnValue;
}
// System.Int32 System.IO.Compression.DeflateStream::UnmanagedWrite(System.IntPtr,System.Int32)
extern Il2CppClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern Il2CppClass* Marshal_t785896760_il2cpp_TypeInfo_var;
extern const uint32_t DeflateStream_UnmanagedWrite_m3254658624_MetadataUsageId;
extern "C"  int32_t DeflateStream_UnmanagedWrite_m3254658624 (DeflateStream_t3198596725 * __this, IntPtr_t ___buffer0, int32_t ___length1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DeflateStream_UnmanagedWrite_m3254658624_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = 0;
		goto IL_006b;
	}

IL_0007:
	{
		ByteU5BU5D_t3397334013* L_0 = __this->get_io_buffer_7();
		if (L_0)
		{
			goto IL_0022;
		}
	}
	{
		__this->set_io_buffer_7(((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)((int32_t)4096))));
	}

IL_0022:
	{
		int32_t L_1 = ___length1;
		ByteU5BU5D_t3397334013* L_2 = __this->get_io_buffer_7();
		NullCheck(L_2);
		int32_t L_3 = Math_Min_m4290821911(NULL /*static, unused*/, L_1, (((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length)))), /*hidden argument*/NULL);
		V_1 = L_3;
		IntPtr_t L_4 = ___buffer0;
		ByteU5BU5D_t3397334013* L_5 = __this->get_io_buffer_7();
		int32_t L_6 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t785896760_il2cpp_TypeInfo_var);
		Marshal_Copy_m1683535972(NULL /*static, unused*/, L_4, L_5, 0, L_6, /*hidden argument*/NULL);
		Stream_t3255436806 * L_7 = __this->get_base_stream_1();
		ByteU5BU5D_t3397334013* L_8 = __this->get_io_buffer_7();
		int32_t L_9 = V_1;
		NullCheck(L_7);
		VirtActionInvoker3< ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(20 /* System.Void System.IO.Stream::Write(System.Byte[],System.Int32,System.Int32) */, L_7, L_8, 0, L_9);
		void* L_10 = IntPtr_ToPointer_m1888290092((&___buffer0), /*hidden argument*/NULL);
		int32_t L_11 = V_1;
		IntPtr__ctor_m3033286303((&___buffer0), (void*)(void*)((void*)((intptr_t)L_10+(int32_t)L_11)), /*hidden argument*/NULL);
		int32_t L_12 = ___length1;
		int32_t L_13 = V_1;
		___length1 = ((int32_t)((int32_t)L_12-(int32_t)L_13));
		int32_t L_14 = V_0;
		int32_t L_15 = V_1;
		V_0 = ((int32_t)((int32_t)L_14+(int32_t)L_15));
	}

IL_006b:
	{
		int32_t L_16 = ___length1;
		if ((((int32_t)L_16) > ((int32_t)0)))
		{
			goto IL_0007;
		}
	}
	{
		int32_t L_17 = V_0;
		return L_17;
	}
}
// System.Int32 System.IO.Compression.DeflateStream::ReadInternal(System.Byte[],System.Int32,System.Int32)
extern Il2CppCodeGenString* _stringLiteral2825034745;
extern const uint32_t DeflateStream_ReadInternal_m1462513020_MetadataUsageId;
extern "C"  int32_t DeflateStream_ReadInternal_m1462513020 (DeflateStream_t3198596725 * __this, ByteU5BU5D_t3397334013* ___array0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DeflateStream_ReadInternal_m1462513020_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	uint8_t* V_1 = NULL;
	IntPtr_t V_2;
	memset(&V_2, 0, sizeof(V_2));
	uintptr_t G_B6_0 = 0;
	{
		int32_t L_0 = ___count2;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return 0;
	}

IL_0008:
	{
		V_0 = 0;
		ByteU5BU5D_t3397334013* L_1 = ___array0;
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		ByteU5BU5D_t3397334013* L_2 = ___array0;
		NullCheck(L_2);
		if ((((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length)))))
		{
			goto IL_001f;
		}
	}

IL_0018:
	{
		G_B6_0 = (((uintptr_t)0));
		goto IL_0026;
	}

IL_001f:
	{
		ByteU5BU5D_t3397334013* L_3 = ___array0;
		NullCheck(L_3);
		G_B6_0 = ((uintptr_t)(((L_3)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))));
	}

IL_0026:
	{
		V_1 = (uint8_t*)G_B6_0;
		uint8_t* L_4 = V_1;
		int32_t L_5 = ___offset1;
		IntPtr__ctor_m3033286303((&V_2), (void*)(void*)((uint8_t*)((intptr_t)L_4+(int32_t)L_5)), /*hidden argument*/NULL);
		IntPtr_t L_6 = __this->get_z_stream_6();
		IntPtr_t L_7 = V_2;
		int32_t L_8 = ___count2;
		int32_t L_9 = DeflateStream_ReadZStream_m4150396437(NULL /*static, unused*/, L_6, L_7, L_8, /*hidden argument*/NULL);
		V_0 = L_9;
		V_1 = (uint8_t*)(((uintptr_t)0));
		int32_t L_10 = V_0;
		DeflateStream_CheckResult_m390537652(NULL /*static, unused*/, L_10, _stringLiteral2825034745, /*hidden argument*/NULL);
		int32_t L_11 = V_0;
		return L_11;
	}
}
// System.Int32 System.IO.Compression.DeflateStream::Read(System.Byte[],System.Int32,System.Int32)
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1660902408;
extern Il2CppCodeGenString* _stringLiteral1664233771;
extern Il2CppCodeGenString* _stringLiteral592973055;
extern Il2CppCodeGenString* _stringLiteral2288770012;
extern Il2CppCodeGenString* _stringLiteral2944076390;
extern const uint32_t DeflateStream_Read_m2906745725_MetadataUsageId;
extern "C"  int32_t DeflateStream_Read_m2906745725 (DeflateStream_t3198596725 * __this, ByteU5BU5D_t3397334013* ___dest0, int32_t ___dest_offset1, int32_t ___count2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DeflateStream_Read_m2906745725_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		bool L_0 = __this->get_disposed_4();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		Type_t * L_1 = Object_GetType_m191970594(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, L_1);
		ObjectDisposedException_t2695136451 * L_3 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_3, L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001c:
	{
		ByteU5BU5D_t3397334013* L_4 = ___dest0;
		if (L_4)
		{
			goto IL_002d;
		}
	}
	{
		ArgumentNullException_t628810857 * L_5 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_5, _stringLiteral1660902408, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_002d:
	{
		bool L_6 = VirtFuncInvoker0< bool >::Invoke(5 /* System.Boolean System.IO.Compression.DeflateStream::get_CanRead() */, __this);
		if (L_6)
		{
			goto IL_0043;
		}
	}
	{
		InvalidOperationException_t721527559 * L_7 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_7, _stringLiteral1664233771, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_0043:
	{
		ByteU5BU5D_t3397334013* L_8 = ___dest0;
		NullCheck(L_8);
		V_0 = (((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length))));
		int32_t L_9 = ___dest_offset1;
		if ((((int32_t)L_9) < ((int32_t)0)))
		{
			goto IL_0055;
		}
	}
	{
		int32_t L_10 = ___count2;
		if ((((int32_t)L_10) >= ((int32_t)0)))
		{
			goto IL_0060;
		}
	}

IL_0055:
	{
		ArgumentException_t3259014390 * L_11 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_11, _stringLiteral592973055, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_11);
	}

IL_0060:
	{
		int32_t L_12 = ___dest_offset1;
		int32_t L_13 = V_0;
		if ((((int32_t)L_12) <= ((int32_t)L_13)))
		{
			goto IL_0072;
		}
	}
	{
		ArgumentException_t3259014390 * L_14 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_14, _stringLiteral2288770012, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_14);
	}

IL_0072:
	{
		int32_t L_15 = ___dest_offset1;
		int32_t L_16 = ___count2;
		int32_t L_17 = V_0;
		if ((((int32_t)((int32_t)((int32_t)L_15+(int32_t)L_16))) <= ((int32_t)L_17)))
		{
			goto IL_0086;
		}
	}
	{
		ArgumentException_t3259014390 * L_18 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_18, _stringLiteral2944076390, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_18);
	}

IL_0086:
	{
		ByteU5BU5D_t3397334013* L_19 = ___dest0;
		int32_t L_20 = ___dest_offset1;
		int32_t L_21 = ___count2;
		int32_t L_22 = DeflateStream_ReadInternal_m1462513020(__this, L_19, L_20, L_21, /*hidden argument*/NULL);
		return L_22;
	}
}
// System.Void System.IO.Compression.DeflateStream::WriteInternal(System.Byte[],System.Int32,System.Int32)
extern Il2CppCodeGenString* _stringLiteral3697212408;
extern const uint32_t DeflateStream_WriteInternal_m1685486359_MetadataUsageId;
extern "C"  void DeflateStream_WriteInternal_m1685486359 (DeflateStream_t3198596725 * __this, ByteU5BU5D_t3397334013* ___array0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DeflateStream_WriteInternal_m1685486359_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	uint8_t* V_1 = NULL;
	IntPtr_t V_2;
	memset(&V_2, 0, sizeof(V_2));
	uintptr_t G_B6_0 = 0;
	{
		int32_t L_0 = ___count2;
		if (L_0)
		{
			goto IL_0007;
		}
	}
	{
		return;
	}

IL_0007:
	{
		V_0 = 0;
		ByteU5BU5D_t3397334013* L_1 = ___array0;
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		ByteU5BU5D_t3397334013* L_2 = ___array0;
		NullCheck(L_2);
		if ((((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length)))))
		{
			goto IL_001e;
		}
	}

IL_0017:
	{
		G_B6_0 = (((uintptr_t)0));
		goto IL_0025;
	}

IL_001e:
	{
		ByteU5BU5D_t3397334013* L_3 = ___array0;
		NullCheck(L_3);
		G_B6_0 = ((uintptr_t)(((L_3)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))));
	}

IL_0025:
	{
		V_1 = (uint8_t*)G_B6_0;
		uint8_t* L_4 = V_1;
		int32_t L_5 = ___offset1;
		IntPtr__ctor_m3033286303((&V_2), (void*)(void*)((uint8_t*)((intptr_t)L_4+(int32_t)L_5)), /*hidden argument*/NULL);
		IntPtr_t L_6 = __this->get_z_stream_6();
		IntPtr_t L_7 = V_2;
		int32_t L_8 = ___count2;
		int32_t L_9 = DeflateStream_WriteZStream_m2377915276(NULL /*static, unused*/, L_6, L_7, L_8, /*hidden argument*/NULL);
		V_0 = L_9;
		V_1 = (uint8_t*)(((uintptr_t)0));
		int32_t L_10 = V_0;
		DeflateStream_CheckResult_m390537652(NULL /*static, unused*/, L_10, _stringLiteral3697212408, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.IO.Compression.DeflateStream::Write(System.Byte[],System.Int32,System.Int32)
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral386853832;
extern Il2CppCodeGenString* _stringLiteral3512041062;
extern Il2CppCodeGenString* _stringLiteral1554746267;
extern Il2CppCodeGenString* _stringLiteral1660180179;
extern const uint32_t DeflateStream_Write_m3722706532_MetadataUsageId;
extern "C"  void DeflateStream_Write_m3722706532 (DeflateStream_t3198596725 * __this, ByteU5BU5D_t3397334013* ___src0, int32_t ___src_offset1, int32_t ___count2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DeflateStream_Write_m3722706532_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_disposed_4();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		Type_t * L_1 = Object_GetType_m191970594(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, L_1);
		ObjectDisposedException_t2695136451 * L_3 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_3, L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001c:
	{
		ByteU5BU5D_t3397334013* L_4 = ___src0;
		if (L_4)
		{
			goto IL_002d;
		}
	}
	{
		ArgumentNullException_t628810857 * L_5 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_5, _stringLiteral386853832, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_002d:
	{
		int32_t L_6 = ___src_offset1;
		if ((((int32_t)L_6) >= ((int32_t)0)))
		{
			goto IL_003f;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_7 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_7, _stringLiteral3512041062, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_003f:
	{
		int32_t L_8 = ___count2;
		if ((((int32_t)L_8) >= ((int32_t)0)))
		{
			goto IL_0051;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_9 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_9, _stringLiteral1554746267, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0051:
	{
		bool L_10 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.IO.Compression.DeflateStream::get_CanWrite() */, __this);
		if (L_10)
		{
			goto IL_0067;
		}
	}
	{
		NotSupportedException_t1793819818 * L_11 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_11, _stringLiteral1660180179, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_11);
	}

IL_0067:
	{
		ByteU5BU5D_t3397334013* L_12 = ___src0;
		int32_t L_13 = ___src_offset1;
		int32_t L_14 = ___count2;
		DeflateStream_WriteInternal_m1685486359(__this, L_12, L_13, L_14, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.IO.Compression.DeflateStream::CheckResult(System.Int32,System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* IOException_t2458421087_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1421870506;
extern Il2CppCodeGenString* _stringLiteral3327755779;
extern Il2CppCodeGenString* _stringLiteral2442942850;
extern Il2CppCodeGenString* _stringLiteral3868087864;
extern Il2CppCodeGenString* _stringLiteral3992420715;
extern Il2CppCodeGenString* _stringLiteral2364697139;
extern Il2CppCodeGenString* _stringLiteral3440112500;
extern Il2CppCodeGenString* _stringLiteral3954571644;
extern Il2CppCodeGenString* _stringLiteral372029310;
extern const uint32_t DeflateStream_CheckResult_m390537652_MetadataUsageId;
extern "C"  void DeflateStream_CheckResult_m390537652 (Il2CppObject * __this /* static, unused */, int32_t ___result0, String_t* ___where1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DeflateStream_CheckResult_m390537652_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	{
		int32_t L_0 = ___result0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0008;
		}
	}
	{
		return;
	}

IL_0008:
	{
		int32_t L_1 = ___result0;
		V_1 = L_1;
		int32_t L_2 = V_1;
		if (((int32_t)((int32_t)L_2+(int32_t)((int32_t)11))) == 0)
		{
			goto IL_0091;
		}
		if (((int32_t)((int32_t)L_2+(int32_t)((int32_t)11))) == 1)
		{
			goto IL_0086;
		}
		if (((int32_t)((int32_t)L_2+(int32_t)((int32_t)11))) == 2)
		{
			goto IL_009c;
		}
		if (((int32_t)((int32_t)L_2+(int32_t)((int32_t)11))) == 3)
		{
			goto IL_009c;
		}
		if (((int32_t)((int32_t)L_2+(int32_t)((int32_t)11))) == 4)
		{
			goto IL_009c;
		}
		if (((int32_t)((int32_t)L_2+(int32_t)((int32_t)11))) == 5)
		{
			goto IL_007b;
		}
		if (((int32_t)((int32_t)L_2+(int32_t)((int32_t)11))) == 6)
		{
			goto IL_0070;
		}
		if (((int32_t)((int32_t)L_2+(int32_t)((int32_t)11))) == 7)
		{
			goto IL_0065;
		}
		if (((int32_t)((int32_t)L_2+(int32_t)((int32_t)11))) == 8)
		{
			goto IL_005a;
		}
		if (((int32_t)((int32_t)L_2+(int32_t)((int32_t)11))) == 9)
		{
			goto IL_004f;
		}
		if (((int32_t)((int32_t)L_2+(int32_t)((int32_t)11))) == 10)
		{
			goto IL_0044;
		}
	}
	{
		goto IL_009c;
	}

IL_0044:
	{
		V_0 = _stringLiteral1421870506;
		goto IL_00a7;
	}

IL_004f:
	{
		V_0 = _stringLiteral3327755779;
		goto IL_00a7;
	}

IL_005a:
	{
		V_0 = _stringLiteral2442942850;
		goto IL_00a7;
	}

IL_0065:
	{
		V_0 = _stringLiteral3868087864;
		goto IL_00a7;
	}

IL_0070:
	{
		V_0 = _stringLiteral3992420715;
		goto IL_00a7;
	}

IL_007b:
	{
		V_0 = _stringLiteral2364697139;
		goto IL_00a7;
	}

IL_0086:
	{
		V_0 = _stringLiteral3440112500;
		goto IL_00a7;
	}

IL_0091:
	{
		V_0 = _stringLiteral3954571644;
		goto IL_00a7;
	}

IL_009c:
	{
		V_0 = _stringLiteral1421870506;
		goto IL_00a7;
	}

IL_00a7:
	{
		String_t* L_3 = V_0;
		String_t* L_4 = ___where1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m612901809(NULL /*static, unused*/, L_3, _stringLiteral372029310, L_4, /*hidden argument*/NULL);
		IOException_t2458421087 * L_6 = (IOException_t2458421087 *)il2cpp_codegen_object_new(IOException_t2458421087_il2cpp_TypeInfo_var);
		IOException__ctor_m3496190950(L_6, L_5, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}
}
// System.Void System.IO.Compression.DeflateStream::Flush()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3033397600;
extern const uint32_t DeflateStream_Flush_m3971344454_MetadataUsageId;
extern "C"  void DeflateStream_Flush_m3971344454 (DeflateStream_t3198596725 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DeflateStream_Flush_m3971344454_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		bool L_0 = __this->get_disposed_4();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		Type_t * L_1 = Object_GetType_m191970594(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, L_1);
		ObjectDisposedException_t2695136451 * L_3 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_3, L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001c:
	{
		bool L_4 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.IO.Compression.DeflateStream::get_CanWrite() */, __this);
		if (!L_4)
		{
			goto IL_003e;
		}
	}
	{
		IntPtr_t L_5 = __this->get_z_stream_6();
		int32_t L_6 = DeflateStream_Flush_m2824917332(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		int32_t L_7 = V_0;
		DeflateStream_CheckResult_m390537652(NULL /*static, unused*/, L_7, _stringLiteral3033397600, /*hidden argument*/NULL);
	}

IL_003e:
	{
		return;
	}
}
// System.IAsyncResult System.IO.Compression.DeflateStream::BeginRead(System.Byte[],System.Int32,System.Int32,System.AsyncCallback,System.Object)
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppClass* ReadMethod_t3362229488_il2cpp_TypeInfo_var;
extern const MethodInfo* DeflateStream_ReadInternal_m1462513020_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3900380547;
extern Il2CppCodeGenString* _stringLiteral2713104920;
extern Il2CppCodeGenString* _stringLiteral1554746267;
extern Il2CppCodeGenString* _stringLiteral81997655;
extern Il2CppCodeGenString* _stringLiteral1673268925;
extern Il2CppCodeGenString* _stringLiteral1625371405;
extern const uint32_t DeflateStream_BeginRead_m1772882044_MetadataUsageId;
extern "C"  Il2CppObject * DeflateStream_BeginRead_m1772882044 (DeflateStream_t3198596725 * __this, ByteU5BU5D_t3397334013* ___buffer0, int32_t ___offset1, int32_t ___count2, AsyncCallback_t163412349 * ___cback3, Il2CppObject * ___state4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DeflateStream_BeginRead_m1772882044_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ReadMethod_t3362229488 * V_0 = NULL;
	{
		bool L_0 = __this->get_disposed_4();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		Type_t * L_1 = Object_GetType_m191970594(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, L_1);
		ObjectDisposedException_t2695136451 * L_3 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_3, L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001c:
	{
		bool L_4 = VirtFuncInvoker0< bool >::Invoke(5 /* System.Boolean System.IO.Compression.DeflateStream::get_CanRead() */, __this);
		if (L_4)
		{
			goto IL_0032;
		}
	}
	{
		NotSupportedException_t1793819818 * L_5 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_5, _stringLiteral3900380547, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0032:
	{
		ByteU5BU5D_t3397334013* L_6 = ___buffer0;
		if (L_6)
		{
			goto IL_0043;
		}
	}
	{
		ArgumentNullException_t628810857 * L_7 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_7, _stringLiteral2713104920, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_0043:
	{
		int32_t L_8 = ___count2;
		if ((((int32_t)L_8) >= ((int32_t)0)))
		{
			goto IL_005a;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_9 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m4234257711(L_9, _stringLiteral1554746267, _stringLiteral81997655, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_005a:
	{
		int32_t L_10 = ___offset1;
		if ((((int32_t)L_10) >= ((int32_t)0)))
		{
			goto IL_0071;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_11 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m4234257711(L_11, _stringLiteral1673268925, _stringLiteral81997655, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_11);
	}

IL_0071:
	{
		int32_t L_12 = ___count2;
		int32_t L_13 = ___offset1;
		ByteU5BU5D_t3397334013* L_14 = ___buffer0;
		NullCheck(L_14);
		if ((((int32_t)((int32_t)((int32_t)L_12+(int32_t)L_13))) <= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length)))))))
		{
			goto IL_0087;
		}
	}
	{
		ArgumentException_t3259014390 * L_15 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_15, _stringLiteral1625371405, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_15);
	}

IL_0087:
	{
		IntPtr_t L_16;
		L_16.set_m_value_0((void*)(void*)DeflateStream_ReadInternal_m1462513020_MethodInfo_var);
		ReadMethod_t3362229488 * L_17 = (ReadMethod_t3362229488 *)il2cpp_codegen_object_new(ReadMethod_t3362229488_il2cpp_TypeInfo_var);
		ReadMethod__ctor_m626043682(L_17, __this, L_16, /*hidden argument*/NULL);
		V_0 = L_17;
		ReadMethod_t3362229488 * L_18 = V_0;
		ByteU5BU5D_t3397334013* L_19 = ___buffer0;
		int32_t L_20 = ___offset1;
		int32_t L_21 = ___count2;
		AsyncCallback_t163412349 * L_22 = ___cback3;
		Il2CppObject * L_23 = ___state4;
		NullCheck(L_18);
		Il2CppObject * L_24 = ReadMethod_BeginInvoke_m3639860050(L_18, L_19, L_20, L_21, L_22, L_23, /*hidden argument*/NULL);
		return L_24;
	}
}
// System.IAsyncResult System.IO.Compression.DeflateStream::BeginWrite(System.Byte[],System.Int32,System.Int32,System.AsyncCallback,System.Object)
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppClass* WriteMethod_t1894833619_il2cpp_TypeInfo_var;
extern const MethodInfo* DeflateStream_WriteInternal_m1685486359_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4227906053;
extern Il2CppCodeGenString* _stringLiteral2713104920;
extern Il2CppCodeGenString* _stringLiteral1554746267;
extern Il2CppCodeGenString* _stringLiteral81997655;
extern Il2CppCodeGenString* _stringLiteral1673268925;
extern Il2CppCodeGenString* _stringLiteral1625371405;
extern const uint32_t DeflateStream_BeginWrite_m232975127_MetadataUsageId;
extern "C"  Il2CppObject * DeflateStream_BeginWrite_m232975127 (DeflateStream_t3198596725 * __this, ByteU5BU5D_t3397334013* ___buffer0, int32_t ___offset1, int32_t ___count2, AsyncCallback_t163412349 * ___cback3, Il2CppObject * ___state4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DeflateStream_BeginWrite_m232975127_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	WriteMethod_t1894833619 * V_0 = NULL;
	{
		bool L_0 = __this->get_disposed_4();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		Type_t * L_1 = Object_GetType_m191970594(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, L_1);
		ObjectDisposedException_t2695136451 * L_3 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_3, L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001c:
	{
		bool L_4 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.IO.Compression.DeflateStream::get_CanWrite() */, __this);
		if (L_4)
		{
			goto IL_0032;
		}
	}
	{
		InvalidOperationException_t721527559 * L_5 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_5, _stringLiteral4227906053, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0032:
	{
		ByteU5BU5D_t3397334013* L_6 = ___buffer0;
		if (L_6)
		{
			goto IL_0043;
		}
	}
	{
		ArgumentNullException_t628810857 * L_7 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_7, _stringLiteral2713104920, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_0043:
	{
		int32_t L_8 = ___count2;
		if ((((int32_t)L_8) >= ((int32_t)0)))
		{
			goto IL_005a;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_9 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m4234257711(L_9, _stringLiteral1554746267, _stringLiteral81997655, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_005a:
	{
		int32_t L_10 = ___offset1;
		if ((((int32_t)L_10) >= ((int32_t)0)))
		{
			goto IL_0071;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_11 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m4234257711(L_11, _stringLiteral1673268925, _stringLiteral81997655, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_11);
	}

IL_0071:
	{
		int32_t L_12 = ___count2;
		int32_t L_13 = ___offset1;
		ByteU5BU5D_t3397334013* L_14 = ___buffer0;
		NullCheck(L_14);
		if ((((int32_t)((int32_t)((int32_t)L_12+(int32_t)L_13))) <= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length)))))))
		{
			goto IL_0087;
		}
	}
	{
		ArgumentException_t3259014390 * L_15 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_15, _stringLiteral1625371405, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_15);
	}

IL_0087:
	{
		IntPtr_t L_16;
		L_16.set_m_value_0((void*)(void*)DeflateStream_WriteInternal_m1685486359_MethodInfo_var);
		WriteMethod_t1894833619 * L_17 = (WriteMethod_t1894833619 *)il2cpp_codegen_object_new(WriteMethod_t1894833619_il2cpp_TypeInfo_var);
		WriteMethod__ctor_m2748889395(L_17, __this, L_16, /*hidden argument*/NULL);
		V_0 = L_17;
		WriteMethod_t1894833619 * L_18 = V_0;
		ByteU5BU5D_t3397334013* L_19 = ___buffer0;
		int32_t L_20 = ___offset1;
		int32_t L_21 = ___count2;
		AsyncCallback_t163412349 * L_22 = ___cback3;
		Il2CppObject * L_23 = ___state4;
		NullCheck(L_18);
		Il2CppObject * L_24 = WriteMethod_BeginInvoke_m3664291003(L_18, L_19, L_20, L_21, L_22, L_23, /*hidden argument*/NULL);
		return L_24;
	}
}
// System.Int32 System.IO.Compression.DeflateStream::EndRead(System.IAsyncResult)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppClass* AsyncResult_t2232356043_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppClass* ReadMethod_t3362229488_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral521266722;
extern Il2CppCodeGenString* _stringLiteral2085105847;
extern const uint32_t DeflateStream_EndRead_m1965323294_MetadataUsageId;
extern "C"  int32_t DeflateStream_EndRead_m1965323294 (DeflateStream_t3198596725 * __this, Il2CppObject * ___async_result0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DeflateStream_EndRead_m1965323294_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AsyncResult_t2232356043 * V_0 = NULL;
	ReadMethod_t3362229488 * V_1 = NULL;
	{
		Il2CppObject * L_0 = ___async_result0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, _stringLiteral521266722, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Il2CppObject * L_2 = ___async_result0;
		V_0 = ((AsyncResult_t2232356043 *)IsInstClass(L_2, AsyncResult_t2232356043_il2cpp_TypeInfo_var));
		AsyncResult_t2232356043 * L_3 = V_0;
		if (L_3)
		{
			goto IL_002e;
		}
	}
	{
		ArgumentException_t3259014390 * L_4 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m544251339(L_4, _stringLiteral2085105847, _stringLiteral521266722, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_002e:
	{
		AsyncResult_t2232356043 * L_5 = V_0;
		NullCheck(L_5);
		Il2CppObject * L_6 = VirtFuncInvoker0< Il2CppObject * >::Invoke(12 /* System.Object System.Runtime.Remoting.Messaging.AsyncResult::get_AsyncDelegate() */, L_5);
		V_1 = ((ReadMethod_t3362229488 *)IsInstSealed(L_6, ReadMethod_t3362229488_il2cpp_TypeInfo_var));
		ReadMethod_t3362229488 * L_7 = V_1;
		if (L_7)
		{
			goto IL_0050;
		}
	}
	{
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m544251339(L_8, _stringLiteral2085105847, _stringLiteral521266722, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_0050:
	{
		ReadMethod_t3362229488 * L_9 = V_1;
		Il2CppObject * L_10 = ___async_result0;
		NullCheck(L_9);
		int32_t L_11 = ReadMethod_EndInvoke_m2107115960(L_9, L_10, /*hidden argument*/NULL);
		return L_11;
	}
}
// System.Void System.IO.Compression.DeflateStream::EndWrite(System.IAsyncResult)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppClass* AsyncResult_t2232356043_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppClass* WriteMethod_t1894833619_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral521266722;
extern Il2CppCodeGenString* _stringLiteral2085105847;
extern const uint32_t DeflateStream_EndWrite_m644759061_MetadataUsageId;
extern "C"  void DeflateStream_EndWrite_m644759061 (DeflateStream_t3198596725 * __this, Il2CppObject * ___async_result0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DeflateStream_EndWrite_m644759061_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AsyncResult_t2232356043 * V_0 = NULL;
	WriteMethod_t1894833619 * V_1 = NULL;
	{
		Il2CppObject * L_0 = ___async_result0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, _stringLiteral521266722, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Il2CppObject * L_2 = ___async_result0;
		V_0 = ((AsyncResult_t2232356043 *)IsInstClass(L_2, AsyncResult_t2232356043_il2cpp_TypeInfo_var));
		AsyncResult_t2232356043 * L_3 = V_0;
		if (L_3)
		{
			goto IL_002e;
		}
	}
	{
		ArgumentException_t3259014390 * L_4 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m544251339(L_4, _stringLiteral2085105847, _stringLiteral521266722, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_002e:
	{
		AsyncResult_t2232356043 * L_5 = V_0;
		NullCheck(L_5);
		Il2CppObject * L_6 = VirtFuncInvoker0< Il2CppObject * >::Invoke(12 /* System.Object System.Runtime.Remoting.Messaging.AsyncResult::get_AsyncDelegate() */, L_5);
		V_1 = ((WriteMethod_t1894833619 *)IsInstSealed(L_6, WriteMethod_t1894833619_il2cpp_TypeInfo_var));
		WriteMethod_t1894833619 * L_7 = V_1;
		if (L_7)
		{
			goto IL_0050;
		}
	}
	{
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m544251339(L_8, _stringLiteral2085105847, _stringLiteral521266722, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_0050:
	{
		WriteMethod_t1894833619 * L_9 = V_1;
		Il2CppObject * L_10 = ___async_result0;
		NullCheck(L_9);
		WriteMethod_EndInvoke_m2263736537(L_9, L_10, /*hidden argument*/NULL);
		return;
	}
}
// System.Int64 System.IO.Compression.DeflateStream::Seek(System.Int64,System.IO.SeekOrigin)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t DeflateStream_Seek_m125269572_MetadataUsageId;
extern "C"  int64_t DeflateStream_Seek_m125269572 (DeflateStream_t3198596725 * __this, int64_t ___offset0, int32_t ___origin1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DeflateStream_Seek_m125269572_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.IO.Compression.DeflateStream::SetLength(System.Int64)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t DeflateStream_SetLength_m533378364_MetadataUsageId;
extern "C"  void DeflateStream_SetLength_m533378364 (DeflateStream_t3198596725 * __this, int64_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DeflateStream_SetLength_m533378364_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.IO.Compression.DeflateStream::get_CanRead()
extern "C"  bool DeflateStream_get_CanRead_m3431289753 (DeflateStream_t3198596725 * __this, const MethodInfo* method)
{
	int32_t G_B4_0 = 0;
	{
		bool L_0 = __this->get_disposed_4();
		if (L_0)
		{
			goto IL_0023;
		}
	}
	{
		int32_t L_1 = __this->get_mode_2();
		if (L_1)
		{
			goto IL_0023;
		}
	}
	{
		Stream_t3255436806 * L_2 = __this->get_base_stream_1();
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(5 /* System.Boolean System.IO.Stream::get_CanRead() */, L_2);
		G_B4_0 = ((int32_t)(L_3));
		goto IL_0024;
	}

IL_0023:
	{
		G_B4_0 = 0;
	}

IL_0024:
	{
		return (bool)G_B4_0;
	}
}
// System.Boolean System.IO.Compression.DeflateStream::get_CanSeek()
extern "C"  bool DeflateStream_get_CanSeek_m1401569467 (DeflateStream_t3198596725 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean System.IO.Compression.DeflateStream::get_CanWrite()
extern "C"  bool DeflateStream_get_CanWrite_m2090715306 (DeflateStream_t3198596725 * __this, const MethodInfo* method)
{
	int32_t G_B4_0 = 0;
	{
		bool L_0 = __this->get_disposed_4();
		if (L_0)
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_1 = __this->get_mode_2();
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_0024;
		}
	}
	{
		Stream_t3255436806 * L_2 = __this->get_base_stream_1();
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.IO.Stream::get_CanWrite() */, L_2);
		G_B4_0 = ((int32_t)(L_3));
		goto IL_0025;
	}

IL_0024:
	{
		G_B4_0 = 0;
	}

IL_0025:
	{
		return (bool)G_B4_0;
	}
}
// System.Int64 System.IO.Compression.DeflateStream::get_Length()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t DeflateStream_get_Length_m3272162824_MetadataUsageId;
extern "C"  int64_t DeflateStream_get_Length_m3272162824 (DeflateStream_t3198596725 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DeflateStream_get_Length_m3272162824_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Int64 System.IO.Compression.DeflateStream::get_Position()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t DeflateStream_get_Position_m100062355_MetadataUsageId;
extern "C"  int64_t DeflateStream_get_Position_m100062355 (DeflateStream_t3198596725 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DeflateStream_get_Position_m100062355_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.IO.Compression.DeflateStream::set_Position(System.Int64)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t DeflateStream_set_Position_m1897718228_MetadataUsageId;
extern "C"  void DeflateStream_set_Position_m1897718228 (DeflateStream_t3198596725 * __this, int64_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DeflateStream_set_Position_m1897718228_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
extern "C" intptr_t CDECL CreateZStream(int32_t, int32_t, Il2CppMethodPointer, intptr_t);
// System.IntPtr System.IO.Compression.DeflateStream::CreateZStream(System.IO.Compression.CompressionMode,System.Boolean,System.IO.Compression.DeflateStream/UnmanagedReadOrWrite,System.IntPtr)
extern "C"  IntPtr_t DeflateStream_CreateZStream_m2111962461 (Il2CppObject * __this /* static, unused */, int32_t ___compress0, bool ___gzip1, UnmanagedReadOrWrite_t1990215745 * ___feeder2, IntPtr_t ___data3, const MethodInfo* method)
{
	typedef intptr_t (CDECL *PInvokeFunc) (int32_t, int32_t, Il2CppMethodPointer, intptr_t);

	// Marshaling of parameter '___feeder2' to native representation
	Il2CppMethodPointer ____feeder2_marshaled = NULL;
	____feeder2_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___feeder2));

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(CreateZStream)(___compress0, static_cast<int32_t>(___gzip1), ____feeder2_marshaled, reinterpret_cast<intptr_t>((___data3).get_m_value_0()));

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)(returnValue)));

	return _returnValue_unmarshaled;
}
extern "C" int32_t CDECL CloseZStream(intptr_t);
// System.Int32 System.IO.Compression.DeflateStream::CloseZStream(System.IntPtr)
extern "C"  int32_t DeflateStream_CloseZStream_m1553633498 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stream0, const MethodInfo* method)
{
	typedef int32_t (CDECL *PInvokeFunc) (intptr_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(CloseZStream)(reinterpret_cast<intptr_t>((___stream0).get_m_value_0()));

	return returnValue;
}
extern "C" int32_t CDECL Flush(intptr_t);
// System.Int32 System.IO.Compression.DeflateStream::Flush(System.IntPtr)
extern "C"  int32_t DeflateStream_Flush_m2824917332 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stream0, const MethodInfo* method)
{
	typedef int32_t (CDECL *PInvokeFunc) (intptr_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(Flush)(reinterpret_cast<intptr_t>((___stream0).get_m_value_0()));

	return returnValue;
}
extern "C" int32_t CDECL ReadZStream(intptr_t, intptr_t, int32_t);
// System.Int32 System.IO.Compression.DeflateStream::ReadZStream(System.IntPtr,System.IntPtr,System.Int32)
extern "C"  int32_t DeflateStream_ReadZStream_m4150396437 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stream0, IntPtr_t ___buffer1, int32_t ___length2, const MethodInfo* method)
{
	typedef int32_t (CDECL *PInvokeFunc) (intptr_t, intptr_t, int32_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(ReadZStream)(reinterpret_cast<intptr_t>((___stream0).get_m_value_0()), reinterpret_cast<intptr_t>((___buffer1).get_m_value_0()), ___length2);

	return returnValue;
}
extern "C" int32_t CDECL WriteZStream(intptr_t, intptr_t, int32_t);
// System.Int32 System.IO.Compression.DeflateStream::WriteZStream(System.IntPtr,System.IntPtr,System.Int32)
extern "C"  int32_t DeflateStream_WriteZStream_m2377915276 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stream0, IntPtr_t ___buffer1, int32_t ___length2, const MethodInfo* method)
{
	typedef int32_t (CDECL *PInvokeFunc) (intptr_t, intptr_t, int32_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(WriteZStream)(reinterpret_cast<intptr_t>((___stream0).get_m_value_0()), reinterpret_cast<intptr_t>((___buffer1).get_m_value_0()), ___length2);

	return returnValue;
}
// System.Void System.IO.Compression.DeflateStream/ReadMethod::.ctor(System.Object,System.IntPtr)
extern "C"  void ReadMethod__ctor_m626043682 (ReadMethod_t3362229488 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Int32 System.IO.Compression.DeflateStream/ReadMethod::Invoke(System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t ReadMethod_Invoke_m278417217 (ReadMethod_t3362229488 * __this, ByteU5BU5D_t3397334013* ___array0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ReadMethod_Invoke_m278417217((ReadMethod_t3362229488 *)__this->get_prev_9(),___array0, ___offset1, ___count2, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, ByteU5BU5D_t3397334013* ___array0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___array0, ___offset1, ___count2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (void* __this, ByteU5BU5D_t3397334013* ___array0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___array0, ___offset1, ___count2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, int32_t ___offset1, int32_t ___count2, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___array0, ___offset1, ___count2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  int32_t DelegatePInvokeWrapper_ReadMethod_t3362229488 (ReadMethod_t3362229488 * __this, ByteU5BU5D_t3397334013* ___array0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method)
{
	typedef int32_t (STDCALL *PInvokeFunc)(uint8_t*, int32_t, int32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___array0' to native representation
	uint8_t* ____array0_marshaled = NULL;
	if (___array0 != NULL)
	{
		____array0_marshaled = reinterpret_cast<uint8_t*>((___array0)->GetAddressAtUnchecked(0));
	}

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc(____array0_marshaled, ___offset1, ___count2);

	return returnValue;
}
// System.IAsyncResult System.IO.Compression.DeflateStream/ReadMethod::BeginInvoke(System.Byte[],System.Int32,System.Int32,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t ReadMethod_BeginInvoke_m3639860050_MetadataUsageId;
extern "C"  Il2CppObject * ReadMethod_BeginInvoke_m3639860050 (ReadMethod_t3362229488 * __this, ByteU5BU5D_t3397334013* ___array0, int32_t ___offset1, int32_t ___count2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadMethod_BeginInvoke_m3639860050_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = ___array0;
	__d_args[1] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___offset1);
	__d_args[2] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___count2);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Int32 System.IO.Compression.DeflateStream/ReadMethod::EndInvoke(System.IAsyncResult)
extern "C"  int32_t ReadMethod_EndInvoke_m2107115960 (ReadMethod_t3362229488 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.IO.Compression.DeflateStream/UnmanagedReadOrWrite::.ctor(System.Object,System.IntPtr)
extern "C"  void UnmanagedReadOrWrite__ctor_m482274997 (UnmanagedReadOrWrite_t1990215745 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Int32 System.IO.Compression.DeflateStream/UnmanagedReadOrWrite::Invoke(System.IntPtr,System.Int32,System.IntPtr)
extern "C"  int32_t UnmanagedReadOrWrite_Invoke_m3422277916 (UnmanagedReadOrWrite_t1990215745 * __this, IntPtr_t ___buffer0, int32_t ___length1, IntPtr_t ___data2, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnmanagedReadOrWrite_Invoke_m3422277916((UnmanagedReadOrWrite_t1990215745 *)__this->get_prev_9(),___buffer0, ___length1, ___data2, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, IntPtr_t ___buffer0, int32_t ___length1, IntPtr_t ___data2, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___buffer0, ___length1, ___data2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, IntPtr_t ___buffer0, int32_t ___length1, IntPtr_t ___data2, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___buffer0, ___length1, ___data2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  int32_t DelegatePInvokeWrapper_UnmanagedReadOrWrite_t1990215745 (UnmanagedReadOrWrite_t1990215745 * __this, IntPtr_t ___buffer0, int32_t ___length1, IntPtr_t ___data2, const MethodInfo* method)
{
	typedef int32_t (STDCALL *PInvokeFunc)(intptr_t, int32_t, intptr_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc(reinterpret_cast<intptr_t>((___buffer0).get_m_value_0()), ___length1, reinterpret_cast<intptr_t>((___data2).get_m_value_0()));

	return returnValue;
}
// System.IAsyncResult System.IO.Compression.DeflateStream/UnmanagedReadOrWrite::BeginInvoke(System.IntPtr,System.Int32,System.IntPtr,System.AsyncCallback,System.Object)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t UnmanagedReadOrWrite_BeginInvoke_m1689914535_MetadataUsageId;
extern "C"  Il2CppObject * UnmanagedReadOrWrite_BeginInvoke_m1689914535 (UnmanagedReadOrWrite_t1990215745 * __this, IntPtr_t ___buffer0, int32_t ___length1, IntPtr_t ___data2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnmanagedReadOrWrite_BeginInvoke_m1689914535_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___buffer0);
	__d_args[1] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___length1);
	__d_args[2] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___data2);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Int32 System.IO.Compression.DeflateStream/UnmanagedReadOrWrite::EndInvoke(System.IAsyncResult)
extern "C"  int32_t UnmanagedReadOrWrite_EndInvoke_m2841195209 (UnmanagedReadOrWrite_t1990215745 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.IO.Compression.DeflateStream/WriteMethod::.ctor(System.Object,System.IntPtr)
extern "C"  void WriteMethod__ctor_m2748889395 (WriteMethod_t1894833619 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.IO.Compression.DeflateStream/WriteMethod::Invoke(System.Byte[],System.Int32,System.Int32)
extern "C"  void WriteMethod_Invoke_m2312615584 (WriteMethod_t1894833619 * __this, ByteU5BU5D_t3397334013* ___array0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		WriteMethod_Invoke_m2312615584((WriteMethod_t1894833619 *)__this->get_prev_9(),___array0, ___offset1, ___count2, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, ByteU5BU5D_t3397334013* ___array0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___array0, ___offset1, ___count2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, ByteU5BU5D_t3397334013* ___array0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___array0, ___offset1, ___count2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___offset1, int32_t ___count2, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___array0, ___offset1, ___count2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_WriteMethod_t1894833619 (WriteMethod_t1894833619 * __this, ByteU5BU5D_t3397334013* ___array0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(uint8_t*, int32_t, int32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___array0' to native representation
	uint8_t* ____array0_marshaled = NULL;
	if (___array0 != NULL)
	{
		____array0_marshaled = reinterpret_cast<uint8_t*>((___array0)->GetAddressAtUnchecked(0));
	}

	// Native function invocation
	il2cppPInvokeFunc(____array0_marshaled, ___offset1, ___count2);

}
// System.IAsyncResult System.IO.Compression.DeflateStream/WriteMethod::BeginInvoke(System.Byte[],System.Int32,System.Int32,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t WriteMethod_BeginInvoke_m3664291003_MetadataUsageId;
extern "C"  Il2CppObject * WriteMethod_BeginInvoke_m3664291003 (WriteMethod_t1894833619 * __this, ByteU5BU5D_t3397334013* ___array0, int32_t ___offset1, int32_t ___count2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WriteMethod_BeginInvoke_m3664291003_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = ___array0;
	__d_args[1] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___offset1);
	__d_args[2] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___count2);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Void System.IO.Compression.DeflateStream/WriteMethod::EndInvoke(System.IAsyncResult)
extern "C"  void WriteMethod_EndInvoke_m2263736537 (WriteMethod_t1894833619 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.IO.Compression.GZipStream::.ctor(System.IO.Stream,System.IO.Compression.CompressionMode)
extern "C"  void GZipStream__ctor_m3019216668 (GZipStream_t2274754946 * __this, Stream_t3255436806 * ___compressedStream0, int32_t ___mode1, const MethodInfo* method)
{
	{
		Stream_t3255436806 * L_0 = ___compressedStream0;
		int32_t L_1 = ___mode1;
		GZipStream__ctor_m1848003691(__this, L_0, L_1, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.IO.Compression.GZipStream::.ctor(System.IO.Stream,System.IO.Compression.CompressionMode,System.Boolean)
extern Il2CppClass* Stream_t3255436806_il2cpp_TypeInfo_var;
extern Il2CppClass* DeflateStream_t3198596725_il2cpp_TypeInfo_var;
extern const uint32_t GZipStream__ctor_m1848003691_MetadataUsageId;
extern "C"  void GZipStream__ctor_m1848003691 (GZipStream_t2274754946 * __this, Stream_t3255436806 * ___compressedStream0, int32_t ___mode1, bool ___leaveOpen2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GZipStream__ctor_m1848003691_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Stream_t3255436806_il2cpp_TypeInfo_var);
		Stream__ctor_m1531324023(__this, /*hidden argument*/NULL);
		Stream_t3255436806 * L_0 = ___compressedStream0;
		int32_t L_1 = ___mode1;
		bool L_2 = ___leaveOpen2;
		DeflateStream_t3198596725 * L_3 = (DeflateStream_t3198596725 *)il2cpp_codegen_object_new(DeflateStream_t3198596725_il2cpp_TypeInfo_var);
		DeflateStream__ctor_m2642899039(L_3, L_0, L_1, L_2, (bool)1, /*hidden argument*/NULL);
		__this->set_deflateStream_1(L_3);
		return;
	}
}
// System.Void System.IO.Compression.GZipStream::Dispose(System.Boolean)
extern "C"  void GZipStream_Dispose_m2809588461 (GZipStream_t2274754946 * __this, bool ___disposing0, const MethodInfo* method)
{
	{
		bool L_0 = ___disposing0;
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		DeflateStream_t3198596725 * L_1 = __this->get_deflateStream_1();
		NullCheck(L_1);
		Stream_Dispose_m2417657914(L_1, /*hidden argument*/NULL);
	}

IL_0011:
	{
		bool L_2 = ___disposing0;
		Stream_Dispose_m440254723(__this, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 System.IO.Compression.GZipStream::Read(System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t GZipStream_Read_m1706334 (GZipStream_t2274754946 * __this, ByteU5BU5D_t3397334013* ___dest0, int32_t ___dest_offset1, int32_t ___count2, const MethodInfo* method)
{
	{
		DeflateStream_t3198596725 * L_0 = __this->get_deflateStream_1();
		ByteU5BU5D_t3397334013* L_1 = ___dest0;
		int32_t L_2 = ___dest_offset1;
		int32_t L_3 = ___count2;
		NullCheck(L_0);
		int32_t L_4 = VirtFuncInvoker3< int32_t, ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(16 /* System.Int32 System.IO.Compression.DeflateStream::Read(System.Byte[],System.Int32,System.Int32) */, L_0, L_1, L_2, L_3);
		return L_4;
	}
}
// System.Void System.IO.Compression.GZipStream::Write(System.Byte[],System.Int32,System.Int32)
extern "C"  void GZipStream_Write_m375700627 (GZipStream_t2274754946 * __this, ByteU5BU5D_t3397334013* ___src0, int32_t ___src_offset1, int32_t ___count2, const MethodInfo* method)
{
	{
		DeflateStream_t3198596725 * L_0 = __this->get_deflateStream_1();
		ByteU5BU5D_t3397334013* L_1 = ___src0;
		int32_t L_2 = ___src_offset1;
		int32_t L_3 = ___count2;
		NullCheck(L_0);
		VirtActionInvoker3< ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(20 /* System.Void System.IO.Compression.DeflateStream::Write(System.Byte[],System.Int32,System.Int32) */, L_0, L_1, L_2, L_3);
		return;
	}
}
// System.Void System.IO.Compression.GZipStream::Flush()
extern "C"  void GZipStream_Flush_m3582516281 (GZipStream_t2274754946 * __this, const MethodInfo* method)
{
	{
		DeflateStream_t3198596725 * L_0 = __this->get_deflateStream_1();
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(15 /* System.Void System.IO.Compression.DeflateStream::Flush() */, L_0);
		return;
	}
}
// System.Int64 System.IO.Compression.GZipStream::Seek(System.Int64,System.IO.SeekOrigin)
extern "C"  int64_t GZipStream_Seek_m265045829 (GZipStream_t2274754946 * __this, int64_t ___offset0, int32_t ___origin1, const MethodInfo* method)
{
	{
		DeflateStream_t3198596725 * L_0 = __this->get_deflateStream_1();
		int64_t L_1 = ___offset0;
		int32_t L_2 = ___origin1;
		NullCheck(L_0);
		int64_t L_3 = VirtFuncInvoker2< int64_t, int64_t, int32_t >::Invoke(18 /* System.Int64 System.IO.Compression.DeflateStream::Seek(System.Int64,System.IO.SeekOrigin) */, L_0, L_1, L_2);
		return L_3;
	}
}
// System.Void System.IO.Compression.GZipStream::SetLength(System.Int64)
extern "C"  void GZipStream_SetLength_m2544434589 (GZipStream_t2274754946 * __this, int64_t ___value0, const MethodInfo* method)
{
	{
		DeflateStream_t3198596725 * L_0 = __this->get_deflateStream_1();
		int64_t L_1 = ___value0;
		NullCheck(L_0);
		VirtActionInvoker1< int64_t >::Invoke(19 /* System.Void System.IO.Compression.DeflateStream::SetLength(System.Int64) */, L_0, L_1);
		return;
	}
}
// System.IAsyncResult System.IO.Compression.GZipStream::BeginRead(System.Byte[],System.Int32,System.Int32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GZipStream_BeginRead_m3633019595 (GZipStream_t2274754946 * __this, ByteU5BU5D_t3397334013* ___buffer0, int32_t ___offset1, int32_t ___count2, AsyncCallback_t163412349 * ___cback3, Il2CppObject * ___state4, const MethodInfo* method)
{
	{
		DeflateStream_t3198596725 * L_0 = __this->get_deflateStream_1();
		ByteU5BU5D_t3397334013* L_1 = ___buffer0;
		int32_t L_2 = ___offset1;
		int32_t L_3 = ___count2;
		AsyncCallback_t163412349 * L_4 = ___cback3;
		Il2CppObject * L_5 = ___state4;
		NullCheck(L_0);
		Il2CppObject * L_6 = VirtFuncInvoker5< Il2CppObject *, ByteU5BU5D_t3397334013*, int32_t, int32_t, AsyncCallback_t163412349 *, Il2CppObject * >::Invoke(22 /* System.IAsyncResult System.IO.Compression.DeflateStream::BeginRead(System.Byte[],System.Int32,System.Int32,System.AsyncCallback,System.Object) */, L_0, L_1, L_2, L_3, L_4, L_5);
		return L_6;
	}
}
// System.IAsyncResult System.IO.Compression.GZipStream::BeginWrite(System.Byte[],System.Int32,System.Int32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GZipStream_BeginWrite_m2614829976 (GZipStream_t2274754946 * __this, ByteU5BU5D_t3397334013* ___buffer0, int32_t ___offset1, int32_t ___count2, AsyncCallback_t163412349 * ___cback3, Il2CppObject * ___state4, const MethodInfo* method)
{
	{
		DeflateStream_t3198596725 * L_0 = __this->get_deflateStream_1();
		ByteU5BU5D_t3397334013* L_1 = ___buffer0;
		int32_t L_2 = ___offset1;
		int32_t L_3 = ___count2;
		AsyncCallback_t163412349 * L_4 = ___cback3;
		Il2CppObject * L_5 = ___state4;
		NullCheck(L_0);
		Il2CppObject * L_6 = VirtFuncInvoker5< Il2CppObject *, ByteU5BU5D_t3397334013*, int32_t, int32_t, AsyncCallback_t163412349 *, Il2CppObject * >::Invoke(23 /* System.IAsyncResult System.IO.Compression.DeflateStream::BeginWrite(System.Byte[],System.Int32,System.Int32,System.AsyncCallback,System.Object) */, L_0, L_1, L_2, L_3, L_4, L_5);
		return L_6;
	}
}
// System.Int32 System.IO.Compression.GZipStream::EndRead(System.IAsyncResult)
extern "C"  int32_t GZipStream_EndRead_m3433427983 (GZipStream_t2274754946 * __this, Il2CppObject * ___async_result0, const MethodInfo* method)
{
	{
		DeflateStream_t3198596725 * L_0 = __this->get_deflateStream_1();
		Il2CppObject * L_1 = ___async_result0;
		NullCheck(L_0);
		int32_t L_2 = VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(24 /* System.Int32 System.IO.Compression.DeflateStream::EndRead(System.IAsyncResult) */, L_0, L_1);
		return L_2;
	}
}
// System.Void System.IO.Compression.GZipStream::EndWrite(System.IAsyncResult)
extern "C"  void GZipStream_EndWrite_m3448957682 (GZipStream_t2274754946 * __this, Il2CppObject * ___async_result0, const MethodInfo* method)
{
	{
		DeflateStream_t3198596725 * L_0 = __this->get_deflateStream_1();
		Il2CppObject * L_1 = ___async_result0;
		NullCheck(L_0);
		VirtActionInvoker1< Il2CppObject * >::Invoke(25 /* System.Void System.IO.Compression.DeflateStream::EndWrite(System.IAsyncResult) */, L_0, L_1);
		return;
	}
}
// System.Boolean System.IO.Compression.GZipStream::get_CanRead()
extern "C"  bool GZipStream_get_CanRead_m1081651800 (GZipStream_t2274754946 * __this, const MethodInfo* method)
{
	{
		DeflateStream_t3198596725 * L_0 = __this->get_deflateStream_1();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(5 /* System.Boolean System.IO.Compression.DeflateStream::get_CanRead() */, L_0);
		return L_1;
	}
}
// System.Boolean System.IO.Compression.GZipStream::get_CanSeek()
extern "C"  bool GZipStream_get_CanSeek_m153606340 (GZipStream_t2274754946 * __this, const MethodInfo* method)
{
	{
		DeflateStream_t3198596725 * L_0 = __this->get_deflateStream_1();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(6 /* System.Boolean System.IO.Compression.DeflateStream::get_CanSeek() */, L_0);
		return L_1;
	}
}
// System.Boolean System.IO.Compression.GZipStream::get_CanWrite()
extern "C"  bool GZipStream_get_CanWrite_m836005977 (GZipStream_t2274754946 * __this, const MethodInfo* method)
{
	{
		DeflateStream_t3198596725 * L_0 = __this->get_deflateStream_1();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.IO.Compression.DeflateStream::get_CanWrite() */, L_0);
		return L_1;
	}
}
// System.Int64 System.IO.Compression.GZipStream::get_Length()
extern "C"  int64_t GZipStream_get_Length_m436994325 (GZipStream_t2274754946 * __this, const MethodInfo* method)
{
	{
		DeflateStream_t3198596725 * L_0 = __this->get_deflateStream_1();
		NullCheck(L_0);
		int64_t L_1 = VirtFuncInvoker0< int64_t >::Invoke(8 /* System.Int64 System.IO.Compression.DeflateStream::get_Length() */, L_0);
		return L_1;
	}
}
// System.Int64 System.IO.Compression.GZipStream::get_Position()
extern "C"  int64_t GZipStream_get_Position_m1646138170 (GZipStream_t2274754946 * __this, const MethodInfo* method)
{
	{
		DeflateStream_t3198596725 * L_0 = __this->get_deflateStream_1();
		NullCheck(L_0);
		int64_t L_1 = VirtFuncInvoker0< int64_t >::Invoke(9 /* System.Int64 System.IO.Compression.DeflateStream::get_Position() */, L_0);
		return L_1;
	}
}
// System.Void System.IO.Compression.GZipStream::set_Position(System.Int64)
extern "C"  void GZipStream_set_Position_m2544990935 (GZipStream_t2274754946 * __this, int64_t ___value0, const MethodInfo* method)
{
	{
		DeflateStream_t3198596725 * L_0 = __this->get_deflateStream_1();
		int64_t L_1 = ___value0;
		NullCheck(L_0);
		VirtActionInvoker1< int64_t >::Invoke(10 /* System.Void System.IO.Compression.DeflateStream::set_Position(System.Int64) */, L_0, L_1);
		return;
	}
}
// System.Void System.MonoTODOAttribute::.ctor()
extern "C"  void MonoTODOAttribute__ctor_m4004919844 (MonoTODOAttribute_t3487514020 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.MonoTODOAttribute::.ctor(System.String)
extern "C"  void MonoTODOAttribute__ctor_m991492462 (MonoTODOAttribute_t3487514020 * __this, String_t* ___comment0, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___comment0;
		__this->set_comment_0(L_0);
		return;
	}
}
// System.Void System.Net.AuthenticationManager::.cctor()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* AuthenticationManager_t3410876775_il2cpp_TypeInfo_var;
extern const uint32_t AuthenticationManager__cctor_m910519704_MetadataUsageId;
extern "C"  void AuthenticationManager__cctor_m910519704 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AuthenticationManager__cctor_m910519704_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m2551263788(L_0, /*hidden argument*/NULL);
		((AuthenticationManager_t3410876775_StaticFields*)AuthenticationManager_t3410876775_il2cpp_TypeInfo_var->static_fields)->set_locker_1(L_0);
		((AuthenticationManager_t3410876775_StaticFields*)AuthenticationManager_t3410876775_il2cpp_TypeInfo_var->static_fields)->set_credential_policy_2((Il2CppObject *)NULL);
		return;
	}
}
// System.Void System.Net.AuthenticationManager::EnsureModules()
extern Il2CppClass* AuthenticationManager_t3410876775_il2cpp_TypeInfo_var;
extern Il2CppClass* ArrayList_t4252133567_il2cpp_TypeInfo_var;
extern Il2CppClass* BasicClient_t3996961659_il2cpp_TypeInfo_var;
extern Il2CppClass* DigestClient_t4126467897_il2cpp_TypeInfo_var;
extern const uint32_t AuthenticationManager_EnsureModules_m3945964554_MetadataUsageId;
extern "C"  void AuthenticationManager_EnsureModules_m3945964554 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AuthenticationManager_EnsureModules_m3945964554_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(AuthenticationManager_t3410876775_il2cpp_TypeInfo_var);
		Il2CppObject * L_0 = ((AuthenticationManager_t3410876775_StaticFields*)AuthenticationManager_t3410876775_il2cpp_TypeInfo_var->static_fields)->get_locker_1();
		V_0 = L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(AuthenticationManager_t3410876775_il2cpp_TypeInfo_var);
			ArrayList_t4252133567 * L_2 = ((AuthenticationManager_t3410876775_StaticFields*)AuthenticationManager_t3410876775_il2cpp_TypeInfo_var->static_fields)->get_modules_0();
			if (!L_2)
			{
				goto IL_001b;
			}
		}

IL_0016:
		{
			IL2CPP_LEAVE(0x51, FINALLY_004a);
		}

IL_001b:
		{
			ArrayList_t4252133567 * L_3 = (ArrayList_t4252133567 *)il2cpp_codegen_object_new(ArrayList_t4252133567_il2cpp_TypeInfo_var);
			ArrayList__ctor_m4012174379(L_3, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(AuthenticationManager_t3410876775_il2cpp_TypeInfo_var);
			((AuthenticationManager_t3410876775_StaticFields*)AuthenticationManager_t3410876775_il2cpp_TypeInfo_var->static_fields)->set_modules_0(L_3);
			ArrayList_t4252133567 * L_4 = ((AuthenticationManager_t3410876775_StaticFields*)AuthenticationManager_t3410876775_il2cpp_TypeInfo_var->static_fields)->get_modules_0();
			BasicClient_t3996961659 * L_5 = (BasicClient_t3996961659 *)il2cpp_codegen_object_new(BasicClient_t3996961659_il2cpp_TypeInfo_var);
			BasicClient__ctor_m2878156907(L_5, /*hidden argument*/NULL);
			NullCheck(L_4);
			VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_4, L_5);
			ArrayList_t4252133567 * L_6 = ((AuthenticationManager_t3410876775_StaticFields*)AuthenticationManager_t3410876775_il2cpp_TypeInfo_var->static_fields)->get_modules_0();
			DigestClient_t4126467897 * L_7 = (DigestClient_t4126467897 *)il2cpp_codegen_object_new(DigestClient_t4126467897_il2cpp_TypeInfo_var);
			DigestClient__ctor_m2516808581(L_7, /*hidden argument*/NULL);
			NullCheck(L_6);
			VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_6, L_7);
			IL2CPP_LEAVE(0x51, FINALLY_004a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_004a;
	}

FINALLY_004a:
	{ // begin finally (depth: 1)
		Il2CppObject * L_8 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(74)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(74)
	{
		IL2CPP_JUMP_TBL(0x51, IL_0051)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0051:
	{
		return;
	}
}
// System.Net.Authorization System.Net.AuthenticationManager::Authenticate(System.String,System.Net.WebRequest,System.Net.ICredentials)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppClass* AuthenticationManager_t3410876775_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2759502645;
extern Il2CppCodeGenString* _stringLiteral836861112;
extern Il2CppCodeGenString* _stringLiteral4084294319;
extern const uint32_t AuthenticationManager_Authenticate_m1203560882_MetadataUsageId;
extern "C"  Authorization_t1602399 * AuthenticationManager_Authenticate_m1203560882 (Il2CppObject * __this /* static, unused */, String_t* ___challenge0, WebRequest_t1365124353 * ___request1, Il2CppObject * ___credentials2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AuthenticationManager_Authenticate_m1203560882_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		WebRequest_t1365124353 * L_0 = ___request1;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, _stringLiteral2759502645, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Il2CppObject * L_2 = ___credentials2;
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentNullException_t628810857 * L_3 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_3, _stringLiteral836861112, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0022:
	{
		String_t* L_4 = ___challenge0;
		if (L_4)
		{
			goto IL_0033;
		}
	}
	{
		ArgumentNullException_t628810857 * L_5 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_5, _stringLiteral4084294319, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		String_t* L_6 = ___challenge0;
		WebRequest_t1365124353 * L_7 = ___request1;
		Il2CppObject * L_8 = ___credentials2;
		IL2CPP_RUNTIME_CLASS_INIT(AuthenticationManager_t3410876775_il2cpp_TypeInfo_var);
		Authorization_t1602399 * L_9 = AuthenticationManager_DoAuthenticate_m2946889617(NULL /*static, unused*/, L_6, L_7, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// System.Net.Authorization System.Net.AuthenticationManager::DoAuthenticate(System.String,System.Net.WebRequest,System.Net.ICredentials)
extern Il2CppClass* AuthenticationManager_t3410876775_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IAuthenticationModule_t3093891015_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t AuthenticationManager_DoAuthenticate_m2946889617_MetadataUsageId;
extern "C"  Authorization_t1602399 * AuthenticationManager_DoAuthenticate_m2946889617 (Il2CppObject * __this /* static, unused */, String_t* ___challenge0, WebRequest_t1365124353 * ___request1, Il2CppObject * ___credentials2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AuthenticationManager_DoAuthenticate_m2946889617_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ArrayList_t4252133567 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Authorization_t1602399 * V_3 = NULL;
	Authorization_t1602399 * V_4 = NULL;
	Il2CppObject * V_5 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(AuthenticationManager_t3410876775_il2cpp_TypeInfo_var);
		AuthenticationManager_EnsureModules_m3945964554(NULL /*static, unused*/, /*hidden argument*/NULL);
		ArrayList_t4252133567 * L_0 = ((AuthenticationManager_t3410876775_StaticFields*)AuthenticationManager_t3410876775_il2cpp_TypeInfo_var->static_fields)->get_modules_0();
		V_0 = L_0;
		ArrayList_t4252133567 * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_0011:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(AuthenticationManager_t3410876775_il2cpp_TypeInfo_var);
			ArrayList_t4252133567 * L_2 = ((AuthenticationManager_t3410876775_StaticFields*)AuthenticationManager_t3410876775_il2cpp_TypeInfo_var->static_fields)->get_modules_0();
			NullCheck(L_2);
			Il2CppObject * L_3 = VirtFuncInvoker0< Il2CppObject * >::Invoke(43 /* System.Collections.IEnumerator System.Collections.ArrayList::GetEnumerator() */, L_2);
			V_2 = L_3;
		}

IL_001c:
		try
		{ // begin try (depth: 2)
			{
				goto IL_0051;
			}

IL_0021:
			{
				Il2CppObject * L_4 = V_2;
				NullCheck(L_4);
				Il2CppObject * L_5 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_4);
				V_1 = ((Il2CppObject *)Castclass(L_5, IAuthenticationModule_t3093891015_il2cpp_TypeInfo_var));
				Il2CppObject * L_6 = V_1;
				String_t* L_7 = ___challenge0;
				WebRequest_t1365124353 * L_8 = ___request1;
				Il2CppObject * L_9 = ___credentials2;
				NullCheck(L_6);
				Authorization_t1602399 * L_10 = InterfaceFuncInvoker3< Authorization_t1602399 *, String_t*, WebRequest_t1365124353 *, Il2CppObject * >::Invoke(0 /* System.Net.Authorization System.Net.IAuthenticationModule::Authenticate(System.String,System.Net.WebRequest,System.Net.ICredentials) */, IAuthenticationModule_t3093891015_il2cpp_TypeInfo_var, L_6, L_7, L_8, L_9);
				V_3 = L_10;
				Authorization_t1602399 * L_11 = V_3;
				if (L_11)
				{
					goto IL_0042;
				}
			}

IL_003d:
			{
				goto IL_0051;
			}

IL_0042:
			{
				Authorization_t1602399 * L_12 = V_3;
				Il2CppObject * L_13 = V_1;
				NullCheck(L_12);
				Authorization_set_Module_m1551993521(L_12, L_13, /*hidden argument*/NULL);
				Authorization_t1602399 * L_14 = V_3;
				V_4 = L_14;
				IL2CPP_LEAVE(0x84, FINALLY_0061);
			}

IL_0051:
			{
				Il2CppObject * L_15 = V_2;
				NullCheck(L_15);
				bool L_16 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_15);
				if (L_16)
				{
					goto IL_0021;
				}
			}

IL_005c:
			{
				IL2CPP_LEAVE(0x76, FINALLY_0061);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
			goto FINALLY_0061;
		}

FINALLY_0061:
		{ // begin finally (depth: 2)
			{
				Il2CppObject * L_17 = V_2;
				V_5 = ((Il2CppObject *)IsInst(L_17, IDisposable_t2427283555_il2cpp_TypeInfo_var));
				Il2CppObject * L_18 = V_5;
				if (L_18)
				{
					goto IL_006e;
				}
			}

IL_006d:
			{
				IL2CPP_END_FINALLY(97)
			}

IL_006e:
			{
				Il2CppObject * L_19 = V_5;
				NullCheck(L_19);
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_19);
				IL2CPP_END_FINALLY(97)
			}
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(97)
		{
			IL2CPP_END_CLEANUP(0x84, FINALLY_007b);
			IL2CPP_JUMP_TBL(0x76, IL_0076)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
		}

IL_0076:
		{
			IL2CPP_LEAVE(0x82, FINALLY_007b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_007b;
	}

FINALLY_007b:
	{ // begin finally (depth: 1)
		ArrayList_t4252133567 * L_20 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(123)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(123)
	{
		IL2CPP_JUMP_TBL(0x84, IL_0084)
		IL2CPP_JUMP_TBL(0x82, IL_0082)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0082:
	{
		return (Authorization_t1602399 *)NULL;
	}

IL_0084:
	{
		Authorization_t1602399 * L_21 = V_4;
		return L_21;
	}
}
// System.Net.Authorization System.Net.AuthenticationManager::PreAuthenticate(System.Net.WebRequest,System.Net.ICredentials)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppClass* AuthenticationManager_t3410876775_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IAuthenticationModule_t3093891015_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2759502645;
extern const uint32_t AuthenticationManager_PreAuthenticate_m1303957643_MetadataUsageId;
extern "C"  Authorization_t1602399 * AuthenticationManager_PreAuthenticate_m1303957643 (Il2CppObject * __this /* static, unused */, WebRequest_t1365124353 * ___request0, Il2CppObject * ___credentials1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AuthenticationManager_PreAuthenticate_m1303957643_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ArrayList_t4252133567 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Authorization_t1602399 * V_3 = NULL;
	Authorization_t1602399 * V_4 = NULL;
	Il2CppObject * V_5 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		WebRequest_t1365124353 * L_0 = ___request0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, _stringLiteral2759502645, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Il2CppObject * L_2 = ___credentials1;
		if (L_2)
		{
			goto IL_0019;
		}
	}
	{
		return (Authorization_t1602399 *)NULL;
	}

IL_0019:
	{
		IL2CPP_RUNTIME_CLASS_INIT(AuthenticationManager_t3410876775_il2cpp_TypeInfo_var);
		AuthenticationManager_EnsureModules_m3945964554(NULL /*static, unused*/, /*hidden argument*/NULL);
		ArrayList_t4252133567 * L_3 = ((AuthenticationManager_t3410876775_StaticFields*)AuthenticationManager_t3410876775_il2cpp_TypeInfo_var->static_fields)->get_modules_0();
		V_0 = L_3;
		ArrayList_t4252133567 * L_4 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_002a:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(AuthenticationManager_t3410876775_il2cpp_TypeInfo_var);
			ArrayList_t4252133567 * L_5 = ((AuthenticationManager_t3410876775_StaticFields*)AuthenticationManager_t3410876775_il2cpp_TypeInfo_var->static_fields)->get_modules_0();
			NullCheck(L_5);
			Il2CppObject * L_6 = VirtFuncInvoker0< Il2CppObject * >::Invoke(43 /* System.Collections.IEnumerator System.Collections.ArrayList::GetEnumerator() */, L_5);
			V_2 = L_6;
		}

IL_0035:
		try
		{ // begin try (depth: 2)
			{
				goto IL_0069;
			}

IL_003a:
			{
				Il2CppObject * L_7 = V_2;
				NullCheck(L_7);
				Il2CppObject * L_8 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_7);
				V_1 = ((Il2CppObject *)Castclass(L_8, IAuthenticationModule_t3093891015_il2cpp_TypeInfo_var));
				Il2CppObject * L_9 = V_1;
				WebRequest_t1365124353 * L_10 = ___request0;
				Il2CppObject * L_11 = ___credentials1;
				NullCheck(L_9);
				Authorization_t1602399 * L_12 = InterfaceFuncInvoker2< Authorization_t1602399 *, WebRequest_t1365124353 *, Il2CppObject * >::Invoke(1 /* System.Net.Authorization System.Net.IAuthenticationModule::PreAuthenticate(System.Net.WebRequest,System.Net.ICredentials) */, IAuthenticationModule_t3093891015_il2cpp_TypeInfo_var, L_9, L_10, L_11);
				V_3 = L_12;
				Authorization_t1602399 * L_13 = V_3;
				if (L_13)
				{
					goto IL_005a;
				}
			}

IL_0055:
			{
				goto IL_0069;
			}

IL_005a:
			{
				Authorization_t1602399 * L_14 = V_3;
				Il2CppObject * L_15 = V_1;
				NullCheck(L_14);
				Authorization_set_Module_m1551993521(L_14, L_15, /*hidden argument*/NULL);
				Authorization_t1602399 * L_16 = V_3;
				V_4 = L_16;
				IL2CPP_LEAVE(0x9C, FINALLY_0079);
			}

IL_0069:
			{
				Il2CppObject * L_17 = V_2;
				NullCheck(L_17);
				bool L_18 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_17);
				if (L_18)
				{
					goto IL_003a;
				}
			}

IL_0074:
			{
				IL2CPP_LEAVE(0x8E, FINALLY_0079);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
			goto FINALLY_0079;
		}

FINALLY_0079:
		{ // begin finally (depth: 2)
			{
				Il2CppObject * L_19 = V_2;
				V_5 = ((Il2CppObject *)IsInst(L_19, IDisposable_t2427283555_il2cpp_TypeInfo_var));
				Il2CppObject * L_20 = V_5;
				if (L_20)
				{
					goto IL_0086;
				}
			}

IL_0085:
			{
				IL2CPP_END_FINALLY(121)
			}

IL_0086:
			{
				Il2CppObject * L_21 = V_5;
				NullCheck(L_21);
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_21);
				IL2CPP_END_FINALLY(121)
			}
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(121)
		{
			IL2CPP_END_CLEANUP(0x9C, FINALLY_0093);
			IL2CPP_JUMP_TBL(0x8E, IL_008e)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
		}

IL_008e:
		{
			IL2CPP_LEAVE(0x9A, FINALLY_0093);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0093;
	}

FINALLY_0093:
	{ // begin finally (depth: 1)
		ArrayList_t4252133567 * L_22 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(147)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(147)
	{
		IL2CPP_JUMP_TBL(0x9C, IL_009c)
		IL2CPP_JUMP_TBL(0x9A, IL_009a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_009a:
	{
		return (Authorization_t1602399 *)NULL;
	}

IL_009c:
	{
		Authorization_t1602399 * L_23 = V_4;
		return L_23;
	}
}
// System.Void System.Net.Authorization::.ctor(System.String)
extern "C"  void Authorization__ctor_m2156581301 (Authorization_t1602399 * __this, String_t* ___token0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___token0;
		Authorization__ctor_m1218150454(__this, L_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Net.Authorization::.ctor(System.String,System.Boolean)
extern "C"  void Authorization__ctor_m1218150454 (Authorization_t1602399 * __this, String_t* ___token0, bool ___complete1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___token0;
		bool L_1 = ___complete1;
		Authorization__ctor_m2658077270(__this, L_0, L_1, (String_t*)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Net.Authorization::.ctor(System.String,System.Boolean,System.String)
extern "C"  void Authorization__ctor_m2658077270 (Authorization_t1602399 * __this, String_t* ___token0, bool ___complete1, String_t* ___connectionGroupId2, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___token0;
		__this->set_token_0(L_0);
		bool L_1 = ___complete1;
		__this->set_complete_1(L_1);
		String_t* L_2 = ___connectionGroupId2;
		__this->set_connectionGroupId_2(L_2);
		return;
	}
}
// System.String System.Net.Authorization::get_Message()
extern "C"  String_t* Authorization_get_Message_m2175547724 (Authorization_t1602399 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_token_0();
		return L_0;
	}
}
// System.Boolean System.Net.Authorization::get_Complete()
extern "C"  bool Authorization_get_Complete_m1402288019 (Authorization_t1602399 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_complete_1();
		return L_0;
	}
}
// System.Net.IAuthenticationModule System.Net.Authorization::get_Module()
extern "C"  Il2CppObject * Authorization_get_Module_m2331757864 (Authorization_t1602399 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_module_3();
		return L_0;
	}
}
// System.Void System.Net.Authorization::set_Module(System.Net.IAuthenticationModule)
extern "C"  void Authorization_set_Module_m1551993521 (Authorization_t1602399 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_module_3(L_0);
		return;
	}
}
// System.Void System.Net.BasicClient::.ctor()
extern "C"  void BasicClient__ctor_m2878156907 (BasicClient_t3996961659 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Net.Authorization System.Net.BasicClient::Authenticate(System.String,System.Net.WebRequest,System.Net.ICredentials)
extern Il2CppCodeGenString* _stringLiteral4128911352;
extern const uint32_t BasicClient_Authenticate_m1195963312_MetadataUsageId;
extern "C"  Authorization_t1602399 * BasicClient_Authenticate_m1195963312 (BasicClient_t3996961659 * __this, String_t* ___challenge0, WebRequest_t1365124353 * ___webRequest1, Il2CppObject * ___credentials2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BasicClient_Authenticate_m1195963312_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		Il2CppObject * L_0 = ___credentials2;
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		String_t* L_1 = ___challenge0;
		if (L_1)
		{
			goto IL_000e;
		}
	}

IL_000c:
	{
		return (Authorization_t1602399 *)NULL;
	}

IL_000e:
	{
		String_t* L_2 = ___challenge0;
		NullCheck(L_2);
		String_t* L_3 = String_Trim_m2668767713(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		String_t* L_4 = V_0;
		NullCheck(L_4);
		String_t* L_5 = String_ToLower_m2994460523(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		int32_t L_6 = String_IndexOf_m4251815737(L_5, _stringLiteral4128911352, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_6) == ((uint32_t)(-1)))))
		{
			goto IL_002d;
		}
	}
	{
		return (Authorization_t1602399 *)NULL;
	}

IL_002d:
	{
		WebRequest_t1365124353 * L_7 = ___webRequest1;
		Il2CppObject * L_8 = ___credentials2;
		Authorization_t1602399 * L_9 = BasicClient_InternalAuthenticate_m84947107(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// System.Byte[] System.Net.BasicClient::GetBytes(System.String)
extern Il2CppClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern const uint32_t BasicClient_GetBytes_m2438601342_MetadataUsageId;
extern "C"  ByteU5BU5D_t3397334013* BasicClient_GetBytes_m2438601342 (Il2CppObject * __this /* static, unused */, String_t* ___str0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BasicClient_GetBytes_m2438601342_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	ByteU5BU5D_t3397334013* V_1 = NULL;
	{
		String_t* L_0 = ___str0;
		NullCheck(L_0);
		int32_t L_1 = String_get_Length_m1606060069(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		V_1 = ((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)L_2));
		int32_t L_3 = V_0;
		V_0 = ((int32_t)((int32_t)L_3-(int32_t)1));
		goto IL_0026;
	}

IL_0017:
	{
		ByteU5BU5D_t3397334013* L_4 = V_1;
		int32_t L_5 = V_0;
		String_t* L_6 = ___str0;
		int32_t L_7 = V_0;
		NullCheck(L_6);
		Il2CppChar L_8 = String_get_Chars_m4230566705(L_6, L_7, /*hidden argument*/NULL);
		NullCheck(L_4);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (uint8_t)(((int32_t)((uint8_t)L_8))));
		int32_t L_9 = V_0;
		V_0 = ((int32_t)((int32_t)L_9-(int32_t)1));
	}

IL_0026:
	{
		int32_t L_10 = V_0;
		if ((((int32_t)L_10) >= ((int32_t)0)))
		{
			goto IL_0017;
		}
	}
	{
		ByteU5BU5D_t3397334013* L_11 = V_1;
		return L_11;
	}
}
// System.Net.Authorization System.Net.BasicClient::InternalAuthenticate(System.Net.WebRequest,System.Net.ICredentials)
extern Il2CppClass* HttpWebRequest_t1951404513_il2cpp_TypeInfo_var;
extern Il2CppClass* ICredentials_t3855617113_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern Il2CppClass* Authorization_t1602399_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128911352;
extern Il2CppCodeGenString* _stringLiteral372029336;
extern Il2CppCodeGenString* _stringLiteral372029426;
extern Il2CppCodeGenString* _stringLiteral4085839064;
extern const uint32_t BasicClient_InternalAuthenticate_m84947107_MetadataUsageId;
extern "C"  Authorization_t1602399 * BasicClient_InternalAuthenticate_m84947107 (Il2CppObject * __this /* static, unused */, WebRequest_t1365124353 * ___webRequest0, Il2CppObject * ___credentials1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BasicClient_InternalAuthenticate_m84947107_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	HttpWebRequest_t1951404513 * V_0 = NULL;
	NetworkCredential_t1714133953 * V_1 = NULL;
	String_t* V_2 = NULL;
	String_t* V_3 = NULL;
	String_t* V_4 = NULL;
	ByteU5BU5D_t3397334013* V_5 = NULL;
	String_t* V_6 = NULL;
	{
		WebRequest_t1365124353 * L_0 = ___webRequest0;
		V_0 = ((HttpWebRequest_t1951404513 *)IsInstClass(L_0, HttpWebRequest_t1951404513_il2cpp_TypeInfo_var));
		HttpWebRequest_t1951404513 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		Il2CppObject * L_2 = ___credentials1;
		if (L_2)
		{
			goto IL_0015;
		}
	}

IL_0013:
	{
		return (Authorization_t1602399 *)NULL;
	}

IL_0015:
	{
		Il2CppObject * L_3 = ___credentials1;
		HttpWebRequest_t1951404513 * L_4 = V_0;
		NullCheck(L_4);
		Uri_t19570940 * L_5 = HttpWebRequest_get_AuthUri_m4186746998(L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		NetworkCredential_t1714133953 * L_6 = InterfaceFuncInvoker2< NetworkCredential_t1714133953 *, Uri_t19570940 *, String_t* >::Invoke(0 /* System.Net.NetworkCredential System.Net.ICredentials::GetCredential(System.Uri,System.String) */, ICredentials_t3855617113_il2cpp_TypeInfo_var, L_3, L_5, _stringLiteral4128911352);
		V_1 = L_6;
		NetworkCredential_t1714133953 * L_7 = V_1;
		if (L_7)
		{
			goto IL_002f;
		}
	}
	{
		return (Authorization_t1602399 *)NULL;
	}

IL_002f:
	{
		NetworkCredential_t1714133953 * L_8 = V_1;
		NullCheck(L_8);
		String_t* L_9 = NetworkCredential_get_UserName_m1552968607(L_8, /*hidden argument*/NULL);
		V_2 = L_9;
		String_t* L_10 = V_2;
		if (!L_10)
		{
			goto IL_004c;
		}
	}
	{
		String_t* L_11 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_13 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_004e;
		}
	}

IL_004c:
	{
		return (Authorization_t1602399 *)NULL;
	}

IL_004e:
	{
		NetworkCredential_t1714133953 * L_14 = V_1;
		NullCheck(L_14);
		String_t* L_15 = NetworkCredential_get_Password_m2037205148(L_14, /*hidden argument*/NULL);
		V_3 = L_15;
		NetworkCredential_t1714133953 * L_16 = V_1;
		NullCheck(L_16);
		String_t* L_17 = NetworkCredential_get_Domain_m2892408761(L_16, /*hidden argument*/NULL);
		V_4 = L_17;
		String_t* L_18 = V_4;
		if (!L_18)
		{
			goto IL_008b;
		}
	}
	{
		String_t* L_19 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_20 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_21 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_19, L_20, /*hidden argument*/NULL);
		if (L_21)
		{
			goto IL_008b;
		}
	}
	{
		String_t* L_22 = V_4;
		NullCheck(L_22);
		String_t* L_23 = String_Trim_m2668767713(L_22, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_24 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_25 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_23, L_24, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00a3;
		}
	}

IL_008b:
	{
		String_t* L_26 = V_2;
		String_t* L_27 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_28 = String_Concat_m612901809(NULL /*static, unused*/, L_26, _stringLiteral372029336, L_27, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_29 = BasicClient_GetBytes_m2438601342(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
		V_5 = L_29;
		goto IL_00d2;
	}

IL_00a3:
	{
		StringU5BU5D_t1642385972* L_30 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		String_t* L_31 = V_4;
		NullCheck(L_30);
		ArrayElementTypeCheck (L_30, L_31);
		(L_30)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_31);
		StringU5BU5D_t1642385972* L_32 = L_30;
		NullCheck(L_32);
		ArrayElementTypeCheck (L_32, _stringLiteral372029426);
		(L_32)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral372029426);
		StringU5BU5D_t1642385972* L_33 = L_32;
		String_t* L_34 = V_2;
		NullCheck(L_33);
		ArrayElementTypeCheck (L_33, L_34);
		(L_33)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)L_34);
		StringU5BU5D_t1642385972* L_35 = L_33;
		NullCheck(L_35);
		ArrayElementTypeCheck (L_35, _stringLiteral372029336);
		(L_35)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral372029336);
		StringU5BU5D_t1642385972* L_36 = L_35;
		String_t* L_37 = V_3;
		NullCheck(L_36);
		ArrayElementTypeCheck (L_36, L_37);
		(L_36)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)L_37);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_38 = String_Concat_m626692867(NULL /*static, unused*/, L_36, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_39 = BasicClient_GetBytes_m2438601342(NULL /*static, unused*/, L_38, /*hidden argument*/NULL);
		V_5 = L_39;
	}

IL_00d2:
	{
		ByteU5BU5D_t3397334013* L_40 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		String_t* L_41 = Convert_ToBase64String_m1936815455(NULL /*static, unused*/, L_40, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_42 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral4085839064, L_41, /*hidden argument*/NULL);
		V_6 = L_42;
		String_t* L_43 = V_6;
		Authorization_t1602399 * L_44 = (Authorization_t1602399 *)il2cpp_codegen_object_new(Authorization_t1602399_il2cpp_TypeInfo_var);
		Authorization__ctor_m2156581301(L_44, L_43, /*hidden argument*/NULL);
		return L_44;
	}
}
// System.Net.Authorization System.Net.BasicClient::PreAuthenticate(System.Net.WebRequest,System.Net.ICredentials)
extern "C"  Authorization_t1602399 * BasicClient_PreAuthenticate_m1447712807 (BasicClient_t3996961659 * __this, WebRequest_t1365124353 * ___webRequest0, Il2CppObject * ___credentials1, const MethodInfo* method)
{
	{
		WebRequest_t1365124353 * L_0 = ___webRequest0;
		Il2CppObject * L_1 = ___credentials1;
		Authorization_t1602399 * L_2 = BasicClient_InternalAuthenticate_m84947107(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.String System.Net.BasicClient::get_AuthenticationType()
extern Il2CppCodeGenString* _stringLiteral4128944152;
extern const uint32_t BasicClient_get_AuthenticationType_m430846715_MetadataUsageId;
extern "C"  String_t* BasicClient_get_AuthenticationType_m430846715 (BasicClient_t3996961659 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BasicClient_get_AuthenticationType_m430846715_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		return _stringLiteral4128944152;
	}
}
// System.Void System.Net.BindIPEndPoint::.ctor(System.Object,System.IntPtr)
extern "C"  void BindIPEndPoint__ctor_m2014121651 (BindIPEndPoint_t635820671 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Net.IPEndPoint System.Net.BindIPEndPoint::Invoke(System.Net.ServicePoint,System.Net.IPEndPoint,System.Int32)
extern "C"  IPEndPoint_t2615413766 * BindIPEndPoint_Invoke_m4182856584 (BindIPEndPoint_t635820671 * __this, ServicePoint_t2765344313 * ___servicePoint0, IPEndPoint_t2615413766 * ___remoteEndPoint1, int32_t ___retryCount2, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		BindIPEndPoint_Invoke_m4182856584((BindIPEndPoint_t635820671 *)__this->get_prev_9(),___servicePoint0, ___remoteEndPoint1, ___retryCount2, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef IPEndPoint_t2615413766 * (*FunctionPointerType) (Il2CppObject *, void* __this, ServicePoint_t2765344313 * ___servicePoint0, IPEndPoint_t2615413766 * ___remoteEndPoint1, int32_t ___retryCount2, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___servicePoint0, ___remoteEndPoint1, ___retryCount2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef IPEndPoint_t2615413766 * (*FunctionPointerType) (void* __this, ServicePoint_t2765344313 * ___servicePoint0, IPEndPoint_t2615413766 * ___remoteEndPoint1, int32_t ___retryCount2, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___servicePoint0, ___remoteEndPoint1, ___retryCount2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef IPEndPoint_t2615413766 * (*FunctionPointerType) (void* __this, IPEndPoint_t2615413766 * ___remoteEndPoint1, int32_t ___retryCount2, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___servicePoint0, ___remoteEndPoint1, ___retryCount2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Net.BindIPEndPoint::BeginInvoke(System.Net.ServicePoint,System.Net.IPEndPoint,System.Int32,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t BindIPEndPoint_BeginInvoke_m801006666_MetadataUsageId;
extern "C"  Il2CppObject * BindIPEndPoint_BeginInvoke_m801006666 (BindIPEndPoint_t635820671 * __this, ServicePoint_t2765344313 * ___servicePoint0, IPEndPoint_t2615413766 * ___remoteEndPoint1, int32_t ___retryCount2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BindIPEndPoint_BeginInvoke_m801006666_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = ___servicePoint0;
	__d_args[1] = ___remoteEndPoint1;
	__d_args[2] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___retryCount2);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Net.IPEndPoint System.Net.BindIPEndPoint::EndInvoke(System.IAsyncResult)
extern "C"  IPEndPoint_t2615413766 * BindIPEndPoint_EndInvoke_m2822459026 (BindIPEndPoint_t635820671 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (IPEndPoint_t2615413766 *)__result;
}
// System.Void System.Net.ChunkStream::.ctor(System.Byte[],System.Int32,System.Int32,System.Net.WebHeaderCollection)
extern "C"  void ChunkStream__ctor_m2091082235 (ChunkStream_t91719323 * __this, ByteU5BU5D_t3397334013* ___buffer0, int32_t ___offset1, int32_t ___size2, WebHeaderCollection_t3028142837 * ___headers3, const MethodInfo* method)
{
	{
		WebHeaderCollection_t3028142837 * L_0 = ___headers3;
		ChunkStream__ctor_m2666552004(__this, L_0, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_1 = ___buffer0;
		int32_t L_2 = ___offset1;
		int32_t L_3 = ___size2;
		ChunkStream_Write_m1232124261(__this, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Net.ChunkStream::.ctor(System.Net.WebHeaderCollection)
extern Il2CppClass* StringBuilder_t1221177846_il2cpp_TypeInfo_var;
extern Il2CppClass* ArrayList_t4252133567_il2cpp_TypeInfo_var;
extern const uint32_t ChunkStream__ctor_m2666552004_MetadataUsageId;
extern "C"  void ChunkStream__ctor_m2666552004 (ChunkStream_t91719323 * __this, WebHeaderCollection_t3028142837 * ___headers0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ChunkStream__ctor_m2666552004_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		WebHeaderCollection_t3028142837 * L_0 = ___headers0;
		__this->set_headers_0(L_0);
		StringBuilder_t1221177846 * L_1 = (StringBuilder_t1221177846 *)il2cpp_codegen_object_new(StringBuilder_t1221177846_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m3946851802(L_1, /*hidden argument*/NULL);
		__this->set_saved_4(L_1);
		ArrayList_t4252133567 * L_2 = (ArrayList_t4252133567 *)il2cpp_codegen_object_new(ArrayList_t4252133567_il2cpp_TypeInfo_var);
		ArrayList__ctor_m4012174379(L_2, /*hidden argument*/NULL);
		__this->set_chunks_8(L_2);
		__this->set_chunkSize_1((-1));
		return;
	}
}
// System.Void System.Net.ChunkStream::ResetBuffer()
extern "C"  void ChunkStream_ResetBuffer_m2756575142 (ChunkStream_t91719323 * __this, const MethodInfo* method)
{
	{
		__this->set_chunkSize_1((-1));
		__this->set_chunkRead_2(0);
		ArrayList_t4252133567 * L_0 = __this->get_chunks_8();
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(31 /* System.Void System.Collections.ArrayList::Clear() */, L_0);
		return;
	}
}
// System.Void System.Net.ChunkStream::WriteAndReadBack(System.Byte[],System.Int32,System.Int32,System.Int32&)
extern "C"  void ChunkStream_WriteAndReadBack_m4051447520 (ChunkStream_t91719323 * __this, ByteU5BU5D_t3397334013* ___buffer0, int32_t ___offset1, int32_t ___size2, int32_t* ___read3, const MethodInfo* method)
{
	{
		int32_t L_0 = ___offset1;
		int32_t* L_1 = ___read3;
		if ((((int32_t)((int32_t)((int32_t)L_0+(int32_t)(*((int32_t*)L_1))))) <= ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		ByteU5BU5D_t3397334013* L_2 = ___buffer0;
		int32_t L_3 = ___offset1;
		int32_t L_4 = ___offset1;
		int32_t* L_5 = ___read3;
		ChunkStream_Write_m1232124261(__this, L_2, L_3, ((int32_t)((int32_t)L_4+(int32_t)(*((int32_t*)L_5)))), /*hidden argument*/NULL);
	}

IL_0018:
	{
		int32_t* L_6 = ___read3;
		ByteU5BU5D_t3397334013* L_7 = ___buffer0;
		int32_t L_8 = ___offset1;
		int32_t L_9 = ___size2;
		int32_t L_10 = ChunkStream_Read_m2644745368(__this, L_7, L_8, L_9, /*hidden argument*/NULL);
		*((int32_t*)(L_6)) = (int32_t)L_10;
		return;
	}
}
// System.Int32 System.Net.ChunkStream::Read(System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t ChunkStream_Read_m2644745368 (ChunkStream_t91719323 * __this, ByteU5BU5D_t3397334013* ___buffer0, int32_t ___offset1, int32_t ___size2, const MethodInfo* method)
{
	{
		ByteU5BU5D_t3397334013* L_0 = ___buffer0;
		int32_t L_1 = ___offset1;
		int32_t L_2 = ___size2;
		int32_t L_3 = ChunkStream_ReadFromChunks_m227440694(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Int32 System.Net.ChunkStream::ReadFromChunks(System.Byte[],System.Int32,System.Int32)
extern Il2CppClass* Chunk_t3860501603_il2cpp_TypeInfo_var;
extern const uint32_t ChunkStream_ReadFromChunks_m227440694_MetadataUsageId;
extern "C"  int32_t ChunkStream_ReadFromChunks_m227440694 (ChunkStream_t91719323 * __this, ByteU5BU5D_t3397334013* ___buffer0, int32_t ___offset1, int32_t ___size2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ChunkStream_ReadFromChunks_m227440694_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	Chunk_t3860501603 * V_3 = NULL;
	{
		ArrayList_t4252133567 * L_0 = __this->get_chunks_8();
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_0);
		V_0 = L_1;
		V_1 = 0;
		V_2 = 0;
		goto IL_0077;
	}

IL_0015:
	{
		ArrayList_t4252133567 * L_2 = __this->get_chunks_8();
		int32_t L_3 = V_2;
		NullCheck(L_2);
		Il2CppObject * L_4 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_2, L_3);
		V_3 = ((Chunk_t3860501603 *)CastclassClass(L_4, Chunk_t3860501603_il2cpp_TypeInfo_var));
		Chunk_t3860501603 * L_5 = V_3;
		if (L_5)
		{
			goto IL_0032;
		}
	}
	{
		goto IL_0073;
	}

IL_0032:
	{
		Chunk_t3860501603 * L_6 = V_3;
		NullCheck(L_6);
		int32_t L_7 = L_6->get_Offset_1();
		Chunk_t3860501603 * L_8 = V_3;
		NullCheck(L_8);
		ByteU5BU5D_t3397334013* L_9 = L_8->get_Bytes_0();
		NullCheck(L_9);
		if ((!(((uint32_t)L_7) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length))))))))
		{
			goto IL_0057;
		}
	}
	{
		ArrayList_t4252133567 * L_10 = __this->get_chunks_8();
		int32_t L_11 = V_2;
		NullCheck(L_10);
		VirtActionInvoker2< int32_t, Il2CppObject * >::Invoke(22 /* System.Void System.Collections.ArrayList::set_Item(System.Int32,System.Object) */, L_10, L_11, NULL);
		goto IL_0073;
	}

IL_0057:
	{
		int32_t L_12 = V_1;
		Chunk_t3860501603 * L_13 = V_3;
		ByteU5BU5D_t3397334013* L_14 = ___buffer0;
		int32_t L_15 = ___offset1;
		int32_t L_16 = V_1;
		int32_t L_17 = ___size2;
		int32_t L_18 = V_1;
		NullCheck(L_13);
		int32_t L_19 = Chunk_Read_m647929994(L_13, L_14, ((int32_t)((int32_t)L_15+(int32_t)L_16)), ((int32_t)((int32_t)L_17-(int32_t)L_18)), /*hidden argument*/NULL);
		V_1 = ((int32_t)((int32_t)L_12+(int32_t)L_19));
		int32_t L_20 = V_1;
		int32_t L_21 = ___size2;
		if ((!(((uint32_t)L_20) == ((uint32_t)L_21))))
		{
			goto IL_0073;
		}
	}
	{
		goto IL_007e;
	}

IL_0073:
	{
		int32_t L_22 = V_2;
		V_2 = ((int32_t)((int32_t)L_22+(int32_t)1));
	}

IL_0077:
	{
		int32_t L_23 = V_2;
		int32_t L_24 = V_0;
		if ((((int32_t)L_23) < ((int32_t)L_24)))
		{
			goto IL_0015;
		}
	}

IL_007e:
	{
		int32_t L_25 = V_1;
		return L_25;
	}
}
// System.Void System.Net.ChunkStream::Write(System.Byte[],System.Int32,System.Int32)
extern "C"  void ChunkStream_Write_m1232124261 (ChunkStream_t91719323 * __this, ByteU5BU5D_t3397334013* ___buffer0, int32_t ___offset1, int32_t ___size2, const MethodInfo* method)
{
	{
		ByteU5BU5D_t3397334013* L_0 = ___buffer0;
		int32_t L_1 = ___size2;
		ChunkStream_InternalWrite_m1159278446(__this, L_0, (&___offset1), L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Net.ChunkStream::InternalWrite(System.Byte[],System.Int32&,System.Int32)
extern "C"  void ChunkStream_InternalWrite_m1159278446 (ChunkStream_t91719323 * __this, ByteU5BU5D_t3397334013* ___buffer0, int32_t* ___offset1, int32_t ___size2, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_state_3();
		if (L_0)
		{
			goto IL_0040;
		}
	}
	{
		ByteU5BU5D_t3397334013* L_1 = ___buffer0;
		int32_t* L_2 = ___offset1;
		int32_t L_3 = ___size2;
		int32_t L_4 = ChunkStream_GetChunkSize_m3708881784(__this, L_1, L_2, L_3, /*hidden argument*/NULL);
		__this->set_state_3(L_4);
		int32_t L_5 = __this->get_state_3();
		if (L_5)
		{
			goto IL_0026;
		}
	}
	{
		return;
	}

IL_0026:
	{
		StringBuilder_t1221177846 * L_6 = __this->get_saved_4();
		NullCheck(L_6);
		StringBuilder_set_Length_m3039225444(L_6, 0, /*hidden argument*/NULL);
		__this->set_sawCR_5((bool)0);
		__this->set_gotit_6((bool)0);
	}

IL_0040:
	{
		int32_t L_7 = __this->get_state_3();
		if ((!(((uint32_t)L_7) == ((uint32_t)1))))
		{
			goto IL_0070;
		}
	}
	{
		int32_t* L_8 = ___offset1;
		int32_t L_9 = ___size2;
		if ((((int32_t)(*((int32_t*)L_8))) >= ((int32_t)L_9)))
		{
			goto IL_0070;
		}
	}
	{
		ByteU5BU5D_t3397334013* L_10 = ___buffer0;
		int32_t* L_11 = ___offset1;
		int32_t L_12 = ___size2;
		int32_t L_13 = ChunkStream_ReadBody_m4069812912(__this, L_10, L_11, L_12, /*hidden argument*/NULL);
		__this->set_state_3(L_13);
		int32_t L_14 = __this->get_state_3();
		if ((!(((uint32_t)L_14) == ((uint32_t)1))))
		{
			goto IL_0070;
		}
	}
	{
		return;
	}

IL_0070:
	{
		int32_t L_15 = __this->get_state_3();
		if ((!(((uint32_t)L_15) == ((uint32_t)2))))
		{
			goto IL_00a7;
		}
	}
	{
		int32_t* L_16 = ___offset1;
		int32_t L_17 = ___size2;
		if ((((int32_t)(*((int32_t*)L_16))) >= ((int32_t)L_17)))
		{
			goto IL_00a7;
		}
	}
	{
		ByteU5BU5D_t3397334013* L_18 = ___buffer0;
		int32_t* L_19 = ___offset1;
		int32_t L_20 = ___size2;
		int32_t L_21 = ChunkStream_ReadCRLF_m651757405(__this, L_18, L_19, L_20, /*hidden argument*/NULL);
		__this->set_state_3(L_21);
		int32_t L_22 = __this->get_state_3();
		if ((!(((uint32_t)L_22) == ((uint32_t)2))))
		{
			goto IL_00a0;
		}
	}
	{
		return;
	}

IL_00a0:
	{
		__this->set_sawCR_5((bool)0);
	}

IL_00a7:
	{
		int32_t L_23 = __this->get_state_3();
		if ((!(((uint32_t)L_23) == ((uint32_t)3))))
		{
			goto IL_00f1;
		}
	}
	{
		int32_t* L_24 = ___offset1;
		int32_t L_25 = ___size2;
		if ((((int32_t)(*((int32_t*)L_24))) >= ((int32_t)L_25)))
		{
			goto IL_00f1;
		}
	}
	{
		ByteU5BU5D_t3397334013* L_26 = ___buffer0;
		int32_t* L_27 = ___offset1;
		int32_t L_28 = ___size2;
		int32_t L_29 = ChunkStream_ReadTrailer_m1002886219(__this, L_26, L_27, L_28, /*hidden argument*/NULL);
		__this->set_state_3(L_29);
		int32_t L_30 = __this->get_state_3();
		if ((!(((uint32_t)L_30) == ((uint32_t)3))))
		{
			goto IL_00d7;
		}
	}
	{
		return;
	}

IL_00d7:
	{
		StringBuilder_t1221177846 * L_31 = __this->get_saved_4();
		NullCheck(L_31);
		StringBuilder_set_Length_m3039225444(L_31, 0, /*hidden argument*/NULL);
		__this->set_sawCR_5((bool)0);
		__this->set_gotit_6((bool)0);
	}

IL_00f1:
	{
		int32_t* L_32 = ___offset1;
		int32_t L_33 = ___size2;
		if ((((int32_t)(*((int32_t*)L_32))) >= ((int32_t)L_33)))
		{
			goto IL_0102;
		}
	}
	{
		ByteU5BU5D_t3397334013* L_34 = ___buffer0;
		int32_t* L_35 = ___offset1;
		int32_t L_36 = ___size2;
		ChunkStream_InternalWrite_m1159278446(__this, L_34, L_35, L_36, /*hidden argument*/NULL);
	}

IL_0102:
	{
		return;
	}
}
// System.Boolean System.Net.ChunkStream::get_WantMore()
extern "C"  bool ChunkStream_get_WantMore_m1215897223 (ChunkStream_t91719323 * __this, const MethodInfo* method)
{
	int32_t G_B4_0 = 0;
	{
		int32_t L_0 = __this->get_chunkRead_2();
		int32_t L_1 = __this->get_chunkSize_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_002a;
		}
	}
	{
		int32_t L_2 = __this->get_chunkSize_1();
		if (L_2)
		{
			goto IL_002a;
		}
	}
	{
		int32_t L_3 = __this->get_state_3();
		G_B4_0 = ((((int32_t)((((int32_t)L_3) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_002b;
	}

IL_002a:
	{
		G_B4_0 = 1;
	}

IL_002b:
	{
		return (bool)G_B4_0;
	}
}
// System.Int32 System.Net.ChunkStream::get_ChunkLeft()
extern "C"  int32_t ChunkStream_get_ChunkLeft_m3001057228 (ChunkStream_t91719323 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_chunkSize_1();
		int32_t L_1 = __this->get_chunkRead_2();
		return ((int32_t)((int32_t)L_0-(int32_t)L_1));
	}
}
// System.Net.ChunkStream/State System.Net.ChunkStream::ReadBody(System.Byte[],System.Int32&,System.Int32)
extern Il2CppClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern Il2CppClass* Chunk_t3860501603_il2cpp_TypeInfo_var;
extern const uint32_t ChunkStream_ReadBody_m4069812912_MetadataUsageId;
extern "C"  int32_t ChunkStream_ReadBody_m4069812912 (ChunkStream_t91719323 * __this, ByteU5BU5D_t3397334013* ___buffer0, int32_t* ___offset1, int32_t ___size2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ChunkStream_ReadBody_m4069812912_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	ByteU5BU5D_t3397334013* V_1 = NULL;
	int32_t G_B7_0 = 0;
	{
		int32_t L_0 = __this->get_chunkSize_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (int32_t)(2);
	}

IL_000d:
	{
		int32_t L_1 = ___size2;
		int32_t* L_2 = ___offset1;
		V_0 = ((int32_t)((int32_t)L_1-(int32_t)(*((int32_t*)L_2))));
		int32_t L_3 = V_0;
		int32_t L_4 = __this->get_chunkRead_2();
		int32_t L_5 = __this->get_chunkSize_1();
		if ((((int32_t)((int32_t)((int32_t)L_3+(int32_t)L_4))) <= ((int32_t)L_5)))
		{
			goto IL_0033;
		}
	}
	{
		int32_t L_6 = __this->get_chunkSize_1();
		int32_t L_7 = __this->get_chunkRead_2();
		V_0 = ((int32_t)((int32_t)L_6-(int32_t)L_7));
	}

IL_0033:
	{
		int32_t L_8 = V_0;
		V_1 = ((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)L_8));
		ByteU5BU5D_t3397334013* L_9 = ___buffer0;
		int32_t* L_10 = ___offset1;
		ByteU5BU5D_t3397334013* L_11 = V_1;
		int32_t L_12 = V_0;
		Buffer_BlockCopy_m1586717258(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_9, (*((int32_t*)L_10)), (Il2CppArray *)(Il2CppArray *)L_11, 0, L_12, /*hidden argument*/NULL);
		ArrayList_t4252133567 * L_13 = __this->get_chunks_8();
		ByteU5BU5D_t3397334013* L_14 = V_1;
		Chunk_t3860501603 * L_15 = (Chunk_t3860501603 *)il2cpp_codegen_object_new(Chunk_t3860501603_il2cpp_TypeInfo_var);
		Chunk__ctor_m1510239680(L_15, L_14, /*hidden argument*/NULL);
		NullCheck(L_13);
		VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_13, L_15);
		int32_t* L_16 = ___offset1;
		int32_t* L_17 = ___offset1;
		int32_t L_18 = V_0;
		*((int32_t*)(L_16)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_17))+(int32_t)L_18));
		int32_t L_19 = __this->get_chunkRead_2();
		int32_t L_20 = V_0;
		__this->set_chunkRead_2(((int32_t)((int32_t)L_19+(int32_t)L_20)));
		int32_t L_21 = __this->get_chunkRead_2();
		int32_t L_22 = __this->get_chunkSize_1();
		if ((!(((uint32_t)L_21) == ((uint32_t)L_22))))
		{
			goto IL_0082;
		}
	}
	{
		G_B7_0 = 2;
		goto IL_0083;
	}

IL_0082:
	{
		G_B7_0 = 1;
	}

IL_0083:
	{
		return (int32_t)(G_B7_0);
	}
}
// System.Net.ChunkStream/State System.Net.ChunkStream::GetChunkSize(System.Byte[],System.Int32&,System.Int32)
extern Il2CppClass* Exception_t1927440687_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral538630005;
extern Il2CppCodeGenString* _stringLiteral3783959438;
extern Il2CppCodeGenString* _stringLiteral3148707172;
extern Il2CppCodeGenString* _stringLiteral2454292366;
extern const uint32_t ChunkStream_GetChunkSize_m3708881784_MetadataUsageId;
extern "C"  int32_t ChunkStream_GetChunkSize_m3708881784 (ChunkStream_t91719323 * __this, ByteU5BU5D_t3397334013* ___buffer0, int32_t* ___offset1, int32_t ___size2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ChunkStream_GetChunkSize_m3708881784_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppChar V_0 = 0x0;
	int32_t V_1 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = 0;
		goto IL_0098;
	}

IL_0007:
	{
		ByteU5BU5D_t3397334013* L_0 = ___buffer0;
		int32_t* L_1 = ___offset1;
		int32_t* L_2 = ___offset1;
		int32_t L_3 = (*((int32_t*)L_2));
		V_1 = L_3;
		*((int32_t*)(L_1)) = (int32_t)((int32_t)((int32_t)L_3+(int32_t)1));
		int32_t L_4 = V_1;
		NullCheck(L_0);
		int32_t L_5 = L_4;
		uint8_t L_6 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		V_0 = (((int32_t)((uint16_t)L_6)));
		Il2CppChar L_7 = V_0;
		if ((!(((uint32_t)L_7) == ((uint32_t)((int32_t)13)))))
		{
			goto IL_003d;
		}
	}
	{
		bool L_8 = __this->get_sawCR_5();
		if (!L_8)
		{
			goto IL_0031;
		}
	}
	{
		ChunkStream_ThrowProtocolViolation_m900952530(NULL /*static, unused*/, _stringLiteral538630005, /*hidden argument*/NULL);
	}

IL_0031:
	{
		__this->set_sawCR_5((bool)1);
		goto IL_0098;
	}

IL_003d:
	{
		bool L_9 = __this->get_sawCR_5();
		if (!L_9)
		{
			goto IL_0055;
		}
	}
	{
		Il2CppChar L_10 = V_0;
		if ((!(((uint32_t)L_10) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_0055;
		}
	}
	{
		goto IL_00a0;
	}

IL_0055:
	{
		Il2CppChar L_11 = V_0;
		if ((!(((uint32_t)L_11) == ((uint32_t)((int32_t)32)))))
		{
			goto IL_0064;
		}
	}
	{
		__this->set_gotit_6((bool)1);
	}

IL_0064:
	{
		bool L_12 = __this->get_gotit_6();
		if (L_12)
		{
			goto IL_007c;
		}
	}
	{
		StringBuilder_t1221177846 * L_13 = __this->get_saved_4();
		Il2CppChar L_14 = V_0;
		NullCheck(L_13);
		StringBuilder_Append_m3618697540(L_13, L_14, /*hidden argument*/NULL);
	}

IL_007c:
	{
		StringBuilder_t1221177846 * L_15 = __this->get_saved_4();
		NullCheck(L_15);
		int32_t L_16 = StringBuilder_get_Length_m1608241323(L_15, /*hidden argument*/NULL);
		if ((((int32_t)L_16) <= ((int32_t)((int32_t)20))))
		{
			goto IL_0098;
		}
	}
	{
		ChunkStream_ThrowProtocolViolation_m900952530(NULL /*static, unused*/, _stringLiteral3783959438, /*hidden argument*/NULL);
	}

IL_0098:
	{
		int32_t* L_17 = ___offset1;
		int32_t L_18 = ___size2;
		if ((((int32_t)(*((int32_t*)L_17))) < ((int32_t)L_18)))
		{
			goto IL_0007;
		}
	}

IL_00a0:
	{
		bool L_19 = __this->get_sawCR_5();
		if (!L_19)
		{
			goto IL_00b3;
		}
	}
	{
		Il2CppChar L_20 = V_0;
		if ((((int32_t)L_20) == ((int32_t)((int32_t)10))))
		{
			goto IL_010d;
		}
	}

IL_00b3:
	{
		int32_t* L_21 = ___offset1;
		int32_t L_22 = ___size2;
		if ((((int32_t)(*((int32_t*)L_21))) >= ((int32_t)L_22)))
		{
			goto IL_00c5;
		}
	}
	{
		ChunkStream_ThrowProtocolViolation_m900952530(NULL /*static, unused*/, _stringLiteral3148707172, /*hidden argument*/NULL);
	}

IL_00c5:
	try
	{ // begin try (depth: 1)
		{
			StringBuilder_t1221177846 * L_23 = __this->get_saved_4();
			NullCheck(L_23);
			int32_t L_24 = StringBuilder_get_Length_m1608241323(L_23, /*hidden argument*/NULL);
			if ((((int32_t)L_24) <= ((int32_t)0)))
			{
				goto IL_00f6;
			}
		}

IL_00d6:
		{
			StringBuilder_t1221177846 * L_25 = __this->get_saved_4();
			NullCheck(L_25);
			String_t* L_26 = StringBuilder_ToString_m1507807375(L_25, /*hidden argument*/NULL);
			String_t* L_27 = ChunkStream_RemoveChunkExtension_m551025530(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
			int32_t L_28 = Int32_Parse_m837442623(NULL /*static, unused*/, L_27, ((int32_t)515), /*hidden argument*/NULL);
			__this->set_chunkSize_1(L_28);
		}

IL_00f6:
		{
			goto IL_010b;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1927440687_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_00fb;
		throw e;
	}

CATCH_00fb:
	{ // begin catch(System.Exception)
		ChunkStream_ThrowProtocolViolation_m900952530(NULL /*static, unused*/, _stringLiteral2454292366, /*hidden argument*/NULL);
		goto IL_010b;
	} // end catch (depth: 1)

IL_010b:
	{
		return (int32_t)(0);
	}

IL_010d:
	{
		__this->set_chunkRead_2(0);
	}

IL_0114:
	try
	{ // begin try (depth: 1)
		StringBuilder_t1221177846 * L_29 = __this->get_saved_4();
		NullCheck(L_29);
		String_t* L_30 = StringBuilder_ToString_m1507807375(L_29, /*hidden argument*/NULL);
		String_t* L_31 = ChunkStream_RemoveChunkExtension_m551025530(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		int32_t L_32 = Int32_Parse_m837442623(NULL /*static, unused*/, L_31, ((int32_t)515), /*hidden argument*/NULL);
		__this->set_chunkSize_1(L_32);
		goto IL_0149;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1927440687_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0139;
		throw e;
	}

CATCH_0139:
	{ // begin catch(System.Exception)
		ChunkStream_ThrowProtocolViolation_m900952530(NULL /*static, unused*/, _stringLiteral2454292366, /*hidden argument*/NULL);
		goto IL_0149;
	} // end catch (depth: 1)

IL_0149:
	{
		int32_t L_33 = __this->get_chunkSize_1();
		if (L_33)
		{
			goto IL_015d;
		}
	}
	{
		__this->set_trailerState_7(2);
		return (int32_t)(3);
	}

IL_015d:
	{
		return (int32_t)(1);
	}
}
// System.String System.Net.ChunkStream::RemoveChunkExtension(System.String)
extern "C"  String_t* ChunkStream_RemoveChunkExtension_m551025530 (Il2CppObject * __this /* static, unused */, String_t* ___input0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		String_t* L_0 = ___input0;
		NullCheck(L_0);
		int32_t L_1 = String_IndexOf_m2358239236(L_0, ((int32_t)59), /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_0012;
		}
	}
	{
		String_t* L_3 = ___input0;
		return L_3;
	}

IL_0012:
	{
		String_t* L_4 = ___input0;
		int32_t L_5 = V_0;
		NullCheck(L_4);
		String_t* L_6 = String_Substring_m12482732(L_4, 0, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Net.ChunkStream/State System.Net.ChunkStream::ReadCRLF(System.Byte[],System.Int32&,System.Int32)
extern Il2CppCodeGenString* _stringLiteral2444436081;
extern Il2CppCodeGenString* _stringLiteral475067613;
extern const uint32_t ChunkStream_ReadCRLF_m651757405_MetadataUsageId;
extern "C"  int32_t ChunkStream_ReadCRLF_m651757405 (ChunkStream_t91719323 * __this, ByteU5BU5D_t3397334013* ___buffer0, int32_t* ___offset1, int32_t ___size2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ChunkStream_ReadCRLF_m651757405_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		bool L_0 = __this->get_sawCR_5();
		if (L_0)
		{
			goto IL_0039;
		}
	}
	{
		ByteU5BU5D_t3397334013* L_1 = ___buffer0;
		int32_t* L_2 = ___offset1;
		int32_t* L_3 = ___offset1;
		int32_t L_4 = (*((int32_t*)L_3));
		V_0 = L_4;
		*((int32_t*)(L_2)) = (int32_t)((int32_t)((int32_t)L_4+(int32_t)1));
		int32_t L_5 = V_0;
		NullCheck(L_1);
		int32_t L_6 = L_5;
		uint8_t L_7 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		if ((((int32_t)(((int32_t)((uint16_t)L_7)))) == ((int32_t)((int32_t)13))))
		{
			goto IL_0028;
		}
	}
	{
		ChunkStream_ThrowProtocolViolation_m900952530(NULL /*static, unused*/, _stringLiteral2444436081, /*hidden argument*/NULL);
	}

IL_0028:
	{
		__this->set_sawCR_5((bool)1);
		int32_t* L_8 = ___offset1;
		int32_t L_9 = ___size2;
		if ((!(((uint32_t)(*((int32_t*)L_8))) == ((uint32_t)L_9))))
		{
			goto IL_0039;
		}
	}
	{
		return (int32_t)(2);
	}

IL_0039:
	{
		bool L_10 = __this->get_sawCR_5();
		if (!L_10)
		{
			goto IL_0061;
		}
	}
	{
		ByteU5BU5D_t3397334013* L_11 = ___buffer0;
		int32_t* L_12 = ___offset1;
		int32_t* L_13 = ___offset1;
		int32_t L_14 = (*((int32_t*)L_13));
		V_0 = L_14;
		*((int32_t*)(L_12)) = (int32_t)((int32_t)((int32_t)L_14+(int32_t)1));
		int32_t L_15 = V_0;
		NullCheck(L_11);
		int32_t L_16 = L_15;
		uint8_t L_17 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		if ((((int32_t)(((int32_t)((uint16_t)L_17)))) == ((int32_t)((int32_t)10))))
		{
			goto IL_0061;
		}
	}
	{
		ChunkStream_ThrowProtocolViolation_m900952530(NULL /*static, unused*/, _stringLiteral475067613, /*hidden argument*/NULL);
	}

IL_0061:
	{
		return (int32_t)(0);
	}
}
// System.Net.ChunkStream/State System.Net.ChunkStream::ReadTrailer(System.Byte[],System.Int32&,System.Int32)
extern Il2CppClass* StringReader_t1480123486_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2168004016;
extern Il2CppCodeGenString* _stringLiteral4048851412;
extern Il2CppCodeGenString* _stringLiteral1456051193;
extern const uint32_t ChunkStream_ReadTrailer_m1002886219_MetadataUsageId;
extern "C"  int32_t ChunkStream_ReadTrailer_m1002886219 (ChunkStream_t91719323 * __this, ByteU5BU5D_t3397334013* ___buffer0, int32_t* ___offset1, int32_t ___size2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ChunkStream_ReadTrailer_m1002886219_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppChar V_0 = 0x0;
	int32_t V_1 = 0;
	String_t* V_2 = NULL;
	StringReader_t1480123486 * V_3 = NULL;
	String_t* V_4 = NULL;
	int32_t V_5 = 0;
	int32_t G_B19_0 = 0;
	String_t* G_B19_1 = NULL;
	StringBuilder_t1221177846 * G_B19_2 = NULL;
	int32_t G_B18_0 = 0;
	String_t* G_B18_1 = NULL;
	StringBuilder_t1221177846 * G_B18_2 = NULL;
	int32_t G_B20_0 = 0;
	int32_t G_B20_1 = 0;
	String_t* G_B20_2 = NULL;
	StringBuilder_t1221177846 * G_B20_3 = NULL;
	{
		V_0 = 0;
		int32_t L_0 = __this->get_trailerState_7();
		if ((!(((uint32_t)L_0) == ((uint32_t)2))))
		{
			goto IL_0052;
		}
	}
	{
		ByteU5BU5D_t3397334013* L_1 = ___buffer0;
		int32_t* L_2 = ___offset1;
		NullCheck(L_1);
		int32_t L_3 = (*((int32_t*)L_2));
		uint8_t L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		if ((!(((uint32_t)(((int32_t)((uint16_t)L_4)))) == ((uint32_t)((int32_t)13)))))
		{
			goto IL_0052;
		}
	}
	{
		StringBuilder_t1221177846 * L_5 = __this->get_saved_4();
		NullCheck(L_5);
		int32_t L_6 = StringBuilder_get_Length_m1608241323(L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0052;
		}
	}
	{
		int32_t* L_7 = ___offset1;
		int32_t* L_8 = ___offset1;
		*((int32_t*)(L_7)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_8))+(int32_t)1));
		int32_t* L_9 = ___offset1;
		int32_t L_10 = ___size2;
		if ((((int32_t)(*((int32_t*)L_9))) >= ((int32_t)L_10)))
		{
			goto IL_004c;
		}
	}
	{
		ByteU5BU5D_t3397334013* L_11 = ___buffer0;
		int32_t* L_12 = ___offset1;
		NullCheck(L_11);
		int32_t L_13 = (*((int32_t*)L_12));
		uint8_t L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		if ((!(((uint32_t)(((int32_t)((uint16_t)L_14)))) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_004c;
		}
	}
	{
		int32_t* L_15 = ___offset1;
		int32_t* L_16 = ___offset1;
		*((int32_t*)(L_15)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_16))+(int32_t)1));
		return (int32_t)(0);
	}

IL_004c:
	{
		int32_t* L_17 = ___offset1;
		int32_t* L_18 = ___offset1;
		*((int32_t*)(L_17)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_18))-(int32_t)1));
	}

IL_0052:
	{
		int32_t L_19 = __this->get_trailerState_7();
		V_1 = L_19;
		V_2 = _stringLiteral2168004016;
		goto IL_0104;
	}

IL_0064:
	{
		ByteU5BU5D_t3397334013* L_20 = ___buffer0;
		int32_t* L_21 = ___offset1;
		int32_t* L_22 = ___offset1;
		int32_t L_23 = (*((int32_t*)L_22));
		V_5 = L_23;
		*((int32_t*)(L_21)) = (int32_t)((int32_t)((int32_t)L_23+(int32_t)1));
		int32_t L_24 = V_5;
		NullCheck(L_20);
		int32_t L_25 = L_24;
		uint8_t L_26 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_25));
		V_0 = (((int32_t)((uint16_t)L_26)));
		int32_t L_27 = V_1;
		if (!L_27)
		{
			goto IL_0080;
		}
	}
	{
		int32_t L_28 = V_1;
		if ((!(((uint32_t)L_28) == ((uint32_t)2))))
		{
			goto IL_0091;
		}
	}

IL_0080:
	{
		Il2CppChar L_29 = V_0;
		if ((!(((uint32_t)L_29) == ((uint32_t)((int32_t)13)))))
		{
			goto IL_0091;
		}
	}
	{
		int32_t L_30 = V_1;
		V_1 = ((int32_t)((int32_t)L_30+(int32_t)1));
		goto IL_0104;
	}

IL_0091:
	{
		int32_t L_31 = V_1;
		if ((((int32_t)L_31) == ((int32_t)1)))
		{
			goto IL_009f;
		}
	}
	{
		int32_t L_32 = V_1;
		if ((!(((uint32_t)L_32) == ((uint32_t)3))))
		{
			goto IL_00b0;
		}
	}

IL_009f:
	{
		Il2CppChar L_33 = V_0;
		if ((!(((uint32_t)L_33) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_00b0;
		}
	}
	{
		int32_t L_34 = V_1;
		V_1 = ((int32_t)((int32_t)L_34+(int32_t)1));
		goto IL_0104;
	}

IL_00b0:
	{
		int32_t L_35 = V_1;
		if ((((int32_t)L_35) <= ((int32_t)0)))
		{
			goto IL_0104;
		}
	}
	{
		StringBuilder_t1221177846 * L_36 = __this->get_saved_4();
		String_t* L_37 = V_2;
		StringBuilder_t1221177846 * L_38 = __this->get_saved_4();
		NullCheck(L_38);
		int32_t L_39 = StringBuilder_get_Length_m1608241323(L_38, /*hidden argument*/NULL);
		G_B18_0 = 0;
		G_B18_1 = L_37;
		G_B18_2 = L_36;
		if (L_39)
		{
			G_B19_0 = 0;
			G_B19_1 = L_37;
			G_B19_2 = L_36;
			goto IL_00d7;
		}
	}
	{
		int32_t L_40 = V_1;
		G_B20_0 = ((int32_t)((int32_t)L_40-(int32_t)2));
		G_B20_1 = G_B18_0;
		G_B20_2 = G_B18_1;
		G_B20_3 = G_B18_2;
		goto IL_00d8;
	}

IL_00d7:
	{
		int32_t L_41 = V_1;
		G_B20_0 = L_41;
		G_B20_1 = G_B19_0;
		G_B20_2 = G_B19_1;
		G_B20_3 = G_B19_2;
	}

IL_00d8:
	{
		NullCheck(G_B20_2);
		String_t* L_42 = String_Substring_m12482732(G_B20_2, G_B20_1, G_B20_0, /*hidden argument*/NULL);
		NullCheck(G_B20_3);
		StringBuilder_Append_m3636508479(G_B20_3, L_42, /*hidden argument*/NULL);
		V_1 = 0;
		StringBuilder_t1221177846 * L_43 = __this->get_saved_4();
		NullCheck(L_43);
		int32_t L_44 = StringBuilder_get_Length_m1608241323(L_43, /*hidden argument*/NULL);
		if ((((int32_t)L_44) <= ((int32_t)((int32_t)4196))))
		{
			goto IL_0104;
		}
	}
	{
		ChunkStream_ThrowProtocolViolation_m900952530(NULL /*static, unused*/, _stringLiteral4048851412, /*hidden argument*/NULL);
	}

IL_0104:
	{
		int32_t* L_45 = ___offset1;
		int32_t L_46 = ___size2;
		if ((((int32_t)(*((int32_t*)L_45))) >= ((int32_t)L_46)))
		{
			goto IL_0113;
		}
	}
	{
		int32_t L_47 = V_1;
		if ((((int32_t)L_47) < ((int32_t)4)))
		{
			goto IL_0064;
		}
	}

IL_0113:
	{
		int32_t L_48 = V_1;
		if ((((int32_t)L_48) >= ((int32_t)4)))
		{
			goto IL_0135;
		}
	}
	{
		int32_t L_49 = V_1;
		__this->set_trailerState_7(L_49);
		int32_t* L_50 = ___offset1;
		int32_t L_51 = ___size2;
		if ((((int32_t)(*((int32_t*)L_50))) >= ((int32_t)L_51)))
		{
			goto IL_0133;
		}
	}
	{
		ChunkStream_ThrowProtocolViolation_m900952530(NULL /*static, unused*/, _stringLiteral1456051193, /*hidden argument*/NULL);
	}

IL_0133:
	{
		return (int32_t)(3);
	}

IL_0135:
	{
		StringBuilder_t1221177846 * L_52 = __this->get_saved_4();
		NullCheck(L_52);
		String_t* L_53 = StringBuilder_ToString_m1507807375(L_52, /*hidden argument*/NULL);
		StringReader_t1480123486 * L_54 = (StringReader_t1480123486 *)il2cpp_codegen_object_new(StringReader_t1480123486_il2cpp_TypeInfo_var);
		StringReader__ctor_m643998729(L_54, L_53, /*hidden argument*/NULL);
		V_3 = L_54;
		goto IL_0158;
	}

IL_014b:
	{
		WebHeaderCollection_t3028142837 * L_55 = __this->get_headers_0();
		String_t* L_56 = V_4;
		NullCheck(L_55);
		WebHeaderCollection_Add_m1093270864(L_55, L_56, /*hidden argument*/NULL);
	}

IL_0158:
	{
		StringReader_t1480123486 * L_57 = V_3;
		NullCheck(L_57);
		String_t* L_58 = VirtFuncInvoker0< String_t* >::Invoke(10 /* System.String System.IO.StringReader::ReadLine() */, L_57);
		String_t* L_59 = L_58;
		V_4 = L_59;
		if (!L_59)
		{
			goto IL_0177;
		}
	}
	{
		String_t* L_60 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_61 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_62 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_60, L_61, /*hidden argument*/NULL);
		if (L_62)
		{
			goto IL_014b;
		}
	}

IL_0177:
	{
		return (int32_t)(0);
	}
}
// System.Void System.Net.ChunkStream::ThrowProtocolViolation(System.String)
extern Il2CppClass* WebException_t3368933679_il2cpp_TypeInfo_var;
extern const uint32_t ChunkStream_ThrowProtocolViolation_m900952530_MetadataUsageId;
extern "C"  void ChunkStream_ThrowProtocolViolation_m900952530 (Il2CppObject * __this /* static, unused */, String_t* ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ChunkStream_ThrowProtocolViolation_m900952530_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	WebException_t3368933679 * V_0 = NULL;
	{
		String_t* L_0 = ___message0;
		WebException_t3368933679 * L_1 = (WebException_t3368933679 *)il2cpp_codegen_object_new(WebException_t3368933679_il2cpp_TypeInfo_var);
		WebException__ctor_m2717250633(L_1, L_0, (Exception_t1927440687 *)NULL, ((int32_t)11), (WebResponse_t1895226051 *)NULL, /*hidden argument*/NULL);
		V_0 = L_1;
		WebException_t3368933679 * L_2 = V_0;
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}
}
// System.Void System.Net.ChunkStream/Chunk::.ctor(System.Byte[])
extern "C"  void Chunk__ctor_m1510239680 (Chunk_t3860501603 * __this, ByteU5BU5D_t3397334013* ___chunk0, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_0 = ___chunk0;
		__this->set_Bytes_0(L_0);
		return;
	}
}
// System.Int32 System.Net.ChunkStream/Chunk::Read(System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t Chunk_Read_m647929994 (Chunk_t3860501603 * __this, ByteU5BU5D_t3397334013* ___buffer0, int32_t ___offset1, int32_t ___size2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = ___size2;
		ByteU5BU5D_t3397334013* L_1 = __this->get_Bytes_0();
		NullCheck(L_1);
		int32_t L_2 = __this->get_Offset_1();
		if ((((int32_t)L_0) <= ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))-(int32_t)L_2)))))
		{
			goto IL_0029;
		}
	}
	{
		ByteU5BU5D_t3397334013* L_3 = __this->get_Bytes_0();
		NullCheck(L_3);
		int32_t L_4 = __this->get_Offset_1();
		G_B3_0 = ((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length))))-(int32_t)L_4));
		goto IL_002a;
	}

IL_0029:
	{
		int32_t L_5 = ___size2;
		G_B3_0 = L_5;
	}

IL_002a:
	{
		V_0 = G_B3_0;
		ByteU5BU5D_t3397334013* L_6 = __this->get_Bytes_0();
		int32_t L_7 = __this->get_Offset_1();
		ByteU5BU5D_t3397334013* L_8 = ___buffer0;
		int32_t L_9 = ___offset1;
		int32_t L_10 = V_0;
		Buffer_BlockCopy_m1586717258(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_6, L_7, (Il2CppArray *)(Il2CppArray *)L_8, L_9, L_10, /*hidden argument*/NULL);
		int32_t L_11 = __this->get_Offset_1();
		int32_t L_12 = V_0;
		__this->set_Offset_1(((int32_t)((int32_t)L_11+(int32_t)L_12)));
		int32_t L_13 = V_0;
		return L_13;
	}
}
// System.Void System.Net.Cookie::.ctor()
extern Il2CppClass* DateTime_t693205669_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Cookie__ctor_m1446200832_MetadataUsageId;
extern "C"  void Cookie__ctor_m1446200832 (Cookie_t3154017544 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Cookie__ctor_m1446200832_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t693205669_il2cpp_TypeInfo_var);
		DateTime_t693205669  L_0 = ((DateTime_t693205669_StaticFields*)DateTime_t693205669_il2cpp_TypeInfo_var->static_fields)->get_MinValue_13();
		__this->set_expires_4(L_0);
		DateTime_t693205669  L_1 = DateTime_get_Now_m24136300(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_timestamp_11(L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_domain_3(L_2);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_name_6(L_3);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_val_12(L_4);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_comment_0(L_5);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_port_8(L_6);
		return;
	}
}
// System.Void System.Net.Cookie::.ctor(System.String,System.String)
extern "C"  void Cookie__ctor_m1989068482 (Cookie_t3154017544 * __this, String_t* ___name0, String_t* ___value1, const MethodInfo* method)
{
	{
		Cookie__ctor_m1446200832(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name0;
		Cookie_set_Name_m143303530(__this, L_0, /*hidden argument*/NULL);
		String_t* L_1 = ___value1;
		Cookie_set_Value_m1671590012(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Net.Cookie::.cctor()
extern Il2CppClass* CharU5BU5D_t1328083999_il2cpp_TypeInfo_var;
extern Il2CppClass* Cookie_t3154017544_il2cpp_TypeInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t1486305139____U24U24fieldU2D1_0_FieldInfo_var;
extern Il2CppCodeGenString* _stringLiteral749251466;
extern const uint32_t Cookie__cctor_m3187715567_MetadataUsageId;
extern "C"  void Cookie__cctor_m3187715567 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Cookie__cctor_m3187715567_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		CharU5BU5D_t1328083999* L_0 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)7));
		RuntimeHelpers_InitializeArray_m3920580167(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t1486305139____U24U24fieldU2D1_0_FieldInfo_var), /*hidden argument*/NULL);
		((Cookie_t3154017544_StaticFields*)Cookie_t3154017544_il2cpp_TypeInfo_var->static_fields)->set_reservedCharsName_14(L_0);
		CharU5BU5D_t1328083999* L_1 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)2));
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)34));
		CharU5BU5D_t1328083999* L_2 = L_1;
		NullCheck(L_2);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppChar)((int32_t)44));
		((Cookie_t3154017544_StaticFields*)Cookie_t3154017544_il2cpp_TypeInfo_var->static_fields)->set_portSeparators_15(L_2);
		((Cookie_t3154017544_StaticFields*)Cookie_t3154017544_il2cpp_TypeInfo_var->static_fields)->set_tspecials_16(_stringLiteral749251466);
		return;
	}
}
// System.String System.Net.Cookie::get_Comment()
extern "C"  String_t* Cookie_get_Comment_m2183197327 (Cookie_t3154017544 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_comment_0();
		return L_0;
	}
}
// System.Void System.Net.Cookie::set_Comment(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Cookie_set_Comment_m1504498068_MetadataUsageId;
extern "C"  void Cookie_set_Comment_m1504498068 (Cookie_t3154017544 * __this, String_t* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Cookie_set_Comment_m1504498068_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Cookie_t3154017544 * G_B2_0 = NULL;
	Cookie_t3154017544 * G_B1_0 = NULL;
	String_t* G_B3_0 = NULL;
	Cookie_t3154017544 * G_B3_1 = NULL;
	{
		String_t* L_0 = ___value0;
		G_B1_0 = __this;
		if (L_0)
		{
			G_B2_0 = __this;
			goto IL_0011;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_1;
		G_B3_1 = G_B1_0;
		goto IL_0012;
	}

IL_0011:
	{
		String_t* L_2 = ___value0;
		G_B3_0 = L_2;
		G_B3_1 = G_B2_0;
	}

IL_0012:
	{
		NullCheck(G_B3_1);
		G_B3_1->set_comment_0(G_B3_0);
		return;
	}
}
// System.Uri System.Net.Cookie::get_CommentUri()
extern "C"  Uri_t19570940 * Cookie_get_CommentUri_m4105081148 (Cookie_t3154017544 * __this, const MethodInfo* method)
{
	{
		Uri_t19570940 * L_0 = __this->get_commentUri_1();
		return L_0;
	}
}
// System.Void System.Net.Cookie::set_CommentUri(System.Uri)
extern "C"  void Cookie_set_CommentUri_m2880099413 (Cookie_t3154017544 * __this, Uri_t19570940 * ___value0, const MethodInfo* method)
{
	{
		Uri_t19570940 * L_0 = ___value0;
		__this->set_commentUri_1(L_0);
		return;
	}
}
// System.Boolean System.Net.Cookie::get_Discard()
extern "C"  bool Cookie_get_Discard_m4277637175 (Cookie_t3154017544 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_discard_2();
		return L_0;
	}
}
// System.Void System.Net.Cookie::set_Discard(System.Boolean)
extern "C"  void Cookie_set_Discard_m8095924 (Cookie_t3154017544 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_discard_2(L_0);
		return;
	}
}
// System.String System.Net.Cookie::get_Domain()
extern "C"  String_t* Cookie_get_Domain_m78058352 (Cookie_t3154017544 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_domain_3();
		return L_0;
	}
}
// System.Void System.Net.Cookie::set_Domain(System.String)
extern Il2CppClass* Cookie_t3154017544_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Cookie_set_Domain_m334320991_MetadataUsageId;
extern "C"  void Cookie_set_Domain_m334320991 (Cookie_t3154017544 * __this, String_t* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Cookie_set_Domain_m334320991_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Cookie_t3154017544_il2cpp_TypeInfo_var);
		bool L_1 = Cookie_IsNullOrEmpty_m3107130375(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_domain_3(L_2);
		Cookie_set_ExactDomain_m1681540717(__this, (bool)1, /*hidden argument*/NULL);
		goto IL_003d;
	}

IL_0022:
	{
		String_t* L_3 = ___value0;
		__this->set_domain_3(L_3);
		String_t* L_4 = ___value0;
		NullCheck(L_4);
		Il2CppChar L_5 = String_get_Chars_m4230566705(L_4, 0, /*hidden argument*/NULL);
		Cookie_set_ExactDomain_m1681540717(__this, (bool)((((int32_t)((((int32_t)L_5) == ((int32_t)((int32_t)46)))? 1 : 0)) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
	}

IL_003d:
	{
		return;
	}
}
// System.Boolean System.Net.Cookie::get_ExactDomain()
extern "C"  bool Cookie_get_ExactDomain_m3704355968 (Cookie_t3154017544 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_exact_domain_17();
		return L_0;
	}
}
// System.Void System.Net.Cookie::set_ExactDomain(System.Boolean)
extern "C"  void Cookie_set_ExactDomain_m1681540717 (Cookie_t3154017544 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_exact_domain_17(L_0);
		return;
	}
}
// System.Boolean System.Net.Cookie::get_Expired()
extern Il2CppClass* DateTime_t693205669_il2cpp_TypeInfo_var;
extern const uint32_t Cookie_get_Expired_m1519158270_MetadataUsageId;
extern "C"  bool Cookie_get_Expired_m1519158270 (Cookie_t3154017544 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Cookie_get_Expired_m1519158270_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		DateTime_t693205669  L_0 = __this->get_expires_4();
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t693205669_il2cpp_TypeInfo_var);
		DateTime_t693205669  L_1 = DateTime_get_Now_m24136300(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_2 = DateTime_op_LessThanOrEqual_m2191641069(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0027;
		}
	}
	{
		DateTime_t693205669  L_3 = __this->get_expires_4();
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t693205669_il2cpp_TypeInfo_var);
		DateTime_t693205669  L_4 = ((DateTime_t693205669_StaticFields*)DateTime_t693205669_il2cpp_TypeInfo_var->static_fields)->get_MinValue_13();
		bool L_5 = DateTime_op_Inequality_m1607380213(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_5));
		goto IL_0028;
	}

IL_0027:
	{
		G_B3_0 = 0;
	}

IL_0028:
	{
		return (bool)G_B3_0;
	}
}
// System.DateTime System.Net.Cookie::get_Expires()
extern "C"  DateTime_t693205669  Cookie_get_Expires_m522897886 (Cookie_t3154017544 * __this, const MethodInfo* method)
{
	{
		DateTime_t693205669  L_0 = __this->get_expires_4();
		return L_0;
	}
}
// System.Void System.Net.Cookie::set_Expires(System.DateTime)
extern "C"  void Cookie_set_Expires_m1840037255 (Cookie_t3154017544 * __this, DateTime_t693205669  ___value0, const MethodInfo* method)
{
	{
		DateTime_t693205669  L_0 = ___value0;
		__this->set_expires_4(L_0);
		return;
	}
}
// System.Boolean System.Net.Cookie::get_HttpOnly()
extern "C"  bool Cookie_get_HttpOnly_m252147641 (Cookie_t3154017544 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_httpOnly_5();
		return L_0;
	}
}
// System.Void System.Net.Cookie::set_HttpOnly(System.Boolean)
extern "C"  void Cookie_set_HttpOnly_m2601679756 (Cookie_t3154017544 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_httpOnly_5(L_0);
		return;
	}
}
// System.String System.Net.Cookie::get_Name()
extern "C"  String_t* Cookie_get_Name_m892827391 (Cookie_t3154017544 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_name_6();
		return L_0;
	}
}
// System.Void System.Net.Cookie::set_Name(System.String)
extern Il2CppClass* Cookie_t3154017544_il2cpp_TypeInfo_var;
extern Il2CppClass* CookieException_t1505724635_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral345660856;
extern Il2CppCodeGenString* _stringLiteral3505558023;
extern const uint32_t Cookie_set_Name_m143303530_MetadataUsageId;
extern "C"  void Cookie_set_Name_m143303530 (Cookie_t3154017544 * __this, String_t* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Cookie_set_Name_m143303530_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Cookie_t3154017544_il2cpp_TypeInfo_var);
		bool L_1 = Cookie_IsNullOrEmpty_m3107130375(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		CookieException_t1505724635 * L_2 = (CookieException_t1505724635 *)il2cpp_codegen_object_new(CookieException_t1505724635_il2cpp_TypeInfo_var);
		CookieException__ctor_m39051905(L_2, _stringLiteral345660856, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0016:
	{
		String_t* L_3 = ___value0;
		NullCheck(L_3);
		Il2CppChar L_4 = String_get_Chars_m4230566705(L_3, 0, /*hidden argument*/NULL);
		if ((((int32_t)L_4) == ((int32_t)((int32_t)36))))
		{
			goto IL_0035;
		}
	}
	{
		String_t* L_5 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Cookie_t3154017544_il2cpp_TypeInfo_var);
		CharU5BU5D_t1328083999* L_6 = ((Cookie_t3154017544_StaticFields*)Cookie_t3154017544_il2cpp_TypeInfo_var->static_fields)->get_reservedCharsName_14();
		NullCheck(L_5);
		int32_t L_7 = String_IndexOfAny_m2016554902(L_5, L_6, /*hidden argument*/NULL);
		if ((((int32_t)L_7) == ((int32_t)(-1))))
		{
			goto IL_004b;
		}
	}

IL_0035:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_name_6(L_8);
		CookieException_t1505724635 * L_9 = (CookieException_t1505724635 *)il2cpp_codegen_object_new(CookieException_t1505724635_il2cpp_TypeInfo_var);
		CookieException__ctor_m39051905(L_9, _stringLiteral3505558023, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_004b:
	{
		String_t* L_10 = ___value0;
		__this->set_name_6(L_10);
		return;
	}
}
// System.String System.Net.Cookie::get_Path()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Cookie_get_Path_m4051852505_MetadataUsageId;
extern "C"  String_t* Cookie_get_Path_m4051852505 (Cookie_t3154017544 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Cookie_get_Path_m4051852505_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* G_B3_0 = NULL;
	{
		String_t* L_0 = __this->get_path_7();
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_1;
		goto IL_001b;
	}

IL_0015:
	{
		String_t* L_2 = __this->get_path_7();
		G_B3_0 = L_2;
	}

IL_001b:
	{
		return G_B3_0;
	}
}
// System.Void System.Net.Cookie::set_Path(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Cookie_set_Path_m3432117582_MetadataUsageId;
extern "C"  void Cookie_set_Path_m3432117582 (Cookie_t3154017544 * __this, String_t* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Cookie_set_Path_m3432117582_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Cookie_t3154017544 * G_B2_0 = NULL;
	Cookie_t3154017544 * G_B1_0 = NULL;
	String_t* G_B3_0 = NULL;
	Cookie_t3154017544 * G_B3_1 = NULL;
	{
		String_t* L_0 = ___value0;
		G_B1_0 = __this;
		if (L_0)
		{
			G_B2_0 = __this;
			goto IL_0011;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_1;
		G_B3_1 = G_B1_0;
		goto IL_0012;
	}

IL_0011:
	{
		String_t* L_2 = ___value0;
		G_B3_0 = L_2;
		G_B3_1 = G_B2_0;
	}

IL_0012:
	{
		NullCheck(G_B3_1);
		G_B3_1->set_path_7(G_B3_0);
		return;
	}
}
// System.String System.Net.Cookie::get_Port()
extern "C"  String_t* Cookie_get_Port_m3259286681 (Cookie_t3154017544 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_port_8();
		return L_0;
	}
}
// System.Void System.Net.Cookie::set_Port(System.String)
extern Il2CppClass* Cookie_t3154017544_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* CookieException_t1505724635_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1927440687_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral17862528;
extern Il2CppCodeGenString* _stringLiteral2462063384;
extern Il2CppCodeGenString* _stringLiteral120662745;
extern const uint32_t Cookie_set_Port_m312020554_MetadataUsageId;
extern "C"  void Cookie_set_Port_m312020554 (Cookie_t3154017544 * __this, String_t* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Cookie_set_Port_m312020554_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringU5BU5D_t1642385972* V_0 = NULL;
	int32_t V_1 = 0;
	Exception_t1927440687 * V_2 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Cookie_t3154017544_il2cpp_TypeInfo_var);
		bool L_1 = Cookie_IsNullOrEmpty_m3107130375(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_port_8(L_2);
		return;
	}

IL_0017:
	{
		String_t* L_3 = ___value0;
		NullCheck(L_3);
		Il2CppChar L_4 = String_get_Chars_m4230566705(L_3, 0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_4) == ((uint32_t)((int32_t)34)))))
		{
			goto IL_003a;
		}
	}
	{
		String_t* L_5 = ___value0;
		String_t* L_6 = ___value0;
		NullCheck(L_6);
		int32_t L_7 = String_get_Length_m1606060069(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		Il2CppChar L_8 = String_get_Chars_m4230566705(L_5, ((int32_t)((int32_t)L_7-(int32_t)1)), /*hidden argument*/NULL);
		if ((((int32_t)L_8) == ((int32_t)((int32_t)34))))
		{
			goto IL_0050;
		}
	}

IL_003a:
	{
		String_t* L_9 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral17862528, L_9, _stringLiteral2462063384, /*hidden argument*/NULL);
		CookieException_t1505724635 * L_11 = (CookieException_t1505724635 *)il2cpp_codegen_object_new(CookieException_t1505724635_il2cpp_TypeInfo_var);
		CookieException__ctor_m39051905(L_11, L_10, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_11);
	}

IL_0050:
	{
		String_t* L_12 = ___value0;
		__this->set_port_8(L_12);
		String_t* L_13 = __this->get_port_8();
		IL2CPP_RUNTIME_CLASS_INIT(Cookie_t3154017544_il2cpp_TypeInfo_var);
		CharU5BU5D_t1328083999* L_14 = ((Cookie_t3154017544_StaticFields*)Cookie_t3154017544_il2cpp_TypeInfo_var->static_fields)->get_portSeparators_15();
		NullCheck(L_13);
		StringU5BU5D_t1642385972* L_15 = String_Split_m3326265864(L_13, L_14, /*hidden argument*/NULL);
		V_0 = L_15;
		StringU5BU5D_t1642385972* L_16 = V_0;
		NullCheck(L_16);
		__this->set_ports_9(((Int32U5BU5D_t3030399641*)SZArrayNew(Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_16)->max_length)))))));
		V_1 = 0;
		goto IL_00d5;
	}

IL_007d:
	{
		Int32U5BU5D_t3030399641* L_17 = __this->get_ports_9();
		int32_t L_18 = V_1;
		NullCheck(L_17);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(L_18), (int32_t)((int32_t)-2147483648LL));
		StringU5BU5D_t1642385972* L_19 = V_0;
		int32_t L_20 = V_1;
		NullCheck(L_19);
		int32_t L_21 = L_20;
		String_t* L_22 = (L_19)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		NullCheck(L_22);
		int32_t L_23 = String_get_Length_m1606060069(L_22, /*hidden argument*/NULL);
		if (L_23)
		{
			goto IL_009c;
		}
	}
	{
		goto IL_00d1;
	}

IL_009c:
	try
	{ // begin try (depth: 1)
		Int32U5BU5D_t3030399641* L_24 = __this->get_ports_9();
		int32_t L_25 = V_1;
		StringU5BU5D_t1642385972* L_26 = V_0;
		int32_t L_27 = V_1;
		NullCheck(L_26);
		int32_t L_28 = L_27;
		String_t* L_29 = (L_26)->GetAt(static_cast<il2cpp_array_size_t>(L_28));
		int32_t L_30 = Int32_Parse_m3683414232(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		NullCheck(L_24);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(L_25), (int32_t)L_30);
		goto IL_00d1;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1927440687_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_00b1;
		throw e;
	}

CATCH_00b1:
	{ // begin catch(System.Exception)
		{
			V_2 = ((Exception_t1927440687 *)__exception_local);
			String_t* L_31 = ___value0;
			StringU5BU5D_t1642385972* L_32 = V_0;
			int32_t L_33 = V_1;
			NullCheck(L_32);
			int32_t L_34 = L_33;
			String_t* L_35 = (L_32)->GetAt(static_cast<il2cpp_array_size_t>(L_34));
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_36 = String_Concat_m1561703559(NULL /*static, unused*/, _stringLiteral17862528, L_31, _stringLiteral120662745, L_35, /*hidden argument*/NULL);
			Exception_t1927440687 * L_37 = V_2;
			CookieException_t1505724635 * L_38 = (CookieException_t1505724635 *)il2cpp_codegen_object_new(CookieException_t1505724635_il2cpp_TypeInfo_var);
			CookieException__ctor_m2392418273(L_38, L_36, L_37, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_38);
		}

IL_00cc:
		{
			goto IL_00d1;
		}
	} // end catch (depth: 1)

IL_00d1:
	{
		int32_t L_39 = V_1;
		V_1 = ((int32_t)((int32_t)L_39+(int32_t)1));
	}

IL_00d5:
	{
		int32_t L_40 = V_1;
		Int32U5BU5D_t3030399641* L_41 = __this->get_ports_9();
		NullCheck(L_41);
		if ((((int32_t)L_40) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_41)->max_length)))))))
		{
			goto IL_007d;
		}
	}
	{
		Cookie_set_Version_m1740254262(__this, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32[] System.Net.Cookie::get_Ports()
extern "C"  Int32U5BU5D_t3030399641* Cookie_get_Ports_m3110993987 (Cookie_t3154017544 * __this, const MethodInfo* method)
{
	{
		Int32U5BU5D_t3030399641* L_0 = __this->get_ports_9();
		return L_0;
	}
}
// System.Boolean System.Net.Cookie::get_Secure()
extern "C"  bool Cookie_get_Secure_m3357936990 (Cookie_t3154017544 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_secure_10();
		return L_0;
	}
}
// System.Void System.Net.Cookie::set_Secure(System.Boolean)
extern "C"  void Cookie_set_Secure_m2609176681 (Cookie_t3154017544 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_secure_10(L_0);
		return;
	}
}
// System.DateTime System.Net.Cookie::get_TimeStamp()
extern "C"  DateTime_t693205669  Cookie_get_TimeStamp_m2398884362 (Cookie_t3154017544 * __this, const MethodInfo* method)
{
	{
		DateTime_t693205669  L_0 = __this->get_timestamp_11();
		return L_0;
	}
}
// System.String System.Net.Cookie::get_Value()
extern "C"  String_t* Cookie_get_Value_m2421772599 (Cookie_t3154017544 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_val_12();
		return L_0;
	}
}
// System.Void System.Net.Cookie::set_Value(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Cookie_set_Value_m1671590012_MetadataUsageId;
extern "C"  void Cookie_set_Value_m1671590012 (Cookie_t3154017544 * __this, String_t* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Cookie_set_Value_m1671590012_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___value0;
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_val_12(L_1);
		return;
	}

IL_0012:
	{
		String_t* L_2 = ___value0;
		__this->set_val_12(L_2);
		return;
	}
}
// System.Int32 System.Net.Cookie::get_Version()
extern "C"  int32_t Cookie_get_Version_m1900649029 (Cookie_t3154017544 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_version_13();
		return L_0;
	}
}
// System.Void System.Net.Cookie::set_Version(System.Int32)
extern "C"  void Cookie_set_Version_m1740254262 (Cookie_t3154017544 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_000f;
		}
	}
	{
		int32_t L_1 = ___value0;
		if ((((int32_t)L_1) <= ((int32_t)((int32_t)10))))
		{
			goto IL_001b;
		}
	}

IL_000f:
	{
		__this->set_version_13(0);
		goto IL_0022;
	}

IL_001b:
	{
		int32_t L_2 = ___value0;
		__this->set_version_13(L_2);
	}

IL_0022:
	{
		return;
	}
}
// System.Boolean System.Net.Cookie::Equals(System.Object)
extern Il2CppClass* Cookie_t3154017544_il2cpp_TypeInfo_var;
extern Il2CppClass* CultureInfo_t3500843524_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Cookie_Equals_m953158749_MetadataUsageId;
extern "C"  bool Cookie_Equals_m953158749 (Cookie_t3154017544 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Cookie_Equals_m953158749_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Cookie_t3154017544 * V_0 = NULL;
	int32_t G_B7_0 = 0;
	{
		Il2CppObject * L_0 = ___obj0;
		V_0 = ((Cookie_t3154017544 *)IsInstSealed(L_0, Cookie_t3154017544_il2cpp_TypeInfo_var));
		Cookie_t3154017544 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_008d;
		}
	}
	{
		String_t* L_2 = __this->get_name_6();
		Cookie_t3154017544 * L_3 = V_0;
		NullCheck(L_3);
		String_t* L_4 = L_3->get_name_6();
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3500843524_il2cpp_TypeInfo_var);
		CultureInfo_t3500843524 * L_5 = CultureInfo_get_InvariantCulture_m398972276(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		int32_t L_6 = String_Compare_m1847873744(NULL /*static, unused*/, L_2, L_4, (bool)1, L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_008d;
		}
	}
	{
		String_t* L_7 = __this->get_val_12();
		Cookie_t3154017544 * L_8 = V_0;
		NullCheck(L_8);
		String_t* L_9 = L_8->get_val_12();
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3500843524_il2cpp_TypeInfo_var);
		CultureInfo_t3500843524 * L_10 = CultureInfo_get_InvariantCulture_m398972276(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		int32_t L_11 = String_Compare_m1847873744(NULL /*static, unused*/, L_7, L_9, (bool)0, L_10, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_008d;
		}
	}
	{
		String_t* L_12 = Cookie_get_Path_m4051852505(__this, /*hidden argument*/NULL);
		Cookie_t3154017544 * L_13 = V_0;
		NullCheck(L_13);
		String_t* L_14 = Cookie_get_Path_m4051852505(L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3500843524_il2cpp_TypeInfo_var);
		CultureInfo_t3500843524 * L_15 = CultureInfo_get_InvariantCulture_m398972276(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		int32_t L_16 = String_Compare_m1847873744(NULL /*static, unused*/, L_12, L_14, (bool)0, L_15, /*hidden argument*/NULL);
		if (L_16)
		{
			goto IL_008d;
		}
	}
	{
		String_t* L_17 = __this->get_domain_3();
		Cookie_t3154017544 * L_18 = V_0;
		NullCheck(L_18);
		String_t* L_19 = L_18->get_domain_3();
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3500843524_il2cpp_TypeInfo_var);
		CultureInfo_t3500843524 * L_20 = CultureInfo_get_InvariantCulture_m398972276(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		int32_t L_21 = String_Compare_m1847873744(NULL /*static, unused*/, L_17, L_19, (bool)1, L_20, /*hidden argument*/NULL);
		if (L_21)
		{
			goto IL_008d;
		}
	}
	{
		int32_t L_22 = __this->get_version_13();
		Cookie_t3154017544 * L_23 = V_0;
		NullCheck(L_23);
		int32_t L_24 = L_23->get_version_13();
		G_B7_0 = ((((int32_t)L_22) == ((int32_t)L_24))? 1 : 0);
		goto IL_008e;
	}

IL_008d:
	{
		G_B7_0 = 0;
	}

IL_008e:
	{
		return (bool)G_B7_0;
	}
}
// System.Int32 System.Net.Cookie::GetHashCode()
extern Il2CppClass* CaseInsensitiveHashCodeProvider_t2307530285_il2cpp_TypeInfo_var;
extern Il2CppClass* Cookie_t3154017544_il2cpp_TypeInfo_var;
extern const uint32_t Cookie_GetHashCode_m2397790267_MetadataUsageId;
extern "C"  int32_t Cookie_GetHashCode_m2397790267 (Cookie_t3154017544 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Cookie_GetHashCode_m2397790267_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CaseInsensitiveHashCodeProvider_t2307530285_il2cpp_TypeInfo_var);
		CaseInsensitiveHashCodeProvider_t2307530285 * L_0 = CaseInsensitiveHashCodeProvider_get_DefaultInvariant_m1293455465(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_1 = __this->get_name_6();
		NullCheck(L_0);
		int32_t L_2 = CaseInsensitiveHashCodeProvider_GetHashCode_m3726312478(L_0, L_1, /*hidden argument*/NULL);
		String_t* L_3 = __this->get_val_12();
		NullCheck(L_3);
		int32_t L_4 = String_GetHashCode_m931956593(L_3, /*hidden argument*/NULL);
		String_t* L_5 = Cookie_get_Path_m4051852505(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		int32_t L_6 = String_GetHashCode_m931956593(L_5, /*hidden argument*/NULL);
		CaseInsensitiveHashCodeProvider_t2307530285 * L_7 = CaseInsensitiveHashCodeProvider_get_DefaultInvariant_m1293455465(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_8 = __this->get_domain_3();
		NullCheck(L_7);
		int32_t L_9 = CaseInsensitiveHashCodeProvider_GetHashCode_m3726312478(L_7, L_8, /*hidden argument*/NULL);
		int32_t L_10 = __this->get_version_13();
		IL2CPP_RUNTIME_CLASS_INIT(Cookie_t3154017544_il2cpp_TypeInfo_var);
		int32_t L_11 = Cookie_hash_m3727374953(NULL /*static, unused*/, L_2, L_4, L_6, L_9, L_10, /*hidden argument*/NULL);
		return L_11;
	}
}
// System.Int32 System.Net.Cookie::hash(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t Cookie_hash_m3727374953 (Il2CppObject * __this /* static, unused */, int32_t ___i0, int32_t ___j1, int32_t ___k2, int32_t ___l3, int32_t ___m4, const MethodInfo* method)
{
	{
		int32_t L_0 = ___i0;
		int32_t L_1 = ___j1;
		int32_t L_2 = ___j1;
		int32_t L_3 = ___k2;
		int32_t L_4 = ___k2;
		int32_t L_5 = ___l3;
		int32_t L_6 = ___l3;
		int32_t L_7 = ___m4;
		int32_t L_8 = ___m4;
		return ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_0^(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1<<(int32_t)((int32_t)13)))|(int32_t)((int32_t)((int32_t)L_2>>(int32_t)((int32_t)19)))))))^(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_3<<(int32_t)((int32_t)26)))|(int32_t)((int32_t)((int32_t)L_4>>(int32_t)6))))))^(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_5<<(int32_t)7))|(int32_t)((int32_t)((int32_t)L_6>>(int32_t)((int32_t)25)))))))^(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_7<<(int32_t)((int32_t)20)))|(int32_t)((int32_t)((int32_t)L_8>>(int32_t)((int32_t)12)))))));
	}
}
// System.String System.Net.Cookie::ToString()
extern "C"  String_t* Cookie_ToString_m2088265195 (Cookie_t3154017544 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = Cookie_ToString_m4074255426(__this, (Uri_t19570940 *)NULL, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.String System.Net.Cookie::ToString(System.Uri)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* StringBuilder_t1221177846_il2cpp_TypeInfo_var;
extern Il2CppClass* Cookie_t3154017544_il2cpp_TypeInfo_var;
extern Il2CppClass* Uri_t19570940_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4032660195;
extern Il2CppCodeGenString* _stringLiteral811305495;
extern Il2CppCodeGenString* _stringLiteral372029329;
extern Il2CppCodeGenString* _stringLiteral702535551;
extern Il2CppCodeGenString* _stringLiteral3038142920;
extern Il2CppCodeGenString* _stringLiteral488745458;
extern Il2CppCodeGenString* _stringLiteral1905740579;
extern const uint32_t Cookie_ToString_m4074255426_MetadataUsageId;
extern "C"  String_t* Cookie_ToString_m4074255426 (Cookie_t3154017544 * __this, Uri_t19570940 * ___uri0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Cookie_ToString_m4074255426_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t1221177846 * V_0 = NULL;
	bool V_1 = false;
	int32_t G_B13_0 = 0;
	{
		String_t* L_0 = __this->get_name_6();
		NullCheck(L_0);
		int32_t L_1 = String_get_Length_m1606060069(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_2;
	}

IL_0016:
	{
		StringBuilder_t1221177846 * L_3 = (StringBuilder_t1221177846 *)il2cpp_codegen_object_new(StringBuilder_t1221177846_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m536337337(L_3, ((int32_t)64), /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = __this->get_version_13();
		if ((((int32_t)L_4) <= ((int32_t)0)))
		{
			goto IL_004b;
		}
	}
	{
		StringBuilder_t1221177846 * L_5 = V_0;
		NullCheck(L_5);
		StringBuilder_t1221177846 * L_6 = StringBuilder_Append_m3636508479(L_5, _stringLiteral4032660195, /*hidden argument*/NULL);
		int32_t L_7 = __this->get_version_13();
		NullCheck(L_6);
		StringBuilder_t1221177846 * L_8 = StringBuilder_Append_m2109474214(L_6, L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		StringBuilder_Append_m3636508479(L_8, _stringLiteral811305495, /*hidden argument*/NULL);
	}

IL_004b:
	{
		StringBuilder_t1221177846 * L_9 = V_0;
		String_t* L_10 = __this->get_name_6();
		NullCheck(L_9);
		StringBuilder_t1221177846 * L_11 = StringBuilder_Append_m3636508479(L_9, L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		StringBuilder_t1221177846 * L_12 = StringBuilder_Append_m3636508479(L_11, _stringLiteral372029329, /*hidden argument*/NULL);
		String_t* L_13 = __this->get_val_12();
		NullCheck(L_12);
		StringBuilder_Append_m3636508479(L_12, L_13, /*hidden argument*/NULL);
		int32_t L_14 = __this->get_version_13();
		if (L_14)
		{
			goto IL_007f;
		}
	}
	{
		StringBuilder_t1221177846 * L_15 = V_0;
		NullCheck(L_15);
		String_t* L_16 = StringBuilder_ToString_m1507807375(L_15, /*hidden argument*/NULL);
		return L_16;
	}

IL_007f:
	{
		String_t* L_17 = __this->get_path_7();
		IL2CPP_RUNTIME_CLASS_INIT(Cookie_t3154017544_il2cpp_TypeInfo_var);
		bool L_18 = Cookie_IsNullOrEmpty_m3107130375(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		if (L_18)
		{
			goto IL_00ab;
		}
	}
	{
		StringBuilder_t1221177846 * L_19 = V_0;
		NullCheck(L_19);
		StringBuilder_t1221177846 * L_20 = StringBuilder_Append_m3636508479(L_19, _stringLiteral702535551, /*hidden argument*/NULL);
		String_t* L_21 = __this->get_path_7();
		NullCheck(L_20);
		StringBuilder_Append_m3636508479(L_20, L_21, /*hidden argument*/NULL);
		goto IL_00ce;
	}

IL_00ab:
	{
		Uri_t19570940 * L_22 = ___uri0;
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t19570940_il2cpp_TypeInfo_var);
		bool L_23 = Uri_op_Inequality_m853767938(NULL /*static, unused*/, L_22, (Uri_t19570940 *)NULL, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_00ce;
		}
	}
	{
		StringBuilder_t1221177846 * L_24 = V_0;
		NullCheck(L_24);
		StringBuilder_t1221177846 * L_25 = StringBuilder_Append_m3636508479(L_24, _stringLiteral3038142920, /*hidden argument*/NULL);
		String_t* L_26 = __this->get_path_7();
		NullCheck(L_25);
		StringBuilder_Append_m3636508479(L_25, L_26, /*hidden argument*/NULL);
	}

IL_00ce:
	{
		Uri_t19570940 * L_27 = ___uri0;
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t19570940_il2cpp_TypeInfo_var);
		bool L_28 = Uri_op_Equality_m110355127(NULL /*static, unused*/, L_27, (Uri_t19570940 *)NULL, /*hidden argument*/NULL);
		if (L_28)
		{
			goto IL_00ed;
		}
	}
	{
		Uri_t19570940 * L_29 = ___uri0;
		NullCheck(L_29);
		String_t* L_30 = Uri_get_Host_m2492204157(L_29, /*hidden argument*/NULL);
		String_t* L_31 = __this->get_domain_3();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_32 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_30, L_31, /*hidden argument*/NULL);
		G_B13_0 = ((int32_t)(L_32));
		goto IL_00ee;
	}

IL_00ed:
	{
		G_B13_0 = 1;
	}

IL_00ee:
	{
		V_1 = (bool)G_B13_0;
		bool L_33 = V_1;
		if (!L_33)
		{
			goto IL_011c;
		}
	}
	{
		String_t* L_34 = __this->get_domain_3();
		IL2CPP_RUNTIME_CLASS_INIT(Cookie_t3154017544_il2cpp_TypeInfo_var);
		bool L_35 = Cookie_IsNullOrEmpty_m3107130375(NULL /*static, unused*/, L_34, /*hidden argument*/NULL);
		if (L_35)
		{
			goto IL_011c;
		}
	}
	{
		StringBuilder_t1221177846 * L_36 = V_0;
		NullCheck(L_36);
		StringBuilder_t1221177846 * L_37 = StringBuilder_Append_m3636508479(L_36, _stringLiteral488745458, /*hidden argument*/NULL);
		String_t* L_38 = __this->get_domain_3();
		NullCheck(L_37);
		StringBuilder_Append_m3636508479(L_37, L_38, /*hidden argument*/NULL);
	}

IL_011c:
	{
		String_t* L_39 = __this->get_port_8();
		if (!L_39)
		{
			goto IL_014e;
		}
	}
	{
		String_t* L_40 = __this->get_port_8();
		NullCheck(L_40);
		int32_t L_41 = String_get_Length_m1606060069(L_40, /*hidden argument*/NULL);
		if (!L_41)
		{
			goto IL_014e;
		}
	}
	{
		StringBuilder_t1221177846 * L_42 = V_0;
		NullCheck(L_42);
		StringBuilder_t1221177846 * L_43 = StringBuilder_Append_m3636508479(L_42, _stringLiteral1905740579, /*hidden argument*/NULL);
		String_t* L_44 = __this->get_port_8();
		NullCheck(L_43);
		StringBuilder_Append_m3636508479(L_43, L_44, /*hidden argument*/NULL);
	}

IL_014e:
	{
		StringBuilder_t1221177846 * L_45 = V_0;
		NullCheck(L_45);
		String_t* L_46 = StringBuilder_ToString_m1507807375(L_45, /*hidden argument*/NULL);
		return L_46;
	}
}
// System.Boolean System.Net.Cookie::IsNullOrEmpty(System.String)
extern "C"  bool Cookie_IsNullOrEmpty_m3107130375 (Il2CppObject * __this /* static, unused */, String_t* ___s0, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		String_t* L_0 = ___s0;
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		String_t* L_1 = ___s0;
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_m1606060069(L_1, /*hidden argument*/NULL);
		G_B3_0 = ((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
		goto IL_0012;
	}

IL_0011:
	{
		G_B3_0 = 1;
	}

IL_0012:
	{
		return (bool)G_B3_0;
	}
}
// System.Void System.Net.CookieCollection::.ctor()
extern Il2CppClass* List_1_t2523138676_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m262868154_MethodInfo_var;
extern const uint32_t CookieCollection__ctor_m1766956480_MetadataUsageId;
extern "C"  void CookieCollection__ctor_m1766956480 (CookieCollection_t521422364 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CookieCollection__ctor_m1766956480_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t2523138676 * L_0 = (List_1_t2523138676 *)il2cpp_codegen_object_new(List_1_t2523138676_il2cpp_TypeInfo_var);
		List_1__ctor_m262868154(L_0, /*hidden argument*/List_1__ctor_m262868154_MethodInfo_var);
		__this->set_list_0(L_0);
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Net.CookieCollection::.cctor()
extern Il2CppClass* CookieCollectionComparer_t3570802680_il2cpp_TypeInfo_var;
extern Il2CppClass* CookieCollection_t521422364_il2cpp_TypeInfo_var;
extern const uint32_t CookieCollection__cctor_m1612480269_MetadataUsageId;
extern "C"  void CookieCollection__cctor_m1612480269 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CookieCollection__cctor_m1612480269_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		CookieCollectionComparer_t3570802680 * L_0 = (CookieCollectionComparer_t3570802680 *)il2cpp_codegen_object_new(CookieCollectionComparer_t3570802680_il2cpp_TypeInfo_var);
		CookieCollectionComparer__ctor_m2525729418(L_0, /*hidden argument*/NULL);
		((CookieCollection_t521422364_StaticFields*)CookieCollection_t521422364_il2cpp_TypeInfo_var->static_fields)->set_Comparer_1(L_0);
		return;
	}
}
// System.Collections.Generic.IList`1<System.Net.Cookie> System.Net.CookieCollection::get_List()
extern "C"  Il2CppObject* CookieCollection_get_List_m2326436043 (CookieCollection_t521422364 * __this, const MethodInfo* method)
{
	{
		List_1_t2523138676 * L_0 = __this->get_list_0();
		return L_0;
	}
}
// System.Int32 System.Net.CookieCollection::get_Count()
extern const MethodInfo* List_1_get_Count_m4280013514_MethodInfo_var;
extern const uint32_t CookieCollection_get_Count_m2224755408_MetadataUsageId;
extern "C"  int32_t CookieCollection_get_Count_m2224755408 (CookieCollection_t521422364 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CookieCollection_get_Count_m2224755408_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t2523138676 * L_0 = __this->get_list_0();
		NullCheck(L_0);
		int32_t L_1 = List_1_get_Count_m4280013514(L_0, /*hidden argument*/List_1_get_Count_m4280013514_MethodInfo_var);
		return L_1;
	}
}
// System.Boolean System.Net.CookieCollection::get_IsSynchronized()
extern "C"  bool CookieCollection_get_IsSynchronized_m3152974947 (CookieCollection_t521422364 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Net.CookieCollection::get_SyncRoot()
extern "C"  Il2CppObject * CookieCollection_get_SyncRoot_m121352699 (CookieCollection_t521422364 * __this, const MethodInfo* method)
{
	{
		return __this;
	}
}
// System.Void System.Net.CookieCollection::CopyTo(System.Array,System.Int32)
extern Il2CppClass* ICollection_t91669223_il2cpp_TypeInfo_var;
extern const uint32_t CookieCollection_CopyTo_m3467802731_MetadataUsageId;
extern "C"  void CookieCollection_CopyTo_m3467802731 (CookieCollection_t521422364 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CookieCollection_CopyTo_m3467802731_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t2523138676 * L_0 = __this->get_list_0();
		Il2CppArray * L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck(L_0);
		InterfaceActionInvoker2< Il2CppArray *, int32_t >::Invoke(3 /* System.Void System.Collections.ICollection::CopyTo(System.Array,System.Int32) */, ICollection_t91669223_il2cpp_TypeInfo_var, L_0, L_1, L_2);
		return;
	}
}
// System.Collections.IEnumerator System.Net.CookieCollection::GetEnumerator()
extern Il2CppClass* Enumerator_t2057868350_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m1117076131_MethodInfo_var;
extern const uint32_t CookieCollection_GetEnumerator_m870936748_MetadataUsageId;
extern "C"  Il2CppObject * CookieCollection_GetEnumerator_m870936748 (CookieCollection_t521422364 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CookieCollection_GetEnumerator_m870936748_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t2523138676 * L_0 = __this->get_list_0();
		NullCheck(L_0);
		Enumerator_t2057868350  L_1 = List_1_GetEnumerator_m1117076131(L_0, /*hidden argument*/List_1_GetEnumerator_m1117076131_MethodInfo_var);
		Enumerator_t2057868350  L_2 = L_1;
		Il2CppObject * L_3 = Box(Enumerator_t2057868350_il2cpp_TypeInfo_var, &L_2);
		return (Il2CppObject *)L_3;
	}
}
// System.Void System.Net.CookieCollection::Add(System.Net.Cookie)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Add_m2712306774_MethodInfo_var;
extern const MethodInfo* List_1_set_Item_m2790170672_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2167626068;
extern const uint32_t CookieCollection_Add_m1645185609_MetadataUsageId;
extern "C"  void CookieCollection_Add_m1645185609 (CookieCollection_t521422364 * __this, Cookie_t3154017544 * ___cookie0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CookieCollection_Add_m1645185609_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		Cookie_t3154017544 * L_0 = ___cookie0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, _stringLiteral2167626068, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Cookie_t3154017544 * L_2 = ___cookie0;
		int32_t L_3 = CookieCollection_SearchCookie_m3532173322(__this, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = V_0;
		if ((!(((uint32_t)L_4) == ((uint32_t)(-1)))))
		{
			goto IL_0031;
		}
	}
	{
		List_1_t2523138676 * L_5 = __this->get_list_0();
		Cookie_t3154017544 * L_6 = ___cookie0;
		NullCheck(L_5);
		List_1_Add_m2712306774(L_5, L_6, /*hidden argument*/List_1_Add_m2712306774_MethodInfo_var);
		goto IL_003e;
	}

IL_0031:
	{
		List_1_t2523138676 * L_7 = __this->get_list_0();
		int32_t L_8 = V_0;
		Cookie_t3154017544 * L_9 = ___cookie0;
		NullCheck(L_7);
		List_1_set_Item_m2790170672(L_7, L_8, L_9, /*hidden argument*/List_1_set_Item_m2790170672_MethodInfo_var);
	}

IL_003e:
	{
		return;
	}
}
// System.Void System.Net.CookieCollection::Sort()
extern Il2CppClass* CookieCollection_t521422364_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Count_m4280013514_MethodInfo_var;
extern const MethodInfo* List_1_Sort_m219529749_MethodInfo_var;
extern const uint32_t CookieCollection_Sort_m1952831886_MetadataUsageId;
extern "C"  void CookieCollection_Sort_m1952831886 (CookieCollection_t521422364 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CookieCollection_Sort_m1952831886_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t2523138676 * L_0 = __this->get_list_0();
		NullCheck(L_0);
		int32_t L_1 = List_1_get_Count_m4280013514(L_0, /*hidden argument*/List_1_get_Count_m4280013514_MethodInfo_var);
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_0021;
		}
	}
	{
		List_1_t2523138676 * L_2 = __this->get_list_0();
		IL2CPP_RUNTIME_CLASS_INIT(CookieCollection_t521422364_il2cpp_TypeInfo_var);
		CookieCollectionComparer_t3570802680 * L_3 = ((CookieCollection_t521422364_StaticFields*)CookieCollection_t521422364_il2cpp_TypeInfo_var->static_fields)->get_Comparer_1();
		NullCheck(L_2);
		List_1_Sort_m219529749(L_2, L_3, /*hidden argument*/List_1_Sort_m219529749_MethodInfo_var);
	}

IL_0021:
	{
		return;
	}
}
// System.Int32 System.Net.CookieCollection::SearchCookie(System.Net.Cookie)
extern Il2CppClass* CultureInfo_t3500843524_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Count_m4280013514_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m1665845025_MethodInfo_var;
extern const uint32_t CookieCollection_SearchCookie_m3532173322_MetadataUsageId;
extern "C"  int32_t CookieCollection_SearchCookie_m3532173322 (CookieCollection_t521422364 * __this, Cookie_t3154017544 * ___cookie0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CookieCollection_SearchCookie_m3532173322_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	String_t* V_2 = NULL;
	int32_t V_3 = 0;
	Cookie_t3154017544 * V_4 = NULL;
	{
		Cookie_t3154017544 * L_0 = ___cookie0;
		NullCheck(L_0);
		String_t* L_1 = Cookie_get_Name_m892827391(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Cookie_t3154017544 * L_2 = ___cookie0;
		NullCheck(L_2);
		String_t* L_3 = Cookie_get_Domain_m78058352(L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		Cookie_t3154017544 * L_4 = ___cookie0;
		NullCheck(L_4);
		String_t* L_5 = Cookie_get_Path_m4051852505(L_4, /*hidden argument*/NULL);
		V_2 = L_5;
		List_1_t2523138676 * L_6 = __this->get_list_0();
		NullCheck(L_6);
		int32_t L_7 = List_1_get_Count_m4280013514(L_6, /*hidden argument*/List_1_get_Count_m4280013514_MethodInfo_var);
		V_3 = ((int32_t)((int32_t)L_7-(int32_t)1));
		goto IL_00aa;
	}

IL_0028:
	{
		List_1_t2523138676 * L_8 = __this->get_list_0();
		int32_t L_9 = V_3;
		NullCheck(L_8);
		Cookie_t3154017544 * L_10 = List_1_get_Item_m1665845025(L_8, L_9, /*hidden argument*/List_1_get_Item_m1665845025_MethodInfo_var);
		V_4 = L_10;
		Cookie_t3154017544 * L_11 = V_4;
		NullCheck(L_11);
		int32_t L_12 = Cookie_get_Version_m1900649029(L_11, /*hidden argument*/NULL);
		Cookie_t3154017544 * L_13 = ___cookie0;
		NullCheck(L_13);
		int32_t L_14 = Cookie_get_Version_m1900649029(L_13, /*hidden argument*/NULL);
		if ((((int32_t)L_12) == ((int32_t)L_14)))
		{
			goto IL_004d;
		}
	}
	{
		goto IL_00a6;
	}

IL_004d:
	{
		String_t* L_15 = V_1;
		Cookie_t3154017544 * L_16 = V_4;
		NullCheck(L_16);
		String_t* L_17 = Cookie_get_Domain_m78058352(L_16, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3500843524_il2cpp_TypeInfo_var);
		CultureInfo_t3500843524 * L_18 = CultureInfo_get_InvariantCulture_m398972276(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		int32_t L_19 = String_Compare_m1847873744(NULL /*static, unused*/, L_15, L_17, (bool)1, L_18, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_006a;
		}
	}
	{
		goto IL_00a6;
	}

IL_006a:
	{
		String_t* L_20 = V_0;
		Cookie_t3154017544 * L_21 = V_4;
		NullCheck(L_21);
		String_t* L_22 = Cookie_get_Name_m892827391(L_21, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3500843524_il2cpp_TypeInfo_var);
		CultureInfo_t3500843524 * L_23 = CultureInfo_get_InvariantCulture_m398972276(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		int32_t L_24 = String_Compare_m1847873744(NULL /*static, unused*/, L_20, L_22, (bool)1, L_23, /*hidden argument*/NULL);
		if (!L_24)
		{
			goto IL_0087;
		}
	}
	{
		goto IL_00a6;
	}

IL_0087:
	{
		String_t* L_25 = V_2;
		Cookie_t3154017544 * L_26 = V_4;
		NullCheck(L_26);
		String_t* L_27 = Cookie_get_Path_m4051852505(L_26, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3500843524_il2cpp_TypeInfo_var);
		CultureInfo_t3500843524 * L_28 = CultureInfo_get_InvariantCulture_m398972276(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		int32_t L_29 = String_Compare_m1847873744(NULL /*static, unused*/, L_25, L_27, (bool)1, L_28, /*hidden argument*/NULL);
		if (!L_29)
		{
			goto IL_00a4;
		}
	}
	{
		goto IL_00a6;
	}

IL_00a4:
	{
		int32_t L_30 = V_3;
		return L_30;
	}

IL_00a6:
	{
		int32_t L_31 = V_3;
		V_3 = ((int32_t)((int32_t)L_31-(int32_t)1));
	}

IL_00aa:
	{
		int32_t L_32 = V_3;
		if ((((int32_t)L_32) >= ((int32_t)0)))
		{
			goto IL_0028;
		}
	}
	{
		return (-1);
	}
}
// System.Net.Cookie System.Net.CookieCollection::get_Item(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Count_m4280013514_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m1665845025_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t CookieCollection_get_Item_m1262022058_MetadataUsageId;
extern "C"  Cookie_t3154017544 * CookieCollection_get_Item_m1262022058 (CookieCollection_t521422364 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CookieCollection_get_Item_m1262022058_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		int32_t L_1 = ___index0;
		List_1_t2523138676 * L_2 = __this->get_list_0();
		NullCheck(L_2);
		int32_t L_3 = List_1_get_Count_m4280013514(L_2, /*hidden argument*/List_1_get_Count_m4280013514_MethodInfo_var);
		if ((((int32_t)L_1) < ((int32_t)L_3)))
		{
			goto IL_0023;
		}
	}

IL_0018:
	{
		ArgumentOutOfRangeException_t279959794 * L_4 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_4, _stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0023:
	{
		List_1_t2523138676 * L_5 = __this->get_list_0();
		int32_t L_6 = ___index0;
		NullCheck(L_5);
		Cookie_t3154017544 * L_7 = List_1_get_Item_m1665845025(L_5, L_6, /*hidden argument*/List_1_get_Item_m1665845025_MethodInfo_var);
		return L_7;
	}
}
// System.Void System.Net.CookieCollection/CookieCollectionComparer::.ctor()
extern "C"  void CookieCollectionComparer__ctor_m2525729418 (CookieCollectionComparer_t3570802680 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 System.Net.CookieCollection/CookieCollectionComparer::Compare(System.Net.Cookie,System.Net.Cookie)
extern "C"  int32_t CookieCollectionComparer_Compare_m1612231159 (CookieCollectionComparer_t3570802680 * __this, Cookie_t3154017544 * ___x0, Cookie_t3154017544 * ___y1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Cookie_t3154017544 * L_0 = ___x0;
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		Cookie_t3154017544 * L_1 = ___y1;
		if (L_1)
		{
			goto IL_000e;
		}
	}

IL_000c:
	{
		return 0;
	}

IL_000e:
	{
		Cookie_t3154017544 * L_2 = ___x0;
		NullCheck(L_2);
		String_t* L_3 = Cookie_get_Name_m892827391(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		int32_t L_4 = String_get_Length_m1606060069(L_3, /*hidden argument*/NULL);
		Cookie_t3154017544 * L_5 = ___x0;
		NullCheck(L_5);
		String_t* L_6 = Cookie_get_Value_m2421772599(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		int32_t L_7 = String_get_Length_m1606060069(L_6, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_4+(int32_t)L_7));
		Cookie_t3154017544 * L_8 = ___y1;
		NullCheck(L_8);
		String_t* L_9 = Cookie_get_Name_m892827391(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		int32_t L_10 = String_get_Length_m1606060069(L_9, /*hidden argument*/NULL);
		Cookie_t3154017544 * L_11 = ___y1;
		NullCheck(L_11);
		String_t* L_12 = Cookie_get_Value_m2421772599(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		int32_t L_13 = String_get_Length_m1606060069(L_12, /*hidden argument*/NULL);
		V_1 = ((int32_t)((int32_t)L_10+(int32_t)L_13));
		int32_t L_14 = V_0;
		int32_t L_15 = V_1;
		return ((int32_t)((int32_t)L_14-(int32_t)L_15));
	}
}
// System.Void System.Net.CookieContainer::.ctor()
extern "C"  void CookieContainer__ctor_m2232515219 (CookieContainer_t2808809223 * __this, const MethodInfo* method)
{
	{
		__this->set_capacity_0(((int32_t)300));
		__this->set_perDomainCapacity_1(((int32_t)20));
		__this->set_maxCookieSize_2(((int32_t)4096));
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Net.CookieContainer::AddCookie(System.Net.Cookie)
extern Il2CppClass* CookieCollection_t521422364_il2cpp_TypeInfo_var;
extern Il2CppClass* Cookie_t3154017544_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029315;
extern const uint32_t CookieContainer_AddCookie_m2988233116_MetadataUsageId;
extern "C"  void CookieContainer_AddCookie_m2988233116 (CookieContainer_t2808809223 * __this, Cookie_t3154017544 * ___cookie0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CookieContainer_AddCookie_m2988233116_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Cookie_t3154017544 * V_0 = NULL;
	Cookie_t3154017544 * G_B9_0 = NULL;
	Cookie_t3154017544 * G_B8_0 = NULL;
	String_t* G_B10_0 = NULL;
	Cookie_t3154017544 * G_B10_1 = NULL;
	{
		CookieCollection_t521422364 * L_0 = __this->get_cookies_3();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		CookieCollection_t521422364 * L_1 = (CookieCollection_t521422364 *)il2cpp_codegen_object_new(CookieCollection_t521422364_il2cpp_TypeInfo_var);
		CookieCollection__ctor_m1766956480(L_1, /*hidden argument*/NULL);
		__this->set_cookies_3(L_1);
	}

IL_0016:
	{
		CookieCollection_t521422364 * L_2 = __this->get_cookies_3();
		NullCheck(L_2);
		int32_t L_3 = CookieCollection_get_Count_m2224755408(L_2, /*hidden argument*/NULL);
		int32_t L_4 = __this->get_capacity_0();
		if ((((int32_t)L_3) < ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		CookieContainer_RemoveOldest_m2278733834(__this, (String_t*)NULL, /*hidden argument*/NULL);
	}

IL_0033:
	{
		CookieCollection_t521422364 * L_5 = __this->get_cookies_3();
		NullCheck(L_5);
		int32_t L_6 = CookieCollection_get_Count_m2224755408(L_5, /*hidden argument*/NULL);
		int32_t L_7 = __this->get_perDomainCapacity_1();
		if ((((int32_t)L_6) < ((int32_t)L_7)))
		{
			goto IL_006c;
		}
	}
	{
		Cookie_t3154017544 * L_8 = ___cookie0;
		NullCheck(L_8);
		String_t* L_9 = Cookie_get_Domain_m78058352(L_8, /*hidden argument*/NULL);
		int32_t L_10 = CookieContainer_CountDomain_m1088970898(__this, L_9, /*hidden argument*/NULL);
		int32_t L_11 = __this->get_perDomainCapacity_1();
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_006c;
		}
	}
	{
		Cookie_t3154017544 * L_12 = ___cookie0;
		NullCheck(L_12);
		String_t* L_13 = Cookie_get_Domain_m78058352(L_12, /*hidden argument*/NULL);
		CookieContainer_RemoveOldest_m2278733834(__this, L_13, /*hidden argument*/NULL);
	}

IL_006c:
	{
		Cookie_t3154017544 * L_14 = ___cookie0;
		NullCheck(L_14);
		String_t* L_15 = Cookie_get_Name_m892827391(L_14, /*hidden argument*/NULL);
		Cookie_t3154017544 * L_16 = ___cookie0;
		NullCheck(L_16);
		String_t* L_17 = Cookie_get_Value_m2421772599(L_16, /*hidden argument*/NULL);
		Cookie_t3154017544 * L_18 = (Cookie_t3154017544 *)il2cpp_codegen_object_new(Cookie_t3154017544_il2cpp_TypeInfo_var);
		Cookie__ctor_m1989068482(L_18, L_15, L_17, /*hidden argument*/NULL);
		V_0 = L_18;
		Cookie_t3154017544 * L_19 = V_0;
		Cookie_t3154017544 * L_20 = ___cookie0;
		NullCheck(L_20);
		String_t* L_21 = Cookie_get_Path_m4051852505(L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		int32_t L_22 = String_get_Length_m1606060069(L_21, /*hidden argument*/NULL);
		G_B8_0 = L_19;
		if (L_22)
		{
			G_B9_0 = L_19;
			goto IL_0099;
		}
	}
	{
		G_B10_0 = _stringLiteral372029315;
		G_B10_1 = G_B8_0;
		goto IL_009f;
	}

IL_0099:
	{
		Cookie_t3154017544 * L_23 = ___cookie0;
		NullCheck(L_23);
		String_t* L_24 = Cookie_get_Path_m4051852505(L_23, /*hidden argument*/NULL);
		G_B10_0 = L_24;
		G_B10_1 = G_B9_0;
	}

IL_009f:
	{
		NullCheck(G_B10_1);
		Cookie_set_Path_m3432117582(G_B10_1, G_B10_0, /*hidden argument*/NULL);
		Cookie_t3154017544 * L_25 = V_0;
		Cookie_t3154017544 * L_26 = ___cookie0;
		NullCheck(L_26);
		String_t* L_27 = Cookie_get_Domain_m78058352(L_26, /*hidden argument*/NULL);
		NullCheck(L_25);
		Cookie_set_Domain_m334320991(L_25, L_27, /*hidden argument*/NULL);
		Cookie_t3154017544 * L_28 = V_0;
		Cookie_t3154017544 * L_29 = ___cookie0;
		NullCheck(L_29);
		bool L_30 = Cookie_get_ExactDomain_m3704355968(L_29, /*hidden argument*/NULL);
		NullCheck(L_28);
		Cookie_set_ExactDomain_m1681540717(L_28, L_30, /*hidden argument*/NULL);
		Cookie_t3154017544 * L_31 = V_0;
		Cookie_t3154017544 * L_32 = ___cookie0;
		NullCheck(L_32);
		int32_t L_33 = Cookie_get_Version_m1900649029(L_32, /*hidden argument*/NULL);
		NullCheck(L_31);
		Cookie_set_Version_m1740254262(L_31, L_33, /*hidden argument*/NULL);
		Cookie_t3154017544 * L_34 = V_0;
		Cookie_t3154017544 * L_35 = ___cookie0;
		NullCheck(L_35);
		DateTime_t693205669  L_36 = Cookie_get_Expires_m522897886(L_35, /*hidden argument*/NULL);
		NullCheck(L_34);
		Cookie_set_Expires_m1840037255(L_34, L_36, /*hidden argument*/NULL);
		Cookie_t3154017544 * L_37 = V_0;
		Cookie_t3154017544 * L_38 = ___cookie0;
		NullCheck(L_38);
		Uri_t19570940 * L_39 = Cookie_get_CommentUri_m4105081148(L_38, /*hidden argument*/NULL);
		NullCheck(L_37);
		Cookie_set_CommentUri_m2880099413(L_37, L_39, /*hidden argument*/NULL);
		Cookie_t3154017544 * L_40 = V_0;
		Cookie_t3154017544 * L_41 = ___cookie0;
		NullCheck(L_41);
		String_t* L_42 = Cookie_get_Comment_m2183197327(L_41, /*hidden argument*/NULL);
		NullCheck(L_40);
		Cookie_set_Comment_m1504498068(L_40, L_42, /*hidden argument*/NULL);
		Cookie_t3154017544 * L_43 = V_0;
		Cookie_t3154017544 * L_44 = ___cookie0;
		NullCheck(L_44);
		bool L_45 = Cookie_get_Discard_m4277637175(L_44, /*hidden argument*/NULL);
		NullCheck(L_43);
		Cookie_set_Discard_m8095924(L_43, L_45, /*hidden argument*/NULL);
		Cookie_t3154017544 * L_46 = V_0;
		Cookie_t3154017544 * L_47 = ___cookie0;
		NullCheck(L_47);
		bool L_48 = Cookie_get_HttpOnly_m252147641(L_47, /*hidden argument*/NULL);
		NullCheck(L_46);
		Cookie_set_HttpOnly_m2601679756(L_46, L_48, /*hidden argument*/NULL);
		Cookie_t3154017544 * L_49 = V_0;
		Cookie_t3154017544 * L_50 = ___cookie0;
		NullCheck(L_50);
		bool L_51 = Cookie_get_Secure_m3357936990(L_50, /*hidden argument*/NULL);
		NullCheck(L_49);
		Cookie_set_Secure_m2609176681(L_49, L_51, /*hidden argument*/NULL);
		CookieCollection_t521422364 * L_52 = __this->get_cookies_3();
		Cookie_t3154017544 * L_53 = V_0;
		NullCheck(L_52);
		CookieCollection_Add_m1645185609(L_52, L_53, /*hidden argument*/NULL);
		CookieContainer_CheckExpiration_m1872209538(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 System.Net.CookieContainer::CountDomain(System.String)
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* Cookie_t3154017544_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t CookieContainer_CountDomain_m1088970898_MetadataUsageId;
extern "C"  int32_t CookieContainer_CountDomain_m1088970898 (CookieContainer_t2808809223 * __this, String_t* ___domain0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CookieContainer_CountDomain_m1088970898_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Cookie_t3154017544 * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = 0;
		CookieCollection_t521422364 * L_0 = __this->get_cookies_3();
		NullCheck(L_0);
		Il2CppObject * L_1 = CookieCollection_GetEnumerator_m870936748(L_0, /*hidden argument*/NULL);
		V_2 = L_1;
	}

IL_000e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0035;
		}

IL_0013:
		{
			Il2CppObject * L_2 = V_2;
			NullCheck(L_2);
			Il2CppObject * L_3 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_2);
			V_1 = ((Cookie_t3154017544 *)CastclassSealed(L_3, Cookie_t3154017544_il2cpp_TypeInfo_var));
			String_t* L_4 = ___domain0;
			Cookie_t3154017544 * L_5 = V_1;
			NullCheck(L_5);
			String_t* L_6 = Cookie_get_Domain_m78058352(L_5, /*hidden argument*/NULL);
			bool L_7 = CookieContainer_CheckDomain_m3129371454(NULL /*static, unused*/, L_4, L_6, (bool)1, /*hidden argument*/NULL);
			if (!L_7)
			{
				goto IL_0035;
			}
		}

IL_0031:
		{
			int32_t L_8 = V_0;
			V_0 = ((int32_t)((int32_t)L_8+(int32_t)1));
		}

IL_0035:
		{
			Il2CppObject * L_9 = V_2;
			NullCheck(L_9);
			bool L_10 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_9);
			if (L_10)
			{
				goto IL_0013;
			}
		}

IL_0040:
		{
			IL2CPP_LEAVE(0x57, FINALLY_0045);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0045;
	}

FINALLY_0045:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_11 = V_2;
			V_3 = ((Il2CppObject *)IsInst(L_11, IDisposable_t2427283555_il2cpp_TypeInfo_var));
			Il2CppObject * L_12 = V_3;
			if (L_12)
			{
				goto IL_0050;
			}
		}

IL_004f:
		{
			IL2CPP_END_FINALLY(69)
		}

IL_0050:
		{
			Il2CppObject * L_13 = V_3;
			NullCheck(L_13);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_13);
			IL2CPP_END_FINALLY(69)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(69)
	{
		IL2CPP_JUMP_TBL(0x57, IL_0057)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0057:
	{
		int32_t L_14 = V_0;
		return L_14;
	}
}
// System.Void System.Net.CookieContainer::RemoveOldest(System.String)
extern Il2CppClass* DateTime_t693205669_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* IList_1_t3694958145_il2cpp_TypeInfo_var;
extern const uint32_t CookieContainer_RemoveOldest_m2278733834_MetadataUsageId;
extern "C"  void CookieContainer_RemoveOldest_m2278733834 (CookieContainer_t2808809223 * __this, String_t* ___domain0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CookieContainer_RemoveOldest_m2278733834_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	DateTime_t693205669  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t V_2 = 0;
	Cookie_t3154017544 * V_3 = NULL;
	{
		V_0 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t693205669_il2cpp_TypeInfo_var);
		DateTime_t693205669  L_0 = ((DateTime_t693205669_StaticFields*)DateTime_t693205669_il2cpp_TypeInfo_var->static_fields)->get_MaxValue_12();
		V_1 = L_0;
		V_2 = 0;
		goto IL_0051;
	}

IL_000f:
	{
		CookieCollection_t521422364 * L_1 = __this->get_cookies_3();
		int32_t L_2 = V_2;
		NullCheck(L_1);
		Cookie_t3154017544 * L_3 = CookieCollection_get_Item_m1262022058(L_1, L_2, /*hidden argument*/NULL);
		V_3 = L_3;
		Cookie_t3154017544 * L_4 = V_3;
		NullCheck(L_4);
		DateTime_t693205669  L_5 = Cookie_get_TimeStamp_m2398884362(L_4, /*hidden argument*/NULL);
		DateTime_t693205669  L_6 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t693205669_il2cpp_TypeInfo_var);
		bool L_7 = DateTime_op_LessThan_m3944619870(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		String_t* L_8 = ___domain0;
		if (!L_8)
		{
			goto IL_0044;
		}
	}
	{
		String_t* L_9 = ___domain0;
		Cookie_t3154017544 * L_10 = V_3;
		NullCheck(L_10);
		String_t* L_11 = Cookie_get_Domain_m78058352(L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_12 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_9, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_004d;
		}
	}

IL_0044:
	{
		Cookie_t3154017544 * L_13 = V_3;
		NullCheck(L_13);
		DateTime_t693205669  L_14 = Cookie_get_TimeStamp_m2398884362(L_13, /*hidden argument*/NULL);
		V_1 = L_14;
		int32_t L_15 = V_2;
		V_0 = L_15;
	}

IL_004d:
	{
		int32_t L_16 = V_2;
		V_2 = ((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0051:
	{
		int32_t L_17 = V_2;
		CookieCollection_t521422364 * L_18 = __this->get_cookies_3();
		NullCheck(L_18);
		int32_t L_19 = CookieCollection_get_Count_m2224755408(L_18, /*hidden argument*/NULL);
		if ((((int32_t)L_17) < ((int32_t)L_19)))
		{
			goto IL_000f;
		}
	}
	{
		CookieCollection_t521422364 * L_20 = __this->get_cookies_3();
		NullCheck(L_20);
		Il2CppObject* L_21 = CookieCollection_get_List_m2326436043(L_20, /*hidden argument*/NULL);
		int32_t L_22 = V_0;
		NullCheck(L_21);
		InterfaceActionInvoker1< int32_t >::Invoke(2 /* System.Void System.Collections.Generic.IList`1<System.Net.Cookie>::RemoveAt(System.Int32) */, IList_1_t3694958145_il2cpp_TypeInfo_var, L_21, L_22);
		return;
	}
}
// System.Void System.Net.CookieContainer::CheckExpiration()
extern Il2CppClass* IList_1_t3694958145_il2cpp_TypeInfo_var;
extern const uint32_t CookieContainer_CheckExpiration_m1872209538_MetadataUsageId;
extern "C"  void CookieContainer_CheckExpiration_m1872209538 (CookieContainer_t2808809223 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CookieContainer_CheckExpiration_m1872209538_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Cookie_t3154017544 * V_1 = NULL;
	{
		CookieCollection_t521422364 * L_0 = __this->get_cookies_3();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		CookieCollection_t521422364 * L_1 = __this->get_cookies_3();
		NullCheck(L_1);
		int32_t L_2 = CookieCollection_get_Count_m2224755408(L_1, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_2-(int32_t)1));
		goto IL_004c;
	}

IL_001f:
	{
		CookieCollection_t521422364 * L_3 = __this->get_cookies_3();
		int32_t L_4 = V_0;
		NullCheck(L_3);
		Cookie_t3154017544 * L_5 = CookieCollection_get_Item_m1262022058(L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		Cookie_t3154017544 * L_6 = V_1;
		NullCheck(L_6);
		bool L_7 = Cookie_get_Expired_m1519158270(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0048;
		}
	}
	{
		CookieCollection_t521422364 * L_8 = __this->get_cookies_3();
		NullCheck(L_8);
		Il2CppObject* L_9 = CookieCollection_get_List_m2326436043(L_8, /*hidden argument*/NULL);
		int32_t L_10 = V_0;
		NullCheck(L_9);
		InterfaceActionInvoker1< int32_t >::Invoke(2 /* System.Void System.Collections.Generic.IList`1<System.Net.Cookie>::RemoveAt(System.Int32) */, IList_1_t3694958145_il2cpp_TypeInfo_var, L_9, L_10);
	}

IL_0048:
	{
		int32_t L_11 = V_0;
		V_0 = ((int32_t)((int32_t)L_11-(int32_t)1));
	}

IL_004c:
	{
		int32_t L_12 = V_0;
		if ((((int32_t)L_12) >= ((int32_t)0)))
		{
			goto IL_001f;
		}
	}
	{
		return;
	}
}
// System.Void System.Net.CookieContainer::Cook(System.Uri,System.Net.Cookie)
extern Il2CppClass* CookieException_t1505724635_il2cpp_TypeInfo_var;
extern Il2CppClass* Uri_t19570940_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1418267372;
extern Il2CppCodeGenString* _stringLiteral2423334592;
extern Il2CppCodeGenString* _stringLiteral372029315;
extern Il2CppCodeGenString* _stringLiteral372029312;
extern const uint32_t CookieContainer_Cook_m4063584936_MetadataUsageId;
extern "C"  void CookieContainer_Cook_m4063584936 (CookieContainer_t2808809223 * __this, Uri_t19570940 * ___uri0, Cookie_t3154017544 * ___cookie1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CookieContainer_Cook_m4063584936_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		Cookie_t3154017544 * L_0 = ___cookie1;
		NullCheck(L_0);
		String_t* L_1 = Cookie_get_Name_m892827391(L_0, /*hidden argument*/NULL);
		bool L_2 = CookieContainer_IsNullOrEmpty_m212241700(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001b;
		}
	}
	{
		CookieException_t1505724635 * L_3 = (CookieException_t1505724635 *)il2cpp_codegen_object_new(CookieException_t1505724635_il2cpp_TypeInfo_var);
		CookieException__ctor_m39051905(L_3, _stringLiteral1418267372, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001b:
	{
		Cookie_t3154017544 * L_4 = ___cookie1;
		NullCheck(L_4);
		String_t* L_5 = Cookie_get_Value_m2421772599(L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0031;
		}
	}
	{
		CookieException_t1505724635 * L_6 = (CookieException_t1505724635 *)il2cpp_codegen_object_new(CookieException_t1505724635_il2cpp_TypeInfo_var);
		CookieException__ctor_m39051905(L_6, _stringLiteral2423334592, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_0031:
	{
		Uri_t19570940 * L_7 = ___uri0;
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t19570940_il2cpp_TypeInfo_var);
		bool L_8 = Uri_op_Inequality_m853767938(NULL /*static, unused*/, L_7, (Uri_t19570940 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0059;
		}
	}
	{
		Cookie_t3154017544 * L_9 = ___cookie1;
		NullCheck(L_9);
		String_t* L_10 = Cookie_get_Domain_m78058352(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		int32_t L_11 = String_get_Length_m1606060069(L_10, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_0059;
		}
	}
	{
		Cookie_t3154017544 * L_12 = ___cookie1;
		Uri_t19570940 * L_13 = ___uri0;
		NullCheck(L_13);
		String_t* L_14 = Uri_get_Host_m2492204157(L_13, /*hidden argument*/NULL);
		NullCheck(L_12);
		Cookie_set_Domain_m334320991(L_12, L_14, /*hidden argument*/NULL);
	}

IL_0059:
	{
		Cookie_t3154017544 * L_15 = ___cookie1;
		NullCheck(L_15);
		int32_t L_16 = Cookie_get_Version_m1900649029(L_15, /*hidden argument*/NULL);
		if (L_16)
		{
			goto IL_009c;
		}
	}
	{
		Cookie_t3154017544 * L_17 = ___cookie1;
		NullCheck(L_17);
		String_t* L_18 = Cookie_get_Path_m4051852505(L_17, /*hidden argument*/NULL);
		bool L_19 = CookieContainer_IsNullOrEmpty_m212241700(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_009c;
		}
	}
	{
		Uri_t19570940 * L_20 = ___uri0;
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t19570940_il2cpp_TypeInfo_var);
		bool L_21 = Uri_op_Inequality_m853767938(NULL /*static, unused*/, L_20, (Uri_t19570940 *)NULL, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_0091;
		}
	}
	{
		Cookie_t3154017544 * L_22 = ___cookie1;
		Uri_t19570940 * L_23 = ___uri0;
		NullCheck(L_23);
		String_t* L_24 = Uri_get_AbsolutePath_m802771013(L_23, /*hidden argument*/NULL);
		NullCheck(L_22);
		Cookie_set_Path_m3432117582(L_22, L_24, /*hidden argument*/NULL);
		goto IL_009c;
	}

IL_0091:
	{
		Cookie_t3154017544 * L_25 = ___cookie1;
		NullCheck(L_25);
		Cookie_set_Path_m3432117582(L_25, _stringLiteral372029315, /*hidden argument*/NULL);
	}

IL_009c:
	{
		Cookie_t3154017544 * L_26 = ___cookie1;
		NullCheck(L_26);
		String_t* L_27 = Cookie_get_Port_m3259286681(L_26, /*hidden argument*/NULL);
		NullCheck(L_27);
		int32_t L_28 = String_get_Length_m1606060069(L_27, /*hidden argument*/NULL);
		if (L_28)
		{
			goto IL_00e6;
		}
	}
	{
		Uri_t19570940 * L_29 = ___uri0;
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t19570940_il2cpp_TypeInfo_var);
		bool L_30 = Uri_op_Inequality_m853767938(NULL /*static, unused*/, L_29, (Uri_t19570940 *)NULL, /*hidden argument*/NULL);
		if (!L_30)
		{
			goto IL_00e6;
		}
	}
	{
		Uri_t19570940 * L_31 = ___uri0;
		NullCheck(L_31);
		bool L_32 = Uri_get_IsDefaultPort_m3069892608(L_31, /*hidden argument*/NULL);
		if (L_32)
		{
			goto IL_00e6;
		}
	}
	{
		Cookie_t3154017544 * L_33 = ___cookie1;
		Uri_t19570940 * L_34 = ___uri0;
		NullCheck(L_34);
		int32_t L_35 = Uri_get_Port_m834512465(L_34, /*hidden argument*/NULL);
		V_0 = L_35;
		String_t* L_36 = Int32_ToString_m2960866144((&V_0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_37 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral372029312, L_36, _stringLiteral372029312, /*hidden argument*/NULL);
		NullCheck(L_33);
		Cookie_set_Port_m312020554(L_33, L_37, /*hidden argument*/NULL);
	}

IL_00e6:
	{
		return;
	}
}
// System.Void System.Net.CookieContainer::Add(System.Uri,System.Net.Cookie)
extern Il2CppClass* Uri_t19570940_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral386853516;
extern Il2CppCodeGenString* _stringLiteral2167626068;
extern const uint32_t CookieContainer_Add_m1442357673_MetadataUsageId;
extern "C"  void CookieContainer_Add_m1442357673 (CookieContainer_t2808809223 * __this, Uri_t19570940 * ___uri0, Cookie_t3154017544 * ___cookie1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CookieContainer_Add_m1442357673_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Uri_t19570940 * L_0 = ___uri0;
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t19570940_il2cpp_TypeInfo_var);
		bool L_1 = Uri_op_Equality_m110355127(NULL /*static, unused*/, L_0, (Uri_t19570940 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t628810857 * L_2 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_2, _stringLiteral386853516, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		Cookie_t3154017544 * L_3 = ___cookie1;
		if (L_3)
		{
			goto IL_0028;
		}
	}
	{
		ArgumentNullException_t628810857 * L_4 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_4, _stringLiteral2167626068, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0028:
	{
		Cookie_t3154017544 * L_5 = ___cookie1;
		NullCheck(L_5);
		bool L_6 = Cookie_get_Expired_m1519158270(L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0042;
		}
	}
	{
		Uri_t19570940 * L_7 = ___uri0;
		Cookie_t3154017544 * L_8 = ___cookie1;
		CookieContainer_Cook_m4063584936(__this, L_7, L_8, /*hidden argument*/NULL);
		Cookie_t3154017544 * L_9 = ___cookie1;
		CookieContainer_AddCookie_m2988233116(__this, L_9, /*hidden argument*/NULL);
	}

IL_0042:
	{
		return;
	}
}
// System.String System.Net.CookieContainer::GetCookieHeader(System.Uri)
extern Il2CppClass* Uri_t19570940_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* StringBuilder_t1221177846_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* Cookie_t3154017544_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral386853516;
extern Il2CppCodeGenString* _stringLiteral811305495;
extern const uint32_t CookieContainer_GetCookieHeader_m3789169220_MetadataUsageId;
extern "C"  String_t* CookieContainer_GetCookieHeader_m3789169220 (CookieContainer_t2808809223 * __this, Uri_t19570940 * ___uri0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CookieContainer_GetCookieHeader_m3789169220_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CookieCollection_t521422364 * V_0 = NULL;
	StringBuilder_t1221177846 * V_1 = NULL;
	Cookie_t3154017544 * V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	Il2CppObject * V_4 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Uri_t19570940 * L_0 = ___uri0;
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t19570940_il2cpp_TypeInfo_var);
		bool L_1 = Uri_op_Equality_m110355127(NULL /*static, unused*/, L_0, (Uri_t19570940 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t628810857 * L_2 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_2, _stringLiteral386853516, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		Uri_t19570940 * L_3 = ___uri0;
		CookieCollection_t521422364 * L_4 = CookieContainer_GetCookies_m781592810(__this, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		CookieCollection_t521422364 * L_5 = V_0;
		NullCheck(L_5);
		int32_t L_6 = CookieCollection_get_Count_m2224755408(L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0030;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_7;
	}

IL_0030:
	{
		StringBuilder_t1221177846 * L_8 = (StringBuilder_t1221177846 *)il2cpp_codegen_object_new(StringBuilder_t1221177846_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m3946851802(L_8, /*hidden argument*/NULL);
		V_1 = L_8;
		CookieCollection_t521422364 * L_9 = V_0;
		NullCheck(L_9);
		Il2CppObject * L_10 = CookieCollection_GetEnumerator_m870936748(L_9, /*hidden argument*/NULL);
		V_3 = L_10;
	}

IL_003d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0068;
		}

IL_0042:
		{
			Il2CppObject * L_11 = V_3;
			NullCheck(L_11);
			Il2CppObject * L_12 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_11);
			V_2 = ((Cookie_t3154017544 *)CastclassSealed(L_12, Cookie_t3154017544_il2cpp_TypeInfo_var));
			StringBuilder_t1221177846 * L_13 = V_1;
			Cookie_t3154017544 * L_14 = V_2;
			Uri_t19570940 * L_15 = ___uri0;
			NullCheck(L_14);
			String_t* L_16 = Cookie_ToString_m4074255426(L_14, L_15, /*hidden argument*/NULL);
			NullCheck(L_13);
			StringBuilder_Append_m3636508479(L_13, L_16, /*hidden argument*/NULL);
			StringBuilder_t1221177846 * L_17 = V_1;
			NullCheck(L_17);
			StringBuilder_Append_m3636508479(L_17, _stringLiteral811305495, /*hidden argument*/NULL);
		}

IL_0068:
		{
			Il2CppObject * L_18 = V_3;
			NullCheck(L_18);
			bool L_19 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_18);
			if (L_19)
			{
				goto IL_0042;
			}
		}

IL_0073:
		{
			IL2CPP_LEAVE(0x8D, FINALLY_0078);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0078;
	}

FINALLY_0078:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_20 = V_3;
			V_4 = ((Il2CppObject *)IsInst(L_20, IDisposable_t2427283555_il2cpp_TypeInfo_var));
			Il2CppObject * L_21 = V_4;
			if (L_21)
			{
				goto IL_0085;
			}
		}

IL_0084:
		{
			IL2CPP_END_FINALLY(120)
		}

IL_0085:
		{
			Il2CppObject * L_22 = V_4;
			NullCheck(L_22);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_22);
			IL2CPP_END_FINALLY(120)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(120)
	{
		IL2CPP_JUMP_TBL(0x8D, IL_008d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_008d:
	{
		StringBuilder_t1221177846 * L_23 = V_1;
		NullCheck(L_23);
		int32_t L_24 = StringBuilder_get_Length_m1608241323(L_23, /*hidden argument*/NULL);
		if ((((int32_t)L_24) <= ((int32_t)0)))
		{
			goto IL_00a7;
		}
	}
	{
		StringBuilder_t1221177846 * L_25 = V_1;
		StringBuilder_t1221177846 * L_26 = L_25;
		NullCheck(L_26);
		int32_t L_27 = StringBuilder_get_Length_m1608241323(L_26, /*hidden argument*/NULL);
		NullCheck(L_26);
		StringBuilder_set_Length_m3039225444(L_26, ((int32_t)((int32_t)L_27-(int32_t)2)), /*hidden argument*/NULL);
	}

IL_00a7:
	{
		StringBuilder_t1221177846 * L_28 = V_1;
		NullCheck(L_28);
		String_t* L_29 = StringBuilder_ToString_m1507807375(L_28, /*hidden argument*/NULL);
		return L_29;
	}
}
// System.Boolean System.Net.CookieContainer::CheckDomain(System.String,System.String,System.Boolean)
extern Il2CppClass* CultureInfo_t3500843524_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t CookieContainer_CheckDomain_m3129371454_MetadataUsageId;
extern "C"  bool CookieContainer_CheckDomain_m3129371454 (Il2CppObject * __this /* static, unused */, String_t* ___domain0, String_t* ___host1, bool ___exact2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CookieContainer_CheckDomain_m3129371454_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		String_t* L_0 = ___domain0;
		NullCheck(L_0);
		int32_t L_1 = String_get_Length_m1606060069(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		bool L_2 = ___exact2;
		if (!L_2)
		{
			goto IL_0024;
		}
	}
	{
		String_t* L_3 = ___host1;
		String_t* L_4 = ___domain0;
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3500843524_il2cpp_TypeInfo_var);
		CultureInfo_t3500843524 * L_5 = CultureInfo_get_InvariantCulture_m398972276(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		int32_t L_6 = String_Compare_m1847873744(NULL /*static, unused*/, L_3, L_4, (bool)1, L_5, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_6) == ((int32_t)0))? 1 : 0);
	}

IL_0024:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3500843524_il2cpp_TypeInfo_var);
		CultureInfo_t3500843524 * L_7 = CultureInfo_get_InvariantCulture_m398972276(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_7);
		CompareInfo_t2310920157 * L_8 = VirtFuncInvoker0< CompareInfo_t2310920157 * >::Invoke(11 /* System.Globalization.CompareInfo System.Globalization.CultureInfo::get_CompareInfo() */, L_7);
		String_t* L_9 = ___host1;
		String_t* L_10 = ___domain0;
		NullCheck(L_8);
		bool L_11 = VirtFuncInvoker3< bool, String_t*, String_t*, int32_t >::Invoke(14 /* System.Boolean System.Globalization.CompareInfo::IsSuffix(System.String,System.String,System.Globalization.CompareOptions) */, L_8, L_9, L_10, 1);
		if (L_11)
		{
			goto IL_003d;
		}
	}
	{
		return (bool)0;
	}

IL_003d:
	{
		String_t* L_12 = ___domain0;
		NullCheck(L_12);
		Il2CppChar L_13 = String_get_Chars_m4230566705(L_12, 0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_13) == ((uint32_t)((int32_t)46)))))
		{
			goto IL_004d;
		}
	}
	{
		return (bool)1;
	}

IL_004d:
	{
		String_t* L_14 = ___host1;
		NullCheck(L_14);
		int32_t L_15 = String_get_Length_m1606060069(L_14, /*hidden argument*/NULL);
		String_t* L_16 = ___domain0;
		NullCheck(L_16);
		int32_t L_17 = String_get_Length_m1606060069(L_16, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_15-(int32_t)L_17))-(int32_t)1));
		int32_t L_18 = V_0;
		if ((((int32_t)L_18) >= ((int32_t)0)))
		{
			goto IL_0066;
		}
	}
	{
		return (bool)0;
	}

IL_0066:
	{
		String_t* L_19 = ___host1;
		int32_t L_20 = V_0;
		NullCheck(L_19);
		Il2CppChar L_21 = String_get_Chars_m4230566705(L_19, L_20, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_21) == ((int32_t)((int32_t)46)))? 1 : 0);
	}
}
// System.Net.CookieCollection System.Net.CookieContainer::GetCookies(System.Uri)
extern Il2CppClass* Uri_t19570940_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppClass* CookieCollection_t521422364_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* Cookie_t3154017544_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const MethodInfo* Array_IndexOf_TisInt32_t2071877448_m1686802248_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral386853516;
extern Il2CppCodeGenString* _stringLiteral372029315;
extern Il2CppCodeGenString* _stringLiteral4021508911;
extern const uint32_t CookieContainer_GetCookies_m781592810_MetadataUsageId;
extern "C"  CookieCollection_t521422364 * CookieContainer_GetCookies_m781592810 (CookieContainer_t2808809223 * __this, Uri_t19570940 * ___uri0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CookieContainer_GetCookies_m781592810_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CookieCollection_t521422364 * V_0 = NULL;
	Cookie_t3154017544 * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	String_t* V_3 = NULL;
	String_t* V_4 = NULL;
	String_t* V_5 = NULL;
	Il2CppObject * V_6 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Uri_t19570940 * L_0 = ___uri0;
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t19570940_il2cpp_TypeInfo_var);
		bool L_1 = Uri_op_Equality_m110355127(NULL /*static, unused*/, L_0, (Uri_t19570940 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t628810857 * L_2 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_2, _stringLiteral386853516, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		CookieContainer_CheckExpiration_m1872209538(__this, /*hidden argument*/NULL);
		CookieCollection_t521422364 * L_3 = (CookieCollection_t521422364 *)il2cpp_codegen_object_new(CookieCollection_t521422364_il2cpp_TypeInfo_var);
		CookieCollection__ctor_m1766956480(L_3, /*hidden argument*/NULL);
		V_0 = L_3;
		CookieCollection_t521422364 * L_4 = __this->get_cookies_3();
		if (L_4)
		{
			goto IL_0030;
		}
	}
	{
		CookieCollection_t521422364 * L_5 = V_0;
		return L_5;
	}

IL_0030:
	{
		CookieCollection_t521422364 * L_6 = __this->get_cookies_3();
		NullCheck(L_6);
		Il2CppObject * L_7 = CookieCollection_GetEnumerator_m870936748(L_6, /*hidden argument*/NULL);
		V_2 = L_7;
	}

IL_003c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0177;
		}

IL_0041:
		{
			Il2CppObject * L_8 = V_2;
			NullCheck(L_8);
			Il2CppObject * L_9 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_8);
			V_1 = ((Cookie_t3154017544 *)CastclassSealed(L_9, Cookie_t3154017544_il2cpp_TypeInfo_var));
			Cookie_t3154017544 * L_10 = V_1;
			NullCheck(L_10);
			String_t* L_11 = Cookie_get_Domain_m78058352(L_10, /*hidden argument*/NULL);
			V_3 = L_11;
			String_t* L_12 = V_3;
			Uri_t19570940 * L_13 = ___uri0;
			NullCheck(L_13);
			String_t* L_14 = Uri_get_Host_m2492204157(L_13, /*hidden argument*/NULL);
			Cookie_t3154017544 * L_15 = V_1;
			NullCheck(L_15);
			bool L_16 = Cookie_get_ExactDomain_m3704355968(L_15, /*hidden argument*/NULL);
			bool L_17 = CookieContainer_CheckDomain_m3129371454(NULL /*static, unused*/, L_12, L_14, L_16, /*hidden argument*/NULL);
			if (L_17)
			{
				goto IL_0070;
			}
		}

IL_006b:
		{
			goto IL_0177;
		}

IL_0070:
		{
			Cookie_t3154017544 * L_18 = V_1;
			NullCheck(L_18);
			String_t* L_19 = Cookie_get_Port_m3259286681(L_18, /*hidden argument*/NULL);
			NullCheck(L_19);
			int32_t L_20 = String_get_Length_m1606060069(L_19, /*hidden argument*/NULL);
			if ((((int32_t)L_20) <= ((int32_t)0)))
			{
				goto IL_00b4;
			}
		}

IL_0081:
		{
			Cookie_t3154017544 * L_21 = V_1;
			NullCheck(L_21);
			Int32U5BU5D_t3030399641* L_22 = Cookie_get_Ports_m3110993987(L_21, /*hidden argument*/NULL);
			if (!L_22)
			{
				goto IL_00b4;
			}
		}

IL_008c:
		{
			Uri_t19570940 * L_23 = ___uri0;
			NullCheck(L_23);
			int32_t L_24 = Uri_get_Port_m834512465(L_23, /*hidden argument*/NULL);
			if ((((int32_t)L_24) == ((int32_t)(-1))))
			{
				goto IL_00b4;
			}
		}

IL_0098:
		{
			Cookie_t3154017544 * L_25 = V_1;
			NullCheck(L_25);
			Int32U5BU5D_t3030399641* L_26 = Cookie_get_Ports_m3110993987(L_25, /*hidden argument*/NULL);
			Uri_t19570940 * L_27 = ___uri0;
			NullCheck(L_27);
			int32_t L_28 = Uri_get_Port_m834512465(L_27, /*hidden argument*/NULL);
			int32_t L_29 = Array_IndexOf_TisInt32_t2071877448_m1686802248(NULL /*static, unused*/, L_26, L_28, /*hidden argument*/Array_IndexOf_TisInt32_t2071877448_m1686802248_MethodInfo_var);
			if ((!(((uint32_t)L_29) == ((uint32_t)(-1)))))
			{
				goto IL_00b4;
			}
		}

IL_00af:
		{
			goto IL_0177;
		}

IL_00b4:
		{
			Cookie_t3154017544 * L_30 = V_1;
			NullCheck(L_30);
			String_t* L_31 = Cookie_get_Path_m4051852505(L_30, /*hidden argument*/NULL);
			V_4 = L_31;
			Uri_t19570940 * L_32 = ___uri0;
			NullCheck(L_32);
			String_t* L_33 = Uri_get_AbsolutePath_m802771013(L_32, /*hidden argument*/NULL);
			V_5 = L_33;
			String_t* L_34 = V_4;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_35 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
			bool L_36 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_34, L_35, /*hidden argument*/NULL);
			if (!L_36)
			{
				goto IL_014b;
			}
		}

IL_00d5:
		{
			String_t* L_37 = V_4;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_38 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_37, _stringLiteral372029315, /*hidden argument*/NULL);
			if (!L_38)
			{
				goto IL_014b;
			}
		}

IL_00e6:
		{
			String_t* L_39 = V_5;
			String_t* L_40 = V_4;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_41 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_39, L_40, /*hidden argument*/NULL);
			if (!L_41)
			{
				goto IL_014b;
			}
		}

IL_00f4:
		{
			String_t* L_42 = V_5;
			String_t* L_43 = V_4;
			NullCheck(L_42);
			bool L_44 = String_StartsWith_m1841920685(L_42, L_43, /*hidden argument*/NULL);
			if (L_44)
			{
				goto IL_0107;
			}
		}

IL_0102:
		{
			goto IL_0177;
		}

IL_0107:
		{
			String_t* L_45 = V_4;
			String_t* L_46 = V_4;
			NullCheck(L_46);
			int32_t L_47 = String_get_Length_m1606060069(L_46, /*hidden argument*/NULL);
			NullCheck(L_45);
			Il2CppChar L_48 = String_get_Chars_m4230566705(L_45, ((int32_t)((int32_t)L_47-(int32_t)1)), /*hidden argument*/NULL);
			if ((((int32_t)L_48) == ((int32_t)((int32_t)47))))
			{
				goto IL_014b;
			}
		}

IL_011e:
		{
			String_t* L_49 = V_5;
			NullCheck(L_49);
			int32_t L_50 = String_get_Length_m1606060069(L_49, /*hidden argument*/NULL);
			String_t* L_51 = V_4;
			NullCheck(L_51);
			int32_t L_52 = String_get_Length_m1606060069(L_51, /*hidden argument*/NULL);
			if ((((int32_t)L_50) <= ((int32_t)L_52)))
			{
				goto IL_014b;
			}
		}

IL_0131:
		{
			String_t* L_53 = V_5;
			String_t* L_54 = V_4;
			NullCheck(L_54);
			int32_t L_55 = String_get_Length_m1606060069(L_54, /*hidden argument*/NULL);
			NullCheck(L_53);
			Il2CppChar L_56 = String_get_Chars_m4230566705(L_53, L_55, /*hidden argument*/NULL);
			if ((((int32_t)L_56) == ((int32_t)((int32_t)47))))
			{
				goto IL_014b;
			}
		}

IL_0146:
		{
			goto IL_0177;
		}

IL_014b:
		{
			Cookie_t3154017544 * L_57 = V_1;
			NullCheck(L_57);
			bool L_58 = Cookie_get_Secure_m3357936990(L_57, /*hidden argument*/NULL);
			if (!L_58)
			{
				goto IL_0170;
			}
		}

IL_0156:
		{
			Uri_t19570940 * L_59 = ___uri0;
			NullCheck(L_59);
			String_t* L_60 = Uri_get_Scheme_m55908894(L_59, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_61 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_60, _stringLiteral4021508911, /*hidden argument*/NULL);
			if (!L_61)
			{
				goto IL_0170;
			}
		}

IL_016b:
		{
			goto IL_0177;
		}

IL_0170:
		{
			CookieCollection_t521422364 * L_62 = V_0;
			Cookie_t3154017544 * L_63 = V_1;
			NullCheck(L_62);
			CookieCollection_Add_m1645185609(L_62, L_63, /*hidden argument*/NULL);
		}

IL_0177:
		{
			Il2CppObject * L_64 = V_2;
			NullCheck(L_64);
			bool L_65 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_64);
			if (L_65)
			{
				goto IL_0041;
			}
		}

IL_0182:
		{
			IL2CPP_LEAVE(0x19C, FINALLY_0187);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0187;
	}

FINALLY_0187:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_66 = V_2;
			V_6 = ((Il2CppObject *)IsInst(L_66, IDisposable_t2427283555_il2cpp_TypeInfo_var));
			Il2CppObject * L_67 = V_6;
			if (L_67)
			{
				goto IL_0194;
			}
		}

IL_0193:
		{
			IL2CPP_END_FINALLY(391)
		}

IL_0194:
		{
			Il2CppObject * L_68 = V_6;
			NullCheck(L_68);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_68);
			IL2CPP_END_FINALLY(391)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(391)
	{
		IL2CPP_JUMP_TBL(0x19C, IL_019c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_019c:
	{
		CookieCollection_t521422364 * L_69 = V_0;
		NullCheck(L_69);
		CookieCollection_Sort_m1952831886(L_69, /*hidden argument*/NULL);
		CookieCollection_t521422364 * L_70 = V_0;
		return L_70;
	}
}
// System.Boolean System.Net.CookieContainer::IsNullOrEmpty(System.String)
extern "C"  bool CookieContainer_IsNullOrEmpty_m212241700 (Il2CppObject * __this /* static, unused */, String_t* ___s0, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		String_t* L_0 = ___s0;
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		String_t* L_1 = ___s0;
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_m1606060069(L_1, /*hidden argument*/NULL);
		G_B3_0 = ((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
		goto IL_0012;
	}

IL_0011:
	{
		G_B3_0 = 1;
	}

IL_0012:
	{
		return (bool)G_B3_0;
	}
}
// System.Void System.Net.CookieException::.ctor()
extern "C"  void CookieException__ctor_m2298456999 (CookieException_t1505724635 * __this, const MethodInfo* method)
{
	{
		FormatException__ctor_m3521145315(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Net.CookieException::.ctor(System.String)
extern "C"  void CookieException__ctor_m39051905 (CookieException_t1505724635 * __this, String_t* ___msg0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___msg0;
		FormatException__ctor_m1466217969(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Net.CookieException::.ctor(System.String,System.Exception)
extern "C"  void CookieException__ctor_m2392418273 (CookieException_t1505724635 * __this, String_t* ___msg0, Exception_t1927440687 * ___e1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___msg0;
		Exception_t1927440687 * L_1 = ___e1;
		FormatException__ctor_m333678921(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Net.CookieException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void CookieException__ctor_m987412654 (CookieException_t1505724635 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method)
{
	{
		SerializationInfo_t228987430 * L_0 = ___info0;
		StreamingContext_t1417235061  L_1 = ___context1;
		FormatException__ctor_m3740644286(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Net.CookieException::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void CookieException_System_Runtime_Serialization_ISerializable_GetObjectData_m4084541976 (CookieException_t1505724635 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method)
{
	{
		SerializationInfo_t228987430 * L_0 = ___info0;
		StreamingContext_t1417235061  L_1 = ___context1;
		Exception_GetObjectData_m2653827630(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Net.CookieException::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void CookieException_GetObjectData_m3892299871 (CookieException_t1505724635 * __this, SerializationInfo_t228987430 * ___serializationInfo0, StreamingContext_t1417235061  ___streamingContext1, const MethodInfo* method)
{
	{
		SerializationInfo_t228987430 * L_0 = ___serializationInfo0;
		StreamingContext_t1417235061  L_1 = ___streamingContext1;
		Exception_GetObjectData_m2653827630(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
