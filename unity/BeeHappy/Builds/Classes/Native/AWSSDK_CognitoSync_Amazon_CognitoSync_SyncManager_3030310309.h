﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// Amazon.CognitoSync.SyncManager.Record
struct Record_t868799569;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.CognitoSync.SyncManager.SyncConflict
struct  SyncConflict_t3030310309  : public Il2CppObject
{
public:
	// System.String Amazon.CognitoSync.SyncManager.SyncConflict::_key
	String_t* ____key_0;
	// Amazon.CognitoSync.SyncManager.Record Amazon.CognitoSync.SyncManager.SyncConflict::_remoteRecord
	Record_t868799569 * ____remoteRecord_1;
	// Amazon.CognitoSync.SyncManager.Record Amazon.CognitoSync.SyncManager.SyncConflict::_localRecord
	Record_t868799569 * ____localRecord_2;

public:
	inline static int32_t get_offset_of__key_0() { return static_cast<int32_t>(offsetof(SyncConflict_t3030310309, ____key_0)); }
	inline String_t* get__key_0() const { return ____key_0; }
	inline String_t** get_address_of__key_0() { return &____key_0; }
	inline void set__key_0(String_t* value)
	{
		____key_0 = value;
		Il2CppCodeGenWriteBarrier(&____key_0, value);
	}

	inline static int32_t get_offset_of__remoteRecord_1() { return static_cast<int32_t>(offsetof(SyncConflict_t3030310309, ____remoteRecord_1)); }
	inline Record_t868799569 * get__remoteRecord_1() const { return ____remoteRecord_1; }
	inline Record_t868799569 ** get_address_of__remoteRecord_1() { return &____remoteRecord_1; }
	inline void set__remoteRecord_1(Record_t868799569 * value)
	{
		____remoteRecord_1 = value;
		Il2CppCodeGenWriteBarrier(&____remoteRecord_1, value);
	}

	inline static int32_t get_offset_of__localRecord_2() { return static_cast<int32_t>(offsetof(SyncConflict_t3030310309, ____localRecord_2)); }
	inline Record_t868799569 * get__localRecord_2() const { return ____localRecord_2; }
	inline Record_t868799569 ** get_address_of__localRecord_2() { return &____localRecord_2; }
	inline void set__localRecord_2(Record_t868799569 * value)
	{
		____localRecord_2 = value;
		Il2CppCodeGenWriteBarrier(&____localRecord_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
