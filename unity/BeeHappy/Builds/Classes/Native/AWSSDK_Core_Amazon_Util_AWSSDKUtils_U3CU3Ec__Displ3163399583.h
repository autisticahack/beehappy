﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Util.AWSSDKUtils/<>c__DisplayClass37_1`1<System.Object>
struct  U3CU3Ec__DisplayClass37_1_1_t3163399583  : public Il2CppObject
{
public:
	// System.Object Amazon.Util.AWSSDKUtils/<>c__DisplayClass37_1`1::sender
	Il2CppObject * ___sender_0;
	// T Amazon.Util.AWSSDKUtils/<>c__DisplayClass37_1`1::args
	Il2CppObject * ___args_1;

public:
	inline static int32_t get_offset_of_sender_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass37_1_1_t3163399583, ___sender_0)); }
	inline Il2CppObject * get_sender_0() const { return ___sender_0; }
	inline Il2CppObject ** get_address_of_sender_0() { return &___sender_0; }
	inline void set_sender_0(Il2CppObject * value)
	{
		___sender_0 = value;
		Il2CppCodeGenWriteBarrier(&___sender_0, value);
	}

	inline static int32_t get_offset_of_args_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass37_1_1_t3163399583, ___args_1)); }
	inline Il2CppObject * get_args_1() const { return ___args_1; }
	inline Il2CppObject ** get_address_of_args_1() { return &___args_1; }
	inline void set_args_1(Il2CppObject * value)
	{
		___args_1 = value;
		Il2CppCodeGenWriteBarrier(&___args_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
