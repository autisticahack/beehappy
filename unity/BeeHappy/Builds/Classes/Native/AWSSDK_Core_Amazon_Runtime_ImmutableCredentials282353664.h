﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.ImmutableCredentials
struct  ImmutableCredentials_t282353664  : public Il2CppObject
{
public:
	// System.String Amazon.Runtime.ImmutableCredentials::<AccessKey>k__BackingField
	String_t* ___U3CAccessKeyU3Ek__BackingField_0;
	// System.String Amazon.Runtime.ImmutableCredentials::<SecretKey>k__BackingField
	String_t* ___U3CSecretKeyU3Ek__BackingField_1;
	// System.String Amazon.Runtime.ImmutableCredentials::<Token>k__BackingField
	String_t* ___U3CTokenU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CAccessKeyU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ImmutableCredentials_t282353664, ___U3CAccessKeyU3Ek__BackingField_0)); }
	inline String_t* get_U3CAccessKeyU3Ek__BackingField_0() const { return ___U3CAccessKeyU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CAccessKeyU3Ek__BackingField_0() { return &___U3CAccessKeyU3Ek__BackingField_0; }
	inline void set_U3CAccessKeyU3Ek__BackingField_0(String_t* value)
	{
		___U3CAccessKeyU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CAccessKeyU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CSecretKeyU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ImmutableCredentials_t282353664, ___U3CSecretKeyU3Ek__BackingField_1)); }
	inline String_t* get_U3CSecretKeyU3Ek__BackingField_1() const { return ___U3CSecretKeyU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CSecretKeyU3Ek__BackingField_1() { return &___U3CSecretKeyU3Ek__BackingField_1; }
	inline void set_U3CSecretKeyU3Ek__BackingField_1(String_t* value)
	{
		___U3CSecretKeyU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CSecretKeyU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CTokenU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ImmutableCredentials_t282353664, ___U3CTokenU3Ek__BackingField_2)); }
	inline String_t* get_U3CTokenU3Ek__BackingField_2() const { return ___U3CTokenU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CTokenU3Ek__BackingField_2() { return &___U3CTokenU3Ek__BackingField_2; }
	inline void set_U3CTokenU3Ek__BackingField_2(String_t* value)
	{
		___U3CTokenU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CTokenU3Ek__BackingField_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
