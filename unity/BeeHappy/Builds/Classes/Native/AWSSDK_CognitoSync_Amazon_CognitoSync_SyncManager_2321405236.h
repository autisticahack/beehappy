﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.Record>
struct List_1_t237920701;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.CognitoSync.SyncManager.DatasetUpdates
struct  DatasetUpdates_t2321405236  : public Il2CppObject
{
public:
	// System.String Amazon.CognitoSync.SyncManager.DatasetUpdates::_datasetName
	String_t* ____datasetName_0;
	// System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.Record> Amazon.CognitoSync.SyncManager.DatasetUpdates::_records
	List_1_t237920701 * ____records_1;
	// System.Int64 Amazon.CognitoSync.SyncManager.DatasetUpdates::_syncCount
	int64_t ____syncCount_2;
	// System.String Amazon.CognitoSync.SyncManager.DatasetUpdates::_syncSessionToken
	String_t* ____syncSessionToken_3;
	// System.Boolean Amazon.CognitoSync.SyncManager.DatasetUpdates::_exists
	bool ____exists_4;
	// System.Boolean Amazon.CognitoSync.SyncManager.DatasetUpdates::_deleted
	bool ____deleted_5;
	// System.Collections.Generic.List`1<System.String> Amazon.CognitoSync.SyncManager.DatasetUpdates::_mergedDatasetNameList
	List_1_t1398341365 * ____mergedDatasetNameList_6;

public:
	inline static int32_t get_offset_of__datasetName_0() { return static_cast<int32_t>(offsetof(DatasetUpdates_t2321405236, ____datasetName_0)); }
	inline String_t* get__datasetName_0() const { return ____datasetName_0; }
	inline String_t** get_address_of__datasetName_0() { return &____datasetName_0; }
	inline void set__datasetName_0(String_t* value)
	{
		____datasetName_0 = value;
		Il2CppCodeGenWriteBarrier(&____datasetName_0, value);
	}

	inline static int32_t get_offset_of__records_1() { return static_cast<int32_t>(offsetof(DatasetUpdates_t2321405236, ____records_1)); }
	inline List_1_t237920701 * get__records_1() const { return ____records_1; }
	inline List_1_t237920701 ** get_address_of__records_1() { return &____records_1; }
	inline void set__records_1(List_1_t237920701 * value)
	{
		____records_1 = value;
		Il2CppCodeGenWriteBarrier(&____records_1, value);
	}

	inline static int32_t get_offset_of__syncCount_2() { return static_cast<int32_t>(offsetof(DatasetUpdates_t2321405236, ____syncCount_2)); }
	inline int64_t get__syncCount_2() const { return ____syncCount_2; }
	inline int64_t* get_address_of__syncCount_2() { return &____syncCount_2; }
	inline void set__syncCount_2(int64_t value)
	{
		____syncCount_2 = value;
	}

	inline static int32_t get_offset_of__syncSessionToken_3() { return static_cast<int32_t>(offsetof(DatasetUpdates_t2321405236, ____syncSessionToken_3)); }
	inline String_t* get__syncSessionToken_3() const { return ____syncSessionToken_3; }
	inline String_t** get_address_of__syncSessionToken_3() { return &____syncSessionToken_3; }
	inline void set__syncSessionToken_3(String_t* value)
	{
		____syncSessionToken_3 = value;
		Il2CppCodeGenWriteBarrier(&____syncSessionToken_3, value);
	}

	inline static int32_t get_offset_of__exists_4() { return static_cast<int32_t>(offsetof(DatasetUpdates_t2321405236, ____exists_4)); }
	inline bool get__exists_4() const { return ____exists_4; }
	inline bool* get_address_of__exists_4() { return &____exists_4; }
	inline void set__exists_4(bool value)
	{
		____exists_4 = value;
	}

	inline static int32_t get_offset_of__deleted_5() { return static_cast<int32_t>(offsetof(DatasetUpdates_t2321405236, ____deleted_5)); }
	inline bool get__deleted_5() const { return ____deleted_5; }
	inline bool* get_address_of__deleted_5() { return &____deleted_5; }
	inline void set__deleted_5(bool value)
	{
		____deleted_5 = value;
	}

	inline static int32_t get_offset_of__mergedDatasetNameList_6() { return static_cast<int32_t>(offsetof(DatasetUpdates_t2321405236, ____mergedDatasetNameList_6)); }
	inline List_1_t1398341365 * get__mergedDatasetNameList_6() const { return ____mergedDatasetNameList_6; }
	inline List_1_t1398341365 ** get_address_of__mergedDatasetNameList_6() { return &____mergedDatasetNameList_6; }
	inline void set__mergedDatasetNameList_6(List_1_t1398341365 * value)
	{
		____mergedDatasetNameList_6 = value;
		Il2CppCodeGenWriteBarrier(&____mergedDatasetNameList_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
