﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.Linq.XContainer/<Elements>c__Iterator1E
struct U3CElementsU3Ec__Iterator1E_t2952571908;
// System.Xml.Linq.XElement
struct XElement_t553821050;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Xml.Linq.XElement>
struct IEnumerator_1_t2324312173;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Xml.Linq.XContainer/<Elements>c__Iterator1E::.ctor()
extern "C"  void U3CElementsU3Ec__Iterator1E__ctor_m3406126187 (U3CElementsU3Ec__Iterator1E_t2952571908 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Linq.XElement System.Xml.Linq.XContainer/<Elements>c__Iterator1E::System.Collections.Generic.IEnumerator<System.Xml.Linq.XElement>.get_Current()
extern "C"  XElement_t553821050 * U3CElementsU3Ec__Iterator1E_System_Collections_Generic_IEnumeratorU3CSystem_Xml_Linq_XElementU3E_get_Current_m2073608262 (U3CElementsU3Ec__Iterator1E_t2952571908 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Xml.Linq.XContainer/<Elements>c__Iterator1E::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CElementsU3Ec__Iterator1E_System_Collections_IEnumerator_get_Current_m349738649 (U3CElementsU3Ec__Iterator1E_t2952571908 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Xml.Linq.XContainer/<Elements>c__Iterator1E::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CElementsU3Ec__Iterator1E_System_Collections_IEnumerable_GetEnumerator_m2673083068 (U3CElementsU3Ec__Iterator1E_t2952571908 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.Xml.Linq.XElement> System.Xml.Linq.XContainer/<Elements>c__Iterator1E::System.Collections.Generic.IEnumerable<System.Xml.Linq.XElement>.GetEnumerator()
extern "C"  Il2CppObject* U3CElementsU3Ec__Iterator1E_System_Collections_Generic_IEnumerableU3CSystem_Xml_Linq_XElementU3E_GetEnumerator_m3635517331 (U3CElementsU3Ec__Iterator1E_t2952571908 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.Linq.XContainer/<Elements>c__Iterator1E::MoveNext()
extern "C"  bool U3CElementsU3Ec__Iterator1E_MoveNext_m4130196865 (U3CElementsU3Ec__Iterator1E_t2952571908 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Linq.XContainer/<Elements>c__Iterator1E::Dispose()
extern "C"  void U3CElementsU3Ec__Iterator1E_Dispose_m493034168 (U3CElementsU3Ec__Iterator1E_t2952571908 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Linq.XContainer/<Elements>c__Iterator1E::Reset()
extern "C"  void U3CElementsU3Ec__Iterator1E_Reset_m833408002 (U3CElementsU3Ec__Iterator1E_t2952571908 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
