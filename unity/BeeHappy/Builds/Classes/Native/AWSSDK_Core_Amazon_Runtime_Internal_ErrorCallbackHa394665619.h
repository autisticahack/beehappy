﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`2<Amazon.Runtime.IExecutionContext,System.Exception>
struct Action_2_t3657272536;

#include "AWSSDK_Core_Amazon_Runtime_Internal_PipelineHandle1486422324.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.ErrorCallbackHandler
struct  ErrorCallbackHandler_t394665619  : public PipelineHandler_t1486422324
{
public:
	// System.Action`2<Amazon.Runtime.IExecutionContext,System.Exception> Amazon.Runtime.Internal.ErrorCallbackHandler::<OnError>k__BackingField
	Action_2_t3657272536 * ___U3COnErrorU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3COnErrorU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ErrorCallbackHandler_t394665619, ___U3COnErrorU3Ek__BackingField_3)); }
	inline Action_2_t3657272536 * get_U3COnErrorU3Ek__BackingField_3() const { return ___U3COnErrorU3Ek__BackingField_3; }
	inline Action_2_t3657272536 ** get_address_of_U3COnErrorU3Ek__BackingField_3() { return &___U3COnErrorU3Ek__BackingField_3; }
	inline void set_U3COnErrorU3Ek__BackingField_3(Action_2_t3657272536 * value)
	{
		___U3COnErrorU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3COnErrorU3Ek__BackingField_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
