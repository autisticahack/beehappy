﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.Runtime.AmazonServiceCallback`2<Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest,Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityResponse>
struct AmazonServiceCallback_2_t142122009;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.SecurityToken.AmazonSecurityTokenServiceClient/<>c__DisplayClass16_0
struct  U3CU3Ec__DisplayClass16_0_t2700714712  : public Il2CppObject
{
public:
	// Amazon.Runtime.AmazonServiceCallback`2<Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest,Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityResponse> Amazon.SecurityToken.AmazonSecurityTokenServiceClient/<>c__DisplayClass16_0::callback
	AmazonServiceCallback_2_t142122009 * ___callback_0;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass16_0_t2700714712, ___callback_0)); }
	inline AmazonServiceCallback_2_t142122009 * get_callback_0() const { return ___callback_0; }
	inline AmazonServiceCallback_2_t142122009 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(AmazonServiceCallback_2_t142122009 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier(&___callback_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
