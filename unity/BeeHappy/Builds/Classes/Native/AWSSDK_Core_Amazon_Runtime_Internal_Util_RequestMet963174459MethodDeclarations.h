﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.Util.RequestMetrics/<>c
struct U3CU3Ec_t963174459;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_Runtime_Metric3273440202.h"

// System.Void Amazon.Runtime.Internal.Util.RequestMetrics/<>c::.cctor()
extern "C"  void U3CU3Ec__cctor_m3680934809 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.RequestMetrics/<>c::.ctor()
extern "C"  void U3CU3Ec__ctor_m2022341178 (U3CU3Ec_t963174459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Runtime.Internal.Util.RequestMetrics/<>c::<GetErrors>b__33_0(Amazon.Runtime.Metric)
extern "C"  String_t* U3CU3Ec_U3CGetErrorsU3Eb__33_0_m262061299 (U3CU3Ec_t963174459 * __this, int32_t ___k0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
