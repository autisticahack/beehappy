﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.Util.InternalLogger
struct InternalLogger_t2972373343;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type1303803226.h"

// System.Type Amazon.Runtime.Internal.Util.InternalLogger::get_DeclaringType()
extern "C"  Type_t * InternalLogger_get_DeclaringType_m3916897789 (InternalLogger_t2972373343 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.InternalLogger::set_DeclaringType(System.Type)
extern "C"  void InternalLogger_set_DeclaringType_m3648033422 (InternalLogger_t2972373343 * __this, Type_t * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.Util.InternalLogger::get_IsEnabled()
extern "C"  bool InternalLogger_get_IsEnabled_m433216287 (InternalLogger_t2972373343 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.InternalLogger::set_IsEnabled(System.Boolean)
extern "C"  void InternalLogger_set_IsEnabled_m3041899632 (InternalLogger_t2972373343 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.InternalLogger::.ctor(System.Type)
extern "C"  void InternalLogger__ctor_m94232968 (InternalLogger_t2972373343 * __this, Type_t * ___declaringType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.Util.InternalLogger::get_IsErrorEnabled()
extern "C"  bool InternalLogger_get_IsErrorEnabled_m279168931 (InternalLogger_t2972373343 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.Util.InternalLogger::get_IsDebugEnabled()
extern "C"  bool InternalLogger_get_IsDebugEnabled_m2503413896 (InternalLogger_t2972373343 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.Util.InternalLogger::get_IsInfoEnabled()
extern "C"  bool InternalLogger_get_IsInfoEnabled_m832281509 (InternalLogger_t2972373343 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
