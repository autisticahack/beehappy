﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.UnityWebRequestFactory
struct UnityWebRequestFactory_t752669660;
// Amazon.Runtime.IHttpRequest`1<System.String>
struct IHttpRequest_1_t2649998629;
// System.Uri
struct Uri_t19570940;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Uri19570940.h"

// Amazon.Runtime.IHttpRequest`1<System.String> Amazon.Runtime.Internal.UnityWebRequestFactory::CreateHttpRequest(System.Uri)
extern "C"  Il2CppObject* UnityWebRequestFactory_CreateHttpRequest_m3063793492 (UnityWebRequestFactory_t752669660 * __this, Uri_t19570940 * ___requestUri0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.UnityWebRequestFactory::Dispose()
extern "C"  void UnityWebRequestFactory_Dispose_m2787597825 (UnityWebRequestFactory_t752669660 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.UnityWebRequestFactory::.ctor()
extern "C"  void UnityWebRequestFactory__ctor_m2220179152 (UnityWebRequestFactory_t752669660 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
