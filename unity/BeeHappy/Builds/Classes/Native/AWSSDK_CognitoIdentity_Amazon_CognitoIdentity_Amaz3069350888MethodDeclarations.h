﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.CognitoIdentity.AmazonCognitoIdentityClient
struct AmazonCognitoIdentityClient_t3069350888;
// Amazon.Runtime.AWSCredentials
struct AWSCredentials_t3583921007;
// Amazon.RegionEndpoint
struct RegionEndpoint_t661522805;
// Amazon.CognitoIdentity.AmazonCognitoIdentityConfig
struct AmazonCognitoIdentityConfig_t1124767059;
// Amazon.Runtime.Internal.Auth.AbstractAWSSigner
struct AbstractAWSSigner_t2114314031;
// Amazon.CognitoIdentity.Model.GetCredentialsForIdentityResponse
struct GetCredentialsForIdentityResponse_t2302678766;
// Amazon.CognitoIdentity.Model.GetCredentialsForIdentityRequest
struct GetCredentialsForIdentityRequest_t932830458;
// Amazon.CognitoIdentity.Model.GetIdResponse
struct GetIdResponse_t2091118072;
// Amazon.CognitoIdentity.Model.GetIdRequest
struct GetIdRequest_t4078561340;
// Amazon.CognitoIdentity.Model.GetOpenIdTokenResponse
struct GetOpenIdTokenResponse_t955597635;
// Amazon.CognitoIdentity.Model.GetOpenIdTokenRequest
struct GetOpenIdTokenRequest_t2698079901;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_Runtime_AWSCredentials3583921007.h"
#include "AWSSDK_Core_Amazon_RegionEndpoint661522805.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Amaz1124767059.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Model932830458.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Mode4078561340.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Mode2698079901.h"

// System.Void Amazon.CognitoIdentity.AmazonCognitoIdentityClient::.ctor(Amazon.Runtime.AWSCredentials,Amazon.RegionEndpoint)
extern "C"  void AmazonCognitoIdentityClient__ctor_m3549263274 (AmazonCognitoIdentityClient_t3069350888 * __this, AWSCredentials_t3583921007 * ___credentials0, RegionEndpoint_t661522805 * ___region1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoIdentity.AmazonCognitoIdentityClient::.ctor(Amazon.Runtime.AWSCredentials,Amazon.CognitoIdentity.AmazonCognitoIdentityConfig)
extern "C"  void AmazonCognitoIdentityClient__ctor_m567738961 (AmazonCognitoIdentityClient_t3069350888 * __this, AWSCredentials_t3583921007 * ___credentials0, AmazonCognitoIdentityConfig_t1124767059 * ___clientConfig1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.Internal.Auth.AbstractAWSSigner Amazon.CognitoIdentity.AmazonCognitoIdentityClient::CreateSigner()
extern "C"  AbstractAWSSigner_t2114314031 * AmazonCognitoIdentityClient_CreateSigner_m3979572499 (AmazonCognitoIdentityClient_t3069350888 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoIdentity.AmazonCognitoIdentityClient::Dispose(System.Boolean)
extern "C"  void AmazonCognitoIdentityClient_Dispose_m3910889566 (AmazonCognitoIdentityClient_t3069350888 * __this, bool ___disposing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.CognitoIdentity.Model.GetCredentialsForIdentityResponse Amazon.CognitoIdentity.AmazonCognitoIdentityClient::GetCredentialsForIdentity(Amazon.CognitoIdentity.Model.GetCredentialsForIdentityRequest)
extern "C"  GetCredentialsForIdentityResponse_t2302678766 * AmazonCognitoIdentityClient_GetCredentialsForIdentity_m4012515634 (AmazonCognitoIdentityClient_t3069350888 * __this, GetCredentialsForIdentityRequest_t932830458 * ___request0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.CognitoIdentity.Model.GetIdResponse Amazon.CognitoIdentity.AmazonCognitoIdentityClient::GetId(Amazon.CognitoIdentity.Model.GetIdRequest)
extern "C"  GetIdResponse_t2091118072 * AmazonCognitoIdentityClient_GetId_m2702301968 (AmazonCognitoIdentityClient_t3069350888 * __this, GetIdRequest_t4078561340 * ___request0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.CognitoIdentity.Model.GetOpenIdTokenResponse Amazon.CognitoIdentity.AmazonCognitoIdentityClient::GetOpenIdToken(Amazon.CognitoIdentity.Model.GetOpenIdTokenRequest)
extern "C"  GetOpenIdTokenResponse_t955597635 * AmazonCognitoIdentityClient_GetOpenIdToken_m1574299933 (AmazonCognitoIdentityClient_t3069350888 * __this, GetOpenIdTokenRequest_t2698079901 * ___request0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
