﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.AsyncExecutionContext
struct AsyncExecutionContext_t3009477291;
// Amazon.Runtime.IAsyncResponseContext
struct IAsyncResponseContext_t340181993;
// Amazon.Runtime.IAsyncRequestContext
struct IAsyncRequestContext_t1452803323;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// Amazon.Runtime.IAsyncResponseContext Amazon.Runtime.Internal.AsyncExecutionContext::get_ResponseContext()
extern "C"  Il2CppObject * AsyncExecutionContext_get_ResponseContext_m2911732200 (AsyncExecutionContext_t3009477291 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.AsyncExecutionContext::set_ResponseContext(Amazon.Runtime.IAsyncResponseContext)
extern "C"  void AsyncExecutionContext_set_ResponseContext_m1036449239 (AsyncExecutionContext_t3009477291 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.IAsyncRequestContext Amazon.Runtime.Internal.AsyncExecutionContext::get_RequestContext()
extern "C"  Il2CppObject * AsyncExecutionContext_get_RequestContext_m290028254 (AsyncExecutionContext_t3009477291 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.AsyncExecutionContext::set_RequestContext(Amazon.Runtime.IAsyncRequestContext)
extern "C"  void AsyncExecutionContext_set_RequestContext_m2521319931 (AsyncExecutionContext_t3009477291 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Amazon.Runtime.Internal.AsyncExecutionContext::get_RuntimeState()
extern "C"  Il2CppObject * AsyncExecutionContext_get_RuntimeState_m361220764 (AsyncExecutionContext_t3009477291 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.AsyncExecutionContext::set_RuntimeState(System.Object)
extern "C"  void AsyncExecutionContext_set_RuntimeState_m1640313663 (AsyncExecutionContext_t3009477291 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.AsyncExecutionContext::.ctor(Amazon.Runtime.IAsyncRequestContext,Amazon.Runtime.IAsyncResponseContext)
extern "C"  void AsyncExecutionContext__ctor_m1810308361 (AsyncExecutionContext_t3009477291 * __this, Il2CppObject * ___requestContext0, Il2CppObject * ___responseContext1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
