﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Transactions.SinglePhaseEnlistment
struct SinglePhaseEnlistment_t4207375558;
// System.Transactions.Transaction
struct Transaction_t869361102;
// System.Transactions.ISinglePhaseNotification
struct ISinglePhaseNotification_t242502729;

#include "codegen/il2cpp-codegen.h"
#include "System_Transactions_System_Transactions_Transaction869361102.h"

// System.Void System.Transactions.SinglePhaseEnlistment::.ctor(System.Transactions.Transaction,System.Transactions.ISinglePhaseNotification)
extern "C"  void SinglePhaseEnlistment__ctor_m2623225126 (SinglePhaseEnlistment_t4207375558 * __this, Transaction_t869361102 * ___tx0, Il2CppObject * ___enlisted1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
