﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.Runtime.Internal.Util.ThreadPoolThrottler`1<Amazon.Runtime.IAsyncExecutionContext>
struct ThreadPoolThrottler_1_t736579114;
// System.Object
struct Il2CppObject;

#include "AWSSDK_Core_Amazon_Runtime_Internal_PipelineHandle1486422324.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.ThreadPoolExecutionHandler
struct  ThreadPoolExecutionHandler_t2707466110  : public PipelineHandler_t1486422324
{
public:

public:
};

struct ThreadPoolExecutionHandler_t2707466110_StaticFields
{
public:
	// Amazon.Runtime.Internal.Util.ThreadPoolThrottler`1<Amazon.Runtime.IAsyncExecutionContext> Amazon.Runtime.Internal.ThreadPoolExecutionHandler::_throttler
	ThreadPoolThrottler_1_t736579114 * ____throttler_3;
	// System.Object Amazon.Runtime.Internal.ThreadPoolExecutionHandler::_lock
	Il2CppObject * ____lock_4;

public:
	inline static int32_t get_offset_of__throttler_3() { return static_cast<int32_t>(offsetof(ThreadPoolExecutionHandler_t2707466110_StaticFields, ____throttler_3)); }
	inline ThreadPoolThrottler_1_t736579114 * get__throttler_3() const { return ____throttler_3; }
	inline ThreadPoolThrottler_1_t736579114 ** get_address_of__throttler_3() { return &____throttler_3; }
	inline void set__throttler_3(ThreadPoolThrottler_1_t736579114 * value)
	{
		____throttler_3 = value;
		Il2CppCodeGenWriteBarrier(&____throttler_3, value);
	}

	inline static int32_t get_offset_of__lock_4() { return static_cast<int32_t>(offsetof(ThreadPoolExecutionHandler_t2707466110_StaticFields, ____lock_4)); }
	inline Il2CppObject * get__lock_4() const { return ____lock_4; }
	inline Il2CppObject ** get_address_of__lock_4() { return &____lock_4; }
	inline void set__lock_4(Il2CppObject * value)
	{
		____lock_4 = value;
		Il2CppCodeGenWriteBarrier(&____lock_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
