﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<Amazon.CognitoSync.Model.Record>
struct List_1_t4105675293;

#include "AWSSDK_Core_Amazon_Runtime_AmazonWebServiceResponse529043356.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.CognitoSync.Model.UpdateRecordsResponse
struct  UpdateRecordsResponse_t1893058994  : public AmazonWebServiceResponse_t529043356
{
public:
	// System.Collections.Generic.List`1<Amazon.CognitoSync.Model.Record> Amazon.CognitoSync.Model.UpdateRecordsResponse::_records
	List_1_t4105675293 * ____records_3;

public:
	inline static int32_t get_offset_of__records_3() { return static_cast<int32_t>(offsetof(UpdateRecordsResponse_t1893058994, ____records_3)); }
	inline List_1_t4105675293 * get__records_3() const { return ____records_3; }
	inline List_1_t4105675293 ** get_address_of__records_3() { return &____records_3; }
	inline void set__records_3(List_1_t4105675293 * value)
	{
		____records_3 = value;
		Il2CppCodeGenWriteBarrier(&____records_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
