﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr260769561MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Type,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
#define Transform_1__ctor_m434010124(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t866906092 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m442128521_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Type,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m3663802396(__this, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Transform_1_t866906092 *, Type_t *, int32_t, const MethodInfo*))Transform_1_Invoke_m3541896077_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Type,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
#define Transform_1_BeginInvoke_m32670971(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t866906092 *, Type_t *, int32_t, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m3476592820_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Type,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
#define Transform_1_EndInvoke_m2667156486(__this, ___result0, method) ((  DictionaryEntry_t3048875398  (*) (Transform_1_t866906092 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m3296427051_gshared)(__this, ___result0, method)
