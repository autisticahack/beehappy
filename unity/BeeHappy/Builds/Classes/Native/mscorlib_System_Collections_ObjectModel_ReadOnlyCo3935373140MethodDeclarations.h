﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>
struct ReadOnlyCollection_1_t3935373140;
// System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>
struct IList_1_t4290528049;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>[]
struct KeyValuePair_2U5BU5D_t598529833;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>
struct IEnumerator_1_t1225111275;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23749587448.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m2944298190_gshared (ReadOnlyCollection_1_t3935373140 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m2944298190(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t3935373140 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m2944298190_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1426589894_gshared (ReadOnlyCollection_1_t3935373140 * __this, KeyValuePair_2_t3749587448  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1426589894(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t3935373140 *, KeyValuePair_2_t3749587448 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1426589894_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1607705970_gshared (ReadOnlyCollection_1_t3935373140 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1607705970(__this, method) ((  void (*) (ReadOnlyCollection_1_t3935373140 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1607705970_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3140313671_gshared (ReadOnlyCollection_1_t3935373140 * __this, int32_t ___index0, KeyValuePair_2_t3749587448  ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3140313671(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t3935373140 *, int32_t, KeyValuePair_2_t3749587448 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3140313671_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m4290599695_gshared (ReadOnlyCollection_1_t3935373140 * __this, KeyValuePair_2_t3749587448  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m4290599695(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t3935373140 *, KeyValuePair_2_t3749587448 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m4290599695_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m2539307899_gshared (ReadOnlyCollection_1_t3935373140 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m2539307899(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t3935373140 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m2539307899_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  KeyValuePair_2_t3749587448  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3260206987_gshared (ReadOnlyCollection_1_t3935373140 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3260206987(__this, ___index0, method) ((  KeyValuePair_2_t3749587448  (*) (ReadOnlyCollection_1_t3935373140 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3260206987_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1298018364_gshared (ReadOnlyCollection_1_t3935373140 * __this, int32_t ___index0, KeyValuePair_2_t3749587448  ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1298018364(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t3935373140 *, int32_t, KeyValuePair_2_t3749587448 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1298018364_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3125609028_gshared (ReadOnlyCollection_1_t3935373140 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3125609028(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3935373140 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3125609028_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1882058577_gshared (ReadOnlyCollection_1_t3935373140 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1882058577(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t3935373140 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1882058577_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m4022053088_gshared (ReadOnlyCollection_1_t3935373140 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m4022053088(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t3935373140 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m4022053088_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m3518465797_gshared (ReadOnlyCollection_1_t3935373140 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m3518465797(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t3935373140 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m3518465797_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m2957409009_gshared (ReadOnlyCollection_1_t3935373140 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m2957409009(__this, method) ((  void (*) (ReadOnlyCollection_1_t3935373140 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m2957409009_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m878186993_gshared (ReadOnlyCollection_1_t3935373140 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m878186993(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t3935373140 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m878186993_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3908815787_gshared (ReadOnlyCollection_1_t3935373140 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3908815787(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t3935373140 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3908815787_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m618854126_gshared (ReadOnlyCollection_1_t3935373140 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m618854126(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t3935373140 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m618854126_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m2016483108_gshared (ReadOnlyCollection_1_t3935373140 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m2016483108(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t3935373140 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m2016483108_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m350920196_gshared (ReadOnlyCollection_1_t3935373140 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m350920196(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t3935373140 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m350920196_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3377582085_gshared (ReadOnlyCollection_1_t3935373140 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3377582085(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3935373140 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3377582085_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m2692917937_gshared (ReadOnlyCollection_1_t3935373140 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m2692917937(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t3935373140 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m2692917937_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m4058522126_gshared (ReadOnlyCollection_1_t3935373140 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m4058522126(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3935373140 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m4058522126_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2207245985_gshared (ReadOnlyCollection_1_t3935373140 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2207245985(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3935373140 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2207245985_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m3103781786_gshared (ReadOnlyCollection_1_t3935373140 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m3103781786(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t3935373140 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m3103781786_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m274076051_gshared (ReadOnlyCollection_1_t3935373140 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m274076051(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t3935373140 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m274076051_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m1019355076_gshared (ReadOnlyCollection_1_t3935373140 * __this, KeyValuePair_2_t3749587448  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m1019355076(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t3935373140 *, KeyValuePair_2_t3749587448 , const MethodInfo*))ReadOnlyCollection_1_Contains_m1019355076_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m3202968754_gshared (ReadOnlyCollection_1_t3935373140 * __this, KeyValuePair_2U5BU5D_t598529833* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m3202968754(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t3935373140 *, KeyValuePair_2U5BU5D_t598529833*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m3202968754_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m3559139069_gshared (ReadOnlyCollection_1_t3935373140 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m3559139069(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t3935373140 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m3559139069_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m2383959256_gshared (ReadOnlyCollection_1_t3935373140 * __this, KeyValuePair_2_t3749587448  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m2383959256(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t3935373140 *, KeyValuePair_2_t3749587448 , const MethodInfo*))ReadOnlyCollection_1_IndexOf_m2383959256_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m3067796389_gshared (ReadOnlyCollection_1_t3935373140 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m3067796389(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t3935373140 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m3067796389_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::get_Item(System.Int32)
extern "C"  KeyValuePair_2_t3749587448  ReadOnlyCollection_1_get_Item_m1724510567_gshared (ReadOnlyCollection_1_t3935373140 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m1724510567(__this, ___index0, method) ((  KeyValuePair_2_t3749587448  (*) (ReadOnlyCollection_1_t3935373140 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m1724510567_gshared)(__this, ___index0, method)
