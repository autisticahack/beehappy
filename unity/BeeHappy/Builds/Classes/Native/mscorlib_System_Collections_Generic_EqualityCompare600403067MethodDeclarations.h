﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Amazon.Runtime.Metric>
struct DefaultComparer_t600403067;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_Runtime_Metric3273440202.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Amazon.Runtime.Metric>::.ctor()
extern "C"  void DefaultComparer__ctor_m550166934_gshared (DefaultComparer_t600403067 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m550166934(__this, method) ((  void (*) (DefaultComparer_t600403067 *, const MethodInfo*))DefaultComparer__ctor_m550166934_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Amazon.Runtime.Metric>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m2195120501_gshared (DefaultComparer_t600403067 * __this, int32_t ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m2195120501(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t600403067 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m2195120501_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Amazon.Runtime.Metric>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m1673379525_gshared (DefaultComparer_t600403067 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m1673379525(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t600403067 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m1673379525_gshared)(__this, ___x0, ___y1, method)
