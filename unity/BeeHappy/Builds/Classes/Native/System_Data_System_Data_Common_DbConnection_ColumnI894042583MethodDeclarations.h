﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Data.Common.DbConnection/ColumnInfo
struct ColumnInfo_t894042583;
struct ColumnInfo_t894042583_marshaled_pinvoke;
struct ColumnInfo_t894042583_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "System_Data_System_Data_Common_DbConnection_ColumnI894042583.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Type1303803226.h"

// System.Void System.Data.Common.DbConnection/ColumnInfo::.ctor(System.String,System.Type)
extern "C"  void ColumnInfo__ctor_m953501460 (ColumnInfo_t894042583 * __this, String_t* ___name0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct ColumnInfo_t894042583;
struct ColumnInfo_t894042583_marshaled_pinvoke;

extern "C" void ColumnInfo_t894042583_marshal_pinvoke(const ColumnInfo_t894042583& unmarshaled, ColumnInfo_t894042583_marshaled_pinvoke& marshaled);
extern "C" void ColumnInfo_t894042583_marshal_pinvoke_back(const ColumnInfo_t894042583_marshaled_pinvoke& marshaled, ColumnInfo_t894042583& unmarshaled);
extern "C" void ColumnInfo_t894042583_marshal_pinvoke_cleanup(ColumnInfo_t894042583_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct ColumnInfo_t894042583;
struct ColumnInfo_t894042583_marshaled_com;

extern "C" void ColumnInfo_t894042583_marshal_com(const ColumnInfo_t894042583& unmarshaled, ColumnInfo_t894042583_marshaled_com& marshaled);
extern "C" void ColumnInfo_t894042583_marshal_com_back(const ColumnInfo_t894042583_marshaled_com& marshaled, ColumnInfo_t894042583& unmarshaled);
extern "C" void ColumnInfo_t894042583_marshal_com_cleanup(ColumnInfo_t894042583_marshaled_com& marshaled);
