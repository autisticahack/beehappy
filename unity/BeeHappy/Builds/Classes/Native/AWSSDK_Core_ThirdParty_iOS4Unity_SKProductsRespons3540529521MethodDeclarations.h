﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ThirdParty.iOS4Unity.SKProductsResponse
struct SKProductsResponse_t3540529521;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr2504060609.h"

// System.Void ThirdParty.iOS4Unity.SKProductsResponse::.cctor()
extern "C"  void SKProductsResponse__cctor_m2132612730 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.iOS4Unity.SKProductsResponse::.ctor(System.IntPtr)
extern "C"  void SKProductsResponse__ctor_m1522197705 (SKProductsResponse_t3540529521 * __this, IntPtr_t ___handle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
