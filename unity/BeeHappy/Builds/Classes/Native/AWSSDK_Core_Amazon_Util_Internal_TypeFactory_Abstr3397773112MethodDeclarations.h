﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Util.Internal.TypeFactory/AbstractTypeInfo
struct AbstractTypeInfo_t3397773112;
// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void Amazon.Util.Internal.TypeFactory/AbstractTypeInfo::.ctor(System.Type)
extern "C"  void AbstractTypeInfo__ctor_m2615692644 (AbstractTypeInfo_t3397773112 * __this, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Amazon.Util.Internal.TypeFactory/AbstractTypeInfo::get_Type()
extern "C"  Type_t * AbstractTypeInfo_get_Type_m81605808 (AbstractTypeInfo_t3397773112 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Amazon.Util.Internal.TypeFactory/AbstractTypeInfo::GetHashCode()
extern "C"  int32_t AbstractTypeInfo_GetHashCode_m3841022028 (AbstractTypeInfo_t3397773112 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Util.Internal.TypeFactory/AbstractTypeInfo::Equals(System.Object)
extern "C"  bool AbstractTypeInfo_Equals_m1517428708 (AbstractTypeInfo_t3397773112 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Util.Internal.TypeFactory/AbstractTypeInfo::get_FullName()
extern "C"  String_t* AbstractTypeInfo_get_FullName_m3024132245 (AbstractTypeInfo_t3397773112 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
