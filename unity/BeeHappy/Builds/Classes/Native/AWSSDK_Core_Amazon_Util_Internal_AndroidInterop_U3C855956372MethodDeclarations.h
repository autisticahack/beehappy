﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Util.Internal.AndroidInterop/<>c__2`1<System.Object>
struct U3CU3Ec__2_1_t855956372;
// System.Reflection.MethodInfo
struct MethodInfo_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_MethodInfo3330546337.h"

// System.Void Amazon.Util.Internal.AndroidInterop/<>c__2`1<System.Object>::.cctor()
extern "C"  void U3CU3Ec__2_1__cctor_m3271770178_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define U3CU3Ec__2_1__cctor_m3271770178(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))U3CU3Ec__2_1__cctor_m3271770178_gshared)(__this /* static, unused */, method)
// System.Void Amazon.Util.Internal.AndroidInterop/<>c__2`1<System.Object>::.ctor()
extern "C"  void U3CU3Ec__2_1__ctor_m3948945169_gshared (U3CU3Ec__2_1_t855956372 * __this, const MethodInfo* method);
#define U3CU3Ec__2_1__ctor_m3948945169(__this, method) ((  void (*) (U3CU3Ec__2_1_t855956372 *, const MethodInfo*))U3CU3Ec__2_1__ctor_m3948945169_gshared)(__this, method)
// System.Boolean Amazon.Util.Internal.AndroidInterop/<>c__2`1<System.Object>::<CallMethod>b__2_0(System.Reflection.MethodInfo)
extern "C"  bool U3CU3Ec__2_1_U3CCallMethodU3Eb__2_0_m3197395284_gshared (U3CU3Ec__2_1_t855956372 * __this, MethodInfo_t * ___x0, const MethodInfo* method);
#define U3CU3Ec__2_1_U3CCallMethodU3Eb__2_0_m3197395284(__this, ___x0, method) ((  bool (*) (U3CU3Ec__2_1_t855956372 *, MethodInfo_t *, const MethodInfo*))U3CU3Ec__2_1_U3CCallMethodU3Eb__2_0_m3197395284_gshared)(__this, ___x0, method)
// System.Boolean Amazon.Util.Internal.AndroidInterop/<>c__2`1<System.Object>::<CallMethod>b__2_1(System.Reflection.MethodInfo)
extern "C"  bool U3CU3Ec__2_1_U3CCallMethodU3Eb__2_1_m3462756879_gshared (U3CU3Ec__2_1_t855956372 * __this, MethodInfo_t * ___x0, const MethodInfo* method);
#define U3CU3Ec__2_1_U3CCallMethodU3Eb__2_1_m3462756879(__this, ___x0, method) ((  bool (*) (U3CU3Ec__2_1_t855956372 *, MethodInfo_t *, const MethodInfo*))U3CU3Ec__2_1_U3CCallMethodU3Eb__2_1_m3462756879_gshared)(__this, ___x0, method)
