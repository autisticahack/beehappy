﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ThirdParty.iOS4Unity.NSObject
struct NSObject_t1518098886;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_NSObject1518098886.h"
#include "mscorlib_System_IntPtr2504060609.h"

// System.Void ThirdParty.iOS4Unity.Runtime::RegisterNSObject(ThirdParty.iOS4Unity.NSObject)
extern "C"  void Runtime_RegisterNSObject_m2743187242 (Il2CppObject * __this /* static, unused */, NSObject_t1518098886 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.iOS4Unity.Runtime::UnregisterNSObject(System.IntPtr)
extern "C"  void Runtime_UnregisterNSObject_m1970674904 (Il2CppObject * __this /* static, unused */, IntPtr_t ___handle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.iOS4Unity.Runtime::.cctor()
extern "C"  void Runtime__cctor_m248034931 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
