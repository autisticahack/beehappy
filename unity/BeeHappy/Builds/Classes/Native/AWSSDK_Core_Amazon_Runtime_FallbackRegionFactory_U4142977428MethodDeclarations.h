﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.FallbackRegionFactory/<>c
struct U3CU3Ec_t4142977428;
// Amazon.Runtime.AWSRegion
struct AWSRegion_t969138115;

#include "codegen/il2cpp-codegen.h"

// System.Void Amazon.Runtime.FallbackRegionFactory/<>c::.cctor()
extern "C"  void U3CU3Ec__cctor_m1030783046 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.FallbackRegionFactory/<>c::.ctor()
extern "C"  void U3CU3Ec__ctor_m3083997235 (U3CU3Ec_t4142977428 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.AWSRegion Amazon.Runtime.FallbackRegionFactory/<>c::<Reset>b__11_0()
extern "C"  AWSRegion_t969138115 * U3CU3Ec_U3CResetU3Eb__11_0_m3045317029 (U3CU3Ec_t4142977428 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.AWSRegion Amazon.Runtime.FallbackRegionFactory/<>c::<Reset>b__11_1()
extern "C"  AWSRegion_t969138115 * U3CU3Ec_U3CResetU3Eb__11_1_m3045317124 (U3CU3Ec_t4142977428 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
