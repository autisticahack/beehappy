﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.Record>
struct List_1_t237920701;

#include "mscorlib_System_EventArgs3289624707.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.CognitoSync.SyncManager.SyncSuccessEventArgs
struct  SyncSuccessEventArgs_t4277381347  : public EventArgs_t3289624707
{
public:
	// System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.Record> Amazon.CognitoSync.SyncManager.SyncSuccessEventArgs::<UpdatedRecords>k__BackingField
	List_1_t237920701 * ___U3CUpdatedRecordsU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CUpdatedRecordsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(SyncSuccessEventArgs_t4277381347, ___U3CUpdatedRecordsU3Ek__BackingField_1)); }
	inline List_1_t237920701 * get_U3CUpdatedRecordsU3Ek__BackingField_1() const { return ___U3CUpdatedRecordsU3Ek__BackingField_1; }
	inline List_1_t237920701 ** get_address_of_U3CUpdatedRecordsU3Ek__BackingField_1() { return &___U3CUpdatedRecordsU3Ek__BackingField_1; }
	inline void set_U3CUpdatedRecordsU3Ek__BackingField_1(List_1_t237920701 * value)
	{
		___U3CUpdatedRecordsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CUpdatedRecordsU3Ek__BackingField_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
