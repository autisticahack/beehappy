﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_NetworkReachability1092747145.h"

// UnityEngine.NetworkReachability Amazon.Util.Storage.Internal.NetworkInfo::get_Reachability()
extern "C"  int32_t NetworkInfo_get_Reachability_m2677139352 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
