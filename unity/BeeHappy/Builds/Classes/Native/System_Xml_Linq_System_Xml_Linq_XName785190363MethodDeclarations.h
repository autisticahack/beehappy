﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.Linq.XName
struct XName_t785190363;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// System.String
struct String_t;
// System.Xml.Linq.XNamespace
struct XNamespace_t1613015075;
// System.Exception
struct Exception_t1927440687;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serialization228987430.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"
#include "mscorlib_System_String2029220233.h"
#include "System_Xml_Linq_System_Xml_Linq_XNamespace1613015075.h"
#include "System_Xml_Linq_System_Xml_Linq_XName785190363.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Xml.Linq.XName::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void XName__ctor_m3384925849 (XName_t785190363 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Linq.XName::.ctor(System.String,System.Xml.Linq.XNamespace)
extern "C"  void XName__ctor_m293531099 (XName_t785190363 * __this, String_t* ___local0, XNamespace_t1613015075 * ___ns1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.Linq.XName::System.IEquatable<System.Xml.Linq.XName>.Equals(System.Xml.Linq.XName)
extern "C"  bool XName_System_IEquatableU3CSystem_Xml_Linq_XNameU3E_Equals_m1026119289 (XName_t785190363 * __this, XName_t785190363 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Linq.XName::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void XName_System_Runtime_Serialization_ISerializable_GetObjectData_m580019797 (XName_t785190363 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception System.Xml.Linq.XName::ErrorInvalidExpandedName()
extern "C"  Exception_t1927440687 * XName_ErrorInvalidExpandedName_m1515653754 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.Linq.XName::get_LocalName()
extern "C"  String_t* XName_get_LocalName_m995190518 (XName_t785190363 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Linq.XNamespace System.Xml.Linq.XName::get_Namespace()
extern "C"  XNamespace_t1613015075 * XName_get_Namespace_m3835650466 (XName_t785190363 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.Linq.XName::get_NamespaceName()
extern "C"  String_t* XName_get_NamespaceName_m4269716270 (XName_t785190363 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.Linq.XName::Equals(System.Object)
extern "C"  bool XName_Equals_m3014313799 (XName_t785190363 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Linq.XName System.Xml.Linq.XName::Get(System.String)
extern "C"  XName_t785190363 * XName_Get_m477503418 (Il2CppObject * __this /* static, unused */, String_t* ___expandedName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Linq.XName::ExpandName(System.String,System.String&,System.String&)
extern "C"  void XName_ExpandName_m3429394577 (Il2CppObject * __this /* static, unused */, String_t* ___expandedName0, String_t** ___local1, String_t** ___ns2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Linq.XName System.Xml.Linq.XName::Get(System.String,System.String)
extern "C"  XName_t785190363 * XName_Get_m2187745362 (Il2CppObject * __this /* static, unused */, String_t* ___localName0, String_t* ___namespaceName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Xml.Linq.XName::GetHashCode()
extern "C"  int32_t XName_GetHashCode_m518193777 (XName_t785190363 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.Linq.XName::ToString()
extern "C"  String_t* XName_ToString_m2526618213 (XName_t785190363 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.Linq.XName::op_Equality(System.Xml.Linq.XName,System.Xml.Linq.XName)
extern "C"  bool XName_op_Equality_m148690364 (Il2CppObject * __this /* static, unused */, XName_t785190363 * ___n10, XName_t785190363 * ___n21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Linq.XName System.Xml.Linq.XName::op_Implicit(System.String)
extern "C"  XName_t785190363 * XName_op_Implicit_m3820697839 (Il2CppObject * __this /* static, unused */, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.Linq.XName::op_Inequality(System.Xml.Linq.XName,System.Xml.Linq.XName)
extern "C"  bool XName_op_Inequality_m4197580533 (Il2CppObject * __this /* static, unused */, XName_t785190363 * ___n10, XName_t785190363 * ___n21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
