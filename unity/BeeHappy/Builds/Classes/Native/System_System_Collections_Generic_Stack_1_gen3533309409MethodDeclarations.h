﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Stack`1<ThirdParty.Json.LitJson.JsonToken>
struct Stack_1_t3533309409;
// System.Object
struct Il2CppObject;
// System.Array
struct Il2CppArray;
// System.Collections.Generic.IEnumerator`1<ThirdParty.Json.LitJson.JsonToken>
struct IEnumerator_1_t4216072378;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// ThirdParty.Json.LitJson.JsonToken[]
struct JsonTokenU5BU5D_t35336510;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_JsonToken2445581255.h"
#include "System_System_Collections_Generic_Stack_1_Enumerat4183307769.h"

// System.Void System.Collections.Generic.Stack`1<ThirdParty.Json.LitJson.JsonToken>::.ctor()
extern "C"  void Stack_1__ctor_m3467349316_gshared (Stack_1_t3533309409 * __this, const MethodInfo* method);
#define Stack_1__ctor_m3467349316(__this, method) ((  void (*) (Stack_1_t3533309409 *, const MethodInfo*))Stack_1__ctor_m3467349316_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1<ThirdParty.Json.LitJson.JsonToken>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Stack_1_System_Collections_ICollection_get_IsSynchronized_m2712687689_gshared (Stack_1_t3533309409 * __this, const MethodInfo* method);
#define Stack_1_System_Collections_ICollection_get_IsSynchronized_m2712687689(__this, method) ((  bool (*) (Stack_1_t3533309409 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_IsSynchronized_m2712687689_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1<ThirdParty.Json.LitJson.JsonToken>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Stack_1_System_Collections_ICollection_get_SyncRoot_m2843962149_gshared (Stack_1_t3533309409 * __this, const MethodInfo* method);
#define Stack_1_System_Collections_ICollection_get_SyncRoot_m2843962149(__this, method) ((  Il2CppObject * (*) (Stack_1_t3533309409 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_SyncRoot_m2843962149_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<ThirdParty.Json.LitJson.JsonToken>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Stack_1_System_Collections_ICollection_CopyTo_m3063148513_gshared (Stack_1_t3533309409 * __this, Il2CppArray * ___dest0, int32_t ___idx1, const MethodInfo* method);
#define Stack_1_System_Collections_ICollection_CopyTo_m3063148513(__this, ___dest0, ___idx1, method) ((  void (*) (Stack_1_t3533309409 *, Il2CppArray *, int32_t, const MethodInfo*))Stack_1_System_Collections_ICollection_CopyTo_m3063148513_gshared)(__this, ___dest0, ___idx1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Stack`1<ThirdParty.Json.LitJson.JsonToken>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1068801193_gshared (Stack_1_t3533309409 * __this, const MethodInfo* method);
#define Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1068801193(__this, method) ((  Il2CppObject* (*) (Stack_1_t3533309409 *, const MethodInfo*))Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1068801193_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Stack`1<ThirdParty.Json.LitJson.JsonToken>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Stack_1_System_Collections_IEnumerable_GetEnumerator_m923911668_gshared (Stack_1_t3533309409 * __this, const MethodInfo* method);
#define Stack_1_System_Collections_IEnumerable_GetEnumerator_m923911668(__this, method) ((  Il2CppObject * (*) (Stack_1_t3533309409 *, const MethodInfo*))Stack_1_System_Collections_IEnumerable_GetEnumerator_m923911668_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1<ThirdParty.Json.LitJson.JsonToken>::Contains(T)
extern "C"  bool Stack_1_Contains_m681148384_gshared (Stack_1_t3533309409 * __this, int32_t ___t0, const MethodInfo* method);
#define Stack_1_Contains_m681148384(__this, ___t0, method) ((  bool (*) (Stack_1_t3533309409 *, int32_t, const MethodInfo*))Stack_1_Contains_m681148384_gshared)(__this, ___t0, method)
// System.Void System.Collections.Generic.Stack`1<ThirdParty.Json.LitJson.JsonToken>::CopyTo(T[],System.Int32)
extern "C"  void Stack_1_CopyTo_m3439091878_gshared (Stack_1_t3533309409 * __this, JsonTokenU5BU5D_t35336510* ___dest0, int32_t ___idx1, const MethodInfo* method);
#define Stack_1_CopyTo_m3439091878(__this, ___dest0, ___idx1, method) ((  void (*) (Stack_1_t3533309409 *, JsonTokenU5BU5D_t35336510*, int32_t, const MethodInfo*))Stack_1_CopyTo_m3439091878_gshared)(__this, ___dest0, ___idx1, method)
// T System.Collections.Generic.Stack`1<ThirdParty.Json.LitJson.JsonToken>::Peek()
extern "C"  int32_t Stack_1_Peek_m2985740121_gshared (Stack_1_t3533309409 * __this, const MethodInfo* method);
#define Stack_1_Peek_m2985740121(__this, method) ((  int32_t (*) (Stack_1_t3533309409 *, const MethodInfo*))Stack_1_Peek_m2985740121_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<ThirdParty.Json.LitJson.JsonToken>::Pop()
extern "C"  int32_t Stack_1_Pop_m607882821_gshared (Stack_1_t3533309409 * __this, const MethodInfo* method);
#define Stack_1_Pop_m607882821(__this, method) ((  int32_t (*) (Stack_1_t3533309409 *, const MethodInfo*))Stack_1_Pop_m607882821_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<ThirdParty.Json.LitJson.JsonToken>::Push(T)
extern "C"  void Stack_1_Push_m912983667_gshared (Stack_1_t3533309409 * __this, int32_t ___t0, const MethodInfo* method);
#define Stack_1_Push_m912983667(__this, ___t0, method) ((  void (*) (Stack_1_t3533309409 *, int32_t, const MethodInfo*))Stack_1_Push_m912983667_gshared)(__this, ___t0, method)
// T[] System.Collections.Generic.Stack`1<ThirdParty.Json.LitJson.JsonToken>::ToArray()
extern "C"  JsonTokenU5BU5D_t35336510* Stack_1_ToArray_m247963952_gshared (Stack_1_t3533309409 * __this, const MethodInfo* method);
#define Stack_1_ToArray_m247963952(__this, method) ((  JsonTokenU5BU5D_t35336510* (*) (Stack_1_t3533309409 *, const MethodInfo*))Stack_1_ToArray_m247963952_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Stack`1<ThirdParty.Json.LitJson.JsonToken>::get_Count()
extern "C"  int32_t Stack_1_get_Count_m1641722018_gshared (Stack_1_t3533309409 * __this, const MethodInfo* method);
#define Stack_1_get_Count_m1641722018(__this, method) ((  int32_t (*) (Stack_1_t3533309409 *, const MethodInfo*))Stack_1_get_Count_m1641722018_gshared)(__this, method)
// System.Collections.Generic.Stack`1/Enumerator<T> System.Collections.Generic.Stack`1<ThirdParty.Json.LitJson.JsonToken>::GetEnumerator()
extern "C"  Enumerator_t4183307769  Stack_1_GetEnumerator_m534699437_gshared (Stack_1_t3533309409 * __this, const MethodInfo* method);
#define Stack_1_GetEnumerator_m534699437(__this, method) ((  Enumerator_t4183307769  (*) (Stack_1_t3533309409 *, const MethodInfo*))Stack_1_GetEnumerator_m534699437_gshared)(__this, method)
