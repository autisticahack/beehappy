﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>
struct List_1_t3118708580;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2653438254.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23749587448.h"

// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m2675115469_gshared (Enumerator_t2653438254 * __this, List_1_t3118708580 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m2675115469(__this, ___l0, method) ((  void (*) (Enumerator_t2653438254 *, List_1_t3118708580 *, const MethodInfo*))Enumerator__ctor_m2675115469_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4017277405_gshared (Enumerator_t2653438254 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m4017277405(__this, method) ((  void (*) (Enumerator_t2653438254 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m4017277405_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1567080597_gshared (Enumerator_t2653438254 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1567080597(__this, method) ((  Il2CppObject * (*) (Enumerator_t2653438254 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1567080597_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::Dispose()
extern "C"  void Enumerator_Dispose_m1712795290_gshared (Enumerator_t2653438254 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1712795290(__this, method) ((  void (*) (Enumerator_t2653438254 *, const MethodInfo*))Enumerator_Dispose_m1712795290_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::VerifyState()
extern "C"  void Enumerator_VerifyState_m998682547_gshared (Enumerator_t2653438254 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m998682547(__this, method) ((  void (*) (Enumerator_t2653438254 *, const MethodInfo*))Enumerator_VerifyState_m998682547_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3068457093_gshared (Enumerator_t2653438254 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3068457093(__this, method) ((  bool (*) (Enumerator_t2653438254 *, const MethodInfo*))Enumerator_MoveNext_m3068457093_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::get_Current()
extern "C"  KeyValuePair_2_t3749587448  Enumerator_get_Current_m1327994608_gshared (Enumerator_t2653438254 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1327994608(__this, method) ((  KeyValuePair_2_t3749587448  (*) (Enumerator_t2653438254 *, const MethodInfo*))Enumerator_get_Current_m1327994608_gshared)(__this, method)
