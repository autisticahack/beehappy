﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.SecurityToken.Model.Internal.MarshallTransformations.CredentialsUnmarshaller
struct CredentialsUnmarshaller_t3599710454;
// Amazon.SecurityToken.Model.Credentials
struct Credentials_t3554032640;
// Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext
struct XmlUnmarshallerContext_t1179575220;
// Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext
struct JsonUnmarshallerContext_t456235889;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_XmlU1179575220.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_JsonU456235889.h"

// Amazon.SecurityToken.Model.Credentials Amazon.SecurityToken.Model.Internal.MarshallTransformations.CredentialsUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext)
extern "C"  Credentials_t3554032640 * CredentialsUnmarshaller_Unmarshall_m2423097439 (CredentialsUnmarshaller_t3599710454 * __this, XmlUnmarshallerContext_t1179575220 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.SecurityToken.Model.Credentials Amazon.SecurityToken.Model.Internal.MarshallTransformations.CredentialsUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern "C"  Credentials_t3554032640 * CredentialsUnmarshaller_Unmarshall_m4114969110 (CredentialsUnmarshaller_t3599710454 * __this, JsonUnmarshallerContext_t456235889 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.SecurityToken.Model.Internal.MarshallTransformations.CredentialsUnmarshaller Amazon.SecurityToken.Model.Internal.MarshallTransformations.CredentialsUnmarshaller::get_Instance()
extern "C"  CredentialsUnmarshaller_t3599710454 * CredentialsUnmarshaller_get_Instance_m3716340548 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.SecurityToken.Model.Internal.MarshallTransformations.CredentialsUnmarshaller::.ctor()
extern "C"  void CredentialsUnmarshaller__ctor_m2680197702 (CredentialsUnmarshaller_t3599710454 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.SecurityToken.Model.Internal.MarshallTransformations.CredentialsUnmarshaller::.cctor()
extern "C"  void CredentialsUnmarshaller__cctor_m3449932329 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
