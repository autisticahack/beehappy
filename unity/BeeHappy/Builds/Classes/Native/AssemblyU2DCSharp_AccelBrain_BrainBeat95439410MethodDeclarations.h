﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AccelBrain.BrainBeat
struct BrainBeat_t95439410;
// System.Single[]
struct SingleU5BU5D_t577127397;

#include "codegen/il2cpp-codegen.h"

// System.Void AccelBrain.BrainBeat::.ctor()
extern "C"  void BrainBeat__ctor_m3595805611 (BrainBeat_t95439410 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AccelBrain.BrainBeat::set_LeftFrequency(System.Double)
extern "C"  void BrainBeat_set_LeftFrequency_m405114723 (BrainBeat_t95439410 * __this, double ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double AccelBrain.BrainBeat::get_LeftFrequency()
extern "C"  double BrainBeat_get_LeftFrequency_m2404184092 (BrainBeat_t95439410 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AccelBrain.BrainBeat::set_RightFrequency(System.Double)
extern "C"  void BrainBeat_set_RightFrequency_m2161147482 (BrainBeat_t95439410 * __this, double ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double AccelBrain.BrainBeat::get_RightFrequency()
extern "C"  double BrainBeat_get_RightFrequency_m218941375 (BrainBeat_t95439410 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AccelBrain.BrainBeat::set_Gain(System.Double)
extern "C"  void BrainBeat_set_Gain_m304053317 (BrainBeat_t95439410 * __this, double ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double AccelBrain.BrainBeat::get_Gain()
extern "C"  double BrainBeat_get_Gain_m3671760702 (BrainBeat_t95439410 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AccelBrain.BrainBeat::set_SampleRate(System.Double)
extern "C"  void BrainBeat_set_SampleRate_m1018522490 (BrainBeat_t95439410 * __this, double ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double AccelBrain.BrainBeat::get_SampleRate()
extern "C"  double BrainBeat_get_SampleRate_m2530175775 (BrainBeat_t95439410 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single[] AccelBrain.BrainBeat::AudioFilterRead(System.Single[],System.Int32)
extern "C"  SingleU5BU5D_t577127397* BrainBeat_AudioFilterRead_m1460037605 (BrainBeat_t95439410 * __this, SingleU5BU5D_t577127397* ___data0, int32_t ___channels1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AccelBrain.BrainBeat::PlayBeat()
extern "C"  void BrainBeat_PlayBeat_m2654305297 (BrainBeat_t95439410 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AccelBrain.BrainBeat::StopBeat()
extern "C"  void BrainBeat_StopBeat_m2801581641 (BrainBeat_t95439410 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
