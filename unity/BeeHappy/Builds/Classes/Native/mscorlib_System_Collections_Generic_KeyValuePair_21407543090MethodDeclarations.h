﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21407543090.h"
#include "mscorlib_System_Object2689449295.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_ObjectMetadata4058137740.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m1145061153_gshared (KeyValuePair_2_t1407543090 * __this, Il2CppObject * ___key0, ObjectMetadata_t4058137740  ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m1145061153(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t1407543090 *, Il2CppObject *, ObjectMetadata_t4058137740 , const MethodInfo*))KeyValuePair_2__ctor_m1145061153_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m2438555343_gshared (KeyValuePair_2_t1407543090 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m2438555343(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t1407543090 *, const MethodInfo*))KeyValuePair_2_get_Key_m2438555343_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m3437532682_gshared (KeyValuePair_2_t1407543090 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m3437532682(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1407543090 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Key_m3437532682_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::get_Value()
extern "C"  ObjectMetadata_t4058137740  KeyValuePair_2_get_Value_m3741940767_gshared (KeyValuePair_2_t1407543090 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m3741940767(__this, method) ((  ObjectMetadata_t4058137740  (*) (KeyValuePair_2_t1407543090 *, const MethodInfo*))KeyValuePair_2_get_Value_m3741940767_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m4131840234_gshared (KeyValuePair_2_t1407543090 * __this, ObjectMetadata_t4058137740  ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m4131840234(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1407543090 *, ObjectMetadata_t4058137740 , const MethodInfo*))KeyValuePair_2_set_Value_m4131840234_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m1862320128_gshared (KeyValuePair_2_t1407543090 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m1862320128(__this, method) ((  String_t* (*) (KeyValuePair_2_t1407543090 *, const MethodInfo*))KeyValuePair_2_ToString_m1862320128_gshared)(__this, method)
