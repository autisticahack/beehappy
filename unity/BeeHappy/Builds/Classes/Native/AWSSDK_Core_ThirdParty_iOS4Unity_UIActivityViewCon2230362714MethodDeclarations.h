﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ThirdParty.iOS4Unity.UIActivityViewController
struct UIActivityViewController_t2230362714;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr2504060609.h"

// System.Void ThirdParty.iOS4Unity.UIActivityViewController::.cctor()
extern "C"  void UIActivityViewController__cctor_m2344876883 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.iOS4Unity.UIActivityViewController::.ctor(System.IntPtr)
extern "C"  void UIActivityViewController__ctor_m1950878868 (UIActivityViewController_t2230362714 * __this, IntPtr_t ___handle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
