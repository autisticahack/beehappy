﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.AsyncRequestContext
struct AsyncRequestContext_t3705567694;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// System.Object
struct Il2CppObject;
// Amazon.Runtime.AsyncOptions
struct AsyncOptions_t558351272;
// System.Action`4<Amazon.Runtime.AmazonWebServiceRequest,Amazon.Runtime.AmazonWebServiceResponse,System.Exception,Amazon.Runtime.AsyncOptions>
struct Action_4_t897279368;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_Runtime_AsyncOptions558351272.h"

// System.Void Amazon.Runtime.Internal.AsyncRequestContext::.ctor(System.Boolean)
extern "C"  void AsyncRequestContext__ctor_m2283679951 (AsyncRequestContext_t3705567694 * __this, bool ___enableMetrics0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.AsyncCallback Amazon.Runtime.Internal.AsyncRequestContext::get_Callback()
extern "C"  AsyncCallback_t163412349 * AsyncRequestContext_get_Callback_m575277935 (AsyncRequestContext_t3705567694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Amazon.Runtime.Internal.AsyncRequestContext::get_State()
extern "C"  Il2CppObject * AsyncRequestContext_get_State_m2139504091 (AsyncRequestContext_t3705567694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.AsyncOptions Amazon.Runtime.Internal.AsyncRequestContext::get_AsyncOptions()
extern "C"  AsyncOptions_t558351272 * AsyncRequestContext_get_AsyncOptions_m1362447346 (AsyncRequestContext_t3705567694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.AsyncRequestContext::set_AsyncOptions(Amazon.Runtime.AsyncOptions)
extern "C"  void AsyncRequestContext_set_AsyncOptions_m80089439 (AsyncRequestContext_t3705567694 * __this, AsyncOptions_t558351272 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action`4<Amazon.Runtime.AmazonWebServiceRequest,Amazon.Runtime.AmazonWebServiceResponse,System.Exception,Amazon.Runtime.AsyncOptions> Amazon.Runtime.Internal.AsyncRequestContext::get_Action()
extern "C"  Action_4_t897279368 * AsyncRequestContext_get_Action_m1907222961 (AsyncRequestContext_t3705567694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.AsyncRequestContext::set_Action(System.Action`4<Amazon.Runtime.AmazonWebServiceRequest,Amazon.Runtime.AmazonWebServiceResponse,System.Exception,Amazon.Runtime.AsyncOptions>)
extern "C"  void AsyncRequestContext_set_Action_m672207450 (AsyncRequestContext_t3705567694 * __this, Action_4_t897279368 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
