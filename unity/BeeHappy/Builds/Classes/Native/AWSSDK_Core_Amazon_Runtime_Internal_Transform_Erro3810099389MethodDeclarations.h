﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.Transform.ErrorResponseUnmarshaller
struct ErrorResponseUnmarshaller_t3810099389;
// Amazon.Runtime.Internal.ErrorResponse
struct ErrorResponse_t3502566035;
// Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext
struct XmlUnmarshallerContext_t1179575220;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_XmlU1179575220.h"

// Amazon.Runtime.Internal.ErrorResponse Amazon.Runtime.Internal.Transform.ErrorResponseUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext)
extern "C"  ErrorResponse_t3502566035 * ErrorResponseUnmarshaller_Unmarshall_m4167121930 (ErrorResponseUnmarshaller_t3810099389 * __this, XmlUnmarshallerContext_t1179575220 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.Internal.Transform.ErrorResponseUnmarshaller Amazon.Runtime.Internal.Transform.ErrorResponseUnmarshaller::GetInstance()
extern "C"  ErrorResponseUnmarshaller_t3810099389 * ErrorResponseUnmarshaller_GetInstance_m574966211 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Transform.ErrorResponseUnmarshaller::.ctor()
extern "C"  void ErrorResponseUnmarshaller__ctor_m2343028981 (ErrorResponseUnmarshaller_t3810099389 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
