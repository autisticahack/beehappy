﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.RequestEventArgs
struct RequestEventArgs_t434225820;

#include "codegen/il2cpp-codegen.h"

// System.Void Amazon.Runtime.RequestEventArgs::.ctor()
extern "C"  void RequestEventArgs__ctor_m3091082359 (RequestEventArgs_t434225820 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
