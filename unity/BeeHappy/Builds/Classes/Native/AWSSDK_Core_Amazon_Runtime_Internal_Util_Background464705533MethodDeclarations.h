﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.Util.BackgroundInvoker/<>c
struct U3CU3Ec_t464705533;
// System.Action
struct Action_t3226471752;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action3226471752.h"

// System.Void Amazon.Runtime.Internal.Util.BackgroundInvoker/<>c::.cctor()
extern "C"  void U3CU3Ec__cctor_m338509419 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.BackgroundInvoker/<>c::.ctor()
extern "C"  void U3CU3Ec__ctor_m1905858952 (U3CU3Ec_t464705533 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.BackgroundInvoker/<>c::<.ctor>b__0_0(System.Action)
extern "C"  void U3CU3Ec_U3C_ctorU3Eb__0_0_m1458084720 (U3CU3Ec_t464705533 * __this, Action_t3226471752 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
