﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Util.Internal.AndroidInterop/<>c__6`1<System.Object>
struct U3CU3Ec__6_1_t2218249864;
// System.Reflection.MethodInfo
struct MethodInfo_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_MethodInfo3330546337.h"

// System.Void Amazon.Util.Internal.AndroidInterop/<>c__6`1<System.Object>::.cctor()
extern "C"  void U3CU3Ec__6_1__cctor_m1923233326_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define U3CU3Ec__6_1__cctor_m1923233326(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))U3CU3Ec__6_1__cctor_m1923233326_gshared)(__this /* static, unused */, method)
// System.Void Amazon.Util.Internal.AndroidInterop/<>c__6`1<System.Object>::.ctor()
extern "C"  void U3CU3Ec__6_1__ctor_m822546429_gshared (U3CU3Ec__6_1_t2218249864 * __this, const MethodInfo* method);
#define U3CU3Ec__6_1__ctor_m822546429(__this, method) ((  void (*) (U3CU3Ec__6_1_t2218249864 *, const MethodInfo*))U3CU3Ec__6_1__ctor_m822546429_gshared)(__this, method)
// System.Boolean Amazon.Util.Internal.AndroidInterop/<>c__6`1<System.Object>::<GetJavaField>b__6_0(System.Reflection.MethodInfo)
extern "C"  bool U3CU3Ec__6_1_U3CGetJavaFieldU3Eb__6_0_m2345458499_gshared (U3CU3Ec__6_1_t2218249864 * __this, MethodInfo_t * ___x0, const MethodInfo* method);
#define U3CU3Ec__6_1_U3CGetJavaFieldU3Eb__6_0_m2345458499(__this, ___x0, method) ((  bool (*) (U3CU3Ec__6_1_t2218249864 *, MethodInfo_t *, const MethodInfo*))U3CU3Ec__6_1_U3CGetJavaFieldU3Eb__6_0_m2345458499_gshared)(__this, ___x0, method)
// System.Boolean Amazon.Util.Internal.AndroidInterop/<>c__6`1<System.Object>::<GetJavaField>b__6_1(System.Reflection.MethodInfo)
extern "C"  bool U3CU3Ec__6_1_U3CGetJavaFieldU3Eb__6_1_m4082877064_gshared (U3CU3Ec__6_1_t2218249864 * __this, MethodInfo_t * ___x0, const MethodInfo* method);
#define U3CU3Ec__6_1_U3CGetJavaFieldU3Eb__6_1_m4082877064(__this, ___x0, method) ((  bool (*) (U3CU3Ec__6_1_t2218249864 *, MethodInfo_t *, const MethodInfo*))U3CU3Ec__6_1_U3CGetJavaFieldU3Eb__6_1_m4082877064_gshared)(__this, ___x0, method)
