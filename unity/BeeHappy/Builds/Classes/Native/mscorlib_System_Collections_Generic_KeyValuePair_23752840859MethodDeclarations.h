﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21407543090MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,ThirdParty.Json.LitJson.ObjectMetadata>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m610505060(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3752840859 *, Type_t *, ObjectMetadata_t4058137740 , const MethodInfo*))KeyValuePair_2__ctor_m1145061153_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Type,ThirdParty.Json.LitJson.ObjectMetadata>::get_Key()
#define KeyValuePair_2_get_Key_m2704771338(__this, method) ((  Type_t * (*) (KeyValuePair_2_t3752840859 *, const MethodInfo*))KeyValuePair_2_get_Key_m2438555343_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,ThirdParty.Json.LitJson.ObjectMetadata>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m1219191989(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3752840859 *, Type_t *, const MethodInfo*))KeyValuePair_2_set_Key_m3437532682_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Type,ThirdParty.Json.LitJson.ObjectMetadata>::get_Value()
#define KeyValuePair_2_get_Value_m257981778(__this, method) ((  ObjectMetadata_t4058137740  (*) (KeyValuePair_2_t3752840859 *, const MethodInfo*))KeyValuePair_2_get_Value_m3741940767_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,ThirdParty.Json.LitJson.ObjectMetadata>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m3024527885(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3752840859 *, ObjectMetadata_t4058137740 , const MethodInfo*))KeyValuePair_2_set_Value_m4131840234_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Type,ThirdParty.Json.LitJson.ObjectMetadata>::ToString()
#define KeyValuePair_2_ToString_m1880099703(__this, method) ((  String_t* (*) (KeyValuePair_2_t3752840859 *, const MethodInfo*))KeyValuePair_2_ToString_m1862320128_gshared)(__this, method)
