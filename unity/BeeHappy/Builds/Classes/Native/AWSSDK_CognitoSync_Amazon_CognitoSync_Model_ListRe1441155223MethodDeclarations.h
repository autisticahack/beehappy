﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.CognitoSync.Model.ListRecordsResponse
struct ListRecordsResponse_t1441155223;
// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// System.Collections.Generic.List`1<Amazon.CognitoSync.Model.Record>
struct List_1_t4105675293;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Amazon.CognitoSync.Model.ListRecordsResponse::set_Count(System.Int32)
extern "C"  void ListRecordsResponse_set_Count_m3413059031 (ListRecordsResponse_t1441155223 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.CognitoSync.Model.ListRecordsResponse::get_DatasetDeletedAfterRequestedSyncCount()
extern "C"  bool ListRecordsResponse_get_DatasetDeletedAfterRequestedSyncCount_m393299136 (ListRecordsResponse_t1441155223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Model.ListRecordsResponse::set_DatasetDeletedAfterRequestedSyncCount(System.Boolean)
extern "C"  void ListRecordsResponse_set_DatasetDeletedAfterRequestedSyncCount_m869205565 (ListRecordsResponse_t1441155223 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.CognitoSync.Model.ListRecordsResponse::get_DatasetExists()
extern "C"  bool ListRecordsResponse_get_DatasetExists_m2696383051 (ListRecordsResponse_t1441155223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Model.ListRecordsResponse::set_DatasetExists(System.Boolean)
extern "C"  void ListRecordsResponse_set_DatasetExists_m1690312618 (ListRecordsResponse_t1441155223 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Amazon.CognitoSync.Model.ListRecordsResponse::get_DatasetSyncCount()
extern "C"  int64_t ListRecordsResponse_get_DatasetSyncCount_m1978127724 (ListRecordsResponse_t1441155223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Model.ListRecordsResponse::set_DatasetSyncCount(System.Int64)
extern "C"  void ListRecordsResponse_set_DatasetSyncCount_m1878334309 (ListRecordsResponse_t1441155223 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Model.ListRecordsResponse::set_LastModifiedBy(System.String)
extern "C"  void ListRecordsResponse_set_LastModifiedBy_m3952259795 (ListRecordsResponse_t1441155223 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> Amazon.CognitoSync.Model.ListRecordsResponse::get_MergedDatasetNames()
extern "C"  List_1_t1398341365 * ListRecordsResponse_get_MergedDatasetNames_m2692923686 (ListRecordsResponse_t1441155223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Model.ListRecordsResponse::set_MergedDatasetNames(System.Collections.Generic.List`1<System.String>)
extern "C"  void ListRecordsResponse_set_MergedDatasetNames_m2954755311 (ListRecordsResponse_t1441155223 * __this, List_1_t1398341365 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.CognitoSync.Model.ListRecordsResponse::get_NextToken()
extern "C"  String_t* ListRecordsResponse_get_NextToken_m3530795590 (ListRecordsResponse_t1441155223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Model.ListRecordsResponse::set_NextToken(System.String)
extern "C"  void ListRecordsResponse_set_NextToken_m574741923 (ListRecordsResponse_t1441155223 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Amazon.CognitoSync.Model.Record> Amazon.CognitoSync.Model.ListRecordsResponse::get_Records()
extern "C"  List_1_t4105675293 * ListRecordsResponse_get_Records_m560619478 (ListRecordsResponse_t1441155223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Model.ListRecordsResponse::set_Records(System.Collections.Generic.List`1<Amazon.CognitoSync.Model.Record>)
extern "C"  void ListRecordsResponse_set_Records_m2411209079 (ListRecordsResponse_t1441155223 * __this, List_1_t4105675293 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.CognitoSync.Model.ListRecordsResponse::get_SyncSessionToken()
extern "C"  String_t* ListRecordsResponse_get_SyncSessionToken_m1892889276 (ListRecordsResponse_t1441155223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Model.ListRecordsResponse::set_SyncSessionToken(System.String)
extern "C"  void ListRecordsResponse_set_SyncSessionToken_m1064647461 (ListRecordsResponse_t1441155223 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Model.ListRecordsResponse::.ctor()
extern "C"  void ListRecordsResponse__ctor_m1695905560 (ListRecordsResponse_t1441155223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
