﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>
struct Dictionary_2_t1224867506;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3914370944.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m4088904673_gshared (Enumerator_t3914370944 * __this, Dictionary_2_t1224867506 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m4088904673(__this, ___host0, method) ((  void (*) (Enumerator_t3914370944 *, Dictionary_2_t1224867506 *, const MethodInfo*))Enumerator__ctor_m4088904673_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1411351484_gshared (Enumerator_t3914370944 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1411351484(__this, method) ((  Il2CppObject * (*) (Enumerator_t3914370944 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1411351484_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1944504356_gshared (Enumerator_t3914370944 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1944504356(__this, method) ((  void (*) (Enumerator_t3914370944 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1944504356_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::Dispose()
extern "C"  void Enumerator_Dispose_m3979808513_gshared (Enumerator_t3914370944 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3979808513(__this, method) ((  void (*) (Enumerator_t3914370944 *, const MethodInfo*))Enumerator_Dispose_m3979808513_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2015454696_gshared (Enumerator_t3914370944 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2015454696(__this, method) ((  bool (*) (Enumerator_t3914370944 *, const MethodInfo*))Enumerator_MoveNext_m2015454696_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m3493547464_gshared (Enumerator_t3914370944 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3493547464(__this, method) ((  Il2CppObject * (*) (Enumerator_t3914370944 *, const MethodInfo*))Enumerator_get_Current_m3493547464_gshared)(__this, method)
