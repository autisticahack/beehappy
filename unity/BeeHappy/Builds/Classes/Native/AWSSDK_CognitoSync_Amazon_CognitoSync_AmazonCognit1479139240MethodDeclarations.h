﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.CognitoSync.AmazonCognitoSyncConfig
struct AmazonCognitoSyncConfig_t1479139240;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void Amazon.CognitoSync.AmazonCognitoSyncConfig::.ctor()
extern "C"  void AmazonCognitoSyncConfig__ctor_m3850301200 (AmazonCognitoSyncConfig_t1479139240 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.CognitoSync.AmazonCognitoSyncConfig::get_RegionEndpointServiceName()
extern "C"  String_t* AmazonCognitoSyncConfig_get_RegionEndpointServiceName_m100136023 (AmazonCognitoSyncConfig_t1479139240 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.CognitoSync.AmazonCognitoSyncConfig::get_UserAgent()
extern "C"  String_t* AmazonCognitoSyncConfig_get_UserAgent_m4281866396 (AmazonCognitoSyncConfig_t1479139240 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.AmazonCognitoSyncConfig::.cctor()
extern "C"  void AmazonCognitoSyncConfig__cctor_m1671251825 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
