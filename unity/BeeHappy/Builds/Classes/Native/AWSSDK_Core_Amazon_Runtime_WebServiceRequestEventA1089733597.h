﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.IDictionary`2<System.String,System.String>
struct IDictionary_2_t1943082916;
// System.String
struct String_t;
// System.Uri
struct Uri_t19570940;
// Amazon.Runtime.AmazonWebServiceRequest
struct AmazonWebServiceRequest_t3384026212;

#include "AWSSDK_Core_Amazon_Runtime_RequestEventArgs434225820.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.WebServiceRequestEventArgs
struct  WebServiceRequestEventArgs_t1089733597  : public RequestEventArgs_t434225820
{
public:
	// System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.Runtime.WebServiceRequestEventArgs::<Headers>k__BackingField
	Il2CppObject* ___U3CHeadersU3Ek__BackingField_1;
	// System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.Runtime.WebServiceRequestEventArgs::<Parameters>k__BackingField
	Il2CppObject* ___U3CParametersU3Ek__BackingField_2;
	// System.String Amazon.Runtime.WebServiceRequestEventArgs::<ServiceName>k__BackingField
	String_t* ___U3CServiceNameU3Ek__BackingField_3;
	// System.Uri Amazon.Runtime.WebServiceRequestEventArgs::<Endpoint>k__BackingField
	Uri_t19570940 * ___U3CEndpointU3Ek__BackingField_4;
	// Amazon.Runtime.AmazonWebServiceRequest Amazon.Runtime.WebServiceRequestEventArgs::<Request>k__BackingField
	AmazonWebServiceRequest_t3384026212 * ___U3CRequestU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CHeadersU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(WebServiceRequestEventArgs_t1089733597, ___U3CHeadersU3Ek__BackingField_1)); }
	inline Il2CppObject* get_U3CHeadersU3Ek__BackingField_1() const { return ___U3CHeadersU3Ek__BackingField_1; }
	inline Il2CppObject** get_address_of_U3CHeadersU3Ek__BackingField_1() { return &___U3CHeadersU3Ek__BackingField_1; }
	inline void set_U3CHeadersU3Ek__BackingField_1(Il2CppObject* value)
	{
		___U3CHeadersU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CHeadersU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CParametersU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(WebServiceRequestEventArgs_t1089733597, ___U3CParametersU3Ek__BackingField_2)); }
	inline Il2CppObject* get_U3CParametersU3Ek__BackingField_2() const { return ___U3CParametersU3Ek__BackingField_2; }
	inline Il2CppObject** get_address_of_U3CParametersU3Ek__BackingField_2() { return &___U3CParametersU3Ek__BackingField_2; }
	inline void set_U3CParametersU3Ek__BackingField_2(Il2CppObject* value)
	{
		___U3CParametersU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CParametersU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CServiceNameU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(WebServiceRequestEventArgs_t1089733597, ___U3CServiceNameU3Ek__BackingField_3)); }
	inline String_t* get_U3CServiceNameU3Ek__BackingField_3() const { return ___U3CServiceNameU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CServiceNameU3Ek__BackingField_3() { return &___U3CServiceNameU3Ek__BackingField_3; }
	inline void set_U3CServiceNameU3Ek__BackingField_3(String_t* value)
	{
		___U3CServiceNameU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CServiceNameU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3CEndpointU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(WebServiceRequestEventArgs_t1089733597, ___U3CEndpointU3Ek__BackingField_4)); }
	inline Uri_t19570940 * get_U3CEndpointU3Ek__BackingField_4() const { return ___U3CEndpointU3Ek__BackingField_4; }
	inline Uri_t19570940 ** get_address_of_U3CEndpointU3Ek__BackingField_4() { return &___U3CEndpointU3Ek__BackingField_4; }
	inline void set_U3CEndpointU3Ek__BackingField_4(Uri_t19570940 * value)
	{
		___U3CEndpointU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CEndpointU3Ek__BackingField_4, value);
	}

	inline static int32_t get_offset_of_U3CRequestU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(WebServiceRequestEventArgs_t1089733597, ___U3CRequestU3Ek__BackingField_5)); }
	inline AmazonWebServiceRequest_t3384026212 * get_U3CRequestU3Ek__BackingField_5() const { return ___U3CRequestU3Ek__BackingField_5; }
	inline AmazonWebServiceRequest_t3384026212 ** get_address_of_U3CRequestU3Ek__BackingField_5() { return &___U3CRequestU3Ek__BackingField_5; }
	inline void set_U3CRequestU3Ek__BackingField_5(AmazonWebServiceRequest_t3384026212 * value)
	{
		___U3CRequestU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CRequestU3Ek__BackingField_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
