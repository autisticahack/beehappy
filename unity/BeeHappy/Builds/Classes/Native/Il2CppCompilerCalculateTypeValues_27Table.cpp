﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Model792472136.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Model712152635.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Model932830458.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Mode2302678766.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Mode4078561340.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Mode2091118072.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Mode2698079901.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Model955597635.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Mode1116634264.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Model_50815546.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Mode2482528107.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Mode3250699605.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Model457234085.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Mode4018382715.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Model403010890.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Mode3276202790.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Mode3086492122.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Mode3584137889.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Mode2007495598.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Mode3365653223.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Mode2243065850.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Mode1236206204.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Mode3130322723.h"
#include "Mono_Data_Sqlite_U3CModuleU3E3783534214.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SQLite3817015169.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SQLite3_UTF163316796172.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SQLiteBase2015643195.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SQLiteOpenFlagsE1776899264.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteCommandBui1402779198.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteCommand788407733.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteConnection1726730022.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_UpdateEventArgs978982032.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_CommitEventArgs134745060.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteConnection2583492534.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteConnection3179814164.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteConvert2634749803.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_TypeAffinity326266980.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SQLiteDateFormat3821473988.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SQLiteType3850755444.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SQLiteTypeNames2939892384.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteDataAdapte3827703907.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteDataReader477797653.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SQLiteEnlistment3046029297.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteException1130998045.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SQLiteErrorCode3937847087.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteFactory27939560.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteFunctionAttri2332230.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteFunction2830561746.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteFunction_A1049664947.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteFunctionEx3223060411.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_FunctionType2476427920.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteKeyReader2769465850.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteKeyReader_K777769675.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteKeyReader_1349310091.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteParameterC4204843499.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteParameter354437343.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteStatement4106757957.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteTransactio2796588764.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SR2207075715.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_UnsafeNativeMeth3272507145.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteConnection4248178292.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteStatementH3796671787.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SQLiteUpdateCall4116372060.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SQLiteCommitCall2277160688.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SQLiteRollbackCal380854181.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SQLiteCommitHand2608225979.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SQLiteUpdateEvent857020267.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SQLiteCallback2492596833.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SQLiteFinalCallb3453741687.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SQLiteCollation4048482007.h"
#include "Mono_Data_Sqlite_U3CPrivateImplementationDetailsU31486305137.h"
#include "Mono_Data_Sqlite_U3CPrivateImplementationDetailsU31278837972.h"
#include "Mono_Data_Sqlite_U3CPrivateImplementationDetailsU31957337327.h"
#include "AWSSDK_CognitoSync_U3CModuleU3E3783534214.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_AmazonCognit1479139240.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_AmazonCognit2119632619.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_Operation3836154307.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_AmazonCognit2140463921.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_AmazonCognit3423722813.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_Model_Dataset149289424.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_Model_Delete3672322944.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_Model_Delete2927760190.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_Model_Intern1047494334.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_Model_Invalid711981474.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_Model_Invali1425711985.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_Model_Lambda2625119802.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_Model_LimitE4017622043.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_Model_ListRe3074726983.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_Model_ListRe1441155223.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_Model_NotAut1831495561.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_Model_Record441586865.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_Model_RecordPa97905615.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_Model_Resour3114547969.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_Model_Resource26593744.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_Model_TooMan3785917292.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_Model_UpdateR197801344.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_Model_Update1893058994.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_Model_Intern3862241748.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2700 = { sizeof (Credentials_t792472136), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2700[5] = 
{
	Credentials_t792472136::get_offset_of__credentials_0(),
	Credentials_t792472136::get_offset_of__accessKeyId_1(),
	Credentials_t792472136::get_offset_of__expiration_2(),
	Credentials_t792472136::get_offset_of__secretKey_3(),
	Credentials_t792472136::get_offset_of__sessionToken_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2701 = { sizeof (ExternalServiceException_t712152635), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2702 = { sizeof (GetCredentialsForIdentityRequest_t932830458), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2702[3] = 
{
	GetCredentialsForIdentityRequest_t932830458::get_offset_of__customRoleArn_3(),
	GetCredentialsForIdentityRequest_t932830458::get_offset_of__identityId_4(),
	GetCredentialsForIdentityRequest_t932830458::get_offset_of__logins_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2703 = { sizeof (GetCredentialsForIdentityResponse_t2302678766), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2703[2] = 
{
	GetCredentialsForIdentityResponse_t2302678766::get_offset_of__credentials_3(),
	GetCredentialsForIdentityResponse_t2302678766::get_offset_of__identityId_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2704 = { sizeof (GetIdRequest_t4078561340), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2704[3] = 
{
	GetIdRequest_t4078561340::get_offset_of__accountId_3(),
	GetIdRequest_t4078561340::get_offset_of__identityPoolId_4(),
	GetIdRequest_t4078561340::get_offset_of__logins_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2705 = { sizeof (GetIdResponse_t2091118072), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2705[1] = 
{
	GetIdResponse_t2091118072::get_offset_of__identityId_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2706 = { sizeof (GetOpenIdTokenRequest_t2698079901), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2706[2] = 
{
	GetOpenIdTokenRequest_t2698079901::get_offset_of__identityId_3(),
	GetOpenIdTokenRequest_t2698079901::get_offset_of__logins_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2707 = { sizeof (GetOpenIdTokenResponse_t955597635), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2707[2] = 
{
	GetOpenIdTokenResponse_t955597635::get_offset_of__identityId_3(),
	GetOpenIdTokenResponse_t955597635::get_offset_of__token_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2708 = { sizeof (InternalErrorException_t1116634264), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2709 = { sizeof (InvalidIdentityPoolConfigurationException_t50815546), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2710 = { sizeof (InvalidParameterException_t2482528107), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2711 = { sizeof (LimitExceededException_t3250699605), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2712 = { sizeof (NotAuthorizedException_t457234085), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2713 = { sizeof (ResourceConflictException_t4018382715), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2714 = { sizeof (ResourceNotFoundException_t403010890), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2715 = { sizeof (TooManyRequestsException_t3276202790), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2716 = { sizeof (CredentialsUnmarshaller_t3086492122), -1, sizeof(CredentialsUnmarshaller_t3086492122_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2716[1] = 
{
	CredentialsUnmarshaller_t3086492122_StaticFields::get_offset_of__instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2717 = { sizeof (GetCredentialsForIdentityRequestMarshaller_t3584137889), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2718 = { sizeof (GetCredentialsForIdentityResponseUnmarshaller_t2007495598), -1, sizeof(GetCredentialsForIdentityResponseUnmarshaller_t2007495598_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2718[1] = 
{
	GetCredentialsForIdentityResponseUnmarshaller_t2007495598_StaticFields::get_offset_of__instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2719 = { sizeof (GetIdRequestMarshaller_t3365653223), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2720 = { sizeof (GetIdResponseUnmarshaller_t2243065850), -1, sizeof(GetIdResponseUnmarshaller_t2243065850_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2720[1] = 
{
	GetIdResponseUnmarshaller_t2243065850_StaticFields::get_offset_of__instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2721 = { sizeof (GetOpenIdTokenRequestMarshaller_t1236206204), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2722 = { sizeof (GetOpenIdTokenResponseUnmarshaller_t3130322723), -1, sizeof(GetOpenIdTokenResponseUnmarshaller_t3130322723_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2722[1] = 
{
	GetOpenIdTokenResponseUnmarshaller_t3130322723_StaticFields::get_offset_of__instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2723 = { sizeof (U3CModuleU3E_t3783534227), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2724 = { sizeof (SQLite3_t817015169), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2724[6] = 
{
	SQLite3_t817015169::get_offset_of__sql_14(),
	SQLite3_t817015169::get_offset_of__fileName_15(),
	SQLite3_t817015169::get_offset_of__usePool_16(),
	SQLite3_t817015169::get_offset_of__poolVersion_17(),
	SQLite3_t817015169::get_offset_of__buildingSchema_18(),
	SQLite3_t817015169::get_offset_of__functionsArray_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2725 = { sizeof (SQLite3_UTF16_t3316796172), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2726 = { sizeof (SQLiteBase_t2015643195), -1, sizeof(SQLiteBase_t2015643195_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2726[1] = 
{
	SQLiteBase_t2015643195_StaticFields::get_offset_of__lock_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2727 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2728 = { sizeof (SQLiteOpenFlagsEnum_t1776899264)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2728[7] = 
{
	SQLiteOpenFlagsEnum_t1776899264::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2729 = { sizeof (SqliteCommandBuilder_t1402779198), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2730 = { sizeof (SqliteCommand_t788407733), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2730[11] = 
{
	SqliteCommand_t788407733::get_offset_of__commandText_4(),
	SqliteCommand_t788407733::get_offset_of__cnn_5(),
	SqliteCommand_t788407733::get_offset_of__version_6(),
	SqliteCommand_t788407733::get_offset_of__activeReader_7(),
	SqliteCommand_t788407733::get_offset_of__commandTimeout_8(),
	SqliteCommand_t788407733::get_offset_of__designTimeVisible_9(),
	SqliteCommand_t788407733::get_offset_of__updateRowSource_10(),
	SqliteCommand_t788407733::get_offset_of__parameterCollection_11(),
	SqliteCommand_t788407733::get_offset_of__statementList_12(),
	SqliteCommand_t788407733::get_offset_of__remainingText_13(),
	SqliteCommand_t788407733::get_offset_of__transaction_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2731 = { sizeof (SqliteConnection_t1726730022), -1, sizeof(SqliteConnection_t1726730022_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2731[20] = 
{
	SqliteConnection_t1726730022::get_offset_of__connectionState_5(),
	SqliteConnection_t1726730022::get_offset_of__connectionString_6(),
	SqliteConnection_t1726730022::get_offset_of__transactionLevel_7(),
	SqliteConnection_t1726730022::get_offset_of__defaultIsolation_8(),
	SqliteConnection_t1726730022::get_offset_of__enlistment_9(),
	SqliteConnection_t1726730022::get_offset_of__sql_10(),
	SqliteConnection_t1726730022::get_offset_of__dataSource_11(),
	SqliteConnection_t1726730022::get_offset_of__password_12(),
	SqliteConnection_t1726730022::get_offset_of__defaultTimeout_13(),
	SqliteConnection_t1726730022::get_offset_of__binaryGuid_14(),
	SqliteConnection_t1726730022::get_offset_of__version_15(),
	SqliteConnection_t1726730022::get_offset_of__updateCallback_16(),
	SqliteConnection_t1726730022::get_offset_of__commitCallback_17(),
	SqliteConnection_t1726730022::get_offset_of__rollbackCallback_18(),
	SqliteConnection_t1726730022::get_offset_of__updateHandler_19(),
	SqliteConnection_t1726730022::get_offset_of__commitHandler_20(),
	SqliteConnection_t1726730022::get_offset_of__rollbackHandler_21(),
	SqliteConnection_t1726730022::get_offset_of_StateChange_22(),
	SqliteConnection_t1726730022_StaticFields::get_offset_of_U3CU3Ef__switchU24map1_23(),
	SqliteConnection_t1726730022_StaticFields::get_offset_of_U3CU3Ef__switchU24map2_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2732 = { sizeof (UpdateEventArgs_t978982032), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2733 = { sizeof (CommitEventArgs_t134745060), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2734 = { sizeof (SqliteConnectionPool_t2583492534), -1, sizeof(SqliteConnectionPool_t2583492534_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2734[2] = 
{
	SqliteConnectionPool_t2583492534_StaticFields::get_offset_of__connections_0(),
	SqliteConnectionPool_t2583492534_StaticFields::get_offset_of__poolVersion_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2735 = { sizeof (Pool_t3179814164), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2735[3] = 
{
	Pool_t3179814164::get_offset_of_Queue_0(),
	Pool_t3179814164::get_offset_of_PoolVersion_1(),
	Pool_t3179814164::get_offset_of_MaxPoolSize_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2736 = { sizeof (SqliteConvert_t2634749803), -1, sizeof(SqliteConvert_t2634749803_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2736[13] = 
{
	SqliteConvert_t2634749803_StaticFields::get_offset_of__datetimeFormats_0(),
	SqliteConvert_t2634749803_StaticFields::get_offset_of__utf8_1(),
	SqliteConvert_t2634749803::get_offset_of__datetimeFormat_2(),
	SqliteConvert_t2634749803_StaticFields::get_offset_of__affinitytotype_3(),
	SqliteConvert_t2634749803_StaticFields::get_offset_of__typetodbtype_4(),
	SqliteConvert_t2634749803_StaticFields::get_offset_of__dbtypetocolumnsize_5(),
	SqliteConvert_t2634749803_StaticFields::get_offset_of__dbtypetonumericprecision_6(),
	SqliteConvert_t2634749803_StaticFields::get_offset_of__dbtypetonumericscale_7(),
	SqliteConvert_t2634749803_StaticFields::get_offset_of__dbtypeNames_8(),
	SqliteConvert_t2634749803_StaticFields::get_offset_of__dbtypeToType_9(),
	SqliteConvert_t2634749803_StaticFields::get_offset_of__typecodeAffinities_10(),
	SqliteConvert_t2634749803_StaticFields::get_offset_of__typeNames_11(),
	SqliteConvert_t2634749803_StaticFields::get_offset_of_U3CU3Ef__switchU24map0_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2737 = { sizeof (TypeAffinity_t326266980)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2737[9] = 
{
	TypeAffinity_t326266980::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2738 = { sizeof (SQLiteDateFormats_t3821473988)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2738[4] = 
{
	SQLiteDateFormats_t3821473988::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2739 = { sizeof (SQLiteType_t3850755444), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2739[2] = 
{
	SQLiteType_t3850755444::get_offset_of_Type_0(),
	SQLiteType_t3850755444::get_offset_of_Affinity_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2740 = { sizeof (SQLiteTypeNames_t2939892384)+ sizeof (Il2CppObject), sizeof(SQLiteTypeNames_t2939892384_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2740[2] = 
{
	SQLiteTypeNames_t2939892384::get_offset_of_typeName_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SQLiteTypeNames_t2939892384::get_offset_of_dataType_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2741 = { sizeof (SqliteDataAdapter_t3827703907), -1, sizeof(SqliteDataAdapter_t3827703907_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2741[2] = 
{
	SqliteDataAdapter_t3827703907_StaticFields::get_offset_of__updatingEventPH_18(),
	SqliteDataAdapter_t3827703907_StaticFields::get_offset_of__updatedEventPH_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2742 = { sizeof (SqliteDataReader_t477797653), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2742[11] = 
{
	SqliteDataReader_t477797653::get_offset_of__command_1(),
	SqliteDataReader_t477797653::get_offset_of__activeStatementIndex_2(),
	SqliteDataReader_t477797653::get_offset_of__activeStatement_3(),
	SqliteDataReader_t477797653::get_offset_of__readingState_4(),
	SqliteDataReader_t477797653::get_offset_of__rowsAffected_5(),
	SqliteDataReader_t477797653::get_offset_of__fieldCount_6(),
	SqliteDataReader_t477797653::get_offset_of__fieldTypeArray_7(),
	SqliteDataReader_t477797653::get_offset_of__commandBehavior_8(),
	SqliteDataReader_t477797653::get_offset_of__disposeCommand_9(),
	SqliteDataReader_t477797653::get_offset_of__keyInfo_10(),
	SqliteDataReader_t477797653::get_offset_of__version_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2743 = { sizeof (SQLiteEnlistment_t3046029297), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2743[3] = 
{
	SQLiteEnlistment_t3046029297::get_offset_of__transaction_0(),
	SQLiteEnlistment_t3046029297::get_offset_of__scope_1(),
	SQLiteEnlistment_t3046029297::get_offset_of__disposeConnection_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2744 = { sizeof (SqliteException_t1130998045), -1, sizeof(SqliteException_t1130998045_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2744[2] = 
{
	SqliteException_t1130998045::get_offset_of__errorCode_11(),
	SqliteException_t1130998045_StaticFields::get_offset_of__errorMessages_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2745 = { sizeof (SQLiteErrorCode_t3937847087)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2745[30] = 
{
	SQLiteErrorCode_t3937847087::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2746 = { sizeof (SqliteFactory_t27939560), -1, sizeof(SqliteFactory_t27939560_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2746[3] = 
{
	SqliteFactory_t27939560_StaticFields::get_offset_of_Instance_0(),
	SqliteFactory_t27939560_StaticFields::get_offset_of__dbProviderServicesType_1(),
	SqliteFactory_t27939560_StaticFields::get_offset_of__sqliteServices_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2747 = { sizeof (SqliteFunctionAttribute_t2332230), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2747[4] = 
{
	SqliteFunctionAttribute_t2332230::get_offset_of__name_0(),
	SqliteFunctionAttribute_t2332230::get_offset_of__arguments_1(),
	SqliteFunctionAttribute_t2332230::get_offset_of__functionType_2(),
	SqliteFunctionAttribute_t2332230::get_offset_of__instanceType_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2748 = { sizeof (SqliteFunction_t2830561746), -1, sizeof(SqliteFunction_t2830561746_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2748[9] = 
{
	SqliteFunction_t2830561746::get_offset_of__base_0(),
	SqliteFunction_t2830561746::get_offset_of__contextDataList_1(),
	SqliteFunction_t2830561746::get_offset_of__InvokeFunc_2(),
	SqliteFunction_t2830561746::get_offset_of__StepFunc_3(),
	SqliteFunction_t2830561746::get_offset_of__FinalFunc_4(),
	SqliteFunction_t2830561746::get_offset_of__CompareFunc_5(),
	SqliteFunction_t2830561746::get_offset_of__CompareFunc16_6(),
	SqliteFunction_t2830561746::get_offset_of__context_7(),
	SqliteFunction_t2830561746_StaticFields::get_offset_of__registeredFunctions_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2749 = { sizeof (AggregateData_t1049664947), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2749[2] = 
{
	AggregateData_t1049664947::get_offset_of__count_0(),
	AggregateData_t1049664947::get_offset_of__data_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2750 = { sizeof (SqliteFunctionEx_t3223060411), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2751 = { sizeof (FunctionType_t2476427920)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2751[4] = 
{
	FunctionType_t2476427920::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2752 = { sizeof (SqliteKeyReader_t2769465850), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2752[3] = 
{
	SqliteKeyReader_t2769465850::get_offset_of__keyInfo_0(),
	SqliteKeyReader_t2769465850::get_offset_of__stmt_1(),
	SqliteKeyReader_t2769465850::get_offset_of__isValid_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2753 = { sizeof (KeyInfo_t777769675)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2753[8] = 
{
	KeyInfo_t777769675::get_offset_of_databaseName_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	KeyInfo_t777769675::get_offset_of_tableName_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	KeyInfo_t777769675::get_offset_of_columnName_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	KeyInfo_t777769675::get_offset_of_database_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	KeyInfo_t777769675::get_offset_of_rootPage_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	KeyInfo_t777769675::get_offset_of_cursor_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	KeyInfo_t777769675::get_offset_of_query_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	KeyInfo_t777769675::get_offset_of_column_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2754 = { sizeof (KeyQuery_t1349310091), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2754[2] = 
{
	KeyQuery_t1349310091::get_offset_of__command_0(),
	KeyQuery_t1349310091::get_offset_of__reader_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2755 = { sizeof (SqliteParameterCollection_t4204843499), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2755[3] = 
{
	SqliteParameterCollection_t4204843499::get_offset_of__command_1(),
	SqliteParameterCollection_t4204843499::get_offset_of__parameterList_2(),
	SqliteParameterCollection_t4204843499::get_offset_of__unboundFlag_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2756 = { sizeof (SqliteParameter_t354437343), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2756[8] = 
{
	SqliteParameter_t354437343::get_offset_of__dbType_1(),
	SqliteParameter_t354437343::get_offset_of__rowVersion_2(),
	SqliteParameter_t354437343::get_offset_of__objValue_3(),
	SqliteParameter_t354437343::get_offset_of__sourceColumn_4(),
	SqliteParameter_t354437343::get_offset_of__parameterName_5(),
	SqliteParameter_t354437343::get_offset_of__dataSize_6(),
	SqliteParameter_t354437343::get_offset_of__nullable_7(),
	SqliteParameter_t354437343::get_offset_of__nullMapping_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2757 = { sizeof (SqliteStatement_t4106757957), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2757[8] = 
{
	SqliteStatement_t4106757957::get_offset_of__sql_0(),
	SqliteStatement_t4106757957::get_offset_of__sqlStatement_1(),
	SqliteStatement_t4106757957::get_offset_of__sqlite_stmt_2(),
	SqliteStatement_t4106757957::get_offset_of__unnamedParameters_3(),
	SqliteStatement_t4106757957::get_offset_of__paramNames_4(),
	SqliteStatement_t4106757957::get_offset_of__paramValues_5(),
	SqliteStatement_t4106757957::get_offset_of__command_6(),
	SqliteStatement_t4106757957::get_offset_of__types_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2758 = { sizeof (SqliteTransaction_t2796588764), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2758[3] = 
{
	SqliteTransaction_t2796588764::get_offset_of__cnn_1(),
	SqliteTransaction_t2796588764::get_offset_of__version_2(),
	SqliteTransaction_t2796588764::get_offset_of__level_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2759 = { sizeof (SR_t2207075715), -1, sizeof(SR_t2207075715_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2759[2] = 
{
	SR_t2207075715_StaticFields::get_offset_of_resourceMan_0(),
	SR_t2207075715_StaticFields::get_offset_of_resourceCulture_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2760 = { sizeof (UnsafeNativeMethods_t3272507145), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2761 = { sizeof (SqliteConnectionHandle_t4248178292), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2762 = { sizeof (SqliteStatementHandle_t3796671787), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2763 = { sizeof (SQLiteUpdateCallback_t4116372060), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2764 = { sizeof (SQLiteCommitCallback_t2277160688), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2765 = { sizeof (SQLiteRollbackCallback_t380854181), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2766 = { sizeof (SQLiteCommitHandler_t2608225979), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2767 = { sizeof (SQLiteUpdateEventHandler_t857020267), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2768 = { sizeof (SQLiteCallback_t2492596833), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2769 = { sizeof (SQLiteFinalCallback_t3453741687), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2770 = { sizeof (SQLiteCollation_t4048482007), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2771 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305145), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2771[2] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24U24fieldU2D0_0(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24U24fieldU2D1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2772 = { sizeof (U24ArrayTypeU24104_t1278837972)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24104_t1278837972 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2773 = { sizeof (U24ArrayTypeU248_t1957337329)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU248_t1957337329 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2774 = { sizeof (U3CModuleU3E_t3783534228), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2775 = { sizeof (AmazonCognitoSyncConfig_t1479139240), -1, sizeof(AmazonCognitoSyncConfig_t1479139240_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2775[2] = 
{
	AmazonCognitoSyncConfig_t1479139240_StaticFields::get_offset_of_UserAgentString_21(),
	AmazonCognitoSyncConfig_t1479139240::get_offset_of__userAgent_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2776 = { sizeof (AmazonCognitoSyncException_t2119632619), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2777 = { sizeof (Operation_t3836154307), -1, sizeof(Operation_t3836154307_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2777[2] = 
{
	Operation_t3836154307_StaticFields::get_offset_of_Remove_3(),
	Operation_t3836154307_StaticFields::get_offset_of_Replace_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2778 = { sizeof (AmazonCognitoSyncRequest_t2140463921), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2779 = { sizeof (AmazonCognitoSyncClient_t3423722813), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2780 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2781 = { sizeof (Dataset_t149289424), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2781[7] = 
{
	Dataset_t149289424::get_offset_of__creationDate_0(),
	Dataset_t149289424::get_offset_of__datasetName_1(),
	Dataset_t149289424::get_offset_of__dataStorage_2(),
	Dataset_t149289424::get_offset_of__identityId_3(),
	Dataset_t149289424::get_offset_of__lastModifiedBy_4(),
	Dataset_t149289424::get_offset_of__lastModifiedDate_5(),
	Dataset_t149289424::get_offset_of__numRecords_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2782 = { sizeof (DeleteDatasetRequest_t3672322944), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2782[3] = 
{
	DeleteDatasetRequest_t3672322944::get_offset_of__datasetName_3(),
	DeleteDatasetRequest_t3672322944::get_offset_of__identityId_4(),
	DeleteDatasetRequest_t3672322944::get_offset_of__identityPoolId_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2783 = { sizeof (DeleteDatasetResponse_t2927760190), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2783[1] = 
{
	DeleteDatasetResponse_t2927760190::get_offset_of__dataset_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2784 = { sizeof (InternalErrorException_t1047494334), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2785 = { sizeof (InvalidLambdaFunctionOutputException_t711981474), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2786 = { sizeof (InvalidParameterException_t1425711985), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2787 = { sizeof (LambdaThrottledException_t2625119802), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2788 = { sizeof (LimitExceededException_t4017622043), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2789 = { sizeof (ListRecordsRequest_t3074726983), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2789[7] = 
{
	ListRecordsRequest_t3074726983::get_offset_of__datasetName_3(),
	ListRecordsRequest_t3074726983::get_offset_of__identityId_4(),
	ListRecordsRequest_t3074726983::get_offset_of__identityPoolId_5(),
	ListRecordsRequest_t3074726983::get_offset_of__lastSyncCount_6(),
	ListRecordsRequest_t3074726983::get_offset_of__maxResults_7(),
	ListRecordsRequest_t3074726983::get_offset_of__nextToken_8(),
	ListRecordsRequest_t3074726983::get_offset_of__syncSessionToken_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2790 = { sizeof (ListRecordsResponse_t1441155223), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2790[9] = 
{
	ListRecordsResponse_t1441155223::get_offset_of__count_3(),
	ListRecordsResponse_t1441155223::get_offset_of__datasetDeletedAfterRequestedSyncCount_4(),
	ListRecordsResponse_t1441155223::get_offset_of__datasetExists_5(),
	ListRecordsResponse_t1441155223::get_offset_of__datasetSyncCount_6(),
	ListRecordsResponse_t1441155223::get_offset_of__lastModifiedBy_7(),
	ListRecordsResponse_t1441155223::get_offset_of__mergedDatasetNames_8(),
	ListRecordsResponse_t1441155223::get_offset_of__nextToken_9(),
	ListRecordsResponse_t1441155223::get_offset_of__records_10(),
	ListRecordsResponse_t1441155223::get_offset_of__syncSessionToken_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2791 = { sizeof (NotAuthorizedException_t1831495561), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2792 = { sizeof (Record_t441586865), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2792[6] = 
{
	Record_t441586865::get_offset_of__deviceLastModifiedDate_0(),
	Record_t441586865::get_offset_of__key_1(),
	Record_t441586865::get_offset_of__lastModifiedBy_2(),
	Record_t441586865::get_offset_of__lastModifiedDate_3(),
	Record_t441586865::get_offset_of__syncCount_4(),
	Record_t441586865::get_offset_of__value_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2793 = { sizeof (RecordPatch_t97905615), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2793[5] = 
{
	RecordPatch_t97905615::get_offset_of__deviceLastModifiedDate_0(),
	RecordPatch_t97905615::get_offset_of__key_1(),
	RecordPatch_t97905615::get_offset_of__op_2(),
	RecordPatch_t97905615::get_offset_of__syncCount_3(),
	RecordPatch_t97905615::get_offset_of__value_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2794 = { sizeof (ResourceConflictException_t3114547969), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2795 = { sizeof (ResourceNotFoundException_t26593744), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2796 = { sizeof (TooManyRequestsException_t3785917292), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2797 = { sizeof (UpdateRecordsRequest_t197801344), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2797[7] = 
{
	UpdateRecordsRequest_t197801344::get_offset_of__clientContext_3(),
	UpdateRecordsRequest_t197801344::get_offset_of__datasetName_4(),
	UpdateRecordsRequest_t197801344::get_offset_of__deviceId_5(),
	UpdateRecordsRequest_t197801344::get_offset_of__identityId_6(),
	UpdateRecordsRequest_t197801344::get_offset_of__identityPoolId_7(),
	UpdateRecordsRequest_t197801344::get_offset_of__recordPatches_8(),
	UpdateRecordsRequest_t197801344::get_offset_of__syncSessionToken_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2798 = { sizeof (UpdateRecordsResponse_t1893058994), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2798[1] = 
{
	UpdateRecordsResponse_t1893058994::get_offset_of__records_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2799 = { sizeof (DatasetUnmarshaller_t3862241748), -1, sizeof(DatasetUnmarshaller_t3862241748_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2799[1] = 
{
	DatasetUnmarshaller_t3862241748_StaticFields::get_offset_of__instance_0(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
