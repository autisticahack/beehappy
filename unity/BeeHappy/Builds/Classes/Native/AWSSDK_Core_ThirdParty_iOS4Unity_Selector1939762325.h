﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.iOS4Unity.Selector
struct  Selector_t1939762325  : public Il2CppObject
{
public:
	// System.IntPtr ThirdParty.iOS4Unity.Selector::handle
	IntPtr_t ___handle_15;

public:
	inline static int32_t get_offset_of_handle_15() { return static_cast<int32_t>(offsetof(Selector_t1939762325, ___handle_15)); }
	inline IntPtr_t get_handle_15() const { return ___handle_15; }
	inline IntPtr_t* get_address_of_handle_15() { return &___handle_15; }
	inline void set_handle_15(IntPtr_t value)
	{
		___handle_15 = value;
	}
};

struct Selector_t1939762325_StaticFields
{
public:
	// System.IntPtr ThirdParty.iOS4Unity.Selector::Init
	IntPtr_t ___Init_0;
	// System.IntPtr ThirdParty.iOS4Unity.Selector::InitWithCoder
	IntPtr_t ___InitWithCoder_1;
	// System.IntPtr ThirdParty.iOS4Unity.Selector::InitWithName
	IntPtr_t ___InitWithName_2;
	// System.IntPtr ThirdParty.iOS4Unity.Selector::InitWithFrame
	IntPtr_t ___InitWithFrame_3;
	// System.IntPtr ThirdParty.iOS4Unity.Selector::MethodSignatureForSelector
	IntPtr_t ___MethodSignatureForSelector_4;
	// System.IntPtr ThirdParty.iOS4Unity.Selector::FrameLength
	IntPtr_t ___FrameLength_5;
	// System.IntPtr ThirdParty.iOS4Unity.Selector::RetainCount
	IntPtr_t ___RetainCount_6;
	// System.IntPtr ThirdParty.iOS4Unity.Selector::AllocHandle
	IntPtr_t ___AllocHandle_7;
	// System.IntPtr ThirdParty.iOS4Unity.Selector::ReleaseHandle
	IntPtr_t ___ReleaseHandle_8;
	// System.IntPtr ThirdParty.iOS4Unity.Selector::RetainHandle
	IntPtr_t ___RetainHandle_9;
	// System.IntPtr ThirdParty.iOS4Unity.Selector::AutoreleaseHandle
	IntPtr_t ___AutoreleaseHandle_10;
	// System.IntPtr ThirdParty.iOS4Unity.Selector::DoesNotRecognizeSelectorHandle
	IntPtr_t ___DoesNotRecognizeSelectorHandle_11;
	// System.IntPtr ThirdParty.iOS4Unity.Selector::PerformSelectorOnMainThreadWithObjectWaitUntilDoneHandle
	IntPtr_t ___PerformSelectorOnMainThreadWithObjectWaitUntilDoneHandle_12;
	// System.IntPtr ThirdParty.iOS4Unity.Selector::PerformSelectorWithObjectAfterDelayHandle
	IntPtr_t ___PerformSelectorWithObjectAfterDelayHandle_13;
	// System.IntPtr ThirdParty.iOS4Unity.Selector::UTF8StringHandle
	IntPtr_t ___UTF8StringHandle_14;

public:
	inline static int32_t get_offset_of_Init_0() { return static_cast<int32_t>(offsetof(Selector_t1939762325_StaticFields, ___Init_0)); }
	inline IntPtr_t get_Init_0() const { return ___Init_0; }
	inline IntPtr_t* get_address_of_Init_0() { return &___Init_0; }
	inline void set_Init_0(IntPtr_t value)
	{
		___Init_0 = value;
	}

	inline static int32_t get_offset_of_InitWithCoder_1() { return static_cast<int32_t>(offsetof(Selector_t1939762325_StaticFields, ___InitWithCoder_1)); }
	inline IntPtr_t get_InitWithCoder_1() const { return ___InitWithCoder_1; }
	inline IntPtr_t* get_address_of_InitWithCoder_1() { return &___InitWithCoder_1; }
	inline void set_InitWithCoder_1(IntPtr_t value)
	{
		___InitWithCoder_1 = value;
	}

	inline static int32_t get_offset_of_InitWithName_2() { return static_cast<int32_t>(offsetof(Selector_t1939762325_StaticFields, ___InitWithName_2)); }
	inline IntPtr_t get_InitWithName_2() const { return ___InitWithName_2; }
	inline IntPtr_t* get_address_of_InitWithName_2() { return &___InitWithName_2; }
	inline void set_InitWithName_2(IntPtr_t value)
	{
		___InitWithName_2 = value;
	}

	inline static int32_t get_offset_of_InitWithFrame_3() { return static_cast<int32_t>(offsetof(Selector_t1939762325_StaticFields, ___InitWithFrame_3)); }
	inline IntPtr_t get_InitWithFrame_3() const { return ___InitWithFrame_3; }
	inline IntPtr_t* get_address_of_InitWithFrame_3() { return &___InitWithFrame_3; }
	inline void set_InitWithFrame_3(IntPtr_t value)
	{
		___InitWithFrame_3 = value;
	}

	inline static int32_t get_offset_of_MethodSignatureForSelector_4() { return static_cast<int32_t>(offsetof(Selector_t1939762325_StaticFields, ___MethodSignatureForSelector_4)); }
	inline IntPtr_t get_MethodSignatureForSelector_4() const { return ___MethodSignatureForSelector_4; }
	inline IntPtr_t* get_address_of_MethodSignatureForSelector_4() { return &___MethodSignatureForSelector_4; }
	inline void set_MethodSignatureForSelector_4(IntPtr_t value)
	{
		___MethodSignatureForSelector_4 = value;
	}

	inline static int32_t get_offset_of_FrameLength_5() { return static_cast<int32_t>(offsetof(Selector_t1939762325_StaticFields, ___FrameLength_5)); }
	inline IntPtr_t get_FrameLength_5() const { return ___FrameLength_5; }
	inline IntPtr_t* get_address_of_FrameLength_5() { return &___FrameLength_5; }
	inline void set_FrameLength_5(IntPtr_t value)
	{
		___FrameLength_5 = value;
	}

	inline static int32_t get_offset_of_RetainCount_6() { return static_cast<int32_t>(offsetof(Selector_t1939762325_StaticFields, ___RetainCount_6)); }
	inline IntPtr_t get_RetainCount_6() const { return ___RetainCount_6; }
	inline IntPtr_t* get_address_of_RetainCount_6() { return &___RetainCount_6; }
	inline void set_RetainCount_6(IntPtr_t value)
	{
		___RetainCount_6 = value;
	}

	inline static int32_t get_offset_of_AllocHandle_7() { return static_cast<int32_t>(offsetof(Selector_t1939762325_StaticFields, ___AllocHandle_7)); }
	inline IntPtr_t get_AllocHandle_7() const { return ___AllocHandle_7; }
	inline IntPtr_t* get_address_of_AllocHandle_7() { return &___AllocHandle_7; }
	inline void set_AllocHandle_7(IntPtr_t value)
	{
		___AllocHandle_7 = value;
	}

	inline static int32_t get_offset_of_ReleaseHandle_8() { return static_cast<int32_t>(offsetof(Selector_t1939762325_StaticFields, ___ReleaseHandle_8)); }
	inline IntPtr_t get_ReleaseHandle_8() const { return ___ReleaseHandle_8; }
	inline IntPtr_t* get_address_of_ReleaseHandle_8() { return &___ReleaseHandle_8; }
	inline void set_ReleaseHandle_8(IntPtr_t value)
	{
		___ReleaseHandle_8 = value;
	}

	inline static int32_t get_offset_of_RetainHandle_9() { return static_cast<int32_t>(offsetof(Selector_t1939762325_StaticFields, ___RetainHandle_9)); }
	inline IntPtr_t get_RetainHandle_9() const { return ___RetainHandle_9; }
	inline IntPtr_t* get_address_of_RetainHandle_9() { return &___RetainHandle_9; }
	inline void set_RetainHandle_9(IntPtr_t value)
	{
		___RetainHandle_9 = value;
	}

	inline static int32_t get_offset_of_AutoreleaseHandle_10() { return static_cast<int32_t>(offsetof(Selector_t1939762325_StaticFields, ___AutoreleaseHandle_10)); }
	inline IntPtr_t get_AutoreleaseHandle_10() const { return ___AutoreleaseHandle_10; }
	inline IntPtr_t* get_address_of_AutoreleaseHandle_10() { return &___AutoreleaseHandle_10; }
	inline void set_AutoreleaseHandle_10(IntPtr_t value)
	{
		___AutoreleaseHandle_10 = value;
	}

	inline static int32_t get_offset_of_DoesNotRecognizeSelectorHandle_11() { return static_cast<int32_t>(offsetof(Selector_t1939762325_StaticFields, ___DoesNotRecognizeSelectorHandle_11)); }
	inline IntPtr_t get_DoesNotRecognizeSelectorHandle_11() const { return ___DoesNotRecognizeSelectorHandle_11; }
	inline IntPtr_t* get_address_of_DoesNotRecognizeSelectorHandle_11() { return &___DoesNotRecognizeSelectorHandle_11; }
	inline void set_DoesNotRecognizeSelectorHandle_11(IntPtr_t value)
	{
		___DoesNotRecognizeSelectorHandle_11 = value;
	}

	inline static int32_t get_offset_of_PerformSelectorOnMainThreadWithObjectWaitUntilDoneHandle_12() { return static_cast<int32_t>(offsetof(Selector_t1939762325_StaticFields, ___PerformSelectorOnMainThreadWithObjectWaitUntilDoneHandle_12)); }
	inline IntPtr_t get_PerformSelectorOnMainThreadWithObjectWaitUntilDoneHandle_12() const { return ___PerformSelectorOnMainThreadWithObjectWaitUntilDoneHandle_12; }
	inline IntPtr_t* get_address_of_PerformSelectorOnMainThreadWithObjectWaitUntilDoneHandle_12() { return &___PerformSelectorOnMainThreadWithObjectWaitUntilDoneHandle_12; }
	inline void set_PerformSelectorOnMainThreadWithObjectWaitUntilDoneHandle_12(IntPtr_t value)
	{
		___PerformSelectorOnMainThreadWithObjectWaitUntilDoneHandle_12 = value;
	}

	inline static int32_t get_offset_of_PerformSelectorWithObjectAfterDelayHandle_13() { return static_cast<int32_t>(offsetof(Selector_t1939762325_StaticFields, ___PerformSelectorWithObjectAfterDelayHandle_13)); }
	inline IntPtr_t get_PerformSelectorWithObjectAfterDelayHandle_13() const { return ___PerformSelectorWithObjectAfterDelayHandle_13; }
	inline IntPtr_t* get_address_of_PerformSelectorWithObjectAfterDelayHandle_13() { return &___PerformSelectorWithObjectAfterDelayHandle_13; }
	inline void set_PerformSelectorWithObjectAfterDelayHandle_13(IntPtr_t value)
	{
		___PerformSelectorWithObjectAfterDelayHandle_13 = value;
	}

	inline static int32_t get_offset_of_UTF8StringHandle_14() { return static_cast<int32_t>(offsetof(Selector_t1939762325_StaticFields, ___UTF8StringHandle_14)); }
	inline IntPtr_t get_UTF8StringHandle_14() const { return ___UTF8StringHandle_14; }
	inline IntPtr_t* get_address_of_UTF8StringHandle_14() { return &___UTF8StringHandle_14; }
	inline void set_UTF8StringHandle_14(IntPtr_t value)
	{
		___UTF8StringHandle_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
