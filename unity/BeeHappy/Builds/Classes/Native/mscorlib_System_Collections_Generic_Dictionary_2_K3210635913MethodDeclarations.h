﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>
struct KeyCollection_t3210635913;
// System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>
struct Dictionary_2_t727138142;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object[]
struct ObjectU5BU5D_t3614634134;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3416641580.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m547435096_gshared (KeyCollection_t3210635913 * __this, Dictionary_2_t727138142 * ___dictionary0, const MethodInfo* method);
#define KeyCollection__ctor_m547435096(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t3210635913 *, Dictionary_2_t727138142 *, const MethodInfo*))KeyCollection__ctor_m547435096_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1668870622_gshared (KeyCollection_t3210635913 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1668870622(__this, ___item0, method) ((  void (*) (KeyCollection_t3210635913 *, Il2CppObject *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1668870622_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2756193719_gshared (KeyCollection_t3210635913 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2756193719(__this, method) ((  void (*) (KeyCollection_t3210635913 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2756193719_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m833199734_gshared (KeyCollection_t3210635913 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m833199734(__this, ___item0, method) ((  bool (*) (KeyCollection_t3210635913 *, Il2CppObject *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m833199734_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3201040055_gshared (KeyCollection_t3210635913 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3201040055(__this, ___item0, method) ((  bool (*) (KeyCollection_t3210635913 *, Il2CppObject *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3201040055_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1066449161_gshared (KeyCollection_t3210635913 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1066449161(__this, method) ((  Il2CppObject* (*) (KeyCollection_t3210635913 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1066449161_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m3169515245_gshared (KeyCollection_t3210635913 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m3169515245(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t3210635913 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m3169515245_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m4259404390_gshared (KeyCollection_t3210635913 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m4259404390(__this, method) ((  Il2CppObject * (*) (KeyCollection_t3210635913 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m4259404390_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m853124483_gshared (KeyCollection_t3210635913 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m853124483(__this, method) ((  bool (*) (KeyCollection_t3210635913 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m853124483_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1428134417_gshared (KeyCollection_t3210635913 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1428134417(__this, method) ((  bool (*) (KeyCollection_t3210635913 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1428134417_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m2308578909_gshared (KeyCollection_t3210635913 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m2308578909(__this, method) ((  Il2CppObject * (*) (KeyCollection_t3210635913 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m2308578909_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m4000657415_gshared (KeyCollection_t3210635913 * __this, ObjectU5BU5D_t3614634134* ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_CopyTo_m4000657415(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t3210635913 *, ObjectU5BU5D_t3614634134*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m4000657415_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::GetEnumerator()
extern "C"  Enumerator_t3416641580  KeyCollection_GetEnumerator_m1247305548_gshared (KeyCollection_t3210635913 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m1247305548(__this, method) ((  Enumerator_t3416641580  (*) (KeyCollection_t3210635913 *, const MethodInfo*))KeyCollection_GetEnumerator_m1247305548_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m3260694481_gshared (KeyCollection_t3210635913 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m3260694481(__this, method) ((  int32_t (*) (KeyCollection_t3210635913 *, const MethodInfo*))KeyCollection_get_Count_m3260694481_gshared)(__this, method)
