﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Data.Sqlite.SQLiteUpdateCallback
struct SQLiteUpdateCallback_t4116372060;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Mono.Data.Sqlite.SQLiteUpdateCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void SQLiteUpdateCallback__ctor_m1903374555 (SQLiteUpdateCallback_t4116372060 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SQLiteUpdateCallback::Invoke(System.IntPtr,System.Int32,System.IntPtr,System.IntPtr,System.Int64)
extern "C"  void SQLiteUpdateCallback_Invoke_m2980125214 (SQLiteUpdateCallback_t4116372060 * __this, IntPtr_t ___puser0, int32_t ___type1, IntPtr_t ___database2, IntPtr_t ___table3, int64_t ___rowid4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Mono.Data.Sqlite.SQLiteUpdateCallback::BeginInvoke(System.IntPtr,System.Int32,System.IntPtr,System.IntPtr,System.Int64,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * SQLiteUpdateCallback_BeginInvoke_m2815356417 (SQLiteUpdateCallback_t4116372060 * __this, IntPtr_t ___puser0, int32_t ___type1, IntPtr_t ___database2, IntPtr_t ___table3, int64_t ___rowid4, AsyncCallback_t163412349 * ___callback5, Il2CppObject * ___object6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SQLiteUpdateCallback::EndInvoke(System.IAsyncResult)
extern "C"  void SQLiteUpdateCallback_EndInvoke_m2488866105 (SQLiteUpdateCallback_t4116372060 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
