﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.Runtime.Internal.Transform.IWebResponseData
struct IWebResponseData_t3815728244;

#include "mscorlib_System_Exception1927440687.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.HttpErrorResponseException
struct  HttpErrorResponseException_t3191555406  : public Exception_t1927440687
{
public:
	// Amazon.Runtime.Internal.Transform.IWebResponseData Amazon.Runtime.Internal.HttpErrorResponseException::<Response>k__BackingField
	Il2CppObject * ___U3CResponseU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_U3CResponseU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(HttpErrorResponseException_t3191555406, ___U3CResponseU3Ek__BackingField_11)); }
	inline Il2CppObject * get_U3CResponseU3Ek__BackingField_11() const { return ___U3CResponseU3Ek__BackingField_11; }
	inline Il2CppObject ** get_address_of_U3CResponseU3Ek__BackingField_11() { return &___U3CResponseU3Ek__BackingField_11; }
	inline void set_U3CResponseU3Ek__BackingField_11(Il2CppObject * value)
	{
		___U3CResponseU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CResponseU3Ek__BackingField_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
