﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.SecurityToken.Model.Internal.MarshallTransformations.AssumeRoleWithWebIdentityResponseUnmarshaller
struct AssumeRoleWithWebIdentityResponseUnmarshaller_t3698885375;

#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_XmlRe760346204.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.SecurityToken.Model.Internal.MarshallTransformations.AssumeRoleWithWebIdentityResponseUnmarshaller
struct  AssumeRoleWithWebIdentityResponseUnmarshaller_t3698885375  : public XmlResponseUnmarshaller_t760346204
{
public:

public:
};

struct AssumeRoleWithWebIdentityResponseUnmarshaller_t3698885375_StaticFields
{
public:
	// Amazon.SecurityToken.Model.Internal.MarshallTransformations.AssumeRoleWithWebIdentityResponseUnmarshaller Amazon.SecurityToken.Model.Internal.MarshallTransformations.AssumeRoleWithWebIdentityResponseUnmarshaller::_instance
	AssumeRoleWithWebIdentityResponseUnmarshaller_t3698885375 * ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(AssumeRoleWithWebIdentityResponseUnmarshaller_t3698885375_StaticFields, ____instance_0)); }
	inline AssumeRoleWithWebIdentityResponseUnmarshaller_t3698885375 * get__instance_0() const { return ____instance_0; }
	inline AssumeRoleWithWebIdentityResponseUnmarshaller_t3698885375 ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(AssumeRoleWithWebIdentityResponseUnmarshaller_t3698885375 * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier(&____instance_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
