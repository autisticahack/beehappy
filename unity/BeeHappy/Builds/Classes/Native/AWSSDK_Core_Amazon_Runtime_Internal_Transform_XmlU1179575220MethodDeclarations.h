﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext
struct XmlUnmarshallerContext_t1179575220;
// System.Xml.XmlReader
struct XmlReader_t3675626668;
// System.IO.Stream
struct Stream_t3255436806;
// Amazon.Runtime.Internal.Transform.IWebResponseData
struct IWebResponseData_t3815728244;
// System.String
struct String_t;
// System.Collections.Generic.Stack`1<System.String>
struct Stack_1_t3116948387;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_Stream3255436806.h"

// System.Xml.XmlReader Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::get_XmlReader()
extern "C"  XmlReader_t3675626668 * XmlUnmarshallerContext_get_XmlReader_m2579864562 (XmlUnmarshallerContext_t1179575220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::.ctor(System.IO.Stream,System.Boolean,Amazon.Runtime.Internal.Transform.IWebResponseData)
extern "C"  void XmlUnmarshallerContext__ctor_m2742237319 (XmlUnmarshallerContext_t1179575220 * __this, Stream_t3255436806 * ___responseStream0, bool ___maintainResponseBody1, Il2CppObject * ___responseData2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::get_CurrentPath()
extern "C"  String_t* XmlUnmarshallerContext_get_CurrentPath_m2476965402 (XmlUnmarshallerContext_t1179575220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::get_CurrentDepth()
extern "C"  int32_t XmlUnmarshallerContext_get_CurrentDepth_m1865238287 (XmlUnmarshallerContext_t1179575220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::Read()
extern "C"  bool XmlUnmarshallerContext_Read_m2980703512 (XmlUnmarshallerContext_t1179575220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::ReadText()
extern "C"  String_t* XmlUnmarshallerContext_ReadText_m1398543636 (XmlUnmarshallerContext_t1179575220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::get_IsStartElement()
extern "C"  bool XmlUnmarshallerContext_get_IsStartElement_m2401357685 (XmlUnmarshallerContext_t1179575220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::get_IsEndElement()
extern "C"  bool XmlUnmarshallerContext_get_IsEndElement_m3478191890 (XmlUnmarshallerContext_t1179575220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::get_IsStartOfDocument()
extern "C"  bool XmlUnmarshallerContext_get_IsStartOfDocument_m1539538381 (XmlUnmarshallerContext_t1179575220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::get_IsAttribute()
extern "C"  bool XmlUnmarshallerContext_get_IsAttribute_m1726264931 (XmlUnmarshallerContext_t1179575220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::StackToPath(System.Collections.Generic.Stack`1<System.String>)
extern "C"  String_t* XmlUnmarshallerContext_StackToPath_m4168463517 (Il2CppObject * __this /* static, unused */, Stack_1_t3116948387 * ___stack0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::ReadElement()
extern "C"  void XmlUnmarshallerContext_ReadElement_m2398980740 (XmlUnmarshallerContext_t1179575220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::Dispose(System.Boolean)
extern "C"  void XmlUnmarshallerContext_Dispose_m3294144284 (XmlUnmarshallerContext_t1179575220 * __this, bool ___disposing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::.cctor()
extern "C"  void XmlUnmarshallerContext__cctor_m1472551255 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
