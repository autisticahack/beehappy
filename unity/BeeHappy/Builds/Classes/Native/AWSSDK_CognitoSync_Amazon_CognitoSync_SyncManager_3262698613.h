﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AWSSDK_CognitoSync_Amazon_CognitoSync_SyncManager_1234943264.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.CognitoSync.SyncManager.DataConflictException
struct  DataConflictException_t3262698613  : public DataStorageException_t1234943264
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
