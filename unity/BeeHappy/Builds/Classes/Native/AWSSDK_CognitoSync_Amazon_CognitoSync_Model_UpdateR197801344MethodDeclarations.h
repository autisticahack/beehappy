﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.CognitoSync.Model.UpdateRecordsRequest
struct UpdateRecordsRequest_t197801344;
// System.String
struct String_t;
// System.Collections.Generic.List`1<Amazon.CognitoSync.Model.RecordPatch>
struct List_1_t3761994043;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.String Amazon.CognitoSync.Model.UpdateRecordsRequest::get_ClientContext()
extern "C"  String_t* UpdateRecordsRequest_get_ClientContext_m4179831131 (UpdateRecordsRequest_t197801344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.CognitoSync.Model.UpdateRecordsRequest::IsSetClientContext()
extern "C"  bool UpdateRecordsRequest_IsSetClientContext_m4111227847 (UpdateRecordsRequest_t197801344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.CognitoSync.Model.UpdateRecordsRequest::get_DatasetName()
extern "C"  String_t* UpdateRecordsRequest_get_DatasetName_m2632319948 (UpdateRecordsRequest_t197801344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Model.UpdateRecordsRequest::set_DatasetName(System.String)
extern "C"  void UpdateRecordsRequest_set_DatasetName_m818933943 (UpdateRecordsRequest_t197801344 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.CognitoSync.Model.UpdateRecordsRequest::IsSetDatasetName()
extern "C"  bool UpdateRecordsRequest_IsSetDatasetName_m1952768794 (UpdateRecordsRequest_t197801344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.CognitoSync.Model.UpdateRecordsRequest::get_DeviceId()
extern "C"  String_t* UpdateRecordsRequest_get_DeviceId_m3247013120 (UpdateRecordsRequest_t197801344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.CognitoSync.Model.UpdateRecordsRequest::IsSetDeviceId()
extern "C"  bool UpdateRecordsRequest_IsSetDeviceId_m1734150062 (UpdateRecordsRequest_t197801344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.CognitoSync.Model.UpdateRecordsRequest::get_IdentityId()
extern "C"  String_t* UpdateRecordsRequest_get_IdentityId_m2476650404 (UpdateRecordsRequest_t197801344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Model.UpdateRecordsRequest::set_IdentityId(System.String)
extern "C"  void UpdateRecordsRequest_set_IdentityId_m2559102633 (UpdateRecordsRequest_t197801344 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.CognitoSync.Model.UpdateRecordsRequest::IsSetIdentityId()
extern "C"  bool UpdateRecordsRequest_IsSetIdentityId_m3259721230 (UpdateRecordsRequest_t197801344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.CognitoSync.Model.UpdateRecordsRequest::get_IdentityPoolId()
extern "C"  String_t* UpdateRecordsRequest_get_IdentityPoolId_m712134322 (UpdateRecordsRequest_t197801344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Model.UpdateRecordsRequest::set_IdentityPoolId(System.String)
extern "C"  void UpdateRecordsRequest_set_IdentityPoolId_m4132215133 (UpdateRecordsRequest_t197801344 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.CognitoSync.Model.UpdateRecordsRequest::IsSetIdentityPoolId()
extern "C"  bool UpdateRecordsRequest_IsSetIdentityPoolId_m732038656 (UpdateRecordsRequest_t197801344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Amazon.CognitoSync.Model.RecordPatch> Amazon.CognitoSync.Model.UpdateRecordsRequest::get_RecordPatches()
extern "C"  List_1_t3761994043 * UpdateRecordsRequest_get_RecordPatches_m3965023996 (UpdateRecordsRequest_t197801344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Model.UpdateRecordsRequest::set_RecordPatches(System.Collections.Generic.List`1<Amazon.CognitoSync.Model.RecordPatch>)
extern "C"  void UpdateRecordsRequest_set_RecordPatches_m874290675 (UpdateRecordsRequest_t197801344 * __this, List_1_t3761994043 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.CognitoSync.Model.UpdateRecordsRequest::IsSetRecordPatches()
extern "C"  bool UpdateRecordsRequest_IsSetRecordPatches_m756108448 (UpdateRecordsRequest_t197801344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.CognitoSync.Model.UpdateRecordsRequest::get_SyncSessionToken()
extern "C"  String_t* UpdateRecordsRequest_get_SyncSessionToken_m652369963 (UpdateRecordsRequest_t197801344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Model.UpdateRecordsRequest::set_SyncSessionToken(System.String)
extern "C"  void UpdateRecordsRequest_set_SyncSessionToken_m3249105440 (UpdateRecordsRequest_t197801344 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.CognitoSync.Model.UpdateRecordsRequest::IsSetSyncSessionToken()
extern "C"  bool UpdateRecordsRequest_IsSetSyncSessionToken_m2420701911 (UpdateRecordsRequest_t197801344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Model.UpdateRecordsRequest::.ctor()
extern "C"  void UpdateRecordsRequest__ctor_m4022816137 (UpdateRecordsRequest_t197801344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
