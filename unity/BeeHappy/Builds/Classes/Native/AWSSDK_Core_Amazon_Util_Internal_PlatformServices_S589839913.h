﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// System.Collections.Generic.IDictionary`2<System.Type,System.Type>
struct IDictionary_2_t1240244544;
// System.Collections.Generic.IDictionary`2<System.Type,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>
struct IDictionary_2_t1569248696;
// System.Collections.Generic.IDictionary`2<System.Type,System.Object>
struct IDictionary_2_t2625890613;
// Amazon.Util.Internal.PlatformServices.ServiceFactory
struct ServiceFactory_t589839913;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Util.Internal.PlatformServices.ServiceFactory
struct  ServiceFactory_t589839913  : public Il2CppObject
{
public:
	// System.Collections.Generic.IDictionary`2<System.Type,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel> Amazon.Util.Internal.PlatformServices.ServiceFactory::_instantationMappings
	Il2CppObject* ____instantationMappings_3;
	// System.Collections.Generic.IDictionary`2<System.Type,System.Object> Amazon.Util.Internal.PlatformServices.ServiceFactory::_singletonServices
	Il2CppObject* ____singletonServices_4;

public:
	inline static int32_t get_offset_of__instantationMappings_3() { return static_cast<int32_t>(offsetof(ServiceFactory_t589839913, ____instantationMappings_3)); }
	inline Il2CppObject* get__instantationMappings_3() const { return ____instantationMappings_3; }
	inline Il2CppObject** get_address_of__instantationMappings_3() { return &____instantationMappings_3; }
	inline void set__instantationMappings_3(Il2CppObject* value)
	{
		____instantationMappings_3 = value;
		Il2CppCodeGenWriteBarrier(&____instantationMappings_3, value);
	}

	inline static int32_t get_offset_of__singletonServices_4() { return static_cast<int32_t>(offsetof(ServiceFactory_t589839913, ____singletonServices_4)); }
	inline Il2CppObject* get__singletonServices_4() const { return ____singletonServices_4; }
	inline Il2CppObject** get_address_of__singletonServices_4() { return &____singletonServices_4; }
	inline void set__singletonServices_4(Il2CppObject* value)
	{
		____singletonServices_4 = value;
		Il2CppCodeGenWriteBarrier(&____singletonServices_4, value);
	}
};

struct ServiceFactory_t589839913_StaticFields
{
public:
	// System.Object Amazon.Util.Internal.PlatformServices.ServiceFactory::_lock
	Il2CppObject * ____lock_0;
	// System.Boolean Amazon.Util.Internal.PlatformServices.ServiceFactory::_factoryInitialized
	bool ____factoryInitialized_1;
	// System.Collections.Generic.IDictionary`2<System.Type,System.Type> Amazon.Util.Internal.PlatformServices.ServiceFactory::_mappings
	Il2CppObject* ____mappings_2;
	// Amazon.Util.Internal.PlatformServices.ServiceFactory Amazon.Util.Internal.PlatformServices.ServiceFactory::Instance
	ServiceFactory_t589839913 * ___Instance_5;

public:
	inline static int32_t get_offset_of__lock_0() { return static_cast<int32_t>(offsetof(ServiceFactory_t589839913_StaticFields, ____lock_0)); }
	inline Il2CppObject * get__lock_0() const { return ____lock_0; }
	inline Il2CppObject ** get_address_of__lock_0() { return &____lock_0; }
	inline void set__lock_0(Il2CppObject * value)
	{
		____lock_0 = value;
		Il2CppCodeGenWriteBarrier(&____lock_0, value);
	}

	inline static int32_t get_offset_of__factoryInitialized_1() { return static_cast<int32_t>(offsetof(ServiceFactory_t589839913_StaticFields, ____factoryInitialized_1)); }
	inline bool get__factoryInitialized_1() const { return ____factoryInitialized_1; }
	inline bool* get_address_of__factoryInitialized_1() { return &____factoryInitialized_1; }
	inline void set__factoryInitialized_1(bool value)
	{
		____factoryInitialized_1 = value;
	}

	inline static int32_t get_offset_of__mappings_2() { return static_cast<int32_t>(offsetof(ServiceFactory_t589839913_StaticFields, ____mappings_2)); }
	inline Il2CppObject* get__mappings_2() const { return ____mappings_2; }
	inline Il2CppObject** get_address_of__mappings_2() { return &____mappings_2; }
	inline void set__mappings_2(Il2CppObject* value)
	{
		____mappings_2 = value;
		Il2CppCodeGenWriteBarrier(&____mappings_2, value);
	}

	inline static int32_t get_offset_of_Instance_5() { return static_cast<int32_t>(offsetof(ServiceFactory_t589839913_StaticFields, ___Instance_5)); }
	inline ServiceFactory_t589839913 * get_Instance_5() const { return ___Instance_5; }
	inline ServiceFactory_t589839913 ** get_address_of_Instance_5() { return &___Instance_5; }
	inline void set_Instance_5(ServiceFactory_t589839913 * value)
	{
		___Instance_5 = value;
		Il2CppCodeGenWriteBarrier(&___Instance_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
