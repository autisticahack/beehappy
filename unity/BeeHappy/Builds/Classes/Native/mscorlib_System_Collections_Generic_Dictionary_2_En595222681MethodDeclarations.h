﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2544892208MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m79873196(__this, ___dictionary0, method) ((  void (*) (Enumerator_t595222681 *, Dictionary_2_t3570165275 *, const MethodInfo*))Enumerator__ctor_m1253376471_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m4008204539(__this, method) ((  Il2CppObject * (*) (Enumerator_t595222681 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1783562472_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2526536183(__this, method) ((  void (*) (Enumerator_t595222681 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2320340484_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Type,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3302674378(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t595222681 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m283920581_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3961552697(__this, method) ((  Il2CppObject * (*) (Enumerator_t595222681 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2409199898_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1149087793(__this, method) ((  Il2CppObject * (*) (Enumerator_t595222681 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m948336_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Type,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::MoveNext()
#define Enumerator_MoveNext_m754517011(__this, method) ((  bool (*) (Enumerator_t595222681 *, const MethodInfo*))Enumerator_MoveNext_m3850333932_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Type,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::get_Current()
#define Enumerator_get_Current_m3700235707(__this, method) ((  KeyValuePair_2_t1327510497  (*) (Enumerator_t595222681 *, const MethodInfo*))Enumerator_get_Current_m663494932_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Type,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m4271572860(__this, method) ((  Type_t * (*) (Enumerator_t595222681 *, const MethodInfo*))Enumerator_get_CurrentKey_m4133598171_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Type,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m4112181692(__this, method) ((  int32_t (*) (Enumerator_t595222681 *, const MethodInfo*))Enumerator_get_CurrentValue_m17055867_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::Reset()
#define Enumerator_Reset_m3581023982(__this, method) ((  void (*) (Enumerator_t595222681 *, const MethodInfo*))Enumerator_Reset_m1928144285_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::VerifyState()
#define Enumerator_VerifyState_m381725829(__this, method) ((  void (*) (Enumerator_t595222681 *, const MethodInfo*))Enumerator_VerifyState_m1152067668_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m3271166975(__this, method) ((  void (*) (Enumerator_t595222681 *, const MethodInfo*))Enumerator_VerifyCurrent_m2033497076_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::Dispose()
#define Enumerator_Dispose_m1531457544(__this, method) ((  void (*) (Enumerator_t595222681 *, const MethodInfo*))Enumerator_Dispose_m1362422387_gshared)(__this, method)
