﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.AmazonServiceCallback`2<System.Object,System.Object>
struct AmazonServiceCallback_2_t2110732071;
// System.Object
struct Il2CppObject;
// Amazon.Runtime.AmazonServiceResult`2<System.Object,System.Object>
struct AmazonServiceResult_2_t1630799325;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Amazon.Runtime.AmazonServiceCallback`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void AmazonServiceCallback_2__ctor_m2776589747_gshared (AmazonServiceCallback_2_t2110732071 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define AmazonServiceCallback_2__ctor_m2776589747(__this, ___object0, ___method1, method) ((  void (*) (AmazonServiceCallback_2_t2110732071 *, Il2CppObject *, IntPtr_t, const MethodInfo*))AmazonServiceCallback_2__ctor_m2776589747_gshared)(__this, ___object0, ___method1, method)
// System.Void Amazon.Runtime.AmazonServiceCallback`2<System.Object,System.Object>::Invoke(Amazon.Runtime.AmazonServiceResult`2<TRequest,TResponse>)
extern "C"  void AmazonServiceCallback_2_Invoke_m40441109_gshared (AmazonServiceCallback_2_t2110732071 * __this, AmazonServiceResult_2_t1630799325 * ___responseObject0, const MethodInfo* method);
#define AmazonServiceCallback_2_Invoke_m40441109(__this, ___responseObject0, method) ((  void (*) (AmazonServiceCallback_2_t2110732071 *, AmazonServiceResult_2_t1630799325 *, const MethodInfo*))AmazonServiceCallback_2_Invoke_m40441109_gshared)(__this, ___responseObject0, method)
// System.IAsyncResult Amazon.Runtime.AmazonServiceCallback`2<System.Object,System.Object>::BeginInvoke(Amazon.Runtime.AmazonServiceResult`2<TRequest,TResponse>,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * AmazonServiceCallback_2_BeginInvoke_m2934541552_gshared (AmazonServiceCallback_2_t2110732071 * __this, AmazonServiceResult_2_t1630799325 * ___responseObject0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method);
#define AmazonServiceCallback_2_BeginInvoke_m2934541552(__this, ___responseObject0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (AmazonServiceCallback_2_t2110732071 *, AmazonServiceResult_2_t1630799325 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))AmazonServiceCallback_2_BeginInvoke_m2934541552_gshared)(__this, ___responseObject0, ___callback1, ___object2, method)
// System.Void Amazon.Runtime.AmazonServiceCallback`2<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void AmazonServiceCallback_2_EndInvoke_m1628064041_gshared (AmazonServiceCallback_2_t2110732071 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define AmazonServiceCallback_2_EndInvoke_m1628064041(__this, ___result0, method) ((  void (*) (AmazonServiceCallback_2_t2110732071 *, Il2CppObject *, const MethodInfo*))AmazonServiceCallback_2_EndInvoke_m1628064041_gshared)(__this, ___result0, method)
