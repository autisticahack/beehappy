﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.CredentialsUnmarshaller
struct CredentialsUnmarshaller_t3086492122;
// Amazon.CognitoIdentity.Model.Credentials
struct Credentials_t792472136;
// Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext
struct XmlUnmarshallerContext_t1179575220;
// Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext
struct JsonUnmarshallerContext_t456235889;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_XmlU1179575220.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_JsonU456235889.h"

// Amazon.CognitoIdentity.Model.Credentials Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.CredentialsUnmarshaller::Amazon.Runtime.Internal.Transform.IUnmarshaller<Amazon.CognitoIdentity.Model.Credentials,Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext>.Unmarshall(Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext)
extern "C"  Credentials_t792472136 * CredentialsUnmarshaller_Amazon_Runtime_Internal_Transform_IUnmarshallerU3CAmazon_CognitoIdentity_Model_CredentialsU2CAmazon_Runtime_Internal_Transform_XmlUnmarshallerContextU3E_Unmarshall_m2756927442 (CredentialsUnmarshaller_t3086492122 * __this, XmlUnmarshallerContext_t1179575220 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.CognitoIdentity.Model.Credentials Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.CredentialsUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern "C"  Credentials_t792472136 * CredentialsUnmarshaller_Unmarshall_m1741641120 (CredentialsUnmarshaller_t3086492122 * __this, JsonUnmarshallerContext_t456235889 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.CredentialsUnmarshaller Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.CredentialsUnmarshaller::get_Instance()
extern "C"  CredentialsUnmarshaller_t3086492122 * CredentialsUnmarshaller_get_Instance_m1500740394 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.CredentialsUnmarshaller::.ctor()
extern "C"  void CredentialsUnmarshaller__ctor_m3987899510 (CredentialsUnmarshaller_t3086492122 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.CredentialsUnmarshaller::.cctor()
extern "C"  void CredentialsUnmarshaller__cctor_m2327109913 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
