﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ThirdParty.iOS4Unity.UIDevice
struct UIDevice_t860038178;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr2504060609.h"

// System.Void ThirdParty.iOS4Unity.UIDevice::.cctor()
extern "C"  void UIDevice__cctor_m1139141347 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.iOS4Unity.UIDevice::.ctor(System.IntPtr)
extern "C"  void UIDevice__ctor_m217874266 (UIDevice_t860038178 * __this, IntPtr_t ___handle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ThirdParty.iOS4Unity.UIDevice ThirdParty.iOS4Unity.UIDevice::get_CurrentDevice()
extern "C"  UIDevice_t860038178 * UIDevice_get_CurrentDevice_m2709480474 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ThirdParty.iOS4Unity.UIDevice::get_Model()
extern "C"  String_t* UIDevice_get_Model_m454493711 (UIDevice_t860038178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ThirdParty.iOS4Unity.UIDevice::get_SystemName()
extern "C"  String_t* UIDevice_get_SystemName_m1474594034 (UIDevice_t860038178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ThirdParty.iOS4Unity.UIDevice::get_SystemVersion()
extern "C"  String_t* UIDevice_get_SystemVersion_m1140415835 (UIDevice_t860038178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
