﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Data.Sqlite.SqliteConvert
struct SqliteConvert_t2634749803;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Type
struct Type_t;
// Mono.Data.Sqlite.SQLiteType
struct SQLiteType_t3850755444;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SQLiteDateFormat3821473988.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_DateTime693205669.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SQLiteType3850755444.h"
#include "System_Data_System_Data_DbType3924915636.h"
#include "mscorlib_System_Type1303803226.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_TypeAffinity326266980.h"

// System.Void Mono.Data.Sqlite.SqliteConvert::.ctor(Mono.Data.Sqlite.SQLiteDateFormats)
extern "C"  void SqliteConvert__ctor_m3810757105 (SqliteConvert_t2634749803 * __this, int32_t ___fmt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteConvert::.cctor()
extern "C"  void SqliteConvert__cctor_m3663796867 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Data.Sqlite.SqliteConvert::ToUTF8(System.String)
extern "C"  ByteU5BU5D_t3397334013* SqliteConvert_ToUTF8_m3797457148 (Il2CppObject * __this /* static, unused */, String_t* ___sourceText0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Data.Sqlite.SqliteConvert::ToUTF8(System.DateTime)
extern "C"  ByteU5BU5D_t3397334013* SqliteConvert_ToUTF8_m1174433064 (SqliteConvert_t2634749803 * __this, DateTime_t693205669  ___dateTimeValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Data.Sqlite.SqliteConvert::ToString(System.IntPtr,System.Int32)
extern "C"  String_t* SqliteConvert_ToString_m2460618300 (SqliteConvert_t2634749803 * __this, IntPtr_t ___nativestring0, int32_t ___nativestringlen1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Data.Sqlite.SqliteConvert::UTF8ToString(System.IntPtr,System.Int32)
extern "C"  String_t* SqliteConvert_UTF8ToString_m1234427123 (Il2CppObject * __this /* static, unused */, IntPtr_t ___nativestring0, int32_t ___nativestringlen1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime Mono.Data.Sqlite.SqliteConvert::ToDateTime(System.String)
extern "C"  DateTime_t693205669  SqliteConvert_ToDateTime_m1587306865 (SqliteConvert_t2634749803 * __this, String_t* ___dateText0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime Mono.Data.Sqlite.SqliteConvert::ToDateTime(System.Double)
extern "C"  DateTime_t693205669  SqliteConvert_ToDateTime_m1098251173 (SqliteConvert_t2634749803 * __this, double ___julianDay0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double Mono.Data.Sqlite.SqliteConvert::ToJulianDay(System.DateTime)
extern "C"  double SqliteConvert_ToJulianDay_m1354514381 (SqliteConvert_t2634749803 * __this, DateTime_t693205669  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Data.Sqlite.SqliteConvert::ToString(System.DateTime)
extern "C"  String_t* SqliteConvert_ToString_m3774947505 (SqliteConvert_t2634749803 * __this, DateTime_t693205669  ___dateValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime Mono.Data.Sqlite.SqliteConvert::ToDateTime(System.IntPtr,System.Int32)
extern "C"  DateTime_t693205669  SqliteConvert_ToDateTime_m606967572 (SqliteConvert_t2634749803 * __this, IntPtr_t ___ptr0, int32_t ___len1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] Mono.Data.Sqlite.SqliteConvert::Split(System.String,System.Char)
extern "C"  StringU5BU5D_t1642385972* SqliteConvert_Split_m4038550782 (Il2CppObject * __this /* static, unused */, String_t* ___source0, Il2CppChar ___separator1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Data.Sqlite.SqliteConvert::ToBoolean(System.String)
extern "C"  bool SqliteConvert_ToBoolean_m3044712497 (Il2CppObject * __this /* static, unused */, String_t* ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Mono.Data.Sqlite.SqliteConvert::SQLiteTypeToType(Mono.Data.Sqlite.SQLiteType)
extern "C"  Type_t * SqliteConvert_SQLiteTypeToType_m4113020186 (Il2CppObject * __this /* static, unused */, SQLiteType_t3850755444 * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.DbType Mono.Data.Sqlite.SqliteConvert::TypeToDbType(System.Type)
extern "C"  int32_t SqliteConvert_TypeToDbType_m1055901380 (Il2CppObject * __this /* static, unused */, Type_t * ___typ0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.SqliteConvert::DbTypeToColumnSize(System.Data.DbType)
extern "C"  int32_t SqliteConvert_DbTypeToColumnSize_m3343591987 (Il2CppObject * __this /* static, unused */, int32_t ___typ0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Mono.Data.Sqlite.SqliteConvert::DbTypeToNumericPrecision(System.Data.DbType)
extern "C"  Il2CppObject * SqliteConvert_DbTypeToNumericPrecision_m2545944732 (Il2CppObject * __this /* static, unused */, int32_t ___typ0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Mono.Data.Sqlite.SqliteConvert::DbTypeToNumericScale(System.Data.DbType)
extern "C"  Il2CppObject * SqliteConvert_DbTypeToNumericScale_m794374666 (Il2CppObject * __this /* static, unused */, int32_t ___typ0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Data.Sqlite.SqliteConvert::DbTypeToTypeName(System.Data.DbType)
extern "C"  String_t* SqliteConvert_DbTypeToTypeName_m17748596 (Il2CppObject * __this /* static, unused */, int32_t ___typ0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Mono.Data.Sqlite.SqliteConvert::DbTypeToType(System.Data.DbType)
extern "C"  Type_t * SqliteConvert_DbTypeToType_m1932425588 (Il2CppObject * __this /* static, unused */, int32_t ___typ0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Data.Sqlite.TypeAffinity Mono.Data.Sqlite.SqliteConvert::TypeToAffinity(System.Type)
extern "C"  int32_t SqliteConvert_TypeToAffinity_m2856160318 (Il2CppObject * __this /* static, unused */, Type_t * ___typ0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.DbType Mono.Data.Sqlite.SqliteConvert::TypeNameToDbType(System.String)
extern "C"  int32_t SqliteConvert_TypeNameToDbType_m3757768006 (Il2CppObject * __this /* static, unused */, String_t* ___Name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
