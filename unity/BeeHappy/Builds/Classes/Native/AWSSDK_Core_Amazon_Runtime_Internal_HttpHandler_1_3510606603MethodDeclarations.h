﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_HttpHandler_1_4170835665MethodDeclarations.h"

// System.Object Amazon.Runtime.Internal.HttpHandler`1<System.String>::get_CallbackSender()
#define HttpHandler_1_get_CallbackSender_m1650052783(__this, method) ((  Il2CppObject * (*) (HttpHandler_1_t3510606603 *, const MethodInfo*))HttpHandler_1_get_CallbackSender_m674323003_gshared)(__this, method)
// System.Void Amazon.Runtime.Internal.HttpHandler`1<System.String>::set_CallbackSender(System.Object)
#define HttpHandler_1_set_CallbackSender_m713079334(__this, ___value0, method) ((  void (*) (HttpHandler_1_t3510606603 *, Il2CppObject *, const MethodInfo*))HttpHandler_1_set_CallbackSender_m1603440352_gshared)(__this, ___value0, method)
// System.Void Amazon.Runtime.Internal.HttpHandler`1<System.String>::.ctor(Amazon.Runtime.IHttpRequestFactory`1<TRequestContent>,System.Object)
#define HttpHandler_1__ctor_m1337199814(__this, ___requestFactory0, ___callbackSender1, method) ((  void (*) (HttpHandler_1_t3510606603 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))HttpHandler_1__ctor_m1273353708_gshared)(__this, ___requestFactory0, ___callbackSender1, method)
// System.Void Amazon.Runtime.Internal.HttpHandler`1<System.String>::InvokeSync(Amazon.Runtime.IExecutionContext)
#define HttpHandler_1_InvokeSync_m2526965332(__this, ___executionContext0, method) ((  void (*) (HttpHandler_1_t3510606603 *, Il2CppObject *, const MethodInfo*))HttpHandler_1_InvokeSync_m3925324746_gshared)(__this, ___executionContext0, method)
// System.Void Amazon.Runtime.Internal.HttpHandler`1<System.String>::CompleteFailedRequest(Amazon.Runtime.IHttpRequest`1<TRequestContent>)
#define HttpHandler_1_CompleteFailedRequest_m2600249863(__this /* static, unused */, ___httpRequest0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))HttpHandler_1_CompleteFailedRequest_m2847384443_gshared)(__this /* static, unused */, ___httpRequest0, method)
// System.IAsyncResult Amazon.Runtime.Internal.HttpHandler`1<System.String>::InvokeAsync(Amazon.Runtime.IAsyncExecutionContext)
#define HttpHandler_1_InvokeAsync_m2170840425(__this, ___executionContext0, method) ((  Il2CppObject * (*) (HttpHandler_1_t3510606603 *, Il2CppObject *, const MethodInfo*))HttpHandler_1_InvokeAsync_m2861142229_gshared)(__this, ___executionContext0, method)
// System.Void Amazon.Runtime.Internal.HttpHandler`1<System.String>::GetRequestStreamCallback(System.IAsyncResult)
#define HttpHandler_1_GetRequestStreamCallback_m2758082580(__this, ___result0, method) ((  void (*) (HttpHandler_1_t3510606603 *, Il2CppObject *, const MethodInfo*))HttpHandler_1_GetRequestStreamCallback_m1249572158_gshared)(__this, ___result0, method)
// System.Void Amazon.Runtime.Internal.HttpHandler`1<System.String>::GetRequestStreamCallbackHelper(System.Object)
#define HttpHandler_1_GetRequestStreamCallbackHelper_m725863747(__this, ___state0, method) ((  void (*) (HttpHandler_1_t3510606603 *, Il2CppObject *, const MethodInfo*))HttpHandler_1_GetRequestStreamCallbackHelper_m4234283863_gshared)(__this, ___state0, method)
// System.Void Amazon.Runtime.Internal.HttpHandler`1<System.String>::GetResponseCallback(System.IAsyncResult)
#define HttpHandler_1_GetResponseCallback_m79728086(__this, ___result0, method) ((  void (*) (HttpHandler_1_t3510606603 *, Il2CppObject *, const MethodInfo*))HttpHandler_1_GetResponseCallback_m2219318320_gshared)(__this, ___result0, method)
// System.Void Amazon.Runtime.Internal.HttpHandler`1<System.String>::GetResponseCallbackHelper(System.Object)
#define HttpHandler_1_GetResponseCallbackHelper_m231023229(__this, ___state0, method) ((  void (*) (HttpHandler_1_t3510606603 *, Il2CppObject *, const MethodInfo*))HttpHandler_1_GetResponseCallbackHelper_m514406937_gshared)(__this, ___state0, method)
// System.Void Amazon.Runtime.Internal.HttpHandler`1<System.String>::SetMetrics(Amazon.Runtime.IRequestContext)
#define HttpHandler_1_SetMetrics_m234306661(__this /* static, unused */, ___requestContext0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))HttpHandler_1_SetMetrics_m2391049017_gshared)(__this /* static, unused */, ___requestContext0, method)
// System.Void Amazon.Runtime.Internal.HttpHandler`1<System.String>::WriteContentToRequestBody(TRequestContent,Amazon.Runtime.IHttpRequest`1<TRequestContent>,Amazon.Runtime.IRequestContext)
#define HttpHandler_1_WriteContentToRequestBody_m2776070983(__this, ___requestContent0, ___httpRequest1, ___requestContext2, method) ((  void (*) (HttpHandler_1_t3510606603 *, String_t*, Il2CppObject*, Il2CppObject *, const MethodInfo*))HttpHandler_1_WriteContentToRequestBody_m1084199443_gshared)(__this, ___requestContent0, ___httpRequest1, ___requestContext2, method)
// Amazon.Runtime.IHttpRequest`1<TRequestContent> Amazon.Runtime.Internal.HttpHandler`1<System.String>::CreateWebRequest(Amazon.Runtime.IRequestContext)
#define HttpHandler_1_CreateWebRequest_m1083205143(__this, ___requestContext0, method) ((  Il2CppObject* (*) (HttpHandler_1_t3510606603 *, Il2CppObject *, const MethodInfo*))HttpHandler_1_CreateWebRequest_m3217971059_gshared)(__this, ___requestContext0, method)
// System.Void Amazon.Runtime.Internal.HttpHandler`1<System.String>::Dispose()
#define HttpHandler_1_Dispose_m4172253156(__this, method) ((  void (*) (HttpHandler_1_t3510606603 *, const MethodInfo*))HttpHandler_1_Dispose_m3091244702_gshared)(__this, method)
// System.Void Amazon.Runtime.Internal.HttpHandler`1<System.String>::Dispose(System.Boolean)
#define HttpHandler_1_Dispose_m3428953849(__this, ___disposing0, method) ((  void (*) (HttpHandler_1_t3510606603 *, bool, const MethodInfo*))HttpHandler_1_Dispose_m2081298693_gshared)(__this, ___disposing0, method)
