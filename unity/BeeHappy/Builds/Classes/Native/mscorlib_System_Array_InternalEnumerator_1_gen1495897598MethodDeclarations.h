﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1495897598.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_637145336.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,ThirdParty.Json.LitJson.PropertyMetadata>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3324011410_gshared (InternalEnumerator_1_t1495897598 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m3324011410(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1495897598 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3324011410_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,ThirdParty.Json.LitJson.PropertyMetadata>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1960840710_gshared (InternalEnumerator_1_t1495897598 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1960840710(__this, method) ((  void (*) (InternalEnumerator_1_t1495897598 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1960840710_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,ThirdParty.Json.LitJson.PropertyMetadata>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3674943160_gshared (InternalEnumerator_1_t1495897598 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3674943160(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1495897598 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3674943160_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,ThirdParty.Json.LitJson.PropertyMetadata>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m550548093_gshared (InternalEnumerator_1_t1495897598 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m550548093(__this, method) ((  void (*) (InternalEnumerator_1_t1495897598 *, const MethodInfo*))InternalEnumerator_1_Dispose_m550548093_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,ThirdParty.Json.LitJson.PropertyMetadata>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2278430514_gshared (InternalEnumerator_1_t1495897598 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2278430514(__this, method) ((  bool (*) (InternalEnumerator_1_t1495897598 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2278430514_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,ThirdParty.Json.LitJson.PropertyMetadata>>::get_Current()
extern "C"  KeyValuePair_2_t637145336  InternalEnumerator_1_get_Current_m2926757973_gshared (InternalEnumerator_1_t1495897598 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m2926757973(__this, method) ((  KeyValuePair_2_t637145336  (*) (InternalEnumerator_1_t1495897598 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2926757973_gshared)(__this, method)
