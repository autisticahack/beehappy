﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.EndpointResolver
struct EndpointResolver_t2369326703;
// Amazon.Runtime.IExecutionContext
struct IExecutionContext_t2477130752;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// Amazon.Runtime.IAsyncExecutionContext
struct IAsyncExecutionContext_t3792344986;
// System.Uri
struct Uri_t19570940;
// Amazon.Runtime.IRequestContext
struct IRequestContext_t1620878341;
// Amazon.Runtime.IClientConfig
struct IClientConfig_t4078933728;
// Amazon.Runtime.Internal.IRequest
struct IRequest_t2400804350;

#include "codegen/il2cpp-codegen.h"

// System.Void Amazon.Runtime.Internal.EndpointResolver::InvokeSync(Amazon.Runtime.IExecutionContext)
extern "C"  void EndpointResolver_InvokeSync_m1289411332 (EndpointResolver_t2369326703 * __this, Il2CppObject * ___executionContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Amazon.Runtime.Internal.EndpointResolver::InvokeAsync(Amazon.Runtime.IAsyncExecutionContext)
extern "C"  Il2CppObject * EndpointResolver_InvokeAsync_m2836555073 (EndpointResolver_t2369326703 * __this, Il2CppObject * ___executionContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.EndpointResolver::PreInvoke(Amazon.Runtime.IExecutionContext)
extern "C"  void EndpointResolver_PreInvoke_m2101665878 (EndpointResolver_t2369326703 * __this, Il2CppObject * ___executionContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Uri Amazon.Runtime.Internal.EndpointResolver::DetermineEndpoint(Amazon.Runtime.IRequestContext)
extern "C"  Uri_t19570940 * EndpointResolver_DetermineEndpoint_m2717032208 (EndpointResolver_t2369326703 * __this, Il2CppObject * ___requestContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Uri Amazon.Runtime.Internal.EndpointResolver::DetermineEndpoint(Amazon.Runtime.IClientConfig,Amazon.Runtime.Internal.IRequest)
extern "C"  Uri_t19570940 * EndpointResolver_DetermineEndpoint_m2834055088 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___config0, Il2CppObject * ___request1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.EndpointResolver::.ctor()
extern "C"  void EndpointResolver__ctor_m3515502805 (EndpointResolver_t2369326703 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
