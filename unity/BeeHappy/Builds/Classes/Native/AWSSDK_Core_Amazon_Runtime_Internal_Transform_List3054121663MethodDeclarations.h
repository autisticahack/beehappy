﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_List1903008017MethodDeclarations.h"

// System.Void Amazon.Runtime.Internal.Transform.ListUnmarshaller`2<Amazon.CognitoSync.Model.Record,Amazon.CognitoSync.Model.Internal.MarshallTransformations.RecordUnmarshaller>::.ctor(IUnmarshaller)
#define ListUnmarshaller_2__ctor_m1713120964(__this, ___iUnmarshaller0, method) ((  void (*) (ListUnmarshaller_2_t3054121663 *, RecordUnmarshaller_t3056833063 *, const MethodInfo*))ListUnmarshaller_2__ctor_m1344739393_gshared)(__this, ___iUnmarshaller0, method)
// System.Collections.Generic.List`1<I> Amazon.Runtime.Internal.Transform.ListUnmarshaller`2<Amazon.CognitoSync.Model.Record,Amazon.CognitoSync.Model.Internal.MarshallTransformations.RecordUnmarshaller>::Unmarshall(Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext)
#define ListUnmarshaller_2_Unmarshall_m3344575457(__this, ___context0, method) ((  List_1_t4105675293 * (*) (ListUnmarshaller_2_t3054121663 *, XmlUnmarshallerContext_t1179575220 *, const MethodInfo*))ListUnmarshaller_2_Unmarshall_m4244807702_gshared)(__this, ___context0, method)
// System.Collections.Generic.List`1<I> Amazon.Runtime.Internal.Transform.ListUnmarshaller`2<Amazon.CognitoSync.Model.Record,Amazon.CognitoSync.Model.Internal.MarshallTransformations.RecordUnmarshaller>::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
#define ListUnmarshaller_2_Unmarshall_m2899052195(__this, ___context0, method) ((  List_1_t4105675293 * (*) (ListUnmarshaller_2_t3054121663 *, JsonUnmarshallerContext_t456235889 *, const MethodInfo*))ListUnmarshaller_2_Unmarshall_m3347864357_gshared)(__this, ___context0, method)
