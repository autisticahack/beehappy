﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.Runtime.AsyncOptions
struct AsyncOptions_t558351272;
// Amazon.CognitoSync.SyncManager.Dataset
struct Dataset_t3040902086;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.CognitoSync.SyncManager.Dataset/<>c__DisplayClass71_0
struct  U3CU3Ec__DisplayClass71_0_t1829637511  : public Il2CppObject
{
public:
	// Amazon.Runtime.AsyncOptions Amazon.CognitoSync.SyncManager.Dataset/<>c__DisplayClass71_0::options
	AsyncOptions_t558351272 * ___options_0;
	// Amazon.CognitoSync.SyncManager.Dataset Amazon.CognitoSync.SyncManager.Dataset/<>c__DisplayClass71_0::<>4__this
	Dataset_t3040902086 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_options_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass71_0_t1829637511, ___options_0)); }
	inline AsyncOptions_t558351272 * get_options_0() const { return ___options_0; }
	inline AsyncOptions_t558351272 ** get_address_of_options_0() { return &___options_0; }
	inline void set_options_0(AsyncOptions_t558351272 * value)
	{
		___options_0 = value;
		Il2CppCodeGenWriteBarrier(&___options_0, value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass71_0_t1829637511, ___U3CU3E4__this_1)); }
	inline Dataset_t3040902086 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline Dataset_t3040902086 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(Dataset_t3040902086 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E4__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
