﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.RegionEndpoint
struct RegionEndpoint_t661522805;
// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_TimeSpan3430258949.h"
#include "AWSSDK_Core_Amazon_Runtime_SigningAlgorithm3740229458.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.ClientConfig
struct  ClientConfig_t3664713661  : public Il2CppObject
{
public:
	// Amazon.RegionEndpoint Amazon.Runtime.ClientConfig::regionEndpoint
	RegionEndpoint_t661522805 * ___regionEndpoint_2;
	// System.Boolean Amazon.Runtime.ClientConfig::probeForRegionEndpoint
	bool ___probeForRegionEndpoint_3;
	// System.Boolean Amazon.Runtime.ClientConfig::throttleRetries
	bool ___throttleRetries_4;
	// System.Boolean Amazon.Runtime.ClientConfig::useHttp
	bool ___useHttp_5;
	// System.String Amazon.Runtime.ClientConfig::serviceURL
	String_t* ___serviceURL_6;
	// System.String Amazon.Runtime.ClientConfig::authRegion
	String_t* ___authRegion_7;
	// System.String Amazon.Runtime.ClientConfig::authServiceName
	String_t* ___authServiceName_8;
	// System.String Amazon.Runtime.ClientConfig::signatureVersion
	String_t* ___signatureVersion_9;
	// Amazon.Runtime.SigningAlgorithm Amazon.Runtime.ClientConfig::signatureMethod
	int32_t ___signatureMethod_10;
	// System.Int32 Amazon.Runtime.ClientConfig::maxErrorRetry
	int32_t ___maxErrorRetry_11;
	// System.Boolean Amazon.Runtime.ClientConfig::logResponse
	bool ___logResponse_12;
	// System.Int32 Amazon.Runtime.ClientConfig::bufferSize
	int32_t ___bufferSize_13;
	// System.Int64 Amazon.Runtime.ClientConfig::progressUpdateInterval
	int64_t ___progressUpdateInterval_14;
	// System.Boolean Amazon.Runtime.ClientConfig::resignRetries
	bool ___resignRetries_15;
	// System.Boolean Amazon.Runtime.ClientConfig::logMetrics
	bool ___logMetrics_16;
	// System.Boolean Amazon.Runtime.ClientConfig::disableLogging
	bool ___disableLogging_17;
	// System.Boolean Amazon.Runtime.ClientConfig::allowAutoRedirect
	bool ___allowAutoRedirect_18;
	// System.Boolean Amazon.Runtime.ClientConfig::useDualstackEndpoint
	bool ___useDualstackEndpoint_19;
	// System.Int32 Amazon.Runtime.ClientConfig::proxyPort
	int32_t ___proxyPort_20;

public:
	inline static int32_t get_offset_of_regionEndpoint_2() { return static_cast<int32_t>(offsetof(ClientConfig_t3664713661, ___regionEndpoint_2)); }
	inline RegionEndpoint_t661522805 * get_regionEndpoint_2() const { return ___regionEndpoint_2; }
	inline RegionEndpoint_t661522805 ** get_address_of_regionEndpoint_2() { return &___regionEndpoint_2; }
	inline void set_regionEndpoint_2(RegionEndpoint_t661522805 * value)
	{
		___regionEndpoint_2 = value;
		Il2CppCodeGenWriteBarrier(&___regionEndpoint_2, value);
	}

	inline static int32_t get_offset_of_probeForRegionEndpoint_3() { return static_cast<int32_t>(offsetof(ClientConfig_t3664713661, ___probeForRegionEndpoint_3)); }
	inline bool get_probeForRegionEndpoint_3() const { return ___probeForRegionEndpoint_3; }
	inline bool* get_address_of_probeForRegionEndpoint_3() { return &___probeForRegionEndpoint_3; }
	inline void set_probeForRegionEndpoint_3(bool value)
	{
		___probeForRegionEndpoint_3 = value;
	}

	inline static int32_t get_offset_of_throttleRetries_4() { return static_cast<int32_t>(offsetof(ClientConfig_t3664713661, ___throttleRetries_4)); }
	inline bool get_throttleRetries_4() const { return ___throttleRetries_4; }
	inline bool* get_address_of_throttleRetries_4() { return &___throttleRetries_4; }
	inline void set_throttleRetries_4(bool value)
	{
		___throttleRetries_4 = value;
	}

	inline static int32_t get_offset_of_useHttp_5() { return static_cast<int32_t>(offsetof(ClientConfig_t3664713661, ___useHttp_5)); }
	inline bool get_useHttp_5() const { return ___useHttp_5; }
	inline bool* get_address_of_useHttp_5() { return &___useHttp_5; }
	inline void set_useHttp_5(bool value)
	{
		___useHttp_5 = value;
	}

	inline static int32_t get_offset_of_serviceURL_6() { return static_cast<int32_t>(offsetof(ClientConfig_t3664713661, ___serviceURL_6)); }
	inline String_t* get_serviceURL_6() const { return ___serviceURL_6; }
	inline String_t** get_address_of_serviceURL_6() { return &___serviceURL_6; }
	inline void set_serviceURL_6(String_t* value)
	{
		___serviceURL_6 = value;
		Il2CppCodeGenWriteBarrier(&___serviceURL_6, value);
	}

	inline static int32_t get_offset_of_authRegion_7() { return static_cast<int32_t>(offsetof(ClientConfig_t3664713661, ___authRegion_7)); }
	inline String_t* get_authRegion_7() const { return ___authRegion_7; }
	inline String_t** get_address_of_authRegion_7() { return &___authRegion_7; }
	inline void set_authRegion_7(String_t* value)
	{
		___authRegion_7 = value;
		Il2CppCodeGenWriteBarrier(&___authRegion_7, value);
	}

	inline static int32_t get_offset_of_authServiceName_8() { return static_cast<int32_t>(offsetof(ClientConfig_t3664713661, ___authServiceName_8)); }
	inline String_t* get_authServiceName_8() const { return ___authServiceName_8; }
	inline String_t** get_address_of_authServiceName_8() { return &___authServiceName_8; }
	inline void set_authServiceName_8(String_t* value)
	{
		___authServiceName_8 = value;
		Il2CppCodeGenWriteBarrier(&___authServiceName_8, value);
	}

	inline static int32_t get_offset_of_signatureVersion_9() { return static_cast<int32_t>(offsetof(ClientConfig_t3664713661, ___signatureVersion_9)); }
	inline String_t* get_signatureVersion_9() const { return ___signatureVersion_9; }
	inline String_t** get_address_of_signatureVersion_9() { return &___signatureVersion_9; }
	inline void set_signatureVersion_9(String_t* value)
	{
		___signatureVersion_9 = value;
		Il2CppCodeGenWriteBarrier(&___signatureVersion_9, value);
	}

	inline static int32_t get_offset_of_signatureMethod_10() { return static_cast<int32_t>(offsetof(ClientConfig_t3664713661, ___signatureMethod_10)); }
	inline int32_t get_signatureMethod_10() const { return ___signatureMethod_10; }
	inline int32_t* get_address_of_signatureMethod_10() { return &___signatureMethod_10; }
	inline void set_signatureMethod_10(int32_t value)
	{
		___signatureMethod_10 = value;
	}

	inline static int32_t get_offset_of_maxErrorRetry_11() { return static_cast<int32_t>(offsetof(ClientConfig_t3664713661, ___maxErrorRetry_11)); }
	inline int32_t get_maxErrorRetry_11() const { return ___maxErrorRetry_11; }
	inline int32_t* get_address_of_maxErrorRetry_11() { return &___maxErrorRetry_11; }
	inline void set_maxErrorRetry_11(int32_t value)
	{
		___maxErrorRetry_11 = value;
	}

	inline static int32_t get_offset_of_logResponse_12() { return static_cast<int32_t>(offsetof(ClientConfig_t3664713661, ___logResponse_12)); }
	inline bool get_logResponse_12() const { return ___logResponse_12; }
	inline bool* get_address_of_logResponse_12() { return &___logResponse_12; }
	inline void set_logResponse_12(bool value)
	{
		___logResponse_12 = value;
	}

	inline static int32_t get_offset_of_bufferSize_13() { return static_cast<int32_t>(offsetof(ClientConfig_t3664713661, ___bufferSize_13)); }
	inline int32_t get_bufferSize_13() const { return ___bufferSize_13; }
	inline int32_t* get_address_of_bufferSize_13() { return &___bufferSize_13; }
	inline void set_bufferSize_13(int32_t value)
	{
		___bufferSize_13 = value;
	}

	inline static int32_t get_offset_of_progressUpdateInterval_14() { return static_cast<int32_t>(offsetof(ClientConfig_t3664713661, ___progressUpdateInterval_14)); }
	inline int64_t get_progressUpdateInterval_14() const { return ___progressUpdateInterval_14; }
	inline int64_t* get_address_of_progressUpdateInterval_14() { return &___progressUpdateInterval_14; }
	inline void set_progressUpdateInterval_14(int64_t value)
	{
		___progressUpdateInterval_14 = value;
	}

	inline static int32_t get_offset_of_resignRetries_15() { return static_cast<int32_t>(offsetof(ClientConfig_t3664713661, ___resignRetries_15)); }
	inline bool get_resignRetries_15() const { return ___resignRetries_15; }
	inline bool* get_address_of_resignRetries_15() { return &___resignRetries_15; }
	inline void set_resignRetries_15(bool value)
	{
		___resignRetries_15 = value;
	}

	inline static int32_t get_offset_of_logMetrics_16() { return static_cast<int32_t>(offsetof(ClientConfig_t3664713661, ___logMetrics_16)); }
	inline bool get_logMetrics_16() const { return ___logMetrics_16; }
	inline bool* get_address_of_logMetrics_16() { return &___logMetrics_16; }
	inline void set_logMetrics_16(bool value)
	{
		___logMetrics_16 = value;
	}

	inline static int32_t get_offset_of_disableLogging_17() { return static_cast<int32_t>(offsetof(ClientConfig_t3664713661, ___disableLogging_17)); }
	inline bool get_disableLogging_17() const { return ___disableLogging_17; }
	inline bool* get_address_of_disableLogging_17() { return &___disableLogging_17; }
	inline void set_disableLogging_17(bool value)
	{
		___disableLogging_17 = value;
	}

	inline static int32_t get_offset_of_allowAutoRedirect_18() { return static_cast<int32_t>(offsetof(ClientConfig_t3664713661, ___allowAutoRedirect_18)); }
	inline bool get_allowAutoRedirect_18() const { return ___allowAutoRedirect_18; }
	inline bool* get_address_of_allowAutoRedirect_18() { return &___allowAutoRedirect_18; }
	inline void set_allowAutoRedirect_18(bool value)
	{
		___allowAutoRedirect_18 = value;
	}

	inline static int32_t get_offset_of_useDualstackEndpoint_19() { return static_cast<int32_t>(offsetof(ClientConfig_t3664713661, ___useDualstackEndpoint_19)); }
	inline bool get_useDualstackEndpoint_19() const { return ___useDualstackEndpoint_19; }
	inline bool* get_address_of_useDualstackEndpoint_19() { return &___useDualstackEndpoint_19; }
	inline void set_useDualstackEndpoint_19(bool value)
	{
		___useDualstackEndpoint_19 = value;
	}

	inline static int32_t get_offset_of_proxyPort_20() { return static_cast<int32_t>(offsetof(ClientConfig_t3664713661, ___proxyPort_20)); }
	inline int32_t get_proxyPort_20() const { return ___proxyPort_20; }
	inline int32_t* get_address_of_proxyPort_20() { return &___proxyPort_20; }
	inline void set_proxyPort_20(int32_t value)
	{
		___proxyPort_20 = value;
	}
};

struct ClientConfig_t3664713661_StaticFields
{
public:
	// System.TimeSpan Amazon.Runtime.ClientConfig::InfiniteTimeout
	TimeSpan_t3430258949  ___InfiniteTimeout_0;
	// System.TimeSpan Amazon.Runtime.ClientConfig::MaxTimeout
	TimeSpan_t3430258949  ___MaxTimeout_1;

public:
	inline static int32_t get_offset_of_InfiniteTimeout_0() { return static_cast<int32_t>(offsetof(ClientConfig_t3664713661_StaticFields, ___InfiniteTimeout_0)); }
	inline TimeSpan_t3430258949  get_InfiniteTimeout_0() const { return ___InfiniteTimeout_0; }
	inline TimeSpan_t3430258949 * get_address_of_InfiniteTimeout_0() { return &___InfiniteTimeout_0; }
	inline void set_InfiniteTimeout_0(TimeSpan_t3430258949  value)
	{
		___InfiniteTimeout_0 = value;
	}

	inline static int32_t get_offset_of_MaxTimeout_1() { return static_cast<int32_t>(offsetof(ClientConfig_t3664713661_StaticFields, ___MaxTimeout_1)); }
	inline TimeSpan_t3430258949  get_MaxTimeout_1() const { return ___MaxTimeout_1; }
	inline TimeSpan_t3430258949 * get_address_of_MaxTimeout_1() { return &___MaxTimeout_1; }
	inline void set_MaxTimeout_1(TimeSpan_t3430258949  value)
	{
		___MaxTimeout_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
