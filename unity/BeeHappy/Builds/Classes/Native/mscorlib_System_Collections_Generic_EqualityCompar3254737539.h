﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Collections_Generic_EqualityCompare206442649.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>
struct  DefaultComparer_t3254737539  : public EqualityComparer_1_t206442649
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
