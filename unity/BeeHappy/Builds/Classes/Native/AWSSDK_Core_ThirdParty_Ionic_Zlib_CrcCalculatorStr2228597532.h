﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.IO.Stream
struct Stream_t3255436806;
// ThirdParty.Ionic.Zlib.CRC32
struct CRC32_t619824607;

#include "mscorlib_System_IO_Stream3255436806.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.Ionic.Zlib.CrcCalculatorStream
struct  CrcCalculatorStream_t2228597532  : public Stream_t3255436806
{
public:
	// System.IO.Stream ThirdParty.Ionic.Zlib.CrcCalculatorStream::_InnerStream
	Stream_t3255436806 * ____InnerStream_1;
	// ThirdParty.Ionic.Zlib.CRC32 ThirdParty.Ionic.Zlib.CrcCalculatorStream::_Crc32
	CRC32_t619824607 * ____Crc32_2;
	// System.Int64 ThirdParty.Ionic.Zlib.CrcCalculatorStream::_length
	int64_t ____length_3;

public:
	inline static int32_t get_offset_of__InnerStream_1() { return static_cast<int32_t>(offsetof(CrcCalculatorStream_t2228597532, ____InnerStream_1)); }
	inline Stream_t3255436806 * get__InnerStream_1() const { return ____InnerStream_1; }
	inline Stream_t3255436806 ** get_address_of__InnerStream_1() { return &____InnerStream_1; }
	inline void set__InnerStream_1(Stream_t3255436806 * value)
	{
		____InnerStream_1 = value;
		Il2CppCodeGenWriteBarrier(&____InnerStream_1, value);
	}

	inline static int32_t get_offset_of__Crc32_2() { return static_cast<int32_t>(offsetof(CrcCalculatorStream_t2228597532, ____Crc32_2)); }
	inline CRC32_t619824607 * get__Crc32_2() const { return ____Crc32_2; }
	inline CRC32_t619824607 ** get_address_of__Crc32_2() { return &____Crc32_2; }
	inline void set__Crc32_2(CRC32_t619824607 * value)
	{
		____Crc32_2 = value;
		Il2CppCodeGenWriteBarrier(&____Crc32_2, value);
	}

	inline static int32_t get_offset_of__length_3() { return static_cast<int32_t>(offsetof(CrcCalculatorStream_t2228597532, ____length_3)); }
	inline int64_t get__length_3() const { return ____length_3; }
	inline int64_t* get_address_of__length_3() { return &____length_3; }
	inline void set__length_3(int64_t value)
	{
		____length_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
