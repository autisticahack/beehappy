﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.String,Amazon.RegionEndpoint/Endpoint>
struct Dictionary_2_t968504607;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Internal.RegionEndpointV3/ServiceMap
struct  ServiceMap_t3203095671  : public Il2CppObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,Amazon.RegionEndpoint/Endpoint> Amazon.Internal.RegionEndpointV3/ServiceMap::_serviceMap
	Dictionary_2_t968504607 * ____serviceMap_0;
	// System.Collections.Generic.Dictionary`2<System.String,Amazon.RegionEndpoint/Endpoint> Amazon.Internal.RegionEndpointV3/ServiceMap::_dualServiceMap
	Dictionary_2_t968504607 * ____dualServiceMap_1;

public:
	inline static int32_t get_offset_of__serviceMap_0() { return static_cast<int32_t>(offsetof(ServiceMap_t3203095671, ____serviceMap_0)); }
	inline Dictionary_2_t968504607 * get__serviceMap_0() const { return ____serviceMap_0; }
	inline Dictionary_2_t968504607 ** get_address_of__serviceMap_0() { return &____serviceMap_0; }
	inline void set__serviceMap_0(Dictionary_2_t968504607 * value)
	{
		____serviceMap_0 = value;
		Il2CppCodeGenWriteBarrier(&____serviceMap_0, value);
	}

	inline static int32_t get_offset_of__dualServiceMap_1() { return static_cast<int32_t>(offsetof(ServiceMap_t3203095671, ____dualServiceMap_1)); }
	inline Dictionary_2_t968504607 * get__dualServiceMap_1() const { return ____dualServiceMap_1; }
	inline Dictionary_2_t968504607 ** get_address_of__dualServiceMap_1() { return &____dualServiceMap_1; }
	inline void set__dualServiceMap_1(Dictionary_2_t968504607 * value)
	{
		____dualServiceMap_1 = value;
		Il2CppCodeGenWriteBarrier(&____dualServiceMap_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
