﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.HttpErrorResponseException
struct HttpErrorResponseException_t3191555406;
// Amazon.Runtime.Internal.Transform.IWebResponseData
struct IWebResponseData_t3815728244;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serialization228987430.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"

// Amazon.Runtime.Internal.Transform.IWebResponseData Amazon.Runtime.Internal.HttpErrorResponseException::get_Response()
extern "C"  Il2CppObject * HttpErrorResponseException_get_Response_m1065893860 (HttpErrorResponseException_t3191555406 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.HttpErrorResponseException::set_Response(Amazon.Runtime.Internal.Transform.IWebResponseData)
extern "C"  void HttpErrorResponseException_set_Response_m4057595209 (HttpErrorResponseException_t3191555406 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.HttpErrorResponseException::.ctor(Amazon.Runtime.Internal.Transform.IWebResponseData)
extern "C"  void HttpErrorResponseException__ctor_m865988347 (HttpErrorResponseException_t3191555406 * __this, Il2CppObject * ___response0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.HttpErrorResponseException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void HttpErrorResponseException__ctor_m30678083 (HttpErrorResponseException_t3191555406 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.HttpErrorResponseException::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void HttpErrorResponseException_GetObjectData_m407584610 (HttpErrorResponseException_t3191555406 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
