﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// System.EventHandler`1<Amazon.Runtime.StreamTransferProgressArgs>
struct EventHandler_1_t1230945235;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.StreamReadTracker
struct  StreamReadTracker_t1958363340  : public Il2CppObject
{
public:
	// System.Object Amazon.Runtime.Internal.StreamReadTracker::sender
	Il2CppObject * ___sender_0;
	// System.EventHandler`1<Amazon.Runtime.StreamTransferProgressArgs> Amazon.Runtime.Internal.StreamReadTracker::callback
	EventHandler_1_t1230945235 * ___callback_1;
	// System.Int64 Amazon.Runtime.Internal.StreamReadTracker::contentLength
	int64_t ___contentLength_2;
	// System.Int64 Amazon.Runtime.Internal.StreamReadTracker::totalBytesRead
	int64_t ___totalBytesRead_3;
	// System.Int64 Amazon.Runtime.Internal.StreamReadTracker::totalIncrementTransferred
	int64_t ___totalIncrementTransferred_4;
	// System.Int64 Amazon.Runtime.Internal.StreamReadTracker::progressUpdateInterval
	int64_t ___progressUpdateInterval_5;

public:
	inline static int32_t get_offset_of_sender_0() { return static_cast<int32_t>(offsetof(StreamReadTracker_t1958363340, ___sender_0)); }
	inline Il2CppObject * get_sender_0() const { return ___sender_0; }
	inline Il2CppObject ** get_address_of_sender_0() { return &___sender_0; }
	inline void set_sender_0(Il2CppObject * value)
	{
		___sender_0 = value;
		Il2CppCodeGenWriteBarrier(&___sender_0, value);
	}

	inline static int32_t get_offset_of_callback_1() { return static_cast<int32_t>(offsetof(StreamReadTracker_t1958363340, ___callback_1)); }
	inline EventHandler_1_t1230945235 * get_callback_1() const { return ___callback_1; }
	inline EventHandler_1_t1230945235 ** get_address_of_callback_1() { return &___callback_1; }
	inline void set_callback_1(EventHandler_1_t1230945235 * value)
	{
		___callback_1 = value;
		Il2CppCodeGenWriteBarrier(&___callback_1, value);
	}

	inline static int32_t get_offset_of_contentLength_2() { return static_cast<int32_t>(offsetof(StreamReadTracker_t1958363340, ___contentLength_2)); }
	inline int64_t get_contentLength_2() const { return ___contentLength_2; }
	inline int64_t* get_address_of_contentLength_2() { return &___contentLength_2; }
	inline void set_contentLength_2(int64_t value)
	{
		___contentLength_2 = value;
	}

	inline static int32_t get_offset_of_totalBytesRead_3() { return static_cast<int32_t>(offsetof(StreamReadTracker_t1958363340, ___totalBytesRead_3)); }
	inline int64_t get_totalBytesRead_3() const { return ___totalBytesRead_3; }
	inline int64_t* get_address_of_totalBytesRead_3() { return &___totalBytesRead_3; }
	inline void set_totalBytesRead_3(int64_t value)
	{
		___totalBytesRead_3 = value;
	}

	inline static int32_t get_offset_of_totalIncrementTransferred_4() { return static_cast<int32_t>(offsetof(StreamReadTracker_t1958363340, ___totalIncrementTransferred_4)); }
	inline int64_t get_totalIncrementTransferred_4() const { return ___totalIncrementTransferred_4; }
	inline int64_t* get_address_of_totalIncrementTransferred_4() { return &___totalIncrementTransferred_4; }
	inline void set_totalIncrementTransferred_4(int64_t value)
	{
		___totalIncrementTransferred_4 = value;
	}

	inline static int32_t get_offset_of_progressUpdateInterval_5() { return static_cast<int32_t>(offsetof(StreamReadTracker_t1958363340, ___progressUpdateInterval_5)); }
	inline int64_t get_progressUpdateInterval_5() const { return ___progressUpdateInterval_5; }
	inline int64_t* get_address_of_progressUpdateInterval_5() { return &___progressUpdateInterval_5; }
	inline void set_progressUpdateInterval_5(int64_t value)
	{
		___progressUpdateInterval_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
