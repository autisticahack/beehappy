﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Util.Internal.InternalSDKUtils/<>c__DisplayClass31_0
struct U3CU3Ec__DisplayClass31_0_t4069291374;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void Amazon.Util.Internal.InternalSDKUtils/<>c__DisplayClass31_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass31_0__ctor_m1692275111 (U3CU3Ec__DisplayClass31_0_t4069291374 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Util.Internal.InternalSDKUtils/<>c__DisplayClass31_0::<AsyncExecutor>b__0(System.Object)
extern "C"  void U3CU3Ec__DisplayClass31_0_U3CAsyncExecutorU3Eb__0_m3065129460 (U3CU3Ec__DisplayClass31_0_t4069291374 * __this, Il2CppObject * ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
