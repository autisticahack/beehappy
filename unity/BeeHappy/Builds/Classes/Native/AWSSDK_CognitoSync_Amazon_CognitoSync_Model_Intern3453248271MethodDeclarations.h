﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.CognitoSync.Model.Internal.MarshallTransformations.UpdateRecordsRequestMarshaller
struct UpdateRecordsRequestMarshaller_t3453248271;
// Amazon.Runtime.Internal.IRequest
struct IRequest_t2400804350;
// Amazon.Runtime.AmazonWebServiceRequest
struct AmazonWebServiceRequest_t3384026212;
// Amazon.CognitoSync.Model.UpdateRecordsRequest
struct UpdateRecordsRequest_t197801344;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_Runtime_AmazonWebServiceRequest3384026212.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_Model_UpdateR197801344.h"

// Amazon.Runtime.Internal.IRequest Amazon.CognitoSync.Model.Internal.MarshallTransformations.UpdateRecordsRequestMarshaller::Marshall(Amazon.Runtime.AmazonWebServiceRequest)
extern "C"  Il2CppObject * UpdateRecordsRequestMarshaller_Marshall_m2815251387 (UpdateRecordsRequestMarshaller_t3453248271 * __this, AmazonWebServiceRequest_t3384026212 * ___input0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.Internal.IRequest Amazon.CognitoSync.Model.Internal.MarshallTransformations.UpdateRecordsRequestMarshaller::Marshall(Amazon.CognitoSync.Model.UpdateRecordsRequest)
extern "C"  Il2CppObject * UpdateRecordsRequestMarshaller_Marshall_m790872838 (UpdateRecordsRequestMarshaller_t3453248271 * __this, UpdateRecordsRequest_t197801344 * ___publicRequest0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Model.Internal.MarshallTransformations.UpdateRecordsRequestMarshaller::.ctor()
extern "C"  void UpdateRecordsRequestMarshaller__ctor_m3584973243 (UpdateRecordsRequestMarshaller_t3453248271 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
