﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.Util.BackgroundDispatcher`1<System.Object>
struct BackgroundDispatcher_1_t2693883954;
// System.Action`1<System.Object>
struct Action_1_t2491248677;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void Amazon.Runtime.Internal.Util.BackgroundDispatcher`1<System.Object>::set_IsRunning(System.Boolean)
extern "C"  void BackgroundDispatcher_1_set_IsRunning_m2776258133_gshared (BackgroundDispatcher_1_t2693883954 * __this, bool ___value0, const MethodInfo* method);
#define BackgroundDispatcher_1_set_IsRunning_m2776258133(__this, ___value0, method) ((  void (*) (BackgroundDispatcher_1_t2693883954 *, bool, const MethodInfo*))BackgroundDispatcher_1_set_IsRunning_m2776258133_gshared)(__this, ___value0, method)
// System.Void Amazon.Runtime.Internal.Util.BackgroundDispatcher`1<System.Object>::.ctor(System.Action`1<T>)
extern "C"  void BackgroundDispatcher_1__ctor_m2175859570_gshared (BackgroundDispatcher_1_t2693883954 * __this, Action_1_t2491248677 * ___action0, const MethodInfo* method);
#define BackgroundDispatcher_1__ctor_m2175859570(__this, ___action0, method) ((  void (*) (BackgroundDispatcher_1_t2693883954 *, Action_1_t2491248677 *, const MethodInfo*))BackgroundDispatcher_1__ctor_m2175859570_gshared)(__this, ___action0, method)
// System.Void Amazon.Runtime.Internal.Util.BackgroundDispatcher`1<System.Object>::Finalize()
extern "C"  void BackgroundDispatcher_1_Finalize_m1140862552_gshared (BackgroundDispatcher_1_t2693883954 * __this, const MethodInfo* method);
#define BackgroundDispatcher_1_Finalize_m1140862552(__this, method) ((  void (*) (BackgroundDispatcher_1_t2693883954 *, const MethodInfo*))BackgroundDispatcher_1_Finalize_m1140862552_gshared)(__this, method)
// System.Void Amazon.Runtime.Internal.Util.BackgroundDispatcher`1<System.Object>::Dispose(System.Boolean)
extern "C"  void BackgroundDispatcher_1_Dispose_m2779145776_gshared (BackgroundDispatcher_1_t2693883954 * __this, bool ___disposing0, const MethodInfo* method);
#define BackgroundDispatcher_1_Dispose_m2779145776(__this, ___disposing0, method) ((  void (*) (BackgroundDispatcher_1_t2693883954 *, bool, const MethodInfo*))BackgroundDispatcher_1_Dispose_m2779145776_gshared)(__this, ___disposing0, method)
// System.Void Amazon.Runtime.Internal.Util.BackgroundDispatcher`1<System.Object>::Dispose()
extern "C"  void BackgroundDispatcher_1_Dispose_m3297611021_gshared (BackgroundDispatcher_1_t2693883954 * __this, const MethodInfo* method);
#define BackgroundDispatcher_1_Dispose_m3297611021(__this, method) ((  void (*) (BackgroundDispatcher_1_t2693883954 *, const MethodInfo*))BackgroundDispatcher_1_Dispose_m3297611021_gshared)(__this, method)
// System.Void Amazon.Runtime.Internal.Util.BackgroundDispatcher`1<System.Object>::Dispatch(T)
extern "C"  void BackgroundDispatcher_1_Dispatch_m2867538446_gshared (BackgroundDispatcher_1_t2693883954 * __this, Il2CppObject * ___data0, const MethodInfo* method);
#define BackgroundDispatcher_1_Dispatch_m2867538446(__this, ___data0, method) ((  void (*) (BackgroundDispatcher_1_t2693883954 *, Il2CppObject *, const MethodInfo*))BackgroundDispatcher_1_Dispatch_m2867538446_gshared)(__this, ___data0, method)
// System.Void Amazon.Runtime.Internal.Util.BackgroundDispatcher`1<System.Object>::Stop()
extern "C"  void BackgroundDispatcher_1_Stop_m4186726394_gshared (BackgroundDispatcher_1_t2693883954 * __this, const MethodInfo* method);
#define BackgroundDispatcher_1_Stop_m4186726394(__this, method) ((  void (*) (BackgroundDispatcher_1_t2693883954 *, const MethodInfo*))BackgroundDispatcher_1_Stop_m4186726394_gshared)(__this, method)
// System.Void Amazon.Runtime.Internal.Util.BackgroundDispatcher`1<System.Object>::Run()
extern "C"  void BackgroundDispatcher_1_Run_m3030340363_gshared (BackgroundDispatcher_1_t2693883954 * __this, const MethodInfo* method);
#define BackgroundDispatcher_1_Run_m3030340363(__this, method) ((  void (*) (BackgroundDispatcher_1_t2693883954 *, const MethodInfo*))BackgroundDispatcher_1_Run_m3030340363_gshared)(__this, method)
// System.Void Amazon.Runtime.Internal.Util.BackgroundDispatcher`1<System.Object>::HandleInvoked()
extern "C"  void BackgroundDispatcher_1_HandleInvoked_m4068660570_gshared (BackgroundDispatcher_1_t2693883954 * __this, const MethodInfo* method);
#define BackgroundDispatcher_1_HandleInvoked_m4068660570(__this, method) ((  void (*) (BackgroundDispatcher_1_t2693883954 *, const MethodInfo*))BackgroundDispatcher_1_HandleInvoked_m4068660570_gshared)(__this, method)
