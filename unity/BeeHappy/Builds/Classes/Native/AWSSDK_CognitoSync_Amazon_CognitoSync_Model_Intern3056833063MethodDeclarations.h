﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.CognitoSync.Model.Internal.MarshallTransformations.RecordUnmarshaller
struct RecordUnmarshaller_t3056833063;
// Amazon.CognitoSync.Model.Record
struct Record_t441586865;
// Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext
struct XmlUnmarshallerContext_t1179575220;
// Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext
struct JsonUnmarshallerContext_t456235889;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_XmlU1179575220.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_JsonU456235889.h"

// Amazon.CognitoSync.Model.Record Amazon.CognitoSync.Model.Internal.MarshallTransformations.RecordUnmarshaller::Amazon.Runtime.Internal.Transform.IUnmarshaller<Amazon.CognitoSync.Model.Record,Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext>.Unmarshall(Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext)
extern "C"  Record_t441586865 * RecordUnmarshaller_Amazon_Runtime_Internal_Transform_IUnmarshallerU3CAmazon_CognitoSync_Model_RecordU2CAmazon_Runtime_Internal_Transform_XmlUnmarshallerContextU3E_Unmarshall_m1187390151 (RecordUnmarshaller_t3056833063 * __this, XmlUnmarshallerContext_t1179575220 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.CognitoSync.Model.Record Amazon.CognitoSync.Model.Internal.MarshallTransformations.RecordUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern "C"  Record_t441586865 * RecordUnmarshaller_Unmarshall_m3155437287 (RecordUnmarshaller_t3056833063 * __this, JsonUnmarshallerContext_t456235889 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.CognitoSync.Model.Internal.MarshallTransformations.RecordUnmarshaller Amazon.CognitoSync.Model.Internal.MarshallTransformations.RecordUnmarshaller::get_Instance()
extern "C"  RecordUnmarshaller_t3056833063 * RecordUnmarshaller_get_Instance_m883318935 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Model.Internal.MarshallTransformations.RecordUnmarshaller::.ctor()
extern "C"  void RecordUnmarshaller__ctor_m2869822083 (RecordUnmarshaller_t3056833063 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Model.Internal.MarshallTransformations.RecordUnmarshaller::.cctor()
extern "C"  void RecordUnmarshaller__cctor_m1165574258 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
