﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23277180024MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m1288980636(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t1327510497 *, Type_t *, int32_t, const MethodInfo*))KeyValuePair_2__ctor_m3044907881_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Type,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::get_Key()
#define KeyValuePair_2_get_Key_m595275343(__this, method) ((  Type_t * (*) (KeyValuePair_2_t1327510497 *, const MethodInfo*))KeyValuePair_2_get_Key_m4122353311_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m2627470921(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1327510497 *, Type_t *, const MethodInfo*))KeyValuePair_2_set_Key_m696936972_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Type,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::get_Value()
#define KeyValuePair_2_get_Value_m3335388172(__this, method) ((  int32_t (*) (KeyValuePair_2_t1327510497 *, const MethodInfo*))KeyValuePair_2_get_Value_m116637759_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1465116289(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1327510497 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Value_m590249628_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Type,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::ToString()
#define KeyValuePair_2_ToString_m14936471(__this, method) ((  String_t* (*) (KeyValuePair_2_t1327510497 *, const MethodInfo*))KeyValuePair_2_ToString_m2693555248_gshared)(__this, method)
