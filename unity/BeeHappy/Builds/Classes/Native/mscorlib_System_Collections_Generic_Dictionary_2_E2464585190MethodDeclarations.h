﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Object>
struct Dictionary_2_t1144560488;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2464585190.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23196873006.h"
#include "AWSSDK_Core_Amazon_Runtime_Metric3273440202.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2882573199_gshared (Enumerator_t2464585190 * __this, Dictionary_2_t1144560488 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m2882573199(__this, ___dictionary0, method) ((  void (*) (Enumerator_t2464585190 *, Dictionary_2_t1144560488 *, const MethodInfo*))Enumerator__ctor_m2882573199_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2753081182_gshared (Enumerator_t2464585190 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2753081182(__this, method) ((  Il2CppObject * (*) (Enumerator_t2464585190 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2753081182_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3448018626_gshared (Enumerator_t2464585190 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3448018626(__this, method) ((  void (*) (Enumerator_t2464585190 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3448018626_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1386174965_gshared (Enumerator_t2464585190 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1386174965(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t2464585190 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1386174965_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3637591552_gshared (Enumerator_t2464585190 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3637591552(__this, method) ((  Il2CppObject * (*) (Enumerator_t2464585190 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3637591552_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1974664938_gshared (Enumerator_t2464585190 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1974664938(__this, method) ((  Il2CppObject * (*) (Enumerator_t2464585190 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1974664938_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3844071554_gshared (Enumerator_t2464585190 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3844071554(__this, method) ((  bool (*) (Enumerator_t2464585190 *, const MethodInfo*))Enumerator_MoveNext_m3844071554_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t3196873006  Enumerator_get_Current_m1831421754_gshared (Enumerator_t2464585190 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1831421754(__this, method) ((  KeyValuePair_2_t3196873006  (*) (Enumerator_t2464585190 *, const MethodInfo*))Enumerator_get_Current_m1831421754_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Object>::get_CurrentKey()
extern "C"  int32_t Enumerator_get_CurrentKey_m2750410819_gshared (Enumerator_t2464585190 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m2750410819(__this, method) ((  int32_t (*) (Enumerator_t2464585190 *, const MethodInfo*))Enumerator_get_CurrentKey_m2750410819_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Object>::get_CurrentValue()
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m4218670947_gshared (Enumerator_t2464585190 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m4218670947(__this, method) ((  Il2CppObject * (*) (Enumerator_t2464585190 *, const MethodInfo*))Enumerator_get_CurrentValue_m4218670947_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Object>::Reset()
extern "C"  void Enumerator_Reset_m1867529997_gshared (Enumerator_t2464585190 * __this, const MethodInfo* method);
#define Enumerator_Reset_m1867529997(__this, method) ((  void (*) (Enumerator_t2464585190 *, const MethodInfo*))Enumerator_Reset_m1867529997_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Object>::VerifyState()
extern "C"  void Enumerator_VerifyState_m1404756950_gshared (Enumerator_t2464585190 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m1404756950(__this, method) ((  void (*) (Enumerator_t2464585190 *, const MethodInfo*))Enumerator_VerifyState_m1404756950_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Object>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m4158200186_gshared (Enumerator_t2464585190 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m4158200186(__this, method) ((  void (*) (Enumerator_t2464585190 *, const MethodInfo*))Enumerator_VerifyCurrent_m4158200186_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m2167966803_gshared (Enumerator_t2464585190 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2167966803(__this, method) ((  void (*) (Enumerator_t2464585190 *, const MethodInfo*))Enumerator_Dispose_m2167966803_gshared)(__this, method)
