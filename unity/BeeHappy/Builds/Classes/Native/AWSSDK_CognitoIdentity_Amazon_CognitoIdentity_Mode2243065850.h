﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetIdResponseUnmarshaller
struct GetIdResponseUnmarshaller_t2243065850;

#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_Json2685947887.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetIdResponseUnmarshaller
struct  GetIdResponseUnmarshaller_t2243065850  : public JsonResponseUnmarshaller_t2685947887
{
public:

public:
};

struct GetIdResponseUnmarshaller_t2243065850_StaticFields
{
public:
	// Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetIdResponseUnmarshaller Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetIdResponseUnmarshaller::_instance
	GetIdResponseUnmarshaller_t2243065850 * ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(GetIdResponseUnmarshaller_t2243065850_StaticFields, ____instance_0)); }
	inline GetIdResponseUnmarshaller_t2243065850 * get__instance_0() const { return ____instance_0; }
	inline GetIdResponseUnmarshaller_t2243065850 ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(GetIdResponseUnmarshaller_t2243065850 * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier(&____instance_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
