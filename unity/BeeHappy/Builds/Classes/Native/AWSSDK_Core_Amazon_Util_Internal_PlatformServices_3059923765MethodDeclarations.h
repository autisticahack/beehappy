﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Util.Internal.PlatformServices.NetworkReachability
struct NetworkReachability_t3059923765;
// System.EventHandler`1<Amazon.Util.Internal.PlatformServices.NetworkStatusEventArgs>
struct EventHandler_1_t198474825;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_Util_Internal_PlatformServices_N879095062.h"

// Amazon.Util.Internal.PlatformServices.NetworkStatus Amazon.Util.Internal.PlatformServices.NetworkReachability::get_NetworkStatus()
extern "C"  int32_t NetworkReachability_get_NetworkStatus_m1575079198 (NetworkReachability_t3059923765 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Util.Internal.PlatformServices.NetworkReachability::add_NetworkReachabilityChanged(System.EventHandler`1<Amazon.Util.Internal.PlatformServices.NetworkStatusEventArgs>)
extern "C"  void NetworkReachability_add_NetworkReachabilityChanged_m2672281924 (NetworkReachability_t3059923765 * __this, EventHandler_1_t198474825 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Util.Internal.PlatformServices.NetworkReachability::remove_NetworkReachabilityChanged(System.EventHandler`1<Amazon.Util.Internal.PlatformServices.NetworkStatusEventArgs>)
extern "C"  void NetworkReachability_remove_NetworkReachabilityChanged_m3029116953 (NetworkReachability_t3059923765 * __this, EventHandler_1_t198474825 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Util.Internal.PlatformServices.NetworkReachability::OnNetworkReachabilityChanged(Amazon.Util.Internal.PlatformServices.NetworkStatus)
extern "C"  void NetworkReachability_OnNetworkReachabilityChanged_m2505659662 (NetworkReachability_t3059923765 * __this, int32_t ___status0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Util.Internal.PlatformServices.NetworkReachability::.cctor()
extern "C"  void NetworkReachability__cctor_m356069701 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
