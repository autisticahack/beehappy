﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.CognitoSync.Model.Internal.MarshallTransformations.UpdateRecordsResponseUnmarshaller
struct UpdateRecordsResponseUnmarshaller_t3670264418;
// Amazon.Runtime.AmazonWebServiceResponse
struct AmazonWebServiceResponse_t529043356;
// Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext
struct JsonUnmarshallerContext_t456235889;
// Amazon.Runtime.AmazonServiceException
struct AmazonServiceException_t3748559634;
// System.Exception
struct Exception_t1927440687;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_JsonU456235889.h"
#include "mscorlib_System_Exception1927440687.h"
#include "System_System_Net_HttpStatusCode1898409641.h"

// Amazon.Runtime.AmazonWebServiceResponse Amazon.CognitoSync.Model.Internal.MarshallTransformations.UpdateRecordsResponseUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern "C"  AmazonWebServiceResponse_t529043356 * UpdateRecordsResponseUnmarshaller_Unmarshall_m3711320138 (UpdateRecordsResponseUnmarshaller_t3670264418 * __this, JsonUnmarshallerContext_t456235889 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.AmazonServiceException Amazon.CognitoSync.Model.Internal.MarshallTransformations.UpdateRecordsResponseUnmarshaller::UnmarshallException(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,System.Exception,System.Net.HttpStatusCode)
extern "C"  AmazonServiceException_t3748559634 * UpdateRecordsResponseUnmarshaller_UnmarshallException_m2589398612 (UpdateRecordsResponseUnmarshaller_t3670264418 * __this, JsonUnmarshallerContext_t456235889 * ___context0, Exception_t1927440687 * ___innerException1, int32_t ___statusCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.CognitoSync.Model.Internal.MarshallTransformations.UpdateRecordsResponseUnmarshaller Amazon.CognitoSync.Model.Internal.MarshallTransformations.UpdateRecordsResponseUnmarshaller::get_Instance()
extern "C"  UpdateRecordsResponseUnmarshaller_t3670264418 * UpdateRecordsResponseUnmarshaller_get_Instance_m3035537883 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Model.Internal.MarshallTransformations.UpdateRecordsResponseUnmarshaller::.ctor()
extern "C"  void UpdateRecordsResponseUnmarshaller__ctor_m410896848 (UpdateRecordsResponseUnmarshaller_t3670264418 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Model.Internal.MarshallTransformations.UpdateRecordsResponseUnmarshaller::.cctor()
extern "C"  void UpdateRecordsResponseUnmarshaller__cctor_m4077563469 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
