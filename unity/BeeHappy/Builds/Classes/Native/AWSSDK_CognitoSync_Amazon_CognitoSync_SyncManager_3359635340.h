﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.Runtime.Internal.Util.Logger
struct Logger_t2262497814;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// Mono.Data.Sqlite.SqliteConnection
struct SqliteConnection_t1726730022;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage
struct  SQLiteLocalStorage_t3359635340  : public Il2CppObject
{
public:
	// Amazon.Runtime.Internal.Util.Logger Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage::_logger
	Logger_t2262497814 * ____logger_0;
	// System.String Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage::directoryPath
	String_t* ___directoryPath_2;
	// System.String Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage::filePath
	String_t* ___filePath_3;
	// Mono.Data.Sqlite.SqliteConnection Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage::connection
	SqliteConnection_t1726730022 * ___connection_4;

public:
	inline static int32_t get_offset_of__logger_0() { return static_cast<int32_t>(offsetof(SQLiteLocalStorage_t3359635340, ____logger_0)); }
	inline Logger_t2262497814 * get__logger_0() const { return ____logger_0; }
	inline Logger_t2262497814 ** get_address_of__logger_0() { return &____logger_0; }
	inline void set__logger_0(Logger_t2262497814 * value)
	{
		____logger_0 = value;
		Il2CppCodeGenWriteBarrier(&____logger_0, value);
	}

	inline static int32_t get_offset_of_directoryPath_2() { return static_cast<int32_t>(offsetof(SQLiteLocalStorage_t3359635340, ___directoryPath_2)); }
	inline String_t* get_directoryPath_2() const { return ___directoryPath_2; }
	inline String_t** get_address_of_directoryPath_2() { return &___directoryPath_2; }
	inline void set_directoryPath_2(String_t* value)
	{
		___directoryPath_2 = value;
		Il2CppCodeGenWriteBarrier(&___directoryPath_2, value);
	}

	inline static int32_t get_offset_of_filePath_3() { return static_cast<int32_t>(offsetof(SQLiteLocalStorage_t3359635340, ___filePath_3)); }
	inline String_t* get_filePath_3() const { return ___filePath_3; }
	inline String_t** get_address_of_filePath_3() { return &___filePath_3; }
	inline void set_filePath_3(String_t* value)
	{
		___filePath_3 = value;
		Il2CppCodeGenWriteBarrier(&___filePath_3, value);
	}

	inline static int32_t get_offset_of_connection_4() { return static_cast<int32_t>(offsetof(SQLiteLocalStorage_t3359635340, ___connection_4)); }
	inline SqliteConnection_t1726730022 * get_connection_4() const { return ___connection_4; }
	inline SqliteConnection_t1726730022 ** get_address_of_connection_4() { return &___connection_4; }
	inline void set_connection_4(SqliteConnection_t1726730022 * value)
	{
		___connection_4 = value;
		Il2CppCodeGenWriteBarrier(&___connection_4, value);
	}
};

struct SQLiteLocalStorage_t3359635340_StaticFields
{
public:
	// System.Object Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage::sqlite_lock
	Il2CppObject * ___sqlite_lock_1;

public:
	inline static int32_t get_offset_of_sqlite_lock_1() { return static_cast<int32_t>(offsetof(SQLiteLocalStorage_t3359635340_StaticFields, ___sqlite_lock_1)); }
	inline Il2CppObject * get_sqlite_lock_1() const { return ___sqlite_lock_1; }
	inline Il2CppObject ** get_address_of_sqlite_lock_1() { return &___sqlite_lock_1; }
	inline void set_sqlite_lock_1(Il2CppObject * value)
	{
		___sqlite_lock_1 = value;
		Il2CppCodeGenWriteBarrier(&___sqlite_lock_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
