﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.ErrorCallbackHandler
struct ErrorCallbackHandler_t394665619;
// System.Action`2<Amazon.Runtime.IExecutionContext,System.Exception>
struct Action_2_t3657272536;
// Amazon.Runtime.IExecutionContext
struct IExecutionContext_t2477130752;
// Amazon.Runtime.IAsyncExecutionContext
struct IAsyncExecutionContext_t3792344986;
// System.Exception
struct Exception_t1927440687;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1927440687.h"

// System.Action`2<Amazon.Runtime.IExecutionContext,System.Exception> Amazon.Runtime.Internal.ErrorCallbackHandler::get_OnError()
extern "C"  Action_2_t3657272536 * ErrorCallbackHandler_get_OnError_m134569557 (ErrorCallbackHandler_t394665619 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.ErrorCallbackHandler::set_OnError(System.Action`2<Amazon.Runtime.IExecutionContext,System.Exception>)
extern "C"  void ErrorCallbackHandler_set_OnError_m2174659240 (ErrorCallbackHandler_t394665619 * __this, Action_2_t3657272536 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.ErrorCallbackHandler::InvokeSync(Amazon.Runtime.IExecutionContext)
extern "C"  void ErrorCallbackHandler_InvokeSync_m4101850372 (ErrorCallbackHandler_t394665619 * __this, Il2CppObject * ___executionContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.ErrorCallbackHandler::InvokeAsyncCallback(Amazon.Runtime.IAsyncExecutionContext)
extern "C"  void ErrorCallbackHandler_InvokeAsyncCallback_m1411473448 (ErrorCallbackHandler_t394665619 * __this, Il2CppObject * ___executionContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.ErrorCallbackHandler::HandleException(Amazon.Runtime.IExecutionContext,System.Exception)
extern "C"  void ErrorCallbackHandler_HandleException_m2163380876 (ErrorCallbackHandler_t394665619 * __this, Il2CppObject * ___executionContext0, Exception_t1927440687 * ___exception1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.ErrorCallbackHandler::.ctor()
extern "C"  void ErrorCallbackHandler__ctor_m2074753445 (ErrorCallbackHandler_t394665619 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
