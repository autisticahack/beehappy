﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SpinningCube4269489377.h"
#include "AssemblyU2DCSharp_UpdateText2942283158.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3100 = { sizeof (SpinningCube_t4269489377), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3100[2] = 
{
	SpinningCube_t4269489377::get_offset_of_m_Speed_2(),
	SpinningCube_t4269489377::get_offset_of_m_RotationDirection_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3101 = { sizeof (UpdateText_t2942283158), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3101[1] = 
{
	UpdateText_t2942283158::get_offset_of_Text_2(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
