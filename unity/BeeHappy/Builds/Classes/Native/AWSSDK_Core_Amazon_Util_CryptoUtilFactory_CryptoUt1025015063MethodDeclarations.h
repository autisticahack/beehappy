﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Util.CryptoUtilFactory/CryptoUtil
struct CryptoUtil_t1025015063;
// System.Security.Cryptography.KeyedHashAlgorithm
struct KeyedHashAlgorithm_t1374150027;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.IO.Stream
struct Stream_t3255436806;
// System.Security.Cryptography.HashAlgorithm
struct HashAlgorithm_t2624936259;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_Runtime_SigningAlgorithm3740229458.h"
#include "mscorlib_System_IO_Stream3255436806.h"

// System.Void Amazon.Util.CryptoUtilFactory/CryptoUtil::.ctor()
extern "C"  void CryptoUtil__ctor_m3930988002 (CryptoUtil_t1025015063 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.KeyedHashAlgorithm Amazon.Util.CryptoUtilFactory/CryptoUtil::ThreadSafeCreateKeyedHashedAlgorithm(Amazon.Runtime.SigningAlgorithm)
extern "C"  KeyedHashAlgorithm_t1374150027 * CryptoUtil_ThreadSafeCreateKeyedHashedAlgorithm_m2685900100 (CryptoUtil_t1025015063 * __this, int32_t ___algorithmName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Amazon.Util.CryptoUtilFactory/CryptoUtil::ComputeSHA256Hash(System.Byte[])
extern "C"  ByteU5BU5D_t3397334013* CryptoUtil_ComputeSHA256Hash_m3032844001 (CryptoUtil_t1025015063 * __this, ByteU5BU5D_t3397334013* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Amazon.Util.CryptoUtilFactory/CryptoUtil::ComputeSHA256Hash(System.IO.Stream)
extern "C"  ByteU5BU5D_t3397334013* CryptoUtil_ComputeSHA256Hash_m2568609377 (CryptoUtil_t1025015063 * __this, Stream_t3255436806 * ___steam0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Amazon.Util.CryptoUtilFactory/CryptoUtil::HMACSignBinary(System.Byte[],System.Byte[],Amazon.Runtime.SigningAlgorithm)
extern "C"  ByteU5BU5D_t3397334013* CryptoUtil_HMACSignBinary_m1089082359 (CryptoUtil_t1025015063 * __this, ByteU5BU5D_t3397334013* ___data0, ByteU5BU5D_t3397334013* ___key1, int32_t ___algorithmName2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.HashAlgorithm Amazon.Util.CryptoUtilFactory/CryptoUtil::get_SHA256HashAlgorithmInstance()
extern "C"  HashAlgorithm_t2624936259 * CryptoUtil_get_SHA256HashAlgorithmInstance_m683410281 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Util.CryptoUtilFactory/CryptoUtil::.cctor()
extern "C"  void CryptoUtil__cctor_m1574473153 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
