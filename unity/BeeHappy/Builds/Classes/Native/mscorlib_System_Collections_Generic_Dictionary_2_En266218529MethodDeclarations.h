﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3601534125MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Type>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m2074800571(__this, ___dictionary0, method) ((  void (*) (Enumerator_t266218529 *, Dictionary_2_t3241161123 *, const MethodInfo*))Enumerator__ctor_m3742107451_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Type>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3897704524(__this, method) ((  Il2CppObject * (*) (Enumerator_t266218529 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m229223308_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Type>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3772920104(__this, method) ((  void (*) (Enumerator_t266218529 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3225937576_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Type>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3773191605(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t266218529 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m221119093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Type>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2447632202(__this, method) ((  Il2CppObject * (*) (Enumerator_t266218529 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m467957770_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Type>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3230134208(__this, method) ((  Il2CppObject * (*) (Enumerator_t266218529 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2325383168_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Type>::MoveNext()
#define Enumerator_MoveNext_m1160159432(__this, method) ((  bool (*) (Enumerator_t266218529 *, const MethodInfo*))Enumerator_MoveNext_m3349738440_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Type>::get_Current()
#define Enumerator_get_Current_m628680624(__this, method) ((  KeyValuePair_2_t998506345  (*) (Enumerator_t266218529 *, const MethodInfo*))Enumerator_get_Current_m25299632_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Type>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m3868191495(__this, method) ((  Type_t * (*) (Enumerator_t266218529 *, const MethodInfo*))Enumerator_get_CurrentKey_m3839846791_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Type>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m3827123879(__this, method) ((  Type_t * (*) (Enumerator_t266218529 *, const MethodInfo*))Enumerator_get_CurrentValue_m402763047_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Type>::Reset()
#define Enumerator_Reset_m164088893(__this, method) ((  void (*) (Enumerator_t266218529 *, const MethodInfo*))Enumerator_Reset_m3129803197_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Type>::VerifyState()
#define Enumerator_VerifyState_m1000850484(__this, method) ((  void (*) (Enumerator_t266218529 *, const MethodInfo*))Enumerator_VerifyState_m262343092_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Type>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m303260912(__this, method) ((  void (*) (Enumerator_t266218529 *, const MethodInfo*))Enumerator_VerifyCurrent_m1702320752_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Type>::Dispose()
#define Enumerator_Dispose_m2820686903(__this, method) ((  void (*) (Enumerator_t266218529 *, const MethodInfo*))Enumerator_Dispose_m1905011127_gshared)(__this, method)
