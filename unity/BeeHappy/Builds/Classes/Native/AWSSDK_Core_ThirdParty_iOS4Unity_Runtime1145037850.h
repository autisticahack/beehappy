﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// System.Collections.Generic.Dictionary`2<System.Type,System.Func`2<System.IntPtr,System.Object>>
struct Dictionary_2_t1317859972;
// System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>
struct Dictionary_2_t3131474613;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.iOS4Unity.Runtime
struct  Runtime_t1145037850  : public Il2CppObject
{
public:

public:
};

struct Runtime_t1145037850_StaticFields
{
public:
	// System.Object ThirdParty.iOS4Unity.Runtime::_contructorLock
	Il2CppObject * ____contructorLock_0;
	// System.Object ThirdParty.iOS4Unity.Runtime::_objectLock
	Il2CppObject * ____objectLock_1;
	// System.Collections.Generic.Dictionary`2<System.Type,System.Func`2<System.IntPtr,System.Object>> ThirdParty.iOS4Unity.Runtime::_constructors
	Dictionary_2_t1317859972 * ____constructors_2;
	// System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object> ThirdParty.iOS4Unity.Runtime::_objects
	Dictionary_2_t3131474613 * ____objects_3;

public:
	inline static int32_t get_offset_of__contructorLock_0() { return static_cast<int32_t>(offsetof(Runtime_t1145037850_StaticFields, ____contructorLock_0)); }
	inline Il2CppObject * get__contructorLock_0() const { return ____contructorLock_0; }
	inline Il2CppObject ** get_address_of__contructorLock_0() { return &____contructorLock_0; }
	inline void set__contructorLock_0(Il2CppObject * value)
	{
		____contructorLock_0 = value;
		Il2CppCodeGenWriteBarrier(&____contructorLock_0, value);
	}

	inline static int32_t get_offset_of__objectLock_1() { return static_cast<int32_t>(offsetof(Runtime_t1145037850_StaticFields, ____objectLock_1)); }
	inline Il2CppObject * get__objectLock_1() const { return ____objectLock_1; }
	inline Il2CppObject ** get_address_of__objectLock_1() { return &____objectLock_1; }
	inline void set__objectLock_1(Il2CppObject * value)
	{
		____objectLock_1 = value;
		Il2CppCodeGenWriteBarrier(&____objectLock_1, value);
	}

	inline static int32_t get_offset_of__constructors_2() { return static_cast<int32_t>(offsetof(Runtime_t1145037850_StaticFields, ____constructors_2)); }
	inline Dictionary_2_t1317859972 * get__constructors_2() const { return ____constructors_2; }
	inline Dictionary_2_t1317859972 ** get_address_of__constructors_2() { return &____constructors_2; }
	inline void set__constructors_2(Dictionary_2_t1317859972 * value)
	{
		____constructors_2 = value;
		Il2CppCodeGenWriteBarrier(&____constructors_2, value);
	}

	inline static int32_t get_offset_of__objects_3() { return static_cast<int32_t>(offsetof(Runtime_t1145037850_StaticFields, ____objects_3)); }
	inline Dictionary_2_t3131474613 * get__objects_3() const { return ____objects_3; }
	inline Dictionary_2_t3131474613 ** get_address_of__objects_3() { return &____objects_3; }
	inline void set__objects_3(Dictionary_2_t3131474613 * value)
	{
		____objects_3 = value;
		Il2CppCodeGenWriteBarrier(&____objects_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
