﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Uri
struct Uri_t19570940;
// System.IDisposable
struct IDisposable_t2427283555;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.Threading.ManualResetEvent
struct ManualResetEvent_t926074657;
// Amazon.Runtime.Internal.Transform.IWebResponseData
struct IWebResponseData_t3815728244;
// System.Exception
struct Exception_t1927440687;
// System.String
struct String_t;
// Amazon.Runtime.Internal.StreamReadTracker
struct StreamReadTracker_t1958363340;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.UnityRequest
struct  UnityRequest_t4218620704  : public Il2CppObject
{
public:
	// System.Uri Amazon.Runtime.Internal.UnityRequest::<RequestUri>k__BackingField
	Uri_t19570940 * ___U3CRequestUriU3Ek__BackingField_0;
	// System.IDisposable Amazon.Runtime.Internal.UnityRequest::<WwwRequest>k__BackingField
	Il2CppObject * ___U3CWwwRequestU3Ek__BackingField_1;
	// System.Byte[] Amazon.Runtime.Internal.UnityRequest::<RequestContent>k__BackingField
	ByteU5BU5D_t3397334013* ___U3CRequestContentU3Ek__BackingField_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> Amazon.Runtime.Internal.UnityRequest::<Headers>k__BackingField
	Dictionary_2_t3943999495 * ___U3CHeadersU3Ek__BackingField_3;
	// System.AsyncCallback Amazon.Runtime.Internal.UnityRequest::<Callback>k__BackingField
	AsyncCallback_t163412349 * ___U3CCallbackU3Ek__BackingField_4;
	// System.IAsyncResult Amazon.Runtime.Internal.UnityRequest::<AsyncResult>k__BackingField
	Il2CppObject * ___U3CAsyncResultU3Ek__BackingField_5;
	// System.Threading.ManualResetEvent Amazon.Runtime.Internal.UnityRequest::<WaitHandle>k__BackingField
	ManualResetEvent_t926074657 * ___U3CWaitHandleU3Ek__BackingField_6;
	// System.Boolean Amazon.Runtime.Internal.UnityRequest::<IsSync>k__BackingField
	bool ___U3CIsSyncU3Ek__BackingField_7;
	// Amazon.Runtime.Internal.Transform.IWebResponseData Amazon.Runtime.Internal.UnityRequest::<Response>k__BackingField
	Il2CppObject * ___U3CResponseU3Ek__BackingField_8;
	// System.Exception Amazon.Runtime.Internal.UnityRequest::<Exception>k__BackingField
	Exception_t1927440687 * ___U3CExceptionU3Ek__BackingField_9;
	// System.String Amazon.Runtime.Internal.UnityRequest::<Method>k__BackingField
	String_t* ___U3CMethodU3Ek__BackingField_10;
	// System.Boolean Amazon.Runtime.Internal.UnityRequest::_disposed
	bool ____disposed_11;
	// Amazon.Runtime.Internal.StreamReadTracker Amazon.Runtime.Internal.UnityRequest::<Tracker>k__BackingField
	StreamReadTracker_t1958363340 * ___U3CTrackerU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_U3CRequestUriU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(UnityRequest_t4218620704, ___U3CRequestUriU3Ek__BackingField_0)); }
	inline Uri_t19570940 * get_U3CRequestUriU3Ek__BackingField_0() const { return ___U3CRequestUriU3Ek__BackingField_0; }
	inline Uri_t19570940 ** get_address_of_U3CRequestUriU3Ek__BackingField_0() { return &___U3CRequestUriU3Ek__BackingField_0; }
	inline void set_U3CRequestUriU3Ek__BackingField_0(Uri_t19570940 * value)
	{
		___U3CRequestUriU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CRequestUriU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CWwwRequestU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(UnityRequest_t4218620704, ___U3CWwwRequestU3Ek__BackingField_1)); }
	inline Il2CppObject * get_U3CWwwRequestU3Ek__BackingField_1() const { return ___U3CWwwRequestU3Ek__BackingField_1; }
	inline Il2CppObject ** get_address_of_U3CWwwRequestU3Ek__BackingField_1() { return &___U3CWwwRequestU3Ek__BackingField_1; }
	inline void set_U3CWwwRequestU3Ek__BackingField_1(Il2CppObject * value)
	{
		___U3CWwwRequestU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CWwwRequestU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CRequestContentU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(UnityRequest_t4218620704, ___U3CRequestContentU3Ek__BackingField_2)); }
	inline ByteU5BU5D_t3397334013* get_U3CRequestContentU3Ek__BackingField_2() const { return ___U3CRequestContentU3Ek__BackingField_2; }
	inline ByteU5BU5D_t3397334013** get_address_of_U3CRequestContentU3Ek__BackingField_2() { return &___U3CRequestContentU3Ek__BackingField_2; }
	inline void set_U3CRequestContentU3Ek__BackingField_2(ByteU5BU5D_t3397334013* value)
	{
		___U3CRequestContentU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CRequestContentU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CHeadersU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(UnityRequest_t4218620704, ___U3CHeadersU3Ek__BackingField_3)); }
	inline Dictionary_2_t3943999495 * get_U3CHeadersU3Ek__BackingField_3() const { return ___U3CHeadersU3Ek__BackingField_3; }
	inline Dictionary_2_t3943999495 ** get_address_of_U3CHeadersU3Ek__BackingField_3() { return &___U3CHeadersU3Ek__BackingField_3; }
	inline void set_U3CHeadersU3Ek__BackingField_3(Dictionary_2_t3943999495 * value)
	{
		___U3CHeadersU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CHeadersU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(UnityRequest_t4218620704, ___U3CCallbackU3Ek__BackingField_4)); }
	inline AsyncCallback_t163412349 * get_U3CCallbackU3Ek__BackingField_4() const { return ___U3CCallbackU3Ek__BackingField_4; }
	inline AsyncCallback_t163412349 ** get_address_of_U3CCallbackU3Ek__BackingField_4() { return &___U3CCallbackU3Ek__BackingField_4; }
	inline void set_U3CCallbackU3Ek__BackingField_4(AsyncCallback_t163412349 * value)
	{
		___U3CCallbackU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCallbackU3Ek__BackingField_4, value);
	}

	inline static int32_t get_offset_of_U3CAsyncResultU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(UnityRequest_t4218620704, ___U3CAsyncResultU3Ek__BackingField_5)); }
	inline Il2CppObject * get_U3CAsyncResultU3Ek__BackingField_5() const { return ___U3CAsyncResultU3Ek__BackingField_5; }
	inline Il2CppObject ** get_address_of_U3CAsyncResultU3Ek__BackingField_5() { return &___U3CAsyncResultU3Ek__BackingField_5; }
	inline void set_U3CAsyncResultU3Ek__BackingField_5(Il2CppObject * value)
	{
		___U3CAsyncResultU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CAsyncResultU3Ek__BackingField_5, value);
	}

	inline static int32_t get_offset_of_U3CWaitHandleU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(UnityRequest_t4218620704, ___U3CWaitHandleU3Ek__BackingField_6)); }
	inline ManualResetEvent_t926074657 * get_U3CWaitHandleU3Ek__BackingField_6() const { return ___U3CWaitHandleU3Ek__BackingField_6; }
	inline ManualResetEvent_t926074657 ** get_address_of_U3CWaitHandleU3Ek__BackingField_6() { return &___U3CWaitHandleU3Ek__BackingField_6; }
	inline void set_U3CWaitHandleU3Ek__BackingField_6(ManualResetEvent_t926074657 * value)
	{
		___U3CWaitHandleU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CWaitHandleU3Ek__BackingField_6, value);
	}

	inline static int32_t get_offset_of_U3CIsSyncU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(UnityRequest_t4218620704, ___U3CIsSyncU3Ek__BackingField_7)); }
	inline bool get_U3CIsSyncU3Ek__BackingField_7() const { return ___U3CIsSyncU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CIsSyncU3Ek__BackingField_7() { return &___U3CIsSyncU3Ek__BackingField_7; }
	inline void set_U3CIsSyncU3Ek__BackingField_7(bool value)
	{
		___U3CIsSyncU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CResponseU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(UnityRequest_t4218620704, ___U3CResponseU3Ek__BackingField_8)); }
	inline Il2CppObject * get_U3CResponseU3Ek__BackingField_8() const { return ___U3CResponseU3Ek__BackingField_8; }
	inline Il2CppObject ** get_address_of_U3CResponseU3Ek__BackingField_8() { return &___U3CResponseU3Ek__BackingField_8; }
	inline void set_U3CResponseU3Ek__BackingField_8(Il2CppObject * value)
	{
		___U3CResponseU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CResponseU3Ek__BackingField_8, value);
	}

	inline static int32_t get_offset_of_U3CExceptionU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(UnityRequest_t4218620704, ___U3CExceptionU3Ek__BackingField_9)); }
	inline Exception_t1927440687 * get_U3CExceptionU3Ek__BackingField_9() const { return ___U3CExceptionU3Ek__BackingField_9; }
	inline Exception_t1927440687 ** get_address_of_U3CExceptionU3Ek__BackingField_9() { return &___U3CExceptionU3Ek__BackingField_9; }
	inline void set_U3CExceptionU3Ek__BackingField_9(Exception_t1927440687 * value)
	{
		___U3CExceptionU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CExceptionU3Ek__BackingField_9, value);
	}

	inline static int32_t get_offset_of_U3CMethodU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(UnityRequest_t4218620704, ___U3CMethodU3Ek__BackingField_10)); }
	inline String_t* get_U3CMethodU3Ek__BackingField_10() const { return ___U3CMethodU3Ek__BackingField_10; }
	inline String_t** get_address_of_U3CMethodU3Ek__BackingField_10() { return &___U3CMethodU3Ek__BackingField_10; }
	inline void set_U3CMethodU3Ek__BackingField_10(String_t* value)
	{
		___U3CMethodU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CMethodU3Ek__BackingField_10, value);
	}

	inline static int32_t get_offset_of__disposed_11() { return static_cast<int32_t>(offsetof(UnityRequest_t4218620704, ____disposed_11)); }
	inline bool get__disposed_11() const { return ____disposed_11; }
	inline bool* get_address_of__disposed_11() { return &____disposed_11; }
	inline void set__disposed_11(bool value)
	{
		____disposed_11 = value;
	}

	inline static int32_t get_offset_of_U3CTrackerU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(UnityRequest_t4218620704, ___U3CTrackerU3Ek__BackingField_12)); }
	inline StreamReadTracker_t1958363340 * get_U3CTrackerU3Ek__BackingField_12() const { return ___U3CTrackerU3Ek__BackingField_12; }
	inline StreamReadTracker_t1958363340 ** get_address_of_U3CTrackerU3Ek__BackingField_12() { return &___U3CTrackerU3Ek__BackingField_12; }
	inline void set_U3CTrackerU3Ek__BackingField_12(StreamReadTracker_t1958363340 * value)
	{
		___U3CTrackerU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CTrackerU3Ek__BackingField_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
