﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.CognitoIdentity.Model.GetOpenIdTokenResponse
struct GetOpenIdTokenResponse_t955597635;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.String Amazon.CognitoIdentity.Model.GetOpenIdTokenResponse::get_IdentityId()
extern "C"  String_t* GetOpenIdTokenResponse_get_IdentityId_m146036971 (GetOpenIdTokenResponse_t955597635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoIdentity.Model.GetOpenIdTokenResponse::set_IdentityId(System.String)
extern "C"  void GetOpenIdTokenResponse_set_IdentityId_m1894373264 (GetOpenIdTokenResponse_t955597635 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.CognitoIdentity.Model.GetOpenIdTokenResponse::get_Token()
extern "C"  String_t* GetOpenIdTokenResponse_get_Token_m2941493527 (GetOpenIdTokenResponse_t955597635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoIdentity.Model.GetOpenIdTokenResponse::set_Token(System.String)
extern "C"  void GetOpenIdTokenResponse_set_Token_m40921210 (GetOpenIdTokenResponse_t955597635 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoIdentity.Model.GetOpenIdTokenResponse::.ctor()
extern "C"  void GetOpenIdTokenResponse__ctor_m3771882210 (GetOpenIdTokenResponse_t955597635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
