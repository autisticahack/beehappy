﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_BeardRotator_State2595507119.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BeardRotator
struct  BeardRotator_t1832164839  : public MonoBehaviour_t1158329972
{
public:
	// System.Single BeardRotator::RotationSpeed
	float ___RotationSpeed_2;
	// UnityEngine.UI.Text BeardRotator::Title
	Text_t356221433 * ___Title_3;
	// BeardRotator/State BeardRotator::m_BeardState
	int32_t ___m_BeardState_4;

public:
	inline static int32_t get_offset_of_RotationSpeed_2() { return static_cast<int32_t>(offsetof(BeardRotator_t1832164839, ___RotationSpeed_2)); }
	inline float get_RotationSpeed_2() const { return ___RotationSpeed_2; }
	inline float* get_address_of_RotationSpeed_2() { return &___RotationSpeed_2; }
	inline void set_RotationSpeed_2(float value)
	{
		___RotationSpeed_2 = value;
	}

	inline static int32_t get_offset_of_Title_3() { return static_cast<int32_t>(offsetof(BeardRotator_t1832164839, ___Title_3)); }
	inline Text_t356221433 * get_Title_3() const { return ___Title_3; }
	inline Text_t356221433 ** get_address_of_Title_3() { return &___Title_3; }
	inline void set_Title_3(Text_t356221433 * value)
	{
		___Title_3 = value;
		Il2CppCodeGenWriteBarrier(&___Title_3, value);
	}

	inline static int32_t get_offset_of_m_BeardState_4() { return static_cast<int32_t>(offsetof(BeardRotator_t1832164839, ___m_BeardState_4)); }
	inline int32_t get_m_BeardState_4() const { return ___m_BeardState_4; }
	inline int32_t* get_address_of_m_BeardState_4() { return &___m_BeardState_4; }
	inline void set_m_BeardState_4(int32_t value)
	{
		___m_BeardState_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
