﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Predicate_1_gen1132419410MethodDeclarations.h"

// System.Void System.Predicate`1<Amazon.Runtime.Internal.Util.MetricError>::.ctor(System.Object,System.IntPtr)
#define Predicate_1__ctor_m3660588384(__this, ___object0, ___method1, method) ((  void (*) (Predicate_1_t3702382217 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m2289454599_gshared)(__this, ___object0, ___method1, method)
// System.Boolean System.Predicate`1<Amazon.Runtime.Internal.Util.MetricError>::Invoke(T)
#define Predicate_1_Invoke_m687327768(__this, ___obj0, method) ((  bool (*) (Predicate_1_t3702382217 *, MetricError_t964444806 *, const MethodInfo*))Predicate_1_Invoke_m4047721271_gshared)(__this, ___obj0, method)
// System.IAsyncResult System.Predicate`1<Amazon.Runtime.Internal.Util.MetricError>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m2089279935(__this, ___obj0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Predicate_1_t3702382217 *, MetricError_t964444806 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Predicate_1_BeginInvoke_m3556950370_gshared)(__this, ___obj0, ___callback1, ___object2, method)
// System.Boolean System.Predicate`1<Amazon.Runtime.Internal.Util.MetricError>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m997665050(__this, ___result0, method) ((  bool (*) (Predicate_1_t3702382217 *, Il2CppObject *, const MethodInfo*))Predicate_1_EndInvoke_m3656575065_gshared)(__this, ___result0, method)
