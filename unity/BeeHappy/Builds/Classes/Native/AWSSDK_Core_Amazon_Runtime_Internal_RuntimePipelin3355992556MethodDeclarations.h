﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.RuntimePipeline
struct RuntimePipeline_t3355992556;
// Amazon.Runtime.IPipelineHandler
struct IPipelineHandler_t2320756001;
// System.Collections.Generic.IList`1<Amazon.Runtime.IPipelineHandler>
struct IList_1_t2861696602;
// Amazon.Runtime.Internal.Util.ILogger
struct ILogger_t676853571;
// Amazon.Runtime.IResponseContext
struct IResponseContext_t898521739;
// Amazon.Runtime.IExecutionContext
struct IExecutionContext_t2477130752;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// Amazon.Runtime.IAsyncExecutionContext
struct IAsyncExecutionContext_t3792344986;

#include "codegen/il2cpp-codegen.h"

// Amazon.Runtime.IPipelineHandler Amazon.Runtime.Internal.RuntimePipeline::get_Handler()
extern "C"  Il2CppObject * RuntimePipeline_get_Handler_m4062218801 (RuntimePipeline_t3355992556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.RuntimePipeline::.ctor(System.Collections.Generic.IList`1<Amazon.Runtime.IPipelineHandler>,Amazon.Runtime.Internal.Util.ILogger)
extern "C"  void RuntimePipeline__ctor_m4039979946 (RuntimePipeline_t3355992556 * __this, Il2CppObject* ___handlers0, Il2CppObject * ___logger1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.IResponseContext Amazon.Runtime.Internal.RuntimePipeline::InvokeSync(Amazon.Runtime.IExecutionContext)
extern "C"  Il2CppObject * RuntimePipeline_InvokeSync_m2643040767 (RuntimePipeline_t3355992556 * __this, Il2CppObject * ___executionContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Amazon.Runtime.Internal.RuntimePipeline::InvokeAsync(Amazon.Runtime.IAsyncExecutionContext)
extern "C"  Il2CppObject * RuntimePipeline_InvokeAsync_m2300571596 (RuntimePipeline_t3355992556 * __this, Il2CppObject * ___executionContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.RuntimePipeline::AddHandler(Amazon.Runtime.IPipelineHandler)
extern "C"  void RuntimePipeline_AddHandler_m489344122 (RuntimePipeline_t3355992556 * __this, Il2CppObject * ___handler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.RuntimePipeline::InsertHandler(Amazon.Runtime.IPipelineHandler,Amazon.Runtime.IPipelineHandler)
extern "C"  void RuntimePipeline_InsertHandler_m311879345 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___handler0, Il2CppObject * ___current1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.IPipelineHandler Amazon.Runtime.Internal.RuntimePipeline::GetInnermostHandler(Amazon.Runtime.IPipelineHandler)
extern "C"  Il2CppObject * RuntimePipeline_GetInnermostHandler_m2757998392 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___handler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.RuntimePipeline::SetHandlerProperties(Amazon.Runtime.IPipelineHandler)
extern "C"  void RuntimePipeline_SetHandlerProperties_m1082999518 (RuntimePipeline_t3355992556 * __this, Il2CppObject * ___handler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.RuntimePipeline::Dispose()
extern "C"  void RuntimePipeline_Dispose_m4293259213 (RuntimePipeline_t3355992556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.RuntimePipeline::Dispose(System.Boolean)
extern "C"  void RuntimePipeline_Dispose_m3350048696 (RuntimePipeline_t3355992556 * __this, bool ___disposing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.RuntimePipeline::ThrowIfDisposed()
extern "C"  void RuntimePipeline_ThrowIfDisposed_m3314933928 (RuntimePipeline_t3355992556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
