﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;

#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Amazo492659996.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.CognitoIdentity.Model.GetOpenIdTokenRequest
struct  GetOpenIdTokenRequest_t2698079901  : public AmazonCognitoIdentityRequest_t492659996
{
public:
	// System.String Amazon.CognitoIdentity.Model.GetOpenIdTokenRequest::_identityId
	String_t* ____identityId_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> Amazon.CognitoIdentity.Model.GetOpenIdTokenRequest::_logins
	Dictionary_2_t3943999495 * ____logins_4;

public:
	inline static int32_t get_offset_of__identityId_3() { return static_cast<int32_t>(offsetof(GetOpenIdTokenRequest_t2698079901, ____identityId_3)); }
	inline String_t* get__identityId_3() const { return ____identityId_3; }
	inline String_t** get_address_of__identityId_3() { return &____identityId_3; }
	inline void set__identityId_3(String_t* value)
	{
		____identityId_3 = value;
		Il2CppCodeGenWriteBarrier(&____identityId_3, value);
	}

	inline static int32_t get_offset_of__logins_4() { return static_cast<int32_t>(offsetof(GetOpenIdTokenRequest_t2698079901, ____logins_4)); }
	inline Dictionary_2_t3943999495 * get__logins_4() const { return ____logins_4; }
	inline Dictionary_2_t3943999495 ** get_address_of__logins_4() { return &____logins_4; }
	inline void set__logins_4(Dictionary_2_t3943999495 * value)
	{
		____logins_4 = value;
		Il2CppCodeGenWriteBarrier(&____logins_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
