﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.CognitoSync.AmazonCognitoSyncClient
struct AmazonCognitoSyncClient_t3423722813;
// Amazon.Runtime.AWSCredentials
struct AWSCredentials_t3583921007;
// Amazon.CognitoSync.AmazonCognitoSyncConfig
struct AmazonCognitoSyncConfig_t1479139240;
// Amazon.Runtime.Internal.Auth.AbstractAWSSigner
struct AbstractAWSSigner_t2114314031;
// Amazon.Runtime.Internal.RuntimePipeline
struct RuntimePipeline_t3355992556;
// Amazon.CognitoSync.Model.DeleteDatasetResponse
struct DeleteDatasetResponse_t2927760190;
// Amazon.CognitoSync.Model.DeleteDatasetRequest
struct DeleteDatasetRequest_t3672322944;
// Amazon.CognitoSync.Model.ListRecordsResponse
struct ListRecordsResponse_t1441155223;
// Amazon.CognitoSync.Model.ListRecordsRequest
struct ListRecordsRequest_t3074726983;
// Amazon.CognitoSync.Model.UpdateRecordsResponse
struct UpdateRecordsResponse_t1893058994;
// Amazon.CognitoSync.Model.UpdateRecordsRequest
struct UpdateRecordsRequest_t197801344;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_Runtime_AWSCredentials3583921007.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_AmazonCognit1479139240.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_RuntimePipelin3355992556.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_Model_Delete3672322944.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_Model_ListRe3074726983.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_Model_UpdateR197801344.h"

// System.Void Amazon.CognitoSync.AmazonCognitoSyncClient::.ctor(Amazon.Runtime.AWSCredentials,Amazon.CognitoSync.AmazonCognitoSyncConfig)
extern "C"  void AmazonCognitoSyncClient__ctor_m2913021536 (AmazonCognitoSyncClient_t3423722813 * __this, AWSCredentials_t3583921007 * ___credentials0, AmazonCognitoSyncConfig_t1479139240 * ___clientConfig1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.Internal.Auth.AbstractAWSSigner Amazon.CognitoSync.AmazonCognitoSyncClient::CreateSigner()
extern "C"  AbstractAWSSigner_t2114314031 * AmazonCognitoSyncClient_CreateSigner_m973127012 (AmazonCognitoSyncClient_t3423722813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.AmazonCognitoSyncClient::CustomizeRuntimePipeline(Amazon.Runtime.Internal.RuntimePipeline)
extern "C"  void AmazonCognitoSyncClient_CustomizeRuntimePipeline_m1411441703 (AmazonCognitoSyncClient_t3423722813 * __this, RuntimePipeline_t3355992556 * ___pipeline0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.AmazonCognitoSyncClient::Dispose(System.Boolean)
extern "C"  void AmazonCognitoSyncClient_Dispose_m3538729773 (AmazonCognitoSyncClient_t3423722813 * __this, bool ___disposing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.CognitoSync.Model.DeleteDatasetResponse Amazon.CognitoSync.AmazonCognitoSyncClient::DeleteDataset(Amazon.CognitoSync.Model.DeleteDatasetRequest)
extern "C"  DeleteDatasetResponse_t2927760190 * AmazonCognitoSyncClient_DeleteDataset_m1780231267 (AmazonCognitoSyncClient_t3423722813 * __this, DeleteDatasetRequest_t3672322944 * ___request0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.CognitoSync.Model.ListRecordsResponse Amazon.CognitoSync.AmazonCognitoSyncClient::ListRecords(Amazon.CognitoSync.Model.ListRecordsRequest)
extern "C"  ListRecordsResponse_t1441155223 * AmazonCognitoSyncClient_ListRecords_m504320196 (AmazonCognitoSyncClient_t3423722813 * __this, ListRecordsRequest_t3074726983 * ___request0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.CognitoSync.Model.UpdateRecordsResponse Amazon.CognitoSync.AmazonCognitoSyncClient::UpdateRecords(Amazon.CognitoSync.Model.UpdateRecordsRequest)
extern "C"  UpdateRecordsResponse_t1893058994 * AmazonCognitoSyncClient_UpdateRecords_m132020303 (AmazonCognitoSyncClient_t3423722813 * __this, UpdateRecordsRequest_t197801344 * ___request0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
