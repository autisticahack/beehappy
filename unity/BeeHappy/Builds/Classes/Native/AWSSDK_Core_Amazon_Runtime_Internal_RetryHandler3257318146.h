﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.Runtime.Internal.Util.ILogger
struct ILogger_t676853571;
// Amazon.Runtime.RetryPolicy
struct RetryPolicy_t1476739446;

#include "AWSSDK_Core_Amazon_Runtime_Internal_PipelineHandle1486422324.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.RetryHandler
struct  RetryHandler_t3257318146  : public PipelineHandler_t1486422324
{
public:
	// Amazon.Runtime.Internal.Util.ILogger Amazon.Runtime.Internal.RetryHandler::_logger
	Il2CppObject * ____logger_3;
	// Amazon.Runtime.RetryPolicy Amazon.Runtime.Internal.RetryHandler::<RetryPolicy>k__BackingField
	RetryPolicy_t1476739446 * ___U3CRetryPolicyU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of__logger_3() { return static_cast<int32_t>(offsetof(RetryHandler_t3257318146, ____logger_3)); }
	inline Il2CppObject * get__logger_3() const { return ____logger_3; }
	inline Il2CppObject ** get_address_of__logger_3() { return &____logger_3; }
	inline void set__logger_3(Il2CppObject * value)
	{
		____logger_3 = value;
		Il2CppCodeGenWriteBarrier(&____logger_3, value);
	}

	inline static int32_t get_offset_of_U3CRetryPolicyU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(RetryHandler_t3257318146, ___U3CRetryPolicyU3Ek__BackingField_4)); }
	inline RetryPolicy_t1476739446 * get_U3CRetryPolicyU3Ek__BackingField_4() const { return ___U3CRetryPolicyU3Ek__BackingField_4; }
	inline RetryPolicy_t1476739446 ** get_address_of_U3CRetryPolicyU3Ek__BackingField_4() { return &___U3CRetryPolicyU3Ek__BackingField_4; }
	inline void set_U3CRetryPolicyU3Ek__BackingField_4(RetryPolicy_t1476739446 * value)
	{
		___U3CRetryPolicyU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CRetryPolicyU3Ek__BackingField_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
