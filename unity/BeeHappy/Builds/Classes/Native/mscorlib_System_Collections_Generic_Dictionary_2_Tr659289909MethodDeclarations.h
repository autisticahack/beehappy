﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ThirdParty.Json.LitJson.ArrayMetadata,ThirdParty.Json.LitJson.ArrayMetadata>
struct Transform_1_t659289909;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_ArrayMetadata1135078014.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ThirdParty.Json.LitJson.ArrayMetadata,ThirdParty.Json.LitJson.ArrayMetadata>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2057341239_gshared (Transform_1_t659289909 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Transform_1__ctor_m2057341239(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t659289909 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m2057341239_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ThirdParty.Json.LitJson.ArrayMetadata,ThirdParty.Json.LitJson.ArrayMetadata>::Invoke(TKey,TValue)
extern "C"  ArrayMetadata_t1135078014  Transform_1_Invoke_m4165158243_gshared (Transform_1_t659289909 * __this, Il2CppObject * ___key0, ArrayMetadata_t1135078014  ___value1, const MethodInfo* method);
#define Transform_1_Invoke_m4165158243(__this, ___key0, ___value1, method) ((  ArrayMetadata_t1135078014  (*) (Transform_1_t659289909 *, Il2CppObject *, ArrayMetadata_t1135078014 , const MethodInfo*))Transform_1_Invoke_m4165158243_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ThirdParty.Json.LitJson.ArrayMetadata,ThirdParty.Json.LitJson.ArrayMetadata>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m899691010_gshared (Transform_1_t659289909 * __this, Il2CppObject * ___key0, ArrayMetadata_t1135078014  ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define Transform_1_BeginInvoke_m899691010(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t659289909 *, Il2CppObject *, ArrayMetadata_t1135078014 , AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m899691010_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ThirdParty.Json.LitJson.ArrayMetadata,ThirdParty.Json.LitJson.ArrayMetadata>::EndInvoke(System.IAsyncResult)
extern "C"  ArrayMetadata_t1135078014  Transform_1_EndInvoke_m3478379553_gshared (Transform_1_t659289909 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Transform_1_EndInvoke_m3478379553(__this, ___result0, method) ((  ArrayMetadata_t1135078014  (*) (Transform_1_t659289909 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m3478379553_gshared)(__this, ___result0, method)
