﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Queue_1_gen2509106130MethodDeclarations.h"

// System.Void System.Collections.Generic.Queue`1<Amazon.Runtime.Internal.RuntimeAsyncResult>::.ctor()
#define Queue_1__ctor_m1410252916(__this, method) ((  void (*) (Queue_1_t4099012848 *, const MethodInfo*))Queue_1__ctor_m1845245813_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<Amazon.Runtime.Internal.RuntimeAsyncResult>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Queue_1_System_Collections_ICollection_CopyTo_m3525030231(__this, ___array0, ___idx1, method) ((  void (*) (Queue_1_t4099012848 *, Il2CppArray *, int32_t, const MethodInfo*))Queue_1_System_Collections_ICollection_CopyTo_m772787461_gshared)(__this, ___array0, ___idx1, method)
// System.Boolean System.Collections.Generic.Queue`1<Amazon.Runtime.Internal.RuntimeAsyncResult>::System.Collections.ICollection.get_IsSynchronized()
#define Queue_1_System_Collections_ICollection_get_IsSynchronized_m542963263(__this, method) ((  bool (*) (Queue_1_t4099012848 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_IsSynchronized_m307125669_gshared)(__this, method)
// System.Object System.Collections.Generic.Queue`1<Amazon.Runtime.Internal.RuntimeAsyncResult>::System.Collections.ICollection.get_SyncRoot()
#define Queue_1_System_Collections_ICollection_get_SyncRoot_m613041223(__this, method) ((  Il2CppObject * (*) (Queue_1_t4099012848 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_SyncRoot_m311152041_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Queue`1<Amazon.Runtime.Internal.RuntimeAsyncResult>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3699042115(__this, method) ((  Il2CppObject* (*) (Queue_1_t4099012848 *, const MethodInfo*))Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m4179169157_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Queue`1<Amazon.Runtime.Internal.RuntimeAsyncResult>::System.Collections.IEnumerable.GetEnumerator()
#define Queue_1_System_Collections_IEnumerable_GetEnumerator_m1012456522(__this, method) ((  Il2CppObject * (*) (Queue_1_t4099012848 *, const MethodInfo*))Queue_1_System_Collections_IEnumerable_GetEnumerator_m251608368_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<Amazon.Runtime.Internal.RuntimeAsyncResult>::CopyTo(T[],System.Int32)
#define Queue_1_CopyTo_m3105162888(__this, ___array0, ___idx1, method) ((  void (*) (Queue_1_t4099012848 *, RuntimeAsyncResultU5BU5D_t281224512*, int32_t, const MethodInfo*))Queue_1_CopyTo_m1419617898_gshared)(__this, ___array0, ___idx1, method)
// T System.Collections.Generic.Queue`1<Amazon.Runtime.Internal.RuntimeAsyncResult>::Dequeue()
#define Queue_1_Dequeue_m1378890534(__this, method) ((  RuntimeAsyncResult_t4279356013 * (*) (Queue_1_t4099012848 *, const MethodInfo*))Queue_1_Dequeue_m4118160228_gshared)(__this, method)
// T System.Collections.Generic.Queue`1<Amazon.Runtime.Internal.RuntimeAsyncResult>::Peek()
#define Queue_1_Peek_m608931039(__this, method) ((  RuntimeAsyncResult_t4279356013 * (*) (Queue_1_t4099012848 *, const MethodInfo*))Queue_1_Peek_m1463479953_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<Amazon.Runtime.Internal.RuntimeAsyncResult>::Enqueue(T)
#define Queue_1_Enqueue_m267991393(__this, ___item0, method) ((  void (*) (Queue_1_t4099012848 *, RuntimeAsyncResult_t4279356013 *, const MethodInfo*))Queue_1_Enqueue_m2424099095_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Queue`1<Amazon.Runtime.Internal.RuntimeAsyncResult>::SetCapacity(System.Int32)
#define Queue_1_SetCapacity_m1864174492(__this, ___new_size0, method) ((  void (*) (Queue_1_t4099012848 *, int32_t, const MethodInfo*))Queue_1_SetCapacity_m3858927842_gshared)(__this, ___new_size0, method)
// System.Int32 System.Collections.Generic.Queue`1<Amazon.Runtime.Internal.RuntimeAsyncResult>::get_Count()
#define Queue_1_get_Count_m2333242774(__this, method) ((  int32_t (*) (Queue_1_t4099012848 *, const MethodInfo*))Queue_1_get_Count_m3795587777_gshared)(__this, method)
// System.Collections.Generic.Queue`1/Enumerator<T> System.Collections.Generic.Queue`1<Amazon.Runtime.Internal.RuntimeAsyncResult>::GetEnumerator()
#define Queue_1_GetEnumerator_m4086336968(__this, method) ((  Enumerator_t314108632  (*) (Queue_1_t4099012848 *, const MethodInfo*))Queue_1_GetEnumerator_m2842671368_gshared)(__this, method)
