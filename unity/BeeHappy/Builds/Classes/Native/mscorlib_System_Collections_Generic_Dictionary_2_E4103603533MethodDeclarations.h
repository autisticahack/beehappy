﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3601534125MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Amazon.CognitoSync.SyncManager.Record>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m4046094018(__this, ___dictionary0, method) ((  void (*) (Enumerator_t4103603533 *, Dictionary_2_t2783578831 *, const MethodInfo*))Enumerator__ctor_m3742107451_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,Amazon.CognitoSync.SyncManager.Record>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m73043245(__this, method) ((  Il2CppObject * (*) (Enumerator_t4103603533 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m229223308_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Amazon.CognitoSync.SyncManager.Record>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3206784997(__this, method) ((  void (*) (Enumerator_t4103603533 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3225937576_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,Amazon.CognitoSync.SyncManager.Record>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m359016532(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t4103603533 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m221119093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,Amazon.CognitoSync.SyncManager.Record>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1822896987(__this, method) ((  Il2CppObject * (*) (Enumerator_t4103603533 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m467957770_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,Amazon.CognitoSync.SyncManager.Record>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m512655307(__this, method) ((  Il2CppObject * (*) (Enumerator_t4103603533 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2325383168_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,Amazon.CognitoSync.SyncManager.Record>::MoveNext()
#define Enumerator_MoveNext_m3057102973(__this, method) ((  bool (*) (Enumerator_t4103603533 *, const MethodInfo*))Enumerator_MoveNext_m3349738440_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,Amazon.CognitoSync.SyncManager.Record>::get_Current()
#define Enumerator_get_Current_m262678841(__this, method) ((  KeyValuePair_2_t540924053  (*) (Enumerator_t4103603533 *, const MethodInfo*))Enumerator_get_Current_m25299632_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,Amazon.CognitoSync.SyncManager.Record>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m2112046882(__this, method) ((  String_t* (*) (Enumerator_t4103603533 *, const MethodInfo*))Enumerator_get_CurrentKey_m3839846791_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,Amazon.CognitoSync.SyncManager.Record>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m4052412514(__this, method) ((  Record_t868799569 * (*) (Enumerator_t4103603533 *, const MethodInfo*))Enumerator_get_CurrentValue_m402763047_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Amazon.CognitoSync.SyncManager.Record>::Reset()
#define Enumerator_Reset_m3355183964(__this, method) ((  void (*) (Enumerator_t4103603533 *, const MethodInfo*))Enumerator_Reset_m3129803197_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Amazon.CognitoSync.SyncManager.Record>::VerifyState()
#define Enumerator_VerifyState_m1358473307(__this, method) ((  void (*) (Enumerator_t4103603533 *, const MethodInfo*))Enumerator_VerifyState_m262343092_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Amazon.CognitoSync.SyncManager.Record>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m3697726261(__this, method) ((  void (*) (Enumerator_t4103603533 *, const MethodInfo*))Enumerator_VerifyCurrent_m1702320752_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Amazon.CognitoSync.SyncManager.Record>::Dispose()
#define Enumerator_Dispose_m3609026706(__this, method) ((  void (*) (Enumerator_t4103603533 *, const MethodInfo*))Enumerator_Dispose_m1905011127_gshared)(__this, method)
