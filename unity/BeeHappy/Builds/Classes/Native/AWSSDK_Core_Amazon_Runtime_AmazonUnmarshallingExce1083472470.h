﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AWSSDK_Core_Amazon_Runtime_AmazonServiceException3748559634.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.AmazonUnmarshallingException
struct  AmazonUnmarshallingException_t1083472470  : public AmazonServiceException_t3748559634
{
public:
	// System.String Amazon.Runtime.AmazonUnmarshallingException::<LastKnownLocation>k__BackingField
	String_t* ___U3CLastKnownLocationU3Ek__BackingField_15;
	// System.String Amazon.Runtime.AmazonUnmarshallingException::<ResponseBody>k__BackingField
	String_t* ___U3CResponseBodyU3Ek__BackingField_16;

public:
	inline static int32_t get_offset_of_U3CLastKnownLocationU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(AmazonUnmarshallingException_t1083472470, ___U3CLastKnownLocationU3Ek__BackingField_15)); }
	inline String_t* get_U3CLastKnownLocationU3Ek__BackingField_15() const { return ___U3CLastKnownLocationU3Ek__BackingField_15; }
	inline String_t** get_address_of_U3CLastKnownLocationU3Ek__BackingField_15() { return &___U3CLastKnownLocationU3Ek__BackingField_15; }
	inline void set_U3CLastKnownLocationU3Ek__BackingField_15(String_t* value)
	{
		___U3CLastKnownLocationU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier(&___U3CLastKnownLocationU3Ek__BackingField_15, value);
	}

	inline static int32_t get_offset_of_U3CResponseBodyU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(AmazonUnmarshallingException_t1083472470, ___U3CResponseBodyU3Ek__BackingField_16)); }
	inline String_t* get_U3CResponseBodyU3Ek__BackingField_16() const { return ___U3CResponseBodyU3Ek__BackingField_16; }
	inline String_t** get_address_of_U3CResponseBodyU3Ek__BackingField_16() { return &___U3CResponseBodyU3Ek__BackingField_16; }
	inline void set_U3CResponseBodyU3Ek__BackingField_16(String_t* value)
	{
		___U3CResponseBodyU3Ek__BackingField_16 = value;
		Il2CppCodeGenWriteBarrier(&___U3CResponseBodyU3Ek__BackingField_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
