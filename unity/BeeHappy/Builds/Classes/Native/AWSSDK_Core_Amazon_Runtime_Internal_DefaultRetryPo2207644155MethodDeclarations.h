﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.DefaultRetryPolicy
struct DefaultRetryPolicy_t2207644155;
// System.Collections.Generic.ICollection`1<System.Net.HttpStatusCode>
struct ICollection_1_t2850484946;
// System.Collections.Generic.ICollection`1<System.String>
struct ICollection_1_t2981295538;
// System.Collections.Generic.ICollection`1<System.Net.WebExceptionStatus>
struct ICollection_1_t2121448836;
// Amazon.Runtime.IClientConfig
struct IClientConfig_t4078933728;
// Amazon.Runtime.IExecutionContext
struct IExecutionContext_t2477130752;
// System.Exception
struct Exception_t1927440687;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1927440687.h"

// System.Int32 Amazon.Runtime.Internal.DefaultRetryPolicy::get_MaxBackoffInMilliseconds()
extern "C"  int32_t DefaultRetryPolicy_get_MaxBackoffInMilliseconds_m655411305 (DefaultRetryPolicy_t2207644155 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.ICollection`1<System.Net.HttpStatusCode> Amazon.Runtime.Internal.DefaultRetryPolicy::get_HttpStatusCodesToRetryOn()
extern "C"  Il2CppObject* DefaultRetryPolicy_get_HttpStatusCodesToRetryOn_m1247714973 (DefaultRetryPolicy_t2207644155 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.ICollection`1<System.String> Amazon.Runtime.Internal.DefaultRetryPolicy::get_ErrorCodesToRetryOn()
extern "C"  Il2CppObject* DefaultRetryPolicy_get_ErrorCodesToRetryOn_m3816816166 (DefaultRetryPolicy_t2207644155 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.ICollection`1<System.Net.WebExceptionStatus> Amazon.Runtime.Internal.DefaultRetryPolicy::get_WebExceptionStatusesToRetryOn()
extern "C"  Il2CppObject* DefaultRetryPolicy_get_WebExceptionStatusesToRetryOn_m3036266840 (DefaultRetryPolicy_t2207644155 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.DefaultRetryPolicy::.ctor(Amazon.Runtime.IClientConfig)
extern "C"  void DefaultRetryPolicy__ctor_m1925598415 (DefaultRetryPolicy_t2207644155 * __this, Il2CppObject * ___config0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.DefaultRetryPolicy::CanRetry(Amazon.Runtime.IExecutionContext)
extern "C"  bool DefaultRetryPolicy_CanRetry_m116696049 (DefaultRetryPolicy_t2207644155 * __this, Il2CppObject * ___executionContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.DefaultRetryPolicy::RetryForException(Amazon.Runtime.IExecutionContext,System.Exception)
extern "C"  bool DefaultRetryPolicy_RetryForException_m4160401281 (DefaultRetryPolicy_t2207644155 * __this, Il2CppObject * ___executionContext0, Exception_t1927440687 * ___exception1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.DefaultRetryPolicy::OnRetry(Amazon.Runtime.IExecutionContext)
extern "C"  bool DefaultRetryPolicy_OnRetry_m1484887984 (DefaultRetryPolicy_t2207644155 * __this, Il2CppObject * ___executionContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.DefaultRetryPolicy::NotifySuccess(Amazon.Runtime.IExecutionContext)
extern "C"  void DefaultRetryPolicy_NotifySuccess_m4117495937 (DefaultRetryPolicy_t2207644155 * __this, Il2CppObject * ___executionContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.DefaultRetryPolicy::RetryForExceptionSync(System.Exception)
extern "C"  bool DefaultRetryPolicy_RetryForExceptionSync_m627619648 (DefaultRetryPolicy_t2207644155 * __this, Exception_t1927440687 * ___exception0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.DefaultRetryPolicy::RetryLimitReached(Amazon.Runtime.IExecutionContext)
extern "C"  bool DefaultRetryPolicy_RetryLimitReached_m3886335800 (DefaultRetryPolicy_t2207644155 * __this, Il2CppObject * ___executionContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.DefaultRetryPolicy::WaitBeforeRetry(Amazon.Runtime.IExecutionContext)
extern "C"  void DefaultRetryPolicy_WaitBeforeRetry_m3600969231 (DefaultRetryPolicy_t2207644155 * __this, Il2CppObject * ___executionContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.DefaultRetryPolicy::WaitBeforeRetry(System.Int32,System.Int32)
extern "C"  void DefaultRetryPolicy_WaitBeforeRetry_m3493954937 (Il2CppObject * __this /* static, unused */, int32_t ___retries0, int32_t ___maxBackoffInMilliseconds1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Amazon.Runtime.Internal.DefaultRetryPolicy::CalculateRetryDelay(System.Int32,System.Int32)
extern "C"  int32_t DefaultRetryPolicy_CalculateRetryDelay_m476336838 (Il2CppObject * __this /* static, unused */, int32_t ___retries0, int32_t ___maxBackoffInMilliseconds1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.DefaultRetryPolicy::.cctor()
extern "C"  void DefaultRetryPolicy__cctor_m3293810740 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
