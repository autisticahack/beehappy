﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.StreamReadTracker
struct StreamReadTracker_t1958363340;
// System.Object
struct Il2CppObject;
// System.EventHandler`1<Amazon.Runtime.StreamTransferProgressArgs>
struct EventHandler_1_t1230945235;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void Amazon.Runtime.Internal.StreamReadTracker::.ctor(System.Object,System.EventHandler`1<Amazon.Runtime.StreamTransferProgressArgs>,System.Int64,System.Int64)
extern "C"  void StreamReadTracker__ctor_m1013210235 (StreamReadTracker_t1958363340 * __this, Il2CppObject * ___sender0, EventHandler_1_t1230945235 * ___callback1, int64_t ___contentLength2, int64_t ___progressUpdateInterval3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.StreamReadTracker::ReadProgress(System.Int32)
extern "C"  void StreamReadTracker_ReadProgress_m1528998970 (StreamReadTracker_t1958363340 * __this, int32_t ___bytesRead0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.StreamReadTracker::UpdateProgress(System.Single)
extern "C"  void StreamReadTracker_UpdateProgress_m4291833423 (StreamReadTracker_t1958363340 * __this, float ___progress0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
