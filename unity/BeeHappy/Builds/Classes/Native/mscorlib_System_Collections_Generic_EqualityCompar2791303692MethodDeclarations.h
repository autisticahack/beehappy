﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Net.WebExceptionStatus>
struct DefaultComparer_t2791303692;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_WebExceptionStatus1169373531.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Net.WebExceptionStatus>::.ctor()
extern "C"  void DefaultComparer__ctor_m1197296345_gshared (DefaultComparer_t2791303692 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m1197296345(__this, method) ((  void (*) (DefaultComparer_t2791303692 *, const MethodInfo*))DefaultComparer__ctor_m1197296345_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Net.WebExceptionStatus>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m1224499238_gshared (DefaultComparer_t2791303692 * __this, int32_t ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m1224499238(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t2791303692 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m1224499238_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Net.WebExceptionStatus>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m2800120830_gshared (DefaultComparer_t2791303692 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m2800120830(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t2791303692 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m2800120830_gshared)(__this, ___x0, ___y1, method)
