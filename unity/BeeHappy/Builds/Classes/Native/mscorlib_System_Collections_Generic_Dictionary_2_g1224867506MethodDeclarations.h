﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>
struct Dictionary_2_t1224867506;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t1902082073;
// System.Collections.Generic.IDictionary`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>
struct IDictionary_2_t3518918223;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t3641524600;
// System.Collections.ICollection
struct ICollection_t91669223;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>[]
struct KeyValuePair_2U5BU5D_t855235241;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>>
struct IEnumerator_1_t752703851;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t259680273;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>
struct KeyCollection_t3708365277;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>
struct ValueCollection_t4222894645;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serialization228987430.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23277180024.h"
#include "mscorlib_System_Array3829468939.h"
#include "AWSSDK_Core_Amazon_Util_Internal_PlatformServices_1632807378.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2544892208.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::.ctor()
extern "C"  void Dictionary_2__ctor_m4147800043_gshared (Dictionary_2_t1224867506 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m4147800043(__this, method) ((  void (*) (Dictionary_2_t1224867506 *, const MethodInfo*))Dictionary_2__ctor_m4147800043_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m1006290304_gshared (Dictionary_2_t1224867506 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m1006290304(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t1224867506 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m1006290304_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C"  void Dictionary_2__ctor_m2024578485_gshared (Dictionary_2_t1224867506 * __this, Il2CppObject* ___dictionary0, const MethodInfo* method);
#define Dictionary_2__ctor_m2024578485(__this, ___dictionary0, method) ((  void (*) (Dictionary_2_t1224867506 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2024578485_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m3734459164_gshared (Dictionary_2_t1224867506 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m3734459164(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t1224867506 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m3734459164_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m266881356_gshared (Dictionary_2_t1224867506 * __this, Il2CppObject* ___dictionary0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m266881356(__this, ___dictionary0, ___comparer1, method) ((  void (*) (Dictionary_2_t1224867506 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m266881356_gshared)(__this, ___dictionary0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m3518183142_gshared (Dictionary_2_t1224867506 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m3518183142(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t1224867506 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2__ctor_m3518183142_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1058558855_gshared (Dictionary_2_t1224867506 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1058558855(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t1224867506 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1058558855_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Keys_m2307858949_gshared (Dictionary_2_t1224867506 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m2307858949(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1224867506 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m2307858949_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.IDictionary.get_Values()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Values_m3963459397_gshared (Dictionary_2_t1224867506 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Values_m3963459397(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1224867506 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m3963459397_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m3297781491_gshared (Dictionary_2_t1224867506 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m3297781491(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t1224867506 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m3297781491_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m849104178_gshared (Dictionary_2_t1224867506 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m849104178(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1224867506 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m849104178_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m1918295741_gshared (Dictionary_2_t1224867506 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m1918295741(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1224867506 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m1918295741_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool Dictionary_2_System_Collections_IDictionary_Contains_m4015420945_gshared (Dictionary_2_t1224867506 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m4015420945(__this, ___key0, method) ((  bool (*) (Dictionary_2_t1224867506 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m4015420945_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m2160484718_gshared (Dictionary_2_t1224867506 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m2160484718(__this, ___key0, method) ((  void (*) (Dictionary_2_t1224867506 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m2160484718_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m844444279_gshared (Dictionary_2_t1224867506 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m844444279(__this, method) ((  bool (*) (Dictionary_2_t1224867506 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m844444279_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m2769144343_gshared (Dictionary_2_t1224867506 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m2769144343(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1224867506 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m2769144343_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2400940605_gshared (Dictionary_2_t1224867506 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2400940605(__this, method) ((  bool (*) (Dictionary_2_t1224867506 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2400940605_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3616840532_gshared (Dictionary_2_t1224867506 * __this, KeyValuePair_2_t3277180024  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3616840532(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t1224867506 *, KeyValuePair_2_t3277180024 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3616840532_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2657726548_gshared (Dictionary_2_t1224867506 * __this, KeyValuePair_2_t3277180024  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2657726548(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t1224867506 *, KeyValuePair_2_t3277180024 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2657726548_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m718656728_gshared (Dictionary_2_t1224867506 * __this, KeyValuePair_2U5BU5D_t855235241* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m718656728(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1224867506 *, KeyValuePair_2U5BU5D_t855235241*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m718656728_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1869148147_gshared (Dictionary_2_t1224867506 * __this, KeyValuePair_2_t3277180024  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1869148147(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t1224867506 *, KeyValuePair_2_t3277180024 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1869148147_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m3282088791_gshared (Dictionary_2_t1224867506 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m3282088791(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1224867506 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m3282088791_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3481500786_gshared (Dictionary_2_t1224867506 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3481500786(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1224867506 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3481500786_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3342605877_gshared (Dictionary_2_t1224867506 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3342605877(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t1224867506 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3342605877_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1643109640_gshared (Dictionary_2_t1224867506 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1643109640(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1224867506 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1643109640_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m2926416983_gshared (Dictionary_2_t1224867506 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m2926416983(__this, method) ((  int32_t (*) (Dictionary_2_t1224867506 *, const MethodInfo*))Dictionary_2_get_Count_m2926416983_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::get_Item(TKey)
extern "C"  int32_t Dictionary_2_get_Item_m1013639792_gshared (Dictionary_2_t1224867506 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m1013639792(__this, ___key0, method) ((  int32_t (*) (Dictionary_2_t1224867506 *, Il2CppObject *, const MethodInfo*))Dictionary_2_get_Item_m1013639792_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m617182371_gshared (Dictionary_2_t1224867506 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m617182371(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1224867506 *, Il2CppObject *, int32_t, const MethodInfo*))Dictionary_2_set_Item_m617182371_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m3633257507_gshared (Dictionary_2_t1224867506 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m3633257507(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t1224867506 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m3633257507_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m975792704_gshared (Dictionary_2_t1224867506 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m975792704(__this, ___size0, method) ((  void (*) (Dictionary_2_t1224867506 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m975792704_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m3474884150_gshared (Dictionary_2_t1224867506 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m3474884150(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1224867506 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m3474884150_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t3277180024  Dictionary_2_make_pair_m4182062344_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m4182062344(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t3277180024  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, int32_t, const MethodInfo*))Dictionary_2_make_pair_m4182062344_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::pick_key(TKey,TValue)
extern "C"  Il2CppObject * Dictionary_2_pick_key_m2548113094_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_pick_key_m2548113094(__this /* static, unused */, ___key0, ___value1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, int32_t, const MethodInfo*))Dictionary_2_pick_key_m2548113094_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::pick_value(TKey,TValue)
extern "C"  int32_t Dictionary_2_pick_value_m2401393622_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m2401393622(__this /* static, unused */, ___key0, ___value1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, Il2CppObject *, int32_t, const MethodInfo*))Dictionary_2_pick_value_m2401393622_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m4108132327_gshared (Dictionary_2_t1224867506 * __this, KeyValuePair_2U5BU5D_t855235241* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m4108132327(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1224867506 *, KeyValuePair_2U5BU5D_t855235241*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m4108132327_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::Resize()
extern "C"  void Dictionary_2_Resize_m3559845525_gshared (Dictionary_2_t1224867506 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m3559845525(__this, method) ((  void (*) (Dictionary_2_t1224867506 *, const MethodInfo*))Dictionary_2_Resize_m3559845525_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m3458810848_gshared (Dictionary_2_t1224867506 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m3458810848(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1224867506 *, Il2CppObject *, int32_t, const MethodInfo*))Dictionary_2_Add_m3458810848_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::Clear()
extern "C"  void Dictionary_2_Clear_m52936784_gshared (Dictionary_2_t1224867506 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m52936784(__this, method) ((  void (*) (Dictionary_2_t1224867506 *, const MethodInfo*))Dictionary_2_Clear_m52936784_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m1324064360_gshared (Dictionary_2_t1224867506 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m1324064360(__this, ___key0, method) ((  bool (*) (Dictionary_2_t1224867506 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ContainsKey_m1324064360_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m2211108696_gshared (Dictionary_2_t1224867506 * __this, int32_t ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m2211108696(__this, ___value0, method) ((  bool (*) (Dictionary_2_t1224867506 *, int32_t, const MethodInfo*))Dictionary_2_ContainsValue_m2211108696_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m180785227_gshared (Dictionary_2_t1224867506 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m180785227(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t1224867506 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2_GetObjectData_m180785227_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m4008568515_gshared (Dictionary_2_t1224867506 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m4008568515(__this, ___sender0, method) ((  void (*) (Dictionary_2_t1224867506 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m4008568515_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m210678492_gshared (Dictionary_2_t1224867506 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m210678492(__this, ___key0, method) ((  bool (*) (Dictionary_2_t1224867506 *, Il2CppObject *, const MethodInfo*))Dictionary_2_Remove_m210678492_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m1073173075_gshared (Dictionary_2_t1224867506 * __this, Il2CppObject * ___key0, int32_t* ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m1073173075(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t1224867506 *, Il2CppObject *, int32_t*, const MethodInfo*))Dictionary_2_TryGetValue_m1073173075_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::get_Keys()
extern "C"  KeyCollection_t3708365277 * Dictionary_2_get_Keys_m3443206916_gshared (Dictionary_2_t1224867506 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m3443206916(__this, method) ((  KeyCollection_t3708365277 * (*) (Dictionary_2_t1224867506 *, const MethodInfo*))Dictionary_2_get_Keys_m3443206916_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::get_Values()
extern "C"  ValueCollection_t4222894645 * Dictionary_2_get_Values_m826557372_gshared (Dictionary_2_t1224867506 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m826557372(__this, method) ((  ValueCollection_t4222894645 * (*) (Dictionary_2_t1224867506 *, const MethodInfo*))Dictionary_2_get_Values_m826557372_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::ToTKey(System.Object)
extern "C"  Il2CppObject * Dictionary_2_ToTKey_m3441713453_gshared (Dictionary_2_t1224867506 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m3441713453(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t1224867506 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m3441713453_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::ToTValue(System.Object)
extern "C"  int32_t Dictionary_2_ToTValue_m1199044997_gshared (Dictionary_2_t1224867506 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m1199044997(__this, ___value0, method) ((  int32_t (*) (Dictionary_2_t1224867506 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m1199044997_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m4098034971_gshared (Dictionary_2_t1224867506 * __this, KeyValuePair_2_t3277180024  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m4098034971(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t1224867506 *, KeyValuePair_2_t3277180024 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m4098034971_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::GetEnumerator()
extern "C"  Enumerator_t2544892208  Dictionary_2_GetEnumerator_m3734528040_gshared (Dictionary_2_t1224867506 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m3734528040(__this, method) ((  Enumerator_t2544892208  (*) (Dictionary_2_t1224867506 *, const MethodInfo*))Dictionary_2_GetEnumerator_m3734528040_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::<CopyTo>m__0(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Dictionary_2_U3CCopyToU3Em__0_m3948947373_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m3948947373(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, int32_t, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m3948947373_gshared)(__this /* static, unused */, ___key0, ___value1, method)
