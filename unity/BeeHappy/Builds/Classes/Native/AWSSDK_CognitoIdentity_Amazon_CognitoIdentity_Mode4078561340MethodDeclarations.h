﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.CognitoIdentity.Model.GetIdRequest
struct GetIdRequest_t4078561340;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.String Amazon.CognitoIdentity.Model.GetIdRequest::get_AccountId()
extern "C"  String_t* GetIdRequest_get_AccountId_m2559012565 (GetIdRequest_t4078561340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoIdentity.Model.GetIdRequest::set_AccountId(System.String)
extern "C"  void GetIdRequest_set_AccountId_m4016883484 (GetIdRequest_t4078561340 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.CognitoIdentity.Model.GetIdRequest::IsSetAccountId()
extern "C"  bool GetIdRequest_IsSetAccountId_m2868615701 (GetIdRequest_t4078561340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.CognitoIdentity.Model.GetIdRequest::get_IdentityPoolId()
extern "C"  String_t* GetIdRequest_get_IdentityPoolId_m2191128700 (GetIdRequest_t4078561340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoIdentity.Model.GetIdRequest::set_IdentityPoolId(System.String)
extern "C"  void GetIdRequest_set_IdentityPoolId_m1186750113 (GetIdRequest_t4078561340 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.CognitoIdentity.Model.GetIdRequest::IsSetIdentityPoolId()
extern "C"  bool GetIdRequest_IsSetIdentityPoolId_m213241084 (GetIdRequest_t4078561340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> Amazon.CognitoIdentity.Model.GetIdRequest::get_Logins()
extern "C"  Dictionary_2_t3943999495 * GetIdRequest_get_Logins_m1911199310 (GetIdRequest_t4078561340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoIdentity.Model.GetIdRequest::set_Logins(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void GetIdRequest_set_Logins_m1388169875 (GetIdRequest_t4078561340 * __this, Dictionary_2_t3943999495 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.CognitoIdentity.Model.GetIdRequest::IsSetLogins()
extern "C"  bool GetIdRequest_IsSetLogins_m529970793 (GetIdRequest_t4078561340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoIdentity.Model.GetIdRequest::.ctor()
extern "C"  void GetIdRequest__ctor_m3030190485 (GetIdRequest_t4078561340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
