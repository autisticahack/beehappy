﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V4222894645MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m2906981187(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t2273225118 *, Dictionary_2_t3570165275 *, const MethodInfo*))ValueCollection__ctor_m1534037786_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2710313449(__this, ___item0, method) ((  void (*) (ValueCollection_t2273225118 *, int32_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m4084879748_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3619953588(__this, method) ((  void (*) (ValueCollection_t2273225118 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1695851195_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2183674031(__this, ___item0, method) ((  bool (*) (ValueCollection_t2273225118 *, int32_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3443378028_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2579383710(__this, ___item0, method) ((  bool (*) (ValueCollection_t2273225118 *, int32_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m459294531_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3771075896(__this, method) ((  Il2CppObject* (*) (ValueCollection_t2273225118 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2422542625_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m844347128(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2273225118 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m3476840469_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1565739069(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2273225118 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2846809116_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1403224402(__this, method) ((  bool (*) (ValueCollection_t2273225118 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3733867415_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2907096648(__this, method) ((  bool (*) (ValueCollection_t2273225118 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1007804449_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m679891976(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2273225118 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m669103221_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m254001140(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2273225118 *, InstantiationModelU5BU5D_t1614949031*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m1635440867_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::GetEnumerator()
#define ValueCollection_GetEnumerator_m2783250607(__this, method) ((  Enumerator_t961730743  (*) (ValueCollection_t2273225118 *, const MethodInfo*))ValueCollection_GetEnumerator_m11874664_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::get_Count()
#define ValueCollection_get_Count_m1663099408(__this, method) ((  int32_t (*) (ValueCollection_t2273225118 *, const MethodInfo*))ValueCollection_get_Count_m316804641_gshared)(__this, method)
