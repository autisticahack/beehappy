﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.UnityRequest
struct UnityRequest_t4218620704;
// System.Uri
struct Uri_t19570940;
// System.IDisposable
struct IDisposable_t2427283555;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.Threading.ManualResetEvent
struct ManualResetEvent_t926074657;
// Amazon.Runtime.Internal.Transform.IWebResponseData
struct IWebResponseData_t3815728244;
// System.Exception
struct Exception_t1927440687;
// System.String
struct String_t;
// Amazon.Runtime.IRequestContext
struct IRequestContext_t1620878341;
// System.Collections.Generic.IDictionary`2<System.String,System.String>
struct IDictionary_2_t1943082916;
// System.IO.Stream
struct Stream_t3255436806;
// System.Object
struct Il2CppObject;
// Amazon.Runtime.Internal.StreamReadTracker
struct StreamReadTracker_t1958363340;
// System.EventHandler`1<Amazon.Runtime.StreamTransferProgressArgs>
struct EventHandler_1_t1230945235;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Uri19570940.h"
#include "mscorlib_System_AsyncCallback163412349.h"
#include "mscorlib_System_Threading_ManualResetEvent926074657.h"
#include "mscorlib_System_Exception1927440687.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_IO_Stream3255436806.h"
#include "mscorlib_System_Object2689449295.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_StreamReadTrac1958363340.h"

// System.Uri Amazon.Runtime.Internal.UnityRequest::get_RequestUri()
extern "C"  Uri_t19570940 * UnityRequest_get_RequestUri_m91389028 (UnityRequest_t4218620704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.UnityRequest::set_RequestUri(System.Uri)
extern "C"  void UnityRequest_set_RequestUri_m706404141 (UnityRequest_t4218620704 * __this, Uri_t19570940 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.UnityRequest::set_WwwRequest(System.IDisposable)
extern "C"  void UnityRequest_set_WwwRequest_m2910215923 (UnityRequest_t4218620704 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Amazon.Runtime.Internal.UnityRequest::get_RequestContent()
extern "C"  ByteU5BU5D_t3397334013* UnityRequest_get_RequestContent_m1876539115 (UnityRequest_t4218620704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.UnityRequest::set_RequestContent(System.Byte[])
extern "C"  void UnityRequest_set_RequestContent_m4063456902 (UnityRequest_t4218620704 * __this, ByteU5BU5D_t3397334013* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> Amazon.Runtime.Internal.UnityRequest::get_Headers()
extern "C"  Dictionary_2_t3943999495 * UnityRequest_get_Headers_m262105399 (UnityRequest_t4218620704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.UnityRequest::set_Headers(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void UnityRequest_set_Headers_m3391784830 (UnityRequest_t4218620704 * __this, Dictionary_2_t3943999495 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.AsyncCallback Amazon.Runtime.Internal.UnityRequest::get_Callback()
extern "C"  AsyncCallback_t163412349 * UnityRequest_get_Callback_m1472095557 (UnityRequest_t4218620704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.UnityRequest::set_Callback(System.AsyncCallback)
extern "C"  void UnityRequest_set_Callback_m272908986 (UnityRequest_t4218620704 * __this, AsyncCallback_t163412349 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Amazon.Runtime.Internal.UnityRequest::get_AsyncResult()
extern "C"  Il2CppObject * UnityRequest_get_AsyncResult_m1816251510 (UnityRequest_t4218620704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.UnityRequest::set_AsyncResult(System.IAsyncResult)
extern "C"  void UnityRequest_set_AsyncResult_m2522086933 (UnityRequest_t4218620704 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.ManualResetEvent Amazon.Runtime.Internal.UnityRequest::get_WaitHandle()
extern "C"  ManualResetEvent_t926074657 * UnityRequest_get_WaitHandle_m2291990137 (UnityRequest_t4218620704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.UnityRequest::set_WaitHandle(System.Threading.ManualResetEvent)
extern "C"  void UnityRequest_set_WaitHandle_m1744666860 (UnityRequest_t4218620704 * __this, ManualResetEvent_t926074657 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.UnityRequest::get_IsSync()
extern "C"  bool UnityRequest_get_IsSync_m2492222484 (UnityRequest_t4218620704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.UnityRequest::set_IsSync(System.Boolean)
extern "C"  void UnityRequest_set_IsSync_m2617973865 (UnityRequest_t4218620704 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.Internal.Transform.IWebResponseData Amazon.Runtime.Internal.UnityRequest::get_Response()
extern "C"  Il2CppObject * UnityRequest_get_Response_m38472320 (UnityRequest_t4218620704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.UnityRequest::set_Response(Amazon.Runtime.Internal.Transform.IWebResponseData)
extern "C"  void UnityRequest_set_Response_m3371676607 (UnityRequest_t4218620704 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception Amazon.Runtime.Internal.UnityRequest::get_Exception()
extern "C"  Exception_t1927440687 * UnityRequest_get_Exception_m3896756239 (UnityRequest_t4218620704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.UnityRequest::set_Exception(System.Exception)
extern "C"  void UnityRequest_set_Exception_m23933150 (UnityRequest_t4218620704 * __this, Exception_t1927440687 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Runtime.Internal.UnityRequest::get_Method()
extern "C"  String_t* UnityRequest_get_Method_m1691562077 (UnityRequest_t4218620704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.UnityRequest::set_Method(System.String)
extern "C"  void UnityRequest_set_Method_m1245950806 (UnityRequest_t4218620704 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.UnityRequest::.ctor(System.Uri)
extern "C"  void UnityRequest__ctor_m2515552371 (UnityRequest_t4218620704 * __this, Uri_t19570940 * ___requestUri0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.UnityRequest::ConfigureRequest(Amazon.Runtime.IRequestContext)
extern "C"  void UnityRequest_ConfigureRequest_m1789576028 (UnityRequest_t4218620704 * __this, Il2CppObject * ___requestContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.UnityRequest::SetRequestHeaders(System.Collections.Generic.IDictionary`2<System.String,System.String>)
extern "C"  void UnityRequest_SetRequestHeaders_m488539079 (UnityRequest_t4218620704 * __this, Il2CppObject* ___headers0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Runtime.Internal.UnityRequest::GetRequestContent()
extern "C"  String_t* UnityRequest_GetRequestContent_m290758581 (UnityRequest_t4218620704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.Internal.Transform.IWebResponseData Amazon.Runtime.Internal.UnityRequest::GetResponse()
extern "C"  Il2CppObject * UnityRequest_GetResponse_m690758923 (UnityRequest_t4218620704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.UnityRequest::WriteToRequestBody(System.String,System.IO.Stream,System.Collections.Generic.IDictionary`2<System.String,System.String>,Amazon.Runtime.IRequestContext)
extern "C"  void UnityRequest_WriteToRequestBody_m194688019 (UnityRequest_t4218620704 * __this, String_t* ___requestContent0, Stream_t3255436806 * ___contentStream1, Il2CppObject* ___contentHeaders2, Il2CppObject * ___requestContext3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.UnityRequest::WriteToRequestBody(System.String,System.Byte[],System.Collections.Generic.IDictionary`2<System.String,System.String>)
extern "C"  void UnityRequest_WriteToRequestBody_m3466759678 (UnityRequest_t4218620704 * __this, String_t* ___requestContent0, ByteU5BU5D_t3397334013* ___content1, Il2CppObject* ___contentHeaders2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Amazon.Runtime.Internal.UnityRequest::BeginGetRequestContent(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnityRequest_BeginGetRequestContent_m3487985587 (UnityRequest_t4218620704 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___state1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Runtime.Internal.UnityRequest::EndGetRequestContent(System.IAsyncResult)
extern "C"  String_t* UnityRequest_EndGetRequestContent_m3844551383 (UnityRequest_t4218620704 * __this, Il2CppObject * ___asyncResult0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Amazon.Runtime.Internal.UnityRequest::BeginGetResponse(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnityRequest_BeginGetResponse_m792880280 (UnityRequest_t4218620704 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___state1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.Internal.Transform.IWebResponseData Amazon.Runtime.Internal.UnityRequest::EndGetResponse(System.IAsyncResult)
extern "C"  Il2CppObject * UnityRequest_EndGetResponse_m2207628997 (UnityRequest_t4218620704 * __this, Il2CppObject * ___asyncResult0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.UnityRequest::Dispose()
extern "C"  void UnityRequest_Dispose_m3725468509 (UnityRequest_t4218620704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.UnityRequest::Dispose(System.Boolean)
extern "C"  void UnityRequest_Dispose_m3828770322 (UnityRequest_t4218620704 * __this, bool ___disposing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.Internal.StreamReadTracker Amazon.Runtime.Internal.UnityRequest::get_Tracker()
extern "C"  StreamReadTracker_t1958363340 * UnityRequest_get_Tracker_m1990324145 (UnityRequest_t4218620704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.UnityRequest::set_Tracker(Amazon.Runtime.Internal.StreamReadTracker)
extern "C"  void UnityRequest_set_Tracker_m1861143576 (UnityRequest_t4218620704 * __this, StreamReadTracker_t1958363340 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream Amazon.Runtime.Internal.UnityRequest::SetupProgressListeners(System.IO.Stream,System.Int64,System.Object,System.EventHandler`1<Amazon.Runtime.StreamTransferProgressArgs>)
extern "C"  Stream_t3255436806 * UnityRequest_SetupProgressListeners_m1676141577 (UnityRequest_t4218620704 * __this, Stream_t3255436806 * ___originalStream0, int64_t ___progressUpdateInterval1, Il2CppObject * ___sender2, EventHandler_1_t1230945235 * ___callback3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.UnityRequest::OnUploadProgressChanged(System.Single)
extern "C"  void UnityRequest_OnUploadProgressChanged_m840243908 (UnityRequest_t4218620704 * __this, float ___progress0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.UnityRequest::<Dispose>b__58_0()
extern "C"  void UnityRequest_U3CDisposeU3Eb__58_0_m2624018091 (UnityRequest_t4218620704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
