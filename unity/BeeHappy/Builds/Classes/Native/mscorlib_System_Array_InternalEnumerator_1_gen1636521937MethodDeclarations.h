﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1636521937.h"
#include "mscorlib_System_Array3829468939.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteKeyReader_K777769675.h"

// System.Void System.Array/InternalEnumerator`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1211168654_gshared (InternalEnumerator_1_t1636521937 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1211168654(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1636521937 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1211168654_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2022555022_gshared (InternalEnumerator_1_t1636521937 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2022555022(__this, method) ((  void (*) (InternalEnumerator_1_t1636521937 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2022555022_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3781559578_gshared (InternalEnumerator_1_t1636521937 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3781559578(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1636521937 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3781559578_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2909222703_gshared (InternalEnumerator_1_t1636521937 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2909222703(__this, method) ((  void (*) (InternalEnumerator_1_t1636521937 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2909222703_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3278815350_gshared (InternalEnumerator_1_t1636521937 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m3278815350(__this, method) ((  bool (*) (InternalEnumerator_1_t1636521937 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3278815350_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::get_Current()
extern "C"  KeyInfo_t777769675  InternalEnumerator_1_get_Current_m1090794879_gshared (InternalEnumerator_1_t1636521937 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1090794879(__this, method) ((  KeyInfo_t777769675  (*) (InternalEnumerator_1_t1636521937 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1090794879_gshared)(__this, method)
