﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.UnityMainThreadDispatcher/<>c__DisplayClass7_0
struct U3CU3Ec__DisplayClass7_0_t142100588;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void Amazon.Runtime.Internal.UnityMainThreadDispatcher/<>c__DisplayClass7_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass7_0__ctor_m1526944587 (U3CU3Ec__DisplayClass7_0_t142100588 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.UnityMainThreadDispatcher/<>c__DisplayClass7_0::<InvokeRequest>b__0(System.Object)
extern "C"  void U3CU3Ec__DisplayClass7_0_U3CInvokeRequestU3Eb__0_m4180129846 (U3CU3Ec__DisplayClass7_0_t142100588 * __this, Il2CppObject * ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
