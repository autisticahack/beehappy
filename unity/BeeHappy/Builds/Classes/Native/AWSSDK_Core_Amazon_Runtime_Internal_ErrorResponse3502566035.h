﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"
#include "AWSSDK_Core_Amazon_Runtime_ErrorType1448377524.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.ErrorResponse
struct  ErrorResponse_t3502566035  : public Il2CppObject
{
public:
	// Amazon.Runtime.ErrorType Amazon.Runtime.Internal.ErrorResponse::type
	int32_t ___type_0;
	// System.String Amazon.Runtime.Internal.ErrorResponse::code
	String_t* ___code_1;
	// System.String Amazon.Runtime.Internal.ErrorResponse::message
	String_t* ___message_2;
	// System.String Amazon.Runtime.Internal.ErrorResponse::requestId
	String_t* ___requestId_3;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(ErrorResponse_t3502566035, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_code_1() { return static_cast<int32_t>(offsetof(ErrorResponse_t3502566035, ___code_1)); }
	inline String_t* get_code_1() const { return ___code_1; }
	inline String_t** get_address_of_code_1() { return &___code_1; }
	inline void set_code_1(String_t* value)
	{
		___code_1 = value;
		Il2CppCodeGenWriteBarrier(&___code_1, value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(ErrorResponse_t3502566035, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier(&___message_2, value);
	}

	inline static int32_t get_offset_of_requestId_3() { return static_cast<int32_t>(offsetof(ErrorResponse_t3502566035, ___requestId_3)); }
	inline String_t* get_requestId_3() const { return ___requestId_3; }
	inline String_t** get_address_of_requestId_3() { return &___requestId_3; }
	inline void set_requestId_3(String_t* value)
	{
		___requestId_3 = value;
		Il2CppCodeGenWriteBarrier(&___requestId_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
