﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Data.Sqlite.SQLiteCollation
struct SQLiteCollation_t4048482007;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Mono.Data.Sqlite.SQLiteCollation::.ctor(System.Object,System.IntPtr)
extern "C"  void SQLiteCollation__ctor_m2130956730 (SQLiteCollation_t4048482007 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.SQLiteCollation::Invoke(System.IntPtr,System.Int32,System.IntPtr,System.Int32,System.IntPtr)
extern "C"  int32_t SQLiteCollation_Invoke_m2471544448 (SQLiteCollation_t4048482007 * __this, IntPtr_t ___puser0, int32_t ___len11, IntPtr_t ___pv12, int32_t ___len23, IntPtr_t ___pv24, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Mono.Data.Sqlite.SQLiteCollation::BeginInvoke(System.IntPtr,System.Int32,System.IntPtr,System.Int32,System.IntPtr,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * SQLiteCollation_BeginInvoke_m933940359 (SQLiteCollation_t4048482007 * __this, IntPtr_t ___puser0, int32_t ___len11, IntPtr_t ___pv12, int32_t ___len23, IntPtr_t ___pv24, AsyncCallback_t163412349 * ___callback5, Il2CppObject * ___object6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.SQLiteCollation::EndInvoke(System.IAsyncResult)
extern "C"  int32_t SQLiteCollation_EndInvoke_m3147049884 (SQLiteCollation_t4048482007 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
