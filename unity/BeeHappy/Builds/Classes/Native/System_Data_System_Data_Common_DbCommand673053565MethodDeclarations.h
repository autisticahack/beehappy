﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Data.Common.DbCommand
struct DbCommand_t673053565;
// System.Data.IDbConnection
struct IDbConnection_t604990601;
// System.Data.IDataReader
struct IDataReader_t1865004018;
// System.Data.Common.DbConnection
struct DbConnection_t1449646780;
// System.Data.Common.DbParameterCollection
struct DbParameterCollection_t3118895993;
// System.Data.Common.DbTransaction
struct DbTransaction_t3114611728;
// System.Data.Common.DbParameter
struct DbParameter_t939375515;
// System.Data.Common.DbDataReader
struct DbDataReader_t79450127;

#include "codegen/il2cpp-codegen.h"
#include "System_Data_System_Data_CommandBehavior1693351995.h"
#include "System_Data_System_Data_Common_DbConnection1449646780.h"
#include "System_Data_System_Data_Common_DbTransaction3114611728.h"

// System.Void System.Data.Common.DbCommand::.ctor()
extern "C"  void DbCommand__ctor_m2041138171 (DbCommand_t673053565 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.IDbConnection System.Data.Common.DbCommand::System.Data.IDbCommand.get_Connection()
extern "C"  Il2CppObject * DbCommand_System_Data_IDbCommand_get_Connection_m2950562242 (DbCommand_t673053565 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.IDataReader System.Data.Common.DbCommand::System.Data.IDbCommand.ExecuteReader(System.Data.CommandBehavior)
extern "C"  Il2CppObject * DbCommand_System_Data_IDbCommand_ExecuteReader_m1030570494 (DbCommand_t673053565 * __this, int32_t ___behavior0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DbConnection System.Data.Common.DbCommand::get_Connection()
extern "C"  DbConnection_t1449646780 * DbCommand_get_Connection_m4085935113 (DbCommand_t673053565 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.DbCommand::set_Connection(System.Data.Common.DbConnection)
extern "C"  void DbCommand_set_Connection_m2099873922 (DbCommand_t673053565 * __this, DbConnection_t1449646780 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DbParameterCollection System.Data.Common.DbCommand::get_Parameters()
extern "C"  DbParameterCollection_t3118895993 * DbCommand_get_Parameters_m971364592 (DbCommand_t673053565 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DbTransaction System.Data.Common.DbCommand::get_Transaction()
extern "C"  DbTransaction_t3114611728 * DbCommand_get_Transaction_m1801669913 (DbCommand_t673053565 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.DbCommand::set_Transaction(System.Data.Common.DbTransaction)
extern "C"  void DbCommand_set_Transaction_m3987418442 (DbCommand_t673053565 * __this, DbTransaction_t3114611728 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DbParameter System.Data.Common.DbCommand::CreateParameter()
extern "C"  DbParameter_t939375515 * DbCommand_CreateParameter_m3592379778 (DbCommand_t673053565 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DbDataReader System.Data.Common.DbCommand::ExecuteReader(System.Data.CommandBehavior)
extern "C"  DbDataReader_t79450127 * DbCommand_ExecuteReader_m1348136093 (DbCommand_t673053565 * __this, int32_t ___behavior0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
