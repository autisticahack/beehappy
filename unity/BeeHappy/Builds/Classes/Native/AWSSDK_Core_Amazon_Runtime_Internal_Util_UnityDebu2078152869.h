﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "System_System_Diagnostics_TraceListener3414949279.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Util.UnityDebugTraceListener
struct  UnityDebugTraceListener_t2078152869  : public TraceListener_t3414949279
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
