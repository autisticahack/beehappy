﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.iOS4Unity.NSObject
struct  NSObject_t1518098886  : public Il2CppObject
{
public:
	// System.IntPtr ThirdParty.iOS4Unity.NSObject::Handle
	IntPtr_t ___Handle_1;
	// System.Boolean ThirdParty.iOS4Unity.NSObject::_shouldRelease
	bool ____shouldRelease_2;

public:
	inline static int32_t get_offset_of_Handle_1() { return static_cast<int32_t>(offsetof(NSObject_t1518098886, ___Handle_1)); }
	inline IntPtr_t get_Handle_1() const { return ___Handle_1; }
	inline IntPtr_t* get_address_of_Handle_1() { return &___Handle_1; }
	inline void set_Handle_1(IntPtr_t value)
	{
		___Handle_1 = value;
	}

	inline static int32_t get_offset_of__shouldRelease_2() { return static_cast<int32_t>(offsetof(NSObject_t1518098886, ____shouldRelease_2)); }
	inline bool get__shouldRelease_2() const { return ____shouldRelease_2; }
	inline bool* get_address_of__shouldRelease_2() { return &____shouldRelease_2; }
	inline void set__shouldRelease_2(bool value)
	{
		____shouldRelease_2 = value;
	}
};

struct NSObject_t1518098886_StaticFields
{
public:
	// System.IntPtr ThirdParty.iOS4Unity.NSObject::_classHandle
	IntPtr_t ____classHandle_0;

public:
	inline static int32_t get_offset_of__classHandle_0() { return static_cast<int32_t>(offsetof(NSObject_t1518098886_StaticFields, ____classHandle_0)); }
	inline IntPtr_t get__classHandle_0() const { return ____classHandle_0; }
	inline IntPtr_t* get_address_of__classHandle_0() { return &____classHandle_0; }
	inline void set__classHandle_0(IntPtr_t value)
	{
		____classHandle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
