﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.AsyncCallback
struct AsyncCallback_t163412349;
// System.Object
struct Il2CppObject;
// Amazon.Runtime.AsyncOptions
struct AsyncOptions_t558351272;
// System.Action`4<Amazon.Runtime.AmazonWebServiceRequest,Amazon.Runtime.AmazonWebServiceResponse,System.Exception,Amazon.Runtime.AsyncOptions>
struct Action_4_t897279368;

#include "AWSSDK_Core_Amazon_Runtime_Internal_RequestContext2912551040.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.AsyncRequestContext
struct  AsyncRequestContext_t3705567694  : public RequestContext_t2912551040
{
public:
	// System.AsyncCallback Amazon.Runtime.Internal.AsyncRequestContext::<Callback>k__BackingField
	AsyncCallback_t163412349 * ___U3CCallbackU3Ek__BackingField_11;
	// System.Object Amazon.Runtime.Internal.AsyncRequestContext::<State>k__BackingField
	Il2CppObject * ___U3CStateU3Ek__BackingField_12;
	// Amazon.Runtime.AsyncOptions Amazon.Runtime.Internal.AsyncRequestContext::<AsyncOptions>k__BackingField
	AsyncOptions_t558351272 * ___U3CAsyncOptionsU3Ek__BackingField_13;
	// System.Action`4<Amazon.Runtime.AmazonWebServiceRequest,Amazon.Runtime.AmazonWebServiceResponse,System.Exception,Amazon.Runtime.AsyncOptions> Amazon.Runtime.Internal.AsyncRequestContext::<Action>k__BackingField
	Action_4_t897279368 * ___U3CActionU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(AsyncRequestContext_t3705567694, ___U3CCallbackU3Ek__BackingField_11)); }
	inline AsyncCallback_t163412349 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline AsyncCallback_t163412349 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(AsyncCallback_t163412349 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCallbackU3Ek__BackingField_11, value);
	}

	inline static int32_t get_offset_of_U3CStateU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(AsyncRequestContext_t3705567694, ___U3CStateU3Ek__BackingField_12)); }
	inline Il2CppObject * get_U3CStateU3Ek__BackingField_12() const { return ___U3CStateU3Ek__BackingField_12; }
	inline Il2CppObject ** get_address_of_U3CStateU3Ek__BackingField_12() { return &___U3CStateU3Ek__BackingField_12; }
	inline void set_U3CStateU3Ek__BackingField_12(Il2CppObject * value)
	{
		___U3CStateU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CStateU3Ek__BackingField_12, value);
	}

	inline static int32_t get_offset_of_U3CAsyncOptionsU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(AsyncRequestContext_t3705567694, ___U3CAsyncOptionsU3Ek__BackingField_13)); }
	inline AsyncOptions_t558351272 * get_U3CAsyncOptionsU3Ek__BackingField_13() const { return ___U3CAsyncOptionsU3Ek__BackingField_13; }
	inline AsyncOptions_t558351272 ** get_address_of_U3CAsyncOptionsU3Ek__BackingField_13() { return &___U3CAsyncOptionsU3Ek__BackingField_13; }
	inline void set_U3CAsyncOptionsU3Ek__BackingField_13(AsyncOptions_t558351272 * value)
	{
		___U3CAsyncOptionsU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CAsyncOptionsU3Ek__BackingField_13, value);
	}

	inline static int32_t get_offset_of_U3CActionU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(AsyncRequestContext_t3705567694, ___U3CActionU3Ek__BackingField_14)); }
	inline Action_4_t897279368 * get_U3CActionU3Ek__BackingField_14() const { return ___U3CActionU3Ek__BackingField_14; }
	inline Action_4_t897279368 ** get_address_of_U3CActionU3Ek__BackingField_14() { return &___U3CActionU3Ek__BackingField_14; }
	inline void set_U3CActionU3Ek__BackingField_14(Action_4_t897279368 * value)
	{
		___U3CActionU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CActionU3Ek__BackingField_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
