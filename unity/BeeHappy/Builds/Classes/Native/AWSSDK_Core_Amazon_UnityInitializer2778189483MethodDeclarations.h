﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.UnityInitializer
struct UnityInitializer_t2778189483;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void Amazon.UnityInitializer::.ctor()
extern "C"  void UnityInitializer__ctor_m155920996 (UnityInitializer_t2778189483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.UnityInitializer::AttachToGameObject(UnityEngine.GameObject)
extern "C"  void UnityInitializer_AttachToGameObject_m1571603833 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___gameObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.UnityInitializer Amazon.UnityInitializer::get_Instance()
extern "C"  UnityInitializer_t2778189483 * UnityInitializer_get_Instance_m819414178 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.UnityInitializer::Awake()
extern "C"  void UnityInitializer_Awake_m2541475451 (UnityInitializer_t2778189483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.UnityInitializer::IsMainThread()
extern "C"  bool UnityInitializer_IsMainThread_m1565754229 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.UnityInitializer::.cctor()
extern "C"  void UnityInitializer__cctor_m1427167625 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
