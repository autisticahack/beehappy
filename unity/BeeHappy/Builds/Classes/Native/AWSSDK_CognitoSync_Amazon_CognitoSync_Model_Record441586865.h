﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Nullable_1_gen3251239280.h"
#include "mscorlib_System_Nullable_1_gen3467111648.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.CognitoSync.Model.Record
struct  Record_t441586865  : public Il2CppObject
{
public:
	// System.Nullable`1<System.DateTime> Amazon.CognitoSync.Model.Record::_deviceLastModifiedDate
	Nullable_1_t3251239280  ____deviceLastModifiedDate_0;
	// System.String Amazon.CognitoSync.Model.Record::_key
	String_t* ____key_1;
	// System.String Amazon.CognitoSync.Model.Record::_lastModifiedBy
	String_t* ____lastModifiedBy_2;
	// System.Nullable`1<System.DateTime> Amazon.CognitoSync.Model.Record::_lastModifiedDate
	Nullable_1_t3251239280  ____lastModifiedDate_3;
	// System.Nullable`1<System.Int64> Amazon.CognitoSync.Model.Record::_syncCount
	Nullable_1_t3467111648  ____syncCount_4;
	// System.String Amazon.CognitoSync.Model.Record::_value
	String_t* ____value_5;

public:
	inline static int32_t get_offset_of__deviceLastModifiedDate_0() { return static_cast<int32_t>(offsetof(Record_t441586865, ____deviceLastModifiedDate_0)); }
	inline Nullable_1_t3251239280  get__deviceLastModifiedDate_0() const { return ____deviceLastModifiedDate_0; }
	inline Nullable_1_t3251239280 * get_address_of__deviceLastModifiedDate_0() { return &____deviceLastModifiedDate_0; }
	inline void set__deviceLastModifiedDate_0(Nullable_1_t3251239280  value)
	{
		____deviceLastModifiedDate_0 = value;
	}

	inline static int32_t get_offset_of__key_1() { return static_cast<int32_t>(offsetof(Record_t441586865, ____key_1)); }
	inline String_t* get__key_1() const { return ____key_1; }
	inline String_t** get_address_of__key_1() { return &____key_1; }
	inline void set__key_1(String_t* value)
	{
		____key_1 = value;
		Il2CppCodeGenWriteBarrier(&____key_1, value);
	}

	inline static int32_t get_offset_of__lastModifiedBy_2() { return static_cast<int32_t>(offsetof(Record_t441586865, ____lastModifiedBy_2)); }
	inline String_t* get__lastModifiedBy_2() const { return ____lastModifiedBy_2; }
	inline String_t** get_address_of__lastModifiedBy_2() { return &____lastModifiedBy_2; }
	inline void set__lastModifiedBy_2(String_t* value)
	{
		____lastModifiedBy_2 = value;
		Il2CppCodeGenWriteBarrier(&____lastModifiedBy_2, value);
	}

	inline static int32_t get_offset_of__lastModifiedDate_3() { return static_cast<int32_t>(offsetof(Record_t441586865, ____lastModifiedDate_3)); }
	inline Nullable_1_t3251239280  get__lastModifiedDate_3() const { return ____lastModifiedDate_3; }
	inline Nullable_1_t3251239280 * get_address_of__lastModifiedDate_3() { return &____lastModifiedDate_3; }
	inline void set__lastModifiedDate_3(Nullable_1_t3251239280  value)
	{
		____lastModifiedDate_3 = value;
	}

	inline static int32_t get_offset_of__syncCount_4() { return static_cast<int32_t>(offsetof(Record_t441586865, ____syncCount_4)); }
	inline Nullable_1_t3467111648  get__syncCount_4() const { return ____syncCount_4; }
	inline Nullable_1_t3467111648 * get_address_of__syncCount_4() { return &____syncCount_4; }
	inline void set__syncCount_4(Nullable_1_t3467111648  value)
	{
		____syncCount_4 = value;
	}

	inline static int32_t get_offset_of__value_5() { return static_cast<int32_t>(offsetof(Record_t441586865, ____value_5)); }
	inline String_t* get__value_5() const { return ____value_5; }
	inline String_t** get_address_of__value_5() { return &____value_5; }
	inline void set__value_5(String_t* value)
	{
		____value_5 = value;
		Il2CppCodeGenWriteBarrier(&____value_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
