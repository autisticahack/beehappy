﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Int64>
struct Dictionary_2_t3659156526;
// System.Collections.Generic.IEqualityComparer`1<Amazon.Runtime.Metric>
struct IEqualityComparer_1_t2486072980;
// System.Collections.Generic.IDictionary`2<Amazon.Runtime.Metric,System.Int64>
struct IDictionary_2_t1658239947;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// System.Collections.Generic.ICollection`1<Amazon.Runtime.Metric>
struct ICollection_1_t4225515507;
// System.Collections.ICollection
struct ICollection_t91669223;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<Amazon.Runtime.Metric,System.Int64>[]
struct KeyValuePair_2U5BU5D_t4007959485;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<Amazon.Runtime.Metric,System.Int64>>
struct IEnumerator_1_t3186992871;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t259680273;
// System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,System.Int64>
struct KeyCollection_t1847687001;
// System.Collections.Generic.Dictionary`2/ValueCollection<Amazon.Runtime.Metric,System.Int64>
struct ValueCollection_t2362216369;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serialization228987430.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21416501748.h"
#include "mscorlib_System_Array3829468939.h"
#include "AWSSDK_Core_Amazon_Runtime_Metric3273440202.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En684213932.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Int64>::.ctor()
extern "C"  void Dictionary_2__ctor_m2982017912_gshared (Dictionary_2_t3659156526 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m2982017912(__this, method) ((  void (*) (Dictionary_2_t3659156526 *, const MethodInfo*))Dictionary_2__ctor_m2982017912_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Int64>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m1656800300_gshared (Dictionary_2_t3659156526 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m1656800300(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t3659156526 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m1656800300_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Int64>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C"  void Dictionary_2__ctor_m2160223661_gshared (Dictionary_2_t3659156526 * __this, Il2CppObject* ___dictionary0, const MethodInfo* method);
#define Dictionary_2__ctor_m2160223661(__this, ___dictionary0, method) ((  void (*) (Dictionary_2_t3659156526 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2160223661_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Int64>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m2109170212_gshared (Dictionary_2_t3659156526 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m2109170212(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t3659156526 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m2109170212_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Int64>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m2509350724_gshared (Dictionary_2_t3659156526 * __this, Il2CppObject* ___dictionary0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m2509350724(__this, ___dictionary0, ___comparer1, method) ((  void (*) (Dictionary_2_t3659156526 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2509350724_gshared)(__this, ___dictionary0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Int64>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m2991664146_gshared (Dictionary_2_t3659156526 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m2991664146(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t3659156526 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2__ctor_m2991664146_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Int64>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m468673511_gshared (Dictionary_2_t3659156526 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m468673511(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t3659156526 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m468673511_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Int64>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Keys_m885056813_gshared (Dictionary_2_t3659156526 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m885056813(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3659156526 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m885056813_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Int64>::System.Collections.IDictionary.get_Values()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Values_m626001981_gshared (Dictionary_2_t3659156526 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Values_m626001981(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3659156526 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m626001981_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Int64>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m3950489387_gshared (Dictionary_2_t3659156526 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m3950489387(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t3659156526 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m3950489387_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Int64>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m232523830_gshared (Dictionary_2_t3659156526 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m232523830(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3659156526 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m232523830_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Int64>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m3639217813_gshared (Dictionary_2_t3659156526 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m3639217813(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3659156526 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m3639217813_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Int64>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool Dictionary_2_System_Collections_IDictionary_Contains_m2037179169_gshared (Dictionary_2_t3659156526 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m2037179169(__this, ___key0, method) ((  bool (*) (Dictionary_2_t3659156526 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m2037179169_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Int64>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m1994442378_gshared (Dictionary_2_t3659156526 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m1994442378(__this, ___key0, method) ((  void (*) (Dictionary_2_t3659156526 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m1994442378_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Int64>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3282090115_gshared (Dictionary_2_t3659156526 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3282090115(__this, method) ((  bool (*) (Dictionary_2_t3659156526 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3282090115_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Int64>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3979309527_gshared (Dictionary_2_t3659156526 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3979309527(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3659156526 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3979309527_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Int64>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2128489205_gshared (Dictionary_2_t3659156526 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2128489205(__this, method) ((  bool (*) (Dictionary_2_t3659156526 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2128489205_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Int64>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m476247704_gshared (Dictionary_2_t3659156526 * __this, KeyValuePair_2_t1416501748  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m476247704(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t3659156526 *, KeyValuePair_2_t1416501748 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m476247704_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Int64>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3136386716_gshared (Dictionary_2_t3659156526 * __this, KeyValuePair_2_t1416501748  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3136386716(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t3659156526 *, KeyValuePair_2_t1416501748 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3136386716_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Int64>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m3283301732_gshared (Dictionary_2_t3659156526 * __this, KeyValuePair_2U5BU5D_t4007959485* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m3283301732(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3659156526 *, KeyValuePair_2U5BU5D_t4007959485*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m3283301732_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Int64>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2731286503_gshared (Dictionary_2_t3659156526 * __this, KeyValuePair_2_t1416501748  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2731286503(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t3659156526 *, KeyValuePair_2_t1416501748 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2731286503_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Int64>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m4265579587_gshared (Dictionary_2_t3659156526 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m4265579587(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3659156526 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m4265579587_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Int64>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3087313130_gshared (Dictionary_2_t3659156526 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3087313130(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3659156526 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3087313130_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Int64>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m822325997_gshared (Dictionary_2_t3659156526 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m822325997(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t3659156526 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m822325997_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Int64>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2672774608_gshared (Dictionary_2_t3659156526 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2672774608(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3659156526 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2672774608_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Int64>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m2191033851_gshared (Dictionary_2_t3659156526 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m2191033851(__this, method) ((  int32_t (*) (Dictionary_2_t3659156526 *, const MethodInfo*))Dictionary_2_get_Count_m2191033851_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Int64>::get_Item(TKey)
extern "C"  int64_t Dictionary_2_get_Item_m4137522588_gshared (Dictionary_2_t3659156526 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m4137522588(__this, ___key0, method) ((  int64_t (*) (Dictionary_2_t3659156526 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m4137522588_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Int64>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m612651501_gshared (Dictionary_2_t3659156526 * __this, int32_t ___key0, int64_t ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m612651501(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3659156526 *, int32_t, int64_t, const MethodInfo*))Dictionary_2_set_Item_m612651501_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Int64>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m1950433943_gshared (Dictionary_2_t3659156526 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m1950433943(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t3659156526 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m1950433943_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Int64>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m2811147088_gshared (Dictionary_2_t3659156526 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m2811147088(__this, ___size0, method) ((  void (*) (Dictionary_2_t3659156526 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m2811147088_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Int64>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m488080626_gshared (Dictionary_2_t3659156526 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m488080626(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3659156526 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m488080626_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Int64>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t1416501748  Dictionary_2_make_pair_m4098311912_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, int64_t ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m4098311912(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t1416501748  (*) (Il2CppObject * /* static, unused */, int32_t, int64_t, const MethodInfo*))Dictionary_2_make_pair_m4098311912_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Int64>::pick_key(TKey,TValue)
extern "C"  int32_t Dictionary_2_pick_key_m4031978734_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, int64_t ___value1, const MethodInfo* method);
#define Dictionary_2_pick_key_m4031978734(__this /* static, unused */, ___key0, ___value1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, int32_t, int64_t, const MethodInfo*))Dictionary_2_pick_key_m4031978734_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Int64>::pick_value(TKey,TValue)
extern "C"  int64_t Dictionary_2_pick_value_m1291067502_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, int64_t ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m1291067502(__this /* static, unused */, ___key0, ___value1, method) ((  int64_t (*) (Il2CppObject * /* static, unused */, int32_t, int64_t, const MethodInfo*))Dictionary_2_pick_value_m1291067502_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Int64>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m3782590579_gshared (Dictionary_2_t3659156526 * __this, KeyValuePair_2U5BU5D_t4007959485* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m3782590579(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3659156526 *, KeyValuePair_2U5BU5D_t4007959485*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m3782590579_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Int64>::Resize()
extern "C"  void Dictionary_2_Resize_m1701365245_gshared (Dictionary_2_t3659156526 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m1701365245(__this, method) ((  void (*) (Dictionary_2_t3659156526 *, const MethodInfo*))Dictionary_2_Resize_m1701365245_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Int64>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m367267728_gshared (Dictionary_2_t3659156526 * __this, int32_t ___key0, int64_t ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m367267728(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3659156526 *, int32_t, int64_t, const MethodInfo*))Dictionary_2_Add_m367267728_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Int64>::Clear()
extern "C"  void Dictionary_2_Clear_m998975968_gshared (Dictionary_2_t3659156526 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m998975968(__this, method) ((  void (*) (Dictionary_2_t3659156526 *, const MethodInfo*))Dictionary_2_Clear_m998975968_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Int64>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m3228408644_gshared (Dictionary_2_t3659156526 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m3228408644(__this, ___key0, method) ((  bool (*) (Dictionary_2_t3659156526 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m3228408644_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Int64>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m652393252_gshared (Dictionary_2_t3659156526 * __this, int64_t ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m652393252(__this, ___value0, method) ((  bool (*) (Dictionary_2_t3659156526 *, int64_t, const MethodInfo*))Dictionary_2_ContainsValue_m652393252_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Int64>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m2266060959_gshared (Dictionary_2_t3659156526 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m2266060959(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t3659156526 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2_GetObjectData_m2266060959_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Int64>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m1791721383_gshared (Dictionary_2_t3659156526 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m1791721383(__this, ___sender0, method) ((  void (*) (Dictionary_2_t3659156526 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m1791721383_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Int64>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m3000460116_gshared (Dictionary_2_t3659156526 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m3000460116(__this, ___key0, method) ((  bool (*) (Dictionary_2_t3659156526 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m3000460116_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Int64>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m4250763159_gshared (Dictionary_2_t3659156526 * __this, int32_t ___key0, int64_t* ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m4250763159(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t3659156526 *, int32_t, int64_t*, const MethodInfo*))Dictionary_2_TryGetValue_m4250763159_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Int64>::get_Keys()
extern "C"  KeyCollection_t1847687001 * Dictionary_2_get_Keys_m348810428_gshared (Dictionary_2_t3659156526 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m348810428(__this, method) ((  KeyCollection_t1847687001 * (*) (Dictionary_2_t3659156526 *, const MethodInfo*))Dictionary_2_get_Keys_m348810428_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Int64>::get_Values()
extern "C"  ValueCollection_t2362216369 * Dictionary_2_get_Values_m2130884060_gshared (Dictionary_2_t3659156526 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m2130884060(__this, method) ((  ValueCollection_t2362216369 * (*) (Dictionary_2_t3659156526 *, const MethodInfo*))Dictionary_2_get_Values_m2130884060_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Int64>::ToTKey(System.Object)
extern "C"  int32_t Dictionary_2_ToTKey_m3481935197_gshared (Dictionary_2_t3659156526 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m3481935197(__this, ___key0, method) ((  int32_t (*) (Dictionary_2_t3659156526 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m3481935197_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Int64>::ToTValue(System.Object)
extern "C"  int64_t Dictionary_2_ToTValue_m4114554557_gshared (Dictionary_2_t3659156526 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m4114554557(__this, ___value0, method) ((  int64_t (*) (Dictionary_2_t3659156526 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m4114554557_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Int64>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m1014927295_gshared (Dictionary_2_t3659156526 * __this, KeyValuePair_2_t1416501748  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m1014927295(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t3659156526 *, KeyValuePair_2_t1416501748 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m1014927295_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Int64>::GetEnumerator()
extern "C"  Enumerator_t684213932  Dictionary_2_GetEnumerator_m1102437162_gshared (Dictionary_2_t3659156526 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m1102437162(__this, method) ((  Enumerator_t684213932  (*) (Dictionary_2_t3659156526 *, const MethodInfo*))Dictionary_2_GetEnumerator_m1102437162_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Int64>::<CopyTo>m__0(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Dictionary_2_U3CCopyToU3Em__0_m3015852301_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, int64_t ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m3015852301(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Il2CppObject * /* static, unused */, int32_t, int64_t, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m3015852301_gshared)(__this /* static, unused */, ___key0, ___value1, method)
