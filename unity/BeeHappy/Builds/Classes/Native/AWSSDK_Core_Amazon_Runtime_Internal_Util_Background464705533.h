﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.Runtime.Internal.Util.BackgroundInvoker/<>c
struct U3CU3Ec_t464705533;
// System.Action`1<System.Action>
struct Action_1_t3028271134;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Util.BackgroundInvoker/<>c
struct  U3CU3Ec_t464705533  : public Il2CppObject
{
public:

public:
};

struct U3CU3Ec_t464705533_StaticFields
{
public:
	// Amazon.Runtime.Internal.Util.BackgroundInvoker/<>c Amazon.Runtime.Internal.Util.BackgroundInvoker/<>c::<>9
	U3CU3Ec_t464705533 * ___U3CU3E9_0;
	// System.Action`1<System.Action> Amazon.Runtime.Internal.Util.BackgroundInvoker/<>c::<>9__0_0
	Action_1_t3028271134 * ___U3CU3E9__0_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t464705533_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t464705533 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t464705533 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t464705533 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9_0, value);
	}

	inline static int32_t get_offset_of_U3CU3E9__0_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t464705533_StaticFields, ___U3CU3E9__0_0_1)); }
	inline Action_1_t3028271134 * get_U3CU3E9__0_0_1() const { return ___U3CU3E9__0_0_1; }
	inline Action_1_t3028271134 ** get_address_of_U3CU3E9__0_0_1() { return &___U3CU3E9__0_0_1; }
	inline void set_U3CU3E9__0_0_1(Action_1_t3028271134 * value)
	{
		___U3CU3E9__0_0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9__0_0_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
