﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Stack`1<System.String>
struct Stack_1_t3116948387;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;
// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext/JsonPathStack
struct  JsonPathStack_t805090354  : public Il2CppObject
{
public:
	// System.Collections.Generic.Stack`1<System.String> Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext/JsonPathStack::stack
	Stack_1_t3116948387 * ___stack_0;
	// System.Int32 Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext/JsonPathStack::currentDepth
	int32_t ___currentDepth_1;
	// System.Text.StringBuilder Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext/JsonPathStack::stackStringBuilder
	StringBuilder_t1221177846 * ___stackStringBuilder_2;
	// System.String Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext/JsonPathStack::stackString
	String_t* ___stackString_3;

public:
	inline static int32_t get_offset_of_stack_0() { return static_cast<int32_t>(offsetof(JsonPathStack_t805090354, ___stack_0)); }
	inline Stack_1_t3116948387 * get_stack_0() const { return ___stack_0; }
	inline Stack_1_t3116948387 ** get_address_of_stack_0() { return &___stack_0; }
	inline void set_stack_0(Stack_1_t3116948387 * value)
	{
		___stack_0 = value;
		Il2CppCodeGenWriteBarrier(&___stack_0, value);
	}

	inline static int32_t get_offset_of_currentDepth_1() { return static_cast<int32_t>(offsetof(JsonPathStack_t805090354, ___currentDepth_1)); }
	inline int32_t get_currentDepth_1() const { return ___currentDepth_1; }
	inline int32_t* get_address_of_currentDepth_1() { return &___currentDepth_1; }
	inline void set_currentDepth_1(int32_t value)
	{
		___currentDepth_1 = value;
	}

	inline static int32_t get_offset_of_stackStringBuilder_2() { return static_cast<int32_t>(offsetof(JsonPathStack_t805090354, ___stackStringBuilder_2)); }
	inline StringBuilder_t1221177846 * get_stackStringBuilder_2() const { return ___stackStringBuilder_2; }
	inline StringBuilder_t1221177846 ** get_address_of_stackStringBuilder_2() { return &___stackStringBuilder_2; }
	inline void set_stackStringBuilder_2(StringBuilder_t1221177846 * value)
	{
		___stackStringBuilder_2 = value;
		Il2CppCodeGenWriteBarrier(&___stackStringBuilder_2, value);
	}

	inline static int32_t get_offset_of_stackString_3() { return static_cast<int32_t>(offsetof(JsonPathStack_t805090354, ___stackString_3)); }
	inline String_t* get_stackString_3() const { return ___stackString_3; }
	inline String_t** get_address_of_stackString_3() { return &___stackString_3; }
	inline void set_stackString_3(String_t* value)
	{
		___stackString_3 = value;
		Il2CppCodeGenWriteBarrier(&___stackString_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
