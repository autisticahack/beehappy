﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AccelBrain.BinauralBeat
struct BinauralBeat_t4195950836;
// System.Single[]
struct SingleU5BU5D_t577127397;

#include "codegen/il2cpp-codegen.h"

// System.Void AccelBrain.BinauralBeat::.ctor()
extern "C"  void BinauralBeat__ctor_m2423997387 (BinauralBeat_t4195950836 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single[] AccelBrain.BinauralBeat::UpdatePhase(System.Single[],System.Int32)
extern "C"  SingleU5BU5D_t577127397* BinauralBeat_UpdatePhase_m665429791 (BinauralBeat_t4195950836 * __this, SingleU5BU5D_t577127397* ___data0, int32_t ___channels1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
