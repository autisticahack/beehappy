﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Util_Backgroun2693883954MethodDeclarations.h"

// System.Void Amazon.Runtime.Internal.Util.BackgroundDispatcher`1<System.Action>::set_IsRunning(System.Boolean)
#define BackgroundDispatcher_1_set_IsRunning_m38967788(__this, ___value0, method) ((  void (*) (BackgroundDispatcher_1_t3230906411 *, bool, const MethodInfo*))BackgroundDispatcher_1_set_IsRunning_m2776258133_gshared)(__this, ___value0, method)
// System.Void Amazon.Runtime.Internal.Util.BackgroundDispatcher`1<System.Action>::.ctor(System.Action`1<T>)
#define BackgroundDispatcher_1__ctor_m3109929531(__this, ___action0, method) ((  void (*) (BackgroundDispatcher_1_t3230906411 *, Action_1_t3028271134 *, const MethodInfo*))BackgroundDispatcher_1__ctor_m2175859570_gshared)(__this, ___action0, method)
// System.Void Amazon.Runtime.Internal.Util.BackgroundDispatcher`1<System.Action>::Finalize()
#define BackgroundDispatcher_1_Finalize_m586035163(__this, method) ((  void (*) (BackgroundDispatcher_1_t3230906411 *, const MethodInfo*))BackgroundDispatcher_1_Finalize_m1140862552_gshared)(__this, method)
// System.Void Amazon.Runtime.Internal.Util.BackgroundDispatcher`1<System.Action>::Dispose(System.Boolean)
#define BackgroundDispatcher_1_Dispose_m3874969693(__this, ___disposing0, method) ((  void (*) (BackgroundDispatcher_1_t3230906411 *, bool, const MethodInfo*))BackgroundDispatcher_1_Dispose_m2779145776_gshared)(__this, ___disposing0, method)
// System.Void Amazon.Runtime.Internal.Util.BackgroundDispatcher`1<System.Action>::Dispose()
#define BackgroundDispatcher_1_Dispose_m3022874372(__this, method) ((  void (*) (BackgroundDispatcher_1_t3230906411 *, const MethodInfo*))BackgroundDispatcher_1_Dispose_m3297611021_gshared)(__this, method)
// System.Void Amazon.Runtime.Internal.Util.BackgroundDispatcher`1<System.Action>::Dispatch(T)
#define BackgroundDispatcher_1_Dispatch_m2553871301(__this, ___data0, method) ((  void (*) (BackgroundDispatcher_1_t3230906411 *, Action_t3226471752 *, const MethodInfo*))BackgroundDispatcher_1_Dispatch_m2867538446_gshared)(__this, ___data0, method)
// System.Void Amazon.Runtime.Internal.Util.BackgroundDispatcher`1<System.Action>::Stop()
#define BackgroundDispatcher_1_Stop_m585381433(__this, method) ((  void (*) (BackgroundDispatcher_1_t3230906411 *, const MethodInfo*))BackgroundDispatcher_1_Stop_m4186726394_gshared)(__this, method)
// System.Void Amazon.Runtime.Internal.Util.BackgroundDispatcher`1<System.Action>::Run()
#define BackgroundDispatcher_1_Run_m4095251048(__this, method) ((  void (*) (BackgroundDispatcher_1_t3230906411 *, const MethodInfo*))BackgroundDispatcher_1_Run_m3030340363_gshared)(__this, method)
// System.Void Amazon.Runtime.Internal.Util.BackgroundDispatcher`1<System.Action>::HandleInvoked()
#define BackgroundDispatcher_1_HandleInvoked_m4068175683(__this, method) ((  void (*) (BackgroundDispatcher_1_t3230906411 *, const MethodInfo*))BackgroundDispatcher_1_HandleInvoked_m4068660570_gshared)(__this, method)
