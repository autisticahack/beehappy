﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GlobalControl
struct GlobalControl_t733410152;

#include "codegen/il2cpp-codegen.h"

// System.Void GlobalControl::.ctor()
extern "C"  void GlobalControl__ctor_m1751950731 (GlobalControl_t733410152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalControl::Awake()
extern "C"  void GlobalControl_Awake_m2280696964 (GlobalControl_t733410152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
