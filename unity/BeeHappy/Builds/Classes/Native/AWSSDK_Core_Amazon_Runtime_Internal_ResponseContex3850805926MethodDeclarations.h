﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.ResponseContext
struct ResponseContext_t3850805926;
// Amazon.Runtime.AmazonWebServiceResponse
struct AmazonWebServiceResponse_t529043356;
// Amazon.Runtime.Internal.Transform.IWebResponseData
struct IWebResponseData_t3815728244;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_Runtime_AmazonWebServiceResponse529043356.h"

// Amazon.Runtime.AmazonWebServiceResponse Amazon.Runtime.Internal.ResponseContext::get_Response()
extern "C"  AmazonWebServiceResponse_t529043356 * ResponseContext_get_Response_m529627935 (ResponseContext_t3850805926 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.ResponseContext::set_Response(Amazon.Runtime.AmazonWebServiceResponse)
extern "C"  void ResponseContext_set_Response_m1473138272 (ResponseContext_t3850805926 * __this, AmazonWebServiceResponse_t529043356 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.Internal.Transform.IWebResponseData Amazon.Runtime.Internal.ResponseContext::get_HttpResponse()
extern "C"  Il2CppObject * ResponseContext_get_HttpResponse_m2566406534 (ResponseContext_t3850805926 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.ResponseContext::set_HttpResponse(Amazon.Runtime.Internal.Transform.IWebResponseData)
extern "C"  void ResponseContext_set_HttpResponse_m1189829017 (ResponseContext_t3850805926 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.ResponseContext::.ctor()
extern "C"  void ResponseContext__ctor_m1598500152 (ResponseContext_t3850805926 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
