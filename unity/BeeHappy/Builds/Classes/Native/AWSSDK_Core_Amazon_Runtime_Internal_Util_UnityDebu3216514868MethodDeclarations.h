﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.Util.UnityDebugLogger
struct UnityDebugLogger_t3216514868;
// System.Type
struct Type_t;
// System.Exception
struct Exception_t1927440687;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t3614634134;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_Exception1927440687.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Amazon.Runtime.Internal.Util.UnityDebugLogger::.ctor(System.Type)
extern "C"  void UnityDebugLogger__ctor_m1607192419 (UnityDebugLogger_t3216514868 * __this, Type_t * ___declaringType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.UnityDebugLogger::Error(System.Exception,System.String,System.Object[])
extern "C"  void UnityDebugLogger_Error_m4083115790 (UnityDebugLogger_t3216514868 * __this, Exception_t1927440687 * ___exception0, String_t* ___messageFormat1, ObjectU5BU5D_t3614634134* ___args2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.UnityDebugLogger::Debug(System.Exception,System.String,System.Object[])
extern "C"  void UnityDebugLogger_Debug_m1776825079 (UnityDebugLogger_t3216514868 * __this, Exception_t1927440687 * ___exception0, String_t* ___messageFormat1, ObjectU5BU5D_t3614634134* ___args2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.UnityDebugLogger::DebugFormat(System.String,System.Object[])
extern "C"  void UnityDebugLogger_DebugFormat_m2240071064 (UnityDebugLogger_t3216514868 * __this, String_t* ___messageFormat0, ObjectU5BU5D_t3614634134* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.UnityDebugLogger::InfoFormat(System.String,System.Object[])
extern "C"  void UnityDebugLogger_InfoFormat_m482101243 (UnityDebugLogger_t3216514868 * __this, String_t* ___message0, ObjectU5BU5D_t3614634134* ___arguments1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.Util.UnityDebugLogger::get_IsDebugEnabled()
extern "C"  bool UnityDebugLogger_get_IsDebugEnabled_m323444055 (UnityDebugLogger_t3216514868 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.Util.UnityDebugLogger::get_IsErrorEnabled()
extern "C"  bool UnityDebugLogger_get_IsErrorEnabled_m2345989618 (UnityDebugLogger_t3216514868 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.Util.UnityDebugLogger::get_IsInfoEnabled()
extern "C"  bool UnityDebugLogger_get_IsInfoEnabled_m1656059208 (UnityDebugLogger_t3216514868 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
