﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.CognitoSync.SyncManager.Dataset
struct Dataset_t3040902086;
// System.String
struct String_t;
// Amazon.CognitoSync.SyncManager.ILocalStorage
struct ILocalStorage_t1053993977;
// Amazon.CognitoSync.SyncManager.Internal.CognitoSyncStorage
struct CognitoSyncStorage_t101590051;
// Amazon.CognitoIdentity.CognitoAWSCredentials
struct CognitoAWSCredentials_t2370264792;
// Amazon.CognitoSync.SyncManager.DatasetMetadata
struct DatasetMetadata_t1205730659;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.Record>
struct List_1_t237920701;
// Amazon.Runtime.AsyncOptions
struct AsyncOptions_t558351272;
// System.EventHandler`1<Amazon.CognitoSync.SyncManager.SyncSuccessEventArgs>
struct EventHandler_1_t2868688519;
// System.EventHandler`1<Amazon.CognitoSync.SyncManager.SyncFailureEventArgs>
struct EventHandler_1_t339652670;
// System.Exception
struct Exception_t1927440687;
// System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.SyncConflict>
struct List_1_t2399431441;
// System.Object
struct Il2CppObject;
// Amazon.Util.Internal.PlatformServices.NetworkStatusEventArgs
struct NetworkStatusEventArgs_t1607167653;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Cogn2370264792.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_SyncManager_I101590051.h"
#include "AWSSDK_Core_Amazon_Runtime_AsyncOptions558351272.h"
#include "mscorlib_System_Exception1927440687.h"
#include "mscorlib_System_Object2689449295.h"
#include "AWSSDK_Core_Amazon_Util_Internal_PlatformServices_1607167653.h"

// System.String Amazon.CognitoSync.SyncManager.Dataset::get_DatasetName()
extern "C"  String_t* Dataset_get_DatasetName_m2161193675 (Dataset_t3040902086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.CognitoSync.SyncManager.ILocalStorage Amazon.CognitoSync.SyncManager.Dataset::get_Local()
extern "C"  Il2CppObject * Dataset_get_Local_m2937190166 (Dataset_t3040902086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.CognitoSync.SyncManager.Internal.CognitoSyncStorage Amazon.CognitoSync.SyncManager.Dataset::get_Remote()
extern "C"  CognitoSyncStorage_t101590051 * Dataset_get_Remote_m3980912366 (Dataset_t3040902086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.CognitoIdentity.CognitoAWSCredentials Amazon.CognitoSync.SyncManager.Dataset::get_CognitoCredentials()
extern "C"  CognitoAWSCredentials_t2370264792 * Dataset_get_CognitoCredentials_m2482903278 (Dataset_t3040902086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.SyncManager.Dataset::.ctor(System.String,Amazon.CognitoIdentity.CognitoAWSCredentials,Amazon.CognitoSync.SyncManager.ILocalStorage,Amazon.CognitoSync.SyncManager.Internal.CognitoSyncStorage)
extern "C"  void Dataset__ctor_m1444350612 (Dataset_t3040902086 * __this, String_t* ___datasetName0, CognitoAWSCredentials_t2370264792 * ___cognitoCredentials1, Il2CppObject * ___local2, CognitoSyncStorage_t101590051 * ___remote3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.SyncManager.Dataset::Dispose()
extern "C"  void Dataset_Dispose_m324351829 (Dataset_t3040902086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.CognitoSync.SyncManager.DatasetMetadata Amazon.CognitoSync.SyncManager.Dataset::get_Metadata()
extern "C"  DatasetMetadata_t1205730659 * Dataset_get_Metadata_m1847517466 (Dataset_t3040902086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> Amazon.CognitoSync.SyncManager.Dataset::get_LocalMergedDatasets()
extern "C"  List_1_t1398341365 * Dataset_get_LocalMergedDatasets_m1577558948 (Dataset_t3040902086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.CognitoSync.SyncManager.Dataset::Get(System.String)
extern "C"  String_t* Dataset_Get_m1682763723 (Dataset_t3040902086 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.SyncManager.Dataset::Put(System.String,System.String)
extern "C"  void Dataset_Put_m929553147 (Dataset_t3040902086 * __this, String_t* ___key0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.SyncManager.Dataset::Resolve(System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.Record>)
extern "C"  void Dataset_Resolve_m3481005177 (Dataset_t3040902086 * __this, List_1_t237920701 * ___remoteRecords0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.SyncManager.Dataset::EndSynchronizeAndCleanup()
extern "C"  void Dataset_EndSynchronizeAndCleanup_m3945080446 (Dataset_t3040902086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.SyncManager.Dataset::RunSyncOperation(System.Int32,Amazon.Runtime.AsyncOptions)
extern "C"  void Dataset_RunSyncOperation_m2957506524 (Dataset_t3040902086 * __this, int32_t ___retry0, AsyncOptions_t558351272 * ___options1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.SyncManager.Dataset::SynchornizeInternal(Amazon.Runtime.AsyncOptions)
extern "C"  void Dataset_SynchornizeInternal_m1543143363 (Dataset_t3040902086 * __this, AsyncOptions_t558351272 * ___options0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.CognitoSync.SyncManager.Dataset::get_IdentityId()
extern "C"  String_t* Dataset_get_IdentityId_m2637396569 (Dataset_t3040902086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.Record> Amazon.CognitoSync.SyncManager.Dataset::get_ModifiedRecords()
extern "C"  List_1_t237920701 * Dataset_get_ModifiedRecords_m744267798 (Dataset_t3040902086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.SyncManager.Dataset::add_OnSyncSuccess(System.EventHandler`1<Amazon.CognitoSync.SyncManager.SyncSuccessEventArgs>)
extern "C"  void Dataset_add_OnSyncSuccess_m3911304524 (Dataset_t3040902086 * __this, EventHandler_1_t2868688519 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.SyncManager.Dataset::remove_OnSyncSuccess(System.EventHandler`1<Amazon.CognitoSync.SyncManager.SyncSuccessEventArgs>)
extern "C"  void Dataset_remove_OnSyncSuccess_m1453138909 (Dataset_t3040902086 * __this, EventHandler_1_t2868688519 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.SyncManager.Dataset::add_OnSyncFailure(System.EventHandler`1<Amazon.CognitoSync.SyncManager.SyncFailureEventArgs>)
extern "C"  void Dataset_add_OnSyncFailure_m3168034446 (Dataset_t3040902086 * __this, EventHandler_1_t339652670 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.SyncManager.Dataset::remove_OnSyncFailure(System.EventHandler`1<Amazon.CognitoSync.SyncManager.SyncFailureEventArgs>)
extern "C"  void Dataset_remove_OnSyncFailure_m3155988773 (Dataset_t3040902086 * __this, EventHandler_1_t339652670 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.SyncManager.Dataset::FireSyncSuccessEvent(System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.Record>)
extern "C"  void Dataset_FireSyncSuccessEvent_m531650873 (Dataset_t3040902086 * __this, List_1_t237920701 * ___records0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.SyncManager.Dataset::FireSyncFailureEvent(System.Exception)
extern "C"  void Dataset_FireSyncFailureEvent_m1516112925 (Dataset_t3040902086 * __this, Exception_t1927440687 * ___exception0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.SyncManager.Dataset::ClearAllDelegates()
extern "C"  void Dataset_ClearAllDelegates_m2342481326 (Dataset_t3040902086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.CognitoSync.SyncManager.Dataset::ResolveConflictsWithDefaultPolicy(System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.SyncConflict>)
extern "C"  bool Dataset_ResolveConflictsWithDefaultPolicy_m2018311653 (Dataset_t3040902086 * __this, List_1_t2399431441 * ___conflicts0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.SyncManager.Dataset::SynchronizeAsync(Amazon.Runtime.AsyncOptions)
extern "C"  void Dataset_SynchronizeAsync_m579289082 (Dataset_t3040902086 * __this, AsyncOptions_t558351272 * ___options0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.SyncManager.Dataset::DatasetSetupInternal()
extern "C"  void Dataset_DatasetSetupInternal_m2337812700 (Dataset_t3040902086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.SyncManager.Dataset::OnNetworkReachabilityChanged(System.Object,Amazon.Util.Internal.PlatformServices.NetworkStatusEventArgs)
extern "C"  void Dataset_OnNetworkReachabilityChanged_m141952307 (Dataset_t3040902086 * __this, Il2CppObject * ___sender0, NetworkStatusEventArgs_t1607167653 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.SyncManager.Dataset::Dispose(System.Boolean)
extern "C"  void Dataset_Dispose_m1499194874 (Dataset_t3040902086 * __this, bool ___disposing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.SyncManager.Dataset::SynchronizeHelperAsync(Amazon.Runtime.AsyncOptions)
extern "C"  void Dataset_SynchronizeHelperAsync_m1974048278 (Dataset_t3040902086 * __this, AsyncOptions_t558351272 * ___options0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.SyncManager.Dataset::FireSyncSuccessEvent(System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.Record>,Amazon.Runtime.AsyncOptions)
extern "C"  void Dataset_FireSyncSuccessEvent_m3183952141 (Dataset_t3040902086 * __this, List_1_t237920701 * ___records0, AsyncOptions_t558351272 * ___options1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.SyncManager.Dataset::FireSyncFailureEvent(System.Exception,Amazon.Runtime.AsyncOptions)
extern "C"  void Dataset_FireSyncFailureEvent_m4224676081 (Dataset_t3040902086 * __this, Exception_t1927440687 * ___exception0, AsyncOptions_t558351272 * ___options1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
