﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// Amazon.Runtime.Internal.UnityMainThreadDispatcher
struct UnityMainThreadDispatcher_t4098072663;
// Amazon.Runtime.IUnityHttpRequest
struct IUnityHttpRequest_t1859903397;
// UnityEngine.WWW
struct WWW_t2919945039;
// Amazon.Runtime.Internal.UnityMainThreadDispatcher/<>c__DisplayClass7_0
struct U3CU3Ec__DisplayClass7_0_t142100588;
// Amazon.Runtime.Internal.UnityRequest
struct UnityRequest_t4218620704;
// UnityEngine.AsyncOperation
struct AsyncOperation_t3814632279;
// Amazon.Runtime.Internal.UnityWebRequestWrapper
struct UnityWebRequestWrapper_t1542496577;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.UnityMainThreadDispatcher/<InvokeRequest>d__7
struct  U3CInvokeRequestU3Ed__7_t3085820275  : public Il2CppObject
{
public:
	// System.Int32 Amazon.Runtime.Internal.UnityMainThreadDispatcher/<InvokeRequest>d__7::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Amazon.Runtime.Internal.UnityMainThreadDispatcher/<InvokeRequest>d__7::<>2__current
	Il2CppObject * ___U3CU3E2__current_1;
	// Amazon.Runtime.Internal.UnityMainThreadDispatcher Amazon.Runtime.Internal.UnityMainThreadDispatcher/<InvokeRequest>d__7::<>4__this
	UnityMainThreadDispatcher_t4098072663 * ___U3CU3E4__this_2;
	// Amazon.Runtime.IUnityHttpRequest Amazon.Runtime.Internal.UnityMainThreadDispatcher/<InvokeRequest>d__7::request
	Il2CppObject * ___request_3;
	// UnityEngine.WWW Amazon.Runtime.Internal.UnityMainThreadDispatcher/<InvokeRequest>d__7::<wwwRequest>5__1
	WWW_t2919945039 * ___U3CwwwRequestU3E5__1_4;
	// System.Boolean Amazon.Runtime.Internal.UnityMainThreadDispatcher/<InvokeRequest>d__7::<uploadCompleted>5__2
	bool ___U3CuploadCompletedU3E5__2_5;
	// Amazon.Runtime.Internal.UnityMainThreadDispatcher/<>c__DisplayClass7_0 Amazon.Runtime.Internal.UnityMainThreadDispatcher/<InvokeRequest>d__7::<>8__3
	U3CU3Ec__DisplayClass7_0_t142100588 * ___U3CU3E8__3_6;
	// Amazon.Runtime.Internal.UnityRequest Amazon.Runtime.Internal.UnityMainThreadDispatcher/<InvokeRequest>d__7::<unityRequest>5__4
	UnityRequest_t4218620704 * ___U3CunityRequestU3E5__4_7;
	// UnityEngine.AsyncOperation Amazon.Runtime.Internal.UnityMainThreadDispatcher/<InvokeRequest>d__7::<operation>5__5
	AsyncOperation_t3814632279 * ___U3CoperationU3E5__5_8;
	// System.Boolean Amazon.Runtime.Internal.UnityMainThreadDispatcher/<InvokeRequest>d__7::<uploadCompleted>5__6
	bool ___U3CuploadCompletedU3E5__6_9;
	// Amazon.Runtime.Internal.UnityWebRequestWrapper Amazon.Runtime.Internal.UnityMainThreadDispatcher/<InvokeRequest>d__7::<unityWebRequest>5__7
	UnityWebRequestWrapper_t1542496577 * ___U3CunityWebRequestU3E5__7_10;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CInvokeRequestU3Ed__7_t3085820275, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CInvokeRequestU3Ed__7_t3085820275, ___U3CU3E2__current_1)); }
	inline Il2CppObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline Il2CppObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(Il2CppObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E2__current_1, value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CInvokeRequestU3Ed__7_t3085820275, ___U3CU3E4__this_2)); }
	inline UnityMainThreadDispatcher_t4098072663 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline UnityMainThreadDispatcher_t4098072663 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(UnityMainThreadDispatcher_t4098072663 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E4__this_2, value);
	}

	inline static int32_t get_offset_of_request_3() { return static_cast<int32_t>(offsetof(U3CInvokeRequestU3Ed__7_t3085820275, ___request_3)); }
	inline Il2CppObject * get_request_3() const { return ___request_3; }
	inline Il2CppObject ** get_address_of_request_3() { return &___request_3; }
	inline void set_request_3(Il2CppObject * value)
	{
		___request_3 = value;
		Il2CppCodeGenWriteBarrier(&___request_3, value);
	}

	inline static int32_t get_offset_of_U3CwwwRequestU3E5__1_4() { return static_cast<int32_t>(offsetof(U3CInvokeRequestU3Ed__7_t3085820275, ___U3CwwwRequestU3E5__1_4)); }
	inline WWW_t2919945039 * get_U3CwwwRequestU3E5__1_4() const { return ___U3CwwwRequestU3E5__1_4; }
	inline WWW_t2919945039 ** get_address_of_U3CwwwRequestU3E5__1_4() { return &___U3CwwwRequestU3E5__1_4; }
	inline void set_U3CwwwRequestU3E5__1_4(WWW_t2919945039 * value)
	{
		___U3CwwwRequestU3E5__1_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwRequestU3E5__1_4, value);
	}

	inline static int32_t get_offset_of_U3CuploadCompletedU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CInvokeRequestU3Ed__7_t3085820275, ___U3CuploadCompletedU3E5__2_5)); }
	inline bool get_U3CuploadCompletedU3E5__2_5() const { return ___U3CuploadCompletedU3E5__2_5; }
	inline bool* get_address_of_U3CuploadCompletedU3E5__2_5() { return &___U3CuploadCompletedU3E5__2_5; }
	inline void set_U3CuploadCompletedU3E5__2_5(bool value)
	{
		___U3CuploadCompletedU3E5__2_5 = value;
	}

	inline static int32_t get_offset_of_U3CU3E8__3_6() { return static_cast<int32_t>(offsetof(U3CInvokeRequestU3Ed__7_t3085820275, ___U3CU3E8__3_6)); }
	inline U3CU3Ec__DisplayClass7_0_t142100588 * get_U3CU3E8__3_6() const { return ___U3CU3E8__3_6; }
	inline U3CU3Ec__DisplayClass7_0_t142100588 ** get_address_of_U3CU3E8__3_6() { return &___U3CU3E8__3_6; }
	inline void set_U3CU3E8__3_6(U3CU3Ec__DisplayClass7_0_t142100588 * value)
	{
		___U3CU3E8__3_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E8__3_6, value);
	}

	inline static int32_t get_offset_of_U3CunityRequestU3E5__4_7() { return static_cast<int32_t>(offsetof(U3CInvokeRequestU3Ed__7_t3085820275, ___U3CunityRequestU3E5__4_7)); }
	inline UnityRequest_t4218620704 * get_U3CunityRequestU3E5__4_7() const { return ___U3CunityRequestU3E5__4_7; }
	inline UnityRequest_t4218620704 ** get_address_of_U3CunityRequestU3E5__4_7() { return &___U3CunityRequestU3E5__4_7; }
	inline void set_U3CunityRequestU3E5__4_7(UnityRequest_t4218620704 * value)
	{
		___U3CunityRequestU3E5__4_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CunityRequestU3E5__4_7, value);
	}

	inline static int32_t get_offset_of_U3CoperationU3E5__5_8() { return static_cast<int32_t>(offsetof(U3CInvokeRequestU3Ed__7_t3085820275, ___U3CoperationU3E5__5_8)); }
	inline AsyncOperation_t3814632279 * get_U3CoperationU3E5__5_8() const { return ___U3CoperationU3E5__5_8; }
	inline AsyncOperation_t3814632279 ** get_address_of_U3CoperationU3E5__5_8() { return &___U3CoperationU3E5__5_8; }
	inline void set_U3CoperationU3E5__5_8(AsyncOperation_t3814632279 * value)
	{
		___U3CoperationU3E5__5_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CoperationU3E5__5_8, value);
	}

	inline static int32_t get_offset_of_U3CuploadCompletedU3E5__6_9() { return static_cast<int32_t>(offsetof(U3CInvokeRequestU3Ed__7_t3085820275, ___U3CuploadCompletedU3E5__6_9)); }
	inline bool get_U3CuploadCompletedU3E5__6_9() const { return ___U3CuploadCompletedU3E5__6_9; }
	inline bool* get_address_of_U3CuploadCompletedU3E5__6_9() { return &___U3CuploadCompletedU3E5__6_9; }
	inline void set_U3CuploadCompletedU3E5__6_9(bool value)
	{
		___U3CuploadCompletedU3E5__6_9 = value;
	}

	inline static int32_t get_offset_of_U3CunityWebRequestU3E5__7_10() { return static_cast<int32_t>(offsetof(U3CInvokeRequestU3Ed__7_t3085820275, ___U3CunityWebRequestU3E5__7_10)); }
	inline UnityWebRequestWrapper_t1542496577 * get_U3CunityWebRequestU3E5__7_10() const { return ___U3CunityWebRequestU3E5__7_10; }
	inline UnityWebRequestWrapper_t1542496577 ** get_address_of_U3CunityWebRequestU3E5__7_10() { return &___U3CunityWebRequestU3E5__7_10; }
	inline void set_U3CunityWebRequestU3E5__7_10(UnityWebRequestWrapper_t1542496577 * value)
	{
		___U3CunityWebRequestU3E5__7_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CunityWebRequestU3E5__7_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
