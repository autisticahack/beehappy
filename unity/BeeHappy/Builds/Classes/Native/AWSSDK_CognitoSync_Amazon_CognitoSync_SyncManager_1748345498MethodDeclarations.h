﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.CognitoSync.SyncManager.SyncFailureEventArgs
struct SyncFailureEventArgs_t1748345498;
// System.Exception
struct Exception_t1927440687;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1927440687.h"

// System.Exception Amazon.CognitoSync.SyncManager.SyncFailureEventArgs::get_Exception()
extern "C"  Exception_t1927440687 * SyncFailureEventArgs_get_Exception_m3183413697 (SyncFailureEventArgs_t1748345498 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.SyncManager.SyncFailureEventArgs::set_Exception(System.Exception)
extern "C"  void SyncFailureEventArgs_set_Exception_m4201609848 (SyncFailureEventArgs_t1748345498 * __this, Exception_t1927440687 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.SyncManager.SyncFailureEventArgs::.ctor(System.Exception)
extern "C"  void SyncFailureEventArgs__ctor_m968959894 (SyncFailureEventArgs_t1748345498 * __this, Exception_t1927440687 * ___exception0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
