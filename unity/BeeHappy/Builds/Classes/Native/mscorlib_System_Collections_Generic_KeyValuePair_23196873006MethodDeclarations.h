﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23196873006.h"
#include "AWSSDK_Core_Amazon_Runtime_Metric3273440202.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.Generic.KeyValuePair`2<Amazon.Runtime.Metric,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m595192313_gshared (KeyValuePair_2_t3196873006 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m595192313(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3196873006 *, int32_t, Il2CppObject *, const MethodInfo*))KeyValuePair_2__ctor_m595192313_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<Amazon.Runtime.Metric,System.Object>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m2312610863_gshared (KeyValuePair_2_t3196873006 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m2312610863(__this, method) ((  int32_t (*) (KeyValuePair_2_t3196873006 *, const MethodInfo*))KeyValuePair_2_get_Key_m2312610863_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Amazon.Runtime.Metric,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m547641270_gshared (KeyValuePair_2_t3196873006 * __this, int32_t ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m547641270(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3196873006 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m547641270_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<Amazon.Runtime.Metric,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m3734313679_gshared (KeyValuePair_2_t3196873006 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m3734313679(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t3196873006 *, const MethodInfo*))KeyValuePair_2_get_Value_m3734313679_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Amazon.Runtime.Metric,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1567047294_gshared (KeyValuePair_2_t3196873006 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m1567047294(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3196873006 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Value_m1567047294_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<Amazon.Runtime.Metric,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m3523821758_gshared (KeyValuePair_2_t3196873006 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m3523821758(__this, method) ((  String_t* (*) (KeyValuePair_2_t3196873006 *, const MethodInfo*))KeyValuePair_2_ToString_m3523821758_gshared)(__this, method)
