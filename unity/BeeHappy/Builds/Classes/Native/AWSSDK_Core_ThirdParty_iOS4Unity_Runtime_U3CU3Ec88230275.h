﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ThirdParty.iOS4Unity.Runtime/<>c
struct U3CU3Ec_t88230275;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.iOS4Unity.Runtime/<>c
struct  U3CU3Ec_t88230275  : public Il2CppObject
{
public:

public:
};

struct U3CU3Ec_t88230275_StaticFields
{
public:
	// ThirdParty.iOS4Unity.Runtime/<>c ThirdParty.iOS4Unity.Runtime/<>c::<>9
	U3CU3Ec_t88230275 * ___U3CU3E9_0;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t88230275_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t88230275 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t88230275 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t88230275 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
