﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.AmazonServiceException
struct AmazonServiceException_t3748559634;
// System.String
struct String_t;
// System.Exception
struct Exception_t1927440687;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Exception1927440687.h"
#include "AWSSDK_Core_Amazon_Runtime_ErrorType1448377524.h"
#include "System_System_Net_HttpStatusCode1898409641.h"
#include "mscorlib_System_Runtime_Serialization_Serialization228987430.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"

// System.Void Amazon.Runtime.AmazonServiceException::.ctor()
extern "C"  void AmazonServiceException__ctor_m2410644645 (AmazonServiceException_t3748559634 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.AmazonServiceException::.ctor(System.String)
extern "C"  void AmazonServiceException__ctor_m1286506575 (AmazonServiceException_t3748559634 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.AmazonServiceException::.ctor(System.String,System.Exception)
extern "C"  void AmazonServiceException__ctor_m971573063 (AmazonServiceException_t3748559634 * __this, String_t* ___message0, Exception_t1927440687 * ___innerException1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.AmazonServiceException::.ctor(System.Exception)
extern "C"  void AmazonServiceException__ctor_m470936405 (AmazonServiceException_t3748559634 * __this, Exception_t1927440687 * ___innerException0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.AmazonServiceException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
extern "C"  void AmazonServiceException__ctor_m1032639064 (AmazonServiceException_t3748559634 * __this, String_t* ___message0, Exception_t1927440687 * ___innerException1, int32_t ___errorType2, String_t* ___errorCode3, String_t* ___requestId4, int32_t ___statusCode5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Runtime.AmazonServiceException::BuildGenericErrorMessage(System.String,System.Net.HttpStatusCode)
extern "C"  String_t* AmazonServiceException_BuildGenericErrorMessage_m1298145741 (Il2CppObject * __this /* static, unused */, String_t* ___errorCode0, int32_t ___statusCode1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Runtime.AmazonServiceException::get_ErrorCode()
extern "C"  String_t* AmazonServiceException_get_ErrorCode_m1224619710 (AmazonServiceException_t3748559634 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Runtime.AmazonServiceException::get_RequestId()
extern "C"  String_t* AmazonServiceException_get_RequestId_m2499802897 (AmazonServiceException_t3748559634 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.AmazonServiceException::set_RequestId(System.String)
extern "C"  void AmazonServiceException_set_RequestId_m3608804166 (AmazonServiceException_t3748559634 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.HttpStatusCode Amazon.Runtime.AmazonServiceException::get_StatusCode()
extern "C"  int32_t AmazonServiceException_get_StatusCode_m590330469 (AmazonServiceException_t3748559634 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.AmazonServiceException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void AmazonServiceException__ctor_m903377152 (AmazonServiceException_t3748559634 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.AmazonServiceException::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void AmazonServiceException_GetObjectData_m3684451185 (AmazonServiceException_t3748559634 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
