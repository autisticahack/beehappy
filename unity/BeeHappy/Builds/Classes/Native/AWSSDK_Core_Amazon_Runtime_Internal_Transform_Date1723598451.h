﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.Runtime.Internal.Transform.DateTimeUnmarshaller
struct DateTimeUnmarshaller_t1723598451;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Transform.DateTimeUnmarshaller
struct  DateTimeUnmarshaller_t1723598451  : public Il2CppObject
{
public:

public:
};

struct DateTimeUnmarshaller_t1723598451_StaticFields
{
public:
	// Amazon.Runtime.Internal.Transform.DateTimeUnmarshaller Amazon.Runtime.Internal.Transform.DateTimeUnmarshaller::_instance
	DateTimeUnmarshaller_t1723598451 * ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(DateTimeUnmarshaller_t1723598451_StaticFields, ____instance_0)); }
	inline DateTimeUnmarshaller_t1723598451 * get__instance_0() const { return ____instance_0; }
	inline DateTimeUnmarshaller_t1723598451 ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(DateTimeUnmarshaller_t1723598451 * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier(&____instance_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
