﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3601534125MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Collections.Generic.Dictionary`2<System.String,Amazon.Runtime.ConstantClass>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m3516714071(__this, ___dictionary0, method) ((  void (*) (Enumerator_t582787155 *, Dictionary_2_t3557729749 *, const MethodInfo*))Enumerator__ctor_m3742107451_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Collections.Generic.Dictionary`2<System.String,Amazon.Runtime.ConstantClass>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2994670184(__this, method) ((  Il2CppObject * (*) (Enumerator_t582787155 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m229223308_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Collections.Generic.Dictionary`2<System.String,Amazon.Runtime.ConstantClass>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1739089388(__this, method) ((  void (*) (Enumerator_t582787155 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3225937576_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Collections.Generic.Dictionary`2<System.String,Amazon.Runtime.ConstantClass>>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1779265(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t582787155 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m221119093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Collections.Generic.Dictionary`2<System.String,Amazon.Runtime.ConstantClass>>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3784266070(__this, method) ((  Il2CppObject * (*) (Enumerator_t582787155 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m467957770_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Collections.Generic.Dictionary`2<System.String,Amazon.Runtime.ConstantClass>>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3504317140(__this, method) ((  Il2CppObject * (*) (Enumerator_t582787155 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2325383168_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Collections.Generic.Dictionary`2<System.String,Amazon.Runtime.ConstantClass>>::MoveNext()
#define Enumerator_MoveNext_m1493048676(__this, method) ((  bool (*) (Enumerator_t582787155 *, const MethodInfo*))Enumerator_MoveNext_m3349738440_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Collections.Generic.Dictionary`2<System.String,Amazon.Runtime.ConstantClass>>::get_Current()
#define Enumerator_get_Current_m2408851580(__this, method) ((  KeyValuePair_2_t1315074971  (*) (Enumerator_t582787155 *, const MethodInfo*))Enumerator_get_Current_m25299632_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Collections.Generic.Dictionary`2<System.String,Amazon.Runtime.ConstantClass>>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m4100402795(__this, method) ((  Type_t * (*) (Enumerator_t582787155 *, const MethodInfo*))Enumerator_get_CurrentKey_m3839846791_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Collections.Generic.Dictionary`2<System.String,Amazon.Runtime.ConstantClass>>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m379018379(__this, method) ((  Dictionary_2_t1620371852 * (*) (Enumerator_t582787155 *, const MethodInfo*))Enumerator_get_CurrentValue_m402763047_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Collections.Generic.Dictionary`2<System.String,Amazon.Runtime.ConstantClass>>::Reset()
#define Enumerator_Reset_m368039313(__this, method) ((  void (*) (Enumerator_t582787155 *, const MethodInfo*))Enumerator_Reset_m3129803197_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Collections.Generic.Dictionary`2<System.String,Amazon.Runtime.ConstantClass>>::VerifyState()
#define Enumerator_VerifyState_m3488020688(__this, method) ((  void (*) (Enumerator_t582787155 *, const MethodInfo*))Enumerator_VerifyState_m262343092_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Collections.Generic.Dictionary`2<System.String,Amazon.Runtime.ConstantClass>>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m2523144412(__this, method) ((  void (*) (Enumerator_t582787155 *, const MethodInfo*))Enumerator_VerifyCurrent_m1702320752_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Collections.Generic.Dictionary`2<System.String,Amazon.Runtime.ConstantClass>>::Dispose()
#define Enumerator_Dispose_m1979821563(__this, method) ((  void (*) (Enumerator_t582787155 *, const MethodInfo*))Enumerator_Dispose_m1905011127_gshared)(__this, method)
