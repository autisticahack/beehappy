﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.Util.HashStream
struct HashStream_t2318308192;
// Amazon.Runtime.Internal.Util.IHashingWrapper
struct IHashingWrapper_t2610776002;
// System.Byte[]
struct ByteU5BU5D_t3397334013;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_SeekOrigin2475945306.h"

// Amazon.Runtime.Internal.Util.IHashingWrapper Amazon.Runtime.Internal.Util.HashStream::get_Algorithm()
extern "C"  Il2CppObject * HashStream_get_Algorithm_m3399874980 (HashStream_t2318308192 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.HashStream::set_Algorithm(Amazon.Runtime.Internal.Util.IHashingWrapper)
extern "C"  void HashStream_set_Algorithm_m2695660979 (HashStream_t2318308192 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.Util.HashStream::get_FinishedHashing()
extern "C"  bool HashStream_get_FinishedHashing_m3075564029 (HashStream_t2318308192 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Amazon.Runtime.Internal.Util.HashStream::get_CurrentPosition()
extern "C"  int64_t HashStream_get_CurrentPosition_m2242651182 (HashStream_t2318308192 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.HashStream::set_CurrentPosition(System.Int64)
extern "C"  void HashStream_set_CurrentPosition_m1939911211 (HashStream_t2318308192 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Amazon.Runtime.Internal.Util.HashStream::get_CalculatedHash()
extern "C"  ByteU5BU5D_t3397334013* HashStream_get_CalculatedHash_m547255679 (HashStream_t2318308192 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.HashStream::set_CalculatedHash(System.Byte[])
extern "C"  void HashStream_set_CalculatedHash_m4119873842 (HashStream_t2318308192 * __this, ByteU5BU5D_t3397334013* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Amazon.Runtime.Internal.Util.HashStream::get_ExpectedHash()
extern "C"  ByteU5BU5D_t3397334013* HashStream_get_ExpectedHash_m4278445949 (HashStream_t2318308192 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Amazon.Runtime.Internal.Util.HashStream::get_ExpectedLength()
extern "C"  int64_t HashStream_get_ExpectedLength_m1633639326 (HashStream_t2318308192 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Amazon.Runtime.Internal.Util.HashStream::Read(System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t HashStream_Read_m2193101907 (HashStream_t2318308192 * __this, ByteU5BU5D_t3397334013* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.HashStream::Close()
extern "C"  void HashStream_Close_m3148455692 (HashStream_t2318308192 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.HashStream::Dispose(System.Boolean)
extern "C"  void HashStream_Dispose_m92967216 (HashStream_t2318308192 * __this, bool ___disposing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.Util.HashStream::get_CanSeek()
extern "C"  bool HashStream_get_CanSeek_m2179057753 (HashStream_t2318308192 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Amazon.Runtime.Internal.Util.HashStream::get_Position()
extern "C"  int64_t HashStream_get_Position_m3227481777 (HashStream_t2318308192 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.HashStream::set_Position(System.Int64)
extern "C"  void HashStream_set_Position_m2814670364 (HashStream_t2318308192 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Amazon.Runtime.Internal.Util.HashStream::Seek(System.Int64,System.IO.SeekOrigin)
extern "C"  int64_t HashStream_Seek_m1226570182 (HashStream_t2318308192 * __this, int64_t ___offset0, int32_t ___origin1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Amazon.Runtime.Internal.Util.HashStream::get_Length()
extern "C"  int64_t HashStream_get_Length_m3206449078 (HashStream_t2318308192 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.HashStream::CalculateHash()
extern "C"  void HashStream_CalculateHash_m595013268 (HashStream_t2318308192 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.HashStream::Reset()
extern "C"  void HashStream_Reset_m3410198249 (HashStream_t2318308192 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.Util.HashStream::CompareHashes(System.Byte[],System.Byte[])
extern "C"  bool HashStream_CompareHashes_m2250798627 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___expected0, ByteU5BU5D_t3397334013* ___actual1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
