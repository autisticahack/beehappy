﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.Util.ChunkedUploadWrapperStream
struct ChunkedUploadWrapperStream_t779979984;
// System.IO.Stream
struct Stream_t3255436806;
// Amazon.Runtime.Internal.Auth.AWS4SigningResult
struct AWS4SigningResult_t430803065;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_Stream3255436806.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Auth_AWS4Signin430803065.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Amazon.Runtime.Internal.Util.ChunkedUploadWrapperStream::.ctor(System.IO.Stream,System.Int32,Amazon.Runtime.Internal.Auth.AWS4SigningResult)
extern "C"  void ChunkedUploadWrapperStream__ctor_m1678276214 (ChunkedUploadWrapperStream_t779979984 * __this, Stream_t3255436806 * ___stream0, int32_t ___wrappedStreamBufferSize1, AWS4SigningResult_t430803065 * ___headerSigningResult2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Amazon.Runtime.Internal.Util.ChunkedUploadWrapperStream::Read(System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t ChunkedUploadWrapperStream_Read_m2452812107 (ChunkedUploadWrapperStream_t779979984 * __this, ByteU5BU5D_t3397334013* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.Internal.Auth.AWS4SigningResult Amazon.Runtime.Internal.Util.ChunkedUploadWrapperStream::get_HeaderSigningResult()
extern "C"  AWS4SigningResult_t430803065 * ChunkedUploadWrapperStream_get_HeaderSigningResult_m3122177057 (ChunkedUploadWrapperStream_t779979984 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.ChunkedUploadWrapperStream::set_HeaderSigningResult(Amazon.Runtime.Internal.Auth.AWS4SigningResult)
extern "C"  void ChunkedUploadWrapperStream_set_HeaderSigningResult_m2247365636 (ChunkedUploadWrapperStream_t779979984 * __this, AWS4SigningResult_t430803065 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Runtime.Internal.Util.ChunkedUploadWrapperStream::get_PreviousChunkSignature()
extern "C"  String_t* ChunkedUploadWrapperStream_get_PreviousChunkSignature_m3146593668 (ChunkedUploadWrapperStream_t779979984 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.ChunkedUploadWrapperStream::set_PreviousChunkSignature(System.String)
extern "C"  void ChunkedUploadWrapperStream_set_PreviousChunkSignature_m4279694933 (ChunkedUploadWrapperStream_t779979984 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.ChunkedUploadWrapperStream::ConstructOutputBufferChunk(System.Int32)
extern "C"  void ChunkedUploadWrapperStream_ConstructOutputBufferChunk_m2570058724 (ChunkedUploadWrapperStream_t779979984 * __this, int32_t ___dataLen0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Amazon.Runtime.Internal.Util.ChunkedUploadWrapperStream::get_Length()
extern "C"  int64_t ChunkedUploadWrapperStream_get_Length_m3000202290 (ChunkedUploadWrapperStream_t779979984 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.Util.ChunkedUploadWrapperStream::get_CanSeek()
extern "C"  bool ChunkedUploadWrapperStream_get_CanSeek_m1506207013 (ChunkedUploadWrapperStream_t779979984 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Amazon.Runtime.Internal.Util.ChunkedUploadWrapperStream::ComputeChunkedContentLength(System.Int64)
extern "C"  int64_t ChunkedUploadWrapperStream_ComputeChunkedContentLength_m1591879545 (Il2CppObject * __this /* static, unused */, int64_t ___originalLength0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Amazon.Runtime.Internal.Util.ChunkedUploadWrapperStream::CalculateChunkHeaderLength(System.Int64)
extern "C"  int64_t ChunkedUploadWrapperStream_CalculateChunkHeaderLength_m3604661347 (Il2CppObject * __this /* static, unused */, int64_t ___chunkDataSize0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Amazon.Runtime.Internal.Util.ChunkedUploadWrapperStream::FillInputBuffer()
extern "C"  int32_t ChunkedUploadWrapperStream_FillInputBuffer_m269543763 (ChunkedUploadWrapperStream_t779979984 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.ChunkedUploadWrapperStream::.cctor()
extern "C"  void ChunkedUploadWrapperStream__cctor_m2450494803 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
