﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<System.Diagnostics.TraceListener>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m2974431817(__this, ___l0, method) ((  void (*) (Enumerator_t2318800085 *, List_1_t2784070411 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Diagnostics.TraceListener>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m361503801(__this, method) ((  void (*) (Enumerator_t2318800085 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Diagnostics.TraceListener>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1963389637(__this, method) ((  Il2CppObject * (*) (Enumerator_t2318800085 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Diagnostics.TraceListener>::Dispose()
#define Enumerator_Dispose_m936614864(__this, method) ((  void (*) (Enumerator_t2318800085 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Diagnostics.TraceListener>::VerifyState()
#define Enumerator_VerifyState_m3747639231(__this, method) ((  void (*) (Enumerator_t2318800085 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Diagnostics.TraceListener>::MoveNext()
#define Enumerator_MoveNext_m2371229001(__this, method) ((  bool (*) (Enumerator_t2318800085 *, const MethodInfo*))Enumerator_MoveNext_m984484162_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Diagnostics.TraceListener>::get_Current()
#define Enumerator_get_Current_m3522897984(__this, method) ((  TraceListener_t3414949279 * (*) (Enumerator_t2318800085 *, const MethodInfo*))Enumerator_get_Current_m2193722336_gshared)(__this, method)
