﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.CognitoIdentity.CognitoAWSCredentials/IdentityState
struct IdentityState_t2011390993;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.String Amazon.CognitoIdentity.CognitoAWSCredentials/IdentityState::get_IdentityId()
extern "C"  String_t* IdentityState_get_IdentityId_m3454520601 (IdentityState_t2011390993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoIdentity.CognitoAWSCredentials/IdentityState::set_IdentityId(System.String)
extern "C"  void IdentityState_set_IdentityId_m748250444 (IdentityState_t2011390993 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.CognitoIdentity.CognitoAWSCredentials/IdentityState::get_LoginProvider()
extern "C"  String_t* IdentityState_get_LoginProvider_m3629843770 (IdentityState_t2011390993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.CognitoIdentity.CognitoAWSCredentials/IdentityState::get_LoginToken()
extern "C"  String_t* IdentityState_get_LoginToken_m1363466894 (IdentityState_t2011390993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoIdentity.CognitoAWSCredentials/IdentityState::set_FromCache(System.Boolean)
extern "C"  void IdentityState_set_FromCache_m3620589710 (IdentityState_t2011390993 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoIdentity.CognitoAWSCredentials/IdentityState::.ctor(System.String,System.Boolean)
extern "C"  void IdentityState__ctor_m2951748439 (IdentityState_t2011390993 * __this, String_t* ___identityId0, bool ___fromCache1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.CognitoIdentity.CognitoAWSCredentials/IdentityState::get_LoginSpecified()
extern "C"  bool IdentityState_get_LoginSpecified_m1764907814 (IdentityState_t2011390993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
