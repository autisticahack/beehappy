﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ThirdParty.iOS4Unity.SKPayment
struct SKPayment_t4242751664;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr2504060609.h"

// System.Void ThirdParty.iOS4Unity.SKPayment::.cctor()
extern "C"  void SKPayment__cctor_m856996525 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.iOS4Unity.SKPayment::.ctor(System.IntPtr)
extern "C"  void SKPayment__ctor_m3086273736 (SKPayment_t4242751664 * __this, IntPtr_t ___handle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
