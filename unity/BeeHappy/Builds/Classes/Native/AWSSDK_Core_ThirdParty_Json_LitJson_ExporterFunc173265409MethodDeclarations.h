﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ThirdParty.Json.LitJson.ExporterFunc
struct ExporterFunc_t173265409;
// System.Object
struct Il2CppObject;
// ThirdParty.Json.LitJson.JsonWriter
struct JsonWriter_t3014444111;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_JsonWriter3014444111.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void ThirdParty.Json.LitJson.ExporterFunc::.ctor(System.Object,System.IntPtr)
extern "C"  void ExporterFunc__ctor_m1402811244 (ExporterFunc_t173265409 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.ExporterFunc::Invoke(System.Object,ThirdParty.Json.LitJson.JsonWriter)
extern "C"  void ExporterFunc_Invoke_m1119516307 (ExporterFunc_t173265409 * __this, Il2CppObject * ___obj0, JsonWriter_t3014444111 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult ThirdParty.Json.LitJson.ExporterFunc::BeginInvoke(System.Object,ThirdParty.Json.LitJson.JsonWriter,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ExporterFunc_BeginInvoke_m149663916 (ExporterFunc_t173265409 * __this, Il2CppObject * ___obj0, JsonWriter_t3014444111 * ___writer1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.ExporterFunc::EndInvoke(System.IAsyncResult)
extern "C"  void ExporterFunc_EndInvoke_m2740655690 (ExporterFunc_t173265409 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
