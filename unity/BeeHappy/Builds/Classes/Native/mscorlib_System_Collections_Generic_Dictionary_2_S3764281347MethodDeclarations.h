﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<Amazon.Runtime.Metric,System.Int64>
struct ShimEnumerator_t3764281347;
// System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Int64>
struct Dictionary_2_t3659156526;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<Amazon.Runtime.Metric,System.Int64>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m2883604990_gshared (ShimEnumerator_t3764281347 * __this, Dictionary_2_t3659156526 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m2883604990(__this, ___host0, method) ((  void (*) (ShimEnumerator_t3764281347 *, Dictionary_2_t3659156526 *, const MethodInfo*))ShimEnumerator__ctor_m2883604990_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<Amazon.Runtime.Metric,System.Int64>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m762667293_gshared (ShimEnumerator_t3764281347 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m762667293(__this, method) ((  bool (*) (ShimEnumerator_t3764281347 *, const MethodInfo*))ShimEnumerator_MoveNext_m762667293_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<Amazon.Runtime.Metric,System.Int64>::get_Entry()
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m231825841_gshared (ShimEnumerator_t3764281347 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m231825841(__this, method) ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t3764281347 *, const MethodInfo*))ShimEnumerator_get_Entry_m231825841_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Amazon.Runtime.Metric,System.Int64>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m3697060374_gshared (ShimEnumerator_t3764281347 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m3697060374(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t3764281347 *, const MethodInfo*))ShimEnumerator_get_Key_m3697060374_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Amazon.Runtime.Metric,System.Int64>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m1899706092_gshared (ShimEnumerator_t3764281347 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m1899706092(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t3764281347 *, const MethodInfo*))ShimEnumerator_get_Value_m1899706092_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Amazon.Runtime.Metric,System.Int64>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m2901726476_gshared (ShimEnumerator_t3764281347 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m2901726476(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t3764281347 *, const MethodInfo*))ShimEnumerator_get_Current_m2901726476_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<Amazon.Runtime.Metric,System.Int64>::Reset()
extern "C"  void ShimEnumerator_Reset_m2790213288_gshared (ShimEnumerator_t3764281347 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m2790213288(__this, method) ((  void (*) (ShimEnumerator_t3764281347 *, const MethodInfo*))ShimEnumerator_Reset_m2790213288_gshared)(__this, method)
