﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>
struct Collection_1_t319514429;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// Mono.Data.Sqlite.SqliteKeyReader/KeyInfo[]
struct KeyInfoU5BU5D_t3526347178;
// System.Collections.Generic.IEnumerator`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>
struct IEnumerator_1_t2548260798;
// System.Collections.Generic.IList`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>
struct IList_1_t1318710276;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteKeyReader_K777769675.h"

// System.Void System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::.ctor()
extern "C"  void Collection_1__ctor_m1359382362_gshared (Collection_1_t319514429 * __this, const MethodInfo* method);
#define Collection_1__ctor_m1359382362(__this, method) ((  void (*) (Collection_1_t319514429 *, const MethodInfo*))Collection_1__ctor_m1359382362_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m823070109_gshared (Collection_1_t319514429 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m823070109(__this, method) ((  bool (*) (Collection_1_t319514429 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m823070109_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m3785648642_gshared (Collection_1_t319514429 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m3785648642(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t319514429 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m3785648642_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m1631456845_gshared (Collection_1_t319514429 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m1631456845(__this, method) ((  Il2CppObject * (*) (Collection_1_t319514429 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m1631456845_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m516424150_gshared (Collection_1_t319514429 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m516424150(__this, ___value0, method) ((  int32_t (*) (Collection_1_t319514429 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m516424150_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m4111963052_gshared (Collection_1_t319514429 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m4111963052(__this, ___value0, method) ((  bool (*) (Collection_1_t319514429 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m4111963052_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m3916359588_gshared (Collection_1_t319514429 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m3916359588(__this, ___value0, method) ((  int32_t (*) (Collection_1_t319514429 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m3916359588_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m20629033_gshared (Collection_1_t319514429 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m20629033(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t319514429 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m20629033_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m2493772073_gshared (Collection_1_t319514429 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m2493772073(__this, ___value0, method) ((  void (*) (Collection_1_t319514429 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m2493772073_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m229595970_gshared (Collection_1_t319514429 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m229595970(__this, method) ((  bool (*) (Collection_1_t319514429 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m229595970_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m1519672908_gshared (Collection_1_t319514429 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m1519672908(__this, method) ((  Il2CppObject * (*) (Collection_1_t319514429 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m1519672908_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m4116547773_gshared (Collection_1_t319514429 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m4116547773(__this, method) ((  bool (*) (Collection_1_t319514429 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m4116547773_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m2506478294_gshared (Collection_1_t319514429 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m2506478294(__this, method) ((  bool (*) (Collection_1_t319514429 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m2506478294_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m328437705_gshared (Collection_1_t319514429 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m328437705(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t319514429 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m328437705_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m3874916888_gshared (Collection_1_t319514429 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m3874916888(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t319514429 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m3874916888_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::Add(T)
extern "C"  void Collection_1_Add_m3586133349_gshared (Collection_1_t319514429 * __this, KeyInfo_t777769675  ___item0, const MethodInfo* method);
#define Collection_1_Add_m3586133349(__this, ___item0, method) ((  void (*) (Collection_1_t319514429 *, KeyInfo_t777769675 , const MethodInfo*))Collection_1_Add_m3586133349_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::Clear()
extern "C"  void Collection_1_Clear_m1444376281_gshared (Collection_1_t319514429 * __this, const MethodInfo* method);
#define Collection_1_Clear_m1444376281(__this, method) ((  void (*) (Collection_1_t319514429 *, const MethodInfo*))Collection_1_Clear_m1444376281_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::ClearItems()
extern "C"  void Collection_1_ClearItems_m4077669243_gshared (Collection_1_t319514429 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m4077669243(__this, method) ((  void (*) (Collection_1_t319514429 *, const MethodInfo*))Collection_1_ClearItems_m4077669243_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::Contains(T)
extern "C"  bool Collection_1_Contains_m1918863063_gshared (Collection_1_t319514429 * __this, KeyInfo_t777769675  ___item0, const MethodInfo* method);
#define Collection_1_Contains_m1918863063(__this, ___item0, method) ((  bool (*) (Collection_1_t319514429 *, KeyInfo_t777769675 , const MethodInfo*))Collection_1_Contains_m1918863063_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m978152205_gshared (Collection_1_t319514429 * __this, KeyInfoU5BU5D_t3526347178* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m978152205(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t319514429 *, KeyInfoU5BU5D_t3526347178*, int32_t, const MethodInfo*))Collection_1_CopyTo_m978152205_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m756731074_gshared (Collection_1_t319514429 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m756731074(__this, method) ((  Il2CppObject* (*) (Collection_1_t319514429 *, const MethodInfo*))Collection_1_GetEnumerator_m756731074_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m608472621_gshared (Collection_1_t319514429 * __this, KeyInfo_t777769675  ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m608472621(__this, ___item0, method) ((  int32_t (*) (Collection_1_t319514429 *, KeyInfo_t777769675 , const MethodInfo*))Collection_1_IndexOf_m608472621_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m850988216_gshared (Collection_1_t319514429 * __this, int32_t ___index0, KeyInfo_t777769675  ___item1, const MethodInfo* method);
#define Collection_1_Insert_m850988216(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t319514429 *, int32_t, KeyInfo_t777769675 , const MethodInfo*))Collection_1_Insert_m850988216_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m2179219823_gshared (Collection_1_t319514429 * __this, int32_t ___index0, KeyInfo_t777769675  ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m2179219823(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t319514429 *, int32_t, KeyInfo_t777769675 , const MethodInfo*))Collection_1_InsertItem_m2179219823_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::Remove(T)
extern "C"  bool Collection_1_Remove_m969621806_gshared (Collection_1_t319514429 * __this, KeyInfo_t777769675  ___item0, const MethodInfo* method);
#define Collection_1_Remove_m969621806(__this, ___item0, method) ((  bool (*) (Collection_1_t319514429 *, KeyInfo_t777769675 , const MethodInfo*))Collection_1_Remove_m969621806_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m59891244_gshared (Collection_1_t319514429 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m59891244(__this, ___index0, method) ((  void (*) (Collection_1_t319514429 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m59891244_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m2373120534_gshared (Collection_1_t319514429 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m2373120534(__this, ___index0, method) ((  void (*) (Collection_1_t319514429 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m2373120534_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m881931654_gshared (Collection_1_t319514429 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m881931654(__this, method) ((  int32_t (*) (Collection_1_t319514429 *, const MethodInfo*))Collection_1_get_Count_m881931654_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::get_Item(System.Int32)
extern "C"  KeyInfo_t777769675  Collection_1_get_Item_m819305944_gshared (Collection_1_t319514429 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m819305944(__this, ___index0, method) ((  KeyInfo_t777769675  (*) (Collection_1_t319514429 *, int32_t, const MethodInfo*))Collection_1_get_Item_m819305944_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m311017949_gshared (Collection_1_t319514429 * __this, int32_t ___index0, KeyInfo_t777769675  ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m311017949(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t319514429 *, int32_t, KeyInfo_t777769675 , const MethodInfo*))Collection_1_set_Item_m311017949_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m4116118296_gshared (Collection_1_t319514429 * __this, int32_t ___index0, KeyInfo_t777769675  ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m4116118296(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t319514429 *, int32_t, KeyInfo_t777769675 , const MethodInfo*))Collection_1_SetItem_m4116118296_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m802338811_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m802338811(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m802338811_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::ConvertItem(System.Object)
extern "C"  KeyInfo_t777769675  Collection_1_ConvertItem_m3452082663_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m3452082663(__this /* static, unused */, ___item0, method) ((  KeyInfo_t777769675  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m3452082663_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m3705482039_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m3705482039(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m3705482039_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m3709101511_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m3709101511(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m3709101511_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m3637636804_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m3637636804(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m3637636804_gshared)(__this /* static, unused */, ___list0, method)
