﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_Security_LocalCertificateSelecti3696771181.h"
#include "System_System_Net_Security_RemoteCertificateValida2756269959.h"
#include "System_System_Net_BindIPEndPoint635820671.h"
#include "System_System_Net_HttpContinueDelegate2713047268.h"
#include "System_System_Text_RegularExpressions_MatchEvaluato710107290.h"
#include "System_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "System_U3CPrivateImplementationDetailsU3E_U24Array1703410334.h"
#include "System_U3CPrivateImplementationDetailsU3E_U24ArrayT116038554.h"
#include "System_U3CPrivateImplementationDetailsU3E_U24Array3672778802.h"
#include "System_Core_U3CModuleU3E3783534214.h"
#include "System_Core_System_Runtime_CompilerServices_Extens1840441203.h"
#include "System_Core_Locale4255929014.h"
#include "System_Core_System_MonoTODOAttribute3487514019.h"
#include "System_Core_Mono_Security_Cryptography_KeyBuilder3965881084.h"
#include "System_Core_Mono_Security_Cryptography_SymmetricTr1394030013.h"
#include "System_Core_System_Linq_Check578192424.h"
#include "System_Core_System_Linq_Enumerable2148412300.h"
#include "System_Core_System_Linq_Enumerable_Fallback2408918324.h"
#include "System_Core_System_Linq_SortDirection759359329.h"
#include "System_Core_System_Security_Cryptography_Aes2354947465.h"
#include "System_Core_System_Security_Cryptography_AesManage3721278648.h"
#include "System_Core_System_Security_Cryptography_AesTransf3733702461.h"
#include "System_Core_System_Threading_LockRecursionExceptio2514728202.h"
#include "System_Core_System_Threading_ReaderWriterLockSlim3961242320.h"
#include "System_Core_System_Threading_ReaderWriterLockSlim_2550015005.h"
#include "System_Core_System_Action3226471752.h"
#include "System_Core_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "System_Core_U3CPrivateImplementationDetailsU3E_U242844921915.h"
#include "System_Core_U3CPrivateImplementationDetailsU3E_U24A116038562.h"
#include "System_Core_U3CPrivateImplementationDetailsU3E_U242038352954.h"
#include "System_Core_U3CPrivateImplementationDetailsU3E_U242672183894.h"
#include "System_Xml_U3CModuleU3E3783534214.h"
#include "System_Xml_System_MonoTODOAttribute3487514019.h"
#include "System_Xml_Mono_Xml_Schema_XmlSchemaValidatingRead3861297856.h"
#include "System_Xml_Mono_Xml_Schema_XmlSchemaValidatingRead2009608413.h"
#include "System_Xml_Mono_Xml_Schema_XsdIdentitySelector185499482.h"
#include "System_Xml_Mono_Xml_Schema_XsdIdentityField2563516441.h"
#include "System_Xml_Mono_Xml_Schema_XsdIdentityPath2037874.h"
#include "System_Xml_Mono_Xml_Schema_XsdIdentityStep452377251.h"
#include "System_Xml_Mono_Xml_Schema_XsdKeyEntryField3632833996.h"
#include "System_Xml_Mono_Xml_Schema_XsdKeyEntryFieldCollect2946985218.h"
#include "System_Xml_Mono_Xml_Schema_XsdKeyEntry3532015222.h"
#include "System_Xml_Mono_Xml_Schema_XsdKeyEntryCollection1714053544.h"
#include "System_Xml_Mono_Xml_Schema_XsdKeyTable2980902100.h"
#include "System_Xml_Mono_Xml_Schema_XsdParticleStateManager4119977941.h"
#include "System_Xml_Mono_Xml_Schema_XsdValidationState3545965403.h"
#include "System_Xml_Mono_Xml_Schema_XsdElementValidationStat152111323.h"
#include "System_Xml_Mono_Xml_Schema_XsdSequenceValidationSt3542030006.h"
#include "System_Xml_Mono_Xml_Schema_XsdChoiceValidationStat1506407122.h"
#include "System_Xml_Mono_Xml_Schema_XsdAllValidationState1028212608.h"
#include "System_Xml_Mono_Xml_Schema_XsdAnyValidationState204702461.h"
#include "System_Xml_Mono_Xml_Schema_XsdAppendedValidationSt3724408090.h"
#include "System_Xml_Mono_Xml_Schema_XsdEmptyValidationState1914323912.h"
#include "System_Xml_Mono_Xml_Schema_XsdInvalidValidationSta1112885736.h"
#include "System_Xml_Mono_Xml_Schema_XsdValidatingReader1704923617.h"
#include "System_Xml_Mono_Xml_Schema_XsdValidationContext3720679969.h"
#include "System_Xml_Mono_Xml_Schema_XsdIDManager3860002335.h"
#include "System_Xml_Mono_Xml_Schema_XsdWildcard625524157.h"
#include "System_Xml_Mono_Xml_XmlFilterReader256953178.h"
#include "System_Xml_Mono_Xml_Schema_XsdWhitespaceFacet2758097827.h"
#include "System_Xml_Mono_Xml_Schema_XsdOrdering3741887935.h"
#include "System_Xml_Mono_Xml_Schema_XsdAnySimpleType1096449895.h"
#include "System_Xml_Mono_Xml_Schema_XdtAnyAtomicType3359210273.h"
#include "System_Xml_Mono_Xml_Schema_XdtUntypedAtomic2904699188.h"
#include "System_Xml_Mono_Xml_Schema_XsdString263933896.h"
#include "System_Xml_Mono_Xml_Schema_XsdNormalizedString2132216291.h"
#include "System_Xml_Mono_Xml_Schema_XsdToken2902215462.h"
#include "System_Xml_Mono_Xml_Schema_XsdLanguage3897851897.h"
#include "System_Xml_Mono_Xml_Schema_XsdNMToken547135877.h"
#include "System_Xml_Mono_Xml_Schema_XsdNMTokens1753890866.h"
#include "System_Xml_Mono_Xml_Schema_XsdName1409945702.h"
#include "System_Xml_Mono_Xml_Schema_XsdNCName414304927.h"
#include "System_Xml_Mono_Xml_Schema_XsdID1067193160.h"
#include "System_Xml_Mono_Xml_Schema_XsdIDRef1377311049.h"
#include "System_Xml_Mono_Xml_Schema_XsdIDRefs763119546.h"
#include "System_Xml_Mono_Xml_Schema_XsdEntity2664305022.h"
#include "System_Xml_Mono_Xml_Schema_XsdEntities1103053540.h"
#include "System_Xml_Mono_Xml_Schema_XsdNotation720379093.h"
#include "System_Xml_Mono_Xml_Schema_XsdDecimal2932251550.h"
#include "System_Xml_Mono_Xml_Schema_XsdInteger1330502157.h"
#include "System_Xml_Mono_Xml_Schema_XsdLong4179235589.h"
#include "System_Xml_Mono_Xml_Schema_XsdInt1488443894.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1400 = { sizeof (LocalCertificateSelectionCallback_t3696771181), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1401 = { sizeof (RemoteCertificateValidationCallback_t2756269959), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1402 = { sizeof (BindIPEndPoint_t635820671), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1403 = { sizeof (HttpContinueDelegate_t2713047268), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1404 = { sizeof (MatchEvaluator_t710107290), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1405 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305139), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1405[4] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24U24fieldU2D1_0(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24U24fieldU2D2_1(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24U24fieldU2D3_2(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24U24fieldU2D4_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1406 = { sizeof (U24ArrayTypeU2416_t1703410336)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2416_t1703410336 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1407 = { sizeof (U24ArrayTypeU24128_t116038555)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24128_t116038555 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1408 = { sizeof (U24ArrayTypeU2412_t3672778806)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2412_t3672778806 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1409 = { sizeof (U3CModuleU3E_t3783534217), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1410 = { sizeof (ExtensionAttribute_t1840441203), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1411 = { sizeof (Locale_t4255929017), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1412 = { sizeof (MonoTODOAttribute_t3487514021), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1413 = { sizeof (KeyBuilder_t3965881086), -1, sizeof(KeyBuilder_t3965881086_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1413[1] = 
{
	KeyBuilder_t3965881086_StaticFields::get_offset_of_rng_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1414 = { sizeof (SymmetricTransform_t1394030014), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1414[12] = 
{
	SymmetricTransform_t1394030014::get_offset_of_algo_0(),
	SymmetricTransform_t1394030014::get_offset_of_encrypt_1(),
	SymmetricTransform_t1394030014::get_offset_of_BlockSizeByte_2(),
	SymmetricTransform_t1394030014::get_offset_of_temp_3(),
	SymmetricTransform_t1394030014::get_offset_of_temp2_4(),
	SymmetricTransform_t1394030014::get_offset_of_workBuff_5(),
	SymmetricTransform_t1394030014::get_offset_of_workout_6(),
	SymmetricTransform_t1394030014::get_offset_of_FeedBackByte_7(),
	SymmetricTransform_t1394030014::get_offset_of_FeedBackIter_8(),
	SymmetricTransform_t1394030014::get_offset_of_m_disposed_9(),
	SymmetricTransform_t1394030014::get_offset_of_lastBlock_10(),
	SymmetricTransform_t1394030014::get_offset_of__rng_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1415 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1415[14] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1416 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1416[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1417 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1417[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1418 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1418[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1419 = { sizeof (Check_t578192424), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1420 = { sizeof (Enumerable_t2148412300), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1421 = { sizeof (Fallback_t2408918324)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1421[3] = 
{
	Fallback_t2408918324::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1422 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1422[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1423 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1423[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1424 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1425 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1425[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1426 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1426[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1427 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1427[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1428 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1428[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1429 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1429[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1430 = { sizeof (SortDirection_t759359329)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1430[3] = 
{
	SortDirection_t759359329::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1431 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1431[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1432 = { sizeof (Aes_t2354947465), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1433 = { sizeof (AesManaged_t3721278648), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1434 = { sizeof (AesTransform_t3733702461), -1, sizeof(AesTransform_t3733702461_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1434[14] = 
{
	AesTransform_t3733702461::get_offset_of_expandedKey_12(),
	AesTransform_t3733702461::get_offset_of_Nk_13(),
	AesTransform_t3733702461::get_offset_of_Nr_14(),
	AesTransform_t3733702461_StaticFields::get_offset_of_Rcon_15(),
	AesTransform_t3733702461_StaticFields::get_offset_of_SBox_16(),
	AesTransform_t3733702461_StaticFields::get_offset_of_iSBox_17(),
	AesTransform_t3733702461_StaticFields::get_offset_of_T0_18(),
	AesTransform_t3733702461_StaticFields::get_offset_of_T1_19(),
	AesTransform_t3733702461_StaticFields::get_offset_of_T2_20(),
	AesTransform_t3733702461_StaticFields::get_offset_of_T3_21(),
	AesTransform_t3733702461_StaticFields::get_offset_of_iT0_22(),
	AesTransform_t3733702461_StaticFields::get_offset_of_iT1_23(),
	AesTransform_t3733702461_StaticFields::get_offset_of_iT2_24(),
	AesTransform_t3733702461_StaticFields::get_offset_of_iT3_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1435 = { sizeof (LockRecursionException_t2514728202), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1436 = { sizeof (ReaderWriterLockSlim_t3961242320), -1, sizeof(ReaderWriterLockSlim_t3961242320_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1436[12] = 
{
	ReaderWriterLockSlim_t3961242320_StaticFields::get_offset_of_smp_0(),
	ReaderWriterLockSlim_t3961242320::get_offset_of_myLock_1(),
	ReaderWriterLockSlim_t3961242320::get_offset_of_owners_2(),
	ReaderWriterLockSlim_t3961242320::get_offset_of_upgradable_thread_3(),
	ReaderWriterLockSlim_t3961242320::get_offset_of_write_thread_4(),
	ReaderWriterLockSlim_t3961242320::get_offset_of_numWriteWaiters_5(),
	ReaderWriterLockSlim_t3961242320::get_offset_of_numReadWaiters_6(),
	ReaderWriterLockSlim_t3961242320::get_offset_of_numUpgradeWaiters_7(),
	ReaderWriterLockSlim_t3961242320::get_offset_of_writeEvent_8(),
	ReaderWriterLockSlim_t3961242320::get_offset_of_readEvent_9(),
	ReaderWriterLockSlim_t3961242320::get_offset_of_upgradeEvent_10(),
	ReaderWriterLockSlim_t3961242320::get_offset_of_read_locks_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1437 = { sizeof (LockDetails_t2550015005), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1437[2] = 
{
	LockDetails_t2550015005::get_offset_of_ThreadId_0(),
	LockDetails_t2550015005::get_offset_of_ReadLocks_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1438 = { sizeof (Action_t3226471752), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1439 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1440 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1441 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1442 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305140), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1442[12] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24U24fieldU2D0_0(),
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24U24fieldU2D1_1(),
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24U24fieldU2D2_2(),
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24U24fieldU2D3_3(),
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24U24fieldU2D4_4(),
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24U24fieldU2D5_5(),
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24U24fieldU2D6_6(),
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24U24fieldU2D7_7(),
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24U24fieldU2D8_8(),
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24U24fieldU2D9_9(),
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24U24fieldU2D10_10(),
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24U24fieldU2D11_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1443 = { sizeof (U24ArrayTypeU24136_t2844921916)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24136_t2844921916 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1444 = { sizeof (U24ArrayTypeU24120_t116038563)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24120_t116038563 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1445 = { sizeof (U24ArrayTypeU24256_t2038352956)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24256_t2038352956 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1446 = { sizeof (U24ArrayTypeU241024_t2672183895)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU241024_t2672183895 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1447 = { sizeof (U3CModuleU3E_t3783534218), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1448 = { sizeof (MonoTODOAttribute_t3487514022), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1449 = { sizeof (XmlSchemaValidatingReader_t3861297856), -1, sizeof(XmlSchemaValidatingReader_t3861297856_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1449[17] = 
{
	XmlSchemaValidatingReader_t3861297856_StaticFields::get_offset_of_emptyAttributeArray_3(),
	XmlSchemaValidatingReader_t3861297856::get_offset_of_reader_4(),
	XmlSchemaValidatingReader_t3861297856::get_offset_of_options_5(),
	XmlSchemaValidatingReader_t3861297856::get_offset_of_v_6(),
	XmlSchemaValidatingReader_t3861297856::get_offset_of_getter_7(),
	XmlSchemaValidatingReader_t3861297856::get_offset_of_xsinfo_8(),
	XmlSchemaValidatingReader_t3861297856::get_offset_of_readerLineInfo_9(),
	XmlSchemaValidatingReader_t3861297856::get_offset_of_nsResolver_10(),
	XmlSchemaValidatingReader_t3861297856::get_offset_of_defaultAttributes_11(),
	XmlSchemaValidatingReader_t3861297856::get_offset_of_currentDefaultAttribute_12(),
	XmlSchemaValidatingReader_t3861297856::get_offset_of_defaultAttributesCache_13(),
	XmlSchemaValidatingReader_t3861297856::get_offset_of_defaultAttributeConsumed_14(),
	XmlSchemaValidatingReader_t3861297856::get_offset_of_currentAttrType_15(),
	XmlSchemaValidatingReader_t3861297856::get_offset_of_validationDone_16(),
	XmlSchemaValidatingReader_t3861297856::get_offset_of_element_17(),
	XmlSchemaValidatingReader_t3861297856_StaticFields::get_offset_of_U3CU3Ef__switchU24map0_18(),
	XmlSchemaValidatingReader_t3861297856_StaticFields::get_offset_of_U3CU3Ef__switchU24map1_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1450 = { sizeof (U3CXmlSchemaValidatingReaderU3Ec__AnonStorey4_t2009608413), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1450[2] = 
{
	U3CXmlSchemaValidatingReaderU3Ec__AnonStorey4_t2009608413::get_offset_of_settings_0(),
	U3CXmlSchemaValidatingReaderU3Ec__AnonStorey4_t2009608413::get_offset_of_U3CU3Ef__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1451 = { sizeof (XsdIdentitySelector_t185499482), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1451[3] = 
{
	XsdIdentitySelector_t185499482::get_offset_of_selectorPaths_0(),
	XsdIdentitySelector_t185499482::get_offset_of_fields_1(),
	XsdIdentitySelector_t185499482::get_offset_of_cachedFields_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1452 = { sizeof (XsdIdentityField_t2563516441), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1452[2] = 
{
	XsdIdentityField_t2563516441::get_offset_of_fieldPaths_0(),
	XsdIdentityField_t2563516441::get_offset_of_index_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1453 = { sizeof (XsdIdentityPath_t2037874), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1453[2] = 
{
	XsdIdentityPath_t2037874::get_offset_of_OrderedSteps_0(),
	XsdIdentityPath_t2037874::get_offset_of_Descendants_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1454 = { sizeof (XsdIdentityStep_t452377251), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1454[6] = 
{
	XsdIdentityStep_t452377251::get_offset_of_IsCurrent_0(),
	XsdIdentityStep_t452377251::get_offset_of_IsAttribute_1(),
	XsdIdentityStep_t452377251::get_offset_of_IsAnyName_2(),
	XsdIdentityStep_t452377251::get_offset_of_NsName_3(),
	XsdIdentityStep_t452377251::get_offset_of_Name_4(),
	XsdIdentityStep_t452377251::get_offset_of_Namespace_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1455 = { sizeof (XsdKeyEntryField_t3632833996), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1455[13] = 
{
	XsdKeyEntryField_t3632833996::get_offset_of_entry_0(),
	XsdKeyEntryField_t3632833996::get_offset_of_field_1(),
	XsdKeyEntryField_t3632833996::get_offset_of_FieldFound_2(),
	XsdKeyEntryField_t3632833996::get_offset_of_FieldLineNumber_3(),
	XsdKeyEntryField_t3632833996::get_offset_of_FieldLinePosition_4(),
	XsdKeyEntryField_t3632833996::get_offset_of_FieldHasLineInfo_5(),
	XsdKeyEntryField_t3632833996::get_offset_of_FieldType_6(),
	XsdKeyEntryField_t3632833996::get_offset_of_Identity_7(),
	XsdKeyEntryField_t3632833996::get_offset_of_IsXsiNil_8(),
	XsdKeyEntryField_t3632833996::get_offset_of_FieldFoundDepth_9(),
	XsdKeyEntryField_t3632833996::get_offset_of_FieldFoundPath_10(),
	XsdKeyEntryField_t3632833996::get_offset_of_Consuming_11(),
	XsdKeyEntryField_t3632833996::get_offset_of_Consumed_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1456 = { sizeof (XsdKeyEntryFieldCollection_t2946985218), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1457 = { sizeof (XsdKeyEntry_t3532015222), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1457[8] = 
{
	XsdKeyEntry_t3532015222::get_offset_of_StartDepth_0(),
	XsdKeyEntry_t3532015222::get_offset_of_SelectorLineNumber_1(),
	XsdKeyEntry_t3532015222::get_offset_of_SelectorLinePosition_2(),
	XsdKeyEntry_t3532015222::get_offset_of_SelectorHasLineInfo_3(),
	XsdKeyEntry_t3532015222::get_offset_of_KeyFields_4(),
	XsdKeyEntry_t3532015222::get_offset_of_KeyRefFound_5(),
	XsdKeyEntry_t3532015222::get_offset_of_OwnerSequence_6(),
	XsdKeyEntry_t3532015222::get_offset_of_keyFound_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1458 = { sizeof (XsdKeyEntryCollection_t1714053544), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1459 = { sizeof (XsdKeyTable_t2980902100), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1459[9] = 
{
	XsdKeyTable_t2980902100::get_offset_of_alwaysTrue_0(),
	XsdKeyTable_t2980902100::get_offset_of_selector_1(),
	XsdKeyTable_t2980902100::get_offset_of_source_2(),
	XsdKeyTable_t2980902100::get_offset_of_qname_3(),
	XsdKeyTable_t2980902100::get_offset_of_refKeyName_4(),
	XsdKeyTable_t2980902100::get_offset_of_Entries_5(),
	XsdKeyTable_t2980902100::get_offset_of_FinishedEntries_6(),
	XsdKeyTable_t2980902100::get_offset_of_StartDepth_7(),
	XsdKeyTable_t2980902100::get_offset_of_ReferencedKey_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1460 = { sizeof (XsdParticleStateManager_t4119977941), -1, sizeof(XsdParticleStateManager_t4119977941_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1460[6] = 
{
	XsdParticleStateManager_t4119977941::get_offset_of_table_0(),
	XsdParticleStateManager_t4119977941::get_offset_of_processContents_1(),
	XsdParticleStateManager_t4119977941::get_offset_of_CurrentElement_2(),
	XsdParticleStateManager_t4119977941::get_offset_of_ContextStack_3(),
	XsdParticleStateManager_t4119977941::get_offset_of_Context_4(),
	XsdParticleStateManager_t4119977941_StaticFields::get_offset_of_U3CU3Ef__switchU24map2_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1461 = { sizeof (XsdValidationState_t3545965403), -1, sizeof(XsdValidationState_t3545965403_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1461[3] = 
{
	XsdValidationState_t3545965403_StaticFields::get_offset_of_invalid_0(),
	XsdValidationState_t3545965403::get_offset_of_occured_1(),
	XsdValidationState_t3545965403::get_offset_of_manager_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1462 = { sizeof (XsdElementValidationState_t152111323), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1462[1] = 
{
	XsdElementValidationState_t152111323::get_offset_of_element_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1463 = { sizeof (XsdSequenceValidationState_t3542030006), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1463[4] = 
{
	XsdSequenceValidationState_t3542030006::get_offset_of_seq_3(),
	XsdSequenceValidationState_t3542030006::get_offset_of_current_4(),
	XsdSequenceValidationState_t3542030006::get_offset_of_currentAutomata_5(),
	XsdSequenceValidationState_t3542030006::get_offset_of_emptiable_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1464 = { sizeof (XsdChoiceValidationState_t1506407122), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1464[3] = 
{
	XsdChoiceValidationState_t1506407122::get_offset_of_choice_3(),
	XsdChoiceValidationState_t1506407122::get_offset_of_emptiable_4(),
	XsdChoiceValidationState_t1506407122::get_offset_of_emptiableComputed_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1465 = { sizeof (XsdAllValidationState_t1028212608), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1465[2] = 
{
	XsdAllValidationState_t1028212608::get_offset_of_all_3(),
	XsdAllValidationState_t1028212608::get_offset_of_consumed_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1466 = { sizeof (XsdAnyValidationState_t204702461), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1466[1] = 
{
	XsdAnyValidationState_t204702461::get_offset_of_any_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1467 = { sizeof (XsdAppendedValidationState_t3724408090), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1467[2] = 
{
	XsdAppendedValidationState_t3724408090::get_offset_of_head_3(),
	XsdAppendedValidationState_t3724408090::get_offset_of_rest_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1468 = { sizeof (XsdEmptyValidationState_t1914323912), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1469 = { sizeof (XsdInvalidValidationState_t1112885736), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1470 = { sizeof (XsdValidatingReader_t1704923617), -1, sizeof(XsdValidatingReader_t1704923617_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1470[30] = 
{
	XsdValidatingReader_t1704923617_StaticFields::get_offset_of_emptyAttributeArray_3(),
	XsdValidatingReader_t1704923617::get_offset_of_reader_4(),
	XsdValidatingReader_t1704923617::get_offset_of_resolver_5(),
	XsdValidatingReader_t1704923617::get_offset_of_sourceReaderSchemaInfo_6(),
	XsdValidatingReader_t1704923617::get_offset_of_readerLineInfo_7(),
	XsdValidatingReader_t1704923617::get_offset_of_validationType_8(),
	XsdValidatingReader_t1704923617::get_offset_of_schemas_9(),
	XsdValidatingReader_t1704923617::get_offset_of_namespaces_10(),
	XsdValidatingReader_t1704923617::get_offset_of_validationStarted_11(),
	XsdValidatingReader_t1704923617::get_offset_of_checkIdentity_12(),
	XsdValidatingReader_t1704923617::get_offset_of_idManager_13(),
	XsdValidatingReader_t1704923617::get_offset_of_checkKeyConstraints_14(),
	XsdValidatingReader_t1704923617::get_offset_of_keyTables_15(),
	XsdValidatingReader_t1704923617::get_offset_of_currentKeyFieldConsumers_16(),
	XsdValidatingReader_t1704923617::get_offset_of_tmpKeyrefPool_17(),
	XsdValidatingReader_t1704923617::get_offset_of_elementQNameStack_18(),
	XsdValidatingReader_t1704923617::get_offset_of_state_19(),
	XsdValidatingReader_t1704923617::get_offset_of_skipValidationDepth_20(),
	XsdValidatingReader_t1704923617::get_offset_of_xsiNilDepth_21(),
	XsdValidatingReader_t1704923617::get_offset_of_storedCharacters_22(),
	XsdValidatingReader_t1704923617::get_offset_of_shouldValidateCharacters_23(),
	XsdValidatingReader_t1704923617::get_offset_of_defaultAttributes_24(),
	XsdValidatingReader_t1704923617::get_offset_of_currentDefaultAttribute_25(),
	XsdValidatingReader_t1704923617::get_offset_of_defaultAttributesCache_26(),
	XsdValidatingReader_t1704923617::get_offset_of_defaultAttributeConsumed_27(),
	XsdValidatingReader_t1704923617::get_offset_of_currentAttrType_28(),
	XsdValidatingReader_t1704923617::get_offset_of_ValidationEventHandler_29(),
	XsdValidatingReader_t1704923617_StaticFields::get_offset_of_U3CU3Ef__switchU24map3_30(),
	XsdValidatingReader_t1704923617_StaticFields::get_offset_of_U3CU3Ef__switchU24map4_31(),
	XsdValidatingReader_t1704923617_StaticFields::get_offset_of_U3CU3Ef__switchU24map5_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1471 = { sizeof (XsdValidationContext_t3720679969), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1471[3] = 
{
	XsdValidationContext_t3720679969::get_offset_of_xsi_type_0(),
	XsdValidationContext_t3720679969::get_offset_of_State_1(),
	XsdValidationContext_t3720679969::get_offset_of_element_stack_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1472 = { sizeof (XsdIDManager_t3860002335), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1472[3] = 
{
	XsdIDManager_t3860002335::get_offset_of_idList_0(),
	XsdIDManager_t3860002335::get_offset_of_missingIDReferences_1(),
	XsdIDManager_t3860002335::get_offset_of_thisElementId_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1473 = { sizeof (XsdWildcard_t625524157), -1, sizeof(XsdWildcard_t625524157_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1473[10] = 
{
	XsdWildcard_t625524157::get_offset_of_xsobj_0(),
	XsdWildcard_t625524157::get_offset_of_ResolvedProcessing_1(),
	XsdWildcard_t625524157::get_offset_of_TargetNamespace_2(),
	XsdWildcard_t625524157::get_offset_of_SkipCompile_3(),
	XsdWildcard_t625524157::get_offset_of_HasValueAny_4(),
	XsdWildcard_t625524157::get_offset_of_HasValueLocal_5(),
	XsdWildcard_t625524157::get_offset_of_HasValueOther_6(),
	XsdWildcard_t625524157::get_offset_of_HasValueTargetNamespace_7(),
	XsdWildcard_t625524157::get_offset_of_ResolvedNamespaces_8(),
	XsdWildcard_t625524157_StaticFields::get_offset_of_U3CU3Ef__switchU24map6_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1474 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1475 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1476 = { sizeof (XmlFilterReader_t256953178), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1476[3] = 
{
	XmlFilterReader_t256953178::get_offset_of_reader_3(),
	XmlFilterReader_t256953178::get_offset_of_settings_4(),
	XmlFilterReader_t256953178::get_offset_of_lineInfo_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1477 = { sizeof (XsdWhitespaceFacet_t2758097827)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1477[4] = 
{
	XsdWhitespaceFacet_t2758097827::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1478 = { sizeof (XsdOrdering_t3741887935)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1478[5] = 
{
	XsdOrdering_t3741887935::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1479 = { sizeof (XsdAnySimpleType_t1096449895), -1, sizeof(XsdAnySimpleType_t1096449895_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1479[6] = 
{
	XsdAnySimpleType_t1096449895_StaticFields::get_offset_of_instance_55(),
	XsdAnySimpleType_t1096449895_StaticFields::get_offset_of_whitespaceArray_56(),
	XsdAnySimpleType_t1096449895_StaticFields::get_offset_of_booleanAllowedFacets_57(),
	XsdAnySimpleType_t1096449895_StaticFields::get_offset_of_decimalAllowedFacets_58(),
	XsdAnySimpleType_t1096449895_StaticFields::get_offset_of_durationAllowedFacets_59(),
	XsdAnySimpleType_t1096449895_StaticFields::get_offset_of_stringAllowedFacets_60(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1480 = { sizeof (XdtAnyAtomicType_t3359210273), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1481 = { sizeof (XdtUntypedAtomic_t2904699188), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1482 = { sizeof (XsdString_t263933896), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1483 = { sizeof (XsdNormalizedString_t2132216291), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1484 = { sizeof (XsdToken_t2902215462), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1485 = { sizeof (XsdLanguage_t3897851897), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1486 = { sizeof (XsdNMToken_t547135877), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1487 = { sizeof (XsdNMTokens_t1753890866), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1488 = { sizeof (XsdName_t1409945702), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1489 = { sizeof (XsdNCName_t414304927), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1490 = { sizeof (XsdID_t1067193160), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1491 = { sizeof (XsdIDRef_t1377311049), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1492 = { sizeof (XsdIDRefs_t763119546), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1493 = { sizeof (XsdEntity_t2664305022), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1494 = { sizeof (XsdEntities_t1103053540), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1495 = { sizeof (XsdNotation_t720379093), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1496 = { sizeof (XsdDecimal_t2932251550), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1497 = { sizeof (XsdInteger_t1330502157), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1498 = { sizeof (XsdLong_t4179235589), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1499 = { sizeof (XsdInt_t1488443894), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
