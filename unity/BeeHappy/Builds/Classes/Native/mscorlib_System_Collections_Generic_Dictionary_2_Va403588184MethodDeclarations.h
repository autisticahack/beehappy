﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2353257711MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,ThirdParty.Json.LitJson.ObjectMetadata>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m1086199455(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t403588184 *, Dictionary_2_t1700528341 *, const MethodInfo*))ValueCollection__ctor_m1737578728_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3174442893(__this, ___item0, method) ((  void (*) (ValueCollection_t403588184 *, ObjectMetadata_t4058137740 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3425682418_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3210438260(__this, method) ((  void (*) (ValueCollection_t403588184 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3333272915_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2264549147(__this, ___item0, method) ((  bool (*) (ValueCollection_t403588184 *, ObjectMetadata_t4058137740 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m395256518_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m852813374(__this, ___item0, method) ((  bool (*) (ValueCollection_t403588184 *, ObjectMetadata_t4058137740 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1977060715_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3136769798(__this, method) ((  Il2CppObject* (*) (ValueCollection_t403588184 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3619735941_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m1684584304(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t403588184 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m1938516405_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m439610869(__this, method) ((  Il2CppObject * (*) (ValueCollection_t403588184 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1327038742_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1243368658(__this, method) ((  bool (*) (ValueCollection_t403588184 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3773505079_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3810305504(__this, method) ((  bool (*) (ValueCollection_t403588184 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m710597849_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m3220659766(__this, method) ((  Il2CppObject * (*) (ValueCollection_t403588184 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m1051465185_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,ThirdParty.Json.LitJson.ObjectMetadata>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m1924915588(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t403588184 *, ObjectMetadataU5BU5D_t1554594757*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m381283883_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,ThirdParty.Json.LitJson.ObjectMetadata>::GetEnumerator()
#define ValueCollection_GetEnumerator_m1254766047(__this, method) ((  Enumerator_t3387061105  (*) (ValueCollection_t403588184 *, const MethodInfo*))ValueCollection_GetEnumerator_m1513899744_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,ThirdParty.Json.LitJson.ObjectMetadata>::get_Count()
#define ValueCollection_get_Count_m248522300(__this, method) ((  int32_t (*) (ValueCollection_t403588184 *, const MethodInfo*))ValueCollection_get_Count_m2421229277_gshared)(__this, method)
