﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Util.LoggingConfig
struct LoggingConfig_t4162907495;
// Amazon.Runtime.IMetricsFormatter
struct IMetricsFormatter_t495884108;
// Amazon.LoggingSection
struct LoggingSection_t905443946;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_LoggingOptions2865640765.h"
#include "AWSSDK_Core_Amazon_ResponseLoggingOption3443611737.h"
#include "AWSSDK_Core_Amazon_LogMetricsFormatOption97749509.h"
#include "AWSSDK_Core_Amazon_LoggingSection905443946.h"

// Amazon.LoggingOptions Amazon.Util.LoggingConfig::get_LogTo()
extern "C"  int32_t LoggingConfig_get_LogTo_m2527703070 (LoggingConfig_t4162907495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Util.LoggingConfig::set_LogTo(Amazon.LoggingOptions)
extern "C"  void LoggingConfig_set_LogTo_m2759407017 (LoggingConfig_t4162907495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.ResponseLoggingOption Amazon.Util.LoggingConfig::get_LogResponses()
extern "C"  int32_t LoggingConfig_get_LogResponses_m3613817537 (LoggingConfig_t4162907495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Util.LoggingConfig::set_LogResponses(Amazon.ResponseLoggingOption)
extern "C"  void LoggingConfig_set_LogResponses_m2298548422 (LoggingConfig_t4162907495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Amazon.Util.LoggingConfig::get_LogResponsesSizeLimit()
extern "C"  int32_t LoggingConfig_get_LogResponsesSizeLimit_m2903854815 (LoggingConfig_t4162907495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Util.LoggingConfig::set_LogResponsesSizeLimit(System.Int32)
extern "C"  void LoggingConfig_set_LogResponsesSizeLimit_m1860309678 (LoggingConfig_t4162907495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Util.LoggingConfig::get_LogMetrics()
extern "C"  bool LoggingConfig_get_LogMetrics_m820215732 (LoggingConfig_t4162907495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Util.LoggingConfig::set_LogMetrics(System.Boolean)
extern "C"  void LoggingConfig_set_LogMetrics_m1197184717 (LoggingConfig_t4162907495 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.LogMetricsFormatOption Amazon.Util.LoggingConfig::get_LogMetricsFormat()
extern "C"  int32_t LoggingConfig_get_LogMetricsFormat_m102152033 (LoggingConfig_t4162907495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Util.LoggingConfig::set_LogMetricsFormat(Amazon.LogMetricsFormatOption)
extern "C"  void LoggingConfig_set_LogMetricsFormat_m2177219328 (LoggingConfig_t4162907495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.IMetricsFormatter Amazon.Util.LoggingConfig::get_LogMetricsCustomFormatter()
extern "C"  Il2CppObject * LoggingConfig_get_LogMetricsCustomFormatter_m875376318 (LoggingConfig_t4162907495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Util.LoggingConfig::set_LogMetricsCustomFormatter(Amazon.Runtime.IMetricsFormatter)
extern "C"  void LoggingConfig_set_LogMetricsCustomFormatter_m2153040261 (LoggingConfig_t4162907495 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Util.LoggingConfig::.ctor()
extern "C"  void LoggingConfig__ctor_m3489170538 (LoggingConfig_t4162907495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Util.LoggingConfig::Configure(Amazon.LoggingSection)
extern "C"  void LoggingConfig_Configure_m829115694 (LoggingConfig_t4162907495 * __this, LoggingSection_t905443946 * ___section0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Util.LoggingConfig::.cctor()
extern "C"  void LoggingConfig__cctor_m3203378235 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
