﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityResponse
struct AssumeRoleWithWebIdentityResponse_t3931705881;
// Amazon.SecurityToken.Model.AssumedRoleUser
struct AssumedRoleUser_t150458319;
// System.String
struct String_t;
// Amazon.SecurityToken.Model.Credentials
struct Credentials_t3554032640;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_Model_Ass150458319.h"
#include "mscorlib_System_String2029220233.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_Model_Cr3554032640.h"

// System.Void Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityResponse::set_AssumedRoleUser(Amazon.SecurityToken.Model.AssumedRoleUser)
extern "C"  void AssumeRoleWithWebIdentityResponse_set_AssumedRoleUser_m3816583747 (AssumeRoleWithWebIdentityResponse_t3931705881 * __this, AssumedRoleUser_t150458319 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityResponse::set_Audience(System.String)
extern "C"  void AssumeRoleWithWebIdentityResponse_set_Audience_m3186117859 (AssumeRoleWithWebIdentityResponse_t3931705881 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.SecurityToken.Model.Credentials Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityResponse::get_Credentials()
extern "C"  Credentials_t3554032640 * AssumeRoleWithWebIdentityResponse_get_Credentials_m496982176 (AssumeRoleWithWebIdentityResponse_t3931705881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityResponse::set_Credentials(Amazon.SecurityToken.Model.Credentials)
extern "C"  void AssumeRoleWithWebIdentityResponse_set_Credentials_m3033710271 (AssumeRoleWithWebIdentityResponse_t3931705881 * __this, Credentials_t3554032640 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityResponse::set_PackedPolicySize(System.Int32)
extern "C"  void AssumeRoleWithWebIdentityResponse_set_PackedPolicySize_m1522389737 (AssumeRoleWithWebIdentityResponse_t3931705881 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityResponse::set_Provider(System.String)
extern "C"  void AssumeRoleWithWebIdentityResponse_set_Provider_m3771874134 (AssumeRoleWithWebIdentityResponse_t3931705881 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityResponse::set_SubjectFromWebIdentityToken(System.String)
extern "C"  void AssumeRoleWithWebIdentityResponse_set_SubjectFromWebIdentityToken_m2341987586 (AssumeRoleWithWebIdentityResponse_t3931705881 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityResponse::.ctor()
extern "C"  void AssumeRoleWithWebIdentityResponse__ctor_m4120461074 (AssumeRoleWithWebIdentityResponse_t3931705881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
