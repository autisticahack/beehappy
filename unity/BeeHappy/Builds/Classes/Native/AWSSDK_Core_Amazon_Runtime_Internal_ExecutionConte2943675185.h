﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.Runtime.IRequestContext
struct IRequestContext_t1620878341;
// Amazon.Runtime.IResponseContext
struct IResponseContext_t898521739;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.ExecutionContext
struct  ExecutionContext_t2943675185  : public Il2CppObject
{
public:
	// Amazon.Runtime.IRequestContext Amazon.Runtime.Internal.ExecutionContext::<RequestContext>k__BackingField
	Il2CppObject * ___U3CRequestContextU3Ek__BackingField_0;
	// Amazon.Runtime.IResponseContext Amazon.Runtime.Internal.ExecutionContext::<ResponseContext>k__BackingField
	Il2CppObject * ___U3CResponseContextU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CRequestContextU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ExecutionContext_t2943675185, ___U3CRequestContextU3Ek__BackingField_0)); }
	inline Il2CppObject * get_U3CRequestContextU3Ek__BackingField_0() const { return ___U3CRequestContextU3Ek__BackingField_0; }
	inline Il2CppObject ** get_address_of_U3CRequestContextU3Ek__BackingField_0() { return &___U3CRequestContextU3Ek__BackingField_0; }
	inline void set_U3CRequestContextU3Ek__BackingField_0(Il2CppObject * value)
	{
		___U3CRequestContextU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CRequestContextU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CResponseContextU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ExecutionContext_t2943675185, ___U3CResponseContextU3Ek__BackingField_1)); }
	inline Il2CppObject * get_U3CResponseContextU3Ek__BackingField_1() const { return ___U3CResponseContextU3Ek__BackingField_1; }
	inline Il2CppObject ** get_address_of_U3CResponseContextU3Ek__BackingField_1() { return &___U3CResponseContextU3Ek__BackingField_1; }
	inline void set_U3CResponseContextU3Ek__BackingField_1(Il2CppObject * value)
	{
		___U3CResponseContextU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CResponseContextU3Ek__BackingField_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
