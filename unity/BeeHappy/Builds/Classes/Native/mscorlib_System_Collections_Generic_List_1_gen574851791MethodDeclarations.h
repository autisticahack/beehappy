﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2058570427MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.DatasetMetadata>::.ctor()
#define List_1__ctor_m1883786105(__this, method) ((  void (*) (List_1_t574851791 *, const MethodInfo*))List_1__ctor_m3568471827_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.DatasetMetadata>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m449626264(__this, ___collection0, method) ((  void (*) (List_1_t574851791 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m454375187_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.DatasetMetadata>::.ctor(System.Int32)
#define List_1__ctor_m2068482894(__this, ___capacity0, method) ((  void (*) (List_1_t574851791 *, int32_t, const MethodInfo*))List_1__ctor_m136460305_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.DatasetMetadata>::.cctor()
#define List_1__cctor_m2467269526(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m138621019_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.DatasetMetadata>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2987076853(__this, method) ((  Il2CppObject* (*) (List_1_t574851791 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m154161632_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.DatasetMetadata>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m1408110189(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t574851791 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m2020941110_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.DatasetMetadata>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m4128826316(__this, method) ((  Il2CppObject * (*) (List_1_t574851791 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m3552870393_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.DatasetMetadata>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m3985194893(__this, ___item0, method) ((  int32_t (*) (List_1_t574851791 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m1765626550_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.DatasetMetadata>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m96479629(__this, ___item0, method) ((  bool (*) (List_1_t574851791 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m149594880_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.DatasetMetadata>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m4088348087(__this, ___item0, method) ((  int32_t (*) (List_1_t574851791 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m406088260_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.DatasetMetadata>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m3539006206(__this, ___index0, ___item1, method) ((  void (*) (List_1_t574851791 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m3961795241_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.DatasetMetadata>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m2371919164(__this, ___item0, method) ((  void (*) (List_1_t574851791 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m3415450529_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.DatasetMetadata>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1507250940(__this, method) ((  bool (*) (List_1_t574851791 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2131934397_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.DatasetMetadata>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m757727009(__this, method) ((  bool (*) (List_1_t574851791 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m418560222_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.DatasetMetadata>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m1138954977(__this, method) ((  Il2CppObject * (*) (List_1_t574851791 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1594235606_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.DatasetMetadata>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m4067079926(__this, method) ((  bool (*) (List_1_t574851791 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m2120144013_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.DatasetMetadata>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m532412661(__this, method) ((  bool (*) (List_1_t574851791 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m257950146_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.DatasetMetadata>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m3724953428(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t574851791 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m936612973_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.DatasetMetadata>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m2170049407(__this, ___index0, ___value1, method) ((  void (*) (List_1_t574851791 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m162109184_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.DatasetMetadata>::Add(T)
#define List_1_Add_m3269987093(__this, ___item0, method) ((  void (*) (List_1_t574851791 *, DatasetMetadata_t1205730659 *, const MethodInfo*))List_1_Add_m3051125391_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.DatasetMetadata>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m331751829(__this, ___newCount0, method) ((  void (*) (List_1_t574851791 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m185971996_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.DatasetMetadata>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m3891116381(__this, ___collection0, method) ((  void (*) (List_1_t574851791 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m1580067148_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.DatasetMetadata>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m4020403725(__this, ___enumerable0, method) ((  void (*) (List_1_t574851791 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m2489692396_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.DatasetMetadata>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m1145919132(__this, ___collection0, method) ((  void (*) (List_1_t574851791 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m3614127065_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.DatasetMetadata>::AsReadOnly()
#define List_1_AsReadOnly_m4082328453(__this, method) ((  ReadOnlyCollection_1_t1391516351 * (*) (List_1_t574851791 *, const MethodInfo*))List_1_AsReadOnly_m2563000362_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.DatasetMetadata>::Clear()
#define List_1_Clear_m2692894774(__this, method) ((  void (*) (List_1_t574851791 *, const MethodInfo*))List_1_Clear_m4254626809_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.DatasetMetadata>::Contains(T)
#define List_1_Contains_m1447656852(__this, ___item0, method) ((  bool (*) (List_1_t574851791 *, DatasetMetadata_t1205730659 *, const MethodInfo*))List_1_Contains_m2577748987_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.DatasetMetadata>::CopyTo(T[])
#define List_1_CopyTo_m547870637(__this, ___array0, method) ((  void (*) (List_1_t574851791 *, DatasetMetadataU5BU5D_t1005480370*, const MethodInfo*))List_1_CopyTo_m626830950_gshared)(__this, ___array0, method)
// System.Void System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.DatasetMetadata>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m4219721186(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t574851791 *, DatasetMetadataU5BU5D_t1005480370*, int32_t, const MethodInfo*))List_1_CopyTo_m1758262197_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.DatasetMetadata>::Find(System.Predicate`1<T>)
#define List_1_Find_m2827102496(__this, ___match0, method) ((  DatasetMetadata_t1205730659 * (*) (List_1_t574851791 *, Predicate_1_t3943668070 *, const MethodInfo*))List_1_Find_m1725159095_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.DatasetMetadata>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m3493263965(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t3943668070 *, const MethodInfo*))List_1_CheckMatch_m1196994270_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.DatasetMetadata>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m1267424930(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t574851791 *, int32_t, int32_t, Predicate_1_t3943668070 *, const MethodInfo*))List_1_GetIndex_m3409004147_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.DatasetMetadata>::GetEnumerator()
#define List_1_GetEnumerator_m3299153578(__this, method) ((  Enumerator_t109581465  (*) (List_1_t574851791 *, const MethodInfo*))List_1_GetEnumerator_m1147178262_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.DatasetMetadata>::IndexOf(T)
#define List_1_IndexOf_m1224074900(__this, ___item0, method) ((  int32_t (*) (List_1_t574851791 *, DatasetMetadata_t1205730659 *, const MethodInfo*))List_1_IndexOf_m2070479489_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.DatasetMetadata>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m4157732425(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t574851791 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3137156970_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.DatasetMetadata>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m4118023638(__this, ___index0, method) ((  void (*) (List_1_t574851791 *, int32_t, const MethodInfo*))List_1_CheckIndex_m524615377_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.DatasetMetadata>::Insert(System.Int32,T)
#define List_1_Insert_m690985031(__this, ___index0, ___item1, method) ((  void (*) (List_1_t574851791 *, int32_t, DatasetMetadata_t1205730659 *, const MethodInfo*))List_1_Insert_m11735664_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.DatasetMetadata>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m361783996(__this, ___collection0, method) ((  void (*) (List_1_t574851791 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m3968030679_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.DatasetMetadata>::Remove(T)
#define List_1_Remove_m3969498107(__this, ___item0, method) ((  bool (*) (List_1_t574851791 *, DatasetMetadata_t1205730659 *, const MethodInfo*))List_1_Remove_m1271859478_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.DatasetMetadata>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m588138341(__this, ___match0, method) ((  int32_t (*) (List_1_t574851791 *, Predicate_1_t3943668070 *, const MethodInfo*))List_1_RemoveAll_m2972055270_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.DatasetMetadata>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m2221904483(__this, ___index0, method) ((  void (*) (List_1_t574851791 *, int32_t, const MethodInfo*))List_1_RemoveAt_m3615096820_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.DatasetMetadata>::Reverse()
#define List_1_Reverse_m3902504881(__this, method) ((  void (*) (List_1_t574851791 *, const MethodInfo*))List_1_Reverse_m4038478200_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.DatasetMetadata>::Sort()
#define List_1_Sort_m2030882723(__this, method) ((  void (*) (List_1_t574851791 *, const MethodInfo*))List_1_Sort_m554162636_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.DatasetMetadata>::Sort(System.Collections.Generic.IComparer`1<T>)
#define List_1_Sort_m4114468663(__this, ___comparer0, method) ((  void (*) (List_1_t574851791 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m1776685136_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.DatasetMetadata>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m3282594270(__this, ___comparison0, method) ((  void (*) (List_1_t574851791 *, Comparison_1_t2467469510 *, const MethodInfo*))List_1_Sort_m785723827_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.DatasetMetadata>::ToArray()
#define List_1_ToArray_m916816772(__this, method) ((  DatasetMetadataU5BU5D_t1005480370* (*) (List_1_t574851791 *, const MethodInfo*))List_1_ToArray_m1887490109_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.DatasetMetadata>::TrimExcess()
#define List_1_TrimExcess_m580644280(__this, method) ((  void (*) (List_1_t574851791 *, const MethodInfo*))List_1_TrimExcess_m1944241237_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.DatasetMetadata>::get_Capacity()
#define List_1_get_Capacity_m864537874(__this, method) ((  int32_t (*) (List_1_t574851791 *, const MethodInfo*))List_1_get_Capacity_m3133733835_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.DatasetMetadata>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m2322001373(__this, ___value0, method) ((  void (*) (List_1_t574851791 *, int32_t, const MethodInfo*))List_1_set_Capacity_m491101164_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.DatasetMetadata>::get_Count()
#define List_1_get_Count_m2798634909(__this, method) ((  int32_t (*) (List_1_t574851791 *, const MethodInfo*))List_1_get_Count_m2899908835_gshared)(__this, method)
// T System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.DatasetMetadata>::get_Item(System.Int32)
#define List_1_get_Item_m996816271(__this, ___index0, method) ((  DatasetMetadata_t1205730659 * (*) (List_1_t574851791 *, int32_t, const MethodInfo*))List_1_get_Item_m1354830498_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.DatasetMetadata>::set_Item(System.Int32,T)
#define List_1_set_Item_m3739870376(__this, ___index0, ___value1, method) ((  void (*) (List_1_t574851791 *, int32_t, DatasetMetadata_t1205730659 *, const MethodInfo*))List_1_set_Item_m4128108021_gshared)(__this, ___index0, ___value1, method)
