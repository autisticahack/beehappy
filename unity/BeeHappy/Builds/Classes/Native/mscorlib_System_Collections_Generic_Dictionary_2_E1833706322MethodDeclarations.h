﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2464585190MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Collections.Generic.List`1<System.Object>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m2856835333(__this, ___dictionary0, method) ((  void (*) (Enumerator_t1833706322 *, Dictionary_2_t513681620 *, const MethodInfo*))Enumerator__ctor_m2882573199_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Collections.Generic.List`1<System.Object>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2135579674(__this, method) ((  Il2CppObject * (*) (Enumerator_t1833706322 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2753081182_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Collections.Generic.List`1<System.Object>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m4048396836(__this, method) ((  void (*) (Enumerator_t1833706322 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3448018626_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Collections.Generic.List`1<System.Object>>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3569629691(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t1833706322 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1386174965_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Collections.Generic.List`1<System.Object>>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m781561868(__this, method) ((  Il2CppObject * (*) (Enumerator_t1833706322 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3637591552_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Collections.Generic.List`1<System.Object>>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2643853626(__this, method) ((  Il2CppObject * (*) (Enumerator_t1833706322 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1974664938_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Collections.Generic.List`1<System.Object>>::MoveNext()
#define Enumerator_MoveNext_m2339358443(__this, method) ((  bool (*) (Enumerator_t1833706322 *, const MethodInfo*))Enumerator_MoveNext_m3844071554_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Collections.Generic.List`1<System.Object>>::get_Current()
#define Enumerator_get_Current_m1224254474(__this, method) ((  KeyValuePair_2_t2565994138  (*) (Enumerator_t1833706322 *, const MethodInfo*))Enumerator_get_Current_m1831421754_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Collections.Generic.List`1<System.Object>>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m1636891193(__this, method) ((  int32_t (*) (Enumerator_t1833706322 *, const MethodInfo*))Enumerator_get_CurrentKey_m2750410819_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Collections.Generic.List`1<System.Object>>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m2994965041(__this, method) ((  List_1_t2058570427 * (*) (Enumerator_t1833706322 *, const MethodInfo*))Enumerator_get_CurrentValue_m4218670947_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Collections.Generic.List`1<System.Object>>::Reset()
#define Enumerator_Reset_m3969433339(__this, method) ((  void (*) (Enumerator_t1833706322 *, const MethodInfo*))Enumerator_Reset_m1867529997_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Collections.Generic.List`1<System.Object>>::VerifyState()
#define Enumerator_VerifyState_m3666525424(__this, method) ((  void (*) (Enumerator_t1833706322 *, const MethodInfo*))Enumerator_VerifyState_m1404756950_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Collections.Generic.List`1<System.Object>>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m3045951732(__this, method) ((  void (*) (Enumerator_t1833706322 *, const MethodInfo*))Enumerator_VerifyCurrent_m4158200186_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Collections.Generic.List`1<System.Object>>::Dispose()
#define Enumerator_Dispose_m1622657885(__this, method) ((  void (*) (Enumerator_t1833706322 *, const MethodInfo*))Enumerator_Dispose_m2167966803_gshared)(__this, method)
