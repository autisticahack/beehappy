﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.HashSet`1<System.Net.HttpStatusCode>
struct HashSet_1_t231870495;
// System.Collections.Generic.IEqualityComparer`1<System.Net.HttpStatusCode>
struct IEqualityComparer_1_t1111042419;
// System.Collections.Generic.IEnumerable`1<System.Net.HttpStatusCode>
struct IEnumerable_1_t2190536686;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// System.Collections.Generic.IEnumerator`1<System.Net.HttpStatusCode>
struct IEnumerator_1_t3668900764;
// System.Net.HttpStatusCode[]
struct HttpStatusCodeU5BU5D_t1305255060;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serialization228987430.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"
#include "System_System_Net_HttpStatusCode1898409641.h"
#include "mscorlib_System_Object2689449295.h"
#include "System_Core_System_Collections_Generic_HashSet_1_E3015153633.h"

// System.Void System.Collections.Generic.HashSet`1<System.Net.HttpStatusCode>::.ctor()
extern "C"  void HashSet_1__ctor_m3870339028_gshared (HashSet_1_t231870495 * __this, const MethodInfo* method);
#define HashSet_1__ctor_m3870339028(__this, method) ((  void (*) (HashSet_1_t231870495 *, const MethodInfo*))HashSet_1__ctor_m3870339028_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<System.Net.HttpStatusCode>::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
extern "C"  void HashSet_1__ctor_m3798256662_gshared (HashSet_1_t231870495 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define HashSet_1__ctor_m3798256662(__this, ___comparer0, method) ((  void (*) (HashSet_1_t231870495 *, Il2CppObject*, const MethodInfo*))HashSet_1__ctor_m3798256662_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.HashSet`1<System.Net.HttpStatusCode>::.ctor(System.Collections.Generic.IEnumerable`1<T>,System.Collections.Generic.IEqualityComparer`1<T>)
extern "C"  void HashSet_1__ctor_m689565271_gshared (HashSet_1_t231870495 * __this, Il2CppObject* ___collection0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define HashSet_1__ctor_m689565271(__this, ___collection0, ___comparer1, method) ((  void (*) (HashSet_1_t231870495 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))HashSet_1__ctor_m689565271_gshared)(__this, ___collection0, ___comparer1, method)
// System.Void System.Collections.Generic.HashSet`1<System.Net.HttpStatusCode>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void HashSet_1__ctor_m3678429275_gshared (HashSet_1_t231870495 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define HashSet_1__ctor_m3678429275(__this, ___info0, ___context1, method) ((  void (*) (HashSet_1_t231870495 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))HashSet_1__ctor_m3678429275_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1<System.Net.HttpStatusCode>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3316057088_gshared (HashSet_1_t231870495 * __this, const MethodInfo* method);
#define HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3316057088(__this, method) ((  Il2CppObject* (*) (HashSet_1_t231870495 *, const MethodInfo*))HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3316057088_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Net.HttpStatusCode>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2338079681_gshared (HashSet_1_t231870495 * __this, const MethodInfo* method);
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2338079681(__this, method) ((  bool (*) (HashSet_1_t231870495 *, const MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2338079681_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<System.Net.HttpStatusCode>::System.Collections.Generic.ICollection<T>.CopyTo(T[],System.Int32)
extern "C"  void HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m77326877_gshared (HashSet_1_t231870495 * __this, HttpStatusCodeU5BU5D_t1305255060* ___array0, int32_t ___index1, const MethodInfo* method);
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m77326877(__this, ___array0, ___index1, method) ((  void (*) (HashSet_1_t231870495 *, HttpStatusCodeU5BU5D_t1305255060*, int32_t, const MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m77326877_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.HashSet`1<System.Net.HttpStatusCode>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1848519573_gshared (HashSet_1_t231870495 * __this, int32_t ___item0, const MethodInfo* method);
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1848519573(__this, ___item0, method) ((  void (*) (HashSet_1_t231870495 *, int32_t, const MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1848519573_gshared)(__this, ___item0, method)
// System.Collections.IEnumerator System.Collections.Generic.HashSet`1<System.Net.HttpStatusCode>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * HashSet_1_System_Collections_IEnumerable_GetEnumerator_m182790337_gshared (HashSet_1_t231870495 * __this, const MethodInfo* method);
#define HashSet_1_System_Collections_IEnumerable_GetEnumerator_m182790337(__this, method) ((  Il2CppObject * (*) (HashSet_1_t231870495 *, const MethodInfo*))HashSet_1_System_Collections_IEnumerable_GetEnumerator_m182790337_gshared)(__this, method)
// System.Int32 System.Collections.Generic.HashSet`1<System.Net.HttpStatusCode>::get_Count()
extern "C"  int32_t HashSet_1_get_Count_m740495132_gshared (HashSet_1_t231870495 * __this, const MethodInfo* method);
#define HashSet_1_get_Count_m740495132(__this, method) ((  int32_t (*) (HashSet_1_t231870495 *, const MethodInfo*))HashSet_1_get_Count_m740495132_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<System.Net.HttpStatusCode>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<T>)
extern "C"  void HashSet_1_Init_m4176037263_gshared (HashSet_1_t231870495 * __this, int32_t ___capacity0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define HashSet_1_Init_m4176037263(__this, ___capacity0, ___comparer1, method) ((  void (*) (HashSet_1_t231870495 *, int32_t, Il2CppObject*, const MethodInfo*))HashSet_1_Init_m4176037263_gshared)(__this, ___capacity0, ___comparer1, method)
// System.Void System.Collections.Generic.HashSet`1<System.Net.HttpStatusCode>::InitArrays(System.Int32)
extern "C"  void HashSet_1_InitArrays_m4160309109_gshared (HashSet_1_t231870495 * __this, int32_t ___size0, const MethodInfo* method);
#define HashSet_1_InitArrays_m4160309109(__this, ___size0, method) ((  void (*) (HashSet_1_t231870495 *, int32_t, const MethodInfo*))HashSet_1_InitArrays_m4160309109_gshared)(__this, ___size0, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Net.HttpStatusCode>::SlotsContainsAt(System.Int32,System.Int32,T)
extern "C"  bool HashSet_1_SlotsContainsAt_m2237646017_gshared (HashSet_1_t231870495 * __this, int32_t ___index0, int32_t ___hash1, int32_t ___item2, const MethodInfo* method);
#define HashSet_1_SlotsContainsAt_m2237646017(__this, ___index0, ___hash1, ___item2, method) ((  bool (*) (HashSet_1_t231870495 *, int32_t, int32_t, int32_t, const MethodInfo*))HashSet_1_SlotsContainsAt_m2237646017_gshared)(__this, ___index0, ___hash1, ___item2, method)
// System.Void System.Collections.Generic.HashSet`1<System.Net.HttpStatusCode>::CopyTo(T[],System.Int32)
extern "C"  void HashSet_1_CopyTo_m3615159241_gshared (HashSet_1_t231870495 * __this, HttpStatusCodeU5BU5D_t1305255060* ___array0, int32_t ___index1, const MethodInfo* method);
#define HashSet_1_CopyTo_m3615159241(__this, ___array0, ___index1, method) ((  void (*) (HashSet_1_t231870495 *, HttpStatusCodeU5BU5D_t1305255060*, int32_t, const MethodInfo*))HashSet_1_CopyTo_m3615159241_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.HashSet`1<System.Net.HttpStatusCode>::CopyTo(T[],System.Int32,System.Int32)
extern "C"  void HashSet_1_CopyTo_m2005650404_gshared (HashSet_1_t231870495 * __this, HttpStatusCodeU5BU5D_t1305255060* ___array0, int32_t ___index1, int32_t ___count2, const MethodInfo* method);
#define HashSet_1_CopyTo_m2005650404(__this, ___array0, ___index1, ___count2, method) ((  void (*) (HashSet_1_t231870495 *, HttpStatusCodeU5BU5D_t1305255060*, int32_t, int32_t, const MethodInfo*))HashSet_1_CopyTo_m2005650404_gshared)(__this, ___array0, ___index1, ___count2, method)
// System.Void System.Collections.Generic.HashSet`1<System.Net.HttpStatusCode>::Resize()
extern "C"  void HashSet_1_Resize_m2404947652_gshared (HashSet_1_t231870495 * __this, const MethodInfo* method);
#define HashSet_1_Resize_m2404947652(__this, method) ((  void (*) (HashSet_1_t231870495 *, const MethodInfo*))HashSet_1_Resize_m2404947652_gshared)(__this, method)
// System.Int32 System.Collections.Generic.HashSet`1<System.Net.HttpStatusCode>::GetLinkHashCode(System.Int32)
extern "C"  int32_t HashSet_1_GetLinkHashCode_m4229982504_gshared (HashSet_1_t231870495 * __this, int32_t ___index0, const MethodInfo* method);
#define HashSet_1_GetLinkHashCode_m4229982504(__this, ___index0, method) ((  int32_t (*) (HashSet_1_t231870495 *, int32_t, const MethodInfo*))HashSet_1_GetLinkHashCode_m4229982504_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.Generic.HashSet`1<System.Net.HttpStatusCode>::GetItemHashCode(T)
extern "C"  int32_t HashSet_1_GetItemHashCode_m2325712112_gshared (HashSet_1_t231870495 * __this, int32_t ___item0, const MethodInfo* method);
#define HashSet_1_GetItemHashCode_m2325712112(__this, ___item0, method) ((  int32_t (*) (HashSet_1_t231870495 *, int32_t, const MethodInfo*))HashSet_1_GetItemHashCode_m2325712112_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Net.HttpStatusCode>::Add(T)
extern "C"  bool HashSet_1_Add_m1943294914_gshared (HashSet_1_t231870495 * __this, int32_t ___item0, const MethodInfo* method);
#define HashSet_1_Add_m1943294914(__this, ___item0, method) ((  bool (*) (HashSet_1_t231870495 *, int32_t, const MethodInfo*))HashSet_1_Add_m1943294914_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.HashSet`1<System.Net.HttpStatusCode>::Clear()
extern "C"  void HashSet_1_Clear_m591795877_gshared (HashSet_1_t231870495 * __this, const MethodInfo* method);
#define HashSet_1_Clear_m591795877(__this, method) ((  void (*) (HashSet_1_t231870495 *, const MethodInfo*))HashSet_1_Clear_m591795877_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Net.HttpStatusCode>::Contains(T)
extern "C"  bool HashSet_1_Contains_m418992247_gshared (HashSet_1_t231870495 * __this, int32_t ___item0, const MethodInfo* method);
#define HashSet_1_Contains_m418992247(__this, ___item0, method) ((  bool (*) (HashSet_1_t231870495 *, int32_t, const MethodInfo*))HashSet_1_Contains_m418992247_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Net.HttpStatusCode>::Remove(T)
extern "C"  bool HashSet_1_Remove_m3897634860_gshared (HashSet_1_t231870495 * __this, int32_t ___item0, const MethodInfo* method);
#define HashSet_1_Remove_m3897634860(__this, ___item0, method) ((  bool (*) (HashSet_1_t231870495 *, int32_t, const MethodInfo*))HashSet_1_Remove_m3897634860_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.HashSet`1<System.Net.HttpStatusCode>::IntersectWith(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void HashSet_1_IntersectWith_m2493779360_gshared (HashSet_1_t231870495 * __this, Il2CppObject* ___other0, const MethodInfo* method);
#define HashSet_1_IntersectWith_m2493779360(__this, ___other0, method) ((  void (*) (HashSet_1_t231870495 *, Il2CppObject*, const MethodInfo*))HashSet_1_IntersectWith_m2493779360_gshared)(__this, ___other0, method)
// System.Void System.Collections.Generic.HashSet`1<System.Net.HttpStatusCode>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void HashSet_1_GetObjectData_m1289567190_gshared (HashSet_1_t231870495 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define HashSet_1_GetObjectData_m1289567190(__this, ___info0, ___context1, method) ((  void (*) (HashSet_1_t231870495 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))HashSet_1_GetObjectData_m1289567190_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.HashSet`1<System.Net.HttpStatusCode>::OnDeserialization(System.Object)
extern "C"  void HashSet_1_OnDeserialization_m1503832760_gshared (HashSet_1_t231870495 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define HashSet_1_OnDeserialization_m1503832760(__this, ___sender0, method) ((  void (*) (HashSet_1_t231870495 *, Il2CppObject *, const MethodInfo*))HashSet_1_OnDeserialization_m1503832760_gshared)(__this, ___sender0, method)
// System.Collections.Generic.HashSet`1/Enumerator<T> System.Collections.Generic.HashSet`1<System.Net.HttpStatusCode>::GetEnumerator()
extern "C"  Enumerator_t3015153633  HashSet_1_GetEnumerator_m3295661586_gshared (HashSet_1_t231870495 * __this, const MethodInfo* method);
#define HashSet_1_GetEnumerator_m3295661586(__this, method) ((  Enumerator_t3015153633  (*) (HashSet_1_t231870495 *, const MethodInfo*))HashSet_1_GetEnumerator_m3295661586_gshared)(__this, method)
