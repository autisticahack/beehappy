﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetIdRequestMarshaller
struct GetIdRequestMarshaller_t3365653223;
// Amazon.Runtime.Internal.IRequest
struct IRequest_t2400804350;
// Amazon.Runtime.AmazonWebServiceRequest
struct AmazonWebServiceRequest_t3384026212;
// Amazon.CognitoIdentity.Model.GetIdRequest
struct GetIdRequest_t4078561340;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_Runtime_AmazonWebServiceRequest3384026212.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Mode4078561340.h"

// Amazon.Runtime.Internal.IRequest Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetIdRequestMarshaller::Marshall(Amazon.Runtime.AmazonWebServiceRequest)
extern "C"  Il2CppObject * GetIdRequestMarshaller_Marshall_m3007728487 (GetIdRequestMarshaller_t3365653223 * __this, AmazonWebServiceRequest_t3384026212 * ___input0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.Internal.IRequest Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetIdRequestMarshaller::Marshall(Amazon.CognitoIdentity.Model.GetIdRequest)
extern "C"  Il2CppObject * GetIdRequestMarshaller_Marshall_m1677148795 (GetIdRequestMarshaller_t3365653223 * __this, GetIdRequest_t4078561340 * ___publicRequest0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetIdRequestMarshaller::.ctor()
extern "C"  void GetIdRequestMarshaller__ctor_m1760098451 (GetIdRequestMarshaller_t3365653223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
