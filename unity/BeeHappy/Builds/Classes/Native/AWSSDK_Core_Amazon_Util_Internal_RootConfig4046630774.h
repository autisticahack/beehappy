﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.Util.LoggingConfig
struct LoggingConfig_t4162907495;
// Amazon.Util.ProxyConfig
struct ProxyConfig_t2693849256;
// System.String
struct String_t;
// System.Collections.Generic.IDictionary`2<System.String,System.Xml.Linq.XElement>
struct IDictionary_2_t467683733;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Util.Internal.RootConfig
struct  RootConfig_t4046630774  : public Il2CppObject
{
public:
	// Amazon.Util.LoggingConfig Amazon.Util.Internal.RootConfig::<Logging>k__BackingField
	LoggingConfig_t4162907495 * ___U3CLoggingU3Ek__BackingField_0;
	// Amazon.Util.ProxyConfig Amazon.Util.Internal.RootConfig::<Proxy>k__BackingField
	ProxyConfig_t2693849256 * ___U3CProxyU3Ek__BackingField_1;
	// System.String Amazon.Util.Internal.RootConfig::<EndpointDefinition>k__BackingField
	String_t* ___U3CEndpointDefinitionU3Ek__BackingField_2;
	// System.String Amazon.Util.Internal.RootConfig::<Region>k__BackingField
	String_t* ___U3CRegionU3Ek__BackingField_3;
	// System.String Amazon.Util.Internal.RootConfig::<ProfileName>k__BackingField
	String_t* ___U3CProfileNameU3Ek__BackingField_4;
	// System.String Amazon.Util.Internal.RootConfig::<ProfilesLocation>k__BackingField
	String_t* ___U3CProfilesLocationU3Ek__BackingField_5;
	// System.Boolean Amazon.Util.Internal.RootConfig::<UseSdkCache>k__BackingField
	bool ___U3CUseSdkCacheU3Ek__BackingField_6;
	// System.Boolean Amazon.Util.Internal.RootConfig::<CorrectForClockSkew>k__BackingField
	bool ___U3CCorrectForClockSkewU3Ek__BackingField_7;
	// System.String Amazon.Util.Internal.RootConfig::<ApplicationName>k__BackingField
	String_t* ___U3CApplicationNameU3Ek__BackingField_8;
	// System.Collections.Generic.IDictionary`2<System.String,System.Xml.Linq.XElement> Amazon.Util.Internal.RootConfig::<ServiceSections>k__BackingField
	Il2CppObject* ___U3CServiceSectionsU3Ek__BackingField_9;

public:
	inline static int32_t get_offset_of_U3CLoggingU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RootConfig_t4046630774, ___U3CLoggingU3Ek__BackingField_0)); }
	inline LoggingConfig_t4162907495 * get_U3CLoggingU3Ek__BackingField_0() const { return ___U3CLoggingU3Ek__BackingField_0; }
	inline LoggingConfig_t4162907495 ** get_address_of_U3CLoggingU3Ek__BackingField_0() { return &___U3CLoggingU3Ek__BackingField_0; }
	inline void set_U3CLoggingU3Ek__BackingField_0(LoggingConfig_t4162907495 * value)
	{
		___U3CLoggingU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CLoggingU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CProxyU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RootConfig_t4046630774, ___U3CProxyU3Ek__BackingField_1)); }
	inline ProxyConfig_t2693849256 * get_U3CProxyU3Ek__BackingField_1() const { return ___U3CProxyU3Ek__BackingField_1; }
	inline ProxyConfig_t2693849256 ** get_address_of_U3CProxyU3Ek__BackingField_1() { return &___U3CProxyU3Ek__BackingField_1; }
	inline void set_U3CProxyU3Ek__BackingField_1(ProxyConfig_t2693849256 * value)
	{
		___U3CProxyU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CProxyU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CEndpointDefinitionU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(RootConfig_t4046630774, ___U3CEndpointDefinitionU3Ek__BackingField_2)); }
	inline String_t* get_U3CEndpointDefinitionU3Ek__BackingField_2() const { return ___U3CEndpointDefinitionU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CEndpointDefinitionU3Ek__BackingField_2() { return &___U3CEndpointDefinitionU3Ek__BackingField_2; }
	inline void set_U3CEndpointDefinitionU3Ek__BackingField_2(String_t* value)
	{
		___U3CEndpointDefinitionU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CEndpointDefinitionU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CRegionU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(RootConfig_t4046630774, ___U3CRegionU3Ek__BackingField_3)); }
	inline String_t* get_U3CRegionU3Ek__BackingField_3() const { return ___U3CRegionU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CRegionU3Ek__BackingField_3() { return &___U3CRegionU3Ek__BackingField_3; }
	inline void set_U3CRegionU3Ek__BackingField_3(String_t* value)
	{
		___U3CRegionU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CRegionU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3CProfileNameU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(RootConfig_t4046630774, ___U3CProfileNameU3Ek__BackingField_4)); }
	inline String_t* get_U3CProfileNameU3Ek__BackingField_4() const { return ___U3CProfileNameU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CProfileNameU3Ek__BackingField_4() { return &___U3CProfileNameU3Ek__BackingField_4; }
	inline void set_U3CProfileNameU3Ek__BackingField_4(String_t* value)
	{
		___U3CProfileNameU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CProfileNameU3Ek__BackingField_4, value);
	}

	inline static int32_t get_offset_of_U3CProfilesLocationU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(RootConfig_t4046630774, ___U3CProfilesLocationU3Ek__BackingField_5)); }
	inline String_t* get_U3CProfilesLocationU3Ek__BackingField_5() const { return ___U3CProfilesLocationU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CProfilesLocationU3Ek__BackingField_5() { return &___U3CProfilesLocationU3Ek__BackingField_5; }
	inline void set_U3CProfilesLocationU3Ek__BackingField_5(String_t* value)
	{
		___U3CProfilesLocationU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CProfilesLocationU3Ek__BackingField_5, value);
	}

	inline static int32_t get_offset_of_U3CUseSdkCacheU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(RootConfig_t4046630774, ___U3CUseSdkCacheU3Ek__BackingField_6)); }
	inline bool get_U3CUseSdkCacheU3Ek__BackingField_6() const { return ___U3CUseSdkCacheU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CUseSdkCacheU3Ek__BackingField_6() { return &___U3CUseSdkCacheU3Ek__BackingField_6; }
	inline void set_U3CUseSdkCacheU3Ek__BackingField_6(bool value)
	{
		___U3CUseSdkCacheU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CCorrectForClockSkewU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(RootConfig_t4046630774, ___U3CCorrectForClockSkewU3Ek__BackingField_7)); }
	inline bool get_U3CCorrectForClockSkewU3Ek__BackingField_7() const { return ___U3CCorrectForClockSkewU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CCorrectForClockSkewU3Ek__BackingField_7() { return &___U3CCorrectForClockSkewU3Ek__BackingField_7; }
	inline void set_U3CCorrectForClockSkewU3Ek__BackingField_7(bool value)
	{
		___U3CCorrectForClockSkewU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CApplicationNameU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(RootConfig_t4046630774, ___U3CApplicationNameU3Ek__BackingField_8)); }
	inline String_t* get_U3CApplicationNameU3Ek__BackingField_8() const { return ___U3CApplicationNameU3Ek__BackingField_8; }
	inline String_t** get_address_of_U3CApplicationNameU3Ek__BackingField_8() { return &___U3CApplicationNameU3Ek__BackingField_8; }
	inline void set_U3CApplicationNameU3Ek__BackingField_8(String_t* value)
	{
		___U3CApplicationNameU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CApplicationNameU3Ek__BackingField_8, value);
	}

	inline static int32_t get_offset_of_U3CServiceSectionsU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(RootConfig_t4046630774, ___U3CServiceSectionsU3Ek__BackingField_9)); }
	inline Il2CppObject* get_U3CServiceSectionsU3Ek__BackingField_9() const { return ___U3CServiceSectionsU3Ek__BackingField_9; }
	inline Il2CppObject** get_address_of_U3CServiceSectionsU3Ek__BackingField_9() { return &___U3CServiceSectionsU3Ek__BackingField_9; }
	inline void set_U3CServiceSectionsU3Ek__BackingField_9(Il2CppObject* value)
	{
		___U3CServiceSectionsU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CServiceSectionsU3Ek__BackingField_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
