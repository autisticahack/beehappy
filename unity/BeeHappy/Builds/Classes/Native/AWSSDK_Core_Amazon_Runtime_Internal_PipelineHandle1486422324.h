﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.Runtime.Internal.Util.ILogger
struct ILogger_t676853571;
// Amazon.Runtime.IPipelineHandler
struct IPipelineHandler_t2320756001;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.PipelineHandler
struct  PipelineHandler_t1486422324  : public Il2CppObject
{
public:
	// Amazon.Runtime.Internal.Util.ILogger Amazon.Runtime.Internal.PipelineHandler::<Logger>k__BackingField
	Il2CppObject * ___U3CLoggerU3Ek__BackingField_0;
	// Amazon.Runtime.IPipelineHandler Amazon.Runtime.Internal.PipelineHandler::<InnerHandler>k__BackingField
	Il2CppObject * ___U3CInnerHandlerU3Ek__BackingField_1;
	// Amazon.Runtime.IPipelineHandler Amazon.Runtime.Internal.PipelineHandler::<OuterHandler>k__BackingField
	Il2CppObject * ___U3COuterHandlerU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CLoggerU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(PipelineHandler_t1486422324, ___U3CLoggerU3Ek__BackingField_0)); }
	inline Il2CppObject * get_U3CLoggerU3Ek__BackingField_0() const { return ___U3CLoggerU3Ek__BackingField_0; }
	inline Il2CppObject ** get_address_of_U3CLoggerU3Ek__BackingField_0() { return &___U3CLoggerU3Ek__BackingField_0; }
	inline void set_U3CLoggerU3Ek__BackingField_0(Il2CppObject * value)
	{
		___U3CLoggerU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CLoggerU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CInnerHandlerU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PipelineHandler_t1486422324, ___U3CInnerHandlerU3Ek__BackingField_1)); }
	inline Il2CppObject * get_U3CInnerHandlerU3Ek__BackingField_1() const { return ___U3CInnerHandlerU3Ek__BackingField_1; }
	inline Il2CppObject ** get_address_of_U3CInnerHandlerU3Ek__BackingField_1() { return &___U3CInnerHandlerU3Ek__BackingField_1; }
	inline void set_U3CInnerHandlerU3Ek__BackingField_1(Il2CppObject * value)
	{
		___U3CInnerHandlerU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CInnerHandlerU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3COuterHandlerU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PipelineHandler_t1486422324, ___U3COuterHandlerU3Ek__BackingField_2)); }
	inline Il2CppObject * get_U3COuterHandlerU3Ek__BackingField_2() const { return ___U3COuterHandlerU3Ek__BackingField_2; }
	inline Il2CppObject ** get_address_of_U3COuterHandlerU3Ek__BackingField_2() { return &___U3COuterHandlerU3Ek__BackingField_2; }
	inline void set_U3COuterHandlerU3Ek__BackingField_2(Il2CppObject * value)
	{
		___U3COuterHandlerU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3COuterHandlerU3Ek__BackingField_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
