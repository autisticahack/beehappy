﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AWSSDK_CognitoSync_Amazon_CognitoSync_AmazonCognit2119632619.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.CognitoSync.Model.InvalidParameterException
struct  InvalidParameterException_t1425711985  : public AmazonCognitoSyncException_t2119632619
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
