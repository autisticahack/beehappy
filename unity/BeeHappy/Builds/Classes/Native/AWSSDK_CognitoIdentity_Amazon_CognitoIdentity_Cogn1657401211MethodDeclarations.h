﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.CognitoIdentity.CognitoAWSCredentials/IdentityChangedArgs
struct IdentityChangedArgs_t1657401211;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.String Amazon.CognitoIdentity.CognitoAWSCredentials/IdentityChangedArgs::get_OldIdentityId()
extern "C"  String_t* IdentityChangedArgs_get_OldIdentityId_m794934388 (IdentityChangedArgs_t1657401211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoIdentity.CognitoAWSCredentials/IdentityChangedArgs::set_OldIdentityId(System.String)
extern "C"  void IdentityChangedArgs_set_OldIdentityId_m4233207127 (IdentityChangedArgs_t1657401211 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.CognitoIdentity.CognitoAWSCredentials/IdentityChangedArgs::get_NewIdentityId()
extern "C"  String_t* IdentityChangedArgs_get_NewIdentityId_m79701011 (IdentityChangedArgs_t1657401211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoIdentity.CognitoAWSCredentials/IdentityChangedArgs::set_NewIdentityId(System.String)
extern "C"  void IdentityChangedArgs_set_NewIdentityId_m1416932598 (IdentityChangedArgs_t1657401211 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoIdentity.CognitoAWSCredentials/IdentityChangedArgs::.ctor(System.String,System.String)
extern "C"  void IdentityChangedArgs__ctor_m2632388964 (IdentityChangedArgs_t1657401211 * __this, String_t* ___oldIdentityId0, String_t* ___newIdentityId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
