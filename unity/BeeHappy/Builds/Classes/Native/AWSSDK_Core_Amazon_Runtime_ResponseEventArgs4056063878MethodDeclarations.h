﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.ResponseEventArgs
struct ResponseEventArgs_t4056063878;

#include "codegen/il2cpp-codegen.h"

// System.Void Amazon.Runtime.ResponseEventArgs::.ctor()
extern "C"  void ResponseEventArgs__ctor_m907517989 (ResponseEventArgs_t4056063878 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
