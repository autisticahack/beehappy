﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.RefreshingAWSCredentials/CredentialsRefreshState
struct CredentialsRefreshState_t3294867821;
// Amazon.Runtime.ImmutableCredentials
struct ImmutableCredentials_t282353664;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_Runtime_ImmutableCredentials282353664.h"
#include "mscorlib_System_DateTime693205669.h"

// Amazon.Runtime.ImmutableCredentials Amazon.Runtime.RefreshingAWSCredentials/CredentialsRefreshState::get_Credentials()
extern "C"  ImmutableCredentials_t282353664 * CredentialsRefreshState_get_Credentials_m3601129110 (CredentialsRefreshState_t3294867821 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.RefreshingAWSCredentials/CredentialsRefreshState::set_Credentials(Amazon.Runtime.ImmutableCredentials)
extern "C"  void CredentialsRefreshState_set_Credentials_m2239425685 (CredentialsRefreshState_t3294867821 * __this, ImmutableCredentials_t282353664 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime Amazon.Runtime.RefreshingAWSCredentials/CredentialsRefreshState::get_Expiration()
extern "C"  DateTime_t693205669  CredentialsRefreshState_get_Expiration_m2309339127 (CredentialsRefreshState_t3294867821 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.RefreshingAWSCredentials/CredentialsRefreshState::set_Expiration(System.DateTime)
extern "C"  void CredentialsRefreshState_set_Expiration_m1621602840 (CredentialsRefreshState_t3294867821 * __this, DateTime_t693205669  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.RefreshingAWSCredentials/CredentialsRefreshState::.ctor(Amazon.Runtime.ImmutableCredentials,System.DateTime)
extern "C"  void CredentialsRefreshState__ctor_m2847911208 (CredentialsRefreshState_t3294867821 * __this, ImmutableCredentials_t282353664 * ___credentials0, DateTime_t693205669  ___expiration1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
