﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Data.Sqlite.SQLiteCommitHandler
struct SQLiteCommitHandler_t2608225979;
// System.Object
struct Il2CppObject;
// Mono.Data.Sqlite.CommitEventArgs
struct CommitEventArgs_t134745060;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_CommitEventArgs134745060.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Mono.Data.Sqlite.SQLiteCommitHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void SQLiteCommitHandler__ctor_m2769072834 (SQLiteCommitHandler_t2608225979 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SQLiteCommitHandler::Invoke(System.Object,Mono.Data.Sqlite.CommitEventArgs)
extern "C"  void SQLiteCommitHandler_Invoke_m3398867721 (SQLiteCommitHandler_t2608225979 * __this, Il2CppObject * ___sender0, CommitEventArgs_t134745060 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Mono.Data.Sqlite.SQLiteCommitHandler::BeginInvoke(System.Object,Mono.Data.Sqlite.CommitEventArgs,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * SQLiteCommitHandler_BeginInvoke_m4009953254 (SQLiteCommitHandler_t2608225979 * __this, Il2CppObject * ___sender0, CommitEventArgs_t134745060 * ___e1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SQLiteCommitHandler::EndInvoke(System.IAsyncResult)
extern "C"  void SQLiteCommitHandler_EndInvoke_m115998092 (SQLiteCommitHandler_t2608225979 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
