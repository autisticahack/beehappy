﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<Amazon.Runtime.FallbackRegionFactory/RegionGenerator>
struct List_1_t2492863754;
// Amazon.RegionEndpoint
struct RegionEndpoint_t661522805;

#include "codegen/il2cpp-codegen.h"

// System.Void Amazon.Runtime.FallbackRegionFactory::.cctor()
extern "C"  void FallbackRegionFactory__cctor_m2157577090 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Amazon.Runtime.FallbackRegionFactory/RegionGenerator> Amazon.Runtime.FallbackRegionFactory::get_AllGenerators()
extern "C"  List_1_t2492863754 * FallbackRegionFactory_get_AllGenerators_m1160040000 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.FallbackRegionFactory::set_AllGenerators(System.Collections.Generic.List`1<Amazon.Runtime.FallbackRegionFactory/RegionGenerator>)
extern "C"  void FallbackRegionFactory_set_AllGenerators_m187265337 (Il2CppObject * __this /* static, unused */, List_1_t2492863754 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Amazon.Runtime.FallbackRegionFactory/RegionGenerator> Amazon.Runtime.FallbackRegionFactory::get_NonMetadataGenerators()
extern "C"  List_1_t2492863754 * FallbackRegionFactory_get_NonMetadataGenerators_m86620809 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.FallbackRegionFactory::set_NonMetadataGenerators(System.Collections.Generic.List`1<Amazon.Runtime.FallbackRegionFactory/RegionGenerator>)
extern "C"  void FallbackRegionFactory_set_NonMetadataGenerators_m3321318560 (Il2CppObject * __this /* static, unused */, List_1_t2492863754 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.FallbackRegionFactory::Reset()
extern "C"  void FallbackRegionFactory_Reset_m4135799384 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.RegionEndpoint Amazon.Runtime.FallbackRegionFactory::GetRegionEndpoint(System.Boolean)
extern "C"  RegionEndpoint_t661522805 * FallbackRegionFactory_GetRegionEndpoint_m3879892175 (Il2CppObject * __this /* static, unused */, bool ___includeInstanceMetadata0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
