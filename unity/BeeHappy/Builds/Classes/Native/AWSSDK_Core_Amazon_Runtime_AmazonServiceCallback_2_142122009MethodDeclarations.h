﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_Runtime_AmazonServiceCallback_22110732071MethodDeclarations.h"

// System.Void Amazon.Runtime.AmazonServiceCallback`2<Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest,Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityResponse>::.ctor(System.Object,System.IntPtr)
#define AmazonServiceCallback_2__ctor_m3392561651(__this, ___object0, ___method1, method) ((  void (*) (AmazonServiceCallback_2_t142122009 *, Il2CppObject *, IntPtr_t, const MethodInfo*))AmazonServiceCallback_2__ctor_m2776589747_gshared)(__this, ___object0, ___method1, method)
// System.Void Amazon.Runtime.AmazonServiceCallback`2<Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest,Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityResponse>::Invoke(Amazon.Runtime.AmazonServiceResult`2<TRequest,TResponse>)
#define AmazonServiceCallback_2_Invoke_m1778014778(__this, ___responseObject0, method) ((  void (*) (AmazonServiceCallback_2_t142122009 *, AmazonServiceResult_2_t3957156559 *, const MethodInfo*))AmazonServiceCallback_2_Invoke_m40441109_gshared)(__this, ___responseObject0, method)
// System.IAsyncResult Amazon.Runtime.AmazonServiceCallback`2<Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest,Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityResponse>::BeginInvoke(Amazon.Runtime.AmazonServiceResult`2<TRequest,TResponse>,System.AsyncCallback,System.Object)
#define AmazonServiceCallback_2_BeginInvoke_m1689658832(__this, ___responseObject0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (AmazonServiceCallback_2_t142122009 *, AmazonServiceResult_2_t3957156559 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))AmazonServiceCallback_2_BeginInvoke_m2934541552_gshared)(__this, ___responseObject0, ___callback1, ___object2, method)
// System.Void Amazon.Runtime.AmazonServiceCallback`2<Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest,Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityResponse>::EndInvoke(System.IAsyncResult)
#define AmazonServiceCallback_2_EndInvoke_m626888323(__this, ___result0, method) ((  void (*) (AmazonServiceCallback_2_t142122009 *, Il2CppObject *, const MethodInfo*))AmazonServiceCallback_2_EndInvoke_m1628064041_gshared)(__this, ___result0, method)
