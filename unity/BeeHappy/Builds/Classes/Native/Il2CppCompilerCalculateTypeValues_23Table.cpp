﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_Runtime_RetryPolicy1476739446.h"
#include "AWSSDK_Core_Amazon_Runtime_AsyncOptions558351272.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_CapacityManage2181902141.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_RetryCapacity2794668894.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_DefaultRequest3080757440.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_DefaultRequest_U12014549.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_ErrorResponse3502566035.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_StreamReadTrac1958363340.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_RequestContext2912551040.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_AsyncRequestCo3705567694.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_ResponseContex3850805926.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_AsyncResponseCo210983900.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_ExecutionConte2943675185.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_AsyncExecution3009477291.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_HttpErrorRespo3191555406.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_PipelineHandle1486422324.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_RuntimePipelin3355992556.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_ErrorHandler1573534074.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_HttpErrorRespo3899688054.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_CallbackHandler174745619.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_CredentialsRetr208022274.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_EndpointResolv2369326703.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_ErrorCallbackHa394665619.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Marshaller3371889241.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_MetricsHandler1107617423.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Signer1810841758.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Unmarshaller1994457618.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_UnityWebRequest752669660.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_UnityRequest4218620704.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_UnityWwwReques3206525961.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_UnityWwwRequest118609659.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_SimpleAsyncRes4203640901.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_DefaultRetryPo2207644155.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_RetryHandler3257318146.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_RuntimeAsyncRe4279356013.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_ThreadPoolExec2707466110.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_UnityMainThrea4098072663.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_UnityMainThread142100588.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_UnityMainThrea3085820275.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_UnityRequestQu3684366031.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_UnityWebReques1542496577.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_DownloadHandle1800693841.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_UploadHandlerR3528989734.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Util_CachingWra380166576.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Util_ChunkedUpl779979984.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Util_ChunkedUp4036775975.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Util_Backgroun1722929158.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Util_Background464705533.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Util_Extension2019041272.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Util_Hashing180418764.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Util_Hashing_U3244221195.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Util_HashStrea2318308192.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Util_InternalC4018195642.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Util_InternalCo527408354.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Util_Logger2262497814.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Util_InternalL2972373343.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Util_InternalL1777422022.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Util_InternalL3882482337.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Util_LogMessag4004523899.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Util_RequestMet218029284.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Util_RequestMet963174459.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Util_Timing847194262.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Util_TimingEven181853090.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Util_MetricErro964444806.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Util_PartialWr1744080210.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Util_StringUti1089313136.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Util_WrapperStr816765491.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Util_UnityDebu3216514868.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Util_UnityDebu2078152869.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_Erro3810099389.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_Mars3045611074.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_Json4063314926.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_Json4149926919.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_JsonU456235889.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_JsonU805090354.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_Resp3934041557.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_XmlRe760346204.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_Json2685947887.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2300 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2301 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2302 = { sizeof (RetryPolicy_t1476739446), -1, sizeof(RetryPolicy_t1476739446_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2302[4] = 
{
	RetryPolicy_t1476739446::get_offset_of_U3CMaxRetriesU3Ek__BackingField_0(),
	RetryPolicy_t1476739446::get_offset_of_U3CLoggerU3Ek__BackingField_1(),
	RetryPolicy_t1476739446_StaticFields::get_offset_of_clockSkewErrorCodes_2(),
	RetryPolicy_t1476739446_StaticFields::get_offset_of_clockSkewMaxThreshold_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2303 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2304 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2304[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2305 = { sizeof (AsyncOptions_t558351272), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2305[2] = 
{
	AsyncOptions_t558351272::get_offset_of_U3CExecuteCallbackOnMainThreadU3Ek__BackingField_0(),
	AsyncOptions_t558351272::get_offset_of_U3CStateU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2306 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2307 = { sizeof (CapacityManager_t2181902141), -1, sizeof(CapacityManager_t2181902141_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2307[6] = 
{
	CapacityManager_t2181902141::get_offset_of__disposed_0(),
	CapacityManager_t2181902141_StaticFields::get_offset_of__serviceUrlToCapacityMap_1(),
	CapacityManager_t2181902141::get_offset_of__rwlock_2(),
	CapacityManager_t2181902141::get_offset_of_THROTTLE_RETRY_REQUEST_COST_3(),
	CapacityManager_t2181902141::get_offset_of_THROTTLED_RETRIES_4(),
	CapacityManager_t2181902141::get_offset_of_THROTTLE_REQUEST_COST_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2308 = { sizeof (RetryCapacity_t2794668894), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2308[2] = 
{
	RetryCapacity_t2794668894::get_offset_of__maxCapacity_0(),
	RetryCapacity_t2794668894::get_offset_of_U3CAvailableCapacityU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2309 = { sizeof (DefaultRequest_t3080757440), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2309[21] = 
{
	DefaultRequest_t3080757440::get_offset_of_parameters_0(),
	DefaultRequest_t3080757440::get_offset_of_headers_1(),
	DefaultRequest_t3080757440::get_offset_of_subResources_2(),
	DefaultRequest_t3080757440::get_offset_of_endpoint_3(),
	DefaultRequest_t3080757440::get_offset_of_resourcePath_4(),
	DefaultRequest_t3080757440::get_offset_of_serviceName_5(),
	DefaultRequest_t3080757440::get_offset_of_originalRequest_6(),
	DefaultRequest_t3080757440::get_offset_of_content_7(),
	DefaultRequest_t3080757440::get_offset_of_contentStream_8(),
	DefaultRequest_t3080757440::get_offset_of_contentStreamHash_9(),
	DefaultRequest_t3080757440::get_offset_of_httpMethod_10(),
	DefaultRequest_t3080757440::get_offset_of_useQueryString_11(),
	DefaultRequest_t3080757440::get_offset_of_requestName_12(),
	DefaultRequest_t3080757440::get_offset_of_alternateRegion_13(),
	DefaultRequest_t3080757440::get_offset_of_originalStreamLength_14(),
	DefaultRequest_t3080757440::get_offset_of_U3CSetContentFromParametersU3Ek__BackingField_15(),
	DefaultRequest_t3080757440::get_offset_of_U3CSuppress404ExceptionsU3Ek__BackingField_16(),
	DefaultRequest_t3080757440::get_offset_of_U3CAWS4SignerResultU3Ek__BackingField_17(),
	DefaultRequest_t3080757440::get_offset_of_U3CUseChunkEncodingU3Ek__BackingField_18(),
	DefaultRequest_t3080757440::get_offset_of_U3CUseSigV4U3Ek__BackingField_19(),
	DefaultRequest_t3080757440::get_offset_of_U3CAuthenticationRegionU3Ek__BackingField_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2310 = { sizeof (U3CU3Ec_t12014549), -1, sizeof(U3CU3Ec_t12014549_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2310[2] = 
{
	U3CU3Ec_t12014549_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t12014549_StaticFields::get_offset_of_U3CU3E9__57_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2311 = { sizeof (ErrorResponse_t3502566035), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2311[4] = 
{
	ErrorResponse_t3502566035::get_offset_of_type_0(),
	ErrorResponse_t3502566035::get_offset_of_code_1(),
	ErrorResponse_t3502566035::get_offset_of_message_2(),
	ErrorResponse_t3502566035::get_offset_of_requestId_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2312 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2313 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2314 = { sizeof (StreamReadTracker_t1958363340), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2314[6] = 
{
	StreamReadTracker_t1958363340::get_offset_of_sender_0(),
	StreamReadTracker_t1958363340::get_offset_of_callback_1(),
	StreamReadTracker_t1958363340::get_offset_of_contentLength_2(),
	StreamReadTracker_t1958363340::get_offset_of_totalBytesRead_3(),
	StreamReadTracker_t1958363340::get_offset_of_totalIncrementTransferred_4(),
	StreamReadTracker_t1958363340::get_offset_of_progressUpdateInterval_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2315 = { sizeof (RequestContext_t2912551040), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2315[11] = 
{
	RequestContext_t2912551040::get_offset_of_U3CRequestU3Ek__BackingField_0(),
	RequestContext_t2912551040::get_offset_of_U3CMetricsU3Ek__BackingField_1(),
	RequestContext_t2912551040::get_offset_of_U3CSignerU3Ek__BackingField_2(),
	RequestContext_t2912551040::get_offset_of_U3CClientConfigU3Ek__BackingField_3(),
	RequestContext_t2912551040::get_offset_of_U3CRetriesU3Ek__BackingField_4(),
	RequestContext_t2912551040::get_offset_of_U3CIsSignedU3Ek__BackingField_5(),
	RequestContext_t2912551040::get_offset_of_U3CIsAsyncU3Ek__BackingField_6(),
	RequestContext_t2912551040::get_offset_of_U3COriginalRequestU3Ek__BackingField_7(),
	RequestContext_t2912551040::get_offset_of_U3CMarshallerU3Ek__BackingField_8(),
	RequestContext_t2912551040::get_offset_of_U3CUnmarshallerU3Ek__BackingField_9(),
	RequestContext_t2912551040::get_offset_of_U3CImmutableCredentialsU3Ek__BackingField_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2316 = { sizeof (AsyncRequestContext_t3705567694), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2316[4] = 
{
	AsyncRequestContext_t3705567694::get_offset_of_U3CCallbackU3Ek__BackingField_11(),
	AsyncRequestContext_t3705567694::get_offset_of_U3CStateU3Ek__BackingField_12(),
	AsyncRequestContext_t3705567694::get_offset_of_U3CAsyncOptionsU3Ek__BackingField_13(),
	AsyncRequestContext_t3705567694::get_offset_of_U3CActionU3Ek__BackingField_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2317 = { sizeof (ResponseContext_t3850805926), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2317[2] = 
{
	ResponseContext_t3850805926::get_offset_of_U3CResponseU3Ek__BackingField_0(),
	ResponseContext_t3850805926::get_offset_of_U3CHttpResponseU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2318 = { sizeof (AsyncResponseContext_t210983900), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2318[1] = 
{
	AsyncResponseContext_t210983900::get_offset_of_U3CAsyncResultU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2319 = { sizeof (ExecutionContext_t2943675185), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2319[2] = 
{
	ExecutionContext_t2943675185::get_offset_of_U3CRequestContextU3Ek__BackingField_0(),
	ExecutionContext_t2943675185::get_offset_of_U3CResponseContextU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2320 = { sizeof (AsyncExecutionContext_t3009477291), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2320[3] = 
{
	AsyncExecutionContext_t3009477291::get_offset_of_U3CResponseContextU3Ek__BackingField_0(),
	AsyncExecutionContext_t3009477291::get_offset_of_U3CRequestContextU3Ek__BackingField_1(),
	AsyncExecutionContext_t3009477291::get_offset_of_U3CRuntimeStateU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2321 = { sizeof (HttpErrorResponseException_t3191555406), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2321[1] = 
{
	HttpErrorResponseException_t3191555406::get_offset_of_U3CResponseU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2322 = { sizeof (PipelineHandler_t1486422324), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2322[3] = 
{
	PipelineHandler_t1486422324::get_offset_of_U3CLoggerU3Ek__BackingField_0(),
	PipelineHandler_t1486422324::get_offset_of_U3CInnerHandlerU3Ek__BackingField_1(),
	PipelineHandler_t1486422324::get_offset_of_U3COuterHandlerU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2323 = { sizeof (RuntimePipeline_t3355992556), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2323[3] = 
{
	RuntimePipeline_t3355992556::get_offset_of__disposed_0(),
	RuntimePipeline_t3355992556::get_offset_of__logger_1(),
	RuntimePipeline_t3355992556::get_offset_of__handler_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2324 = { sizeof (ErrorHandler_t1573534074), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2324[1] = 
{
	ErrorHandler_t1573534074::get_offset_of__exceptionHandlers_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2325 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2325[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2326 = { sizeof (HttpErrorResponseExceptionHandler_t3899688054), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2327 = { sizeof (CallbackHandler_t174745619), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2327[2] = 
{
	CallbackHandler_t174745619::get_offset_of_U3COnPreInvokeU3Ek__BackingField_3(),
	CallbackHandler_t174745619::get_offset_of_U3COnPostInvokeU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2328 = { sizeof (CredentialsRetriever_t208022274), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2328[1] = 
{
	CredentialsRetriever_t208022274::get_offset_of_U3CCredentialsU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2329 = { sizeof (EndpointResolver_t2369326703), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2330 = { sizeof (ErrorCallbackHandler_t394665619), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2330[1] = 
{
	ErrorCallbackHandler_t394665619::get_offset_of_U3COnErrorU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2331 = { sizeof (Marshaller_t3371889241), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2332 = { sizeof (MetricsHandler_t1107617423), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2333 = { sizeof (Signer_t1810841758), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2334 = { sizeof (Unmarshaller_t1994457618), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2334[1] = 
{
	Unmarshaller_t1994457618::get_offset_of__supportsResponseLogging_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2335 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2335[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2336 = { sizeof (UnityWebRequestFactory_t752669660), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2336[1] = 
{
	UnityWebRequestFactory_t752669660::get_offset_of__unityRequest_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2337 = { sizeof (UnityRequest_t4218620704), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2337[13] = 
{
	UnityRequest_t4218620704::get_offset_of_U3CRequestUriU3Ek__BackingField_0(),
	UnityRequest_t4218620704::get_offset_of_U3CWwwRequestU3Ek__BackingField_1(),
	UnityRequest_t4218620704::get_offset_of_U3CRequestContentU3Ek__BackingField_2(),
	UnityRequest_t4218620704::get_offset_of_U3CHeadersU3Ek__BackingField_3(),
	UnityRequest_t4218620704::get_offset_of_U3CCallbackU3Ek__BackingField_4(),
	UnityRequest_t4218620704::get_offset_of_U3CAsyncResultU3Ek__BackingField_5(),
	UnityRequest_t4218620704::get_offset_of_U3CWaitHandleU3Ek__BackingField_6(),
	UnityRequest_t4218620704::get_offset_of_U3CIsSyncU3Ek__BackingField_7(),
	UnityRequest_t4218620704::get_offset_of_U3CResponseU3Ek__BackingField_8(),
	UnityRequest_t4218620704::get_offset_of_U3CExceptionU3Ek__BackingField_9(),
	UnityRequest_t4218620704::get_offset_of_U3CMethodU3Ek__BackingField_10(),
	UnityRequest_t4218620704::get_offset_of__disposed_11(),
	UnityRequest_t4218620704::get_offset_of_U3CTrackerU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2338 = { sizeof (UnityWwwRequestFactory_t3206525961), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2338[1] = 
{
	UnityWwwRequestFactory_t3206525961::get_offset_of__unityWwwRequest_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2339 = { sizeof (UnityWwwRequest_t118609659), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2339[13] = 
{
	UnityWwwRequest_t118609659::get_offset_of_U3CRequestUriU3Ek__BackingField_0(),
	UnityWwwRequest_t118609659::get_offset_of_U3CWwwRequestU3Ek__BackingField_1(),
	UnityWwwRequest_t118609659::get_offset_of_U3CRequestContentU3Ek__BackingField_2(),
	UnityWwwRequest_t118609659::get_offset_of_U3CHeadersU3Ek__BackingField_3(),
	UnityWwwRequest_t118609659::get_offset_of_U3CCallbackU3Ek__BackingField_4(),
	UnityWwwRequest_t118609659::get_offset_of_U3CAsyncResultU3Ek__BackingField_5(),
	UnityWwwRequest_t118609659::get_offset_of_U3CWaitHandleU3Ek__BackingField_6(),
	UnityWwwRequest_t118609659::get_offset_of_U3CIsSyncU3Ek__BackingField_7(),
	UnityWwwRequest_t118609659::get_offset_of_U3CResponseU3Ek__BackingField_8(),
	UnityWwwRequest_t118609659::get_offset_of_U3CExceptionU3Ek__BackingField_9(),
	UnityWwwRequest_t118609659::get_offset_of_U3CMethodU3Ek__BackingField_10(),
	UnityWwwRequest_t118609659::get_offset_of_U3CTrackerU3Ek__BackingField_11(),
	UnityWwwRequest_t118609659::get_offset_of__disposed_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2340 = { sizeof (SimpleAsyncResult_t4203640901), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2340[1] = 
{
	SimpleAsyncResult_t4203640901::get_offset_of_U3CAsyncStateU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2341 = { sizeof (DefaultRetryPolicy_t2207644155), -1, sizeof(DefaultRetryPolicy_t2207644155_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2341[7] = 
{
	DefaultRetryPolicy_t2207644155_StaticFields::get_offset_of__capacityManagerInstance_4(),
	DefaultRetryPolicy_t2207644155::get_offset_of__maxBackoffInMilliseconds_5(),
	DefaultRetryPolicy_t2207644155::get_offset_of__retryCapacity_6(),
	DefaultRetryPolicy_t2207644155::get_offset_of__httpStatusCodesToRetryOn_7(),
	DefaultRetryPolicy_t2207644155::get_offset_of__webExceptionStatusesToRetryOn_8(),
	DefaultRetryPolicy_t2207644155_StaticFields::get_offset_of__coreCLRRetryErrorMessages_9(),
	DefaultRetryPolicy_t2207644155::get_offset_of__errorCodesToRetryOn_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2342 = { sizeof (RetryHandler_t3257318146), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2342[2] = 
{
	RetryHandler_t3257318146::get_offset_of__logger_3(),
	RetryHandler_t3257318146::get_offset_of_U3CRetryPolicyU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2343 = { sizeof (RuntimeAsyncResult_t4279356013), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2343[14] = 
{
	RuntimeAsyncResult_t4279356013::get_offset_of__lockObj_0(),
	RuntimeAsyncResult_t4279356013::get_offset_of__waitHandle_1(),
	RuntimeAsyncResult_t4279356013::get_offset_of__disposed_2(),
	RuntimeAsyncResult_t4279356013::get_offset_of__callbackInvoked_3(),
	RuntimeAsyncResult_t4279356013::get_offset_of__logger_4(),
	RuntimeAsyncResult_t4279356013::get_offset_of_U3CAsyncCallbackU3Ek__BackingField_5(),
	RuntimeAsyncResult_t4279356013::get_offset_of_U3CAsyncStateU3Ek__BackingField_6(),
	RuntimeAsyncResult_t4279356013::get_offset_of_U3CCompletedSynchronouslyU3Ek__BackingField_7(),
	RuntimeAsyncResult_t4279356013::get_offset_of_U3CIsCompletedU3Ek__BackingField_8(),
	RuntimeAsyncResult_t4279356013::get_offset_of_U3CExceptionU3Ek__BackingField_9(),
	RuntimeAsyncResult_t4279356013::get_offset_of_U3CResponseU3Ek__BackingField_10(),
	RuntimeAsyncResult_t4279356013::get_offset_of_U3CRequestU3Ek__BackingField_11(),
	RuntimeAsyncResult_t4279356013::get_offset_of_U3CActionU3Ek__BackingField_12(),
	RuntimeAsyncResult_t4279356013::get_offset_of_U3CAsyncOptionsU3Ek__BackingField_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2344 = { sizeof (ThreadPoolExecutionHandler_t2707466110), -1, sizeof(ThreadPoolExecutionHandler_t2707466110_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2344[2] = 
{
	ThreadPoolExecutionHandler_t2707466110_StaticFields::get_offset_of__throttler_3(),
	ThreadPoolExecutionHandler_t2707466110_StaticFields::get_offset_of__lock_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2345 = { sizeof (UnityMainThreadDispatcher_t4098072663), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2345[4] = 
{
	UnityMainThreadDispatcher_t4098072663::get_offset_of__logger_2(),
	UnityMainThreadDispatcher_t4098072663::get_offset_of__nextUpdateTime_3(),
	UnityMainThreadDispatcher_t4098072663::get_offset_of__updateInterval_4(),
	UnityMainThreadDispatcher_t4098072663::get_offset_of__currentNetworkStatus_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2346 = { sizeof (U3CU3Ec__DisplayClass7_0_t142100588), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2346[2] = 
{
	U3CU3Ec__DisplayClass7_0_t142100588::get_offset_of_request_0(),
	U3CU3Ec__DisplayClass7_0_t142100588::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2347 = { sizeof (U3CInvokeRequestU3Ed__7_t3085820275), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2347[11] = 
{
	U3CInvokeRequestU3Ed__7_t3085820275::get_offset_of_U3CU3E1__state_0(),
	U3CInvokeRequestU3Ed__7_t3085820275::get_offset_of_U3CU3E2__current_1(),
	U3CInvokeRequestU3Ed__7_t3085820275::get_offset_of_U3CU3E4__this_2(),
	U3CInvokeRequestU3Ed__7_t3085820275::get_offset_of_request_3(),
	U3CInvokeRequestU3Ed__7_t3085820275::get_offset_of_U3CwwwRequestU3E5__1_4(),
	U3CInvokeRequestU3Ed__7_t3085820275::get_offset_of_U3CuploadCompletedU3E5__2_5(),
	U3CInvokeRequestU3Ed__7_t3085820275::get_offset_of_U3CU3E8__3_6(),
	U3CInvokeRequestU3Ed__7_t3085820275::get_offset_of_U3CunityRequestU3E5__4_7(),
	U3CInvokeRequestU3Ed__7_t3085820275::get_offset_of_U3CoperationU3E5__5_8(),
	U3CInvokeRequestU3Ed__7_t3085820275::get_offset_of_U3CuploadCompletedU3E5__6_9(),
	U3CInvokeRequestU3Ed__7_t3085820275::get_offset_of_U3CunityWebRequestU3E5__7_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2348 = { sizeof (UnityRequestQueue_t3684366031), -1, sizeof(UnityRequestQueue_t3684366031_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2348[7] = 
{
	UnityRequestQueue_t3684366031_StaticFields::get_offset_of__instance_0(),
	UnityRequestQueue_t3684366031_StaticFields::get_offset_of__requestsLock_1(),
	UnityRequestQueue_t3684366031_StaticFields::get_offset_of__callbacksLock_2(),
	UnityRequestQueue_t3684366031_StaticFields::get_offset_of__mainThreadCallbackLock_3(),
	UnityRequestQueue_t3684366031::get_offset_of__requests_4(),
	UnityRequestQueue_t3684366031::get_offset_of__callbacks_5(),
	UnityRequestQueue_t3684366031::get_offset_of__mainThreadCallbacks_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2349 = { sizeof (UnityWebRequestWrapper_t1542496577), -1, sizeof(UnityWebRequestWrapper_t1542496577_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2349[19] = 
{
	UnityWebRequestWrapper_t1542496577_StaticFields::get_offset_of_unityWebRequestType_0(),
	UnityWebRequestWrapper_t1542496577_StaticFields::get_offset_of_unityWebRequestProperties_1(),
	UnityWebRequestWrapper_t1542496577_StaticFields::get_offset_of_unityWebRequestMethods_2(),
	UnityWebRequestWrapper_t1542496577_StaticFields::get_offset_of_setRequestHeaderMethod_3(),
	UnityWebRequestWrapper_t1542496577_StaticFields::get_offset_of_sendMethod_4(),
	UnityWebRequestWrapper_t1542496577_StaticFields::get_offset_of_getResponseHeadersMethod_5(),
	UnityWebRequestWrapper_t1542496577_StaticFields::get_offset_of_isDoneGetMethod_6(),
	UnityWebRequestWrapper_t1542496577_StaticFields::get_offset_of_downloadProgressGetMethod_7(),
	UnityWebRequestWrapper_t1542496577_StaticFields::get_offset_of_uploadProgressGetMethod_8(),
	UnityWebRequestWrapper_t1542496577_StaticFields::get_offset_of_isErrorGetMethod_9(),
	UnityWebRequestWrapper_t1542496577_StaticFields::get_offset_of_downloadedBytesGetMethod_10(),
	UnityWebRequestWrapper_t1542496577_StaticFields::get_offset_of_responseCodeGetMethod_11(),
	UnityWebRequestWrapper_t1542496577_StaticFields::get_offset_of_downloadHandlerSetMethod_12(),
	UnityWebRequestWrapper_t1542496577_StaticFields::get_offset_of_uploadHandlerSetMethod_13(),
	UnityWebRequestWrapper_t1542496577_StaticFields::get_offset_of_errorGetMethod_14(),
	UnityWebRequestWrapper_t1542496577::get_offset_of_unityWebRequestInstance_15(),
	UnityWebRequestWrapper_t1542496577::get_offset_of_downloadHandler_16(),
	UnityWebRequestWrapper_t1542496577::get_offset_of_uploadHandler_17(),
	UnityWebRequestWrapper_t1542496577::get_offset_of_disposedValue_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2350 = { sizeof (DownloadHandlerBufferWrapper_t1800693841), -1, sizeof(DownloadHandlerBufferWrapper_t1800693841_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2350[7] = 
{
	DownloadHandlerBufferWrapper_t1800693841_StaticFields::get_offset_of_downloadHandlerBufferType_0(),
	DownloadHandlerBufferWrapper_t1800693841_StaticFields::get_offset_of_downloadHandlerBufferProperties_1(),
	DownloadHandlerBufferWrapper_t1800693841_StaticFields::get_offset_of_downloadHandlerBufferMethods_2(),
	DownloadHandlerBufferWrapper_t1800693841_StaticFields::get_offset_of_dataProperty_3(),
	DownloadHandlerBufferWrapper_t1800693841_StaticFields::get_offset_of_dataGetMethod_4(),
	DownloadHandlerBufferWrapper_t1800693841::get_offset_of_U3CInstanceU3Ek__BackingField_5(),
	DownloadHandlerBufferWrapper_t1800693841::get_offset_of_disposedValue_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2351 = { sizeof (UploadHandlerRawWrapper_t3528989734), -1, sizeof(UploadHandlerRawWrapper_t3528989734_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2351[3] = 
{
	UploadHandlerRawWrapper_t3528989734_StaticFields::get_offset_of_uploadHandlerRawType_0(),
	UploadHandlerRawWrapper_t3528989734::get_offset_of_U3CInstanceU3Ek__BackingField_1(),
	UploadHandlerRawWrapper_t3528989734::get_offset_of_disposedValue_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2352 = { sizeof (CachingWrapperStream_t380166576), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2352[3] = 
{
	CachingWrapperStream_t380166576::get_offset_of__cacheLimit_2(),
	CachingWrapperStream_t380166576::get_offset_of__cachedBytes_3(),
	CachingWrapperStream_t380166576::get_offset_of_U3CAllReadBytesU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2353 = { sizeof (ChunkedUploadWrapperStream_t779979984), -1, sizeof(ChunkedUploadWrapperStream_t779979984_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2353[11] = 
{
	ChunkedUploadWrapperStream_t779979984_StaticFields::get_offset_of_DefaultChunkSize_2(),
	ChunkedUploadWrapperStream_t779979984::get_offset_of__inputBuffer_3(),
	ChunkedUploadWrapperStream_t779979984::get_offset_of__outputBuffer_4(),
	ChunkedUploadWrapperStream_t779979984::get_offset_of__outputBufferPos_5(),
	ChunkedUploadWrapperStream_t779979984::get_offset_of__outputBufferDataLen_6(),
	ChunkedUploadWrapperStream_t779979984::get_offset_of__wrappedStreamBufferSize_7(),
	ChunkedUploadWrapperStream_t779979984::get_offset_of__wrappedStreamConsumed_8(),
	ChunkedUploadWrapperStream_t779979984::get_offset_of__outputBufferIsTerminatingChunk_9(),
	ChunkedUploadWrapperStream_t779979984::get_offset_of__readStrategy_10(),
	ChunkedUploadWrapperStream_t779979984::get_offset_of_U3CHeaderSigningResultU3Ek__BackingField_11(),
	ChunkedUploadWrapperStream_t779979984::get_offset_of_U3CPreviousChunkSignatureU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2354 = { sizeof (ReadStrategy_t4036775975)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2354[3] = 
{
	ReadStrategy_t4036775975::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2355 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2356 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2356[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2357 = { sizeof (BackgroundInvoker_t1722929158), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2358 = { sizeof (U3CU3Ec_t464705533), -1, sizeof(U3CU3Ec_t464705533_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2358[2] = 
{
	U3CU3Ec_t464705533_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t464705533_StaticFields::get_offset_of_U3CU3E9__0_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2359 = { sizeof (Extensions_t2019041272), -1, sizeof(Extensions_t2019041272_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2359[2] = 
{
	Extensions_t2019041272_StaticFields::get_offset_of_ticksPerSecond_0(),
	Extensions_t2019041272_StaticFields::get_offset_of_tickFrequency_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2360 = { sizeof (Hashing_t180418764), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2361 = { sizeof (U3CU3Ec_t3244221195), -1, sizeof(U3CU3Ec_t3244221195_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2361[2] = 
{
	U3CU3Ec_t3244221195_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t3244221195_StaticFields::get_offset_of_U3CU3E9__0_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2362 = { sizeof (HashStream_t2318308192), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2362[5] = 
{
	HashStream_t2318308192::get_offset_of_U3CAlgorithmU3Ek__BackingField_2(),
	HashStream_t2318308192::get_offset_of_U3CCurrentPositionU3Ek__BackingField_3(),
	HashStream_t2318308192::get_offset_of_U3CCalculatedHashU3Ek__BackingField_4(),
	HashStream_t2318308192::get_offset_of_U3CExpectedHashU3Ek__BackingField_5(),
	HashStream_t2318308192::get_offset_of_U3CExpectedLengthU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2363 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2364 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2365 = { sizeof (InternalConsoleLogger_t4018195642), -1, sizeof(InternalConsoleLogger_t4018195642_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2365[1] = 
{
	InternalConsoleLogger_t4018195642_StaticFields::get_offset_of__sequanceId_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2366 = { sizeof (LogLevel_t527408354)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2366[7] = 
{
	LogLevel_t527408354::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2367 = { sizeof (Logger_t2262497814), -1, sizeof(Logger_t2262497814_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2367[3] = 
{
	Logger_t2262497814_StaticFields::get_offset_of_cachedLoggers_0(),
	Logger_t2262497814::get_offset_of_loggers_1(),
	Logger_t2262497814_StaticFields::get_offset_of_emptyLogger_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2368 = { sizeof (InternalLogger_t2972373343), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2368[2] = 
{
	InternalLogger_t2972373343::get_offset_of_U3CDeclaringTypeU3Ek__BackingField_0(),
	InternalLogger_t2972373343::get_offset_of_U3CIsEnabledU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2369 = { sizeof (InternalLog4netLogger_t1777422022), -1, sizeof(InternalLog4netLogger_t1777422022_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2369[20] = 
{
	InternalLog4netLogger_t1777422022_StaticFields::get_offset_of_loadState_2(),
	InternalLog4netLogger_t1777422022_StaticFields::get_offset_of_LOCK_3(),
	InternalLog4netLogger_t1777422022_StaticFields::get_offset_of_logMangerType_4(),
	InternalLog4netLogger_t1777422022_StaticFields::get_offset_of_logMangerTypeInfo_5(),
	InternalLog4netLogger_t1777422022_StaticFields::get_offset_of_getLoggerWithTypeMethod_6(),
	InternalLog4netLogger_t1777422022_StaticFields::get_offset_of_logType_7(),
	InternalLog4netLogger_t1777422022_StaticFields::get_offset_of_logTypeInfo_8(),
	InternalLog4netLogger_t1777422022_StaticFields::get_offset_of_logMethod_9(),
	InternalLog4netLogger_t1777422022_StaticFields::get_offset_of_levelType_10(),
	InternalLog4netLogger_t1777422022_StaticFields::get_offset_of_levelTypeInfo_11(),
	InternalLog4netLogger_t1777422022_StaticFields::get_offset_of_debugLevelPropertyValue_12(),
	InternalLog4netLogger_t1777422022_StaticFields::get_offset_of_infoLevelPropertyValue_13(),
	InternalLog4netLogger_t1777422022_StaticFields::get_offset_of_errorLevelPropertyValue_14(),
	InternalLog4netLogger_t1777422022_StaticFields::get_offset_of_isEnabledForMethod_15(),
	InternalLog4netLogger_t1777422022_StaticFields::get_offset_of_systemStringFormatType_16(),
	InternalLog4netLogger_t1777422022_StaticFields::get_offset_of_loggerType_17(),
	InternalLog4netLogger_t1777422022::get_offset_of_internalLogger_18(),
	InternalLog4netLogger_t1777422022::get_offset_of_isErrorEnabled_19(),
	InternalLog4netLogger_t1777422022::get_offset_of_isDebugEnabled_20(),
	InternalLog4netLogger_t1777422022::get_offset_of_isInfoEnabled_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2370 = { sizeof (LoadState_t3882482337)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2370[5] = 
{
	LoadState_t3882482337::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2371 = { sizeof (LogMessage_t4004523899), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2371[3] = 
{
	LogMessage_t4004523899::get_offset_of_U3CArgsU3Ek__BackingField_0(),
	LogMessage_t4004523899::get_offset_of_U3CProviderU3Ek__BackingField_1(),
	LogMessage_t4004523899::get_offset_of_U3CFormatU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2372 = { sizeof (RequestMetrics_t218029284), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2372[8] = 
{
	RequestMetrics_t218029284::get_offset_of_metricsLock_0(),
	RequestMetrics_t218029284::get_offset_of_stopWatch_1(),
	RequestMetrics_t218029284::get_offset_of_inFlightTimings_2(),
	RequestMetrics_t218029284::get_offset_of_errors_3(),
	RequestMetrics_t218029284::get_offset_of_U3CPropertiesU3Ek__BackingField_4(),
	RequestMetrics_t218029284::get_offset_of_U3CTimingsU3Ek__BackingField_5(),
	RequestMetrics_t218029284::get_offset_of_U3CCountersU3Ek__BackingField_6(),
	RequestMetrics_t218029284::get_offset_of_U3CIsEnabledU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2373 = { sizeof (U3CU3Ec_t963174459), -1, sizeof(U3CU3Ec_t963174459_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2373[2] = 
{
	U3CU3Ec_t963174459_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t963174459_StaticFields::get_offset_of_U3CU3E9__33_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2374 = { sizeof (Timing_t847194262), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2374[3] = 
{
	Timing_t847194262::get_offset_of_startTime_0(),
	Timing_t847194262::get_offset_of_endTime_1(),
	Timing_t847194262::get_offset_of_U3CIsFinishedU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2375 = { sizeof (TimingEvent_t181853090), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2375[3] = 
{
	TimingEvent_t181853090::get_offset_of_metric_0(),
	TimingEvent_t181853090::get_offset_of_metrics_1(),
	TimingEvent_t181853090::get_offset_of_disposed_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2376 = { sizeof (MetricError_t964444806), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2376[4] = 
{
	MetricError_t964444806::get_offset_of_U3CMetricU3Ek__BackingField_0(),
	MetricError_t964444806::get_offset_of_U3CMessageU3Ek__BackingField_1(),
	MetricError_t964444806::get_offset_of_U3CExceptionU3Ek__BackingField_2(),
	MetricError_t964444806::get_offset_of_U3CTimeU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2377 = { sizeof (PartialWrapperStream_t1744080210), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2377[2] = 
{
	PartialWrapperStream_t1744080210::get_offset_of_initialPosition_2(),
	PartialWrapperStream_t1744080210::get_offset_of_partSize_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2378 = { sizeof (StringUtils_t1089313136), -1, sizeof(StringUtils_t1089313136_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2378[1] = 
{
	StringUtils_t1089313136_StaticFields::get_offset_of_UTF_8_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2379 = { sizeof (WrapperStream_t816765491), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2379[1] = 
{
	WrapperStream_t816765491::get_offset_of_U3CBaseStreamU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2380 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2380[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2381 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2381[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2382 = { sizeof (UnityDebugLogger_t3216514868), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2383 = { sizeof (UnityDebugTraceListener_t2078152869), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2384 = { sizeof (ErrorResponseUnmarshaller_t3810099389), -1, sizeof(ErrorResponseUnmarshaller_t3810099389_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2384[1] = 
{
	ErrorResponseUnmarshaller_t3810099389_StaticFields::get_offset_of_instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2385 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2386 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2387 = { sizeof (MarshallerContext_t3045611074), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2387[1] = 
{
	MarshallerContext_t3045611074::get_offset_of_U3CRequestU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2388 = { sizeof (JsonMarshallerContext_t4063314926), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2388[1] = 
{
	JsonMarshallerContext_t4063314926::get_offset_of_U3CWriterU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2389 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2390 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2391 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2392 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2393 = { sizeof (JsonErrorResponseUnmarshaller_t4149926919), -1, sizeof(JsonErrorResponseUnmarshaller_t4149926919_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2393[1] = 
{
	JsonErrorResponseUnmarshaller_t4149926919_StaticFields::get_offset_of_instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2394 = { sizeof (JsonUnmarshallerContext_t456235889), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2394[7] = 
{
	JsonUnmarshallerContext_t456235889::get_offset_of_streamReader_6(),
	JsonUnmarshallerContext_t456235889::get_offset_of_jsonReader_7(),
	JsonUnmarshallerContext_t456235889::get_offset_of_stack_8(),
	JsonUnmarshallerContext_t456235889::get_offset_of_currentField_9(),
	JsonUnmarshallerContext_t456235889::get_offset_of_currentToken_10(),
	JsonUnmarshallerContext_t456235889::get_offset_of_disposed_11(),
	JsonUnmarshallerContext_t456235889::get_offset_of_wasPeeked_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2395 = { sizeof (JsonPathStack_t805090354), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2395[4] = 
{
	JsonPathStack_t805090354::get_offset_of_stack_0(),
	JsonPathStack_t805090354::get_offset_of_currentDepth_1(),
	JsonPathStack_t805090354::get_offset_of_stackStringBuilder_2(),
	JsonPathStack_t805090354::get_offset_of_stackString_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2396 = { sizeof (ResponseUnmarshaller_t3934041557), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2397 = { sizeof (XmlResponseUnmarshaller_t760346204), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2398 = { sizeof (JsonResponseUnmarshaller_t2685947887), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2399 = { 0, 0, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
