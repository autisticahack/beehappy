﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.List`1<Amazon.CognitoSync.Model.RecordPatch>
struct List_1_t3761994043;

#include "AWSSDK_CognitoSync_Amazon_CognitoSync_AmazonCognit2140463921.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.CognitoSync.Model.UpdateRecordsRequest
struct  UpdateRecordsRequest_t197801344  : public AmazonCognitoSyncRequest_t2140463921
{
public:
	// System.String Amazon.CognitoSync.Model.UpdateRecordsRequest::_clientContext
	String_t* ____clientContext_3;
	// System.String Amazon.CognitoSync.Model.UpdateRecordsRequest::_datasetName
	String_t* ____datasetName_4;
	// System.String Amazon.CognitoSync.Model.UpdateRecordsRequest::_deviceId
	String_t* ____deviceId_5;
	// System.String Amazon.CognitoSync.Model.UpdateRecordsRequest::_identityId
	String_t* ____identityId_6;
	// System.String Amazon.CognitoSync.Model.UpdateRecordsRequest::_identityPoolId
	String_t* ____identityPoolId_7;
	// System.Collections.Generic.List`1<Amazon.CognitoSync.Model.RecordPatch> Amazon.CognitoSync.Model.UpdateRecordsRequest::_recordPatches
	List_1_t3761994043 * ____recordPatches_8;
	// System.String Amazon.CognitoSync.Model.UpdateRecordsRequest::_syncSessionToken
	String_t* ____syncSessionToken_9;

public:
	inline static int32_t get_offset_of__clientContext_3() { return static_cast<int32_t>(offsetof(UpdateRecordsRequest_t197801344, ____clientContext_3)); }
	inline String_t* get__clientContext_3() const { return ____clientContext_3; }
	inline String_t** get_address_of__clientContext_3() { return &____clientContext_3; }
	inline void set__clientContext_3(String_t* value)
	{
		____clientContext_3 = value;
		Il2CppCodeGenWriteBarrier(&____clientContext_3, value);
	}

	inline static int32_t get_offset_of__datasetName_4() { return static_cast<int32_t>(offsetof(UpdateRecordsRequest_t197801344, ____datasetName_4)); }
	inline String_t* get__datasetName_4() const { return ____datasetName_4; }
	inline String_t** get_address_of__datasetName_4() { return &____datasetName_4; }
	inline void set__datasetName_4(String_t* value)
	{
		____datasetName_4 = value;
		Il2CppCodeGenWriteBarrier(&____datasetName_4, value);
	}

	inline static int32_t get_offset_of__deviceId_5() { return static_cast<int32_t>(offsetof(UpdateRecordsRequest_t197801344, ____deviceId_5)); }
	inline String_t* get__deviceId_5() const { return ____deviceId_5; }
	inline String_t** get_address_of__deviceId_5() { return &____deviceId_5; }
	inline void set__deviceId_5(String_t* value)
	{
		____deviceId_5 = value;
		Il2CppCodeGenWriteBarrier(&____deviceId_5, value);
	}

	inline static int32_t get_offset_of__identityId_6() { return static_cast<int32_t>(offsetof(UpdateRecordsRequest_t197801344, ____identityId_6)); }
	inline String_t* get__identityId_6() const { return ____identityId_6; }
	inline String_t** get_address_of__identityId_6() { return &____identityId_6; }
	inline void set__identityId_6(String_t* value)
	{
		____identityId_6 = value;
		Il2CppCodeGenWriteBarrier(&____identityId_6, value);
	}

	inline static int32_t get_offset_of__identityPoolId_7() { return static_cast<int32_t>(offsetof(UpdateRecordsRequest_t197801344, ____identityPoolId_7)); }
	inline String_t* get__identityPoolId_7() const { return ____identityPoolId_7; }
	inline String_t** get_address_of__identityPoolId_7() { return &____identityPoolId_7; }
	inline void set__identityPoolId_7(String_t* value)
	{
		____identityPoolId_7 = value;
		Il2CppCodeGenWriteBarrier(&____identityPoolId_7, value);
	}

	inline static int32_t get_offset_of__recordPatches_8() { return static_cast<int32_t>(offsetof(UpdateRecordsRequest_t197801344, ____recordPatches_8)); }
	inline List_1_t3761994043 * get__recordPatches_8() const { return ____recordPatches_8; }
	inline List_1_t3761994043 ** get_address_of__recordPatches_8() { return &____recordPatches_8; }
	inline void set__recordPatches_8(List_1_t3761994043 * value)
	{
		____recordPatches_8 = value;
		Il2CppCodeGenWriteBarrier(&____recordPatches_8, value);
	}

	inline static int32_t get_offset_of__syncSessionToken_9() { return static_cast<int32_t>(offsetof(UpdateRecordsRequest_t197801344, ____syncSessionToken_9)); }
	inline String_t* get__syncSessionToken_9() const { return ____syncSessionToken_9; }
	inline String_t** get_address_of__syncSessionToken_9() { return &____syncSessionToken_9; }
	inline void set__syncSessionToken_9(String_t* value)
	{
		____syncSessionToken_9 = value;
		Il2CppCodeGenWriteBarrier(&____syncSessionToken_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
