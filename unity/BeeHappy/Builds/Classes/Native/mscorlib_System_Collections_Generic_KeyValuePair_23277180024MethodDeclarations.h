﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23277180024.h"
#include "mscorlib_System_Object2689449295.h"
#include "AWSSDK_Core_Amazon_Util_Internal_PlatformServices_1632807378.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3044907881_gshared (KeyValuePair_2_t3277180024 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m3044907881(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3277180024 *, Il2CppObject *, int32_t, const MethodInfo*))KeyValuePair_2__ctor_m3044907881_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m4122353311_gshared (KeyValuePair_2_t3277180024 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m4122353311(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t3277180024 *, const MethodInfo*))KeyValuePair_2_get_Key_m4122353311_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m696936972_gshared (KeyValuePair_2_t3277180024 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m696936972(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3277180024 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Key_m696936972_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m116637759_gshared (KeyValuePair_2_t3277180024 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m116637759(__this, method) ((  int32_t (*) (KeyValuePair_2_t3277180024 *, const MethodInfo*))KeyValuePair_2_get_Value_m116637759_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m590249628_gshared (KeyValuePair_2_t3277180024 * __this, int32_t ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m590249628(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3277180024 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Value_m590249628_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m2693555248_gshared (KeyValuePair_2_t3277180024 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m2693555248(__this, method) ((  String_t* (*) (KeyValuePair_2_t3277180024 *, const MethodInfo*))KeyValuePair_2_ToString_m2693555248_gshared)(__this, method)
