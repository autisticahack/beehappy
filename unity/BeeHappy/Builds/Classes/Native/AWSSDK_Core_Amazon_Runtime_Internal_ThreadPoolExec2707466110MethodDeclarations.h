﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.ThreadPoolExecutionHandler
struct ThreadPoolExecutionHandler_t2707466110;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// Amazon.Runtime.IAsyncExecutionContext
struct IAsyncExecutionContext_t3792344986;
// System.Exception
struct Exception_t1927440687;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1927440687.h"

// System.Void Amazon.Runtime.Internal.ThreadPoolExecutionHandler::.ctor(System.Int32)
extern "C"  void ThreadPoolExecutionHandler__ctor_m3867576401 (ThreadPoolExecutionHandler_t2707466110 * __this, int32_t ___concurrentRequests0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Amazon.Runtime.Internal.ThreadPoolExecutionHandler::InvokeAsync(Amazon.Runtime.IAsyncExecutionContext)
extern "C"  Il2CppObject * ThreadPoolExecutionHandler_InvokeAsync_m610662284 (ThreadPoolExecutionHandler_t2707466110 * __this, Il2CppObject * ___executionContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.ThreadPoolExecutionHandler::InvokeAsyncHelper(Amazon.Runtime.IAsyncExecutionContext)
extern "C"  void ThreadPoolExecutionHandler_InvokeAsyncHelper_m974355402 (ThreadPoolExecutionHandler_t2707466110 * __this, Il2CppObject * ___executionContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.ThreadPoolExecutionHandler::ErrorCallback(System.Exception,Amazon.Runtime.IAsyncExecutionContext)
extern "C"  void ThreadPoolExecutionHandler_ErrorCallback_m494005841 (ThreadPoolExecutionHandler_t2707466110 * __this, Exception_t1927440687 * ___exception0, Il2CppObject * ___executionContext1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.ThreadPoolExecutionHandler::.cctor()
extern "C"  void ThreadPoolExecutionHandler__cctor_m4080287091 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
