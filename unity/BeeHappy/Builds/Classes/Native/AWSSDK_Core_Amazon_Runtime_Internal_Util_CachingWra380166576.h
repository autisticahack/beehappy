﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<System.Byte>
struct List_1_t3052225568;

#include "AWSSDK_Core_Amazon_Runtime_Internal_Util_WrapperStr816765491.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Util.CachingWrapperStream
struct  CachingWrapperStream_t380166576  : public WrapperStream_t816765491
{
public:
	// System.Int32 Amazon.Runtime.Internal.Util.CachingWrapperStream::_cacheLimit
	int32_t ____cacheLimit_2;
	// System.Int32 Amazon.Runtime.Internal.Util.CachingWrapperStream::_cachedBytes
	int32_t ____cachedBytes_3;
	// System.Collections.Generic.List`1<System.Byte> Amazon.Runtime.Internal.Util.CachingWrapperStream::<AllReadBytes>k__BackingField
	List_1_t3052225568 * ___U3CAllReadBytesU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of__cacheLimit_2() { return static_cast<int32_t>(offsetof(CachingWrapperStream_t380166576, ____cacheLimit_2)); }
	inline int32_t get__cacheLimit_2() const { return ____cacheLimit_2; }
	inline int32_t* get_address_of__cacheLimit_2() { return &____cacheLimit_2; }
	inline void set__cacheLimit_2(int32_t value)
	{
		____cacheLimit_2 = value;
	}

	inline static int32_t get_offset_of__cachedBytes_3() { return static_cast<int32_t>(offsetof(CachingWrapperStream_t380166576, ____cachedBytes_3)); }
	inline int32_t get__cachedBytes_3() const { return ____cachedBytes_3; }
	inline int32_t* get_address_of__cachedBytes_3() { return &____cachedBytes_3; }
	inline void set__cachedBytes_3(int32_t value)
	{
		____cachedBytes_3 = value;
	}

	inline static int32_t get_offset_of_U3CAllReadBytesU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(CachingWrapperStream_t380166576, ___U3CAllReadBytesU3Ek__BackingField_4)); }
	inline List_1_t3052225568 * get_U3CAllReadBytesU3Ek__BackingField_4() const { return ___U3CAllReadBytesU3Ek__BackingField_4; }
	inline List_1_t3052225568 ** get_address_of_U3CAllReadBytesU3Ek__BackingField_4() { return &___U3CAllReadBytesU3Ek__BackingField_4; }
	inline void set_U3CAllReadBytesU3Ek__BackingField_4(List_1_t3052225568 * value)
	{
		___U3CAllReadBytesU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CAllReadBytesU3Ek__BackingField_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
