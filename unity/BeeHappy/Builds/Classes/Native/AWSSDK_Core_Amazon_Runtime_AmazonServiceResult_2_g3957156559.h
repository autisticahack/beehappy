﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest
struct AssumeRoleWithWebIdentityRequest_t193094215;
// Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityResponse
struct AssumeRoleWithWebIdentityResponse_t3931705881;
// System.Exception
struct Exception_t1927440687;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.AmazonServiceResult`2<Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest,Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityResponse>
struct  AmazonServiceResult_2_t3957156559  : public Il2CppObject
{
public:
	// TRequest Amazon.Runtime.AmazonServiceResult`2::<Request>k__BackingField
	AssumeRoleWithWebIdentityRequest_t193094215 * ___U3CRequestU3Ek__BackingField_0;
	// TResponse Amazon.Runtime.AmazonServiceResult`2::<Response>k__BackingField
	AssumeRoleWithWebIdentityResponse_t3931705881 * ___U3CResponseU3Ek__BackingField_1;
	// System.Exception Amazon.Runtime.AmazonServiceResult`2::<Exception>k__BackingField
	Exception_t1927440687 * ___U3CExceptionU3Ek__BackingField_2;
	// System.Object Amazon.Runtime.AmazonServiceResult`2::<state>k__BackingField
	Il2CppObject * ___U3CstateU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CRequestU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AmazonServiceResult_2_t3957156559, ___U3CRequestU3Ek__BackingField_0)); }
	inline AssumeRoleWithWebIdentityRequest_t193094215 * get_U3CRequestU3Ek__BackingField_0() const { return ___U3CRequestU3Ek__BackingField_0; }
	inline AssumeRoleWithWebIdentityRequest_t193094215 ** get_address_of_U3CRequestU3Ek__BackingField_0() { return &___U3CRequestU3Ek__BackingField_0; }
	inline void set_U3CRequestU3Ek__BackingField_0(AssumeRoleWithWebIdentityRequest_t193094215 * value)
	{
		___U3CRequestU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CRequestU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CResponseU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AmazonServiceResult_2_t3957156559, ___U3CResponseU3Ek__BackingField_1)); }
	inline AssumeRoleWithWebIdentityResponse_t3931705881 * get_U3CResponseU3Ek__BackingField_1() const { return ___U3CResponseU3Ek__BackingField_1; }
	inline AssumeRoleWithWebIdentityResponse_t3931705881 ** get_address_of_U3CResponseU3Ek__BackingField_1() { return &___U3CResponseU3Ek__BackingField_1; }
	inline void set_U3CResponseU3Ek__BackingField_1(AssumeRoleWithWebIdentityResponse_t3931705881 * value)
	{
		___U3CResponseU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CResponseU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CExceptionU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AmazonServiceResult_2_t3957156559, ___U3CExceptionU3Ek__BackingField_2)); }
	inline Exception_t1927440687 * get_U3CExceptionU3Ek__BackingField_2() const { return ___U3CExceptionU3Ek__BackingField_2; }
	inline Exception_t1927440687 ** get_address_of_U3CExceptionU3Ek__BackingField_2() { return &___U3CExceptionU3Ek__BackingField_2; }
	inline void set_U3CExceptionU3Ek__BackingField_2(Exception_t1927440687 * value)
	{
		___U3CExceptionU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CExceptionU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CstateU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AmazonServiceResult_2_t3957156559, ___U3CstateU3Ek__BackingField_3)); }
	inline Il2CppObject * get_U3CstateU3Ek__BackingField_3() const { return ___U3CstateU3Ek__BackingField_3; }
	inline Il2CppObject ** get_address_of_U3CstateU3Ek__BackingField_3() { return &___U3CstateU3Ek__BackingField_3; }
	inline void set_U3CstateU3Ek__BackingField_3(Il2CppObject * value)
	{
		___U3CstateU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CstateU3Ek__BackingField_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
