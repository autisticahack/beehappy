﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_SortedDictionary3931121312MethodDeclarations.h"

// System.Void System.Collections.Generic.SortedDictionary`2<System.String,System.String>::.ctor()
#define SortedDictionary_2__ctor_m3035244098(__this, method) ((  void (*) (SortedDictionary_2_t1298644088 *, const MethodInfo*))SortedDictionary_2__ctor_m2646846914_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.String,System.String>::.ctor(System.Collections.Generic.IComparer`1<TKey>)
#define SortedDictionary_2__ctor_m807331720(__this, ___comparer0, method) ((  void (*) (SortedDictionary_2_t1298644088 *, Il2CppObject*, const MethodInfo*))SortedDictionary_2__ctor_m2297982677_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.String,System.String>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IComparer`1<TKey>)
#define SortedDictionary_2__ctor_m2658076141(__this, ___dic0, ___comparer1, method) ((  void (*) (SortedDictionary_2_t1298644088 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))SortedDictionary_2__ctor_m4170642563_gshared)(__this, ___dic0, ___comparer1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.SortedDictionary`2<System.String,System.String>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
#define SortedDictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m2693636252(__this, method) ((  Il2CppObject* (*) (SortedDictionary_2_t1298644088 *, const MethodInfo*))SortedDictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m3197491164_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.String,System.String>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m736919081(__this, ___item0, method) ((  void (*) (SortedDictionary_2_t1298644088 *, KeyValuePair_2_t1701344717 , const MethodInfo*))SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1627652201_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.String,System.String>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3620847047(__this, ___item0, method) ((  bool (*) (SortedDictionary_2_t1298644088 *, KeyValuePair_2_t1701344717 , const MethodInfo*))SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1375072327_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.String,System.String>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m4219920744(__this, method) ((  bool (*) (SortedDictionary_2_t1298644088 *, const MethodInfo*))SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1154929448_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.String,System.String>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2868995014(__this, ___item0, method) ((  bool (*) (SortedDictionary_2_t1298644088 *, KeyValuePair_2_t1701344717 , const MethodInfo*))SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1895620934_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.String,System.String>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define SortedDictionary_2_System_Collections_IDictionary_Add_m109703944(__this, ___key0, ___value1, method) ((  void (*) (SortedDictionary_2_t1298644088 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_Add_m1435283656_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.String,System.String>::System.Collections.IDictionary.Contains(System.Object)
#define SortedDictionary_2_System_Collections_IDictionary_Contains_m4088716476(__this, ___key0, method) ((  bool (*) (SortedDictionary_2_t1298644088 *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_Contains_m2099850172_gshared)(__this, ___key0, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.SortedDictionary`2<System.String,System.String>::System.Collections.IDictionary.GetEnumerator()
#define SortedDictionary_2_System_Collections_IDictionary_GetEnumerator_m2050499157(__this, method) ((  Il2CppObject * (*) (SortedDictionary_2_t1298644088 *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_GetEnumerator_m1167000725_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.SortedDictionary`2<System.String,System.String>::System.Collections.IDictionary.get_Keys()
#define SortedDictionary_2_System_Collections_IDictionary_get_Keys_m902615448(__this, method) ((  Il2CppObject * (*) (SortedDictionary_2_t1298644088 *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_get_Keys_m2424577816_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.String,System.String>::System.Collections.IDictionary.Remove(System.Object)
#define SortedDictionary_2_System_Collections_IDictionary_Remove_m3139921409(__this, ___key0, method) ((  void (*) (SortedDictionary_2_t1298644088 *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_Remove_m3973233665_gshared)(__this, ___key0, method)
// System.Collections.ICollection System.Collections.Generic.SortedDictionary`2<System.String,System.String>::System.Collections.IDictionary.get_Values()
#define SortedDictionary_2_System_Collections_IDictionary_get_Values_m1527396658(__this, method) ((  Il2CppObject * (*) (SortedDictionary_2_t1298644088 *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_get_Values_m4006702514_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedDictionary`2<System.String,System.String>::System.Collections.IDictionary.get_Item(System.Object)
#define SortedDictionary_2_System_Collections_IDictionary_get_Item_m2146345858(__this, ___key0, method) ((  Il2CppObject * (*) (SortedDictionary_2_t1298644088 *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_get_Item_m749784322_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.String,System.String>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define SortedDictionary_2_System_Collections_IDictionary_set_Item_m1079068709(__this, ___key0, ___value1, method) ((  void (*) (SortedDictionary_2_t1298644088 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_set_Item_m2214150629_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.String,System.String>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define SortedDictionary_2_System_Collections_ICollection_CopyTo_m937443070(__this, ___array0, ___index1, method) ((  void (*) (SortedDictionary_2_t1298644088 *, Il2CppArray *, int32_t, const MethodInfo*))SortedDictionary_2_System_Collections_ICollection_CopyTo_m2402063550_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.String,System.String>::System.Collections.ICollection.get_IsSynchronized()
#define SortedDictionary_2_System_Collections_ICollection_get_IsSynchronized_m1596729450(__this, method) ((  bool (*) (SortedDictionary_2_t1298644088 *, const MethodInfo*))SortedDictionary_2_System_Collections_ICollection_get_IsSynchronized_m1383457962_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedDictionary`2<System.String,System.String>::System.Collections.ICollection.get_SyncRoot()
#define SortedDictionary_2_System_Collections_ICollection_get_SyncRoot_m2133590334(__this, method) ((  Il2CppObject * (*) (SortedDictionary_2_t1298644088 *, const MethodInfo*))SortedDictionary_2_System_Collections_ICollection_get_SyncRoot_m2986761918_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.SortedDictionary`2<System.String,System.String>::System.Collections.IEnumerable.GetEnumerator()
#define SortedDictionary_2_System_Collections_IEnumerable_GetEnumerator_m2679205671(__this, method) ((  Il2CppObject * (*) (SortedDictionary_2_t1298644088 *, const MethodInfo*))SortedDictionary_2_System_Collections_IEnumerable_GetEnumerator_m50684391_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.SortedDictionary`2<System.String,System.String>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define SortedDictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m609944096(__this, method) ((  Il2CppObject* (*) (SortedDictionary_2_t1298644088 *, const MethodInfo*))SortedDictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1660205152_gshared)(__this, method)
// System.Int32 System.Collections.Generic.SortedDictionary`2<System.String,System.String>::get_Count()
#define SortedDictionary_2_get_Count_m980191874(__this, method) ((  int32_t (*) (SortedDictionary_2_t1298644088 *, const MethodInfo*))SortedDictionary_2_get_Count_m738745858_gshared)(__this, method)
// TValue System.Collections.Generic.SortedDictionary`2<System.String,System.String>::get_Item(TKey)
#define SortedDictionary_2_get_Item_m3193981509(__this, ___key0, method) ((  String_t* (*) (SortedDictionary_2_t1298644088 *, String_t*, const MethodInfo*))SortedDictionary_2_get_Item_m2496989701_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.String,System.String>::set_Item(TKey,TValue)
#define SortedDictionary_2_set_Item_m1948416508(__this, ___key0, ___value1, method) ((  void (*) (SortedDictionary_2_t1298644088 *, String_t*, String_t*, const MethodInfo*))SortedDictionary_2_set_Item_m2198693116_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.String,System.String>::Add(TKey,TValue)
#define SortedDictionary_2_Add_m802554599(__this, ___key0, ___value1, method) ((  void (*) (SortedDictionary_2_t1298644088 *, String_t*, String_t*, const MethodInfo*))SortedDictionary_2_Add_m3261466727_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.String,System.String>::Clear()
#define SortedDictionary_2_Clear_m1589219783(__this, method) ((  void (*) (SortedDictionary_2_t1298644088 *, const MethodInfo*))SortedDictionary_2_Clear_m2350355463_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.String,System.String>::ContainsKey(TKey)
#define SortedDictionary_2_ContainsKey_m62963701(__this, ___key0, method) ((  bool (*) (SortedDictionary_2_t1298644088 *, String_t*, const MethodInfo*))SortedDictionary_2_ContainsKey_m2379881333_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.String,System.String>::ContainsValue(TValue)
#define SortedDictionary_2_ContainsValue_m2630270797(__this, ___value0, method) ((  bool (*) (SortedDictionary_2_t1298644088 *, String_t*, const MethodInfo*))SortedDictionary_2_ContainsValue_m3842886989_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.String,System.String>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define SortedDictionary_2_CopyTo_m2485171048(__this, ___array0, ___arrayIndex1, method) ((  void (*) (SortedDictionary_2_t1298644088 *, KeyValuePair_2U5BU5D_t1360691296*, int32_t, const MethodInfo*))SortedDictionary_2_CopyTo_m2987881320_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.Generic.SortedDictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.SortedDictionary`2<System.String,System.String>::GetEnumerator()
#define SortedDictionary_2_GetEnumerator_m767988576(__this, method) ((  Enumerator_t2667205588  (*) (SortedDictionary_2_t1298644088 *, const MethodInfo*))SortedDictionary_2_GetEnumerator_m3792996474_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.String,System.String>::Remove(TKey)
#define SortedDictionary_2_Remove_m383796391(__this, ___key0, method) ((  bool (*) (SortedDictionary_2_t1298644088 *, String_t*, const MethodInfo*))SortedDictionary_2_Remove_m2920055847_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.String,System.String>::TryGetValue(TKey,TValue&)
#define SortedDictionary_2_TryGetValue_m1668157384(__this, ___key0, ___value1, method) ((  bool (*) (SortedDictionary_2_t1298644088 *, String_t*, String_t**, const MethodInfo*))SortedDictionary_2_TryGetValue_m3440732424_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.SortedDictionary`2<System.String,System.String>::ToKey(System.Object)
#define SortedDictionary_2_ToKey_m2076573398(__this, ___key0, method) ((  String_t* (*) (SortedDictionary_2_t1298644088 *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_ToKey_m874477334_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.SortedDictionary`2<System.String,System.String>::ToValue(System.Object)
#define SortedDictionary_2_ToValue_m888856854(__this, ___value0, method) ((  String_t* (*) (SortedDictionary_2_t1298644088 *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_ToValue_m1308966358_gshared)(__this, ___value0, method)
