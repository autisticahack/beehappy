﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.Runtime.ImmutableCredentials
struct ImmutableCredentials_t282353664;
// System.String
struct String_t;

#include "AWSSDK_Core_Amazon_Runtime_AWSCredentials3583921007.h"
#include "mscorlib_System_Nullable_1_gen3251239280.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.CognitoIdentity.Model.Credentials
struct  Credentials_t792472136  : public AWSCredentials_t3583921007
{
public:
	// Amazon.Runtime.ImmutableCredentials Amazon.CognitoIdentity.Model.Credentials::_credentials
	ImmutableCredentials_t282353664 * ____credentials_0;
	// System.String Amazon.CognitoIdentity.Model.Credentials::_accessKeyId
	String_t* ____accessKeyId_1;
	// System.Nullable`1<System.DateTime> Amazon.CognitoIdentity.Model.Credentials::_expiration
	Nullable_1_t3251239280  ____expiration_2;
	// System.String Amazon.CognitoIdentity.Model.Credentials::_secretKey
	String_t* ____secretKey_3;
	// System.String Amazon.CognitoIdentity.Model.Credentials::_sessionToken
	String_t* ____sessionToken_4;

public:
	inline static int32_t get_offset_of__credentials_0() { return static_cast<int32_t>(offsetof(Credentials_t792472136, ____credentials_0)); }
	inline ImmutableCredentials_t282353664 * get__credentials_0() const { return ____credentials_0; }
	inline ImmutableCredentials_t282353664 ** get_address_of__credentials_0() { return &____credentials_0; }
	inline void set__credentials_0(ImmutableCredentials_t282353664 * value)
	{
		____credentials_0 = value;
		Il2CppCodeGenWriteBarrier(&____credentials_0, value);
	}

	inline static int32_t get_offset_of__accessKeyId_1() { return static_cast<int32_t>(offsetof(Credentials_t792472136, ____accessKeyId_1)); }
	inline String_t* get__accessKeyId_1() const { return ____accessKeyId_1; }
	inline String_t** get_address_of__accessKeyId_1() { return &____accessKeyId_1; }
	inline void set__accessKeyId_1(String_t* value)
	{
		____accessKeyId_1 = value;
		Il2CppCodeGenWriteBarrier(&____accessKeyId_1, value);
	}

	inline static int32_t get_offset_of__expiration_2() { return static_cast<int32_t>(offsetof(Credentials_t792472136, ____expiration_2)); }
	inline Nullable_1_t3251239280  get__expiration_2() const { return ____expiration_2; }
	inline Nullable_1_t3251239280 * get_address_of__expiration_2() { return &____expiration_2; }
	inline void set__expiration_2(Nullable_1_t3251239280  value)
	{
		____expiration_2 = value;
	}

	inline static int32_t get_offset_of__secretKey_3() { return static_cast<int32_t>(offsetof(Credentials_t792472136, ____secretKey_3)); }
	inline String_t* get__secretKey_3() const { return ____secretKey_3; }
	inline String_t** get_address_of__secretKey_3() { return &____secretKey_3; }
	inline void set__secretKey_3(String_t* value)
	{
		____secretKey_3 = value;
		Il2CppCodeGenWriteBarrier(&____secretKey_3, value);
	}

	inline static int32_t get_offset_of__sessionToken_4() { return static_cast<int32_t>(offsetof(Credentials_t792472136, ____sessionToken_4)); }
	inline String_t* get__sessionToken_4() const { return ____sessionToken_4; }
	inline String_t** get_address_of__sessionToken_4() { return &____sessionToken_4; }
	inline void set__sessionToken_4(String_t* value)
	{
		____sessionToken_4 = value;
		Il2CppCodeGenWriteBarrier(&____sessionToken_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
