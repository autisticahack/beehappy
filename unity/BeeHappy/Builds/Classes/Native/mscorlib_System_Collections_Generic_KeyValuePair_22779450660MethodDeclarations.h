﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22779450660.h"
#include "mscorlib_System_Object2689449295.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_ArrayMetadata1135078014.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3859112953_gshared (KeyValuePair_2_t2779450660 * __this, Il2CppObject * ___key0, ArrayMetadata_t1135078014  ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m3859112953(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2779450660 *, Il2CppObject *, ArrayMetadata_t1135078014 , const MethodInfo*))KeyValuePair_2__ctor_m3859112953_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m1489805227_gshared (KeyValuePair_2_t2779450660 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m1489805227(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t2779450660 *, const MethodInfo*))KeyValuePair_2_get_Key_m1489805227_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1755966420_gshared (KeyValuePair_2_t2779450660 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m1755966420(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2779450660 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Key_m1755966420_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::get_Value()
extern "C"  ArrayMetadata_t1135078014  KeyValuePair_2_get_Value_m3869691915_gshared (KeyValuePair_2_t2779450660 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m3869691915(__this, method) ((  ArrayMetadata_t1135078014  (*) (KeyValuePair_2_t2779450660 *, const MethodInfo*))KeyValuePair_2_get_Value_m3869691915_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m3110329900_gshared (KeyValuePair_2_t2779450660 * __this, ArrayMetadata_t1135078014  ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m3110329900(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2779450660 *, ArrayMetadata_t1135078014 , const MethodInfo*))KeyValuePair_2_set_Value_m3110329900_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m2776738692_gshared (KeyValuePair_2_t2779450660 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m2776738692(__this, method) ((  String_t* (*) (KeyValuePair_2_t2779450660 *, const MethodInfo*))KeyValuePair_2_ToString_m2776738692_gshared)(__this, method)
