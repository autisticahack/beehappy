﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.CognitoSync.AmazonCognitoSyncRequest
struct AmazonCognitoSyncRequest_t2140463921;

#include "codegen/il2cpp-codegen.h"

// System.Void Amazon.CognitoSync.AmazonCognitoSyncRequest::.ctor()
extern "C"  void AmazonCognitoSyncRequest__ctor_m89249017 (AmazonCognitoSyncRequest_t2140463921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
