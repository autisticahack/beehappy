﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Byte[]
struct ByteU5BU5D_t3397334013;
// Amazon.Runtime.Internal.Auth.AWS4SigningResult
struct AWS4SigningResult_t430803065;
// System.String
struct String_t;

#include "AWSSDK_Core_Amazon_Runtime_Internal_Util_WrapperStr816765491.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Util_ChunkedUp4036775975.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Util.ChunkedUploadWrapperStream
struct  ChunkedUploadWrapperStream_t779979984  : public WrapperStream_t816765491
{
public:
	// System.Byte[] Amazon.Runtime.Internal.Util.ChunkedUploadWrapperStream::_inputBuffer
	ByteU5BU5D_t3397334013* ____inputBuffer_3;
	// System.Byte[] Amazon.Runtime.Internal.Util.ChunkedUploadWrapperStream::_outputBuffer
	ByteU5BU5D_t3397334013* ____outputBuffer_4;
	// System.Int32 Amazon.Runtime.Internal.Util.ChunkedUploadWrapperStream::_outputBufferPos
	int32_t ____outputBufferPos_5;
	// System.Int32 Amazon.Runtime.Internal.Util.ChunkedUploadWrapperStream::_outputBufferDataLen
	int32_t ____outputBufferDataLen_6;
	// System.Int32 Amazon.Runtime.Internal.Util.ChunkedUploadWrapperStream::_wrappedStreamBufferSize
	int32_t ____wrappedStreamBufferSize_7;
	// System.Boolean Amazon.Runtime.Internal.Util.ChunkedUploadWrapperStream::_wrappedStreamConsumed
	bool ____wrappedStreamConsumed_8;
	// System.Boolean Amazon.Runtime.Internal.Util.ChunkedUploadWrapperStream::_outputBufferIsTerminatingChunk
	bool ____outputBufferIsTerminatingChunk_9;
	// Amazon.Runtime.Internal.Util.ChunkedUploadWrapperStream/ReadStrategy Amazon.Runtime.Internal.Util.ChunkedUploadWrapperStream::_readStrategy
	int32_t ____readStrategy_10;
	// Amazon.Runtime.Internal.Auth.AWS4SigningResult Amazon.Runtime.Internal.Util.ChunkedUploadWrapperStream::<HeaderSigningResult>k__BackingField
	AWS4SigningResult_t430803065 * ___U3CHeaderSigningResultU3Ek__BackingField_11;
	// System.String Amazon.Runtime.Internal.Util.ChunkedUploadWrapperStream::<PreviousChunkSignature>k__BackingField
	String_t* ___U3CPreviousChunkSignatureU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of__inputBuffer_3() { return static_cast<int32_t>(offsetof(ChunkedUploadWrapperStream_t779979984, ____inputBuffer_3)); }
	inline ByteU5BU5D_t3397334013* get__inputBuffer_3() const { return ____inputBuffer_3; }
	inline ByteU5BU5D_t3397334013** get_address_of__inputBuffer_3() { return &____inputBuffer_3; }
	inline void set__inputBuffer_3(ByteU5BU5D_t3397334013* value)
	{
		____inputBuffer_3 = value;
		Il2CppCodeGenWriteBarrier(&____inputBuffer_3, value);
	}

	inline static int32_t get_offset_of__outputBuffer_4() { return static_cast<int32_t>(offsetof(ChunkedUploadWrapperStream_t779979984, ____outputBuffer_4)); }
	inline ByteU5BU5D_t3397334013* get__outputBuffer_4() const { return ____outputBuffer_4; }
	inline ByteU5BU5D_t3397334013** get_address_of__outputBuffer_4() { return &____outputBuffer_4; }
	inline void set__outputBuffer_4(ByteU5BU5D_t3397334013* value)
	{
		____outputBuffer_4 = value;
		Il2CppCodeGenWriteBarrier(&____outputBuffer_4, value);
	}

	inline static int32_t get_offset_of__outputBufferPos_5() { return static_cast<int32_t>(offsetof(ChunkedUploadWrapperStream_t779979984, ____outputBufferPos_5)); }
	inline int32_t get__outputBufferPos_5() const { return ____outputBufferPos_5; }
	inline int32_t* get_address_of__outputBufferPos_5() { return &____outputBufferPos_5; }
	inline void set__outputBufferPos_5(int32_t value)
	{
		____outputBufferPos_5 = value;
	}

	inline static int32_t get_offset_of__outputBufferDataLen_6() { return static_cast<int32_t>(offsetof(ChunkedUploadWrapperStream_t779979984, ____outputBufferDataLen_6)); }
	inline int32_t get__outputBufferDataLen_6() const { return ____outputBufferDataLen_6; }
	inline int32_t* get_address_of__outputBufferDataLen_6() { return &____outputBufferDataLen_6; }
	inline void set__outputBufferDataLen_6(int32_t value)
	{
		____outputBufferDataLen_6 = value;
	}

	inline static int32_t get_offset_of__wrappedStreamBufferSize_7() { return static_cast<int32_t>(offsetof(ChunkedUploadWrapperStream_t779979984, ____wrappedStreamBufferSize_7)); }
	inline int32_t get__wrappedStreamBufferSize_7() const { return ____wrappedStreamBufferSize_7; }
	inline int32_t* get_address_of__wrappedStreamBufferSize_7() { return &____wrappedStreamBufferSize_7; }
	inline void set__wrappedStreamBufferSize_7(int32_t value)
	{
		____wrappedStreamBufferSize_7 = value;
	}

	inline static int32_t get_offset_of__wrappedStreamConsumed_8() { return static_cast<int32_t>(offsetof(ChunkedUploadWrapperStream_t779979984, ____wrappedStreamConsumed_8)); }
	inline bool get__wrappedStreamConsumed_8() const { return ____wrappedStreamConsumed_8; }
	inline bool* get_address_of__wrappedStreamConsumed_8() { return &____wrappedStreamConsumed_8; }
	inline void set__wrappedStreamConsumed_8(bool value)
	{
		____wrappedStreamConsumed_8 = value;
	}

	inline static int32_t get_offset_of__outputBufferIsTerminatingChunk_9() { return static_cast<int32_t>(offsetof(ChunkedUploadWrapperStream_t779979984, ____outputBufferIsTerminatingChunk_9)); }
	inline bool get__outputBufferIsTerminatingChunk_9() const { return ____outputBufferIsTerminatingChunk_9; }
	inline bool* get_address_of__outputBufferIsTerminatingChunk_9() { return &____outputBufferIsTerminatingChunk_9; }
	inline void set__outputBufferIsTerminatingChunk_9(bool value)
	{
		____outputBufferIsTerminatingChunk_9 = value;
	}

	inline static int32_t get_offset_of__readStrategy_10() { return static_cast<int32_t>(offsetof(ChunkedUploadWrapperStream_t779979984, ____readStrategy_10)); }
	inline int32_t get__readStrategy_10() const { return ____readStrategy_10; }
	inline int32_t* get_address_of__readStrategy_10() { return &____readStrategy_10; }
	inline void set__readStrategy_10(int32_t value)
	{
		____readStrategy_10 = value;
	}

	inline static int32_t get_offset_of_U3CHeaderSigningResultU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(ChunkedUploadWrapperStream_t779979984, ___U3CHeaderSigningResultU3Ek__BackingField_11)); }
	inline AWS4SigningResult_t430803065 * get_U3CHeaderSigningResultU3Ek__BackingField_11() const { return ___U3CHeaderSigningResultU3Ek__BackingField_11; }
	inline AWS4SigningResult_t430803065 ** get_address_of_U3CHeaderSigningResultU3Ek__BackingField_11() { return &___U3CHeaderSigningResultU3Ek__BackingField_11; }
	inline void set_U3CHeaderSigningResultU3Ek__BackingField_11(AWS4SigningResult_t430803065 * value)
	{
		___U3CHeaderSigningResultU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CHeaderSigningResultU3Ek__BackingField_11, value);
	}

	inline static int32_t get_offset_of_U3CPreviousChunkSignatureU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(ChunkedUploadWrapperStream_t779979984, ___U3CPreviousChunkSignatureU3Ek__BackingField_12)); }
	inline String_t* get_U3CPreviousChunkSignatureU3Ek__BackingField_12() const { return ___U3CPreviousChunkSignatureU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CPreviousChunkSignatureU3Ek__BackingField_12() { return &___U3CPreviousChunkSignatureU3Ek__BackingField_12; }
	inline void set_U3CPreviousChunkSignatureU3Ek__BackingField_12(String_t* value)
	{
		___U3CPreviousChunkSignatureU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CPreviousChunkSignatureU3Ek__BackingField_12, value);
	}
};

struct ChunkedUploadWrapperStream_t779979984_StaticFields
{
public:
	// System.Int32 Amazon.Runtime.Internal.Util.ChunkedUploadWrapperStream::DefaultChunkSize
	int32_t ___DefaultChunkSize_2;

public:
	inline static int32_t get_offset_of_DefaultChunkSize_2() { return static_cast<int32_t>(offsetof(ChunkedUploadWrapperStream_t779979984_StaticFields, ___DefaultChunkSize_2)); }
	inline int32_t get_DefaultChunkSize_2() const { return ___DefaultChunkSize_2; }
	inline int32_t* get_address_of_DefaultChunkSize_2() { return &___DefaultChunkSize_2; }
	inline void set_DefaultChunkSize_2(int32_t value)
	{
		___DefaultChunkSize_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
