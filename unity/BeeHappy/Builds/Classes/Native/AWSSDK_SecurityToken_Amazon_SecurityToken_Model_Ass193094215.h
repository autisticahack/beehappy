﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AWSSDK_SecurityToken_Amazon_SecurityToken_AmazonSec265680841.h"
#include "mscorlib_System_Nullable_1_gen334943763.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest
struct  AssumeRoleWithWebIdentityRequest_t193094215  : public AmazonSecurityTokenServiceRequest_t265680841
{
public:
	// System.Nullable`1<System.Int32> Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest::_durationSeconds
	Nullable_1_t334943763  ____durationSeconds_3;
	// System.String Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest::_policy
	String_t* ____policy_4;
	// System.String Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest::_providerId
	String_t* ____providerId_5;
	// System.String Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest::_roleArn
	String_t* ____roleArn_6;
	// System.String Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest::_roleSessionName
	String_t* ____roleSessionName_7;
	// System.String Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest::_webIdentityToken
	String_t* ____webIdentityToken_8;

public:
	inline static int32_t get_offset_of__durationSeconds_3() { return static_cast<int32_t>(offsetof(AssumeRoleWithWebIdentityRequest_t193094215, ____durationSeconds_3)); }
	inline Nullable_1_t334943763  get__durationSeconds_3() const { return ____durationSeconds_3; }
	inline Nullable_1_t334943763 * get_address_of__durationSeconds_3() { return &____durationSeconds_3; }
	inline void set__durationSeconds_3(Nullable_1_t334943763  value)
	{
		____durationSeconds_3 = value;
	}

	inline static int32_t get_offset_of__policy_4() { return static_cast<int32_t>(offsetof(AssumeRoleWithWebIdentityRequest_t193094215, ____policy_4)); }
	inline String_t* get__policy_4() const { return ____policy_4; }
	inline String_t** get_address_of__policy_4() { return &____policy_4; }
	inline void set__policy_4(String_t* value)
	{
		____policy_4 = value;
		Il2CppCodeGenWriteBarrier(&____policy_4, value);
	}

	inline static int32_t get_offset_of__providerId_5() { return static_cast<int32_t>(offsetof(AssumeRoleWithWebIdentityRequest_t193094215, ____providerId_5)); }
	inline String_t* get__providerId_5() const { return ____providerId_5; }
	inline String_t** get_address_of__providerId_5() { return &____providerId_5; }
	inline void set__providerId_5(String_t* value)
	{
		____providerId_5 = value;
		Il2CppCodeGenWriteBarrier(&____providerId_5, value);
	}

	inline static int32_t get_offset_of__roleArn_6() { return static_cast<int32_t>(offsetof(AssumeRoleWithWebIdentityRequest_t193094215, ____roleArn_6)); }
	inline String_t* get__roleArn_6() const { return ____roleArn_6; }
	inline String_t** get_address_of__roleArn_6() { return &____roleArn_6; }
	inline void set__roleArn_6(String_t* value)
	{
		____roleArn_6 = value;
		Il2CppCodeGenWriteBarrier(&____roleArn_6, value);
	}

	inline static int32_t get_offset_of__roleSessionName_7() { return static_cast<int32_t>(offsetof(AssumeRoleWithWebIdentityRequest_t193094215, ____roleSessionName_7)); }
	inline String_t* get__roleSessionName_7() const { return ____roleSessionName_7; }
	inline String_t** get_address_of__roleSessionName_7() { return &____roleSessionName_7; }
	inline void set__roleSessionName_7(String_t* value)
	{
		____roleSessionName_7 = value;
		Il2CppCodeGenWriteBarrier(&____roleSessionName_7, value);
	}

	inline static int32_t get_offset_of__webIdentityToken_8() { return static_cast<int32_t>(offsetof(AssumeRoleWithWebIdentityRequest_t193094215, ____webIdentityToken_8)); }
	inline String_t* get__webIdentityToken_8() const { return ____webIdentityToken_8; }
	inline String_t** get_address_of__webIdentityToken_8() { return &____webIdentityToken_8; }
	inline void set__webIdentityToken_8(String_t* value)
	{
		____webIdentityToken_8 = value;
		Il2CppCodeGenWriteBarrier(&____webIdentityToken_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
