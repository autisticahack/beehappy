﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.AmazonServiceClient
struct AmazonServiceClient_t3583134838;
// Amazon.Runtime.Internal.RuntimePipeline
struct RuntimePipeline_t3355992556;
// Amazon.Runtime.AWSCredentials
struct AWSCredentials_t3583921007;
// Amazon.Runtime.IClientConfig
struct IClientConfig_t4078933728;
// Amazon.Runtime.ClientConfig
struct ClientConfig_t3664713661;
// Amazon.Runtime.Internal.Auth.AbstractAWSSigner
struct AbstractAWSSigner_t2114314031;
// Amazon.Runtime.IExecutionContext
struct IExecutionContext_t2477130752;
// System.Exception
struct Exception_t1927440687;
// System.Uri
struct Uri_t19570940;
// Amazon.Runtime.Internal.IRequest
struct IRequest_t2400804350;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_RuntimePipelin3355992556.h"
#include "AWSSDK_Core_Amazon_Runtime_AWSCredentials3583921007.h"
#include "AWSSDK_Core_Amazon_Runtime_ClientConfig3664713661.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Auth_AbstractA2114314031.h"
#include "mscorlib_System_Exception1927440687.h"
#include "System_System_Uri19570940.h"

// Amazon.Runtime.Internal.RuntimePipeline Amazon.Runtime.AmazonServiceClient::get_RuntimePipeline()
extern "C"  RuntimePipeline_t3355992556 * AmazonServiceClient_get_RuntimePipeline_m2901721168 (AmazonServiceClient_t3583134838 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.AmazonServiceClient::set_RuntimePipeline(Amazon.Runtime.Internal.RuntimePipeline)
extern "C"  void AmazonServiceClient_set_RuntimePipeline_m807613865 (AmazonServiceClient_t3583134838 * __this, RuntimePipeline_t3355992556 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.AWSCredentials Amazon.Runtime.AmazonServiceClient::get_Credentials()
extern "C"  AWSCredentials_t3583921007 * AmazonServiceClient_get_Credentials_m4184551956 (AmazonServiceClient_t3583134838 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.AmazonServiceClient::set_Credentials(Amazon.Runtime.AWSCredentials)
extern "C"  void AmazonServiceClient_set_Credentials_m1333368801 (AmazonServiceClient_t3583134838 * __this, AWSCredentials_t3583921007 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.IClientConfig Amazon.Runtime.AmazonServiceClient::get_Config()
extern "C"  Il2CppObject * AmazonServiceClient_get_Config_m1094903683 (AmazonServiceClient_t3583134838 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.AmazonServiceClient::set_Config(Amazon.Runtime.IClientConfig)
extern "C"  void AmazonServiceClient_set_Config_m925366602 (AmazonServiceClient_t3583134838 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.AmazonServiceClient::get_SupportResponseLogging()
extern "C"  bool AmazonServiceClient_get_SupportResponseLogging_m174157009 (AmazonServiceClient_t3583134838 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.AmazonServiceClient::.ctor(Amazon.Runtime.AWSCredentials,Amazon.Runtime.ClientConfig)
extern "C"  void AmazonServiceClient__ctor_m2824112809 (AmazonServiceClient_t3583134838 * __this, AWSCredentials_t3583921007 * ___credentials0, ClientConfig_t3664713661 * ___config1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.Internal.Auth.AbstractAWSSigner Amazon.Runtime.AmazonServiceClient::get_Signer()
extern "C"  AbstractAWSSigner_t2114314031 * AmazonServiceClient_get_Signer_m3156837961 (AmazonServiceClient_t3583134838 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.AmazonServiceClient::set_Signer(Amazon.Runtime.Internal.Auth.AbstractAWSSigner)
extern "C"  void AmazonServiceClient_set_Signer_m3054521448 (AmazonServiceClient_t3583134838 * __this, AbstractAWSSigner_t2114314031 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.AmazonServiceClient::Initialize()
extern "C"  void AmazonServiceClient_Initialize_m280327123 (AmazonServiceClient_t3583134838 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.AmazonServiceClient::ProcessPreRequestHandlers(Amazon.Runtime.IExecutionContext)
extern "C"  void AmazonServiceClient_ProcessPreRequestHandlers_m3675044555 (AmazonServiceClient_t3583134838 * __this, Il2CppObject * ___executionContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.AmazonServiceClient::ProcessRequestHandlers(Amazon.Runtime.IExecutionContext)
extern "C"  void AmazonServiceClient_ProcessRequestHandlers_m3683250926 (AmazonServiceClient_t3583134838 * __this, Il2CppObject * ___executionContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.AmazonServiceClient::ProcessResponseHandlers(Amazon.Runtime.IExecutionContext)
extern "C"  void AmazonServiceClient_ProcessResponseHandlers_m708987218 (AmazonServiceClient_t3583134838 * __this, Il2CppObject * ___executionContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.AmazonServiceClient::ProcessExceptionHandlers(Amazon.Runtime.IExecutionContext,System.Exception)
extern "C"  void AmazonServiceClient_ProcessExceptionHandlers_m84431086 (AmazonServiceClient_t3583134838 * __this, Il2CppObject * ___executionContext0, Exception_t1927440687 * ___exception1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.AmazonServiceClient::Dispose()
extern "C"  void AmazonServiceClient_Dispose_m1178128722 (AmazonServiceClient_t3583134838 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.AmazonServiceClient::Dispose(System.Boolean)
extern "C"  void AmazonServiceClient_Dispose_m3444168299 (AmazonServiceClient_t3583134838 * __this, bool ___disposing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.AmazonServiceClient::ThrowIfDisposed()
extern "C"  void AmazonServiceClient_ThrowIfDisposed_m2259003041 (AmazonServiceClient_t3583134838 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.AmazonServiceClient::CustomizeRuntimePipeline(Amazon.Runtime.Internal.RuntimePipeline)
extern "C"  void AmazonServiceClient_CustomizeRuntimePipeline_m1182256829 (AmazonServiceClient_t3583134838 * __this, RuntimePipeline_t3355992556 * ___pipeline0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.AmazonServiceClient::BuildRuntimePipeline()
extern "C"  void AmazonServiceClient_BuildRuntimePipeline_m1806874091 (AmazonServiceClient_t3583134838 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Uri Amazon.Runtime.AmazonServiceClient::ComposeUrl(Amazon.Runtime.Internal.IRequest)
extern "C"  Uri_t19570940 * AmazonServiceClient_ComposeUrl_m937508045 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___iRequest0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.AmazonServiceClient::DontUnescapePathDotsAndSlashes(System.Uri)
extern "C"  void AmazonServiceClient_DontUnescapePathDotsAndSlashes_m3320126854 (Il2CppObject * __this /* static, unused */, Uri_t19570940 * ___uri0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
