﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.String,Amazon.Runtime.Internal.RetryCapacity>
struct Dictionary_2_t414480860;
// System.Threading.ReaderWriterLockSlim
struct ReaderWriterLockSlim_t3961242320;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.CapacityManager
struct  CapacityManager_t2181902141  : public Il2CppObject
{
public:
	// System.Boolean Amazon.Runtime.Internal.CapacityManager::_disposed
	bool ____disposed_0;
	// System.Threading.ReaderWriterLockSlim Amazon.Runtime.Internal.CapacityManager::_rwlock
	ReaderWriterLockSlim_t3961242320 * ____rwlock_2;
	// System.Int32 Amazon.Runtime.Internal.CapacityManager::THROTTLE_RETRY_REQUEST_COST
	int32_t ___THROTTLE_RETRY_REQUEST_COST_3;
	// System.Int32 Amazon.Runtime.Internal.CapacityManager::THROTTLED_RETRIES
	int32_t ___THROTTLED_RETRIES_4;
	// System.Int32 Amazon.Runtime.Internal.CapacityManager::THROTTLE_REQUEST_COST
	int32_t ___THROTTLE_REQUEST_COST_5;

public:
	inline static int32_t get_offset_of__disposed_0() { return static_cast<int32_t>(offsetof(CapacityManager_t2181902141, ____disposed_0)); }
	inline bool get__disposed_0() const { return ____disposed_0; }
	inline bool* get_address_of__disposed_0() { return &____disposed_0; }
	inline void set__disposed_0(bool value)
	{
		____disposed_0 = value;
	}

	inline static int32_t get_offset_of__rwlock_2() { return static_cast<int32_t>(offsetof(CapacityManager_t2181902141, ____rwlock_2)); }
	inline ReaderWriterLockSlim_t3961242320 * get__rwlock_2() const { return ____rwlock_2; }
	inline ReaderWriterLockSlim_t3961242320 ** get_address_of__rwlock_2() { return &____rwlock_2; }
	inline void set__rwlock_2(ReaderWriterLockSlim_t3961242320 * value)
	{
		____rwlock_2 = value;
		Il2CppCodeGenWriteBarrier(&____rwlock_2, value);
	}

	inline static int32_t get_offset_of_THROTTLE_RETRY_REQUEST_COST_3() { return static_cast<int32_t>(offsetof(CapacityManager_t2181902141, ___THROTTLE_RETRY_REQUEST_COST_3)); }
	inline int32_t get_THROTTLE_RETRY_REQUEST_COST_3() const { return ___THROTTLE_RETRY_REQUEST_COST_3; }
	inline int32_t* get_address_of_THROTTLE_RETRY_REQUEST_COST_3() { return &___THROTTLE_RETRY_REQUEST_COST_3; }
	inline void set_THROTTLE_RETRY_REQUEST_COST_3(int32_t value)
	{
		___THROTTLE_RETRY_REQUEST_COST_3 = value;
	}

	inline static int32_t get_offset_of_THROTTLED_RETRIES_4() { return static_cast<int32_t>(offsetof(CapacityManager_t2181902141, ___THROTTLED_RETRIES_4)); }
	inline int32_t get_THROTTLED_RETRIES_4() const { return ___THROTTLED_RETRIES_4; }
	inline int32_t* get_address_of_THROTTLED_RETRIES_4() { return &___THROTTLED_RETRIES_4; }
	inline void set_THROTTLED_RETRIES_4(int32_t value)
	{
		___THROTTLED_RETRIES_4 = value;
	}

	inline static int32_t get_offset_of_THROTTLE_REQUEST_COST_5() { return static_cast<int32_t>(offsetof(CapacityManager_t2181902141, ___THROTTLE_REQUEST_COST_5)); }
	inline int32_t get_THROTTLE_REQUEST_COST_5() const { return ___THROTTLE_REQUEST_COST_5; }
	inline int32_t* get_address_of_THROTTLE_REQUEST_COST_5() { return &___THROTTLE_REQUEST_COST_5; }
	inline void set_THROTTLE_REQUEST_COST_5(int32_t value)
	{
		___THROTTLE_REQUEST_COST_5 = value;
	}
};

struct CapacityManager_t2181902141_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,Amazon.Runtime.Internal.RetryCapacity> Amazon.Runtime.Internal.CapacityManager::_serviceUrlToCapacityMap
	Dictionary_2_t414480860 * ____serviceUrlToCapacityMap_1;

public:
	inline static int32_t get_offset_of__serviceUrlToCapacityMap_1() { return static_cast<int32_t>(offsetof(CapacityManager_t2181902141_StaticFields, ____serviceUrlToCapacityMap_1)); }
	inline Dictionary_2_t414480860 * get__serviceUrlToCapacityMap_1() const { return ____serviceUrlToCapacityMap_1; }
	inline Dictionary_2_t414480860 ** get_address_of__serviceUrlToCapacityMap_1() { return &____serviceUrlToCapacityMap_1; }
	inline void set__serviceUrlToCapacityMap_1(Dictionary_2_t414480860 * value)
	{
		____serviceUrlToCapacityMap_1 = value;
		Il2CppCodeGenWriteBarrier(&____serviceUrlToCapacityMap_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
