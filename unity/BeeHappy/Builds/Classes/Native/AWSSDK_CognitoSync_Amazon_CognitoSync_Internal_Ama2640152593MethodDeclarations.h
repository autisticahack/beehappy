﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.CognitoSync.Internal.AmazonCognitoSyncPostSignHandler
struct AmazonCognitoSyncPostSignHandler_t2640152593;
// Amazon.Runtime.IExecutionContext
struct IExecutionContext_t2477130752;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// Amazon.Runtime.IAsyncExecutionContext
struct IAsyncExecutionContext_t3792344986;

#include "codegen/il2cpp-codegen.h"

// System.Void Amazon.CognitoSync.Internal.AmazonCognitoSyncPostSignHandler::InvokeSync(Amazon.Runtime.IExecutionContext)
extern "C"  void AmazonCognitoSyncPostSignHandler_InvokeSync_m2728373743 (AmazonCognitoSyncPostSignHandler_t2640152593 * __this, Il2CppObject * ___executionContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Amazon.CognitoSync.Internal.AmazonCognitoSyncPostSignHandler::InvokeAsync(Amazon.Runtime.IAsyncExecutionContext)
extern "C"  Il2CppObject * AmazonCognitoSyncPostSignHandler_InvokeAsync_m3570459138 (AmazonCognitoSyncPostSignHandler_t2640152593 * __this, Il2CppObject * ___executionContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Internal.AmazonCognitoSyncPostSignHandler::PreInvoke(Amazon.Runtime.IExecutionContext)
extern "C"  void AmazonCognitoSyncPostSignHandler_PreInvoke_m2152594379 (AmazonCognitoSyncPostSignHandler_t2640152593 * __this, Il2CppObject * ___executionContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Internal.AmazonCognitoSyncPostSignHandler::ProcessRequestHandlers(Amazon.Runtime.IExecutionContext)
extern "C"  void AmazonCognitoSyncPostSignHandler_ProcessRequestHandlers_m2817903865 (AmazonCognitoSyncPostSignHandler_t2640152593 * __this, Il2CppObject * ___executionContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Internal.AmazonCognitoSyncPostSignHandler::.ctor()
extern "C"  void AmazonCognitoSyncPostSignHandler__ctor_m1662842986 (AmazonCognitoSyncPostSignHandler_t2640152593 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
