﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23196873006MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<Amazon.Runtime.Metric,Amazon.Runtime.Internal.Util.Timing>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m2952439040(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t1354617973 *, int32_t, Timing_t847194262 *, const MethodInfo*))KeyValuePair_2__ctor_m595192313_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<Amazon.Runtime.Metric,Amazon.Runtime.Internal.Util.Timing>::get_Key()
#define KeyValuePair_2_get_Key_m2807078702(__this, method) ((  int32_t (*) (KeyValuePair_2_t1354617973 *, const MethodInfo*))KeyValuePair_2_get_Key_m2312610863_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Amazon.Runtime.Metric,Amazon.Runtime.Internal.Util.Timing>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m2985013615(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1354617973 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m547641270_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<Amazon.Runtime.Metric,Amazon.Runtime.Internal.Util.Timing>::get_Value()
#define KeyValuePair_2_get_Value_m3270663854(__this, method) ((  Timing_t847194262 * (*) (KeyValuePair_2_t1354617973 *, const MethodInfo*))KeyValuePair_2_get_Value_m3734313679_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Amazon.Runtime.Metric,Amazon.Runtime.Internal.Util.Timing>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m342137231(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1354617973 *, Timing_t847194262 *, const MethodInfo*))KeyValuePair_2_set_Value_m1567047294_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<Amazon.Runtime.Metric,Amazon.Runtime.Internal.Util.Timing>::ToString()
#define KeyValuePair_2_ToString_m2681143781(__this, method) ((  String_t* (*) (KeyValuePair_2_t1354617973 *, const MethodInfo*))KeyValuePair_2_ToString_m3523821758_gshared)(__this, method)
