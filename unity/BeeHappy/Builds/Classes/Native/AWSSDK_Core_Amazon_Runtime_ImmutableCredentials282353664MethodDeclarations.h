﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.ImmutableCredentials
struct ImmutableCredentials_t282353664;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"

// System.String Amazon.Runtime.ImmutableCredentials::get_AccessKey()
extern "C"  String_t* ImmutableCredentials_get_AccessKey_m1633856210 (ImmutableCredentials_t282353664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.ImmutableCredentials::set_AccessKey(System.String)
extern "C"  void ImmutableCredentials_set_AccessKey_m608721307 (ImmutableCredentials_t282353664 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Runtime.ImmutableCredentials::get_SecretKey()
extern "C"  String_t* ImmutableCredentials_get_SecretKey_m2315966932 (ImmutableCredentials_t282353664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.ImmutableCredentials::set_SecretKey(System.String)
extern "C"  void ImmutableCredentials_set_SecretKey_m4192155481 (ImmutableCredentials_t282353664 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Runtime.ImmutableCredentials::get_Token()
extern "C"  String_t* ImmutableCredentials_get_Token_m2619824998 (ImmutableCredentials_t282353664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.ImmutableCredentials::set_Token(System.String)
extern "C"  void ImmutableCredentials_set_Token_m265128363 (ImmutableCredentials_t282353664 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.ImmutableCredentials::get_UseToken()
extern "C"  bool ImmutableCredentials_get_UseToken_m673031238 (ImmutableCredentials_t282353664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.ImmutableCredentials::.ctor(System.String,System.String,System.String)
extern "C"  void ImmutableCredentials__ctor_m1993825745 (ImmutableCredentials_t282353664 * __this, String_t* ___awsAccessKeyId0, String_t* ___awsSecretAccessKey1, String_t* ___token2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.ImmutableCredentials::.ctor()
extern "C"  void ImmutableCredentials__ctor_m2100079895 (ImmutableCredentials_t282353664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.ImmutableCredentials Amazon.Runtime.ImmutableCredentials::Copy()
extern "C"  ImmutableCredentials_t282353664 * ImmutableCredentials_Copy_m872003133 (ImmutableCredentials_t282353664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Amazon.Runtime.ImmutableCredentials::GetHashCode()
extern "C"  int32_t ImmutableCredentials_GetHashCode_m2433635646 (ImmutableCredentials_t282353664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.ImmutableCredentials::Equals(System.Object)
extern "C"  bool ImmutableCredentials_Equals_m144366446 (ImmutableCredentials_t282353664 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
