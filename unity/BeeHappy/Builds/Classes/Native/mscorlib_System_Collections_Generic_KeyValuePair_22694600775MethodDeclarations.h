﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Delegate>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m1331080339(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2694600775 *, String_t*, Delegate_t3022476291 *, const MethodInfo*))KeyValuePair_2__ctor_m1640124561_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,System.Delegate>::get_Key()
#define KeyValuePair_2_get_Key_m3829997369(__this, method) ((  String_t* (*) (KeyValuePair_2_t2694600775 *, const MethodInfo*))KeyValuePair_2_get_Key_m2561166459_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Delegate>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m3166370330(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2694600775 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m744486900_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,System.Delegate>::get_Value()
#define KeyValuePair_2_get_Value_m3939756889(__this, method) ((  Delegate_t3022476291 * (*) (KeyValuePair_2_t2694600775 *, const MethodInfo*))KeyValuePair_2_get_Value_m499643803_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Delegate>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m2962455698(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2694600775 *, Delegate_t3022476291 *, const MethodInfo*))KeyValuePair_2_set_Value_m1416408204_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,System.Delegate>::ToString()
#define KeyValuePair_2_ToString_m3429372606(__this, method) ((  String_t* (*) (KeyValuePair_2_t2694600775 *, const MethodInfo*))KeyValuePair_2_ToString_m2613351884_gshared)(__this, method)
