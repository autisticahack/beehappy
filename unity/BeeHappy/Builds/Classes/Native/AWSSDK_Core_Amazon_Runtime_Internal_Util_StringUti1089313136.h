﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Text.Encoding
struct Encoding_t663144255;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Util.StringUtils
struct  StringUtils_t1089313136  : public Il2CppObject
{
public:

public:
};

struct StringUtils_t1089313136_StaticFields
{
public:
	// System.Text.Encoding Amazon.Runtime.Internal.Util.StringUtils::UTF_8
	Encoding_t663144255 * ___UTF_8_0;

public:
	inline static int32_t get_offset_of_UTF_8_0() { return static_cast<int32_t>(offsetof(StringUtils_t1089313136_StaticFields, ___UTF_8_0)); }
	inline Encoding_t663144255 * get_UTF_8_0() const { return ___UTF_8_0; }
	inline Encoding_t663144255 ** get_address_of_UTF_8_0() { return &___UTF_8_0; }
	inline void set_UTF_8_0(Encoding_t663144255 * value)
	{
		___UTF_8_0 = value;
		Il2CppCodeGenWriteBarrier(&___UTF_8_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
