﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<System.Action>
struct Action_1_t3028271134;
// System.Collections.Generic.Queue`1<System.Action>
struct Queue_1_t3046128587;
// System.Threading.Thread
struct Thread_t241561612;
// System.Threading.AutoResetEvent
struct AutoResetEvent_t15112628;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Util.BackgroundDispatcher`1<System.Action>
struct  BackgroundDispatcher_1_t3230906411  : public Il2CppObject
{
public:
	// System.Boolean Amazon.Runtime.Internal.Util.BackgroundDispatcher`1::isDisposed
	bool ___isDisposed_0;
	// System.Action`1<T> Amazon.Runtime.Internal.Util.BackgroundDispatcher`1::action
	Action_1_t3028271134 * ___action_1;
	// System.Collections.Generic.Queue`1<T> Amazon.Runtime.Internal.Util.BackgroundDispatcher`1::queue
	Queue_1_t3046128587 * ___queue_2;
	// System.Threading.Thread Amazon.Runtime.Internal.Util.BackgroundDispatcher`1::backgroundThread
	Thread_t241561612 * ___backgroundThread_3;
	// System.Threading.AutoResetEvent Amazon.Runtime.Internal.Util.BackgroundDispatcher`1::resetEvent
	AutoResetEvent_t15112628 * ___resetEvent_4;
	// System.Boolean Amazon.Runtime.Internal.Util.BackgroundDispatcher`1::shouldStop
	bool ___shouldStop_5;
	// System.Boolean Amazon.Runtime.Internal.Util.BackgroundDispatcher`1::<IsRunning>k__BackingField
	bool ___U3CIsRunningU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_isDisposed_0() { return static_cast<int32_t>(offsetof(BackgroundDispatcher_1_t3230906411, ___isDisposed_0)); }
	inline bool get_isDisposed_0() const { return ___isDisposed_0; }
	inline bool* get_address_of_isDisposed_0() { return &___isDisposed_0; }
	inline void set_isDisposed_0(bool value)
	{
		___isDisposed_0 = value;
	}

	inline static int32_t get_offset_of_action_1() { return static_cast<int32_t>(offsetof(BackgroundDispatcher_1_t3230906411, ___action_1)); }
	inline Action_1_t3028271134 * get_action_1() const { return ___action_1; }
	inline Action_1_t3028271134 ** get_address_of_action_1() { return &___action_1; }
	inline void set_action_1(Action_1_t3028271134 * value)
	{
		___action_1 = value;
		Il2CppCodeGenWriteBarrier(&___action_1, value);
	}

	inline static int32_t get_offset_of_queue_2() { return static_cast<int32_t>(offsetof(BackgroundDispatcher_1_t3230906411, ___queue_2)); }
	inline Queue_1_t3046128587 * get_queue_2() const { return ___queue_2; }
	inline Queue_1_t3046128587 ** get_address_of_queue_2() { return &___queue_2; }
	inline void set_queue_2(Queue_1_t3046128587 * value)
	{
		___queue_2 = value;
		Il2CppCodeGenWriteBarrier(&___queue_2, value);
	}

	inline static int32_t get_offset_of_backgroundThread_3() { return static_cast<int32_t>(offsetof(BackgroundDispatcher_1_t3230906411, ___backgroundThread_3)); }
	inline Thread_t241561612 * get_backgroundThread_3() const { return ___backgroundThread_3; }
	inline Thread_t241561612 ** get_address_of_backgroundThread_3() { return &___backgroundThread_3; }
	inline void set_backgroundThread_3(Thread_t241561612 * value)
	{
		___backgroundThread_3 = value;
		Il2CppCodeGenWriteBarrier(&___backgroundThread_3, value);
	}

	inline static int32_t get_offset_of_resetEvent_4() { return static_cast<int32_t>(offsetof(BackgroundDispatcher_1_t3230906411, ___resetEvent_4)); }
	inline AutoResetEvent_t15112628 * get_resetEvent_4() const { return ___resetEvent_4; }
	inline AutoResetEvent_t15112628 ** get_address_of_resetEvent_4() { return &___resetEvent_4; }
	inline void set_resetEvent_4(AutoResetEvent_t15112628 * value)
	{
		___resetEvent_4 = value;
		Il2CppCodeGenWriteBarrier(&___resetEvent_4, value);
	}

	inline static int32_t get_offset_of_shouldStop_5() { return static_cast<int32_t>(offsetof(BackgroundDispatcher_1_t3230906411, ___shouldStop_5)); }
	inline bool get_shouldStop_5() const { return ___shouldStop_5; }
	inline bool* get_address_of_shouldStop_5() { return &___shouldStop_5; }
	inline void set_shouldStop_5(bool value)
	{
		___shouldStop_5 = value;
	}

	inline static int32_t get_offset_of_U3CIsRunningU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(BackgroundDispatcher_1_t3230906411, ___U3CIsRunningU3Ek__BackingField_6)); }
	inline bool get_U3CIsRunningU3Ek__BackingField_6() const { return ___U3CIsRunningU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CIsRunningU3Ek__BackingField_6() { return &___U3CIsRunningU3Ek__BackingField_6; }
	inline void set_U3CIsRunningU3Ek__BackingField_6(bool value)
	{
		___U3CIsRunningU3Ek__BackingField_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
