﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.CognitoSync.Model.DeleteDatasetRequest
struct DeleteDatasetRequest_t3672322944;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.String Amazon.CognitoSync.Model.DeleteDatasetRequest::get_DatasetName()
extern "C"  String_t* DeleteDatasetRequest_get_DatasetName_m3546224380 (DeleteDatasetRequest_t3672322944 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Model.DeleteDatasetRequest::set_DatasetName(System.String)
extern "C"  void DeleteDatasetRequest_set_DatasetName_m1718572659 (DeleteDatasetRequest_t3672322944 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.CognitoSync.Model.DeleteDatasetRequest::IsSetDatasetName()
extern "C"  bool DeleteDatasetRequest_IsSetDatasetName_m201190818 (DeleteDatasetRequest_t3672322944 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.CognitoSync.Model.DeleteDatasetRequest::get_IdentityId()
extern "C"  String_t* DeleteDatasetRequest_get_IdentityId_m111637104 (DeleteDatasetRequest_t3672322944 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Model.DeleteDatasetRequest::set_IdentityId(System.String)
extern "C"  void DeleteDatasetRequest_set_IdentityId_m611708513 (DeleteDatasetRequest_t3672322944 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.CognitoSync.Model.DeleteDatasetRequest::IsSetIdentityId()
extern "C"  bool DeleteDatasetRequest_IsSetIdentityId_m2244983850 (DeleteDatasetRequest_t3672322944 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.CognitoSync.Model.DeleteDatasetRequest::get_IdentityPoolId()
extern "C"  String_t* DeleteDatasetRequest_get_IdentityPoolId_m3220866646 (DeleteDatasetRequest_t3672322944 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Model.DeleteDatasetRequest::set_IdentityPoolId(System.String)
extern "C"  void DeleteDatasetRequest_set_IdentityPoolId_m1693813 (DeleteDatasetRequest_t3672322944 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.CognitoSync.Model.DeleteDatasetRequest::IsSetIdentityPoolId()
extern "C"  bool DeleteDatasetRequest_IsSetIdentityPoolId_m3032275260 (DeleteDatasetRequest_t3672322944 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Model.DeleteDatasetRequest::.ctor()
extern "C"  void DeleteDatasetRequest__ctor_m1150399625 (DeleteDatasetRequest_t3672322944 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
