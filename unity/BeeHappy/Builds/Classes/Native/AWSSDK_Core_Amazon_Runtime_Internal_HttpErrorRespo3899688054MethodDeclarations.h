﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.HttpErrorResponseExceptionHandler
struct HttpErrorResponseExceptionHandler_t3899688054;
// Amazon.Runtime.Internal.Util.ILogger
struct ILogger_t676853571;
// Amazon.Runtime.IExecutionContext
struct IExecutionContext_t2477130752;
// Amazon.Runtime.Internal.HttpErrorResponseException
struct HttpErrorResponseException_t3191555406;
// Amazon.Runtime.Internal.Transform.IWebResponseData
struct IWebResponseData_t3815728244;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_HttpErrorRespo3191555406.h"

// System.Void Amazon.Runtime.Internal.HttpErrorResponseExceptionHandler::.ctor(Amazon.Runtime.Internal.Util.ILogger)
extern "C"  void HttpErrorResponseExceptionHandler__ctor_m254649400 (HttpErrorResponseExceptionHandler_t3899688054 * __this, Il2CppObject * ___logger0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.HttpErrorResponseExceptionHandler::HandleException(Amazon.Runtime.IExecutionContext,Amazon.Runtime.Internal.HttpErrorResponseException)
extern "C"  bool HttpErrorResponseExceptionHandler_HandleException_m733395142 (HttpErrorResponseExceptionHandler_t3899688054 * __this, Il2CppObject * ___executionContext0, HttpErrorResponseException_t3191555406 * ___exception1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.HttpErrorResponseExceptionHandler::HandleSuppressed404(Amazon.Runtime.IExecutionContext,Amazon.Runtime.Internal.Transform.IWebResponseData)
extern "C"  bool HttpErrorResponseExceptionHandler_HandleSuppressed404_m4293641139 (HttpErrorResponseExceptionHandler_t3899688054 * __this, Il2CppObject * ___executionContext0, Il2CppObject * ___httpErrorResponse1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
