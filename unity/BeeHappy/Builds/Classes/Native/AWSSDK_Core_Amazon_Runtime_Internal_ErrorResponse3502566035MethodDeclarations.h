﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.ErrorResponse
struct ErrorResponse_t3502566035;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_Runtime_ErrorType1448377524.h"
#include "mscorlib_System_String2029220233.h"

// Amazon.Runtime.ErrorType Amazon.Runtime.Internal.ErrorResponse::get_Type()
extern "C"  int32_t ErrorResponse_get_Type_m3388577699 (ErrorResponse_t3502566035 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.ErrorResponse::set_Type(Amazon.Runtime.ErrorType)
extern "C"  void ErrorResponse_set_Type_m1083047574 (ErrorResponse_t3502566035 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Runtime.Internal.ErrorResponse::get_Code()
extern "C"  String_t* ErrorResponse_get_Code_m1007089050 (ErrorResponse_t3502566035 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.ErrorResponse::set_Code(System.String)
extern "C"  void ErrorResponse_set_Code_m3633082089 (ErrorResponse_t3502566035 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Runtime.Internal.ErrorResponse::get_Message()
extern "C"  String_t* ErrorResponse_get_Message_m1145825032 (ErrorResponse_t3502566035 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.ErrorResponse::set_Message(System.String)
extern "C"  void ErrorResponse_set_Message_m1143938639 (ErrorResponse_t3502566035 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Runtime.Internal.ErrorResponse::get_RequestId()
extern "C"  String_t* ErrorResponse_get_RequestId_m2536924555 (ErrorResponse_t3502566035 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.ErrorResponse::set_RequestId(System.String)
extern "C"  void ErrorResponse_set_RequestId_m3138094108 (ErrorResponse_t3502566035 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.ErrorResponse::.ctor()
extern "C"  void ErrorResponse__ctor_m3059762343 (ErrorResponse_t3502566035 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
