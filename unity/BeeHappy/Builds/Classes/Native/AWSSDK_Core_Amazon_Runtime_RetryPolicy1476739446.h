﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.Runtime.Internal.Util.ILogger
struct ILogger_t676853571;
// System.Collections.Generic.HashSet`1<System.String>
struct HashSet_1_t362681087;

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_TimeSpan3430258949.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.RetryPolicy
struct  RetryPolicy_t1476739446  : public Il2CppObject
{
public:
	// System.Int32 Amazon.Runtime.RetryPolicy::<MaxRetries>k__BackingField
	int32_t ___U3CMaxRetriesU3Ek__BackingField_0;
	// Amazon.Runtime.Internal.Util.ILogger Amazon.Runtime.RetryPolicy::<Logger>k__BackingField
	Il2CppObject * ___U3CLoggerU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CMaxRetriesU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RetryPolicy_t1476739446, ___U3CMaxRetriesU3Ek__BackingField_0)); }
	inline int32_t get_U3CMaxRetriesU3Ek__BackingField_0() const { return ___U3CMaxRetriesU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CMaxRetriesU3Ek__BackingField_0() { return &___U3CMaxRetriesU3Ek__BackingField_0; }
	inline void set_U3CMaxRetriesU3Ek__BackingField_0(int32_t value)
	{
		___U3CMaxRetriesU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CLoggerU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RetryPolicy_t1476739446, ___U3CLoggerU3Ek__BackingField_1)); }
	inline Il2CppObject * get_U3CLoggerU3Ek__BackingField_1() const { return ___U3CLoggerU3Ek__BackingField_1; }
	inline Il2CppObject ** get_address_of_U3CLoggerU3Ek__BackingField_1() { return &___U3CLoggerU3Ek__BackingField_1; }
	inline void set_U3CLoggerU3Ek__BackingField_1(Il2CppObject * value)
	{
		___U3CLoggerU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CLoggerU3Ek__BackingField_1, value);
	}
};

struct RetryPolicy_t1476739446_StaticFields
{
public:
	// System.Collections.Generic.HashSet`1<System.String> Amazon.Runtime.RetryPolicy::clockSkewErrorCodes
	HashSet_1_t362681087 * ___clockSkewErrorCodes_2;
	// System.TimeSpan Amazon.Runtime.RetryPolicy::clockSkewMaxThreshold
	TimeSpan_t3430258949  ___clockSkewMaxThreshold_3;

public:
	inline static int32_t get_offset_of_clockSkewErrorCodes_2() { return static_cast<int32_t>(offsetof(RetryPolicy_t1476739446_StaticFields, ___clockSkewErrorCodes_2)); }
	inline HashSet_1_t362681087 * get_clockSkewErrorCodes_2() const { return ___clockSkewErrorCodes_2; }
	inline HashSet_1_t362681087 ** get_address_of_clockSkewErrorCodes_2() { return &___clockSkewErrorCodes_2; }
	inline void set_clockSkewErrorCodes_2(HashSet_1_t362681087 * value)
	{
		___clockSkewErrorCodes_2 = value;
		Il2CppCodeGenWriteBarrier(&___clockSkewErrorCodes_2, value);
	}

	inline static int32_t get_offset_of_clockSkewMaxThreshold_3() { return static_cast<int32_t>(offsetof(RetryPolicy_t1476739446_StaticFields, ___clockSkewMaxThreshold_3)); }
	inline TimeSpan_t3430258949  get_clockSkewMaxThreshold_3() const { return ___clockSkewMaxThreshold_3; }
	inline TimeSpan_t3430258949 * get_address_of_clockSkewMaxThreshold_3() { return &___clockSkewMaxThreshold_3; }
	inline void set_clockSkewMaxThreshold_3(TimeSpan_t3430258949  value)
	{
		___clockSkewMaxThreshold_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
