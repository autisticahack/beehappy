﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.CognitoSync.SyncManager.Internal.DatasetUtils
struct  DatasetUtils_t516935539  : public Il2CppObject
{
public:

public:
};

struct DatasetUtils_t516935539_StaticFields
{
public:
	// System.String Amazon.CognitoSync.SyncManager.Internal.DatasetUtils::DATASET_NAME_PATTERN
	String_t* ___DATASET_NAME_PATTERN_0;
	// System.String Amazon.CognitoSync.SyncManager.Internal.DatasetUtils::UNKNOWN_IDENTITY_ID
	String_t* ___UNKNOWN_IDENTITY_ID_1;

public:
	inline static int32_t get_offset_of_DATASET_NAME_PATTERN_0() { return static_cast<int32_t>(offsetof(DatasetUtils_t516935539_StaticFields, ___DATASET_NAME_PATTERN_0)); }
	inline String_t* get_DATASET_NAME_PATTERN_0() const { return ___DATASET_NAME_PATTERN_0; }
	inline String_t** get_address_of_DATASET_NAME_PATTERN_0() { return &___DATASET_NAME_PATTERN_0; }
	inline void set_DATASET_NAME_PATTERN_0(String_t* value)
	{
		___DATASET_NAME_PATTERN_0 = value;
		Il2CppCodeGenWriteBarrier(&___DATASET_NAME_PATTERN_0, value);
	}

	inline static int32_t get_offset_of_UNKNOWN_IDENTITY_ID_1() { return static_cast<int32_t>(offsetof(DatasetUtils_t516935539_StaticFields, ___UNKNOWN_IDENTITY_ID_1)); }
	inline String_t* get_UNKNOWN_IDENTITY_ID_1() const { return ___UNKNOWN_IDENTITY_ID_1; }
	inline String_t** get_address_of_UNKNOWN_IDENTITY_ID_1() { return &___UNKNOWN_IDENTITY_ID_1; }
	inline void set_UNKNOWN_IDENTITY_ID_1(String_t* value)
	{
		___UNKNOWN_IDENTITY_ID_1 = value;
		Il2CppCodeGenWriteBarrier(&___UNKNOWN_IDENTITY_ID_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
