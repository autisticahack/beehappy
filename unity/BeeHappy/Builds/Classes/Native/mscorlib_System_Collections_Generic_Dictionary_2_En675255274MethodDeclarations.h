﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>
struct Dictionary_2_t3650197868;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En675255274.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21407543090.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_ObjectMetadata4058137740.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3882269915_gshared (Enumerator_t675255274 * __this, Dictionary_2_t3650197868 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m3882269915(__this, ___dictionary0, method) ((  void (*) (Enumerator_t675255274 *, Dictionary_2_t3650197868 *, const MethodInfo*))Enumerator__ctor_m3882269915_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3006262826_gshared (Enumerator_t675255274 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3006262826(__this, method) ((  Il2CppObject * (*) (Enumerator_t675255274 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3006262826_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m786642932_gshared (Enumerator_t675255274 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m786642932(__this, method) ((  void (*) (Enumerator_t675255274 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m786642932_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3805059129_gshared (Enumerator_t675255274 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3805059129(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t675255274 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3805059129_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3135173292_gshared (Enumerator_t675255274 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3135173292(__this, method) ((  Il2CppObject * (*) (Enumerator_t675255274 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3135173292_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3285338134_gshared (Enumerator_t675255274 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3285338134(__this, method) ((  Il2CppObject * (*) (Enumerator_t675255274 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3285338134_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m513463628_gshared (Enumerator_t675255274 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m513463628(__this, method) ((  bool (*) (Enumerator_t675255274 *, const MethodInfo*))Enumerator_MoveNext_m513463628_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::get_Current()
extern "C"  KeyValuePair_2_t1407543090  Enumerator_get_Current_m2333579100_gshared (Enumerator_t675255274 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2333579100(__this, method) ((  KeyValuePair_2_t1407543090  (*) (Enumerator_t675255274 *, const MethodInfo*))Enumerator_get_Current_m2333579100_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::get_CurrentKey()
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m329261871_gshared (Enumerator_t675255274 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m329261871(__this, method) ((  Il2CppObject * (*) (Enumerator_t675255274 *, const MethodInfo*))Enumerator_get_CurrentKey_m329261871_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::get_CurrentValue()
extern "C"  ObjectMetadata_t4058137740  Enumerator_get_CurrentValue_m2906228503_gshared (Enumerator_t675255274 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m2906228503(__this, method) ((  ObjectMetadata_t4058137740  (*) (Enumerator_t675255274 *, const MethodInfo*))Enumerator_get_CurrentValue_m2906228503_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::Reset()
extern "C"  void Enumerator_Reset_m975923089_gshared (Enumerator_t675255274 * __this, const MethodInfo* method);
#define Enumerator_Reset_m975923089(__this, method) ((  void (*) (Enumerator_t675255274 *, const MethodInfo*))Enumerator_Reset_m975923089_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::VerifyState()
extern "C"  void Enumerator_VerifyState_m3950116612_gshared (Enumerator_t675255274 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m3950116612(__this, method) ((  void (*) (Enumerator_t675255274 *, const MethodInfo*))Enumerator_VerifyState_m3950116612_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m424726820_gshared (Enumerator_t675255274 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m424726820(__this, method) ((  void (*) (Enumerator_t675255274 *, const MethodInfo*))Enumerator_VerifyCurrent_m424726820_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::Dispose()
extern "C"  void Enumerator_Dispose_m3127619583_gshared (Enumerator_t675255274 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3127619583(__this, method) ((  void (*) (Enumerator_t675255274 *, const MethodInfo*))Enumerator_Dispose_m3127619583_gshared)(__this, method)
