﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.DownloadHandlerBufferWrapper
struct DownloadHandlerBufferWrapper_t1800693841;
// System.Object
struct Il2CppObject;
// System.Byte[]
struct ByteU5BU5D_t3397334013;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Object Amazon.Runtime.Internal.DownloadHandlerBufferWrapper::get_Instance()
extern "C"  Il2CppObject * DownloadHandlerBufferWrapper_get_Instance_m2995027770 (DownloadHandlerBufferWrapper_t1800693841 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.DownloadHandlerBufferWrapper::set_Instance(System.Object)
extern "C"  void DownloadHandlerBufferWrapper_set_Instance_m393153045 (DownloadHandlerBufferWrapper_t1800693841 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.DownloadHandlerBufferWrapper::.cctor()
extern "C"  void DownloadHandlerBufferWrapper__cctor_m468129122 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.DownloadHandlerBufferWrapper::.ctor()
extern "C"  void DownloadHandlerBufferWrapper__ctor_m3368120675 (DownloadHandlerBufferWrapper_t1800693841 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Amazon.Runtime.Internal.DownloadHandlerBufferWrapper::get_Data()
extern "C"  ByteU5BU5D_t3397334013* DownloadHandlerBufferWrapper_get_Data_m1172320050 (DownloadHandlerBufferWrapper_t1800693841 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.DownloadHandlerBufferWrapper::Dispose(System.Boolean)
extern "C"  void DownloadHandlerBufferWrapper_Dispose_m4284950631 (DownloadHandlerBufferWrapper_t1800693841 * __this, bool ___disposing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.DownloadHandlerBufferWrapper::Dispose()
extern "C"  void DownloadHandlerBufferWrapper_Dispose_m2166590030 (DownloadHandlerBufferWrapper_t1800693841 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
