﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AWSSDK_Core_ThirdParty_iOS4Unity_NSObject1518098886.h"
#include "mscorlib_System_IntPtr2504060609.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.iOS4Unity.UIDevice
struct  UIDevice_t860038178  : public NSObject_t1518098886
{
public:

public:
};

struct UIDevice_t860038178_StaticFields
{
public:
	// System.IntPtr ThirdParty.iOS4Unity.UIDevice::_classHandle
	IntPtr_t ____classHandle_3;
	// System.Int32 ThirdParty.iOS4Unity.UIDevice::_majorVersion
	int32_t ____majorVersion_4;
	// System.Int32 ThirdParty.iOS4Unity.UIDevice::_minorVersion
	int32_t ____minorVersion_5;

public:
	inline static int32_t get_offset_of__classHandle_3() { return static_cast<int32_t>(offsetof(UIDevice_t860038178_StaticFields, ____classHandle_3)); }
	inline IntPtr_t get__classHandle_3() const { return ____classHandle_3; }
	inline IntPtr_t* get_address_of__classHandle_3() { return &____classHandle_3; }
	inline void set__classHandle_3(IntPtr_t value)
	{
		____classHandle_3 = value;
	}

	inline static int32_t get_offset_of__majorVersion_4() { return static_cast<int32_t>(offsetof(UIDevice_t860038178_StaticFields, ____majorVersion_4)); }
	inline int32_t get__majorVersion_4() const { return ____majorVersion_4; }
	inline int32_t* get_address_of__majorVersion_4() { return &____majorVersion_4; }
	inline void set__majorVersion_4(int32_t value)
	{
		____majorVersion_4 = value;
	}

	inline static int32_t get_offset_of__minorVersion_5() { return static_cast<int32_t>(offsetof(UIDevice_t860038178_StaticFields, ____minorVersion_5)); }
	inline int32_t get__minorVersion_5() const { return ____minorVersion_5; }
	inline int32_t* get_address_of__minorVersion_5() { return &____minorVersion_5; }
	inline void set__minorVersion_5(int32_t value)
	{
		____minorVersion_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
