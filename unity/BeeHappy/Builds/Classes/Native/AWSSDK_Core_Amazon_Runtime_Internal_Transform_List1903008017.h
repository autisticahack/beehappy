﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Transform.ListUnmarshaller`2<System.Object,System.Object>
struct  ListUnmarshaller_2_t1903008017  : public Il2CppObject
{
public:
	// IUnmarshaller Amazon.Runtime.Internal.Transform.ListUnmarshaller`2::iUnmarshaller
	Il2CppObject * ___iUnmarshaller_0;

public:
	inline static int32_t get_offset_of_iUnmarshaller_0() { return static_cast<int32_t>(offsetof(ListUnmarshaller_2_t1903008017, ___iUnmarshaller_0)); }
	inline Il2CppObject * get_iUnmarshaller_0() const { return ___iUnmarshaller_0; }
	inline Il2CppObject ** get_address_of_iUnmarshaller_0() { return &___iUnmarshaller_0; }
	inline void set_iUnmarshaller_0(Il2CppObject * value)
	{
		___iUnmarshaller_0 = value;
		Il2CppCodeGenWriteBarrier(&___iUnmarshaller_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
