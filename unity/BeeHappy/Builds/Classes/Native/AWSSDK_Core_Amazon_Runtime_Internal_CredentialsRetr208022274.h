﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.Runtime.AWSCredentials
struct AWSCredentials_t3583921007;

#include "AWSSDK_Core_Amazon_Runtime_Internal_PipelineHandle1486422324.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.CredentialsRetriever
struct  CredentialsRetriever_t208022274  : public PipelineHandler_t1486422324
{
public:
	// Amazon.Runtime.AWSCredentials Amazon.Runtime.Internal.CredentialsRetriever::<Credentials>k__BackingField
	AWSCredentials_t3583921007 * ___U3CCredentialsU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CCredentialsU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(CredentialsRetriever_t208022274, ___U3CCredentialsU3Ek__BackingField_3)); }
	inline AWSCredentials_t3583921007 * get_U3CCredentialsU3Ek__BackingField_3() const { return ___U3CCredentialsU3Ek__BackingField_3; }
	inline AWSCredentials_t3583921007 ** get_address_of_U3CCredentialsU3Ek__BackingField_3() { return &___U3CCredentialsU3Ek__BackingField_3; }
	inline void set_U3CCredentialsU3Ek__BackingField_3(AWSCredentials_t3583921007 * value)
	{
		___U3CCredentialsU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCredentialsU3Ek__BackingField_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
