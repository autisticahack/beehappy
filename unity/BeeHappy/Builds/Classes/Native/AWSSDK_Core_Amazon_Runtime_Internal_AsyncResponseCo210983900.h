﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.Runtime.Internal.RuntimeAsyncResult
struct RuntimeAsyncResult_t4279356013;

#include "AWSSDK_Core_Amazon_Runtime_Internal_ResponseContex3850805926.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.AsyncResponseContext
struct  AsyncResponseContext_t210983900  : public ResponseContext_t3850805926
{
public:
	// Amazon.Runtime.Internal.RuntimeAsyncResult Amazon.Runtime.Internal.AsyncResponseContext::<AsyncResult>k__BackingField
	RuntimeAsyncResult_t4279356013 * ___U3CAsyncResultU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CAsyncResultU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AsyncResponseContext_t210983900, ___U3CAsyncResultU3Ek__BackingField_2)); }
	inline RuntimeAsyncResult_t4279356013 * get_U3CAsyncResultU3Ek__BackingField_2() const { return ___U3CAsyncResultU3Ek__BackingField_2; }
	inline RuntimeAsyncResult_t4279356013 ** get_address_of_U3CAsyncResultU3Ek__BackingField_2() { return &___U3CAsyncResultU3Ek__BackingField_2; }
	inline void set_U3CAsyncResultU3Ek__BackingField_2(RuntimeAsyncResult_t4279356013 * value)
	{
		___U3CAsyncResultU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CAsyncResultU3Ek__BackingField_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
