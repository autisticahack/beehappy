﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.RetryPolicy
struct RetryPolicy_t1476739446;
// Amazon.Runtime.Internal.Util.ILogger
struct ILogger_t676853571;
// Amazon.Runtime.IExecutionContext
struct IExecutionContext_t2477130752;
// System.Exception
struct Exception_t1927440687;
// Amazon.Runtime.AmazonServiceException
struct AmazonServiceException_t3748559634;
// Amazon.Runtime.Internal.Transform.IWebResponseData
struct IWebResponseData_t3815728244;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1927440687.h"
#include "mscorlib_System_Nullable_1_gen2088641033.h"
#include "AWSSDK_Core_Amazon_Runtime_AmazonServiceException3748559634.h"
#include "mscorlib_System_DateTime693205669.h"

// System.Int32 Amazon.Runtime.RetryPolicy::get_MaxRetries()
extern "C"  int32_t RetryPolicy_get_MaxRetries_m4127357800 (RetryPolicy_t1476739446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.RetryPolicy::set_MaxRetries(System.Int32)
extern "C"  void RetryPolicy_set_MaxRetries_m936691491 (RetryPolicy_t1476739446 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.Internal.Util.ILogger Amazon.Runtime.RetryPolicy::get_Logger()
extern "C"  Il2CppObject * RetryPolicy_get_Logger_m2265420851 (RetryPolicy_t1476739446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.RetryPolicy::set_Logger(Amazon.Runtime.Internal.Util.ILogger)
extern "C"  void RetryPolicy_set_Logger_m2775615670 (RetryPolicy_t1476739446 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.RetryPolicy::Retry(Amazon.Runtime.IExecutionContext,System.Exception)
extern "C"  bool RetryPolicy_Retry_m2511830087 (RetryPolicy_t1476739446 * __this, Il2CppObject * ___executionContext0, Exception_t1927440687 * ___exception1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.RetryPolicy::NotifySuccess(Amazon.Runtime.IExecutionContext)
extern "C"  void RetryPolicy_NotifySuccess_m1581866819 (RetryPolicy_t1476739446 * __this, Il2CppObject * ___executionContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.RetryPolicy::OnRetry(Amazon.Runtime.IExecutionContext)
extern "C"  bool RetryPolicy_OnRetry_m1737477514 (RetryPolicy_t1476739446 * __this, Il2CppObject * ___executionContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Boolean> Amazon.Runtime.RetryPolicy::RetrySync(Amazon.Runtime.IExecutionContext,System.Exception)
extern "C"  Nullable_1_t2088641033  RetryPolicy_RetrySync_m1008912761 (RetryPolicy_t1476739446 * __this, Il2CppObject * ___executionContext0, Exception_t1927440687 * ___exception1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.RetryPolicy::IsClockskew(Amazon.Runtime.IExecutionContext,System.Exception)
extern "C"  bool RetryPolicy_IsClockskew_m3825315995 (RetryPolicy_t1476739446 * __this, Il2CppObject * ___executionContext0, Exception_t1927440687 * ___exception1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.RetryPolicy::TryParseDateHeader(Amazon.Runtime.AmazonServiceException,System.DateTime&)
extern "C"  bool RetryPolicy_TryParseDateHeader_m475514586 (Il2CppObject * __this /* static, unused */, AmazonServiceException_t3748559634 * ___ase0, DateTime_t693205669 * ___serverTime1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.RetryPolicy::TryParseExceptionMessage(Amazon.Runtime.AmazonServiceException,System.DateTime&)
extern "C"  bool RetryPolicy_TryParseExceptionMessage_m3165109285 (Il2CppObject * __this /* static, unused */, AmazonServiceException_t3748559634 * ___ase0, DateTime_t693205669 * ___serverTime1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.Internal.Transform.IWebResponseData Amazon.Runtime.RetryPolicy::GetWebData(Amazon.Runtime.AmazonServiceException)
extern "C"  Il2CppObject * RetryPolicy_GetWebData_m846041893 (Il2CppObject * __this /* static, unused */, AmazonServiceException_t3748559634 * ___ase0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.RetryPolicy::.ctor()
extern "C"  void RetryPolicy__ctor_m3424698453 (RetryPolicy_t1476739446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.RetryPolicy::.cctor()
extern "C"  void RetryPolicy__cctor_m1926441058 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
