﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<Amazon.Runtime.IExecutionContext>
struct Action_1_t2278930134;

#include "AWSSDK_Core_Amazon_Runtime_Internal_PipelineHandle1486422324.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.CallbackHandler
struct  CallbackHandler_t174745619  : public PipelineHandler_t1486422324
{
public:
	// System.Action`1<Amazon.Runtime.IExecutionContext> Amazon.Runtime.Internal.CallbackHandler::<OnPreInvoke>k__BackingField
	Action_1_t2278930134 * ___U3COnPreInvokeU3Ek__BackingField_3;
	// System.Action`1<Amazon.Runtime.IExecutionContext> Amazon.Runtime.Internal.CallbackHandler::<OnPostInvoke>k__BackingField
	Action_1_t2278930134 * ___U3COnPostInvokeU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3COnPreInvokeU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(CallbackHandler_t174745619, ___U3COnPreInvokeU3Ek__BackingField_3)); }
	inline Action_1_t2278930134 * get_U3COnPreInvokeU3Ek__BackingField_3() const { return ___U3COnPreInvokeU3Ek__BackingField_3; }
	inline Action_1_t2278930134 ** get_address_of_U3COnPreInvokeU3Ek__BackingField_3() { return &___U3COnPreInvokeU3Ek__BackingField_3; }
	inline void set_U3COnPreInvokeU3Ek__BackingField_3(Action_1_t2278930134 * value)
	{
		___U3COnPreInvokeU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3COnPreInvokeU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3COnPostInvokeU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(CallbackHandler_t174745619, ___U3COnPostInvokeU3Ek__BackingField_4)); }
	inline Action_1_t2278930134 * get_U3COnPostInvokeU3Ek__BackingField_4() const { return ___U3COnPostInvokeU3Ek__BackingField_4; }
	inline Action_1_t2278930134 ** get_address_of_U3COnPostInvokeU3Ek__BackingField_4() { return &___U3COnPostInvokeU3Ek__BackingField_4; }
	inline void set_U3COnPostInvokeU3Ek__BackingField_4(Action_1_t2278930134 * value)
	{
		___U3COnPostInvokeU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3COnPostInvokeU3Ek__BackingField_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
