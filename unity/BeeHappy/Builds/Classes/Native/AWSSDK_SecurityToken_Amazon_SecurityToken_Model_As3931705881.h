﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.SecurityToken.Model.AssumedRoleUser
struct AssumedRoleUser_t150458319;
// System.String
struct String_t;
// Amazon.SecurityToken.Model.Credentials
struct Credentials_t3554032640;

#include "AWSSDK_Core_Amazon_Runtime_AmazonWebServiceResponse529043356.h"
#include "mscorlib_System_Nullable_1_gen334943763.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityResponse
struct  AssumeRoleWithWebIdentityResponse_t3931705881  : public AmazonWebServiceResponse_t529043356
{
public:
	// Amazon.SecurityToken.Model.AssumedRoleUser Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityResponse::_assumedRoleUser
	AssumedRoleUser_t150458319 * ____assumedRoleUser_3;
	// System.String Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityResponse::_audience
	String_t* ____audience_4;
	// Amazon.SecurityToken.Model.Credentials Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityResponse::_credentials
	Credentials_t3554032640 * ____credentials_5;
	// System.Nullable`1<System.Int32> Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityResponse::_packedPolicySize
	Nullable_1_t334943763  ____packedPolicySize_6;
	// System.String Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityResponse::_provider
	String_t* ____provider_7;
	// System.String Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityResponse::_subjectFromWebIdentityToken
	String_t* ____subjectFromWebIdentityToken_8;

public:
	inline static int32_t get_offset_of__assumedRoleUser_3() { return static_cast<int32_t>(offsetof(AssumeRoleWithWebIdentityResponse_t3931705881, ____assumedRoleUser_3)); }
	inline AssumedRoleUser_t150458319 * get__assumedRoleUser_3() const { return ____assumedRoleUser_3; }
	inline AssumedRoleUser_t150458319 ** get_address_of__assumedRoleUser_3() { return &____assumedRoleUser_3; }
	inline void set__assumedRoleUser_3(AssumedRoleUser_t150458319 * value)
	{
		____assumedRoleUser_3 = value;
		Il2CppCodeGenWriteBarrier(&____assumedRoleUser_3, value);
	}

	inline static int32_t get_offset_of__audience_4() { return static_cast<int32_t>(offsetof(AssumeRoleWithWebIdentityResponse_t3931705881, ____audience_4)); }
	inline String_t* get__audience_4() const { return ____audience_4; }
	inline String_t** get_address_of__audience_4() { return &____audience_4; }
	inline void set__audience_4(String_t* value)
	{
		____audience_4 = value;
		Il2CppCodeGenWriteBarrier(&____audience_4, value);
	}

	inline static int32_t get_offset_of__credentials_5() { return static_cast<int32_t>(offsetof(AssumeRoleWithWebIdentityResponse_t3931705881, ____credentials_5)); }
	inline Credentials_t3554032640 * get__credentials_5() const { return ____credentials_5; }
	inline Credentials_t3554032640 ** get_address_of__credentials_5() { return &____credentials_5; }
	inline void set__credentials_5(Credentials_t3554032640 * value)
	{
		____credentials_5 = value;
		Il2CppCodeGenWriteBarrier(&____credentials_5, value);
	}

	inline static int32_t get_offset_of__packedPolicySize_6() { return static_cast<int32_t>(offsetof(AssumeRoleWithWebIdentityResponse_t3931705881, ____packedPolicySize_6)); }
	inline Nullable_1_t334943763  get__packedPolicySize_6() const { return ____packedPolicySize_6; }
	inline Nullable_1_t334943763 * get_address_of__packedPolicySize_6() { return &____packedPolicySize_6; }
	inline void set__packedPolicySize_6(Nullable_1_t334943763  value)
	{
		____packedPolicySize_6 = value;
	}

	inline static int32_t get_offset_of__provider_7() { return static_cast<int32_t>(offsetof(AssumeRoleWithWebIdentityResponse_t3931705881, ____provider_7)); }
	inline String_t* get__provider_7() const { return ____provider_7; }
	inline String_t** get_address_of__provider_7() { return &____provider_7; }
	inline void set__provider_7(String_t* value)
	{
		____provider_7 = value;
		Il2CppCodeGenWriteBarrier(&____provider_7, value);
	}

	inline static int32_t get_offset_of__subjectFromWebIdentityToken_8() { return static_cast<int32_t>(offsetof(AssumeRoleWithWebIdentityResponse_t3931705881, ____subjectFromWebIdentityToken_8)); }
	inline String_t* get__subjectFromWebIdentityToken_8() const { return ____subjectFromWebIdentityToken_8; }
	inline String_t** get_address_of__subjectFromWebIdentityToken_8() { return &____subjectFromWebIdentityToken_8; }
	inline void set__subjectFromWebIdentityToken_8(String_t* value)
	{
		____subjectFromWebIdentityToken_8 = value;
		Il2CppCodeGenWriteBarrier(&____subjectFromWebIdentityToken_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
