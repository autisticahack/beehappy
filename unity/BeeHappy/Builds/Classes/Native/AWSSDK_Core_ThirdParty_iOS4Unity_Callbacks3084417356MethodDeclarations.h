﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ThirdParty.iOS4Unity.NSObject
struct NSObject_t1518098886;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_NSObject1518098886.h"

// System.Void ThirdParty.iOS4Unity.Callbacks::UnsubscribeAll(ThirdParty.iOS4Unity.NSObject)
extern "C"  void Callbacks_UnsubscribeAll_m1229431383 (Il2CppObject * __this /* static, unused */, NSObject_t1518098886 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.iOS4Unity.Callbacks::.cctor()
extern "C"  void Callbacks__cctor_m2942783777 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
