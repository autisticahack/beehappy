﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.CognitoSync.SyncManager.Dataset/DatasetMergedDelegate
struct DatasetMergedDelegate_t713795432;
// System.Object
struct Il2CppObject;
// Amazon.CognitoSync.SyncManager.Dataset
struct Dataset_t3040902086;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_SyncManager_3040902086.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Amazon.CognitoSync.SyncManager.Dataset/DatasetMergedDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void DatasetMergedDelegate__ctor_m3961819062 (DatasetMergedDelegate_t713795432 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.CognitoSync.SyncManager.Dataset/DatasetMergedDelegate::Invoke(Amazon.CognitoSync.SyncManager.Dataset,System.Collections.Generic.List`1<System.String>)
extern "C"  bool DatasetMergedDelegate_Invoke_m2393779350 (DatasetMergedDelegate_t713795432 * __this, Dataset_t3040902086 * ___dataset0, List_1_t1398341365 * ___datasetNames1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Amazon.CognitoSync.SyncManager.Dataset/DatasetMergedDelegate::BeginInvoke(Amazon.CognitoSync.SyncManager.Dataset,System.Collections.Generic.List`1<System.String>,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DatasetMergedDelegate_BeginInvoke_m1569511369 (DatasetMergedDelegate_t713795432 * __this, Dataset_t3040902086 * ___dataset0, List_1_t1398341365 * ___datasetNames1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.CognitoSync.SyncManager.Dataset/DatasetMergedDelegate::EndInvoke(System.IAsyncResult)
extern "C"  bool DatasetMergedDelegate_EndInvoke_m799509932 (DatasetMergedDelegate_t713795432 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
