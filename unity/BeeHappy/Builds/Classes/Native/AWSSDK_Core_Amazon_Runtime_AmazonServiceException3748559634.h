﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Exception1927440687.h"
#include "AWSSDK_Core_Amazon_Runtime_ErrorType1448377524.h"
#include "System_System_Net_HttpStatusCode1898409641.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.AmazonServiceException
struct  AmazonServiceException_t3748559634  : public Exception_t1927440687
{
public:
	// Amazon.Runtime.ErrorType Amazon.Runtime.AmazonServiceException::errorType
	int32_t ___errorType_11;
	// System.String Amazon.Runtime.AmazonServiceException::errorCode
	String_t* ___errorCode_12;
	// System.String Amazon.Runtime.AmazonServiceException::requestId
	String_t* ___requestId_13;
	// System.Net.HttpStatusCode Amazon.Runtime.AmazonServiceException::statusCode
	int32_t ___statusCode_14;

public:
	inline static int32_t get_offset_of_errorType_11() { return static_cast<int32_t>(offsetof(AmazonServiceException_t3748559634, ___errorType_11)); }
	inline int32_t get_errorType_11() const { return ___errorType_11; }
	inline int32_t* get_address_of_errorType_11() { return &___errorType_11; }
	inline void set_errorType_11(int32_t value)
	{
		___errorType_11 = value;
	}

	inline static int32_t get_offset_of_errorCode_12() { return static_cast<int32_t>(offsetof(AmazonServiceException_t3748559634, ___errorCode_12)); }
	inline String_t* get_errorCode_12() const { return ___errorCode_12; }
	inline String_t** get_address_of_errorCode_12() { return &___errorCode_12; }
	inline void set_errorCode_12(String_t* value)
	{
		___errorCode_12 = value;
		Il2CppCodeGenWriteBarrier(&___errorCode_12, value);
	}

	inline static int32_t get_offset_of_requestId_13() { return static_cast<int32_t>(offsetof(AmazonServiceException_t3748559634, ___requestId_13)); }
	inline String_t* get_requestId_13() const { return ___requestId_13; }
	inline String_t** get_address_of_requestId_13() { return &___requestId_13; }
	inline void set_requestId_13(String_t* value)
	{
		___requestId_13 = value;
		Il2CppCodeGenWriteBarrier(&___requestId_13, value);
	}

	inline static int32_t get_offset_of_statusCode_14() { return static_cast<int32_t>(offsetof(AmazonServiceException_t3748559634, ___statusCode_14)); }
	inline int32_t get_statusCode_14() const { return ___statusCode_14; }
	inline int32_t* get_address_of_statusCode_14() { return &___statusCode_14; }
	inline void set_statusCode_14(int32_t value)
	{
		___statusCode_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
