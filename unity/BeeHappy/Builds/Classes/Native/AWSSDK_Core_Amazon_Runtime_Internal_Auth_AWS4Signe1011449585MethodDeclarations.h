﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.Auth.AWS4Signer
struct AWS4Signer_t1011449585;
// Amazon.Runtime.Internal.IRequest
struct IRequest_t2400804350;
// Amazon.Runtime.IClientConfig
struct IClientConfig_t4078933728;
// Amazon.Runtime.Internal.Util.RequestMetrics
struct RequestMetrics_t218029284;
// System.String
struct String_t;
// Amazon.Runtime.Internal.Auth.AWS4SigningResult
struct AWS4SigningResult_t430803065;
// System.Collections.Generic.IDictionary`2<System.String,System.String>
struct IDictionary_2_t1943082916;
// System.Uri
struct Uri_t19570940;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// Amazon.RegionEndpoint
struct RegionEndpoint_t661522805;
// System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>
struct IEnumerable_1_t1993471762;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Auth_ClientPro4005895111.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Util_RequestMet218029284.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_DateTime693205669.h"
#include "System_System_Uri19570940.h"
#include "AWSSDK_Core_Amazon_Runtime_SigningAlgorithm3740229458.h"
#include "AWSSDK_Core_Amazon_RegionEndpoint661522805.h"

// Amazon.Runtime.Internal.Auth.ClientProtocol Amazon.Runtime.Internal.Auth.AWS4Signer::get_Protocol()
extern "C"  int32_t AWS4Signer_get_Protocol_m2096966557 (AWS4Signer_t1011449585 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Auth.AWS4Signer::Sign(Amazon.Runtime.Internal.IRequest,Amazon.Runtime.IClientConfig,Amazon.Runtime.Internal.Util.RequestMetrics,System.String,System.String)
extern "C"  void AWS4Signer_Sign_m3558264876 (AWS4Signer_t1011449585 * __this, Il2CppObject * ___request0, Il2CppObject * ___clientConfig1, RequestMetrics_t218029284 * ___metrics2, String_t* ___awsAccessKeyId3, String_t* ___awsSecretAccessKey4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.Internal.Auth.AWS4SigningResult Amazon.Runtime.Internal.Auth.AWS4Signer::SignRequest(Amazon.Runtime.Internal.IRequest,Amazon.Runtime.IClientConfig,Amazon.Runtime.Internal.Util.RequestMetrics,System.String,System.String)
extern "C"  AWS4SigningResult_t430803065 * AWS4Signer_SignRequest_m332746686 (AWS4Signer_t1011449585 * __this, Il2CppObject * ___request0, Il2CppObject * ___clientConfig1, RequestMetrics_t218029284 * ___metrics2, String_t* ___awsAccessKeyId3, String_t* ___awsSecretAccessKey4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime Amazon.Runtime.Internal.Auth.AWS4Signer::InitializeHeaders(System.Collections.Generic.IDictionary`2<System.String,System.String>,System.Uri)
extern "C"  DateTime_t693205669  AWS4Signer_InitializeHeaders_m3292004365 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___headers0, Uri_t19570940 * ___requestEndpoint1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime Amazon.Runtime.Internal.Auth.AWS4Signer::InitializeHeaders(System.Collections.Generic.IDictionary`2<System.String,System.String>,System.Uri,System.DateTime)
extern "C"  DateTime_t693205669  AWS4Signer_InitializeHeaders_m3890887611 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___headers0, Uri_t19570940 * ___requestEndpoint1, DateTime_t693205669  ___requestDateTime2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Auth.AWS4Signer::CleanHeaders(System.Collections.Generic.IDictionary`2<System.String,System.String>)
extern "C"  void AWS4Signer_CleanHeaders_m2822253474 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___headers0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.Internal.Auth.AWS4SigningResult Amazon.Runtime.Internal.Auth.AWS4Signer::ComputeSignature(System.String,System.String,System.String,System.DateTime,System.String,System.String,System.String,Amazon.Runtime.Internal.Util.RequestMetrics)
extern "C"  AWS4SigningResult_t430803065 * AWS4Signer_ComputeSignature_m2118039702 (Il2CppObject * __this /* static, unused */, String_t* ___awsAccessKey0, String_t* ___awsSecretAccessKey1, String_t* ___region2, DateTime_t693205669  ___signedAt3, String_t* ___service4, String_t* ___signedHeaders5, String_t* ___canonicalRequest6, RequestMetrics_t218029284 * ___metrics7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Runtime.Internal.Auth.AWS4Signer::FormatDateTime(System.DateTime,System.String)
extern "C"  String_t* AWS4Signer_FormatDateTime_m2751369886 (Il2CppObject * __this /* static, unused */, DateTime_t693205669  ___dt0, String_t* ___formatString1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Amazon.Runtime.Internal.Auth.AWS4Signer::ComposeSigningKey(System.String,System.String,System.String,System.String)
extern "C"  ByteU5BU5D_t3397334013* AWS4Signer_ComposeSigningKey_m2920935411 (Il2CppObject * __this /* static, unused */, String_t* ___awsSecretAccessKey0, String_t* ___region1, String_t* ___date2, String_t* ___service3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Runtime.Internal.Auth.AWS4Signer::SetRequestBodyHash(Amazon.Runtime.Internal.IRequest)
extern "C"  String_t* AWS4Signer_SetRequestBodyHash_m365635736 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___request0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Amazon.Runtime.Internal.Auth.AWS4Signer::SignBlob(System.Byte[],System.String)
extern "C"  ByteU5BU5D_t3397334013* AWS4Signer_SignBlob_m4147237546 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___key0, String_t* ___data1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Amazon.Runtime.Internal.Auth.AWS4Signer::SignBlob(System.Byte[],System.Byte[])
extern "C"  ByteU5BU5D_t3397334013* AWS4Signer_SignBlob_m215152411 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___key0, ByteU5BU5D_t3397334013* ___data1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Amazon.Runtime.Internal.Auth.AWS4Signer::ComputeKeyedHash(Amazon.Runtime.SigningAlgorithm,System.Byte[],System.String)
extern "C"  ByteU5BU5D_t3397334013* AWS4Signer_ComputeKeyedHash_m2533075637 (Il2CppObject * __this /* static, unused */, int32_t ___algorithm0, ByteU5BU5D_t3397334013* ___key1, String_t* ___data2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Amazon.Runtime.Internal.Auth.AWS4Signer::ComputeKeyedHash(Amazon.Runtime.SigningAlgorithm,System.Byte[],System.Byte[])
extern "C"  ByteU5BU5D_t3397334013* AWS4Signer_ComputeKeyedHash_m445560474 (Il2CppObject * __this /* static, unused */, int32_t ___algorithm0, ByteU5BU5D_t3397334013* ___key1, ByteU5BU5D_t3397334013* ___data2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Amazon.Runtime.Internal.Auth.AWS4Signer::ComputeHash(System.String)
extern "C"  ByteU5BU5D_t3397334013* AWS4Signer_ComputeHash_m1424626006 (Il2CppObject * __this /* static, unused */, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Amazon.Runtime.Internal.Auth.AWS4Signer::ComputeHash(System.Byte[])
extern "C"  ByteU5BU5D_t3397334013* AWS4Signer_ComputeHash_m2538890683 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Runtime.Internal.Auth.AWS4Signer::DetermineSigningRegion(Amazon.Runtime.IClientConfig,System.String,Amazon.RegionEndpoint,Amazon.Runtime.Internal.IRequest)
extern "C"  String_t* AWS4Signer_DetermineSigningRegion_m1715143186 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___clientConfig0, String_t* ___serviceName1, RegionEndpoint_t661522805 * ___alternateEndpoint2, Il2CppObject * ___request3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Runtime.Internal.Auth.AWS4Signer::DetermineService(Amazon.Runtime.IClientConfig)
extern "C"  String_t* AWS4Signer_DetermineService_m1773627804 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___clientConfig0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Runtime.Internal.Auth.AWS4Signer::CanonicalizeRequest(System.Uri,System.String,System.String,System.Collections.Generic.IDictionary`2<System.String,System.String>,System.String,System.String)
extern "C"  String_t* AWS4Signer_CanonicalizeRequest_m4227504778 (Il2CppObject * __this /* static, unused */, Uri_t19570940 * ___endpoint0, String_t* ___resourcePath1, String_t* ___httpMethod2, Il2CppObject* ___sortedHeaders3, String_t* ___canonicalQueryString4, String_t* ___precomputedBodyHash5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.Runtime.Internal.Auth.AWS4Signer::SortHeaders(System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>)
extern "C"  Il2CppObject* AWS4Signer_SortHeaders_m2876594912 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___requestHeaders0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Runtime.Internal.Auth.AWS4Signer::CanonicalizeHeaders(System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>)
extern "C"  String_t* AWS4Signer_CanonicalizeHeaders_m2370890140 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___sortedHeaders0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Runtime.Internal.Auth.AWS4Signer::CanonicalizeHeaderNames(System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>)
extern "C"  String_t* AWS4Signer_CanonicalizeHeaderNames_m4034853875 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___sortedHeaders0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.Runtime.Internal.Auth.AWS4Signer::GetParametersToCanonicalize(Amazon.Runtime.Internal.IRequest)
extern "C"  Il2CppObject* AWS4Signer_GetParametersToCanonicalize_m1197790850 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___request0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Runtime.Internal.Auth.AWS4Signer::CanonicalizeQueryParameters(System.Collections.Generic.IDictionary`2<System.String,System.String>)
extern "C"  String_t* AWS4Signer_CanonicalizeQueryParameters_m667677574 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___parameters0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Runtime.Internal.Auth.AWS4Signer::CanonicalizeQueryParameters(System.Collections.Generic.IDictionary`2<System.String,System.String>,System.Boolean)
extern "C"  String_t* AWS4Signer_CanonicalizeQueryParameters_m123918983 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___parameters0, bool ___uriEncodeParameters1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Runtime.Internal.Auth.AWS4Signer::CompressSpaces(System.String)
extern "C"  String_t* AWS4Signer_CompressSpaces_m1937608613 (Il2CppObject * __this /* static, unused */, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Amazon.Runtime.Internal.Auth.AWS4Signer::GetRequestPayloadBytes(Amazon.Runtime.Internal.IRequest)
extern "C"  ByteU5BU5D_t3397334013* AWS4Signer_GetRequestPayloadBytes_m2523733126 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___request0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Auth.AWS4Signer::.ctor()
extern "C"  void AWS4Signer__ctor_m1541432815 (AWS4Signer_t1011449585 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Auth.AWS4Signer::.cctor()
extern "C"  void AWS4Signer__cctor_m3481838980 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
