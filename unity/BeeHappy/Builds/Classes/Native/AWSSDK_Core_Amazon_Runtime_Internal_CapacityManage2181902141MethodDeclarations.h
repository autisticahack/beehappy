﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.CapacityManager
struct CapacityManager_t2181902141;
// Amazon.Runtime.Internal.RetryCapacity
struct RetryCapacity_t2794668894;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_RetryCapacity2794668894.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Amazon.Runtime.Internal.CapacityManager::Dispose()
extern "C"  void CapacityManager_Dispose_m3194449494 (CapacityManager_t2181902141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.CapacityManager::Dispose(System.Boolean)
extern "C"  void CapacityManager_Dispose_m1634075203 (CapacityManager_t2181902141 * __this, bool ___disposing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.CapacityManager::.ctor(System.Int32,System.Int32,System.Int32)
extern "C"  void CapacityManager__ctor_m3694604884 (CapacityManager_t2181902141 * __this, int32_t ___throttleRetryCount0, int32_t ___throttleRetryCost1, int32_t ___throttleCost2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.CapacityManager::TryAcquireCapacity(Amazon.Runtime.Internal.RetryCapacity)
extern "C"  bool CapacityManager_TryAcquireCapacity_m2348150307 (CapacityManager_t2181902141 * __this, RetryCapacity_t2794668894 * ___retryCapacity0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.CapacityManager::TryReleaseCapacity(System.Boolean,Amazon.Runtime.Internal.RetryCapacity)
extern "C"  void CapacityManager_TryReleaseCapacity_m3049727801 (CapacityManager_t2181902141 * __this, bool ___isRetryRequest0, RetryCapacity_t2794668894 * ___retryCapacity1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.Internal.RetryCapacity Amazon.Runtime.Internal.CapacityManager::GetRetryCapacity(System.String)
extern "C"  RetryCapacity_t2794668894 * CapacityManager_GetRetryCapacity_m3107340935 (CapacityManager_t2181902141 * __this, String_t* ___serviceURL0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.CapacityManager::TryGetRetryCapacity(System.String,Amazon.Runtime.Internal.RetryCapacity&)
extern "C"  bool CapacityManager_TryGetRetryCapacity_m2174878029 (CapacityManager_t2181902141 * __this, String_t* ___key0, RetryCapacity_t2794668894 ** ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.Internal.RetryCapacity Amazon.Runtime.Internal.CapacityManager::AddNewRetryCapacity(System.String)
extern "C"  RetryCapacity_t2794668894 * CapacityManager_AddNewRetryCapacity_m2619678828 (CapacityManager_t2181902141 * __this, String_t* ___serviceURL0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.CapacityManager::ReleaseCapacity(System.Int32,Amazon.Runtime.Internal.RetryCapacity)
extern "C"  void CapacityManager_ReleaseCapacity_m3979706196 (Il2CppObject * __this /* static, unused */, int32_t ___capacity0, RetryCapacity_t2794668894 * ___retryCapacity1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.CapacityManager::.cctor()
extern "C"  void CapacityManager__cctor_m771662746 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
