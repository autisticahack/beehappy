﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>
struct Dictionary_2_t3417634846;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.Dictionary`2<System.Object,System.Int32>
struct Dictionary_2_t1663937576;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t2281509423;
// System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>
struct Dictionary_2_t727138142;
// System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>
struct Dictionary_2_t3650197868;
// System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>
struct Dictionary_2_t2730857826;
// System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,System.Int64>
struct KeyCollection_t1847687001;
// System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Int64>
struct Dictionary_2_t3659156526;
// System.Collections.Generic.IEnumerator`1<Amazon.Runtime.Metric>
struct IEnumerator_1_t748964029;
// System.Array
struct Il2CppArray;
// System.Collections.Generic.Dictionary`2/Transform`1<Amazon.Runtime.Metric,System.Int64,Amazon.Runtime.Metric>
struct Transform_1_t3638058609;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// Amazon.Runtime.Metric[]
struct MetricU5BU5D_t2477685199;
// System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,System.Object>
struct KeyCollection_t3628058259;
// System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Object>
struct Dictionary_2_t1144560488;
// System.Collections.Generic.Dictionary`2/Transform`1<Amazon.Runtime.Metric,System.Object,Amazon.Runtime.Metric>
struct Transform_1_t2240600335;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Int32>
struct KeyCollection_t3563200854;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t1079703083;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t3842368571;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Int32>
struct Transform_1_t2737799006;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Object>
struct KeyCollection_t4180772701;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Object>
struct Dictionary_2_t1697274930;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Int32>
struct Transform_1_t3322033499;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Object>
struct KeyCollection_t1867498172;
// System.Collections.Generic.Dictionary`2<System.Int64,System.Object>
struct Dictionary_2_t3678967697;
// System.Collections.Generic.IEnumerator`1<System.Int64>
struct IEnumerator_1_t2679569160;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Object,System.Int64>
struct Transform_1_t216544365;
// System.Int64[]
struct Int64U5BU5D_t717125112;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Object>
struct KeyCollection_t1320005088;
// System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>
struct Dictionary_2_t3131474613;
// System.Collections.Generic.IEnumerator`1<System.IntPtr>
struct IEnumerator_1_t4274551732;
// System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Object,System.IntPtr>
struct Transform_1_t2168890597;
// System.IntPtr[]
struct IntPtrU5BU5D_t169632028;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>
struct KeyCollection_t3708365277;
// System.Collections.Generic.Dictionary`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>
struct Dictionary_2_t1224867506;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel,System.Object>
struct Transform_1_t4196310754;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Boolean>
struct KeyCollection_t1606165321;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Object>
struct Transform_1_t1854428742;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Int32>
struct KeyCollection_t4147435347;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Object>
struct Transform_1_t1316794068;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Object>
struct KeyCollection_t470039898;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Object>
struct Transform_1_t1901028561;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>
struct KeyCollection_t3210635913;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ThirdParty.Json.LitJson.ArrayMetadata,System.Object>
struct Transform_1_t2213661190;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>
struct KeyCollection_t1838728343;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ThirdParty.Json.LitJson.ObjectMetadata,System.Object>
struct Transform_1_t4135956480;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,UnityEngine.TextEditor/TextEditOp>
struct KeyCollection_t919388301;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.TextEditor/TextEditOp,System.Object>
struct Transform_1_t4054574962;
// System.Collections.Generic.Dictionary`2/ShimEnumerator<Amazon.Runtime.Metric,System.Int64>
struct ShimEnumerator_t3764281347;
// System.Collections.Generic.Dictionary`2/ShimEnumerator<Amazon.Runtime.Metric,System.Object>
struct ShimEnumerator_t1249685309;
// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>
struct ShimEnumerator_t1184827904;
// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>
struct ShimEnumerator_t1802399751;
// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int64,System.Object>
struct ShimEnumerator_t3784092518;
// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.IntPtr,System.Object>
struct ShimEnumerator_t3236599434;
// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>
struct ShimEnumerator_t1329992327;
// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>
struct ShimEnumerator_t3522759667;
// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>
struct ShimEnumerator_t1769062397;
// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>
struct ShimEnumerator_t2386634244;
// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>
struct ShimEnumerator_t832262963;
// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>
struct ShimEnumerator_t3755322689;
// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>
struct ShimEnumerator_t2835982647;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// System.Collections.Generic.Dictionary`2/Transform`1<Amazon.Runtime.Metric,System.Int64,System.Collections.DictionaryEntry>
struct Transform_1_t3413493805;
// System.Collections.Generic.Dictionary`2/Transform`1<Amazon.Runtime.Metric,System.Int64,System.Collections.Generic.KeyValuePair`2<Amazon.Runtime.Metric,System.Int64>>
struct Transform_1_t1781120155;
// System.Collections.Generic.Dictionary`2/Transform`1<Amazon.Runtime.Metric,System.Int64,System.Int64>
struct Transform_1_t1273696444;
// System.Collections.Generic.Dictionary`2/Transform`1<Amazon.Runtime.Metric,System.Object,System.Collections.DictionaryEntry>
struct Transform_1_t2016035531;
// System.Collections.Generic.Dictionary`2/Transform`1<Amazon.Runtime.Metric,System.Object,System.Collections.Generic.KeyValuePair`2<Amazon.Runtime.Metric,System.Object>>
struct Transform_1_t2164033139;
// System.Collections.Generic.Dictionary`2/Transform`1<Amazon.Runtime.Metric,System.Object,System.Object>
struct Transform_1_t1656609428;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Collections.DictionaryEntry>
struct Transform_1_t3714796956;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>
struct Transform_1_t3797937159;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Collections.DictionaryEntry>
struct Transform_1_t4064153;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>
struct Transform_1_t704776203;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Object>
struct Transform_1_t3939605346;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Object,System.Collections.DictionaryEntry>
struct Transform_1_t2356341726;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Object,System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>>
struct Transform_1_t743779247;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Object,System.Object>
struct Transform_1_t1996915623;
// System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Object,System.Collections.DictionaryEntry>
struct Transform_1_t2713705386;
// System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Object,System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>>
struct Transform_1_t553649823;
// System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Object,System.Object>
struct Transform_1_t2354279283;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>
struct Transform_1_t3139668837;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel,System.Collections.DictionaryEntry>
struct Transform_1_t260769561;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel,System.Collections.Generic.KeyValuePair`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>>
struct Transform_1_t489074187;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Boolean>
struct Transform_1_t2990554165;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Collections.DictionaryEntry>
struct Transform_1_t2213854845;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>
struct Transform_1_t339959515;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Collections.DictionaryEntry>
struct Transform_1_t1676220171;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>
struct Transform_1_t2343594867;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Int32>
struct Transform_1_t699222221;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Collections.DictionaryEntry>
struct Transform_1_t2260454664;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct Transform_1_t3545401207;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ThirdParty.Json.LitJson.ArrayMetadata,System.Collections.DictionaryEntry>
struct Transform_1_t2573087293;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ThirdParty.Json.LitJson.ArrayMetadata,System.Collections.Generic.KeyValuePair`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>>
struct Transform_1_t2303662555;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ThirdParty.Json.LitJson.ArrayMetadata,ThirdParty.Json.LitJson.ArrayMetadata>
struct Transform_1_t659289909;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ThirdParty.Json.LitJson.ObjectMetadata,System.Collections.DictionaryEntry>
struct Transform_1_t200415287;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ThirdParty.Json.LitJson.ObjectMetadata,System.Collections.Generic.KeyValuePair`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>>
struct Transform_1_t2854050275;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ThirdParty.Json.LitJson.ObjectMetadata,ThirdParty.Json.LitJson.ObjectMetadata>
struct Transform_1_t1209677629;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.TextEditor/TextEditOp,System.Collections.DictionaryEntry>
struct Transform_1_t119033769;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.TextEditor/TextEditOp,System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>
struct Transform_1_t1853328715;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.TextEditor/TextEditOp,UnityEngine.TextEditor/TextEditOp>
struct Transform_1_t208956069;
// System.Collections.Generic.Dictionary`2/ValueCollection<Amazon.Runtime.Metric,System.Int64>
struct ValueCollection_t2362216369;
// System.Collections.Generic.Dictionary`2/ValueCollection<Amazon.Runtime.Metric,System.Object>
struct ValueCollection_t4142587627;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Int32>
struct ValueCollection_t4077730222;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Object>
struct ValueCollection_t400334773;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,System.Object>
struct ValueCollection_t2382027540;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1812170988.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1812170988MethodDeclarations.h"
#include "mscorlib_System_Void1841601450.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3417634846.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3417634846MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En442692252.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En442692252MethodDeclarations.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21174980068.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21174980068MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Key58473718.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Key58473718MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1663937576.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1663937576MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2983962278.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2983962278MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23716250094.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23716250094MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke676045565.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke676045565MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2281509423.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2281509423MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3601534125.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3601534125MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3416641580.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3416641580MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge727138142.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge727138142MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2047162844.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2047162844MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22779450660.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22779450660MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2044734010.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2044734010MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3650197868.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3650197868MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En675255274.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En675255274MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21407543090.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21407543090MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1125393968.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1125393968MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2730857826.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2730857826MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E4050882528.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E4050882528MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_488203048.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_488203048MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1847687001.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1847687001MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3659156526.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException628810857MethodDeclarations.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_ArgumentNullException628810857.h"
#include "AWSSDK_Core_Amazon_Runtime_Metric3273440202.h"
#include "mscorlib_System_NotSupportedException1793819818MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1793819818.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3659156526MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2053692668.h"
#include "mscorlib_System_Int322071877448.h"
#include "AWSSDK.Core_ArrayTypes.h"
#include "mscorlib_System_Int64909078037.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3638058609.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3638058609MethodDeclarations.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2053692668MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3628058259.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3628058259MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1144560488.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1144560488MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3834063926.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2240600335.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2240600335MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3834063926MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3563200854.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3563200854MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1079703083.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1079703083MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3769206521.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2737799006.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2737799006MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3769206521MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K4180772701.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K4180772701MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1697274930.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1697274930MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Key91811072.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3322033499.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3322033499MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Key91811072MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1867498172.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1867498172MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3678967697.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3678967697MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2073503839.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr216544365.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr216544365MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2073503839MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1320005088.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1320005088MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3131474613.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3131474613MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1526010755.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2168890597.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2168890597MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1526010755MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3708365277.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3708365277MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1224867506.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1224867506MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3914370944.h"
#include "AWSSDK_Core_Amazon_Util_Internal_PlatformServices_1632807378.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T4196310754.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T4196310754MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3914370944MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1606165321.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1606165321MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1854428742.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1854428742MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K4147435347.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K4147435347MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1316794068.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1316794068MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke470039898.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke470039898MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1901028561.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1901028561MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3210635913.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3210635913MethodDeclarations.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_ArrayMetadata1135078014.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2213661190.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2213661190MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1838728343.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1838728343MethodDeclarations.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_ObjectMetadata4058137740.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T4135956480.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T4135956480MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke919388301.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke919388301MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp3138797698.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T4054574962.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T4054574962MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S3764281347.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S3764281347MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En684213932.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En684213932MethodDeclarations.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21416501748.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21416501748MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S1249685309.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S1249685309MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2464585190.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2464585190MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23196873006.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23196873006MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S1184827904.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S1184827904MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2399727785.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2399727785MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23132015601.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23132015601MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S1802399751.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S1802399751MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3017299632.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3017299632MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23749587448.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23749587448MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S3784092518.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S3784092518MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En704025103.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En704025103MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21436312919.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21436312919MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S3236599434.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S3236599434MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En156532019.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En156532019MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_888819835.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_888819835MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S1329992327.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S1329992327MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2544892208.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2544892208MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23277180024.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23277180024MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S3522759667.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S3522759667MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S1769062397.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S1769062397MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S2386634244.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S2386634244MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Sh832262963.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Sh832262963MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S3755322689.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S3755322689MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S2835982647.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S2835982647MethodDeclarations.h"
#include "mscorlib_System_AsyncCallback163412349.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3413493805.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3413493805MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1781120155.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1781120155MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1273696444.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1273696444MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2016035531.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2016035531MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2164033139.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2164033139MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1656609428.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1656609428MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3714796956.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3714796956MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3797937159.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3797937159MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tran4064153.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tran4064153MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr704776203.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr704776203MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3939605346.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3939605346MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2356341726.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2356341726MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr743779247.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr743779247MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1996915623.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1996915623MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2713705386.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2713705386MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr553649823.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr553649823MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2354279283.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2354279283MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3139668837.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3139668837MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr260769561.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr260769561MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr489074187.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr489074187MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2990554165.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2990554165MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2213854845.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2213854845MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr339959515.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr339959515MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1676220171.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1676220171MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2343594867.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2343594867MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr699222221.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr699222221MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2260454664.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2260454664MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3545401207.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3545401207MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2573087293.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2573087293MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2303662555.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2303662555MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr659289909.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr659289909MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr200415287.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr200415287MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2854050275.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2854050275MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1209677629.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1209677629MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr119033769.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr119033769MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1853328715.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1853328715MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr208956069.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr208956069MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1050721994.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1050721994MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2831093252.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2831093252MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2766235847.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2766235847MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V3383807694.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V3383807694MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1070533165.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1070533165MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va523040081.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va523040081MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2911400270.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2911400270MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va809200314.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va809200314MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V3350470340.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V3350470340MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V3968042187.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V3968042187MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2413670906.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2413670906MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1041763336.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1041763336MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va122423294.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va122423294MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2362216369.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2362216369MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V4142587627.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V4142587627MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V4077730222.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V4077730222MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va400334773.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va400334773MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2382027540.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2382027540MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Int64>::Do_ICollectionCopyTo<Amazon.Runtime.Metric>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisMetric_t3273440202_m3202304059_gshared (Dictionary_2_t3659156526 * __this, Il2CppArray * p0, int32_t p1, Transform_1_t3638058609 * p2, const MethodInfo* method);
#define Dictionary_2_Do_ICollectionCopyTo_TisMetric_t3273440202_m3202304059(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t3659156526 *, Il2CppArray *, int32_t, Transform_1_t3638058609 *, const MethodInfo*))Dictionary_2_Do_ICollectionCopyTo_TisMetric_t3273440202_m3202304059_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Int64>::Do_CopyTo<Amazon.Runtime.Metric,Amazon.Runtime.Metric>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_CopyTo_TisMetric_t3273440202_TisMetric_t3273440202_m3813448539_gshared (Dictionary_2_t3659156526 * __this, MetricU5BU5D_t2477685199* p0, int32_t p1, Transform_1_t3638058609 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisMetric_t3273440202_TisMetric_t3273440202_m3813448539(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t3659156526 *, MetricU5BU5D_t2477685199*, int32_t, Transform_1_t3638058609 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisMetric_t3273440202_TisMetric_t3273440202_m3813448539_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Object>::Do_ICollectionCopyTo<Amazon.Runtime.Metric>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisMetric_t3273440202_m2940895167_gshared (Dictionary_2_t1144560488 * __this, Il2CppArray * p0, int32_t p1, Transform_1_t2240600335 * p2, const MethodInfo* method);
#define Dictionary_2_Do_ICollectionCopyTo_TisMetric_t3273440202_m2940895167(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t1144560488 *, Il2CppArray *, int32_t, Transform_1_t2240600335 *, const MethodInfo*))Dictionary_2_Do_ICollectionCopyTo_TisMetric_t3273440202_m2940895167_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Object>::Do_CopyTo<Amazon.Runtime.Metric,Amazon.Runtime.Metric>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_CopyTo_TisMetric_t3273440202_TisMetric_t3273440202_m2896860231_gshared (Dictionary_2_t1144560488 * __this, MetricU5BU5D_t2477685199* p0, int32_t p1, Transform_1_t2240600335 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisMetric_t3273440202_TisMetric_t3273440202_m2896860231(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t1144560488 *, MetricU5BU5D_t2477685199*, int32_t, Transform_1_t2240600335 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisMetric_t3273440202_TisMetric_t3273440202_m2896860231_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::Do_ICollectionCopyTo<System.Int32>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisInt32_t2071877448_m4216657666_gshared (Dictionary_2_t1079703083 * __this, Il2CppArray * p0, int32_t p1, Transform_1_t2737799006 * p2, const MethodInfo* method);
#define Dictionary_2_Do_ICollectionCopyTo_TisInt32_t2071877448_m4216657666(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t1079703083 *, Il2CppArray *, int32_t, Transform_1_t2737799006 *, const MethodInfo*))Dictionary_2_Do_ICollectionCopyTo_TisInt32_t2071877448_m4216657666_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::Do_CopyTo<System.Int32,System.Int32>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_CopyTo_TisInt32_t2071877448_TisInt32_t2071877448_m2922514531_gshared (Dictionary_2_t1079703083 * __this, Int32U5BU5D_t3030399641* p0, int32_t p1, Transform_1_t2737799006 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisInt32_t2071877448_TisInt32_t2071877448_m2922514531(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t1079703083 *, Int32U5BU5D_t3030399641*, int32_t, Transform_1_t2737799006 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisInt32_t2071877448_TisInt32_t2071877448_m2922514531_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::Do_ICollectionCopyTo<System.Int32>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisInt32_t2071877448_m1526703923_gshared (Dictionary_2_t1697274930 * __this, Il2CppArray * p0, int32_t p1, Transform_1_t3322033499 * p2, const MethodInfo* method);
#define Dictionary_2_Do_ICollectionCopyTo_TisInt32_t2071877448_m1526703923(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t1697274930 *, Il2CppArray *, int32_t, Transform_1_t3322033499 *, const MethodInfo*))Dictionary_2_Do_ICollectionCopyTo_TisInt32_t2071877448_m1526703923_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::Do_CopyTo<System.Int32,System.Int32>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_CopyTo_TisInt32_t2071877448_TisInt32_t2071877448_m838060550_gshared (Dictionary_2_t1697274930 * __this, Int32U5BU5D_t3030399641* p0, int32_t p1, Transform_1_t3322033499 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisInt32_t2071877448_TisInt32_t2071877448_m838060550(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t1697274930 *, Int32U5BU5D_t3030399641*, int32_t, Transform_1_t3322033499 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisInt32_t2071877448_TisInt32_t2071877448_m838060550_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int64,System.Object>::Do_ICollectionCopyTo<System.Int64>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisInt64_t909078037_m546108627_gshared (Dictionary_2_t3678967697 * __this, Il2CppArray * p0, int32_t p1, Transform_1_t216544365 * p2, const MethodInfo* method);
#define Dictionary_2_Do_ICollectionCopyTo_TisInt64_t909078037_m546108627(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t3678967697 *, Il2CppArray *, int32_t, Transform_1_t216544365 *, const MethodInfo*))Dictionary_2_Do_ICollectionCopyTo_TisInt64_t909078037_m546108627_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int64,System.Object>::Do_CopyTo<System.Int64,System.Int64>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_CopyTo_TisInt64_t909078037_TisInt64_t909078037_m2066450503_gshared (Dictionary_2_t3678967697 * __this, Int64U5BU5D_t717125112* p0, int32_t p1, Transform_1_t216544365 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisInt64_t909078037_TisInt64_t909078037_m2066450503(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t3678967697 *, Int64U5BU5D_t717125112*, int32_t, Transform_1_t216544365 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisInt64_t909078037_TisInt64_t909078037_m2066450503_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>::Do_ICollectionCopyTo<System.IntPtr>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisIntPtr_t_m3403843327_gshared (Dictionary_2_t3131474613 * __this, Il2CppArray * p0, int32_t p1, Transform_1_t2168890597 * p2, const MethodInfo* method);
#define Dictionary_2_Do_ICollectionCopyTo_TisIntPtr_t_m3403843327(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t3131474613 *, Il2CppArray *, int32_t, Transform_1_t2168890597 *, const MethodInfo*))Dictionary_2_Do_ICollectionCopyTo_TisIntPtr_t_m3403843327_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>::Do_CopyTo<System.IntPtr,System.IntPtr>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_CopyTo_TisIntPtr_t_TisIntPtr_t_m3955064935_gshared (Dictionary_2_t3131474613 * __this, IntPtrU5BU5D_t169632028* p0, int32_t p1, Transform_1_t2168890597 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisIntPtr_t_TisIntPtr_t_m3955064935(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t3131474613 *, IntPtrU5BU5D_t169632028*, int32_t, Transform_1_t2168890597 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisIntPtr_t_TisIntPtr_t_m3955064935_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::Do_ICollectionCopyTo<System.Object>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m2066586955_gshared (Dictionary_2_t1224867506 * __this, Il2CppArray * p0, int32_t p1, Transform_1_t4196310754 * p2, const MethodInfo* method);
#define Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m2066586955(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t1224867506 *, Il2CppArray *, int32_t, Transform_1_t4196310754 *, const MethodInfo*))Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m2066586955_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::Do_CopyTo<System.Object,System.Object>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_CopyTo_TisIl2CppObject_TisIl2CppObject_m4107372367_gshared (Dictionary_2_t1224867506 * __this, ObjectU5BU5D_t3614634134* p0, int32_t p1, Transform_1_t4196310754 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisIl2CppObject_TisIl2CppObject_m4107372367(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t1224867506 *, ObjectU5BU5D_t3614634134*, int32_t, Transform_1_t4196310754 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisIl2CppObject_TisIl2CppObject_m4107372367_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::Do_ICollectionCopyTo<System.Object>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m1664293864_gshared (Dictionary_2_t3417634846 * __this, Il2CppArray * p0, int32_t p1, Transform_1_t1854428742 * p2, const MethodInfo* method);
#define Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m1664293864(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t3417634846 *, Il2CppArray *, int32_t, Transform_1_t1854428742 *, const MethodInfo*))Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m1664293864_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::Do_CopyTo<System.Object,System.Object>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_CopyTo_TisIl2CppObject_TisIl2CppObject_m3704460300_gshared (Dictionary_2_t3417634846 * __this, ObjectU5BU5D_t3614634134* p0, int32_t p1, Transform_1_t1854428742 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisIl2CppObject_TisIl2CppObject_m3704460300(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t3417634846 *, ObjectU5BU5D_t3614634134*, int32_t, Transform_1_t1854428742 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisIl2CppObject_TisIl2CppObject_m3704460300_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::Do_ICollectionCopyTo<System.Object>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m63223048_gshared (Dictionary_2_t1663937576 * __this, Il2CppArray * p0, int32_t p1, Transform_1_t1316794068 * p2, const MethodInfo* method);
#define Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m63223048(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t1663937576 *, Il2CppArray *, int32_t, Transform_1_t1316794068 *, const MethodInfo*))Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m63223048_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::Do_CopyTo<System.Object,System.Object>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_CopyTo_TisIl2CppObject_TisIl2CppObject_m3552744036_gshared (Dictionary_2_t1663937576 * __this, ObjectU5BU5D_t3614634134* p0, int32_t p1, Transform_1_t1316794068 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisIl2CppObject_TisIl2CppObject_m3552744036(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t1663937576 *, ObjectU5BU5D_t3614634134*, int32_t, Transform_1_t1316794068 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisIl2CppObject_TisIl2CppObject_m3552744036_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Do_ICollectionCopyTo<System.Object>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m4092802079_gshared (Dictionary_2_t2281509423 * __this, Il2CppArray * p0, int32_t p1, Transform_1_t1901028561 * p2, const MethodInfo* method);
#define Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m4092802079(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t2281509423 *, Il2CppArray *, int32_t, Transform_1_t1901028561 *, const MethodInfo*))Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m4092802079_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Do_CopyTo<System.Object,System.Object>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_CopyTo_TisIl2CppObject_TisIl2CppObject_m1804181923_gshared (Dictionary_2_t2281509423 * __this, ObjectU5BU5D_t3614634134* p0, int32_t p1, Transform_1_t1901028561 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisIl2CppObject_TisIl2CppObject_m1804181923(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t2281509423 *, ObjectU5BU5D_t3614634134*, int32_t, Transform_1_t1901028561 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisIl2CppObject_TisIl2CppObject_m1804181923_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::Do_ICollectionCopyTo<System.Object>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m142106703_gshared (Dictionary_2_t727138142 * __this, Il2CppArray * p0, int32_t p1, Transform_1_t2213661190 * p2, const MethodInfo* method);
#define Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m142106703(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t727138142 *, Il2CppArray *, int32_t, Transform_1_t2213661190 *, const MethodInfo*))Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m142106703_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::Do_CopyTo<System.Object,System.Object>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_CopyTo_TisIl2CppObject_TisIl2CppObject_m3560194779_gshared (Dictionary_2_t727138142 * __this, ObjectU5BU5D_t3614634134* p0, int32_t p1, Transform_1_t2213661190 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisIl2CppObject_TisIl2CppObject_m3560194779(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t727138142 *, ObjectU5BU5D_t3614634134*, int32_t, Transform_1_t2213661190 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisIl2CppObject_TisIl2CppObject_m3560194779_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::Do_ICollectionCopyTo<System.Object>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m1869012627_gshared (Dictionary_2_t3650197868 * __this, Il2CppArray * p0, int32_t p1, Transform_1_t4135956480 * p2, const MethodInfo* method);
#define Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m1869012627(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t3650197868 *, Il2CppArray *, int32_t, Transform_1_t4135956480 *, const MethodInfo*))Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m1869012627_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::Do_CopyTo<System.Object,System.Object>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_CopyTo_TisIl2CppObject_TisIl2CppObject_m3775759503_gshared (Dictionary_2_t3650197868 * __this, ObjectU5BU5D_t3614634134* p0, int32_t p1, Transform_1_t4135956480 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisIl2CppObject_TisIl2CppObject_m3775759503(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t3650197868 *, ObjectU5BU5D_t3614634134*, int32_t, Transform_1_t4135956480 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisIl2CppObject_TisIl2CppObject_m3775759503_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::Do_ICollectionCopyTo<System.Object>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m3006115099_gshared (Dictionary_2_t2730857826 * __this, Il2CppArray * p0, int32_t p1, Transform_1_t4054574962 * p2, const MethodInfo* method);
#define Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m3006115099(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t2730857826 *, Il2CppArray *, int32_t, Transform_1_t4054574962 *, const MethodInfo*))Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m3006115099_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::Do_CopyTo<System.Object,System.Object>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_CopyTo_TisIl2CppObject_TisIl2CppObject_m2739099807_gshared (Dictionary_2_t2730857826 * __this, ObjectU5BU5D_t3614634134* p0, int32_t p1, Transform_1_t4054574962 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisIl2CppObject_TisIl2CppObject_m2739099807(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t2730857826 *, ObjectU5BU5D_t3614634134*, int32_t, Transform_1_t4054574962 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisIl2CppObject_TisIl2CppObject_m2739099807_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Int64>::Do_ICollectionCopyTo<System.Int64>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisInt64_t909078037_m1499713103_gshared (Dictionary_2_t3659156526 * __this, Il2CppArray * p0, int32_t p1, Transform_1_t1273696444 * p2, const MethodInfo* method);
#define Dictionary_2_Do_ICollectionCopyTo_TisInt64_t909078037_m1499713103(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t3659156526 *, Il2CppArray *, int32_t, Transform_1_t1273696444 *, const MethodInfo*))Dictionary_2_Do_ICollectionCopyTo_TisInt64_t909078037_m1499713103_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Int64>::Do_CopyTo<System.Int64,System.Int64>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_CopyTo_TisInt64_t909078037_TisInt64_t909078037_m2077765515_gshared (Dictionary_2_t3659156526 * __this, Int64U5BU5D_t717125112* p0, int32_t p1, Transform_1_t1273696444 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisInt64_t909078037_TisInt64_t909078037_m2077765515(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t3659156526 *, Int64U5BU5D_t717125112*, int32_t, Transform_1_t1273696444 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisInt64_t909078037_TisInt64_t909078037_m2077765515_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Object>::Do_ICollectionCopyTo<System.Object>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m2575290059_gshared (Dictionary_2_t1144560488 * __this, Il2CppArray * p0, int32_t p1, Transform_1_t1656609428 * p2, const MethodInfo* method);
#define Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m2575290059(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t1144560488 *, Il2CppArray *, int32_t, Transform_1_t1656609428 *, const MethodInfo*))Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m2575290059_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Object>::Do_CopyTo<System.Object,System.Object>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_CopyTo_TisIl2CppObject_TisIl2CppObject_m3597547783_gshared (Dictionary_2_t1144560488 * __this, ObjectU5BU5D_t3614634134* p0, int32_t p1, Transform_1_t1656609428 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisIl2CppObject_TisIl2CppObject_m3597547783(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t1144560488 *, ObjectU5BU5D_t3614634134*, int32_t, Transform_1_t1656609428 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisIl2CppObject_TisIl2CppObject_m3597547783_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::Do_ICollectionCopyTo<System.Object>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m545661084_gshared (Dictionary_2_t1697274930 * __this, Il2CppArray * p0, int32_t p1, Transform_1_t3939605346 * p2, const MethodInfo* method);
#define Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m545661084(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t1697274930 *, Il2CppArray *, int32_t, Transform_1_t3939605346 *, const MethodInfo*))Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m545661084_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::Do_CopyTo<System.Object,System.Object>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_CopyTo_TisIl2CppObject_TisIl2CppObject_m2625001464_gshared (Dictionary_2_t1697274930 * __this, ObjectU5BU5D_t3614634134* p0, int32_t p1, Transform_1_t3939605346 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisIl2CppObject_TisIl2CppObject_m2625001464(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t1697274930 *, ObjectU5BU5D_t3614634134*, int32_t, Transform_1_t3939605346 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisIl2CppObject_TisIl2CppObject_m2625001464_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int64,System.Object>::Do_ICollectionCopyTo<System.Object>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m589606899_gshared (Dictionary_2_t3678967697 * __this, Il2CppArray * p0, int32_t p1, Transform_1_t1996915623 * p2, const MethodInfo* method);
#define Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m589606899(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t3678967697 *, Il2CppArray *, int32_t, Transform_1_t1996915623 *, const MethodInfo*))Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m589606899_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int64,System.Object>::Do_CopyTo<System.Object,System.Object>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_CopyTo_TisIl2CppObject_TisIl2CppObject_m2574905031_gshared (Dictionary_2_t3678967697 * __this, ObjectU5BU5D_t3614634134* p0, int32_t p1, Transform_1_t1996915623 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisIl2CppObject_TisIl2CppObject_m2574905031(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t3678967697 *, ObjectU5BU5D_t3614634134*, int32_t, Transform_1_t1996915623 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisIl2CppObject_TisIl2CppObject_m2574905031_gshared)(__this, p0, p1, p2, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m4035163080_gshared (Enumerator_t1812170988 * __this, Dictionary_2_t3417634846 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t3417634846 * L_0 = ___host0;
		NullCheck((Dictionary_2_t3417634846 *)L_0);
		Enumerator_t442692252  L_1 = ((  Enumerator_t442692252  (*) (Dictionary_2_t3417634846 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t3417634846 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m4035163080_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t3417634846 * ___host0, const MethodInfo* method)
{
	Enumerator_t1812170988 * _thisAdjusted = reinterpret_cast<Enumerator_t1812170988 *>(__this + 1);
	Enumerator__ctor_m4035163080(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Boolean>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3424705613_gshared (Enumerator_t1812170988 * __this, const MethodInfo* method)
{
	{
		Enumerator_t442692252 * L_0 = (Enumerator_t442692252 *)__this->get_address_of_host_enumerator_0();
		Il2CppObject * L_1 = Enumerator_get_CurrentKey_m565000604((Enumerator_t442692252 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3424705613_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1812170988 * _thisAdjusted = reinterpret_cast<Enumerator_t1812170988 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m3424705613(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Boolean>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2102606953_gshared (Enumerator_t1812170988 * __this, const MethodInfo* method)
{
	{
		Enumerator_t442692252 * L_0 = (Enumerator_t442692252 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m3115320746((Enumerator_t442692252 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2102606953_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1812170988 * _thisAdjusted = reinterpret_cast<Enumerator_t1812170988 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m2102606953(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Boolean>::Dispose()
extern "C"  void Enumerator_Dispose_m4156741384_gshared (Enumerator_t1812170988 * __this, const MethodInfo* method)
{
	{
		Enumerator_t442692252 * L_0 = (Enumerator_t442692252 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m2711120408((Enumerator_t442692252 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m4156741384_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1812170988 * _thisAdjusted = reinterpret_cast<Enumerator_t1812170988 *>(__this + 1);
	Enumerator_Dispose_m4156741384(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Boolean>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3293466169_gshared (Enumerator_t1812170988 * __this, const MethodInfo* method)
{
	{
		Enumerator_t442692252 * L_0 = (Enumerator_t442692252 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m1856697671((Enumerator_t442692252 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m3293466169_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1812170988 * _thisAdjusted = reinterpret_cast<Enumerator_t1812170988 *>(__this + 1);
	return Enumerator_MoveNext_m3293466169(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Boolean>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m3073091827_gshared (Enumerator_t1812170988 * __this, const MethodInfo* method)
{
	{
		Enumerator_t442692252 * L_0 = (Enumerator_t442692252 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t1174980068 * L_1 = (KeyValuePair_2_t1174980068 *)L_0->get_address_of_current_3();
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m2113318928((KeyValuePair_2_t1174980068 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_get_Current_m3073091827_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1812170988 * _thisAdjusted = reinterpret_cast<Enumerator_t1812170988 *>(__this + 1);
	return Enumerator_get_Current_m3073091827(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1642475044_gshared (Enumerator_t58473718 * __this, Dictionary_2_t1663937576 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t1663937576 * L_0 = ___host0;
		NullCheck((Dictionary_2_t1663937576 *)L_0);
		Enumerator_t2983962278  L_1 = ((  Enumerator_t2983962278  (*) (Dictionary_2_t1663937576 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t1663937576 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1642475044_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t1663937576 * ___host0, const MethodInfo* method)
{
	Enumerator_t58473718 * _thisAdjusted = reinterpret_cast<Enumerator_t58473718 *>(__this + 1);
	Enumerator__ctor_m1642475044(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m610493107_gshared (Enumerator_t58473718 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2983962278 * L_0 = (Enumerator_t2983962278 *)__this->get_address_of_host_enumerator_0();
		Il2CppObject * L_1 = Enumerator_get_CurrentKey_m1408186928((Enumerator_t2983962278 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m610493107_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t58473718 * _thisAdjusted = reinterpret_cast<Enumerator_t58473718 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m610493107(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1684647011_gshared (Enumerator_t58473718 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2983962278 * L_0 = (Enumerator_t2983962278 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m1132695838((Enumerator_t2983962278 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1684647011_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t58473718 * _thisAdjusted = reinterpret_cast<Enumerator_t58473718 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m1684647011(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m688620792_gshared (Enumerator_t58473718 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2983962278 * L_0 = (Enumerator_t2983962278 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m401572848((Enumerator_t2983962278 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m688620792_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t58473718 * _thisAdjusted = reinterpret_cast<Enumerator_t58473718 *>(__this + 1);
	Enumerator_Dispose_m688620792(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m639383991_gshared (Enumerator_t58473718 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2983962278 * L_0 = (Enumerator_t2983962278 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m435964161((Enumerator_t2983962278 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m639383991_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t58473718 * _thisAdjusted = reinterpret_cast<Enumerator_t58473718 *>(__this + 1);
	return Enumerator_MoveNext_m639383991(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int32>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m1529104153_gshared (Enumerator_t58473718 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2983962278 * L_0 = (Enumerator_t2983962278 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t3716250094 * L_1 = (KeyValuePair_2_t3716250094 *)L_0->get_address_of_current_3();
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m1454531804((KeyValuePair_2_t3716250094 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_get_Current_m1529104153_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t58473718 * _thisAdjusted = reinterpret_cast<Enumerator_t58473718 *>(__this + 1);
	return Enumerator_get_Current_m1529104153(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2650732273_gshared (Enumerator_t676045565 * __this, Dictionary_2_t2281509423 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t2281509423 * L_0 = ___host0;
		NullCheck((Dictionary_2_t2281509423 *)L_0);
		Enumerator_t3601534125  L_1 = ((  Enumerator_t3601534125  (*) (Dictionary_2_t2281509423 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t2281509423 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m2650732273_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t2281509423 * ___host0, const MethodInfo* method)
{
	Enumerator_t676045565 * _thisAdjusted = reinterpret_cast<Enumerator_t676045565 *>(__this + 1);
	Enumerator__ctor_m2650732273(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1623176564_gshared (Enumerator_t676045565 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3601534125 * L_0 = (Enumerator_t3601534125 *)__this->get_address_of_host_enumerator_0();
		Il2CppObject * L_1 = Enumerator_get_CurrentKey_m3839846791((Enumerator_t3601534125 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1623176564_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t676045565 * _thisAdjusted = reinterpret_cast<Enumerator_t676045565 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1623176564(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3446745716_gshared (Enumerator_t676045565 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3601534125 * L_0 = (Enumerator_t3601534125 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m3129803197((Enumerator_t3601534125 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3446745716_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t676045565 * _thisAdjusted = reinterpret_cast<Enumerator_t676045565 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m3446745716(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m1587212441_gshared (Enumerator_t676045565 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3601534125 * L_0 = (Enumerator_t3601534125 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m1905011127((Enumerator_t3601534125 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m1587212441_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t676045565 * _thisAdjusted = reinterpret_cast<Enumerator_t676045565 *>(__this + 1);
	Enumerator_Dispose_m1587212441(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m235441832_gshared (Enumerator_t676045565 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3601534125 * L_0 = (Enumerator_t3601534125 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m3349738440((Enumerator_t3601534125 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m235441832_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t676045565 * _thisAdjusted = reinterpret_cast<Enumerator_t676045565 *>(__this + 1);
	return Enumerator_MoveNext_m235441832(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m937156828_gshared (Enumerator_t676045565 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3601534125 * L_0 = (Enumerator_t3601534125 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t38854645 * L_1 = (KeyValuePair_2_t38854645 *)L_0->get_address_of_current_3();
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m2561166459((KeyValuePair_2_t38854645 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_get_Current_m937156828_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t676045565 * _thisAdjusted = reinterpret_cast<Enumerator_t676045565 *>(__this + 1);
	return Enumerator_get_Current_m937156828(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m860887073_gshared (Enumerator_t3416641580 * __this, Dictionary_2_t727138142 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t727138142 * L_0 = ___host0;
		NullCheck((Dictionary_2_t727138142 *)L_0);
		Enumerator_t2047162844  L_1 = ((  Enumerator_t2047162844  (*) (Dictionary_2_t727138142 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t727138142 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m860887073_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t727138142 * ___host0, const MethodInfo* method)
{
	Enumerator_t3416641580 * _thisAdjusted = reinterpret_cast<Enumerator_t3416641580 *>(__this + 1);
	Enumerator__ctor_m860887073(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m945475828_gshared (Enumerator_t3416641580 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2047162844 * L_0 = (Enumerator_t2047162844 *)__this->get_address_of_host_enumerator_0();
		Il2CppObject * L_1 = Enumerator_get_CurrentKey_m3038333311((Enumerator_t2047162844 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m945475828_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3416641580 * _thisAdjusted = reinterpret_cast<Enumerator_t3416641580 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m945475828(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m436067500_gshared (Enumerator_t3416641580 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2047162844 * L_0 = (Enumerator_t2047162844 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m2586713469((Enumerator_t2047162844 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m436067500_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3416641580 * _thisAdjusted = reinterpret_cast<Enumerator_t3416641580 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m436067500(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::Dispose()
extern "C"  void Enumerator_Dispose_m1760329057_gshared (Enumerator_t3416641580 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2047162844 * L_0 = (Enumerator_t2047162844 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m4092052839((Enumerator_t2047162844 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m1760329057_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3416641580 * _thisAdjusted = reinterpret_cast<Enumerator_t3416641580 *>(__this + 1);
	Enumerator_Dispose_m1760329057(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m659395664_gshared (Enumerator_t3416641580 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2047162844 * L_0 = (Enumerator_t2047162844 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m2137572552((Enumerator_t2047162844 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m659395664_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3416641580 * _thisAdjusted = reinterpret_cast<Enumerator_t3416641580 *>(__this + 1);
	return Enumerator_MoveNext_m659395664(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m927196420_gshared (Enumerator_t3416641580 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2047162844 * L_0 = (Enumerator_t2047162844 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t2779450660 * L_1 = (KeyValuePair_2_t2779450660 *)L_0->get_address_of_current_3();
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m1489805227((KeyValuePair_2_t2779450660 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_get_Current_m927196420_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3416641580 * _thisAdjusted = reinterpret_cast<Enumerator_t3416641580 *>(__this + 1);
	return Enumerator_get_Current_m927196420(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m4120924977_gshared (Enumerator_t2044734010 * __this, Dictionary_2_t3650197868 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t3650197868 * L_0 = ___host0;
		NullCheck((Dictionary_2_t3650197868 *)L_0);
		Enumerator_t675255274  L_1 = ((  Enumerator_t675255274  (*) (Dictionary_2_t3650197868 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t3650197868 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m4120924977_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t3650197868 * ___host0, const MethodInfo* method)
{
	Enumerator_t2044734010 * _thisAdjusted = reinterpret_cast<Enumerator_t2044734010 *>(__this + 1);
	Enumerator__ctor_m4120924977(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1515014342_gshared (Enumerator_t2044734010 * __this, const MethodInfo* method)
{
	{
		Enumerator_t675255274 * L_0 = (Enumerator_t675255274 *)__this->get_address_of_host_enumerator_0();
		Il2CppObject * L_1 = Enumerator_get_CurrentKey_m329261871((Enumerator_t675255274 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1515014342_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2044734010 * _thisAdjusted = reinterpret_cast<Enumerator_t2044734010 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1515014342(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3947696880_gshared (Enumerator_t2044734010 * __this, const MethodInfo* method)
{
	{
		Enumerator_t675255274 * L_0 = (Enumerator_t675255274 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m975923089((Enumerator_t675255274 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3947696880_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2044734010 * _thisAdjusted = reinterpret_cast<Enumerator_t2044734010 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m3947696880(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::Dispose()
extern "C"  void Enumerator_Dispose_m165842793_gshared (Enumerator_t2044734010 * __this, const MethodInfo* method)
{
	{
		Enumerator_t675255274 * L_0 = (Enumerator_t675255274 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m3127619583((Enumerator_t675255274 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m165842793_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2044734010 * _thisAdjusted = reinterpret_cast<Enumerator_t2044734010 *>(__this + 1);
	Enumerator_Dispose_m165842793(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3358238188_gshared (Enumerator_t2044734010 * __this, const MethodInfo* method)
{
	{
		Enumerator_t675255274 * L_0 = (Enumerator_t675255274 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m513463628((Enumerator_t675255274 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m3358238188_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2044734010 * _thisAdjusted = reinterpret_cast<Enumerator_t2044734010 *>(__this + 1);
	return Enumerator_MoveNext_m3358238188(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m3799973880_gshared (Enumerator_t2044734010 * __this, const MethodInfo* method)
{
	{
		Enumerator_t675255274 * L_0 = (Enumerator_t675255274 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t1407543090 * L_1 = (KeyValuePair_2_t1407543090 *)L_0->get_address_of_current_3();
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m2438555343((KeyValuePair_2_t1407543090 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_get_Current_m3799973880_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2044734010 * _thisAdjusted = reinterpret_cast<Enumerator_t2044734010 *>(__this + 1);
	return Enumerator_get_Current_m3799973880(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1836822257_gshared (Enumerator_t1125393968 * __this, Dictionary_2_t2730857826 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t2730857826 * L_0 = ___host0;
		NullCheck((Dictionary_2_t2730857826 *)L_0);
		Enumerator_t4050882528  L_1 = ((  Enumerator_t4050882528  (*) (Dictionary_2_t2730857826 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t2730857826 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1836822257_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t2730857826 * ___host0, const MethodInfo* method)
{
	Enumerator_t1125393968 * _thisAdjusted = reinterpret_cast<Enumerator_t1125393968 *>(__this + 1);
	Enumerator__ctor_m1836822257(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2956120428_gshared (Enumerator_t1125393968 * __this, const MethodInfo* method)
{
	{
		Enumerator_t4050882528 * L_0 = (Enumerator_t4050882528 *)__this->get_address_of_host_enumerator_0();
		Il2CppObject * L_1 = Enumerator_get_CurrentKey_m2295079915((Enumerator_t4050882528 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2956120428_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1125393968 * _thisAdjusted = reinterpret_cast<Enumerator_t1125393968 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2956120428(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2980420916_gshared (Enumerator_t1125393968 * __this, const MethodInfo* method)
{
	{
		Enumerator_t4050882528 * L_0 = (Enumerator_t4050882528 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m2489846157((Enumerator_t4050882528 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2980420916_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1125393968 * _thisAdjusted = reinterpret_cast<Enumerator_t1125393968 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m2980420916(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::Dispose()
extern "C"  void Enumerator_Dispose_m2948365329_gshared (Enumerator_t1125393968 * __this, const MethodInfo* method)
{
	{
		Enumerator_t4050882528 * L_0 = (Enumerator_t4050882528 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m793495907((Enumerator_t4050882528 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2948365329_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1125393968 * _thisAdjusted = reinterpret_cast<Enumerator_t1125393968 *>(__this + 1);
	Enumerator_Dispose_m2948365329(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1064937496_gshared (Enumerator_t1125393968 * __this, const MethodInfo* method)
{
	{
		Enumerator_t4050882528 * L_0 = (Enumerator_t4050882528 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m2619008060((Enumerator_t4050882528 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m1064937496_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1125393968 * _thisAdjusted = reinterpret_cast<Enumerator_t1125393968 *>(__this + 1);
	return Enumerator_MoveNext_m1064937496(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m892120696_gshared (Enumerator_t1125393968 * __this, const MethodInfo* method)
{
	{
		Enumerator_t4050882528 * L_0 = (Enumerator_t4050882528 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t488203048 * L_1 = (KeyValuePair_2_t488203048 *)L_0->get_address_of_current_3();
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m573362703((KeyValuePair_2_t488203048 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_get_Current_m892120696_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1125393968 * _thisAdjusted = reinterpret_cast<Enumerator_t1125393968 *>(__this + 1);
	return Enumerator_get_Current_m892120696(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,System.Int64>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3629755714;
extern const uint32_t KeyCollection__ctor_m3448972796_MetadataUsageId;
extern "C"  void KeyCollection__ctor_m3448972796_gshared (KeyCollection_t1847687001 * __this, Dictionary_2_t3659156526 * ___dictionary0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection__ctor_m3448972796_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t3659156526 * L_0 = ___dictionary0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral3629755714, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		Dictionary_2_t3659156526 * L_2 = ___dictionary0;
		__this->set_dictionary_0(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,System.Int64>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2652494438_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2652494438_gshared (KeyCollection_t1847687001 * __this, int32_t ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2652494438_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,System.Int64>::System.Collections.Generic.ICollection<TKey>.Clear()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3374680127_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3374680127_gshared (KeyCollection_t1847687001 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3374680127_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,System.Int64>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1045149434_gshared (KeyCollection_t1847687001 * __this, int32_t ___item0, const MethodInfo* method)
{
	{
		Dictionary_2_t3659156526 * L_0 = (Dictionary_2_t3659156526 *)__this->get_dictionary_0();
		int32_t L_1 = ___item0;
		NullCheck((Dictionary_2_t3659156526 *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t3659156526 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t3659156526 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,System.Int64>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m28084919_MetadataUsageId;
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m28084919_gshared (KeyCollection_t1847687001 * __this, int32_t ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m28084919_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,System.Int64>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3233431833_gshared (KeyCollection_t1847687001 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t1847687001 *)__this);
		Enumerator_t2053692668  L_0 = ((  Enumerator_t2053692668  (*) (KeyCollection_t1847687001 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t1847687001 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t2053692668  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,System.Int64>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m1547472073_gshared (KeyCollection_t1847687001 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	MetricU5BU5D_t2477685199* V_0 = NULL;
	{
		Il2CppArray * L_0 = ___array0;
		V_0 = (MetricU5BU5D_t2477685199*)((MetricU5BU5D_t2477685199*)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3)));
		MetricU5BU5D_t2477685199* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		MetricU5BU5D_t2477685199* L_2 = V_0;
		int32_t L_3 = ___index1;
		NullCheck((KeyCollection_t1847687001 *)__this);
		((  void (*) (KeyCollection_t1847687001 *, MetricU5BU5D_t2477685199*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((KeyCollection_t1847687001 *)__this, (MetricU5BU5D_t2477685199*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}

IL_0016:
	{
		Dictionary_2_t3659156526 * L_4 = (Dictionary_2_t3659156526 *)__this->get_dictionary_0();
		Il2CppArray * L_5 = ___array0;
		int32_t L_6 = ___index1;
		NullCheck((Dictionary_2_t3659156526 *)L_4);
		((  void (*) (Dictionary_2_t3659156526 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t3659156526 *)L_4, (Il2CppArray *)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t3659156526 * L_7 = (Dictionary_2_t3659156526 *)__this->get_dictionary_0();
		Il2CppArray * L_8 = ___array0;
		int32_t L_9 = ___index1;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t3638058609 * L_11 = (Transform_1_t3638058609 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t3638058609 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_11, (Il2CppObject *)NULL, (IntPtr_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t3659156526 *)L_7);
		((  void (*) (Dictionary_2_t3659156526 *, Il2CppArray *, int32_t, Transform_1_t3638058609 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Dictionary_2_t3659156526 *)L_7, (Il2CppArray *)L_8, (int32_t)L_9, (Transform_1_t3638058609 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,System.Int64>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2743961514_gshared (KeyCollection_t1847687001 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t1847687001 *)__this);
		Enumerator_t2053692668  L_0 = ((  Enumerator_t2053692668  (*) (KeyCollection_t1847687001 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t1847687001 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t2053692668  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,System.Int64>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m648743819_gshared (KeyCollection_t1847687001 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,System.Int64>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3936441933_gshared (KeyCollection_t1847687001 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,System.Int64>::System.Collections.ICollection.get_SyncRoot()
extern Il2CppClass* ICollection_t91669223_il2cpp_TypeInfo_var;
extern const uint32_t KeyCollection_System_Collections_ICollection_get_SyncRoot_m2425750661_MetadataUsageId;
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m2425750661_gshared (KeyCollection_t1847687001 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_ICollection_get_SyncRoot_m2425750661_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t3659156526 * L_0 = (Dictionary_2_t3659156526 *)__this->get_dictionary_0();
		NullCheck((Il2CppObject *)L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t91669223_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,System.Int64>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m3475369863_gshared (KeyCollection_t1847687001 * __this, MetricU5BU5D_t2477685199* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		Dictionary_2_t3659156526 * L_0 = (Dictionary_2_t3659156526 *)__this->get_dictionary_0();
		MetricU5BU5D_t2477685199* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Dictionary_2_t3659156526 *)L_0);
		((  void (*) (Dictionary_2_t3659156526 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t3659156526 *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t3659156526 * L_3 = (Dictionary_2_t3659156526 *)__this->get_dictionary_0();
		MetricU5BU5D_t2477685199* L_4 = ___array0;
		int32_t L_5 = ___index1;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t3638058609 * L_7 = (Transform_1_t3638058609 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t3638058609 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_7, (Il2CppObject *)NULL, (IntPtr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t3659156526 *)L_3);
		((  void (*) (Dictionary_2_t3659156526 *, MetricU5BU5D_t2477685199*, int32_t, Transform_1_t3638058609 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((Dictionary_2_t3659156526 *)L_3, (MetricU5BU5D_t2477685199*)L_4, (int32_t)L_5, (Transform_1_t3638058609 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return;
	}
}
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,System.Int64>::GetEnumerator()
extern "C"  Enumerator_t2053692668  KeyCollection_GetEnumerator_m1156193322_gshared (KeyCollection_t1847687001 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t3659156526 * L_0 = (Dictionary_2_t3659156526 *)__this->get_dictionary_0();
		Enumerator_t2053692668  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Enumerator__ctor_m1698121165(&L_1, (Dictionary_2_t3659156526 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,System.Int64>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m1239091985_gshared (KeyCollection_t1847687001 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t3659156526 * L_0 = (Dictionary_2_t3659156526 *)__this->get_dictionary_0();
		NullCheck((Dictionary_2_t3659156526 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Dictionary_2_t3659156526 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t3659156526 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3629755714;
extern const uint32_t KeyCollection__ctor_m2275592418_MetadataUsageId;
extern "C"  void KeyCollection__ctor_m2275592418_gshared (KeyCollection_t3628058259 * __this, Dictionary_2_t1144560488 * ___dictionary0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection__ctor_m2275592418_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t1144560488 * L_0 = ___dictionary0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral3629755714, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		Dictionary_2_t1144560488 * L_2 = ___dictionary0;
		__this->set_dictionary_0(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,System.Object>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m22610452_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m22610452_gshared (KeyCollection_t3628058259 * __this, int32_t ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m22610452_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,System.Object>::System.Collections.Generic.ICollection<TKey>.Clear()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3969664523_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3969664523_gshared (KeyCollection_t3628058259 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3969664523_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,System.Object>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2395394556_gshared (KeyCollection_t3628058259 * __this, int32_t ___item0, const MethodInfo* method)
{
	{
		Dictionary_2_t1144560488 * L_0 = (Dictionary_2_t1144560488 *)__this->get_dictionary_0();
		int32_t L_1 = ___item0;
		NullCheck((Dictionary_2_t1144560488 *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t1144560488 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t1144560488 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,System.Object>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1815209971_MetadataUsageId;
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1815209971_gshared (KeyCollection_t3628058259 * __this, int32_t ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1815209971_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,System.Object>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m246573329_gshared (KeyCollection_t3628058259 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t3628058259 *)__this);
		Enumerator_t3834063926  L_0 = ((  Enumerator_t3834063926  (*) (KeyCollection_t3628058259 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t3628058259 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t3834063926  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m3092510701_gshared (KeyCollection_t3628058259 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	MetricU5BU5D_t2477685199* V_0 = NULL;
	{
		Il2CppArray * L_0 = ___array0;
		V_0 = (MetricU5BU5D_t2477685199*)((MetricU5BU5D_t2477685199*)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3)));
		MetricU5BU5D_t2477685199* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		MetricU5BU5D_t2477685199* L_2 = V_0;
		int32_t L_3 = ___index1;
		NullCheck((KeyCollection_t3628058259 *)__this);
		((  void (*) (KeyCollection_t3628058259 *, MetricU5BU5D_t2477685199*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((KeyCollection_t3628058259 *)__this, (MetricU5BU5D_t2477685199*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}

IL_0016:
	{
		Dictionary_2_t1144560488 * L_4 = (Dictionary_2_t1144560488 *)__this->get_dictionary_0();
		Il2CppArray * L_5 = ___array0;
		int32_t L_6 = ___index1;
		NullCheck((Dictionary_2_t1144560488 *)L_4);
		((  void (*) (Dictionary_2_t1144560488 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t1144560488 *)L_4, (Il2CppArray *)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t1144560488 * L_7 = (Dictionary_2_t1144560488 *)__this->get_dictionary_0();
		Il2CppArray * L_8 = ___array0;
		int32_t L_9 = ___index1;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t2240600335 * L_11 = (Transform_1_t2240600335 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t2240600335 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_11, (Il2CppObject *)NULL, (IntPtr_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t1144560488 *)L_7);
		((  void (*) (Dictionary_2_t1144560488 *, Il2CppArray *, int32_t, Transform_1_t2240600335 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Dictionary_2_t1144560488 *)L_7, (Il2CppArray *)L_8, (int32_t)L_9, (Transform_1_t2240600335 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1668434068_gshared (KeyCollection_t3628058259 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t3628058259 *)__this);
		Enumerator_t3834063926  L_0 = ((  Enumerator_t3834063926  (*) (KeyCollection_t3628058259 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t3628058259 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t3834063926  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,System.Object>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2896445255_gshared (KeyCollection_t3628058259 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m451785489_gshared (KeyCollection_t3628058259 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern Il2CppClass* ICollection_t91669223_il2cpp_TypeInfo_var;
extern const uint32_t KeyCollection_System_Collections_ICollection_get_SyncRoot_m1284653485_MetadataUsageId;
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m1284653485_gshared (KeyCollection_t3628058259 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_ICollection_get_SyncRoot_m1284653485_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t1144560488 * L_0 = (Dictionary_2_t1144560488 *)__this->get_dictionary_0();
		NullCheck((Il2CppObject *)L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t91669223_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,System.Object>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m3857679723_gshared (KeyCollection_t3628058259 * __this, MetricU5BU5D_t2477685199* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		Dictionary_2_t1144560488 * L_0 = (Dictionary_2_t1144560488 *)__this->get_dictionary_0();
		MetricU5BU5D_t2477685199* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Dictionary_2_t1144560488 *)L_0);
		((  void (*) (Dictionary_2_t1144560488 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t1144560488 *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t1144560488 * L_3 = (Dictionary_2_t1144560488 *)__this->get_dictionary_0();
		MetricU5BU5D_t2477685199* L_4 = ___array0;
		int32_t L_5 = ___index1;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t2240600335 * L_7 = (Transform_1_t2240600335 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t2240600335 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_7, (Il2CppObject *)NULL, (IntPtr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t1144560488 *)L_3);
		((  void (*) (Dictionary_2_t1144560488 *, MetricU5BU5D_t2477685199*, int32_t, Transform_1_t2240600335 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((Dictionary_2_t1144560488 *)L_3, (MetricU5BU5D_t2477685199*)L_4, (int32_t)L_5, (Transform_1_t2240600335 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return;
	}
}
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,System.Object>::GetEnumerator()
extern "C"  Enumerator_t3834063926  KeyCollection_GetEnumerator_m912416230_gshared (KeyCollection_t3628058259 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t1144560488 * L_0 = (Dictionary_2_t1144560488 *)__this->get_dictionary_0();
		Enumerator_t3834063926  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Enumerator__ctor_m2774311225(&L_1, (Dictionary_2_t1144560488 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,System.Object>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m3852128017_gshared (KeyCollection_t3628058259 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t1144560488 * L_0 = (Dictionary_2_t1144560488 *)__this->get_dictionary_0();
		NullCheck((Dictionary_2_t1144560488 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Dictionary_2_t1144560488 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t1144560488 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3629755714;
extern const uint32_t KeyCollection__ctor_m3425792570_MetadataUsageId;
extern "C"  void KeyCollection__ctor_m3425792570_gshared (KeyCollection_t3563200854 * __this, Dictionary_2_t1079703083 * ___dictionary0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection__ctor_m3425792570_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t1079703083 * L_0 = ___dictionary0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral3629755714, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		Dictionary_2_t1079703083 * L_2 = ___dictionary0;
		__this->set_dictionary_0(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Int32>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3194320320_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3194320320_gshared (KeyCollection_t3563200854 * __this, int32_t ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3194320320_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Int32>::System.Collections.Generic.ICollection<TKey>.Clear()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1966210003_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1966210003_gshared (KeyCollection_t3563200854 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1966210003_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Int32>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1541777540_gshared (KeyCollection_t3563200854 * __this, int32_t ___item0, const MethodInfo* method)
{
	{
		Dictionary_2_t1079703083 * L_0 = (Dictionary_2_t1079703083 *)__this->get_dictionary_0();
		int32_t L_1 = ___item0;
		NullCheck((Dictionary_2_t1079703083 *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t1079703083 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t1079703083 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Int32>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m4021128907_MetadataUsageId;
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m4021128907_gshared (KeyCollection_t3563200854 * __this, int32_t ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m4021128907_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Int32>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2865503421_gshared (KeyCollection_t3563200854 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t3563200854 *)__this);
		Enumerator_t3769206521  L_0 = ((  Enumerator_t3769206521  (*) (KeyCollection_t3563200854 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t3563200854 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t3769206521  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m4204328697_gshared (KeyCollection_t3563200854 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	Int32U5BU5D_t3030399641* V_0 = NULL;
	{
		Il2CppArray * L_0 = ___array0;
		V_0 = (Int32U5BU5D_t3030399641*)((Int32U5BU5D_t3030399641*)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3)));
		Int32U5BU5D_t3030399641* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		Int32U5BU5D_t3030399641* L_2 = V_0;
		int32_t L_3 = ___index1;
		NullCheck((KeyCollection_t3563200854 *)__this);
		((  void (*) (KeyCollection_t3563200854 *, Int32U5BU5D_t3030399641*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((KeyCollection_t3563200854 *)__this, (Int32U5BU5D_t3030399641*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}

IL_0016:
	{
		Dictionary_2_t1079703083 * L_4 = (Dictionary_2_t1079703083 *)__this->get_dictionary_0();
		Il2CppArray * L_5 = ___array0;
		int32_t L_6 = ___index1;
		NullCheck((Dictionary_2_t1079703083 *)L_4);
		((  void (*) (Dictionary_2_t1079703083 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t1079703083 *)L_4, (Il2CppArray *)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t1079703083 * L_7 = (Dictionary_2_t1079703083 *)__this->get_dictionary_0();
		Il2CppArray * L_8 = ___array0;
		int32_t L_9 = ___index1;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t2737799006 * L_11 = (Transform_1_t2737799006 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t2737799006 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_11, (Il2CppObject *)NULL, (IntPtr_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t1079703083 *)L_7);
		((  void (*) (Dictionary_2_t1079703083 *, Il2CppArray *, int32_t, Transform_1_t2737799006 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Dictionary_2_t1079703083 *)L_7, (Il2CppArray *)L_8, (int32_t)L_9, (Transform_1_t2737799006 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2660051048_gshared (KeyCollection_t3563200854 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t3563200854 *)__this);
		Enumerator_t3769206521  L_0 = ((  Enumerator_t3769206521  (*) (KeyCollection_t3563200854 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t3563200854 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t3769206521  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Int32>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1819096223_gshared (KeyCollection_t3563200854 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1330657765_gshared (KeyCollection_t3563200854 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern Il2CppClass* ICollection_t91669223_il2cpp_TypeInfo_var;
extern const uint32_t KeyCollection_System_Collections_ICollection_get_SyncRoot_m1792869049_MetadataUsageId;
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m1792869049_gshared (KeyCollection_t3563200854 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_ICollection_get_SyncRoot_m1792869049_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t1079703083 * L_0 = (Dictionary_2_t1079703083 *)__this->get_dictionary_0();
		NullCheck((Il2CppObject *)L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t91669223_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Int32>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m1956025779_gshared (KeyCollection_t3563200854 * __this, Int32U5BU5D_t3030399641* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		Dictionary_2_t1079703083 * L_0 = (Dictionary_2_t1079703083 *)__this->get_dictionary_0();
		Int32U5BU5D_t3030399641* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Dictionary_2_t1079703083 *)L_0);
		((  void (*) (Dictionary_2_t1079703083 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t1079703083 *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t1079703083 * L_3 = (Dictionary_2_t1079703083 *)__this->get_dictionary_0();
		Int32U5BU5D_t3030399641* L_4 = ___array0;
		int32_t L_5 = ___index1;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t2737799006 * L_7 = (Transform_1_t2737799006 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t2737799006 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_7, (Il2CppObject *)NULL, (IntPtr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t1079703083 *)L_3);
		((  void (*) (Dictionary_2_t1079703083 *, Int32U5BU5D_t3030399641*, int32_t, Transform_1_t2737799006 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((Dictionary_2_t1079703083 *)L_3, (Int32U5BU5D_t3030399641*)L_4, (int32_t)L_5, (Transform_1_t2737799006 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return;
	}
}
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Int32>::GetEnumerator()
extern "C"  Enumerator_t3769206521  KeyCollection_GetEnumerator_m2531464174_gshared (KeyCollection_t3563200854 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t1079703083 * L_0 = (Dictionary_2_t1079703083 *)__this->get_dictionary_0();
		Enumerator_t3769206521  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Enumerator__ctor_m356032285(&L_1, (Dictionary_2_t1079703083 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Int32>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m1839036997_gshared (KeyCollection_t3563200854 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t1079703083 * L_0 = (Dictionary_2_t1079703083 *)__this->get_dictionary_0();
		NullCheck((Dictionary_2_t1079703083 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Dictionary_2_t1079703083 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t1079703083 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3629755714;
extern const uint32_t KeyCollection__ctor_m3555561037_MetadataUsageId;
extern "C"  void KeyCollection__ctor_m3555561037_gshared (KeyCollection_t4180772701 * __this, Dictionary_2_t1697274930 * ___dictionary0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection__ctor_m3555561037_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t1697274930 * L_0 = ___dictionary0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral3629755714, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		Dictionary_2_t1697274930 * L_2 = ___dictionary0;
		__this->set_dictionary_0(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Object>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3743129415_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3743129415_gshared (KeyCollection_t4180772701 * __this, int32_t ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3743129415_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Object>::System.Collections.Generic.ICollection<TKey>.Clear()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m522538414_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m522538414_gshared (KeyCollection_t4180772701 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m522538414_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Object>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3840423669_gshared (KeyCollection_t4180772701 * __this, int32_t ___item0, const MethodInfo* method)
{
	{
		Dictionary_2_t1697274930 * L_0 = (Dictionary_2_t1697274930 *)__this->get_dictionary_0();
		int32_t L_1 = ___item0;
		NullCheck((Dictionary_2_t1697274930 *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t1697274930 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t1697274930 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Object>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2545700230_MetadataUsageId;
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2545700230_gshared (KeyCollection_t4180772701 * __this, int32_t ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2545700230_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Object>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3763330846_gshared (KeyCollection_t4180772701 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t4180772701 *)__this);
		Enumerator_t91811072  L_0 = ((  Enumerator_t91811072  (*) (KeyCollection_t4180772701 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t4180772701 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t91811072  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m4142454252_gshared (KeyCollection_t4180772701 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	Int32U5BU5D_t3030399641* V_0 = NULL;
	{
		Il2CppArray * L_0 = ___array0;
		V_0 = (Int32U5BU5D_t3030399641*)((Int32U5BU5D_t3030399641*)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3)));
		Int32U5BU5D_t3030399641* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		Int32U5BU5D_t3030399641* L_2 = V_0;
		int32_t L_3 = ___index1;
		NullCheck((KeyCollection_t4180772701 *)__this);
		((  void (*) (KeyCollection_t4180772701 *, Int32U5BU5D_t3030399641*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((KeyCollection_t4180772701 *)__this, (Int32U5BU5D_t3030399641*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}

IL_0016:
	{
		Dictionary_2_t1697274930 * L_4 = (Dictionary_2_t1697274930 *)__this->get_dictionary_0();
		Il2CppArray * L_5 = ___array0;
		int32_t L_6 = ___index1;
		NullCheck((Dictionary_2_t1697274930 *)L_4);
		((  void (*) (Dictionary_2_t1697274930 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t1697274930 *)L_4, (Il2CppArray *)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t1697274930 * L_7 = (Dictionary_2_t1697274930 *)__this->get_dictionary_0();
		Il2CppArray * L_8 = ___array0;
		int32_t L_9 = ___index1;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t3322033499 * L_11 = (Transform_1_t3322033499 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t3322033499 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_11, (Il2CppObject *)NULL, (IntPtr_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t1697274930 *)L_7);
		((  void (*) (Dictionary_2_t1697274930 *, Il2CppArray *, int32_t, Transform_1_t3322033499 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Dictionary_2_t1697274930 *)L_7, (Il2CppArray *)L_8, (int32_t)L_9, (Transform_1_t3322033499 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1589406383_gshared (KeyCollection_t4180772701 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t4180772701 *)__this);
		Enumerator_t91811072  L_0 = ((  Enumerator_t91811072  (*) (KeyCollection_t4180772701 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t4180772701 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t91811072  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Object>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1119857328_gshared (KeyCollection_t4180772701 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1423289640_gshared (KeyCollection_t4180772701 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern Il2CppClass* ICollection_t91669223_il2cpp_TypeInfo_var;
extern const uint32_t KeyCollection_System_Collections_ICollection_get_SyncRoot_m3802319686_MetadataUsageId;
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m3802319686_gshared (KeyCollection_t4180772701 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_ICollection_get_SyncRoot_m3802319686_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t1697274930 * L_0 = (Dictionary_2_t1697274930 *)__this->get_dictionary_0();
		NullCheck((Il2CppObject *)L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t91669223_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Object>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m4056130090_gshared (KeyCollection_t4180772701 * __this, Int32U5BU5D_t3030399641* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		Dictionary_2_t1697274930 * L_0 = (Dictionary_2_t1697274930 *)__this->get_dictionary_0();
		Int32U5BU5D_t3030399641* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Dictionary_2_t1697274930 *)L_0);
		((  void (*) (Dictionary_2_t1697274930 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t1697274930 *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t1697274930 * L_3 = (Dictionary_2_t1697274930 *)__this->get_dictionary_0();
		Int32U5BU5D_t3030399641* L_4 = ___array0;
		int32_t L_5 = ___index1;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t3322033499 * L_7 = (Transform_1_t3322033499 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t3322033499 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_7, (Il2CppObject *)NULL, (IntPtr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t1697274930 *)L_3);
		((  void (*) (Dictionary_2_t1697274930 *, Int32U5BU5D_t3030399641*, int32_t, Transform_1_t3322033499 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((Dictionary_2_t1697274930 *)L_3, (Int32U5BU5D_t3030399641*)L_4, (int32_t)L_5, (Transform_1_t3322033499 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return;
	}
}
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Object>::GetEnumerator()
extern "C"  Enumerator_t91811072  KeyCollection_GetEnumerator_m627495629_gshared (KeyCollection_t4180772701 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t1697274930 * L_0 = (Dictionary_2_t1697274930 *)__this->get_dictionary_0();
		Enumerator_t91811072  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Enumerator__ctor_m2975000652(&L_1, (Dictionary_2_t1697274930 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Object>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m2942903136_gshared (KeyCollection_t4180772701 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t1697274930 * L_0 = (Dictionary_2_t1697274930 *)__this->get_dictionary_0();
		NullCheck((Dictionary_2_t1697274930 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Dictionary_2_t1697274930 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t1697274930 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3629755714;
extern const uint32_t KeyCollection__ctor_m3885543862_MetadataUsageId;
extern "C"  void KeyCollection__ctor_m3885543862_gshared (KeyCollection_t1867498172 * __this, Dictionary_2_t3678967697 * ___dictionary0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection__ctor_m3885543862_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t3678967697 * L_0 = ___dictionary0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral3629755714, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		Dictionary_2_t3678967697 * L_2 = ___dictionary0;
		__this->set_dictionary_0(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Object>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3319720888_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3319720888_gshared (KeyCollection_t1867498172 * __this, int64_t ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3319720888_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Object>::System.Collections.Generic.ICollection<TKey>.Clear()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3601347139_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3601347139_gshared (KeyCollection_t1867498172 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3601347139_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Object>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2597166232_gshared (KeyCollection_t1867498172 * __this, int64_t ___item0, const MethodInfo* method)
{
	{
		Dictionary_2_t3678967697 * L_0 = (Dictionary_2_t3678967697 *)__this->get_dictionary_0();
		int64_t L_1 = ___item0;
		NullCheck((Dictionary_2_t3678967697 *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t3678967697 *, int64_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t3678967697 *)L_0, (int64_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Object>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1060868683_MetadataUsageId;
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1060868683_gshared (KeyCollection_t1867498172 * __this, int64_t ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1060868683_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Object>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m152152421_gshared (KeyCollection_t1867498172 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t1867498172 *)__this);
		Enumerator_t2073503839  L_0 = ((  Enumerator_t2073503839  (*) (KeyCollection_t1867498172 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t1867498172 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t2073503839  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m1539094133_gshared (KeyCollection_t1867498172 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	Int64U5BU5D_t717125112* V_0 = NULL;
	{
		Il2CppArray * L_0 = ___array0;
		V_0 = (Int64U5BU5D_t717125112*)((Int64U5BU5D_t717125112*)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3)));
		Int64U5BU5D_t717125112* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		Int64U5BU5D_t717125112* L_2 = V_0;
		int32_t L_3 = ___index1;
		NullCheck((KeyCollection_t1867498172 *)__this);
		((  void (*) (KeyCollection_t1867498172 *, Int64U5BU5D_t717125112*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((KeyCollection_t1867498172 *)__this, (Int64U5BU5D_t717125112*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}

IL_0016:
	{
		Dictionary_2_t3678967697 * L_4 = (Dictionary_2_t3678967697 *)__this->get_dictionary_0();
		Il2CppArray * L_5 = ___array0;
		int32_t L_6 = ___index1;
		NullCheck((Dictionary_2_t3678967697 *)L_4);
		((  void (*) (Dictionary_2_t3678967697 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t3678967697 *)L_4, (Il2CppArray *)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t3678967697 * L_7 = (Dictionary_2_t3678967697 *)__this->get_dictionary_0();
		Il2CppArray * L_8 = ___array0;
		int32_t L_9 = ___index1;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t216544365 * L_11 = (Transform_1_t216544365 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t216544365 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_11, (Il2CppObject *)NULL, (IntPtr_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t3678967697 *)L_7);
		((  void (*) (Dictionary_2_t3678967697 *, Il2CppArray *, int32_t, Transform_1_t216544365 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Dictionary_2_t3678967697 *)L_7, (Il2CppArray *)L_8, (int32_t)L_9, (Transform_1_t216544365 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1351799188_gshared (KeyCollection_t1867498172 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t1867498172 *)__this);
		Enumerator_t2073503839  L_0 = ((  Enumerator_t2073503839  (*) (KeyCollection_t1867498172 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t1867498172 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t2073503839  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Object>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3534394359_gshared (KeyCollection_t1867498172 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3200643657_gshared (KeyCollection_t1867498172 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern Il2CppClass* ICollection_t91669223_il2cpp_TypeInfo_var;
extern const uint32_t KeyCollection_System_Collections_ICollection_get_SyncRoot_m1440087769_MetadataUsageId;
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m1440087769_gshared (KeyCollection_t1867498172 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_ICollection_get_SyncRoot_m1440087769_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t3678967697 * L_0 = (Dictionary_2_t3678967697 *)__this->get_dictionary_0();
		NullCheck((Il2CppObject *)L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t91669223_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Object>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m1238003947_gshared (KeyCollection_t1867498172 * __this, Int64U5BU5D_t717125112* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		Dictionary_2_t3678967697 * L_0 = (Dictionary_2_t3678967697 *)__this->get_dictionary_0();
		Int64U5BU5D_t717125112* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Dictionary_2_t3678967697 *)L_0);
		((  void (*) (Dictionary_2_t3678967697 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t3678967697 *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t3678967697 * L_3 = (Dictionary_2_t3678967697 *)__this->get_dictionary_0();
		Int64U5BU5D_t717125112* L_4 = ___array0;
		int32_t L_5 = ___index1;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t216544365 * L_7 = (Transform_1_t216544365 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t216544365 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_7, (Il2CppObject *)NULL, (IntPtr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t3678967697 *)L_3);
		((  void (*) (Dictionary_2_t3678967697 *, Int64U5BU5D_t717125112*, int32_t, Transform_1_t216544365 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((Dictionary_2_t3678967697 *)L_3, (Int64U5BU5D_t717125112*)L_4, (int32_t)L_5, (Transform_1_t216544365 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return;
	}
}
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Object>::GetEnumerator()
extern "C"  Enumerator_t2073503839  KeyCollection_GetEnumerator_m3687526536_gshared (KeyCollection_t1867498172 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t3678967697 * L_0 = (Dictionary_2_t3678967697 *)__this->get_dictionary_0();
		Enumerator_t2073503839  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Enumerator__ctor_m3400877137(&L_1, (Dictionary_2_t3678967697 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Object>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m972382317_gshared (KeyCollection_t1867498172 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t3678967697 * L_0 = (Dictionary_2_t3678967697 *)__this->get_dictionary_0();
		NullCheck((Dictionary_2_t3678967697 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Dictionary_2_t3678967697 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t3678967697 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3629755714;
extern const uint32_t KeyCollection__ctor_m40025794_MetadataUsageId;
extern "C"  void KeyCollection__ctor_m40025794_gshared (KeyCollection_t1320005088 * __this, Dictionary_2_t3131474613 * ___dictionary0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection__ctor_m40025794_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t3131474613 * L_0 = ___dictionary0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral3629755714, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		Dictionary_2_t3131474613 * L_2 = ___dictionary0;
		__this->set_dictionary_0(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Object>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m295062388_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m295062388_gshared (KeyCollection_t1320005088 * __this, IntPtr_t ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m295062388_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Object>::System.Collections.Generic.ICollection<TKey>.Clear()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1170030795_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1170030795_gshared (KeyCollection_t1320005088 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1170030795_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Object>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m671614396_gshared (KeyCollection_t1320005088 * __this, IntPtr_t ___item0, const MethodInfo* method)
{
	{
		Dictionary_2_t3131474613 * L_0 = (Dictionary_2_t3131474613 *)__this->get_dictionary_0();
		IntPtr_t L_1 = ___item0;
		NullCheck((Dictionary_2_t3131474613 *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t3131474613 *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t3131474613 *)L_0, (IntPtr_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Object>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3118644019_MetadataUsageId;
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3118644019_gshared (KeyCollection_t1320005088 * __this, IntPtr_t ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3118644019_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Object>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2104224529_gshared (KeyCollection_t1320005088 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t1320005088 *)__this);
		Enumerator_t1526010755  L_0 = ((  Enumerator_t1526010755  (*) (KeyCollection_t1320005088 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t1320005088 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t1526010755  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m236144141_gshared (KeyCollection_t1320005088 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	IntPtrU5BU5D_t169632028* V_0 = NULL;
	{
		Il2CppArray * L_0 = ___array0;
		V_0 = (IntPtrU5BU5D_t169632028*)((IntPtrU5BU5D_t169632028*)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3)));
		IntPtrU5BU5D_t169632028* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		IntPtrU5BU5D_t169632028* L_2 = V_0;
		int32_t L_3 = ___index1;
		NullCheck((KeyCollection_t1320005088 *)__this);
		((  void (*) (KeyCollection_t1320005088 *, IntPtrU5BU5D_t169632028*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((KeyCollection_t1320005088 *)__this, (IntPtrU5BU5D_t169632028*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}

IL_0016:
	{
		Dictionary_2_t3131474613 * L_4 = (Dictionary_2_t3131474613 *)__this->get_dictionary_0();
		Il2CppArray * L_5 = ___array0;
		int32_t L_6 = ___index1;
		NullCheck((Dictionary_2_t3131474613 *)L_4);
		((  void (*) (Dictionary_2_t3131474613 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t3131474613 *)L_4, (Il2CppArray *)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t3131474613 * L_7 = (Dictionary_2_t3131474613 *)__this->get_dictionary_0();
		Il2CppArray * L_8 = ___array0;
		int32_t L_9 = ___index1;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t2168890597 * L_11 = (Transform_1_t2168890597 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t2168890597 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_11, (Il2CppObject *)NULL, (IntPtr_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t3131474613 *)L_7);
		((  void (*) (Dictionary_2_t3131474613 *, Il2CppArray *, int32_t, Transform_1_t2168890597 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Dictionary_2_t3131474613 *)L_7, (Il2CppArray *)L_8, (int32_t)L_9, (Transform_1_t2168890597 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2864841844_gshared (KeyCollection_t1320005088 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t1320005088 *)__this);
		Enumerator_t1526010755  L_0 = ((  Enumerator_t1526010755  (*) (KeyCollection_t1320005088 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t1320005088 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t1526010755  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Object>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2924879015_gshared (KeyCollection_t1320005088 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1336146065_gshared (KeyCollection_t1320005088 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern Il2CppClass* ICollection_t91669223_il2cpp_TypeInfo_var;
extern const uint32_t KeyCollection_System_Collections_ICollection_get_SyncRoot_m1967424173_MetadataUsageId;
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m1967424173_gshared (KeyCollection_t1320005088 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_ICollection_get_SyncRoot_m1967424173_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t3131474613 * L_0 = (Dictionary_2_t3131474613 *)__this->get_dictionary_0();
		NullCheck((Il2CppObject *)L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t91669223_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Object>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m3820790603_gshared (KeyCollection_t1320005088 * __this, IntPtrU5BU5D_t169632028* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		Dictionary_2_t3131474613 * L_0 = (Dictionary_2_t3131474613 *)__this->get_dictionary_0();
		IntPtrU5BU5D_t169632028* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Dictionary_2_t3131474613 *)L_0);
		((  void (*) (Dictionary_2_t3131474613 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t3131474613 *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t3131474613 * L_3 = (Dictionary_2_t3131474613 *)__this->get_dictionary_0();
		IntPtrU5BU5D_t169632028* L_4 = ___array0;
		int32_t L_5 = ___index1;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t2168890597 * L_7 = (Transform_1_t2168890597 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t2168890597 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_7, (Il2CppObject *)NULL, (IntPtr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t3131474613 *)L_3);
		((  void (*) (Dictionary_2_t3131474613 *, IntPtrU5BU5D_t169632028*, int32_t, Transform_1_t2168890597 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((Dictionary_2_t3131474613 *)L_3, (IntPtrU5BU5D_t169632028*)L_4, (int32_t)L_5, (Transform_1_t2168890597 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return;
	}
}
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Object>::GetEnumerator()
extern "C"  Enumerator_t1526010755  KeyCollection_GetEnumerator_m349130886_gshared (KeyCollection_t1320005088 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t3131474613 * L_0 = (Dictionary_2_t3131474613 *)__this->get_dictionary_0();
		Enumerator_t1526010755  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Enumerator__ctor_m3235517721(&L_1, (Dictionary_2_t3131474613 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Object>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m1219588529_gshared (KeyCollection_t1320005088 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t3131474613 * L_0 = (Dictionary_2_t3131474613 *)__this->get_dictionary_0();
		NullCheck((Dictionary_2_t3131474613 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Dictionary_2_t3131474613 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t3131474613 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3629755714;
extern const uint32_t KeyCollection__ctor_m944849420_MetadataUsageId;
extern "C"  void KeyCollection__ctor_m944849420_gshared (KeyCollection_t3708365277 * __this, Dictionary_2_t1224867506 * ___dictionary0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection__ctor_m944849420_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t1224867506 * L_0 = ___dictionary0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral3629755714, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		Dictionary_2_t1224867506 * L_2 = ___dictionary0;
		__this->set_dictionary_0(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m788825550_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m788825550_gshared (KeyCollection_t3708365277 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m788825550_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.Generic.ICollection<TKey>.Clear()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2769795483_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2769795483_gshared (KeyCollection_t3708365277 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2769795483_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m827225850_gshared (KeyCollection_t3708365277 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		Dictionary_2_t1224867506 * L_0 = (Dictionary_2_t1224867506 *)__this->get_dictionary_0();
		Il2CppObject * L_1 = ___item0;
		NullCheck((Dictionary_2_t1224867506 *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t1224867506 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t1224867506 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2474066187_MetadataUsageId;
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2474066187_gshared (KeyCollection_t3708365277 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2474066187_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3310486225_gshared (KeyCollection_t3708365277 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t3708365277 *)__this);
		Enumerator_t3914370944  L_0 = ((  Enumerator_t3914370944  (*) (KeyCollection_t3708365277 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t3708365277 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t3914370944  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m1732125405_gshared (KeyCollection_t3708365277 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	ObjectU5BU5D_t3614634134* V_0 = NULL;
	{
		Il2CppArray * L_0 = ___array0;
		V_0 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3)));
		ObjectU5BU5D_t3614634134* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_2 = V_0;
		int32_t L_3 = ___index1;
		NullCheck((KeyCollection_t3708365277 *)__this);
		((  void (*) (KeyCollection_t3708365277 *, ObjectU5BU5D_t3614634134*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((KeyCollection_t3708365277 *)__this, (ObjectU5BU5D_t3614634134*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}

IL_0016:
	{
		Dictionary_2_t1224867506 * L_4 = (Dictionary_2_t1224867506 *)__this->get_dictionary_0();
		Il2CppArray * L_5 = ___array0;
		int32_t L_6 = ___index1;
		NullCheck((Dictionary_2_t1224867506 *)L_4);
		((  void (*) (Dictionary_2_t1224867506 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t1224867506 *)L_4, (Il2CppArray *)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t1224867506 * L_7 = (Dictionary_2_t1224867506 *)__this->get_dictionary_0();
		Il2CppArray * L_8 = ___array0;
		int32_t L_9 = ___index1;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t4196310754 * L_11 = (Transform_1_t4196310754 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t4196310754 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_11, (Il2CppObject *)NULL, (IntPtr_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t1224867506 *)L_7);
		((  void (*) (Dictionary_2_t1224867506 *, Il2CppArray *, int32_t, Transform_1_t4196310754 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Dictionary_2_t1224867506 *)L_7, (Il2CppArray *)L_8, (int32_t)L_9, (Transform_1_t4196310754 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3631903454_gshared (KeyCollection_t3708365277 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t3708365277 *)__this);
		Enumerator_t3914370944  L_0 = ((  Enumerator_t3914370944  (*) (KeyCollection_t3708365277 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t3708365277 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t3914370944  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2757042039_gshared (KeyCollection_t3708365277 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m907397289_gshared (KeyCollection_t3708365277 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.ICollection.get_SyncRoot()
extern Il2CppClass* ICollection_t91669223_il2cpp_TypeInfo_var;
extern const uint32_t KeyCollection_System_Collections_ICollection_get_SyncRoot_m3998101453_MetadataUsageId;
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m3998101453_gshared (KeyCollection_t3708365277 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_ICollection_get_SyncRoot_m3998101453_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t1224867506 * L_0 = (Dictionary_2_t1224867506 *)__this->get_dictionary_0();
		NullCheck((Il2CppObject *)L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t91669223_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m1902388931_gshared (KeyCollection_t3708365277 * __this, ObjectU5BU5D_t3614634134* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		Dictionary_2_t1224867506 * L_0 = (Dictionary_2_t1224867506 *)__this->get_dictionary_0();
		ObjectU5BU5D_t3614634134* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Dictionary_2_t1224867506 *)L_0);
		((  void (*) (Dictionary_2_t1224867506 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t1224867506 *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t1224867506 * L_3 = (Dictionary_2_t1224867506 *)__this->get_dictionary_0();
		ObjectU5BU5D_t3614634134* L_4 = ___array0;
		int32_t L_5 = ___index1;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t4196310754 * L_7 = (Transform_1_t4196310754 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t4196310754 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_7, (Il2CppObject *)NULL, (IntPtr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t1224867506 *)L_3);
		((  void (*) (Dictionary_2_t1224867506 *, ObjectU5BU5D_t3614634134*, int32_t, Transform_1_t4196310754 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((Dictionary_2_t1224867506 *)L_3, (ObjectU5BU5D_t3614634134*)L_4, (int32_t)L_5, (Transform_1_t4196310754 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return;
	}
}
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::GetEnumerator()
extern "C"  Enumerator_t3914370944  KeyCollection_GetEnumerator_m131469480_gshared (KeyCollection_t3708365277 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t1224867506 * L_0 = (Dictionary_2_t1224867506 *)__this->get_dictionary_0();
		Enumerator_t3914370944  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Enumerator__ctor_m4088904673(&L_1, (Dictionary_2_t1224867506 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m4252672945_gshared (KeyCollection_t3708365277 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t1224867506 * L_0 = (Dictionary_2_t1224867506 *)__this->get_dictionary_0();
		NullCheck((Dictionary_2_t1224867506 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Dictionary_2_t1224867506 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t1224867506 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3629755714;
extern const uint32_t KeyCollection__ctor_m1150705803_MetadataUsageId;
extern "C"  void KeyCollection__ctor_m1150705803_gshared (KeyCollection_t1606165321 * __this, Dictionary_2_t3417634846 * ___dictionary0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection__ctor_m1150705803_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t3417634846 * L_0 = ___dictionary0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral3629755714, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		Dictionary_2_t3417634846 * L_2 = ___dictionary0;
		__this->set_dictionary_0(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Boolean>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3083232821_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3083232821_gshared (KeyCollection_t1606165321 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3083232821_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Boolean>::System.Collections.Generic.ICollection<TKey>.Clear()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1899586830_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1899586830_gshared (KeyCollection_t1606165321 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1899586830_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Boolean>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m4184116031_gshared (KeyCollection_t1606165321 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		Dictionary_2_t3417634846 * L_0 = (Dictionary_2_t3417634846 *)__this->get_dictionary_0();
		Il2CppObject * L_1 = ___item0;
		NullCheck((Dictionary_2_t3417634846 *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t3417634846 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t3417634846 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Boolean>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2523591834_MetadataUsageId;
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2523591834_gshared (KeyCollection_t1606165321 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2523591834_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Boolean>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2668668382_gshared (KeyCollection_t1606165321 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t1606165321 *)__this);
		Enumerator_t1812170988  L_0 = ((  Enumerator_t1812170988  (*) (KeyCollection_t1606165321 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t1606165321 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t1812170988  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Boolean>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m2400289464_gshared (KeyCollection_t1606165321 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	ObjectU5BU5D_t3614634134* V_0 = NULL;
	{
		Il2CppArray * L_0 = ___array0;
		V_0 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3)));
		ObjectU5BU5D_t3614634134* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_2 = V_0;
		int32_t L_3 = ___index1;
		NullCheck((KeyCollection_t1606165321 *)__this);
		((  void (*) (KeyCollection_t1606165321 *, ObjectU5BU5D_t3614634134*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((KeyCollection_t1606165321 *)__this, (ObjectU5BU5D_t3614634134*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}

IL_0016:
	{
		Dictionary_2_t3417634846 * L_4 = (Dictionary_2_t3417634846 *)__this->get_dictionary_0();
		Il2CppArray * L_5 = ___array0;
		int32_t L_6 = ___index1;
		NullCheck((Dictionary_2_t3417634846 *)L_4);
		((  void (*) (Dictionary_2_t3417634846 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t3417634846 *)L_4, (Il2CppArray *)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t3417634846 * L_7 = (Dictionary_2_t3417634846 *)__this->get_dictionary_0();
		Il2CppArray * L_8 = ___array0;
		int32_t L_9 = ___index1;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t1854428742 * L_11 = (Transform_1_t1854428742 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t1854428742 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_11, (Il2CppObject *)NULL, (IntPtr_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t3417634846 *)L_7);
		((  void (*) (Dictionary_2_t3417634846 *, Il2CppArray *, int32_t, Transform_1_t1854428742 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Dictionary_2_t3417634846 *)L_7, (Il2CppArray *)L_8, (int32_t)L_9, (Transform_1_t1854428742 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Boolean>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3918984589_gshared (KeyCollection_t1606165321 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t1606165321 *)__this);
		Enumerator_t1812170988  L_0 = ((  Enumerator_t1812170988  (*) (KeyCollection_t1606165321 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t1606165321 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t1812170988  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Boolean>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2773140624_gshared (KeyCollection_t1606165321 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Boolean>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1443910992_gshared (KeyCollection_t1606165321 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Boolean>::System.Collections.ICollection.get_SyncRoot()
extern Il2CppClass* ICollection_t91669223_il2cpp_TypeInfo_var;
extern const uint32_t KeyCollection_System_Collections_ICollection_get_SyncRoot_m4267671038_MetadataUsageId;
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m4267671038_gshared (KeyCollection_t1606165321 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_ICollection_get_SyncRoot_m4267671038_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t3417634846 * L_0 = (Dictionary_2_t3417634846 *)__this->get_dictionary_0();
		NullCheck((Il2CppObject *)L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t91669223_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Boolean>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m668976366_gshared (KeyCollection_t1606165321 * __this, ObjectU5BU5D_t3614634134* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		Dictionary_2_t3417634846 * L_0 = (Dictionary_2_t3417634846 *)__this->get_dictionary_0();
		ObjectU5BU5D_t3614634134* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Dictionary_2_t3417634846 *)L_0);
		((  void (*) (Dictionary_2_t3417634846 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t3417634846 *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t3417634846 * L_3 = (Dictionary_2_t3417634846 *)__this->get_dictionary_0();
		ObjectU5BU5D_t3614634134* L_4 = ___array0;
		int32_t L_5 = ___index1;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t1854428742 * L_7 = (Transform_1_t1854428742 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t1854428742 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_7, (Il2CppObject *)NULL, (IntPtr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t3417634846 *)L_3);
		((  void (*) (Dictionary_2_t3417634846 *, ObjectU5BU5D_t3614634134*, int32_t, Transform_1_t1854428742 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((Dictionary_2_t3417634846 *)L_3, (ObjectU5BU5D_t3614634134*)L_4, (int32_t)L_5, (Transform_1_t1854428742 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return;
	}
}
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Boolean>::GetEnumerator()
extern "C"  Enumerator_t1812170988  KeyCollection_GetEnumerator_m136587363_gshared (KeyCollection_t1606165321 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t3417634846 * L_0 = (Dictionary_2_t3417634846 *)__this->get_dictionary_0();
		Enumerator_t1812170988  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Enumerator__ctor_m4035163080(&L_1, (Dictionary_2_t3417634846 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Boolean>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m515229044_gshared (KeyCollection_t1606165321 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t3417634846 * L_0 = (Dictionary_2_t3417634846 *)__this->get_dictionary_0();
		NullCheck((Dictionary_2_t3417634846 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Dictionary_2_t3417634846 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t3417634846 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3629755714;
extern const uint32_t KeyCollection__ctor_m1627846113_MetadataUsageId;
extern "C"  void KeyCollection__ctor_m1627846113_gshared (KeyCollection_t4147435347 * __this, Dictionary_2_t1663937576 * ___dictionary0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection__ctor_m1627846113_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t1663937576 * L_0 = ___dictionary0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral3629755714, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		Dictionary_2_t1663937576 * L_2 = ___dictionary0;
		__this->set_dictionary_0(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Int32>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3888838575_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3888838575_gshared (KeyCollection_t4147435347 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3888838575_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Int32>::System.Collections.Generic.ICollection<TKey>.Clear()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m237922362_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m237922362_gshared (KeyCollection_t4147435347 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m237922362_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Int32>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1114577217_gshared (KeyCollection_t4147435347 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		Dictionary_2_t1663937576 * L_0 = (Dictionary_2_t1663937576 *)__this->get_dictionary_0();
		Il2CppObject * L_1 = ___item0;
		NullCheck((Dictionary_2_t1663937576 *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t1663937576 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t1663937576 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Int32>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2082552954_MetadataUsageId;
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2082552954_gshared (KeyCollection_t4147435347 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2082552954_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Int32>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m4237862166_gshared (KeyCollection_t4147435347 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t4147435347 *)__this);
		Enumerator_t58473718  L_0 = ((  Enumerator_t58473718  (*) (KeyCollection_t4147435347 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t4147435347 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t58473718  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m3219210900_gshared (KeyCollection_t4147435347 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	ObjectU5BU5D_t3614634134* V_0 = NULL;
	{
		Il2CppArray * L_0 = ___array0;
		V_0 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3)));
		ObjectU5BU5D_t3614634134* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_2 = V_0;
		int32_t L_3 = ___index1;
		NullCheck((KeyCollection_t4147435347 *)__this);
		((  void (*) (KeyCollection_t4147435347 *, ObjectU5BU5D_t3614634134*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((KeyCollection_t4147435347 *)__this, (ObjectU5BU5D_t3614634134*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}

IL_0016:
	{
		Dictionary_2_t1663937576 * L_4 = (Dictionary_2_t1663937576 *)__this->get_dictionary_0();
		Il2CppArray * L_5 = ___array0;
		int32_t L_6 = ___index1;
		NullCheck((Dictionary_2_t1663937576 *)L_4);
		((  void (*) (Dictionary_2_t1663937576 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t1663937576 *)L_4, (Il2CppArray *)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t1663937576 * L_7 = (Dictionary_2_t1663937576 *)__this->get_dictionary_0();
		Il2CppArray * L_8 = ___array0;
		int32_t L_9 = ___index1;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t1316794068 * L_11 = (Transform_1_t1316794068 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t1316794068 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_11, (Il2CppObject *)NULL, (IntPtr_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t1663937576 *)L_7);
		((  void (*) (Dictionary_2_t1663937576 *, Il2CppArray *, int32_t, Transform_1_t1316794068 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Dictionary_2_t1663937576 *)L_7, (Il2CppArray *)L_8, (int32_t)L_9, (Transform_1_t1316794068 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3069883463_gshared (KeyCollection_t4147435347 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t4147435347 *)__this);
		Enumerator_t58473718  L_0 = ((  Enumerator_t58473718  (*) (KeyCollection_t4147435347 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t4147435347 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t58473718  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Int32>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2823494596_gshared (KeyCollection_t4147435347 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1497186736_gshared (KeyCollection_t4147435347 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern Il2CppClass* ICollection_t91669223_il2cpp_TypeInfo_var;
extern const uint32_t KeyCollection_System_Collections_ICollection_get_SyncRoot_m3825728526_MetadataUsageId;
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m3825728526_gshared (KeyCollection_t4147435347 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_ICollection_get_SyncRoot_m3825728526_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t1663937576 * L_0 = (Dictionary_2_t1663937576 *)__this->get_dictionary_0();
		NullCheck((Il2CppObject *)L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t91669223_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Int32>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m1942070334_gshared (KeyCollection_t4147435347 * __this, ObjectU5BU5D_t3614634134* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		Dictionary_2_t1663937576 * L_0 = (Dictionary_2_t1663937576 *)__this->get_dictionary_0();
		ObjectU5BU5D_t3614634134* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Dictionary_2_t1663937576 *)L_0);
		((  void (*) (Dictionary_2_t1663937576 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t1663937576 *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t1663937576 * L_3 = (Dictionary_2_t1663937576 *)__this->get_dictionary_0();
		ObjectU5BU5D_t3614634134* L_4 = ___array0;
		int32_t L_5 = ___index1;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t1316794068 * L_7 = (Transform_1_t1316794068 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t1316794068 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_7, (Il2CppObject *)NULL, (IntPtr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t1663937576 *)L_3);
		((  void (*) (Dictionary_2_t1663937576 *, ObjectU5BU5D_t3614634134*, int32_t, Transform_1_t1316794068 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((Dictionary_2_t1663937576 *)L_3, (ObjectU5BU5D_t3614634134*)L_4, (int32_t)L_5, (Transform_1_t1316794068 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return;
	}
}
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Int32>::GetEnumerator()
extern "C"  Enumerator_t58473718  KeyCollection_GetEnumerator_m3925297057_gshared (KeyCollection_t4147435347 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t1663937576 * L_0 = (Dictionary_2_t1663937576 *)__this->get_dictionary_0();
		Enumerator_t58473718  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Enumerator__ctor_m1642475044(&L_1, (Dictionary_2_t1663937576 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Int32>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m3930094200_gshared (KeyCollection_t4147435347 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t1663937576 * L_0 = (Dictionary_2_t1663937576 *)__this->get_dictionary_0();
		NullCheck((Dictionary_2_t1663937576 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Dictionary_2_t1663937576 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t1663937576 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3629755714;
extern const uint32_t KeyCollection__ctor_m4000691336_MetadataUsageId;
extern "C"  void KeyCollection__ctor_m4000691336_gshared (KeyCollection_t470039898 * __this, Dictionary_2_t2281509423 * ___dictionary0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection__ctor_m4000691336_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t2281509423 * L_0 = ___dictionary0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral3629755714, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		Dictionary_2_t2281509423 * L_2 = ___dictionary0;
		__this->set_dictionary_0(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Object>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m726860246_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m726860246_gshared (KeyCollection_t470039898 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m726860246_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Object>::System.Collections.Generic.ICollection<TKey>.Clear()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3185000447_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3185000447_gshared (KeyCollection_t470039898 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3185000447_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Object>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m580889838_gshared (KeyCollection_t470039898 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		Dictionary_2_t2281509423 * L_0 = (Dictionary_2_t2281509423 *)__this->get_dictionary_0();
		Il2CppObject * L_1 = ___item0;
		NullCheck((Dictionary_2_t2281509423 *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t2281509423 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t2281509423 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Object>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1818919095_MetadataUsageId;
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1818919095_gshared (KeyCollection_t470039898 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1818919095_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Object>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m701895513_gshared (KeyCollection_t470039898 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t470039898 *)__this);
		Enumerator_t676045565  L_0 = ((  Enumerator_t676045565  (*) (KeyCollection_t470039898 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t470039898 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t676045565  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m201091229_gshared (KeyCollection_t470039898 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	ObjectU5BU5D_t3614634134* V_0 = NULL;
	{
		Il2CppArray * L_0 = ___array0;
		V_0 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3)));
		ObjectU5BU5D_t3614634134* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_2 = V_0;
		int32_t L_3 = ___index1;
		NullCheck((KeyCollection_t470039898 *)__this);
		((  void (*) (KeyCollection_t470039898 *, ObjectU5BU5D_t3614634134*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((KeyCollection_t470039898 *)__this, (ObjectU5BU5D_t3614634134*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}

IL_0016:
	{
		Dictionary_2_t2281509423 * L_4 = (Dictionary_2_t2281509423 *)__this->get_dictionary_0();
		Il2CppArray * L_5 = ___array0;
		int32_t L_6 = ___index1;
		NullCheck((Dictionary_2_t2281509423 *)L_4);
		((  void (*) (Dictionary_2_t2281509423 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t2281509423 *)L_4, (Il2CppArray *)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t2281509423 * L_7 = (Dictionary_2_t2281509423 *)__this->get_dictionary_0();
		Il2CppArray * L_8 = ___array0;
		int32_t L_9 = ___index1;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t1901028561 * L_11 = (Transform_1_t1901028561 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t1901028561 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_11, (Il2CppObject *)NULL, (IntPtr_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t2281509423 *)L_7);
		((  void (*) (Dictionary_2_t2281509423 *, Il2CppArray *, int32_t, Transform_1_t1901028561 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Dictionary_2_t2281509423 *)L_7, (Il2CppArray *)L_8, (int32_t)L_9, (Transform_1_t1901028561 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1743416022_gshared (KeyCollection_t470039898 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t470039898 *)__this);
		Enumerator_t676045565  L_0 = ((  Enumerator_t676045565  (*) (KeyCollection_t470039898 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t470039898 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t676045565  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Object>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m701366755_gshared (KeyCollection_t470039898 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m4278618649_gshared (KeyCollection_t470039898 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern Il2CppClass* ICollection_t91669223_il2cpp_TypeInfo_var;
extern const uint32_t KeyCollection_System_Collections_ICollection_get_SyncRoot_m3348206461_MetadataUsageId;
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m3348206461_gshared (KeyCollection_t470039898 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_ICollection_get_SyncRoot_m3348206461_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t2281509423 * L_0 = (Dictionary_2_t2281509423 *)__this->get_dictionary_0();
		NullCheck((Il2CppObject *)L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t91669223_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Object>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m1469814847_gshared (KeyCollection_t470039898 * __this, ObjectU5BU5D_t3614634134* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		Dictionary_2_t2281509423 * L_0 = (Dictionary_2_t2281509423 *)__this->get_dictionary_0();
		ObjectU5BU5D_t3614634134* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Dictionary_2_t2281509423 *)L_0);
		((  void (*) (Dictionary_2_t2281509423 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t2281509423 *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t2281509423 * L_3 = (Dictionary_2_t2281509423 *)__this->get_dictionary_0();
		ObjectU5BU5D_t3614634134* L_4 = ___array0;
		int32_t L_5 = ___index1;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t1901028561 * L_7 = (Transform_1_t1901028561 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t1901028561 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_7, (Il2CppObject *)NULL, (IntPtr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t2281509423 *)L_3);
		((  void (*) (Dictionary_2_t2281509423 *, ObjectU5BU5D_t3614634134*, int32_t, Transform_1_t1901028561 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((Dictionary_2_t2281509423 *)L_3, (ObjectU5BU5D_t3614634134*)L_4, (int32_t)L_5, (Transform_1_t1901028561 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return;
	}
}
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Object>::GetEnumerator()
extern "C"  Enumerator_t676045565  KeyCollection_GetEnumerator_m3123493604_gshared (KeyCollection_t470039898 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t2281509423 * L_0 = (Dictionary_2_t2281509423 *)__this->get_dictionary_0();
		Enumerator_t676045565  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Enumerator__ctor_m2650732273(&L_1, (Dictionary_2_t2281509423 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Object>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m2913499705_gshared (KeyCollection_t470039898 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t2281509423 * L_0 = (Dictionary_2_t2281509423 *)__this->get_dictionary_0();
		NullCheck((Dictionary_2_t2281509423 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Dictionary_2_t2281509423 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t2281509423 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3629755714;
extern const uint32_t KeyCollection__ctor_m547435096_MetadataUsageId;
extern "C"  void KeyCollection__ctor_m547435096_gshared (KeyCollection_t3210635913 * __this, Dictionary_2_t727138142 * ___dictionary0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection__ctor_m547435096_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t727138142 * L_0 = ___dictionary0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral3629755714, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		Dictionary_2_t727138142 * L_2 = ___dictionary0;
		__this->set_dictionary_0(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1668870622_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1668870622_gshared (KeyCollection_t3210635913 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1668870622_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.Generic.ICollection<TKey>.Clear()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2756193719_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2756193719_gshared (KeyCollection_t3210635913 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2756193719_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m833199734_gshared (KeyCollection_t3210635913 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		Dictionary_2_t727138142 * L_0 = (Dictionary_2_t727138142 *)__this->get_dictionary_0();
		Il2CppObject * L_1 = ___item0;
		NullCheck((Dictionary_2_t727138142 *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t727138142 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t727138142 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3201040055_MetadataUsageId;
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3201040055_gshared (KeyCollection_t3210635913 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3201040055_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1066449161_gshared (KeyCollection_t3210635913 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t3210635913 *)__this);
		Enumerator_t3416641580  L_0 = ((  Enumerator_t3416641580  (*) (KeyCollection_t3210635913 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t3210635913 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t3416641580  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m3169515245_gshared (KeyCollection_t3210635913 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	ObjectU5BU5D_t3614634134* V_0 = NULL;
	{
		Il2CppArray * L_0 = ___array0;
		V_0 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3)));
		ObjectU5BU5D_t3614634134* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_2 = V_0;
		int32_t L_3 = ___index1;
		NullCheck((KeyCollection_t3210635913 *)__this);
		((  void (*) (KeyCollection_t3210635913 *, ObjectU5BU5D_t3614634134*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((KeyCollection_t3210635913 *)__this, (ObjectU5BU5D_t3614634134*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}

IL_0016:
	{
		Dictionary_2_t727138142 * L_4 = (Dictionary_2_t727138142 *)__this->get_dictionary_0();
		Il2CppArray * L_5 = ___array0;
		int32_t L_6 = ___index1;
		NullCheck((Dictionary_2_t727138142 *)L_4);
		((  void (*) (Dictionary_2_t727138142 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t727138142 *)L_4, (Il2CppArray *)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t727138142 * L_7 = (Dictionary_2_t727138142 *)__this->get_dictionary_0();
		Il2CppArray * L_8 = ___array0;
		int32_t L_9 = ___index1;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t2213661190 * L_11 = (Transform_1_t2213661190 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t2213661190 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_11, (Il2CppObject *)NULL, (IntPtr_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t727138142 *)L_7);
		((  void (*) (Dictionary_2_t727138142 *, Il2CppArray *, int32_t, Transform_1_t2213661190 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Dictionary_2_t727138142 *)L_7, (Il2CppArray *)L_8, (int32_t)L_9, (Transform_1_t2213661190 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m4259404390_gshared (KeyCollection_t3210635913 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t3210635913 *)__this);
		Enumerator_t3416641580  L_0 = ((  Enumerator_t3416641580  (*) (KeyCollection_t3210635913 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t3210635913 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t3416641580  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m853124483_gshared (KeyCollection_t3210635913 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1428134417_gshared (KeyCollection_t3210635913 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.ICollection.get_SyncRoot()
extern Il2CppClass* ICollection_t91669223_il2cpp_TypeInfo_var;
extern const uint32_t KeyCollection_System_Collections_ICollection_get_SyncRoot_m2308578909_MetadataUsageId;
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m2308578909_gshared (KeyCollection_t3210635913 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_ICollection_get_SyncRoot_m2308578909_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t727138142 * L_0 = (Dictionary_2_t727138142 *)__this->get_dictionary_0();
		NullCheck((Il2CppObject *)L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t91669223_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m4000657415_gshared (KeyCollection_t3210635913 * __this, ObjectU5BU5D_t3614634134* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		Dictionary_2_t727138142 * L_0 = (Dictionary_2_t727138142 *)__this->get_dictionary_0();
		ObjectU5BU5D_t3614634134* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Dictionary_2_t727138142 *)L_0);
		((  void (*) (Dictionary_2_t727138142 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t727138142 *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t727138142 * L_3 = (Dictionary_2_t727138142 *)__this->get_dictionary_0();
		ObjectU5BU5D_t3614634134* L_4 = ___array0;
		int32_t L_5 = ___index1;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t2213661190 * L_7 = (Transform_1_t2213661190 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t2213661190 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_7, (Il2CppObject *)NULL, (IntPtr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t727138142 *)L_3);
		((  void (*) (Dictionary_2_t727138142 *, ObjectU5BU5D_t3614634134*, int32_t, Transform_1_t2213661190 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((Dictionary_2_t727138142 *)L_3, (ObjectU5BU5D_t3614634134*)L_4, (int32_t)L_5, (Transform_1_t2213661190 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return;
	}
}
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::GetEnumerator()
extern "C"  Enumerator_t3416641580  KeyCollection_GetEnumerator_m1247305548_gshared (KeyCollection_t3210635913 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t727138142 * L_0 = (Dictionary_2_t727138142 *)__this->get_dictionary_0();
		Enumerator_t3416641580  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Enumerator__ctor_m860887073(&L_1, (Dictionary_2_t727138142 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m3260694481_gshared (KeyCollection_t3210635913 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t727138142 * L_0 = (Dictionary_2_t727138142 *)__this->get_dictionary_0();
		NullCheck((Dictionary_2_t727138142 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Dictionary_2_t727138142 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t727138142 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3629755714;
extern const uint32_t KeyCollection__ctor_m1439175414_MetadataUsageId;
extern "C"  void KeyCollection__ctor_m1439175414_gshared (KeyCollection_t1838728343 * __this, Dictionary_2_t3650197868 * ___dictionary0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection__ctor_m1439175414_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t3650197868 * L_0 = ___dictionary0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral3629755714, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		Dictionary_2_t3650197868 * L_2 = ___dictionary0;
		__this->set_dictionary_0(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m741303632_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m741303632_gshared (KeyCollection_t1838728343 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m741303632_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.Generic.ICollection<TKey>.Clear()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m849613147_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m849613147_gshared (KeyCollection_t1838728343 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m849613147_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3874625808_gshared (KeyCollection_t1838728343 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		Dictionary_2_t3650197868 * L_0 = (Dictionary_2_t3650197868 *)__this->get_dictionary_0();
		Il2CppObject * L_1 = ___item0;
		NullCheck((Dictionary_2_t3650197868 *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t3650197868 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t3650197868 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1301535531_MetadataUsageId;
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1301535531_gshared (KeyCollection_t1838728343 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1301535531_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3508981317_gshared (KeyCollection_t1838728343 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t1838728343 *)__this);
		Enumerator_t2044734010  L_0 = ((  Enumerator_t2044734010  (*) (KeyCollection_t1838728343 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t1838728343 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t2044734010  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m2690697909_gshared (KeyCollection_t1838728343 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	ObjectU5BU5D_t3614634134* V_0 = NULL;
	{
		Il2CppArray * L_0 = ___array0;
		V_0 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3)));
		ObjectU5BU5D_t3614634134* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_2 = V_0;
		int32_t L_3 = ___index1;
		NullCheck((KeyCollection_t1838728343 *)__this);
		((  void (*) (KeyCollection_t1838728343 *, ObjectU5BU5D_t3614634134*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((KeyCollection_t1838728343 *)__this, (ObjectU5BU5D_t3614634134*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}

IL_0016:
	{
		Dictionary_2_t3650197868 * L_4 = (Dictionary_2_t3650197868 *)__this->get_dictionary_0();
		Il2CppArray * L_5 = ___array0;
		int32_t L_6 = ___index1;
		NullCheck((Dictionary_2_t3650197868 *)L_4);
		((  void (*) (Dictionary_2_t3650197868 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t3650197868 *)L_4, (Il2CppArray *)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t3650197868 * L_7 = (Dictionary_2_t3650197868 *)__this->get_dictionary_0();
		Il2CppArray * L_8 = ___array0;
		int32_t L_9 = ___index1;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t4135956480 * L_11 = (Transform_1_t4135956480 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t4135956480 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_11, (Il2CppObject *)NULL, (IntPtr_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t3650197868 *)L_7);
		((  void (*) (Dictionary_2_t3650197868 *, Il2CppArray *, int32_t, Transform_1_t4135956480 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Dictionary_2_t3650197868 *)L_7, (Il2CppArray *)L_8, (int32_t)L_9, (Transform_1_t4135956480 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1851700276_gshared (KeyCollection_t1838728343 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t1838728343 *)__this);
		Enumerator_t2044734010  L_0 = ((  Enumerator_t2044734010  (*) (KeyCollection_t1838728343 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t1838728343 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t2044734010  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3045537847_gshared (KeyCollection_t1838728343 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1756756033_gshared (KeyCollection_t1838728343 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.ICollection.get_SyncRoot()
extern Il2CppClass* ICollection_t91669223_il2cpp_TypeInfo_var;
extern const uint32_t KeyCollection_System_Collections_ICollection_get_SyncRoot_m659350425_MetadataUsageId;
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m659350425_gshared (KeyCollection_t1838728343 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_ICollection_get_SyncRoot_m659350425_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t3650197868 * L_0 = (Dictionary_2_t3650197868 *)__this->get_dictionary_0();
		NullCheck((Il2CppObject *)L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t91669223_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m847362259_gshared (KeyCollection_t1838728343 * __this, ObjectU5BU5D_t3614634134* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		Dictionary_2_t3650197868 * L_0 = (Dictionary_2_t3650197868 *)__this->get_dictionary_0();
		ObjectU5BU5D_t3614634134* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Dictionary_2_t3650197868 *)L_0);
		((  void (*) (Dictionary_2_t3650197868 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t3650197868 *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t3650197868 * L_3 = (Dictionary_2_t3650197868 *)__this->get_dictionary_0();
		ObjectU5BU5D_t3614634134* L_4 = ___array0;
		int32_t L_5 = ___index1;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t4135956480 * L_7 = (Transform_1_t4135956480 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t4135956480 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_7, (Il2CppObject *)NULL, (IntPtr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t3650197868 *)L_3);
		((  void (*) (Dictionary_2_t3650197868 *, ObjectU5BU5D_t3614634134*, int32_t, Transform_1_t4135956480 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((Dictionary_2_t3650197868 *)L_3, (ObjectU5BU5D_t3614634134*)L_4, (int32_t)L_5, (Transform_1_t4135956480 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return;
	}
}
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::GetEnumerator()
extern "C"  Enumerator_t2044734010  KeyCollection_GetEnumerator_m3668909536_gshared (KeyCollection_t1838728343 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t3650197868 * L_0 = (Dictionary_2_t3650197868 *)__this->get_dictionary_0();
		Enumerator_t2044734010  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Enumerator__ctor_m4120924977(&L_1, (Dictionary_2_t3650197868 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m3604538821_gshared (KeyCollection_t1838728343 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t3650197868 * L_0 = (Dictionary_2_t3650197868 *)__this->get_dictionary_0();
		NullCheck((Dictionary_2_t3650197868 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Dictionary_2_t3650197868 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t3650197868 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3629755714;
extern const uint32_t KeyCollection__ctor_m2534614652_MetadataUsageId;
extern "C"  void KeyCollection__ctor_m2534614652_gshared (KeyCollection_t919388301 * __this, Dictionary_2_t2730857826 * ___dictionary0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection__ctor_m2534614652_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t2730857826 * L_0 = ___dictionary0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral3629755714, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		Dictionary_2_t2730857826 * L_2 = ___dictionary0;
		__this->set_dictionary_0(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m93312222_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m93312222_gshared (KeyCollection_t919388301 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m93312222_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.ICollection<TKey>.Clear()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1060683979_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1060683979_gshared (KeyCollection_t919388301 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1060683979_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m167656554_gshared (KeyCollection_t919388301 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		Dictionary_2_t2730857826 * L_0 = (Dictionary_2_t2730857826 *)__this->get_dictionary_0();
		Il2CppObject * L_1 = ___item0;
		NullCheck((Dictionary_2_t2730857826 *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t2730857826 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t2730857826 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m936199739_MetadataUsageId;
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m936199739_gshared (KeyCollection_t919388301 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m936199739_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m4209558273_gshared (KeyCollection_t919388301 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t919388301 *)__this);
		Enumerator_t1125393968  L_0 = ((  Enumerator_t1125393968  (*) (KeyCollection_t919388301 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t919388301 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t1125393968  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m196917069_gshared (KeyCollection_t919388301 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	ObjectU5BU5D_t3614634134* V_0 = NULL;
	{
		Il2CppArray * L_0 = ___array0;
		V_0 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3)));
		ObjectU5BU5D_t3614634134* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_2 = V_0;
		int32_t L_3 = ___index1;
		NullCheck((KeyCollection_t919388301 *)__this);
		((  void (*) (KeyCollection_t919388301 *, ObjectU5BU5D_t3614634134*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((KeyCollection_t919388301 *)__this, (ObjectU5BU5D_t3614634134*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}

IL_0016:
	{
		Dictionary_2_t2730857826 * L_4 = (Dictionary_2_t2730857826 *)__this->get_dictionary_0();
		Il2CppArray * L_5 = ___array0;
		int32_t L_6 = ___index1;
		NullCheck((Dictionary_2_t2730857826 *)L_4);
		((  void (*) (Dictionary_2_t2730857826 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t2730857826 *)L_4, (Il2CppArray *)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t2730857826 * L_7 = (Dictionary_2_t2730857826 *)__this->get_dictionary_0();
		Il2CppArray * L_8 = ___array0;
		int32_t L_9 = ___index1;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t4054574962 * L_11 = (Transform_1_t4054574962 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t4054574962 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_11, (Il2CppObject *)NULL, (IntPtr_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t2730857826 *)L_7);
		((  void (*) (Dictionary_2_t2730857826 *, Il2CppArray *, int32_t, Transform_1_t4054574962 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Dictionary_2_t2730857826 *)L_7, (Il2CppArray *)L_8, (int32_t)L_9, (Transform_1_t4054574962 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1631701166_gshared (KeyCollection_t919388301 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t919388301 *)__this);
		Enumerator_t1125393968  L_0 = ((  Enumerator_t1125393968  (*) (KeyCollection_t919388301 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t919388301 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t1125393968  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1142379495_gshared (KeyCollection_t919388301 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m778075033_gshared (KeyCollection_t919388301 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.ICollection.get_SyncRoot()
extern Il2CppClass* ICollection_t91669223_il2cpp_TypeInfo_var;
extern const uint32_t KeyCollection_System_Collections_ICollection_get_SyncRoot_m2096719453_MetadataUsageId;
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m2096719453_gshared (KeyCollection_t919388301 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_ICollection_get_SyncRoot_m2096719453_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t2730857826 * L_0 = (Dictionary_2_t2730857826 *)__this->get_dictionary_0();
		NullCheck((Il2CppObject *)L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t91669223_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,UnityEngine.TextEditor/TextEditOp>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m1945683251_gshared (KeyCollection_t919388301 * __this, ObjectU5BU5D_t3614634134* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		Dictionary_2_t2730857826 * L_0 = (Dictionary_2_t2730857826 *)__this->get_dictionary_0();
		ObjectU5BU5D_t3614634134* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Dictionary_2_t2730857826 *)L_0);
		((  void (*) (Dictionary_2_t2730857826 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t2730857826 *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t2730857826 * L_3 = (Dictionary_2_t2730857826 *)__this->get_dictionary_0();
		ObjectU5BU5D_t3614634134* L_4 = ___array0;
		int32_t L_5 = ___index1;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t4054574962 * L_7 = (Transform_1_t4054574962 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t4054574962 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_7, (Il2CppObject *)NULL, (IntPtr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t2730857826 *)L_3);
		((  void (*) (Dictionary_2_t2730857826 *, ObjectU5BU5D_t3614634134*, int32_t, Transform_1_t4054574962 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((Dictionary_2_t2730857826 *)L_3, (ObjectU5BU5D_t3614634134*)L_4, (int32_t)L_5, (Transform_1_t4054574962 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return;
	}
}
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,UnityEngine.TextEditor/TextEditOp>::GetEnumerator()
extern "C"  Enumerator_t1125393968  KeyCollection_GetEnumerator_m2119247512_gshared (KeyCollection_t919388301 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t2730857826 * L_0 = (Dictionary_2_t2730857826 *)__this->get_dictionary_0();
		Enumerator_t1125393968  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Enumerator__ctor_m1836822257(&L_1, (Dictionary_2_t2730857826 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m1745786689_gshared (KeyCollection_t919388301 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t2730857826 * L_0 = (Dictionary_2_t2730857826 *)__this->get_dictionary_0();
		NullCheck((Dictionary_2_t2730857826 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Dictionary_2_t2730857826 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t2730857826 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<Amazon.Runtime.Metric,System.Int64>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m2883604990_gshared (ShimEnumerator_t3764281347 * __this, Dictionary_2_t3659156526 * ___host0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t3659156526 * L_0 = ___host0;
		NullCheck((Dictionary_2_t3659156526 *)L_0);
		Enumerator_t684213932  L_1 = ((  Enumerator_t684213932  (*) (Dictionary_2_t3659156526 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t3659156526 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<Amazon.Runtime.Metric,System.Int64>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m762667293_gshared (ShimEnumerator_t3764281347 * __this, const MethodInfo* method)
{
	{
		Enumerator_t684213932 * L_0 = (Enumerator_t684213932 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m1520629361((Enumerator_t684213932 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_1;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<Amazon.Runtime.Metric,System.Int64>::get_Entry()
extern Il2CppClass* IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Entry_m231825841_MetadataUsageId;
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m231825841_gshared (ShimEnumerator_t3764281347 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Entry_m231825841_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_t684213932  L_0 = (Enumerator_t684213932 )__this->get_host_enumerator_0();
		Enumerator_t684213932  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		DictionaryEntry_t3048875398  L_3 = InterfaceFuncInvoker0< DictionaryEntry_t3048875398  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		return L_3;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Amazon.Runtime.Metric,System.Int64>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m3697060374_gshared (ShimEnumerator_t3764281347 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1416501748  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t684213932 * L_0 = (Enumerator_t684213932 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t1416501748  L_1 = Enumerator_get_Current_m2197039620((Enumerator_t684213932 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t1416501748 )L_1;
		int32_t L_2 = KeyValuePair_2_get_Key_m404179876((KeyValuePair_2_t1416501748 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Amazon.Runtime.Metric,System.Int64>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m1899706092_gshared (ShimEnumerator_t3764281347 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1416501748  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t684213932 * L_0 = (Enumerator_t684213932 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t1416501748  L_1 = Enumerator_get_Current_m2197039620((Enumerator_t684213932 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t1416501748 )L_1;
		int64_t L_2 = KeyValuePair_2_get_Value_m3929255215((KeyValuePair_2_t1416501748 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		int64_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Amazon.Runtime.Metric,System.Int64>::get_Current()
extern Il2CppClass* DictionaryEntry_t3048875398_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Current_m2901726476_MetadataUsageId;
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m2901726476_gshared (ShimEnumerator_t3764281347 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Current_m2901726476_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((ShimEnumerator_t3764281347 *)__this);
		DictionaryEntry_t3048875398  L_0 = ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t3764281347 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ShimEnumerator_t3764281347 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		DictionaryEntry_t3048875398  L_1 = L_0;
		Il2CppObject * L_2 = Box(DictionaryEntry_t3048875398_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<Amazon.Runtime.Metric,System.Int64>::Reset()
extern "C"  void ShimEnumerator_Reset_m2790213288_gshared (ShimEnumerator_t3764281347 * __this, const MethodInfo* method)
{
	{
		Enumerator_t684213932 * L_0 = (Enumerator_t684213932 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m1537962437((Enumerator_t684213932 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<Amazon.Runtime.Metric,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m96709936_gshared (ShimEnumerator_t1249685309 * __this, Dictionary_2_t1144560488 * ___host0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t1144560488 * L_0 = ___host0;
		NullCheck((Dictionary_2_t1144560488 *)L_0);
		Enumerator_t2464585190  L_1 = ((  Enumerator_t2464585190  (*) (Dictionary_2_t1144560488 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t1144560488 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<Amazon.Runtime.Metric,System.Object>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m3639386733_gshared (ShimEnumerator_t1249685309 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2464585190 * L_0 = (Enumerator_t2464585190 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m3844071554((Enumerator_t2464585190 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_1;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<Amazon.Runtime.Metric,System.Object>::get_Entry()
extern Il2CppClass* IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Entry_m3554919881_MetadataUsageId;
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m3554919881_gshared (ShimEnumerator_t1249685309 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Entry_m3554919881_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_t2464585190  L_0 = (Enumerator_t2464585190 )__this->get_host_enumerator_0();
		Enumerator_t2464585190  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		DictionaryEntry_t3048875398  L_3 = InterfaceFuncInvoker0< DictionaryEntry_t3048875398  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		return L_3;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Amazon.Runtime.Metric,System.Object>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m2991689478_gshared (ShimEnumerator_t1249685309 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3196873006  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t2464585190 * L_0 = (Enumerator_t2464585190 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t3196873006  L_1 = Enumerator_get_Current_m1831421754((Enumerator_t2464585190 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t3196873006 )L_1;
		int32_t L_2 = KeyValuePair_2_get_Key_m2312610863((KeyValuePair_2_t3196873006 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Amazon.Runtime.Metric,System.Object>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m3635880764_gshared (ShimEnumerator_t1249685309 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3196873006  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t2464585190 * L_0 = (Enumerator_t2464585190 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t3196873006  L_1 = Enumerator_get_Current_m1831421754((Enumerator_t2464585190 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t3196873006 )L_1;
		Il2CppObject * L_2 = KeyValuePair_2_get_Value_m3734313679((KeyValuePair_2_t3196873006 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_2;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Amazon.Runtime.Metric,System.Object>::get_Current()
extern Il2CppClass* DictionaryEntry_t3048875398_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Current_m731476104_MetadataUsageId;
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m731476104_gshared (ShimEnumerator_t1249685309 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Current_m731476104_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((ShimEnumerator_t1249685309 *)__this);
		DictionaryEntry_t3048875398  L_0 = ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t1249685309 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ShimEnumerator_t1249685309 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		DictionaryEntry_t3048875398  L_1 = L_0;
		Il2CppObject * L_2 = Box(DictionaryEntry_t3048875398_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<Amazon.Runtime.Metric,System.Object>::Reset()
extern "C"  void ShimEnumerator_Reset_m2042563214_gshared (ShimEnumerator_t1249685309 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2464585190 * L_0 = (Enumerator_t2464585190 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m1867529997((Enumerator_t2464585190 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m4003681516_gshared (ShimEnumerator_t1184827904 * __this, Dictionary_2_t1079703083 * ___host0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t1079703083 * L_0 = ___host0;
		NullCheck((Dictionary_2_t1079703083 *)L_0);
		Enumerator_t2399727785  L_1 = ((  Enumerator_t2399727785  (*) (Dictionary_2_t1079703083 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t1079703083 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m2566611377_gshared (ShimEnumerator_t1184827904 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2399727785 * L_0 = (Enumerator_t2399727785 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m3809319098((Enumerator_t2399727785 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_1;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>::get_Entry()
extern Il2CppClass* IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Entry_m1209161525_MetadataUsageId;
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m1209161525_gshared (ShimEnumerator_t1184827904 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Entry_m1209161525_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_t2399727785  L_0 = (Enumerator_t2399727785 )__this->get_host_enumerator_0();
		Enumerator_t2399727785  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		DictionaryEntry_t3048875398  L_3 = InterfaceFuncInvoker0< DictionaryEntry_t3048875398  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		return L_3;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m3497731930_gshared (ShimEnumerator_t1184827904 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3132015601  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t2399727785 * L_0 = (Enumerator_t2399727785 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t3132015601  L_1 = Enumerator_get_Current_m3655806242((Enumerator_t2399727785 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t3132015601 )L_1;
		int32_t L_2 = KeyValuePair_2_get_Key_m1683812983((KeyValuePair_2_t3132015601 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m184655936_gshared (ShimEnumerator_t1184827904 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3132015601  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t2399727785 * L_0 = (Enumerator_t2399727785 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t3132015601  L_1 = Enumerator_get_Current_m3655806242((Enumerator_t2399727785 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t3132015601 )L_1;
		int32_t L_2 = KeyValuePair_2_get_Value_m1136571287((KeyValuePair_2_t3132015601 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>::get_Current()
extern Il2CppClass* DictionaryEntry_t3048875398_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Current_m972214768_MetadataUsageId;
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m972214768_gshared (ShimEnumerator_t1184827904 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Current_m972214768_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((ShimEnumerator_t1184827904 *)__this);
		DictionaryEntry_t3048875398  L_0 = ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t1184827904 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ShimEnumerator_t1184827904 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		DictionaryEntry_t3048875398  L_1 = L_0;
		Il2CppObject * L_2 = Box(DictionaryEntry_t3048875398_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>::Reset()
extern "C"  void ShimEnumerator_Reset_m2258067398_gshared (ShimEnumerator_t1184827904 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2399727785 * L_0 = (Enumerator_t2399727785 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m2787013825((Enumerator_t2399727785 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m3996137855_gshared (ShimEnumerator_t1802399751 * __this, Dictionary_2_t1697274930 * ___host0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t1697274930 * L_0 = ___host0;
		NullCheck((Dictionary_2_t1697274930 *)L_0);
		Enumerator_t3017299632  L_1 = ((  Enumerator_t3017299632  (*) (Dictionary_2_t1697274930 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t1697274930 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m3313047792_gshared (ShimEnumerator_t1802399751 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3017299632 * L_0 = (Enumerator_t3017299632 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m2770956757((Enumerator_t3017299632 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_1;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::get_Entry()
extern Il2CppClass* IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Entry_m2387156530_MetadataUsageId;
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m2387156530_gshared (ShimEnumerator_t1802399751 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Entry_m2387156530_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_t3017299632  L_0 = (Enumerator_t3017299632 )__this->get_host_enumerator_0();
		Enumerator_t3017299632  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		DictionaryEntry_t3048875398  L_3 = InterfaceFuncInvoker0< DictionaryEntry_t3048875398  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		return L_3;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m2823867931_gshared (ShimEnumerator_t1802399751 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3749587448  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t3017299632 * L_0 = (Enumerator_t3017299632 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t3749587448  L_1 = Enumerator_get_Current_m2230224741((Enumerator_t3017299632 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t3749587448 )L_1;
		int32_t L_2 = KeyValuePair_2_get_Key_m1435832840((KeyValuePair_2_t3749587448 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m3551354763_gshared (ShimEnumerator_t1802399751 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3749587448  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t3017299632 * L_0 = (Enumerator_t3017299632 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t3749587448  L_1 = Enumerator_get_Current_m2230224741((Enumerator_t3017299632 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t3749587448 )L_1;
		Il2CppObject * L_2 = KeyValuePair_2_get_Value_m3690000728((KeyValuePair_2_t3749587448 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_2;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::get_Current()
extern Il2CppClass* DictionaryEntry_t3048875398_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Current_m1093801549_MetadataUsageId;
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m1093801549_gshared (ShimEnumerator_t1802399751 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Current_m1093801549_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((ShimEnumerator_t1802399751 *)__this);
		DictionaryEntry_t3048875398  L_0 = ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t1802399751 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ShimEnumerator_t1802399751 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		DictionaryEntry_t3048875398  L_1 = L_0;
		Il2CppObject * L_2 = Box(DictionaryEntry_t3048875398_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::Reset()
extern "C"  void ShimEnumerator_Reset_m98005789_gshared (ShimEnumerator_t1802399751 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3017299632 * L_0 = (Enumerator_t3017299632 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m761796566((Enumerator_t3017299632 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int64,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m1960799224_gshared (ShimEnumerator_t3784092518 * __this, Dictionary_2_t3678967697 * ___host0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t3678967697 * L_0 = ___host0;
		NullCheck((Dictionary_2_t3678967697 *)L_0);
		Enumerator_t704025103  L_1 = ((  Enumerator_t704025103  (*) (Dictionary_2_t3678967697 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t3678967697 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int64,System.Object>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m3904393313_gshared (ShimEnumerator_t3784092518 * __this, const MethodInfo* method)
{
	{
		Enumerator_t704025103 * L_0 = (Enumerator_t704025103 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m3139459564((Enumerator_t704025103 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_1;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int64,System.Object>::get_Entry()
extern Il2CppClass* IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Entry_m1063618285_MetadataUsageId;
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m1063618285_gshared (ShimEnumerator_t3784092518 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Entry_m1063618285_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_t704025103  L_0 = (Enumerator_t704025103 )__this->get_host_enumerator_0();
		Enumerator_t704025103  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		DictionaryEntry_t3048875398  L_3 = InterfaceFuncInvoker0< DictionaryEntry_t3048875398  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		return L_3;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int64,System.Object>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m827112712_gshared (ShimEnumerator_t3784092518 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1436312919  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t704025103 * L_0 = (Enumerator_t704025103 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t1436312919  L_1 = Enumerator_get_Current_m3988231868((Enumerator_t704025103 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t1436312919 )L_1;
		int64_t L_2 = KeyValuePair_2_get_Key_m3157806383((KeyValuePair_2_t1436312919 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		int64_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int64,System.Object>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m1524802482_gshared (ShimEnumerator_t3784092518 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1436312919  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t704025103 * L_0 = (Enumerator_t704025103 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t1436312919  L_1 = Enumerator_get_Current_m3988231868((Enumerator_t704025103 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t1436312919 )L_1;
		Il2CppObject * L_2 = KeyValuePair_2_get_Value_m1978486399((KeyValuePair_2_t1436312919 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_2;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int64,System.Object>::get_Current()
extern Il2CppClass* DictionaryEntry_t3048875398_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Current_m2391401798_MetadataUsageId;
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m2391401798_gshared (ShimEnumerator_t3784092518 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Current_m2391401798_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((ShimEnumerator_t3784092518 *)__this);
		DictionaryEntry_t3048875398  L_0 = ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t3784092518 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ShimEnumerator_t3784092518 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		DictionaryEntry_t3048875398  L_1 = L_0;
		Il2CppObject * L_2 = Box(DictionaryEntry_t3048875398_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int64,System.Object>::Reset()
extern "C"  void ShimEnumerator_Reset_m2823211066_gshared (ShimEnumerator_t3784092518 * __this, const MethodInfo* method)
{
	{
		Enumerator_t704025103 * L_0 = (Enumerator_t704025103 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m598725905((Enumerator_t704025103 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.IntPtr,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m1293350960_gshared (ShimEnumerator_t3236599434 * __this, Dictionary_2_t3131474613 * ___host0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t3131474613 * L_0 = ___host0;
		NullCheck((Dictionary_2_t3131474613 *)L_0);
		Enumerator_t156532019  L_1 = ((  Enumerator_t156532019  (*) (Dictionary_2_t3131474613 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t3131474613 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.IntPtr,System.Object>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m3308190317_gshared (ShimEnumerator_t3236599434 * __this, const MethodInfo* method)
{
	{
		Enumerator_t156532019 * L_0 = (Enumerator_t156532019 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m266619810((Enumerator_t156532019 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_1;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.IntPtr,System.Object>::get_Entry()
extern Il2CppClass* IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Entry_m3789806921_MetadataUsageId;
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m3789806921_gshared (ShimEnumerator_t3236599434 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Entry_m3789806921_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_t156532019  L_0 = (Enumerator_t156532019 )__this->get_host_enumerator_0();
		Enumerator_t156532019  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		DictionaryEntry_t3048875398  L_3 = InterfaceFuncInvoker0< DictionaryEntry_t3048875398  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		return L_3;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.IntPtr,System.Object>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m1171725158_gshared (ShimEnumerator_t3236599434 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t888819835  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t156532019 * L_0 = (Enumerator_t156532019 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t888819835  L_1 = Enumerator_get_Current_m3216113338((Enumerator_t156532019 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t888819835 )L_1;
		IntPtr_t L_2 = KeyValuePair_2_get_Key_m1574332879((KeyValuePair_2_t888819835 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		IntPtr_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.IntPtr,System.Object>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m635365372_gshared (ShimEnumerator_t3236599434 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t888819835  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t156532019 * L_0 = (Enumerator_t156532019 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t888819835  L_1 = Enumerator_get_Current_m3216113338((Enumerator_t156532019 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t888819835 )L_1;
		Il2CppObject * L_2 = KeyValuePair_2_get_Value_m544293807((KeyValuePair_2_t888819835 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_2;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.IntPtr,System.Object>::get_Current()
extern Il2CppClass* DictionaryEntry_t3048875398_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Current_m3610131784_MetadataUsageId;
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m3610131784_gshared (ShimEnumerator_t3236599434 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Current_m3610131784_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((ShimEnumerator_t3236599434 *)__this);
		DictionaryEntry_t3048875398  L_0 = ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t3236599434 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ShimEnumerator_t3236599434 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		DictionaryEntry_t3048875398  L_1 = L_0;
		Il2CppObject * L_2 = Box(DictionaryEntry_t3048875398_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.IntPtr,System.Object>::Reset()
extern "C"  void ShimEnumerator_Reset_m2611430510_gshared (ShimEnumerator_t3236599434 * __this, const MethodInfo* method)
{
	{
		Enumerator_t156532019 * L_0 = (Enumerator_t156532019 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m1147555085((Enumerator_t156532019 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m2676277834_gshared (ShimEnumerator_t1329992327 * __this, Dictionary_2_t1224867506 * ___host0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t1224867506 * L_0 = ___host0;
		NullCheck((Dictionary_2_t1224867506 *)L_0);
		Enumerator_t2544892208  L_1 = ((  Enumerator_t2544892208  (*) (Dictionary_2_t1224867506 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t1224867506 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m2738243069_gshared (ShimEnumerator_t1329992327 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2544892208 * L_0 = (Enumerator_t2544892208 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m3850333932((Enumerator_t2544892208 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_1;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::get_Entry()
extern Il2CppClass* IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Entry_m2717618457_MetadataUsageId;
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m2717618457_gshared (ShimEnumerator_t1329992327 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Entry_m2717618457_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_t2544892208  L_0 = (Enumerator_t2544892208 )__this->get_host_enumerator_0();
		Enumerator_t2544892208  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		DictionaryEntry_t3048875398  L_3 = InterfaceFuncInvoker0< DictionaryEntry_t3048875398  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		return L_3;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m4266238612_gshared (ShimEnumerator_t1329992327 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3277180024  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t2544892208 * L_0 = (Enumerator_t2544892208 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t3277180024  L_1 = Enumerator_get_Current_m663494932((Enumerator_t2544892208 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t3277180024 )L_1;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m4122353311((KeyValuePair_2_t3277180024 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_2;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m2503482038_gshared (ShimEnumerator_t1329992327 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3277180024  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t2544892208 * L_0 = (Enumerator_t2544892208 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t3277180024  L_1 = Enumerator_get_Current_m663494932((Enumerator_t2544892208 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t3277180024 )L_1;
		int32_t L_2 = KeyValuePair_2_get_Value_m116637759((KeyValuePair_2_t3277180024 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::get_Current()
extern Il2CppClass* DictionaryEntry_t3048875398_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Current_m3392383502_MetadataUsageId;
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m3392383502_gshared (ShimEnumerator_t1329992327 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Current_m3392383502_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((ShimEnumerator_t1329992327 *)__this);
		DictionaryEntry_t3048875398  L_0 = ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t1329992327 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ShimEnumerator_t1329992327 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		DictionaryEntry_t3048875398  L_1 = L_0;
		Il2CppObject * L_2 = Box(DictionaryEntry_t3048875398_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::Reset()
extern "C"  void ShimEnumerator_Reset_m3134598960_gshared (ShimEnumerator_t1329992327 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2544892208 * L_0 = (Enumerator_t2544892208 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m1928144285((Enumerator_t2544892208 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m2428699265_gshared (ShimEnumerator_t3522759667 * __this, Dictionary_2_t3417634846 * ___host0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t3417634846 * L_0 = ___host0;
		NullCheck((Dictionary_2_t3417634846 *)L_0);
		Enumerator_t442692252  L_1 = ((  Enumerator_t442692252  (*) (Dictionary_2_t3417634846 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t3417634846 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m2943029388_gshared (ShimEnumerator_t3522759667 * __this, const MethodInfo* method)
{
	{
		Enumerator_t442692252 * L_0 = (Enumerator_t442692252 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m1856697671((Enumerator_t442692252 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_1;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::get_Entry()
extern Il2CppClass* IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Entry_m2332479818_MetadataUsageId;
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m2332479818_gshared (ShimEnumerator_t3522759667 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Entry_m2332479818_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_t442692252  L_0 = (Enumerator_t442692252 )__this->get_host_enumerator_0();
		Enumerator_t442692252  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		DictionaryEntry_t3048875398  L_3 = InterfaceFuncInvoker0< DictionaryEntry_t3048875398  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		return L_3;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m616785465_gshared (ShimEnumerator_t3522759667 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1174980068  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t442692252 * L_0 = (Enumerator_t442692252 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t1174980068  L_1 = Enumerator_get_Current_m1020413567((Enumerator_t442692252 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t1174980068 )L_1;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m2113318928((KeyValuePair_2_t1174980068 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_2;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m1396288849_gshared (ShimEnumerator_t3522759667 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1174980068  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t442692252 * L_0 = (Enumerator_t442692252 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t1174980068  L_1 = Enumerator_get_Current_m1020413567((Enumerator_t442692252 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t1174980068 )L_1;
		bool L_2 = KeyValuePair_2_get_Value_m1916631176((KeyValuePair_2_t1174980068 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		bool L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::get_Current()
extern Il2CppClass* DictionaryEntry_t3048875398_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Current_m2516732679_MetadataUsageId;
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m2516732679_gshared (ShimEnumerator_t3522759667 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Current_m2516732679_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((ShimEnumerator_t3522759667 *)__this);
		DictionaryEntry_t3048875398  L_0 = ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t3522759667 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ShimEnumerator_t3522759667 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		DictionaryEntry_t3048875398  L_1 = L_0;
		Il2CppObject * L_2 = Box(DictionaryEntry_t3048875398_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::Reset()
extern "C"  void ShimEnumerator_Reset_m2247049027_gshared (ShimEnumerator_t3522759667 * __this, const MethodInfo* method)
{
	{
		Enumerator_t442692252 * L_0 = (Enumerator_t442692252 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m3115320746((Enumerator_t442692252 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m1807768263_gshared (ShimEnumerator_t1769062397 * __this, Dictionary_2_t1663937576 * ___host0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t1663937576 * L_0 = ___host0;
		NullCheck((Dictionary_2_t1663937576 *)L_0);
		Enumerator_t2983962278  L_1 = ((  Enumerator_t2983962278  (*) (Dictionary_2_t1663937576 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t1663937576 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m2728191736_gshared (ShimEnumerator_t1769062397 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2983962278 * L_0 = (Enumerator_t2983962278 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m435964161((Enumerator_t2983962278 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_1;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::get_Entry()
extern Il2CppClass* IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Entry_m2171963450_MetadataUsageId;
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m2171963450_gshared (ShimEnumerator_t1769062397 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Entry_m2171963450_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_t2983962278  L_0 = (Enumerator_t2983962278 )__this->get_host_enumerator_0();
		Enumerator_t2983962278  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		DictionaryEntry_t3048875398  L_3 = InterfaceFuncInvoker0< DictionaryEntry_t3048875398  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		return L_3;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m4014537779_gshared (ShimEnumerator_t1769062397 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3716250094  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t2983962278 * L_0 = (Enumerator_t2983962278 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t3716250094  L_1 = Enumerator_get_Current_m1932198897((Enumerator_t2983962278 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t3716250094 )L_1;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m1454531804((KeyValuePair_2_t3716250094 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_2;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m1198202883_gshared (ShimEnumerator_t1769062397 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3716250094  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t2983962278 * L_0 = (Enumerator_t2983962278 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t3716250094  L_1 = Enumerator_get_Current_m1932198897((Enumerator_t2983962278 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t3716250094 )L_1;
		int32_t L_2 = KeyValuePair_2_get_Value_m3699669100((KeyValuePair_2_t3716250094 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::get_Current()
extern Il2CppClass* DictionaryEntry_t3048875398_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Current_m696250329_MetadataUsageId;
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m696250329_gshared (ShimEnumerator_t1769062397 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Current_m696250329_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((ShimEnumerator_t1769062397 *)__this);
		DictionaryEntry_t3048875398  L_0 = ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t1769062397 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ShimEnumerator_t1769062397 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		DictionaryEntry_t3048875398  L_1 = L_0;
		Il2CppObject * L_2 = Box(DictionaryEntry_t3048875398_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::Reset()
extern "C"  void ShimEnumerator_Reset_m208070833_gshared (ShimEnumerator_t1769062397 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2983962278 * L_0 = (Enumerator_t2983962278 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m1132695838((Enumerator_t2983962278 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m119758426_gshared (ShimEnumerator_t2386634244 * __this, Dictionary_2_t2281509423 * ___host0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t2281509423 * L_0 = ___host0;
		NullCheck((Dictionary_2_t2281509423 *)L_0);
		Enumerator_t3601534125  L_1 = ((  Enumerator_t3601534125  (*) (Dictionary_2_t2281509423 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t2281509423 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m2013866013_gshared (ShimEnumerator_t2386634244 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3601534125 * L_0 = (Enumerator_t3601534125 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m3349738440((Enumerator_t3601534125 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_1;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::get_Entry()
extern Il2CppClass* IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Entry_m4233876641_MetadataUsageId;
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m4233876641_gshared (ShimEnumerator_t2386634244 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Entry_m4233876641_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_t3601534125  L_0 = (Enumerator_t3601534125 )__this->get_host_enumerator_0();
		Enumerator_t3601534125  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		DictionaryEntry_t3048875398  L_3 = InterfaceFuncInvoker0< DictionaryEntry_t3048875398  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		return L_3;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m3962796804_gshared (ShimEnumerator_t2386634244 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t38854645  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t3601534125 * L_0 = (Enumerator_t3601534125 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t38854645  L_1 = Enumerator_get_Current_m25299632((Enumerator_t3601534125 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t38854645 )L_1;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m2561166459((KeyValuePair_2_t38854645 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_2;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m2522747790_gshared (ShimEnumerator_t2386634244 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t38854645  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t3601534125 * L_0 = (Enumerator_t3601534125 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t38854645  L_1 = Enumerator_get_Current_m25299632((Enumerator_t3601534125 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t38854645 )L_1;
		Il2CppObject * L_2 = KeyValuePair_2_get_Value_m499643803((KeyValuePair_2_t38854645 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_2;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::get_Current()
extern Il2CppClass* DictionaryEntry_t3048875398_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Current_m2121723938_MetadataUsageId;
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m2121723938_gshared (ShimEnumerator_t2386634244 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Current_m2121723938_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((ShimEnumerator_t2386634244 *)__this);
		DictionaryEntry_t3048875398  L_0 = ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t2386634244 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ShimEnumerator_t2386634244 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		DictionaryEntry_t3048875398  L_1 = L_0;
		Il2CppObject * L_2 = Box(DictionaryEntry_t3048875398_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::Reset()
extern "C"  void ShimEnumerator_Reset_m1100368508_gshared (ShimEnumerator_t2386634244 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3601534125 * L_0 = (Enumerator_t3601534125 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m3129803197((Enumerator_t3601534125 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m774979914_gshared (ShimEnumerator_t832262963 * __this, Dictionary_2_t727138142 * ___host0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t727138142 * L_0 = ___host0;
		NullCheck((Dictionary_2_t727138142 *)L_0);
		Enumerator_t2047162844  L_1 = ((  Enumerator_t2047162844  (*) (Dictionary_2_t727138142 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t727138142 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m1198409173_gshared (ShimEnumerator_t832262963 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2047162844 * L_0 = (Enumerator_t2047162844 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m2137572552((Enumerator_t2047162844 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_1;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::get_Entry()
extern Il2CppClass* IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Entry_m3444299241_MetadataUsageId;
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m3444299241_gshared (ShimEnumerator_t832262963 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Entry_m3444299241_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_t2047162844  L_0 = (Enumerator_t2047162844 )__this->get_host_enumerator_0();
		Enumerator_t2047162844  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		DictionaryEntry_t3048875398  L_3 = InterfaceFuncInvoker0< DictionaryEntry_t3048875398  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		return L_3;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m2804359804_gshared (ShimEnumerator_t832262963 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2779450660  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t2047162844 * L_0 = (Enumerator_t2047162844 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t2779450660  L_1 = Enumerator_get_Current_m923671264((Enumerator_t2047162844 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t2779450660 )L_1;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m1489805227((KeyValuePair_2_t2779450660 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_2;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m505292094_gshared (ShimEnumerator_t832262963 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2779450660  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t2047162844 * L_0 = (Enumerator_t2047162844 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t2779450660  L_1 = Enumerator_get_Current_m923671264((Enumerator_t2047162844 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t2779450660 )L_1;
		ArrayMetadata_t1135078014  L_2 = KeyValuePair_2_get_Value_m3869691915((KeyValuePair_2_t2779450660 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		ArrayMetadata_t1135078014  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::get_Current()
extern Il2CppClass* DictionaryEntry_t3048875398_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Current_m489028466_MetadataUsageId;
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m489028466_gshared (ShimEnumerator_t832262963 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Current_m489028466_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((ShimEnumerator_t832262963 *)__this);
		DictionaryEntry_t3048875398  L_0 = ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t832262963 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ShimEnumerator_t832262963 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		DictionaryEntry_t3048875398  L_1 = L_0;
		Il2CppObject * L_2 = Box(DictionaryEntry_t3048875398_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::Reset()
extern "C"  void ShimEnumerator_Reset_m1108407716_gshared (ShimEnumerator_t832262963 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2047162844 * L_0 = (Enumerator_t2047162844 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m2586713469((Enumerator_t2047162844 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m3230415864_gshared (ShimEnumerator_t3755322689 * __this, Dictionary_2_t3650197868 * ___host0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t3650197868 * L_0 = ___host0;
		NullCheck((Dictionary_2_t3650197868 *)L_0);
		Enumerator_t675255274  L_1 = ((  Enumerator_t675255274  (*) (Dictionary_2_t3650197868 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t3650197868 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m2274929081_gshared (ShimEnumerator_t3755322689 * __this, const MethodInfo* method)
{
	{
		Enumerator_t675255274 * L_0 = (Enumerator_t675255274 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m513463628((Enumerator_t675255274 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_1;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::get_Entry()
extern Il2CppClass* IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Entry_m4288459749_MetadataUsageId;
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m4288459749_gshared (ShimEnumerator_t3755322689 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Entry_m4288459749_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_t675255274  L_0 = (Enumerator_t675255274 )__this->get_host_enumerator_0();
		Enumerator_t675255274  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		DictionaryEntry_t3048875398  L_3 = InterfaceFuncInvoker0< DictionaryEntry_t3048875398  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		return L_3;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m1774081888_gshared (ShimEnumerator_t3755322689 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1407543090  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t675255274 * L_0 = (Enumerator_t675255274 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t1407543090  L_1 = Enumerator_get_Current_m2333579100((Enumerator_t675255274 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t1407543090 )L_1;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m2438555343((KeyValuePair_2_t1407543090 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_2;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m2379223890_gshared (ShimEnumerator_t3755322689 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1407543090  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t675255274 * L_0 = (Enumerator_t675255274 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t1407543090  L_1 = Enumerator_get_Current_m2333579100((Enumerator_t675255274 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t1407543090 )L_1;
		ObjectMetadata_t4058137740  L_2 = KeyValuePair_2_get_Value_m3741940767((KeyValuePair_2_t1407543090 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		ObjectMetadata_t4058137740  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::get_Current()
extern Il2CppClass* DictionaryEntry_t3048875398_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Current_m332693414_MetadataUsageId;
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m332693414_gshared (ShimEnumerator_t3755322689 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Current_m332693414_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((ShimEnumerator_t3755322689 *)__this);
		DictionaryEntry_t3048875398  L_0 = ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t3755322689 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ShimEnumerator_t3755322689 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		DictionaryEntry_t3048875398  L_1 = L_0;
		Il2CppObject * L_2 = Box(DictionaryEntry_t3048875398_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::Reset()
extern "C"  void ShimEnumerator_Reset_m3971790434_gshared (ShimEnumerator_t3755322689 * __this, const MethodInfo* method)
{
	{
		Enumerator_t675255274 * L_0 = (Enumerator_t675255274 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m975923089((Enumerator_t675255274 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m16282010_gshared (ShimEnumerator_t2835982647 * __this, Dictionary_2_t2730857826 * ___host0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t2730857826 * L_0 = ___host0;
		NullCheck((Dictionary_2_t2730857826 *)L_0);
		Enumerator_t4050882528  L_1 = ((  Enumerator_t4050882528  (*) (Dictionary_2_t2730857826 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t2730857826 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m1608656109_gshared (ShimEnumerator_t2835982647 * __this, const MethodInfo* method)
{
	{
		Enumerator_t4050882528 * L_0 = (Enumerator_t4050882528 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m2619008060((Enumerator_t4050882528 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_1;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Entry()
extern Il2CppClass* IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Entry_m2332854825_MetadataUsageId;
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m2332854825_gshared (ShimEnumerator_t2835982647 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Entry_m2332854825_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_t4050882528  L_0 = (Enumerator_t4050882528 )__this->get_host_enumerator_0();
		Enumerator_t4050882528  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		DictionaryEntry_t3048875398  L_3 = InterfaceFuncInvoker0< DictionaryEntry_t3048875398  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		return L_3;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m513105924_gshared (ShimEnumerator_t2835982647 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t488203048  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t4050882528 * L_0 = (Enumerator_t4050882528 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t488203048  L_1 = Enumerator_get_Current_m492584036((Enumerator_t4050882528 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t488203048 )L_1;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m573362703((KeyValuePair_2_t488203048 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_2;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m3758954534_gshared (ShimEnumerator_t2835982647 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t488203048  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t4050882528 * L_0 = (Enumerator_t4050882528 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t488203048  L_1 = Enumerator_get_Current_m492584036((Enumerator_t4050882528 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t488203048 )L_1;
		int32_t L_2 = KeyValuePair_2_get_Value_m1644876463((KeyValuePair_2_t488203048 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Current()
extern Il2CppClass* DictionaryEntry_t3048875398_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Current_m3364144670_MetadataUsageId;
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m3364144670_gshared (ShimEnumerator_t2835982647 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Current_m3364144670_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((ShimEnumerator_t2835982647 *)__this);
		DictionaryEntry_t3048875398  L_0 = ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t2835982647 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ShimEnumerator_t2835982647 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		DictionaryEntry_t3048875398  L_1 = L_0;
		Il2CppObject * L_2 = Box(DictionaryEntry_t3048875398_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::Reset()
extern "C"  void ShimEnumerator_Reset_m1247878208_gshared (ShimEnumerator_t2835982647 * __this, const MethodInfo* method)
{
	{
		Enumerator_t4050882528 * L_0 = (Enumerator_t4050882528 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m2489846157((Enumerator_t4050882528 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<Amazon.Runtime.Metric,System.Int64,Amazon.Runtime.Metric>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1855900835_gshared (Transform_1_t3638058609 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Amazon.Runtime.Metric,System.Int64,Amazon.Runtime.Metric>::Invoke(TKey,TValue)
extern "C"  int32_t Transform_1_Invoke_m2564737479_gshared (Transform_1_t3638058609 * __this, int32_t ___key0, int64_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2564737479((Transform_1_t3638058609 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, int64_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, int32_t ___key0, int64_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<Amazon.Runtime.Metric,System.Int64,Amazon.Runtime.Metric>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Metric_t3273440202_il2cpp_TypeInfo_var;
extern Il2CppClass* Int64_t909078037_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1470240396_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1470240396_gshared (Transform_1_t3638058609 * __this, int32_t ___key0, int64_t ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1470240396_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Metric_t3273440202_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Int64_t909078037_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Amazon.Runtime.Metric,System.Int64,Amazon.Runtime.Metric>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Transform_1_EndInvoke_m3200079909_gshared (Transform_1_t3638058609 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<Amazon.Runtime.Metric,System.Int64,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1913881749_gshared (Transform_1_t3413493805 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Amazon.Runtime.Metric,System.Int64,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Transform_1_Invoke_m1872386593_gshared (Transform_1_t3413493805 * __this, int32_t ___key0, int64_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1872386593((Transform_1_t3413493805 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, int64_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (void* __this, int32_t ___key0, int64_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<Amazon.Runtime.Metric,System.Int64,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Metric_t3273440202_il2cpp_TypeInfo_var;
extern Il2CppClass* Int64_t909078037_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2773571292_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2773571292_gshared (Transform_1_t3413493805 * __this, int32_t ___key0, int64_t ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2773571292_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Metric_t3273440202_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Int64_t909078037_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Amazon.Runtime.Metric,System.Int64,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t3048875398  Transform_1_EndInvoke_m320309047_gshared (Transform_1_t3413493805 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t3048875398 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<Amazon.Runtime.Metric,System.Int64,System.Collections.Generic.KeyValuePair`2<Amazon.Runtime.Metric,System.Int64>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3855468548_gshared (Transform_1_t1781120155 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Amazon.Runtime.Metric,System.Int64,System.Collections.Generic.KeyValuePair`2<Amazon.Runtime.Metric,System.Int64>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t1416501748  Transform_1_Invoke_m2509382148_gshared (Transform_1_t1781120155 * __this, int32_t ___key0, int64_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2509382148((Transform_1_t1781120155 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t1416501748  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, int64_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t1416501748  (*FunctionPointerType) (void* __this, int32_t ___key0, int64_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<Amazon.Runtime.Metric,System.Int64,System.Collections.Generic.KeyValuePair`2<Amazon.Runtime.Metric,System.Int64>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Metric_t3273440202_il2cpp_TypeInfo_var;
extern Il2CppClass* Int64_t909078037_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m3900991731_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3900991731_gshared (Transform_1_t1781120155 * __this, int32_t ___key0, int64_t ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3900991731_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Metric_t3273440202_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Int64_t909078037_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Amazon.Runtime.Metric,System.Int64,System.Collections.Generic.KeyValuePair`2<Amazon.Runtime.Metric,System.Int64>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t1416501748  Transform_1_EndInvoke_m2968234926_gshared (Transform_1_t1781120155 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t1416501748 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<Amazon.Runtime.Metric,System.Int64,System.Int64>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3335433483_gshared (Transform_1_t1273696444 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Amazon.Runtime.Metric,System.Int64,System.Int64>::Invoke(TKey,TValue)
extern "C"  int64_t Transform_1_Invoke_m3716357007_gshared (Transform_1_t1273696444 * __this, int32_t ___key0, int64_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3716357007((Transform_1_t1273696444 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int64_t (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, int64_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int64_t (*FunctionPointerType) (void* __this, int32_t ___key0, int64_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<Amazon.Runtime.Metric,System.Int64,System.Int64>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Metric_t3273440202_il2cpp_TypeInfo_var;
extern Il2CppClass* Int64_t909078037_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m3145674124_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3145674124_gshared (Transform_1_t1273696444 * __this, int32_t ___key0, int64_t ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3145674124_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Metric_t3273440202_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Int64_t909078037_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Amazon.Runtime.Metric,System.Int64,System.Int64>::EndInvoke(System.IAsyncResult)
extern "C"  int64_t Transform_1_EndInvoke_m2929915597_gshared (Transform_1_t1273696444 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int64_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<Amazon.Runtime.Metric,System.Object,Amazon.Runtime.Metric>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1146480631_gshared (Transform_1_t2240600335 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Amazon.Runtime.Metric,System.Object,Amazon.Runtime.Metric>::Invoke(TKey,TValue)
extern "C"  int32_t Transform_1_Invoke_m629402659_gshared (Transform_1_t2240600335 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m629402659((Transform_1_t2240600335 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<Amazon.Runtime.Metric,System.Object,Amazon.Runtime.Metric>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Metric_t3273440202_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m719009986_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m719009986_gshared (Transform_1_t2240600335 * __this, int32_t ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m719009986_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Metric_t3273440202_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Amazon.Runtime.Metric,System.Object,Amazon.Runtime.Metric>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Transform_1_EndInvoke_m2238630369_gshared (Transform_1_t2240600335 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<Amazon.Runtime.Metric,System.Object,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1390018089_gshared (Transform_1_t2016035531 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Amazon.Runtime.Metric,System.Object,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Transform_1_Invoke_m3629793757_gshared (Transform_1_t2016035531 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3629793757((Transform_1_t2016035531 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<Amazon.Runtime.Metric,System.Object,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Metric_t3273440202_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2429879322_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2429879322_gshared (Transform_1_t2016035531 * __this, int32_t ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2429879322_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Metric_t3273440202_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Amazon.Runtime.Metric,System.Object,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t3048875398  Transform_1_EndInvoke_m3230763475_gshared (Transform_1_t2016035531 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t3048875398 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<Amazon.Runtime.Metric,System.Object,System.Collections.Generic.KeyValuePair`2<Amazon.Runtime.Metric,System.Object>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m447988758_gshared (Transform_1_t2164033139 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Amazon.Runtime.Metric,System.Object,System.Collections.Generic.KeyValuePair`2<Amazon.Runtime.Metric,System.Object>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t3196873006  Transform_1_Invoke_m2337722006_gshared (Transform_1_t2164033139 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2337722006((Transform_1_t2164033139 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t3196873006  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t3196873006  (*FunctionPointerType) (void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<Amazon.Runtime.Metric,System.Object,System.Collections.Generic.KeyValuePair`2<Amazon.Runtime.Metric,System.Object>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Metric_t3273440202_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1926484351_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1926484351_gshared (Transform_1_t2164033139 * __this, int32_t ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1926484351_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Metric_t3273440202_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Amazon.Runtime.Metric,System.Object,System.Collections.Generic.KeyValuePair`2<Amazon.Runtime.Metric,System.Object>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t3196873006  Transform_1_EndInvoke_m3144339628_gshared (Transform_1_t2164033139 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t3196873006 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<Amazon.Runtime.Metric,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m712824371_gshared (Transform_1_t1656609428 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Amazon.Runtime.Metric,System.Object,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m511417255_gshared (Transform_1_t1656609428 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m511417255((Transform_1_t1656609428 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<Amazon.Runtime.Metric,System.Object,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Metric_t3273440202_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m3945044328_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3945044328_gshared (Transform_1_t1656609428 * __this, int32_t ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3945044328_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Metric_t3273440202_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Amazon.Runtime.Metric,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Transform_1_EndInvoke_m2302275177_gshared (Transform_1_t1656609428 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1301543437_gshared (Transform_1_t3714796956 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Transform_1_Invoke_m3240675425_gshared (Transform_1_t3714796956 * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3240675425((Transform_1_t3714796956 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (void* __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2628363682_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2628363682_gshared (Transform_1_t3714796956 * __this, int32_t ___key0, int32_t ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2628363682_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t3048875398  Transform_1_EndInvoke_m1726077867_gshared (Transform_1_t3714796956 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t3048875398 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3002075318_gshared (Transform_1_t3797937159 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t3132015601  Transform_1_Invoke_m388048438_gshared (Transform_1_t3797937159 * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m388048438((Transform_1_t3797937159 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t3132015601  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t3132015601  (*FunctionPointerType) (void* __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m3071137759_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3071137759_gshared (Transform_1_t3797937159 * __this, int32_t ___key0, int32_t ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3071137759_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t3132015601  Transform_1_EndInvoke_m3831512460_gshared (Transform_1_t3797937159 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t3132015601 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3512061196_gshared (Transform_1_t2737799006 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Int32>::Invoke(TKey,TValue)
extern "C"  int32_t Transform_1_Invoke_m488493188_gshared (Transform_1_t2737799006 * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m488493188((Transform_1_t2737799006 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Int32>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m929665079_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m929665079_gshared (Transform_1_t2737799006 * __this, int32_t ___key0, int32_t ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m929665079_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Transform_1_EndInvoke_m805300450_gshared (Transform_1_t2737799006 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2152205186_gshared (Transform_1_t4064153 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Transform_1_Invoke_m4020530914_gshared (Transform_1_t4064153 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m4020530914((Transform_1_t4064153 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2179239469_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2179239469_gshared (Transform_1_t4064153 * __this, int32_t ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2179239469_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t3048875398  Transform_1_EndInvoke_m620026520_gshared (Transform_1_t4064153 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t3048875398 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m713310742_gshared (Transform_1_t704776203 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t3749587448  Transform_1_Invoke_m1436021910_gshared (Transform_1_t704776203 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1436021910((Transform_1_t704776203 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t3749587448  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t3749587448  (*FunctionPointerType) (void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1786442111_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1786442111_gshared (Transform_1_t704776203 * __this, int32_t ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1786442111_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t3749587448  Transform_1_EndInvoke_m590952364_gshared (Transform_1_t704776203 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t3749587448 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3844763875_gshared (Transform_1_t3322033499 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Int32>::Invoke(TKey,TValue)
extern "C"  int32_t Transform_1_Invoke_m3763570295_gshared (Transform_1_t3322033499 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3763570295((Transform_1_t3322033499 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Int32>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m802712344_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m802712344_gshared (Transform_1_t3322033499 * __this, int32_t ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m802712344_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Transform_1_EndInvoke_m336301945_gshared (Transform_1_t3322033499 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2914458810_gshared (Transform_1_t3939605346 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m2347662626_gshared (Transform_1_t3939605346 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2347662626((Transform_1_t3939605346 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1919808363_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1919808363_gshared (Transform_1_t3939605346 * __this, int32_t ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1919808363_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Transform_1_EndInvoke_m1010744720_gshared (Transform_1_t3939605346 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Object,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3373533409_gshared (Transform_1_t2356341726 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Object,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Transform_1_Invoke_m1299483797_gshared (Transform_1_t2356341726 * __this, int64_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1299483797((Transform_1_t2356341726 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (Il2CppObject *, void* __this, int64_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (void* __this, int64_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Object,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int64_t909078037_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m3448776694_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3448776694_gshared (Transform_1_t2356341726 * __this, int64_t ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3448776694_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int64_t909078037_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Object,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t3048875398  Transform_1_EndInvoke_m3006940059_gshared (Transform_1_t2356341726 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t3048875398 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Object,System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m232902212_gshared (Transform_1_t743779247 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Object,System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t1436312919  Transform_1_Invoke_m1183831012_gshared (Transform_1_t743779247 * __this, int64_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1183831012((Transform_1_t743779247 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t1436312919  (*FunctionPointerType) (Il2CppObject *, void* __this, int64_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t1436312919  (*FunctionPointerType) (void* __this, int64_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Object,System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int64_t909078037_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m655666259_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m655666259_gshared (Transform_1_t743779247 * __this, int64_t ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m655666259_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int64_t909078037_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Object,System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t1436312919  Transform_1_EndInvoke_m229614702_gshared (Transform_1_t743779247 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t1436312919 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Object,System.Int64>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3927316327_gshared (Transform_1_t216544365 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Object,System.Int64>::Invoke(TKey,TValue)
extern "C"  int64_t Transform_1_Invoke_m3042893651_gshared (Transform_1_t216544365 * __this, int64_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3042893651((Transform_1_t216544365 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int64_t (*FunctionPointerType) (Il2CppObject *, void* __this, int64_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int64_t (*FunctionPointerType) (void* __this, int64_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Object,System.Int64>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int64_t909078037_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m3566819466_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3566819466_gshared (Transform_1_t216544365 * __this, int64_t ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3566819466_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int64_t909078037_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Object,System.Int64>::EndInvoke(System.IAsyncResult)
extern "C"  int64_t Transform_1_EndInvoke_m2544704049_gshared (Transform_1_t216544365 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int64_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m4269627275_gshared (Transform_1_t1996915623 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Object,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m333092703_gshared (Transform_1_t1996915623 * __this, int64_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m333092703((Transform_1_t1996915623 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, int64_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, int64_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Object,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int64_t909078037_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m4016145208_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m4016145208_gshared (Transform_1_t1996915623 * __this, int64_t ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m4016145208_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int64_t909078037_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Transform_1_EndInvoke_m3793097937_gshared (Transform_1_t1996915623 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Object,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1237139657_gshared (Transform_1_t2713705386 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Object,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Transform_1_Invoke_m2675095773_gshared (Transform_1_t2713705386 * __this, IntPtr_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2675095773((Transform_1_t2713705386 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (Il2CppObject *, void* __this, IntPtr_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (void* __this, IntPtr_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Object,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m4270324442_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m4270324442_gshared (Transform_1_t2713705386 * __this, IntPtr_t ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m4270324442_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Object,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t3048875398  Transform_1_EndInvoke_m4121732147_gshared (Transform_1_t2713705386 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t3048875398 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Object,System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m4075225366_gshared (Transform_1_t553649823 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Object,System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t888819835  Transform_1_Invoke_m1023901590_gshared (Transform_1_t553649823 * __this, IntPtr_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1023901590((Transform_1_t553649823 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t888819835  (*FunctionPointerType) (Il2CppObject *, void* __this, IntPtr_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t888819835  (*FunctionPointerType) (void* __this, IntPtr_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Object,System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m345445247_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m345445247_gshared (Transform_1_t553649823 * __this, IntPtr_t ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m345445247_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Object,System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t888819835  Transform_1_EndInvoke_m3664875948_gshared (Transform_1_t553649823 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t888819835 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Object,System.IntPtr>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2284483831_gshared (Transform_1_t2168890597 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Object,System.IntPtr>::Invoke(TKey,TValue)
extern "C"  IntPtr_t Transform_1_Invoke_m524403747_gshared (Transform_1_t2168890597 * __this, IntPtr_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m524403747((Transform_1_t2168890597 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef IntPtr_t (*FunctionPointerType) (Il2CppObject *, void* __this, IntPtr_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef IntPtr_t (*FunctionPointerType) (void* __this, IntPtr_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Object,System.IntPtr>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2914101698_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2914101698_gshared (Transform_1_t2168890597 * __this, IntPtr_t ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2914101698_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Object,System.IntPtr>::EndInvoke(System.IAsyncResult)
extern "C"  IntPtr_t Transform_1_EndInvoke_m1944338913_gshared (Transform_1_t2168890597 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(IntPtr_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3731381235_gshared (Transform_1_t2354279283 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Object,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m1111426439_gshared (Transform_1_t2354279283 * __this, IntPtr_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1111426439((Transform_1_t2354279283 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, IntPtr_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, IntPtr_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Object,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m3901963208_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3901963208_gshared (Transform_1_t2354279283 * __this, IntPtr_t ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3901963208_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Transform_1_EndInvoke_m275018665_gshared (Transform_1_t2354279283 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1434434935_gshared (Transform_1_t3139668837 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::Invoke(TKey,TValue)
extern "C"  int32_t Transform_1_Invoke_m1021538339_gshared (Transform_1_t3139668837 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1021538339((Transform_1_t3139668837 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* InstantiationModel_t1632807378_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2540431042_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2540431042_gshared (Transform_1_t3139668837 * __this, Il2CppObject * ___key0, int32_t ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2540431042_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(InstantiationModel_t1632807378_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Transform_1_EndInvoke_m434975841_gshared (Transform_1_t3139668837 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m442128521_gshared (Transform_1_t260769561 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Transform_1_Invoke_m3541896077_gshared (Transform_1_t260769561 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3541896077((Transform_1_t260769561 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (void* __this, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* InstantiationModel_t1632807378_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m3476592820_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3476592820_gshared (Transform_1_t260769561 * __this, Il2CppObject * ___key0, int32_t ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3476592820_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(InstantiationModel_t1632807378_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t3048875398  Transform_1_EndInvoke_m3296427051_gshared (Transform_1_t260769561 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t3048875398 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel,System.Collections.Generic.KeyValuePair`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3575590710_gshared (Transform_1_t489074187 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel,System.Collections.Generic.KeyValuePair`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t3277180024  Transform_1_Invoke_m2509508406_gshared (Transform_1_t489074187 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2509508406((Transform_1_t489074187 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t3277180024  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef KeyValuePair_2_t3277180024  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t3277180024  (*FunctionPointerType) (void* __this, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel,System.Collections.Generic.KeyValuePair`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* InstantiationModel_t1632807378_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1158911071_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1158911071_gshared (Transform_1_t489074187 * __this, Il2CppObject * ___key0, int32_t ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1158911071_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(InstantiationModel_t1632807378_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel,System.Collections.Generic.KeyValuePair`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t3277180024  Transform_1_EndInvoke_m2434074380_gshared (Transform_1_t489074187 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t3277180024 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3172091291_gshared (Transform_1_t4196310754 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m2634208159_gshared (Transform_1_t4196310754 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2634208159((Transform_1_t4196310754 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* InstantiationModel_t1632807378_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m3338353434_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3338353434_gshared (Transform_1_t4196310754 * __this, Il2CppObject * ___key0, int32_t ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3338353434_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(InstantiationModel_t1632807378_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Transform_1_EndInvoke_m784823241_gshared (Transform_1_t4196310754 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3569730739_gshared (Transform_1_t2990554165 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Boolean>::Invoke(TKey,TValue)
extern "C"  bool Transform_1_Invoke_m2906736839_gshared (Transform_1_t2990554165 * __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2906736839((Transform_1_t2990554165 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, bool ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Boolean>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m3826027984_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3826027984_gshared (Transform_1_t2990554165 * __this, Il2CppObject * ___key0, bool ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3826027984_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C"  bool Transform_1_EndInvoke_m258407721_gshared (Transform_1_t2990554165 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1978472014_gshared (Transform_1_t2213854845 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Transform_1_Invoke_m2509306846_gshared (Transform_1_t2213854845 * __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2509306846((Transform_1_t2213854845 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (void* __this, bool ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1167293475_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1167293475_gshared (Transform_1_t2213854845 * __this, Il2CppObject * ___key0, bool ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1167293475_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t3048875398  Transform_1_EndInvoke_m2742732284_gshared (Transform_1_t2213854845 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t3048875398 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m974062490_gshared (Transform_1_t339959515 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t1174980068  Transform_1_Invoke_m4136847354_gshared (Transform_1_t339959515 * __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m4136847354((Transform_1_t339959515 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t1174980068  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef KeyValuePair_2_t1174980068  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t1174980068  (*FunctionPointerType) (void* __this, bool ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2640141359_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2640141359_gshared (Transform_1_t339959515 * __this, Il2CppObject * ___key0, bool ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2640141359_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t1174980068  Transform_1_EndInvoke_m3779953636_gshared (Transform_1_t339959515 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t1174980068 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m4273332630_gshared (Transform_1_t1854428742 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m3830249854_gshared (Transform_1_t1854428742 * __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3830249854((Transform_1_t1854428742 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, bool ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2082152109_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2082152109_gshared (Transform_1_t1854428742 * __this, Il2CppObject * ___key0, bool ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2082152109_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Transform_1_EndInvoke_m1709357192_gshared (Transform_1_t1854428742 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m353209818_gshared (Transform_1_t1676220171 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Transform_1_Invoke_m719893226_gshared (Transform_1_t1676220171 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m719893226((Transform_1_t1676220171 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (void* __this, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m786657825_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m786657825_gshared (Transform_1_t1676220171 * __this, Il2CppObject * ___key0, int32_t ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m786657825_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t3048875398  Transform_1_EndInvoke_m664119620_gshared (Transform_1_t1676220171 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t3048875398 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m583305686_gshared (Transform_1_t2343594867 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t3716250094  Transform_1_Invoke_m1172879766_gshared (Transform_1_t2343594867 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1172879766((Transform_1_t2343594867 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t3716250094  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef KeyValuePair_2_t3716250094  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t3716250094  (*FunctionPointerType) (void* __this, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2336029567_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2336029567_gshared (Transform_1_t2343594867 * __this, Il2CppObject * ___key0, int32_t ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2336029567_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t3716250094  Transform_1_EndInvoke_m1025924012_gshared (Transform_1_t2343594867 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t3716250094 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1642784939_gshared (Transform_1_t699222221 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Int32>::Invoke(TKey,TValue)
extern "C"  int32_t Transform_1_Invoke_m2099058127_gshared (Transform_1_t699222221 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2099058127((Transform_1_t699222221 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Int32>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m3169382212_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3169382212_gshared (Transform_1_t699222221 * __this, Il2CppObject * ___key0, int32_t ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3169382212_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Transform_1_EndInvoke_m7550125_gshared (Transform_1_t699222221 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2375232750_gshared (Transform_1_t1316794068 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m2777900342_gshared (Transform_1_t1316794068 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2777900342((Transform_1_t1316794068 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1856627011_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1856627011_gshared (Transform_1_t1316794068 * __this, Il2CppObject * ___key0, int32_t ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1856627011_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Transform_1_EndInvoke_m2379644184_gshared (Transform_1_t1316794068 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m4161450529_gshared (Transform_1_t2260454664 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Transform_1_Invoke_m2770612589_gshared (Transform_1_t2260454664 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2770612589((Transform_1_t2260454664 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (void* __this, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3014766640_gshared (Transform_1_t2260454664 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t3048875398  Transform_1_EndInvoke_m803975703_gshared (Transform_1_t2260454664 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t3048875398 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2658320534_gshared (Transform_1_t3545401207 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t38854645  Transform_1_Invoke_m1976033878_gshared (Transform_1_t3545401207 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1976033878((Transform_1_t3545401207 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t38854645  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef KeyValuePair_2_t38854645  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t38854645  (*FunctionPointerType) (void* __this, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3105433791_gshared (Transform_1_t3545401207 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t38854645  Transform_1_EndInvoke_m687617772_gshared (Transform_1_t3545401207 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t38854645 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3849972087_gshared (Transform_1_t1901028561 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m1224512163_gshared (Transform_1_t1901028561 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1224512163((Transform_1_t1901028561 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2122310722_gshared (Transform_1_t1901028561 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Transform_1_EndInvoke_m1237128929_gshared (Transform_1_t1901028561 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ThirdParty.Json.LitJson.ArrayMetadata,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3198348921_gshared (Transform_1_t2573087293 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ThirdParty.Json.LitJson.ArrayMetadata,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Transform_1_Invoke_m2118969653_gshared (Transform_1_t2573087293 * __this, Il2CppObject * ___key0, ArrayMetadata_t1135078014  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2118969653((Transform_1_t2573087293 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, ArrayMetadata_t1135078014  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, ArrayMetadata_t1135078014  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (void* __this, ArrayMetadata_t1135078014  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ThirdParty.Json.LitJson.ArrayMetadata,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* ArrayMetadata_t1135078014_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m4063938384_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m4063938384_gshared (Transform_1_t2573087293 * __this, Il2CppObject * ___key0, ArrayMetadata_t1135078014  ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m4063938384_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(ArrayMetadata_t1135078014_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ThirdParty.Json.LitJson.ArrayMetadata,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t3048875398  Transform_1_EndInvoke_m1085171679_gshared (Transform_1_t2573087293 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t3048875398 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ThirdParty.Json.LitJson.ArrayMetadata,System.Collections.Generic.KeyValuePair`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2267194134_gshared (Transform_1_t2303662555 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ThirdParty.Json.LitJson.ArrayMetadata,System.Collections.Generic.KeyValuePair`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t2779450660  Transform_1_Invoke_m198086102_gshared (Transform_1_t2303662555 * __this, Il2CppObject * ___key0, ArrayMetadata_t1135078014  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m198086102((Transform_1_t2303662555 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t2779450660  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, ArrayMetadata_t1135078014  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef KeyValuePair_2_t2779450660  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, ArrayMetadata_t1135078014  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t2779450660  (*FunctionPointerType) (void* __this, ArrayMetadata_t1135078014  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ThirdParty.Json.LitJson.ArrayMetadata,System.Collections.Generic.KeyValuePair`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* ArrayMetadata_t1135078014_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1368567871_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1368567871_gshared (Transform_1_t2303662555 * __this, Il2CppObject * ___key0, ArrayMetadata_t1135078014  ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1368567871_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(ArrayMetadata_t1135078014_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ThirdParty.Json.LitJson.ArrayMetadata,System.Collections.Generic.KeyValuePair`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t2779450660  Transform_1_EndInvoke_m230384236_gshared (Transform_1_t2303662555 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t2779450660 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ThirdParty.Json.LitJson.ArrayMetadata,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1867485887_gshared (Transform_1_t2213661190 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ThirdParty.Json.LitJson.ArrayMetadata,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m3423412715_gshared (Transform_1_t2213661190 * __this, Il2CppObject * ___key0, ArrayMetadata_t1135078014  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3423412715((Transform_1_t2213661190 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, ArrayMetadata_t1135078014  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, ArrayMetadata_t1135078014  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, ArrayMetadata_t1135078014  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ThirdParty.Json.LitJson.ArrayMetadata,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* ArrayMetadata_t1135078014_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2369144738_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2369144738_gshared (Transform_1_t2213661190 * __this, Il2CppObject * ___key0, ArrayMetadata_t1135078014  ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2369144738_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(ArrayMetadata_t1135078014_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ThirdParty.Json.LitJson.ArrayMetadata,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Transform_1_EndInvoke_m483337609_gshared (Transform_1_t2213661190 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ThirdParty.Json.LitJson.ArrayMetadata,ThirdParty.Json.LitJson.ArrayMetadata>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2057341239_gshared (Transform_1_t659289909 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ThirdParty.Json.LitJson.ArrayMetadata,ThirdParty.Json.LitJson.ArrayMetadata>::Invoke(TKey,TValue)
extern "C"  ArrayMetadata_t1135078014  Transform_1_Invoke_m4165158243_gshared (Transform_1_t659289909 * __this, Il2CppObject * ___key0, ArrayMetadata_t1135078014  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m4165158243((Transform_1_t659289909 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef ArrayMetadata_t1135078014  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, ArrayMetadata_t1135078014  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef ArrayMetadata_t1135078014  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, ArrayMetadata_t1135078014  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef ArrayMetadata_t1135078014  (*FunctionPointerType) (void* __this, ArrayMetadata_t1135078014  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ThirdParty.Json.LitJson.ArrayMetadata,ThirdParty.Json.LitJson.ArrayMetadata>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* ArrayMetadata_t1135078014_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m899691010_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m899691010_gshared (Transform_1_t659289909 * __this, Il2CppObject * ___key0, ArrayMetadata_t1135078014  ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m899691010_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(ArrayMetadata_t1135078014_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ThirdParty.Json.LitJson.ArrayMetadata,ThirdParty.Json.LitJson.ArrayMetadata>::EndInvoke(System.IAsyncResult)
extern "C"  ArrayMetadata_t1135078014  Transform_1_EndInvoke_m3478379553_gshared (Transform_1_t659289909 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(ArrayMetadata_t1135078014 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ThirdParty.Json.LitJson.ObjectMetadata,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3340152729_gshared (Transform_1_t200415287 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ThirdParty.Json.LitJson.ObjectMetadata,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Transform_1_Invoke_m2908356477_gshared (Transform_1_t200415287 * __this, Il2CppObject * ___key0, ObjectMetadata_t4058137740  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2908356477((Transform_1_t200415287 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, ObjectMetadata_t4058137740  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, ObjectMetadata_t4058137740  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (void* __this, ObjectMetadata_t4058137740  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ThirdParty.Json.LitJson.ObjectMetadata,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* ObjectMetadata_t4058137740_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1651615094_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1651615094_gshared (Transform_1_t200415287 * __this, Il2CppObject * ___key0, ObjectMetadata_t4058137740  ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1651615094_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(ObjectMetadata_t4058137740_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ThirdParty.Json.LitJson.ObjectMetadata,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t3048875398  Transform_1_EndInvoke_m4238639539_gshared (Transform_1_t200415287 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t3048875398 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ThirdParty.Json.LitJson.ObjectMetadata,System.Collections.Generic.KeyValuePair`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m284953412_gshared (Transform_1_t2854050275 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ThirdParty.Json.LitJson.ObjectMetadata,System.Collections.Generic.KeyValuePair`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t1407543090  Transform_1_Invoke_m3267673476_gshared (Transform_1_t2854050275 * __this, Il2CppObject * ___key0, ObjectMetadata_t4058137740  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3267673476((Transform_1_t2854050275 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t1407543090  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, ObjectMetadata_t4058137740  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef KeyValuePair_2_t1407543090  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, ObjectMetadata_t4058137740  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t1407543090  (*FunctionPointerType) (void* __this, ObjectMetadata_t4058137740  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ThirdParty.Json.LitJson.ObjectMetadata,System.Collections.Generic.KeyValuePair`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* ObjectMetadata_t4058137740_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2998251379_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2998251379_gshared (Transform_1_t2854050275 * __this, Il2CppObject * ___key0, ObjectMetadata_t4058137740  ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2998251379_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(ObjectMetadata_t4058137740_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ThirdParty.Json.LitJson.ObjectMetadata,System.Collections.Generic.KeyValuePair`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t1407543090  Transform_1_EndInvoke_m788982190_gshared (Transform_1_t2854050275 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t1407543090 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ThirdParty.Json.LitJson.ObjectMetadata,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m897368995_gshared (Transform_1_t4135956480 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ThirdParty.Json.LitJson.ObjectMetadata,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m926129863_gshared (Transform_1_t4135956480 * __this, Il2CppObject * ___key0, ObjectMetadata_t4058137740  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m926129863((Transform_1_t4135956480 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, ObjectMetadata_t4058137740  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, ObjectMetadata_t4058137740  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, ObjectMetadata_t4058137740  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ThirdParty.Json.LitJson.ObjectMetadata,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* ObjectMetadata_t4058137740_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m765669624_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m765669624_gshared (Transform_1_t4135956480 * __this, Il2CppObject * ___key0, ObjectMetadata_t4058137740  ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m765669624_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(ObjectMetadata_t4058137740_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ThirdParty.Json.LitJson.ObjectMetadata,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Transform_1_EndInvoke_m3260600265_gshared (Transform_1_t4135956480 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ThirdParty.Json.LitJson.ObjectMetadata,ThirdParty.Json.LitJson.ObjectMetadata>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2356499999_gshared (Transform_1_t1209677629 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ThirdParty.Json.LitJson.ObjectMetadata,ThirdParty.Json.LitJson.ObjectMetadata>::Invoke(TKey,TValue)
extern "C"  ObjectMetadata_t4058137740  Transform_1_Invoke_m3678771547_gshared (Transform_1_t1209677629 * __this, Il2CppObject * ___key0, ObjectMetadata_t4058137740  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3678771547((Transform_1_t1209677629 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef ObjectMetadata_t4058137740  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, ObjectMetadata_t4058137740  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef ObjectMetadata_t4058137740  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, ObjectMetadata_t4058137740  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef ObjectMetadata_t4058137740  (*FunctionPointerType) (void* __this, ObjectMetadata_t4058137740  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ThirdParty.Json.LitJson.ObjectMetadata,ThirdParty.Json.LitJson.ObjectMetadata>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* ObjectMetadata_t4058137740_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1969615422_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1969615422_gshared (Transform_1_t1209677629 * __this, Il2CppObject * ___key0, ObjectMetadata_t4058137740  ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1969615422_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(ObjectMetadata_t4058137740_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ThirdParty.Json.LitJson.ObjectMetadata,ThirdParty.Json.LitJson.ObjectMetadata>::EndInvoke(System.IAsyncResult)
extern "C"  ObjectMetadata_t4058137740  Transform_1_EndInvoke_m3217343613_gshared (Transform_1_t1209677629 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(ObjectMetadata_t4058137740 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.TextEditor/TextEditOp,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3859670361_gshared (Transform_1_t119033769 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.TextEditor/TextEditOp,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Transform_1_Invoke_m2465041021_gshared (Transform_1_t119033769 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2465041021((Transform_1_t119033769 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (void* __this, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.TextEditor/TextEditOp,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* TextEditOp_t3138797698_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2296443620_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2296443620_gshared (Transform_1_t119033769 * __this, Il2CppObject * ___key0, int32_t ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2296443620_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(TextEditOp_t3138797698_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.TextEditor/TextEditOp,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t3048875398  Transform_1_EndInvoke_m3350250331_gshared (Transform_1_t119033769 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t3048875398 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.TextEditor/TextEditOp,System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1077699798_gshared (Transform_1_t1853328715 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.TextEditor/TextEditOp,System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t488203048  Transform_1_Invoke_m3982738838_gshared (Transform_1_t1853328715 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3982738838((Transform_1_t1853328715 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t488203048  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef KeyValuePair_2_t488203048  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t488203048  (*FunctionPointerType) (void* __this, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.TextEditor/TextEditOp,System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* TextEditOp_t3138797698_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m456124159_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m456124159_gshared (Transform_1_t1853328715 * __this, Il2CppObject * ___key0, int32_t ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m456124159_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(TextEditOp_t3138797698_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.TextEditor/TextEditOp,System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t488203048  Transform_1_EndInvoke_m3931650220_gshared (Transform_1_t1853328715 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t488203048 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.TextEditor/TextEditOp,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1159717803_gshared (Transform_1_t4054574962 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.TextEditor/TextEditOp,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m1019050223_gshared (Transform_1_t4054574962 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1019050223((Transform_1_t4054574962 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.TextEditor/TextEditOp,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* TextEditOp_t3138797698_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2225416906_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2225416906_gshared (Transform_1_t4054574962 * __this, Il2CppObject * ___key0, int32_t ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2225416906_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(TextEditOp_t3138797698_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.TextEditor/TextEditOp,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Transform_1_EndInvoke_m1781253113_gshared (Transform_1_t4054574962 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.TextEditor/TextEditOp,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m854873783_gshared (Transform_1_t208956069 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.TextEditor/TextEditOp,UnityEngine.TextEditor/TextEditOp>::Invoke(TKey,TValue)
extern "C"  int32_t Transform_1_Invoke_m2882650595_gshared (Transform_1_t208956069 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2882650595((Transform_1_t208956069 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.TextEditor/TextEditOp,UnityEngine.TextEditor/TextEditOp>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* TextEditOp_t3138797698_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2391125890_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2391125890_gshared (Transform_1_t208956069 * __this, Il2CppObject * ___key0, int32_t ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2391125890_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(TextEditOp_t3138797698_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.TextEditor/TextEditOp,UnityEngine.TextEditor/TextEditOp>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Transform_1_EndInvoke_m958090529_gshared (Transform_1_t208956069 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Amazon.Runtime.Metric,System.Int64>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m236173709_gshared (Enumerator_t1050721994 * __this, Dictionary_2_t3659156526 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t3659156526 * L_0 = ___host0;
		NullCheck((Dictionary_2_t3659156526 *)L_0);
		Enumerator_t684213932  L_1 = ((  Enumerator_t684213932  (*) (Dictionary_2_t3659156526 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t3659156526 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m236173709_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t3659156526 * ___host0, const MethodInfo* method)
{
	Enumerator_t1050721994 * _thisAdjusted = reinterpret_cast<Enumerator_t1050721994 *>(__this + 1);
	Enumerator__ctor_m236173709(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Amazon.Runtime.Metric,System.Int64>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m162752738_gshared (Enumerator_t1050721994 * __this, const MethodInfo* method)
{
	{
		Enumerator_t684213932 * L_0 = (Enumerator_t684213932 *)__this->get_address_of_host_enumerator_0();
		int64_t L_1 = Enumerator_get_CurrentValue_m2007594771((Enumerator_t684213932 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		int64_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_2);
		return L_3;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m162752738_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1050721994 * _thisAdjusted = reinterpret_cast<Enumerator_t1050721994 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m162752738(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Amazon.Runtime.Metric,System.Int64>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1851689148_gshared (Enumerator_t1050721994 * __this, const MethodInfo* method)
{
	{
		Enumerator_t684213932 * L_0 = (Enumerator_t684213932 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m1537962437((Enumerator_t684213932 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1851689148_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1050721994 * _thisAdjusted = reinterpret_cast<Enumerator_t1050721994 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m1851689148(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Amazon.Runtime.Metric,System.Int64>::Dispose()
extern "C"  void Enumerator_Dispose_m1587742133_gshared (Enumerator_t1050721994 * __this, const MethodInfo* method)
{
	{
		Enumerator_t684213932 * L_0 = (Enumerator_t684213932 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m2961717971((Enumerator_t684213932 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m1587742133_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1050721994 * _thisAdjusted = reinterpret_cast<Enumerator_t1050721994 *>(__this + 1);
	Enumerator_Dispose_m1587742133(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Amazon.Runtime.Metric,System.Int64>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3485337056_gshared (Enumerator_t1050721994 * __this, const MethodInfo* method)
{
	{
		Enumerator_t684213932 * L_0 = (Enumerator_t684213932 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m1520629361((Enumerator_t684213932 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m3485337056_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1050721994 * _thisAdjusted = reinterpret_cast<Enumerator_t1050721994 *>(__this + 1);
	return Enumerator_MoveNext_m3485337056(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Amazon.Runtime.Metric,System.Int64>::get_Current()
extern "C"  int64_t Enumerator_get_Current_m1859672514_gshared (Enumerator_t1050721994 * __this, const MethodInfo* method)
{
	{
		Enumerator_t684213932 * L_0 = (Enumerator_t684213932 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t1416501748 * L_1 = (KeyValuePair_2_t1416501748 *)L_0->get_address_of_current_3();
		int64_t L_2 = KeyValuePair_2_get_Value_m3929255215((KeyValuePair_2_t1416501748 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  int64_t Enumerator_get_Current_m1859672514_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1050721994 * _thisAdjusted = reinterpret_cast<Enumerator_t1050721994 *>(__this + 1);
	return Enumerator_get_Current_m1859672514(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Amazon.Runtime.Metric,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m18447153_gshared (Enumerator_t2831093252 * __this, Dictionary_2_t1144560488 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t1144560488 * L_0 = ___host0;
		NullCheck((Dictionary_2_t1144560488 *)L_0);
		Enumerator_t2464585190  L_1 = ((  Enumerator_t2464585190  (*) (Dictionary_2_t1144560488 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t1144560488 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m18447153_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t1144560488 * ___host0, const MethodInfo* method)
{
	Enumerator_t2831093252 * _thisAdjusted = reinterpret_cast<Enumerator_t2831093252 *>(__this + 1);
	Enumerator__ctor_m18447153(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Amazon.Runtime.Metric,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2846411020_gshared (Enumerator_t2831093252 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2464585190 * L_0 = (Enumerator_t2464585190 *)__this->get_address_of_host_enumerator_0();
		Il2CppObject * L_1 = Enumerator_get_CurrentValue_m4218670947((Enumerator_t2464585190 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2846411020_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2831093252 * _thisAdjusted = reinterpret_cast<Enumerator_t2831093252 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2846411020(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Amazon.Runtime.Metric,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3089225940_gshared (Enumerator_t2831093252 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2464585190 * L_0 = (Enumerator_t2464585190 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m1867529997((Enumerator_t2464585190 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3089225940_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2831093252 * _thisAdjusted = reinterpret_cast<Enumerator_t2831093252 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m3089225940(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Amazon.Runtime.Metric,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m1070722481_gshared (Enumerator_t2831093252 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2464585190 * L_0 = (Enumerator_t2464585190 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m2167966803((Enumerator_t2464585190 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m1070722481_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2831093252 * _thisAdjusted = reinterpret_cast<Enumerator_t2831093252 *>(__this + 1);
	Enumerator_Dispose_m1070722481(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Amazon.Runtime.Metric,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1585495576_gshared (Enumerator_t2831093252 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2464585190 * L_0 = (Enumerator_t2464585190 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m3844071554((Enumerator_t2464585190 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m1585495576_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2831093252 * _thisAdjusted = reinterpret_cast<Enumerator_t2831093252 *>(__this + 1);
	return Enumerator_MoveNext_m1585495576(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Amazon.Runtime.Metric,System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m1001769806_gshared (Enumerator_t2831093252 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2464585190 * L_0 = (Enumerator_t2464585190 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t3196873006 * L_1 = (KeyValuePair_2_t3196873006 *)L_0->get_address_of_current_3();
		Il2CppObject * L_2 = KeyValuePair_2_get_Value_m3734313679((KeyValuePair_2_t3196873006 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_get_Current_m1001769806_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2831093252 * _thisAdjusted = reinterpret_cast<Enumerator_t2831093252 *>(__this + 1);
	return Enumerator_get_Current_m1001769806(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m370613445_gshared (Enumerator_t2766235847 * __this, Dictionary_2_t1079703083 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t1079703083 * L_0 = ___host0;
		NullCheck((Dictionary_2_t1079703083 *)L_0);
		Enumerator_t2399727785  L_1 = ((  Enumerator_t2399727785  (*) (Dictionary_2_t1079703083 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t1079703083 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m370613445_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t1079703083 * ___host0, const MethodInfo* method)
{
	Enumerator_t2766235847 * _thisAdjusted = reinterpret_cast<Enumerator_t2766235847 *>(__this + 1);
	Enumerator__ctor_m370613445(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m4017233928_gshared (Enumerator_t2766235847 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2399727785 * L_0 = (Enumerator_t2399727785 *)__this->get_address_of_host_enumerator_0();
		int32_t L_1 = Enumerator_get_CurrentValue_m2708916123((Enumerator_t2399727785 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_2);
		return L_3;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m4017233928_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2766235847 * _thisAdjusted = reinterpret_cast<Enumerator_t2766235847 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m4017233928(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m9625560_gshared (Enumerator_t2766235847 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2399727785 * L_0 = (Enumerator_t2399727785 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m2787013825((Enumerator_t2399727785 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m9625560_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2766235847 * _thisAdjusted = reinterpret_cast<Enumerator_t2766235847 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m9625560(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m2229103293_gshared (Enumerator_t2766235847 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2399727785 * L_0 = (Enumerator_t2399727785 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m1631869499((Enumerator_t2399727785 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2229103293_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2766235847 * _thisAdjusted = reinterpret_cast<Enumerator_t2766235847 *>(__this + 1);
	Enumerator_Dispose_m2229103293(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3657937156_gshared (Enumerator_t2766235847 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2399727785 * L_0 = (Enumerator_t2399727785 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m3809319098((Enumerator_t2399727785 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m3657937156_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2766235847 * _thisAdjusted = reinterpret_cast<Enumerator_t2766235847 *>(__this + 1);
	return Enumerator_MoveNext_m3657937156(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Int32>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m25368454_gshared (Enumerator_t2766235847 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2399727785 * L_0 = (Enumerator_t2399727785 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t3132015601 * L_1 = (KeyValuePair_2_t3132015601 *)L_0->get_address_of_current_3();
		int32_t L_2 = KeyValuePair_2_get_Value_m1136571287((KeyValuePair_2_t3132015601 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  int32_t Enumerator_get_Current_m25368454_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2766235847 * _thisAdjusted = reinterpret_cast<Enumerator_t2766235847 *>(__this + 1);
	return Enumerator_get_Current_m25368454(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2988407410_gshared (Enumerator_t3383807694 * __this, Dictionary_2_t1697274930 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t1697274930 * L_0 = ___host0;
		NullCheck((Dictionary_2_t1697274930 *)L_0);
		Enumerator_t3017299632  L_1 = ((  Enumerator_t3017299632  (*) (Dictionary_2_t1697274930 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t1697274930 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m2988407410_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t1697274930 * ___host0, const MethodInfo* method)
{
	Enumerator_t3383807694 * _thisAdjusted = reinterpret_cast<Enumerator_t3383807694 *>(__this + 1);
	Enumerator__ctor_m2988407410(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1648049763_gshared (Enumerator_t3383807694 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3017299632 * L_0 = (Enumerator_t3017299632 *)__this->get_address_of_host_enumerator_0();
		Il2CppObject * L_1 = Enumerator_get_CurrentValue_m3562053380((Enumerator_t3017299632 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1648049763_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3383807694 * _thisAdjusted = reinterpret_cast<Enumerator_t3383807694 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1648049763(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m655633499_gshared (Enumerator_t3383807694 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3017299632 * L_0 = (Enumerator_t3017299632 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m761796566((Enumerator_t3017299632 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m655633499_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3383807694 * _thisAdjusted = reinterpret_cast<Enumerator_t3383807694 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m655633499(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m2369319718_gshared (Enumerator_t3383807694 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3017299632 * L_0 = (Enumerator_t3017299632 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m2243145188((Enumerator_t3017299632 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2369319718_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3383807694 * _thisAdjusted = reinterpret_cast<Enumerator_t3383807694 *>(__this + 1);
	Enumerator_Dispose_m2369319718(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1091131935_gshared (Enumerator_t3383807694 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3017299632 * L_0 = (Enumerator_t3017299632 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m2770956757((Enumerator_t3017299632 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m1091131935_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3383807694 * _thisAdjusted = reinterpret_cast<Enumerator_t3383807694 *>(__this + 1);
	return Enumerator_MoveNext_m1091131935(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m2132741765_gshared (Enumerator_t3383807694 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3017299632 * L_0 = (Enumerator_t3017299632 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t3749587448 * L_1 = (KeyValuePair_2_t3749587448 *)L_0->get_address_of_current_3();
		Il2CppObject * L_2 = KeyValuePair_2_get_Value_m3690000728((KeyValuePair_2_t3749587448 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_get_Current_m2132741765_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3383807694 * _thisAdjusted = reinterpret_cast<Enumerator_t3383807694 *>(__this + 1);
	return Enumerator_get_Current_m2132741765(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m419014865_gshared (Enumerator_t1070533165 * __this, Dictionary_2_t3678967697 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t3678967697 * L_0 = ___host0;
		NullCheck((Dictionary_2_t3678967697 *)L_0);
		Enumerator_t704025103  L_1 = ((  Enumerator_t704025103  (*) (Dictionary_2_t3678967697 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t3678967697 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m419014865_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t3678967697 * ___host0, const MethodInfo* method)
{
	Enumerator_t1070533165 * _thisAdjusted = reinterpret_cast<Enumerator_t1070533165 *>(__this + 1);
	Enumerator__ctor_m419014865(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2712263668_gshared (Enumerator_t1070533165 * __this, const MethodInfo* method)
{
	{
		Enumerator_t704025103 * L_0 = (Enumerator_t704025103 *)__this->get_address_of_host_enumerator_0();
		Il2CppObject * L_1 = Enumerator_get_CurrentValue_m659195263((Enumerator_t704025103 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2712263668_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1070533165 * _thisAdjusted = reinterpret_cast<Enumerator_t1070533165 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2712263668(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m149043970_gshared (Enumerator_t1070533165 * __this, const MethodInfo* method)
{
	{
		Enumerator_t704025103 * L_0 = (Enumerator_t704025103 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m598725905((Enumerator_t704025103 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m149043970_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1070533165 * _thisAdjusted = reinterpret_cast<Enumerator_t1070533165 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m149043970(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m942415041_gshared (Enumerator_t1070533165 * __this, const MethodInfo* method)
{
	{
		Enumerator_t704025103 * L_0 = (Enumerator_t704025103 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m3446908287((Enumerator_t704025103 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m942415041_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1070533165 * _thisAdjusted = reinterpret_cast<Enumerator_t1070533165 *>(__this + 1);
	Enumerator_Dispose_m942415041(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2045321910_gshared (Enumerator_t1070533165 * __this, const MethodInfo* method)
{
	{
		Enumerator_t704025103 * L_0 = (Enumerator_t704025103 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m3139459564((Enumerator_t704025103 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m2045321910_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1070533165 * _thisAdjusted = reinterpret_cast<Enumerator_t1070533165 *>(__this + 1);
	return Enumerator_MoveNext_m2045321910(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m1952080304_gshared (Enumerator_t1070533165 * __this, const MethodInfo* method)
{
	{
		Enumerator_t704025103 * L_0 = (Enumerator_t704025103 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t1436312919 * L_1 = (KeyValuePair_2_t1436312919 *)L_0->get_address_of_current_3();
		Il2CppObject * L_2 = KeyValuePair_2_get_Value_m1978486399((KeyValuePair_2_t1436312919 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_get_Current_m1952080304_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1070533165 * _thisAdjusted = reinterpret_cast<Enumerator_t1070533165 *>(__this + 1);
	return Enumerator_get_Current_m1952080304(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.IntPtr,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1320388337_gshared (Enumerator_t523040081 * __this, Dictionary_2_t3131474613 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t3131474613 * L_0 = ___host0;
		NullCheck((Dictionary_2_t3131474613 *)L_0);
		Enumerator_t156532019  L_1 = ((  Enumerator_t156532019  (*) (Dictionary_2_t3131474613 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t3131474613 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1320388337_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t3131474613 * ___host0, const MethodInfo* method)
{
	Enumerator_t523040081 * _thisAdjusted = reinterpret_cast<Enumerator_t523040081 *>(__this + 1);
	Enumerator__ctor_m1320388337(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.IntPtr,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2160496972_gshared (Enumerator_t523040081 * __this, const MethodInfo* method)
{
	{
		Enumerator_t156532019 * L_0 = (Enumerator_t156532019 *)__this->get_address_of_host_enumerator_0();
		Il2CppObject * L_1 = Enumerator_get_CurrentValue_m3147984131((Enumerator_t156532019 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2160496972_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t523040081 * _thisAdjusted = reinterpret_cast<Enumerator_t523040081 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2160496972(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.IntPtr,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1146954164_gshared (Enumerator_t523040081 * __this, const MethodInfo* method)
{
	{
		Enumerator_t156532019 * L_0 = (Enumerator_t156532019 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m1147555085((Enumerator_t156532019 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1146954164_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t523040081 * _thisAdjusted = reinterpret_cast<Enumerator_t523040081 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m1146954164(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.IntPtr,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m1062622161_gshared (Enumerator_t523040081 * __this, const MethodInfo* method)
{
	{
		Enumerator_t156532019 * L_0 = (Enumerator_t156532019 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m2117437651((Enumerator_t156532019 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m1062622161_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t523040081 * _thisAdjusted = reinterpret_cast<Enumerator_t523040081 *>(__this + 1);
	Enumerator_Dispose_m1062622161(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.IntPtr,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1404007384_gshared (Enumerator_t523040081 * __this, const MethodInfo* method)
{
	{
		Enumerator_t156532019 * L_0 = (Enumerator_t156532019 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m266619810((Enumerator_t156532019 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m1404007384_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t523040081 * _thisAdjusted = reinterpret_cast<Enumerator_t523040081 *>(__this + 1);
	return Enumerator_MoveNext_m1404007384(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.IntPtr,System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m419581838_gshared (Enumerator_t523040081 * __this, const MethodInfo* method)
{
	{
		Enumerator_t156532019 * L_0 = (Enumerator_t156532019 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t888819835 * L_1 = (KeyValuePair_2_t888819835 *)L_0->get_address_of_current_3();
		Il2CppObject * L_2 = KeyValuePair_2_get_Value_m544293807((KeyValuePair_2_t888819835 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_get_Current_m419581838_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t523040081 * _thisAdjusted = reinterpret_cast<Enumerator_t523040081 *>(__this + 1);
	return Enumerator_get_Current_m419581838(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1575581033_gshared (Enumerator_t2911400270 * __this, Dictionary_2_t1224867506 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t1224867506 * L_0 = ___host0;
		NullCheck((Dictionary_2_t1224867506 *)L_0);
		Enumerator_t2544892208  L_1 = ((  Enumerator_t2544892208  (*) (Dictionary_2_t1224867506 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t1224867506 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1575581033_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t1224867506 * ___host0, const MethodInfo* method)
{
	Enumerator_t2911400270 * _thisAdjusted = reinterpret_cast<Enumerator_t2911400270 *>(__this + 1);
	Enumerator__ctor_m1575581033(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3458593286_gshared (Enumerator_t2911400270 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2544892208 * L_0 = (Enumerator_t2544892208 *)__this->get_address_of_host_enumerator_0();
		int32_t L_1 = Enumerator_get_CurrentValue_m17055867((Enumerator_t2544892208 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_2);
		return L_3;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3458593286_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2911400270 * _thisAdjusted = reinterpret_cast<Enumerator_t2911400270 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m3458593286(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m203558566_gshared (Enumerator_t2911400270 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2544892208 * L_0 = (Enumerator_t2544892208 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m1928144285((Enumerator_t2544892208 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m203558566_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2911400270 * _thisAdjusted = reinterpret_cast<Enumerator_t2911400270 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m203558566(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::Dispose()
extern "C"  void Enumerator_Dispose_m3181308769_gshared (Enumerator_t2911400270 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2544892208 * L_0 = (Enumerator_t2544892208 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m1362422387((Enumerator_t2544892208 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3181308769_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2911400270 * _thisAdjusted = reinterpret_cast<Enumerator_t2911400270 *>(__this + 1);
	Enumerator_Dispose_m3181308769(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2790411218_gshared (Enumerator_t2911400270 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2544892208 * L_0 = (Enumerator_t2544892208 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m3850333932((Enumerator_t2544892208 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m2790411218_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2911400270 * _thisAdjusted = reinterpret_cast<Enumerator_t2911400270 *>(__this + 1);
	return Enumerator_MoveNext_m2790411218(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m1134368040_gshared (Enumerator_t2911400270 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2544892208 * L_0 = (Enumerator_t2544892208 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t3277180024 * L_1 = (KeyValuePair_2_t3277180024 *)L_0->get_address_of_current_3();
		int32_t L_2 = KeyValuePair_2_get_Value_m116637759((KeyValuePair_2_t3277180024 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  int32_t Enumerator_get_Current_m1134368040_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2911400270 * _thisAdjusted = reinterpret_cast<Enumerator_t2911400270 *>(__this + 1);
	return Enumerator_get_Current_m1134368040(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m908409898_gshared (Enumerator_t809200314 * __this, Dictionary_2_t3417634846 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t3417634846 * L_0 = ___host0;
		NullCheck((Dictionary_2_t3417634846 *)L_0);
		Enumerator_t442692252  L_1 = ((  Enumerator_t442692252  (*) (Dictionary_2_t3417634846 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t3417634846 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m908409898_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t3417634846 * ___host0, const MethodInfo* method)
{
	Enumerator_t809200314 * _thisAdjusted = reinterpret_cast<Enumerator_t809200314 *>(__this + 1);
	Enumerator__ctor_m908409898(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Boolean>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2625473469_gshared (Enumerator_t809200314 * __this, const MethodInfo* method)
{
	{
		Enumerator_t442692252 * L_0 = (Enumerator_t442692252 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_get_CurrentValue_m4143929484((Enumerator_t442692252 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		bool L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_2);
		return L_3;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2625473469_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t809200314 * _thisAdjusted = reinterpret_cast<Enumerator_t809200314 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2625473469(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Boolean>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2909592833_gshared (Enumerator_t809200314 * __this, const MethodInfo* method)
{
	{
		Enumerator_t442692252 * L_0 = (Enumerator_t442692252 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m3115320746((Enumerator_t442692252 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2909592833_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t809200314 * _thisAdjusted = reinterpret_cast<Enumerator_t809200314 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m2909592833(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Boolean>::Dispose()
extern "C"  void Enumerator_Dispose_m1323464986_gshared (Enumerator_t809200314 * __this, const MethodInfo* method)
{
	{
		Enumerator_t442692252 * L_0 = (Enumerator_t442692252 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m2711120408((Enumerator_t442692252 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m1323464986_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t809200314 * _thisAdjusted = reinterpret_cast<Enumerator_t809200314 *>(__this + 1);
	Enumerator_Dispose_m1323464986(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Boolean>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1212551889_gshared (Enumerator_t809200314 * __this, const MethodInfo* method)
{
	{
		Enumerator_t442692252 * L_0 = (Enumerator_t442692252 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m1856697671((Enumerator_t442692252 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m1212551889_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t809200314 * _thisAdjusted = reinterpret_cast<Enumerator_t809200314 *>(__this + 1);
	return Enumerator_MoveNext_m1212551889(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Boolean>::get_Current()
extern "C"  bool Enumerator_get_Current_m2986380627_gshared (Enumerator_t809200314 * __this, const MethodInfo* method)
{
	{
		Enumerator_t442692252 * L_0 = (Enumerator_t442692252 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t1174980068 * L_1 = (KeyValuePair_2_t1174980068 *)L_0->get_address_of_current_3();
		bool L_2 = KeyValuePair_2_get_Value_m1916631176((KeyValuePair_2_t1174980068 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  bool Enumerator_get_Current_m2986380627_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t809200314 * _thisAdjusted = reinterpret_cast<Enumerator_t809200314 *>(__this + 1);
	return Enumerator_get_Current_m2986380627(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3539306986_gshared (Enumerator_t3350470340 * __this, Dictionary_2_t1663937576 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t1663937576 * L_0 = ___host0;
		NullCheck((Dictionary_2_t1663937576 *)L_0);
		Enumerator_t2983962278  L_1 = ((  Enumerator_t2983962278  (*) (Dictionary_2_t1663937576 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t1663937576 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3539306986_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t1663937576 * ___host0, const MethodInfo* method)
{
	Enumerator_t3350470340 * _thisAdjusted = reinterpret_cast<Enumerator_t3350470340 *>(__this + 1);
	Enumerator__ctor_m3539306986(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1805365227_gshared (Enumerator_t3350470340 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2983962278 * L_0 = (Enumerator_t2983962278 *)__this->get_address_of_host_enumerator_0();
		int32_t L_1 = Enumerator_get_CurrentValue_m2645962456((Enumerator_t2983962278 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_2);
		return L_3;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1805365227_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3350470340 * _thisAdjusted = reinterpret_cast<Enumerator_t3350470340 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1805365227(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3294415347_gshared (Enumerator_t3350470340 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2983962278 * L_0 = (Enumerator_t2983962278 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m1132695838((Enumerator_t2983962278 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3294415347_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3350470340 * _thisAdjusted = reinterpret_cast<Enumerator_t3350470340 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m3294415347(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m2532362830_gshared (Enumerator_t3350470340 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2983962278 * L_0 = (Enumerator_t2983962278 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m401572848((Enumerator_t2983962278 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2532362830_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3350470340 * _thisAdjusted = reinterpret_cast<Enumerator_t3350470340 *>(__this + 1);
	Enumerator_Dispose_m2532362830(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2534596951_gshared (Enumerator_t3350470340 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2983962278 * L_0 = (Enumerator_t2983962278 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m435964161((Enumerator_t2983962278 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m2534596951_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3350470340 * _thisAdjusted = reinterpret_cast<Enumerator_t3350470340 *>(__this + 1);
	return Enumerator_MoveNext_m2534596951(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Int32>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m2838387513_gshared (Enumerator_t3350470340 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2983962278 * L_0 = (Enumerator_t2983962278 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t3716250094 * L_1 = (KeyValuePair_2_t3716250094 *)L_0->get_address_of_current_3();
		int32_t L_2 = KeyValuePair_2_get_Value_m3699669100((KeyValuePair_2_t3716250094 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  int32_t Enumerator_get_Current_m2838387513_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3350470340 * _thisAdjusted = reinterpret_cast<Enumerator_t3350470340 *>(__this + 1);
	return Enumerator_get_Current_m2838387513(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3819430617_gshared (Enumerator_t3968042187 * __this, Dictionary_2_t2281509423 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t2281509423 * L_0 = ___host0;
		NullCheck((Dictionary_2_t2281509423 *)L_0);
		Enumerator_t3601534125  L_1 = ((  Enumerator_t3601534125  (*) (Dictionary_2_t2281509423 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t2281509423 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3819430617_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t2281509423 * ___host0, const MethodInfo* method)
{
	Enumerator_t3968042187 * _thisAdjusted = reinterpret_cast<Enumerator_t3968042187 *>(__this + 1);
	Enumerator__ctor_m3819430617(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3933483934_gshared (Enumerator_t3968042187 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3601534125 * L_0 = (Enumerator_t3601534125 *)__this->get_address_of_host_enumerator_0();
		Il2CppObject * L_1 = Enumerator_get_CurrentValue_m402763047((Enumerator_t3601534125 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3933483934_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3968042187 * _thisAdjusted = reinterpret_cast<Enumerator_t3968042187 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m3933483934(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2482663638_gshared (Enumerator_t3968042187 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3601534125 * L_0 = (Enumerator_t3601534125 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m3129803197((Enumerator_t3601534125 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2482663638_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3968042187 * _thisAdjusted = reinterpret_cast<Enumerator_t3968042187 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m2482663638(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m4238653081_gshared (Enumerator_t3968042187 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3601534125 * L_0 = (Enumerator_t3601534125 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m1905011127((Enumerator_t3601534125 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m4238653081_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3968042187 * _thisAdjusted = reinterpret_cast<Enumerator_t3968042187 *>(__this + 1);
	Enumerator_Dispose_m4238653081(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m335649778_gshared (Enumerator_t3968042187 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3601534125 * L_0 = (Enumerator_t3601534125 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m3349738440((Enumerator_t3601534125 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m335649778_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3968042187 * _thisAdjusted = reinterpret_cast<Enumerator_t3968042187 *>(__this + 1);
	return Enumerator_MoveNext_m335649778(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m4025002300_gshared (Enumerator_t3968042187 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3601534125 * L_0 = (Enumerator_t3601534125 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t38854645 * L_1 = (KeyValuePair_2_t38854645 *)L_0->get_address_of_current_3();
		Il2CppObject * L_2 = KeyValuePair_2_get_Value_m499643803((KeyValuePair_2_t38854645 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_get_Current_m4025002300_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3968042187 * _thisAdjusted = reinterpret_cast<Enumerator_t3968042187 *>(__this + 1);
	return Enumerator_get_Current_m4025002300(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3721515377_gshared (Enumerator_t2413670906 * __this, Dictionary_2_t727138142 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t727138142 * L_0 = ___host0;
		NullCheck((Dictionary_2_t727138142 *)L_0);
		Enumerator_t2047162844  L_1 = ((  Enumerator_t2047162844  (*) (Dictionary_2_t727138142 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t727138142 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3721515377_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t727138142 * ___host0, const MethodInfo* method)
{
	Enumerator_t2413670906 * _thisAdjusted = reinterpret_cast<Enumerator_t2413670906 *>(__this + 1);
	Enumerator__ctor_m3721515377(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2245862918_gshared (Enumerator_t2413670906 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2047162844 * L_0 = (Enumerator_t2047162844 *)__this->get_address_of_host_enumerator_0();
		ArrayMetadata_t1135078014  L_1 = Enumerator_get_CurrentValue_m2382161119((Enumerator_t2047162844 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		ArrayMetadata_t1135078014  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_2);
		return L_3;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2245862918_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2413670906 * _thisAdjusted = reinterpret_cast<Enumerator_t2413670906 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2245862918(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4229587238_gshared (Enumerator_t2413670906 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2047162844 * L_0 = (Enumerator_t2047162844 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m2586713469((Enumerator_t2047162844 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4229587238_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2413670906 * _thisAdjusted = reinterpret_cast<Enumerator_t2413670906 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m4229587238(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::Dispose()
extern "C"  void Enumerator_Dispose_m1982504185_gshared (Enumerator_t2413670906 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2047162844 * L_0 = (Enumerator_t2047162844 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m4092052839((Enumerator_t2047162844 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m1982504185_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2413670906 * _thisAdjusted = reinterpret_cast<Enumerator_t2413670906 *>(__this + 1);
	Enumerator_Dispose_m1982504185(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3949836258_gshared (Enumerator_t2413670906 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2047162844 * L_0 = (Enumerator_t2047162844 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m2137572552((Enumerator_t2047162844 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m3949836258_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2413670906 * _thisAdjusted = reinterpret_cast<Enumerator_t2413670906 *>(__this + 1);
	return Enumerator_MoveNext_m3949836258(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::get_Current()
extern "C"  ArrayMetadata_t1135078014  Enumerator_get_Current_m148336996_gshared (Enumerator_t2413670906 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2047162844 * L_0 = (Enumerator_t2047162844 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t2779450660 * L_1 = (KeyValuePair_2_t2779450660 *)L_0->get_address_of_current_3();
		ArrayMetadata_t1135078014  L_2 = KeyValuePair_2_get_Value_m3869691915((KeyValuePair_2_t2779450660 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  ArrayMetadata_t1135078014  Enumerator_get_Current_m148336996_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2413670906 * _thisAdjusted = reinterpret_cast<Enumerator_t2413670906 *>(__this + 1);
	return Enumerator_get_Current_m148336996(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3789863417_gshared (Enumerator_t1041763336 * __this, Dictionary_2_t3650197868 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t3650197868 * L_0 = ___host0;
		NullCheck((Dictionary_2_t3650197868 *)L_0);
		Enumerator_t675255274  L_1 = ((  Enumerator_t675255274  (*) (Dictionary_2_t3650197868 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t3650197868 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3789863417_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t3650197868 * ___host0, const MethodInfo* method)
{
	Enumerator_t1041763336 * _thisAdjusted = reinterpret_cast<Enumerator_t1041763336 *>(__this + 1);
	Enumerator__ctor_m3789863417(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m4150435772_gshared (Enumerator_t1041763336 * __this, const MethodInfo* method)
{
	{
		Enumerator_t675255274 * L_0 = (Enumerator_t675255274 *)__this->get_address_of_host_enumerator_0();
		ObjectMetadata_t4058137740  L_1 = Enumerator_get_CurrentValue_m2906228503((Enumerator_t675255274 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		ObjectMetadata_t4058137740  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_2);
		return L_3;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m4150435772_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1041763336 * _thisAdjusted = reinterpret_cast<Enumerator_t1041763336 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m4150435772(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m804193410_gshared (Enumerator_t1041763336 * __this, const MethodInfo* method)
{
	{
		Enumerator_t675255274 * L_0 = (Enumerator_t675255274 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m975923089((Enumerator_t675255274 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m804193410_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1041763336 * _thisAdjusted = reinterpret_cast<Enumerator_t1041763336 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m804193410(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::Dispose()
extern "C"  void Enumerator_Dispose_m4014171073_gshared (Enumerator_t1041763336 * __this, const MethodInfo* method)
{
	{
		Enumerator_t675255274 * L_0 = (Enumerator_t675255274 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m3127619583((Enumerator_t675255274 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m4014171073_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1041763336 * _thisAdjusted = reinterpret_cast<Enumerator_t1041763336 *>(__this + 1);
	Enumerator_Dispose_m4014171073(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2332063510_gshared (Enumerator_t1041763336 * __this, const MethodInfo* method)
{
	{
		Enumerator_t675255274 * L_0 = (Enumerator_t675255274 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m513463628((Enumerator_t675255274 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m2332063510_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1041763336 * _thisAdjusted = reinterpret_cast<Enumerator_t1041763336 *>(__this + 1);
	return Enumerator_MoveNext_m2332063510(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::get_Current()
extern "C"  ObjectMetadata_t4058137740  Enumerator_get_Current_m114914456_gshared (Enumerator_t1041763336 * __this, const MethodInfo* method)
{
	{
		Enumerator_t675255274 * L_0 = (Enumerator_t675255274 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t1407543090 * L_1 = (KeyValuePair_2_t1407543090 *)L_0->get_address_of_current_3();
		ObjectMetadata_t4058137740  L_2 = KeyValuePair_2_get_Value_m3741940767((KeyValuePair_2_t1407543090 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  ObjectMetadata_t4058137740  Enumerator_get_Current_m114914456_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1041763336 * _thisAdjusted = reinterpret_cast<Enumerator_t1041763336 *>(__this + 1);
	return Enumerator_get_Current_m114914456(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1576117593_gshared (Enumerator_t122423294 * __this, Dictionary_2_t2730857826 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t2730857826 * L_0 = ___host0;
		NullCheck((Dictionary_2_t2730857826 *)L_0);
		Enumerator_t4050882528  L_1 = ((  Enumerator_t4050882528  (*) (Dictionary_2_t2730857826 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t2730857826 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1576117593_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t2730857826 * ___host0, const MethodInfo* method)
{
	Enumerator_t122423294 * _thisAdjusted = reinterpret_cast<Enumerator_t122423294 *>(__this + 1);
	Enumerator__ctor_m1576117593(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2191931798_gshared (Enumerator_t122423294 * __this, const MethodInfo* method)
{
	{
		Enumerator_t4050882528 * L_0 = (Enumerator_t4050882528 *)__this->get_address_of_host_enumerator_0();
		int32_t L_1 = Enumerator_get_CurrentValue_m3498625419((Enumerator_t4050882528 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_2);
		return L_3;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2191931798_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t122423294 * _thisAdjusted = reinterpret_cast<Enumerator_t122423294 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2191931798(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1432525334_gshared (Enumerator_t122423294 * __this, const MethodInfo* method)
{
	{
		Enumerator_t4050882528 * L_0 = (Enumerator_t4050882528 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m2489846157((Enumerator_t4050882528 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1432525334_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t122423294 * _thisAdjusted = reinterpret_cast<Enumerator_t122423294 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m1432525334(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::Dispose()
extern "C"  void Enumerator_Dispose_m1120084369_gshared (Enumerator_t122423294 * __this, const MethodInfo* method)
{
	{
		Enumerator_t4050882528 * L_0 = (Enumerator_t4050882528 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m793495907((Enumerator_t4050882528 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m1120084369_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t122423294 * _thisAdjusted = reinterpret_cast<Enumerator_t122423294 *>(__this + 1);
	Enumerator_Dispose_m1120084369(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1523827938_gshared (Enumerator_t122423294 * __this, const MethodInfo* method)
{
	{
		Enumerator_t4050882528 * L_0 = (Enumerator_t4050882528 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m2619008060((Enumerator_t4050882528 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m1523827938_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t122423294 * _thisAdjusted = reinterpret_cast<Enumerator_t122423294 *>(__this + 1);
	return Enumerator_MoveNext_m1523827938(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m3774065880_gshared (Enumerator_t122423294 * __this, const MethodInfo* method)
{
	{
		Enumerator_t4050882528 * L_0 = (Enumerator_t4050882528 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t488203048 * L_1 = (KeyValuePair_2_t488203048 *)L_0->get_address_of_current_3();
		int32_t L_2 = KeyValuePair_2_get_Value_m1644876463((KeyValuePair_2_t488203048 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  int32_t Enumerator_get_Current_m3774065880_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t122423294 * _thisAdjusted = reinterpret_cast<Enumerator_t122423294 *>(__this + 1);
	return Enumerator_get_Current_m3774065880(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Amazon.Runtime.Metric,System.Int64>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3629755714;
extern const uint32_t ValueCollection__ctor_m2887733138_MetadataUsageId;
extern "C"  void ValueCollection__ctor_m2887733138_gshared (ValueCollection_t2362216369 * __this, Dictionary_2_t3659156526 * ___dictionary0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ValueCollection__ctor_m2887733138_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t3659156526 * L_0 = ___dictionary0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral3629755714, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		Dictionary_2_t3659156526 * L_2 = ___dictionary0;
		__this->set_dictionary_0(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Amazon.Runtime.Metric,System.Int64>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3186695692_MetadataUsageId;
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3186695692_gshared (ValueCollection_t2362216369 * __this, int64_t ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3186695692_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Amazon.Runtime.Metric,System.Int64>::System.Collections.Generic.ICollection<TValue>.Clear()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m549208119_MetadataUsageId;
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m549208119_gshared (ValueCollection_t2362216369 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m549208119_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Amazon.Runtime.Metric,System.Int64>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m873160972_gshared (ValueCollection_t2362216369 * __this, int64_t ___item0, const MethodInfo* method)
{
	{
		Dictionary_2_t3659156526 * L_0 = (Dictionary_2_t3659156526 *)__this->get_dictionary_0();
		int64_t L_1 = ___item0;
		NullCheck((Dictionary_2_t3659156526 *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t3659156526 *, int64_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t3659156526 *)L_0, (int64_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Amazon.Runtime.Metric,System.Int64>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3891119087_MetadataUsageId;
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3891119087_gshared (ValueCollection_t2362216369 * __this, int64_t ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3891119087_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<Amazon.Runtime.Metric,System.Int64>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3277928129_gshared (ValueCollection_t2362216369 * __this, const MethodInfo* method)
{
	{
		NullCheck((ValueCollection_t2362216369 *)__this);
		Enumerator_t1050721994  L_0 = ((  Enumerator_t1050721994  (*) (ValueCollection_t2362216369 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((ValueCollection_t2362216369 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t1050721994  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Amazon.Runtime.Metric,System.Int64>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m3003853841_gshared (ValueCollection_t2362216369 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	Int64U5BU5D_t717125112* V_0 = NULL;
	{
		Il2CppArray * L_0 = ___array0;
		V_0 = (Int64U5BU5D_t717125112*)((Int64U5BU5D_t717125112*)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3)));
		Int64U5BU5D_t717125112* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		Int64U5BU5D_t717125112* L_2 = V_0;
		int32_t L_3 = ___index1;
		NullCheck((ValueCollection_t2362216369 *)__this);
		((  void (*) (ValueCollection_t2362216369 *, Int64U5BU5D_t717125112*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((ValueCollection_t2362216369 *)__this, (Int64U5BU5D_t717125112*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}

IL_0016:
	{
		Dictionary_2_t3659156526 * L_4 = (Dictionary_2_t3659156526 *)__this->get_dictionary_0();
		Il2CppArray * L_5 = ___array0;
		int32_t L_6 = ___index1;
		NullCheck((Dictionary_2_t3659156526 *)L_4);
		((  void (*) (Dictionary_2_t3659156526 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t3659156526 *)L_4, (Il2CppArray *)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t3659156526 * L_7 = (Dictionary_2_t3659156526 *)__this->get_dictionary_0();
		Il2CppArray * L_8 = ___array0;
		int32_t L_9 = ___index1;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t1273696444 * L_11 = (Transform_1_t1273696444 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t1273696444 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_11, (Il2CppObject *)NULL, (IntPtr_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t3659156526 *)L_7);
		((  void (*) (Dictionary_2_t3659156526 *, Il2CppArray *, int32_t, Transform_1_t1273696444 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Dictionary_2_t3659156526 *)L_7, (Il2CppArray *)L_8, (int32_t)L_9, (Transform_1_t1273696444 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<Amazon.Runtime.Metric,System.Int64>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m891744880_gshared (ValueCollection_t2362216369 * __this, const MethodInfo* method)
{
	{
		NullCheck((ValueCollection_t2362216369 *)__this);
		Enumerator_t1050721994  L_0 = ((  Enumerator_t1050721994  (*) (ValueCollection_t2362216369 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((ValueCollection_t2362216369 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t1050721994  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Amazon.Runtime.Metric,System.Int64>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2598728811_gshared (ValueCollection_t2362216369 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Amazon.Runtime.Metric,System.Int64>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m307686685_gshared (ValueCollection_t2362216369 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<Amazon.Runtime.Metric,System.Int64>::System.Collections.ICollection.get_SyncRoot()
extern Il2CppClass* ICollection_t91669223_il2cpp_TypeInfo_var;
extern const uint32_t ValueCollection_System_Collections_ICollection_get_SyncRoot_m2981050245_MetadataUsageId;
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m2981050245_gshared (ValueCollection_t2362216369 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ValueCollection_System_Collections_ICollection_get_SyncRoot_m2981050245_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t3659156526 * L_0 = (Dictionary_2_t3659156526 *)__this->get_dictionary_0();
		NullCheck((Il2CppObject *)L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t91669223_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Amazon.Runtime.Metric,System.Int64>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m2365403327_gshared (ValueCollection_t2362216369 * __this, Int64U5BU5D_t717125112* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		Dictionary_2_t3659156526 * L_0 = (Dictionary_2_t3659156526 *)__this->get_dictionary_0();
		Int64U5BU5D_t717125112* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Dictionary_2_t3659156526 *)L_0);
		((  void (*) (Dictionary_2_t3659156526 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t3659156526 *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t3659156526 * L_3 = (Dictionary_2_t3659156526 *)__this->get_dictionary_0();
		Int64U5BU5D_t717125112* L_4 = ___array0;
		int32_t L_5 = ___index1;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t1273696444 * L_7 = (Transform_1_t1273696444 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t1273696444 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_7, (Il2CppObject *)NULL, (IntPtr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t3659156526 *)L_3);
		((  void (*) (Dictionary_2_t3659156526 *, Int64U5BU5D_t717125112*, int32_t, Transform_1_t1273696444 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((Dictionary_2_t3659156526 *)L_3, (Int64U5BU5D_t717125112*)L_4, (int32_t)L_5, (Transform_1_t1273696444 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return;
	}
}
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<Amazon.Runtime.Metric,System.Int64>::GetEnumerator()
extern "C"  Enumerator_t1050721994  ValueCollection_GetEnumerator_m3659315370_gshared (ValueCollection_t2362216369 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t3659156526 * L_0 = (Dictionary_2_t3659156526 *)__this->get_dictionary_0();
		Enumerator_t1050721994  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Enumerator__ctor_m236173709(&L_1, (Dictionary_2_t3659156526 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<Amazon.Runtime.Metric,System.Int64>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m1620867361_gshared (ValueCollection_t2362216369 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t3659156526 * L_0 = (Dictionary_2_t3659156526 *)__this->get_dictionary_0();
		NullCheck((Dictionary_2_t3659156526 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Dictionary_2_t3659156526 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t3659156526 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Amazon.Runtime.Metric,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3629755714;
extern const uint32_t ValueCollection__ctor_m983902812_MetadataUsageId;
extern "C"  void ValueCollection__ctor_m983902812_gshared (ValueCollection_t4142587627 * __this, Dictionary_2_t1144560488 * ___dictionary0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ValueCollection__ctor_m983902812_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t1144560488 * L_0 = ___dictionary0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral3629755714, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		Dictionary_2_t1144560488 * L_2 = ___dictionary0;
		__this->set_dictionary_0(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Amazon.Runtime.Metric,System.Object>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3409785142_MetadataUsageId;
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3409785142_gshared (ValueCollection_t4142587627 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3409785142_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Amazon.Runtime.Metric,System.Object>::System.Collections.Generic.ICollection<TValue>.Clear()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2769354283_MetadataUsageId;
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2769354283_gshared (ValueCollection_t4142587627 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2769354283_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Amazon.Runtime.Metric,System.Object>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2796363082_gshared (ValueCollection_t4142587627 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		Dictionary_2_t1144560488 * L_0 = (Dictionary_2_t1144560488 *)__this->get_dictionary_0();
		Il2CppObject * L_1 = ___item0;
		NullCheck((Dictionary_2_t1144560488 *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t1144560488 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t1144560488 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Amazon.Runtime.Metric,System.Object>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2318512635_MetadataUsageId;
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2318512635_gshared (ValueCollection_t4142587627 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2318512635_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<Amazon.Runtime.Metric,System.Object>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2423026497_gshared (ValueCollection_t4142587627 * __this, const MethodInfo* method)
{
	{
		NullCheck((ValueCollection_t4142587627 *)__this);
		Enumerator_t2831093252  L_0 = ((  Enumerator_t2831093252  (*) (ValueCollection_t4142587627 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((ValueCollection_t4142587627 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t2831093252  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Amazon.Runtime.Metric,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m690507645_gshared (ValueCollection_t4142587627 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	ObjectU5BU5D_t3614634134* V_0 = NULL;
	{
		Il2CppArray * L_0 = ___array0;
		V_0 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3)));
		ObjectU5BU5D_t3614634134* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_2 = V_0;
		int32_t L_3 = ___index1;
		NullCheck((ValueCollection_t4142587627 *)__this);
		((  void (*) (ValueCollection_t4142587627 *, ObjectU5BU5D_t3614634134*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((ValueCollection_t4142587627 *)__this, (ObjectU5BU5D_t3614634134*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}

IL_0016:
	{
		Dictionary_2_t1144560488 * L_4 = (Dictionary_2_t1144560488 *)__this->get_dictionary_0();
		Il2CppArray * L_5 = ___array0;
		int32_t L_6 = ___index1;
		NullCheck((Dictionary_2_t1144560488 *)L_4);
		((  void (*) (Dictionary_2_t1144560488 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t1144560488 *)L_4, (Il2CppArray *)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t1144560488 * L_7 = (Dictionary_2_t1144560488 *)__this->get_dictionary_0();
		Il2CppArray * L_8 = ___array0;
		int32_t L_9 = ___index1;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t1656609428 * L_11 = (Transform_1_t1656609428 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t1656609428 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_11, (Il2CppObject *)NULL, (IntPtr_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t1144560488 *)L_7);
		((  void (*) (Dictionary_2_t1144560488 *, Il2CppArray *, int32_t, Transform_1_t1656609428 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Dictionary_2_t1144560488 *)L_7, (Il2CppArray *)L_8, (int32_t)L_9, (Transform_1_t1656609428 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<Amazon.Runtime.Metric,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2267909886_gshared (ValueCollection_t4142587627 * __this, const MethodInfo* method)
{
	{
		NullCheck((ValueCollection_t4142587627 *)__this);
		Enumerator_t2831093252  L_0 = ((  Enumerator_t2831093252  (*) (ValueCollection_t4142587627 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((ValueCollection_t4142587627 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t2831093252  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Amazon.Runtime.Metric,System.Object>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3813706855_gshared (ValueCollection_t4142587627 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Amazon.Runtime.Metric,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m467693041_gshared (ValueCollection_t4142587627 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<Amazon.Runtime.Metric,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern Il2CppClass* ICollection_t91669223_il2cpp_TypeInfo_var;
extern const uint32_t ValueCollection_System_Collections_ICollection_get_SyncRoot_m3529335277_MetadataUsageId;
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m3529335277_gshared (ValueCollection_t4142587627 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ValueCollection_System_Collections_ICollection_get_SyncRoot_m3529335277_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t1144560488 * L_0 = (Dictionary_2_t1144560488 *)__this->get_dictionary_0();
		NullCheck((Il2CppObject *)L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t91669223_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Amazon.Runtime.Metric,System.Object>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m2739320779_gshared (ValueCollection_t4142587627 * __this, ObjectU5BU5D_t3614634134* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		Dictionary_2_t1144560488 * L_0 = (Dictionary_2_t1144560488 *)__this->get_dictionary_0();
		ObjectU5BU5D_t3614634134* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Dictionary_2_t1144560488 *)L_0);
		((  void (*) (Dictionary_2_t1144560488 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t1144560488 *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t1144560488 * L_3 = (Dictionary_2_t1144560488 *)__this->get_dictionary_0();
		ObjectU5BU5D_t3614634134* L_4 = ___array0;
		int32_t L_5 = ___index1;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t1656609428 * L_7 = (Transform_1_t1656609428 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t1656609428 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_7, (Il2CppObject *)NULL, (IntPtr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t1144560488 *)L_3);
		((  void (*) (Dictionary_2_t1144560488 *, ObjectU5BU5D_t3614634134*, int32_t, Transform_1_t1656609428 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((Dictionary_2_t1144560488 *)L_3, (ObjectU5BU5D_t3614634134*)L_4, (int32_t)L_5, (Transform_1_t1656609428 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return;
	}
}
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<Amazon.Runtime.Metric,System.Object>::GetEnumerator()
extern "C"  Enumerator_t2831093252  ValueCollection_GetEnumerator_m807736870_gshared (ValueCollection_t4142587627 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t1144560488 * L_0 = (Dictionary_2_t1144560488 *)__this->get_dictionary_0();
		Enumerator_t2831093252  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Enumerator__ctor_m18447153(&L_1, (Dictionary_2_t1144560488 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<Amazon.Runtime.Metric,System.Object>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m3120365865_gshared (ValueCollection_t4142587627 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t1144560488 * L_0 = (Dictionary_2_t1144560488 *)__this->get_dictionary_0();
		NullCheck((Dictionary_2_t1144560488 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Dictionary_2_t1144560488 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t1144560488 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3629755714;
extern const uint32_t ValueCollection__ctor_m1016212932_MetadataUsageId;
extern "C"  void ValueCollection__ctor_m1016212932_gshared (ValueCollection_t4077730222 * __this, Dictionary_2_t1079703083 * ___dictionary0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ValueCollection__ctor_m1016212932_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t1079703083 * L_0 = ___dictionary0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral3629755714, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		Dictionary_2_t1079703083 * L_2 = ___dictionary0;
		__this->set_dictionary_0(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Int32>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1909364658_MetadataUsageId;
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1909364658_gshared (ValueCollection_t4077730222 * __this, int32_t ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1909364658_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Int32>::System.Collections.Generic.ICollection<TValue>.Clear()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m4184581491_MetadataUsageId;
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m4184581491_gshared (ValueCollection_t4077730222 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m4184581491_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Int32>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m246642050_gshared (ValueCollection_t4077730222 * __this, int32_t ___item0, const MethodInfo* method)
{
	{
		Dictionary_2_t1079703083 * L_0 = (Dictionary_2_t1079703083 *)__this->get_dictionary_0();
		int32_t L_1 = ___item0;
		NullCheck((Dictionary_2_t1079703083 *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t1079703083 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t1079703083 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Int32>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1476685027_MetadataUsageId;
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1476685027_gshared (ValueCollection_t4077730222 * __this, int32_t ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1476685027_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Int32>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m374665661_gshared (ValueCollection_t4077730222 * __this, const MethodInfo* method)
{
	{
		NullCheck((ValueCollection_t4077730222 *)__this);
		Enumerator_t2766235847  L_0 = ((  Enumerator_t2766235847  (*) (ValueCollection_t4077730222 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((ValueCollection_t4077730222 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t2766235847  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m2486821337_gshared (ValueCollection_t4077730222 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	Int32U5BU5D_t3030399641* V_0 = NULL;
	{
		Il2CppArray * L_0 = ___array0;
		V_0 = (Int32U5BU5D_t3030399641*)((Int32U5BU5D_t3030399641*)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3)));
		Int32U5BU5D_t3030399641* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		Int32U5BU5D_t3030399641* L_2 = V_0;
		int32_t L_3 = ___index1;
		NullCheck((ValueCollection_t4077730222 *)__this);
		((  void (*) (ValueCollection_t4077730222 *, Int32U5BU5D_t3030399641*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((ValueCollection_t4077730222 *)__this, (Int32U5BU5D_t3030399641*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}

IL_0016:
	{
		Dictionary_2_t1079703083 * L_4 = (Dictionary_2_t1079703083 *)__this->get_dictionary_0();
		Il2CppArray * L_5 = ___array0;
		int32_t L_6 = ___index1;
		NullCheck((Dictionary_2_t1079703083 *)L_4);
		((  void (*) (Dictionary_2_t1079703083 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t1079703083 *)L_4, (Il2CppArray *)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t1079703083 * L_7 = (Dictionary_2_t1079703083 *)__this->get_dictionary_0();
		Il2CppArray * L_8 = ___array0;
		int32_t L_9 = ___index1;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t2737799006 * L_11 = (Transform_1_t2737799006 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t2737799006 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_11, (Il2CppObject *)NULL, (IntPtr_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t1079703083 *)L_7);
		((  void (*) (Dictionary_2_t1079703083 *, Il2CppArray *, int32_t, Transform_1_t2737799006 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Dictionary_2_t1079703083 *)L_7, (Il2CppArray *)L_8, (int32_t)L_9, (Transform_1_t2737799006 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1020497378_gshared (ValueCollection_t4077730222 * __this, const MethodInfo* method)
{
	{
		NullCheck((ValueCollection_t4077730222 *)__this);
		Enumerator_t2766235847  L_0 = ((  Enumerator_t2766235847  (*) (ValueCollection_t4077730222 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((ValueCollection_t4077730222 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t2766235847  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Int32>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2046100031_gshared (ValueCollection_t4077730222 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2689217077_gshared (ValueCollection_t4077730222 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern Il2CppClass* ICollection_t91669223_il2cpp_TypeInfo_var;
extern const uint32_t ValueCollection_System_Collections_ICollection_get_SyncRoot_m3444889833_MetadataUsageId;
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m3444889833_gshared (ValueCollection_t4077730222 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ValueCollection_System_Collections_ICollection_get_SyncRoot_m3444889833_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t1079703083 * L_0 = (Dictionary_2_t1079703083 *)__this->get_dictionary_0();
		NullCheck((Il2CppObject *)L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t91669223_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Int32>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m2054124627_gshared (ValueCollection_t4077730222 * __this, Int32U5BU5D_t3030399641* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		Dictionary_2_t1079703083 * L_0 = (Dictionary_2_t1079703083 *)__this->get_dictionary_0();
		Int32U5BU5D_t3030399641* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Dictionary_2_t1079703083 *)L_0);
		((  void (*) (Dictionary_2_t1079703083 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t1079703083 *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t1079703083 * L_3 = (Dictionary_2_t1079703083 *)__this->get_dictionary_0();
		Int32U5BU5D_t3030399641* L_4 = ___array0;
		int32_t L_5 = ___index1;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t2737799006 * L_7 = (Transform_1_t2737799006 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t2737799006 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_7, (Il2CppObject *)NULL, (IntPtr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t1079703083 *)L_3);
		((  void (*) (Dictionary_2_t1079703083 *, Int32U5BU5D_t3030399641*, int32_t, Transform_1_t2737799006 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((Dictionary_2_t1079703083 *)L_3, (Int32U5BU5D_t3030399641*)L_4, (int32_t)L_5, (Transform_1_t2737799006 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return;
	}
}
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Int32>::GetEnumerator()
extern "C"  Enumerator_t2766235847  ValueCollection_GetEnumerator_m494496494_gshared (ValueCollection_t4077730222 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t1079703083 * L_0 = (Dictionary_2_t1079703083 *)__this->get_dictionary_0();
		Enumerator_t2766235847  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Enumerator__ctor_m370613445(&L_1, (Dictionary_2_t1079703083 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Int32>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m3077228621_gshared (ValueCollection_t4077730222 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t1079703083 * L_0 = (Dictionary_2_t1079703083 *)__this->get_dictionary_0();
		NullCheck((Dictionary_2_t1079703083 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Dictionary_2_t1079703083 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t1079703083 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3629755714;
extern const uint32_t ValueCollection__ctor_m882866357_MetadataUsageId;
extern "C"  void ValueCollection__ctor_m882866357_gshared (ValueCollection_t400334773 * __this, Dictionary_2_t1697274930 * ___dictionary0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ValueCollection__ctor_m882866357_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t1697274930 * L_0 = ___dictionary0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral3629755714, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		Dictionary_2_t1697274930 * L_2 = ___dictionary0;
		__this->set_dictionary_0(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Object>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1903672223_MetadataUsageId;
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1903672223_gshared (ValueCollection_t400334773 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1903672223_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Object>::System.Collections.Generic.ICollection<TValue>.Clear()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3271993638_MetadataUsageId;
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3271993638_gshared (ValueCollection_t400334773 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3271993638_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Object>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3958350925_gshared (ValueCollection_t400334773 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		Dictionary_2_t1697274930 * L_0 = (Dictionary_2_t1697274930 *)__this->get_dictionary_0();
		Il2CppObject * L_1 = ___item0;
		NullCheck((Dictionary_2_t1697274930 *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t1697274930 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t1697274930 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Object>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m98888100_MetadataUsageId;
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m98888100_gshared (ValueCollection_t400334773 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m98888100_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Object>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1604400448_gshared (ValueCollection_t400334773 * __this, const MethodInfo* method)
{
	{
		NullCheck((ValueCollection_t400334773 *)__this);
		Enumerator_t3383807694  L_0 = ((  Enumerator_t3383807694  (*) (ValueCollection_t400334773 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((ValueCollection_t400334773 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t3383807694  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m2627730402_gshared (ValueCollection_t400334773 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	ObjectU5BU5D_t3614634134* V_0 = NULL;
	{
		Il2CppArray * L_0 = ___array0;
		V_0 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3)));
		ObjectU5BU5D_t3614634134* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_2 = V_0;
		int32_t L_3 = ___index1;
		NullCheck((ValueCollection_t400334773 *)__this);
		((  void (*) (ValueCollection_t400334773 *, ObjectU5BU5D_t3614634134*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((ValueCollection_t400334773 *)__this, (ObjectU5BU5D_t3614634134*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}

IL_0016:
	{
		Dictionary_2_t1697274930 * L_4 = (Dictionary_2_t1697274930 *)__this->get_dictionary_0();
		Il2CppArray * L_5 = ___array0;
		int32_t L_6 = ___index1;
		NullCheck((Dictionary_2_t1697274930 *)L_4);
		((  void (*) (Dictionary_2_t1697274930 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t1697274930 *)L_4, (Il2CppArray *)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t1697274930 * L_7 = (Dictionary_2_t1697274930 *)__this->get_dictionary_0();
		Il2CppArray * L_8 = ___array0;
		int32_t L_9 = ___index1;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t3939605346 * L_11 = (Transform_1_t3939605346 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t3939605346 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_11, (Il2CppObject *)NULL, (IntPtr_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t1697274930 *)L_7);
		((  void (*) (Dictionary_2_t1697274930 *, Il2CppArray *, int32_t, Transform_1_t3939605346 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Dictionary_2_t1697274930 *)L_7, (Il2CppArray *)L_8, (int32_t)L_9, (Transform_1_t3939605346 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1073215119_gshared (ValueCollection_t400334773 * __this, const MethodInfo* method)
{
	{
		NullCheck((ValueCollection_t400334773 *)__this);
		Enumerator_t3383807694  L_0 = ((  Enumerator_t3383807694  (*) (ValueCollection_t400334773 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((ValueCollection_t400334773 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t3383807694  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Object>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1325719984_gshared (ValueCollection_t400334773 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m4041633470_gshared (ValueCollection_t400334773 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern Il2CppClass* ICollection_t91669223_il2cpp_TypeInfo_var;
extern const uint32_t ValueCollection_System_Collections_ICollection_get_SyncRoot_m3927965720_MetadataUsageId;
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m3927965720_gshared (ValueCollection_t400334773 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ValueCollection_System_Collections_ICollection_get_SyncRoot_m3927965720_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t1697274930 * L_0 = (Dictionary_2_t1697274930 *)__this->get_dictionary_0();
		NullCheck((Il2CppObject *)L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t91669223_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Object>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m1460341186_gshared (ValueCollection_t400334773 * __this, ObjectU5BU5D_t3614634134* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		Dictionary_2_t1697274930 * L_0 = (Dictionary_2_t1697274930 *)__this->get_dictionary_0();
		ObjectU5BU5D_t3614634134* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Dictionary_2_t1697274930 *)L_0);
		((  void (*) (Dictionary_2_t1697274930 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t1697274930 *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t1697274930 * L_3 = (Dictionary_2_t1697274930 *)__this->get_dictionary_0();
		ObjectU5BU5D_t3614634134* L_4 = ___array0;
		int32_t L_5 = ___index1;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t3939605346 * L_7 = (Transform_1_t3939605346 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t3939605346 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_7, (Il2CppObject *)NULL, (IntPtr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t1697274930 *)L_3);
		((  void (*) (Dictionary_2_t1697274930 *, ObjectU5BU5D_t3614634134*, int32_t, Transform_1_t3939605346 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((Dictionary_2_t1697274930 *)L_3, (ObjectU5BU5D_t3614634134*)L_4, (int32_t)L_5, (Transform_1_t3939605346 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return;
	}
}
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Object>::GetEnumerator()
extern "C"  Enumerator_t3383807694  ValueCollection_GetEnumerator_m941805197_gshared (ValueCollection_t400334773 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t1697274930 * L_0 = (Dictionary_2_t1697274930 *)__this->get_dictionary_0();
		Enumerator_t3383807694  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Enumerator__ctor_m2988407410(&L_1, (Dictionary_2_t1697274930 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Object>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m90930038_gshared (ValueCollection_t400334773 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t1697274930 * L_0 = (Dictionary_2_t1697274930 *)__this->get_dictionary_0();
		NullCheck((Dictionary_2_t1697274930 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Dictionary_2_t1697274930 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t1697274930 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3629755714;
extern const uint32_t ValueCollection__ctor_m2077882560_MetadataUsageId;
extern "C"  void ValueCollection__ctor_m2077882560_gshared (ValueCollection_t2382027540 * __this, Dictionary_2_t3678967697 * ___dictionary0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ValueCollection__ctor_m2077882560_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t3678967697 * L_0 = ___dictionary0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral3629755714, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		Dictionary_2_t3678967697 * L_2 = ___dictionary0;
		__this->set_dictionary_0(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,System.Object>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m656178_MetadataUsageId;
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m656178_gshared (ValueCollection_t2382027540 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m656178_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,System.Object>::System.Collections.Generic.ICollection<TValue>.Clear()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m979442795_MetadataUsageId;
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m979442795_gshared (ValueCollection_t2382027540 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m979442795_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,System.Object>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m4058548678_gshared (ValueCollection_t2382027540 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		Dictionary_2_t3678967697 * L_0 = (Dictionary_2_t3678967697 *)__this->get_dictionary_0();
		Il2CppObject * L_1 = ___item0;
		NullCheck((Dictionary_2_t3678967697 *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t3678967697 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t3678967697 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,System.Object>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3259492947_MetadataUsageId;
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3259492947_gshared (ValueCollection_t2382027540 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3259492947_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,System.Object>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1223126429_gshared (ValueCollection_t2382027540 * __this, const MethodInfo* method)
{
	{
		NullCheck((ValueCollection_t2382027540 *)__this);
		Enumerator_t1070533165  L_0 = ((  Enumerator_t1070533165  (*) (ValueCollection_t2382027540 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((ValueCollection_t2382027540 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t1070533165  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m3768245709_gshared (ValueCollection_t2382027540 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	ObjectU5BU5D_t3614634134* V_0 = NULL;
	{
		Il2CppArray * L_0 = ___array0;
		V_0 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3)));
		ObjectU5BU5D_t3614634134* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_2 = V_0;
		int32_t L_3 = ___index1;
		NullCheck((ValueCollection_t2382027540 *)__this);
		((  void (*) (ValueCollection_t2382027540 *, ObjectU5BU5D_t3614634134*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((ValueCollection_t2382027540 *)__this, (ObjectU5BU5D_t3614634134*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}

IL_0016:
	{
		Dictionary_2_t3678967697 * L_4 = (Dictionary_2_t3678967697 *)__this->get_dictionary_0();
		Il2CppArray * L_5 = ___array0;
		int32_t L_6 = ___index1;
		NullCheck((Dictionary_2_t3678967697 *)L_4);
		((  void (*) (Dictionary_2_t3678967697 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t3678967697 *)L_4, (Il2CppArray *)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t3678967697 * L_7 = (Dictionary_2_t3678967697 *)__this->get_dictionary_0();
		Il2CppArray * L_8 = ___array0;
		int32_t L_9 = ___index1;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t1996915623 * L_11 = (Transform_1_t1996915623 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t1996915623 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_11, (Il2CppObject *)NULL, (IntPtr_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t3678967697 *)L_7);
		((  void (*) (Dictionary_2_t3678967697 *, Il2CppArray *, int32_t, Transform_1_t1996915623 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Dictionary_2_t3678967697 *)L_7, (Il2CppArray *)L_8, (int32_t)L_9, (Transform_1_t1996915623 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m674376046_gshared (ValueCollection_t2382027540 * __this, const MethodInfo* method)
{
	{
		NullCheck((ValueCollection_t2382027540 *)__this);
		Enumerator_t1070533165  L_0 = ((  Enumerator_t1070533165  (*) (ValueCollection_t2382027540 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((ValueCollection_t2382027540 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t1070533165  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,System.Object>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3628342391_gshared (ValueCollection_t2382027540 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m4219624793_gshared (ValueCollection_t2382027540 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern Il2CppClass* ICollection_t91669223_il2cpp_TypeInfo_var;
extern const uint32_t ValueCollection_System_Collections_ICollection_get_SyncRoot_m722114041_MetadataUsageId;
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m722114041_gshared (ValueCollection_t2382027540 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ValueCollection_System_Collections_ICollection_get_SyncRoot_m722114041_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t3678967697 * L_0 = (Dictionary_2_t3678967697 *)__this->get_dictionary_0();
		NullCheck((Il2CppObject *)L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t91669223_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,System.Object>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m1607943379_gshared (ValueCollection_t2382027540 * __this, ObjectU5BU5D_t3614634134* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		Dictionary_2_t3678967697 * L_0 = (Dictionary_2_t3678967697 *)__this->get_dictionary_0();
		ObjectU5BU5D_t3614634134* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Dictionary_2_t3678967697 *)L_0);
		((  void (*) (Dictionary_2_t3678967697 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t3678967697 *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t3678967697 * L_3 = (Dictionary_2_t3678967697 *)__this->get_dictionary_0();
		ObjectU5BU5D_t3614634134* L_4 = ___array0;
		int32_t L_5 = ___index1;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t1996915623 * L_7 = (Transform_1_t1996915623 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t1996915623 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_7, (Il2CppObject *)NULL, (IntPtr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t3678967697 *)L_3);
		((  void (*) (Dictionary_2_t3678967697 *, ObjectU5BU5D_t3614634134*, int32_t, Transform_1_t1996915623 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((Dictionary_2_t3678967697 *)L_3, (ObjectU5BU5D_t3614634134*)L_4, (int32_t)L_5, (Transform_1_t1996915623 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return;
	}
}
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,System.Object>::GetEnumerator()
extern "C"  Enumerator_t1070533165  ValueCollection_GetEnumerator_m1386936904_gshared (ValueCollection_t2382027540 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t3678967697 * L_0 = (Dictionary_2_t3678967697 *)__this->get_dictionary_0();
		Enumerator_t1070533165  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Enumerator__ctor_m419014865(&L_1, (Dictionary_2_t3678967697 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,System.Object>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m2322833661_gshared (ValueCollection_t2382027540 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t3678967697 * L_0 = (Dictionary_2_t3678967697 *)__this->get_dictionary_0();
		NullCheck((Dictionary_2_t3678967697 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Dictionary_2_t3678967697 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t3678967697 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
