﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>
struct List_1_t146890807;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3976587777.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteKeyReader_K777769675.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3789202636_gshared (Enumerator_t3976587777 * __this, List_1_t146890807 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m3789202636(__this, ___l0, method) ((  void (*) (Enumerator_t3976587777 *, List_1_t146890807 *, const MethodInfo*))Enumerator__ctor_m3789202636_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1134184526_gshared (Enumerator_t3976587777 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1134184526(__this, method) ((  void (*) (Enumerator_t3976587777 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1134184526_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3493405008_gshared (Enumerator_t3976587777 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3493405008(__this, method) ((  Il2CppObject * (*) (Enumerator_t3976587777 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3493405008_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::Dispose()
extern "C"  void Enumerator_Dispose_m2670029411_gshared (Enumerator_t3976587777 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2670029411(__this, method) ((  void (*) (Enumerator_t3976587777 *, const MethodInfo*))Enumerator_Dispose_m2670029411_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::VerifyState()
extern "C"  void Enumerator_VerifyState_m2889214454_gshared (Enumerator_t3976587777 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m2889214454(__this, method) ((  void (*) (Enumerator_t3976587777 *, const MethodInfo*))Enumerator_VerifyState_m2889214454_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3088973430_gshared (Enumerator_t3976587777 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3088973430(__this, method) ((  bool (*) (Enumerator_t3976587777 *, const MethodInfo*))Enumerator_MoveNext_m3088973430_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::get_Current()
extern "C"  KeyInfo_t777769675  Enumerator_get_Current_m294540711_gshared (Enumerator_t3976587777 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m294540711(__this, method) ((  KeyInfo_t777769675  (*) (Enumerator_t3976587777 *, const MethodInfo*))Enumerator_get_Current_m294540711_gshared)(__this, method)
