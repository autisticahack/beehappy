﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.Auth.AWS4Signer/<>c
struct U3CU3Ec_t2266868118;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21701344717.h"

// System.Void Amazon.Runtime.Internal.Auth.AWS4Signer/<>c::.cctor()
extern "C"  void U3CU3Ec__cctor_m1800858940 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Auth.AWS4Signer/<>c::.ctor()
extern "C"  void U3CU3Ec__ctor_m3890615821 (U3CU3Ec_t2266868118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Runtime.Internal.Auth.AWS4Signer/<>c::<CanonicalizeHeaders>b__36_0(System.Collections.Generic.KeyValuePair`2<System.String,System.String>)
extern "C"  String_t* U3CU3Ec_U3CCanonicalizeHeadersU3Eb__36_0_m2062821087 (U3CU3Ec_t2266868118 * __this, KeyValuePair_2_t1701344717  ___kvp0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Runtime.Internal.Auth.AWS4Signer/<>c::<CanonicalizeHeaderNames>b__37_0(System.Collections.Generic.KeyValuePair`2<System.String,System.String>)
extern "C"  String_t* U3CU3Ec_U3CCanonicalizeHeaderNamesU3Eb__37_0_m1243850381 (U3CU3Ec_t2266868118 * __this, KeyValuePair_2_t1701344717  ___kvp0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.Auth.AWS4Signer/<>c::<GetParametersToCanonicalize>b__38_0(System.Collections.Generic.KeyValuePair`2<System.String,System.String>)
extern "C"  bool U3CU3Ec_U3CGetParametersToCanonicalizeU3Eb__38_0_m32737231 (U3CU3Ec_t2266868118 * __this, KeyValuePair_2_t1701344717  ___queryParameter0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
