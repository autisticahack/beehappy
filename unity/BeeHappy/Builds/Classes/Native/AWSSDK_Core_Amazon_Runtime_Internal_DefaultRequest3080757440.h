﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.IDictionary`2<System.String,System.String>
struct IDictionary_2_t1943082916;
// System.Uri
struct Uri_t19570940;
// System.String
struct String_t;
// Amazon.Runtime.AmazonWebServiceRequest
struct AmazonWebServiceRequest_t3384026212;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.IO.Stream
struct Stream_t3255436806;
// Amazon.RegionEndpoint
struct RegionEndpoint_t661522805;
// Amazon.Runtime.Internal.Auth.AWS4SigningResult
struct AWS4SigningResult_t430803065;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.DefaultRequest
struct  DefaultRequest_t3080757440  : public Il2CppObject
{
public:
	// System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.Runtime.Internal.DefaultRequest::parameters
	Il2CppObject* ___parameters_0;
	// System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.Runtime.Internal.DefaultRequest::headers
	Il2CppObject* ___headers_1;
	// System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.Runtime.Internal.DefaultRequest::subResources
	Il2CppObject* ___subResources_2;
	// System.Uri Amazon.Runtime.Internal.DefaultRequest::endpoint
	Uri_t19570940 * ___endpoint_3;
	// System.String Amazon.Runtime.Internal.DefaultRequest::resourcePath
	String_t* ___resourcePath_4;
	// System.String Amazon.Runtime.Internal.DefaultRequest::serviceName
	String_t* ___serviceName_5;
	// Amazon.Runtime.AmazonWebServiceRequest Amazon.Runtime.Internal.DefaultRequest::originalRequest
	AmazonWebServiceRequest_t3384026212 * ___originalRequest_6;
	// System.Byte[] Amazon.Runtime.Internal.DefaultRequest::content
	ByteU5BU5D_t3397334013* ___content_7;
	// System.IO.Stream Amazon.Runtime.Internal.DefaultRequest::contentStream
	Stream_t3255436806 * ___contentStream_8;
	// System.String Amazon.Runtime.Internal.DefaultRequest::contentStreamHash
	String_t* ___contentStreamHash_9;
	// System.String Amazon.Runtime.Internal.DefaultRequest::httpMethod
	String_t* ___httpMethod_10;
	// System.Boolean Amazon.Runtime.Internal.DefaultRequest::useQueryString
	bool ___useQueryString_11;
	// System.String Amazon.Runtime.Internal.DefaultRequest::requestName
	String_t* ___requestName_12;
	// Amazon.RegionEndpoint Amazon.Runtime.Internal.DefaultRequest::alternateRegion
	RegionEndpoint_t661522805 * ___alternateRegion_13;
	// System.Int64 Amazon.Runtime.Internal.DefaultRequest::originalStreamLength
	int64_t ___originalStreamLength_14;
	// System.Boolean Amazon.Runtime.Internal.DefaultRequest::<SetContentFromParameters>k__BackingField
	bool ___U3CSetContentFromParametersU3Ek__BackingField_15;
	// System.Boolean Amazon.Runtime.Internal.DefaultRequest::<Suppress404Exceptions>k__BackingField
	bool ___U3CSuppress404ExceptionsU3Ek__BackingField_16;
	// Amazon.Runtime.Internal.Auth.AWS4SigningResult Amazon.Runtime.Internal.DefaultRequest::<AWS4SignerResult>k__BackingField
	AWS4SigningResult_t430803065 * ___U3CAWS4SignerResultU3Ek__BackingField_17;
	// System.Boolean Amazon.Runtime.Internal.DefaultRequest::<UseChunkEncoding>k__BackingField
	bool ___U3CUseChunkEncodingU3Ek__BackingField_18;
	// System.Boolean Amazon.Runtime.Internal.DefaultRequest::<UseSigV4>k__BackingField
	bool ___U3CUseSigV4U3Ek__BackingField_19;
	// System.String Amazon.Runtime.Internal.DefaultRequest::<AuthenticationRegion>k__BackingField
	String_t* ___U3CAuthenticationRegionU3Ek__BackingField_20;

public:
	inline static int32_t get_offset_of_parameters_0() { return static_cast<int32_t>(offsetof(DefaultRequest_t3080757440, ___parameters_0)); }
	inline Il2CppObject* get_parameters_0() const { return ___parameters_0; }
	inline Il2CppObject** get_address_of_parameters_0() { return &___parameters_0; }
	inline void set_parameters_0(Il2CppObject* value)
	{
		___parameters_0 = value;
		Il2CppCodeGenWriteBarrier(&___parameters_0, value);
	}

	inline static int32_t get_offset_of_headers_1() { return static_cast<int32_t>(offsetof(DefaultRequest_t3080757440, ___headers_1)); }
	inline Il2CppObject* get_headers_1() const { return ___headers_1; }
	inline Il2CppObject** get_address_of_headers_1() { return &___headers_1; }
	inline void set_headers_1(Il2CppObject* value)
	{
		___headers_1 = value;
		Il2CppCodeGenWriteBarrier(&___headers_1, value);
	}

	inline static int32_t get_offset_of_subResources_2() { return static_cast<int32_t>(offsetof(DefaultRequest_t3080757440, ___subResources_2)); }
	inline Il2CppObject* get_subResources_2() const { return ___subResources_2; }
	inline Il2CppObject** get_address_of_subResources_2() { return &___subResources_2; }
	inline void set_subResources_2(Il2CppObject* value)
	{
		___subResources_2 = value;
		Il2CppCodeGenWriteBarrier(&___subResources_2, value);
	}

	inline static int32_t get_offset_of_endpoint_3() { return static_cast<int32_t>(offsetof(DefaultRequest_t3080757440, ___endpoint_3)); }
	inline Uri_t19570940 * get_endpoint_3() const { return ___endpoint_3; }
	inline Uri_t19570940 ** get_address_of_endpoint_3() { return &___endpoint_3; }
	inline void set_endpoint_3(Uri_t19570940 * value)
	{
		___endpoint_3 = value;
		Il2CppCodeGenWriteBarrier(&___endpoint_3, value);
	}

	inline static int32_t get_offset_of_resourcePath_4() { return static_cast<int32_t>(offsetof(DefaultRequest_t3080757440, ___resourcePath_4)); }
	inline String_t* get_resourcePath_4() const { return ___resourcePath_4; }
	inline String_t** get_address_of_resourcePath_4() { return &___resourcePath_4; }
	inline void set_resourcePath_4(String_t* value)
	{
		___resourcePath_4 = value;
		Il2CppCodeGenWriteBarrier(&___resourcePath_4, value);
	}

	inline static int32_t get_offset_of_serviceName_5() { return static_cast<int32_t>(offsetof(DefaultRequest_t3080757440, ___serviceName_5)); }
	inline String_t* get_serviceName_5() const { return ___serviceName_5; }
	inline String_t** get_address_of_serviceName_5() { return &___serviceName_5; }
	inline void set_serviceName_5(String_t* value)
	{
		___serviceName_5 = value;
		Il2CppCodeGenWriteBarrier(&___serviceName_5, value);
	}

	inline static int32_t get_offset_of_originalRequest_6() { return static_cast<int32_t>(offsetof(DefaultRequest_t3080757440, ___originalRequest_6)); }
	inline AmazonWebServiceRequest_t3384026212 * get_originalRequest_6() const { return ___originalRequest_6; }
	inline AmazonWebServiceRequest_t3384026212 ** get_address_of_originalRequest_6() { return &___originalRequest_6; }
	inline void set_originalRequest_6(AmazonWebServiceRequest_t3384026212 * value)
	{
		___originalRequest_6 = value;
		Il2CppCodeGenWriteBarrier(&___originalRequest_6, value);
	}

	inline static int32_t get_offset_of_content_7() { return static_cast<int32_t>(offsetof(DefaultRequest_t3080757440, ___content_7)); }
	inline ByteU5BU5D_t3397334013* get_content_7() const { return ___content_7; }
	inline ByteU5BU5D_t3397334013** get_address_of_content_7() { return &___content_7; }
	inline void set_content_7(ByteU5BU5D_t3397334013* value)
	{
		___content_7 = value;
		Il2CppCodeGenWriteBarrier(&___content_7, value);
	}

	inline static int32_t get_offset_of_contentStream_8() { return static_cast<int32_t>(offsetof(DefaultRequest_t3080757440, ___contentStream_8)); }
	inline Stream_t3255436806 * get_contentStream_8() const { return ___contentStream_8; }
	inline Stream_t3255436806 ** get_address_of_contentStream_8() { return &___contentStream_8; }
	inline void set_contentStream_8(Stream_t3255436806 * value)
	{
		___contentStream_8 = value;
		Il2CppCodeGenWriteBarrier(&___contentStream_8, value);
	}

	inline static int32_t get_offset_of_contentStreamHash_9() { return static_cast<int32_t>(offsetof(DefaultRequest_t3080757440, ___contentStreamHash_9)); }
	inline String_t* get_contentStreamHash_9() const { return ___contentStreamHash_9; }
	inline String_t** get_address_of_contentStreamHash_9() { return &___contentStreamHash_9; }
	inline void set_contentStreamHash_9(String_t* value)
	{
		___contentStreamHash_9 = value;
		Il2CppCodeGenWriteBarrier(&___contentStreamHash_9, value);
	}

	inline static int32_t get_offset_of_httpMethod_10() { return static_cast<int32_t>(offsetof(DefaultRequest_t3080757440, ___httpMethod_10)); }
	inline String_t* get_httpMethod_10() const { return ___httpMethod_10; }
	inline String_t** get_address_of_httpMethod_10() { return &___httpMethod_10; }
	inline void set_httpMethod_10(String_t* value)
	{
		___httpMethod_10 = value;
		Il2CppCodeGenWriteBarrier(&___httpMethod_10, value);
	}

	inline static int32_t get_offset_of_useQueryString_11() { return static_cast<int32_t>(offsetof(DefaultRequest_t3080757440, ___useQueryString_11)); }
	inline bool get_useQueryString_11() const { return ___useQueryString_11; }
	inline bool* get_address_of_useQueryString_11() { return &___useQueryString_11; }
	inline void set_useQueryString_11(bool value)
	{
		___useQueryString_11 = value;
	}

	inline static int32_t get_offset_of_requestName_12() { return static_cast<int32_t>(offsetof(DefaultRequest_t3080757440, ___requestName_12)); }
	inline String_t* get_requestName_12() const { return ___requestName_12; }
	inline String_t** get_address_of_requestName_12() { return &___requestName_12; }
	inline void set_requestName_12(String_t* value)
	{
		___requestName_12 = value;
		Il2CppCodeGenWriteBarrier(&___requestName_12, value);
	}

	inline static int32_t get_offset_of_alternateRegion_13() { return static_cast<int32_t>(offsetof(DefaultRequest_t3080757440, ___alternateRegion_13)); }
	inline RegionEndpoint_t661522805 * get_alternateRegion_13() const { return ___alternateRegion_13; }
	inline RegionEndpoint_t661522805 ** get_address_of_alternateRegion_13() { return &___alternateRegion_13; }
	inline void set_alternateRegion_13(RegionEndpoint_t661522805 * value)
	{
		___alternateRegion_13 = value;
		Il2CppCodeGenWriteBarrier(&___alternateRegion_13, value);
	}

	inline static int32_t get_offset_of_originalStreamLength_14() { return static_cast<int32_t>(offsetof(DefaultRequest_t3080757440, ___originalStreamLength_14)); }
	inline int64_t get_originalStreamLength_14() const { return ___originalStreamLength_14; }
	inline int64_t* get_address_of_originalStreamLength_14() { return &___originalStreamLength_14; }
	inline void set_originalStreamLength_14(int64_t value)
	{
		___originalStreamLength_14 = value;
	}

	inline static int32_t get_offset_of_U3CSetContentFromParametersU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(DefaultRequest_t3080757440, ___U3CSetContentFromParametersU3Ek__BackingField_15)); }
	inline bool get_U3CSetContentFromParametersU3Ek__BackingField_15() const { return ___U3CSetContentFromParametersU3Ek__BackingField_15; }
	inline bool* get_address_of_U3CSetContentFromParametersU3Ek__BackingField_15() { return &___U3CSetContentFromParametersU3Ek__BackingField_15; }
	inline void set_U3CSetContentFromParametersU3Ek__BackingField_15(bool value)
	{
		___U3CSetContentFromParametersU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3CSuppress404ExceptionsU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(DefaultRequest_t3080757440, ___U3CSuppress404ExceptionsU3Ek__BackingField_16)); }
	inline bool get_U3CSuppress404ExceptionsU3Ek__BackingField_16() const { return ___U3CSuppress404ExceptionsU3Ek__BackingField_16; }
	inline bool* get_address_of_U3CSuppress404ExceptionsU3Ek__BackingField_16() { return &___U3CSuppress404ExceptionsU3Ek__BackingField_16; }
	inline void set_U3CSuppress404ExceptionsU3Ek__BackingField_16(bool value)
	{
		___U3CSuppress404ExceptionsU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CAWS4SignerResultU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(DefaultRequest_t3080757440, ___U3CAWS4SignerResultU3Ek__BackingField_17)); }
	inline AWS4SigningResult_t430803065 * get_U3CAWS4SignerResultU3Ek__BackingField_17() const { return ___U3CAWS4SignerResultU3Ek__BackingField_17; }
	inline AWS4SigningResult_t430803065 ** get_address_of_U3CAWS4SignerResultU3Ek__BackingField_17() { return &___U3CAWS4SignerResultU3Ek__BackingField_17; }
	inline void set_U3CAWS4SignerResultU3Ek__BackingField_17(AWS4SigningResult_t430803065 * value)
	{
		___U3CAWS4SignerResultU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3CAWS4SignerResultU3Ek__BackingField_17, value);
	}

	inline static int32_t get_offset_of_U3CUseChunkEncodingU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(DefaultRequest_t3080757440, ___U3CUseChunkEncodingU3Ek__BackingField_18)); }
	inline bool get_U3CUseChunkEncodingU3Ek__BackingField_18() const { return ___U3CUseChunkEncodingU3Ek__BackingField_18; }
	inline bool* get_address_of_U3CUseChunkEncodingU3Ek__BackingField_18() { return &___U3CUseChunkEncodingU3Ek__BackingField_18; }
	inline void set_U3CUseChunkEncodingU3Ek__BackingField_18(bool value)
	{
		___U3CUseChunkEncodingU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_U3CUseSigV4U3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(DefaultRequest_t3080757440, ___U3CUseSigV4U3Ek__BackingField_19)); }
	inline bool get_U3CUseSigV4U3Ek__BackingField_19() const { return ___U3CUseSigV4U3Ek__BackingField_19; }
	inline bool* get_address_of_U3CUseSigV4U3Ek__BackingField_19() { return &___U3CUseSigV4U3Ek__BackingField_19; }
	inline void set_U3CUseSigV4U3Ek__BackingField_19(bool value)
	{
		___U3CUseSigV4U3Ek__BackingField_19 = value;
	}

	inline static int32_t get_offset_of_U3CAuthenticationRegionU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(DefaultRequest_t3080757440, ___U3CAuthenticationRegionU3Ek__BackingField_20)); }
	inline String_t* get_U3CAuthenticationRegionU3Ek__BackingField_20() const { return ___U3CAuthenticationRegionU3Ek__BackingField_20; }
	inline String_t** get_address_of_U3CAuthenticationRegionU3Ek__BackingField_20() { return &___U3CAuthenticationRegionU3Ek__BackingField_20; }
	inline void set_U3CAuthenticationRegionU3Ek__BackingField_20(String_t* value)
	{
		___U3CAuthenticationRegionU3Ek__BackingField_20 = value;
		Il2CppCodeGenWriteBarrier(&___U3CAuthenticationRegionU3Ek__BackingField_20, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
