﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ThirdParty.iOS4Unity.NSNumberFormatter
struct NSNumberFormatter_t3902210636;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr2504060609.h"

// System.Void ThirdParty.iOS4Unity.NSNumberFormatter::.cctor()
extern "C"  void NSNumberFormatter__cctor_m3489081785 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.iOS4Unity.NSNumberFormatter::.ctor(System.IntPtr)
extern "C"  void NSNumberFormatter__ctor_m190852236 (NSNumberFormatter_t3902210636 * __this, IntPtr_t ___handle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
