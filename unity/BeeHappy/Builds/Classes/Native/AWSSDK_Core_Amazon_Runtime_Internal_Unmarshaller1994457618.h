﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AWSSDK_Core_Amazon_Runtime_Internal_PipelineHandle1486422324.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Unmarshaller
struct  Unmarshaller_t1994457618  : public PipelineHandler_t1486422324
{
public:
	// System.Boolean Amazon.Runtime.Internal.Unmarshaller::_supportsResponseLogging
	bool ____supportsResponseLogging_3;

public:
	inline static int32_t get_offset_of__supportsResponseLogging_3() { return static_cast<int32_t>(offsetof(Unmarshaller_t1994457618, ____supportsResponseLogging_3)); }
	inline bool get__supportsResponseLogging_3() const { return ____supportsResponseLogging_3; }
	inline bool* get_address_of__supportsResponseLogging_3() { return &____supportsResponseLogging_3; }
	inline void set__supportsResponseLogging_3(bool value)
	{
		____supportsResponseLogging_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
