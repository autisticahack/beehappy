﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetIdResponseUnmarshaller
struct GetIdResponseUnmarshaller_t2243065850;
// Amazon.Runtime.AmazonWebServiceResponse
struct AmazonWebServiceResponse_t529043356;
// Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext
struct JsonUnmarshallerContext_t456235889;
// Amazon.Runtime.AmazonServiceException
struct AmazonServiceException_t3748559634;
// System.Exception
struct Exception_t1927440687;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_JsonU456235889.h"
#include "mscorlib_System_Exception1927440687.h"
#include "System_System_Net_HttpStatusCode1898409641.h"

// Amazon.Runtime.AmazonWebServiceResponse Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetIdResponseUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern "C"  AmazonWebServiceResponse_t529043356 * GetIdResponseUnmarshaller_Unmarshall_m3860701444 (GetIdResponseUnmarshaller_t2243065850 * __this, JsonUnmarshallerContext_t456235889 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.AmazonServiceException Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetIdResponseUnmarshaller::UnmarshallException(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,System.Exception,System.Net.HttpStatusCode)
extern "C"  AmazonServiceException_t3748559634 * GetIdResponseUnmarshaller_UnmarshallException_m185098930 (GetIdResponseUnmarshaller_t2243065850 * __this, JsonUnmarshallerContext_t456235889 * ___context0, Exception_t1927440687 * ___innerException1, int32_t ___statusCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetIdResponseUnmarshaller Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetIdResponseUnmarshaller::get_Instance()
extern "C"  GetIdResponseUnmarshaller_t2243065850 * GetIdResponseUnmarshaller_get_Instance_m843541898 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetIdResponseUnmarshaller::.ctor()
extern "C"  void GetIdResponseUnmarshaller__ctor_m3528650822 (GetIdResponseUnmarshaller_t2243065850 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetIdResponseUnmarshaller::.cctor()
extern "C"  void GetIdResponseUnmarshaller__cctor_m4123891945 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
