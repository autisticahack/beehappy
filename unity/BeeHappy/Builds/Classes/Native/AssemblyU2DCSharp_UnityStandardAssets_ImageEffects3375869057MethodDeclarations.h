﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityStandardAssets.ImageEffects.NoiseAndScratches
struct NoiseAndScratches_t3375869057;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void UnityStandardAssets.ImageEffects.NoiseAndScratches::.ctor()
extern "C"  void NoiseAndScratches__ctor_m2111453884 (NoiseAndScratches_t3375869057 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.ImageEffects.NoiseAndScratches::Start()
extern "C"  void NoiseAndScratches_Start_m2443812740 (NoiseAndScratches_t3375869057 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material UnityStandardAssets.ImageEffects.NoiseAndScratches::get_material()
extern "C"  Material_t193706927 * NoiseAndScratches_get_material_m3160495361 (NoiseAndScratches_t3375869057 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.ImageEffects.NoiseAndScratches::OnDisable()
extern "C"  void NoiseAndScratches_OnDisable_m1125348713 (NoiseAndScratches_t3375869057 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.ImageEffects.NoiseAndScratches::SanitizeParameters()
extern "C"  void NoiseAndScratches_SanitizeParameters_m2373196241 (NoiseAndScratches_t3375869057 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.ImageEffects.NoiseAndScratches::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void NoiseAndScratches_OnRenderImage_m3266319548 (NoiseAndScratches_t3375869057 * __this, RenderTexture_t2666733923 * ___source0, RenderTexture_t2666733923 * ___destination1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
