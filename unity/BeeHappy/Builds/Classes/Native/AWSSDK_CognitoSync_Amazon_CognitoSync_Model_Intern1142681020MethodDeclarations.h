﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.CognitoSync.Model.Internal.MarshallTransformations.ListRecordsRequestMarshaller
struct ListRecordsRequestMarshaller_t1142681020;
// Amazon.Runtime.Internal.IRequest
struct IRequest_t2400804350;
// Amazon.Runtime.AmazonWebServiceRequest
struct AmazonWebServiceRequest_t3384026212;
// Amazon.CognitoSync.Model.ListRecordsRequest
struct ListRecordsRequest_t3074726983;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_Runtime_AmazonWebServiceRequest3384026212.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_Model_ListRe3074726983.h"

// Amazon.Runtime.Internal.IRequest Amazon.CognitoSync.Model.Internal.MarshallTransformations.ListRecordsRequestMarshaller::Marshall(Amazon.Runtime.AmazonWebServiceRequest)
extern "C"  Il2CppObject * ListRecordsRequestMarshaller_Marshall_m4033017620 (ListRecordsRequestMarshaller_t1142681020 * __this, AmazonWebServiceRequest_t3384026212 * ___input0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.Internal.IRequest Amazon.CognitoSync.Model.Internal.MarshallTransformations.ListRecordsRequestMarshaller::Marshall(Amazon.CognitoSync.Model.ListRecordsRequest)
extern "C"  Il2CppObject * ListRecordsRequestMarshaller_Marshall_m377728134 (ListRecordsRequestMarshaller_t1142681020 * __this, ListRecordsRequest_t3074726983 * ___publicRequest0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Model.Internal.MarshallTransformations.ListRecordsRequestMarshaller::.ctor()
extern "C"  void ListRecordsRequestMarshaller__ctor_m2403072324 (ListRecordsRequestMarshaller_t1142681020 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
