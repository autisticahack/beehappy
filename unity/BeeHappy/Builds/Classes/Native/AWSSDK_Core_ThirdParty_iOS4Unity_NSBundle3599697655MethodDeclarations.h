﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ThirdParty.iOS4Unity.NSBundle
struct NSBundle_t3599697655;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr2504060609.h"

// System.Void ThirdParty.iOS4Unity.NSBundle::.cctor()
extern "C"  void NSBundle__cctor_m4289041184 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.iOS4Unity.NSBundle::.ctor(System.IntPtr)
extern "C"  void NSBundle__ctor_m3696675735 (NSBundle_t3599697655 * __this, IntPtr_t ___handle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ThirdParty.iOS4Unity.NSBundle ThirdParty.iOS4Unity.NSBundle::get_MainBundle()
extern "C"  NSBundle_t3599697655 * NSBundle_get_MainBundle_m3347251356 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
