﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3601534125MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,ThirdParty.Json.LitJson.JsonData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1456656375(__this, ___dictionary0, method) ((  void (*) (Enumerator_t3203088720 *, Dictionary_2_t1883064018 *, const MethodInfo*))Enumerator__ctor_m3742107451_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,ThirdParty.Json.LitJson.JsonData>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m136631480(__this, method) ((  Il2CppObject * (*) (Enumerator_t3203088720 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m229223308_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,ThirdParty.Json.LitJson.JsonData>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1776994822(__this, method) ((  void (*) (Enumerator_t3203088720 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3225937576_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,ThirdParty.Json.LitJson.JsonData>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3360012865(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t3203088720 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m221119093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,ThirdParty.Json.LitJson.JsonData>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1656827502(__this, method) ((  Il2CppObject * (*) (Enumerator_t3203088720 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m467957770_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,ThirdParty.Json.LitJson.JsonData>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m906954956(__this, method) ((  Il2CppObject * (*) (Enumerator_t3203088720 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2325383168_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,ThirdParty.Json.LitJson.JsonData>::MoveNext()
#define Enumerator_MoveNext_m2919719422(__this, method) ((  bool (*) (Enumerator_t3203088720 *, const MethodInfo*))Enumerator_MoveNext_m3349738440_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,ThirdParty.Json.LitJson.JsonData>::get_Current()
#define Enumerator_get_Current_m2369463118(__this, method) ((  KeyValuePair_2_t3935376536  (*) (Enumerator_t3203088720 *, const MethodInfo*))Enumerator_get_Current_m25299632_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,ThirdParty.Json.LitJson.JsonData>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m3042475731(__this, method) ((  String_t* (*) (Enumerator_t3203088720 *, const MethodInfo*))Enumerator_get_CurrentKey_m3839846791_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,ThirdParty.Json.LitJson.JsonData>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m3804773339(__this, method) ((  JsonData_t4263252052 * (*) (Enumerator_t3203088720 *, const MethodInfo*))Enumerator_get_CurrentValue_m402763047_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,ThirdParty.Json.LitJson.JsonData>::Reset()
#define Enumerator_Reset_m1042338361(__this, method) ((  void (*) (Enumerator_t3203088720 *, const MethodInfo*))Enumerator_Reset_m3129803197_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,ThirdParty.Json.LitJson.JsonData>::VerifyState()
#define Enumerator_VerifyState_m3695890318(__this, method) ((  void (*) (Enumerator_t3203088720 *, const MethodInfo*))Enumerator_VerifyState_m262343092_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,ThirdParty.Json.LitJson.JsonData>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m458528246(__this, method) ((  void (*) (Enumerator_t3203088720 *, const MethodInfo*))Enumerator_VerifyCurrent_m1702320752_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,ThirdParty.Json.LitJson.JsonData>::Dispose()
#define Enumerator_Dispose_m2849089643(__this, method) ((  void (*) (Enumerator_t3203088720 *, const MethodInfo*))Enumerator_Dispose_m1905011127_gshared)(__this, method)
