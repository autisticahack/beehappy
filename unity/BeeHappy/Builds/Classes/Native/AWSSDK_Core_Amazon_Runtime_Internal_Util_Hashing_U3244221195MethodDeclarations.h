﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.Util.Hashing/<>c
struct U3CU3Ec_t3244221195;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void Amazon.Runtime.Internal.Util.Hashing/<>c::.cctor()
extern "C"  void U3CU3Ec__cctor_m1700094693 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.Hashing/<>c::.ctor()
extern "C"  void U3CU3Ec__ctor_m675494102 (U3CU3Ec_t3244221195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Amazon.Runtime.Internal.Util.Hashing/<>c::<Hash>b__0_0(System.Object)
extern "C"  int32_t U3CU3Ec_U3CHashU3Eb__0_0_m3008964907 (U3CU3Ec_t3244221195 * __this, Il2CppObject * ___v0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
