﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.AWSConfigs/<>c__DisplayClass89_0
struct U3CU3Ec__DisplayClass89_0_t3765470194;

#include "codegen/il2cpp-codegen.h"

// System.Void Amazon.AWSConfigs/<>c__DisplayClass89_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass89_0__ctor_m3730328895 (U3CU3Ec__DisplayClass89_0_t3765470194 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.AWSConfigs/<>c__DisplayClass89_0::<LoadConfigFromResource>b__0()
extern "C"  void U3CU3Ec__DisplayClass89_0_U3CLoadConfigFromResourceU3Eb__0_m3921710583 (U3CU3Ec__DisplayClass89_0_t3765470194 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
