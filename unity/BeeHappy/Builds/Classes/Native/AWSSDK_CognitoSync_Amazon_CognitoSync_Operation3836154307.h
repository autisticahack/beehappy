﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.CognitoSync.Operation
struct Operation_t3836154307;

#include "AWSSDK_Core_Amazon_Runtime_ConstantClass4000559886.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.CognitoSync.Operation
struct  Operation_t3836154307  : public ConstantClass_t4000559886
{
public:

public:
};

struct Operation_t3836154307_StaticFields
{
public:
	// Amazon.CognitoSync.Operation Amazon.CognitoSync.Operation::Remove
	Operation_t3836154307 * ___Remove_3;
	// Amazon.CognitoSync.Operation Amazon.CognitoSync.Operation::Replace
	Operation_t3836154307 * ___Replace_4;

public:
	inline static int32_t get_offset_of_Remove_3() { return static_cast<int32_t>(offsetof(Operation_t3836154307_StaticFields, ___Remove_3)); }
	inline Operation_t3836154307 * get_Remove_3() const { return ___Remove_3; }
	inline Operation_t3836154307 ** get_address_of_Remove_3() { return &___Remove_3; }
	inline void set_Remove_3(Operation_t3836154307 * value)
	{
		___Remove_3 = value;
		Il2CppCodeGenWriteBarrier(&___Remove_3, value);
	}

	inline static int32_t get_offset_of_Replace_4() { return static_cast<int32_t>(offsetof(Operation_t3836154307_StaticFields, ___Replace_4)); }
	inline Operation_t3836154307 * get_Replace_4() const { return ___Replace_4; }
	inline Operation_t3836154307 ** get_address_of_Replace_4() { return &___Replace_4; }
	inline void set_Replace_4(Operation_t3836154307 * value)
	{
		___Replace_4 = value;
		Il2CppCodeGenWriteBarrier(&___Replace_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
