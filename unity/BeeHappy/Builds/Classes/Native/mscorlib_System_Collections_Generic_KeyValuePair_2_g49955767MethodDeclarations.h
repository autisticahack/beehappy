﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23196873006MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<Amazon.Runtime.Metric,System.Collections.Generic.List`1<Amazon.Runtime.IMetricsTiming>>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m2163059161(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t49955767 *, int32_t, List_1_t3837499352 *, const MethodInfo*))KeyValuePair_2__ctor_m595192313_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<Amazon.Runtime.Metric,System.Collections.Generic.List`1<Amazon.Runtime.IMetricsTiming>>::get_Key()
#define KeyValuePair_2_get_Key_m85562830(__this, method) ((  int32_t (*) (KeyValuePair_2_t49955767 *, const MethodInfo*))KeyValuePair_2_get_Key_m2312610863_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Amazon.Runtime.Metric,System.Collections.Generic.List`1<Amazon.Runtime.IMetricsTiming>>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m2968376810(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t49955767 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m547641270_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<Amazon.Runtime.Metric,System.Collections.Generic.List`1<Amazon.Runtime.IMetricsTiming>>::get_Value()
#define KeyValuePair_2_get_Value_m2680894851(__this, method) ((  List_1_t3837499352 * (*) (KeyValuePair_2_t49955767 *, const MethodInfo*))KeyValuePair_2_get_Value_m3734313679_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Amazon.Runtime.Metric,System.Collections.Generic.List`1<Amazon.Runtime.IMetricsTiming>>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m3199328330(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t49955767 *, List_1_t3837499352 *, const MethodInfo*))KeyValuePair_2_set_Value_m1567047294_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<Amazon.Runtime.Metric,System.Collections.Generic.List`1<Amazon.Runtime.IMetricsTiming>>::ToString()
#define KeyValuePair_2_ToString_m1894987960(__this, method) ((  String_t* (*) (KeyValuePair_2_t49955767 *, const MethodInfo*))KeyValuePair_2_ToString_m3523821758_gshared)(__this, method)
