﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Util_ThreadPoo1583506835MethodDeclarations.h"

// System.Action`1<Q> Amazon.Runtime.Internal.Util.ThreadPoolThrottler`1/ThreadPoolOptions`1<Amazon.Runtime.IAsyncExecutionContext,Amazon.Runtime.IAsyncExecutionContext>::get_Callback()
#define ThreadPoolOptions_1_get_Callback_m3354643085(__this, method) ((  Action_1_t3594144368 * (*) (ThreadPoolOptions_1_t3222238215 *, const MethodInfo*))ThreadPoolOptions_1_get_Callback_m4086324877_gshared)(__this, method)
// System.Void Amazon.Runtime.Internal.Util.ThreadPoolThrottler`1/ThreadPoolOptions`1<Amazon.Runtime.IAsyncExecutionContext,Amazon.Runtime.IAsyncExecutionContext>::set_Callback(System.Action`1<Q>)
#define ThreadPoolOptions_1_set_Callback_m3860114022(__this, ___value0, method) ((  void (*) (ThreadPoolOptions_1_t3222238215 *, Action_1_t3594144368 *, const MethodInfo*))ThreadPoolOptions_1_set_Callback_m57615590_gshared)(__this, ___value0, method)
// System.Action`2<System.Exception,Q> Amazon.Runtime.Internal.Util.ThreadPoolThrottler`1/ThreadPoolOptions`1<Amazon.Runtime.IAsyncExecutionContext,Amazon.Runtime.IAsyncExecutionContext>::get_ErrorCallback()
#define ThreadPoolOptions_1_get_ErrorCallback_m485683510(__this, method) ((  Action_2_t1841170552 * (*) (ThreadPoolOptions_1_t3222238215 *, const MethodInfo*))ThreadPoolOptions_1_get_ErrorCallback_m689805622_gshared)(__this, method)
// System.Void Amazon.Runtime.Internal.Util.ThreadPoolThrottler`1/ThreadPoolOptions`1<Amazon.Runtime.IAsyncExecutionContext,Amazon.Runtime.IAsyncExecutionContext>::set_ErrorCallback(System.Action`2<System.Exception,Q>)
#define ThreadPoolOptions_1_set_ErrorCallback_m2896136937(__this, ___value0, method) ((  void (*) (ThreadPoolOptions_1_t3222238215 *, Action_2_t1841170552 *, const MethodInfo*))ThreadPoolOptions_1_set_ErrorCallback_m4058543465_gshared)(__this, ___value0, method)
// Q Amazon.Runtime.Internal.Util.ThreadPoolThrottler`1/ThreadPoolOptions`1<Amazon.Runtime.IAsyncExecutionContext,Amazon.Runtime.IAsyncExecutionContext>::get_State()
#define ThreadPoolOptions_1_get_State_m3177948949(__this, method) ((  Il2CppObject * (*) (ThreadPoolOptions_1_t3222238215 *, const MethodInfo*))ThreadPoolOptions_1_get_State_m392834197_gshared)(__this, method)
// System.Void Amazon.Runtime.Internal.Util.ThreadPoolThrottler`1/ThreadPoolOptions`1<Amazon.Runtime.IAsyncExecutionContext,Amazon.Runtime.IAsyncExecutionContext>::set_State(Q)
#define ThreadPoolOptions_1_set_State_m936921150(__this, ___value0, method) ((  void (*) (ThreadPoolOptions_1_t3222238215 *, Il2CppObject *, const MethodInfo*))ThreadPoolOptions_1_set_State_m4200571582_gshared)(__this, ___value0, method)
// System.Void Amazon.Runtime.Internal.Util.ThreadPoolThrottler`1/ThreadPoolOptions`1<Amazon.Runtime.IAsyncExecutionContext,Amazon.Runtime.IAsyncExecutionContext>::.ctor()
#define ThreadPoolOptions_1__ctor_m3795381905(__this, method) ((  void (*) (ThreadPoolOptions_1_t3222238215 *, const MethodInfo*))ThreadPoolOptions_1__ctor_m2042521873_gshared)(__this, method)
