﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.Linq.XElement
struct XElement_t553821050;
// System.Xml.Linq.XName
struct XName_t785190363;
// System.Xml.XmlWriter
struct XmlWriter_t1048088568;
// System.Xml.XmlReader
struct XmlReader_t3675626668;
// System.Xml.Linq.XAttribute
struct XAttribute_t3858477518;
// System.Collections.Generic.IEnumerable`1<System.Xml.Linq.XAttribute>
struct IEnumerable_1_t4150604563;
// System.Object
struct Il2CppObject;
// System.Xml.Linq.XNode
struct XNode_t2707504214;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_Linq_System_Xml_Linq_XElement553821050.h"
#include "System_Xml_Linq_System_Xml_Linq_XName785190363.h"
#include "System_Xml_System_Xml_XmlWriter1048088568.h"
#include "System_Xml_System_Xml_XmlReader3675626668.h"
#include "System_Xml_Linq_System_Xml_Linq_XAttribute3858477518.h"
#include "System_Xml_System_Xml_XmlNodeType739504597.h"
#include "System_Xml_Linq_System_Xml_Linq_LoadOptions53198144.h"
#include "mscorlib_System_Object2689449295.h"
#include "System_Xml_Linq_System_Xml_Linq_XNode2707504214.h"

// System.Void System.Xml.Linq.XElement::.ctor(System.Xml.Linq.XElement)
extern "C"  void XElement__ctor_m2448521297 (XElement_t553821050 * __this, XElement_t553821050 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Linq.XElement::.ctor(System.Xml.Linq.XName)
extern "C"  void XElement__ctor_m143752744 (XElement_t553821050 * __this, XName_t785190363 * ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Linq.XElement::.cctor()
extern "C"  void XElement__cctor_m329558096 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Linq.XElement::System.Xml.Serialization.IXmlSerializable.WriteXml(System.Xml.XmlWriter)
extern "C"  void XElement_System_Xml_Serialization_IXmlSerializable_WriteXml_m1858825662 (XElement_t553821050 * __this, XmlWriter_t1048088568 * ___writer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Linq.XElement::System.Xml.Serialization.IXmlSerializable.ReadXml(System.Xml.XmlReader)
extern "C"  void XElement_System_Xml_Serialization_IXmlSerializable_ReadXml_m3210138867 (XElement_t553821050 * __this, XmlReader_t3675626668 * ___reader0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Linq.XAttribute System.Xml.Linq.XElement::get_FirstAttribute()
extern "C"  XAttribute_t3858477518 * XElement_get_FirstAttribute_m3661704431 (XElement_t553821050 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Linq.XElement::set_FirstAttribute(System.Xml.Linq.XAttribute)
extern "C"  void XElement_set_FirstAttribute_m3741633158 (XElement_t553821050 * __this, XAttribute_t3858477518 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Linq.XAttribute System.Xml.Linq.XElement::get_LastAttribute()
extern "C"  XAttribute_t3858477518 * XElement_get_LastAttribute_m85173095 (XElement_t553821050 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Linq.XElement::set_LastAttribute(System.Xml.Linq.XAttribute)
extern "C"  void XElement_set_LastAttribute_m3614769194 (XElement_t553821050 * __this, XAttribute_t3858477518 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Linq.XName System.Xml.Linq.XElement::get_Name()
extern "C"  XName_t785190363 * XElement_get_Name_m1622427113 (XElement_t553821050 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlNodeType System.Xml.Linq.XElement::get_NodeType()
extern "C"  int32_t XElement_get_NodeType_m2052604688 (XElement_t553821050 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Linq.XAttribute System.Xml.Linq.XElement::Attribute(System.Xml.Linq.XName)
extern "C"  XAttribute_t3858477518 * XElement_Attribute_m1640408831 (XElement_t553821050 * __this, XName_t785190363 * ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<System.Xml.Linq.XAttribute> System.Xml.Linq.XElement::Attributes()
extern "C"  Il2CppObject* XElement_Attributes_m3926517386 (XElement_t553821050 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Linq.XElement System.Xml.Linq.XElement::LoadCore(System.Xml.XmlReader,System.Xml.Linq.LoadOptions)
extern "C"  XElement_t553821050 * XElement_LoadCore_m677132477 (Il2CppObject * __this /* static, unused */, XmlReader_t3675626668 * ___r0, int32_t ___options1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Linq.XElement::Save(System.Xml.XmlWriter)
extern "C"  void XElement_Save_m692239662 (XElement_t553821050 * __this, XmlWriter_t1048088568 * ___w0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Linq.XElement::SetAttributeValue(System.Xml.Linq.XName,System.Object)
extern "C"  void XElement_SetAttributeValue_m4226718375 (XElement_t553821050 * __this, XName_t785190363 * ___name0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Linq.XElement::SetAttributeObject(System.Xml.Linq.XAttribute)
extern "C"  void XElement_SetAttributeObject_m819702138 (XElement_t553821050 * __this, XAttribute_t3858477518 * ___a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Linq.XElement::WriteTo(System.Xml.XmlWriter)
extern "C"  void XElement_WriteTo_m4068736895 (XElement_t553821050 * __this, XmlWriter_t1048088568 * ___w0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.Linq.XElement::OnAddingObject(System.Object,System.Boolean,System.Xml.Linq.XNode,System.Boolean)
extern "C"  bool XElement_OnAddingObject_m3270813610 (XElement_t553821050 * __this, Il2CppObject * ___o0, bool ___rejectAttribute1, XNode_t2707504214 * ___refNode2, bool ___addFirst3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
