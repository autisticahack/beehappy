﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// Amazon.Util.LoggingConfig
struct LoggingConfig_t4162907495;
// Amazon.RegionEndpoint
struct RegionEndpoint_t661522805;
// System.ComponentModel.PropertyChangedEventHandler
struct PropertyChangedEventHandler_t3042952059;
// System.Diagnostics.TraceListener
struct TraceListener_t3414949279;
// System.Xml.Linq.XDocument
struct XDocument_t2733326047;
// System.Object
struct Il2CppObject;
// System.Xml.Linq.XElement
struct XElement_t553821050;
// System.Type
struct Type_t;
// System.Collections.IEnumerable
struct IEnumerable_t2911409499;
// System.Collections.Generic.IDictionary`2<System.String,System.Xml.Linq.XElement>
struct IDictionary_2_t467683733;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_TimeSpan3430258949.h"
#include "AWSSDK_Core_Amazon_LoggingOptions2865640765.h"
#include "System_System_ComponentModel_PropertyChangedEventH3042952059.h"
#include "mscorlib_System_String2029220233.h"
#include "AWSSDK_Core_Amazon_AWSConfigs_HttpClientOption4250830711.h"
#include "System_System_Diagnostics_TraceListener3414949279.h"
#include "System_Xml_Linq_System_Xml_Linq_XElement553821050.h"
#include "mscorlib_System_Type1303803226.h"

// System.Boolean Amazon.AWSConfigs::get_CorrectForClockSkew()
extern "C"  bool AWSConfigs_get_CorrectForClockSkew_m520188553 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.TimeSpan Amazon.AWSConfigs::get_ClockOffset()
extern "C"  TimeSpan_t3430258949  AWSConfigs_get_ClockOffset_m3028760786 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.AWSConfigs::set_ClockOffset(System.TimeSpan)
extern "C"  void AWSConfigs_set_ClockOffset_m2877806125 (Il2CppObject * __this /* static, unused */, TimeSpan_t3430258949  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.LoggingOptions Amazon.AWSConfigs::GetLoggingSetting()
extern "C"  int32_t AWSConfigs_GetLoggingSetting_m2490327702 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.AWSConfigs::get_EndpointDefinition()
extern "C"  String_t* AWSConfigs_get_EndpointDefinition_m2039615017 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Util.LoggingConfig Amazon.AWSConfigs::get_LoggingConfig()
extern "C"  LoggingConfig_t4162907495 * AWSConfigs_get_LoggingConfig_m1949071467 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.RegionEndpoint Amazon.AWSConfigs::get_RegionEndpoint()
extern "C"  RegionEndpoint_t661522805 * AWSConfigs_get_RegionEndpoint_m2267951005 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.AWSConfigs::add_PropertyChanged(System.ComponentModel.PropertyChangedEventHandler)
extern "C"  void AWSConfigs_add_PropertyChanged_m859422552 (Il2CppObject * __this /* static, unused */, PropertyChangedEventHandler_t3042952059 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.AWSConfigs::remove_PropertyChanged(System.ComponentModel.PropertyChangedEventHandler)
extern "C"  void AWSConfigs_remove_PropertyChanged_m3873310703 (Il2CppObject * __this /* static, unused */, PropertyChangedEventHandler_t3042952059 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.AWSConfigs::OnPropertyChanged(System.String)
extern "C"  void AWSConfigs_OnPropertyChanged_m3525487265 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.AWSConfigs::GetConfigBool(System.String,System.Boolean)
extern "C"  bool AWSConfigs_GetConfigBool_m910075176 (Il2CppObject * __this /* static, unused */, String_t* ___name0, bool ___defaultValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.AWSConfigs::GetConfig(System.String)
extern "C"  String_t* AWSConfigs_GetConfig_m564124988 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.AWSConfigs/HttpClientOption Amazon.AWSConfigs::get_HttpClient()
extern "C"  int32_t AWSConfigs_get_HttpClient_m406801197 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.AWSConfigs::XmlSectionExists(System.String)
extern "C"  bool AWSConfigs_XmlSectionExists_m257225501 (Il2CppObject * __this /* static, unused */, String_t* ___sectionName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.AWSConfigs::AddTraceListener(System.String,System.Diagnostics.TraceListener)
extern "C"  void AWSConfigs_AddTraceListener_m3738398461 (Il2CppObject * __this /* static, unused */, String_t* ___source0, TraceListener_t3414949279 * ___listener1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Linq.XDocument Amazon.AWSConfigs::LoadConfigFromResource()
extern "C"  XDocument_t2733326047 * AWSConfigs_LoadConfigFromResource_m3314850027 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Amazon.AWSConfigs::GetObject(System.Xml.Linq.XElement,System.String,System.Type)
extern "C"  Il2CppObject * AWSConfigs_GetObject_m3281962810 (Il2CppObject * __this /* static, unused */, XElement_t553821050 * ___rootElement0, String_t* ___propertyName1, Type_t * ___type2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerable Amazon.AWSConfigs::GetList(System.Xml.Linq.XElement,System.Type,System.String)
extern "C"  Il2CppObject * AWSConfigs_GetList_m3269248866 (Il2CppObject * __this /* static, unused */, XElement_t553821050 * ___rootElement0, Type_t * ___listType1, String_t* ___propertyName2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IDictionary`2<System.String,System.Xml.Linq.XElement> Amazon.AWSConfigs::GetUnresolvedElements(System.Xml.Linq.XElement)
extern "C"  Il2CppObject* AWSConfigs_GetUnresolvedElements_m2993778852 (Il2CppObject * __this /* static, unused */, XElement_t553821050 * ___parent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.AWSConfigs::.cctor()
extern "C"  void AWSConfigs__cctor_m2004978692 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
