﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<Amazon.Runtime.Metric,System.Object>
struct ShimEnumerator_t1249685309;
// System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Object>
struct Dictionary_2_t1144560488;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<Amazon.Runtime.Metric,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m96709936_gshared (ShimEnumerator_t1249685309 * __this, Dictionary_2_t1144560488 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m96709936(__this, ___host0, method) ((  void (*) (ShimEnumerator_t1249685309 *, Dictionary_2_t1144560488 *, const MethodInfo*))ShimEnumerator__ctor_m96709936_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<Amazon.Runtime.Metric,System.Object>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m3639386733_gshared (ShimEnumerator_t1249685309 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m3639386733(__this, method) ((  bool (*) (ShimEnumerator_t1249685309 *, const MethodInfo*))ShimEnumerator_MoveNext_m3639386733_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<Amazon.Runtime.Metric,System.Object>::get_Entry()
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m3554919881_gshared (ShimEnumerator_t1249685309 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m3554919881(__this, method) ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t1249685309 *, const MethodInfo*))ShimEnumerator_get_Entry_m3554919881_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Amazon.Runtime.Metric,System.Object>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m2991689478_gshared (ShimEnumerator_t1249685309 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m2991689478(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1249685309 *, const MethodInfo*))ShimEnumerator_get_Key_m2991689478_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Amazon.Runtime.Metric,System.Object>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m3635880764_gshared (ShimEnumerator_t1249685309 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m3635880764(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1249685309 *, const MethodInfo*))ShimEnumerator_get_Value_m3635880764_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Amazon.Runtime.Metric,System.Object>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m731476104_gshared (ShimEnumerator_t1249685309 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m731476104(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1249685309 *, const MethodInfo*))ShimEnumerator_get_Current_m731476104_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<Amazon.Runtime.Metric,System.Object>::Reset()
extern "C"  void ShimEnumerator_Reset_m2042563214_gshared (ShimEnumerator_t1249685309 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m2042563214(__this, method) ((  void (*) (ShimEnumerator_t1249685309 *, const MethodInfo*))ShimEnumerator_Reset_m2042563214_gshared)(__this, method)
