﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ThirdParty.iOS4Unity.NSNotificationCenter
struct NSNotificationCenter_t3366908717;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr2504060609.h"

// System.Void ThirdParty.iOS4Unity.NSNotificationCenter::.cctor()
extern "C"  void NSNotificationCenter__cctor_m1947089158 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.iOS4Unity.NSNotificationCenter::.ctor(System.IntPtr)
extern "C"  void NSNotificationCenter__ctor_m3636442181 (NSNotificationCenter_t3366908717 * __this, IntPtr_t ___handle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
