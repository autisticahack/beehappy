﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.String,ThirdParty.Json.LitJson.JsonData>
struct Dictionary_2_t1883064018;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.Dictionary`2<System.String,Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint>
struct Dictionary_2_t3800020711;
// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint
struct  RegionEndpoint_t1885241449  : public Il2CppObject
{
public:
	// System.String Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint::<SystemName>k__BackingField
	String_t* ___U3CSystemNameU3Ek__BackingField_4;
	// System.String Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint::<DisplayName>k__BackingField
	String_t* ___U3CDisplayNameU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CSystemNameU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(RegionEndpoint_t1885241449, ___U3CSystemNameU3Ek__BackingField_4)); }
	inline String_t* get_U3CSystemNameU3Ek__BackingField_4() const { return ___U3CSystemNameU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CSystemNameU3Ek__BackingField_4() { return &___U3CSystemNameU3Ek__BackingField_4; }
	inline void set_U3CSystemNameU3Ek__BackingField_4(String_t* value)
	{
		___U3CSystemNameU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CSystemNameU3Ek__BackingField_4, value);
	}

	inline static int32_t get_offset_of_U3CDisplayNameU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(RegionEndpoint_t1885241449, ___U3CDisplayNameU3Ek__BackingField_5)); }
	inline String_t* get_U3CDisplayNameU3Ek__BackingField_5() const { return ___U3CDisplayNameU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CDisplayNameU3Ek__BackingField_5() { return &___U3CDisplayNameU3Ek__BackingField_5; }
	inline void set_U3CDisplayNameU3Ek__BackingField_5(String_t* value)
	{
		___U3CDisplayNameU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CDisplayNameU3Ek__BackingField_5, value);
	}
};

struct RegionEndpoint_t1885241449_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,ThirdParty.Json.LitJson.JsonData> Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint::_documentEndpoints
	Dictionary_2_t1883064018 * ____documentEndpoints_0;
	// System.Boolean Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint::loaded
	bool ___loaded_1;
	// System.Object Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint::LOCK_OBJECT
	Il2CppObject * ___LOCK_OBJECT_2;
	// System.Collections.Generic.Dictionary`2<System.String,Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint> Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint::hashBySystemName
	Dictionary_2_t3800020711 * ___hashBySystemName_3;

public:
	inline static int32_t get_offset_of__documentEndpoints_0() { return static_cast<int32_t>(offsetof(RegionEndpoint_t1885241449_StaticFields, ____documentEndpoints_0)); }
	inline Dictionary_2_t1883064018 * get__documentEndpoints_0() const { return ____documentEndpoints_0; }
	inline Dictionary_2_t1883064018 ** get_address_of__documentEndpoints_0() { return &____documentEndpoints_0; }
	inline void set__documentEndpoints_0(Dictionary_2_t1883064018 * value)
	{
		____documentEndpoints_0 = value;
		Il2CppCodeGenWriteBarrier(&____documentEndpoints_0, value);
	}

	inline static int32_t get_offset_of_loaded_1() { return static_cast<int32_t>(offsetof(RegionEndpoint_t1885241449_StaticFields, ___loaded_1)); }
	inline bool get_loaded_1() const { return ___loaded_1; }
	inline bool* get_address_of_loaded_1() { return &___loaded_1; }
	inline void set_loaded_1(bool value)
	{
		___loaded_1 = value;
	}

	inline static int32_t get_offset_of_LOCK_OBJECT_2() { return static_cast<int32_t>(offsetof(RegionEndpoint_t1885241449_StaticFields, ___LOCK_OBJECT_2)); }
	inline Il2CppObject * get_LOCK_OBJECT_2() const { return ___LOCK_OBJECT_2; }
	inline Il2CppObject ** get_address_of_LOCK_OBJECT_2() { return &___LOCK_OBJECT_2; }
	inline void set_LOCK_OBJECT_2(Il2CppObject * value)
	{
		___LOCK_OBJECT_2 = value;
		Il2CppCodeGenWriteBarrier(&___LOCK_OBJECT_2, value);
	}

	inline static int32_t get_offset_of_hashBySystemName_3() { return static_cast<int32_t>(offsetof(RegionEndpoint_t1885241449_StaticFields, ___hashBySystemName_3)); }
	inline Dictionary_2_t3800020711 * get_hashBySystemName_3() const { return ___hashBySystemName_3; }
	inline Dictionary_2_t3800020711 ** get_address_of_hashBySystemName_3() { return &___hashBySystemName_3; }
	inline void set_hashBySystemName_3(Dictionary_2_t3800020711 * value)
	{
		___hashBySystemName_3 = value;
		Il2CppCodeGenWriteBarrier(&___hashBySystemName_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
