﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.Auth.AbstractAWSSigner
struct AbstractAWSSigner_t2114314031;

#include "codegen/il2cpp-codegen.h"

// System.Void Amazon.Runtime.Internal.Auth.AbstractAWSSigner::.ctor()
extern "C"  void AbstractAWSSigner__ctor_m3570633123 (AbstractAWSSigner_t2114314031 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
