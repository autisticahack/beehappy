﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String[]
struct StringU5BU5D_t1642385972;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage/DatasetColumns
struct  DatasetColumns_t3980096245  : public Il2CppObject
{
public:

public:
};

struct DatasetColumns_t3980096245_StaticFields
{
public:
	// System.String[] Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage/DatasetColumns::ALL
	StringU5BU5D_t1642385972* ___ALL_0;
	// System.Int32 Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage/DatasetColumns::IDENTITY_ID_IDX
	int32_t ___IDENTITY_ID_IDX_1;
	// System.Int32 Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage/DatasetColumns::DATASET_NAME_IDX
	int32_t ___DATASET_NAME_IDX_2;
	// System.Int32 Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage/DatasetColumns::CREATION_TIMESTAMP_IDX
	int32_t ___CREATION_TIMESTAMP_IDX_3;
	// System.Int32 Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage/DatasetColumns::LAST_MODIFIED_TIMESTAMP_IDX
	int32_t ___LAST_MODIFIED_TIMESTAMP_IDX_4;
	// System.Int32 Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage/DatasetColumns::LAST_MODIFIED_BY_IDX
	int32_t ___LAST_MODIFIED_BY_IDX_5;
	// System.Int32 Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage/DatasetColumns::STORAGE_SIZE_BYTES_IDX
	int32_t ___STORAGE_SIZE_BYTES_IDX_6;
	// System.Int32 Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage/DatasetColumns::RECORD_COUNT_IDX
	int32_t ___RECORD_COUNT_IDX_7;
	// System.Int32 Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage/DatasetColumns::LAST_SYNC_COUNT_IDX
	int32_t ___LAST_SYNC_COUNT_IDX_8;
	// System.Int32 Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage/DatasetColumns::LAST_SYNC_TIMESTAMP_IDX
	int32_t ___LAST_SYNC_TIMESTAMP_IDX_9;
	// System.Int32 Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage/DatasetColumns::LAST_SYNC_RESULT_IDX
	int32_t ___LAST_SYNC_RESULT_IDX_10;

public:
	inline static int32_t get_offset_of_ALL_0() { return static_cast<int32_t>(offsetof(DatasetColumns_t3980096245_StaticFields, ___ALL_0)); }
	inline StringU5BU5D_t1642385972* get_ALL_0() const { return ___ALL_0; }
	inline StringU5BU5D_t1642385972** get_address_of_ALL_0() { return &___ALL_0; }
	inline void set_ALL_0(StringU5BU5D_t1642385972* value)
	{
		___ALL_0 = value;
		Il2CppCodeGenWriteBarrier(&___ALL_0, value);
	}

	inline static int32_t get_offset_of_IDENTITY_ID_IDX_1() { return static_cast<int32_t>(offsetof(DatasetColumns_t3980096245_StaticFields, ___IDENTITY_ID_IDX_1)); }
	inline int32_t get_IDENTITY_ID_IDX_1() const { return ___IDENTITY_ID_IDX_1; }
	inline int32_t* get_address_of_IDENTITY_ID_IDX_1() { return &___IDENTITY_ID_IDX_1; }
	inline void set_IDENTITY_ID_IDX_1(int32_t value)
	{
		___IDENTITY_ID_IDX_1 = value;
	}

	inline static int32_t get_offset_of_DATASET_NAME_IDX_2() { return static_cast<int32_t>(offsetof(DatasetColumns_t3980096245_StaticFields, ___DATASET_NAME_IDX_2)); }
	inline int32_t get_DATASET_NAME_IDX_2() const { return ___DATASET_NAME_IDX_2; }
	inline int32_t* get_address_of_DATASET_NAME_IDX_2() { return &___DATASET_NAME_IDX_2; }
	inline void set_DATASET_NAME_IDX_2(int32_t value)
	{
		___DATASET_NAME_IDX_2 = value;
	}

	inline static int32_t get_offset_of_CREATION_TIMESTAMP_IDX_3() { return static_cast<int32_t>(offsetof(DatasetColumns_t3980096245_StaticFields, ___CREATION_TIMESTAMP_IDX_3)); }
	inline int32_t get_CREATION_TIMESTAMP_IDX_3() const { return ___CREATION_TIMESTAMP_IDX_3; }
	inline int32_t* get_address_of_CREATION_TIMESTAMP_IDX_3() { return &___CREATION_TIMESTAMP_IDX_3; }
	inline void set_CREATION_TIMESTAMP_IDX_3(int32_t value)
	{
		___CREATION_TIMESTAMP_IDX_3 = value;
	}

	inline static int32_t get_offset_of_LAST_MODIFIED_TIMESTAMP_IDX_4() { return static_cast<int32_t>(offsetof(DatasetColumns_t3980096245_StaticFields, ___LAST_MODIFIED_TIMESTAMP_IDX_4)); }
	inline int32_t get_LAST_MODIFIED_TIMESTAMP_IDX_4() const { return ___LAST_MODIFIED_TIMESTAMP_IDX_4; }
	inline int32_t* get_address_of_LAST_MODIFIED_TIMESTAMP_IDX_4() { return &___LAST_MODIFIED_TIMESTAMP_IDX_4; }
	inline void set_LAST_MODIFIED_TIMESTAMP_IDX_4(int32_t value)
	{
		___LAST_MODIFIED_TIMESTAMP_IDX_4 = value;
	}

	inline static int32_t get_offset_of_LAST_MODIFIED_BY_IDX_5() { return static_cast<int32_t>(offsetof(DatasetColumns_t3980096245_StaticFields, ___LAST_MODIFIED_BY_IDX_5)); }
	inline int32_t get_LAST_MODIFIED_BY_IDX_5() const { return ___LAST_MODIFIED_BY_IDX_5; }
	inline int32_t* get_address_of_LAST_MODIFIED_BY_IDX_5() { return &___LAST_MODIFIED_BY_IDX_5; }
	inline void set_LAST_MODIFIED_BY_IDX_5(int32_t value)
	{
		___LAST_MODIFIED_BY_IDX_5 = value;
	}

	inline static int32_t get_offset_of_STORAGE_SIZE_BYTES_IDX_6() { return static_cast<int32_t>(offsetof(DatasetColumns_t3980096245_StaticFields, ___STORAGE_SIZE_BYTES_IDX_6)); }
	inline int32_t get_STORAGE_SIZE_BYTES_IDX_6() const { return ___STORAGE_SIZE_BYTES_IDX_6; }
	inline int32_t* get_address_of_STORAGE_SIZE_BYTES_IDX_6() { return &___STORAGE_SIZE_BYTES_IDX_6; }
	inline void set_STORAGE_SIZE_BYTES_IDX_6(int32_t value)
	{
		___STORAGE_SIZE_BYTES_IDX_6 = value;
	}

	inline static int32_t get_offset_of_RECORD_COUNT_IDX_7() { return static_cast<int32_t>(offsetof(DatasetColumns_t3980096245_StaticFields, ___RECORD_COUNT_IDX_7)); }
	inline int32_t get_RECORD_COUNT_IDX_7() const { return ___RECORD_COUNT_IDX_7; }
	inline int32_t* get_address_of_RECORD_COUNT_IDX_7() { return &___RECORD_COUNT_IDX_7; }
	inline void set_RECORD_COUNT_IDX_7(int32_t value)
	{
		___RECORD_COUNT_IDX_7 = value;
	}

	inline static int32_t get_offset_of_LAST_SYNC_COUNT_IDX_8() { return static_cast<int32_t>(offsetof(DatasetColumns_t3980096245_StaticFields, ___LAST_SYNC_COUNT_IDX_8)); }
	inline int32_t get_LAST_SYNC_COUNT_IDX_8() const { return ___LAST_SYNC_COUNT_IDX_8; }
	inline int32_t* get_address_of_LAST_SYNC_COUNT_IDX_8() { return &___LAST_SYNC_COUNT_IDX_8; }
	inline void set_LAST_SYNC_COUNT_IDX_8(int32_t value)
	{
		___LAST_SYNC_COUNT_IDX_8 = value;
	}

	inline static int32_t get_offset_of_LAST_SYNC_TIMESTAMP_IDX_9() { return static_cast<int32_t>(offsetof(DatasetColumns_t3980096245_StaticFields, ___LAST_SYNC_TIMESTAMP_IDX_9)); }
	inline int32_t get_LAST_SYNC_TIMESTAMP_IDX_9() const { return ___LAST_SYNC_TIMESTAMP_IDX_9; }
	inline int32_t* get_address_of_LAST_SYNC_TIMESTAMP_IDX_9() { return &___LAST_SYNC_TIMESTAMP_IDX_9; }
	inline void set_LAST_SYNC_TIMESTAMP_IDX_9(int32_t value)
	{
		___LAST_SYNC_TIMESTAMP_IDX_9 = value;
	}

	inline static int32_t get_offset_of_LAST_SYNC_RESULT_IDX_10() { return static_cast<int32_t>(offsetof(DatasetColumns_t3980096245_StaticFields, ___LAST_SYNC_RESULT_IDX_10)); }
	inline int32_t get_LAST_SYNC_RESULT_IDX_10() const { return ___LAST_SYNC_RESULT_IDX_10; }
	inline int32_t* get_address_of_LAST_SYNC_RESULT_IDX_10() { return &___LAST_SYNC_RESULT_IDX_10; }
	inline void set_LAST_SYNC_RESULT_IDX_10(int32_t value)
	{
		___LAST_SYNC_RESULT_IDX_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
