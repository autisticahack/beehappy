﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,System.Int64>
struct KeyCollection_t1847687001;
// System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Int64>
struct Dictionary_2_t3659156526;
// System.Collections.Generic.IEnumerator`1<Amazon.Runtime.Metric>
struct IEnumerator_1_t748964029;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// Amazon.Runtime.Metric[]
struct MetricU5BU5D_t2477685199;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_Runtime_Metric3273440202.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2053692668.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,System.Int64>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m3448972796_gshared (KeyCollection_t1847687001 * __this, Dictionary_2_t3659156526 * ___dictionary0, const MethodInfo* method);
#define KeyCollection__ctor_m3448972796(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t1847687001 *, Dictionary_2_t3659156526 *, const MethodInfo*))KeyCollection__ctor_m3448972796_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,System.Int64>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2652494438_gshared (KeyCollection_t1847687001 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2652494438(__this, ___item0, method) ((  void (*) (KeyCollection_t1847687001 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2652494438_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,System.Int64>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3374680127_gshared (KeyCollection_t1847687001 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3374680127(__this, method) ((  void (*) (KeyCollection_t1847687001 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3374680127_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,System.Int64>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1045149434_gshared (KeyCollection_t1847687001 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1045149434(__this, ___item0, method) ((  bool (*) (KeyCollection_t1847687001 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1045149434_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,System.Int64>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m28084919_gshared (KeyCollection_t1847687001 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m28084919(__this, ___item0, method) ((  bool (*) (KeyCollection_t1847687001 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m28084919_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,System.Int64>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3233431833_gshared (KeyCollection_t1847687001 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3233431833(__this, method) ((  Il2CppObject* (*) (KeyCollection_t1847687001 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3233431833_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,System.Int64>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m1547472073_gshared (KeyCollection_t1847687001 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m1547472073(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1847687001 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m1547472073_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,System.Int64>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2743961514_gshared (KeyCollection_t1847687001 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2743961514(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1847687001 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2743961514_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,System.Int64>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m648743819_gshared (KeyCollection_t1847687001 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m648743819(__this, method) ((  bool (*) (KeyCollection_t1847687001 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m648743819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,System.Int64>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3936441933_gshared (KeyCollection_t1847687001 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3936441933(__this, method) ((  bool (*) (KeyCollection_t1847687001 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3936441933_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,System.Int64>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m2425750661_gshared (KeyCollection_t1847687001 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m2425750661(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1847687001 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m2425750661_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,System.Int64>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m3475369863_gshared (KeyCollection_t1847687001 * __this, MetricU5BU5D_t2477685199* ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_CopyTo_m3475369863(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1847687001 *, MetricU5BU5D_t2477685199*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m3475369863_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,System.Int64>::GetEnumerator()
extern "C"  Enumerator_t2053692668  KeyCollection_GetEnumerator_m1156193322_gshared (KeyCollection_t1847687001 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m1156193322(__this, method) ((  Enumerator_t2053692668  (*) (KeyCollection_t1847687001 *, const MethodInfo*))KeyCollection_GetEnumerator_m1156193322_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,System.Int64>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m1239091985_gshared (KeyCollection_t1847687001 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m1239091985(__this, method) ((  int32_t (*) (KeyCollection_t1847687001 *, const MethodInfo*))KeyCollection_get_Count_m1239091985_gshared)(__this, method)
