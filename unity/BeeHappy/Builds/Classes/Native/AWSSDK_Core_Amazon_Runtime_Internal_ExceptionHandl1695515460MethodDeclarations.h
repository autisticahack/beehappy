﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.ExceptionHandler`1<System.Object>
struct ExceptionHandler_1_t1695515460;
// Amazon.Runtime.Internal.Util.ILogger
struct ILogger_t676853571;
// Amazon.Runtime.IExecutionContext
struct IExecutionContext_t2477130752;
// System.Exception
struct Exception_t1927440687;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1927440687.h"

// Amazon.Runtime.Internal.Util.ILogger Amazon.Runtime.Internal.ExceptionHandler`1<System.Object>::get_Logger()
extern "C"  Il2CppObject * ExceptionHandler_1_get_Logger_m2827722976_gshared (ExceptionHandler_1_t1695515460 * __this, const MethodInfo* method);
#define ExceptionHandler_1_get_Logger_m2827722976(__this, method) ((  Il2CppObject * (*) (ExceptionHandler_1_t1695515460 *, const MethodInfo*))ExceptionHandler_1_get_Logger_m2827722976_gshared)(__this, method)
// System.Void Amazon.Runtime.Internal.ExceptionHandler`1<System.Object>::.ctor(Amazon.Runtime.Internal.Util.ILogger)
extern "C"  void ExceptionHandler_1__ctor_m3680341358_gshared (ExceptionHandler_1_t1695515460 * __this, Il2CppObject * ___logger0, const MethodInfo* method);
#define ExceptionHandler_1__ctor_m3680341358(__this, ___logger0, method) ((  void (*) (ExceptionHandler_1_t1695515460 *, Il2CppObject *, const MethodInfo*))ExceptionHandler_1__ctor_m3680341358_gshared)(__this, ___logger0, method)
// System.Boolean Amazon.Runtime.Internal.ExceptionHandler`1<System.Object>::Handle(Amazon.Runtime.IExecutionContext,System.Exception)
extern "C"  bool ExceptionHandler_1_Handle_m2138797594_gshared (ExceptionHandler_1_t1695515460 * __this, Il2CppObject * ___executionContext0, Exception_t1927440687 * ___exception1, const MethodInfo* method);
#define ExceptionHandler_1_Handle_m2138797594(__this, ___executionContext0, ___exception1, method) ((  bool (*) (ExceptionHandler_1_t1695515460 *, Il2CppObject *, Exception_t1927440687 *, const MethodInfo*))ExceptionHandler_1_Handle_m2138797594_gshared)(__this, ___executionContext0, ___exception1, method)
