﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Util.ICryptoUtil
struct ICryptoUtil_t1471667456;

#include "codegen/il2cpp-codegen.h"

// Amazon.Util.ICryptoUtil Amazon.Util.CryptoUtilFactory::get_CryptoInstance()
extern "C"  Il2CppObject * CryptoUtilFactory_get_CryptoInstance_m224000876 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Util.CryptoUtilFactory::.cctor()
extern "C"  void CryptoUtilFactory__cctor_m4255164417 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
