﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.Runtime.Internal.UnityRequest
struct UnityRequest_t4218620704;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.UnityWebRequestFactory
struct  UnityWebRequestFactory_t752669660  : public Il2CppObject
{
public:
	// Amazon.Runtime.Internal.UnityRequest Amazon.Runtime.Internal.UnityWebRequestFactory::_unityRequest
	UnityRequest_t4218620704 * ____unityRequest_0;

public:
	inline static int32_t get_offset_of__unityRequest_0() { return static_cast<int32_t>(offsetof(UnityWebRequestFactory_t752669660, ____unityRequest_0)); }
	inline UnityRequest_t4218620704 * get__unityRequest_0() const { return ____unityRequest_0; }
	inline UnityRequest_t4218620704 ** get_address_of__unityRequest_0() { return &____unityRequest_0; }
	inline void set__unityRequest_0(UnityRequest_t4218620704 * value)
	{
		____unityRequest_0 = value;
		Il2CppCodeGenWriteBarrier(&____unityRequest_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
