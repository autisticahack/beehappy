﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.SecurityToken.Model.Credentials
struct Credentials_t3554032640;
// Amazon.Runtime.ImmutableCredentials
struct ImmutableCredentials_t282353664;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_DateTime693205669.h"

// Amazon.Runtime.ImmutableCredentials Amazon.SecurityToken.Model.Credentials::GetCredentials()
extern "C"  ImmutableCredentials_t282353664 * Credentials_GetCredentials_m116816754 (Credentials_t3554032640 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.SecurityToken.Model.Credentials::.ctor()
extern "C"  void Credentials__ctor_m280882687 (Credentials_t3554032640 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.SecurityToken.Model.Credentials::get_AccessKeyId()
extern "C"  String_t* Credentials_get_AccessKeyId_m486408405 (Credentials_t3554032640 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.SecurityToken.Model.Credentials::set_AccessKeyId(System.String)
extern "C"  void Credentials_set_AccessKeyId_m658001590 (Credentials_t3554032640 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime Amazon.SecurityToken.Model.Credentials::get_Expiration()
extern "C"  DateTime_t693205669  Credentials_get_Expiration_m963783206 (Credentials_t3554032640 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.SecurityToken.Model.Credentials::set_Expiration(System.DateTime)
extern "C"  void Credentials_set_Expiration_m4276484463 (Credentials_t3554032640 * __this, DateTime_t693205669  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.SecurityToken.Model.Credentials::get_SecretAccessKey()
extern "C"  String_t* Credentials_get_SecretAccessKey_m927403830 (Credentials_t3554032640 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.SecurityToken.Model.Credentials::set_SecretAccessKey(System.String)
extern "C"  void Credentials_set_SecretAccessKey_m1846861829 (Credentials_t3554032640 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.SecurityToken.Model.Credentials::get_SessionToken()
extern "C"  String_t* Credentials_get_SessionToken_m2509135854 (Credentials_t3554032640 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.SecurityToken.Model.Credentials::set_SessionToken(System.String)
extern "C"  void Credentials_set_SessionToken_m37439381 (Credentials_t3554032640 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
