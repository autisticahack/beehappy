﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Predicate_1_gen1132419410MethodDeclarations.h"

// System.Void System.Predicate`1<System.Diagnostics.TraceListener>::.ctor(System.Object,System.IntPtr)
#define Predicate_1__ctor_m629095327(__this, ___object0, ___method1, method) ((  void (*) (Predicate_1_t1857919394 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m2289454599_gshared)(__this, ___object0, ___method1, method)
// System.Boolean System.Predicate`1<System.Diagnostics.TraceListener>::Invoke(T)
#define Predicate_1_Invoke_m3750920271(__this, ___obj0, method) ((  bool (*) (Predicate_1_t1857919394 *, TraceListener_t3414949279 *, const MethodInfo*))Predicate_1_Invoke_m4047721271_gshared)(__this, ___obj0, method)
// System.IAsyncResult System.Predicate`1<System.Diagnostics.TraceListener>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m1487183264(__this, ___obj0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Predicate_1_t1857919394 *, TraceListener_t3414949279 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Predicate_1_BeginInvoke_m3556950370_gshared)(__this, ___obj0, ___callback1, ___object2, method)
// System.Boolean System.Predicate`1<System.Diagnostics.TraceListener>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m2491773721(__this, ___result0, method) ((  bool (*) (Predicate_1_t1857919394 *, Il2CppObject *, const MethodInfo*))Predicate_1_EndInvoke_m3656575065_gshared)(__this, ___result0, method)
