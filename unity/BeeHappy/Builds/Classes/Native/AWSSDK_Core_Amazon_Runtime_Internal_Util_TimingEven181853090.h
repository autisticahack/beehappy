﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.Runtime.Internal.Util.RequestMetrics
struct RequestMetrics_t218029284;

#include "mscorlib_System_Object2689449295.h"
#include "AWSSDK_Core_Amazon_Runtime_Metric3273440202.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Util.TimingEvent
struct  TimingEvent_t181853090  : public Il2CppObject
{
public:
	// Amazon.Runtime.Metric Amazon.Runtime.Internal.Util.TimingEvent::metric
	int32_t ___metric_0;
	// Amazon.Runtime.Internal.Util.RequestMetrics Amazon.Runtime.Internal.Util.TimingEvent::metrics
	RequestMetrics_t218029284 * ___metrics_1;
	// System.Boolean Amazon.Runtime.Internal.Util.TimingEvent::disposed
	bool ___disposed_2;

public:
	inline static int32_t get_offset_of_metric_0() { return static_cast<int32_t>(offsetof(TimingEvent_t181853090, ___metric_0)); }
	inline int32_t get_metric_0() const { return ___metric_0; }
	inline int32_t* get_address_of_metric_0() { return &___metric_0; }
	inline void set_metric_0(int32_t value)
	{
		___metric_0 = value;
	}

	inline static int32_t get_offset_of_metrics_1() { return static_cast<int32_t>(offsetof(TimingEvent_t181853090, ___metrics_1)); }
	inline RequestMetrics_t218029284 * get_metrics_1() const { return ___metrics_1; }
	inline RequestMetrics_t218029284 ** get_address_of_metrics_1() { return &___metrics_1; }
	inline void set_metrics_1(RequestMetrics_t218029284 * value)
	{
		___metrics_1 = value;
		Il2CppCodeGenWriteBarrier(&___metrics_1, value);
	}

	inline static int32_t get_offset_of_disposed_2() { return static_cast<int32_t>(offsetof(TimingEvent_t181853090, ___disposed_2)); }
	inline bool get_disposed_2() const { return ___disposed_2; }
	inline bool* get_address_of_disposed_2() { return &___disposed_2; }
	inline void set_disposed_2(bool value)
	{
		___disposed_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
