﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Xml.XmlNodeType>
struct DefaultComparer_t2361434758;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XmlNodeType739504597.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Xml.XmlNodeType>::.ctor()
extern "C"  void DefaultComparer__ctor_m3765986853_gshared (DefaultComparer_t2361434758 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m3765986853(__this, method) ((  void (*) (DefaultComparer_t2361434758 *, const MethodInfo*))DefaultComparer__ctor_m3765986853_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Xml.XmlNodeType>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m4029637706_gshared (DefaultComparer_t2361434758 * __this, int32_t ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m4029637706(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t2361434758 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m4029637706_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Xml.XmlNodeType>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m968483290_gshared (DefaultComparer_t2361434758 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m968483290(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t2361434758 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m968483290_gshared)(__this, ___x0, ___y1, method)
