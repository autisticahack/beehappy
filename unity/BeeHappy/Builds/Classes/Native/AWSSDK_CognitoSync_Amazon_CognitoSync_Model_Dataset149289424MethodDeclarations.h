﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.CognitoSync.Model.Dataset
struct Dataset_t149289424;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime693205669.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Amazon.CognitoSync.Model.Dataset::set_CreationDate(System.DateTime)
extern "C"  void Dataset_set_CreationDate_m3983411339 (Dataset_t149289424 * __this, DateTime_t693205669  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Model.Dataset::set_DatasetName(System.String)
extern "C"  void Dataset_set_DatasetName_m3899395419 (Dataset_t149289424 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Model.Dataset::set_DataStorage(System.Int64)
extern "C"  void Dataset_set_DataStorage_m1734138947 (Dataset_t149289424 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Model.Dataset::set_IdentityId(System.String)
extern "C"  void Dataset_set_IdentityId_m150176189 (Dataset_t149289424 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Model.Dataset::set_LastModifiedBy(System.String)
extern "C"  void Dataset_set_LastModifiedBy_m1053326698 (Dataset_t149289424 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Model.Dataset::set_LastModifiedDate(System.DateTime)
extern "C"  void Dataset_set_LastModifiedDate_m3184631065 (Dataset_t149289424 * __this, DateTime_t693205669  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Model.Dataset::set_NumRecords(System.Int64)
extern "C"  void Dataset_set_NumRecords_m2805955452 (Dataset_t149289424 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Model.Dataset::.ctor()
extern "C"  void Dataset__ctor_m2162682141 (Dataset_t149289424 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
