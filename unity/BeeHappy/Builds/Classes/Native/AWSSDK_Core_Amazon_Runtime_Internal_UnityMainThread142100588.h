﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.Runtime.IUnityHttpRequest
struct IUnityHttpRequest_t1859903397;
// Amazon.Runtime.Internal.UnityMainThreadDispatcher
struct UnityMainThreadDispatcher_t4098072663;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.UnityMainThreadDispatcher/<>c__DisplayClass7_0
struct  U3CU3Ec__DisplayClass7_0_t142100588  : public Il2CppObject
{
public:
	// Amazon.Runtime.IUnityHttpRequest Amazon.Runtime.Internal.UnityMainThreadDispatcher/<>c__DisplayClass7_0::request
	Il2CppObject * ___request_0;
	// Amazon.Runtime.Internal.UnityMainThreadDispatcher Amazon.Runtime.Internal.UnityMainThreadDispatcher/<>c__DisplayClass7_0::<>4__this
	UnityMainThreadDispatcher_t4098072663 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_request_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_0_t142100588, ___request_0)); }
	inline Il2CppObject * get_request_0() const { return ___request_0; }
	inline Il2CppObject ** get_address_of_request_0() { return &___request_0; }
	inline void set_request_0(Il2CppObject * value)
	{
		___request_0 = value;
		Il2CppCodeGenWriteBarrier(&___request_0, value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_0_t142100588, ___U3CU3E4__this_1)); }
	inline UnityMainThreadDispatcher_t4098072663 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline UnityMainThreadDispatcher_t4098072663 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(UnityMainThreadDispatcher_t4098072663 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E4__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
