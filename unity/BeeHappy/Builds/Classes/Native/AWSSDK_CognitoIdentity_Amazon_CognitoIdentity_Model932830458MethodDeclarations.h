﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.CognitoIdentity.Model.GetCredentialsForIdentityRequest
struct GetCredentialsForIdentityRequest_t932830458;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.String Amazon.CognitoIdentity.Model.GetCredentialsForIdentityRequest::get_CustomRoleArn()
extern "C"  String_t* GetCredentialsForIdentityRequest_get_CustomRoleArn_m2338986275 (GetCredentialsForIdentityRequest_t932830458 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.CognitoIdentity.Model.GetCredentialsForIdentityRequest::IsSetCustomRoleArn()
extern "C"  bool GetCredentialsForIdentityRequest_IsSetCustomRoleArn_m3799822147 (GetCredentialsForIdentityRequest_t932830458 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.CognitoIdentity.Model.GetCredentialsForIdentityRequest::get_IdentityId()
extern "C"  String_t* GetCredentialsForIdentityRequest_get_IdentityId_m2563301616 (GetCredentialsForIdentityRequest_t932830458 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoIdentity.Model.GetCredentialsForIdentityRequest::set_IdentityId(System.String)
extern "C"  void GetCredentialsForIdentityRequest_set_IdentityId_m1467243909 (GetCredentialsForIdentityRequest_t932830458 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.CognitoIdentity.Model.GetCredentialsForIdentityRequest::IsSetIdentityId()
extern "C"  bool GetCredentialsForIdentityRequest_IsSetIdentityId_m3149237072 (GetCredentialsForIdentityRequest_t932830458 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> Amazon.CognitoIdentity.Model.GetCredentialsForIdentityRequest::get_Logins()
extern "C"  Dictionary_2_t3943999495 * GetCredentialsForIdentityRequest_get_Logins_m3877107988 (GetCredentialsForIdentityRequest_t932830458 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoIdentity.Model.GetCredentialsForIdentityRequest::set_Logins(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void GetCredentialsForIdentityRequest_set_Logins_m4103779471 (GetCredentialsForIdentityRequest_t932830458 * __this, Dictionary_2_t3943999495 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.CognitoIdentity.Model.GetCredentialsForIdentityRequest::IsSetLogins()
extern "C"  bool GetCredentialsForIdentityRequest_IsSetLogins_m952199241 (GetCredentialsForIdentityRequest_t932830458 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoIdentity.Model.GetCredentialsForIdentityRequest::.ctor()
extern "C"  void GetCredentialsForIdentityRequest__ctor_m1038175453 (GetCredentialsForIdentityRequest_t932830458 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
