﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.Util.TimingEvent
struct TimingEvent_t181853090;
// Amazon.Runtime.Internal.Util.RequestMetrics
struct RequestMetrics_t218029284;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Util_RequestMet218029284.h"
#include "AWSSDK_Core_Amazon_Runtime_Metric3273440202.h"

// System.Void Amazon.Runtime.Internal.Util.TimingEvent::.ctor(Amazon.Runtime.Internal.Util.RequestMetrics,Amazon.Runtime.Metric)
extern "C"  void TimingEvent__ctor_m682088859 (TimingEvent_t181853090 * __this, RequestMetrics_t218029284 * ___metrics0, int32_t ___metric1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.TimingEvent::Dispose(System.Boolean)
extern "C"  void TimingEvent_Dispose_m1724772968 (TimingEvent_t181853090 * __this, bool ___disposing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.TimingEvent::Dispose()
extern "C"  void TimingEvent_Dispose_m2613406261 (TimingEvent_t181853090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.TimingEvent::Finalize()
extern "C"  void TimingEvent_Finalize_m3651470528 (TimingEvent_t181853090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
