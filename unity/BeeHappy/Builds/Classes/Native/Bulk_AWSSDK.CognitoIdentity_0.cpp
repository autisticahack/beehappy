﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// Amazon.CognitoIdentity.AmazonCognitoIdentityClient
struct AmazonCognitoIdentityClient_t3069350888;
// Amazon.Runtime.AWSCredentials
struct AWSCredentials_t3583921007;
// Amazon.RegionEndpoint
struct RegionEndpoint_t661522805;
// Amazon.CognitoIdentity.AmazonCognitoIdentityConfig
struct AmazonCognitoIdentityConfig_t1124767059;
// Amazon.Runtime.Internal.Auth.AbstractAWSSigner
struct AbstractAWSSigner_t2114314031;
// Amazon.CognitoIdentity.Model.GetCredentialsForIdentityResponse
struct GetCredentialsForIdentityResponse_t2302678766;
// Amazon.CognitoIdentity.Model.GetCredentialsForIdentityRequest
struct GetCredentialsForIdentityRequest_t932830458;
// Amazon.Runtime.Internal.Transform.IMarshaller`2<Amazon.Runtime.Internal.IRequest,Amazon.Runtime.AmazonWebServiceRequest>
struct IMarshaller_2_t3817076711;
// Amazon.Runtime.Internal.Transform.ResponseUnmarshaller
struct ResponseUnmarshaller_t3934041557;
// System.Object
struct Il2CppObject;
// Amazon.CognitoIdentity.Model.GetIdResponse
struct GetIdResponse_t2091118072;
// Amazon.CognitoIdentity.Model.GetIdRequest
struct GetIdRequest_t4078561340;
// Amazon.CognitoIdentity.Model.GetOpenIdTokenResponse
struct GetOpenIdTokenResponse_t955597635;
// Amazon.CognitoIdentity.Model.GetOpenIdTokenRequest
struct GetOpenIdTokenRequest_t2698079901;
// System.String
struct String_t;
// Amazon.CognitoIdentity.AmazonCognitoIdentityException
struct AmazonCognitoIdentityException_t158465174;
// System.Exception
struct Exception_t1927440687;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// Amazon.CognitoIdentity.AmazonCognitoIdentityRequest
struct AmazonCognitoIdentityRequest_t492659996;
// Amazon.CognitoIdentity.CognitoAWSCredentials
struct CognitoAWSCredentials_t2370264792;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;
// Amazon.CognitoIdentity.CognitoAWSCredentials/IdentityState
struct IdentityState_t2011390993;
// System.EventHandler`1<Amazon.CognitoIdentity.CognitoAWSCredentials/IdentityChangedArgs>
struct EventHandler_1_t248708383;
// Amazon.CognitoIdentity.IAmazonCognitoIdentity
struct IAmazonCognitoIdentity_t1261655088;
// Amazon.SecurityToken.IAmazonSecurityTokenService
struct IAmazonSecurityTokenService_t4042936971;
// Amazon.Runtime.RefreshingAWSCredentials/CredentialsRefreshState
struct CredentialsRefreshState_t3294867821;
// Amazon.SecurityToken.Model.Credentials
struct Credentials_t3554032640;
// Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest
struct AssumeRoleWithWebIdentityRequest_t193094215;
// Amazon.Util.Internal.PlatformServices.IApplicationSettings
struct IApplicationSettings_t2849513616;
// Amazon.CognitoIdentity.CognitoAWSCredentials/<>c__DisplayClass59_0
struct U3CU3Ec__DisplayClass59_0_t795329086;
// Amazon.Runtime.AmazonServiceResult`2<Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest,Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityResponse>
struct AmazonServiceResult_2_t3957156559;
// Amazon.CognitoIdentity.CognitoAWSCredentials/IdentityChangedArgs
struct IdentityChangedArgs_t1657401211;
// Amazon.CognitoIdentity.Model.Credentials
struct Credentials_t792472136;
// Amazon.Runtime.ImmutableCredentials
struct ImmutableCredentials_t282353664;
// Amazon.CognitoIdentity.Model.ExternalServiceException
struct ExternalServiceException_t712152635;
// Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.CredentialsUnmarshaller
struct CredentialsUnmarshaller_t3086492122;
// Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext
struct XmlUnmarshallerContext_t1179575220;
// Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext
struct JsonUnmarshallerContext_t456235889;
// Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetCredentialsForIdentityRequestMarshaller
struct GetCredentialsForIdentityRequestMarshaller_t3584137889;
// Amazon.Runtime.Internal.IRequest
struct IRequest_t2400804350;
// Amazon.Runtime.AmazonWebServiceRequest
struct AmazonWebServiceRequest_t3384026212;
// Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetCredentialsForIdentityResponseUnmarshaller
struct GetCredentialsForIdentityResponseUnmarshaller_t2007495598;
// Amazon.Runtime.AmazonWebServiceResponse
struct AmazonWebServiceResponse_t529043356;
// Amazon.Runtime.AmazonServiceException
struct AmazonServiceException_t3748559634;
// Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetIdRequestMarshaller
struct GetIdRequestMarshaller_t3365653223;
// Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetIdResponseUnmarshaller
struct GetIdResponseUnmarshaller_t2243065850;
// Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetOpenIdTokenRequestMarshaller
struct GetOpenIdTokenRequestMarshaller_t1236206204;
// Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetOpenIdTokenResponseUnmarshaller
struct GetOpenIdTokenResponseUnmarshaller_t3130322723;
// Amazon.CognitoIdentity.Model.InternalErrorException
struct InternalErrorException_t1116634264;
// Amazon.CognitoIdentity.Model.InvalidIdentityPoolConfigurationException
struct InvalidIdentityPoolConfigurationException_t50815546;
// Amazon.CognitoIdentity.Model.InvalidParameterException
struct InvalidParameterException_t2482528107;
// Amazon.CognitoIdentity.Model.LimitExceededException
struct LimitExceededException_t3250699605;
// Amazon.CognitoIdentity.Model.NotAuthorizedException
struct NotAuthorizedException_t457234085;
// Amazon.CognitoIdentity.Model.ResourceConflictException
struct ResourceConflictException_t4018382715;
// Amazon.CognitoIdentity.Model.ResourceNotFoundException
struct ResourceNotFoundException_t403010890;
// Amazon.CognitoIdentity.Model.TooManyRequestsException
struct TooManyRequestsException_t3276202790;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AWSSDK_CognitoIdentity_U3CModuleU3E3783534214.h"
#include "AWSSDK_CognitoIdentity_U3CModuleU3E3783534214MethodDeclarations.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Amaz3069350888.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Amaz3069350888MethodDeclarations.h"
#include "AWSSDK_Core_Amazon_Runtime_AWSCredentials3583921007.h"
#include "AWSSDK_Core_Amazon_RegionEndpoint661522805.h"
#include "mscorlib_System_Void1841601450.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Amaz1124767059MethodDeclarations.h"
#include "AWSSDK_Core_Amazon_Runtime_ClientConfig3664713661MethodDeclarations.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Amaz1124767059.h"
#include "AWSSDK_Core_Amazon_Runtime_AmazonServiceClient3583134838MethodDeclarations.h"
#include "AWSSDK_Core_Amazon_Runtime_ClientConfig3664713661.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Auth_AbstractA2114314031.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Auth_AWS4Signe1011449585MethodDeclarations.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Auth_AWS4Signe1011449585.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "AWSSDK_Core_Amazon_Runtime_AmazonServiceClient3583134838.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Model932830458.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Mode2302678766.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Mode3584137889MethodDeclarations.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Mode2007495598MethodDeclarations.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Mode3584137889.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Mode2007495598.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_Resp3934041557.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Mode4078561340.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Mode2091118072.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Mode3365653223MethodDeclarations.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Mode2243065850MethodDeclarations.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Mode3365653223.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Mode2243065850.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Mode2698079901.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Model955597635.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Mode1236206204MethodDeclarations.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Mode3130322723MethodDeclarations.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Mode1236206204.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Mode3130322723.h"
#include "mscorlib_System_String2029220233.h"
#include "AWSSDK_Core_Amazon_Util_Internal_InternalSDKUtils3074264706MethodDeclarations.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Amazo158465174.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Amazo158465174MethodDeclarations.h"
#include "mscorlib_System_Exception1927440687.h"
#include "AWSSDK_Core_Amazon_Runtime_ErrorType1448377524.h"
#include "System_System_Net_HttpStatusCode1898409641.h"
#include "AWSSDK_Core_Amazon_Runtime_AmazonServiceException3748559634MethodDeclarations.h"
#include "mscorlib_System_Runtime_Serialization_Serialization228987430.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Amazo492659996.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Amazo492659996MethodDeclarations.h"
#include "AWSSDK_Core_Amazon_Runtime_AmazonWebServiceRequest3384026212MethodDeclarations.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Cogn2370264792.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Cogn2370264792MethodDeclarations.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Cogn1657401211MethodDeclarations.h"
#include "mscorlib_System_EventHandler_1_gen248708383MethodDeclarations.h"
#include "mscorlib_System_EventHandler_1_gen248708383.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Cogn1657401211.h"
#include "mscorlib_System_StringComparison2376310518.h"
#include "AWSSDK_Core_Amazon_Runtime_RefreshingAWSCredential1767066958MethodDeclarations.h"
#include "AWSSDK_Core_Amazon_Runtime_RefreshingAWSCredential1767066958.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3943999495.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3943999495MethodDeclarations.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Cogn3661410707.h"
#include "mscorlib_System_Threading_Monitor3228523394MethodDeclarations.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Cogn2011390993MethodDeclarations.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Cogn2011390993.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Mode4078561340MethodDeclarations.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Mode2091118072MethodDeclarations.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Model457234085.h"
#include "mscorlib_System_Exception1927440687MethodDeclarations.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Model403010890.h"
#include "mscorlib_System_Delegate3022476291MethodDeclarations.h"
#include "mscorlib_System_Delegate3022476291.h"
#include "AWSSDK_Core_Amazon_Runtime_AnonymousAWSCredentials3877662854MethodDeclarations.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_AmazonSe1241355389MethodDeclarations.h"
#include "AWSSDK_Core_Amazon_Runtime_AnonymousAWSCredentials3877662854.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_AmazonSe1241355389.h"
#include "mscorlib_System_ArgumentNullException628810857MethodDeclarations.h"
#include "mscorlib_System_StringComparer1574862926MethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException628810857.h"
#include "mscorlib_System_StringComparer1574862926.h"
#include "AWSSDK_Core_Amazon_Runtime_RefreshingAWSCredential3294867821.h"
#include "mscorlib_System_Int322071877448.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Model932830458MethodDeclarations.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Mode2302678766MethodDeclarations.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Model792472136MethodDeclarations.h"
#include "AWSSDK_Core_Amazon_Runtime_RefreshingAWSCredential3294867821MethodDeclarations.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Model792472136.h"
#include "AWSSDK_Core_Amazon_Runtime_AWSCredentials3583921007MethodDeclarations.h"
#include "AWSSDK_Core_Amazon_Runtime_ImmutableCredentials282353664.h"
#include "mscorlib_System_DateTime693205669.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Mode2698079901MethodDeclarations.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Model955597635MethodDeclarations.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_Model_Ass193094215MethodDeclarations.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_Model_Cr3554032640MethodDeclarations.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_Model_Ass193094215.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_Model_Cr3554032640.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Cogni795329086MethodDeclarations.h"
#include "mscorlib_System_Threading_AutoResetEvent15112628MethodDeclarations.h"
#include "AWSSDK_Core_Amazon_Runtime_AmazonServiceCallback_2_142122009MethodDeclarations.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Cogni795329086.h"
#include "mscorlib_System_Threading_AutoResetEvent15112628.h"
#include "AWSSDK_Core_Amazon_Runtime_AmazonServiceResult_2_g3957156559.h"
#include "AWSSDK_Core_Amazon_Runtime_AmazonServiceCallback_2_142122009.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AWSSDK_Core_Amazon_Runtime_AsyncOptions558351272.h"
#include "mscorlib_System_Threading_WaitHandle677569169MethodDeclarations.h"
#include "mscorlib_System_Threading_WaitHandle677569169.h"
#include "AWSSDK_Core_Amazon_Util_Internal_PlatformServices_S589839913MethodDeclarations.h"
#include "AWSSDK_Core_Amazon_Util_Internal_PlatformServices_S589839913.h"
#include "AWSSDK_Core_Amazon_Util_Internal_PlatformServices_A788768262.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "mscorlib_System_TimeSpan3430258949MethodDeclarations.h"
#include "mscorlib_System_TimeSpan3430258949.h"
#include "mscorlib_System_Double4078015681.h"
#include "AWSSDK_Core_Amazon_Runtime_AmazonServiceResult_2_g3957156559MethodDeclarations.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_Model_As3931705881MethodDeclarations.h"
#include "mscorlib_System_Threading_EventWaitHandle2091316307MethodDeclarations.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_Model_As3931705881.h"
#include "mscorlib_System_EventArgs3289624707MethodDeclarations.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Cogn3661410707MethodDeclarations.h"
#include "AWSSDK_Core_Amazon_Runtime_ImmutableCredentials282353664MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen3251239280.h"
#include "mscorlib_System_Nullable_1_gen3251239280MethodDeclarations.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Model712152635.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Model712152635MethodDeclarations.h"
#include "AWSSDK_Core_Amazon_Runtime_AmazonWebServiceResponse529043356MethodDeclarations.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Mode3086492122.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Mode3086492122MethodDeclarations.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_XmlU1179575220.h"
#include "mscorlib_System_NotImplementedException2785117854MethodDeclarations.h"
#include "mscorlib_System_NotImplementedException2785117854.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_JsonU456235889.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_JsonU456235889MethodDeclarations.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_Unma1608322995MethodDeclarations.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_Stri3953260147MethodDeclarations.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_Date1723598451MethodDeclarations.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_Stri3953260147.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_Date1723598451.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_Unma1608322995.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_JsonToken2445581255.h"
#include "AWSSDK_Core_Amazon_Runtime_AmazonWebServiceRequest3384026212.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_DefaultRequest3080757440MethodDeclarations.h"
#include "mscorlib_System_Globalization_CultureInfo3500843524MethodDeclarations.h"
#include "mscorlib_System_IO_StringWriter4139609088MethodDeclarations.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_JsonWriter3014444111MethodDeclarations.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_Json4063314926MethodDeclarations.h"
#include "mscorlib_System_Text_Encoding663144255MethodDeclarations.h"
#include "mscorlib_System_IO_StringWriter4139609088.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_JsonWriter3014444111.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_Json4063314926.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En969056901.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21701344717.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_DefaultRequest3080757440.h"
#include "mscorlib_System_Globalization_CultureInfo3500843524.h"
#include "mscorlib_System_IO_TextWriter4027217640.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En969056901MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21701344717MethodDeclarations.h"
#include "mscorlib_System_Text_Encoding663144255.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Byte3683104436.h"
#include "AWSSDK_Core_Amazon_Runtime_AmazonWebServiceResponse529043356.h"
#include "AWSSDK_Core_Amazon_Runtime_AmazonServiceException3748559634.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_Json4149926919MethodDeclarations.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_ErrorResponse3502566035MethodDeclarations.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Mode1116634264MethodDeclarations.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Model_50815546MethodDeclarations.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Mode2482528107MethodDeclarations.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Model457234085MethodDeclarations.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Mode4018382715MethodDeclarations.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Model403010890MethodDeclarations.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Mode3276202790MethodDeclarations.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_ErrorResponse3502566035.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_Json4149926919.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Mode1116634264.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Model_50815546.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Mode2482528107.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Mode4018382715.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Mode3276202790.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_Json2685947887MethodDeclarations.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Mode3250699605MethodDeclarations.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Mode3250699605.h"

// !!1 Amazon.Runtime.AmazonServiceClient::Invoke<System.Object,System.Object>(!!0,Amazon.Runtime.Internal.Transform.IMarshaller`2<Amazon.Runtime.Internal.IRequest,Amazon.Runtime.AmazonWebServiceRequest>,Amazon.Runtime.Internal.Transform.ResponseUnmarshaller)
extern "C"  Il2CppObject * AmazonServiceClient_Invoke_TisIl2CppObject_TisIl2CppObject_m4285940291_gshared (AmazonServiceClient_t3583134838 * __this, Il2CppObject * p0, Il2CppObject* p1, ResponseUnmarshaller_t3934041557 * p2, const MethodInfo* method);
#define AmazonServiceClient_Invoke_TisIl2CppObject_TisIl2CppObject_m4285940291(__this, p0, p1, p2, method) ((  Il2CppObject * (*) (AmazonServiceClient_t3583134838 *, Il2CppObject *, Il2CppObject*, ResponseUnmarshaller_t3934041557 *, const MethodInfo*))AmazonServiceClient_Invoke_TisIl2CppObject_TisIl2CppObject_m4285940291_gshared)(__this, p0, p1, p2, method)
// !!1 Amazon.Runtime.AmazonServiceClient::Invoke<Amazon.CognitoIdentity.Model.GetCredentialsForIdentityRequest,Amazon.CognitoIdentity.Model.GetCredentialsForIdentityResponse>(!!0,Amazon.Runtime.Internal.Transform.IMarshaller`2<Amazon.Runtime.Internal.IRequest,Amazon.Runtime.AmazonWebServiceRequest>,Amazon.Runtime.Internal.Transform.ResponseUnmarshaller)
#define AmazonServiceClient_Invoke_TisGetCredentialsForIdentityRequest_t932830458_TisGetCredentialsForIdentityResponse_t2302678766_m557719389(__this, p0, p1, p2, method) ((  GetCredentialsForIdentityResponse_t2302678766 * (*) (AmazonServiceClient_t3583134838 *, GetCredentialsForIdentityRequest_t932830458 *, Il2CppObject*, ResponseUnmarshaller_t3934041557 *, const MethodInfo*))AmazonServiceClient_Invoke_TisIl2CppObject_TisIl2CppObject_m4285940291_gshared)(__this, p0, p1, p2, method)
// !!1 Amazon.Runtime.AmazonServiceClient::Invoke<Amazon.CognitoIdentity.Model.GetIdRequest,Amazon.CognitoIdentity.Model.GetIdResponse>(!!0,Amazon.Runtime.Internal.Transform.IMarshaller`2<Amazon.Runtime.Internal.IRequest,Amazon.Runtime.AmazonWebServiceRequest>,Amazon.Runtime.Internal.Transform.ResponseUnmarshaller)
#define AmazonServiceClient_Invoke_TisGetIdRequest_t4078561340_TisGetIdResponse_t2091118072_m991712797(__this, p0, p1, p2, method) ((  GetIdResponse_t2091118072 * (*) (AmazonServiceClient_t3583134838 *, GetIdRequest_t4078561340 *, Il2CppObject*, ResponseUnmarshaller_t3934041557 *, const MethodInfo*))AmazonServiceClient_Invoke_TisIl2CppObject_TisIl2CppObject_m4285940291_gshared)(__this, p0, p1, p2, method)
// !!1 Amazon.Runtime.AmazonServiceClient::Invoke<Amazon.CognitoIdentity.Model.GetOpenIdTokenRequest,Amazon.CognitoIdentity.Model.GetOpenIdTokenResponse>(!!0,Amazon.Runtime.Internal.Transform.IMarshaller`2<Amazon.Runtime.Internal.IRequest,Amazon.Runtime.AmazonWebServiceRequest>,Amazon.Runtime.Internal.Transform.ResponseUnmarshaller)
#define AmazonServiceClient_Invoke_TisGetOpenIdTokenRequest_t2698079901_TisGetOpenIdTokenResponse_t955597635_m3565345917(__this, p0, p1, p2, method) ((  GetOpenIdTokenResponse_t955597635 * (*) (AmazonServiceClient_t3583134838 *, GetOpenIdTokenRequest_t2698079901 *, Il2CppObject*, ResponseUnmarshaller_t3934041557 *, const MethodInfo*))AmazonServiceClient_Invoke_TisIl2CppObject_TisIl2CppObject_m4285940291_gshared)(__this, p0, p1, p2, method)
// !!0 Amazon.Util.Internal.PlatformServices.ServiceFactory::GetService<System.Object>()
extern "C"  Il2CppObject * ServiceFactory_GetService_TisIl2CppObject_m1215150116_gshared (ServiceFactory_t589839913 * __this, const MethodInfo* method);
#define ServiceFactory_GetService_TisIl2CppObject_m1215150116(__this, method) ((  Il2CppObject * (*) (ServiceFactory_t589839913 *, const MethodInfo*))ServiceFactory_GetService_TisIl2CppObject_m1215150116_gshared)(__this, method)
// !!0 Amazon.Util.Internal.PlatformServices.ServiceFactory::GetService<Amazon.Util.Internal.PlatformServices.IApplicationSettings>()
#define ServiceFactory_GetService_TisIApplicationSettings_t2849513616_m1342956672(__this, method) ((  Il2CppObject * (*) (ServiceFactory_t589839913 *, const MethodInfo*))ServiceFactory_GetService_TisIl2CppObject_m1215150116_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Amazon.CognitoIdentity.AmazonCognitoIdentityClient::.ctor(Amazon.Runtime.AWSCredentials,Amazon.RegionEndpoint)
extern Il2CppClass* AmazonCognitoIdentityConfig_t1124767059_il2cpp_TypeInfo_var;
extern const uint32_t AmazonCognitoIdentityClient__ctor_m3549263274_MetadataUsageId;
extern "C"  void AmazonCognitoIdentityClient__ctor_m3549263274 (AmazonCognitoIdentityClient_t3069350888 * __this, AWSCredentials_t3583921007 * ___credentials0, RegionEndpoint_t661522805 * ___region1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AmazonCognitoIdentityClient__ctor_m3549263274_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AWSCredentials_t3583921007 * L_0 = ___credentials0;
		AmazonCognitoIdentityConfig_t1124767059 * L_1 = (AmazonCognitoIdentityConfig_t1124767059 *)il2cpp_codegen_object_new(AmazonCognitoIdentityConfig_t1124767059_il2cpp_TypeInfo_var);
		AmazonCognitoIdentityConfig__ctor_m680991391(L_1, /*hidden argument*/NULL);
		AmazonCognitoIdentityConfig_t1124767059 * L_2 = L_1;
		RegionEndpoint_t661522805 * L_3 = ___region1;
		NullCheck(L_2);
		ClientConfig_set_RegionEndpoint_m2421036023(L_2, L_3, /*hidden argument*/NULL);
		AmazonCognitoIdentityClient__ctor_m567738961(__this, L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Amazon.CognitoIdentity.AmazonCognitoIdentityClient::.ctor(Amazon.Runtime.AWSCredentials,Amazon.CognitoIdentity.AmazonCognitoIdentityConfig)
extern "C"  void AmazonCognitoIdentityClient__ctor_m567738961 (AmazonCognitoIdentityClient_t3069350888 * __this, AWSCredentials_t3583921007 * ___credentials0, AmazonCognitoIdentityConfig_t1124767059 * ___clientConfig1, const MethodInfo* method)
{
	{
		AWSCredentials_t3583921007 * L_0 = ___credentials0;
		AmazonCognitoIdentityConfig_t1124767059 * L_1 = ___clientConfig1;
		AmazonServiceClient__ctor_m2824112809(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// Amazon.Runtime.Internal.Auth.AbstractAWSSigner Amazon.CognitoIdentity.AmazonCognitoIdentityClient::CreateSigner()
extern Il2CppClass* AWS4Signer_t1011449585_il2cpp_TypeInfo_var;
extern const uint32_t AmazonCognitoIdentityClient_CreateSigner_m3979572499_MetadataUsageId;
extern "C"  AbstractAWSSigner_t2114314031 * AmazonCognitoIdentityClient_CreateSigner_m3979572499 (AmazonCognitoIdentityClient_t3069350888 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AmazonCognitoIdentityClient_CreateSigner_m3979572499_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AWS4Signer_t1011449585 * L_0 = (AWS4Signer_t1011449585 *)il2cpp_codegen_object_new(AWS4Signer_t1011449585_il2cpp_TypeInfo_var);
		AWS4Signer__ctor_m1541432815(L_0, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void Amazon.CognitoIdentity.AmazonCognitoIdentityClient::Dispose(System.Boolean)
extern "C"  void AmazonCognitoIdentityClient_Dispose_m3910889566 (AmazonCognitoIdentityClient_t3069350888 * __this, bool ___disposing0, const MethodInfo* method)
{
	{
		bool L_0 = ___disposing0;
		AmazonServiceClient_Dispose_m3444168299(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// Amazon.CognitoIdentity.Model.GetCredentialsForIdentityResponse Amazon.CognitoIdentity.AmazonCognitoIdentityClient::GetCredentialsForIdentity(Amazon.CognitoIdentity.Model.GetCredentialsForIdentityRequest)
extern Il2CppClass* GetCredentialsForIdentityRequestMarshaller_t3584137889_il2cpp_TypeInfo_var;
extern Il2CppClass* GetCredentialsForIdentityResponseUnmarshaller_t2007495598_il2cpp_TypeInfo_var;
extern const MethodInfo* AmazonServiceClient_Invoke_TisGetCredentialsForIdentityRequest_t932830458_TisGetCredentialsForIdentityResponse_t2302678766_m557719389_MethodInfo_var;
extern const uint32_t AmazonCognitoIdentityClient_GetCredentialsForIdentity_m4012515634_MetadataUsageId;
extern "C"  GetCredentialsForIdentityResponse_t2302678766 * AmazonCognitoIdentityClient_GetCredentialsForIdentity_m4012515634 (AmazonCognitoIdentityClient_t3069350888 * __this, GetCredentialsForIdentityRequest_t932830458 * ___request0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AmazonCognitoIdentityClient_GetCredentialsForIdentity_m4012515634_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GetCredentialsForIdentityRequestMarshaller_t3584137889 * V_0 = NULL;
	GetCredentialsForIdentityResponseUnmarshaller_t2007495598 * V_1 = NULL;
	{
		GetCredentialsForIdentityRequestMarshaller_t3584137889 * L_0 = (GetCredentialsForIdentityRequestMarshaller_t3584137889 *)il2cpp_codegen_object_new(GetCredentialsForIdentityRequestMarshaller_t3584137889_il2cpp_TypeInfo_var);
		GetCredentialsForIdentityRequestMarshaller__ctor_m2870727695(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(GetCredentialsForIdentityResponseUnmarshaller_t2007495598_il2cpp_TypeInfo_var);
		GetCredentialsForIdentityResponseUnmarshaller_t2007495598 * L_1 = GetCredentialsForIdentityResponseUnmarshaller_get_Instance_m1533679114(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_1;
		GetCredentialsForIdentityRequest_t932830458 * L_2 = ___request0;
		GetCredentialsForIdentityRequestMarshaller_t3584137889 * L_3 = V_0;
		GetCredentialsForIdentityResponseUnmarshaller_t2007495598 * L_4 = V_1;
		GetCredentialsForIdentityResponse_t2302678766 * L_5 = AmazonServiceClient_Invoke_TisGetCredentialsForIdentityRequest_t932830458_TisGetCredentialsForIdentityResponse_t2302678766_m557719389(__this, L_2, L_3, L_4, /*hidden argument*/AmazonServiceClient_Invoke_TisGetCredentialsForIdentityRequest_t932830458_TisGetCredentialsForIdentityResponse_t2302678766_m557719389_MethodInfo_var);
		return L_5;
	}
}
// Amazon.CognitoIdentity.Model.GetIdResponse Amazon.CognitoIdentity.AmazonCognitoIdentityClient::GetId(Amazon.CognitoIdentity.Model.GetIdRequest)
extern Il2CppClass* GetIdRequestMarshaller_t3365653223_il2cpp_TypeInfo_var;
extern Il2CppClass* GetIdResponseUnmarshaller_t2243065850_il2cpp_TypeInfo_var;
extern const MethodInfo* AmazonServiceClient_Invoke_TisGetIdRequest_t4078561340_TisGetIdResponse_t2091118072_m991712797_MethodInfo_var;
extern const uint32_t AmazonCognitoIdentityClient_GetId_m2702301968_MetadataUsageId;
extern "C"  GetIdResponse_t2091118072 * AmazonCognitoIdentityClient_GetId_m2702301968 (AmazonCognitoIdentityClient_t3069350888 * __this, GetIdRequest_t4078561340 * ___request0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AmazonCognitoIdentityClient_GetId_m2702301968_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GetIdRequestMarshaller_t3365653223 * V_0 = NULL;
	GetIdResponseUnmarshaller_t2243065850 * V_1 = NULL;
	{
		GetIdRequestMarshaller_t3365653223 * L_0 = (GetIdRequestMarshaller_t3365653223 *)il2cpp_codegen_object_new(GetIdRequestMarshaller_t3365653223_il2cpp_TypeInfo_var);
		GetIdRequestMarshaller__ctor_m1760098451(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(GetIdResponseUnmarshaller_t2243065850_il2cpp_TypeInfo_var);
		GetIdResponseUnmarshaller_t2243065850 * L_1 = GetIdResponseUnmarshaller_get_Instance_m843541898(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_1;
		GetIdRequest_t4078561340 * L_2 = ___request0;
		GetIdRequestMarshaller_t3365653223 * L_3 = V_0;
		GetIdResponseUnmarshaller_t2243065850 * L_4 = V_1;
		GetIdResponse_t2091118072 * L_5 = AmazonServiceClient_Invoke_TisGetIdRequest_t4078561340_TisGetIdResponse_t2091118072_m991712797(__this, L_2, L_3, L_4, /*hidden argument*/AmazonServiceClient_Invoke_TisGetIdRequest_t4078561340_TisGetIdResponse_t2091118072_m991712797_MethodInfo_var);
		return L_5;
	}
}
// Amazon.CognitoIdentity.Model.GetOpenIdTokenResponse Amazon.CognitoIdentity.AmazonCognitoIdentityClient::GetOpenIdToken(Amazon.CognitoIdentity.Model.GetOpenIdTokenRequest)
extern Il2CppClass* GetOpenIdTokenRequestMarshaller_t1236206204_il2cpp_TypeInfo_var;
extern Il2CppClass* GetOpenIdTokenResponseUnmarshaller_t3130322723_il2cpp_TypeInfo_var;
extern const MethodInfo* AmazonServiceClient_Invoke_TisGetOpenIdTokenRequest_t2698079901_TisGetOpenIdTokenResponse_t955597635_m3565345917_MethodInfo_var;
extern const uint32_t AmazonCognitoIdentityClient_GetOpenIdToken_m1574299933_MetadataUsageId;
extern "C"  GetOpenIdTokenResponse_t955597635 * AmazonCognitoIdentityClient_GetOpenIdToken_m1574299933 (AmazonCognitoIdentityClient_t3069350888 * __this, GetOpenIdTokenRequest_t2698079901 * ___request0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AmazonCognitoIdentityClient_GetOpenIdToken_m1574299933_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GetOpenIdTokenRequestMarshaller_t1236206204 * V_0 = NULL;
	GetOpenIdTokenResponseUnmarshaller_t3130322723 * V_1 = NULL;
	{
		GetOpenIdTokenRequestMarshaller_t1236206204 * L_0 = (GetOpenIdTokenRequestMarshaller_t1236206204 *)il2cpp_codegen_object_new(GetOpenIdTokenRequestMarshaller_t1236206204_il2cpp_TypeInfo_var);
		GetOpenIdTokenRequestMarshaller__ctor_m355897200(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(GetOpenIdTokenResponseUnmarshaller_t3130322723_il2cpp_TypeInfo_var);
		GetOpenIdTokenResponseUnmarshaller_t3130322723 * L_1 = GetOpenIdTokenResponseUnmarshaller_get_Instance_m41713146(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_1;
		GetOpenIdTokenRequest_t2698079901 * L_2 = ___request0;
		GetOpenIdTokenRequestMarshaller_t1236206204 * L_3 = V_0;
		GetOpenIdTokenResponseUnmarshaller_t3130322723 * L_4 = V_1;
		GetOpenIdTokenResponse_t955597635 * L_5 = AmazonServiceClient_Invoke_TisGetOpenIdTokenRequest_t2698079901_TisGetOpenIdTokenResponse_t955597635_m3565345917(__this, L_2, L_3, L_4, /*hidden argument*/AmazonServiceClient_Invoke_TisGetOpenIdTokenRequest_t2698079901_TisGetOpenIdTokenResponse_t955597635_m3565345917_MethodInfo_var);
		return L_5;
	}
}
// System.Void Amazon.CognitoIdentity.AmazonCognitoIdentityConfig::.ctor()
extern Il2CppClass* AmazonCognitoIdentityConfig_t1124767059_il2cpp_TypeInfo_var;
extern Il2CppClass* ClientConfig_t3664713661_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1528072110;
extern const uint32_t AmazonCognitoIdentityConfig__ctor_m680991391_MetadataUsageId;
extern "C"  void AmazonCognitoIdentityConfig__ctor_m680991391 (AmazonCognitoIdentityConfig_t1124767059 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AmazonCognitoIdentityConfig__ctor_m680991391_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(AmazonCognitoIdentityConfig_t1124767059_il2cpp_TypeInfo_var);
		String_t* L_0 = ((AmazonCognitoIdentityConfig_t1124767059_StaticFields*)AmazonCognitoIdentityConfig_t1124767059_il2cpp_TypeInfo_var->static_fields)->get_UserAgentString_21();
		__this->set__userAgent_22(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(ClientConfig_t3664713661_il2cpp_TypeInfo_var);
		ClientConfig__ctor_m3613490344(__this, /*hidden argument*/NULL);
		ClientConfig_set_AuthenticationServiceName_m1205259675(__this, _stringLiteral1528072110, /*hidden argument*/NULL);
		return;
	}
}
// System.String Amazon.CognitoIdentity.AmazonCognitoIdentityConfig::get_RegionEndpointServiceName()
extern Il2CppCodeGenString* _stringLiteral1528072110;
extern const uint32_t AmazonCognitoIdentityConfig_get_RegionEndpointServiceName_m3891219750_MetadataUsageId;
extern "C"  String_t* AmazonCognitoIdentityConfig_get_RegionEndpointServiceName_m3891219750 (AmazonCognitoIdentityConfig_t1124767059 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AmazonCognitoIdentityConfig_get_RegionEndpointServiceName_m3891219750_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		return _stringLiteral1528072110;
	}
}
// System.String Amazon.CognitoIdentity.AmazonCognitoIdentityConfig::get_UserAgent()
extern "C"  String_t* AmazonCognitoIdentityConfig_get_UserAgent_m3071703051 (AmazonCognitoIdentityConfig_t1124767059 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get__userAgent_22();
		return L_0;
	}
}
// System.Void Amazon.CognitoIdentity.AmazonCognitoIdentityConfig::.cctor()
extern Il2CppClass* InternalSDKUtils_t3074264706_il2cpp_TypeInfo_var;
extern Il2CppClass* AmazonCognitoIdentityConfig_t1124767059_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3983658366;
extern const uint32_t AmazonCognitoIdentityConfig__cctor_m647399328_MetadataUsageId;
extern "C"  void AmazonCognitoIdentityConfig__cctor_m647399328 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AmazonCognitoIdentityConfig__cctor_m647399328_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InternalSDKUtils_t3074264706_il2cpp_TypeInfo_var);
		String_t* L_0 = InternalSDKUtils_BuildUserAgentString_m4237096378(NULL /*static, unused*/, _stringLiteral3983658366, /*hidden argument*/NULL);
		((AmazonCognitoIdentityConfig_t1124767059_StaticFields*)AmazonCognitoIdentityConfig_t1124767059_il2cpp_TypeInfo_var->static_fields)->set_UserAgentString_21(L_0);
		return;
	}
}
// System.Void Amazon.CognitoIdentity.AmazonCognitoIdentityException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
extern "C"  void AmazonCognitoIdentityException__ctor_m1060593889 (AmazonCognitoIdentityException_t158465174 * __this, String_t* ___message0, Exception_t1927440687 * ___innerException1, int32_t ___errorType2, String_t* ___errorCode3, String_t* ___requestId4, int32_t ___statusCode5, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		Exception_t1927440687 * L_1 = ___innerException1;
		int32_t L_2 = ___errorType2;
		String_t* L_3 = ___errorCode3;
		String_t* L_4 = ___requestId4;
		int32_t L_5 = ___statusCode5;
		AmazonServiceException__ctor_m1032639064(__this, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Amazon.CognitoIdentity.AmazonCognitoIdentityException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void AmazonCognitoIdentityException__ctor_m1824076445 (AmazonCognitoIdentityException_t158465174 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method)
{
	{
		SerializationInfo_t228987430 * L_0 = ___info0;
		StreamingContext_t1417235061  L_1 = ___context1;
		AmazonServiceException__ctor_m903377152(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Amazon.CognitoIdentity.AmazonCognitoIdentityRequest::.ctor()
extern "C"  void AmazonCognitoIdentityRequest__ctor_m2566095720 (AmazonCognitoIdentityRequest_t492659996 * __this, const MethodInfo* method)
{
	{
		AmazonWebServiceRequest__ctor_m2915505731(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Amazon.CognitoIdentity.CognitoAWSCredentials::get_IsIdentitySet()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t CognitoAWSCredentials_get_IsIdentitySet_m3850620751_MetadataUsageId;
extern "C"  bool CognitoAWSCredentials_get_IsIdentitySet_m3850620751 (CognitoAWSCredentials_t2370264792 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CognitoAWSCredentials_get_IsIdentitySet_m3850620751_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = __this->get_identityId_4();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String Amazon.CognitoIdentity.CognitoAWSCredentials::GetCachedIdentityId() */, __this);
		__this->set_identityId_4(L_2);
	}

IL_0019:
	{
		String_t* L_3 = __this->get_identityId_4();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_4 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_4) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void Amazon.CognitoIdentity.CognitoAWSCredentials::UpdateIdentity(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* IdentityChangedArgs_t1657401211_il2cpp_TypeInfo_var;
extern const MethodInfo* EventHandler_1_Invoke_m1504183659_MethodInfo_var;
extern const uint32_t CognitoAWSCredentials_UpdateIdentity_m833388265_MetadataUsageId;
extern "C"  void CognitoAWSCredentials_UpdateIdentity_m833388265 (CognitoAWSCredentials_t2370264792 * __this, String_t* ___newIdentityId0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CognitoAWSCredentials_UpdateIdentity_m833388265_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	EventHandler_1_t248708383 * V_1 = NULL;
	IdentityChangedArgs_t1657401211 * V_2 = NULL;
	{
		String_t* L_0 = __this->get_identityId_4();
		String_t* L_1 = ___newIdentityId0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_Equals_m2950069882(NULL /*static, unused*/, L_0, L_1, 4, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0010;
		}
	}
	{
		return;
	}

IL_0010:
	{
		String_t* L_3 = ___newIdentityId0;
		VirtActionInvoker1< String_t* >::Invoke(9 /* System.Void Amazon.CognitoIdentity.CognitoAWSCredentials::CacheIdentityId(System.String) */, __this, L_3);
		VirtActionInvoker0::Invoke(6 /* System.Void Amazon.Runtime.RefreshingAWSCredentials::ClearCredentials() */, __this);
		String_t* L_4 = __this->get_identityId_4();
		V_0 = L_4;
		String_t* L_5 = ___newIdentityId0;
		__this->set_identityId_4(L_5);
		EventHandler_1_t248708383 * L_6 = __this->get_mIdentityChangedEvent_14();
		V_1 = L_6;
		EventHandler_1_t248708383 * L_7 = V_1;
		if (!L_7)
		{
			goto IL_0045;
		}
	}
	{
		String_t* L_8 = V_0;
		String_t* L_9 = ___newIdentityId0;
		IdentityChangedArgs_t1657401211 * L_10 = (IdentityChangedArgs_t1657401211 *)il2cpp_codegen_object_new(IdentityChangedArgs_t1657401211_il2cpp_TypeInfo_var);
		IdentityChangedArgs__ctor_m2632388964(L_10, L_8, L_9, /*hidden argument*/NULL);
		V_2 = L_10;
		EventHandler_1_t248708383 * L_11 = V_1;
		IdentityChangedArgs_t1657401211 * L_12 = V_2;
		NullCheck(L_11);
		EventHandler_1_Invoke_m1504183659(L_11, __this, L_12, /*hidden argument*/EventHandler_1_Invoke_m1504183659_MethodInfo_var);
	}

IL_0045:
	{
		return;
	}
}
// System.String Amazon.CognitoIdentity.CognitoAWSCredentials::GetNamespacedKey(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029336;
extern const uint32_t CognitoAWSCredentials_GetNamespacedKey_m3390205149_MetadataUsageId;
extern "C"  String_t* CognitoAWSCredentials_GetNamespacedKey_m3390205149 (CognitoAWSCredentials_t2370264792 * __this, String_t* ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CognitoAWSCredentials_GetNamespacedKey_m3390205149_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___key0;
		String_t* L_1 = CognitoAWSCredentials_get_IdentityPoolId_m2723479775(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m612901809(NULL /*static, unused*/, L_0, _stringLiteral372029336, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.String Amazon.CognitoIdentity.CognitoAWSCredentials::get_AccountId()
extern "C"  String_t* CognitoAWSCredentials_get_AccountId_m1435683876 (CognitoAWSCredentials_t2370264792 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CAccountIdU3Ek__BackingField_8();
		return L_0;
	}
}
// System.Void Amazon.CognitoIdentity.CognitoAWSCredentials::set_AccountId(System.String)
extern "C"  void CognitoAWSCredentials_set_AccountId_m309865923 (CognitoAWSCredentials_t2370264792 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CAccountIdU3Ek__BackingField_8(L_0);
		return;
	}
}
// System.String Amazon.CognitoIdentity.CognitoAWSCredentials::get_IdentityPoolId()
extern "C"  String_t* CognitoAWSCredentials_get_IdentityPoolId_m2723479775 (CognitoAWSCredentials_t2370264792 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CIdentityPoolIdU3Ek__BackingField_9();
		return L_0;
	}
}
// System.Void Amazon.CognitoIdentity.CognitoAWSCredentials::set_IdentityPoolId(System.String)
extern "C"  void CognitoAWSCredentials_set_IdentityPoolId_m2126935958 (CognitoAWSCredentials_t2370264792 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CIdentityPoolIdU3Ek__BackingField_9(L_0);
		return;
	}
}
// System.String Amazon.CognitoIdentity.CognitoAWSCredentials::get_UnAuthRoleArn()
extern "C"  String_t* CognitoAWSCredentials_get_UnAuthRoleArn_m4182746162 (CognitoAWSCredentials_t2370264792 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CUnAuthRoleArnU3Ek__BackingField_10();
		return L_0;
	}
}
// System.Void Amazon.CognitoIdentity.CognitoAWSCredentials::set_UnAuthRoleArn(System.String)
extern "C"  void CognitoAWSCredentials_set_UnAuthRoleArn_m338067849 (CognitoAWSCredentials_t2370264792 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CUnAuthRoleArnU3Ek__BackingField_10(L_0);
		return;
	}
}
// System.String Amazon.CognitoIdentity.CognitoAWSCredentials::get_AuthRoleArn()
extern "C"  String_t* CognitoAWSCredentials_get_AuthRoleArn_m2832189913 (CognitoAWSCredentials_t2370264792 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CAuthRoleArnU3Ek__BackingField_11();
		return L_0;
	}
}
// System.Void Amazon.CognitoIdentity.CognitoAWSCredentials::set_AuthRoleArn(System.String)
extern "C"  void CognitoAWSCredentials_set_AuthRoleArn_m1654546 (CognitoAWSCredentials_t2370264792 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CAuthRoleArnU3Ek__BackingField_11(L_0);
		return;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.String> Amazon.CognitoIdentity.CognitoAWSCredentials::get_Logins()
extern "C"  Dictionary_2_t3943999495 * CognitoAWSCredentials_get_Logins_m1002484841 (CognitoAWSCredentials_t2370264792 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t3943999495 * L_0 = __this->get_U3CLoginsU3Ek__BackingField_12();
		return L_0;
	}
}
// System.Void Amazon.CognitoIdentity.CognitoAWSCredentials::set_Logins(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void CognitoAWSCredentials_set_Logins_m932349206 (CognitoAWSCredentials_t2370264792 * __this, Dictionary_2_t3943999495 * ___value0, const MethodInfo* method)
{
	{
		Dictionary_2_t3943999495 * L_0 = ___value0;
		__this->set_U3CLoginsU3Ek__BackingField_12(L_0);
		return;
	}
}
// System.Void Amazon.CognitoIdentity.CognitoAWSCredentials::Clear()
extern const MethodInfo* Dictionary_2_Clear_m2536312639_MethodInfo_var;
extern const uint32_t CognitoAWSCredentials_Clear_m3749298493_MetadataUsageId;
extern "C"  void CognitoAWSCredentials_Clear_m3749298493 (CognitoAWSCredentials_t2370264792 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CognitoAWSCredentials_Clear_m3749298493_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_identityId_4((String_t*)NULL);
		VirtActionInvoker0::Invoke(6 /* System.Void Amazon.Runtime.RefreshingAWSCredentials::ClearCredentials() */, __this);
		VirtActionInvoker0::Invoke(10 /* System.Void Amazon.CognitoIdentity.CognitoAWSCredentials::ClearIdentityCache() */, __this);
		Dictionary_2_t3943999495 * L_0 = CognitoAWSCredentials_get_Logins_m1002484841(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Dictionary_2_Clear_m2536312639(L_0, /*hidden argument*/Dictionary_2_Clear_m2536312639_MethodInfo_var);
		return;
	}
}
// System.String Amazon.CognitoIdentity.CognitoAWSCredentials::GetIdentityId()
extern "C"  String_t* CognitoAWSCredentials_GetIdentityId_m1822078866 (CognitoAWSCredentials_t2370264792 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = CognitoAWSCredentials_GetIdentityId_m1939085957(__this, 0, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.String Amazon.CognitoIdentity.CognitoAWSCredentials::GetIdentityId(Amazon.CognitoIdentity.CognitoAWSCredentials/RefreshIdentityOptions)
extern Il2CppClass* CognitoAWSCredentials_t2370264792_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_set_Item_m4244870320_MethodInfo_var;
extern const uint32_t CognitoAWSCredentials_GetIdentityId_m1939085957_MetadataUsageId;
extern "C"  String_t* CognitoAWSCredentials_GetIdentityId_m1939085957 (CognitoAWSCredentials_t2370264792 * __this, int32_t ___options0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CognitoAWSCredentials_GetIdentityId_m1939085957_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(CognitoAWSCredentials_t2370264792_il2cpp_TypeInfo_var);
		Il2CppObject * L_0 = ((CognitoAWSCredentials_t2370264792_StaticFields*)CognitoAWSCredentials_t2370264792_il2cpp_TypeInfo_var->static_fields)->get_refreshIdLock_3();
		V_0 = L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			bool L_2 = CognitoAWSCredentials_get_IsIdentitySet_m3850620751(__this, /*hidden argument*/NULL);
			if (!L_2)
			{
				goto IL_0018;
			}
		}

IL_0014:
		{
			int32_t L_3 = ___options0;
			if ((!(((uint32_t)L_3) == ((uint32_t)1))))
			{
				goto IL_0068;
			}
		}

IL_0018:
		{
			IdentityState_t2011390993 * L_4 = VirtFuncInvoker0< IdentityState_t2011390993 * >::Invoke(7 /* Amazon.CognitoIdentity.CognitoAWSCredentials/IdentityState Amazon.CognitoIdentity.CognitoAWSCredentials::RefreshIdentity() */, __this);
			__this->set__identityState_13(L_4);
			IdentityState_t2011390993 * L_5 = __this->get__identityState_13();
			NullCheck(L_5);
			String_t* L_6 = IdentityState_get_LoginProvider_m3629843770(L_5, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_7 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
			if (L_7)
			{
				goto IL_0057;
			}
		}

IL_0036:
		{
			Dictionary_2_t3943999495 * L_8 = CognitoAWSCredentials_get_Logins_m1002484841(__this, /*hidden argument*/NULL);
			IdentityState_t2011390993 * L_9 = __this->get__identityState_13();
			NullCheck(L_9);
			String_t* L_10 = IdentityState_get_LoginProvider_m3629843770(L_9, /*hidden argument*/NULL);
			IdentityState_t2011390993 * L_11 = __this->get__identityState_13();
			NullCheck(L_11);
			String_t* L_12 = IdentityState_get_LoginToken_m1363466894(L_11, /*hidden argument*/NULL);
			NullCheck(L_8);
			Dictionary_2_set_Item_m4244870320(L_8, L_10, L_12, /*hidden argument*/Dictionary_2_set_Item_m4244870320_MethodInfo_var);
		}

IL_0057:
		{
			IdentityState_t2011390993 * L_13 = __this->get__identityState_13();
			NullCheck(L_13);
			String_t* L_14 = IdentityState_get_IdentityId_m3454520601(L_13, /*hidden argument*/NULL);
			CognitoAWSCredentials_UpdateIdentity_m833388265(__this, L_14, /*hidden argument*/NULL);
		}

IL_0068:
		{
			IL2CPP_LEAVE(0x71, FINALLY_006a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_006a;
	}

FINALLY_006a:
	{ // begin finally (depth: 1)
		Il2CppObject * L_15 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(106)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(106)
	{
		IL2CPP_JUMP_TBL(0x71, IL_0071)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0071:
	{
		String_t* L_16 = __this->get_identityId_4();
		return L_16;
	}
}
// Amazon.CognitoIdentity.CognitoAWSCredentials/IdentityState Amazon.CognitoIdentity.CognitoAWSCredentials::RefreshIdentity()
extern Il2CppClass* GetIdRequest_t4078561340_il2cpp_TypeInfo_var;
extern Il2CppClass* IdentityState_t2011390993_il2cpp_TypeInfo_var;
extern const uint32_t CognitoAWSCredentials_RefreshIdentity_m2495389495_MetadataUsageId;
extern "C"  IdentityState_t2011390993 * CognitoAWSCredentials_RefreshIdentity_m2495389495 (CognitoAWSCredentials_t2370264792 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CognitoAWSCredentials_RefreshIdentity_m2495389495_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	GetIdRequest_t4078561340 * V_1 = NULL;
	GetIdResponse_t2091118072 * V_2 = NULL;
	{
		V_0 = (bool)1;
		bool L_0 = CognitoAWSCredentials_get_IsIdentitySet_m3850620751(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_004f;
		}
	}
	{
		GetIdRequest_t4078561340 * L_1 = (GetIdRequest_t4078561340 *)il2cpp_codegen_object_new(GetIdRequest_t4078561340_il2cpp_TypeInfo_var);
		GetIdRequest__ctor_m3030190485(L_1, /*hidden argument*/NULL);
		GetIdRequest_t4078561340 * L_2 = L_1;
		String_t* L_3 = CognitoAWSCredentials_get_AccountId_m1435683876(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		GetIdRequest_set_AccountId_m4016883484(L_2, L_3, /*hidden argument*/NULL);
		GetIdRequest_t4078561340 * L_4 = L_2;
		String_t* L_5 = CognitoAWSCredentials_get_IdentityPoolId_m2723479775(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		GetIdRequest_set_IdentityPoolId_m1186750113(L_4, L_5, /*hidden argument*/NULL);
		GetIdRequest_t4078561340 * L_6 = L_4;
		Dictionary_2_t3943999495 * L_7 = CognitoAWSCredentials_get_Logins_m1002484841(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		GetIdRequest_set_Logins_m1388169875(L_6, L_7, /*hidden argument*/NULL);
		V_1 = L_6;
		AmazonCognitoIdentityClient_t3069350888 * L_8 = __this->get_cib_6();
		GetIdRequest_t4078561340 * L_9 = V_1;
		NullCheck(L_8);
		GetIdResponse_t2091118072 * L_10 = AmazonCognitoIdentityClient_GetId_m2702301968(L_8, L_9, /*hidden argument*/NULL);
		V_2 = L_10;
		V_0 = (bool)0;
		GetIdResponse_t2091118072 * L_11 = V_2;
		NullCheck(L_11);
		String_t* L_12 = GetIdResponse_get_IdentityId_m160087976(L_11, /*hidden argument*/NULL);
		CognitoAWSCredentials_UpdateIdentity_m833388265(__this, L_12, /*hidden argument*/NULL);
	}

IL_004f:
	{
		String_t* L_13 = __this->get_identityId_4();
		bool L_14 = V_0;
		IdentityState_t2011390993 * L_15 = (IdentityState_t2011390993 *)il2cpp_codegen_object_new(IdentityState_t2011390993_il2cpp_TypeInfo_var);
		IdentityState__ctor_m2951748439(L_15, L_13, L_14, /*hidden argument*/NULL);
		return L_15;
	}
}
// System.Boolean Amazon.CognitoIdentity.CognitoAWSCredentials::ShouldRetry(Amazon.CognitoIdentity.AmazonCognitoIdentityException)
extern Il2CppClass* NotAuthorizedException_t457234085_il2cpp_TypeInfo_var;
extern Il2CppClass* ResourceNotFoundException_t403010890_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral482204013;
extern const uint32_t CognitoAWSCredentials_ShouldRetry_m1614465732_MetadataUsageId;
extern "C"  bool CognitoAWSCredentials_ShouldRetry_m1614465732 (CognitoAWSCredentials_t2370264792 * __this, AmazonCognitoIdentityException_t158465174 * ___e0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CognitoAWSCredentials_ShouldRetry_m1614465732_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IdentityState_t2011390993 * L_0 = __this->get__identityState_13();
		NullCheck(L_0);
		bool L_1 = IdentityState_get_LoginSpecified_m1764907814(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_003f;
		}
	}
	{
		AmazonCognitoIdentityException_t158465174 * L_2 = ___e0;
		if (!((NotAuthorizedException_t457234085 *)IsInstClass(L_2, NotAuthorizedException_t457234085_il2cpp_TypeInfo_var)))
		{
			goto IL_0028;
		}
	}
	{
		AmazonCognitoIdentityException_t158465174 * L_3 = ___e0;
		NullCheck(L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_3);
		NullCheck(L_4);
		bool L_5 = String_StartsWith_m46695182(L_4, _stringLiteral482204013, 5, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0030;
		}
	}

IL_0028:
	{
		AmazonCognitoIdentityException_t158465174 * L_6 = ___e0;
		if (!((ResourceNotFoundException_t403010890 *)IsInstClass(L_6, ResourceNotFoundException_t403010890_il2cpp_TypeInfo_var)))
		{
			goto IL_003f;
		}
	}

IL_0030:
	{
		__this->set_identityId_4((String_t*)NULL);
		VirtActionInvoker0::Invoke(10 /* System.Void Amazon.CognitoIdentity.CognitoAWSCredentials::ClearIdentityCache() */, __this);
		return (bool)1;
	}

IL_003f:
	{
		return (bool)0;
	}
}
// System.Void Amazon.CognitoIdentity.CognitoAWSCredentials::add_IdentityChangedEvent(System.EventHandler`1<Amazon.CognitoIdentity.CognitoAWSCredentials/IdentityChangedArgs>)
extern Il2CppClass* EventHandler_1_t248708383_il2cpp_TypeInfo_var;
extern const uint32_t CognitoAWSCredentials_add_IdentityChangedEvent_m542934635_MetadataUsageId;
extern "C"  void CognitoAWSCredentials_add_IdentityChangedEvent_m542934635 (CognitoAWSCredentials_t2370264792 * __this, EventHandler_1_t248708383 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CognitoAWSCredentials_add_IdentityChangedEvent_m542934635_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CognitoAWSCredentials_t2370264792 * V_0 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = __this;
		CognitoAWSCredentials_t2370264792 * L_0 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
	}

IL_0008:
	try
	{ // begin try (depth: 1)
		EventHandler_1_t248708383 * L_1 = __this->get_mIdentityChangedEvent_14();
		EventHandler_1_t248708383 * L_2 = ___value0;
		Delegate_t3022476291 * L_3 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		__this->set_mIdentityChangedEvent_14(((EventHandler_1_t248708383 *)CastclassSealed(L_3, EventHandler_1_t248708383_il2cpp_TypeInfo_var)));
		IL2CPP_LEAVE(0x28, FINALLY_0021);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0021;
	}

FINALLY_0021:
	{ // begin finally (depth: 1)
		CognitoAWSCredentials_t2370264792 * L_4 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(33)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(33)
	{
		IL2CPP_JUMP_TBL(0x28, IL_0028)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0028:
	{
		return;
	}
}
// System.Void Amazon.CognitoIdentity.CognitoAWSCredentials::remove_IdentityChangedEvent(System.EventHandler`1<Amazon.CognitoIdentity.CognitoAWSCredentials/IdentityChangedArgs>)
extern Il2CppClass* EventHandler_1_t248708383_il2cpp_TypeInfo_var;
extern const uint32_t CognitoAWSCredentials_remove_IdentityChangedEvent_m1774745486_MetadataUsageId;
extern "C"  void CognitoAWSCredentials_remove_IdentityChangedEvent_m1774745486 (CognitoAWSCredentials_t2370264792 * __this, EventHandler_1_t248708383 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CognitoAWSCredentials_remove_IdentityChangedEvent_m1774745486_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CognitoAWSCredentials_t2370264792 * V_0 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = __this;
		CognitoAWSCredentials_t2370264792 * L_0 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
	}

IL_0008:
	try
	{ // begin try (depth: 1)
		EventHandler_1_t248708383 * L_1 = __this->get_mIdentityChangedEvent_14();
		EventHandler_1_t248708383 * L_2 = ___value0;
		Delegate_t3022476291 * L_3 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		__this->set_mIdentityChangedEvent_14(((EventHandler_1_t248708383 *)CastclassSealed(L_3, EventHandler_1_t248708383_il2cpp_TypeInfo_var)));
		IL2CPP_LEAVE(0x28, FINALLY_0021);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0021;
	}

FINALLY_0021:
	{ // begin finally (depth: 1)
		CognitoAWSCredentials_t2370264792 * L_4 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(33)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(33)
	{
		IL2CPP_JUMP_TBL(0x28, IL_0028)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0028:
	{
		return;
	}
}
// System.Void Amazon.CognitoIdentity.CognitoAWSCredentials::.ctor(System.String,Amazon.RegionEndpoint)
extern "C"  void CognitoAWSCredentials__ctor_m2007748265 (CognitoAWSCredentials_t2370264792 * __this, String_t* ___identityPoolId0, RegionEndpoint_t661522805 * ___region1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___identityPoolId0;
		RegionEndpoint_t661522805 * L_1 = ___region1;
		CognitoAWSCredentials__ctor_m3149997047(__this, (String_t*)NULL, L_0, (String_t*)NULL, (String_t*)NULL, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Amazon.CognitoIdentity.CognitoAWSCredentials::.ctor(System.String,System.String,System.String,System.String,Amazon.RegionEndpoint)
extern Il2CppClass* AnonymousAWSCredentials_t3877662854_il2cpp_TypeInfo_var;
extern Il2CppClass* AmazonCognitoIdentityClient_t3069350888_il2cpp_TypeInfo_var;
extern Il2CppClass* AmazonSecurityTokenServiceClient_t1241355389_il2cpp_TypeInfo_var;
extern const uint32_t CognitoAWSCredentials__ctor_m3149997047_MetadataUsageId;
extern "C"  void CognitoAWSCredentials__ctor_m3149997047 (CognitoAWSCredentials_t2370264792 * __this, String_t* ___accountId0, String_t* ___identityPoolId1, String_t* ___unAuthRoleArn2, String_t* ___authRoleArn3, RegionEndpoint_t661522805 * ___region4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CognitoAWSCredentials__ctor_m3149997047_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___accountId0;
		String_t* L_1 = ___identityPoolId1;
		String_t* L_2 = ___unAuthRoleArn2;
		String_t* L_3 = ___authRoleArn3;
		AnonymousAWSCredentials_t3877662854 * L_4 = (AnonymousAWSCredentials_t3877662854 *)il2cpp_codegen_object_new(AnonymousAWSCredentials_t3877662854_il2cpp_TypeInfo_var);
		AnonymousAWSCredentials__ctor_m211307267(L_4, /*hidden argument*/NULL);
		RegionEndpoint_t661522805 * L_5 = ___region4;
		AmazonCognitoIdentityClient_t3069350888 * L_6 = (AmazonCognitoIdentityClient_t3069350888 *)il2cpp_codegen_object_new(AmazonCognitoIdentityClient_t3069350888_il2cpp_TypeInfo_var);
		AmazonCognitoIdentityClient__ctor_m3549263274(L_6, L_4, L_5, /*hidden argument*/NULL);
		AnonymousAWSCredentials_t3877662854 * L_7 = (AnonymousAWSCredentials_t3877662854 *)il2cpp_codegen_object_new(AnonymousAWSCredentials_t3877662854_il2cpp_TypeInfo_var);
		AnonymousAWSCredentials__ctor_m211307267(L_7, /*hidden argument*/NULL);
		RegionEndpoint_t661522805 * L_8 = ___region4;
		AmazonSecurityTokenServiceClient_t1241355389 * L_9 = (AmazonSecurityTokenServiceClient_t1241355389 *)il2cpp_codegen_object_new(AmazonSecurityTokenServiceClient_t1241355389_il2cpp_TypeInfo_var);
		AmazonSecurityTokenServiceClient__ctor_m77079877(L_9, L_7, L_8, /*hidden argument*/NULL);
		CognitoAWSCredentials__ctor_m2238162873(__this, L_0, L_1, L_2, L_3, L_6, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Amazon.CognitoIdentity.CognitoAWSCredentials::.ctor(System.String,System.String,System.String,System.String,Amazon.CognitoIdentity.IAmazonCognitoIdentity,Amazon.SecurityToken.IAmazonSecurityTokenService)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppClass* StringComparer_t1574862926_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t3943999495_il2cpp_TypeInfo_var;
extern Il2CppClass* AmazonCognitoIdentityClient_t3069350888_il2cpp_TypeInfo_var;
extern Il2CppClass* AmazonSecurityTokenServiceClient_t1241355389_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m3614116030_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2312299319;
extern Il2CppCodeGenString* _stringLiteral2697086785;
extern Il2CppCodeGenString* _stringLiteral3271643745;
extern const uint32_t CognitoAWSCredentials__ctor_m2238162873_MetadataUsageId;
extern "C"  void CognitoAWSCredentials__ctor_m2238162873 (CognitoAWSCredentials_t2370264792 * __this, String_t* ___accountId0, String_t* ___identityPoolId1, String_t* ___unAuthRoleArn2, String_t* ___authRoleArn3, Il2CppObject * ___cibClient4, Il2CppObject * ___stsClient5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CognitoAWSCredentials__ctor_m2238162873_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		RefreshingAWSCredentials__ctor_m2336079865(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___identityPoolId1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		ArgumentNullException_t628810857 * L_2 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_2, _stringLiteral2312299319, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0019:
	{
		Il2CppObject * L_3 = ___cibClient4;
		if (L_3)
		{
			goto IL_0028;
		}
	}
	{
		ArgumentNullException_t628810857 * L_4 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_4, _stringLiteral2697086785, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0028:
	{
		String_t* L_5 = ___unAuthRoleArn2;
		if (L_5)
		{
			goto IL_002f;
		}
	}
	{
		String_t* L_6 = ___authRoleArn3;
		if (!L_6)
		{
			goto IL_003e;
		}
	}

IL_002f:
	{
		Il2CppObject * L_7 = ___stsClient5;
		if (L_7)
		{
			goto IL_003e;
		}
	}
	{
		ArgumentNullException_t628810857 * L_8 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_8, _stringLiteral3271643745, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_003e:
	{
		String_t* L_9 = ___accountId0;
		CognitoAWSCredentials_set_AccountId_m309865923(__this, L_9, /*hidden argument*/NULL);
		String_t* L_10 = ___identityPoolId1;
		CognitoAWSCredentials_set_IdentityPoolId_m2126935958(__this, L_10, /*hidden argument*/NULL);
		String_t* L_11 = ___unAuthRoleArn2;
		CognitoAWSCredentials_set_UnAuthRoleArn_m338067849(__this, L_11, /*hidden argument*/NULL);
		String_t* L_12 = ___authRoleArn3;
		CognitoAWSCredentials_set_AuthRoleArn_m1654546(__this, L_12, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(StringComparer_t1574862926_il2cpp_TypeInfo_var);
		StringComparer_t1574862926 * L_13 = StringComparer_get_Ordinal_m3140767557(NULL /*static, unused*/, /*hidden argument*/NULL);
		Dictionary_2_t3943999495 * L_14 = (Dictionary_2_t3943999495 *)il2cpp_codegen_object_new(Dictionary_2_t3943999495_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m3614116030(L_14, L_13, /*hidden argument*/Dictionary_2__ctor_m3614116030_MethodInfo_var);
		CognitoAWSCredentials_set_Logins_m932349206(__this, L_14, /*hidden argument*/NULL);
		Il2CppObject * L_15 = ___cibClient4;
		__this->set_cib_6(((AmazonCognitoIdentityClient_t3069350888 *)CastclassClass(L_15, AmazonCognitoIdentityClient_t3069350888_il2cpp_TypeInfo_var)));
		Il2CppObject * L_16 = ___stsClient5;
		__this->set_sts_7(((AmazonSecurityTokenServiceClient_t1241355389 *)CastclassClass(L_16, AmazonSecurityTokenServiceClient_t1241355389_il2cpp_TypeInfo_var)));
		String_t* L_17 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String Amazon.CognitoIdentity.CognitoAWSCredentials::GetCachedIdentityId() */, __this);
		V_0 = L_17;
		String_t* L_18 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_19 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		if (L_19)
		{
			goto IL_00a7;
		}
	}
	{
		String_t* L_20 = V_0;
		CognitoAWSCredentials_UpdateIdentity_m833388265(__this, L_20, /*hidden argument*/NULL);
		CredentialsRefreshState_t3294867821 * L_21 = CognitoAWSCredentials_GetCachedCredentials_m4289102006(__this, /*hidden argument*/NULL);
		((RefreshingAWSCredentials_t1767066958 *)__this)->set_currentState_0(L_21);
	}

IL_00a7:
	{
		return;
	}
}
// Amazon.Runtime.RefreshingAWSCredentials/CredentialsRefreshState Amazon.CognitoIdentity.CognitoAWSCredentials::GenerateNewCredentials()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_get_Count_m2692997481_MethodInfo_var;
extern const uint32_t CognitoAWSCredentials_GenerateNewCredentials_m1385113071_MetadataUsageId;
extern "C"  CredentialsRefreshState_t3294867821 * CognitoAWSCredentials_GenerateNewCredentials_m1385113071 (CognitoAWSCredentials_t2370264792 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CognitoAWSCredentials_GenerateNewCredentials_m1385113071_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CredentialsRefreshState_t3294867821 * V_0 = NULL;
	String_t* V_1 = NULL;
	{
		String_t* L_0 = CognitoAWSCredentials_get_UnAuthRoleArn_m4182746162(__this, /*hidden argument*/NULL);
		V_1 = L_0;
		Dictionary_2_t3943999495 * L_1 = CognitoAWSCredentials_get_Logins_m1002484841(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_2 = Dictionary_2_get_Count_m2692997481(L_1, /*hidden argument*/Dictionary_2_get_Count_m2692997481_MethodInfo_var);
		if ((((int32_t)L_2) <= ((int32_t)0)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_3 = CognitoAWSCredentials_get_AuthRoleArn_m2832189913(__this, /*hidden argument*/NULL);
		V_1 = L_3;
	}

IL_001c:
	{
		String_t* L_4 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (!((((int32_t)L_5) == ((int32_t)0))? 1 : 0))
		{
			goto IL_0031;
		}
	}
	{
		String_t* L_6 = V_1;
		CredentialsRefreshState_t3294867821 * L_7 = CognitoAWSCredentials_GetCredentialsForRole_m3530527127(__this, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		goto IL_0038;
	}

IL_0031:
	{
		CredentialsRefreshState_t3294867821 * L_8 = CognitoAWSCredentials_GetPoolCredentials_m3438202962(__this, /*hidden argument*/NULL);
		V_0 = L_8;
	}

IL_0038:
	{
		CredentialsRefreshState_t3294867821 * L_9 = V_0;
		CognitoAWSCredentials_CacheCredentials_m2721308407(__this, L_9, /*hidden argument*/NULL);
		CredentialsRefreshState_t3294867821 * L_10 = V_0;
		return L_10;
	}
}
// Amazon.Runtime.RefreshingAWSCredentials/CredentialsRefreshState Amazon.CognitoIdentity.CognitoAWSCredentials::GetPoolCredentials()
extern Il2CppClass* GetCredentialsForIdentityRequest_t932830458_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t3943999495_il2cpp_TypeInfo_var;
extern Il2CppClass* AmazonCognitoIdentityException_t158465174_il2cpp_TypeInfo_var;
extern Il2CppClass* CredentialsRefreshState_t3294867821_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_get_Count_m2692997481_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m486600382_MethodInfo_var;
extern const MethodInfo* Dictionary_2_set_Item_m4244870320_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3879863438;
extern const uint32_t CognitoAWSCredentials_GetPoolCredentials_m3438202962_MetadataUsageId;
extern "C"  CredentialsRefreshState_t3294867821 * CognitoAWSCredentials_GetPoolCredentials_m3438202962 (CognitoAWSCredentials_t2370264792 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CognitoAWSCredentials_GetPoolCredentials_m3438202962_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	GetCredentialsForIdentityRequest_t932830458 * V_1 = NULL;
	bool V_2 = false;
	GetCredentialsForIdentityResponse_t2302678766 * V_3 = NULL;
	Credentials_t792472136 * V_4 = NULL;
	AmazonCognitoIdentityException_t158465174 * V_5 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = CognitoAWSCredentials_GetIdentityId_m1939085957(__this, 1, /*hidden argument*/NULL);
		V_0 = L_0;
		GetCredentialsForIdentityRequest_t932830458 * L_1 = (GetCredentialsForIdentityRequest_t932830458 *)il2cpp_codegen_object_new(GetCredentialsForIdentityRequest_t932830458_il2cpp_TypeInfo_var);
		GetCredentialsForIdentityRequest__ctor_m1038175453(L_1, /*hidden argument*/NULL);
		GetCredentialsForIdentityRequest_t932830458 * L_2 = L_1;
		String_t* L_3 = V_0;
		NullCheck(L_2);
		GetCredentialsForIdentityRequest_set_IdentityId_m1467243909(L_2, L_3, /*hidden argument*/NULL);
		V_1 = L_2;
		Dictionary_2_t3943999495 * L_4 = CognitoAWSCredentials_get_Logins_m1002484841(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		int32_t L_5 = Dictionary_2_get_Count_m2692997481(L_4, /*hidden argument*/Dictionary_2_get_Count_m2692997481_MethodInfo_var);
		if ((((int32_t)L_5) <= ((int32_t)0)))
		{
			goto IL_002f;
		}
	}
	{
		GetCredentialsForIdentityRequest_t932830458 * L_6 = V_1;
		Dictionary_2_t3943999495 * L_7 = CognitoAWSCredentials_get_Logins_m1002484841(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		GetCredentialsForIdentityRequest_set_Logins_m4103779471(L_6, L_7, /*hidden argument*/NULL);
	}

IL_002f:
	{
		IdentityState_t2011390993 * L_8 = __this->get__identityState_13();
		if (!L_8)
		{
			goto IL_006f;
		}
	}
	{
		IdentityState_t2011390993 * L_9 = __this->get__identityState_13();
		NullCheck(L_9);
		String_t* L_10 = IdentityState_get_LoginToken_m1363466894(L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_11 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_006f;
		}
	}
	{
		GetCredentialsForIdentityRequest_t932830458 * L_12 = V_1;
		Dictionary_2_t3943999495 * L_13 = (Dictionary_2_t3943999495 *)il2cpp_codegen_object_new(Dictionary_2_t3943999495_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m486600382(L_13, /*hidden argument*/Dictionary_2__ctor_m486600382_MethodInfo_var);
		NullCheck(L_12);
		GetCredentialsForIdentityRequest_set_Logins_m4103779471(L_12, L_13, /*hidden argument*/NULL);
		GetCredentialsForIdentityRequest_t932830458 * L_14 = V_1;
		NullCheck(L_14);
		Dictionary_2_t3943999495 * L_15 = GetCredentialsForIdentityRequest_get_Logins_m3877107988(L_14, /*hidden argument*/NULL);
		IdentityState_t2011390993 * L_16 = __this->get__identityState_13();
		NullCheck(L_16);
		String_t* L_17 = IdentityState_get_LoginToken_m1363466894(L_16, /*hidden argument*/NULL);
		NullCheck(L_15);
		Dictionary_2_set_Item_m4244870320(L_15, _stringLiteral3879863438, L_17, /*hidden argument*/Dictionary_2_set_Item_m4244870320_MethodInfo_var);
	}

IL_006f:
	{
		V_2 = (bool)0;
		V_3 = (GetCredentialsForIdentityResponse_t2302678766 *)NULL;
	}

IL_0073:
	try
	{ // begin try (depth: 1)
		GetCredentialsForIdentityRequest_t932830458 * L_18 = V_1;
		GetCredentialsForIdentityResponse_t2302678766 * L_19 = CognitoAWSCredentials_GetCredentialsForIdentity_m3961787566(__this, L_18, /*hidden argument*/NULL);
		V_3 = L_19;
		goto IL_0091;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (AmazonCognitoIdentityException_t158465174_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_007d;
		throw e;
	}

CATCH_007d:
	{ // begin catch(Amazon.CognitoIdentity.AmazonCognitoIdentityException)
		{
			V_5 = ((AmazonCognitoIdentityException_t158465174 *)__exception_local);
			AmazonCognitoIdentityException_t158465174 * L_20 = V_5;
			bool L_21 = CognitoAWSCredentials_ShouldRetry_m1614465732(__this, L_20, /*hidden argument*/NULL);
			if (!L_21)
			{
				goto IL_008d;
			}
		}

IL_0089:
		{
			V_2 = (bool)1;
			goto IL_008f;
		}

IL_008d:
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(__exception_local);
		}

IL_008f:
		{
			goto IL_0091;
		}
	} // end catch (depth: 1)

IL_0091:
	{
		bool L_22 = V_2;
		if (!L_22)
		{
			goto IL_009b;
		}
	}
	{
		CredentialsRefreshState_t3294867821 * L_23 = CognitoAWSCredentials_GetPoolCredentials_m3438202962(__this, /*hidden argument*/NULL);
		return L_23;
	}

IL_009b:
	{
		GetCredentialsForIdentityResponse_t2302678766 * L_24 = V_3;
		NullCheck(L_24);
		String_t* L_25 = GetCredentialsForIdentityResponse_get_IdentityId_m2716953226(L_24, /*hidden argument*/NULL);
		CognitoAWSCredentials_UpdateIdentity_m833388265(__this, L_25, /*hidden argument*/NULL);
		GetCredentialsForIdentityResponse_t2302678766 * L_26 = V_3;
		NullCheck(L_26);
		Credentials_t792472136 * L_27 = GetCredentialsForIdentityResponse_get_Credentials_m2722355219(L_26, /*hidden argument*/NULL);
		V_4 = L_27;
		Credentials_t792472136 * L_28 = V_4;
		NullCheck(L_28);
		ImmutableCredentials_t282353664 * L_29 = VirtFuncInvoker0< ImmutableCredentials_t282353664 * >::Invoke(4 /* Amazon.Runtime.ImmutableCredentials Amazon.Runtime.AWSCredentials::GetCredentials() */, L_28);
		Credentials_t792472136 * L_30 = V_4;
		NullCheck(L_30);
		DateTime_t693205669  L_31 = Credentials_get_Expiration_m1990969830(L_30, /*hidden argument*/NULL);
		CredentialsRefreshState_t3294867821 * L_32 = (CredentialsRefreshState_t3294867821 *)il2cpp_codegen_object_new(CredentialsRefreshState_t3294867821_il2cpp_TypeInfo_var);
		CredentialsRefreshState__ctor_m2847911208(L_32, L_29, L_31, /*hidden argument*/NULL);
		return L_32;
	}
}
// Amazon.Runtime.RefreshingAWSCredentials/CredentialsRefreshState Amazon.CognitoIdentity.CognitoAWSCredentials::GetCredentialsForRole(System.String)
extern Il2CppClass* GetOpenIdTokenRequest_t2698079901_il2cpp_TypeInfo_var;
extern Il2CppClass* AmazonCognitoIdentityException_t158465174_il2cpp_TypeInfo_var;
extern Il2CppClass* AssumeRoleWithWebIdentityRequest_t193094215_il2cpp_TypeInfo_var;
extern Il2CppClass* CognitoAWSCredentials_t2370264792_il2cpp_TypeInfo_var;
extern Il2CppClass* CredentialsRefreshState_t3294867821_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_get_Count_m2692997481_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4125962840;
extern const uint32_t CognitoAWSCredentials_GetCredentialsForRole_m3530527127_MetadataUsageId;
extern "C"  CredentialsRefreshState_t3294867821 * CognitoAWSCredentials_GetCredentialsForRole_m3530527127 (CognitoAWSCredentials_t2370264792 * __this, String_t* ___roleArn0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CognitoAWSCredentials_GetCredentialsForRole_m3530527127_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	GetOpenIdTokenRequest_t2698079901 * V_1 = NULL;
	bool V_2 = false;
	GetOpenIdTokenResponse_t955597635 * V_3 = NULL;
	String_t* V_4 = NULL;
	AssumeRoleWithWebIdentityRequest_t193094215 * V_5 = NULL;
	Credentials_t3554032640 * V_6 = NULL;
	AmazonCognitoIdentityException_t158465174 * V_7 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = CognitoAWSCredentials_GetIdentityId_m1939085957(__this, 1, /*hidden argument*/NULL);
		V_0 = L_0;
		GetOpenIdTokenRequest_t2698079901 * L_1 = (GetOpenIdTokenRequest_t2698079901 *)il2cpp_codegen_object_new(GetOpenIdTokenRequest_t2698079901_il2cpp_TypeInfo_var);
		GetOpenIdTokenRequest__ctor_m2858717872(L_1, /*hidden argument*/NULL);
		GetOpenIdTokenRequest_t2698079901 * L_2 = L_1;
		String_t* L_3 = V_0;
		NullCheck(L_2);
		GetOpenIdTokenRequest_set_IdentityId_m3421098958(L_2, L_3, /*hidden argument*/NULL);
		V_1 = L_2;
		Dictionary_2_t3943999495 * L_4 = CognitoAWSCredentials_get_Logins_m1002484841(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		int32_t L_5 = Dictionary_2_get_Count_m2692997481(L_4, /*hidden argument*/Dictionary_2_get_Count_m2692997481_MethodInfo_var);
		if ((((int32_t)L_5) <= ((int32_t)0)))
		{
			goto IL_002f;
		}
	}
	{
		GetOpenIdTokenRequest_t2698079901 * L_6 = V_1;
		Dictionary_2_t3943999495 * L_7 = CognitoAWSCredentials_get_Logins_m1002484841(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		GetOpenIdTokenRequest_set_Logins_m2054782372(L_6, L_7, /*hidden argument*/NULL);
	}

IL_002f:
	{
		V_2 = (bool)0;
		V_3 = (GetOpenIdTokenResponse_t955597635 *)NULL;
	}

IL_0033:
	try
	{ // begin try (depth: 1)
		GetOpenIdTokenRequest_t2698079901 * L_8 = V_1;
		GetOpenIdTokenResponse_t955597635 * L_9 = CognitoAWSCredentials_GetOpenId_m3568919528(__this, L_8, /*hidden argument*/NULL);
		V_3 = L_9;
		goto IL_0051;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (AmazonCognitoIdentityException_t158465174_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_003d;
		throw e;
	}

CATCH_003d:
	{ // begin catch(Amazon.CognitoIdentity.AmazonCognitoIdentityException)
		{
			V_7 = ((AmazonCognitoIdentityException_t158465174 *)__exception_local);
			AmazonCognitoIdentityException_t158465174 * L_10 = V_7;
			bool L_11 = CognitoAWSCredentials_ShouldRetry_m1614465732(__this, L_10, /*hidden argument*/NULL);
			if (!L_11)
			{
				goto IL_004d;
			}
		}

IL_0049:
		{
			V_2 = (bool)1;
			goto IL_004f;
		}

IL_004d:
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(__exception_local);
		}

IL_004f:
		{
			goto IL_0051;
		}
	} // end catch (depth: 1)

IL_0051:
	{
		bool L_12 = V_2;
		if (!L_12)
		{
			goto IL_005c;
		}
	}
	{
		String_t* L_13 = ___roleArn0;
		CredentialsRefreshState_t3294867821 * L_14 = CognitoAWSCredentials_GetCredentialsForRole_m3530527127(__this, L_13, /*hidden argument*/NULL);
		return L_14;
	}

IL_005c:
	{
		GetOpenIdTokenResponse_t955597635 * L_15 = V_3;
		NullCheck(L_15);
		String_t* L_16 = GetOpenIdTokenResponse_get_Token_m2941493527(L_15, /*hidden argument*/NULL);
		V_4 = L_16;
		GetOpenIdTokenResponse_t955597635 * L_17 = V_3;
		NullCheck(L_17);
		String_t* L_18 = GetOpenIdTokenResponse_get_IdentityId_m146036971(L_17, /*hidden argument*/NULL);
		CognitoAWSCredentials_UpdateIdentity_m833388265(__this, L_18, /*hidden argument*/NULL);
		AssumeRoleWithWebIdentityRequest_t193094215 * L_19 = (AssumeRoleWithWebIdentityRequest_t193094215 *)il2cpp_codegen_object_new(AssumeRoleWithWebIdentityRequest_t193094215_il2cpp_TypeInfo_var);
		AssumeRoleWithWebIdentityRequest__ctor_m3449302728(L_19, /*hidden argument*/NULL);
		AssumeRoleWithWebIdentityRequest_t193094215 * L_20 = L_19;
		String_t* L_21 = V_4;
		NullCheck(L_20);
		AssumeRoleWithWebIdentityRequest_set_WebIdentityToken_m951693196(L_20, L_21, /*hidden argument*/NULL);
		AssumeRoleWithWebIdentityRequest_t193094215 * L_22 = L_20;
		String_t* L_23 = ___roleArn0;
		NullCheck(L_22);
		AssumeRoleWithWebIdentityRequest_set_RoleArn_m1456092898(L_22, L_23, /*hidden argument*/NULL);
		AssumeRoleWithWebIdentityRequest_t193094215 * L_24 = L_22;
		NullCheck(L_24);
		AssumeRoleWithWebIdentityRequest_set_RoleSessionName_m3614869118(L_24, _stringLiteral4125962840, /*hidden argument*/NULL);
		AssumeRoleWithWebIdentityRequest_t193094215 * L_25 = L_24;
		IL2CPP_RUNTIME_CLASS_INIT(CognitoAWSCredentials_t2370264792_il2cpp_TypeInfo_var);
		int32_t L_26 = ((CognitoAWSCredentials_t2370264792_StaticFields*)CognitoAWSCredentials_t2370264792_il2cpp_TypeInfo_var->static_fields)->get_DefaultDurationSeconds_5();
		NullCheck(L_25);
		AssumeRoleWithWebIdentityRequest_set_DurationSeconds_m1975187011(L_25, L_26, /*hidden argument*/NULL);
		V_5 = L_25;
		AssumeRoleWithWebIdentityRequest_t193094215 * L_27 = V_5;
		Credentials_t3554032640 * L_28 = CognitoAWSCredentials_GetStsCredentials_m3127476678(__this, L_27, /*hidden argument*/NULL);
		V_6 = L_28;
		Credentials_t3554032640 * L_29 = V_6;
		NullCheck(L_29);
		ImmutableCredentials_t282353664 * L_30 = VirtFuncInvoker0< ImmutableCredentials_t282353664 * >::Invoke(4 /* Amazon.Runtime.ImmutableCredentials Amazon.Runtime.AWSCredentials::GetCredentials() */, L_29);
		Credentials_t3554032640 * L_31 = V_6;
		NullCheck(L_31);
		DateTime_t693205669  L_32 = Credentials_get_Expiration_m963783206(L_31, /*hidden argument*/NULL);
		CredentialsRefreshState_t3294867821 * L_33 = (CredentialsRefreshState_t3294867821 *)il2cpp_codegen_object_new(CredentialsRefreshState_t3294867821_il2cpp_TypeInfo_var);
		CredentialsRefreshState__ctor_m2847911208(L_33, L_30, L_32, /*hidden argument*/NULL);
		return L_33;
	}
}
// Amazon.SecurityToken.Model.Credentials Amazon.CognitoIdentity.CognitoAWSCredentials::GetStsCredentials(Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest)
extern Il2CppClass* U3CU3Ec__DisplayClass59_0_t795329086_il2cpp_TypeInfo_var;
extern Il2CppClass* AutoResetEvent_t15112628_il2cpp_TypeInfo_var;
extern Il2CppClass* AmazonServiceCallback_2_t142122009_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass59_0_U3CGetStsCredentialsU3Eb__0_m1388819973_MethodInfo_var;
extern const MethodInfo* AmazonServiceCallback_2__ctor_m3392561651_MethodInfo_var;
extern const uint32_t CognitoAWSCredentials_GetStsCredentials_m3127476678_MetadataUsageId;
extern "C"  Credentials_t3554032640 * CognitoAWSCredentials_GetStsCredentials_m3127476678 (CognitoAWSCredentials_t2370264792 * __this, AssumeRoleWithWebIdentityRequest_t193094215 * ___assumeRequest0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CognitoAWSCredentials_GetStsCredentials_m3127476678_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass59_0_t795329086 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass59_0_t795329086 * L_0 = (U3CU3Ec__DisplayClass59_0_t795329086 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass59_0_t795329086_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass59_0__ctor_m4011845581(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass59_0_t795329086 * L_1 = V_0;
		AutoResetEvent_t15112628 * L_2 = (AutoResetEvent_t15112628 *)il2cpp_codegen_object_new(AutoResetEvent_t15112628_il2cpp_TypeInfo_var);
		AutoResetEvent__ctor_m2634317352(L_2, (bool)0, /*hidden argument*/NULL);
		NullCheck(L_1);
		L_1->set_ars_2(L_2);
		U3CU3Ec__DisplayClass59_0_t795329086 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_credentials_1((Credentials_t3554032640 *)NULL);
		U3CU3Ec__DisplayClass59_0_t795329086 * L_4 = V_0;
		NullCheck(L_4);
		L_4->set_exception_0((Exception_t1927440687 *)NULL);
		AmazonSecurityTokenServiceClient_t1241355389 * L_5 = __this->get_sts_7();
		AssumeRoleWithWebIdentityRequest_t193094215 * L_6 = ___assumeRequest0;
		U3CU3Ec__DisplayClass59_0_t795329086 * L_7 = V_0;
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass59_0_U3CGetStsCredentialsU3Eb__0_m1388819973_MethodInfo_var);
		AmazonServiceCallback_2_t142122009 * L_9 = (AmazonServiceCallback_2_t142122009 *)il2cpp_codegen_object_new(AmazonServiceCallback_2_t142122009_il2cpp_TypeInfo_var);
		AmazonServiceCallback_2__ctor_m3392561651(L_9, L_7, L_8, /*hidden argument*/AmazonServiceCallback_2__ctor_m3392561651_MethodInfo_var);
		NullCheck(L_5);
		AmazonSecurityTokenServiceClient_AssumeRoleWithWebIdentityAsync_m255795346(L_5, L_6, L_9, (AsyncOptions_t558351272 *)NULL, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass59_0_t795329086 * L_10 = V_0;
		NullCheck(L_10);
		AutoResetEvent_t15112628 * L_11 = L_10->get_ars_2();
		NullCheck(L_11);
		VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Threading.WaitHandle::WaitOne() */, L_11);
		U3CU3Ec__DisplayClass59_0_t795329086 * L_12 = V_0;
		NullCheck(L_12);
		Exception_t1927440687 * L_13 = L_12->get_exception_0();
		if (!L_13)
		{
			goto IL_0054;
		}
	}
	{
		U3CU3Ec__DisplayClass59_0_t795329086 * L_14 = V_0;
		NullCheck(L_14);
		Exception_t1927440687 * L_15 = L_14->get_exception_0();
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_15);
	}

IL_0054:
	{
		U3CU3Ec__DisplayClass59_0_t795329086 * L_16 = V_0;
		NullCheck(L_16);
		Credentials_t3554032640 * L_17 = L_16->get_credentials_1();
		return L_17;
	}
}
// Amazon.CognitoIdentity.Model.GetCredentialsForIdentityResponse Amazon.CognitoIdentity.CognitoAWSCredentials::GetCredentialsForIdentity(Amazon.CognitoIdentity.Model.GetCredentialsForIdentityRequest)
extern "C"  GetCredentialsForIdentityResponse_t2302678766 * CognitoAWSCredentials_GetCredentialsForIdentity_m3961787566 (CognitoAWSCredentials_t2370264792 * __this, GetCredentialsForIdentityRequest_t932830458 * ___getCredentialsRequest0, const MethodInfo* method)
{
	{
		AmazonCognitoIdentityClient_t3069350888 * L_0 = __this->get_cib_6();
		GetCredentialsForIdentityRequest_t932830458 * L_1 = ___getCredentialsRequest0;
		NullCheck(L_0);
		GetCredentialsForIdentityResponse_t2302678766 * L_2 = AmazonCognitoIdentityClient_GetCredentialsForIdentity_m4012515634(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// Amazon.CognitoIdentity.Model.GetOpenIdTokenResponse Amazon.CognitoIdentity.CognitoAWSCredentials::GetOpenId(Amazon.CognitoIdentity.Model.GetOpenIdTokenRequest)
extern "C"  GetOpenIdTokenResponse_t955597635 * CognitoAWSCredentials_GetOpenId_m3568919528 (CognitoAWSCredentials_t2370264792 * __this, GetOpenIdTokenRequest_t2698079901 * ___getTokenRequest0, const MethodInfo* method)
{
	{
		AmazonCognitoIdentityClient_t3069350888 * L_0 = __this->get_cib_6();
		GetOpenIdTokenRequest_t2698079901 * L_1 = ___getTokenRequest0;
		NullCheck(L_0);
		GetOpenIdTokenResponse_t955597635 * L_2 = AmazonCognitoIdentityClient_GetOpenIdToken_m1574299933(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.String Amazon.CognitoIdentity.CognitoAWSCredentials::GetCachedIdentityId()
extern Il2CppClass* ServiceFactory_t589839913_il2cpp_TypeInfo_var;
extern Il2CppClass* CognitoAWSCredentials_t2370264792_il2cpp_TypeInfo_var;
extern Il2CppClass* IApplicationSettings_t2849513616_il2cpp_TypeInfo_var;
extern const MethodInfo* ServiceFactory_GetService_TisIApplicationSettings_t2849513616_m1342956672_MethodInfo_var;
extern const uint32_t CognitoAWSCredentials_GetCachedIdentityId_m4263931988_MetadataUsageId;
extern "C"  String_t* CognitoAWSCredentials_GetCachedIdentityId_m4263931988 (CognitoAWSCredentials_t2370264792 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CognitoAWSCredentials_GetCachedIdentityId_m4263931988_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ServiceFactory_t589839913_il2cpp_TypeInfo_var);
		ServiceFactory_t589839913 * L_0 = ((ServiceFactory_t589839913_StaticFields*)ServiceFactory_t589839913_il2cpp_TypeInfo_var->static_fields)->get_Instance_5();
		NullCheck(L_0);
		Il2CppObject * L_1 = ServiceFactory_GetService_TisIApplicationSettings_t2849513616_m1342956672(L_0, /*hidden argument*/ServiceFactory_GetService_TisIApplicationSettings_t2849513616_m1342956672_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(CognitoAWSCredentials_t2370264792_il2cpp_TypeInfo_var);
		String_t* L_2 = ((CognitoAWSCredentials_t2370264792_StaticFields*)CognitoAWSCredentials_t2370264792_il2cpp_TypeInfo_var->static_fields)->get_IDENTITY_ID_CACHE_KEY_15();
		String_t* L_3 = CognitoAWSCredentials_GetNamespacedKey_m3390205149(__this, L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_4 = InterfaceFuncInvoker2< String_t*, String_t*, int32_t >::Invoke(1 /* System.String Amazon.Util.Internal.PlatformServices.IApplicationSettings::GetValue(System.String,Amazon.Util.Internal.PlatformServices.ApplicationSettingsMode) */, IApplicationSettings_t2849513616_il2cpp_TypeInfo_var, L_1, L_3, 1);
		return L_4;
	}
}
// System.Void Amazon.CognitoIdentity.CognitoAWSCredentials::CacheIdentityId(System.String)
extern Il2CppClass* ServiceFactory_t589839913_il2cpp_TypeInfo_var;
extern Il2CppClass* CognitoAWSCredentials_t2370264792_il2cpp_TypeInfo_var;
extern Il2CppClass* IApplicationSettings_t2849513616_il2cpp_TypeInfo_var;
extern const MethodInfo* ServiceFactory_GetService_TisIApplicationSettings_t2849513616_m1342956672_MethodInfo_var;
extern const uint32_t CognitoAWSCredentials_CacheIdentityId_m1798499981_MetadataUsageId;
extern "C"  void CognitoAWSCredentials_CacheIdentityId_m1798499981 (CognitoAWSCredentials_t2370264792 * __this, String_t* ___identityId0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CognitoAWSCredentials_CacheIdentityId_m1798499981_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ServiceFactory_t589839913_il2cpp_TypeInfo_var);
		ServiceFactory_t589839913 * L_0 = ((ServiceFactory_t589839913_StaticFields*)ServiceFactory_t589839913_il2cpp_TypeInfo_var->static_fields)->get_Instance_5();
		NullCheck(L_0);
		Il2CppObject * L_1 = ServiceFactory_GetService_TisIApplicationSettings_t2849513616_m1342956672(L_0, /*hidden argument*/ServiceFactory_GetService_TisIApplicationSettings_t2849513616_m1342956672_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(CognitoAWSCredentials_t2370264792_il2cpp_TypeInfo_var);
		String_t* L_2 = ((CognitoAWSCredentials_t2370264792_StaticFields*)CognitoAWSCredentials_t2370264792_il2cpp_TypeInfo_var->static_fields)->get_IDENTITY_ID_CACHE_KEY_15();
		String_t* L_3 = CognitoAWSCredentials_GetNamespacedKey_m3390205149(__this, L_2, /*hidden argument*/NULL);
		String_t* L_4 = ___identityId0;
		NullCheck(L_1);
		InterfaceActionInvoker3< String_t*, String_t*, int32_t >::Invoke(0 /* System.Void Amazon.Util.Internal.PlatformServices.IApplicationSettings::SetValue(System.String,System.String,Amazon.Util.Internal.PlatformServices.ApplicationSettingsMode) */, IApplicationSettings_t2849513616_il2cpp_TypeInfo_var, L_1, L_3, L_4, 1);
		return;
	}
}
// System.Void Amazon.CognitoIdentity.CognitoAWSCredentials::ClearIdentityCache()
extern Il2CppClass* ServiceFactory_t589839913_il2cpp_TypeInfo_var;
extern Il2CppClass* CognitoAWSCredentials_t2370264792_il2cpp_TypeInfo_var;
extern Il2CppClass* IApplicationSettings_t2849513616_il2cpp_TypeInfo_var;
extern const MethodInfo* ServiceFactory_GetService_TisIApplicationSettings_t2849513616_m1342956672_MethodInfo_var;
extern const uint32_t CognitoAWSCredentials_ClearIdentityCache_m2215438351_MetadataUsageId;
extern "C"  void CognitoAWSCredentials_ClearIdentityCache_m2215438351 (CognitoAWSCredentials_t2370264792 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CognitoAWSCredentials_ClearIdentityCache_m2215438351_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ServiceFactory_t589839913_il2cpp_TypeInfo_var);
		ServiceFactory_t589839913 * L_0 = ((ServiceFactory_t589839913_StaticFields*)ServiceFactory_t589839913_il2cpp_TypeInfo_var->static_fields)->get_Instance_5();
		NullCheck(L_0);
		Il2CppObject * L_1 = ServiceFactory_GetService_TisIApplicationSettings_t2849513616_m1342956672(L_0, /*hidden argument*/ServiceFactory_GetService_TisIApplicationSettings_t2849513616_m1342956672_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(CognitoAWSCredentials_t2370264792_il2cpp_TypeInfo_var);
		String_t* L_2 = ((CognitoAWSCredentials_t2370264792_StaticFields*)CognitoAWSCredentials_t2370264792_il2cpp_TypeInfo_var->static_fields)->get_IDENTITY_ID_CACHE_KEY_15();
		String_t* L_3 = CognitoAWSCredentials_GetNamespacedKey_m3390205149(__this, L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		InterfaceActionInvoker2< String_t*, int32_t >::Invoke(2 /* System.Void Amazon.Util.Internal.PlatformServices.IApplicationSettings::RemoveValue(System.String,Amazon.Util.Internal.PlatformServices.ApplicationSettingsMode) */, IApplicationSettings_t2849513616_il2cpp_TypeInfo_var, L_1, L_3, 1);
		return;
	}
}
// System.Void Amazon.CognitoIdentity.CognitoAWSCredentials::CacheCredentials(Amazon.Runtime.RefreshingAWSCredentials/CredentialsRefreshState)
extern "C"  void CognitoAWSCredentials_CacheCredentials_m2721308407 (CognitoAWSCredentials_t2370264792 * __this, CredentialsRefreshState_t3294867821 * ___credentialsState0, const MethodInfo* method)
{
	{
		return;
	}
}
// Amazon.Runtime.RefreshingAWSCredentials/CredentialsRefreshState Amazon.CognitoIdentity.CognitoAWSCredentials::GetCachedCredentials()
extern "C"  CredentialsRefreshState_t3294867821 * CognitoAWSCredentials_GetCachedCredentials_m4289102006 (CognitoAWSCredentials_t2370264792 * __this, const MethodInfo* method)
{
	{
		return (CredentialsRefreshState_t3294867821 *)NULL;
	}
}
// System.Void Amazon.CognitoIdentity.CognitoAWSCredentials::.cctor()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* CognitoAWSCredentials_t2370264792_il2cpp_TypeInfo_var;
extern Il2CppClass* TimeSpan_t3430258949_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2261420624;
extern const uint32_t CognitoAWSCredentials__cctor_m1379474879_MetadataUsageId;
extern "C"  void CognitoAWSCredentials__cctor_m1379474879 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CognitoAWSCredentials__cctor_m1379474879_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	TimeSpan_t3430258949  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m2551263788(L_0, /*hidden argument*/NULL);
		((CognitoAWSCredentials_t2370264792_StaticFields*)CognitoAWSCredentials_t2370264792_il2cpp_TypeInfo_var->static_fields)->set_refreshIdLock_3(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t3430258949_il2cpp_TypeInfo_var);
		TimeSpan_t3430258949  L_1 = TimeSpan_FromHours_m2521548378(NULL /*static, unused*/, (1.0), /*hidden argument*/NULL);
		V_0 = L_1;
		double L_2 = TimeSpan_get_TotalSeconds_m1295026915((&V_0), /*hidden argument*/NULL);
		((CognitoAWSCredentials_t2370264792_StaticFields*)CognitoAWSCredentials_t2370264792_il2cpp_TypeInfo_var->static_fields)->set_DefaultDurationSeconds_5((((int32_t)((int32_t)L_2))));
		((CognitoAWSCredentials_t2370264792_StaticFields*)CognitoAWSCredentials_t2370264792_il2cpp_TypeInfo_var->static_fields)->set_IDENTITY_ID_CACHE_KEY_15(_stringLiteral2261420624);
		Il2CppObject * L_3 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m2551263788(L_3, /*hidden argument*/NULL);
		((CognitoAWSCredentials_t2370264792_StaticFields*)CognitoAWSCredentials_t2370264792_il2cpp_TypeInfo_var->static_fields)->set__lock_16(L_3);
		return;
	}
}
// System.Void Amazon.CognitoIdentity.CognitoAWSCredentials/<>c__DisplayClass59_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass59_0__ctor_m4011845581 (U3CU3Ec__DisplayClass59_0_t795329086 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Amazon.CognitoIdentity.CognitoAWSCredentials/<>c__DisplayClass59_0::<GetStsCredentials>b__0(Amazon.Runtime.AmazonServiceResult`2<Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest,Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityResponse>)
extern const MethodInfo* AmazonServiceResult_2_get_Exception_m2483909116_MethodInfo_var;
extern const MethodInfo* AmazonServiceResult_2_get_Response_m2633586654_MethodInfo_var;
extern const uint32_t U3CU3Ec__DisplayClass59_0_U3CGetStsCredentialsU3Eb__0_m1388819973_MetadataUsageId;
extern "C"  void U3CU3Ec__DisplayClass59_0_U3CGetStsCredentialsU3Eb__0_m1388819973 (U3CU3Ec__DisplayClass59_0_t795329086 * __this, AmazonServiceResult_2_t3957156559 * ___assumeResult0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__DisplayClass59_0_U3CGetStsCredentialsU3Eb__0_m1388819973_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AmazonServiceResult_2_t3957156559 * L_0 = ___assumeResult0;
		NullCheck(L_0);
		Exception_t1927440687 * L_1 = AmazonServiceResult_2_get_Exception_m2483909116(L_0, /*hidden argument*/AmazonServiceResult_2_get_Exception_m2483909116_MethodInfo_var);
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		AmazonServiceResult_2_t3957156559 * L_2 = ___assumeResult0;
		NullCheck(L_2);
		Exception_t1927440687 * L_3 = AmazonServiceResult_2_get_Exception_m2483909116(L_2, /*hidden argument*/AmazonServiceResult_2_get_Exception_m2483909116_MethodInfo_var);
		__this->set_exception_0(L_3);
		goto IL_0027;
	}

IL_0016:
	{
		AmazonServiceResult_2_t3957156559 * L_4 = ___assumeResult0;
		NullCheck(L_4);
		AssumeRoleWithWebIdentityResponse_t3931705881 * L_5 = AmazonServiceResult_2_get_Response_m2633586654(L_4, /*hidden argument*/AmazonServiceResult_2_get_Response_m2633586654_MethodInfo_var);
		NullCheck(L_5);
		Credentials_t3554032640 * L_6 = AssumeRoleWithWebIdentityResponse_get_Credentials_m496982176(L_5, /*hidden argument*/NULL);
		__this->set_credentials_1(L_6);
	}

IL_0027:
	{
		AutoResetEvent_t15112628 * L_7 = __this->get_ars_2();
		NullCheck(L_7);
		EventWaitHandle_Set_m2975776670(L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.String Amazon.CognitoIdentity.CognitoAWSCredentials/IdentityChangedArgs::get_OldIdentityId()
extern "C"  String_t* IdentityChangedArgs_get_OldIdentityId_m794934388 (IdentityChangedArgs_t1657401211 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3COldIdentityIdU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void Amazon.CognitoIdentity.CognitoAWSCredentials/IdentityChangedArgs::set_OldIdentityId(System.String)
extern "C"  void IdentityChangedArgs_set_OldIdentityId_m4233207127 (IdentityChangedArgs_t1657401211 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3COldIdentityIdU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String Amazon.CognitoIdentity.CognitoAWSCredentials/IdentityChangedArgs::get_NewIdentityId()
extern "C"  String_t* IdentityChangedArgs_get_NewIdentityId_m79701011 (IdentityChangedArgs_t1657401211 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CNewIdentityIdU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void Amazon.CognitoIdentity.CognitoAWSCredentials/IdentityChangedArgs::set_NewIdentityId(System.String)
extern "C"  void IdentityChangedArgs_set_NewIdentityId_m1416932598 (IdentityChangedArgs_t1657401211 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CNewIdentityIdU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void Amazon.CognitoIdentity.CognitoAWSCredentials/IdentityChangedArgs::.ctor(System.String,System.String)
extern Il2CppClass* EventArgs_t3289624707_il2cpp_TypeInfo_var;
extern const uint32_t IdentityChangedArgs__ctor_m2632388964_MetadataUsageId;
extern "C"  void IdentityChangedArgs__ctor_m2632388964 (IdentityChangedArgs_t1657401211 * __this, String_t* ___oldIdentityId0, String_t* ___newIdentityId1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IdentityChangedArgs__ctor_m2632388964_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t3289624707_il2cpp_TypeInfo_var);
		EventArgs__ctor_m3696060910(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___oldIdentityId0;
		IdentityChangedArgs_set_OldIdentityId_m4233207127(__this, L_0, /*hidden argument*/NULL);
		String_t* L_1 = ___newIdentityId1;
		IdentityChangedArgs_set_NewIdentityId_m1416932598(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String Amazon.CognitoIdentity.CognitoAWSCredentials/IdentityState::get_IdentityId()
extern "C"  String_t* IdentityState_get_IdentityId_m3454520601 (IdentityState_t2011390993 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CIdentityIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void Amazon.CognitoIdentity.CognitoAWSCredentials/IdentityState::set_IdentityId(System.String)
extern "C"  void IdentityState_set_IdentityId_m748250444 (IdentityState_t2011390993 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CIdentityIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String Amazon.CognitoIdentity.CognitoAWSCredentials/IdentityState::get_LoginProvider()
extern "C"  String_t* IdentityState_get_LoginProvider_m3629843770 (IdentityState_t2011390993 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CLoginProviderU3Ek__BackingField_1();
		return L_0;
	}
}
// System.String Amazon.CognitoIdentity.CognitoAWSCredentials/IdentityState::get_LoginToken()
extern "C"  String_t* IdentityState_get_LoginToken_m1363466894 (IdentityState_t2011390993 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CLoginTokenU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void Amazon.CognitoIdentity.CognitoAWSCredentials/IdentityState::set_FromCache(System.Boolean)
extern "C"  void IdentityState_set_FromCache_m3620589710 (IdentityState_t2011390993 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CFromCacheU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Void Amazon.CognitoIdentity.CognitoAWSCredentials/IdentityState::.ctor(System.String,System.Boolean)
extern "C"  void IdentityState__ctor_m2951748439 (IdentityState_t2011390993 * __this, String_t* ___identityId0, bool ___fromCache1, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___identityId0;
		IdentityState_set_IdentityId_m748250444(__this, L_0, /*hidden argument*/NULL);
		bool L_1 = ___fromCache1;
		IdentityState_set_FromCache_m3620589710(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Amazon.CognitoIdentity.CognitoAWSCredentials/IdentityState::get_LoginSpecified()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t IdentityState_get_LoginSpecified_m1764907814_MetadataUsageId;
extern "C"  bool IdentityState_get_LoginSpecified_m1764907814 (IdentityState_t2011390993 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IdentityState_get_LoginSpecified_m1764907814_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = IdentityState_get_LoginProvider_m3629843770(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0019;
		}
	}
	{
		String_t* L_2 = IdentityState_get_LoginToken_m1363466894(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0019:
	{
		return (bool)0;
	}
}
// Amazon.Runtime.ImmutableCredentials Amazon.CognitoIdentity.Model.Credentials::GetCredentials()
extern Il2CppClass* ImmutableCredentials_t282353664_il2cpp_TypeInfo_var;
extern const uint32_t Credentials_GetCredentials_m104013866_MetadataUsageId;
extern "C"  ImmutableCredentials_t282353664 * Credentials_GetCredentials_m104013866 (Credentials_t792472136 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Credentials_GetCredentials_m104013866_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ImmutableCredentials_t282353664 * L_0 = __this->get__credentials_0();
		if (L_0)
		{
			goto IL_0025;
		}
	}
	{
		String_t* L_1 = Credentials_get_AccessKeyId_m1608339957(__this, /*hidden argument*/NULL);
		String_t* L_2 = Credentials_get_SecretKey_m1201331976(__this, /*hidden argument*/NULL);
		String_t* L_3 = Credentials_get_SessionToken_m3926496798(__this, /*hidden argument*/NULL);
		ImmutableCredentials_t282353664 * L_4 = (ImmutableCredentials_t282353664 *)il2cpp_codegen_object_new(ImmutableCredentials_t282353664_il2cpp_TypeInfo_var);
		ImmutableCredentials__ctor_m1993825745(L_4, L_1, L_2, L_3, /*hidden argument*/NULL);
		__this->set__credentials_0(L_4);
	}

IL_0025:
	{
		ImmutableCredentials_t282353664 * L_5 = __this->get__credentials_0();
		NullCheck(L_5);
		ImmutableCredentials_t282353664 * L_6 = ImmutableCredentials_Copy_m872003133(L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.String Amazon.CognitoIdentity.Model.Credentials::get_AccessKeyId()
extern "C"  String_t* Credentials_get_AccessKeyId_m1608339957 (Credentials_t792472136 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get__accessKeyId_1();
		return L_0;
	}
}
// System.Void Amazon.CognitoIdentity.Model.Credentials::set_AccessKeyId(System.String)
extern "C"  void Credentials_set_AccessKeyId_m3762288358 (Credentials_t792472136 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set__accessKeyId_1(L_0);
		return;
	}
}
// System.DateTime Amazon.CognitoIdentity.Model.Credentials::get_Expiration()
extern const MethodInfo* Nullable_1_GetValueOrDefault_m113677951_MethodInfo_var;
extern const uint32_t Credentials_get_Expiration_m1990969830_MetadataUsageId;
extern "C"  DateTime_t693205669  Credentials_get_Expiration_m1990969830 (Credentials_t792472136 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Credentials_get_Expiration_m1990969830_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Nullable_1_t3251239280 * L_0 = __this->get_address_of__expiration_2();
		DateTime_t693205669  L_1 = Nullable_1_GetValueOrDefault_m113677951(L_0, /*hidden argument*/Nullable_1_GetValueOrDefault_m113677951_MethodInfo_var);
		return L_1;
	}
}
// System.Void Amazon.CognitoIdentity.Model.Credentials::set_Expiration(System.DateTime)
extern const MethodInfo* Nullable_1__ctor_m1750116951_MethodInfo_var;
extern const uint32_t Credentials_set_Expiration_m772139935_MetadataUsageId;
extern "C"  void Credentials_set_Expiration_m772139935 (Credentials_t792472136 * __this, DateTime_t693205669  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Credentials_set_Expiration_m772139935_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		DateTime_t693205669  L_0 = ___value0;
		Nullable_1_t3251239280  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Nullable_1__ctor_m1750116951(&L_1, L_0, /*hidden argument*/Nullable_1__ctor_m1750116951_MethodInfo_var);
		__this->set__expiration_2(L_1);
		return;
	}
}
// System.String Amazon.CognitoIdentity.Model.Credentials::get_SecretKey()
extern "C"  String_t* Credentials_get_SecretKey_m1201331976 (Credentials_t792472136 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get__secretKey_3();
		return L_0;
	}
}
// System.Void Amazon.CognitoIdentity.Model.Credentials::set_SecretKey(System.String)
extern "C"  void Credentials_set_SecretKey_m2468273701 (Credentials_t792472136 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set__secretKey_3(L_0);
		return;
	}
}
// System.String Amazon.CognitoIdentity.Model.Credentials::get_SessionToken()
extern "C"  String_t* Credentials_get_SessionToken_m3926496798 (Credentials_t792472136 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get__sessionToken_4();
		return L_0;
	}
}
// System.Void Amazon.CognitoIdentity.Model.Credentials::set_SessionToken(System.String)
extern "C"  void Credentials_set_SessionToken_m2768715957 (Credentials_t792472136 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set__sessionToken_4(L_0);
		return;
	}
}
// System.Void Amazon.CognitoIdentity.Model.Credentials::.ctor()
extern "C"  void Credentials__ctor_m190010687 (Credentials_t792472136 * __this, const MethodInfo* method)
{
	{
		AWSCredentials__ctor_m2037750724(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Amazon.CognitoIdentity.Model.ExternalServiceException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
extern "C"  void ExternalServiceException__ctor_m3707348563 (ExternalServiceException_t712152635 * __this, String_t* ___message0, Exception_t1927440687 * ___innerException1, int32_t ___errorType2, String_t* ___errorCode3, String_t* ___requestId4, int32_t ___statusCode5, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		Exception_t1927440687 * L_1 = ___innerException1;
		int32_t L_2 = ___errorType2;
		String_t* L_3 = ___errorCode3;
		String_t* L_4 = ___requestId4;
		int32_t L_5 = ___statusCode5;
		AmazonCognitoIdentityException__ctor_m1060593889(__this, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Amazon.CognitoIdentity.Model.ExternalServiceException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void ExternalServiceException__ctor_m4054344219 (ExternalServiceException_t712152635 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method)
{
	{
		SerializationInfo_t228987430 * L_0 = ___info0;
		StreamingContext_t1417235061  L_1 = ___context1;
		AmazonCognitoIdentityException__ctor_m1824076445(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String Amazon.CognitoIdentity.Model.GetCredentialsForIdentityRequest::get_CustomRoleArn()
extern "C"  String_t* GetCredentialsForIdentityRequest_get_CustomRoleArn_m2338986275 (GetCredentialsForIdentityRequest_t932830458 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get__customRoleArn_3();
		return L_0;
	}
}
// System.Boolean Amazon.CognitoIdentity.Model.GetCredentialsForIdentityRequest::IsSetCustomRoleArn()
extern "C"  bool GetCredentialsForIdentityRequest_IsSetCustomRoleArn_m3799822147 (GetCredentialsForIdentityRequest_t932830458 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get__customRoleArn_3();
		return (bool)((!(((Il2CppObject*)(String_t*)L_0) <= ((Il2CppObject*)(Il2CppObject *)NULL)))? 1 : 0);
	}
}
// System.String Amazon.CognitoIdentity.Model.GetCredentialsForIdentityRequest::get_IdentityId()
extern "C"  String_t* GetCredentialsForIdentityRequest_get_IdentityId_m2563301616 (GetCredentialsForIdentityRequest_t932830458 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get__identityId_4();
		return L_0;
	}
}
// System.Void Amazon.CognitoIdentity.Model.GetCredentialsForIdentityRequest::set_IdentityId(System.String)
extern "C"  void GetCredentialsForIdentityRequest_set_IdentityId_m1467243909 (GetCredentialsForIdentityRequest_t932830458 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set__identityId_4(L_0);
		return;
	}
}
// System.Boolean Amazon.CognitoIdentity.Model.GetCredentialsForIdentityRequest::IsSetIdentityId()
extern "C"  bool GetCredentialsForIdentityRequest_IsSetIdentityId_m3149237072 (GetCredentialsForIdentityRequest_t932830458 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get__identityId_4();
		return (bool)((!(((Il2CppObject*)(String_t*)L_0) <= ((Il2CppObject*)(Il2CppObject *)NULL)))? 1 : 0);
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.String> Amazon.CognitoIdentity.Model.GetCredentialsForIdentityRequest::get_Logins()
extern "C"  Dictionary_2_t3943999495 * GetCredentialsForIdentityRequest_get_Logins_m3877107988 (GetCredentialsForIdentityRequest_t932830458 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t3943999495 * L_0 = __this->get__logins_5();
		return L_0;
	}
}
// System.Void Amazon.CognitoIdentity.Model.GetCredentialsForIdentityRequest::set_Logins(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void GetCredentialsForIdentityRequest_set_Logins_m4103779471 (GetCredentialsForIdentityRequest_t932830458 * __this, Dictionary_2_t3943999495 * ___value0, const MethodInfo* method)
{
	{
		Dictionary_2_t3943999495 * L_0 = ___value0;
		__this->set__logins_5(L_0);
		return;
	}
}
// System.Boolean Amazon.CognitoIdentity.Model.GetCredentialsForIdentityRequest::IsSetLogins()
extern const MethodInfo* Dictionary_2_get_Count_m2692997481_MethodInfo_var;
extern const uint32_t GetCredentialsForIdentityRequest_IsSetLogins_m952199241_MetadataUsageId;
extern "C"  bool GetCredentialsForIdentityRequest_IsSetLogins_m952199241 (GetCredentialsForIdentityRequest_t932830458 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GetCredentialsForIdentityRequest_IsSetLogins_m952199241_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t3943999495 * L_0 = __this->get__logins_5();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Dictionary_2_t3943999495 * L_1 = __this->get__logins_5();
		NullCheck(L_1);
		int32_t L_2 = Dictionary_2_get_Count_m2692997481(L_1, /*hidden argument*/Dictionary_2_get_Count_m2692997481_MethodInfo_var);
		return (bool)((((int32_t)L_2) > ((int32_t)0))? 1 : 0);
	}

IL_0017:
	{
		return (bool)0;
	}
}
// System.Void Amazon.CognitoIdentity.Model.GetCredentialsForIdentityRequest::.ctor()
extern Il2CppClass* Dictionary_2_t3943999495_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m486600382_MethodInfo_var;
extern const uint32_t GetCredentialsForIdentityRequest__ctor_m1038175453_MetadataUsageId;
extern "C"  void GetCredentialsForIdentityRequest__ctor_m1038175453 (GetCredentialsForIdentityRequest_t932830458 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GetCredentialsForIdentityRequest__ctor_m1038175453_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t3943999495 * L_0 = (Dictionary_2_t3943999495 *)il2cpp_codegen_object_new(Dictionary_2_t3943999495_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m486600382(L_0, /*hidden argument*/Dictionary_2__ctor_m486600382_MethodInfo_var);
		__this->set__logins_5(L_0);
		AmazonCognitoIdentityRequest__ctor_m2566095720(__this, /*hidden argument*/NULL);
		return;
	}
}
// Amazon.CognitoIdentity.Model.Credentials Amazon.CognitoIdentity.Model.GetCredentialsForIdentityResponse::get_Credentials()
extern "C"  Credentials_t792472136 * GetCredentialsForIdentityResponse_get_Credentials_m2722355219 (GetCredentialsForIdentityResponse_t2302678766 * __this, const MethodInfo* method)
{
	{
		Credentials_t792472136 * L_0 = __this->get__credentials_3();
		return L_0;
	}
}
// System.Void Amazon.CognitoIdentity.Model.GetCredentialsForIdentityResponse::set_Credentials(Amazon.CognitoIdentity.Model.Credentials)
extern "C"  void GetCredentialsForIdentityResponse_set_Credentials_m823810448 (GetCredentialsForIdentityResponse_t2302678766 * __this, Credentials_t792472136 * ___value0, const MethodInfo* method)
{
	{
		Credentials_t792472136 * L_0 = ___value0;
		__this->set__credentials_3(L_0);
		return;
	}
}
// System.String Amazon.CognitoIdentity.Model.GetCredentialsForIdentityResponse::get_IdentityId()
extern "C"  String_t* GetCredentialsForIdentityResponse_get_IdentityId_m2716953226 (GetCredentialsForIdentityResponse_t2302678766 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get__identityId_4();
		return L_0;
	}
}
// System.Void Amazon.CognitoIdentity.Model.GetCredentialsForIdentityResponse::set_IdentityId(System.String)
extern "C"  void GetCredentialsForIdentityResponse_set_IdentityId_m2807465299 (GetCredentialsForIdentityResponse_t2302678766 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set__identityId_4(L_0);
		return;
	}
}
// System.Void Amazon.CognitoIdentity.Model.GetCredentialsForIdentityResponse::.ctor()
extern "C"  void GetCredentialsForIdentityResponse__ctor_m2472739811 (GetCredentialsForIdentityResponse_t2302678766 * __this, const MethodInfo* method)
{
	{
		AmazonWebServiceResponse__ctor_m3474488249(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String Amazon.CognitoIdentity.Model.GetIdRequest::get_AccountId()
extern "C"  String_t* GetIdRequest_get_AccountId_m2559012565 (GetIdRequest_t4078561340 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get__accountId_3();
		return L_0;
	}
}
// System.Void Amazon.CognitoIdentity.Model.GetIdRequest::set_AccountId(System.String)
extern "C"  void GetIdRequest_set_AccountId_m4016883484 (GetIdRequest_t4078561340 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set__accountId_3(L_0);
		return;
	}
}
// System.Boolean Amazon.CognitoIdentity.Model.GetIdRequest::IsSetAccountId()
extern "C"  bool GetIdRequest_IsSetAccountId_m2868615701 (GetIdRequest_t4078561340 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get__accountId_3();
		return (bool)((!(((Il2CppObject*)(String_t*)L_0) <= ((Il2CppObject*)(Il2CppObject *)NULL)))? 1 : 0);
	}
}
// System.String Amazon.CognitoIdentity.Model.GetIdRequest::get_IdentityPoolId()
extern "C"  String_t* GetIdRequest_get_IdentityPoolId_m2191128700 (GetIdRequest_t4078561340 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get__identityPoolId_4();
		return L_0;
	}
}
// System.Void Amazon.CognitoIdentity.Model.GetIdRequest::set_IdentityPoolId(System.String)
extern "C"  void GetIdRequest_set_IdentityPoolId_m1186750113 (GetIdRequest_t4078561340 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set__identityPoolId_4(L_0);
		return;
	}
}
// System.Boolean Amazon.CognitoIdentity.Model.GetIdRequest::IsSetIdentityPoolId()
extern "C"  bool GetIdRequest_IsSetIdentityPoolId_m213241084 (GetIdRequest_t4078561340 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get__identityPoolId_4();
		return (bool)((!(((Il2CppObject*)(String_t*)L_0) <= ((Il2CppObject*)(Il2CppObject *)NULL)))? 1 : 0);
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.String> Amazon.CognitoIdentity.Model.GetIdRequest::get_Logins()
extern "C"  Dictionary_2_t3943999495 * GetIdRequest_get_Logins_m1911199310 (GetIdRequest_t4078561340 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t3943999495 * L_0 = __this->get__logins_5();
		return L_0;
	}
}
// System.Void Amazon.CognitoIdentity.Model.GetIdRequest::set_Logins(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void GetIdRequest_set_Logins_m1388169875 (GetIdRequest_t4078561340 * __this, Dictionary_2_t3943999495 * ___value0, const MethodInfo* method)
{
	{
		Dictionary_2_t3943999495 * L_0 = ___value0;
		__this->set__logins_5(L_0);
		return;
	}
}
// System.Boolean Amazon.CognitoIdentity.Model.GetIdRequest::IsSetLogins()
extern const MethodInfo* Dictionary_2_get_Count_m2692997481_MethodInfo_var;
extern const uint32_t GetIdRequest_IsSetLogins_m529970793_MetadataUsageId;
extern "C"  bool GetIdRequest_IsSetLogins_m529970793 (GetIdRequest_t4078561340 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GetIdRequest_IsSetLogins_m529970793_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t3943999495 * L_0 = __this->get__logins_5();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Dictionary_2_t3943999495 * L_1 = __this->get__logins_5();
		NullCheck(L_1);
		int32_t L_2 = Dictionary_2_get_Count_m2692997481(L_1, /*hidden argument*/Dictionary_2_get_Count_m2692997481_MethodInfo_var);
		return (bool)((((int32_t)L_2) > ((int32_t)0))? 1 : 0);
	}

IL_0017:
	{
		return (bool)0;
	}
}
// System.Void Amazon.CognitoIdentity.Model.GetIdRequest::.ctor()
extern Il2CppClass* Dictionary_2_t3943999495_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m486600382_MethodInfo_var;
extern const uint32_t GetIdRequest__ctor_m3030190485_MetadataUsageId;
extern "C"  void GetIdRequest__ctor_m3030190485 (GetIdRequest_t4078561340 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GetIdRequest__ctor_m3030190485_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t3943999495 * L_0 = (Dictionary_2_t3943999495 *)il2cpp_codegen_object_new(Dictionary_2_t3943999495_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m486600382(L_0, /*hidden argument*/Dictionary_2__ctor_m486600382_MethodInfo_var);
		__this->set__logins_5(L_0);
		AmazonCognitoIdentityRequest__ctor_m2566095720(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String Amazon.CognitoIdentity.Model.GetIdResponse::get_IdentityId()
extern "C"  String_t* GetIdResponse_get_IdentityId_m160087976 (GetIdResponse_t2091118072 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get__identityId_3();
		return L_0;
	}
}
// System.Void Amazon.CognitoIdentity.Model.GetIdResponse::set_IdentityId(System.String)
extern "C"  void GetIdResponse_set_IdentityId_m4089638831 (GetIdResponse_t2091118072 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set__identityId_3(L_0);
		return;
	}
}
// System.Void Amazon.CognitoIdentity.Model.GetIdResponse::.ctor()
extern "C"  void GetIdResponse__ctor_m1965179359 (GetIdResponse_t2091118072 * __this, const MethodInfo* method)
{
	{
		AmazonWebServiceResponse__ctor_m3474488249(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String Amazon.CognitoIdentity.Model.GetOpenIdTokenRequest::get_IdentityId()
extern "C"  String_t* GetOpenIdTokenRequest_get_IdentityId_m4120850273 (GetOpenIdTokenRequest_t2698079901 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get__identityId_3();
		return L_0;
	}
}
// System.Void Amazon.CognitoIdentity.Model.GetOpenIdTokenRequest::set_IdentityId(System.String)
extern "C"  void GetOpenIdTokenRequest_set_IdentityId_m3421098958 (GetOpenIdTokenRequest_t2698079901 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set__identityId_3(L_0);
		return;
	}
}
// System.Boolean Amazon.CognitoIdentity.Model.GetOpenIdTokenRequest::IsSetIdentityId()
extern "C"  bool GetOpenIdTokenRequest_IsSetIdentityId_m2534485277 (GetOpenIdTokenRequest_t2698079901 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get__identityId_3();
		return (bool)((!(((Il2CppObject*)(String_t*)L_0) <= ((Il2CppObject*)(Il2CppObject *)NULL)))? 1 : 0);
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.String> Amazon.CognitoIdentity.Model.GetOpenIdTokenRequest::get_Logins()
extern "C"  Dictionary_2_t3943999495 * GetOpenIdTokenRequest_get_Logins_m3321650339 (GetOpenIdTokenRequest_t2698079901 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t3943999495 * L_0 = __this->get__logins_4();
		return L_0;
	}
}
// System.Void Amazon.CognitoIdentity.Model.GetOpenIdTokenRequest::set_Logins(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void GetOpenIdTokenRequest_set_Logins_m2054782372 (GetOpenIdTokenRequest_t2698079901 * __this, Dictionary_2_t3943999495 * ___value0, const MethodInfo* method)
{
	{
		Dictionary_2_t3943999495 * L_0 = ___value0;
		__this->set__logins_4(L_0);
		return;
	}
}
// System.Boolean Amazon.CognitoIdentity.Model.GetOpenIdTokenRequest::IsSetLogins()
extern const MethodInfo* Dictionary_2_get_Count_m2692997481_MethodInfo_var;
extern const uint32_t GetOpenIdTokenRequest_IsSetLogins_m2650968532_MetadataUsageId;
extern "C"  bool GetOpenIdTokenRequest_IsSetLogins_m2650968532 (GetOpenIdTokenRequest_t2698079901 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GetOpenIdTokenRequest_IsSetLogins_m2650968532_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t3943999495 * L_0 = __this->get__logins_4();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Dictionary_2_t3943999495 * L_1 = __this->get__logins_4();
		NullCheck(L_1);
		int32_t L_2 = Dictionary_2_get_Count_m2692997481(L_1, /*hidden argument*/Dictionary_2_get_Count_m2692997481_MethodInfo_var);
		return (bool)((((int32_t)L_2) > ((int32_t)0))? 1 : 0);
	}

IL_0017:
	{
		return (bool)0;
	}
}
// System.Void Amazon.CognitoIdentity.Model.GetOpenIdTokenRequest::.ctor()
extern Il2CppClass* Dictionary_2_t3943999495_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m486600382_MethodInfo_var;
extern const uint32_t GetOpenIdTokenRequest__ctor_m2858717872_MetadataUsageId;
extern "C"  void GetOpenIdTokenRequest__ctor_m2858717872 (GetOpenIdTokenRequest_t2698079901 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GetOpenIdTokenRequest__ctor_m2858717872_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t3943999495 * L_0 = (Dictionary_2_t3943999495 *)il2cpp_codegen_object_new(Dictionary_2_t3943999495_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m486600382(L_0, /*hidden argument*/Dictionary_2__ctor_m486600382_MethodInfo_var);
		__this->set__logins_4(L_0);
		AmazonCognitoIdentityRequest__ctor_m2566095720(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String Amazon.CognitoIdentity.Model.GetOpenIdTokenResponse::get_IdentityId()
extern "C"  String_t* GetOpenIdTokenResponse_get_IdentityId_m146036971 (GetOpenIdTokenResponse_t955597635 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get__identityId_3();
		return L_0;
	}
}
// System.Void Amazon.CognitoIdentity.Model.GetOpenIdTokenResponse::set_IdentityId(System.String)
extern "C"  void GetOpenIdTokenResponse_set_IdentityId_m1894373264 (GetOpenIdTokenResponse_t955597635 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set__identityId_3(L_0);
		return;
	}
}
// System.String Amazon.CognitoIdentity.Model.GetOpenIdTokenResponse::get_Token()
extern "C"  String_t* GetOpenIdTokenResponse_get_Token_m2941493527 (GetOpenIdTokenResponse_t955597635 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get__token_4();
		return L_0;
	}
}
// System.Void Amazon.CognitoIdentity.Model.GetOpenIdTokenResponse::set_Token(System.String)
extern "C"  void GetOpenIdTokenResponse_set_Token_m40921210 (GetOpenIdTokenResponse_t955597635 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set__token_4(L_0);
		return;
	}
}
// System.Void Amazon.CognitoIdentity.Model.GetOpenIdTokenResponse::.ctor()
extern "C"  void GetOpenIdTokenResponse__ctor_m3771882210 (GetOpenIdTokenResponse_t955597635 * __this, const MethodInfo* method)
{
	{
		AmazonWebServiceResponse__ctor_m3474488249(__this, /*hidden argument*/NULL);
		return;
	}
}
// Amazon.CognitoIdentity.Model.Credentials Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.CredentialsUnmarshaller::Amazon.Runtime.Internal.Transform.IUnmarshaller<Amazon.CognitoIdentity.Model.Credentials,Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext>.Unmarshall(Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext)
extern Il2CppClass* NotImplementedException_t2785117854_il2cpp_TypeInfo_var;
extern const uint32_t CredentialsUnmarshaller_Amazon_Runtime_Internal_Transform_IUnmarshallerU3CAmazon_CognitoIdentity_Model_CredentialsU2CAmazon_Runtime_Internal_Transform_XmlUnmarshallerContextU3E_Unmarshall_m2756927442_MetadataUsageId;
extern "C"  Credentials_t792472136 * CredentialsUnmarshaller_Amazon_Runtime_Internal_Transform_IUnmarshallerU3CAmazon_CognitoIdentity_Model_CredentialsU2CAmazon_Runtime_Internal_Transform_XmlUnmarshallerContextU3E_Unmarshall_m2756927442 (CredentialsUnmarshaller_t3086492122 * __this, XmlUnmarshallerContext_t1179575220 * ___context0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CredentialsUnmarshaller_Amazon_Runtime_Internal_Transform_IUnmarshallerU3CAmazon_CognitoIdentity_Model_CredentialsU2CAmazon_Runtime_Internal_Transform_XmlUnmarshallerContextU3E_Unmarshall_m2756927442_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t2785117854 * L_0 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m808189835(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// Amazon.CognitoIdentity.Model.Credentials Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.CredentialsUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern Il2CppClass* Credentials_t792472136_il2cpp_TypeInfo_var;
extern Il2CppClass* StringUnmarshaller_t3953260147_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTimeUnmarshaller_t1723598451_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3375931278;
extern Il2CppCodeGenString* _stringLiteral3212374675;
extern Il2CppCodeGenString* _stringLiteral816877457;
extern Il2CppCodeGenString* _stringLiteral790267625;
extern const uint32_t CredentialsUnmarshaller_Unmarshall_m1741641120_MetadataUsageId;
extern "C"  Credentials_t792472136 * CredentialsUnmarshaller_Unmarshall_m1741641120 (CredentialsUnmarshaller_t3086492122 * __this, JsonUnmarshallerContext_t456235889 * ___context0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CredentialsUnmarshaller_Unmarshall_m1741641120_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Credentials_t792472136 * V_0 = NULL;
	int32_t V_1 = 0;
	StringUnmarshaller_t3953260147 * V_2 = NULL;
	DateTimeUnmarshaller_t1723598451 * V_3 = NULL;
	StringUnmarshaller_t3953260147 * V_4 = NULL;
	StringUnmarshaller_t3953260147 * V_5 = NULL;
	{
		JsonUnmarshallerContext_t456235889 * L_0 = ___context0;
		NullCheck(L_0);
		VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean Amazon.Runtime.Internal.Transform.UnmarshallerContext::Read() */, L_0);
		JsonUnmarshallerContext_t456235889 * L_1 = ___context0;
		NullCheck(L_1);
		int32_t L_2 = JsonUnmarshallerContext_get_CurrentTokenType_m2873167924(L_1, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_2) == ((uint32_t)((int32_t)13)))))
		{
			goto IL_0013;
		}
	}
	{
		return (Credentials_t792472136 *)NULL;
	}

IL_0013:
	{
		Credentials_t792472136 * L_3 = (Credentials_t792472136 *)il2cpp_codegen_object_new(Credentials_t792472136_il2cpp_TypeInfo_var);
		Credentials__ctor_m190010687(L_3, /*hidden argument*/NULL);
		V_0 = L_3;
		JsonUnmarshallerContext_t456235889 * L_4 = ___context0;
		NullCheck(L_4);
		int32_t L_5 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 Amazon.Runtime.Internal.Transform.UnmarshallerContext::get_CurrentDepth() */, L_4);
		V_1 = L_5;
		goto IL_00b3;
	}

IL_0025:
	{
		JsonUnmarshallerContext_t456235889 * L_6 = ___context0;
		int32_t L_7 = V_1;
		NullCheck(L_6);
		bool L_8 = UnmarshallerContext_TestExpression_m3665247446(L_6, _stringLiteral3375931278, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0048;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(StringUnmarshaller_t3953260147_il2cpp_TypeInfo_var);
		StringUnmarshaller_t3953260147 * L_9 = StringUnmarshaller_get_Instance_m107503370(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = L_9;
		Credentials_t792472136 * L_10 = V_0;
		StringUnmarshaller_t3953260147 * L_11 = V_2;
		JsonUnmarshallerContext_t456235889 * L_12 = ___context0;
		NullCheck(L_11);
		String_t* L_13 = StringUnmarshaller_Unmarshall_m241138027(L_11, L_12, /*hidden argument*/NULL);
		NullCheck(L_10);
		Credentials_set_AccessKeyId_m3762288358(L_10, L_13, /*hidden argument*/NULL);
		goto IL_00b3;
	}

IL_0048:
	{
		JsonUnmarshallerContext_t456235889 * L_14 = ___context0;
		int32_t L_15 = V_1;
		NullCheck(L_14);
		bool L_16 = UnmarshallerContext_TestExpression_m3665247446(L_14, _stringLiteral3212374675, L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_006b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DateTimeUnmarshaller_t1723598451_il2cpp_TypeInfo_var);
		DateTimeUnmarshaller_t1723598451 * L_17 = DateTimeUnmarshaller_get_Instance_m2045152802(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_3 = L_17;
		Credentials_t792472136 * L_18 = V_0;
		DateTimeUnmarshaller_t1723598451 * L_19 = V_3;
		JsonUnmarshallerContext_t456235889 * L_20 = ___context0;
		NullCheck(L_19);
		DateTime_t693205669  L_21 = DateTimeUnmarshaller_Unmarshall_m1465130531(L_19, L_20, /*hidden argument*/NULL);
		NullCheck(L_18);
		Credentials_set_Expiration_m772139935(L_18, L_21, /*hidden argument*/NULL);
		goto IL_00b3;
	}

IL_006b:
	{
		JsonUnmarshallerContext_t456235889 * L_22 = ___context0;
		int32_t L_23 = V_1;
		NullCheck(L_22);
		bool L_24 = UnmarshallerContext_TestExpression_m3665247446(L_22, _stringLiteral816877457, L_23, /*hidden argument*/NULL);
		if (!L_24)
		{
			goto IL_0090;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(StringUnmarshaller_t3953260147_il2cpp_TypeInfo_var);
		StringUnmarshaller_t3953260147 * L_25 = StringUnmarshaller_get_Instance_m107503370(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_4 = L_25;
		Credentials_t792472136 * L_26 = V_0;
		StringUnmarshaller_t3953260147 * L_27 = V_4;
		JsonUnmarshallerContext_t456235889 * L_28 = ___context0;
		NullCheck(L_27);
		String_t* L_29 = StringUnmarshaller_Unmarshall_m241138027(L_27, L_28, /*hidden argument*/NULL);
		NullCheck(L_26);
		Credentials_set_SecretKey_m2468273701(L_26, L_29, /*hidden argument*/NULL);
		goto IL_00b3;
	}

IL_0090:
	{
		JsonUnmarshallerContext_t456235889 * L_30 = ___context0;
		int32_t L_31 = V_1;
		NullCheck(L_30);
		bool L_32 = UnmarshallerContext_TestExpression_m3665247446(L_30, _stringLiteral790267625, L_31, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_00b3;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(StringUnmarshaller_t3953260147_il2cpp_TypeInfo_var);
		StringUnmarshaller_t3953260147 * L_33 = StringUnmarshaller_get_Instance_m107503370(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_5 = L_33;
		Credentials_t792472136 * L_34 = V_0;
		StringUnmarshaller_t3953260147 * L_35 = V_5;
		JsonUnmarshallerContext_t456235889 * L_36 = ___context0;
		NullCheck(L_35);
		String_t* L_37 = StringUnmarshaller_Unmarshall_m241138027(L_35, L_36, /*hidden argument*/NULL);
		NullCheck(L_34);
		Credentials_set_SessionToken_m2768715957(L_34, L_37, /*hidden argument*/NULL);
	}

IL_00b3:
	{
		JsonUnmarshallerContext_t456235889 * L_38 = ___context0;
		int32_t L_39 = V_1;
		NullCheck(L_38);
		bool L_40 = UnmarshallerContext_ReadAtDepth_m2943457426(L_38, L_39, /*hidden argument*/NULL);
		if (L_40)
		{
			goto IL_0025;
		}
	}
	{
		Credentials_t792472136 * L_41 = V_0;
		return L_41;
	}
}
// Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.CredentialsUnmarshaller Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.CredentialsUnmarshaller::get_Instance()
extern Il2CppClass* CredentialsUnmarshaller_t3086492122_il2cpp_TypeInfo_var;
extern const uint32_t CredentialsUnmarshaller_get_Instance_m1500740394_MetadataUsageId;
extern "C"  CredentialsUnmarshaller_t3086492122 * CredentialsUnmarshaller_get_Instance_m1500740394 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CredentialsUnmarshaller_get_Instance_m1500740394_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CredentialsUnmarshaller_t3086492122_il2cpp_TypeInfo_var);
		CredentialsUnmarshaller_t3086492122 * L_0 = ((CredentialsUnmarshaller_t3086492122_StaticFields*)CredentialsUnmarshaller_t3086492122_il2cpp_TypeInfo_var->static_fields)->get__instance_0();
		return L_0;
	}
}
// System.Void Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.CredentialsUnmarshaller::.ctor()
extern "C"  void CredentialsUnmarshaller__ctor_m3987899510 (CredentialsUnmarshaller_t3086492122 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.CredentialsUnmarshaller::.cctor()
extern Il2CppClass* CredentialsUnmarshaller_t3086492122_il2cpp_TypeInfo_var;
extern const uint32_t CredentialsUnmarshaller__cctor_m2327109913_MetadataUsageId;
extern "C"  void CredentialsUnmarshaller__cctor_m2327109913 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CredentialsUnmarshaller__cctor_m2327109913_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		CredentialsUnmarshaller_t3086492122 * L_0 = (CredentialsUnmarshaller_t3086492122 *)il2cpp_codegen_object_new(CredentialsUnmarshaller_t3086492122_il2cpp_TypeInfo_var);
		CredentialsUnmarshaller__ctor_m3987899510(L_0, /*hidden argument*/NULL);
		((CredentialsUnmarshaller_t3086492122_StaticFields*)CredentialsUnmarshaller_t3086492122_il2cpp_TypeInfo_var->static_fields)->set__instance_0(L_0);
		return;
	}
}
// Amazon.Runtime.Internal.IRequest Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetCredentialsForIdentityRequestMarshaller::Marshall(Amazon.Runtime.AmazonWebServiceRequest)
extern Il2CppClass* GetCredentialsForIdentityRequest_t932830458_il2cpp_TypeInfo_var;
extern const uint32_t GetCredentialsForIdentityRequestMarshaller_Marshall_m225982315_MetadataUsageId;
extern "C"  Il2CppObject * GetCredentialsForIdentityRequestMarshaller_Marshall_m225982315 (GetCredentialsForIdentityRequestMarshaller_t3584137889 * __this, AmazonWebServiceRequest_t3384026212 * ___input0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GetCredentialsForIdentityRequestMarshaller_Marshall_m225982315_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AmazonWebServiceRequest_t3384026212 * L_0 = ___input0;
		Il2CppObject * L_1 = GetCredentialsForIdentityRequestMarshaller_Marshall_m722107579(__this, ((GetCredentialsForIdentityRequest_t932830458 *)CastclassClass(L_0, GetCredentialsForIdentityRequest_t932830458_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return L_1;
	}
}
// Amazon.Runtime.Internal.IRequest Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetCredentialsForIdentityRequestMarshaller::Marshall(Amazon.CognitoIdentity.Model.GetCredentialsForIdentityRequest)
extern Il2CppClass* DefaultRequest_t3080757440_il2cpp_TypeInfo_var;
extern Il2CppClass* IRequest_t2400804350_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_2_t1943082916_il2cpp_TypeInfo_var;
extern Il2CppClass* CultureInfo_t3500843524_il2cpp_TypeInfo_var;
extern Il2CppClass* StringWriter_t4139609088_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonWriter_t3014444111_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonMarshallerContext_t4063314926_il2cpp_TypeInfo_var;
extern Il2CppClass* Encoding_t663144255_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_GetEnumerator_m2895728349_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1989408781_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m1372024679_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m1710042386_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m4005245300_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m882561911_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2667713841;
extern Il2CppCodeGenString* _stringLiteral1406409462;
extern Il2CppCodeGenString* _stringLiteral3012313031;
extern Il2CppCodeGenString* _stringLiteral1048821954;
extern Il2CppCodeGenString* _stringLiteral1711157076;
extern Il2CppCodeGenString* _stringLiteral782856060;
extern Il2CppCodeGenString* _stringLiteral372029315;
extern Il2CppCodeGenString* _stringLiteral2049303488;
extern Il2CppCodeGenString* _stringLiteral2345627623;
extern Il2CppCodeGenString* _stringLiteral2335677872;
extern const uint32_t GetCredentialsForIdentityRequestMarshaller_Marshall_m722107579_MetadataUsageId;
extern "C"  Il2CppObject * GetCredentialsForIdentityRequestMarshaller_Marshall_m722107579 (GetCredentialsForIdentityRequestMarshaller_t3584137889 * __this, GetCredentialsForIdentityRequest_t932830458 * ___publicRequest0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GetCredentialsForIdentityRequestMarshaller_Marshall_m722107579_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	String_t* V_1 = NULL;
	String_t* V_2 = NULL;
	StringWriter_t4139609088 * V_3 = NULL;
	JsonWriter_t3014444111 * V_4 = NULL;
	JsonMarshallerContext_t4063314926 * V_5 = NULL;
	String_t* V_6 = NULL;
	Enumerator_t969056901  V_7;
	memset(&V_7, 0, sizeof(V_7));
	KeyValuePair_2_t1701344717  V_8;
	memset(&V_8, 0, sizeof(V_8));
	String_t* V_9 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		GetCredentialsForIdentityRequest_t932830458 * L_0 = ___publicRequest0;
		DefaultRequest_t3080757440 * L_1 = (DefaultRequest_t3080757440 *)il2cpp_codegen_object_new(DefaultRequest_t3080757440_il2cpp_TypeInfo_var);
		DefaultRequest__ctor_m3534427872(L_1, L_0, _stringLiteral2667713841, /*hidden argument*/NULL);
		V_0 = L_1;
		V_1 = _stringLiteral1406409462;
		Il2CppObject * L_2 = V_0;
		NullCheck(L_2);
		Il2CppObject* L_3 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(1 /* System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.Runtime.Internal.IRequest::get_Headers() */, IRequest_t2400804350_il2cpp_TypeInfo_var, L_2);
		String_t* L_4 = V_1;
		NullCheck(L_3);
		InterfaceActionInvoker2< String_t*, String_t* >::Invoke(5 /* System.Void System.Collections.Generic.IDictionary`2<System.String,System.String>::set_Item(!0,!1) */, IDictionary_2_t1943082916_il2cpp_TypeInfo_var, L_3, _stringLiteral3012313031, L_4);
		Il2CppObject * L_5 = V_0;
		NullCheck(L_5);
		Il2CppObject* L_6 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(1 /* System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.Runtime.Internal.IRequest::get_Headers() */, IRequest_t2400804350_il2cpp_TypeInfo_var, L_5);
		NullCheck(L_6);
		InterfaceActionInvoker2< String_t*, String_t* >::Invoke(5 /* System.Void System.Collections.Generic.IDictionary`2<System.String,System.String>::set_Item(!0,!1) */, IDictionary_2_t1943082916_il2cpp_TypeInfo_var, L_6, _stringLiteral1048821954, _stringLiteral1711157076);
		Il2CppObject * L_7 = V_0;
		NullCheck(L_7);
		InterfaceActionInvoker1< String_t* >::Invoke(7 /* System.Void Amazon.Runtime.Internal.IRequest::set_HttpMethod(System.String) */, IRequest_t2400804350_il2cpp_TypeInfo_var, L_7, _stringLiteral782856060);
		V_2 = _stringLiteral372029315;
		Il2CppObject * L_8 = V_0;
		String_t* L_9 = V_2;
		NullCheck(L_8);
		InterfaceActionInvoker1< String_t* >::Invoke(11 /* System.Void Amazon.Runtime.Internal.IRequest::set_ResourcePath(System.String) */, IRequest_t2400804350_il2cpp_TypeInfo_var, L_8, L_9);
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3500843524_il2cpp_TypeInfo_var);
		CultureInfo_t3500843524 * L_10 = CultureInfo_get_InvariantCulture_m398972276(NULL /*static, unused*/, /*hidden argument*/NULL);
		StringWriter_t4139609088 * L_11 = (StringWriter_t4139609088 *)il2cpp_codegen_object_new(StringWriter_t4139609088_il2cpp_TypeInfo_var);
		StringWriter__ctor_m3405704837(L_11, L_10, /*hidden argument*/NULL);
		V_3 = L_11;
	}

IL_005b:
	try
	{ // begin try (depth: 1)
		{
			StringWriter_t4139609088 * L_12 = V_3;
			JsonWriter_t3014444111 * L_13 = (JsonWriter_t3014444111 *)il2cpp_codegen_object_new(JsonWriter_t3014444111_il2cpp_TypeInfo_var);
			JsonWriter__ctor_m2344288487(L_13, L_12, /*hidden argument*/NULL);
			V_4 = L_13;
			JsonWriter_t3014444111 * L_14 = V_4;
			NullCheck(L_14);
			JsonWriter_WriteObjectStart_m3468068136(L_14, /*hidden argument*/NULL);
			Il2CppObject * L_15 = V_0;
			JsonWriter_t3014444111 * L_16 = V_4;
			JsonMarshallerContext_t4063314926 * L_17 = (JsonMarshallerContext_t4063314926 *)il2cpp_codegen_object_new(JsonMarshallerContext_t4063314926_il2cpp_TypeInfo_var);
			JsonMarshallerContext__ctor_m1418069536(L_17, L_15, L_16, /*hidden argument*/NULL);
			V_5 = L_17;
			GetCredentialsForIdentityRequest_t932830458 * L_18 = ___publicRequest0;
			NullCheck(L_18);
			bool L_19 = GetCredentialsForIdentityRequest_IsSetCustomRoleArn_m3799822147(L_18, /*hidden argument*/NULL);
			if (!L_19)
			{
				goto IL_009f;
			}
		}

IL_007c:
		{
			JsonMarshallerContext_t4063314926 * L_20 = V_5;
			NullCheck(L_20);
			JsonWriter_t3014444111 * L_21 = JsonMarshallerContext_get_Writer_m2343372188(L_20, /*hidden argument*/NULL);
			NullCheck(L_21);
			JsonWriter_WritePropertyName_m1171400361(L_21, _stringLiteral2049303488, /*hidden argument*/NULL);
			JsonMarshallerContext_t4063314926 * L_22 = V_5;
			NullCheck(L_22);
			JsonWriter_t3014444111 * L_23 = JsonMarshallerContext_get_Writer_m2343372188(L_22, /*hidden argument*/NULL);
			GetCredentialsForIdentityRequest_t932830458 * L_24 = ___publicRequest0;
			NullCheck(L_24);
			String_t* L_25 = GetCredentialsForIdentityRequest_get_CustomRoleArn_m2338986275(L_24, /*hidden argument*/NULL);
			NullCheck(L_23);
			JsonWriter_Write_m3974344637(L_23, L_25, /*hidden argument*/NULL);
		}

IL_009f:
		{
			GetCredentialsForIdentityRequest_t932830458 * L_26 = ___publicRequest0;
			NullCheck(L_26);
			bool L_27 = GetCredentialsForIdentityRequest_IsSetIdentityId_m3149237072(L_26, /*hidden argument*/NULL);
			if (!L_27)
			{
				goto IL_00ca;
			}
		}

IL_00a7:
		{
			JsonMarshallerContext_t4063314926 * L_28 = V_5;
			NullCheck(L_28);
			JsonWriter_t3014444111 * L_29 = JsonMarshallerContext_get_Writer_m2343372188(L_28, /*hidden argument*/NULL);
			NullCheck(L_29);
			JsonWriter_WritePropertyName_m1171400361(L_29, _stringLiteral2345627623, /*hidden argument*/NULL);
			JsonMarshallerContext_t4063314926 * L_30 = V_5;
			NullCheck(L_30);
			JsonWriter_t3014444111 * L_31 = JsonMarshallerContext_get_Writer_m2343372188(L_30, /*hidden argument*/NULL);
			GetCredentialsForIdentityRequest_t932830458 * L_32 = ___publicRequest0;
			NullCheck(L_32);
			String_t* L_33 = GetCredentialsForIdentityRequest_get_IdentityId_m2563301616(L_32, /*hidden argument*/NULL);
			NullCheck(L_31);
			JsonWriter_Write_m3974344637(L_31, L_33, /*hidden argument*/NULL);
		}

IL_00ca:
		{
			GetCredentialsForIdentityRequest_t932830458 * L_34 = ___publicRequest0;
			NullCheck(L_34);
			bool L_35 = GetCredentialsForIdentityRequest_IsSetLogins_m952199241(L_34, /*hidden argument*/NULL);
			if (!L_35)
			{
				goto IL_0159;
			}
		}

IL_00d5:
		{
			JsonMarshallerContext_t4063314926 * L_36 = V_5;
			NullCheck(L_36);
			JsonWriter_t3014444111 * L_37 = JsonMarshallerContext_get_Writer_m2343372188(L_36, /*hidden argument*/NULL);
			NullCheck(L_37);
			JsonWriter_WritePropertyName_m1171400361(L_37, _stringLiteral2335677872, /*hidden argument*/NULL);
			JsonMarshallerContext_t4063314926 * L_38 = V_5;
			NullCheck(L_38);
			JsonWriter_t3014444111 * L_39 = JsonMarshallerContext_get_Writer_m2343372188(L_38, /*hidden argument*/NULL);
			NullCheck(L_39);
			JsonWriter_WriteObjectStart_m3468068136(L_39, /*hidden argument*/NULL);
			GetCredentialsForIdentityRequest_t932830458 * L_40 = ___publicRequest0;
			NullCheck(L_40);
			Dictionary_2_t3943999495 * L_41 = GetCredentialsForIdentityRequest_get_Logins_m3877107988(L_40, /*hidden argument*/NULL);
			NullCheck(L_41);
			Enumerator_t969056901  L_42 = Dictionary_2_GetEnumerator_m2895728349(L_41, /*hidden argument*/Dictionary_2_GetEnumerator_m2895728349_MethodInfo_var);
			V_7 = L_42;
		}

IL_00ff:
		try
		{ // begin try (depth: 2)
			{
				goto IL_0134;
			}

IL_0101:
			{
				KeyValuePair_2_t1701344717  L_43 = Enumerator_get_Current_m1989408781((&V_7), /*hidden argument*/Enumerator_get_Current_m1989408781_MethodInfo_var);
				V_8 = L_43;
				JsonMarshallerContext_t4063314926 * L_44 = V_5;
				NullCheck(L_44);
				JsonWriter_t3014444111 * L_45 = JsonMarshallerContext_get_Writer_m2343372188(L_44, /*hidden argument*/NULL);
				String_t* L_46 = KeyValuePair_2_get_Key_m1372024679((&V_8), /*hidden argument*/KeyValuePair_2_get_Key_m1372024679_MethodInfo_var);
				NullCheck(L_45);
				JsonWriter_WritePropertyName_m1171400361(L_45, L_46, /*hidden argument*/NULL);
				String_t* L_47 = KeyValuePair_2_get_Value_m1710042386((&V_8), /*hidden argument*/KeyValuePair_2_get_Value_m1710042386_MethodInfo_var);
				V_9 = L_47;
				JsonMarshallerContext_t4063314926 * L_48 = V_5;
				NullCheck(L_48);
				JsonWriter_t3014444111 * L_49 = JsonMarshallerContext_get_Writer_m2343372188(L_48, /*hidden argument*/NULL);
				String_t* L_50 = V_9;
				NullCheck(L_49);
				JsonWriter_Write_m3974344637(L_49, L_50, /*hidden argument*/NULL);
			}

IL_0134:
			{
				bool L_51 = Enumerator_MoveNext_m4005245300((&V_7), /*hidden argument*/Enumerator_MoveNext_m4005245300_MethodInfo_var);
				if (L_51)
				{
					goto IL_0101;
				}
			}

IL_013d:
			{
				IL2CPP_LEAVE(0x14D, FINALLY_013f);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
			goto FINALLY_013f;
		}

FINALLY_013f:
		{ // begin finally (depth: 2)
			Enumerator_Dispose_m882561911((&V_7), /*hidden argument*/Enumerator_Dispose_m882561911_MethodInfo_var);
			IL2CPP_END_FINALLY(319)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(319)
		{
			IL2CPP_JUMP_TBL(0x14D, IL_014d)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
		}

IL_014d:
		{
			JsonMarshallerContext_t4063314926 * L_52 = V_5;
			NullCheck(L_52);
			JsonWriter_t3014444111 * L_53 = JsonMarshallerContext_get_Writer_m2343372188(L_52, /*hidden argument*/NULL);
			NullCheck(L_53);
			JsonWriter_WriteObjectEnd_m4253837441(L_53, /*hidden argument*/NULL);
		}

IL_0159:
		{
			JsonWriter_t3014444111 * L_54 = V_4;
			NullCheck(L_54);
			JsonWriter_WriteObjectEnd_m4253837441(L_54, /*hidden argument*/NULL);
			StringWriter_t4139609088 * L_55 = V_3;
			NullCheck(L_55);
			String_t* L_56 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_55);
			V_6 = L_56;
			Il2CppObject * L_57 = V_0;
			IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
			Encoding_t663144255 * L_58 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
			String_t* L_59 = V_6;
			NullCheck(L_58);
			ByteU5BU5D_t3397334013* L_60 = VirtFuncInvoker1< ByteU5BU5D_t3397334013*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_58, L_59);
			NullCheck(L_57);
			InterfaceActionInvoker1< ByteU5BU5D_t3397334013* >::Invoke(13 /* System.Void Amazon.Runtime.Internal.IRequest::set_Content(System.Byte[]) */, IRequest_t2400804350_il2cpp_TypeInfo_var, L_57, L_60);
			IL2CPP_LEAVE(0x186, FINALLY_017c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_017c;
	}

FINALLY_017c:
	{ // begin finally (depth: 1)
		{
			StringWriter_t4139609088 * L_61 = V_3;
			if (!L_61)
			{
				goto IL_0185;
			}
		}

IL_017f:
		{
			StringWriter_t4139609088 * L_62 = V_3;
			NullCheck(L_62);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_62);
		}

IL_0185:
		{
			IL2CPP_END_FINALLY(380)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(380)
	{
		IL2CPP_JUMP_TBL(0x186, IL_0186)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0186:
	{
		Il2CppObject * L_63 = V_0;
		return L_63;
	}
}
// System.Void Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetCredentialsForIdentityRequestMarshaller::.ctor()
extern "C"  void GetCredentialsForIdentityRequestMarshaller__ctor_m2870727695 (GetCredentialsForIdentityRequestMarshaller_t3584137889 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// Amazon.Runtime.AmazonWebServiceResponse Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetCredentialsForIdentityResponseUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern Il2CppClass* GetCredentialsForIdentityResponse_t2302678766_il2cpp_TypeInfo_var;
extern Il2CppClass* CredentialsUnmarshaller_t3086492122_il2cpp_TypeInfo_var;
extern Il2CppClass* StringUnmarshaller_t3953260147_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2015805848;
extern Il2CppCodeGenString* _stringLiteral2345627623;
extern const uint32_t GetCredentialsForIdentityResponseUnmarshaller_Unmarshall_m634220126_MetadataUsageId;
extern "C"  AmazonWebServiceResponse_t529043356 * GetCredentialsForIdentityResponseUnmarshaller_Unmarshall_m634220126 (GetCredentialsForIdentityResponseUnmarshaller_t2007495598 * __this, JsonUnmarshallerContext_t456235889 * ___context0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GetCredentialsForIdentityResponseUnmarshaller_Unmarshall_m634220126_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GetCredentialsForIdentityResponse_t2302678766 * V_0 = NULL;
	int32_t V_1 = 0;
	CredentialsUnmarshaller_t3086492122 * V_2 = NULL;
	StringUnmarshaller_t3953260147 * V_3 = NULL;
	{
		GetCredentialsForIdentityResponse_t2302678766 * L_0 = (GetCredentialsForIdentityResponse_t2302678766 *)il2cpp_codegen_object_new(GetCredentialsForIdentityResponse_t2302678766_il2cpp_TypeInfo_var);
		GetCredentialsForIdentityResponse__ctor_m2472739811(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		JsonUnmarshallerContext_t456235889 * L_1 = ___context0;
		NullCheck(L_1);
		VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean Amazon.Runtime.Internal.Transform.UnmarshallerContext::Read() */, L_1);
		JsonUnmarshallerContext_t456235889 * L_2 = ___context0;
		NullCheck(L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 Amazon.Runtime.Internal.Transform.UnmarshallerContext::get_CurrentDepth() */, L_2);
		V_1 = L_3;
		goto IL_005a;
	}

IL_0016:
	{
		JsonUnmarshallerContext_t456235889 * L_4 = ___context0;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		bool L_6 = UnmarshallerContext_TestExpression_m3665247446(L_4, _stringLiteral2015805848, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0039;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CredentialsUnmarshaller_t3086492122_il2cpp_TypeInfo_var);
		CredentialsUnmarshaller_t3086492122 * L_7 = CredentialsUnmarshaller_get_Instance_m1500740394(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = L_7;
		GetCredentialsForIdentityResponse_t2302678766 * L_8 = V_0;
		CredentialsUnmarshaller_t3086492122 * L_9 = V_2;
		JsonUnmarshallerContext_t456235889 * L_10 = ___context0;
		NullCheck(L_9);
		Credentials_t792472136 * L_11 = CredentialsUnmarshaller_Unmarshall_m1741641120(L_9, L_10, /*hidden argument*/NULL);
		NullCheck(L_8);
		GetCredentialsForIdentityResponse_set_Credentials_m823810448(L_8, L_11, /*hidden argument*/NULL);
		goto IL_005a;
	}

IL_0039:
	{
		JsonUnmarshallerContext_t456235889 * L_12 = ___context0;
		int32_t L_13 = V_1;
		NullCheck(L_12);
		bool L_14 = UnmarshallerContext_TestExpression_m3665247446(L_12, _stringLiteral2345627623, L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_005a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(StringUnmarshaller_t3953260147_il2cpp_TypeInfo_var);
		StringUnmarshaller_t3953260147 * L_15 = StringUnmarshaller_get_Instance_m107503370(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_3 = L_15;
		GetCredentialsForIdentityResponse_t2302678766 * L_16 = V_0;
		StringUnmarshaller_t3953260147 * L_17 = V_3;
		JsonUnmarshallerContext_t456235889 * L_18 = ___context0;
		NullCheck(L_17);
		String_t* L_19 = StringUnmarshaller_Unmarshall_m241138027(L_17, L_18, /*hidden argument*/NULL);
		NullCheck(L_16);
		GetCredentialsForIdentityResponse_set_IdentityId_m2807465299(L_16, L_19, /*hidden argument*/NULL);
	}

IL_005a:
	{
		JsonUnmarshallerContext_t456235889 * L_20 = ___context0;
		int32_t L_21 = V_1;
		NullCheck(L_20);
		bool L_22 = UnmarshallerContext_ReadAtDepth_m2943457426(L_20, L_21, /*hidden argument*/NULL);
		if (L_22)
		{
			goto IL_0016;
		}
	}
	{
		GetCredentialsForIdentityResponse_t2302678766 * L_23 = V_0;
		return L_23;
	}
}
// Amazon.Runtime.AmazonServiceException Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetCredentialsForIdentityResponseUnmarshaller::UnmarshallException(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,System.Exception,System.Net.HttpStatusCode)
extern Il2CppClass* ExternalServiceException_t712152635_il2cpp_TypeInfo_var;
extern Il2CppClass* InternalErrorException_t1116634264_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidIdentityPoolConfigurationException_t50815546_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidParameterException_t2482528107_il2cpp_TypeInfo_var;
extern Il2CppClass* NotAuthorizedException_t457234085_il2cpp_TypeInfo_var;
extern Il2CppClass* ResourceConflictException_t4018382715_il2cpp_TypeInfo_var;
extern Il2CppClass* ResourceNotFoundException_t403010890_il2cpp_TypeInfo_var;
extern Il2CppClass* TooManyRequestsException_t3276202790_il2cpp_TypeInfo_var;
extern Il2CppClass* AmazonCognitoIdentityException_t158465174_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1101196531;
extern Il2CppCodeGenString* _stringLiteral629809416;
extern Il2CppCodeGenString* _stringLiteral1803736578;
extern Il2CppCodeGenString* _stringLiteral270646655;
extern Il2CppCodeGenString* _stringLiteral4046014057;
extern Il2CppCodeGenString* _stringLiteral4089858387;
extern Il2CppCodeGenString* _stringLiteral2922393814;
extern Il2CppCodeGenString* _stringLiteral2413473506;
extern const uint32_t GetCredentialsForIdentityResponseUnmarshaller_UnmarshallException_m3198160240_MetadataUsageId;
extern "C"  AmazonServiceException_t3748559634 * GetCredentialsForIdentityResponseUnmarshaller_UnmarshallException_m3198160240 (GetCredentialsForIdentityResponseUnmarshaller_t2007495598 * __this, JsonUnmarshallerContext_t456235889 * ___context0, Exception_t1927440687 * ___innerException1, int32_t ___statusCode2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GetCredentialsForIdentityResponseUnmarshaller_UnmarshallException_m3198160240_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ErrorResponse_t3502566035 * V_0 = NULL;
	{
		JsonErrorResponseUnmarshaller_t4149926919 * L_0 = JsonErrorResponseUnmarshaller_GetInstance_m1721802307(NULL /*static, unused*/, /*hidden argument*/NULL);
		JsonUnmarshallerContext_t456235889 * L_1 = ___context0;
		NullCheck(L_0);
		ErrorResponse_t3502566035 * L_2 = JsonErrorResponseUnmarshaller_Unmarshall_m3547220927(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		ErrorResponse_t3502566035 * L_3 = V_0;
		NullCheck(L_3);
		String_t* L_4 = ErrorResponse_get_Code_m1007089050(L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0046;
		}
	}
	{
		ErrorResponse_t3502566035 * L_5 = V_0;
		NullCheck(L_5);
		String_t* L_6 = ErrorResponse_get_Code_m1007089050(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		bool L_7 = String_Equals_m2633592423(L_6, _stringLiteral1101196531, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0046;
		}
	}
	{
		ErrorResponse_t3502566035 * L_8 = V_0;
		NullCheck(L_8);
		String_t* L_9 = ErrorResponse_get_Message_m1145825032(L_8, /*hidden argument*/NULL);
		Exception_t1927440687 * L_10 = ___innerException1;
		ErrorResponse_t3502566035 * L_11 = V_0;
		NullCheck(L_11);
		int32_t L_12 = ErrorResponse_get_Type_m3388577699(L_11, /*hidden argument*/NULL);
		ErrorResponse_t3502566035 * L_13 = V_0;
		NullCheck(L_13);
		String_t* L_14 = ErrorResponse_get_Code_m1007089050(L_13, /*hidden argument*/NULL);
		ErrorResponse_t3502566035 * L_15 = V_0;
		NullCheck(L_15);
		String_t* L_16 = ErrorResponse_get_RequestId_m2536924555(L_15, /*hidden argument*/NULL);
		int32_t L_17 = ___statusCode2;
		ExternalServiceException_t712152635 * L_18 = (ExternalServiceException_t712152635 *)il2cpp_codegen_object_new(ExternalServiceException_t712152635_il2cpp_TypeInfo_var);
		ExternalServiceException__ctor_m3707348563(L_18, L_9, L_10, L_12, L_14, L_16, L_17, /*hidden argument*/NULL);
		return L_18;
	}

IL_0046:
	{
		ErrorResponse_t3502566035 * L_19 = V_0;
		NullCheck(L_19);
		String_t* L_20 = ErrorResponse_get_Code_m1007089050(L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_0080;
		}
	}
	{
		ErrorResponse_t3502566035 * L_21 = V_0;
		NullCheck(L_21);
		String_t* L_22 = ErrorResponse_get_Code_m1007089050(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		bool L_23 = String_Equals_m2633592423(L_22, _stringLiteral629809416, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_0080;
		}
	}
	{
		ErrorResponse_t3502566035 * L_24 = V_0;
		NullCheck(L_24);
		String_t* L_25 = ErrorResponse_get_Message_m1145825032(L_24, /*hidden argument*/NULL);
		Exception_t1927440687 * L_26 = ___innerException1;
		ErrorResponse_t3502566035 * L_27 = V_0;
		NullCheck(L_27);
		int32_t L_28 = ErrorResponse_get_Type_m3388577699(L_27, /*hidden argument*/NULL);
		ErrorResponse_t3502566035 * L_29 = V_0;
		NullCheck(L_29);
		String_t* L_30 = ErrorResponse_get_Code_m1007089050(L_29, /*hidden argument*/NULL);
		ErrorResponse_t3502566035 * L_31 = V_0;
		NullCheck(L_31);
		String_t* L_32 = ErrorResponse_get_RequestId_m2536924555(L_31, /*hidden argument*/NULL);
		int32_t L_33 = ___statusCode2;
		InternalErrorException_t1116634264 * L_34 = (InternalErrorException_t1116634264 *)il2cpp_codegen_object_new(InternalErrorException_t1116634264_il2cpp_TypeInfo_var);
		InternalErrorException__ctor_m586328184(L_34, L_25, L_26, L_28, L_30, L_32, L_33, /*hidden argument*/NULL);
		return L_34;
	}

IL_0080:
	{
		ErrorResponse_t3502566035 * L_35 = V_0;
		NullCheck(L_35);
		String_t* L_36 = ErrorResponse_get_Code_m1007089050(L_35, /*hidden argument*/NULL);
		if (!L_36)
		{
			goto IL_00ba;
		}
	}
	{
		ErrorResponse_t3502566035 * L_37 = V_0;
		NullCheck(L_37);
		String_t* L_38 = ErrorResponse_get_Code_m1007089050(L_37, /*hidden argument*/NULL);
		NullCheck(L_38);
		bool L_39 = String_Equals_m2633592423(L_38, _stringLiteral1803736578, /*hidden argument*/NULL);
		if (!L_39)
		{
			goto IL_00ba;
		}
	}
	{
		ErrorResponse_t3502566035 * L_40 = V_0;
		NullCheck(L_40);
		String_t* L_41 = ErrorResponse_get_Message_m1145825032(L_40, /*hidden argument*/NULL);
		Exception_t1927440687 * L_42 = ___innerException1;
		ErrorResponse_t3502566035 * L_43 = V_0;
		NullCheck(L_43);
		int32_t L_44 = ErrorResponse_get_Type_m3388577699(L_43, /*hidden argument*/NULL);
		ErrorResponse_t3502566035 * L_45 = V_0;
		NullCheck(L_45);
		String_t* L_46 = ErrorResponse_get_Code_m1007089050(L_45, /*hidden argument*/NULL);
		ErrorResponse_t3502566035 * L_47 = V_0;
		NullCheck(L_47);
		String_t* L_48 = ErrorResponse_get_RequestId_m2536924555(L_47, /*hidden argument*/NULL);
		int32_t L_49 = ___statusCode2;
		InvalidIdentityPoolConfigurationException_t50815546 * L_50 = (InvalidIdentityPoolConfigurationException_t50815546 *)il2cpp_codegen_object_new(InvalidIdentityPoolConfigurationException_t50815546_il2cpp_TypeInfo_var);
		InvalidIdentityPoolConfigurationException__ctor_m1901361182(L_50, L_41, L_42, L_44, L_46, L_48, L_49, /*hidden argument*/NULL);
		return L_50;
	}

IL_00ba:
	{
		ErrorResponse_t3502566035 * L_51 = V_0;
		NullCheck(L_51);
		String_t* L_52 = ErrorResponse_get_Code_m1007089050(L_51, /*hidden argument*/NULL);
		if (!L_52)
		{
			goto IL_00f4;
		}
	}
	{
		ErrorResponse_t3502566035 * L_53 = V_0;
		NullCheck(L_53);
		String_t* L_54 = ErrorResponse_get_Code_m1007089050(L_53, /*hidden argument*/NULL);
		NullCheck(L_54);
		bool L_55 = String_Equals_m2633592423(L_54, _stringLiteral270646655, /*hidden argument*/NULL);
		if (!L_55)
		{
			goto IL_00f4;
		}
	}
	{
		ErrorResponse_t3502566035 * L_56 = V_0;
		NullCheck(L_56);
		String_t* L_57 = ErrorResponse_get_Message_m1145825032(L_56, /*hidden argument*/NULL);
		Exception_t1927440687 * L_58 = ___innerException1;
		ErrorResponse_t3502566035 * L_59 = V_0;
		NullCheck(L_59);
		int32_t L_60 = ErrorResponse_get_Type_m3388577699(L_59, /*hidden argument*/NULL);
		ErrorResponse_t3502566035 * L_61 = V_0;
		NullCheck(L_61);
		String_t* L_62 = ErrorResponse_get_Code_m1007089050(L_61, /*hidden argument*/NULL);
		ErrorResponse_t3502566035 * L_63 = V_0;
		NullCheck(L_63);
		String_t* L_64 = ErrorResponse_get_RequestId_m2536924555(L_63, /*hidden argument*/NULL);
		int32_t L_65 = ___statusCode2;
		InvalidParameterException_t2482528107 * L_66 = (InvalidParameterException_t2482528107 *)il2cpp_codegen_object_new(InvalidParameterException_t2482528107_il2cpp_TypeInfo_var);
		InvalidParameterException__ctor_m3019345047(L_66, L_57, L_58, L_60, L_62, L_64, L_65, /*hidden argument*/NULL);
		return L_66;
	}

IL_00f4:
	{
		ErrorResponse_t3502566035 * L_67 = V_0;
		NullCheck(L_67);
		String_t* L_68 = ErrorResponse_get_Code_m1007089050(L_67, /*hidden argument*/NULL);
		if (!L_68)
		{
			goto IL_012e;
		}
	}
	{
		ErrorResponse_t3502566035 * L_69 = V_0;
		NullCheck(L_69);
		String_t* L_70 = ErrorResponse_get_Code_m1007089050(L_69, /*hidden argument*/NULL);
		NullCheck(L_70);
		bool L_71 = String_Equals_m2633592423(L_70, _stringLiteral4046014057, /*hidden argument*/NULL);
		if (!L_71)
		{
			goto IL_012e;
		}
	}
	{
		ErrorResponse_t3502566035 * L_72 = V_0;
		NullCheck(L_72);
		String_t* L_73 = ErrorResponse_get_Message_m1145825032(L_72, /*hidden argument*/NULL);
		Exception_t1927440687 * L_74 = ___innerException1;
		ErrorResponse_t3502566035 * L_75 = V_0;
		NullCheck(L_75);
		int32_t L_76 = ErrorResponse_get_Type_m3388577699(L_75, /*hidden argument*/NULL);
		ErrorResponse_t3502566035 * L_77 = V_0;
		NullCheck(L_77);
		String_t* L_78 = ErrorResponse_get_Code_m1007089050(L_77, /*hidden argument*/NULL);
		ErrorResponse_t3502566035 * L_79 = V_0;
		NullCheck(L_79);
		String_t* L_80 = ErrorResponse_get_RequestId_m2536924555(L_79, /*hidden argument*/NULL);
		int32_t L_81 = ___statusCode2;
		NotAuthorizedException_t457234085 * L_82 = (NotAuthorizedException_t457234085 *)il2cpp_codegen_object_new(NotAuthorizedException_t457234085_il2cpp_TypeInfo_var);
		NotAuthorizedException__ctor_m121792233(L_82, L_73, L_74, L_76, L_78, L_80, L_81, /*hidden argument*/NULL);
		return L_82;
	}

IL_012e:
	{
		ErrorResponse_t3502566035 * L_83 = V_0;
		NullCheck(L_83);
		String_t* L_84 = ErrorResponse_get_Code_m1007089050(L_83, /*hidden argument*/NULL);
		if (!L_84)
		{
			goto IL_0168;
		}
	}
	{
		ErrorResponse_t3502566035 * L_85 = V_0;
		NullCheck(L_85);
		String_t* L_86 = ErrorResponse_get_Code_m1007089050(L_85, /*hidden argument*/NULL);
		NullCheck(L_86);
		bool L_87 = String_Equals_m2633592423(L_86, _stringLiteral4089858387, /*hidden argument*/NULL);
		if (!L_87)
		{
			goto IL_0168;
		}
	}
	{
		ErrorResponse_t3502566035 * L_88 = V_0;
		NullCheck(L_88);
		String_t* L_89 = ErrorResponse_get_Message_m1145825032(L_88, /*hidden argument*/NULL);
		Exception_t1927440687 * L_90 = ___innerException1;
		ErrorResponse_t3502566035 * L_91 = V_0;
		NullCheck(L_91);
		int32_t L_92 = ErrorResponse_get_Type_m3388577699(L_91, /*hidden argument*/NULL);
		ErrorResponse_t3502566035 * L_93 = V_0;
		NullCheck(L_93);
		String_t* L_94 = ErrorResponse_get_Code_m1007089050(L_93, /*hidden argument*/NULL);
		ErrorResponse_t3502566035 * L_95 = V_0;
		NullCheck(L_95);
		String_t* L_96 = ErrorResponse_get_RequestId_m2536924555(L_95, /*hidden argument*/NULL);
		int32_t L_97 = ___statusCode2;
		ResourceConflictException_t4018382715 * L_98 = (ResourceConflictException_t4018382715 *)il2cpp_codegen_object_new(ResourceConflictException_t4018382715_il2cpp_TypeInfo_var);
		ResourceConflictException__ctor_m526234331(L_98, L_89, L_90, L_92, L_94, L_96, L_97, /*hidden argument*/NULL);
		return L_98;
	}

IL_0168:
	{
		ErrorResponse_t3502566035 * L_99 = V_0;
		NullCheck(L_99);
		String_t* L_100 = ErrorResponse_get_Code_m1007089050(L_99, /*hidden argument*/NULL);
		if (!L_100)
		{
			goto IL_01a2;
		}
	}
	{
		ErrorResponse_t3502566035 * L_101 = V_0;
		NullCheck(L_101);
		String_t* L_102 = ErrorResponse_get_Code_m1007089050(L_101, /*hidden argument*/NULL);
		NullCheck(L_102);
		bool L_103 = String_Equals_m2633592423(L_102, _stringLiteral2922393814, /*hidden argument*/NULL);
		if (!L_103)
		{
			goto IL_01a2;
		}
	}
	{
		ErrorResponse_t3502566035 * L_104 = V_0;
		NullCheck(L_104);
		String_t* L_105 = ErrorResponse_get_Message_m1145825032(L_104, /*hidden argument*/NULL);
		Exception_t1927440687 * L_106 = ___innerException1;
		ErrorResponse_t3502566035 * L_107 = V_0;
		NullCheck(L_107);
		int32_t L_108 = ErrorResponse_get_Type_m3388577699(L_107, /*hidden argument*/NULL);
		ErrorResponse_t3502566035 * L_109 = V_0;
		NullCheck(L_109);
		String_t* L_110 = ErrorResponse_get_Code_m1007089050(L_109, /*hidden argument*/NULL);
		ErrorResponse_t3502566035 * L_111 = V_0;
		NullCheck(L_111);
		String_t* L_112 = ErrorResponse_get_RequestId_m2536924555(L_111, /*hidden argument*/NULL);
		int32_t L_113 = ___statusCode2;
		ResourceNotFoundException_t403010890 * L_114 = (ResourceNotFoundException_t403010890 *)il2cpp_codegen_object_new(ResourceNotFoundException_t403010890_il2cpp_TypeInfo_var);
		ResourceNotFoundException__ctor_m3227125662(L_114, L_105, L_106, L_108, L_110, L_112, L_113, /*hidden argument*/NULL);
		return L_114;
	}

IL_01a2:
	{
		ErrorResponse_t3502566035 * L_115 = V_0;
		NullCheck(L_115);
		String_t* L_116 = ErrorResponse_get_Code_m1007089050(L_115, /*hidden argument*/NULL);
		if (!L_116)
		{
			goto IL_01dc;
		}
	}
	{
		ErrorResponse_t3502566035 * L_117 = V_0;
		NullCheck(L_117);
		String_t* L_118 = ErrorResponse_get_Code_m1007089050(L_117, /*hidden argument*/NULL);
		NullCheck(L_118);
		bool L_119 = String_Equals_m2633592423(L_118, _stringLiteral2413473506, /*hidden argument*/NULL);
		if (!L_119)
		{
			goto IL_01dc;
		}
	}
	{
		ErrorResponse_t3502566035 * L_120 = V_0;
		NullCheck(L_120);
		String_t* L_121 = ErrorResponse_get_Message_m1145825032(L_120, /*hidden argument*/NULL);
		Exception_t1927440687 * L_122 = ___innerException1;
		ErrorResponse_t3502566035 * L_123 = V_0;
		NullCheck(L_123);
		int32_t L_124 = ErrorResponse_get_Type_m3388577699(L_123, /*hidden argument*/NULL);
		ErrorResponse_t3502566035 * L_125 = V_0;
		NullCheck(L_125);
		String_t* L_126 = ErrorResponse_get_Code_m1007089050(L_125, /*hidden argument*/NULL);
		ErrorResponse_t3502566035 * L_127 = V_0;
		NullCheck(L_127);
		String_t* L_128 = ErrorResponse_get_RequestId_m2536924555(L_127, /*hidden argument*/NULL);
		int32_t L_129 = ___statusCode2;
		TooManyRequestsException_t3276202790 * L_130 = (TooManyRequestsException_t3276202790 *)il2cpp_codegen_object_new(TooManyRequestsException_t3276202790_il2cpp_TypeInfo_var);
		TooManyRequestsException__ctor_m439418258(L_130, L_121, L_122, L_124, L_126, L_128, L_129, /*hidden argument*/NULL);
		return L_130;
	}

IL_01dc:
	{
		ErrorResponse_t3502566035 * L_131 = V_0;
		NullCheck(L_131);
		String_t* L_132 = ErrorResponse_get_Message_m1145825032(L_131, /*hidden argument*/NULL);
		Exception_t1927440687 * L_133 = ___innerException1;
		ErrorResponse_t3502566035 * L_134 = V_0;
		NullCheck(L_134);
		int32_t L_135 = ErrorResponse_get_Type_m3388577699(L_134, /*hidden argument*/NULL);
		ErrorResponse_t3502566035 * L_136 = V_0;
		NullCheck(L_136);
		String_t* L_137 = ErrorResponse_get_Code_m1007089050(L_136, /*hidden argument*/NULL);
		ErrorResponse_t3502566035 * L_138 = V_0;
		NullCheck(L_138);
		String_t* L_139 = ErrorResponse_get_RequestId_m2536924555(L_138, /*hidden argument*/NULL);
		int32_t L_140 = ___statusCode2;
		AmazonCognitoIdentityException_t158465174 * L_141 = (AmazonCognitoIdentityException_t158465174 *)il2cpp_codegen_object_new(AmazonCognitoIdentityException_t158465174_il2cpp_TypeInfo_var);
		AmazonCognitoIdentityException__ctor_m1060593889(L_141, L_132, L_133, L_135, L_137, L_139, L_140, /*hidden argument*/NULL);
		return L_141;
	}
}
// Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetCredentialsForIdentityResponseUnmarshaller Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetCredentialsForIdentityResponseUnmarshaller::get_Instance()
extern Il2CppClass* GetCredentialsForIdentityResponseUnmarshaller_t2007495598_il2cpp_TypeInfo_var;
extern const uint32_t GetCredentialsForIdentityResponseUnmarshaller_get_Instance_m1533679114_MetadataUsageId;
extern "C"  GetCredentialsForIdentityResponseUnmarshaller_t2007495598 * GetCredentialsForIdentityResponseUnmarshaller_get_Instance_m1533679114 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GetCredentialsForIdentityResponseUnmarshaller_get_Instance_m1533679114_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GetCredentialsForIdentityResponseUnmarshaller_t2007495598_il2cpp_TypeInfo_var);
		GetCredentialsForIdentityResponseUnmarshaller_t2007495598 * L_0 = ((GetCredentialsForIdentityResponseUnmarshaller_t2007495598_StaticFields*)GetCredentialsForIdentityResponseUnmarshaller_t2007495598_il2cpp_TypeInfo_var->static_fields)->get__instance_0();
		return L_0;
	}
}
// System.Void Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetCredentialsForIdentityResponseUnmarshaller::.ctor()
extern "C"  void GetCredentialsForIdentityResponseUnmarshaller__ctor_m294464060 (GetCredentialsForIdentityResponseUnmarshaller_t2007495598 * __this, const MethodInfo* method)
{
	{
		JsonResponseUnmarshaller__ctor_m2636444097(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetCredentialsForIdentityResponseUnmarshaller::.cctor()
extern Il2CppClass* GetCredentialsForIdentityResponseUnmarshaller_t2007495598_il2cpp_TypeInfo_var;
extern const uint32_t GetCredentialsForIdentityResponseUnmarshaller__cctor_m3139304585_MetadataUsageId;
extern "C"  void GetCredentialsForIdentityResponseUnmarshaller__cctor_m3139304585 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GetCredentialsForIdentityResponseUnmarshaller__cctor_m3139304585_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GetCredentialsForIdentityResponseUnmarshaller_t2007495598 * L_0 = (GetCredentialsForIdentityResponseUnmarshaller_t2007495598 *)il2cpp_codegen_object_new(GetCredentialsForIdentityResponseUnmarshaller_t2007495598_il2cpp_TypeInfo_var);
		GetCredentialsForIdentityResponseUnmarshaller__ctor_m294464060(L_0, /*hidden argument*/NULL);
		((GetCredentialsForIdentityResponseUnmarshaller_t2007495598_StaticFields*)GetCredentialsForIdentityResponseUnmarshaller_t2007495598_il2cpp_TypeInfo_var->static_fields)->set__instance_0(L_0);
		return;
	}
}
// Amazon.Runtime.Internal.IRequest Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetIdRequestMarshaller::Marshall(Amazon.Runtime.AmazonWebServiceRequest)
extern Il2CppClass* GetIdRequest_t4078561340_il2cpp_TypeInfo_var;
extern const uint32_t GetIdRequestMarshaller_Marshall_m3007728487_MetadataUsageId;
extern "C"  Il2CppObject * GetIdRequestMarshaller_Marshall_m3007728487 (GetIdRequestMarshaller_t3365653223 * __this, AmazonWebServiceRequest_t3384026212 * ___input0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GetIdRequestMarshaller_Marshall_m3007728487_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AmazonWebServiceRequest_t3384026212 * L_0 = ___input0;
		Il2CppObject * L_1 = GetIdRequestMarshaller_Marshall_m1677148795(__this, ((GetIdRequest_t4078561340 *)CastclassClass(L_0, GetIdRequest_t4078561340_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return L_1;
	}
}
// Amazon.Runtime.Internal.IRequest Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetIdRequestMarshaller::Marshall(Amazon.CognitoIdentity.Model.GetIdRequest)
extern Il2CppClass* DefaultRequest_t3080757440_il2cpp_TypeInfo_var;
extern Il2CppClass* IRequest_t2400804350_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_2_t1943082916_il2cpp_TypeInfo_var;
extern Il2CppClass* CultureInfo_t3500843524_il2cpp_TypeInfo_var;
extern Il2CppClass* StringWriter_t4139609088_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonWriter_t3014444111_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonMarshallerContext_t4063314926_il2cpp_TypeInfo_var;
extern Il2CppClass* Encoding_t663144255_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_GetEnumerator_m2895728349_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1989408781_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m1372024679_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m1710042386_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m4005245300_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m882561911_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2667713841;
extern Il2CppCodeGenString* _stringLiteral3849563724;
extern Il2CppCodeGenString* _stringLiteral3012313031;
extern Il2CppCodeGenString* _stringLiteral1048821954;
extern Il2CppCodeGenString* _stringLiteral1711157076;
extern Il2CppCodeGenString* _stringLiteral782856060;
extern Il2CppCodeGenString* _stringLiteral372029315;
extern Il2CppCodeGenString* _stringLiteral493802888;
extern Il2CppCodeGenString* _stringLiteral2553643991;
extern Il2CppCodeGenString* _stringLiteral2335677872;
extern const uint32_t GetIdRequestMarshaller_Marshall_m1677148795_MetadataUsageId;
extern "C"  Il2CppObject * GetIdRequestMarshaller_Marshall_m1677148795 (GetIdRequestMarshaller_t3365653223 * __this, GetIdRequest_t4078561340 * ___publicRequest0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GetIdRequestMarshaller_Marshall_m1677148795_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	String_t* V_1 = NULL;
	String_t* V_2 = NULL;
	StringWriter_t4139609088 * V_3 = NULL;
	JsonWriter_t3014444111 * V_4 = NULL;
	JsonMarshallerContext_t4063314926 * V_5 = NULL;
	String_t* V_6 = NULL;
	Enumerator_t969056901  V_7;
	memset(&V_7, 0, sizeof(V_7));
	KeyValuePair_2_t1701344717  V_8;
	memset(&V_8, 0, sizeof(V_8));
	String_t* V_9 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		GetIdRequest_t4078561340 * L_0 = ___publicRequest0;
		DefaultRequest_t3080757440 * L_1 = (DefaultRequest_t3080757440 *)il2cpp_codegen_object_new(DefaultRequest_t3080757440_il2cpp_TypeInfo_var);
		DefaultRequest__ctor_m3534427872(L_1, L_0, _stringLiteral2667713841, /*hidden argument*/NULL);
		V_0 = L_1;
		V_1 = _stringLiteral3849563724;
		Il2CppObject * L_2 = V_0;
		NullCheck(L_2);
		Il2CppObject* L_3 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(1 /* System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.Runtime.Internal.IRequest::get_Headers() */, IRequest_t2400804350_il2cpp_TypeInfo_var, L_2);
		String_t* L_4 = V_1;
		NullCheck(L_3);
		InterfaceActionInvoker2< String_t*, String_t* >::Invoke(5 /* System.Void System.Collections.Generic.IDictionary`2<System.String,System.String>::set_Item(!0,!1) */, IDictionary_2_t1943082916_il2cpp_TypeInfo_var, L_3, _stringLiteral3012313031, L_4);
		Il2CppObject * L_5 = V_0;
		NullCheck(L_5);
		Il2CppObject* L_6 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(1 /* System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.Runtime.Internal.IRequest::get_Headers() */, IRequest_t2400804350_il2cpp_TypeInfo_var, L_5);
		NullCheck(L_6);
		InterfaceActionInvoker2< String_t*, String_t* >::Invoke(5 /* System.Void System.Collections.Generic.IDictionary`2<System.String,System.String>::set_Item(!0,!1) */, IDictionary_2_t1943082916_il2cpp_TypeInfo_var, L_6, _stringLiteral1048821954, _stringLiteral1711157076);
		Il2CppObject * L_7 = V_0;
		NullCheck(L_7);
		InterfaceActionInvoker1< String_t* >::Invoke(7 /* System.Void Amazon.Runtime.Internal.IRequest::set_HttpMethod(System.String) */, IRequest_t2400804350_il2cpp_TypeInfo_var, L_7, _stringLiteral782856060);
		V_2 = _stringLiteral372029315;
		Il2CppObject * L_8 = V_0;
		String_t* L_9 = V_2;
		NullCheck(L_8);
		InterfaceActionInvoker1< String_t* >::Invoke(11 /* System.Void Amazon.Runtime.Internal.IRequest::set_ResourcePath(System.String) */, IRequest_t2400804350_il2cpp_TypeInfo_var, L_8, L_9);
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3500843524_il2cpp_TypeInfo_var);
		CultureInfo_t3500843524 * L_10 = CultureInfo_get_InvariantCulture_m398972276(NULL /*static, unused*/, /*hidden argument*/NULL);
		StringWriter_t4139609088 * L_11 = (StringWriter_t4139609088 *)il2cpp_codegen_object_new(StringWriter_t4139609088_il2cpp_TypeInfo_var);
		StringWriter__ctor_m3405704837(L_11, L_10, /*hidden argument*/NULL);
		V_3 = L_11;
	}

IL_005b:
	try
	{ // begin try (depth: 1)
		{
			StringWriter_t4139609088 * L_12 = V_3;
			JsonWriter_t3014444111 * L_13 = (JsonWriter_t3014444111 *)il2cpp_codegen_object_new(JsonWriter_t3014444111_il2cpp_TypeInfo_var);
			JsonWriter__ctor_m2344288487(L_13, L_12, /*hidden argument*/NULL);
			V_4 = L_13;
			JsonWriter_t3014444111 * L_14 = V_4;
			NullCheck(L_14);
			JsonWriter_WriteObjectStart_m3468068136(L_14, /*hidden argument*/NULL);
			Il2CppObject * L_15 = V_0;
			JsonWriter_t3014444111 * L_16 = V_4;
			JsonMarshallerContext_t4063314926 * L_17 = (JsonMarshallerContext_t4063314926 *)il2cpp_codegen_object_new(JsonMarshallerContext_t4063314926_il2cpp_TypeInfo_var);
			JsonMarshallerContext__ctor_m1418069536(L_17, L_15, L_16, /*hidden argument*/NULL);
			V_5 = L_17;
			GetIdRequest_t4078561340 * L_18 = ___publicRequest0;
			NullCheck(L_18);
			bool L_19 = GetIdRequest_IsSetAccountId_m2868615701(L_18, /*hidden argument*/NULL);
			if (!L_19)
			{
				goto IL_009f;
			}
		}

IL_007c:
		{
			JsonMarshallerContext_t4063314926 * L_20 = V_5;
			NullCheck(L_20);
			JsonWriter_t3014444111 * L_21 = JsonMarshallerContext_get_Writer_m2343372188(L_20, /*hidden argument*/NULL);
			NullCheck(L_21);
			JsonWriter_WritePropertyName_m1171400361(L_21, _stringLiteral493802888, /*hidden argument*/NULL);
			JsonMarshallerContext_t4063314926 * L_22 = V_5;
			NullCheck(L_22);
			JsonWriter_t3014444111 * L_23 = JsonMarshallerContext_get_Writer_m2343372188(L_22, /*hidden argument*/NULL);
			GetIdRequest_t4078561340 * L_24 = ___publicRequest0;
			NullCheck(L_24);
			String_t* L_25 = GetIdRequest_get_AccountId_m2559012565(L_24, /*hidden argument*/NULL);
			NullCheck(L_23);
			JsonWriter_Write_m3974344637(L_23, L_25, /*hidden argument*/NULL);
		}

IL_009f:
		{
			GetIdRequest_t4078561340 * L_26 = ___publicRequest0;
			NullCheck(L_26);
			bool L_27 = GetIdRequest_IsSetIdentityPoolId_m213241084(L_26, /*hidden argument*/NULL);
			if (!L_27)
			{
				goto IL_00ca;
			}
		}

IL_00a7:
		{
			JsonMarshallerContext_t4063314926 * L_28 = V_5;
			NullCheck(L_28);
			JsonWriter_t3014444111 * L_29 = JsonMarshallerContext_get_Writer_m2343372188(L_28, /*hidden argument*/NULL);
			NullCheck(L_29);
			JsonWriter_WritePropertyName_m1171400361(L_29, _stringLiteral2553643991, /*hidden argument*/NULL);
			JsonMarshallerContext_t4063314926 * L_30 = V_5;
			NullCheck(L_30);
			JsonWriter_t3014444111 * L_31 = JsonMarshallerContext_get_Writer_m2343372188(L_30, /*hidden argument*/NULL);
			GetIdRequest_t4078561340 * L_32 = ___publicRequest0;
			NullCheck(L_32);
			String_t* L_33 = GetIdRequest_get_IdentityPoolId_m2191128700(L_32, /*hidden argument*/NULL);
			NullCheck(L_31);
			JsonWriter_Write_m3974344637(L_31, L_33, /*hidden argument*/NULL);
		}

IL_00ca:
		{
			GetIdRequest_t4078561340 * L_34 = ___publicRequest0;
			NullCheck(L_34);
			bool L_35 = GetIdRequest_IsSetLogins_m529970793(L_34, /*hidden argument*/NULL);
			if (!L_35)
			{
				goto IL_0159;
			}
		}

IL_00d5:
		{
			JsonMarshallerContext_t4063314926 * L_36 = V_5;
			NullCheck(L_36);
			JsonWriter_t3014444111 * L_37 = JsonMarshallerContext_get_Writer_m2343372188(L_36, /*hidden argument*/NULL);
			NullCheck(L_37);
			JsonWriter_WritePropertyName_m1171400361(L_37, _stringLiteral2335677872, /*hidden argument*/NULL);
			JsonMarshallerContext_t4063314926 * L_38 = V_5;
			NullCheck(L_38);
			JsonWriter_t3014444111 * L_39 = JsonMarshallerContext_get_Writer_m2343372188(L_38, /*hidden argument*/NULL);
			NullCheck(L_39);
			JsonWriter_WriteObjectStart_m3468068136(L_39, /*hidden argument*/NULL);
			GetIdRequest_t4078561340 * L_40 = ___publicRequest0;
			NullCheck(L_40);
			Dictionary_2_t3943999495 * L_41 = GetIdRequest_get_Logins_m1911199310(L_40, /*hidden argument*/NULL);
			NullCheck(L_41);
			Enumerator_t969056901  L_42 = Dictionary_2_GetEnumerator_m2895728349(L_41, /*hidden argument*/Dictionary_2_GetEnumerator_m2895728349_MethodInfo_var);
			V_7 = L_42;
		}

IL_00ff:
		try
		{ // begin try (depth: 2)
			{
				goto IL_0134;
			}

IL_0101:
			{
				KeyValuePair_2_t1701344717  L_43 = Enumerator_get_Current_m1989408781((&V_7), /*hidden argument*/Enumerator_get_Current_m1989408781_MethodInfo_var);
				V_8 = L_43;
				JsonMarshallerContext_t4063314926 * L_44 = V_5;
				NullCheck(L_44);
				JsonWriter_t3014444111 * L_45 = JsonMarshallerContext_get_Writer_m2343372188(L_44, /*hidden argument*/NULL);
				String_t* L_46 = KeyValuePair_2_get_Key_m1372024679((&V_8), /*hidden argument*/KeyValuePair_2_get_Key_m1372024679_MethodInfo_var);
				NullCheck(L_45);
				JsonWriter_WritePropertyName_m1171400361(L_45, L_46, /*hidden argument*/NULL);
				String_t* L_47 = KeyValuePair_2_get_Value_m1710042386((&V_8), /*hidden argument*/KeyValuePair_2_get_Value_m1710042386_MethodInfo_var);
				V_9 = L_47;
				JsonMarshallerContext_t4063314926 * L_48 = V_5;
				NullCheck(L_48);
				JsonWriter_t3014444111 * L_49 = JsonMarshallerContext_get_Writer_m2343372188(L_48, /*hidden argument*/NULL);
				String_t* L_50 = V_9;
				NullCheck(L_49);
				JsonWriter_Write_m3974344637(L_49, L_50, /*hidden argument*/NULL);
			}

IL_0134:
			{
				bool L_51 = Enumerator_MoveNext_m4005245300((&V_7), /*hidden argument*/Enumerator_MoveNext_m4005245300_MethodInfo_var);
				if (L_51)
				{
					goto IL_0101;
				}
			}

IL_013d:
			{
				IL2CPP_LEAVE(0x14D, FINALLY_013f);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
			goto FINALLY_013f;
		}

FINALLY_013f:
		{ // begin finally (depth: 2)
			Enumerator_Dispose_m882561911((&V_7), /*hidden argument*/Enumerator_Dispose_m882561911_MethodInfo_var);
			IL2CPP_END_FINALLY(319)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(319)
		{
			IL2CPP_JUMP_TBL(0x14D, IL_014d)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
		}

IL_014d:
		{
			JsonMarshallerContext_t4063314926 * L_52 = V_5;
			NullCheck(L_52);
			JsonWriter_t3014444111 * L_53 = JsonMarshallerContext_get_Writer_m2343372188(L_52, /*hidden argument*/NULL);
			NullCheck(L_53);
			JsonWriter_WriteObjectEnd_m4253837441(L_53, /*hidden argument*/NULL);
		}

IL_0159:
		{
			JsonWriter_t3014444111 * L_54 = V_4;
			NullCheck(L_54);
			JsonWriter_WriteObjectEnd_m4253837441(L_54, /*hidden argument*/NULL);
			StringWriter_t4139609088 * L_55 = V_3;
			NullCheck(L_55);
			String_t* L_56 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_55);
			V_6 = L_56;
			Il2CppObject * L_57 = V_0;
			IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
			Encoding_t663144255 * L_58 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
			String_t* L_59 = V_6;
			NullCheck(L_58);
			ByteU5BU5D_t3397334013* L_60 = VirtFuncInvoker1< ByteU5BU5D_t3397334013*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_58, L_59);
			NullCheck(L_57);
			InterfaceActionInvoker1< ByteU5BU5D_t3397334013* >::Invoke(13 /* System.Void Amazon.Runtime.Internal.IRequest::set_Content(System.Byte[]) */, IRequest_t2400804350_il2cpp_TypeInfo_var, L_57, L_60);
			IL2CPP_LEAVE(0x186, FINALLY_017c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_017c;
	}

FINALLY_017c:
	{ // begin finally (depth: 1)
		{
			StringWriter_t4139609088 * L_61 = V_3;
			if (!L_61)
			{
				goto IL_0185;
			}
		}

IL_017f:
		{
			StringWriter_t4139609088 * L_62 = V_3;
			NullCheck(L_62);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_62);
		}

IL_0185:
		{
			IL2CPP_END_FINALLY(380)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(380)
	{
		IL2CPP_JUMP_TBL(0x186, IL_0186)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0186:
	{
		Il2CppObject * L_63 = V_0;
		return L_63;
	}
}
// System.Void Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetIdRequestMarshaller::.ctor()
extern "C"  void GetIdRequestMarshaller__ctor_m1760098451 (GetIdRequestMarshaller_t3365653223 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// Amazon.Runtime.AmazonWebServiceResponse Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetIdResponseUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern Il2CppClass* GetIdResponse_t2091118072_il2cpp_TypeInfo_var;
extern Il2CppClass* StringUnmarshaller_t3953260147_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2345627623;
extern const uint32_t GetIdResponseUnmarshaller_Unmarshall_m3860701444_MetadataUsageId;
extern "C"  AmazonWebServiceResponse_t529043356 * GetIdResponseUnmarshaller_Unmarshall_m3860701444 (GetIdResponseUnmarshaller_t2243065850 * __this, JsonUnmarshallerContext_t456235889 * ___context0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GetIdResponseUnmarshaller_Unmarshall_m3860701444_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GetIdResponse_t2091118072 * V_0 = NULL;
	int32_t V_1 = 0;
	StringUnmarshaller_t3953260147 * V_2 = NULL;
	{
		GetIdResponse_t2091118072 * L_0 = (GetIdResponse_t2091118072 *)il2cpp_codegen_object_new(GetIdResponse_t2091118072_il2cpp_TypeInfo_var);
		GetIdResponse__ctor_m1965179359(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		JsonUnmarshallerContext_t456235889 * L_1 = ___context0;
		NullCheck(L_1);
		VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean Amazon.Runtime.Internal.Transform.UnmarshallerContext::Read() */, L_1);
		JsonUnmarshallerContext_t456235889 * L_2 = ___context0;
		NullCheck(L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 Amazon.Runtime.Internal.Transform.UnmarshallerContext::get_CurrentDepth() */, L_2);
		V_1 = L_3;
		goto IL_0037;
	}

IL_0016:
	{
		JsonUnmarshallerContext_t456235889 * L_4 = ___context0;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		bool L_6 = UnmarshallerContext_TestExpression_m3665247446(L_4, _stringLiteral2345627623, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0037;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(StringUnmarshaller_t3953260147_il2cpp_TypeInfo_var);
		StringUnmarshaller_t3953260147 * L_7 = StringUnmarshaller_get_Instance_m107503370(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = L_7;
		GetIdResponse_t2091118072 * L_8 = V_0;
		StringUnmarshaller_t3953260147 * L_9 = V_2;
		JsonUnmarshallerContext_t456235889 * L_10 = ___context0;
		NullCheck(L_9);
		String_t* L_11 = StringUnmarshaller_Unmarshall_m241138027(L_9, L_10, /*hidden argument*/NULL);
		NullCheck(L_8);
		GetIdResponse_set_IdentityId_m4089638831(L_8, L_11, /*hidden argument*/NULL);
	}

IL_0037:
	{
		JsonUnmarshallerContext_t456235889 * L_12 = ___context0;
		int32_t L_13 = V_1;
		NullCheck(L_12);
		bool L_14 = UnmarshallerContext_ReadAtDepth_m2943457426(L_12, L_13, /*hidden argument*/NULL);
		if (L_14)
		{
			goto IL_0016;
		}
	}
	{
		GetIdResponse_t2091118072 * L_15 = V_0;
		return L_15;
	}
}
// Amazon.Runtime.AmazonServiceException Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetIdResponseUnmarshaller::UnmarshallException(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,System.Exception,System.Net.HttpStatusCode)
extern Il2CppClass* ExternalServiceException_t712152635_il2cpp_TypeInfo_var;
extern Il2CppClass* InternalErrorException_t1116634264_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidParameterException_t2482528107_il2cpp_TypeInfo_var;
extern Il2CppClass* LimitExceededException_t3250699605_il2cpp_TypeInfo_var;
extern Il2CppClass* NotAuthorizedException_t457234085_il2cpp_TypeInfo_var;
extern Il2CppClass* ResourceConflictException_t4018382715_il2cpp_TypeInfo_var;
extern Il2CppClass* ResourceNotFoundException_t403010890_il2cpp_TypeInfo_var;
extern Il2CppClass* TooManyRequestsException_t3276202790_il2cpp_TypeInfo_var;
extern Il2CppClass* AmazonCognitoIdentityException_t158465174_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1101196531;
extern Il2CppCodeGenString* _stringLiteral629809416;
extern Il2CppCodeGenString* _stringLiteral270646655;
extern Il2CppCodeGenString* _stringLiteral3617635333;
extern Il2CppCodeGenString* _stringLiteral4046014057;
extern Il2CppCodeGenString* _stringLiteral4089858387;
extern Il2CppCodeGenString* _stringLiteral2922393814;
extern Il2CppCodeGenString* _stringLiteral2413473506;
extern const uint32_t GetIdResponseUnmarshaller_UnmarshallException_m185098930_MetadataUsageId;
extern "C"  AmazonServiceException_t3748559634 * GetIdResponseUnmarshaller_UnmarshallException_m185098930 (GetIdResponseUnmarshaller_t2243065850 * __this, JsonUnmarshallerContext_t456235889 * ___context0, Exception_t1927440687 * ___innerException1, int32_t ___statusCode2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GetIdResponseUnmarshaller_UnmarshallException_m185098930_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ErrorResponse_t3502566035 * V_0 = NULL;
	{
		JsonErrorResponseUnmarshaller_t4149926919 * L_0 = JsonErrorResponseUnmarshaller_GetInstance_m1721802307(NULL /*static, unused*/, /*hidden argument*/NULL);
		JsonUnmarshallerContext_t456235889 * L_1 = ___context0;
		NullCheck(L_0);
		ErrorResponse_t3502566035 * L_2 = JsonErrorResponseUnmarshaller_Unmarshall_m3547220927(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		ErrorResponse_t3502566035 * L_3 = V_0;
		NullCheck(L_3);
		String_t* L_4 = ErrorResponse_get_Code_m1007089050(L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0046;
		}
	}
	{
		ErrorResponse_t3502566035 * L_5 = V_0;
		NullCheck(L_5);
		String_t* L_6 = ErrorResponse_get_Code_m1007089050(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		bool L_7 = String_Equals_m2633592423(L_6, _stringLiteral1101196531, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0046;
		}
	}
	{
		ErrorResponse_t3502566035 * L_8 = V_0;
		NullCheck(L_8);
		String_t* L_9 = ErrorResponse_get_Message_m1145825032(L_8, /*hidden argument*/NULL);
		Exception_t1927440687 * L_10 = ___innerException1;
		ErrorResponse_t3502566035 * L_11 = V_0;
		NullCheck(L_11);
		int32_t L_12 = ErrorResponse_get_Type_m3388577699(L_11, /*hidden argument*/NULL);
		ErrorResponse_t3502566035 * L_13 = V_0;
		NullCheck(L_13);
		String_t* L_14 = ErrorResponse_get_Code_m1007089050(L_13, /*hidden argument*/NULL);
		ErrorResponse_t3502566035 * L_15 = V_0;
		NullCheck(L_15);
		String_t* L_16 = ErrorResponse_get_RequestId_m2536924555(L_15, /*hidden argument*/NULL);
		int32_t L_17 = ___statusCode2;
		ExternalServiceException_t712152635 * L_18 = (ExternalServiceException_t712152635 *)il2cpp_codegen_object_new(ExternalServiceException_t712152635_il2cpp_TypeInfo_var);
		ExternalServiceException__ctor_m3707348563(L_18, L_9, L_10, L_12, L_14, L_16, L_17, /*hidden argument*/NULL);
		return L_18;
	}

IL_0046:
	{
		ErrorResponse_t3502566035 * L_19 = V_0;
		NullCheck(L_19);
		String_t* L_20 = ErrorResponse_get_Code_m1007089050(L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_0080;
		}
	}
	{
		ErrorResponse_t3502566035 * L_21 = V_0;
		NullCheck(L_21);
		String_t* L_22 = ErrorResponse_get_Code_m1007089050(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		bool L_23 = String_Equals_m2633592423(L_22, _stringLiteral629809416, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_0080;
		}
	}
	{
		ErrorResponse_t3502566035 * L_24 = V_0;
		NullCheck(L_24);
		String_t* L_25 = ErrorResponse_get_Message_m1145825032(L_24, /*hidden argument*/NULL);
		Exception_t1927440687 * L_26 = ___innerException1;
		ErrorResponse_t3502566035 * L_27 = V_0;
		NullCheck(L_27);
		int32_t L_28 = ErrorResponse_get_Type_m3388577699(L_27, /*hidden argument*/NULL);
		ErrorResponse_t3502566035 * L_29 = V_0;
		NullCheck(L_29);
		String_t* L_30 = ErrorResponse_get_Code_m1007089050(L_29, /*hidden argument*/NULL);
		ErrorResponse_t3502566035 * L_31 = V_0;
		NullCheck(L_31);
		String_t* L_32 = ErrorResponse_get_RequestId_m2536924555(L_31, /*hidden argument*/NULL);
		int32_t L_33 = ___statusCode2;
		InternalErrorException_t1116634264 * L_34 = (InternalErrorException_t1116634264 *)il2cpp_codegen_object_new(InternalErrorException_t1116634264_il2cpp_TypeInfo_var);
		InternalErrorException__ctor_m586328184(L_34, L_25, L_26, L_28, L_30, L_32, L_33, /*hidden argument*/NULL);
		return L_34;
	}

IL_0080:
	{
		ErrorResponse_t3502566035 * L_35 = V_0;
		NullCheck(L_35);
		String_t* L_36 = ErrorResponse_get_Code_m1007089050(L_35, /*hidden argument*/NULL);
		if (!L_36)
		{
			goto IL_00ba;
		}
	}
	{
		ErrorResponse_t3502566035 * L_37 = V_0;
		NullCheck(L_37);
		String_t* L_38 = ErrorResponse_get_Code_m1007089050(L_37, /*hidden argument*/NULL);
		NullCheck(L_38);
		bool L_39 = String_Equals_m2633592423(L_38, _stringLiteral270646655, /*hidden argument*/NULL);
		if (!L_39)
		{
			goto IL_00ba;
		}
	}
	{
		ErrorResponse_t3502566035 * L_40 = V_0;
		NullCheck(L_40);
		String_t* L_41 = ErrorResponse_get_Message_m1145825032(L_40, /*hidden argument*/NULL);
		Exception_t1927440687 * L_42 = ___innerException1;
		ErrorResponse_t3502566035 * L_43 = V_0;
		NullCheck(L_43);
		int32_t L_44 = ErrorResponse_get_Type_m3388577699(L_43, /*hidden argument*/NULL);
		ErrorResponse_t3502566035 * L_45 = V_0;
		NullCheck(L_45);
		String_t* L_46 = ErrorResponse_get_Code_m1007089050(L_45, /*hidden argument*/NULL);
		ErrorResponse_t3502566035 * L_47 = V_0;
		NullCheck(L_47);
		String_t* L_48 = ErrorResponse_get_RequestId_m2536924555(L_47, /*hidden argument*/NULL);
		int32_t L_49 = ___statusCode2;
		InvalidParameterException_t2482528107 * L_50 = (InvalidParameterException_t2482528107 *)il2cpp_codegen_object_new(InvalidParameterException_t2482528107_il2cpp_TypeInfo_var);
		InvalidParameterException__ctor_m3019345047(L_50, L_41, L_42, L_44, L_46, L_48, L_49, /*hidden argument*/NULL);
		return L_50;
	}

IL_00ba:
	{
		ErrorResponse_t3502566035 * L_51 = V_0;
		NullCheck(L_51);
		String_t* L_52 = ErrorResponse_get_Code_m1007089050(L_51, /*hidden argument*/NULL);
		if (!L_52)
		{
			goto IL_00f4;
		}
	}
	{
		ErrorResponse_t3502566035 * L_53 = V_0;
		NullCheck(L_53);
		String_t* L_54 = ErrorResponse_get_Code_m1007089050(L_53, /*hidden argument*/NULL);
		NullCheck(L_54);
		bool L_55 = String_Equals_m2633592423(L_54, _stringLiteral3617635333, /*hidden argument*/NULL);
		if (!L_55)
		{
			goto IL_00f4;
		}
	}
	{
		ErrorResponse_t3502566035 * L_56 = V_0;
		NullCheck(L_56);
		String_t* L_57 = ErrorResponse_get_Message_m1145825032(L_56, /*hidden argument*/NULL);
		Exception_t1927440687 * L_58 = ___innerException1;
		ErrorResponse_t3502566035 * L_59 = V_0;
		NullCheck(L_59);
		int32_t L_60 = ErrorResponse_get_Type_m3388577699(L_59, /*hidden argument*/NULL);
		ErrorResponse_t3502566035 * L_61 = V_0;
		NullCheck(L_61);
		String_t* L_62 = ErrorResponse_get_Code_m1007089050(L_61, /*hidden argument*/NULL);
		ErrorResponse_t3502566035 * L_63 = V_0;
		NullCheck(L_63);
		String_t* L_64 = ErrorResponse_get_RequestId_m2536924555(L_63, /*hidden argument*/NULL);
		int32_t L_65 = ___statusCode2;
		LimitExceededException_t3250699605 * L_66 = (LimitExceededException_t3250699605 *)il2cpp_codegen_object_new(LimitExceededException_t3250699605_il2cpp_TypeInfo_var);
		LimitExceededException__ctor_m1705519093(L_66, L_57, L_58, L_60, L_62, L_64, L_65, /*hidden argument*/NULL);
		return L_66;
	}

IL_00f4:
	{
		ErrorResponse_t3502566035 * L_67 = V_0;
		NullCheck(L_67);
		String_t* L_68 = ErrorResponse_get_Code_m1007089050(L_67, /*hidden argument*/NULL);
		if (!L_68)
		{
			goto IL_012e;
		}
	}
	{
		ErrorResponse_t3502566035 * L_69 = V_0;
		NullCheck(L_69);
		String_t* L_70 = ErrorResponse_get_Code_m1007089050(L_69, /*hidden argument*/NULL);
		NullCheck(L_70);
		bool L_71 = String_Equals_m2633592423(L_70, _stringLiteral4046014057, /*hidden argument*/NULL);
		if (!L_71)
		{
			goto IL_012e;
		}
	}
	{
		ErrorResponse_t3502566035 * L_72 = V_0;
		NullCheck(L_72);
		String_t* L_73 = ErrorResponse_get_Message_m1145825032(L_72, /*hidden argument*/NULL);
		Exception_t1927440687 * L_74 = ___innerException1;
		ErrorResponse_t3502566035 * L_75 = V_0;
		NullCheck(L_75);
		int32_t L_76 = ErrorResponse_get_Type_m3388577699(L_75, /*hidden argument*/NULL);
		ErrorResponse_t3502566035 * L_77 = V_0;
		NullCheck(L_77);
		String_t* L_78 = ErrorResponse_get_Code_m1007089050(L_77, /*hidden argument*/NULL);
		ErrorResponse_t3502566035 * L_79 = V_0;
		NullCheck(L_79);
		String_t* L_80 = ErrorResponse_get_RequestId_m2536924555(L_79, /*hidden argument*/NULL);
		int32_t L_81 = ___statusCode2;
		NotAuthorizedException_t457234085 * L_82 = (NotAuthorizedException_t457234085 *)il2cpp_codegen_object_new(NotAuthorizedException_t457234085_il2cpp_TypeInfo_var);
		NotAuthorizedException__ctor_m121792233(L_82, L_73, L_74, L_76, L_78, L_80, L_81, /*hidden argument*/NULL);
		return L_82;
	}

IL_012e:
	{
		ErrorResponse_t3502566035 * L_83 = V_0;
		NullCheck(L_83);
		String_t* L_84 = ErrorResponse_get_Code_m1007089050(L_83, /*hidden argument*/NULL);
		if (!L_84)
		{
			goto IL_0168;
		}
	}
	{
		ErrorResponse_t3502566035 * L_85 = V_0;
		NullCheck(L_85);
		String_t* L_86 = ErrorResponse_get_Code_m1007089050(L_85, /*hidden argument*/NULL);
		NullCheck(L_86);
		bool L_87 = String_Equals_m2633592423(L_86, _stringLiteral4089858387, /*hidden argument*/NULL);
		if (!L_87)
		{
			goto IL_0168;
		}
	}
	{
		ErrorResponse_t3502566035 * L_88 = V_0;
		NullCheck(L_88);
		String_t* L_89 = ErrorResponse_get_Message_m1145825032(L_88, /*hidden argument*/NULL);
		Exception_t1927440687 * L_90 = ___innerException1;
		ErrorResponse_t3502566035 * L_91 = V_0;
		NullCheck(L_91);
		int32_t L_92 = ErrorResponse_get_Type_m3388577699(L_91, /*hidden argument*/NULL);
		ErrorResponse_t3502566035 * L_93 = V_0;
		NullCheck(L_93);
		String_t* L_94 = ErrorResponse_get_Code_m1007089050(L_93, /*hidden argument*/NULL);
		ErrorResponse_t3502566035 * L_95 = V_0;
		NullCheck(L_95);
		String_t* L_96 = ErrorResponse_get_RequestId_m2536924555(L_95, /*hidden argument*/NULL);
		int32_t L_97 = ___statusCode2;
		ResourceConflictException_t4018382715 * L_98 = (ResourceConflictException_t4018382715 *)il2cpp_codegen_object_new(ResourceConflictException_t4018382715_il2cpp_TypeInfo_var);
		ResourceConflictException__ctor_m526234331(L_98, L_89, L_90, L_92, L_94, L_96, L_97, /*hidden argument*/NULL);
		return L_98;
	}

IL_0168:
	{
		ErrorResponse_t3502566035 * L_99 = V_0;
		NullCheck(L_99);
		String_t* L_100 = ErrorResponse_get_Code_m1007089050(L_99, /*hidden argument*/NULL);
		if (!L_100)
		{
			goto IL_01a2;
		}
	}
	{
		ErrorResponse_t3502566035 * L_101 = V_0;
		NullCheck(L_101);
		String_t* L_102 = ErrorResponse_get_Code_m1007089050(L_101, /*hidden argument*/NULL);
		NullCheck(L_102);
		bool L_103 = String_Equals_m2633592423(L_102, _stringLiteral2922393814, /*hidden argument*/NULL);
		if (!L_103)
		{
			goto IL_01a2;
		}
	}
	{
		ErrorResponse_t3502566035 * L_104 = V_0;
		NullCheck(L_104);
		String_t* L_105 = ErrorResponse_get_Message_m1145825032(L_104, /*hidden argument*/NULL);
		Exception_t1927440687 * L_106 = ___innerException1;
		ErrorResponse_t3502566035 * L_107 = V_0;
		NullCheck(L_107);
		int32_t L_108 = ErrorResponse_get_Type_m3388577699(L_107, /*hidden argument*/NULL);
		ErrorResponse_t3502566035 * L_109 = V_0;
		NullCheck(L_109);
		String_t* L_110 = ErrorResponse_get_Code_m1007089050(L_109, /*hidden argument*/NULL);
		ErrorResponse_t3502566035 * L_111 = V_0;
		NullCheck(L_111);
		String_t* L_112 = ErrorResponse_get_RequestId_m2536924555(L_111, /*hidden argument*/NULL);
		int32_t L_113 = ___statusCode2;
		ResourceNotFoundException_t403010890 * L_114 = (ResourceNotFoundException_t403010890 *)il2cpp_codegen_object_new(ResourceNotFoundException_t403010890_il2cpp_TypeInfo_var);
		ResourceNotFoundException__ctor_m3227125662(L_114, L_105, L_106, L_108, L_110, L_112, L_113, /*hidden argument*/NULL);
		return L_114;
	}

IL_01a2:
	{
		ErrorResponse_t3502566035 * L_115 = V_0;
		NullCheck(L_115);
		String_t* L_116 = ErrorResponse_get_Code_m1007089050(L_115, /*hidden argument*/NULL);
		if (!L_116)
		{
			goto IL_01dc;
		}
	}
	{
		ErrorResponse_t3502566035 * L_117 = V_0;
		NullCheck(L_117);
		String_t* L_118 = ErrorResponse_get_Code_m1007089050(L_117, /*hidden argument*/NULL);
		NullCheck(L_118);
		bool L_119 = String_Equals_m2633592423(L_118, _stringLiteral2413473506, /*hidden argument*/NULL);
		if (!L_119)
		{
			goto IL_01dc;
		}
	}
	{
		ErrorResponse_t3502566035 * L_120 = V_0;
		NullCheck(L_120);
		String_t* L_121 = ErrorResponse_get_Message_m1145825032(L_120, /*hidden argument*/NULL);
		Exception_t1927440687 * L_122 = ___innerException1;
		ErrorResponse_t3502566035 * L_123 = V_0;
		NullCheck(L_123);
		int32_t L_124 = ErrorResponse_get_Type_m3388577699(L_123, /*hidden argument*/NULL);
		ErrorResponse_t3502566035 * L_125 = V_0;
		NullCheck(L_125);
		String_t* L_126 = ErrorResponse_get_Code_m1007089050(L_125, /*hidden argument*/NULL);
		ErrorResponse_t3502566035 * L_127 = V_0;
		NullCheck(L_127);
		String_t* L_128 = ErrorResponse_get_RequestId_m2536924555(L_127, /*hidden argument*/NULL);
		int32_t L_129 = ___statusCode2;
		TooManyRequestsException_t3276202790 * L_130 = (TooManyRequestsException_t3276202790 *)il2cpp_codegen_object_new(TooManyRequestsException_t3276202790_il2cpp_TypeInfo_var);
		TooManyRequestsException__ctor_m439418258(L_130, L_121, L_122, L_124, L_126, L_128, L_129, /*hidden argument*/NULL);
		return L_130;
	}

IL_01dc:
	{
		ErrorResponse_t3502566035 * L_131 = V_0;
		NullCheck(L_131);
		String_t* L_132 = ErrorResponse_get_Message_m1145825032(L_131, /*hidden argument*/NULL);
		Exception_t1927440687 * L_133 = ___innerException1;
		ErrorResponse_t3502566035 * L_134 = V_0;
		NullCheck(L_134);
		int32_t L_135 = ErrorResponse_get_Type_m3388577699(L_134, /*hidden argument*/NULL);
		ErrorResponse_t3502566035 * L_136 = V_0;
		NullCheck(L_136);
		String_t* L_137 = ErrorResponse_get_Code_m1007089050(L_136, /*hidden argument*/NULL);
		ErrorResponse_t3502566035 * L_138 = V_0;
		NullCheck(L_138);
		String_t* L_139 = ErrorResponse_get_RequestId_m2536924555(L_138, /*hidden argument*/NULL);
		int32_t L_140 = ___statusCode2;
		AmazonCognitoIdentityException_t158465174 * L_141 = (AmazonCognitoIdentityException_t158465174 *)il2cpp_codegen_object_new(AmazonCognitoIdentityException_t158465174_il2cpp_TypeInfo_var);
		AmazonCognitoIdentityException__ctor_m1060593889(L_141, L_132, L_133, L_135, L_137, L_139, L_140, /*hidden argument*/NULL);
		return L_141;
	}
}
// Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetIdResponseUnmarshaller Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetIdResponseUnmarshaller::get_Instance()
extern Il2CppClass* GetIdResponseUnmarshaller_t2243065850_il2cpp_TypeInfo_var;
extern const uint32_t GetIdResponseUnmarshaller_get_Instance_m843541898_MetadataUsageId;
extern "C"  GetIdResponseUnmarshaller_t2243065850 * GetIdResponseUnmarshaller_get_Instance_m843541898 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GetIdResponseUnmarshaller_get_Instance_m843541898_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GetIdResponseUnmarshaller_t2243065850_il2cpp_TypeInfo_var);
		GetIdResponseUnmarshaller_t2243065850 * L_0 = ((GetIdResponseUnmarshaller_t2243065850_StaticFields*)GetIdResponseUnmarshaller_t2243065850_il2cpp_TypeInfo_var->static_fields)->get__instance_0();
		return L_0;
	}
}
// System.Void Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetIdResponseUnmarshaller::.ctor()
extern "C"  void GetIdResponseUnmarshaller__ctor_m3528650822 (GetIdResponseUnmarshaller_t2243065850 * __this, const MethodInfo* method)
{
	{
		JsonResponseUnmarshaller__ctor_m2636444097(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetIdResponseUnmarshaller::.cctor()
extern Il2CppClass* GetIdResponseUnmarshaller_t2243065850_il2cpp_TypeInfo_var;
extern const uint32_t GetIdResponseUnmarshaller__cctor_m4123891945_MetadataUsageId;
extern "C"  void GetIdResponseUnmarshaller__cctor_m4123891945 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GetIdResponseUnmarshaller__cctor_m4123891945_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GetIdResponseUnmarshaller_t2243065850 * L_0 = (GetIdResponseUnmarshaller_t2243065850 *)il2cpp_codegen_object_new(GetIdResponseUnmarshaller_t2243065850_il2cpp_TypeInfo_var);
		GetIdResponseUnmarshaller__ctor_m3528650822(L_0, /*hidden argument*/NULL);
		((GetIdResponseUnmarshaller_t2243065850_StaticFields*)GetIdResponseUnmarshaller_t2243065850_il2cpp_TypeInfo_var->static_fields)->set__instance_0(L_0);
		return;
	}
}
// Amazon.Runtime.Internal.IRequest Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetOpenIdTokenRequestMarshaller::Marshall(Amazon.Runtime.AmazonWebServiceRequest)
extern Il2CppClass* GetOpenIdTokenRequest_t2698079901_il2cpp_TypeInfo_var;
extern const uint32_t GetOpenIdTokenRequestMarshaller_Marshall_m1044976942_MetadataUsageId;
extern "C"  Il2CppObject * GetOpenIdTokenRequestMarshaller_Marshall_m1044976942 (GetOpenIdTokenRequestMarshaller_t1236206204 * __this, AmazonWebServiceRequest_t3384026212 * ___input0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GetOpenIdTokenRequestMarshaller_Marshall_m1044976942_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AmazonWebServiceRequest_t3384026212 * L_0 = ___input0;
		Il2CppObject * L_1 = GetOpenIdTokenRequestMarshaller_Marshall_m3491085579(__this, ((GetOpenIdTokenRequest_t2698079901 *)CastclassClass(L_0, GetOpenIdTokenRequest_t2698079901_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return L_1;
	}
}
// Amazon.Runtime.Internal.IRequest Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetOpenIdTokenRequestMarshaller::Marshall(Amazon.CognitoIdentity.Model.GetOpenIdTokenRequest)
extern Il2CppClass* DefaultRequest_t3080757440_il2cpp_TypeInfo_var;
extern Il2CppClass* IRequest_t2400804350_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_2_t1943082916_il2cpp_TypeInfo_var;
extern Il2CppClass* CultureInfo_t3500843524_il2cpp_TypeInfo_var;
extern Il2CppClass* StringWriter_t4139609088_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonWriter_t3014444111_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonMarshallerContext_t4063314926_il2cpp_TypeInfo_var;
extern Il2CppClass* Encoding_t663144255_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_GetEnumerator_m2895728349_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1989408781_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m1372024679_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m1710042386_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m4005245300_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m882561911_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2667713841;
extern Il2CppCodeGenString* _stringLiteral307953647;
extern Il2CppCodeGenString* _stringLiteral3012313031;
extern Il2CppCodeGenString* _stringLiteral1048821954;
extern Il2CppCodeGenString* _stringLiteral1711157076;
extern Il2CppCodeGenString* _stringLiteral782856060;
extern Il2CppCodeGenString* _stringLiteral372029315;
extern Il2CppCodeGenString* _stringLiteral2345627623;
extern Il2CppCodeGenString* _stringLiteral2335677872;
extern const uint32_t GetOpenIdTokenRequestMarshaller_Marshall_m3491085579_MetadataUsageId;
extern "C"  Il2CppObject * GetOpenIdTokenRequestMarshaller_Marshall_m3491085579 (GetOpenIdTokenRequestMarshaller_t1236206204 * __this, GetOpenIdTokenRequest_t2698079901 * ___publicRequest0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GetOpenIdTokenRequestMarshaller_Marshall_m3491085579_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	String_t* V_1 = NULL;
	String_t* V_2 = NULL;
	StringWriter_t4139609088 * V_3 = NULL;
	JsonWriter_t3014444111 * V_4 = NULL;
	JsonMarshallerContext_t4063314926 * V_5 = NULL;
	String_t* V_6 = NULL;
	Enumerator_t969056901  V_7;
	memset(&V_7, 0, sizeof(V_7));
	KeyValuePair_2_t1701344717  V_8;
	memset(&V_8, 0, sizeof(V_8));
	String_t* V_9 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		GetOpenIdTokenRequest_t2698079901 * L_0 = ___publicRequest0;
		DefaultRequest_t3080757440 * L_1 = (DefaultRequest_t3080757440 *)il2cpp_codegen_object_new(DefaultRequest_t3080757440_il2cpp_TypeInfo_var);
		DefaultRequest__ctor_m3534427872(L_1, L_0, _stringLiteral2667713841, /*hidden argument*/NULL);
		V_0 = L_1;
		V_1 = _stringLiteral307953647;
		Il2CppObject * L_2 = V_0;
		NullCheck(L_2);
		Il2CppObject* L_3 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(1 /* System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.Runtime.Internal.IRequest::get_Headers() */, IRequest_t2400804350_il2cpp_TypeInfo_var, L_2);
		String_t* L_4 = V_1;
		NullCheck(L_3);
		InterfaceActionInvoker2< String_t*, String_t* >::Invoke(5 /* System.Void System.Collections.Generic.IDictionary`2<System.String,System.String>::set_Item(!0,!1) */, IDictionary_2_t1943082916_il2cpp_TypeInfo_var, L_3, _stringLiteral3012313031, L_4);
		Il2CppObject * L_5 = V_0;
		NullCheck(L_5);
		Il2CppObject* L_6 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(1 /* System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.Runtime.Internal.IRequest::get_Headers() */, IRequest_t2400804350_il2cpp_TypeInfo_var, L_5);
		NullCheck(L_6);
		InterfaceActionInvoker2< String_t*, String_t* >::Invoke(5 /* System.Void System.Collections.Generic.IDictionary`2<System.String,System.String>::set_Item(!0,!1) */, IDictionary_2_t1943082916_il2cpp_TypeInfo_var, L_6, _stringLiteral1048821954, _stringLiteral1711157076);
		Il2CppObject * L_7 = V_0;
		NullCheck(L_7);
		InterfaceActionInvoker1< String_t* >::Invoke(7 /* System.Void Amazon.Runtime.Internal.IRequest::set_HttpMethod(System.String) */, IRequest_t2400804350_il2cpp_TypeInfo_var, L_7, _stringLiteral782856060);
		V_2 = _stringLiteral372029315;
		Il2CppObject * L_8 = V_0;
		String_t* L_9 = V_2;
		NullCheck(L_8);
		InterfaceActionInvoker1< String_t* >::Invoke(11 /* System.Void Amazon.Runtime.Internal.IRequest::set_ResourcePath(System.String) */, IRequest_t2400804350_il2cpp_TypeInfo_var, L_8, L_9);
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3500843524_il2cpp_TypeInfo_var);
		CultureInfo_t3500843524 * L_10 = CultureInfo_get_InvariantCulture_m398972276(NULL /*static, unused*/, /*hidden argument*/NULL);
		StringWriter_t4139609088 * L_11 = (StringWriter_t4139609088 *)il2cpp_codegen_object_new(StringWriter_t4139609088_il2cpp_TypeInfo_var);
		StringWriter__ctor_m3405704837(L_11, L_10, /*hidden argument*/NULL);
		V_3 = L_11;
	}

IL_005b:
	try
	{ // begin try (depth: 1)
		{
			StringWriter_t4139609088 * L_12 = V_3;
			JsonWriter_t3014444111 * L_13 = (JsonWriter_t3014444111 *)il2cpp_codegen_object_new(JsonWriter_t3014444111_il2cpp_TypeInfo_var);
			JsonWriter__ctor_m2344288487(L_13, L_12, /*hidden argument*/NULL);
			V_4 = L_13;
			JsonWriter_t3014444111 * L_14 = V_4;
			NullCheck(L_14);
			JsonWriter_WriteObjectStart_m3468068136(L_14, /*hidden argument*/NULL);
			Il2CppObject * L_15 = V_0;
			JsonWriter_t3014444111 * L_16 = V_4;
			JsonMarshallerContext_t4063314926 * L_17 = (JsonMarshallerContext_t4063314926 *)il2cpp_codegen_object_new(JsonMarshallerContext_t4063314926_il2cpp_TypeInfo_var);
			JsonMarshallerContext__ctor_m1418069536(L_17, L_15, L_16, /*hidden argument*/NULL);
			V_5 = L_17;
			GetOpenIdTokenRequest_t2698079901 * L_18 = ___publicRequest0;
			NullCheck(L_18);
			bool L_19 = GetOpenIdTokenRequest_IsSetIdentityId_m2534485277(L_18, /*hidden argument*/NULL);
			if (!L_19)
			{
				goto IL_009f;
			}
		}

IL_007c:
		{
			JsonMarshallerContext_t4063314926 * L_20 = V_5;
			NullCheck(L_20);
			JsonWriter_t3014444111 * L_21 = JsonMarshallerContext_get_Writer_m2343372188(L_20, /*hidden argument*/NULL);
			NullCheck(L_21);
			JsonWriter_WritePropertyName_m1171400361(L_21, _stringLiteral2345627623, /*hidden argument*/NULL);
			JsonMarshallerContext_t4063314926 * L_22 = V_5;
			NullCheck(L_22);
			JsonWriter_t3014444111 * L_23 = JsonMarshallerContext_get_Writer_m2343372188(L_22, /*hidden argument*/NULL);
			GetOpenIdTokenRequest_t2698079901 * L_24 = ___publicRequest0;
			NullCheck(L_24);
			String_t* L_25 = GetOpenIdTokenRequest_get_IdentityId_m4120850273(L_24, /*hidden argument*/NULL);
			NullCheck(L_23);
			JsonWriter_Write_m3974344637(L_23, L_25, /*hidden argument*/NULL);
		}

IL_009f:
		{
			GetOpenIdTokenRequest_t2698079901 * L_26 = ___publicRequest0;
			NullCheck(L_26);
			bool L_27 = GetOpenIdTokenRequest_IsSetLogins_m2650968532(L_26, /*hidden argument*/NULL);
			if (!L_27)
			{
				goto IL_012e;
			}
		}

IL_00aa:
		{
			JsonMarshallerContext_t4063314926 * L_28 = V_5;
			NullCheck(L_28);
			JsonWriter_t3014444111 * L_29 = JsonMarshallerContext_get_Writer_m2343372188(L_28, /*hidden argument*/NULL);
			NullCheck(L_29);
			JsonWriter_WritePropertyName_m1171400361(L_29, _stringLiteral2335677872, /*hidden argument*/NULL);
			JsonMarshallerContext_t4063314926 * L_30 = V_5;
			NullCheck(L_30);
			JsonWriter_t3014444111 * L_31 = JsonMarshallerContext_get_Writer_m2343372188(L_30, /*hidden argument*/NULL);
			NullCheck(L_31);
			JsonWriter_WriteObjectStart_m3468068136(L_31, /*hidden argument*/NULL);
			GetOpenIdTokenRequest_t2698079901 * L_32 = ___publicRequest0;
			NullCheck(L_32);
			Dictionary_2_t3943999495 * L_33 = GetOpenIdTokenRequest_get_Logins_m3321650339(L_32, /*hidden argument*/NULL);
			NullCheck(L_33);
			Enumerator_t969056901  L_34 = Dictionary_2_GetEnumerator_m2895728349(L_33, /*hidden argument*/Dictionary_2_GetEnumerator_m2895728349_MethodInfo_var);
			V_7 = L_34;
		}

IL_00d4:
		try
		{ // begin try (depth: 2)
			{
				goto IL_0109;
			}

IL_00d6:
			{
				KeyValuePair_2_t1701344717  L_35 = Enumerator_get_Current_m1989408781((&V_7), /*hidden argument*/Enumerator_get_Current_m1989408781_MethodInfo_var);
				V_8 = L_35;
				JsonMarshallerContext_t4063314926 * L_36 = V_5;
				NullCheck(L_36);
				JsonWriter_t3014444111 * L_37 = JsonMarshallerContext_get_Writer_m2343372188(L_36, /*hidden argument*/NULL);
				String_t* L_38 = KeyValuePair_2_get_Key_m1372024679((&V_8), /*hidden argument*/KeyValuePair_2_get_Key_m1372024679_MethodInfo_var);
				NullCheck(L_37);
				JsonWriter_WritePropertyName_m1171400361(L_37, L_38, /*hidden argument*/NULL);
				String_t* L_39 = KeyValuePair_2_get_Value_m1710042386((&V_8), /*hidden argument*/KeyValuePair_2_get_Value_m1710042386_MethodInfo_var);
				V_9 = L_39;
				JsonMarshallerContext_t4063314926 * L_40 = V_5;
				NullCheck(L_40);
				JsonWriter_t3014444111 * L_41 = JsonMarshallerContext_get_Writer_m2343372188(L_40, /*hidden argument*/NULL);
				String_t* L_42 = V_9;
				NullCheck(L_41);
				JsonWriter_Write_m3974344637(L_41, L_42, /*hidden argument*/NULL);
			}

IL_0109:
			{
				bool L_43 = Enumerator_MoveNext_m4005245300((&V_7), /*hidden argument*/Enumerator_MoveNext_m4005245300_MethodInfo_var);
				if (L_43)
				{
					goto IL_00d6;
				}
			}

IL_0112:
			{
				IL2CPP_LEAVE(0x122, FINALLY_0114);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
			goto FINALLY_0114;
		}

FINALLY_0114:
		{ // begin finally (depth: 2)
			Enumerator_Dispose_m882561911((&V_7), /*hidden argument*/Enumerator_Dispose_m882561911_MethodInfo_var);
			IL2CPP_END_FINALLY(276)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(276)
		{
			IL2CPP_JUMP_TBL(0x122, IL_0122)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
		}

IL_0122:
		{
			JsonMarshallerContext_t4063314926 * L_44 = V_5;
			NullCheck(L_44);
			JsonWriter_t3014444111 * L_45 = JsonMarshallerContext_get_Writer_m2343372188(L_44, /*hidden argument*/NULL);
			NullCheck(L_45);
			JsonWriter_WriteObjectEnd_m4253837441(L_45, /*hidden argument*/NULL);
		}

IL_012e:
		{
			JsonWriter_t3014444111 * L_46 = V_4;
			NullCheck(L_46);
			JsonWriter_WriteObjectEnd_m4253837441(L_46, /*hidden argument*/NULL);
			StringWriter_t4139609088 * L_47 = V_3;
			NullCheck(L_47);
			String_t* L_48 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_47);
			V_6 = L_48;
			Il2CppObject * L_49 = V_0;
			IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
			Encoding_t663144255 * L_50 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
			String_t* L_51 = V_6;
			NullCheck(L_50);
			ByteU5BU5D_t3397334013* L_52 = VirtFuncInvoker1< ByteU5BU5D_t3397334013*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_50, L_51);
			NullCheck(L_49);
			InterfaceActionInvoker1< ByteU5BU5D_t3397334013* >::Invoke(13 /* System.Void Amazon.Runtime.Internal.IRequest::set_Content(System.Byte[]) */, IRequest_t2400804350_il2cpp_TypeInfo_var, L_49, L_52);
			IL2CPP_LEAVE(0x15B, FINALLY_0151);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0151;
	}

FINALLY_0151:
	{ // begin finally (depth: 1)
		{
			StringWriter_t4139609088 * L_53 = V_3;
			if (!L_53)
			{
				goto IL_015a;
			}
		}

IL_0154:
		{
			StringWriter_t4139609088 * L_54 = V_3;
			NullCheck(L_54);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_54);
		}

IL_015a:
		{
			IL2CPP_END_FINALLY(337)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(337)
	{
		IL2CPP_JUMP_TBL(0x15B, IL_015b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_015b:
	{
		Il2CppObject * L_55 = V_0;
		return L_55;
	}
}
// System.Void Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetOpenIdTokenRequestMarshaller::.ctor()
extern "C"  void GetOpenIdTokenRequestMarshaller__ctor_m355897200 (GetOpenIdTokenRequestMarshaller_t1236206204 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// Amazon.Runtime.AmazonWebServiceResponse Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetOpenIdTokenResponseUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern Il2CppClass* GetOpenIdTokenResponse_t955597635_il2cpp_TypeInfo_var;
extern Il2CppClass* StringUnmarshaller_t3953260147_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2345627623;
extern Il2CppCodeGenString* _stringLiteral1601847199;
extern const uint32_t GetOpenIdTokenResponseUnmarshaller_Unmarshall_m762239849_MetadataUsageId;
extern "C"  AmazonWebServiceResponse_t529043356 * GetOpenIdTokenResponseUnmarshaller_Unmarshall_m762239849 (GetOpenIdTokenResponseUnmarshaller_t3130322723 * __this, JsonUnmarshallerContext_t456235889 * ___context0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GetOpenIdTokenResponseUnmarshaller_Unmarshall_m762239849_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GetOpenIdTokenResponse_t955597635 * V_0 = NULL;
	int32_t V_1 = 0;
	StringUnmarshaller_t3953260147 * V_2 = NULL;
	StringUnmarshaller_t3953260147 * V_3 = NULL;
	{
		GetOpenIdTokenResponse_t955597635 * L_0 = (GetOpenIdTokenResponse_t955597635 *)il2cpp_codegen_object_new(GetOpenIdTokenResponse_t955597635_il2cpp_TypeInfo_var);
		GetOpenIdTokenResponse__ctor_m3771882210(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		JsonUnmarshallerContext_t456235889 * L_1 = ___context0;
		NullCheck(L_1);
		VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean Amazon.Runtime.Internal.Transform.UnmarshallerContext::Read() */, L_1);
		JsonUnmarshallerContext_t456235889 * L_2 = ___context0;
		NullCheck(L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 Amazon.Runtime.Internal.Transform.UnmarshallerContext::get_CurrentDepth() */, L_2);
		V_1 = L_3;
		goto IL_005a;
	}

IL_0016:
	{
		JsonUnmarshallerContext_t456235889 * L_4 = ___context0;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		bool L_6 = UnmarshallerContext_TestExpression_m3665247446(L_4, _stringLiteral2345627623, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0039;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(StringUnmarshaller_t3953260147_il2cpp_TypeInfo_var);
		StringUnmarshaller_t3953260147 * L_7 = StringUnmarshaller_get_Instance_m107503370(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = L_7;
		GetOpenIdTokenResponse_t955597635 * L_8 = V_0;
		StringUnmarshaller_t3953260147 * L_9 = V_2;
		JsonUnmarshallerContext_t456235889 * L_10 = ___context0;
		NullCheck(L_9);
		String_t* L_11 = StringUnmarshaller_Unmarshall_m241138027(L_9, L_10, /*hidden argument*/NULL);
		NullCheck(L_8);
		GetOpenIdTokenResponse_set_IdentityId_m1894373264(L_8, L_11, /*hidden argument*/NULL);
		goto IL_005a;
	}

IL_0039:
	{
		JsonUnmarshallerContext_t456235889 * L_12 = ___context0;
		int32_t L_13 = V_1;
		NullCheck(L_12);
		bool L_14 = UnmarshallerContext_TestExpression_m3665247446(L_12, _stringLiteral1601847199, L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_005a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(StringUnmarshaller_t3953260147_il2cpp_TypeInfo_var);
		StringUnmarshaller_t3953260147 * L_15 = StringUnmarshaller_get_Instance_m107503370(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_3 = L_15;
		GetOpenIdTokenResponse_t955597635 * L_16 = V_0;
		StringUnmarshaller_t3953260147 * L_17 = V_3;
		JsonUnmarshallerContext_t456235889 * L_18 = ___context0;
		NullCheck(L_17);
		String_t* L_19 = StringUnmarshaller_Unmarshall_m241138027(L_17, L_18, /*hidden argument*/NULL);
		NullCheck(L_16);
		GetOpenIdTokenResponse_set_Token_m40921210(L_16, L_19, /*hidden argument*/NULL);
	}

IL_005a:
	{
		JsonUnmarshallerContext_t456235889 * L_20 = ___context0;
		int32_t L_21 = V_1;
		NullCheck(L_20);
		bool L_22 = UnmarshallerContext_ReadAtDepth_m2943457426(L_20, L_21, /*hidden argument*/NULL);
		if (L_22)
		{
			goto IL_0016;
		}
	}
	{
		GetOpenIdTokenResponse_t955597635 * L_23 = V_0;
		return L_23;
	}
}
// Amazon.Runtime.AmazonServiceException Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetOpenIdTokenResponseUnmarshaller::UnmarshallException(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,System.Exception,System.Net.HttpStatusCode)
extern Il2CppClass* ExternalServiceException_t712152635_il2cpp_TypeInfo_var;
extern Il2CppClass* InternalErrorException_t1116634264_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidParameterException_t2482528107_il2cpp_TypeInfo_var;
extern Il2CppClass* NotAuthorizedException_t457234085_il2cpp_TypeInfo_var;
extern Il2CppClass* ResourceConflictException_t4018382715_il2cpp_TypeInfo_var;
extern Il2CppClass* ResourceNotFoundException_t403010890_il2cpp_TypeInfo_var;
extern Il2CppClass* TooManyRequestsException_t3276202790_il2cpp_TypeInfo_var;
extern Il2CppClass* AmazonCognitoIdentityException_t158465174_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1101196531;
extern Il2CppCodeGenString* _stringLiteral629809416;
extern Il2CppCodeGenString* _stringLiteral270646655;
extern Il2CppCodeGenString* _stringLiteral4046014057;
extern Il2CppCodeGenString* _stringLiteral4089858387;
extern Il2CppCodeGenString* _stringLiteral2922393814;
extern Il2CppCodeGenString* _stringLiteral2413473506;
extern const uint32_t GetOpenIdTokenResponseUnmarshaller_UnmarshallException_m1148820989_MetadataUsageId;
extern "C"  AmazonServiceException_t3748559634 * GetOpenIdTokenResponseUnmarshaller_UnmarshallException_m1148820989 (GetOpenIdTokenResponseUnmarshaller_t3130322723 * __this, JsonUnmarshallerContext_t456235889 * ___context0, Exception_t1927440687 * ___innerException1, int32_t ___statusCode2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GetOpenIdTokenResponseUnmarshaller_UnmarshallException_m1148820989_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ErrorResponse_t3502566035 * V_0 = NULL;
	{
		JsonErrorResponseUnmarshaller_t4149926919 * L_0 = JsonErrorResponseUnmarshaller_GetInstance_m1721802307(NULL /*static, unused*/, /*hidden argument*/NULL);
		JsonUnmarshallerContext_t456235889 * L_1 = ___context0;
		NullCheck(L_0);
		ErrorResponse_t3502566035 * L_2 = JsonErrorResponseUnmarshaller_Unmarshall_m3547220927(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		ErrorResponse_t3502566035 * L_3 = V_0;
		NullCheck(L_3);
		String_t* L_4 = ErrorResponse_get_Code_m1007089050(L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0046;
		}
	}
	{
		ErrorResponse_t3502566035 * L_5 = V_0;
		NullCheck(L_5);
		String_t* L_6 = ErrorResponse_get_Code_m1007089050(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		bool L_7 = String_Equals_m2633592423(L_6, _stringLiteral1101196531, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0046;
		}
	}
	{
		ErrorResponse_t3502566035 * L_8 = V_0;
		NullCheck(L_8);
		String_t* L_9 = ErrorResponse_get_Message_m1145825032(L_8, /*hidden argument*/NULL);
		Exception_t1927440687 * L_10 = ___innerException1;
		ErrorResponse_t3502566035 * L_11 = V_0;
		NullCheck(L_11);
		int32_t L_12 = ErrorResponse_get_Type_m3388577699(L_11, /*hidden argument*/NULL);
		ErrorResponse_t3502566035 * L_13 = V_0;
		NullCheck(L_13);
		String_t* L_14 = ErrorResponse_get_Code_m1007089050(L_13, /*hidden argument*/NULL);
		ErrorResponse_t3502566035 * L_15 = V_0;
		NullCheck(L_15);
		String_t* L_16 = ErrorResponse_get_RequestId_m2536924555(L_15, /*hidden argument*/NULL);
		int32_t L_17 = ___statusCode2;
		ExternalServiceException_t712152635 * L_18 = (ExternalServiceException_t712152635 *)il2cpp_codegen_object_new(ExternalServiceException_t712152635_il2cpp_TypeInfo_var);
		ExternalServiceException__ctor_m3707348563(L_18, L_9, L_10, L_12, L_14, L_16, L_17, /*hidden argument*/NULL);
		return L_18;
	}

IL_0046:
	{
		ErrorResponse_t3502566035 * L_19 = V_0;
		NullCheck(L_19);
		String_t* L_20 = ErrorResponse_get_Code_m1007089050(L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_0080;
		}
	}
	{
		ErrorResponse_t3502566035 * L_21 = V_0;
		NullCheck(L_21);
		String_t* L_22 = ErrorResponse_get_Code_m1007089050(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		bool L_23 = String_Equals_m2633592423(L_22, _stringLiteral629809416, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_0080;
		}
	}
	{
		ErrorResponse_t3502566035 * L_24 = V_0;
		NullCheck(L_24);
		String_t* L_25 = ErrorResponse_get_Message_m1145825032(L_24, /*hidden argument*/NULL);
		Exception_t1927440687 * L_26 = ___innerException1;
		ErrorResponse_t3502566035 * L_27 = V_0;
		NullCheck(L_27);
		int32_t L_28 = ErrorResponse_get_Type_m3388577699(L_27, /*hidden argument*/NULL);
		ErrorResponse_t3502566035 * L_29 = V_0;
		NullCheck(L_29);
		String_t* L_30 = ErrorResponse_get_Code_m1007089050(L_29, /*hidden argument*/NULL);
		ErrorResponse_t3502566035 * L_31 = V_0;
		NullCheck(L_31);
		String_t* L_32 = ErrorResponse_get_RequestId_m2536924555(L_31, /*hidden argument*/NULL);
		int32_t L_33 = ___statusCode2;
		InternalErrorException_t1116634264 * L_34 = (InternalErrorException_t1116634264 *)il2cpp_codegen_object_new(InternalErrorException_t1116634264_il2cpp_TypeInfo_var);
		InternalErrorException__ctor_m586328184(L_34, L_25, L_26, L_28, L_30, L_32, L_33, /*hidden argument*/NULL);
		return L_34;
	}

IL_0080:
	{
		ErrorResponse_t3502566035 * L_35 = V_0;
		NullCheck(L_35);
		String_t* L_36 = ErrorResponse_get_Code_m1007089050(L_35, /*hidden argument*/NULL);
		if (!L_36)
		{
			goto IL_00ba;
		}
	}
	{
		ErrorResponse_t3502566035 * L_37 = V_0;
		NullCheck(L_37);
		String_t* L_38 = ErrorResponse_get_Code_m1007089050(L_37, /*hidden argument*/NULL);
		NullCheck(L_38);
		bool L_39 = String_Equals_m2633592423(L_38, _stringLiteral270646655, /*hidden argument*/NULL);
		if (!L_39)
		{
			goto IL_00ba;
		}
	}
	{
		ErrorResponse_t3502566035 * L_40 = V_0;
		NullCheck(L_40);
		String_t* L_41 = ErrorResponse_get_Message_m1145825032(L_40, /*hidden argument*/NULL);
		Exception_t1927440687 * L_42 = ___innerException1;
		ErrorResponse_t3502566035 * L_43 = V_0;
		NullCheck(L_43);
		int32_t L_44 = ErrorResponse_get_Type_m3388577699(L_43, /*hidden argument*/NULL);
		ErrorResponse_t3502566035 * L_45 = V_0;
		NullCheck(L_45);
		String_t* L_46 = ErrorResponse_get_Code_m1007089050(L_45, /*hidden argument*/NULL);
		ErrorResponse_t3502566035 * L_47 = V_0;
		NullCheck(L_47);
		String_t* L_48 = ErrorResponse_get_RequestId_m2536924555(L_47, /*hidden argument*/NULL);
		int32_t L_49 = ___statusCode2;
		InvalidParameterException_t2482528107 * L_50 = (InvalidParameterException_t2482528107 *)il2cpp_codegen_object_new(InvalidParameterException_t2482528107_il2cpp_TypeInfo_var);
		InvalidParameterException__ctor_m3019345047(L_50, L_41, L_42, L_44, L_46, L_48, L_49, /*hidden argument*/NULL);
		return L_50;
	}

IL_00ba:
	{
		ErrorResponse_t3502566035 * L_51 = V_0;
		NullCheck(L_51);
		String_t* L_52 = ErrorResponse_get_Code_m1007089050(L_51, /*hidden argument*/NULL);
		if (!L_52)
		{
			goto IL_00f4;
		}
	}
	{
		ErrorResponse_t3502566035 * L_53 = V_0;
		NullCheck(L_53);
		String_t* L_54 = ErrorResponse_get_Code_m1007089050(L_53, /*hidden argument*/NULL);
		NullCheck(L_54);
		bool L_55 = String_Equals_m2633592423(L_54, _stringLiteral4046014057, /*hidden argument*/NULL);
		if (!L_55)
		{
			goto IL_00f4;
		}
	}
	{
		ErrorResponse_t3502566035 * L_56 = V_0;
		NullCheck(L_56);
		String_t* L_57 = ErrorResponse_get_Message_m1145825032(L_56, /*hidden argument*/NULL);
		Exception_t1927440687 * L_58 = ___innerException1;
		ErrorResponse_t3502566035 * L_59 = V_0;
		NullCheck(L_59);
		int32_t L_60 = ErrorResponse_get_Type_m3388577699(L_59, /*hidden argument*/NULL);
		ErrorResponse_t3502566035 * L_61 = V_0;
		NullCheck(L_61);
		String_t* L_62 = ErrorResponse_get_Code_m1007089050(L_61, /*hidden argument*/NULL);
		ErrorResponse_t3502566035 * L_63 = V_0;
		NullCheck(L_63);
		String_t* L_64 = ErrorResponse_get_RequestId_m2536924555(L_63, /*hidden argument*/NULL);
		int32_t L_65 = ___statusCode2;
		NotAuthorizedException_t457234085 * L_66 = (NotAuthorizedException_t457234085 *)il2cpp_codegen_object_new(NotAuthorizedException_t457234085_il2cpp_TypeInfo_var);
		NotAuthorizedException__ctor_m121792233(L_66, L_57, L_58, L_60, L_62, L_64, L_65, /*hidden argument*/NULL);
		return L_66;
	}

IL_00f4:
	{
		ErrorResponse_t3502566035 * L_67 = V_0;
		NullCheck(L_67);
		String_t* L_68 = ErrorResponse_get_Code_m1007089050(L_67, /*hidden argument*/NULL);
		if (!L_68)
		{
			goto IL_012e;
		}
	}
	{
		ErrorResponse_t3502566035 * L_69 = V_0;
		NullCheck(L_69);
		String_t* L_70 = ErrorResponse_get_Code_m1007089050(L_69, /*hidden argument*/NULL);
		NullCheck(L_70);
		bool L_71 = String_Equals_m2633592423(L_70, _stringLiteral4089858387, /*hidden argument*/NULL);
		if (!L_71)
		{
			goto IL_012e;
		}
	}
	{
		ErrorResponse_t3502566035 * L_72 = V_0;
		NullCheck(L_72);
		String_t* L_73 = ErrorResponse_get_Message_m1145825032(L_72, /*hidden argument*/NULL);
		Exception_t1927440687 * L_74 = ___innerException1;
		ErrorResponse_t3502566035 * L_75 = V_0;
		NullCheck(L_75);
		int32_t L_76 = ErrorResponse_get_Type_m3388577699(L_75, /*hidden argument*/NULL);
		ErrorResponse_t3502566035 * L_77 = V_0;
		NullCheck(L_77);
		String_t* L_78 = ErrorResponse_get_Code_m1007089050(L_77, /*hidden argument*/NULL);
		ErrorResponse_t3502566035 * L_79 = V_0;
		NullCheck(L_79);
		String_t* L_80 = ErrorResponse_get_RequestId_m2536924555(L_79, /*hidden argument*/NULL);
		int32_t L_81 = ___statusCode2;
		ResourceConflictException_t4018382715 * L_82 = (ResourceConflictException_t4018382715 *)il2cpp_codegen_object_new(ResourceConflictException_t4018382715_il2cpp_TypeInfo_var);
		ResourceConflictException__ctor_m526234331(L_82, L_73, L_74, L_76, L_78, L_80, L_81, /*hidden argument*/NULL);
		return L_82;
	}

IL_012e:
	{
		ErrorResponse_t3502566035 * L_83 = V_0;
		NullCheck(L_83);
		String_t* L_84 = ErrorResponse_get_Code_m1007089050(L_83, /*hidden argument*/NULL);
		if (!L_84)
		{
			goto IL_0168;
		}
	}
	{
		ErrorResponse_t3502566035 * L_85 = V_0;
		NullCheck(L_85);
		String_t* L_86 = ErrorResponse_get_Code_m1007089050(L_85, /*hidden argument*/NULL);
		NullCheck(L_86);
		bool L_87 = String_Equals_m2633592423(L_86, _stringLiteral2922393814, /*hidden argument*/NULL);
		if (!L_87)
		{
			goto IL_0168;
		}
	}
	{
		ErrorResponse_t3502566035 * L_88 = V_0;
		NullCheck(L_88);
		String_t* L_89 = ErrorResponse_get_Message_m1145825032(L_88, /*hidden argument*/NULL);
		Exception_t1927440687 * L_90 = ___innerException1;
		ErrorResponse_t3502566035 * L_91 = V_0;
		NullCheck(L_91);
		int32_t L_92 = ErrorResponse_get_Type_m3388577699(L_91, /*hidden argument*/NULL);
		ErrorResponse_t3502566035 * L_93 = V_0;
		NullCheck(L_93);
		String_t* L_94 = ErrorResponse_get_Code_m1007089050(L_93, /*hidden argument*/NULL);
		ErrorResponse_t3502566035 * L_95 = V_0;
		NullCheck(L_95);
		String_t* L_96 = ErrorResponse_get_RequestId_m2536924555(L_95, /*hidden argument*/NULL);
		int32_t L_97 = ___statusCode2;
		ResourceNotFoundException_t403010890 * L_98 = (ResourceNotFoundException_t403010890 *)il2cpp_codegen_object_new(ResourceNotFoundException_t403010890_il2cpp_TypeInfo_var);
		ResourceNotFoundException__ctor_m3227125662(L_98, L_89, L_90, L_92, L_94, L_96, L_97, /*hidden argument*/NULL);
		return L_98;
	}

IL_0168:
	{
		ErrorResponse_t3502566035 * L_99 = V_0;
		NullCheck(L_99);
		String_t* L_100 = ErrorResponse_get_Code_m1007089050(L_99, /*hidden argument*/NULL);
		if (!L_100)
		{
			goto IL_01a2;
		}
	}
	{
		ErrorResponse_t3502566035 * L_101 = V_0;
		NullCheck(L_101);
		String_t* L_102 = ErrorResponse_get_Code_m1007089050(L_101, /*hidden argument*/NULL);
		NullCheck(L_102);
		bool L_103 = String_Equals_m2633592423(L_102, _stringLiteral2413473506, /*hidden argument*/NULL);
		if (!L_103)
		{
			goto IL_01a2;
		}
	}
	{
		ErrorResponse_t3502566035 * L_104 = V_0;
		NullCheck(L_104);
		String_t* L_105 = ErrorResponse_get_Message_m1145825032(L_104, /*hidden argument*/NULL);
		Exception_t1927440687 * L_106 = ___innerException1;
		ErrorResponse_t3502566035 * L_107 = V_0;
		NullCheck(L_107);
		int32_t L_108 = ErrorResponse_get_Type_m3388577699(L_107, /*hidden argument*/NULL);
		ErrorResponse_t3502566035 * L_109 = V_0;
		NullCheck(L_109);
		String_t* L_110 = ErrorResponse_get_Code_m1007089050(L_109, /*hidden argument*/NULL);
		ErrorResponse_t3502566035 * L_111 = V_0;
		NullCheck(L_111);
		String_t* L_112 = ErrorResponse_get_RequestId_m2536924555(L_111, /*hidden argument*/NULL);
		int32_t L_113 = ___statusCode2;
		TooManyRequestsException_t3276202790 * L_114 = (TooManyRequestsException_t3276202790 *)il2cpp_codegen_object_new(TooManyRequestsException_t3276202790_il2cpp_TypeInfo_var);
		TooManyRequestsException__ctor_m439418258(L_114, L_105, L_106, L_108, L_110, L_112, L_113, /*hidden argument*/NULL);
		return L_114;
	}

IL_01a2:
	{
		ErrorResponse_t3502566035 * L_115 = V_0;
		NullCheck(L_115);
		String_t* L_116 = ErrorResponse_get_Message_m1145825032(L_115, /*hidden argument*/NULL);
		Exception_t1927440687 * L_117 = ___innerException1;
		ErrorResponse_t3502566035 * L_118 = V_0;
		NullCheck(L_118);
		int32_t L_119 = ErrorResponse_get_Type_m3388577699(L_118, /*hidden argument*/NULL);
		ErrorResponse_t3502566035 * L_120 = V_0;
		NullCheck(L_120);
		String_t* L_121 = ErrorResponse_get_Code_m1007089050(L_120, /*hidden argument*/NULL);
		ErrorResponse_t3502566035 * L_122 = V_0;
		NullCheck(L_122);
		String_t* L_123 = ErrorResponse_get_RequestId_m2536924555(L_122, /*hidden argument*/NULL);
		int32_t L_124 = ___statusCode2;
		AmazonCognitoIdentityException_t158465174 * L_125 = (AmazonCognitoIdentityException_t158465174 *)il2cpp_codegen_object_new(AmazonCognitoIdentityException_t158465174_il2cpp_TypeInfo_var);
		AmazonCognitoIdentityException__ctor_m1060593889(L_125, L_116, L_117, L_119, L_121, L_123, L_124, /*hidden argument*/NULL);
		return L_125;
	}
}
// Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetOpenIdTokenResponseUnmarshaller Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetOpenIdTokenResponseUnmarshaller::get_Instance()
extern Il2CppClass* GetOpenIdTokenResponseUnmarshaller_t3130322723_il2cpp_TypeInfo_var;
extern const uint32_t GetOpenIdTokenResponseUnmarshaller_get_Instance_m41713146_MetadataUsageId;
extern "C"  GetOpenIdTokenResponseUnmarshaller_t3130322723 * GetOpenIdTokenResponseUnmarshaller_get_Instance_m41713146 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GetOpenIdTokenResponseUnmarshaller_get_Instance_m41713146_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GetOpenIdTokenResponseUnmarshaller_t3130322723_il2cpp_TypeInfo_var);
		GetOpenIdTokenResponseUnmarshaller_t3130322723 * L_0 = ((GetOpenIdTokenResponseUnmarshaller_t3130322723_StaticFields*)GetOpenIdTokenResponseUnmarshaller_t3130322723_il2cpp_TypeInfo_var->static_fields)->get__instance_0();
		return L_0;
	}
}
// System.Void Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetOpenIdTokenResponseUnmarshaller::.ctor()
extern "C"  void GetOpenIdTokenResponseUnmarshaller__ctor_m2574827649 (GetOpenIdTokenResponseUnmarshaller_t3130322723 * __this, const MethodInfo* method)
{
	{
		JsonResponseUnmarshaller__ctor_m2636444097(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetOpenIdTokenResponseUnmarshaller::.cctor()
extern Il2CppClass* GetOpenIdTokenResponseUnmarshaller_t3130322723_il2cpp_TypeInfo_var;
extern const uint32_t GetOpenIdTokenResponseUnmarshaller__cctor_m2618393120_MetadataUsageId;
extern "C"  void GetOpenIdTokenResponseUnmarshaller__cctor_m2618393120 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GetOpenIdTokenResponseUnmarshaller__cctor_m2618393120_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GetOpenIdTokenResponseUnmarshaller_t3130322723 * L_0 = (GetOpenIdTokenResponseUnmarshaller_t3130322723 *)il2cpp_codegen_object_new(GetOpenIdTokenResponseUnmarshaller_t3130322723_il2cpp_TypeInfo_var);
		GetOpenIdTokenResponseUnmarshaller__ctor_m2574827649(L_0, /*hidden argument*/NULL);
		((GetOpenIdTokenResponseUnmarshaller_t3130322723_StaticFields*)GetOpenIdTokenResponseUnmarshaller_t3130322723_il2cpp_TypeInfo_var->static_fields)->set__instance_0(L_0);
		return;
	}
}
// System.Void Amazon.CognitoIdentity.Model.InternalErrorException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
extern "C"  void InternalErrorException__ctor_m586328184 (InternalErrorException_t1116634264 * __this, String_t* ___message0, Exception_t1927440687 * ___innerException1, int32_t ___errorType2, String_t* ___errorCode3, String_t* ___requestId4, int32_t ___statusCode5, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		Exception_t1927440687 * L_1 = ___innerException1;
		int32_t L_2 = ___errorType2;
		String_t* L_3 = ___errorCode3;
		String_t* L_4 = ___requestId4;
		int32_t L_5 = ___statusCode5;
		AmazonCognitoIdentityException__ctor_m1060593889(__this, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Amazon.CognitoIdentity.Model.InternalErrorException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void InternalErrorException__ctor_m86243988 (InternalErrorException_t1116634264 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method)
{
	{
		SerializationInfo_t228987430 * L_0 = ___info0;
		StreamingContext_t1417235061  L_1 = ___context1;
		AmazonCognitoIdentityException__ctor_m1824076445(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Amazon.CognitoIdentity.Model.InvalidIdentityPoolConfigurationException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
extern "C"  void InvalidIdentityPoolConfigurationException__ctor_m1901361182 (InvalidIdentityPoolConfigurationException_t50815546 * __this, String_t* ___message0, Exception_t1927440687 * ___innerException1, int32_t ___errorType2, String_t* ___errorCode3, String_t* ___requestId4, int32_t ___statusCode5, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		Exception_t1927440687 * L_1 = ___innerException1;
		int32_t L_2 = ___errorType2;
		String_t* L_3 = ___errorCode3;
		String_t* L_4 = ___requestId4;
		int32_t L_5 = ___statusCode5;
		AmazonCognitoIdentityException__ctor_m1060593889(__this, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Amazon.CognitoIdentity.Model.InvalidIdentityPoolConfigurationException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void InvalidIdentityPoolConfigurationException__ctor_m3042261410 (InvalidIdentityPoolConfigurationException_t50815546 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method)
{
	{
		SerializationInfo_t228987430 * L_0 = ___info0;
		StreamingContext_t1417235061  L_1 = ___context1;
		AmazonCognitoIdentityException__ctor_m1824076445(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Amazon.CognitoIdentity.Model.InvalidParameterException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
extern "C"  void InvalidParameterException__ctor_m3019345047 (InvalidParameterException_t2482528107 * __this, String_t* ___message0, Exception_t1927440687 * ___innerException1, int32_t ___errorType2, String_t* ___errorCode3, String_t* ___requestId4, int32_t ___statusCode5, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		Exception_t1927440687 * L_1 = ___innerException1;
		int32_t L_2 = ___errorType2;
		String_t* L_3 = ___errorCode3;
		String_t* L_4 = ___requestId4;
		int32_t L_5 = ___statusCode5;
		AmazonCognitoIdentityException__ctor_m1060593889(__this, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Amazon.CognitoIdentity.Model.InvalidParameterException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void InvalidParameterException__ctor_m3208343359 (InvalidParameterException_t2482528107 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method)
{
	{
		SerializationInfo_t228987430 * L_0 = ___info0;
		StreamingContext_t1417235061  L_1 = ___context1;
		AmazonCognitoIdentityException__ctor_m1824076445(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Amazon.CognitoIdentity.Model.LimitExceededException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
extern "C"  void LimitExceededException__ctor_m1705519093 (LimitExceededException_t3250699605 * __this, String_t* ___message0, Exception_t1927440687 * ___innerException1, int32_t ___errorType2, String_t* ___errorCode3, String_t* ___requestId4, int32_t ___statusCode5, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		Exception_t1927440687 * L_1 = ___innerException1;
		int32_t L_2 = ___errorType2;
		String_t* L_3 = ___errorCode3;
		String_t* L_4 = ___requestId4;
		int32_t L_5 = ___statusCode5;
		AmazonCognitoIdentityException__ctor_m1060593889(__this, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Amazon.CognitoIdentity.Model.LimitExceededException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void LimitExceededException__ctor_m3737500505 (LimitExceededException_t3250699605 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method)
{
	{
		SerializationInfo_t228987430 * L_0 = ___info0;
		StreamingContext_t1417235061  L_1 = ___context1;
		AmazonCognitoIdentityException__ctor_m1824076445(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Amazon.CognitoIdentity.Model.NotAuthorizedException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
extern "C"  void NotAuthorizedException__ctor_m121792233 (NotAuthorizedException_t457234085 * __this, String_t* ___message0, Exception_t1927440687 * ___innerException1, int32_t ___errorType2, String_t* ___errorCode3, String_t* ___requestId4, int32_t ___statusCode5, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		Exception_t1927440687 * L_1 = ___innerException1;
		int32_t L_2 = ___errorType2;
		String_t* L_3 = ___errorCode3;
		String_t* L_4 = ___requestId4;
		int32_t L_5 = ___statusCode5;
		AmazonCognitoIdentityException__ctor_m1060593889(__this, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Amazon.CognitoIdentity.Model.NotAuthorizedException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void NotAuthorizedException__ctor_m3158292581 (NotAuthorizedException_t457234085 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method)
{
	{
		SerializationInfo_t228987430 * L_0 = ___info0;
		StreamingContext_t1417235061  L_1 = ___context1;
		AmazonCognitoIdentityException__ctor_m1824076445(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Amazon.CognitoIdentity.Model.ResourceConflictException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
extern "C"  void ResourceConflictException__ctor_m526234331 (ResourceConflictException_t4018382715 * __this, String_t* ___message0, Exception_t1927440687 * ___innerException1, int32_t ___errorType2, String_t* ___errorCode3, String_t* ___requestId4, int32_t ___statusCode5, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		Exception_t1927440687 * L_1 = ___innerException1;
		int32_t L_2 = ___errorType2;
		String_t* L_3 = ___errorCode3;
		String_t* L_4 = ___requestId4;
		int32_t L_5 = ___statusCode5;
		AmazonCognitoIdentityException__ctor_m1060593889(__this, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Amazon.CognitoIdentity.Model.ResourceConflictException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void ResourceConflictException__ctor_m1586531027 (ResourceConflictException_t4018382715 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method)
{
	{
		SerializationInfo_t228987430 * L_0 = ___info0;
		StreamingContext_t1417235061  L_1 = ___context1;
		AmazonCognitoIdentityException__ctor_m1824076445(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Amazon.CognitoIdentity.Model.ResourceNotFoundException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
extern "C"  void ResourceNotFoundException__ctor_m3227125662 (ResourceNotFoundException_t403010890 * __this, String_t* ___message0, Exception_t1927440687 * ___innerException1, int32_t ___errorType2, String_t* ___errorCode3, String_t* ___requestId4, int32_t ___statusCode5, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		Exception_t1927440687 * L_1 = ___innerException1;
		int32_t L_2 = ___errorType2;
		String_t* L_3 = ___errorCode3;
		String_t* L_4 = ___requestId4;
		int32_t L_5 = ___statusCode5;
		AmazonCognitoIdentityException__ctor_m1060593889(__this, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Amazon.CognitoIdentity.Model.ResourceNotFoundException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void ResourceNotFoundException__ctor_m1332952022 (ResourceNotFoundException_t403010890 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method)
{
	{
		SerializationInfo_t228987430 * L_0 = ___info0;
		StreamingContext_t1417235061  L_1 = ___context1;
		AmazonCognitoIdentityException__ctor_m1824076445(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Amazon.CognitoIdentity.Model.TooManyRequestsException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
extern "C"  void TooManyRequestsException__ctor_m439418258 (TooManyRequestsException_t3276202790 * __this, String_t* ___message0, Exception_t1927440687 * ___innerException1, int32_t ___errorType2, String_t* ___errorCode3, String_t* ___requestId4, int32_t ___statusCode5, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		Exception_t1927440687 * L_1 = ___innerException1;
		int32_t L_2 = ___errorType2;
		String_t* L_3 = ___errorCode3;
		String_t* L_4 = ___requestId4;
		int32_t L_5 = ___statusCode5;
		AmazonCognitoIdentityException__ctor_m1060593889(__this, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Amazon.CognitoIdentity.Model.TooManyRequestsException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void TooManyRequestsException__ctor_m4222899162 (TooManyRequestsException_t3276202790 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method)
{
	{
		SerializationInfo_t228987430 * L_0 = ___info0;
		StreamingContext_t1417235061  L_1 = ___context1;
		AmazonCognitoIdentityException__ctor_m1824076445(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
