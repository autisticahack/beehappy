﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.Linq.XObject
struct XObject_t3550811009;
// System.String
struct String_t;
// System.Xml.Linq.XDocument
struct XDocument_t2733326047;
// System.Xml.Linq.XElement
struct XElement_t553821050;
// System.Xml.Linq.XContainer
struct XContainer_t1445911831;
// System.Xml.XmlReader
struct XmlReader_t3675626668;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "System_Xml_Linq_System_Xml_Linq_XContainer1445911831.h"
#include "System_Xml_System_Xml_XmlReader3675626668.h"
#include "System_Xml_Linq_System_Xml_Linq_LoadOptions53198144.h"

// System.Void System.Xml.Linq.XObject::.ctor()
extern "C"  void XObject__ctor_m3797862556 (XObject_t3550811009 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Xml.Linq.XObject::System.Xml.IXmlLineInfo.get_LineNumber()
extern "C"  int32_t XObject_System_Xml_IXmlLineInfo_get_LineNumber_m627565582 (XObject_t3550811009 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Xml.Linq.XObject::System.Xml.IXmlLineInfo.get_LinePosition()
extern "C"  int32_t XObject_System_Xml_IXmlLineInfo_get_LinePosition_m1973748000 (XObject_t3550811009 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.Linq.XObject::System.Xml.IXmlLineInfo.HasLineInfo()
extern "C"  bool XObject_System_Xml_IXmlLineInfo_HasLineInfo_m608625524 (XObject_t3550811009 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Linq.XObject::set_BaseUri(System.String)
extern "C"  void XObject_set_BaseUri_m3120446180 (XObject_t3550811009 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Linq.XDocument System.Xml.Linq.XObject::get_Document()
extern "C"  XDocument_t2733326047 * XObject_get_Document_m418804020 (XObject_t3550811009 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Linq.XElement System.Xml.Linq.XObject::get_Parent()
extern "C"  XElement_t553821050 * XObject_get_Parent_m714109026 (XObject_t3550811009 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Linq.XContainer System.Xml.Linq.XObject::get_Owner()
extern "C"  XContainer_t1445911831 * XObject_get_Owner_m3004402994 (XObject_t3550811009 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Linq.XObject::SetOwner(System.Xml.Linq.XContainer)
extern "C"  void XObject_SetOwner_m582264412 (XObject_t3550811009 * __this, XContainer_t1445911831 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Xml.Linq.XObject::get_LineNumber()
extern "C"  int32_t XObject_get_LineNumber_m677481714 (XObject_t3550811009 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Linq.XObject::set_LineNumber(System.Int32)
extern "C"  void XObject_set_LineNumber_m3407035941 (XObject_t3550811009 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Xml.Linq.XObject::get_LinePosition()
extern "C"  int32_t XObject_get_LinePosition_m3752081380 (XObject_t3550811009 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Linq.XObject::set_LinePosition(System.Int32)
extern "C"  void XObject_set_LinePosition_m4032863679 (XObject_t3550811009 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Linq.XObject::FillLineInfoAndBaseUri(System.Xml.XmlReader,System.Xml.Linq.LoadOptions)
extern "C"  void XObject_FillLineInfoAndBaseUri_m431604165 (XObject_t3550811009 * __this, XmlReader_t3675626668 * ___r0, int32_t ___options1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
