﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.IDictionary`2<System.String,System.String>
struct IDictionary_2_t1943082916;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.ResponseMetadata
struct  ResponseMetadata_t527027456  : public Il2CppObject
{
public:
	// System.String Amazon.Runtime.ResponseMetadata::requestIdField
	String_t* ___requestIdField_0;
	// System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.Runtime.ResponseMetadata::_metadata
	Il2CppObject* ____metadata_1;

public:
	inline static int32_t get_offset_of_requestIdField_0() { return static_cast<int32_t>(offsetof(ResponseMetadata_t527027456, ___requestIdField_0)); }
	inline String_t* get_requestIdField_0() const { return ___requestIdField_0; }
	inline String_t** get_address_of_requestIdField_0() { return &___requestIdField_0; }
	inline void set_requestIdField_0(String_t* value)
	{
		___requestIdField_0 = value;
		Il2CppCodeGenWriteBarrier(&___requestIdField_0, value);
	}

	inline static int32_t get_offset_of__metadata_1() { return static_cast<int32_t>(offsetof(ResponseMetadata_t527027456, ____metadata_1)); }
	inline Il2CppObject* get__metadata_1() const { return ____metadata_1; }
	inline Il2CppObject** get_address_of__metadata_1() { return &____metadata_1; }
	inline void set__metadata_1(Il2CppObject* value)
	{
		____metadata_1 = value;
		Il2CppCodeGenWriteBarrier(&____metadata_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
