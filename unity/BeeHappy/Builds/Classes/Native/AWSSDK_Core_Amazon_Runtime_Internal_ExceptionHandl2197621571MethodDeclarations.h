﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_ExceptionHandl1695515460MethodDeclarations.h"

// Amazon.Runtime.Internal.Util.ILogger Amazon.Runtime.Internal.ExceptionHandler`1<Amazon.Runtime.Internal.HttpErrorResponseException>::get_Logger()
#define ExceptionHandler_1_get_Logger_m638171673(__this, method) ((  Il2CppObject * (*) (ExceptionHandler_1_t2197621571 *, const MethodInfo*))ExceptionHandler_1_get_Logger_m2827722976_gshared)(__this, method)
// System.Void Amazon.Runtime.Internal.ExceptionHandler`1<Amazon.Runtime.Internal.HttpErrorResponseException>::.ctor(Amazon.Runtime.Internal.Util.ILogger)
#define ExceptionHandler_1__ctor_m1910691191(__this, ___logger0, method) ((  void (*) (ExceptionHandler_1_t2197621571 *, Il2CppObject *, const MethodInfo*))ExceptionHandler_1__ctor_m3680341358_gshared)(__this, ___logger0, method)
// System.Boolean Amazon.Runtime.Internal.ExceptionHandler`1<Amazon.Runtime.Internal.HttpErrorResponseException>::Handle(Amazon.Runtime.IExecutionContext,System.Exception)
#define ExceptionHandler_1_Handle_m410309047(__this, ___executionContext0, ___exception1, method) ((  bool (*) (ExceptionHandler_1_t2197621571 *, Il2CppObject *, Exception_t1927440687 *, const MethodInfo*))ExceptionHandler_1_Handle_m2138797594_gshared)(__this, ___executionContext0, ___exception1, method)
