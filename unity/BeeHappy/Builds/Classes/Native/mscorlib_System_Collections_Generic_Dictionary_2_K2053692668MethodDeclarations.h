﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Int64>
struct Dictionary_2_t3659156526;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2053692668.h"
#include "AWSSDK_Core_Amazon_Runtime_Metric3273440202.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Amazon.Runtime.Metric,System.Int64>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1698121165_gshared (Enumerator_t2053692668 * __this, Dictionary_2_t3659156526 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m1698121165(__this, ___host0, method) ((  void (*) (Enumerator_t2053692668 *, Dictionary_2_t3659156526 *, const MethodInfo*))Enumerator__ctor_m1698121165_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Amazon.Runtime.Metric,System.Int64>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1956978320_gshared (Enumerator_t2053692668 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1956978320(__this, method) ((  Il2CppObject * (*) (Enumerator_t2053692668 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1956978320_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Amazon.Runtime.Metric,System.Int64>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m175547014_gshared (Enumerator_t2053692668 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m175547014(__this, method) ((  void (*) (Enumerator_t2053692668 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m175547014_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Amazon.Runtime.Metric,System.Int64>::Dispose()
extern "C"  void Enumerator_Dispose_m3080558725_gshared (Enumerator_t2053692668 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3080558725(__this, method) ((  void (*) (Enumerator_t2053692668 *, const MethodInfo*))Enumerator_Dispose_m3080558725_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Amazon.Runtime.Metric,System.Int64>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3650830322_gshared (Enumerator_t2053692668 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3650830322(__this, method) ((  bool (*) (Enumerator_t2053692668 *, const MethodInfo*))Enumerator_MoveNext_m3650830322_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Amazon.Runtime.Metric,System.Int64>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m4234579618_gshared (Enumerator_t2053692668 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m4234579618(__this, method) ((  int32_t (*) (Enumerator_t2053692668 *, const MethodInfo*))Enumerator_get_Current_m4234579618_gshared)(__this, method)
