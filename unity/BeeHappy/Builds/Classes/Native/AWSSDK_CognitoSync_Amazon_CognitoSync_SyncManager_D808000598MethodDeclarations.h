﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.CognitoSync.SyncManager.Dataset/<>c__DisplayClass72_0
struct U3CU3Ec__DisplayClass72_0_t808000598;

#include "codegen/il2cpp-codegen.h"

// System.Void Amazon.CognitoSync.SyncManager.Dataset/<>c__DisplayClass72_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass72_0__ctor_m2898099852 (U3CU3Ec__DisplayClass72_0_t808000598 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.SyncManager.Dataset/<>c__DisplayClass72_0::<FireSyncSuccessEvent>b__0()
extern "C"  void U3CU3Ec__DisplayClass72_0_U3CFireSyncSuccessEventU3Eb__0_m1059765042 (U3CU3Ec__DisplayClass72_0_t808000598 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
