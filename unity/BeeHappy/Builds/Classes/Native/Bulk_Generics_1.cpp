﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Collections.Generic.Comparer`1/DefaultComparer<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>
struct DefaultComparer_t3562007204;
// System.Collections.Generic.Comparer`1/DefaultComparer<System.Byte>
struct DefaultComparer_t2172374669;
// System.Collections.Generic.Comparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>
struct DefaultComparer_t2238857681;
// System.Collections.Generic.Comparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct DefaultComparer_t2823092174;
// System.Collections.Generic.Comparer`1/DefaultComparer<System.DateTime>
struct DefaultComparer_t3477443198;
// System.Collections.Generic.Comparer`1/DefaultComparer<System.DateTimeOffset>
struct DefaultComparer_t4147226435;
// System.Collections.Generic.Comparer`1/DefaultComparer<System.Guid>
struct DefaultComparer_t1022871826;
// System.Collections.Generic.Comparer`1/DefaultComparer<System.Int32>
struct DefaultComparer_t561147681;
// System.Collections.Generic.Comparer`1/DefaultComparer<System.Object>
struct DefaultComparer_t1178719528;
// System.Collections.Generic.Comparer`1/DefaultComparer<System.Reflection.CustomAttributeNamedArgument>
struct DefaultComparer_t2878395072;
// System.Collections.Generic.Comparer`1/DefaultComparer<System.Reflection.CustomAttributeTypedArgument>
struct DefaultComparer_t4282435443;
// System.Collections.Generic.Comparer`1/DefaultComparer<System.TimeSpan>
struct DefaultComparer_t1919529182;
// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Color32>
struct DefaultComparer_t3658755047;
// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.EventSystems.RaycastResult>
struct DefaultComparer_t2805423905;
// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Experimental.Director.Playable>
struct DefaultComparer_t2156815781;
// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UICharInfo>
struct DefaultComparer_t1545907033;
// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UILineInfo>
struct DefaultComparer_t2110548107;
// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UIVertex>
struct DefaultComparer_t3988496347;
// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Vector2>
struct DefaultComparer_t732977812;
// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Vector3>
struct DefaultComparer_t732977813;
// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Vector4>
struct DefaultComparer_t732977814;
// System.Collections.Generic.Comparer`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>
struct Comparer_1_t3962746090;
// System.Collections.Generic.Comparer`1<System.Byte>
struct Comparer_1_t2573113555;
// System.Collections.Generic.Comparer`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>
struct Comparer_1_t2639596567;
// System.Collections.Generic.Comparer`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct Comparer_1_t3223831060;
// System.Collections.Generic.Comparer`1<System.DateTime>
struct Comparer_1_t3878182084;
// System.Collections.Generic.Comparer`1<System.DateTimeOffset>
struct Comparer_1_t252998025;
// System.Collections.Generic.Comparer`1<System.Guid>
struct Comparer_1_t1423610712;
// System.Collections.Generic.Comparer`1<System.Int32>
struct Comparer_1_t961886567;
// System.Collections.Generic.Comparer`1<System.Object>
struct Comparer_1_t1579458414;
// System.Collections.Generic.Comparer`1<System.Reflection.CustomAttributeNamedArgument>
struct Comparer_1_t3279133958;
// System.Collections.Generic.Comparer`1<System.Reflection.CustomAttributeTypedArgument>
struct Comparer_1_t388207033;
// System.Collections.Generic.Comparer`1<System.TimeSpan>
struct Comparer_1_t2320268068;
// System.Collections.Generic.Comparer`1<UnityEngine.Color32>
struct Comparer_1_t4059493933;
// System.Collections.Generic.Comparer`1<UnityEngine.EventSystems.RaycastResult>
struct Comparer_1_t3206162791;
// System.Collections.Generic.Comparer`1<UnityEngine.Experimental.Director.Playable>
struct Comparer_1_t2557554667;
// System.Collections.Generic.Comparer`1<UnityEngine.UICharInfo>
struct Comparer_1_t1946645919;
// System.Collections.Generic.Comparer`1<UnityEngine.UILineInfo>
struct Comparer_1_t2511286993;
// System.Collections.Generic.Comparer`1<UnityEngine.UIVertex>
struct Comparer_1_t94267937;
// System.Collections.Generic.Comparer`1<UnityEngine.Vector2>
struct Comparer_1_t1133716698;
// System.Collections.Generic.Comparer`1<UnityEngine.Vector3>
struct Comparer_1_t1133716699;
// System.Collections.Generic.Comparer`1<UnityEngine.Vector4>
struct Comparer_1_t1133716700;
// System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Int64>
struct Dictionary_2_t3659156526;
// System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Object>
struct Dictionary_2_t1144560488;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t1079703083;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Object>
struct Dictionary_2_t1697274930;
// System.Collections.Generic.Dictionary`2<System.Int64,System.Object>
struct Dictionary_2_t3678967697;
// System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>
struct Dictionary_2_t3131474613;
// System.Collections.Generic.Dictionary`2<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>
struct Dictionary_2_t1224867506;
// System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>
struct Dictionary_2_t3417634846;
// System.Collections.Generic.Dictionary`2<System.Object,System.Int32>
struct Dictionary_2_t1663937576;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t2281509423;
// System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>
struct Dictionary_2_t727138142;
// System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>
struct Dictionary_2_t3650197868;
// System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>
struct Dictionary_2_t2730857826;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2935262194.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2935262194MethodDeclarations.h"
#include "mscorlib_System_Void1841601450.h"
#include "mscorlib_System_Int322071877448.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Single2076509932.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "mscorlib_System_Array3829468939MethodDeclarations.h"
#include "mscorlib_System_InvalidOperationException721527559MethodDeclarations.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_InvalidOperationException721527559.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3583626735.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3583626735MethodDeclarations.h"
#include "System_System_Text_RegularExpressions_Mark2724874473.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4289011211.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4289011211MethodDeclarations.h"
#include "mscorlib_System_TimeSpan3430258949.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1845634873.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1845634873MethodDeclarations.h"
#include "mscorlib_System_UInt16986882611.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3008434283.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3008434283MethodDeclarations.h"
#include "mscorlib_System_UInt322149682021.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3767949176.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3767949176MethodDeclarations.h"
#include "mscorlib_System_UInt642909196914.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2735343205.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2735343205MethodDeclarations.h"
#include "System_System_Uri_UriScheme1876590943.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4068833557.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4068833557MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlNamespaceManager_NsDecl3210081295.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3372377613.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3372377613MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlNamespaceManager_NsScope2513625351.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1598256859.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1598256859MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlNodeType739504597.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1993830276.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1993830276MethodDeclarations.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_ArrayMetadata1135078014.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3304333517.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3304333517MethodDeclarations.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_JsonToken2445581255.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen621922706.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen621922706MethodDeclarations.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_ObjectMetadata4058137740.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4146492248.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4146492248MethodDeclarations.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_PropertyMetada3287739986.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2879144337.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2879144337MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1733269780.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1733269780MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color32874517518.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2235177892.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2235177892MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ContactPoint1376425630.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen223115942.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen223115942MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ContactPoint2D3659330976.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen879938638.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen879938638MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResul21186376.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen231330514.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen231330514MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Play3667545548.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2308223602.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2308223602MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Keyframe1449471340.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen945932582.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen945932582MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RaycastHit87180320.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen627693740.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen627693740MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RaycastHit2D4063908774.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2620119317.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2620119317MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfo1761367055.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2613618411.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2613618411MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter1754866149.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen240568204.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen240568204MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter3676783238.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3997549960.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3997549960MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp3138797698.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1887381311.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1887381311MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_ContentTy1028629049.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3915389062.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3915389062MethodDeclarations.h"
#include "UnityEngine_UnityEngine_UICharInfo3056636800.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen185062840.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen185062840MethodDeclarations.h"
#include "UnityEngine_UnityEngine_UILineInfo3621277874.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2063011080.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2063011080MethodDeclarations.h"
#include "UnityEngine_UnityEngine_UIVertex1204258818.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3102459841.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3102459841MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3102459842.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3102459842MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3102459843.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3102459843MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"
#include "mscorlib_System_ArraySegment_1_gen2594217482.h"
#include "mscorlib_System_ArraySegment_1_gen2594217482MethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Byte3683104436.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "mscorlib_System_ArraySegment_1_gen1600562341.h"
#include "mscorlib_System_ArraySegment_1_gen1600562341MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_CollectionDebug517719049.h"
#include "mscorlib_System_Collections_Generic_CollectionDebug517719049MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_CollectionDebu2230541861.h"
#include "mscorlib_System_Collections_Generic_CollectionDebu2230541861MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def3562007204.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def3562007204MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen3962746090MethodDeclarations.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteKeyReader_K777769675.h"
#include "mscorlib_System_ArgumentException3259014390MethodDeclarations.h"
#include "mscorlib_System_ArgumentException3259014390.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def2172374669.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def2172374669MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen2573113555MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def2238857681.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def2238857681MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen2639596567MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23749587448.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def2823092174.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def2823092174MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen3223831060MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def3477443198.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def3477443198MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen3878182084MethodDeclarations.h"
#include "mscorlib_System_DateTime693205669.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def4147226435.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def4147226435MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen252998025MethodDeclarations.h"
#include "mscorlib_System_DateTimeOffset1362988906.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def1022871826.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def1022871826MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen1423610712MethodDeclarations.h"
#include "mscorlib_System_Guid2533601593.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Defa561147681.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Defa561147681MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen961886567MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def1178719528.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def1178719528MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen1579458414MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def2878395072.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def2878395072MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen3279133958MethodDeclarations.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArgum94157543.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def4282435443.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def4282435443MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen388207033MethodDeclarations.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArg1498197914.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def1919529182.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def1919529182MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen2320268068MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def3658755047.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def3658755047MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen4059493933MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def2805423905.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def2805423905MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen3206162791MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def2156815781.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def2156815781MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen2557554667MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def1545907033.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def1545907033MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen1946645919MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def2110548107.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def2110548107MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen2511286993MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def3988496347.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def3988496347MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen94267937MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Defa732977812.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Defa732977812MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen1133716698MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Defa732977813.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Defa732977813MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen1133716699MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Defa732977814.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Defa732977814MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen1133716700MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen3962746090.h"
#include "mscorlib_System_Type1303803226MethodDeclarations.h"
#include "mscorlib_System_Activator1850728717MethodDeclarations.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_RuntimeTypeHandle2330101084.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen2573113555.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen2639596567.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen3223831060.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen3878182084.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen252998025.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen1423610712.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen961886567.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen1579458414.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen3279133958.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen388207033.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen2320268068.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen4059493933.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen3206162791.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen2557554667.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen1946645919.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen2511286993.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen94267937.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen1133716698.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen1133716699.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen1133716700.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En684213932.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En684213932MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3659156526.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21416501748.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21416501748MethodDeclarations.h"
#include "AWSSDK_Core_Amazon_Runtime_Metric3273440202.h"
#include "mscorlib_System_Int64909078037.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Link2723257478.h"
#include "AWSSDK.Core_ArrayTypes.h"
#include "mscorlib_System_ObjectDisposedException2695136451MethodDeclarations.h"
#include "mscorlib_System_ObjectDisposedException2695136451.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2464585190.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2464585190MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1144560488.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23196873006.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23196873006MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2399727785.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2399727785MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1079703083.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23132015601.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23132015601MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3017299632.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3017299632MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1697274930.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23749587448MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En704025103.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En704025103MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3678967697.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21436312919.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21436312919MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En156532019.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En156532019MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3131474613.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_888819835.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_888819835MethodDeclarations.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2544892208.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2544892208MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1224867506.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23277180024.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23277180024MethodDeclarations.h"
#include "AWSSDK_Core_Amazon_Util_Internal_PlatformServices_1632807378.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En442692252.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En442692252MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3417634846.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21174980068.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21174980068MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2983962278.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2983962278MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1663937576.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23716250094.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23716250094MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3601534125.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3601534125MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2281509423.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2047162844.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2047162844MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge727138142.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22779450660.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22779450660MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En675255274.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En675255274MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3650197868.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21407543090.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21407543090MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E4050882528.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E4050882528MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2730857826.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_488203048.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_488203048MethodDeclarations.h"
#include "UnityEngine_ArrayTypes.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2053692668.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2053692668MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3659156526MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3834063926.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3834063926MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1144560488MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3769206521.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3769206521MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1079703083MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Key91811072.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Key91811072MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1697274930MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2073503839.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2073503839MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3678967697MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1526010755.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1526010755MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3131474613MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3914370944.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3914370944MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1224867506MethodDeclarations.h"

// !!0 System.Array::InternalArray__get_Item<System.Single>(System.Int32)
extern "C"  float Array_InternalArray__get_Item_TisSingle_t2076509932_m1495809753_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisSingle_t2076509932_m1495809753(__this, p0, method) ((  float (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisSingle_t2076509932_m1495809753_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Text.RegularExpressions.Mark>(System.Int32)
extern "C"  Mark_t2724874473  Array_InternalArray__get_Item_TisMark_t2724874473_m2044327706_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisMark_t2724874473_m2044327706(__this, p0, method) ((  Mark_t2724874473  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisMark_t2724874473_m2044327706_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.TimeSpan>(System.Int32)
extern "C"  TimeSpan_t3430258949  Array_InternalArray__get_Item_TisTimeSpan_t3430258949_m1147719260_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTimeSpan_t3430258949_m1147719260(__this, p0, method) ((  TimeSpan_t3430258949  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTimeSpan_t3430258949_m1147719260_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.UInt16>(System.Int32)
extern "C"  uint16_t Array_InternalArray__get_Item_TisUInt16_t986882611_m2599215710_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisUInt16_t986882611_m2599215710(__this, p0, method) ((  uint16_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisUInt16_t986882611_m2599215710_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.UInt32>(System.Int32)
extern "C"  uint32_t Array_InternalArray__get_Item_TisUInt32_t2149682021_m2554907852_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisUInt32_t2149682021_m2554907852(__this, p0, method) ((  uint32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisUInt32_t2149682021_m2554907852_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.UInt64>(System.Int32)
extern "C"  uint64_t Array_InternalArray__get_Item_TisUInt64_t2909196914_m2580870875_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisUInt64_t2909196914_m2580870875(__this, p0, method) ((  uint64_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisUInt64_t2909196914_m2580870875_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Uri/UriScheme>(System.Int32)
extern "C"  UriScheme_t1876590943  Array_InternalArray__get_Item_TisUriScheme_t1876590943_m1821482697_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisUriScheme_t1876590943_m1821482697(__this, p0, method) ((  UriScheme_t1876590943  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisUriScheme_t1876590943_m1821482697_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Xml.XmlNamespaceManager/NsDecl>(System.Int32)
extern "C"  NsDecl_t3210081295  Array_InternalArray__get_Item_TisNsDecl_t3210081295_m2296051909_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisNsDecl_t3210081295_m2296051909(__this, p0, method) ((  NsDecl_t3210081295  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisNsDecl_t3210081295_m2296051909_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Xml.XmlNamespaceManager/NsScope>(System.Int32)
extern "C"  NsScope_t2513625351  Array_InternalArray__get_Item_TisNsScope_t2513625351_m2981250529_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisNsScope_t2513625351_m2981250529(__this, p0, method) ((  NsScope_t2513625351  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisNsScope_t2513625351_m2981250529_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Xml.XmlNodeType>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisXmlNodeType_t739504597_m3047069777_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisXmlNodeType_t739504597_m3047069777(__this, p0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisXmlNodeType_t739504597_m3047069777_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<ThirdParty.Json.LitJson.ArrayMetadata>(System.Int32)
extern "C"  ArrayMetadata_t1135078014  Array_InternalArray__get_Item_TisArrayMetadata_t1135078014_m3803185906_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisArrayMetadata_t1135078014_m3803185906(__this, p0, method) ((  ArrayMetadata_t1135078014  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisArrayMetadata_t1135078014_m3803185906_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<ThirdParty.Json.LitJson.JsonToken>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisJsonToken_t2445581255_m1147164989_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisJsonToken_t2445581255_m1147164989(__this, p0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisJsonToken_t2445581255_m1147164989_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<ThirdParty.Json.LitJson.ObjectMetadata>(System.Int32)
extern "C"  ObjectMetadata_t4058137740  Array_InternalArray__get_Item_TisObjectMetadata_t4058137740_m1317504028_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisObjectMetadata_t4058137740_m1317504028(__this, p0, method) ((  ObjectMetadata_t4058137740  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisObjectMetadata_t4058137740_m1317504028_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<ThirdParty.Json.LitJson.PropertyMetadata>(System.Int32)
extern "C"  PropertyMetadata_t3287739986  Array_InternalArray__get_Item_TisPropertyMetadata_t3287739986_m2424636378_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisPropertyMetadata_t3287739986_m2424636378(__this, p0, method) ((  PropertyMetadata_t3287739986  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisPropertyMetadata_t3287739986_m2424636378_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.Color>(System.Int32)
extern "C"  Color_t2020392075  Array_InternalArray__get_Item_TisColor_t2020392075_m996560062_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisColor_t2020392075_m996560062(__this, p0, method) ((  Color_t2020392075  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisColor_t2020392075_m996560062_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.Color32>(System.Int32)
extern "C"  Color32_t874517518  Array_InternalArray__get_Item_TisColor32_t874517518_m1877643687_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisColor32_t874517518_m1877643687(__this, p0, method) ((  Color32_t874517518  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisColor32_t874517518_m1877643687_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.ContactPoint>(System.Int32)
extern "C"  ContactPoint_t1376425630  Array_InternalArray__get_Item_TisContactPoint_t1376425630_m3234597783_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisContactPoint_t1376425630_m3234597783(__this, p0, method) ((  ContactPoint_t1376425630  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisContactPoint_t1376425630_m3234597783_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.ContactPoint2D>(System.Int32)
extern "C"  ContactPoint2D_t3659330976  Array_InternalArray__get_Item_TisContactPoint2D_t3659330976_m825151777_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisContactPoint2D_t3659330976_m825151777(__this, p0, method) ((  ContactPoint2D_t3659330976  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisContactPoint2D_t3659330976_m825151777_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.EventSystems.RaycastResult>(System.Int32)
extern "C"  RaycastResult_t21186376  Array_InternalArray__get_Item_TisRaycastResult_t21186376_m4125877765_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisRaycastResult_t21186376_m4125877765(__this, p0, method) ((  RaycastResult_t21186376  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisRaycastResult_t21186376_m4125877765_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.Experimental.Director.Playable>(System.Int32)
extern "C"  Playable_t3667545548  Array_InternalArray__get_Item_TisPlayable_t3667545548_m1976366877_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisPlayable_t3667545548_m1976366877(__this, p0, method) ((  Playable_t3667545548  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisPlayable_t3667545548_m1976366877_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.Keyframe>(System.Int32)
extern "C"  Keyframe_t1449471340  Array_InternalArray__get_Item_TisKeyframe_t1449471340_m1003508933_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyframe_t1449471340_m1003508933(__this, p0, method) ((  Keyframe_t1449471340  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyframe_t1449471340_m1003508933_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.RaycastHit>(System.Int32)
extern "C"  RaycastHit_t87180320  Array_InternalArray__get_Item_TisRaycastHit_t87180320_m3529622569_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisRaycastHit_t87180320_m3529622569(__this, p0, method) ((  RaycastHit_t87180320  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisRaycastHit_t87180320_m3529622569_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.RaycastHit2D>(System.Int32)
extern "C"  RaycastHit2D_t4063908774  Array_InternalArray__get_Item_TisRaycastHit2D_t4063908774_m3592947655_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisRaycastHit2D_t4063908774_m3592947655(__this, p0, method) ((  RaycastHit2D_t4063908774  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisRaycastHit2D_t4063908774_m3592947655_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.SendMouseEvents/HitInfo>(System.Int32)
extern "C"  HitInfo_t1761367055  Array_InternalArray__get_Item_TisHitInfo_t1761367055_m2443000901_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisHitInfo_t1761367055_m2443000901(__this, p0, method) ((  HitInfo_t1761367055  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisHitInfo_t1761367055_m2443000901_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>(System.Int32)
extern "C"  GcAchievementData_t1754866149  Array_InternalArray__get_Item_TisGcAchievementData_t1754866149_m2980277810_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisGcAchievementData_t1754866149_m2980277810(__this, p0, method) ((  GcAchievementData_t1754866149  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisGcAchievementData_t1754866149_m2980277810_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>(System.Int32)
extern "C"  GcScoreData_t3676783238  Array_InternalArray__get_Item_TisGcScoreData_t3676783238_m733932313_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisGcScoreData_t3676783238_m733932313(__this, p0, method) ((  GcScoreData_t3676783238  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisGcScoreData_t3676783238_m733932313_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.TextEditor/TextEditOp>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisTextEditOp_t3138797698_m2832701950_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTextEditOp_t3138797698_m2832701950(__this, p0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTextEditOp_t3138797698_m2832701950_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.UI.InputField/ContentType>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisContentType_t1028629049_m2406619723_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisContentType_t1028629049_m2406619723(__this, p0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisContentType_t1028629049_m2406619723_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.UICharInfo>(System.Int32)
extern "C"  UICharInfo_t3056636800  Array_InternalArray__get_Item_TisUICharInfo_t3056636800_m3872982785_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisUICharInfo_t3056636800_m3872982785(__this, p0, method) ((  UICharInfo_t3056636800  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisUICharInfo_t3056636800_m3872982785_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.UILineInfo>(System.Int32)
extern "C"  UILineInfo_t3621277874  Array_InternalArray__get_Item_TisUILineInfo_t3621277874_m1432166059_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisUILineInfo_t3621277874_m1432166059(__this, p0, method) ((  UILineInfo_t3621277874  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisUILineInfo_t3621277874_m1432166059_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.UIVertex>(System.Int32)
extern "C"  UIVertex_t1204258818  Array_InternalArray__get_Item_TisUIVertex_t1204258818_m3450355955_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisUIVertex_t1204258818_m3450355955(__this, p0, method) ((  UIVertex_t1204258818  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisUIVertex_t1204258818_m3450355955_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.Vector2>(System.Int32)
extern "C"  Vector2_t2243707579  Array_InternalArray__get_Item_TisVector2_t2243707579_m2394947294_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisVector2_t2243707579_m2394947294(__this, p0, method) ((  Vector2_t2243707579  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisVector2_t2243707579_m2394947294_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.Vector3>(System.Int32)
extern "C"  Vector3_t2243707580  Array_InternalArray__get_Item_TisVector3_t2243707580_m2841870745_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisVector3_t2243707580_m2841870745(__this, p0, method) ((  Vector3_t2243707580  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisVector3_t2243707580_m2841870745_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.Vector4>(System.Int32)
extern "C"  Vector4_t2243707581  Array_InternalArray__get_Item_TisVector4_t2243707581_m3866288892_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisVector4_t2243707581_m3866288892(__this, p0, method) ((  Vector4_t2243707581  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisVector4_t2243707581_m3866288892_gshared)(__this, p0, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Array/InternalEnumerator`1<System.Single>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2314640734_gshared (InternalEnumerator_1_t2935262194 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2314640734_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2935262194 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2935262194 *>(__this + 1);
	InternalEnumerator_1__ctor_m2314640734(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Single>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m214315662_gshared (InternalEnumerator_1_t2935262194 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m214315662_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2935262194 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2935262194 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m214315662(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Single>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1231402888_gshared (InternalEnumerator_1_t2935262194 * __this, const MethodInfo* method)
{
	{
		float L_0 = InternalEnumerator_1_get_Current_m727737343((InternalEnumerator_1_t2935262194 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		float L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1231402888_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2935262194 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2935262194 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1231402888(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Single>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2195973811_gshared (InternalEnumerator_1_t2935262194 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2195973811_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2935262194 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2935262194 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2195973811(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Single>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m580128774_gshared (InternalEnumerator_1_t2935262194 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m580128774_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2935262194 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2935262194 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m580128774(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Single>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m727737343_MetadataUsageId;
extern "C"  float InternalEnumerator_1_get_Current_m727737343_gshared (InternalEnumerator_1_t2935262194 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m727737343_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		float L_8 = ((  float (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  float InternalEnumerator_1_get_Current_m727737343_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2935262194 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2935262194 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m727737343(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1240086835_gshared (InternalEnumerator_1_t3583626735 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1240086835_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3583626735 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3583626735 *>(__this + 1);
	InternalEnumerator_1__ctor_m1240086835(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3826378355_gshared (InternalEnumerator_1_t3583626735 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3826378355_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3583626735 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3583626735 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3826378355(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2035754659_gshared (InternalEnumerator_1_t3583626735 * __this, const MethodInfo* method)
{
	{
		Mark_t2724874473  L_0 = InternalEnumerator_1_get_Current_m575280506((InternalEnumerator_1_t3583626735 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Mark_t2724874473  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2035754659_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3583626735 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3583626735 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2035754659(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3744916110_gshared (InternalEnumerator_1_t3583626735 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3744916110_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3583626735 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3583626735 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3744916110(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1741571735_gshared (InternalEnumerator_1_t3583626735 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1741571735_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3583626735 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3583626735 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1741571735(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m575280506_MetadataUsageId;
extern "C"  Mark_t2724874473  InternalEnumerator_1_get_Current_m575280506_gshared (InternalEnumerator_1_t3583626735 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m575280506_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Mark_t2724874473  L_8 = ((  Mark_t2724874473  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Mark_t2724874473  InternalEnumerator_1_get_Current_m575280506_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3583626735 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3583626735 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m575280506(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.TimeSpan>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2189699457_gshared (InternalEnumerator_1_t4289011211 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2189699457_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t4289011211 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4289011211 *>(__this + 1);
	InternalEnumerator_1__ctor_m2189699457(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.TimeSpan>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3249248421_gshared (InternalEnumerator_1_t4289011211 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3249248421_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4289011211 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4289011211 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3249248421(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.TimeSpan>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m439366097_gshared (InternalEnumerator_1_t4289011211 * __this, const MethodInfo* method)
{
	{
		TimeSpan_t3430258949  L_0 = InternalEnumerator_1_get_Current_m3411759116((InternalEnumerator_1_t4289011211 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		TimeSpan_t3430258949  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m439366097_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4289011211 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4289011211 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m439366097(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.TimeSpan>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3838127340_gshared (InternalEnumerator_1_t4289011211 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3838127340_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4289011211 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4289011211 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3838127340(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.TimeSpan>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1674480765_gshared (InternalEnumerator_1_t4289011211 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1674480765_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4289011211 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4289011211 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1674480765(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.TimeSpan>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m3411759116_MetadataUsageId;
extern "C"  TimeSpan_t3430258949  InternalEnumerator_1_get_Current_m3411759116_gshared (InternalEnumerator_1_t4289011211 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3411759116_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		TimeSpan_t3430258949  L_8 = ((  TimeSpan_t3430258949  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  TimeSpan_t3430258949  InternalEnumerator_1_get_Current_m3411759116_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4289011211 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4289011211 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3411759116(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.UInt16>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2981879621_gshared (InternalEnumerator_1_t1845634873 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2981879621_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1845634873 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1845634873 *>(__this + 1);
	InternalEnumerator_1__ctor_m2981879621(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.UInt16>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2571770313_gshared (InternalEnumerator_1_t1845634873 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2571770313_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1845634873 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1845634873 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2571770313(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.UInt16>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1658267053_gshared (InternalEnumerator_1_t1845634873 * __this, const MethodInfo* method)
{
	{
		uint16_t L_0 = InternalEnumerator_1_get_Current_m3179981210((InternalEnumerator_1_t1845634873 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		uint16_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1658267053_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1845634873 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1845634873 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1658267053(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.UInt16>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1824402698_gshared (InternalEnumerator_1_t1845634873 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1824402698_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1845634873 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1845634873 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1824402698(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.UInt16>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2809569305_gshared (InternalEnumerator_1_t1845634873 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2809569305_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1845634873 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1845634873 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2809569305(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.UInt16>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m3179981210_MetadataUsageId;
extern "C"  uint16_t InternalEnumerator_1_get_Current_m3179981210_gshared (InternalEnumerator_1_t1845634873 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3179981210_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		uint16_t L_8 = ((  uint16_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  uint16_t InternalEnumerator_1_get_Current_m3179981210_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1845634873 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1845634873 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3179981210(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.UInt32>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m691972083_gshared (InternalEnumerator_1_t3008434283 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m691972083_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3008434283 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3008434283 *>(__this + 1);
	InternalEnumerator_1__ctor_m691972083(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.UInt32>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3107741851_gshared (InternalEnumerator_1_t3008434283 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3107741851_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3008434283 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3008434283 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3107741851(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.UInt32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2458630467_gshared (InternalEnumerator_1_t3008434283 * __this, const MethodInfo* method)
{
	{
		uint32_t L_0 = InternalEnumerator_1_get_Current_m2198364332((InternalEnumerator_1_t3008434283 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		uint32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2458630467_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3008434283 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3008434283 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2458630467(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.UInt32>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2620838688_gshared (InternalEnumerator_1_t3008434283 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2620838688_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3008434283 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3008434283 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2620838688(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.UInt32>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m470170271_gshared (InternalEnumerator_1_t3008434283 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m470170271_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3008434283 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3008434283 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m470170271(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.UInt32>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m2198364332_MetadataUsageId;
extern "C"  uint32_t InternalEnumerator_1_get_Current_m2198364332_gshared (InternalEnumerator_1_t3008434283 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2198364332_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		uint32_t L_8 = ((  uint32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  uint32_t InternalEnumerator_1_get_Current_m2198364332_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3008434283 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3008434283 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2198364332(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.UInt64>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3084132532_gshared (InternalEnumerator_1_t3767949176 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3084132532_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3767949176 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3767949176 *>(__this + 1);
	InternalEnumerator_1__ctor_m3084132532(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.UInt64>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m187060888_gshared (InternalEnumerator_1_t3767949176 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m187060888_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3767949176 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3767949176 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m187060888(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.UInt64>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m771161214_gshared (InternalEnumerator_1_t3767949176 * __this, const MethodInfo* method)
{
	{
		uint64_t L_0 = InternalEnumerator_1_get_Current_m35328337((InternalEnumerator_1_t3767949176 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		uint64_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m771161214_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3767949176 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3767949176 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m771161214(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.UInt64>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3642485841_gshared (InternalEnumerator_1_t3767949176 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3642485841_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3767949176 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3767949176 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3642485841(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.UInt64>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2954283444_gshared (InternalEnumerator_1_t3767949176 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2954283444_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3767949176 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3767949176 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2954283444(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.UInt64>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m35328337_MetadataUsageId;
extern "C"  uint64_t InternalEnumerator_1_get_Current_m35328337_gshared (InternalEnumerator_1_t3767949176 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m35328337_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		uint64_t L_8 = ((  uint64_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  uint64_t InternalEnumerator_1_get_Current_m35328337_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3767949176 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3767949176 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m35328337(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Uri/UriScheme>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3052252268_gshared (InternalEnumerator_1_t2735343205 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3052252268_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2735343205 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2735343205 *>(__this + 1);
	InternalEnumerator_1__ctor_m3052252268(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Uri/UriScheme>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3606709516_gshared (InternalEnumerator_1_t2735343205 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3606709516_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2735343205 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2735343205 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3606709516(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Uri/UriScheme>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3065287496_gshared (InternalEnumerator_1_t2735343205 * __this, const MethodInfo* method)
{
	{
		UriScheme_t1876590943  L_0 = InternalEnumerator_1_get_Current_m1830023619((InternalEnumerator_1_t2735343205 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		UriScheme_t1876590943  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3065287496_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2735343205 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2735343205 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3065287496(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Uri/UriScheme>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1770651099_gshared (InternalEnumerator_1_t2735343205 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1770651099_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2735343205 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2735343205 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1770651099(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Uri/UriScheme>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3629145604_gshared (InternalEnumerator_1_t2735343205 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3629145604_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2735343205 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2735343205 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3629145604(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Uri/UriScheme>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m1830023619_MetadataUsageId;
extern "C"  UriScheme_t1876590943  InternalEnumerator_1_get_Current_m1830023619_gshared (InternalEnumerator_1_t2735343205 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1830023619_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		UriScheme_t1876590943  L_8 = ((  UriScheme_t1876590943  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  UriScheme_t1876590943  InternalEnumerator_1_get_Current_m1830023619_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2735343205 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2735343205 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1830023619(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Xml.XmlNamespaceManager/NsDecl>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3370506220_gshared (InternalEnumerator_1_t4068833557 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3370506220_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t4068833557 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4068833557 *>(__this + 1);
	InternalEnumerator_1__ctor_m3370506220(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Xml.XmlNamespaceManager/NsDecl>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1048027980_gshared (InternalEnumerator_1_t4068833557 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1048027980_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4068833557 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4068833557 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1048027980(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Xml.XmlNamespaceManager/NsDecl>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4020499658_gshared (InternalEnumerator_1_t4068833557 * __this, const MethodInfo* method)
{
	{
		NsDecl_t3210081295  L_0 = InternalEnumerator_1_get_Current_m565700179((InternalEnumerator_1_t4068833557 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NsDecl_t3210081295  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4020499658_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4068833557 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4068833557 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4020499658(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Xml.XmlNamespaceManager/NsDecl>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3178881927_gshared (InternalEnumerator_1_t4068833557 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3178881927_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4068833557 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4068833557 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3178881927(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Xml.XmlNamespaceManager/NsDecl>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1840052676_gshared (InternalEnumerator_1_t4068833557 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1840052676_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4068833557 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4068833557 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1840052676(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Xml.XmlNamespaceManager/NsDecl>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m565700179_MetadataUsageId;
extern "C"  NsDecl_t3210081295  InternalEnumerator_1_get_Current_m565700179_gshared (InternalEnumerator_1_t4068833557 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m565700179_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		NsDecl_t3210081295  L_8 = ((  NsDecl_t3210081295  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  NsDecl_t3210081295  InternalEnumerator_1_get_Current_m565700179_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4068833557 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4068833557 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m565700179(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Xml.XmlNamespaceManager/NsScope>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1296678738_gshared (InternalEnumerator_1_t3372377613 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1296678738_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3372377613 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3372377613 *>(__this + 1);
	InternalEnumerator_1__ctor_m1296678738(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Xml.XmlNamespaceManager/NsScope>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3816437658_gshared (InternalEnumerator_1_t3372377613 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3816437658_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3372377613 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3372377613 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3816437658(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Xml.XmlNamespaceManager/NsScope>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2821103782_gshared (InternalEnumerator_1_t3372377613 * __this, const MethodInfo* method)
{
	{
		NsScope_t2513625351  L_0 = InternalEnumerator_1_get_Current_m819875371((InternalEnumerator_1_t3372377613 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NsScope_t2513625351  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2821103782_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3372377613 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3372377613 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2821103782(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Xml.XmlNamespaceManager/NsScope>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3375993931_gshared (InternalEnumerator_1_t3372377613 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3375993931_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3372377613 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3372377613 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3375993931(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Xml.XmlNamespaceManager/NsScope>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2387368234_gshared (InternalEnumerator_1_t3372377613 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2387368234_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3372377613 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3372377613 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2387368234(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Xml.XmlNamespaceManager/NsScope>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m819875371_MetadataUsageId;
extern "C"  NsScope_t2513625351  InternalEnumerator_1_get_Current_m819875371_gshared (InternalEnumerator_1_t3372377613 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m819875371_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		NsScope_t2513625351  L_8 = ((  NsScope_t2513625351  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  NsScope_t2513625351  InternalEnumerator_1_get_Current_m819875371_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3372377613 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3372377613 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m819875371(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Xml.XmlNodeType>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1972732386_gshared (InternalEnumerator_1_t1598256859 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1972732386_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1598256859 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1598256859 *>(__this + 1);
	InternalEnumerator_1__ctor_m1972732386(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Xml.XmlNodeType>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m439242922_gshared (InternalEnumerator_1_t1598256859 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m439242922_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1598256859 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1598256859 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m439242922(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Xml.XmlNodeType>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m128604886_gshared (InternalEnumerator_1_t1598256859 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m160651067((InternalEnumerator_1_t1598256859 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m128604886_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1598256859 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1598256859 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m128604886(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Xml.XmlNodeType>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m836947227_gshared (InternalEnumerator_1_t1598256859 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m836947227_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1598256859 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1598256859 *>(__this + 1);
	InternalEnumerator_1_Dispose_m836947227(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Xml.XmlNodeType>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m131387450_gshared (InternalEnumerator_1_t1598256859 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m131387450_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1598256859 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1598256859 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m131387450(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Xml.XmlNodeType>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m160651067_MetadataUsageId;
extern "C"  int32_t InternalEnumerator_1_get_Current_m160651067_gshared (InternalEnumerator_1_t1598256859 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m160651067_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m160651067_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1598256859 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1598256859 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m160651067(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<ThirdParty.Json.LitJson.ArrayMetadata>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2756170553_gshared (InternalEnumerator_1_t1993830276 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2756170553_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1993830276 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1993830276 *>(__this + 1);
	InternalEnumerator_1__ctor_m2756170553(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4280724821_gshared (InternalEnumerator_1_t1993830276 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4280724821_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1993830276 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1993830276 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4280724821(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2232836713_gshared (InternalEnumerator_1_t1993830276 * __this, const MethodInfo* method)
{
	{
		ArrayMetadata_t1135078014  L_0 = InternalEnumerator_1_get_Current_m2815575366((InternalEnumerator_1_t1993830276 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ArrayMetadata_t1135078014  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2232836713_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1993830276 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1993830276 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2232836713(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<ThirdParty.Json.LitJson.ArrayMetadata>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1468042774_gshared (InternalEnumerator_1_t1993830276 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1468042774_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1993830276 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1993830276 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1468042774(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<ThirdParty.Json.LitJson.ArrayMetadata>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m235774285_gshared (InternalEnumerator_1_t1993830276 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m235774285_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1993830276 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1993830276 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m235774285(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<ThirdParty.Json.LitJson.ArrayMetadata>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m2815575366_MetadataUsageId;
extern "C"  ArrayMetadata_t1135078014  InternalEnumerator_1_get_Current_m2815575366_gshared (InternalEnumerator_1_t1993830276 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2815575366_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		ArrayMetadata_t1135078014  L_8 = ((  ArrayMetadata_t1135078014  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  ArrayMetadata_t1135078014  InternalEnumerator_1_get_Current_m2815575366_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1993830276 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1993830276 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2815575366(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<ThirdParty.Json.LitJson.JsonToken>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m872339670_gshared (InternalEnumerator_1_t3304333517 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m872339670_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3304333517 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3304333517 *>(__this + 1);
	InternalEnumerator_1__ctor_m872339670(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<ThirdParty.Json.LitJson.JsonToken>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2770637158_gshared (InternalEnumerator_1_t3304333517 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2770637158_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3304333517 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3304333517 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2770637158(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<ThirdParty.Json.LitJson.JsonToken>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2296830104_gshared (InternalEnumerator_1_t3304333517 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m971602399((InternalEnumerator_1_t3304333517 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2296830104_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3304333517 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3304333517 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2296830104(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<ThirdParty.Json.LitJson.JsonToken>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1490194763_gshared (InternalEnumerator_1_t3304333517 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1490194763_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3304333517 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3304333517 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1490194763(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<ThirdParty.Json.LitJson.JsonToken>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3419452238_gshared (InternalEnumerator_1_t3304333517 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3419452238_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3304333517 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3304333517 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3419452238(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<ThirdParty.Json.LitJson.JsonToken>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m971602399_MetadataUsageId;
extern "C"  int32_t InternalEnumerator_1_get_Current_m971602399_gshared (InternalEnumerator_1_t3304333517 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m971602399_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m971602399_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3304333517 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3304333517 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m971602399(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<ThirdParty.Json.LitJson.ObjectMetadata>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m4110446661_gshared (InternalEnumerator_1_t621922706 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m4110446661_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t621922706 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t621922706 *>(__this + 1);
	InternalEnumerator_1__ctor_m4110446661(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3058495193_gshared (InternalEnumerator_1_t621922706 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3058495193_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t621922706 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t621922706 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3058495193(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3040574665_gshared (InternalEnumerator_1_t621922706 * __this, const MethodInfo* method)
{
	{
		ObjectMetadata_t4058137740  L_0 = InternalEnumerator_1_get_Current_m2772624814((InternalEnumerator_1_t621922706 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectMetadata_t4058137740  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3040574665_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t621922706 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t621922706 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3040574665(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<ThirdParty.Json.LitJson.ObjectMetadata>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2710601380_gshared (InternalEnumerator_1_t621922706 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2710601380_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t621922706 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t621922706 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2710601380(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<ThirdParty.Json.LitJson.ObjectMetadata>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2500732761_gshared (InternalEnumerator_1_t621922706 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2500732761_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t621922706 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t621922706 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2500732761(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<ThirdParty.Json.LitJson.ObjectMetadata>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m2772624814_MetadataUsageId;
extern "C"  ObjectMetadata_t4058137740  InternalEnumerator_1_get_Current_m2772624814_gshared (InternalEnumerator_1_t621922706 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2772624814_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		ObjectMetadata_t4058137740  L_8 = ((  ObjectMetadata_t4058137740  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  ObjectMetadata_t4058137740  InternalEnumerator_1_get_Current_m2772624814_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t621922706 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t621922706 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2772624814(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<ThirdParty.Json.LitJson.PropertyMetadata>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2195808295_gshared (InternalEnumerator_1_t4146492248 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2195808295_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t4146492248 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4146492248 *>(__this + 1);
	InternalEnumerator_1__ctor_m2195808295(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<ThirdParty.Json.LitJson.PropertyMetadata>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1488024775_gshared (InternalEnumerator_1_t4146492248 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1488024775_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4146492248 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4146492248 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1488024775(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<ThirdParty.Json.LitJson.PropertyMetadata>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3085000283_gshared (InternalEnumerator_1_t4146492248 * __this, const MethodInfo* method)
{
	{
		PropertyMetadata_t3287739986  L_0 = InternalEnumerator_1_get_Current_m2096054752((InternalEnumerator_1_t4146492248 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		PropertyMetadata_t3287739986  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3085000283_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4146492248 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4146492248 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3085000283(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<ThirdParty.Json.LitJson.PropertyMetadata>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m349078886_gshared (InternalEnumerator_1_t4146492248 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m349078886_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4146492248 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4146492248 *>(__this + 1);
	InternalEnumerator_1_Dispose_m349078886(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<ThirdParty.Json.LitJson.PropertyMetadata>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2691932099_gshared (InternalEnumerator_1_t4146492248 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2691932099_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4146492248 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4146492248 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2691932099(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<ThirdParty.Json.LitJson.PropertyMetadata>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m2096054752_MetadataUsageId;
extern "C"  PropertyMetadata_t3287739986  InternalEnumerator_1_get_Current_m2096054752_gshared (InternalEnumerator_1_t4146492248 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2096054752_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		PropertyMetadata_t3287739986  L_8 = ((  PropertyMetadata_t3287739986  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  PropertyMetadata_t3287739986  InternalEnumerator_1_get_Current_m2096054752_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4146492248 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4146492248 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2096054752(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Color>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m838009241_gshared (InternalEnumerator_1_t2879144337 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m838009241_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2879144337 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2879144337 *>(__this + 1);
	InternalEnumerator_1__ctor_m838009241(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Color>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4103553221_gshared (InternalEnumerator_1_t2879144337 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4103553221_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2879144337 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2879144337 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4103553221(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Color>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3843687121_gshared (InternalEnumerator_1_t2879144337 * __this, const MethodInfo* method)
{
	{
		Color_t2020392075  L_0 = InternalEnumerator_1_get_Current_m4290564662((InternalEnumerator_1_t2879144337 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Color_t2020392075  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3843687121_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2879144337 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2879144337 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3843687121(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Color>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3984359374_gshared (InternalEnumerator_1_t2879144337 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3984359374_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2879144337 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2879144337 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3984359374(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Color>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3103009949_gshared (InternalEnumerator_1_t2879144337 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3103009949_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2879144337 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2879144337 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3103009949(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.Color>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m4290564662_MetadataUsageId;
extern "C"  Color_t2020392075  InternalEnumerator_1_get_Current_m4290564662_gshared (InternalEnumerator_1_t2879144337 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m4290564662_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Color_t2020392075  L_8 = ((  Color_t2020392075  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Color_t2020392075  InternalEnumerator_1_get_Current_m4290564662_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2879144337 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2879144337 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m4290564662(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Color32>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m96919148_gshared (InternalEnumerator_1_t1733269780 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m96919148_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1733269780 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1733269780 *>(__this + 1);
	InternalEnumerator_1__ctor_m96919148(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Color32>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2275167408_gshared (InternalEnumerator_1_t1733269780 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2275167408_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1733269780 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1733269780 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2275167408(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Color32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30488070_gshared (InternalEnumerator_1_t1733269780 * __this, const MethodInfo* method)
{
	{
		Color32_t874517518  L_0 = InternalEnumerator_1_get_Current_m3143558721((InternalEnumerator_1_t1733269780 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Color32_t874517518  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30488070_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1733269780 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1733269780 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30488070(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Color32>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m876833153_gshared (InternalEnumerator_1_t1733269780 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m876833153_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1733269780 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1733269780 *>(__this + 1);
	InternalEnumerator_1_Dispose_m876833153(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Color32>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4068681772_gshared (InternalEnumerator_1_t1733269780 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m4068681772_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1733269780 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1733269780 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m4068681772(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.Color32>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m3143558721_MetadataUsageId;
extern "C"  Color32_t874517518  InternalEnumerator_1_get_Current_m3143558721_gshared (InternalEnumerator_1_t1733269780 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3143558721_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Color32_t874517518  L_8 = ((  Color32_t874517518  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Color32_t874517518  InternalEnumerator_1_get_Current_m3143558721_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1733269780 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1733269780 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3143558721(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.ContactPoint>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3210262878_gshared (InternalEnumerator_1_t2235177892 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3210262878_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2235177892 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2235177892 *>(__this + 1);
	InternalEnumerator_1__ctor_m3210262878(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.ContactPoint>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2564106794_gshared (InternalEnumerator_1_t2235177892 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2564106794_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2235177892 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2235177892 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2564106794(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.ContactPoint>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1497708066_gshared (InternalEnumerator_1_t2235177892 * __this, const MethodInfo* method)
{
	{
		ContactPoint_t1376425630  L_0 = InternalEnumerator_1_get_Current_m3035290781((InternalEnumerator_1_t2235177892 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ContactPoint_t1376425630  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1497708066_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2235177892 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2235177892 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1497708066(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.ContactPoint>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3715403693_gshared (InternalEnumerator_1_t2235177892 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3715403693_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2235177892 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2235177892 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3715403693(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.ContactPoint>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3299881374_gshared (InternalEnumerator_1_t2235177892 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3299881374_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2235177892 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2235177892 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3299881374(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.ContactPoint>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m3035290781_MetadataUsageId;
extern "C"  ContactPoint_t1376425630  InternalEnumerator_1_get_Current_m3035290781_gshared (InternalEnumerator_1_t2235177892 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3035290781_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		ContactPoint_t1376425630  L_8 = ((  ContactPoint_t1376425630  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  ContactPoint_t1376425630  InternalEnumerator_1_get_Current_m3035290781_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2235177892 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2235177892 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3035290781(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.ContactPoint2D>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3623160640_gshared (InternalEnumerator_1_t223115942 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3623160640_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t223115942 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t223115942 *>(__this + 1);
	InternalEnumerator_1__ctor_m3623160640(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.ContactPoint2D>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2619213736_gshared (InternalEnumerator_1_t223115942 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2619213736_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t223115942 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t223115942 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2619213736(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.ContactPoint2D>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2061144652_gshared (InternalEnumerator_1_t223115942 * __this, const MethodInfo* method)
{
	{
		ContactPoint2D_t3659330976  L_0 = InternalEnumerator_1_get_Current_m4045489063((InternalEnumerator_1_t223115942 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ContactPoint2D_t3659330976  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2061144652_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t223115942 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t223115942 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2061144652(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.ContactPoint2D>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3885764311_gshared (InternalEnumerator_1_t223115942 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3885764311_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t223115942 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t223115942 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3885764311(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.ContactPoint2D>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2906956792_gshared (InternalEnumerator_1_t223115942 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2906956792_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t223115942 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t223115942 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2906956792(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.ContactPoint2D>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m4045489063_MetadataUsageId;
extern "C"  ContactPoint2D_t3659330976  InternalEnumerator_1_get_Current_m4045489063_gshared (InternalEnumerator_1_t223115942 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m4045489063_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		ContactPoint2D_t3659330976  L_8 = ((  ContactPoint2D_t3659330976  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  ContactPoint2D_t3659330976  InternalEnumerator_1_get_Current_m4045489063_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t223115942 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t223115942 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m4045489063(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m994739194_gshared (InternalEnumerator_1_t879938638 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m994739194_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t879938638 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t879938638 *>(__this + 1);
	InternalEnumerator_1__ctor_m994739194(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2046302786_gshared (InternalEnumerator_1_t879938638 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2046302786_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t879938638 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t879938638 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2046302786(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2900144990_gshared (InternalEnumerator_1_t879938638 * __this, const MethodInfo* method)
{
	{
		RaycastResult_t21186376  L_0 = InternalEnumerator_1_get_Current_m319833891((InternalEnumerator_1_t879938638 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		RaycastResult_t21186376  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2900144990_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t879938638 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t879938638 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2900144990(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3805775699_gshared (InternalEnumerator_1_t879938638 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3805775699_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t879938638 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t879938638 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3805775699(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m572812642_gshared (InternalEnumerator_1_t879938638 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m572812642_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t879938638 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t879938638 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m572812642(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m319833891_MetadataUsageId;
extern "C"  RaycastResult_t21186376  InternalEnumerator_1_get_Current_m319833891_gshared (InternalEnumerator_1_t879938638 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m319833891_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		RaycastResult_t21186376  L_8 = ((  RaycastResult_t21186376  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  RaycastResult_t21186376  InternalEnumerator_1_get_Current_m319833891_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t879938638 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t879938638 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m319833891(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Experimental.Director.Playable>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1561877944_gshared (InternalEnumerator_1_t231330514 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1561877944_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t231330514 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t231330514 *>(__this + 1);
	InternalEnumerator_1__ctor_m1561877944(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Experimental.Director.Playable>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m196087328_gshared (InternalEnumerator_1_t231330514 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m196087328_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t231330514 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t231330514 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m196087328(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Experimental.Director.Playable>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1810411564_gshared (InternalEnumerator_1_t231330514 * __this, const MethodInfo* method)
{
	{
		Playable_t3667545548  L_0 = InternalEnumerator_1_get_Current_m1443393095((InternalEnumerator_1_t231330514 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Playable_t3667545548  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1810411564_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t231330514 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t231330514 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1810411564(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Experimental.Director.Playable>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2135741487_gshared (InternalEnumerator_1_t231330514 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2135741487_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t231330514 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t231330514 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2135741487(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Experimental.Director.Playable>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m725174512_gshared (InternalEnumerator_1_t231330514 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m725174512_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t231330514 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t231330514 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m725174512(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.Experimental.Director.Playable>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m1443393095_MetadataUsageId;
extern "C"  Playable_t3667545548  InternalEnumerator_1_get_Current_m1443393095_gshared (InternalEnumerator_1_t231330514 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1443393095_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Playable_t3667545548  L_8 = ((  Playable_t3667545548  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Playable_t3667545548  InternalEnumerator_1_get_Current_m1443393095_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t231330514 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t231330514 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1443393095(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2007859216_gshared (InternalEnumerator_1_t2308223602 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2007859216_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2308223602 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2308223602 *>(__this + 1);
	InternalEnumerator_1__ctor_m2007859216(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2715220344_gshared (InternalEnumerator_1_t2308223602 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2715220344_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2308223602 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2308223602 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2715220344(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m790514740_gshared (InternalEnumerator_1_t2308223602 * __this, const MethodInfo* method)
{
	{
		Keyframe_t1449471340  L_0 = InternalEnumerator_1_get_Current_m3959023023((InternalEnumerator_1_t2308223602 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Keyframe_t1449471340  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m790514740_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2308223602 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2308223602 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m790514740(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3766393335_gshared (InternalEnumerator_1_t2308223602 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3766393335_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2308223602 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2308223602 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3766393335(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2289229080_gshared (InternalEnumerator_1_t2308223602 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2289229080_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2308223602 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2308223602 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2289229080(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m3959023023_MetadataUsageId;
extern "C"  Keyframe_t1449471340  InternalEnumerator_1_get_Current_m3959023023_gshared (InternalEnumerator_1_t2308223602 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3959023023_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Keyframe_t1449471340  L_8 = ((  Keyframe_t1449471340  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Keyframe_t1449471340  InternalEnumerator_1_get_Current_m3959023023_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2308223602 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2308223602 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3959023023(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3664249240_gshared (InternalEnumerator_1_t945932582 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3664249240_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t945932582 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t945932582 *>(__this + 1);
	InternalEnumerator_1__ctor_m3664249240(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m192344320_gshared (InternalEnumerator_1_t945932582 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m192344320_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t945932582 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t945932582 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m192344320(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3043347404_gshared (InternalEnumerator_1_t945932582 * __this, const MethodInfo* method)
{
	{
		RaycastHit_t87180320  L_0 = InternalEnumerator_1_get_Current_m1715820327((InternalEnumerator_1_t945932582 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		RaycastHit_t87180320  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3043347404_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t945932582 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t945932582 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3043347404(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3464626239_gshared (InternalEnumerator_1_t945932582 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3464626239_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t945932582 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t945932582 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3464626239(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3332669936_gshared (InternalEnumerator_1_t945932582 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3332669936_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t945932582 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t945932582 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3332669936(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m1715820327_MetadataUsageId;
extern "C"  RaycastHit_t87180320  InternalEnumerator_1_get_Current_m1715820327_gshared (InternalEnumerator_1_t945932582 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1715820327_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		RaycastHit_t87180320  L_8 = ((  RaycastHit_t87180320  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  RaycastHit_t87180320  InternalEnumerator_1_get_Current_m1715820327_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t945932582 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t945932582 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1715820327(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m32322958_gshared (InternalEnumerator_1_t627693740 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m32322958_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t627693740 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t627693740 *>(__this + 1);
	InternalEnumerator_1__ctor_m32322958(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1777467498_gshared (InternalEnumerator_1_t627693740 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1777467498_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t627693740 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t627693740 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1777467498(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1533037706_gshared (InternalEnumerator_1_t627693740 * __this, const MethodInfo* method)
{
	{
		RaycastHit2D_t4063908774  L_0 = InternalEnumerator_1_get_Current_m1025321669((InternalEnumerator_1_t627693740 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		RaycastHit2D_t4063908774  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1533037706_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t627693740 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t627693740 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1533037706(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m4040890621_gshared (InternalEnumerator_1_t627693740 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m4040890621_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t627693740 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t627693740 *>(__this + 1);
	InternalEnumerator_1_Dispose_m4040890621(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1799288398_gshared (InternalEnumerator_1_t627693740 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1799288398_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t627693740 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t627693740 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1799288398(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m1025321669_MetadataUsageId;
extern "C"  RaycastHit2D_t4063908774  InternalEnumerator_1_get_Current_m1025321669_gshared (InternalEnumerator_1_t627693740 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1025321669_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		RaycastHit2D_t4063908774  L_8 = ((  RaycastHit2D_t4063908774  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  RaycastHit2D_t4063908774  InternalEnumerator_1_get_Current_m1025321669_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t627693740 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t627693740 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1025321669(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3220229132_gshared (InternalEnumerator_1_t2620119317 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3220229132_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2620119317 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2620119317 *>(__this + 1);
	InternalEnumerator_1__ctor_m3220229132(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m574988908_gshared (InternalEnumerator_1_t2620119317 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m574988908_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2620119317 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2620119317 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m574988908(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1933635818_gshared (InternalEnumerator_1_t2620119317 * __this, const MethodInfo* method)
{
	{
		HitInfo_t1761367055  L_0 = InternalEnumerator_1_get_Current_m2826780083((InternalEnumerator_1_t2620119317 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		HitInfo_t1761367055  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1933635818_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2620119317 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2620119317 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1933635818(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m282312359_gshared (InternalEnumerator_1_t2620119317 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m282312359_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2620119317 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2620119317 *>(__this + 1);
	InternalEnumerator_1_Dispose_m282312359(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m886855812_gshared (InternalEnumerator_1_t2620119317 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m886855812_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2620119317 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2620119317 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m886855812(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m2826780083_MetadataUsageId;
extern "C"  HitInfo_t1761367055  InternalEnumerator_1_get_Current_m2826780083_gshared (InternalEnumerator_1_t2620119317 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2826780083_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		HitInfo_t1761367055  L_8 = ((  HitInfo_t1761367055  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  HitInfo_t1761367055  InternalEnumerator_1_get_Current_m2826780083_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2620119317 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2620119317 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2826780083(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3474059021_gshared (InternalEnumerator_1_t2613618411 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3474059021_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2613618411 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2613618411 *>(__this + 1);
	InternalEnumerator_1__ctor_m3474059021(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3946824409_gshared (InternalEnumerator_1_t2613618411 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3946824409_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2613618411 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2613618411 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3946824409(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2685895857_gshared (InternalEnumerator_1_t2613618411 * __this, const MethodInfo* method)
{
	{
		GcAchievementData_t1754866149  L_0 = InternalEnumerator_1_get_Current_m406393356((InternalEnumerator_1_t2613618411 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		GcAchievementData_t1754866149  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2685895857_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2613618411 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2613618411 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2685895857(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m250541766_gshared (InternalEnumerator_1_t2613618411 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m250541766_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2613618411 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2613618411 *>(__this + 1);
	InternalEnumerator_1_Dispose_m250541766(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2520133033_gshared (InternalEnumerator_1_t2613618411 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2520133033_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2613618411 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2613618411 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2520133033(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m406393356_MetadataUsageId;
extern "C"  GcAchievementData_t1754866149  InternalEnumerator_1_get_Current_m406393356_gshared (InternalEnumerator_1_t2613618411 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m406393356_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		GcAchievementData_t1754866149  L_8 = ((  GcAchievementData_t1754866149  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  GcAchievementData_t1754866149  InternalEnumerator_1_get_Current_m406393356_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2613618411 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2613618411 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m406393356(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2891033852_gshared (InternalEnumerator_1_t240568204 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2891033852_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t240568204 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t240568204 *>(__this + 1);
	InternalEnumerator_1__ctor_m2891033852(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1055858572_gshared (InternalEnumerator_1_t240568204 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1055858572_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t240568204 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t240568204 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1055858572(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1201713088_gshared (InternalEnumerator_1_t240568204 * __this, const MethodInfo* method)
{
	{
		GcScoreData_t3676783238  L_0 = InternalEnumerator_1_get_Current_m75828603((InternalEnumerator_1_t240568204 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		GcScoreData_t3676783238  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1201713088_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t240568204 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t240568204 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1201713088(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1982788747_gshared (InternalEnumerator_1_t240568204 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1982788747_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t240568204 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t240568204 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1982788747(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4065131604_gshared (InternalEnumerator_1_t240568204 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m4065131604_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t240568204 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t240568204 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m4065131604(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m75828603_MetadataUsageId;
extern "C"  GcScoreData_t3676783238  InternalEnumerator_1_get_Current_m75828603_gshared (InternalEnumerator_1_t240568204 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m75828603_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		GcScoreData_t3676783238  L_8 = ((  GcScoreData_t3676783238  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  GcScoreData_t3676783238  InternalEnumerator_1_get_Current_m75828603_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t240568204 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t240568204 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m75828603(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.TextEditor/TextEditOp>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1334636825_gshared (InternalEnumerator_1_t3997549960 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1334636825_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3997549960 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3997549960 *>(__this + 1);
	InternalEnumerator_1__ctor_m1334636825(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1511254101_gshared (InternalEnumerator_1_t3997549960 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1511254101_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3997549960 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3997549960 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1511254101(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3349211177_gshared (InternalEnumerator_1_t3997549960 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m3180886734((InternalEnumerator_1_t3997549960 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3349211177_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3997549960 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3997549960 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3349211177(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.TextEditor/TextEditOp>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m4150336622_gshared (InternalEnumerator_1_t3997549960 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m4150336622_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3997549960 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3997549960 *>(__this + 1);
	InternalEnumerator_1_Dispose_m4150336622(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.TextEditor/TextEditOp>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2708162461_gshared (InternalEnumerator_1_t3997549960 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2708162461_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3997549960 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3997549960 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2708162461(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.TextEditor/TextEditOp>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m3180886734_MetadataUsageId;
extern "C"  int32_t InternalEnumerator_1_get_Current_m3180886734_gshared (InternalEnumerator_1_t3997549960 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3180886734_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m3180886734_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3997549960 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3997549960 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3180886734(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2458691472_gshared (InternalEnumerator_1_t1887381311 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2458691472_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1887381311 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1887381311 *>(__this + 1);
	InternalEnumerator_1__ctor_m2458691472(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m86252988_gshared (InternalEnumerator_1_t1887381311 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m86252988_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1887381311 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1887381311 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m86252988(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2389982234_gshared (InternalEnumerator_1_t1887381311 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m3732458101((InternalEnumerator_1_t1887381311 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2389982234_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1887381311 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1887381311 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2389982234(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3291666845_gshared (InternalEnumerator_1_t1887381311 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3291666845_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1887381311 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1887381311 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3291666845(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m252820768_gshared (InternalEnumerator_1_t1887381311 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m252820768_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1887381311 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1887381311 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m252820768(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m3732458101_MetadataUsageId;
extern "C"  int32_t InternalEnumerator_1_get_Current_m3732458101_gshared (InternalEnumerator_1_t1887381311 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3732458101_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m3732458101_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1887381311 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1887381311 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3732458101(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1815261138_gshared (InternalEnumerator_1_t3915389062 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1815261138_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3915389062 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3915389062 *>(__this + 1);
	InternalEnumerator_1__ctor_m1815261138(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2208002250_gshared (InternalEnumerator_1_t3915389062 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2208002250_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3915389062 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3915389062 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2208002250(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m160972190_gshared (InternalEnumerator_1_t3915389062 * __this, const MethodInfo* method)
{
	{
		UICharInfo_t3056636800  L_0 = InternalEnumerator_1_get_Current_m889125315((InternalEnumerator_1_t3915389062 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		UICharInfo_t3056636800  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m160972190_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3915389062 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3915389062 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m160972190(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1399397099_gshared (InternalEnumerator_1_t3915389062 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1399397099_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3915389062 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3915389062 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1399397099(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3850699098_gshared (InternalEnumerator_1_t3915389062 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3850699098_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3915389062 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3915389062 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3850699098(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m889125315_MetadataUsageId;
extern "C"  UICharInfo_t3056636800  InternalEnumerator_1_get_Current_m889125315_gshared (InternalEnumerator_1_t3915389062 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m889125315_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		UICharInfo_t3056636800  L_8 = ((  UICharInfo_t3056636800  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  UICharInfo_t3056636800  InternalEnumerator_1_get_Current_m889125315_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3915389062 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3915389062 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m889125315(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UILineInfo>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m681761736_gshared (InternalEnumerator_1_t185062840 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m681761736_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t185062840 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t185062840 *>(__this + 1);
	InternalEnumerator_1__ctor_m681761736(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UILineInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3775211636_gshared (InternalEnumerator_1_t185062840 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3775211636_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t185062840 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t185062840 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3775211636(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UILineInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2821735692_gshared (InternalEnumerator_1_t185062840 * __this, const MethodInfo* method)
{
	{
		UILineInfo_t3621277874  L_0 = InternalEnumerator_1_get_Current_m2105085649((InternalEnumerator_1_t185062840 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		UILineInfo_t3621277874  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2821735692_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t185062840 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t185062840 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2821735692(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UILineInfo>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2045737049_gshared (InternalEnumerator_1_t185062840 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2045737049_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t185062840 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t185062840 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2045737049(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UILineInfo>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2410670600_gshared (InternalEnumerator_1_t185062840 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2410670600_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t185062840 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t185062840 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2410670600(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.UILineInfo>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m2105085649_MetadataUsageId;
extern "C"  UILineInfo_t3621277874  InternalEnumerator_1_get_Current_m2105085649_gshared (InternalEnumerator_1_t185062840 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2105085649_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		UILineInfo_t3621277874  L_8 = ((  UILineInfo_t3621277874  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  UILineInfo_t3621277874  InternalEnumerator_1_get_Current_m2105085649_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t185062840 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t185062840 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2105085649(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UIVertex>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2956304256_gshared (InternalEnumerator_1_t2063011080 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2956304256_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2063011080 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2063011080 *>(__this + 1);
	InternalEnumerator_1__ctor_m2956304256(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UIVertex>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2315964220_gshared (InternalEnumerator_1_t2063011080 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2315964220_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2063011080 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2063011080 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2315964220(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UIVertex>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2764360876_gshared (InternalEnumerator_1_t2063011080 * __this, const MethodInfo* method)
{
	{
		UIVertex_t1204258818  L_0 = InternalEnumerator_1_get_Current_m1883328177((InternalEnumerator_1_t2063011080 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		UIVertex_t1204258818  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2764360876_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2063011080 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2063011080 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2764360876(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UIVertex>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m4229866913_gshared (InternalEnumerator_1_t2063011080 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m4229866913_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2063011080 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2063011080 *>(__this + 1);
	InternalEnumerator_1_Dispose_m4229866913(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UIVertex>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4061424048_gshared (InternalEnumerator_1_t2063011080 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m4061424048_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2063011080 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2063011080 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m4061424048(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.UIVertex>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m1883328177_MetadataUsageId;
extern "C"  UIVertex_t1204258818  InternalEnumerator_1_get_Current_m1883328177_gshared (InternalEnumerator_1_t2063011080 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1883328177_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		UIVertex_t1204258818  L_8 = ((  UIVertex_t1204258818  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  UIVertex_t1204258818  InternalEnumerator_1_get_Current_m1883328177_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2063011080 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2063011080 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1883328177(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector2>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2808001655_gshared (InternalEnumerator_1_t3102459841 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2808001655_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3102459841 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3102459841 *>(__this + 1);
	InternalEnumerator_1__ctor_m2808001655(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector2>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1018453615_gshared (InternalEnumerator_1_t3102459841 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1018453615_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3102459841 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3102459841 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1018453615(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Vector2>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m442726479_gshared (InternalEnumerator_1_t3102459841 * __this, const MethodInfo* method)
{
	{
		Vector2_t2243707579  L_0 = InternalEnumerator_1_get_Current_m2986222582((InternalEnumerator_1_t3102459841 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Vector2_t2243707579  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m442726479_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3102459841 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3102459841 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m442726479(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector2>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2270401482_gshared (InternalEnumerator_1_t3102459841 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2270401482_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3102459841 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3102459841 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2270401482(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Vector2>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4175772187_gshared (InternalEnumerator_1_t3102459841 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m4175772187_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3102459841 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3102459841 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m4175772187(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.Vector2>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m2986222582_MetadataUsageId;
extern "C"  Vector2_t2243707579  InternalEnumerator_1_get_Current_m2986222582_gshared (InternalEnumerator_1_t3102459841 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2986222582_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Vector2_t2243707579  L_8 = ((  Vector2_t2243707579  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Vector2_t2243707579  InternalEnumerator_1_get_Current_m2986222582_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3102459841 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3102459841 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2986222582(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector3>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2782443954_gshared (InternalEnumerator_1_t3102459842 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2782443954_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3102459842 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3102459842 *>(__this + 1);
	InternalEnumerator_1__ctor_m2782443954(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector3>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2361456586_gshared (InternalEnumerator_1_t3102459842 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2361456586_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3102459842 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3102459842 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2361456586(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Vector3>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m762846484_gshared (InternalEnumerator_1_t3102459842 * __this, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = InternalEnumerator_1_get_Current_m747506907((InternalEnumerator_1_t3102459842 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Vector3_t2243707580  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m762846484_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3102459842 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3102459842 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m762846484(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector3>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m14398895_gshared (InternalEnumerator_1_t3102459842 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m14398895_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3102459842 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3102459842 *>(__this + 1);
	InternalEnumerator_1_Dispose_m14398895(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Vector3>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2953305370_gshared (InternalEnumerator_1_t3102459842 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2953305370_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3102459842 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3102459842 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2953305370(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.Vector3>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m747506907_MetadataUsageId;
extern "C"  Vector3_t2243707580  InternalEnumerator_1_get_Current_m747506907_gshared (InternalEnumerator_1_t3102459842 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m747506907_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Vector3_t2243707580  L_8 = ((  Vector3_t2243707580  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Vector3_t2243707580  InternalEnumerator_1_get_Current_m747506907_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3102459842 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3102459842 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m747506907(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector4>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3901400705_gshared (InternalEnumerator_1_t3102459843 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3901400705_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3102459843 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3102459843 *>(__this + 1);
	InternalEnumerator_1__ctor_m3901400705(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector4>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3994416165_gshared (InternalEnumerator_1_t3102459843 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3994416165_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3102459843 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3102459843 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3994416165(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Vector4>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1699120817_gshared (InternalEnumerator_1_t3102459843 * __this, const MethodInfo* method)
{
	{
		Vector4_t2243707581  L_0 = InternalEnumerator_1_get_Current_m2687258796((InternalEnumerator_1_t3102459843 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Vector4_t2243707581  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1699120817_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3102459843 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3102459843 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1699120817(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector4>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1925604588_gshared (InternalEnumerator_1_t3102459843 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1925604588_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3102459843 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3102459843 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1925604588(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Vector4>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1441038493_gshared (InternalEnumerator_1_t3102459843 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1441038493_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3102459843 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3102459843 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1441038493(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.Vector4>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m2687258796_MetadataUsageId;
extern "C"  Vector4_t2243707581  InternalEnumerator_1_get_Current_m2687258796_gshared (InternalEnumerator_1_t3102459843 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2687258796_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Vector4_t2243707581  L_8 = ((  Vector4_t2243707581  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Vector4_t2243707581  InternalEnumerator_1_get_Current_m2687258796_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3102459843 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3102459843 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2687258796(_thisAdjusted, method);
}
// T[] System.ArraySegment`1<System.Byte>::get_Array()
extern "C"  ByteU5BU5D_t3397334013* ArraySegment_1_get_Array_m3660490680_gshared (ArraySegment_1_t2594217482 * __this, const MethodInfo* method)
{
	{
		ByteU5BU5D_t3397334013* L_0 = (ByteU5BU5D_t3397334013*)__this->get_array_0();
		return L_0;
	}
}
extern "C"  ByteU5BU5D_t3397334013* ArraySegment_1_get_Array_m3660490680_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	ArraySegment_1_t2594217482 * _thisAdjusted = reinterpret_cast<ArraySegment_1_t2594217482 *>(__this + 1);
	return ArraySegment_1_get_Array_m3660490680(_thisAdjusted, method);
}
// System.Int32 System.ArraySegment`1<System.Byte>::get_Offset()
extern "C"  int32_t ArraySegment_1_get_Offset_m211308369_gshared (ArraySegment_1_t2594217482 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_offset_1();
		return L_0;
	}
}
extern "C"  int32_t ArraySegment_1_get_Offset_m211308369_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	ArraySegment_1_t2594217482 * _thisAdjusted = reinterpret_cast<ArraySegment_1_t2594217482 *>(__this + 1);
	return ArraySegment_1_get_Offset_m211308369(_thisAdjusted, method);
}
// System.Int32 System.ArraySegment`1<System.Byte>::get_Count()
extern "C"  int32_t ArraySegment_1_get_Count_m4010248531_gshared (ArraySegment_1_t2594217482 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_count_2();
		return L_0;
	}
}
extern "C"  int32_t ArraySegment_1_get_Count_m4010248531_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	ArraySegment_1_t2594217482 * _thisAdjusted = reinterpret_cast<ArraySegment_1_t2594217482 *>(__this + 1);
	return ArraySegment_1_get_Count_m4010248531(_thisAdjusted, method);
}
// System.Boolean System.ArraySegment`1<System.Byte>::Equals(System.Object)
extern "C"  bool ArraySegment_1_Equals_m3670425628_gshared (ArraySegment_1_t2594217482 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		if (!((Il2CppObject *)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_0018;
		}
	}
	{
		Il2CppObject * L_1 = ___obj0;
		bool L_2 = ArraySegment_1_Equals_m4189829166((ArraySegment_1_t2594217482 *)__this, (ArraySegment_1_t2594217482 )((*(ArraySegment_1_t2594217482 *)((ArraySegment_1_t2594217482 *)UnBox (L_1, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_2;
	}

IL_0018:
	{
		return (bool)0;
	}
}
extern "C"  bool ArraySegment_1_Equals_m3670425628_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	ArraySegment_1_t2594217482 * _thisAdjusted = reinterpret_cast<ArraySegment_1_t2594217482 *>(__this + 1);
	return ArraySegment_1_Equals_m3670425628(_thisAdjusted, ___obj0, method);
}
// System.Boolean System.ArraySegment`1<System.Byte>::Equals(System.ArraySegment`1<T>)
extern "C"  bool ArraySegment_1_Equals_m4189829166_gshared (ArraySegment_1_t2594217482 * __this, ArraySegment_1_t2594217482  ___obj0, const MethodInfo* method)
{
	{
		ByteU5BU5D_t3397334013* L_0 = (ByteU5BU5D_t3397334013*)__this->get_array_0();
		ByteU5BU5D_t3397334013* L_1 = ArraySegment_1_get_Array_m3660490680((ArraySegment_1_t2594217482 *)(&___obj0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		if ((!(((Il2CppObject*)(ByteU5BU5D_t3397334013*)L_0) == ((Il2CppObject*)(ByteU5BU5D_t3397334013*)L_1))))
		{
			goto IL_0038;
		}
	}
	{
		int32_t L_2 = (int32_t)__this->get_offset_1();
		int32_t L_3 = ArraySegment_1_get_Offset_m211308369((ArraySegment_1_t2594217482 *)(&___obj0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		if ((!(((uint32_t)L_2) == ((uint32_t)L_3))))
		{
			goto IL_0038;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_count_2();
		int32_t L_5 = ArraySegment_1_get_Count_m4010248531((ArraySegment_1_t2594217482 *)(&___obj0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		if ((!(((uint32_t)L_4) == ((uint32_t)L_5))))
		{
			goto IL_0038;
		}
	}
	{
		return (bool)1;
	}

IL_0038:
	{
		return (bool)0;
	}
}
extern "C"  bool ArraySegment_1_Equals_m4189829166_AdjustorThunk (Il2CppObject * __this, ArraySegment_1_t2594217482  ___obj0, const MethodInfo* method)
{
	ArraySegment_1_t2594217482 * _thisAdjusted = reinterpret_cast<ArraySegment_1_t2594217482 *>(__this + 1);
	return ArraySegment_1_Equals_m4189829166(_thisAdjusted, ___obj0, method);
}
// System.Int32 System.ArraySegment`1<System.Byte>::GetHashCode()
extern "C"  int32_t ArraySegment_1_GetHashCode_m1471616956_gshared (ArraySegment_1_t2594217482 * __this, const MethodInfo* method)
{
	{
		ByteU5BU5D_t3397334013* L_0 = (ByteU5BU5D_t3397334013*)__this->get_array_0();
		NullCheck((Il2CppObject *)(Il2CppObject *)L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)(Il2CppObject *)L_0);
		int32_t L_2 = (int32_t)__this->get_offset_1();
		int32_t L_3 = (int32_t)__this->get_count_2();
		return ((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)L_2))^(int32_t)L_3));
	}
}
extern "C"  int32_t ArraySegment_1_GetHashCode_m1471616956_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	ArraySegment_1_t2594217482 * _thisAdjusted = reinterpret_cast<ArraySegment_1_t2594217482 *>(__this + 1);
	return ArraySegment_1_GetHashCode_m1471616956(_thisAdjusted, method);
}
// T[] System.ArraySegment`1<System.Object>::get_Array()
extern "C"  ObjectU5BU5D_t3614634134* ArraySegment_1_get_Array_m1808599309_gshared (ArraySegment_1_t1600562341 * __this, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_array_0();
		return L_0;
	}
}
extern "C"  ObjectU5BU5D_t3614634134* ArraySegment_1_get_Array_m1808599309_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	ArraySegment_1_t1600562341 * _thisAdjusted = reinterpret_cast<ArraySegment_1_t1600562341 *>(__this + 1);
	return ArraySegment_1_get_Array_m1808599309(_thisAdjusted, method);
}
// System.Int32 System.ArraySegment`1<System.Object>::get_Offset()
extern "C"  int32_t ArraySegment_1_get_Offset_m28425256_gshared (ArraySegment_1_t1600562341 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_offset_1();
		return L_0;
	}
}
extern "C"  int32_t ArraySegment_1_get_Offset_m28425256_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	ArraySegment_1_t1600562341 * _thisAdjusted = reinterpret_cast<ArraySegment_1_t1600562341 *>(__this + 1);
	return ArraySegment_1_get_Offset_m28425256(_thisAdjusted, method);
}
// System.Int32 System.ArraySegment`1<System.Object>::get_Count()
extern "C"  int32_t ArraySegment_1_get_Count_m570182236_gshared (ArraySegment_1_t1600562341 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_count_2();
		return L_0;
	}
}
extern "C"  int32_t ArraySegment_1_get_Count_m570182236_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	ArraySegment_1_t1600562341 * _thisAdjusted = reinterpret_cast<ArraySegment_1_t1600562341 *>(__this + 1);
	return ArraySegment_1_get_Count_m570182236(_thisAdjusted, method);
}
// System.Boolean System.ArraySegment`1<System.Object>::Equals(System.Object)
extern "C"  bool ArraySegment_1_Equals_m2027598521_gshared (ArraySegment_1_t1600562341 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		if (!((Il2CppObject *)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_0018;
		}
	}
	{
		Il2CppObject * L_1 = ___obj0;
		bool L_2 = ArraySegment_1_Equals_m2459999213((ArraySegment_1_t1600562341 *)__this, (ArraySegment_1_t1600562341 )((*(ArraySegment_1_t1600562341 *)((ArraySegment_1_t1600562341 *)UnBox (L_1, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_2;
	}

IL_0018:
	{
		return (bool)0;
	}
}
extern "C"  bool ArraySegment_1_Equals_m2027598521_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	ArraySegment_1_t1600562341 * _thisAdjusted = reinterpret_cast<ArraySegment_1_t1600562341 *>(__this + 1);
	return ArraySegment_1_Equals_m2027598521(_thisAdjusted, ___obj0, method);
}
// System.Boolean System.ArraySegment`1<System.Object>::Equals(System.ArraySegment`1<T>)
extern "C"  bool ArraySegment_1_Equals_m2459999213_gshared (ArraySegment_1_t1600562341 * __this, ArraySegment_1_t1600562341  ___obj0, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_array_0();
		ObjectU5BU5D_t3614634134* L_1 = ArraySegment_1_get_Array_m1808599309((ArraySegment_1_t1600562341 *)(&___obj0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		if ((!(((Il2CppObject*)(ObjectU5BU5D_t3614634134*)L_0) == ((Il2CppObject*)(ObjectU5BU5D_t3614634134*)L_1))))
		{
			goto IL_0038;
		}
	}
	{
		int32_t L_2 = (int32_t)__this->get_offset_1();
		int32_t L_3 = ArraySegment_1_get_Offset_m28425256((ArraySegment_1_t1600562341 *)(&___obj0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		if ((!(((uint32_t)L_2) == ((uint32_t)L_3))))
		{
			goto IL_0038;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_count_2();
		int32_t L_5 = ArraySegment_1_get_Count_m570182236((ArraySegment_1_t1600562341 *)(&___obj0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		if ((!(((uint32_t)L_4) == ((uint32_t)L_5))))
		{
			goto IL_0038;
		}
	}
	{
		return (bool)1;
	}

IL_0038:
	{
		return (bool)0;
	}
}
extern "C"  bool ArraySegment_1_Equals_m2459999213_AdjustorThunk (Il2CppObject * __this, ArraySegment_1_t1600562341  ___obj0, const MethodInfo* method)
{
	ArraySegment_1_t1600562341 * _thisAdjusted = reinterpret_cast<ArraySegment_1_t1600562341 *>(__this + 1);
	return ArraySegment_1_Equals_m2459999213(_thisAdjusted, ___obj0, method);
}
// System.Int32 System.ArraySegment`1<System.Object>::GetHashCode()
extern "C"  int32_t ArraySegment_1_GetHashCode_m163176103_gshared (ArraySegment_1_t1600562341 * __this, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_array_0();
		NullCheck((Il2CppObject *)(Il2CppObject *)L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)(Il2CppObject *)L_0);
		int32_t L_2 = (int32_t)__this->get_offset_1();
		int32_t L_3 = (int32_t)__this->get_count_2();
		return ((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)L_2))^(int32_t)L_3));
	}
}
extern "C"  int32_t ArraySegment_1_GetHashCode_m163176103_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	ArraySegment_1_t1600562341 * _thisAdjusted = reinterpret_cast<ArraySegment_1_t1600562341 *>(__this + 1);
	return ArraySegment_1_GetHashCode_m163176103(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::.ctor()
extern "C"  void DefaultComparer__ctor_m2649249961_gshared (DefaultComparer_t3562007204 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t3962746090 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t3962746090 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t3962746090 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::Compare(T,T)
extern Il2CppClass* IComparable_t1857082765_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1864089402;
extern const uint32_t DefaultComparer_Compare_m3614427036_MetadataUsageId;
extern "C"  int32_t DefaultComparer_Compare_m3614427036_gshared (DefaultComparer_t3562007204 * __this, KeyInfo_t777769675  ___x0, KeyInfo_t777769675  ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultComparer_Compare_m3614427036_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		goto IL_001e;
	}
	{
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		KeyInfo_t777769675  L_3 = ___x0;
		KeyInfo_t777769675  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		if (!((Il2CppObject*)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))
		{
			goto IL_004d;
		}
	}
	{
		KeyInfo_t777769675  L_6 = ___x0;
		KeyInfo_t777769675  L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_7);
		KeyInfo_t777769675  L_9 = ___y1;
		NullCheck((Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		int32_t L_10 = InterfaceFuncInvoker1< int32_t, KeyInfo_t777769675  >::Invoke(0 /* System.Int32 System.IComparable`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::CompareTo(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (KeyInfo_t777769675 )L_9);
		return L_10;
	}

IL_004d:
	{
		KeyInfo_t777769675  L_11 = ___x0;
		KeyInfo_t777769675  L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_12);
		if (!((Il2CppObject *)IsInst(L_13, IComparable_t1857082765_il2cpp_TypeInfo_var)))
		{
			goto IL_0074;
		}
	}
	{
		KeyInfo_t777769675  L_14 = ___x0;
		KeyInfo_t777769675  L_15 = L_14;
		Il2CppObject * L_16 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_15);
		KeyInfo_t777769675  L_17 = ___y1;
		KeyInfo_t777769675  L_18 = L_17;
		Il2CppObject * L_19 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_18);
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)));
		int32_t L_20 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.IComparable::CompareTo(System.Object) */, IComparable_t1857082765_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)), (Il2CppObject *)L_19);
		return L_20;
	}

IL_0074:
	{
		ArgumentException_t3259014390 * L_21 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_21, (String_t*)_stringLiteral1864089402, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}
}
// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.Byte>::.ctor()
extern "C"  void DefaultComparer__ctor_m1321769145_gshared (DefaultComparer_t2172374669 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t2573113555 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t2573113555 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t2573113555 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.Byte>::Compare(T,T)
extern Il2CppClass* IComparable_t1857082765_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1864089402;
extern const uint32_t DefaultComparer_Compare_m667764052_MetadataUsageId;
extern "C"  int32_t DefaultComparer_Compare_m667764052_gshared (DefaultComparer_t2172374669 * __this, uint8_t ___x0, uint8_t ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultComparer_Compare_m667764052_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		goto IL_001e;
	}
	{
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		uint8_t L_3 = ___x0;
		uint8_t L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		if (!((Il2CppObject*)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))
		{
			goto IL_004d;
		}
	}
	{
		uint8_t L_6 = ___x0;
		uint8_t L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_7);
		uint8_t L_9 = ___y1;
		NullCheck((Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		int32_t L_10 = InterfaceFuncInvoker1< int32_t, uint8_t >::Invoke(0 /* System.Int32 System.IComparable`1<System.Byte>::CompareTo(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (uint8_t)L_9);
		return L_10;
	}

IL_004d:
	{
		uint8_t L_11 = ___x0;
		uint8_t L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_12);
		if (!((Il2CppObject *)IsInst(L_13, IComparable_t1857082765_il2cpp_TypeInfo_var)))
		{
			goto IL_0074;
		}
	}
	{
		uint8_t L_14 = ___x0;
		uint8_t L_15 = L_14;
		Il2CppObject * L_16 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_15);
		uint8_t L_17 = ___y1;
		uint8_t L_18 = L_17;
		Il2CppObject * L_19 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_18);
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)));
		int32_t L_20 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.IComparable::CompareTo(System.Object) */, IComparable_t1857082765_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)), (Il2CppObject *)L_19);
		return L_20;
	}

IL_0074:
	{
		ArgumentException_t3259014390 * L_21 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_21, (String_t*)_stringLiteral1864089402, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}
}
// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::.ctor()
extern "C"  void DefaultComparer__ctor_m2951634824_gshared (DefaultComparer_t2238857681 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t2639596567 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t2639596567 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t2639596567 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::Compare(T,T)
extern Il2CppClass* IComparable_t1857082765_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1864089402;
extern const uint32_t DefaultComparer_Compare_m2551961411_MetadataUsageId;
extern "C"  int32_t DefaultComparer_Compare_m2551961411_gshared (DefaultComparer_t2238857681 * __this, KeyValuePair_2_t3749587448  ___x0, KeyValuePair_2_t3749587448  ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultComparer_Compare_m2551961411_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		goto IL_001e;
	}
	{
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		KeyValuePair_2_t3749587448  L_3 = ___x0;
		KeyValuePair_2_t3749587448  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		if (!((Il2CppObject*)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))
		{
			goto IL_004d;
		}
	}
	{
		KeyValuePair_2_t3749587448  L_6 = ___x0;
		KeyValuePair_2_t3749587448  L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_7);
		KeyValuePair_2_t3749587448  L_9 = ___y1;
		NullCheck((Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		int32_t L_10 = InterfaceFuncInvoker1< int32_t, KeyValuePair_2_t3749587448  >::Invoke(0 /* System.Int32 System.IComparable`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::CompareTo(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (KeyValuePair_2_t3749587448 )L_9);
		return L_10;
	}

IL_004d:
	{
		KeyValuePair_2_t3749587448  L_11 = ___x0;
		KeyValuePair_2_t3749587448  L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_12);
		if (!((Il2CppObject *)IsInst(L_13, IComparable_t1857082765_il2cpp_TypeInfo_var)))
		{
			goto IL_0074;
		}
	}
	{
		KeyValuePair_2_t3749587448  L_14 = ___x0;
		KeyValuePair_2_t3749587448  L_15 = L_14;
		Il2CppObject * L_16 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_15);
		KeyValuePair_2_t3749587448  L_17 = ___y1;
		KeyValuePair_2_t3749587448  L_18 = L_17;
		Il2CppObject * L_19 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_18);
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)));
		int32_t L_20 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.IComparable::CompareTo(System.Object) */, IComparable_t1857082765_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)), (Il2CppObject *)L_19);
		return L_20;
	}

IL_0074:
	{
		ArgumentException_t3259014390 * L_21 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_21, (String_t*)_stringLiteral1864089402, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}
}
// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor()
extern "C"  void DefaultComparer__ctor_m923018577_gshared (DefaultComparer_t2823092174 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t3223831060 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t3223831060 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t3223831060 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Compare(T,T)
extern Il2CppClass* IComparable_t1857082765_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1864089402;
extern const uint32_t DefaultComparer_Compare_m1276226958_MetadataUsageId;
extern "C"  int32_t DefaultComparer_Compare_m1276226958_gshared (DefaultComparer_t2823092174 * __this, KeyValuePair_2_t38854645  ___x0, KeyValuePair_2_t38854645  ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultComparer_Compare_m1276226958_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		goto IL_001e;
	}
	{
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		KeyValuePair_2_t38854645  L_3 = ___x0;
		KeyValuePair_2_t38854645  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		if (!((Il2CppObject*)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))
		{
			goto IL_004d;
		}
	}
	{
		KeyValuePair_2_t38854645  L_6 = ___x0;
		KeyValuePair_2_t38854645  L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_7);
		KeyValuePair_2_t38854645  L_9 = ___y1;
		NullCheck((Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		int32_t L_10 = InterfaceFuncInvoker1< int32_t, KeyValuePair_2_t38854645  >::Invoke(0 /* System.Int32 System.IComparable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::CompareTo(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (KeyValuePair_2_t38854645 )L_9);
		return L_10;
	}

IL_004d:
	{
		KeyValuePair_2_t38854645  L_11 = ___x0;
		KeyValuePair_2_t38854645  L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_12);
		if (!((Il2CppObject *)IsInst(L_13, IComparable_t1857082765_il2cpp_TypeInfo_var)))
		{
			goto IL_0074;
		}
	}
	{
		KeyValuePair_2_t38854645  L_14 = ___x0;
		KeyValuePair_2_t38854645  L_15 = L_14;
		Il2CppObject * L_16 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_15);
		KeyValuePair_2_t38854645  L_17 = ___y1;
		KeyValuePair_2_t38854645  L_18 = L_17;
		Il2CppObject * L_19 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_18);
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)));
		int32_t L_20 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.IComparable::CompareTo(System.Object) */, IComparable_t1857082765_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)), (Il2CppObject *)L_19);
		return L_20;
	}

IL_0074:
	{
		ArgumentException_t3259014390 * L_21 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_21, (String_t*)_stringLiteral1864089402, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}
}
// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.DateTime>::.ctor()
extern "C"  void DefaultComparer__ctor_m1799227370_gshared (DefaultComparer_t3477443198 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t3878182084 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t3878182084 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t3878182084 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.DateTime>::Compare(T,T)
extern Il2CppClass* IComparable_t1857082765_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1864089402;
extern const uint32_t DefaultComparer_Compare_m1606207039_MetadataUsageId;
extern "C"  int32_t DefaultComparer_Compare_m1606207039_gshared (DefaultComparer_t3477443198 * __this, DateTime_t693205669  ___x0, DateTime_t693205669  ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultComparer_Compare_m1606207039_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		goto IL_001e;
	}
	{
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		DateTime_t693205669  L_3 = ___x0;
		DateTime_t693205669  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		if (!((Il2CppObject*)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))
		{
			goto IL_004d;
		}
	}
	{
		DateTime_t693205669  L_6 = ___x0;
		DateTime_t693205669  L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_7);
		DateTime_t693205669  L_9 = ___y1;
		NullCheck((Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		int32_t L_10 = InterfaceFuncInvoker1< int32_t, DateTime_t693205669  >::Invoke(0 /* System.Int32 System.IComparable`1<System.DateTime>::CompareTo(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (DateTime_t693205669 )L_9);
		return L_10;
	}

IL_004d:
	{
		DateTime_t693205669  L_11 = ___x0;
		DateTime_t693205669  L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_12);
		if (!((Il2CppObject *)IsInst(L_13, IComparable_t1857082765_il2cpp_TypeInfo_var)))
		{
			goto IL_0074;
		}
	}
	{
		DateTime_t693205669  L_14 = ___x0;
		DateTime_t693205669  L_15 = L_14;
		Il2CppObject * L_16 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_15);
		DateTime_t693205669  L_17 = ___y1;
		DateTime_t693205669  L_18 = L_17;
		Il2CppObject * L_19 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_18);
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)));
		int32_t L_20 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.IComparable::CompareTo(System.Object) */, IComparable_t1857082765_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)), (Il2CppObject *)L_19);
		return L_20;
	}

IL_0074:
	{
		ArgumentException_t3259014390 * L_21 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_21, (String_t*)_stringLiteral1864089402, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}
}
// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.DateTimeOffset>::.ctor()
extern "C"  void DefaultComparer__ctor_m732373515_gshared (DefaultComparer_t4147226435 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t252998025 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t252998025 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t252998025 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.DateTimeOffset>::Compare(T,T)
extern Il2CppClass* IComparable_t1857082765_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1864089402;
extern const uint32_t DefaultComparer_Compare_m3472472212_MetadataUsageId;
extern "C"  int32_t DefaultComparer_Compare_m3472472212_gshared (DefaultComparer_t4147226435 * __this, DateTimeOffset_t1362988906  ___x0, DateTimeOffset_t1362988906  ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultComparer_Compare_m3472472212_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		goto IL_001e;
	}
	{
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		DateTimeOffset_t1362988906  L_3 = ___x0;
		DateTimeOffset_t1362988906  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		if (!((Il2CppObject*)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))
		{
			goto IL_004d;
		}
	}
	{
		DateTimeOffset_t1362988906  L_6 = ___x0;
		DateTimeOffset_t1362988906  L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_7);
		DateTimeOffset_t1362988906  L_9 = ___y1;
		NullCheck((Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		int32_t L_10 = InterfaceFuncInvoker1< int32_t, DateTimeOffset_t1362988906  >::Invoke(0 /* System.Int32 System.IComparable`1<System.DateTimeOffset>::CompareTo(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (DateTimeOffset_t1362988906 )L_9);
		return L_10;
	}

IL_004d:
	{
		DateTimeOffset_t1362988906  L_11 = ___x0;
		DateTimeOffset_t1362988906  L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_12);
		if (!((Il2CppObject *)IsInst(L_13, IComparable_t1857082765_il2cpp_TypeInfo_var)))
		{
			goto IL_0074;
		}
	}
	{
		DateTimeOffset_t1362988906  L_14 = ___x0;
		DateTimeOffset_t1362988906  L_15 = L_14;
		Il2CppObject * L_16 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_15);
		DateTimeOffset_t1362988906  L_17 = ___y1;
		DateTimeOffset_t1362988906  L_18 = L_17;
		Il2CppObject * L_19 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_18);
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)));
		int32_t L_20 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.IComparable::CompareTo(System.Object) */, IComparable_t1857082765_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)), (Il2CppObject *)L_19);
		return L_20;
	}

IL_0074:
	{
		ArgumentException_t3259014390 * L_21 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_21, (String_t*)_stringLiteral1864089402, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}
}
// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.Guid>::.ctor()
extern "C"  void DefaultComparer__ctor_m3668042_gshared (DefaultComparer_t1022871826 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t1423610712 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t1423610712 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t1423610712 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.Guid>::Compare(T,T)
extern Il2CppClass* IComparable_t1857082765_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1864089402;
extern const uint32_t DefaultComparer_Compare_m3319119721_MetadataUsageId;
extern "C"  int32_t DefaultComparer_Compare_m3319119721_gshared (DefaultComparer_t1022871826 * __this, Guid_t2533601593  ___x0, Guid_t2533601593  ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultComparer_Compare_m3319119721_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		goto IL_001e;
	}
	{
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		Guid_t2533601593  L_3 = ___x0;
		Guid_t2533601593  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		if (!((Il2CppObject*)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))
		{
			goto IL_004d;
		}
	}
	{
		Guid_t2533601593  L_6 = ___x0;
		Guid_t2533601593  L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_7);
		Guid_t2533601593  L_9 = ___y1;
		NullCheck((Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		int32_t L_10 = InterfaceFuncInvoker1< int32_t, Guid_t2533601593  >::Invoke(0 /* System.Int32 System.IComparable`1<System.Guid>::CompareTo(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (Guid_t2533601593 )L_9);
		return L_10;
	}

IL_004d:
	{
		Guid_t2533601593  L_11 = ___x0;
		Guid_t2533601593  L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_12);
		if (!((Il2CppObject *)IsInst(L_13, IComparable_t1857082765_il2cpp_TypeInfo_var)))
		{
			goto IL_0074;
		}
	}
	{
		Guid_t2533601593  L_14 = ___x0;
		Guid_t2533601593  L_15 = L_14;
		Il2CppObject * L_16 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_15);
		Guid_t2533601593  L_17 = ___y1;
		Guid_t2533601593  L_18 = L_17;
		Il2CppObject * L_19 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_18);
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)));
		int32_t L_20 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.IComparable::CompareTo(System.Object) */, IComparable_t1857082765_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)), (Il2CppObject *)L_19);
		return L_20;
	}

IL_0074:
	{
		ArgumentException_t3259014390 * L_21 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_21, (String_t*)_stringLiteral1864089402, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}
}
// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.Int32>::.ctor()
extern "C"  void DefaultComparer__ctor_m2859550749_gshared (DefaultComparer_t561147681 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t961886567 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t961886567 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t961886567 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.Int32>::Compare(T,T)
extern Il2CppClass* IComparable_t1857082765_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1864089402;
extern const uint32_t DefaultComparer_Compare_m925902394_MetadataUsageId;
extern "C"  int32_t DefaultComparer_Compare_m925902394_gshared (DefaultComparer_t561147681 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultComparer_Compare_m925902394_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		goto IL_001e;
	}
	{
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		int32_t L_3 = ___x0;
		int32_t L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		if (!((Il2CppObject*)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))
		{
			goto IL_004d;
		}
	}
	{
		int32_t L_6 = ___x0;
		int32_t L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_7);
		int32_t L_9 = ___y1;
		NullCheck((Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		int32_t L_10 = InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(0 /* System.Int32 System.IComparable`1<System.Int32>::CompareTo(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (int32_t)L_9);
		return L_10;
	}

IL_004d:
	{
		int32_t L_11 = ___x0;
		int32_t L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_12);
		if (!((Il2CppObject *)IsInst(L_13, IComparable_t1857082765_il2cpp_TypeInfo_var)))
		{
			goto IL_0074;
		}
	}
	{
		int32_t L_14 = ___x0;
		int32_t L_15 = L_14;
		Il2CppObject * L_16 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_15);
		int32_t L_17 = ___y1;
		int32_t L_18 = L_17;
		Il2CppObject * L_19 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_18);
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)));
		int32_t L_20 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.IComparable::CompareTo(System.Object) */, IComparable_t1857082765_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)), (Il2CppObject *)L_19);
		return L_20;
	}

IL_0074:
	{
		ArgumentException_t3259014390 * L_21 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_21, (String_t*)_stringLiteral1864089402, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}
}
// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.Object>::.ctor()
extern "C"  void DefaultComparer__ctor_m84239532_gshared (DefaultComparer_t1178719528 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t1579458414 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t1579458414 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t1579458414 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.Object>::Compare(T,T)
extern Il2CppClass* IComparable_t1857082765_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1864089402;
extern const uint32_t DefaultComparer_Compare_m2805784815_MetadataUsageId;
extern "C"  int32_t DefaultComparer_Compare_m2805784815_gshared (DefaultComparer_t1178719528 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultComparer_Compare_m2805784815_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		Il2CppObject * L_1 = ___y1;
		if (L_1)
		{
			goto IL_001c;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		Il2CppObject * L_2 = ___y1;
		if (L_2)
		{
			goto IL_002b;
		}
	}
	{
		return 1;
	}

IL_002b:
	{
		Il2CppObject * L_3 = ___x0;
		if (!((Il2CppObject*)IsInst(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))
		{
			goto IL_004d;
		}
	}
	{
		Il2CppObject * L_4 = ___x0;
		Il2CppObject * L_5 = ___y1;
		NullCheck((Il2CppObject*)((Il2CppObject*)Castclass(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		int32_t L_6 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.IComparable`1<System.Object>::CompareTo(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)((Il2CppObject*)Castclass(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (Il2CppObject *)L_5);
		return L_6;
	}

IL_004d:
	{
		Il2CppObject * L_7 = ___x0;
		if (!((Il2CppObject *)IsInst(L_7, IComparable_t1857082765_il2cpp_TypeInfo_var)))
		{
			goto IL_0074;
		}
	}
	{
		Il2CppObject * L_8 = ___x0;
		Il2CppObject * L_9 = ___y1;
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_8, IComparable_t1857082765_il2cpp_TypeInfo_var)));
		int32_t L_10 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.IComparable::CompareTo(System.Object) */, IComparable_t1857082765_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_8, IComparable_t1857082765_il2cpp_TypeInfo_var)), (Il2CppObject *)L_9);
		return L_10;
	}

IL_0074:
	{
		ArgumentException_t3259014390 * L_11 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_11, (String_t*)_stringLiteral1864089402, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_11);
	}
}
// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.Reflection.CustomAttributeNamedArgument>::.ctor()
extern "C"  void DefaultComparer__ctor_m1661558765_gshared (DefaultComparer_t2878395072 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t3279133958 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t3279133958 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t3279133958 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.Reflection.CustomAttributeNamedArgument>::Compare(T,T)
extern Il2CppClass* IComparable_t1857082765_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1864089402;
extern const uint32_t DefaultComparer_Compare_m2855268154_MetadataUsageId;
extern "C"  int32_t DefaultComparer_Compare_m2855268154_gshared (DefaultComparer_t2878395072 * __this, CustomAttributeNamedArgument_t94157543  ___x0, CustomAttributeNamedArgument_t94157543  ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultComparer_Compare_m2855268154_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		goto IL_001e;
	}
	{
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		CustomAttributeNamedArgument_t94157543  L_3 = ___x0;
		CustomAttributeNamedArgument_t94157543  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		if (!((Il2CppObject*)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))
		{
			goto IL_004d;
		}
	}
	{
		CustomAttributeNamedArgument_t94157543  L_6 = ___x0;
		CustomAttributeNamedArgument_t94157543  L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_7);
		CustomAttributeNamedArgument_t94157543  L_9 = ___y1;
		NullCheck((Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		int32_t L_10 = InterfaceFuncInvoker1< int32_t, CustomAttributeNamedArgument_t94157543  >::Invoke(0 /* System.Int32 System.IComparable`1<System.Reflection.CustomAttributeNamedArgument>::CompareTo(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (CustomAttributeNamedArgument_t94157543 )L_9);
		return L_10;
	}

IL_004d:
	{
		CustomAttributeNamedArgument_t94157543  L_11 = ___x0;
		CustomAttributeNamedArgument_t94157543  L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_12);
		if (!((Il2CppObject *)IsInst(L_13, IComparable_t1857082765_il2cpp_TypeInfo_var)))
		{
			goto IL_0074;
		}
	}
	{
		CustomAttributeNamedArgument_t94157543  L_14 = ___x0;
		CustomAttributeNamedArgument_t94157543  L_15 = L_14;
		Il2CppObject * L_16 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_15);
		CustomAttributeNamedArgument_t94157543  L_17 = ___y1;
		CustomAttributeNamedArgument_t94157543  L_18 = L_17;
		Il2CppObject * L_19 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_18);
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)));
		int32_t L_20 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.IComparable::CompareTo(System.Object) */, IComparable_t1857082765_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)), (Il2CppObject *)L_19);
		return L_20;
	}

IL_0074:
	{
		ArgumentException_t3259014390 * L_21 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_21, (String_t*)_stringLiteral1864089402, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}
}
// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.Reflection.CustomAttributeTypedArgument>::.ctor()
extern "C"  void DefaultComparer__ctor_m1961329658_gshared (DefaultComparer_t4282435443 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t388207033 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t388207033 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t388207033 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.Reflection.CustomAttributeTypedArgument>::Compare(T,T)
extern Il2CppClass* IComparable_t1857082765_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1864089402;
extern const uint32_t DefaultComparer_Compare_m932294475_MetadataUsageId;
extern "C"  int32_t DefaultComparer_Compare_m932294475_gshared (DefaultComparer_t4282435443 * __this, CustomAttributeTypedArgument_t1498197914  ___x0, CustomAttributeTypedArgument_t1498197914  ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultComparer_Compare_m932294475_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		goto IL_001e;
	}
	{
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		CustomAttributeTypedArgument_t1498197914  L_3 = ___x0;
		CustomAttributeTypedArgument_t1498197914  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		if (!((Il2CppObject*)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))
		{
			goto IL_004d;
		}
	}
	{
		CustomAttributeTypedArgument_t1498197914  L_6 = ___x0;
		CustomAttributeTypedArgument_t1498197914  L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_7);
		CustomAttributeTypedArgument_t1498197914  L_9 = ___y1;
		NullCheck((Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		int32_t L_10 = InterfaceFuncInvoker1< int32_t, CustomAttributeTypedArgument_t1498197914  >::Invoke(0 /* System.Int32 System.IComparable`1<System.Reflection.CustomAttributeTypedArgument>::CompareTo(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (CustomAttributeTypedArgument_t1498197914 )L_9);
		return L_10;
	}

IL_004d:
	{
		CustomAttributeTypedArgument_t1498197914  L_11 = ___x0;
		CustomAttributeTypedArgument_t1498197914  L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_12);
		if (!((Il2CppObject *)IsInst(L_13, IComparable_t1857082765_il2cpp_TypeInfo_var)))
		{
			goto IL_0074;
		}
	}
	{
		CustomAttributeTypedArgument_t1498197914  L_14 = ___x0;
		CustomAttributeTypedArgument_t1498197914  L_15 = L_14;
		Il2CppObject * L_16 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_15);
		CustomAttributeTypedArgument_t1498197914  L_17 = ___y1;
		CustomAttributeTypedArgument_t1498197914  L_18 = L_17;
		Il2CppObject * L_19 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_18);
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)));
		int32_t L_20 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.IComparable::CompareTo(System.Object) */, IComparable_t1857082765_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)), (Il2CppObject *)L_19);
		return L_20;
	}

IL_0074:
	{
		ArgumentException_t3259014390 * L_21 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_21, (String_t*)_stringLiteral1864089402, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}
}
// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.TimeSpan>::.ctor()
extern "C"  void DefaultComparer__ctor_m3791334730_gshared (DefaultComparer_t1919529182 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t2320268068 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t2320268068 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t2320268068 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.TimeSpan>::Compare(T,T)
extern Il2CppClass* IComparable_t1857082765_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1864089402;
extern const uint32_t DefaultComparer_Compare_m265474847_MetadataUsageId;
extern "C"  int32_t DefaultComparer_Compare_m265474847_gshared (DefaultComparer_t1919529182 * __this, TimeSpan_t3430258949  ___x0, TimeSpan_t3430258949  ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultComparer_Compare_m265474847_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		goto IL_001e;
	}
	{
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		TimeSpan_t3430258949  L_3 = ___x0;
		TimeSpan_t3430258949  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		if (!((Il2CppObject*)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))
		{
			goto IL_004d;
		}
	}
	{
		TimeSpan_t3430258949  L_6 = ___x0;
		TimeSpan_t3430258949  L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_7);
		TimeSpan_t3430258949  L_9 = ___y1;
		NullCheck((Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		int32_t L_10 = InterfaceFuncInvoker1< int32_t, TimeSpan_t3430258949  >::Invoke(0 /* System.Int32 System.IComparable`1<System.TimeSpan>::CompareTo(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (TimeSpan_t3430258949 )L_9);
		return L_10;
	}

IL_004d:
	{
		TimeSpan_t3430258949  L_11 = ___x0;
		TimeSpan_t3430258949  L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_12);
		if (!((Il2CppObject *)IsInst(L_13, IComparable_t1857082765_il2cpp_TypeInfo_var)))
		{
			goto IL_0074;
		}
	}
	{
		TimeSpan_t3430258949  L_14 = ___x0;
		TimeSpan_t3430258949  L_15 = L_14;
		Il2CppObject * L_16 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_15);
		TimeSpan_t3430258949  L_17 = ___y1;
		TimeSpan_t3430258949  L_18 = L_17;
		Il2CppObject * L_19 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_18);
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)));
		int32_t L_20 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.IComparable::CompareTo(System.Object) */, IComparable_t1857082765_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)), (Il2CppObject *)L_19);
		return L_20;
	}

IL_0074:
	{
		ArgumentException_t3259014390 * L_21 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_21, (String_t*)_stringLiteral1864089402, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}
}
// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Color32>::.ctor()
extern "C"  void DefaultComparer__ctor_m2185307103_gshared (DefaultComparer_t3658755047 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t4059493933 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t4059493933 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t4059493933 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Color32>::Compare(T,T)
extern Il2CppClass* IComparable_t1857082765_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1864089402;
extern const uint32_t DefaultComparer_Compare_m1247109616_MetadataUsageId;
extern "C"  int32_t DefaultComparer_Compare_m1247109616_gshared (DefaultComparer_t3658755047 * __this, Color32_t874517518  ___x0, Color32_t874517518  ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultComparer_Compare_m1247109616_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		goto IL_001e;
	}
	{
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		Color32_t874517518  L_3 = ___x0;
		Color32_t874517518  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		if (!((Il2CppObject*)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))
		{
			goto IL_004d;
		}
	}
	{
		Color32_t874517518  L_6 = ___x0;
		Color32_t874517518  L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_7);
		Color32_t874517518  L_9 = ___y1;
		NullCheck((Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		int32_t L_10 = InterfaceFuncInvoker1< int32_t, Color32_t874517518  >::Invoke(0 /* System.Int32 System.IComparable`1<UnityEngine.Color32>::CompareTo(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (Color32_t874517518 )L_9);
		return L_10;
	}

IL_004d:
	{
		Color32_t874517518  L_11 = ___x0;
		Color32_t874517518  L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_12);
		if (!((Il2CppObject *)IsInst(L_13, IComparable_t1857082765_il2cpp_TypeInfo_var)))
		{
			goto IL_0074;
		}
	}
	{
		Color32_t874517518  L_14 = ___x0;
		Color32_t874517518  L_15 = L_14;
		Il2CppObject * L_16 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_15);
		Color32_t874517518  L_17 = ___y1;
		Color32_t874517518  L_18 = L_17;
		Il2CppObject * L_19 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_18);
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)));
		int32_t L_20 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.IComparable::CompareTo(System.Object) */, IComparable_t1857082765_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)), (Il2CppObject *)L_19);
		return L_20;
	}

IL_0074:
	{
		ArgumentException_t3259014390 * L_21 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_21, (String_t*)_stringLiteral1864089402, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}
}
// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.EventSystems.RaycastResult>::.ctor()
extern "C"  void DefaultComparer__ctor_m3180706193_gshared (DefaultComparer_t2805423905 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t3206162791 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t3206162791 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t3206162791 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.EventSystems.RaycastResult>::Compare(T,T)
extern Il2CppClass* IComparable_t1857082765_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1864089402;
extern const uint32_t DefaultComparer_Compare_m851771764_MetadataUsageId;
extern "C"  int32_t DefaultComparer_Compare_m851771764_gshared (DefaultComparer_t2805423905 * __this, RaycastResult_t21186376  ___x0, RaycastResult_t21186376  ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultComparer_Compare_m851771764_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		goto IL_001e;
	}
	{
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		RaycastResult_t21186376  L_3 = ___x0;
		RaycastResult_t21186376  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		if (!((Il2CppObject*)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))
		{
			goto IL_004d;
		}
	}
	{
		RaycastResult_t21186376  L_6 = ___x0;
		RaycastResult_t21186376  L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_7);
		RaycastResult_t21186376  L_9 = ___y1;
		NullCheck((Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		int32_t L_10 = InterfaceFuncInvoker1< int32_t, RaycastResult_t21186376  >::Invoke(0 /* System.Int32 System.IComparable`1<UnityEngine.EventSystems.RaycastResult>::CompareTo(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (RaycastResult_t21186376 )L_9);
		return L_10;
	}

IL_004d:
	{
		RaycastResult_t21186376  L_11 = ___x0;
		RaycastResult_t21186376  L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_12);
		if (!((Il2CppObject *)IsInst(L_13, IComparable_t1857082765_il2cpp_TypeInfo_var)))
		{
			goto IL_0074;
		}
	}
	{
		RaycastResult_t21186376  L_14 = ___x0;
		RaycastResult_t21186376  L_15 = L_14;
		Il2CppObject * L_16 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_15);
		RaycastResult_t21186376  L_17 = ___y1;
		RaycastResult_t21186376  L_18 = L_17;
		Il2CppObject * L_19 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_18);
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)));
		int32_t L_20 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.IComparable::CompareTo(System.Object) */, IComparable_t1857082765_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)), (Il2CppObject *)L_19);
		return L_20;
	}

IL_0074:
	{
		ArgumentException_t3259014390 * L_21 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_21, (String_t*)_stringLiteral1864089402, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}
}
// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Experimental.Director.Playable>::.ctor()
extern "C"  void DefaultComparer__ctor_m76117625_gshared (DefaultComparer_t2156815781 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t2557554667 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t2557554667 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t2557554667 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Experimental.Director.Playable>::Compare(T,T)
extern Il2CppClass* IComparable_t1857082765_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1864089402;
extern const uint32_t DefaultComparer_Compare_m3277849110_MetadataUsageId;
extern "C"  int32_t DefaultComparer_Compare_m3277849110_gshared (DefaultComparer_t2156815781 * __this, Playable_t3667545548  ___x0, Playable_t3667545548  ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultComparer_Compare_m3277849110_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		goto IL_001e;
	}
	{
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		Playable_t3667545548  L_3 = ___x0;
		Playable_t3667545548  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		if (!((Il2CppObject*)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))
		{
			goto IL_004d;
		}
	}
	{
		Playable_t3667545548  L_6 = ___x0;
		Playable_t3667545548  L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_7);
		Playable_t3667545548  L_9 = ___y1;
		NullCheck((Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		int32_t L_10 = InterfaceFuncInvoker1< int32_t, Playable_t3667545548  >::Invoke(0 /* System.Int32 System.IComparable`1<UnityEngine.Experimental.Director.Playable>::CompareTo(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (Playable_t3667545548 )L_9);
		return L_10;
	}

IL_004d:
	{
		Playable_t3667545548  L_11 = ___x0;
		Playable_t3667545548  L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_12);
		if (!((Il2CppObject *)IsInst(L_13, IComparable_t1857082765_il2cpp_TypeInfo_var)))
		{
			goto IL_0074;
		}
	}
	{
		Playable_t3667545548  L_14 = ___x0;
		Playable_t3667545548  L_15 = L_14;
		Il2CppObject * L_16 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_15);
		Playable_t3667545548  L_17 = ___y1;
		Playable_t3667545548  L_18 = L_17;
		Il2CppObject * L_19 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_18);
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)));
		int32_t L_20 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.IComparable::CompareTo(System.Object) */, IComparable_t1857082765_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)), (Il2CppObject *)L_19);
		return L_20;
	}

IL_0074:
	{
		ArgumentException_t3259014390 * L_21 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_21, (String_t*)_stringLiteral1864089402, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}
}
// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UICharInfo>::.ctor()
extern "C"  void DefaultComparer__ctor_m2470932885_gshared (DefaultComparer_t1545907033 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t1946645919 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t1946645919 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t1946645919 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UICharInfo>::Compare(T,T)
extern Il2CppClass* IComparable_t1857082765_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1864089402;
extern const uint32_t DefaultComparer_Compare_m3386135912_MetadataUsageId;
extern "C"  int32_t DefaultComparer_Compare_m3386135912_gshared (DefaultComparer_t1545907033 * __this, UICharInfo_t3056636800  ___x0, UICharInfo_t3056636800  ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultComparer_Compare_m3386135912_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		goto IL_001e;
	}
	{
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		UICharInfo_t3056636800  L_3 = ___x0;
		UICharInfo_t3056636800  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		if (!((Il2CppObject*)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))
		{
			goto IL_004d;
		}
	}
	{
		UICharInfo_t3056636800  L_6 = ___x0;
		UICharInfo_t3056636800  L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_7);
		UICharInfo_t3056636800  L_9 = ___y1;
		NullCheck((Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		int32_t L_10 = InterfaceFuncInvoker1< int32_t, UICharInfo_t3056636800  >::Invoke(0 /* System.Int32 System.IComparable`1<UnityEngine.UICharInfo>::CompareTo(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (UICharInfo_t3056636800 )L_9);
		return L_10;
	}

IL_004d:
	{
		UICharInfo_t3056636800  L_11 = ___x0;
		UICharInfo_t3056636800  L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_12);
		if (!((Il2CppObject *)IsInst(L_13, IComparable_t1857082765_il2cpp_TypeInfo_var)))
		{
			goto IL_0074;
		}
	}
	{
		UICharInfo_t3056636800  L_14 = ___x0;
		UICharInfo_t3056636800  L_15 = L_14;
		Il2CppObject * L_16 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_15);
		UICharInfo_t3056636800  L_17 = ___y1;
		UICharInfo_t3056636800  L_18 = L_17;
		Il2CppObject * L_19 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_18);
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)));
		int32_t L_20 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.IComparable::CompareTo(System.Object) */, IComparable_t1857082765_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)), (Il2CppObject *)L_19);
		return L_20;
	}

IL_0074:
	{
		ArgumentException_t3259014390 * L_21 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_21, (String_t*)_stringLiteral1864089402, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}
}
// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UILineInfo>::.ctor()
extern "C"  void DefaultComparer__ctor_m709297127_gshared (DefaultComparer_t2110548107 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t2511286993 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t2511286993 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t2511286993 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UILineInfo>::Compare(T,T)
extern Il2CppClass* IComparable_t1857082765_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1864089402;
extern const uint32_t DefaultComparer_Compare_m2804119458_MetadataUsageId;
extern "C"  int32_t DefaultComparer_Compare_m2804119458_gshared (DefaultComparer_t2110548107 * __this, UILineInfo_t3621277874  ___x0, UILineInfo_t3621277874  ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultComparer_Compare_m2804119458_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		goto IL_001e;
	}
	{
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		UILineInfo_t3621277874  L_3 = ___x0;
		UILineInfo_t3621277874  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		if (!((Il2CppObject*)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))
		{
			goto IL_004d;
		}
	}
	{
		UILineInfo_t3621277874  L_6 = ___x0;
		UILineInfo_t3621277874  L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_7);
		UILineInfo_t3621277874  L_9 = ___y1;
		NullCheck((Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		int32_t L_10 = InterfaceFuncInvoker1< int32_t, UILineInfo_t3621277874  >::Invoke(0 /* System.Int32 System.IComparable`1<UnityEngine.UILineInfo>::CompareTo(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (UILineInfo_t3621277874 )L_9);
		return L_10;
	}

IL_004d:
	{
		UILineInfo_t3621277874  L_11 = ___x0;
		UILineInfo_t3621277874  L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_12);
		if (!((Il2CppObject *)IsInst(L_13, IComparable_t1857082765_il2cpp_TypeInfo_var)))
		{
			goto IL_0074;
		}
	}
	{
		UILineInfo_t3621277874  L_14 = ___x0;
		UILineInfo_t3621277874  L_15 = L_14;
		Il2CppObject * L_16 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_15);
		UILineInfo_t3621277874  L_17 = ___y1;
		UILineInfo_t3621277874  L_18 = L_17;
		Il2CppObject * L_19 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_18);
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)));
		int32_t L_20 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.IComparable::CompareTo(System.Object) */, IComparable_t1857082765_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)), (Il2CppObject *)L_19);
		return L_20;
	}

IL_0074:
	{
		ArgumentException_t3259014390 * L_21 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_21, (String_t*)_stringLiteral1864089402, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}
}
// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UIVertex>::.ctor()
extern "C"  void DefaultComparer__ctor_m710539671_gshared (DefaultComparer_t3988496347 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t94267937 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t94267937 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t94267937 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UIVertex>::Compare(T,T)
extern Il2CppClass* IComparable_t1857082765_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1864089402;
extern const uint32_t DefaultComparer_Compare_m3564013922_MetadataUsageId;
extern "C"  int32_t DefaultComparer_Compare_m3564013922_gshared (DefaultComparer_t3988496347 * __this, UIVertex_t1204258818  ___x0, UIVertex_t1204258818  ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultComparer_Compare_m3564013922_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		goto IL_001e;
	}
	{
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		UIVertex_t1204258818  L_3 = ___x0;
		UIVertex_t1204258818  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		if (!((Il2CppObject*)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))
		{
			goto IL_004d;
		}
	}
	{
		UIVertex_t1204258818  L_6 = ___x0;
		UIVertex_t1204258818  L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_7);
		UIVertex_t1204258818  L_9 = ___y1;
		NullCheck((Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		int32_t L_10 = InterfaceFuncInvoker1< int32_t, UIVertex_t1204258818  >::Invoke(0 /* System.Int32 System.IComparable`1<UnityEngine.UIVertex>::CompareTo(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (UIVertex_t1204258818 )L_9);
		return L_10;
	}

IL_004d:
	{
		UIVertex_t1204258818  L_11 = ___x0;
		UIVertex_t1204258818  L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_12);
		if (!((Il2CppObject *)IsInst(L_13, IComparable_t1857082765_il2cpp_TypeInfo_var)))
		{
			goto IL_0074;
		}
	}
	{
		UIVertex_t1204258818  L_14 = ___x0;
		UIVertex_t1204258818  L_15 = L_14;
		Il2CppObject * L_16 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_15);
		UIVertex_t1204258818  L_17 = ___y1;
		UIVertex_t1204258818  L_18 = L_17;
		Il2CppObject * L_19 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_18);
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)));
		int32_t L_20 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.IComparable::CompareTo(System.Object) */, IComparable_t1857082765_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)), (Il2CppObject *)L_19);
		return L_20;
	}

IL_0074:
	{
		ArgumentException_t3259014390 * L_21 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_21, (String_t*)_stringLiteral1864089402, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}
}
// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Vector2>::.ctor()
extern "C"  void DefaultComparer__ctor_m2251954164_gshared (DefaultComparer_t732977812 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t1133716698 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t1133716698 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t1133716698 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Vector2>::Compare(T,T)
extern Il2CppClass* IComparable_t1857082765_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1864089402;
extern const uint32_t DefaultComparer_Compare_m3845579773_MetadataUsageId;
extern "C"  int32_t DefaultComparer_Compare_m3845579773_gshared (DefaultComparer_t732977812 * __this, Vector2_t2243707579  ___x0, Vector2_t2243707579  ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultComparer_Compare_m3845579773_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		goto IL_001e;
	}
	{
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		Vector2_t2243707579  L_3 = ___x0;
		Vector2_t2243707579  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		if (!((Il2CppObject*)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))
		{
			goto IL_004d;
		}
	}
	{
		Vector2_t2243707579  L_6 = ___x0;
		Vector2_t2243707579  L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_7);
		Vector2_t2243707579  L_9 = ___y1;
		NullCheck((Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		int32_t L_10 = InterfaceFuncInvoker1< int32_t, Vector2_t2243707579  >::Invoke(0 /* System.Int32 System.IComparable`1<UnityEngine.Vector2>::CompareTo(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (Vector2_t2243707579 )L_9);
		return L_10;
	}

IL_004d:
	{
		Vector2_t2243707579  L_11 = ___x0;
		Vector2_t2243707579  L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_12);
		if (!((Il2CppObject *)IsInst(L_13, IComparable_t1857082765_il2cpp_TypeInfo_var)))
		{
			goto IL_0074;
		}
	}
	{
		Vector2_t2243707579  L_14 = ___x0;
		Vector2_t2243707579  L_15 = L_14;
		Il2CppObject * L_16 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_15);
		Vector2_t2243707579  L_17 = ___y1;
		Vector2_t2243707579  L_18 = L_17;
		Il2CppObject * L_19 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_18);
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)));
		int32_t L_20 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.IComparable::CompareTo(System.Object) */, IComparable_t1857082765_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)), (Il2CppObject *)L_19);
		return L_20;
	}

IL_0074:
	{
		ArgumentException_t3259014390 * L_21 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_21, (String_t*)_stringLiteral1864089402, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}
}
// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Vector3>::.ctor()
extern "C"  void DefaultComparer__ctor_m1454979065_gshared (DefaultComparer_t732977813 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t1133716699 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t1133716699 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t1133716699 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Vector3>::Compare(T,T)
extern Il2CppClass* IComparable_t1857082765_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1864089402;
extern const uint32_t DefaultComparer_Compare_m2469517726_MetadataUsageId;
extern "C"  int32_t DefaultComparer_Compare_m2469517726_gshared (DefaultComparer_t732977813 * __this, Vector3_t2243707580  ___x0, Vector3_t2243707580  ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultComparer_Compare_m2469517726_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		goto IL_001e;
	}
	{
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		Vector3_t2243707580  L_3 = ___x0;
		Vector3_t2243707580  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		if (!((Il2CppObject*)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))
		{
			goto IL_004d;
		}
	}
	{
		Vector3_t2243707580  L_6 = ___x0;
		Vector3_t2243707580  L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_7);
		Vector3_t2243707580  L_9 = ___y1;
		NullCheck((Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		int32_t L_10 = InterfaceFuncInvoker1< int32_t, Vector3_t2243707580  >::Invoke(0 /* System.Int32 System.IComparable`1<UnityEngine.Vector3>::CompareTo(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (Vector3_t2243707580 )L_9);
		return L_10;
	}

IL_004d:
	{
		Vector3_t2243707580  L_11 = ___x0;
		Vector3_t2243707580  L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_12);
		if (!((Il2CppObject *)IsInst(L_13, IComparable_t1857082765_il2cpp_TypeInfo_var)))
		{
			goto IL_0074;
		}
	}
	{
		Vector3_t2243707580  L_14 = ___x0;
		Vector3_t2243707580  L_15 = L_14;
		Il2CppObject * L_16 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_15);
		Vector3_t2243707580  L_17 = ___y1;
		Vector3_t2243707580  L_18 = L_17;
		Il2CppObject * L_19 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_18);
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)));
		int32_t L_20 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.IComparable::CompareTo(System.Object) */, IComparable_t1857082765_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)), (Il2CppObject *)L_19);
		return L_20;
	}

IL_0074:
	{
		ArgumentException_t3259014390 * L_21 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_21, (String_t*)_stringLiteral1864089402, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}
}
// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Vector4>::.ctor()
extern "C"  void DefaultComparer__ctor_m3680166634_gshared (DefaultComparer_t732977814 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t1133716700 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t1133716700 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t1133716700 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Vector4>::Compare(T,T)
extern Il2CppClass* IComparable_t1857082765_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1864089402;
extern const uint32_t DefaultComparer_Compare_m4039941311_MetadataUsageId;
extern "C"  int32_t DefaultComparer_Compare_m4039941311_gshared (DefaultComparer_t732977814 * __this, Vector4_t2243707581  ___x0, Vector4_t2243707581  ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultComparer_Compare_m4039941311_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		goto IL_001e;
	}
	{
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		Vector4_t2243707581  L_3 = ___x0;
		Vector4_t2243707581  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		if (!((Il2CppObject*)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))
		{
			goto IL_004d;
		}
	}
	{
		Vector4_t2243707581  L_6 = ___x0;
		Vector4_t2243707581  L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_7);
		Vector4_t2243707581  L_9 = ___y1;
		NullCheck((Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		int32_t L_10 = InterfaceFuncInvoker1< int32_t, Vector4_t2243707581  >::Invoke(0 /* System.Int32 System.IComparable`1<UnityEngine.Vector4>::CompareTo(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (Vector4_t2243707581 )L_9);
		return L_10;
	}

IL_004d:
	{
		Vector4_t2243707581  L_11 = ___x0;
		Vector4_t2243707581  L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_12);
		if (!((Il2CppObject *)IsInst(L_13, IComparable_t1857082765_il2cpp_TypeInfo_var)))
		{
			goto IL_0074;
		}
	}
	{
		Vector4_t2243707581  L_14 = ___x0;
		Vector4_t2243707581  L_15 = L_14;
		Il2CppObject * L_16 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_15);
		Vector4_t2243707581  L_17 = ___y1;
		Vector4_t2243707581  L_18 = L_17;
		Il2CppObject * L_19 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_18);
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)));
		int32_t L_20 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.IComparable::CompareTo(System.Object) */, IComparable_t1857082765_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)), (Il2CppObject *)L_19);
		return L_20;
	}

IL_0074:
	{
		ArgumentException_t3259014390 * L_21 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_21, (String_t*)_stringLiteral1864089402, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}
}
// System.Void System.Collections.Generic.Comparer`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::.ctor()
extern "C"  void Comparer_1__ctor_m3869257906_gshared (Comparer_1_t3962746090 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Comparer`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::.cctor()
extern const Il2CppType* GenericComparer_1_t1787398723_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1__cctor_m2056558575_MetadataUsageId;
extern "C"  void Comparer_1__cctor_m2056558575_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1__cctor_m2056558575_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(41 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericComparer_1_t1787398723_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_4 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(86 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1664964607*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m383294261(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((Comparer_1_t3962746090_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((Comparer_1_t3962746090 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t3562007204 * L_8 = (DefaultComparer_t3562007204 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t3562007204 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((Comparer_1_t3962746090_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1_System_Collections_IComparer_Compare_m3842555519_MetadataUsageId;
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m3842555519_gshared (Comparer_1_t3962746090 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1_System_Collections_IComparer_Compare_m3842555519_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_1 = ___y1;
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0013;
	}

IL_0012:
	{
		G_B4_0 = (-1);
	}

IL_0013:
	{
		return G_B4_0;
	}

IL_0014:
	{
		Il2CppObject * L_2 = ___y1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		return 1;
	}

IL_001c:
	{
		Il2CppObject * L_3 = ___x0;
		if (!((Il2CppObject *)IsInst(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_4 = ___y1;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_5 = ___x0;
		Il2CppObject * L_6 = ___y1;
		NullCheck((Comparer_1_t3962746090 *)__this);
		int32_t L_7 = VirtFuncInvoker2< int32_t, KeyInfo_t777769675 , KeyInfo_t777769675  >::Invoke(6 /* System.Int32 System.Collections.Generic.Comparer`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::Compare(T,T) */, (Comparer_1_t3962746090 *)__this, (KeyInfo_t777769675 )((*(KeyInfo_t777769675 *)((KeyInfo_t777769675 *)UnBox (L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (KeyInfo_t777769675 )((*(KeyInfo_t777769675 *)((KeyInfo_t777769675 *)UnBox (L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_7;
	}

IL_0045:
	{
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2105824819(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}
}
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::get_Default()
extern "C"  Comparer_1_t3962746090 * Comparer_1_get_Default_m3762835410_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Comparer_1_t3962746090 * L_0 = ((Comparer_1_t3962746090_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.Comparer`1<System.Byte>::.ctor()
extern "C"  void Comparer_1__ctor_m4256167488_gshared (Comparer_1_t2573113555 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Comparer`1<System.Byte>::.cctor()
extern const Il2CppType* GenericComparer_1_t1787398723_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1__cctor_m2347159423_MetadataUsageId;
extern "C"  void Comparer_1__cctor_m2347159423_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1__cctor_m2347159423_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(41 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericComparer_1_t1787398723_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_4 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(86 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1664964607*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m383294261(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((Comparer_1_t2573113555_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((Comparer_1_t2573113555 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t2172374669 * L_8 = (DefaultComparer_t2172374669 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t2172374669 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((Comparer_1_t2573113555_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1<System.Byte>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1_System_Collections_IComparer_Compare_m2027162031_MetadataUsageId;
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m2027162031_gshared (Comparer_1_t2573113555 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1_System_Collections_IComparer_Compare_m2027162031_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_1 = ___y1;
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0013;
	}

IL_0012:
	{
		G_B4_0 = (-1);
	}

IL_0013:
	{
		return G_B4_0;
	}

IL_0014:
	{
		Il2CppObject * L_2 = ___y1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		return 1;
	}

IL_001c:
	{
		Il2CppObject * L_3 = ___x0;
		if (!((Il2CppObject *)IsInst(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_4 = ___y1;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_5 = ___x0;
		Il2CppObject * L_6 = ___y1;
		NullCheck((Comparer_1_t2573113555 *)__this);
		int32_t L_7 = VirtFuncInvoker2< int32_t, uint8_t, uint8_t >::Invoke(6 /* System.Int32 System.Collections.Generic.Comparer`1<System.Byte>::Compare(T,T) */, (Comparer_1_t2573113555 *)__this, (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_7;
	}

IL_0045:
	{
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2105824819(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}
}
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<System.Byte>::get_Default()
extern "C"  Comparer_1_t2573113555 * Comparer_1_get_Default_m2982649620_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Comparer_1_t2573113555 * L_0 = ((Comparer_1_t2573113555_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.Comparer`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::.ctor()
extern "C"  void Comparer_1__ctor_m3360629391_gshared (Comparer_1_t2639596567 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Comparer`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::.cctor()
extern const Il2CppType* GenericComparer_1_t1787398723_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1__cctor_m3815490192_MetadataUsageId;
extern "C"  void Comparer_1__cctor_m3815490192_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1__cctor_m3815490192_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(41 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericComparer_1_t1787398723_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_4 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(86 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1664964607*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m383294261(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((Comparer_1_t2639596567_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((Comparer_1_t2639596567 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t2238857681 * L_8 = (DefaultComparer_t2238857681 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t2238857681 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((Comparer_1_t2639596567_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1_System_Collections_IComparer_Compare_m831820030_MetadataUsageId;
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m831820030_gshared (Comparer_1_t2639596567 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1_System_Collections_IComparer_Compare_m831820030_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_1 = ___y1;
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0013;
	}

IL_0012:
	{
		G_B4_0 = (-1);
	}

IL_0013:
	{
		return G_B4_0;
	}

IL_0014:
	{
		Il2CppObject * L_2 = ___y1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		return 1;
	}

IL_001c:
	{
		Il2CppObject * L_3 = ___x0;
		if (!((Il2CppObject *)IsInst(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_4 = ___y1;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_5 = ___x0;
		Il2CppObject * L_6 = ___y1;
		NullCheck((Comparer_1_t2639596567 *)__this);
		int32_t L_7 = VirtFuncInvoker2< int32_t, KeyValuePair_2_t3749587448 , KeyValuePair_2_t3749587448  >::Invoke(6 /* System.Int32 System.Collections.Generic.Comparer`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::Compare(T,T) */, (Comparer_1_t2639596567 *)__this, (KeyValuePair_2_t3749587448 )((*(KeyValuePair_2_t3749587448 *)((KeyValuePair_2_t3749587448 *)UnBox (L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (KeyValuePair_2_t3749587448 )((*(KeyValuePair_2_t3749587448 *)((KeyValuePair_2_t3749587448 *)UnBox (L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_7;
	}

IL_0045:
	{
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2105824819(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}
}
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::get_Default()
extern "C"  Comparer_1_t2639596567 * Comparer_1_get_Default_m713673911_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Comparer_1_t2639596567 * L_0 = ((Comparer_1_t2639596567_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.Comparer`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor()
extern "C"  void Comparer_1__ctor_m3401410816_gshared (Comparer_1_t3223831060 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Comparer`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.cctor()
extern const Il2CppType* GenericComparer_1_t1787398723_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1__cctor_m3449696399_MetadataUsageId;
extern "C"  void Comparer_1__cctor_m3449696399_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1__cctor_m3449696399_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(41 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericComparer_1_t1787398723_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_4 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(86 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1664964607*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m383294261(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((Comparer_1_t3223831060_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((Comparer_1_t3223831060 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t2823092174 * L_8 = (DefaultComparer_t2823092174 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t2823092174 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((Comparer_1_t3223831060_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1_System_Collections_IComparer_Compare_m4286853351_MetadataUsageId;
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m4286853351_gshared (Comparer_1_t3223831060 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1_System_Collections_IComparer_Compare_m4286853351_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_1 = ___y1;
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0013;
	}

IL_0012:
	{
		G_B4_0 = (-1);
	}

IL_0013:
	{
		return G_B4_0;
	}

IL_0014:
	{
		Il2CppObject * L_2 = ___y1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		return 1;
	}

IL_001c:
	{
		Il2CppObject * L_3 = ___x0;
		if (!((Il2CppObject *)IsInst(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_4 = ___y1;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_5 = ___x0;
		Il2CppObject * L_6 = ___y1;
		NullCheck((Comparer_1_t3223831060 *)__this);
		int32_t L_7 = VirtFuncInvoker2< int32_t, KeyValuePair_2_t38854645 , KeyValuePair_2_t38854645  >::Invoke(6 /* System.Int32 System.Collections.Generic.Comparer`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Compare(T,T) */, (Comparer_1_t3223831060 *)__this, (KeyValuePair_2_t38854645 )((*(KeyValuePair_2_t38854645 *)((KeyValuePair_2_t38854645 *)UnBox (L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (KeyValuePair_2_t38854645 )((*(KeyValuePair_2_t38854645 *)((KeyValuePair_2_t38854645 *)UnBox (L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_7;
	}

IL_0045:
	{
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2105824819(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}
}
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Default()
extern "C"  Comparer_1_t3223831060 * Comparer_1_get_Default_m4153723304_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Comparer_1_t3223831060 * L_0 = ((Comparer_1_t3223831060_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.Comparer`1<System.DateTime>::.ctor()
extern "C"  void Comparer_1__ctor_m1202126643_gshared (Comparer_1_t3878182084 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Comparer`1<System.DateTime>::.cctor()
extern const Il2CppType* GenericComparer_1_t1787398723_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1__cctor_m1367179810_MetadataUsageId;
extern "C"  void Comparer_1__cctor_m1367179810_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1__cctor_m1367179810_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(41 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericComparer_1_t1787398723_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_4 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(86 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1664964607*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m383294261(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((Comparer_1_t3878182084_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((Comparer_1_t3878182084 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t3477443198 * L_8 = (DefaultComparer_t3477443198 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t3477443198 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((Comparer_1_t3878182084_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1<System.DateTime>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1_System_Collections_IComparer_Compare_m1712675620_MetadataUsageId;
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m1712675620_gshared (Comparer_1_t3878182084 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1_System_Collections_IComparer_Compare_m1712675620_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_1 = ___y1;
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0013;
	}

IL_0012:
	{
		G_B4_0 = (-1);
	}

IL_0013:
	{
		return G_B4_0;
	}

IL_0014:
	{
		Il2CppObject * L_2 = ___y1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		return 1;
	}

IL_001c:
	{
		Il2CppObject * L_3 = ___x0;
		if (!((Il2CppObject *)IsInst(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_4 = ___y1;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_5 = ___x0;
		Il2CppObject * L_6 = ___y1;
		NullCheck((Comparer_1_t3878182084 *)__this);
		int32_t L_7 = VirtFuncInvoker2< int32_t, DateTime_t693205669 , DateTime_t693205669  >::Invoke(6 /* System.Int32 System.Collections.Generic.Comparer`1<System.DateTime>::Compare(T,T) */, (Comparer_1_t3878182084 *)__this, (DateTime_t693205669 )((*(DateTime_t693205669 *)((DateTime_t693205669 *)UnBox (L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (DateTime_t693205669 )((*(DateTime_t693205669 *)((DateTime_t693205669 *)UnBox (L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_7;
	}

IL_0045:
	{
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2105824819(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}
}
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<System.DateTime>::get_Default()
extern "C"  Comparer_1_t3878182084 * Comparer_1_get_Default_m3737432123_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Comparer_1_t3878182084 * L_0 = ((Comparer_1_t3878182084_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.Comparer`1<System.DateTimeOffset>::.ctor()
extern "C"  void Comparer_1__ctor_m3855093372_gshared (Comparer_1_t252998025 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Comparer`1<System.DateTimeOffset>::.cctor()
extern const Il2CppType* GenericComparer_1_t1787398723_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1__cctor_m2809342737_MetadataUsageId;
extern "C"  void Comparer_1__cctor_m2809342737_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1__cctor_m2809342737_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(41 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericComparer_1_t1787398723_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_4 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(86 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1664964607*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m383294261(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((Comparer_1_t252998025_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((Comparer_1_t252998025 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t4147226435 * L_8 = (DefaultComparer_t4147226435 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t4147226435 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((Comparer_1_t252998025_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1<System.DateTimeOffset>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1_System_Collections_IComparer_Compare_m1790257529_MetadataUsageId;
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m1790257529_gshared (Comparer_1_t252998025 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1_System_Collections_IComparer_Compare_m1790257529_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_1 = ___y1;
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0013;
	}

IL_0012:
	{
		G_B4_0 = (-1);
	}

IL_0013:
	{
		return G_B4_0;
	}

IL_0014:
	{
		Il2CppObject * L_2 = ___y1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		return 1;
	}

IL_001c:
	{
		Il2CppObject * L_3 = ___x0;
		if (!((Il2CppObject *)IsInst(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_4 = ___y1;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_5 = ___x0;
		Il2CppObject * L_6 = ___y1;
		NullCheck((Comparer_1_t252998025 *)__this);
		int32_t L_7 = VirtFuncInvoker2< int32_t, DateTimeOffset_t1362988906 , DateTimeOffset_t1362988906  >::Invoke(6 /* System.Int32 System.Collections.Generic.Comparer`1<System.DateTimeOffset>::Compare(T,T) */, (Comparer_1_t252998025 *)__this, (DateTimeOffset_t1362988906 )((*(DateTimeOffset_t1362988906 *)((DateTimeOffset_t1362988906 *)UnBox (L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (DateTimeOffset_t1362988906 )((*(DateTimeOffset_t1362988906 *)((DateTimeOffset_t1362988906 *)UnBox (L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_7;
	}

IL_0045:
	{
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2105824819(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}
}
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<System.DateTimeOffset>::get_Default()
extern "C"  Comparer_1_t252998025 * Comparer_1_get_Default_m1766380520_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Comparer_1_t252998025 * L_0 = ((Comparer_1_t252998025_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.Comparer`1<System.Guid>::.ctor()
extern "C"  void Comparer_1__ctor_m2876014041_gshared (Comparer_1_t1423610712 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Comparer`1<System.Guid>::.cctor()
extern const Il2CppType* GenericComparer_1_t1787398723_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1__cctor_m3801958574_MetadataUsageId;
extern "C"  void Comparer_1__cctor_m3801958574_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1__cctor_m3801958574_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(41 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericComparer_1_t1787398723_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_4 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(86 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1664964607*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m383294261(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((Comparer_1_t1423610712_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((Comparer_1_t1423610712 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t1022871826 * L_8 = (DefaultComparer_t1022871826 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t1022871826 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((Comparer_1_t1423610712_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1<System.Guid>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1_System_Collections_IComparer_Compare_m674728644_MetadataUsageId;
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m674728644_gshared (Comparer_1_t1423610712 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1_System_Collections_IComparer_Compare_m674728644_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_1 = ___y1;
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0013;
	}

IL_0012:
	{
		G_B4_0 = (-1);
	}

IL_0013:
	{
		return G_B4_0;
	}

IL_0014:
	{
		Il2CppObject * L_2 = ___y1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		return 1;
	}

IL_001c:
	{
		Il2CppObject * L_3 = ___x0;
		if (!((Il2CppObject *)IsInst(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_4 = ___y1;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_5 = ___x0;
		Il2CppObject * L_6 = ___y1;
		NullCheck((Comparer_1_t1423610712 *)__this);
		int32_t L_7 = VirtFuncInvoker2< int32_t, Guid_t2533601593 , Guid_t2533601593  >::Invoke(6 /* System.Int32 System.Collections.Generic.Comparer`1<System.Guid>::Compare(T,T) */, (Comparer_1_t1423610712 *)__this, (Guid_t2533601593 )((*(Guid_t2533601593 *)((Guid_t2533601593 *)UnBox (L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (Guid_t2533601593 )((*(Guid_t2533601593 *)((Guid_t2533601593 *)UnBox (L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_7;
	}

IL_0045:
	{
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2105824819(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}
}
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<System.Guid>::get_Default()
extern "C"  Comparer_1_t1423610712 * Comparer_1_get_Default_m3982792633_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Comparer_1_t1423610712 * L_0 = ((Comparer_1_t1423610712_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.Comparer`1<System.Int32>::.ctor()
extern "C"  void Comparer_1__ctor_m2074421588_gshared (Comparer_1_t961886567 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Comparer`1<System.Int32>::.cctor()
extern const Il2CppType* GenericComparer_1_t1787398723_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1__cctor_m2780604723_MetadataUsageId;
extern "C"  void Comparer_1__cctor_m2780604723_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1__cctor_m2780604723_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(41 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericComparer_1_t1787398723_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_4 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(86 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1664964607*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m383294261(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((Comparer_1_t961886567_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((Comparer_1_t961886567 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t561147681 * L_8 = (DefaultComparer_t561147681 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t561147681 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((Comparer_1_t961886567_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1<System.Int32>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1_System_Collections_IComparer_Compare_m3477896499_MetadataUsageId;
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m3477896499_gshared (Comparer_1_t961886567 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1_System_Collections_IComparer_Compare_m3477896499_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_1 = ___y1;
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0013;
	}

IL_0012:
	{
		G_B4_0 = (-1);
	}

IL_0013:
	{
		return G_B4_0;
	}

IL_0014:
	{
		Il2CppObject * L_2 = ___y1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		return 1;
	}

IL_001c:
	{
		Il2CppObject * L_3 = ___x0;
		if (!((Il2CppObject *)IsInst(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_4 = ___y1;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_5 = ___x0;
		Il2CppObject * L_6 = ___y1;
		NullCheck((Comparer_1_t961886567 *)__this);
		int32_t L_7 = VirtFuncInvoker2< int32_t, int32_t, int32_t >::Invoke(6 /* System.Int32 System.Collections.Generic.Comparer`1<System.Int32>::Compare(T,T) */, (Comparer_1_t961886567 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (int32_t)((*(int32_t*)((int32_t*)UnBox (L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_7;
	}

IL_0045:
	{
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2105824819(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}
}
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<System.Int32>::get_Default()
extern "C"  Comparer_1_t961886567 * Comparer_1_get_Default_m699808348_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Comparer_1_t961886567 * L_0 = ((Comparer_1_t961886567_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.Comparer`1<System.Object>::.ctor()
extern "C"  void Comparer_1__ctor_m4082958187_gshared (Comparer_1_t1579458414 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Comparer`1<System.Object>::.cctor()
extern const Il2CppType* GenericComparer_1_t1787398723_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1__cctor_m2962395036_MetadataUsageId;
extern "C"  void Comparer_1__cctor_m2962395036_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1__cctor_m2962395036_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(41 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericComparer_1_t1787398723_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_4 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(86 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1664964607*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m383294261(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((Comparer_1_t1579458414_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((Comparer_1_t1579458414 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t1178719528 * L_8 = (DefaultComparer_t1178719528 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t1178719528 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((Comparer_1_t1579458414_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1<System.Object>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1_System_Collections_IComparer_Compare_m872902762_MetadataUsageId;
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m872902762_gshared (Comparer_1_t1579458414 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1_System_Collections_IComparer_Compare_m872902762_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_1 = ___y1;
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0013;
	}

IL_0012:
	{
		G_B4_0 = (-1);
	}

IL_0013:
	{
		return G_B4_0;
	}

IL_0014:
	{
		Il2CppObject * L_2 = ___y1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		return 1;
	}

IL_001c:
	{
		Il2CppObject * L_3 = ___x0;
		if (!((Il2CppObject *)IsInst(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_4 = ___y1;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_5 = ___x0;
		Il2CppObject * L_6 = ___y1;
		NullCheck((Comparer_1_t1579458414 *)__this);
		int32_t L_7 = VirtFuncInvoker2< int32_t, Il2CppObject *, Il2CppObject * >::Invoke(6 /* System.Int32 System.Collections.Generic.Comparer`1<System.Object>::Compare(T,T) */, (Comparer_1_t1579458414 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))), (Il2CppObject *)((Il2CppObject *)Castclass(L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))));
		return L_7;
	}

IL_0045:
	{
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2105824819(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}
}
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<System.Object>::get_Default()
extern "C"  Comparer_1_t1579458414 * Comparer_1_get_Default_m40106963_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Comparer_1_t1579458414 * L_0 = ((Comparer_1_t1579458414_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.Comparer`1<System.Reflection.CustomAttributeNamedArgument>::.ctor()
extern "C"  void Comparer_1__ctor_m844571340_gshared (Comparer_1_t3279133958 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Comparer`1<System.Reflection.CustomAttributeNamedArgument>::.cctor()
extern const Il2CppType* GenericComparer_1_t1787398723_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1__cctor_m3112251759_MetadataUsageId;
extern "C"  void Comparer_1__cctor_m3112251759_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1__cctor_m3112251759_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(41 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericComparer_1_t1787398723_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_4 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(86 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1664964607*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m383294261(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((Comparer_1_t3279133958_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((Comparer_1_t3279133958 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t2878395072 * L_8 = (DefaultComparer_t2878395072 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t2878395072 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((Comparer_1_t3279133958_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1_System_Collections_IComparer_Compare_m3203078743_MetadataUsageId;
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m3203078743_gshared (Comparer_1_t3279133958 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1_System_Collections_IComparer_Compare_m3203078743_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_1 = ___y1;
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0013;
	}

IL_0012:
	{
		G_B4_0 = (-1);
	}

IL_0013:
	{
		return G_B4_0;
	}

IL_0014:
	{
		Il2CppObject * L_2 = ___y1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		return 1;
	}

IL_001c:
	{
		Il2CppObject * L_3 = ___x0;
		if (!((Il2CppObject *)IsInst(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_4 = ___y1;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_5 = ___x0;
		Il2CppObject * L_6 = ___y1;
		NullCheck((Comparer_1_t3279133958 *)__this);
		int32_t L_7 = VirtFuncInvoker2< int32_t, CustomAttributeNamedArgument_t94157543 , CustomAttributeNamedArgument_t94157543  >::Invoke(6 /* System.Int32 System.Collections.Generic.Comparer`1<System.Reflection.CustomAttributeNamedArgument>::Compare(T,T) */, (Comparer_1_t3279133958 *)__this, (CustomAttributeNamedArgument_t94157543 )((*(CustomAttributeNamedArgument_t94157543 *)((CustomAttributeNamedArgument_t94157543 *)UnBox (L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (CustomAttributeNamedArgument_t94157543 )((*(CustomAttributeNamedArgument_t94157543 *)((CustomAttributeNamedArgument_t94157543 *)UnBox (L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_7;
	}

IL_0045:
	{
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2105824819(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}
}
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<System.Reflection.CustomAttributeNamedArgument>::get_Default()
extern "C"  Comparer_1_t3279133958 * Comparer_1_get_Default_m2605397692_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Comparer_1_t3279133958 * L_0 = ((Comparer_1_t3279133958_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.Comparer`1<System.Reflection.CustomAttributeTypedArgument>::.ctor()
extern "C"  void Comparer_1__ctor_m2364183619_gshared (Comparer_1_t388207033 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Comparer`1<System.Reflection.CustomAttributeTypedArgument>::.cctor()
extern const Il2CppType* GenericComparer_1_t1787398723_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1__cctor_m580294992_MetadataUsageId;
extern "C"  void Comparer_1__cctor_m580294992_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1__cctor_m580294992_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(41 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericComparer_1_t1787398723_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_4 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(86 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1664964607*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m383294261(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((Comparer_1_t388207033_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((Comparer_1_t388207033 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t4282435443 * L_8 = (DefaultComparer_t4282435443 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t4282435443 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((Comparer_1_t388207033_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1_System_Collections_IComparer_Compare_m1635186002_MetadataUsageId;
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m1635186002_gshared (Comparer_1_t388207033 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1_System_Collections_IComparer_Compare_m1635186002_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_1 = ___y1;
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0013;
	}

IL_0012:
	{
		G_B4_0 = (-1);
	}

IL_0013:
	{
		return G_B4_0;
	}

IL_0014:
	{
		Il2CppObject * L_2 = ___y1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		return 1;
	}

IL_001c:
	{
		Il2CppObject * L_3 = ___x0;
		if (!((Il2CppObject *)IsInst(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_4 = ___y1;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_5 = ___x0;
		Il2CppObject * L_6 = ___y1;
		NullCheck((Comparer_1_t388207033 *)__this);
		int32_t L_7 = VirtFuncInvoker2< int32_t, CustomAttributeTypedArgument_t1498197914 , CustomAttributeTypedArgument_t1498197914  >::Invoke(6 /* System.Int32 System.Collections.Generic.Comparer`1<System.Reflection.CustomAttributeTypedArgument>::Compare(T,T) */, (Comparer_1_t388207033 *)__this, (CustomAttributeTypedArgument_t1498197914 )((*(CustomAttributeTypedArgument_t1498197914 *)((CustomAttributeTypedArgument_t1498197914 *)UnBox (L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (CustomAttributeTypedArgument_t1498197914 )((*(CustomAttributeTypedArgument_t1498197914 *)((CustomAttributeTypedArgument_t1498197914 *)UnBox (L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_7;
	}

IL_0045:
	{
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2105824819(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}
}
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<System.Reflection.CustomAttributeTypedArgument>::get_Default()
extern "C"  Comparer_1_t388207033 * Comparer_1_get_Default_m3643271627_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Comparer_1_t388207033 * L_0 = ((Comparer_1_t388207033_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.Comparer`1<System.TimeSpan>::.ctor()
extern "C"  void Comparer_1__ctor_m2195903267_gshared (Comparer_1_t2320268068 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Comparer`1<System.TimeSpan>::.cctor()
extern const Il2CppType* GenericComparer_1_t1787398723_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1__cctor_m2494715342_MetadataUsageId;
extern "C"  void Comparer_1__cctor_m2494715342_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1__cctor_m2494715342_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(41 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericComparer_1_t1787398723_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_4 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(86 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1664964607*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m383294261(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((Comparer_1_t2320268068_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((Comparer_1_t2320268068 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t1919529182 * L_8 = (DefaultComparer_t1919529182 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t1919529182 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((Comparer_1_t2320268068_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1<System.TimeSpan>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1_System_Collections_IComparer_Compare_m2490067344_MetadataUsageId;
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m2490067344_gshared (Comparer_1_t2320268068 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1_System_Collections_IComparer_Compare_m2490067344_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_1 = ___y1;
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0013;
	}

IL_0012:
	{
		G_B4_0 = (-1);
	}

IL_0013:
	{
		return G_B4_0;
	}

IL_0014:
	{
		Il2CppObject * L_2 = ___y1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		return 1;
	}

IL_001c:
	{
		Il2CppObject * L_3 = ___x0;
		if (!((Il2CppObject *)IsInst(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_4 = ___y1;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_5 = ___x0;
		Il2CppObject * L_6 = ___y1;
		NullCheck((Comparer_1_t2320268068 *)__this);
		int32_t L_7 = VirtFuncInvoker2< int32_t, TimeSpan_t3430258949 , TimeSpan_t3430258949  >::Invoke(6 /* System.Int32 System.Collections.Generic.Comparer`1<System.TimeSpan>::Compare(T,T) */, (Comparer_1_t2320268068 *)__this, (TimeSpan_t3430258949 )((*(TimeSpan_t3430258949 *)((TimeSpan_t3430258949 *)UnBox (L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (TimeSpan_t3430258949 )((*(TimeSpan_t3430258949 *)((TimeSpan_t3430258949 *)UnBox (L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_7;
	}

IL_0045:
	{
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2105824819(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}
}
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<System.TimeSpan>::get_Default()
extern "C"  Comparer_1_t2320268068 * Comparer_1_get_Default_m2204997355_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Comparer_1_t2320268068 * L_0 = ((Comparer_1_t2320268068_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.Color32>::.ctor()
extern "C"  void Comparer_1__ctor_m2264852056_gshared (Comparer_1_t4059493933 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.Color32>::.cctor()
extern const Il2CppType* GenericComparer_1_t1787398723_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1__cctor_m179359609_MetadataUsageId;
extern "C"  void Comparer_1__cctor_m179359609_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1__cctor_m179359609_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(41 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericComparer_1_t1787398723_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_4 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(86 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1664964607*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m383294261(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((Comparer_1_t4059493933_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((Comparer_1_t4059493933 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t3658755047 * L_8 = (DefaultComparer_t3658755047 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t3658755047 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((Comparer_1_t4059493933_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.Color32>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1_System_Collections_IComparer_Compare_m2785607073_MetadataUsageId;
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m2785607073_gshared (Comparer_1_t4059493933 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1_System_Collections_IComparer_Compare_m2785607073_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_1 = ___y1;
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0013;
	}

IL_0012:
	{
		G_B4_0 = (-1);
	}

IL_0013:
	{
		return G_B4_0;
	}

IL_0014:
	{
		Il2CppObject * L_2 = ___y1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		return 1;
	}

IL_001c:
	{
		Il2CppObject * L_3 = ___x0;
		if (!((Il2CppObject *)IsInst(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_4 = ___y1;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_5 = ___x0;
		Il2CppObject * L_6 = ___y1;
		NullCheck((Comparer_1_t4059493933 *)__this);
		int32_t L_7 = VirtFuncInvoker2< int32_t, Color32_t874517518 , Color32_t874517518  >::Invoke(6 /* System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.Color32>::Compare(T,T) */, (Comparer_1_t4059493933 *)__this, (Color32_t874517518 )((*(Color32_t874517518 *)((Color32_t874517518 *)UnBox (L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (Color32_t874517518 )((*(Color32_t874517518 *)((Color32_t874517518 *)UnBox (L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_7;
	}

IL_0045:
	{
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2105824819(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}
}
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.Color32>::get_Default()
extern "C"  Comparer_1_t4059493933 * Comparer_1_get_Default_m1826646524_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Comparer_1_t4059493933 * L_0 = ((Comparer_1_t4059493933_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.EventSystems.RaycastResult>::.ctor()
extern "C"  void Comparer_1__ctor_m1728777074_gshared (Comparer_1_t3206162791 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.EventSystems.RaycastResult>::.cctor()
extern const Il2CppType* GenericComparer_1_t1787398723_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1__cctor_m3237813171_MetadataUsageId;
extern "C"  void Comparer_1__cctor_m3237813171_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1__cctor_m3237813171_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(41 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericComparer_1_t1787398723_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_4 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(86 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1664964607*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m383294261(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((Comparer_1_t3206162791_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((Comparer_1_t3206162791 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t2805423905 * L_8 = (DefaultComparer_t2805423905 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t2805423905 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((Comparer_1_t3206162791_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1_System_Collections_IComparer_Compare_m1153499515_MetadataUsageId;
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m1153499515_gshared (Comparer_1_t3206162791 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1_System_Collections_IComparer_Compare_m1153499515_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_1 = ___y1;
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0013;
	}

IL_0012:
	{
		G_B4_0 = (-1);
	}

IL_0013:
	{
		return G_B4_0;
	}

IL_0014:
	{
		Il2CppObject * L_2 = ___y1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		return 1;
	}

IL_001c:
	{
		Il2CppObject * L_3 = ___x0;
		if (!((Il2CppObject *)IsInst(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_4 = ___y1;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_5 = ___x0;
		Il2CppObject * L_6 = ___y1;
		NullCheck((Comparer_1_t3206162791 *)__this);
		int32_t L_7 = VirtFuncInvoker2< int32_t, RaycastResult_t21186376 , RaycastResult_t21186376  >::Invoke(6 /* System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.EventSystems.RaycastResult>::Compare(T,T) */, (Comparer_1_t3206162791 *)__this, (RaycastResult_t21186376 )((*(RaycastResult_t21186376 *)((RaycastResult_t21186376 *)UnBox (L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (RaycastResult_t21186376 )((*(RaycastResult_t21186376 *)((RaycastResult_t21186376 *)UnBox (L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_7;
	}

IL_0045:
	{
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2105824819(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}
}
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.EventSystems.RaycastResult>::get_Default()
extern "C"  Comparer_1_t3206162791 * Comparer_1_get_Default_m4282764954_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Comparer_1_t3206162791 * L_0 = ((Comparer_1_t3206162791_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.Experimental.Director.Playable>::.ctor()
extern "C"  void Comparer_1__ctor_m105573688_gshared (Comparer_1_t2557554667 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.Experimental.Director.Playable>::.cctor()
extern const Il2CppType* GenericComparer_1_t1787398723_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1__cctor_m1146589207_MetadataUsageId;
extern "C"  void Comparer_1__cctor_m1146589207_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1__cctor_m1146589207_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(41 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericComparer_1_t1787398723_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_4 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(86 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1664964607*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m383294261(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((Comparer_1_t2557554667_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((Comparer_1_t2557554667 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t2156815781 * L_8 = (DefaultComparer_t2156815781 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t2156815781 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((Comparer_1_t2557554667_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.Experimental.Director.Playable>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1_System_Collections_IComparer_Compare_m4240302767_MetadataUsageId;
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m4240302767_gshared (Comparer_1_t2557554667 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1_System_Collections_IComparer_Compare_m4240302767_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_1 = ___y1;
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0013;
	}

IL_0012:
	{
		G_B4_0 = (-1);
	}

IL_0013:
	{
		return G_B4_0;
	}

IL_0014:
	{
		Il2CppObject * L_2 = ___y1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		return 1;
	}

IL_001c:
	{
		Il2CppObject * L_3 = ___x0;
		if (!((Il2CppObject *)IsInst(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_4 = ___y1;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_5 = ___x0;
		Il2CppObject * L_6 = ___y1;
		NullCheck((Comparer_1_t2557554667 *)__this);
		int32_t L_7 = VirtFuncInvoker2< int32_t, Playable_t3667545548 , Playable_t3667545548  >::Invoke(6 /* System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.Experimental.Director.Playable>::Compare(T,T) */, (Comparer_1_t2557554667 *)__this, (Playable_t3667545548 )((*(Playable_t3667545548 *)((Playable_t3667545548 *)UnBox (L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (Playable_t3667545548 )((*(Playable_t3667545548 *)((Playable_t3667545548 *)UnBox (L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_7;
	}

IL_0045:
	{
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2105824819(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}
}
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.Experimental.Director.Playable>::get_Default()
extern "C"  Comparer_1_t2557554667 * Comparer_1_get_Default_m1078895976_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Comparer_1_t2557554667 * L_0 = ((Comparer_1_t2557554667_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.UICharInfo>::.ctor()
extern "C"  void Comparer_1__ctor_m1184061702_gshared (Comparer_1_t1946645919 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.UICharInfo>::.cctor()
extern const Il2CppType* GenericComparer_1_t1787398723_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1__cctor_m3069041651_MetadataUsageId;
extern "C"  void Comparer_1__cctor_m3069041651_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1__cctor_m3069041651_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(41 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericComparer_1_t1787398723_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_4 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(86 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1664964607*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m383294261(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((Comparer_1_t1946645919_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((Comparer_1_t1946645919 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t1545907033 * L_8 = (DefaultComparer_t1545907033 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t1545907033 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((Comparer_1_t1946645919_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.UICharInfo>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1_System_Collections_IComparer_Compare_m1621919467_MetadataUsageId;
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m1621919467_gshared (Comparer_1_t1946645919 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1_System_Collections_IComparer_Compare_m1621919467_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_1 = ___y1;
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0013;
	}

IL_0012:
	{
		G_B4_0 = (-1);
	}

IL_0013:
	{
		return G_B4_0;
	}

IL_0014:
	{
		Il2CppObject * L_2 = ___y1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		return 1;
	}

IL_001c:
	{
		Il2CppObject * L_3 = ___x0;
		if (!((Il2CppObject *)IsInst(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_4 = ___y1;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_5 = ___x0;
		Il2CppObject * L_6 = ___y1;
		NullCheck((Comparer_1_t1946645919 *)__this);
		int32_t L_7 = VirtFuncInvoker2< int32_t, UICharInfo_t3056636800 , UICharInfo_t3056636800  >::Invoke(6 /* System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.UICharInfo>::Compare(T,T) */, (Comparer_1_t1946645919 *)__this, (UICharInfo_t3056636800 )((*(UICharInfo_t3056636800 *)((UICharInfo_t3056636800 *)UnBox (L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (UICharInfo_t3056636800 )((*(UICharInfo_t3056636800 *)((UICharInfo_t3056636800 *)UnBox (L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_7;
	}

IL_0045:
	{
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2105824819(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}
}
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.UICharInfo>::get_Default()
extern "C"  Comparer_1_t1946645919 * Comparer_1_get_Default_m91842798_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Comparer_1_t1946645919 * L_0 = ((Comparer_1_t1946645919_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.UILineInfo>::.ctor()
extern "C"  void Comparer_1__ctor_m806168336_gshared (Comparer_1_t2511286993 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.UILineInfo>::.cctor()
extern const Il2CppType* GenericComparer_1_t1787398723_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1__cctor_m3996541505_MetadataUsageId;
extern "C"  void Comparer_1__cctor_m3996541505_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1__cctor_m3996541505_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(41 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericComparer_1_t1787398723_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_4 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(86 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1664964607*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m383294261(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((Comparer_1_t2511286993_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((Comparer_1_t2511286993 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t2110548107 * L_8 = (DefaultComparer_t2110548107 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t2110548107 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((Comparer_1_t2511286993_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.UILineInfo>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1_System_Collections_IComparer_Compare_m2964757477_MetadataUsageId;
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m2964757477_gshared (Comparer_1_t2511286993 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1_System_Collections_IComparer_Compare_m2964757477_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_1 = ___y1;
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0013;
	}

IL_0012:
	{
		G_B4_0 = (-1);
	}

IL_0013:
	{
		return G_B4_0;
	}

IL_0014:
	{
		Il2CppObject * L_2 = ___y1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		return 1;
	}

IL_001c:
	{
		Il2CppObject * L_3 = ___x0;
		if (!((Il2CppObject *)IsInst(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_4 = ___y1;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_5 = ___x0;
		Il2CppObject * L_6 = ___y1;
		NullCheck((Comparer_1_t2511286993 *)__this);
		int32_t L_7 = VirtFuncInvoker2< int32_t, UILineInfo_t3621277874 , UILineInfo_t3621277874  >::Invoke(6 /* System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.UILineInfo>::Compare(T,T) */, (Comparer_1_t2511286993 *)__this, (UILineInfo_t3621277874 )((*(UILineInfo_t3621277874 *)((UILineInfo_t3621277874 *)UnBox (L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (UILineInfo_t3621277874 )((*(UILineInfo_t3621277874 *)((UILineInfo_t3621277874 *)UnBox (L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_7;
	}

IL_0045:
	{
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2105824819(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}
}
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.UILineInfo>::get_Default()
extern "C"  Comparer_1_t2511286993 * Comparer_1_get_Default_m501796660_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Comparer_1_t2511286993 * L_0 = ((Comparer_1_t2511286993_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.UIVertex>::.ctor()
extern "C"  void Comparer_1__ctor_m1157133632_gshared (Comparer_1_t94267937 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.UIVertex>::.cctor()
extern const Il2CppType* GenericComparer_1_t1787398723_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1__cctor_m4067993089_MetadataUsageId;
extern "C"  void Comparer_1__cctor_m4067993089_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1__cctor_m4067993089_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(41 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericComparer_1_t1787398723_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_4 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(86 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1664964607*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m383294261(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((Comparer_1_t94267937_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((Comparer_1_t94267937 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t3988496347 * L_8 = (DefaultComparer_t3988496347 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t3988496347 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((Comparer_1_t94267937_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.UIVertex>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1_System_Collections_IComparer_Compare_m2324509253_MetadataUsageId;
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m2324509253_gshared (Comparer_1_t94267937 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1_System_Collections_IComparer_Compare_m2324509253_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_1 = ___y1;
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0013;
	}

IL_0012:
	{
		G_B4_0 = (-1);
	}

IL_0013:
	{
		return G_B4_0;
	}

IL_0014:
	{
		Il2CppObject * L_2 = ___y1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		return 1;
	}

IL_001c:
	{
		Il2CppObject * L_3 = ___x0;
		if (!((Il2CppObject *)IsInst(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_4 = ___y1;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_5 = ___x0;
		Il2CppObject * L_6 = ___y1;
		NullCheck((Comparer_1_t94267937 *)__this);
		int32_t L_7 = VirtFuncInvoker2< int32_t, UIVertex_t1204258818 , UIVertex_t1204258818  >::Invoke(6 /* System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.UIVertex>::Compare(T,T) */, (Comparer_1_t94267937 *)__this, (UIVertex_t1204258818 )((*(UIVertex_t1204258818 *)((UIVertex_t1204258818 *)UnBox (L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (UIVertex_t1204258818 )((*(UIVertex_t1204258818 *)((UIVertex_t1204258818 *)UnBox (L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_7;
	}

IL_0045:
	{
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2105824819(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}
}
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.UIVertex>::get_Default()
extern "C"  Comparer_1_t94267937 * Comparer_1_get_Default_m1960140044_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Comparer_1_t94267937 * L_0 = ((Comparer_1_t94267937_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.Vector2>::.ctor()
extern "C"  void Comparer_1__ctor_m2941434245_gshared (Comparer_1_t1133716698 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.Vector2>::.cctor()
extern const Il2CppType* GenericComparer_1_t1787398723_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1__cctor_m2253684996_MetadataUsageId;
extern "C"  void Comparer_1__cctor_m2253684996_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1__cctor_m2253684996_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(41 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericComparer_1_t1787398723_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_4 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(86 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1664964607*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m383294261(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((Comparer_1_t1133716698_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((Comparer_1_t1133716698 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t732977812 * L_8 = (DefaultComparer_t732977812 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t732977812 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((Comparer_1_t1133716698_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.Vector2>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1_System_Collections_IComparer_Compare_m637596782_MetadataUsageId;
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m637596782_gshared (Comparer_1_t1133716698 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1_System_Collections_IComparer_Compare_m637596782_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_1 = ___y1;
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0013;
	}

IL_0012:
	{
		G_B4_0 = (-1);
	}

IL_0013:
	{
		return G_B4_0;
	}

IL_0014:
	{
		Il2CppObject * L_2 = ___y1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		return 1;
	}

IL_001c:
	{
		Il2CppObject * L_3 = ___x0;
		if (!((Il2CppObject *)IsInst(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_4 = ___y1;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_5 = ___x0;
		Il2CppObject * L_6 = ___y1;
		NullCheck((Comparer_1_t1133716698 *)__this);
		int32_t L_7 = VirtFuncInvoker2< int32_t, Vector2_t2243707579 , Vector2_t2243707579  >::Invoke(6 /* System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.Vector2>::Compare(T,T) */, (Comparer_1_t1133716698 *)__this, (Vector2_t2243707579 )((*(Vector2_t2243707579 *)((Vector2_t2243707579 *)UnBox (L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (Vector2_t2243707579 )((*(Vector2_t2243707579 *)((Vector2_t2243707579 *)UnBox (L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_7;
	}

IL_0045:
	{
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2105824819(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}
}
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.Vector2>::get_Default()
extern "C"  Comparer_1_t1133716698 * Comparer_1_get_Default_m492688901_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Comparer_1_t1133716698 * L_0 = ((Comparer_1_t1133716698_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.Vector3>::.ctor()
extern "C"  void Comparer_1__ctor_m1169723274_gshared (Comparer_1_t1133716699 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.Vector3>::.cctor()
extern const Il2CppType* GenericComparer_1_t1787398723_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1__cctor_m1573451391_MetadataUsageId;
extern "C"  void Comparer_1__cctor_m1573451391_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1__cctor_m1573451391_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(41 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericComparer_1_t1787398723_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_4 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(86 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1664964607*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m383294261(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((Comparer_1_t1133716699_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((Comparer_1_t1133716699 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t732977813 * L_8 = (DefaultComparer_t732977813 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t732977813 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((Comparer_1_t1133716699_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.Vector3>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1_System_Collections_IComparer_Compare_m2615431023_MetadataUsageId;
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m2615431023_gshared (Comparer_1_t1133716699 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1_System_Collections_IComparer_Compare_m2615431023_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_1 = ___y1;
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0013;
	}

IL_0012:
	{
		G_B4_0 = (-1);
	}

IL_0013:
	{
		return G_B4_0;
	}

IL_0014:
	{
		Il2CppObject * L_2 = ___y1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		return 1;
	}

IL_001c:
	{
		Il2CppObject * L_3 = ___x0;
		if (!((Il2CppObject *)IsInst(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_4 = ___y1;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_5 = ___x0;
		Il2CppObject * L_6 = ___y1;
		NullCheck((Comparer_1_t1133716699 *)__this);
		int32_t L_7 = VirtFuncInvoker2< int32_t, Vector3_t2243707580 , Vector3_t2243707580  >::Invoke(6 /* System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.Vector3>::Compare(T,T) */, (Comparer_1_t1133716699 *)__this, (Vector3_t2243707580 )((*(Vector3_t2243707580 *)((Vector3_t2243707580 *)UnBox (L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (Vector3_t2243707580 )((*(Vector3_t2243707580 *)((Vector3_t2243707580 *)UnBox (L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_7;
	}

IL_0045:
	{
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2105824819(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}
}
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.Vector3>::get_Default()
extern "C"  Comparer_1_t1133716699 * Comparer_1_get_Default_m3185432070_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Comparer_1_t1133716699 * L_0 = ((Comparer_1_t1133716699_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.Vector4>::.ctor()
extern "C"  void Comparer_1__ctor_m4052560291_gshared (Comparer_1_t1133716700 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.Vector4>::.cctor()
extern const Il2CppType* GenericComparer_1_t1787398723_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1__cctor_m1911230094_MetadataUsageId;
extern "C"  void Comparer_1__cctor_m1911230094_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1__cctor_m1911230094_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(41 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericComparer_1_t1787398723_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_4 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(86 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1664964607*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m383294261(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((Comparer_1_t1133716700_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((Comparer_1_t1133716700 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t732977814 * L_8 = (DefaultComparer_t732977814 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t732977814 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((Comparer_1_t1133716700_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.Vector4>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1_System_Collections_IComparer_Compare_m577428976_MetadataUsageId;
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m577428976_gshared (Comparer_1_t1133716700 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1_System_Collections_IComparer_Compare_m577428976_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_1 = ___y1;
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0013;
	}

IL_0012:
	{
		G_B4_0 = (-1);
	}

IL_0013:
	{
		return G_B4_0;
	}

IL_0014:
	{
		Il2CppObject * L_2 = ___y1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		return 1;
	}

IL_001c:
	{
		Il2CppObject * L_3 = ___x0;
		if (!((Il2CppObject *)IsInst(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_4 = ___y1;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_5 = ___x0;
		Il2CppObject * L_6 = ___y1;
		NullCheck((Comparer_1_t1133716700 *)__this);
		int32_t L_7 = VirtFuncInvoker2< int32_t, Vector4_t2243707581 , Vector4_t2243707581  >::Invoke(6 /* System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.Vector4>::Compare(T,T) */, (Comparer_1_t1133716700 *)__this, (Vector4_t2243707581 )((*(Vector4_t2243707581 *)((Vector4_t2243707581 *)UnBox (L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (Vector4_t2243707581 )((*(Vector4_t2243707581 *)((Vector4_t2243707581 *)UnBox (L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_7;
	}

IL_0045:
	{
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2105824819(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}
}
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.Vector4>::get_Default()
extern "C"  Comparer_1_t1133716700 * Comparer_1_get_Default_m48739979_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Comparer_1_t1133716700 * L_0 = ((Comparer_1_t1133716700_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Int64>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1188491919_gshared (Enumerator_t684213932 * __this, Dictionary_2_t3659156526 * ___dictionary0, const MethodInfo* method)
{
	{
		Dictionary_2_t3659156526 * L_0 = ___dictionary0;
		__this->set_dictionary_0(L_0);
		Dictionary_2_t3659156526 * L_1 = ___dictionary0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_generation_14();
		__this->set_stamp_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1188491919_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t3659156526 * ___dictionary0, const MethodInfo* method)
{
	Enumerator_t684213932 * _thisAdjusted = reinterpret_cast<Enumerator_t684213932 *>(__this + 1);
	Enumerator__ctor_m1188491919(_thisAdjusted, ___dictionary0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Int64>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1574127952_gshared (Enumerator_t684213932 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m2735614142((Enumerator_t684213932 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1416501748  L_0 = (KeyValuePair_2_t1416501748 )__this->get_current_3();
		KeyValuePair_2_t1416501748  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1574127952_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t684213932 * _thisAdjusted = reinterpret_cast<Enumerator_t684213932 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1574127952(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Int64>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1235301678_gshared (Enumerator_t684213932 * __this, const MethodInfo* method)
{
	{
		Enumerator_Reset_m1537962437((Enumerator_t684213932 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1235301678_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t684213932 * _thisAdjusted = reinterpret_cast<Enumerator_t684213932 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m1235301678(_thisAdjusted, method);
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Int64>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1213929309_gshared (Enumerator_t684213932 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m2735614142((Enumerator_t684213932 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1416501748 * L_0 = (KeyValuePair_2_t1416501748 *)__this->get_address_of_current_3();
		int32_t L_1 = KeyValuePair_2_get_Key_m404179876((KeyValuePair_2_t1416501748 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), &L_2);
		KeyValuePair_2_t1416501748 * L_4 = (KeyValuePair_2_t1416501748 *)__this->get_address_of_current_3();
		int64_t L_5 = KeyValuePair_2_get_Value_m3929255215((KeyValuePair_2_t1416501748 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		int64_t L_6 = L_5;
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_6);
		DictionaryEntry_t3048875398  L_8;
		memset(&L_8, 0, sizeof(L_8));
		DictionaryEntry__ctor_m2901884110(&L_8, (Il2CppObject *)L_3, (Il2CppObject *)L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1213929309_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t684213932 * _thisAdjusted = reinterpret_cast<Enumerator_t684213932 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1213929309(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Int64>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2871216018_gshared (Enumerator_t684213932 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Enumerator_get_CurrentKey_m2231206827((Enumerator_t684213932 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2871216018_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t684213932 * _thisAdjusted = reinterpret_cast<Enumerator_t684213932 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2871216018(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Int64>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m800838336_gshared (Enumerator_t684213932 * __this, const MethodInfo* method)
{
	{
		int64_t L_0 = Enumerator_get_CurrentValue_m2007594771((Enumerator_t684213932 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		int64_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m800838336_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t684213932 * _thisAdjusted = reinterpret_cast<Enumerator_t684213932 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m800838336(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Int64>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1520629361_gshared (Enumerator_t684213932 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Enumerator_VerifyState_m1404444218((Enumerator_t684213932 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		goto IL_007b;
	}

IL_0019:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		int32_t L_2 = (int32_t)L_1;
		V_1 = (int32_t)L_2;
		__this->set_next_1(((int32_t)((int32_t)L_2+(int32_t)1)));
		int32_t L_3 = V_1;
		V_0 = (int32_t)L_3;
		Dictionary_2_t3659156526 * L_4 = (Dictionary_2_t3659156526 *)__this->get_dictionary_0();
		NullCheck(L_4);
		LinkU5BU5D_t62501539* L_5 = (LinkU5BU5D_t62501539*)L_4->get_linkSlots_5();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		int32_t L_7 = (int32_t)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_7&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_007b;
		}
	}
	{
		Dictionary_2_t3659156526 * L_8 = (Dictionary_2_t3659156526 *)__this->get_dictionary_0();
		NullCheck(L_8);
		MetricU5BU5D_t2477685199* L_9 = (MetricU5BU5D_t2477685199*)L_8->get_keySlots_6();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		int32_t L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		Dictionary_2_t3659156526 * L_13 = (Dictionary_2_t3659156526 *)__this->get_dictionary_0();
		NullCheck(L_13);
		Int64U5BU5D_t717125112* L_14 = (Int64U5BU5D_t717125112*)L_13->get_valueSlots_7();
		int32_t L_15 = V_0;
		NullCheck(L_14);
		int32_t L_16 = L_15;
		int64_t L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		KeyValuePair_2_t1416501748  L_18;
		memset(&L_18, 0, sizeof(L_18));
		KeyValuePair_2__ctor_m1467949693(&L_18, (int32_t)L_12, (int64_t)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		__this->set_current_3(L_18);
		return (bool)1;
	}

IL_007b:
	{
		int32_t L_19 = (int32_t)__this->get_next_1();
		Dictionary_2_t3659156526 * L_20 = (Dictionary_2_t3659156526 *)__this->get_dictionary_0();
		NullCheck(L_20);
		int32_t L_21 = (int32_t)L_20->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_21)))
		{
			goto IL_0019;
		}
	}
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m1520629361_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t684213932 * _thisAdjusted = reinterpret_cast<Enumerator_t684213932 *>(__this + 1);
	return Enumerator_MoveNext_m1520629361(_thisAdjusted, method);
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Int64>::get_Current()
extern "C"  KeyValuePair_2_t1416501748  Enumerator_get_Current_m2197039620_gshared (Enumerator_t684213932 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t1416501748  L_0 = (KeyValuePair_2_t1416501748 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  KeyValuePair_2_t1416501748  Enumerator_get_Current_m2197039620_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t684213932 * _thisAdjusted = reinterpret_cast<Enumerator_t684213932 *>(__this + 1);
	return Enumerator_get_Current_m2197039620(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Int64>::get_CurrentKey()
extern "C"  int32_t Enumerator_get_CurrentKey_m2231206827_gshared (Enumerator_t684213932 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m2735614142((Enumerator_t684213932 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1416501748 * L_0 = (KeyValuePair_2_t1416501748 *)__this->get_address_of_current_3();
		int32_t L_1 = KeyValuePair_2_get_Key_m404179876((KeyValuePair_2_t1416501748 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
extern "C"  int32_t Enumerator_get_CurrentKey_m2231206827_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t684213932 * _thisAdjusted = reinterpret_cast<Enumerator_t684213932 *>(__this + 1);
	return Enumerator_get_CurrentKey_m2231206827(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Int64>::get_CurrentValue()
extern "C"  int64_t Enumerator_get_CurrentValue_m2007594771_gshared (Enumerator_t684213932 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m2735614142((Enumerator_t684213932 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1416501748 * L_0 = (KeyValuePair_2_t1416501748 *)__this->get_address_of_current_3();
		int64_t L_1 = KeyValuePair_2_get_Value_m3929255215((KeyValuePair_2_t1416501748 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  int64_t Enumerator_get_CurrentValue_m2007594771_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t684213932 * _thisAdjusted = reinterpret_cast<Enumerator_t684213932 *>(__this + 1);
	return Enumerator_get_CurrentValue_m2007594771(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Int64>::Reset()
extern "C"  void Enumerator_Reset_m1537962437_gshared (Enumerator_t684213932 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m1404444218((Enumerator_t684213932 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_Reset_m1537962437_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t684213932 * _thisAdjusted = reinterpret_cast<Enumerator_t684213932 *>(__this + 1);
	Enumerator_Reset_m1537962437(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Int64>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral671556834;
extern const uint32_t Enumerator_VerifyState_m1404444218_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m1404444218_gshared (Enumerator_t684213932 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m1404444218_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t3659156526 * L_0 = (Dictionary_2_t3659156526 *)__this->get_dictionary_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		Dictionary_2_t3659156526 * L_2 = (Dictionary_2_t3659156526 *)__this->get_dictionary_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_generation_14();
		int32_t L_4 = (int32_t)__this->get_stamp_2();
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t721527559 * L_5 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_5, (String_t*)_stringLiteral671556834, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m1404444218_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t684213932 * _thisAdjusted = reinterpret_cast<Enumerator_t684213932 *>(__this + 1);
	Enumerator_VerifyState_m1404444218(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Int64>::VerifyCurrent()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2382073330;
extern const uint32_t Enumerator_VerifyCurrent_m2735614142_MetadataUsageId;
extern "C"  void Enumerator_VerifyCurrent_m2735614142_gshared (Enumerator_t684213932 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyCurrent_m2735614142_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m1404444218((Enumerator_t684213932 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2382073330, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001d:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyCurrent_m2735614142_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t684213932 * _thisAdjusted = reinterpret_cast<Enumerator_t684213932 *>(__this + 1);
	Enumerator_VerifyCurrent_m2735614142(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Int64>::Dispose()
extern "C"  void Enumerator_Dispose_m2961717971_gshared (Enumerator_t684213932 * __this, const MethodInfo* method)
{
	{
		__this->set_dictionary_0((Dictionary_2_t3659156526 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2961717971_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t684213932 * _thisAdjusted = reinterpret_cast<Enumerator_t684213932 *>(__this + 1);
	Enumerator_Dispose_m2961717971(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2882573199_gshared (Enumerator_t2464585190 * __this, Dictionary_2_t1144560488 * ___dictionary0, const MethodInfo* method)
{
	{
		Dictionary_2_t1144560488 * L_0 = ___dictionary0;
		__this->set_dictionary_0(L_0);
		Dictionary_2_t1144560488 * L_1 = ___dictionary0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_generation_14();
		__this->set_stamp_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m2882573199_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t1144560488 * ___dictionary0, const MethodInfo* method)
{
	Enumerator_t2464585190 * _thisAdjusted = reinterpret_cast<Enumerator_t2464585190 *>(__this + 1);
	Enumerator__ctor_m2882573199(_thisAdjusted, ___dictionary0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2753081182_gshared (Enumerator_t2464585190 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m4158200186((Enumerator_t2464585190 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3196873006  L_0 = (KeyValuePair_2_t3196873006 )__this->get_current_3();
		KeyValuePair_2_t3196873006  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2753081182_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2464585190 * _thisAdjusted = reinterpret_cast<Enumerator_t2464585190 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2753081182(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3448018626_gshared (Enumerator_t2464585190 * __this, const MethodInfo* method)
{
	{
		Enumerator_Reset_m1867529997((Enumerator_t2464585190 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3448018626_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2464585190 * _thisAdjusted = reinterpret_cast<Enumerator_t2464585190 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m3448018626(_thisAdjusted, method);
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1386174965_gshared (Enumerator_t2464585190 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m4158200186((Enumerator_t2464585190 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3196873006 * L_0 = (KeyValuePair_2_t3196873006 *)__this->get_address_of_current_3();
		int32_t L_1 = KeyValuePair_2_get_Key_m2312610863((KeyValuePair_2_t3196873006 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), &L_2);
		KeyValuePair_2_t3196873006 * L_4 = (KeyValuePair_2_t3196873006 *)__this->get_address_of_current_3();
		Il2CppObject * L_5 = KeyValuePair_2_get_Value_m3734313679((KeyValuePair_2_t3196873006 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		DictionaryEntry_t3048875398  L_6;
		memset(&L_6, 0, sizeof(L_6));
		DictionaryEntry__ctor_m2901884110(&L_6, (Il2CppObject *)L_3, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1386174965_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2464585190 * _thisAdjusted = reinterpret_cast<Enumerator_t2464585190 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1386174965(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3637591552_gshared (Enumerator_t2464585190 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Enumerator_get_CurrentKey_m2750410819((Enumerator_t2464585190 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3637591552_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2464585190 * _thisAdjusted = reinterpret_cast<Enumerator_t2464585190 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3637591552(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1974664938_gshared (Enumerator_t2464585190 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = Enumerator_get_CurrentValue_m4218670947((Enumerator_t2464585190 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return L_0;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1974664938_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2464585190 * _thisAdjusted = reinterpret_cast<Enumerator_t2464585190 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1974664938(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3844071554_gshared (Enumerator_t2464585190 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Enumerator_VerifyState_m1404756950((Enumerator_t2464585190 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		goto IL_007b;
	}

IL_0019:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		int32_t L_2 = (int32_t)L_1;
		V_1 = (int32_t)L_2;
		__this->set_next_1(((int32_t)((int32_t)L_2+(int32_t)1)));
		int32_t L_3 = V_1;
		V_0 = (int32_t)L_3;
		Dictionary_2_t1144560488 * L_4 = (Dictionary_2_t1144560488 *)__this->get_dictionary_0();
		NullCheck(L_4);
		LinkU5BU5D_t62501539* L_5 = (LinkU5BU5D_t62501539*)L_4->get_linkSlots_5();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		int32_t L_7 = (int32_t)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_7&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_007b;
		}
	}
	{
		Dictionary_2_t1144560488 * L_8 = (Dictionary_2_t1144560488 *)__this->get_dictionary_0();
		NullCheck(L_8);
		MetricU5BU5D_t2477685199* L_9 = (MetricU5BU5D_t2477685199*)L_8->get_keySlots_6();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		int32_t L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		Dictionary_2_t1144560488 * L_13 = (Dictionary_2_t1144560488 *)__this->get_dictionary_0();
		NullCheck(L_13);
		ObjectU5BU5D_t3614634134* L_14 = (ObjectU5BU5D_t3614634134*)L_13->get_valueSlots_7();
		int32_t L_15 = V_0;
		NullCheck(L_14);
		int32_t L_16 = L_15;
		Il2CppObject * L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		KeyValuePair_2_t3196873006  L_18;
		memset(&L_18, 0, sizeof(L_18));
		KeyValuePair_2__ctor_m595192313(&L_18, (int32_t)L_12, (Il2CppObject *)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		__this->set_current_3(L_18);
		return (bool)1;
	}

IL_007b:
	{
		int32_t L_19 = (int32_t)__this->get_next_1();
		Dictionary_2_t1144560488 * L_20 = (Dictionary_2_t1144560488 *)__this->get_dictionary_0();
		NullCheck(L_20);
		int32_t L_21 = (int32_t)L_20->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_21)))
		{
			goto IL_0019;
		}
	}
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m3844071554_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2464585190 * _thisAdjusted = reinterpret_cast<Enumerator_t2464585190 *>(__this + 1);
	return Enumerator_MoveNext_m3844071554(_thisAdjusted, method);
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t3196873006  Enumerator_get_Current_m1831421754_gshared (Enumerator_t2464585190 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t3196873006  L_0 = (KeyValuePair_2_t3196873006 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  KeyValuePair_2_t3196873006  Enumerator_get_Current_m1831421754_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2464585190 * _thisAdjusted = reinterpret_cast<Enumerator_t2464585190 *>(__this + 1);
	return Enumerator_get_Current_m1831421754(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Object>::get_CurrentKey()
extern "C"  int32_t Enumerator_get_CurrentKey_m2750410819_gshared (Enumerator_t2464585190 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m4158200186((Enumerator_t2464585190 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3196873006 * L_0 = (KeyValuePair_2_t3196873006 *)__this->get_address_of_current_3();
		int32_t L_1 = KeyValuePair_2_get_Key_m2312610863((KeyValuePair_2_t3196873006 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
extern "C"  int32_t Enumerator_get_CurrentKey_m2750410819_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2464585190 * _thisAdjusted = reinterpret_cast<Enumerator_t2464585190 *>(__this + 1);
	return Enumerator_get_CurrentKey_m2750410819(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Object>::get_CurrentValue()
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m4218670947_gshared (Enumerator_t2464585190 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m4158200186((Enumerator_t2464585190 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3196873006 * L_0 = (KeyValuePair_2_t3196873006 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Value_m3734313679((KeyValuePair_2_t3196873006 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m4218670947_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2464585190 * _thisAdjusted = reinterpret_cast<Enumerator_t2464585190 *>(__this + 1);
	return Enumerator_get_CurrentValue_m4218670947(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Object>::Reset()
extern "C"  void Enumerator_Reset_m1867529997_gshared (Enumerator_t2464585190 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m1404756950((Enumerator_t2464585190 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_Reset_m1867529997_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2464585190 * _thisAdjusted = reinterpret_cast<Enumerator_t2464585190 *>(__this + 1);
	Enumerator_Reset_m1867529997(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Object>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral671556834;
extern const uint32_t Enumerator_VerifyState_m1404756950_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m1404756950_gshared (Enumerator_t2464585190 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m1404756950_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t1144560488 * L_0 = (Dictionary_2_t1144560488 *)__this->get_dictionary_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		Dictionary_2_t1144560488 * L_2 = (Dictionary_2_t1144560488 *)__this->get_dictionary_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_generation_14();
		int32_t L_4 = (int32_t)__this->get_stamp_2();
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t721527559 * L_5 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_5, (String_t*)_stringLiteral671556834, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m1404756950_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2464585190 * _thisAdjusted = reinterpret_cast<Enumerator_t2464585190 *>(__this + 1);
	Enumerator_VerifyState_m1404756950(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Object>::VerifyCurrent()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2382073330;
extern const uint32_t Enumerator_VerifyCurrent_m4158200186_MetadataUsageId;
extern "C"  void Enumerator_VerifyCurrent_m4158200186_gshared (Enumerator_t2464585190 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyCurrent_m4158200186_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m1404756950((Enumerator_t2464585190 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2382073330, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001d:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyCurrent_m4158200186_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2464585190 * _thisAdjusted = reinterpret_cast<Enumerator_t2464585190 *>(__this + 1);
	Enumerator_VerifyCurrent_m4158200186(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Amazon.Runtime.Metric,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m2167966803_gshared (Enumerator_t2464585190 * __this, const MethodInfo* method)
{
	{
		__this->set_dictionary_0((Dictionary_2_t1144560488 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2167966803_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2464585190 * _thisAdjusted = reinterpret_cast<Enumerator_t2464585190 *>(__this + 1);
	Enumerator_Dispose_m2167966803(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m648457671_gshared (Enumerator_t2399727785 * __this, Dictionary_2_t1079703083 * ___dictionary0, const MethodInfo* method)
{
	{
		Dictionary_2_t1079703083 * L_0 = ___dictionary0;
		__this->set_dictionary_0(L_0);
		Dictionary_2_t1079703083 * L_1 = ___dictionary0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_generation_14();
		__this->set_stamp_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m648457671_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t1079703083 * ___dictionary0, const MethodInfo* method)
{
	Enumerator_t2399727785 * _thisAdjusted = reinterpret_cast<Enumerator_t2399727785 *>(__this + 1);
	Enumerator__ctor_m648457671(_thisAdjusted, ___dictionary0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2741808790_gshared (Enumerator_t2399727785 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3667735842((Enumerator_t2399727785 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3132015601  L_0 = (KeyValuePair_2_t3132015601 )__this->get_current_3();
		KeyValuePair_2_t3132015601  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2741808790_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2399727785 * _thisAdjusted = reinterpret_cast<Enumerator_t2399727785 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2741808790(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2861488778_gshared (Enumerator_t2399727785 * __this, const MethodInfo* method)
{
	{
		Enumerator_Reset_m2787013825((Enumerator_t2399727785 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2861488778_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2399727785 * _thisAdjusted = reinterpret_cast<Enumerator_t2399727785 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m2861488778(_thisAdjusted, method);
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1723581873_gshared (Enumerator_t2399727785 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3667735842((Enumerator_t2399727785 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3132015601 * L_0 = (KeyValuePair_2_t3132015601 *)__this->get_address_of_current_3();
		int32_t L_1 = KeyValuePair_2_get_Key_m1683812983((KeyValuePair_2_t3132015601 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), &L_2);
		KeyValuePair_2_t3132015601 * L_4 = (KeyValuePair_2_t3132015601 *)__this->get_address_of_current_3();
		int32_t L_5 = KeyValuePair_2_get_Value_m1136571287((KeyValuePair_2_t3132015601 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		int32_t L_6 = L_5;
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_6);
		DictionaryEntry_t3048875398  L_8;
		memset(&L_8, 0, sizeof(L_8));
		DictionaryEntry__ctor_m2901884110(&L_8, (Il2CppObject *)L_3, (Il2CppObject *)L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1723581873_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2399727785 * _thisAdjusted = reinterpret_cast<Enumerator_t2399727785 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1723581873(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m4169232220_gshared (Enumerator_t2399727785 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Enumerator_get_CurrentKey_m3410494779((Enumerator_t2399727785 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m4169232220_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2399727785 * _thisAdjusted = reinterpret_cast<Enumerator_t2399727785 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m4169232220(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3772256406_gshared (Enumerator_t2399727785 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Enumerator_get_CurrentValue_m2708916123((Enumerator_t2399727785 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3772256406_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2399727785 * _thisAdjusted = reinterpret_cast<Enumerator_t2399727785 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3772256406(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3809319098_gshared (Enumerator_t2399727785 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Enumerator_VerifyState_m4145755786((Enumerator_t2399727785 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		goto IL_007b;
	}

IL_0019:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		int32_t L_2 = (int32_t)L_1;
		V_1 = (int32_t)L_2;
		__this->set_next_1(((int32_t)((int32_t)L_2+(int32_t)1)));
		int32_t L_3 = V_1;
		V_0 = (int32_t)L_3;
		Dictionary_2_t1079703083 * L_4 = (Dictionary_2_t1079703083 *)__this->get_dictionary_0();
		NullCheck(L_4);
		LinkU5BU5D_t62501539* L_5 = (LinkU5BU5D_t62501539*)L_4->get_linkSlots_5();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		int32_t L_7 = (int32_t)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_7&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_007b;
		}
	}
	{
		Dictionary_2_t1079703083 * L_8 = (Dictionary_2_t1079703083 *)__this->get_dictionary_0();
		NullCheck(L_8);
		Int32U5BU5D_t3030399641* L_9 = (Int32U5BU5D_t3030399641*)L_8->get_keySlots_6();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		int32_t L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		Dictionary_2_t1079703083 * L_13 = (Dictionary_2_t1079703083 *)__this->get_dictionary_0();
		NullCheck(L_13);
		Int32U5BU5D_t3030399641* L_14 = (Int32U5BU5D_t3030399641*)L_13->get_valueSlots_7();
		int32_t L_15 = V_0;
		NullCheck(L_14);
		int32_t L_16 = L_15;
		int32_t L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		KeyValuePair_2_t3132015601  L_18;
		memset(&L_18, 0, sizeof(L_18));
		KeyValuePair_2__ctor_m2177183229(&L_18, (int32_t)L_12, (int32_t)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		__this->set_current_3(L_18);
		return (bool)1;
	}

IL_007b:
	{
		int32_t L_19 = (int32_t)__this->get_next_1();
		Dictionary_2_t1079703083 * L_20 = (Dictionary_2_t1079703083 *)__this->get_dictionary_0();
		NullCheck(L_20);
		int32_t L_21 = (int32_t)L_20->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_21)))
		{
			goto IL_0019;
		}
	}
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m3809319098_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2399727785 * _thisAdjusted = reinterpret_cast<Enumerator_t2399727785 *>(__this + 1);
	return Enumerator_MoveNext_m3809319098(_thisAdjusted, method);
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::get_Current()
extern "C"  KeyValuePair_2_t3132015601  Enumerator_get_Current_m3655806242_gshared (Enumerator_t2399727785 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t3132015601  L_0 = (KeyValuePair_2_t3132015601 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  KeyValuePair_2_t3132015601  Enumerator_get_Current_m3655806242_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2399727785 * _thisAdjusted = reinterpret_cast<Enumerator_t2399727785 *>(__this + 1);
	return Enumerator_get_Current_m3655806242(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::get_CurrentKey()
extern "C"  int32_t Enumerator_get_CurrentKey_m3410494779_gshared (Enumerator_t2399727785 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3667735842((Enumerator_t2399727785 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3132015601 * L_0 = (KeyValuePair_2_t3132015601 *)__this->get_address_of_current_3();
		int32_t L_1 = KeyValuePair_2_get_Key_m1683812983((KeyValuePair_2_t3132015601 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
extern "C"  int32_t Enumerator_get_CurrentKey_m3410494779_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2399727785 * _thisAdjusted = reinterpret_cast<Enumerator_t2399727785 *>(__this + 1);
	return Enumerator_get_CurrentKey_m3410494779(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::get_CurrentValue()
extern "C"  int32_t Enumerator_get_CurrentValue_m2708916123_gshared (Enumerator_t2399727785 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3667735842((Enumerator_t2399727785 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3132015601 * L_0 = (KeyValuePair_2_t3132015601 *)__this->get_address_of_current_3();
		int32_t L_1 = KeyValuePair_2_get_Value_m1136571287((KeyValuePair_2_t3132015601 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  int32_t Enumerator_get_CurrentValue_m2708916123_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2399727785 * _thisAdjusted = reinterpret_cast<Enumerator_t2399727785 *>(__this + 1);
	return Enumerator_get_CurrentValue_m2708916123(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::Reset()
extern "C"  void Enumerator_Reset_m2787013825_gshared (Enumerator_t2399727785 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m4145755786((Enumerator_t2399727785 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_Reset_m2787013825_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2399727785 * _thisAdjusted = reinterpret_cast<Enumerator_t2399727785 *>(__this + 1);
	Enumerator_Reset_m2787013825(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral671556834;
extern const uint32_t Enumerator_VerifyState_m4145755786_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m4145755786_gshared (Enumerator_t2399727785 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m4145755786_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t1079703083 * L_0 = (Dictionary_2_t1079703083 *)__this->get_dictionary_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		Dictionary_2_t1079703083 * L_2 = (Dictionary_2_t1079703083 *)__this->get_dictionary_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_generation_14();
		int32_t L_4 = (int32_t)__this->get_stamp_2();
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t721527559 * L_5 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_5, (String_t*)_stringLiteral671556834, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m4145755786_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2399727785 * _thisAdjusted = reinterpret_cast<Enumerator_t2399727785 *>(__this + 1);
	Enumerator_VerifyState_m4145755786(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::VerifyCurrent()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2382073330;
extern const uint32_t Enumerator_VerifyCurrent_m3667735842_MetadataUsageId;
extern "C"  void Enumerator_VerifyCurrent_m3667735842_gshared (Enumerator_t2399727785 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyCurrent_m3667735842_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m4145755786((Enumerator_t2399727785 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2382073330, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001d:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyCurrent_m3667735842_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2399727785 * _thisAdjusted = reinterpret_cast<Enumerator_t2399727785 *>(__this + 1);
	Enumerator_VerifyCurrent_m3667735842(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m1631869499_gshared (Enumerator_t2399727785 * __this, const MethodInfo* method)
{
	{
		__this->set_dictionary_0((Dictionary_2_t1079703083 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m1631869499_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2399727785 * _thisAdjusted = reinterpret_cast<Enumerator_t2399727785 *>(__this + 1);
	Enumerator_Dispose_m1631869499(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1702560852_gshared (Enumerator_t3017299632 * __this, Dictionary_2_t1697274930 * ___dictionary0, const MethodInfo* method)
{
	{
		Dictionary_2_t1697274930 * L_0 = ___dictionary0;
		__this->set_dictionary_0(L_0);
		Dictionary_2_t1697274930 * L_1 = ___dictionary0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_generation_14();
		__this->set_stamp_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1702560852_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t1697274930 * ___dictionary0, const MethodInfo* method)
{
	Enumerator_t3017299632 * _thisAdjusted = reinterpret_cast<Enumerator_t3017299632 *>(__this + 1);
	Enumerator__ctor_m1702560852(_thisAdjusted, ___dictionary0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1631145297_gshared (Enumerator_t3017299632 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m4246196125((Enumerator_t3017299632 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3749587448  L_0 = (KeyValuePair_2_t3749587448 )__this->get_current_3();
		KeyValuePair_2_t3749587448  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1631145297_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3017299632 * _thisAdjusted = reinterpret_cast<Enumerator_t3017299632 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1631145297(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2828524109_gshared (Enumerator_t3017299632 * __this, const MethodInfo* method)
{
	{
		Enumerator_Reset_m761796566((Enumerator_t3017299632 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2828524109_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3017299632 * _thisAdjusted = reinterpret_cast<Enumerator_t3017299632 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m2828524109(_thisAdjusted, method);
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m345330700_gshared (Enumerator_t3017299632 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m4246196125((Enumerator_t3017299632 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3749587448 * L_0 = (KeyValuePair_2_t3749587448 *)__this->get_address_of_current_3();
		int32_t L_1 = KeyValuePair_2_get_Key_m1435832840((KeyValuePair_2_t3749587448 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), &L_2);
		KeyValuePair_2_t3749587448 * L_4 = (KeyValuePair_2_t3749587448 *)__this->get_address_of_current_3();
		Il2CppObject * L_5 = KeyValuePair_2_get_Value_m3690000728((KeyValuePair_2_t3749587448 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		DictionaryEntry_t3048875398  L_6;
		memset(&L_6, 0, sizeof(L_6));
		DictionaryEntry__ctor_m2901884110(&L_6, (Il2CppObject *)L_3, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m345330700_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3017299632 * _thisAdjusted = reinterpret_cast<Enumerator_t3017299632 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m345330700(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1330261287_gshared (Enumerator_t3017299632 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Enumerator_get_CurrentKey_m447338908((Enumerator_t3017299632 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1330261287_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3017299632 * _thisAdjusted = reinterpret_cast<Enumerator_t3017299632 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1330261287(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3853964719_gshared (Enumerator_t3017299632 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = Enumerator_get_CurrentValue_m3562053380((Enumerator_t3017299632 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return L_0;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3853964719_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3017299632 * _thisAdjusted = reinterpret_cast<Enumerator_t3017299632 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3853964719(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2770956757_gshared (Enumerator_t3017299632 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Enumerator_VerifyState_m2118679243((Enumerator_t3017299632 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		goto IL_007b;
	}

IL_0019:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		int32_t L_2 = (int32_t)L_1;
		V_1 = (int32_t)L_2;
		__this->set_next_1(((int32_t)((int32_t)L_2+(int32_t)1)));
		int32_t L_3 = V_1;
		V_0 = (int32_t)L_3;
		Dictionary_2_t1697274930 * L_4 = (Dictionary_2_t1697274930 *)__this->get_dictionary_0();
		NullCheck(L_4);
		LinkU5BU5D_t62501539* L_5 = (LinkU5BU5D_t62501539*)L_4->get_linkSlots_5();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		int32_t L_7 = (int32_t)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_7&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_007b;
		}
	}
	{
		Dictionary_2_t1697274930 * L_8 = (Dictionary_2_t1697274930 *)__this->get_dictionary_0();
		NullCheck(L_8);
		Int32U5BU5D_t3030399641* L_9 = (Int32U5BU5D_t3030399641*)L_8->get_keySlots_6();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		int32_t L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		Dictionary_2_t1697274930 * L_13 = (Dictionary_2_t1697274930 *)__this->get_dictionary_0();
		NullCheck(L_13);
		ObjectU5BU5D_t3614634134* L_14 = (ObjectU5BU5D_t3614634134*)L_13->get_valueSlots_7();
		int32_t L_15 = V_0;
		NullCheck(L_14);
		int32_t L_16 = L_15;
		Il2CppObject * L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		KeyValuePair_2_t3749587448  L_18;
		memset(&L_18, 0, sizeof(L_18));
		KeyValuePair_2__ctor_m3201181706(&L_18, (int32_t)L_12, (Il2CppObject *)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		__this->set_current_3(L_18);
		return (bool)1;
	}

IL_007b:
	{
		int32_t L_19 = (int32_t)__this->get_next_1();
		Dictionary_2_t1697274930 * L_20 = (Dictionary_2_t1697274930 *)__this->get_dictionary_0();
		NullCheck(L_20);
		int32_t L_21 = (int32_t)L_20->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_21)))
		{
			goto IL_0019;
		}
	}
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m2770956757_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3017299632 * _thisAdjusted = reinterpret_cast<Enumerator_t3017299632 *>(__this + 1);
	return Enumerator_MoveNext_m2770956757(_thisAdjusted, method);
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t3749587448  Enumerator_get_Current_m2230224741_gshared (Enumerator_t3017299632 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t3749587448  L_0 = (KeyValuePair_2_t3749587448 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  KeyValuePair_2_t3749587448  Enumerator_get_Current_m2230224741_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3017299632 * _thisAdjusted = reinterpret_cast<Enumerator_t3017299632 *>(__this + 1);
	return Enumerator_get_Current_m2230224741(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::get_CurrentKey()
extern "C"  int32_t Enumerator_get_CurrentKey_m447338908_gshared (Enumerator_t3017299632 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m4246196125((Enumerator_t3017299632 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3749587448 * L_0 = (KeyValuePair_2_t3749587448 *)__this->get_address_of_current_3();
		int32_t L_1 = KeyValuePair_2_get_Key_m1435832840((KeyValuePair_2_t3749587448 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
extern "C"  int32_t Enumerator_get_CurrentKey_m447338908_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3017299632 * _thisAdjusted = reinterpret_cast<Enumerator_t3017299632 *>(__this + 1);
	return Enumerator_get_CurrentKey_m447338908(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::get_CurrentValue()
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m3562053380_gshared (Enumerator_t3017299632 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m4246196125((Enumerator_t3017299632 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3749587448 * L_0 = (KeyValuePair_2_t3749587448 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Value_m3690000728((KeyValuePair_2_t3749587448 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m3562053380_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3017299632 * _thisAdjusted = reinterpret_cast<Enumerator_t3017299632 *>(__this + 1);
	return Enumerator_get_CurrentValue_m3562053380(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::Reset()
extern "C"  void Enumerator_Reset_m761796566_gshared (Enumerator_t3017299632 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m2118679243((Enumerator_t3017299632 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_Reset_m761796566_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3017299632 * _thisAdjusted = reinterpret_cast<Enumerator_t3017299632 *>(__this + 1);
	Enumerator_Reset_m761796566(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral671556834;
extern const uint32_t Enumerator_VerifyState_m2118679243_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m2118679243_gshared (Enumerator_t3017299632 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m2118679243_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t1697274930 * L_0 = (Dictionary_2_t1697274930 *)__this->get_dictionary_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		Dictionary_2_t1697274930 * L_2 = (Dictionary_2_t1697274930 *)__this->get_dictionary_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_generation_14();
		int32_t L_4 = (int32_t)__this->get_stamp_2();
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t721527559 * L_5 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_5, (String_t*)_stringLiteral671556834, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m2118679243_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3017299632 * _thisAdjusted = reinterpret_cast<Enumerator_t3017299632 *>(__this + 1);
	Enumerator_VerifyState_m2118679243(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::VerifyCurrent()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2382073330;
extern const uint32_t Enumerator_VerifyCurrent_m4246196125_MetadataUsageId;
extern "C"  void Enumerator_VerifyCurrent_m4246196125_gshared (Enumerator_t3017299632 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyCurrent_m4246196125_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m2118679243((Enumerator_t3017299632 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2382073330, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001d:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyCurrent_m4246196125_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3017299632 * _thisAdjusted = reinterpret_cast<Enumerator_t3017299632 *>(__this + 1);
	Enumerator_VerifyCurrent_m4246196125(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m2243145188_gshared (Enumerator_t3017299632 * __this, const MethodInfo* method)
{
	{
		__this->set_dictionary_0((Dictionary_2_t1697274930 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2243145188_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3017299632 * _thisAdjusted = reinterpret_cast<Enumerator_t3017299632 *>(__this + 1);
	Enumerator_Dispose_m2243145188(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3045959731_gshared (Enumerator_t704025103 * __this, Dictionary_2_t3678967697 * ___dictionary0, const MethodInfo* method)
{
	{
		Dictionary_2_t3678967697 * L_0 = ___dictionary0;
		__this->set_dictionary_0(L_0);
		Dictionary_2_t3678967697 * L_1 = ___dictionary0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_generation_14();
		__this->set_stamp_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3045959731_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t3678967697 * ___dictionary0, const MethodInfo* method)
{
	Enumerator_t704025103 * _thisAdjusted = reinterpret_cast<Enumerator_t704025103 *>(__this + 1);
	Enumerator__ctor_m3045959731(_thisAdjusted, ___dictionary0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m524777314_gshared (Enumerator_t704025103 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m1011807396((Enumerator_t704025103 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1436312919  L_0 = (KeyValuePair_2_t1436312919 )__this->get_current_3();
		KeyValuePair_2_t1436312919  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m524777314_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t704025103 * _thisAdjusted = reinterpret_cast<Enumerator_t704025103 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m524777314(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3684036852_gshared (Enumerator_t704025103 * __this, const MethodInfo* method)
{
	{
		Enumerator_Reset_m598725905((Enumerator_t704025103 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3684036852_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t704025103 * _thisAdjusted = reinterpret_cast<Enumerator_t704025103 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m3684036852(_thisAdjusted, method);
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1310647545_gshared (Enumerator_t704025103 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m1011807396((Enumerator_t704025103 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1436312919 * L_0 = (KeyValuePair_2_t1436312919 *)__this->get_address_of_current_3();
		int64_t L_1 = KeyValuePair_2_get_Key_m3157806383((KeyValuePair_2_t1436312919 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		int64_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), &L_2);
		KeyValuePair_2_t1436312919 * L_4 = (KeyValuePair_2_t1436312919 *)__this->get_address_of_current_3();
		Il2CppObject * L_5 = KeyValuePair_2_get_Value_m1978486399((KeyValuePair_2_t1436312919 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		DictionaryEntry_t3048875398  L_6;
		memset(&L_6, 0, sizeof(L_6));
		DictionaryEntry__ctor_m2901884110(&L_6, (Il2CppObject *)L_3, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1310647545_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t704025103 * _thisAdjusted = reinterpret_cast<Enumerator_t704025103 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1310647545(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m529281260_gshared (Enumerator_t704025103 * __this, const MethodInfo* method)
{
	{
		int64_t L_0 = Enumerator_get_CurrentKey_m4167196999((Enumerator_t704025103 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		int64_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m529281260_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t704025103 * _thisAdjusted = reinterpret_cast<Enumerator_t704025103 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m529281260(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2302706078_gshared (Enumerator_t704025103 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = Enumerator_get_CurrentValue_m659195263((Enumerator_t704025103 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return L_0;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2302706078_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t704025103 * _thisAdjusted = reinterpret_cast<Enumerator_t704025103 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2302706078(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3139459564_gshared (Enumerator_t704025103 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Enumerator_VerifyState_m3803534940((Enumerator_t704025103 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		goto IL_007b;
	}

IL_0019:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		int32_t L_2 = (int32_t)L_1;
		V_1 = (int32_t)L_2;
		__this->set_next_1(((int32_t)((int32_t)L_2+(int32_t)1)));
		int32_t L_3 = V_1;
		V_0 = (int32_t)L_3;
		Dictionary_2_t3678967697 * L_4 = (Dictionary_2_t3678967697 *)__this->get_dictionary_0();
		NullCheck(L_4);
		LinkU5BU5D_t62501539* L_5 = (LinkU5BU5D_t62501539*)L_4->get_linkSlots_5();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		int32_t L_7 = (int32_t)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_7&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_007b;
		}
	}
	{
		Dictionary_2_t3678967697 * L_8 = (Dictionary_2_t3678967697 *)__this->get_dictionary_0();
		NullCheck(L_8);
		Int64U5BU5D_t717125112* L_9 = (Int64U5BU5D_t717125112*)L_8->get_keySlots_6();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		int64_t L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		Dictionary_2_t3678967697 * L_13 = (Dictionary_2_t3678967697 *)__this->get_dictionary_0();
		NullCheck(L_13);
		ObjectU5BU5D_t3614634134* L_14 = (ObjectU5BU5D_t3614634134*)L_13->get_valueSlots_7();
		int32_t L_15 = V_0;
		NullCheck(L_14);
		int32_t L_16 = L_15;
		Il2CppObject * L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		KeyValuePair_2_t1436312919  L_18;
		memset(&L_18, 0, sizeof(L_18));
		KeyValuePair_2__ctor_m1940188057(&L_18, (int64_t)L_12, (Il2CppObject *)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		__this->set_current_3(L_18);
		return (bool)1;
	}

IL_007b:
	{
		int32_t L_19 = (int32_t)__this->get_next_1();
		Dictionary_2_t3678967697 * L_20 = (Dictionary_2_t3678967697 *)__this->get_dictionary_0();
		NullCheck(L_20);
		int32_t L_21 = (int32_t)L_20->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_21)))
		{
			goto IL_0019;
		}
	}
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m3139459564_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t704025103 * _thisAdjusted = reinterpret_cast<Enumerator_t704025103 *>(__this + 1);
	return Enumerator_MoveNext_m3139459564(_thisAdjusted, method);
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t1436312919  Enumerator_get_Current_m3988231868_gshared (Enumerator_t704025103 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t1436312919  L_0 = (KeyValuePair_2_t1436312919 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  KeyValuePair_2_t1436312919  Enumerator_get_Current_m3988231868_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t704025103 * _thisAdjusted = reinterpret_cast<Enumerator_t704025103 *>(__this + 1);
	return Enumerator_get_Current_m3988231868(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,System.Object>::get_CurrentKey()
extern "C"  int64_t Enumerator_get_CurrentKey_m4167196999_gshared (Enumerator_t704025103 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m1011807396((Enumerator_t704025103 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1436312919 * L_0 = (KeyValuePair_2_t1436312919 *)__this->get_address_of_current_3();
		int64_t L_1 = KeyValuePair_2_get_Key_m3157806383((KeyValuePair_2_t1436312919 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
extern "C"  int64_t Enumerator_get_CurrentKey_m4167196999_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t704025103 * _thisAdjusted = reinterpret_cast<Enumerator_t704025103 *>(__this + 1);
	return Enumerator_get_CurrentKey_m4167196999(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,System.Object>::get_CurrentValue()
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m659195263_gshared (Enumerator_t704025103 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m1011807396((Enumerator_t704025103 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1436312919 * L_0 = (KeyValuePair_2_t1436312919 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Value_m1978486399((KeyValuePair_2_t1436312919 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m659195263_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t704025103 * _thisAdjusted = reinterpret_cast<Enumerator_t704025103 *>(__this + 1);
	return Enumerator_get_CurrentValue_m659195263(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,System.Object>::Reset()
extern "C"  void Enumerator_Reset_m598725905_gshared (Enumerator_t704025103 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m3803534940((Enumerator_t704025103 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_Reset_m598725905_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t704025103 * _thisAdjusted = reinterpret_cast<Enumerator_t704025103 *>(__this + 1);
	Enumerator_Reset_m598725905(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,System.Object>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral671556834;
extern const uint32_t Enumerator_VerifyState_m3803534940_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m3803534940_gshared (Enumerator_t704025103 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m3803534940_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t3678967697 * L_0 = (Dictionary_2_t3678967697 *)__this->get_dictionary_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		Dictionary_2_t3678967697 * L_2 = (Dictionary_2_t3678967697 *)__this->get_dictionary_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_generation_14();
		int32_t L_4 = (int32_t)__this->get_stamp_2();
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t721527559 * L_5 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_5, (String_t*)_stringLiteral671556834, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m3803534940_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t704025103 * _thisAdjusted = reinterpret_cast<Enumerator_t704025103 *>(__this + 1);
	Enumerator_VerifyState_m3803534940(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,System.Object>::VerifyCurrent()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2382073330;
extern const uint32_t Enumerator_VerifyCurrent_m1011807396_MetadataUsageId;
extern "C"  void Enumerator_VerifyCurrent_m1011807396_gshared (Enumerator_t704025103 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyCurrent_m1011807396_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m3803534940((Enumerator_t704025103 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2382073330, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001d:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyCurrent_m1011807396_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t704025103 * _thisAdjusted = reinterpret_cast<Enumerator_t704025103 *>(__this + 1);
	Enumerator_VerifyCurrent_m1011807396(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3446908287_gshared (Enumerator_t704025103 * __this, const MethodInfo* method)
{
	{
		__this->set_dictionary_0((Dictionary_2_t3678967697 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3446908287_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t704025103 * _thisAdjusted = reinterpret_cast<Enumerator_t704025103 *>(__this + 1);
	Enumerator_Dispose_m3446908287(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1897170543_gshared (Enumerator_t156532019 * __this, Dictionary_2_t3131474613 * ___dictionary0, const MethodInfo* method)
{
	{
		Dictionary_2_t3131474613 * L_0 = ___dictionary0;
		__this->set_dictionary_0(L_0);
		Dictionary_2_t3131474613 * L_1 = ___dictionary0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_generation_14();
		__this->set_stamp_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1897170543_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t3131474613 * ___dictionary0, const MethodInfo* method)
{
	Enumerator_t156532019 * _thisAdjusted = reinterpret_cast<Enumerator_t156532019 *>(__this + 1);
	Enumerator__ctor_m1897170543(_thisAdjusted, ___dictionary0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2389891262_gshared (Enumerator_t156532019 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m213906106((Enumerator_t156532019 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t888819835  L_0 = (KeyValuePair_2_t888819835 )__this->get_current_3();
		KeyValuePair_2_t888819835  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2389891262_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t156532019 * _thisAdjusted = reinterpret_cast<Enumerator_t156532019 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2389891262(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3302454658_gshared (Enumerator_t156532019 * __this, const MethodInfo* method)
{
	{
		Enumerator_Reset_m1147555085((Enumerator_t156532019 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3302454658_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t156532019 * _thisAdjusted = reinterpret_cast<Enumerator_t156532019 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m3302454658(_thisAdjusted, method);
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m4077548725_gshared (Enumerator_t156532019 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m213906106((Enumerator_t156532019 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t888819835 * L_0 = (KeyValuePair_2_t888819835 *)__this->get_address_of_current_3();
		IntPtr_t L_1 = KeyValuePair_2_get_Key_m1574332879((KeyValuePair_2_t888819835 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		IntPtr_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), &L_2);
		KeyValuePair_2_t888819835 * L_4 = (KeyValuePair_2_t888819835 *)__this->get_address_of_current_3();
		Il2CppObject * L_5 = KeyValuePair_2_get_Value_m544293807((KeyValuePair_2_t888819835 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		DictionaryEntry_t3048875398  L_6;
		memset(&L_6, 0, sizeof(L_6));
		DictionaryEntry__ctor_m2901884110(&L_6, (Il2CppObject *)L_3, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m4077548725_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t156532019 * _thisAdjusted = reinterpret_cast<Enumerator_t156532019 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m4077548725(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2319419488_gshared (Enumerator_t156532019 * __this, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = Enumerator_get_CurrentKey_m2387752419((Enumerator_t156532019 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		IntPtr_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2319419488_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t156532019 * _thisAdjusted = reinterpret_cast<Enumerator_t156532019 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2319419488(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m120250090_gshared (Enumerator_t156532019 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = Enumerator_get_CurrentValue_m3147984131((Enumerator_t156532019 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return L_0;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m120250090_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t156532019 * _thisAdjusted = reinterpret_cast<Enumerator_t156532019 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m120250090(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m266619810_gshared (Enumerator_t156532019 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Enumerator_VerifyState_m1547102902((Enumerator_t156532019 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		goto IL_007b;
	}

IL_0019:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		int32_t L_2 = (int32_t)L_1;
		V_1 = (int32_t)L_2;
		__this->set_next_1(((int32_t)((int32_t)L_2+(int32_t)1)));
		int32_t L_3 = V_1;
		V_0 = (int32_t)L_3;
		Dictionary_2_t3131474613 * L_4 = (Dictionary_2_t3131474613 *)__this->get_dictionary_0();
		NullCheck(L_4);
		LinkU5BU5D_t62501539* L_5 = (LinkU5BU5D_t62501539*)L_4->get_linkSlots_5();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		int32_t L_7 = (int32_t)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_7&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_007b;
		}
	}
	{
		Dictionary_2_t3131474613 * L_8 = (Dictionary_2_t3131474613 *)__this->get_dictionary_0();
		NullCheck(L_8);
		IntPtrU5BU5D_t169632028* L_9 = (IntPtrU5BU5D_t169632028*)L_8->get_keySlots_6();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		IntPtr_t L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		Dictionary_2_t3131474613 * L_13 = (Dictionary_2_t3131474613 *)__this->get_dictionary_0();
		NullCheck(L_13);
		ObjectU5BU5D_t3614634134* L_14 = (ObjectU5BU5D_t3614634134*)L_13->get_valueSlots_7();
		int32_t L_15 = V_0;
		NullCheck(L_14);
		int32_t L_16 = L_15;
		Il2CppObject * L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		KeyValuePair_2_t888819835  L_18;
		memset(&L_18, 0, sizeof(L_18));
		KeyValuePair_2__ctor_m477426041(&L_18, (IntPtr_t)L_12, (Il2CppObject *)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		__this->set_current_3(L_18);
		return (bool)1;
	}

IL_007b:
	{
		int32_t L_19 = (int32_t)__this->get_next_1();
		Dictionary_2_t3131474613 * L_20 = (Dictionary_2_t3131474613 *)__this->get_dictionary_0();
		NullCheck(L_20);
		int32_t L_21 = (int32_t)L_20->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_21)))
		{
			goto IL_0019;
		}
	}
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m266619810_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t156532019 * _thisAdjusted = reinterpret_cast<Enumerator_t156532019 *>(__this + 1);
	return Enumerator_MoveNext_m266619810(_thisAdjusted, method);
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t888819835  Enumerator_get_Current_m3216113338_gshared (Enumerator_t156532019 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t888819835  L_0 = (KeyValuePair_2_t888819835 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  KeyValuePair_2_t888819835  Enumerator_get_Current_m3216113338_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t156532019 * _thisAdjusted = reinterpret_cast<Enumerator_t156532019 *>(__this + 1);
	return Enumerator_get_Current_m3216113338(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Object>::get_CurrentKey()
extern "C"  IntPtr_t Enumerator_get_CurrentKey_m2387752419_gshared (Enumerator_t156532019 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m213906106((Enumerator_t156532019 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t888819835 * L_0 = (KeyValuePair_2_t888819835 *)__this->get_address_of_current_3();
		IntPtr_t L_1 = KeyValuePair_2_get_Key_m1574332879((KeyValuePair_2_t888819835 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
extern "C"  IntPtr_t Enumerator_get_CurrentKey_m2387752419_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t156532019 * _thisAdjusted = reinterpret_cast<Enumerator_t156532019 *>(__this + 1);
	return Enumerator_get_CurrentKey_m2387752419(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Object>::get_CurrentValue()
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m3147984131_gshared (Enumerator_t156532019 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m213906106((Enumerator_t156532019 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t888819835 * L_0 = (KeyValuePair_2_t888819835 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Value_m544293807((KeyValuePair_2_t888819835 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m3147984131_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t156532019 * _thisAdjusted = reinterpret_cast<Enumerator_t156532019 *>(__this + 1);
	return Enumerator_get_CurrentValue_m3147984131(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Object>::Reset()
extern "C"  void Enumerator_Reset_m1147555085_gshared (Enumerator_t156532019 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m1547102902((Enumerator_t156532019 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_Reset_m1147555085_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t156532019 * _thisAdjusted = reinterpret_cast<Enumerator_t156532019 *>(__this + 1);
	Enumerator_Reset_m1147555085(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Object>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral671556834;
extern const uint32_t Enumerator_VerifyState_m1547102902_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m1547102902_gshared (Enumerator_t156532019 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m1547102902_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t3131474613 * L_0 = (Dictionary_2_t3131474613 *)__this->get_dictionary_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		Dictionary_2_t3131474613 * L_2 = (Dictionary_2_t3131474613 *)__this->get_dictionary_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_generation_14();
		int32_t L_4 = (int32_t)__this->get_stamp_2();
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t721527559 * L_5 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_5, (String_t*)_stringLiteral671556834, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m1547102902_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t156532019 * _thisAdjusted = reinterpret_cast<Enumerator_t156532019 *>(__this + 1);
	Enumerator_VerifyState_m1547102902(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Object>::VerifyCurrent()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2382073330;
extern const uint32_t Enumerator_VerifyCurrent_m213906106_MetadataUsageId;
extern "C"  void Enumerator_VerifyCurrent_m213906106_gshared (Enumerator_t156532019 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyCurrent_m213906106_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m1547102902((Enumerator_t156532019 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2382073330, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001d:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyCurrent_m213906106_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t156532019 * _thisAdjusted = reinterpret_cast<Enumerator_t156532019 *>(__this + 1);
	Enumerator_VerifyCurrent_m213906106(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m2117437651_gshared (Enumerator_t156532019 * __this, const MethodInfo* method)
{
	{
		__this->set_dictionary_0((Dictionary_2_t3131474613 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2117437651_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t156532019 * _thisAdjusted = reinterpret_cast<Enumerator_t156532019 *>(__this + 1);
	Enumerator_Dispose_m2117437651(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1253376471_gshared (Enumerator_t2544892208 * __this, Dictionary_2_t1224867506 * ___dictionary0, const MethodInfo* method)
{
	{
		Dictionary_2_t1224867506 * L_0 = ___dictionary0;
		__this->set_dictionary_0(L_0);
		Dictionary_2_t1224867506 * L_1 = ___dictionary0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_generation_14();
		__this->set_stamp_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1253376471_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t1224867506 * ___dictionary0, const MethodInfo* method)
{
	Enumerator_t2544892208 * _thisAdjusted = reinterpret_cast<Enumerator_t2544892208 *>(__this + 1);
	Enumerator__ctor_m1253376471(_thisAdjusted, ___dictionary0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1783562472_gshared (Enumerator_t2544892208 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m2033497076((Enumerator_t2544892208 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3277180024  L_0 = (KeyValuePair_2_t3277180024 )__this->get_current_3();
		KeyValuePair_2_t3277180024  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1783562472_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2544892208 * _thisAdjusted = reinterpret_cast<Enumerator_t2544892208 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1783562472(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2320340484_gshared (Enumerator_t2544892208 * __this, const MethodInfo* method)
{
	{
		Enumerator_Reset_m1928144285((Enumerator_t2544892208 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2320340484_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2544892208 * _thisAdjusted = reinterpret_cast<Enumerator_t2544892208 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m2320340484(_thisAdjusted, method);
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m283920581_gshared (Enumerator_t2544892208 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m2033497076((Enumerator_t2544892208 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3277180024 * L_0 = (KeyValuePair_2_t3277180024 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Key_m4122353311((KeyValuePair_2_t3277180024 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		KeyValuePair_2_t3277180024 * L_2 = (KeyValuePair_2_t3277180024 *)__this->get_address_of_current_3();
		int32_t L_3 = KeyValuePair_2_get_Value_m116637759((KeyValuePair_2_t3277180024 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		int32_t L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_4);
		DictionaryEntry_t3048875398  L_6;
		memset(&L_6, 0, sizeof(L_6));
		DictionaryEntry__ctor_m2901884110(&L_6, (Il2CppObject *)L_1, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m283920581_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2544892208 * _thisAdjusted = reinterpret_cast<Enumerator_t2544892208 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m283920581(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2409199898_gshared (Enumerator_t2544892208 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = Enumerator_get_CurrentKey_m4133598171((Enumerator_t2544892208 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		return L_0;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2409199898_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2544892208 * _thisAdjusted = reinterpret_cast<Enumerator_t2544892208 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2409199898(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m948336_gshared (Enumerator_t2544892208 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Enumerator_get_CurrentValue_m17055867((Enumerator_t2544892208 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m948336_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2544892208 * _thisAdjusted = reinterpret_cast<Enumerator_t2544892208 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m948336(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3850333932_gshared (Enumerator_t2544892208 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Enumerator_VerifyState_m1152067668((Enumerator_t2544892208 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		goto IL_007b;
	}

IL_0019:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		int32_t L_2 = (int32_t)L_1;
		V_1 = (int32_t)L_2;
		__this->set_next_1(((int32_t)((int32_t)L_2+(int32_t)1)));
		int32_t L_3 = V_1;
		V_0 = (int32_t)L_3;
		Dictionary_2_t1224867506 * L_4 = (Dictionary_2_t1224867506 *)__this->get_dictionary_0();
		NullCheck(L_4);
		LinkU5BU5D_t62501539* L_5 = (LinkU5BU5D_t62501539*)L_4->get_linkSlots_5();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		int32_t L_7 = (int32_t)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_7&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_007b;
		}
	}
	{
		Dictionary_2_t1224867506 * L_8 = (Dictionary_2_t1224867506 *)__this->get_dictionary_0();
		NullCheck(L_8);
		ObjectU5BU5D_t3614634134* L_9 = (ObjectU5BU5D_t3614634134*)L_8->get_keySlots_6();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		Il2CppObject * L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		Dictionary_2_t1224867506 * L_13 = (Dictionary_2_t1224867506 *)__this->get_dictionary_0();
		NullCheck(L_13);
		InstantiationModelU5BU5D_t1614949031* L_14 = (InstantiationModelU5BU5D_t1614949031*)L_13->get_valueSlots_7();
		int32_t L_15 = V_0;
		NullCheck(L_14);
		int32_t L_16 = L_15;
		int32_t L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		KeyValuePair_2_t3277180024  L_18;
		memset(&L_18, 0, sizeof(L_18));
		KeyValuePair_2__ctor_m3044907881(&L_18, (Il2CppObject *)L_12, (int32_t)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		__this->set_current_3(L_18);
		return (bool)1;
	}

IL_007b:
	{
		int32_t L_19 = (int32_t)__this->get_next_1();
		Dictionary_2_t1224867506 * L_20 = (Dictionary_2_t1224867506 *)__this->get_dictionary_0();
		NullCheck(L_20);
		int32_t L_21 = (int32_t)L_20->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_21)))
		{
			goto IL_0019;
		}
	}
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m3850333932_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2544892208 * _thisAdjusted = reinterpret_cast<Enumerator_t2544892208 *>(__this + 1);
	return Enumerator_MoveNext_m3850333932(_thisAdjusted, method);
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::get_Current()
extern "C"  KeyValuePair_2_t3277180024  Enumerator_get_Current_m663494932_gshared (Enumerator_t2544892208 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t3277180024  L_0 = (KeyValuePair_2_t3277180024 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  KeyValuePair_2_t3277180024  Enumerator_get_Current_m663494932_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2544892208 * _thisAdjusted = reinterpret_cast<Enumerator_t2544892208 *>(__this + 1);
	return Enumerator_get_Current_m663494932(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::get_CurrentKey()
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m4133598171_gshared (Enumerator_t2544892208 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m2033497076((Enumerator_t2544892208 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3277180024 * L_0 = (KeyValuePair_2_t3277180024 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Key_m4122353311((KeyValuePair_2_t3277180024 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m4133598171_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2544892208 * _thisAdjusted = reinterpret_cast<Enumerator_t2544892208 *>(__this + 1);
	return Enumerator_get_CurrentKey_m4133598171(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::get_CurrentValue()
extern "C"  int32_t Enumerator_get_CurrentValue_m17055867_gshared (Enumerator_t2544892208 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m2033497076((Enumerator_t2544892208 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3277180024 * L_0 = (KeyValuePair_2_t3277180024 *)__this->get_address_of_current_3();
		int32_t L_1 = KeyValuePair_2_get_Value_m116637759((KeyValuePair_2_t3277180024 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  int32_t Enumerator_get_CurrentValue_m17055867_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2544892208 * _thisAdjusted = reinterpret_cast<Enumerator_t2544892208 *>(__this + 1);
	return Enumerator_get_CurrentValue_m17055867(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::Reset()
extern "C"  void Enumerator_Reset_m1928144285_gshared (Enumerator_t2544892208 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m1152067668((Enumerator_t2544892208 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_Reset_m1928144285_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2544892208 * _thisAdjusted = reinterpret_cast<Enumerator_t2544892208 *>(__this + 1);
	Enumerator_Reset_m1928144285(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral671556834;
extern const uint32_t Enumerator_VerifyState_m1152067668_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m1152067668_gshared (Enumerator_t2544892208 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m1152067668_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t1224867506 * L_0 = (Dictionary_2_t1224867506 *)__this->get_dictionary_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		Dictionary_2_t1224867506 * L_2 = (Dictionary_2_t1224867506 *)__this->get_dictionary_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_generation_14();
		int32_t L_4 = (int32_t)__this->get_stamp_2();
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t721527559 * L_5 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_5, (String_t*)_stringLiteral671556834, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m1152067668_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2544892208 * _thisAdjusted = reinterpret_cast<Enumerator_t2544892208 *>(__this + 1);
	Enumerator_VerifyState_m1152067668(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::VerifyCurrent()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2382073330;
extern const uint32_t Enumerator_VerifyCurrent_m2033497076_MetadataUsageId;
extern "C"  void Enumerator_VerifyCurrent_m2033497076_gshared (Enumerator_t2544892208 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyCurrent_m2033497076_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m1152067668((Enumerator_t2544892208 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2382073330, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001d:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyCurrent_m2033497076_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2544892208 * _thisAdjusted = reinterpret_cast<Enumerator_t2544892208 *>(__this + 1);
	Enumerator_VerifyCurrent_m2033497076(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::Dispose()
extern "C"  void Enumerator_Dispose_m1362422387_gshared (Enumerator_t2544892208 * __this, const MethodInfo* method)
{
	{
		__this->set_dictionary_0((Dictionary_2_t1224867506 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m1362422387_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2544892208 * _thisAdjusted = reinterpret_cast<Enumerator_t2544892208 *>(__this + 1);
	Enumerator_Dispose_m1362422387(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m661036428_gshared (Enumerator_t442692252 * __this, Dictionary_2_t3417634846 * ___dictionary0, const MethodInfo* method)
{
	{
		Dictionary_2_t3417634846 * L_0 = ___dictionary0;
		__this->set_dictionary_0(L_0);
		Dictionary_2_t3417634846 * L_1 = ___dictionary0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_generation_14();
		__this->set_stamp_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m661036428_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t3417634846 * ___dictionary0, const MethodInfo* method)
{
	Enumerator_t442692252 * _thisAdjusted = reinterpret_cast<Enumerator_t442692252 *>(__this + 1);
	Enumerator__ctor_m661036428(_thisAdjusted, ___dictionary0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1692692619_gshared (Enumerator_t442692252 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3330382363((Enumerator_t442692252 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1174980068  L_0 = (KeyValuePair_2_t1174980068 )__this->get_current_3();
		KeyValuePair_2_t1174980068  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1692692619_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t442692252 * _thisAdjusted = reinterpret_cast<Enumerator_t442692252 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1692692619(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m70453843_gshared (Enumerator_t442692252 * __this, const MethodInfo* method)
{
	{
		Enumerator_Reset_m3115320746((Enumerator_t442692252 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m70453843_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t442692252 * _thisAdjusted = reinterpret_cast<Enumerator_t442692252 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m70453843(_thisAdjusted, method);
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3667889028_gshared (Enumerator_t442692252 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3330382363((Enumerator_t442692252 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1174980068 * L_0 = (KeyValuePair_2_t1174980068 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Key_m2113318928((KeyValuePair_2_t1174980068 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		KeyValuePair_2_t1174980068 * L_2 = (KeyValuePair_2_t1174980068 *)__this->get_address_of_current_3();
		bool L_3 = KeyValuePair_2_get_Value_m1916631176((KeyValuePair_2_t1174980068 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		bool L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_4);
		DictionaryEntry_t3048875398  L_6;
		memset(&L_6, 0, sizeof(L_6));
		DictionaryEntry__ctor_m2901884110(&L_6, (Il2CppObject *)L_1, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3667889028_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t442692252 * _thisAdjusted = reinterpret_cast<Enumerator_t442692252 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3667889028(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1214978221_gshared (Enumerator_t442692252 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = Enumerator_get_CurrentKey_m565000604((Enumerator_t442692252 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		return L_0;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1214978221_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t442692252 * _thisAdjusted = reinterpret_cast<Enumerator_t442692252 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1214978221(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m313528997_gshared (Enumerator_t442692252 * __this, const MethodInfo* method)
{
	{
		bool L_0 = Enumerator_get_CurrentValue_m4143929484((Enumerator_t442692252 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		bool L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m313528997_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t442692252 * _thisAdjusted = reinterpret_cast<Enumerator_t442692252 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m313528997(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1856697671_gshared (Enumerator_t442692252 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Enumerator_VerifyState_m1165543189((Enumerator_t442692252 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		goto IL_007b;
	}

IL_0019:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		int32_t L_2 = (int32_t)L_1;
		V_1 = (int32_t)L_2;
		__this->set_next_1(((int32_t)((int32_t)L_2+(int32_t)1)));
		int32_t L_3 = V_1;
		V_0 = (int32_t)L_3;
		Dictionary_2_t3417634846 * L_4 = (Dictionary_2_t3417634846 *)__this->get_dictionary_0();
		NullCheck(L_4);
		LinkU5BU5D_t62501539* L_5 = (LinkU5BU5D_t62501539*)L_4->get_linkSlots_5();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		int32_t L_7 = (int32_t)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_7&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_007b;
		}
	}
	{
		Dictionary_2_t3417634846 * L_8 = (Dictionary_2_t3417634846 *)__this->get_dictionary_0();
		NullCheck(L_8);
		ObjectU5BU5D_t3614634134* L_9 = (ObjectU5BU5D_t3614634134*)L_8->get_keySlots_6();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		Il2CppObject * L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		Dictionary_2_t3417634846 * L_13 = (Dictionary_2_t3417634846 *)__this->get_dictionary_0();
		NullCheck(L_13);
		BooleanU5BU5D_t3568034315* L_14 = (BooleanU5BU5D_t3568034315*)L_13->get_valueSlots_7();
		int32_t L_15 = V_0;
		NullCheck(L_14);
		int32_t L_16 = L_15;
		bool L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		KeyValuePair_2_t1174980068  L_18;
		memset(&L_18, 0, sizeof(L_18));
		KeyValuePair_2__ctor_m4040336782(&L_18, (Il2CppObject *)L_12, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		__this->set_current_3(L_18);
		return (bool)1;
	}

IL_007b:
	{
		int32_t L_19 = (int32_t)__this->get_next_1();
		Dictionary_2_t3417634846 * L_20 = (Dictionary_2_t3417634846 *)__this->get_dictionary_0();
		NullCheck(L_20);
		int32_t L_21 = (int32_t)L_20->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_21)))
		{
			goto IL_0019;
		}
	}
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m1856697671_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t442692252 * _thisAdjusted = reinterpret_cast<Enumerator_t442692252 *>(__this + 1);
	return Enumerator_MoveNext_m1856697671(_thisAdjusted, method);
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::get_Current()
extern "C"  KeyValuePair_2_t1174980068  Enumerator_get_Current_m1020413567_gshared (Enumerator_t442692252 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t1174980068  L_0 = (KeyValuePair_2_t1174980068 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  KeyValuePair_2_t1174980068  Enumerator_get_Current_m1020413567_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t442692252 * _thisAdjusted = reinterpret_cast<Enumerator_t442692252 *>(__this + 1);
	return Enumerator_get_Current_m1020413567(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::get_CurrentKey()
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m565000604_gshared (Enumerator_t442692252 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3330382363((Enumerator_t442692252 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1174980068 * L_0 = (KeyValuePair_2_t1174980068 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Key_m2113318928((KeyValuePair_2_t1174980068 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m565000604_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t442692252 * _thisAdjusted = reinterpret_cast<Enumerator_t442692252 *>(__this + 1);
	return Enumerator_get_CurrentKey_m565000604(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::get_CurrentValue()
extern "C"  bool Enumerator_get_CurrentValue_m4143929484_gshared (Enumerator_t442692252 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3330382363((Enumerator_t442692252 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1174980068 * L_0 = (KeyValuePair_2_t1174980068 *)__this->get_address_of_current_3();
		bool L_1 = KeyValuePair_2_get_Value_m1916631176((KeyValuePair_2_t1174980068 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_get_CurrentValue_m4143929484_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t442692252 * _thisAdjusted = reinterpret_cast<Enumerator_t442692252 *>(__this + 1);
	return Enumerator_get_CurrentValue_m4143929484(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::Reset()
extern "C"  void Enumerator_Reset_m3115320746_gshared (Enumerator_t442692252 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m1165543189((Enumerator_t442692252 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_Reset_m3115320746_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t442692252 * _thisAdjusted = reinterpret_cast<Enumerator_t442692252 *>(__this + 1);
	Enumerator_Reset_m3115320746(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral671556834;
extern const uint32_t Enumerator_VerifyState_m1165543189_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m1165543189_gshared (Enumerator_t442692252 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m1165543189_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t3417634846 * L_0 = (Dictionary_2_t3417634846 *)__this->get_dictionary_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		Dictionary_2_t3417634846 * L_2 = (Dictionary_2_t3417634846 *)__this->get_dictionary_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_generation_14();
		int32_t L_4 = (int32_t)__this->get_stamp_2();
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t721527559 * L_5 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_5, (String_t*)_stringLiteral671556834, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m1165543189_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t442692252 * _thisAdjusted = reinterpret_cast<Enumerator_t442692252 *>(__this + 1);
	Enumerator_VerifyState_m1165543189(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::VerifyCurrent()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2382073330;
extern const uint32_t Enumerator_VerifyCurrent_m3330382363_MetadataUsageId;
extern "C"  void Enumerator_VerifyCurrent_m3330382363_gshared (Enumerator_t442692252 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyCurrent_m3330382363_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m1165543189((Enumerator_t442692252 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2382073330, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001d:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyCurrent_m3330382363_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t442692252 * _thisAdjusted = reinterpret_cast<Enumerator_t442692252 *>(__this + 1);
	Enumerator_VerifyCurrent_m3330382363(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::Dispose()
extern "C"  void Enumerator_Dispose_m2711120408_gshared (Enumerator_t442692252 * __this, const MethodInfo* method)
{
	{
		__this->set_dictionary_0((Dictionary_2_t3417634846 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2711120408_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t442692252 * _thisAdjusted = reinterpret_cast<Enumerator_t442692252 *>(__this + 1);
	Enumerator_Dispose_m2711120408(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3597047336_gshared (Enumerator_t2983962278 * __this, Dictionary_2_t1663937576 * ___dictionary0, const MethodInfo* method)
{
	{
		Dictionary_2_t1663937576 * L_0 = ___dictionary0;
		__this->set_dictionary_0(L_0);
		Dictionary_2_t1663937576 * L_1 = ___dictionary0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_generation_14();
		__this->set_stamp_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3597047336_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t1663937576 * ___dictionary0, const MethodInfo* method)
{
	Enumerator_t2983962278 * _thisAdjusted = reinterpret_cast<Enumerator_t2983962278 *>(__this + 1);
	Enumerator__ctor_m3597047336(_thisAdjusted, ___dictionary0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2010873149_gshared (Enumerator_t2983962278 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3278789713((Enumerator_t2983962278 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3716250094  L_0 = (KeyValuePair_2_t3716250094 )__this->get_current_3();
		KeyValuePair_2_t3716250094  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2010873149_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2983962278 * _thisAdjusted = reinterpret_cast<Enumerator_t2983962278 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2010873149(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3085583937_gshared (Enumerator_t2983962278 * __this, const MethodInfo* method)
{
	{
		Enumerator_Reset_m1132695838((Enumerator_t2983962278 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3085583937_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2983962278 * _thisAdjusted = reinterpret_cast<Enumerator_t2983962278 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m3085583937(_thisAdjusted, method);
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m487599172_gshared (Enumerator_t2983962278 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3278789713((Enumerator_t2983962278 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3716250094 * L_0 = (KeyValuePair_2_t3716250094 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Key_m1454531804((KeyValuePair_2_t3716250094 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		KeyValuePair_2_t3716250094 * L_2 = (KeyValuePair_2_t3716250094 *)__this->get_address_of_current_3();
		int32_t L_3 = KeyValuePair_2_get_Value_m3699669100((KeyValuePair_2_t3716250094 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		int32_t L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_4);
		DictionaryEntry_t3048875398  L_6;
		memset(&L_6, 0, sizeof(L_6));
		DictionaryEntry__ctor_m2901884110(&L_6, (Il2CppObject *)L_1, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m487599172_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2983962278 * _thisAdjusted = reinterpret_cast<Enumerator_t2983962278 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m487599172(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m677423231_gshared (Enumerator_t2983962278 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = Enumerator_get_CurrentKey_m1408186928((Enumerator_t2983962278 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		return L_0;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m677423231_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2983962278 * _thisAdjusted = reinterpret_cast<Enumerator_t2983962278 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m677423231(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3005608231_gshared (Enumerator_t2983962278 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Enumerator_get_CurrentValue_m2645962456((Enumerator_t2983962278 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3005608231_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2983962278 * _thisAdjusted = reinterpret_cast<Enumerator_t2983962278 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3005608231(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m435964161_gshared (Enumerator_t2983962278 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Enumerator_VerifyState_m3173176371((Enumerator_t2983962278 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		goto IL_007b;
	}

IL_0019:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		int32_t L_2 = (int32_t)L_1;
		V_1 = (int32_t)L_2;
		__this->set_next_1(((int32_t)((int32_t)L_2+(int32_t)1)));
		int32_t L_3 = V_1;
		V_0 = (int32_t)L_3;
		Dictionary_2_t1663937576 * L_4 = (Dictionary_2_t1663937576 *)__this->get_dictionary_0();
		NullCheck(L_4);
		LinkU5BU5D_t62501539* L_5 = (LinkU5BU5D_t62501539*)L_4->get_linkSlots_5();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		int32_t L_7 = (int32_t)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_7&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_007b;
		}
	}
	{
		Dictionary_2_t1663937576 * L_8 = (Dictionary_2_t1663937576 *)__this->get_dictionary_0();
		NullCheck(L_8);
		ObjectU5BU5D_t3614634134* L_9 = (ObjectU5BU5D_t3614634134*)L_8->get_keySlots_6();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		Il2CppObject * L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		Dictionary_2_t1663937576 * L_13 = (Dictionary_2_t1663937576 *)__this->get_dictionary_0();
		NullCheck(L_13);
		Int32U5BU5D_t3030399641* L_14 = (Int32U5BU5D_t3030399641*)L_13->get_valueSlots_7();
		int32_t L_15 = V_0;
		NullCheck(L_14);
		int32_t L_16 = L_15;
		int32_t L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		KeyValuePair_2_t3716250094  L_18;
		memset(&L_18, 0, sizeof(L_18));
		KeyValuePair_2__ctor_m1877755778(&L_18, (Il2CppObject *)L_12, (int32_t)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		__this->set_current_3(L_18);
		return (bool)1;
	}

IL_007b:
	{
		int32_t L_19 = (int32_t)__this->get_next_1();
		Dictionary_2_t1663937576 * L_20 = (Dictionary_2_t1663937576 *)__this->get_dictionary_0();
		NullCheck(L_20);
		int32_t L_21 = (int32_t)L_20->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_21)))
		{
			goto IL_0019;
		}
	}
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m435964161_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2983962278 * _thisAdjusted = reinterpret_cast<Enumerator_t2983962278 *>(__this + 1);
	return Enumerator_MoveNext_m435964161(_thisAdjusted, method);
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::get_Current()
extern "C"  KeyValuePair_2_t3716250094  Enumerator_get_Current_m1932198897_gshared (Enumerator_t2983962278 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t3716250094  L_0 = (KeyValuePair_2_t3716250094 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  KeyValuePair_2_t3716250094  Enumerator_get_Current_m1932198897_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2983962278 * _thisAdjusted = reinterpret_cast<Enumerator_t2983962278 *>(__this + 1);
	return Enumerator_get_Current_m1932198897(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::get_CurrentKey()
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m1408186928_gshared (Enumerator_t2983962278 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3278789713((Enumerator_t2983962278 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3716250094 * L_0 = (KeyValuePair_2_t3716250094 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Key_m1454531804((KeyValuePair_2_t3716250094 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m1408186928_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2983962278 * _thisAdjusted = reinterpret_cast<Enumerator_t2983962278 *>(__this + 1);
	return Enumerator_get_CurrentKey_m1408186928(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::get_CurrentValue()
extern "C"  int32_t Enumerator_get_CurrentValue_m2645962456_gshared (Enumerator_t2983962278 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3278789713((Enumerator_t2983962278 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3716250094 * L_0 = (KeyValuePair_2_t3716250094 *)__this->get_address_of_current_3();
		int32_t L_1 = KeyValuePair_2_get_Value_m3699669100((KeyValuePair_2_t3716250094 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  int32_t Enumerator_get_CurrentValue_m2645962456_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2983962278 * _thisAdjusted = reinterpret_cast<Enumerator_t2983962278 *>(__this + 1);
	return Enumerator_get_CurrentValue_m2645962456(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::Reset()
extern "C"  void Enumerator_Reset_m1132695838_gshared (Enumerator_t2983962278 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m3173176371((Enumerator_t2983962278 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_Reset_m1132695838_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2983962278 * _thisAdjusted = reinterpret_cast<Enumerator_t2983962278 *>(__this + 1);
	Enumerator_Reset_m1132695838(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral671556834;
extern const uint32_t Enumerator_VerifyState_m3173176371_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m3173176371_gshared (Enumerator_t2983962278 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m3173176371_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t1663937576 * L_0 = (Dictionary_2_t1663937576 *)__this->get_dictionary_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		Dictionary_2_t1663937576 * L_2 = (Dictionary_2_t1663937576 *)__this->get_dictionary_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_generation_14();
		int32_t L_4 = (int32_t)__this->get_stamp_2();
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t721527559 * L_5 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_5, (String_t*)_stringLiteral671556834, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m3173176371_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2983962278 * _thisAdjusted = reinterpret_cast<Enumerator_t2983962278 *>(__this + 1);
	Enumerator_VerifyState_m3173176371(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::VerifyCurrent()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2382073330;
extern const uint32_t Enumerator_VerifyCurrent_m3278789713_MetadataUsageId;
extern "C"  void Enumerator_VerifyCurrent_m3278789713_gshared (Enumerator_t2983962278 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyCurrent_m3278789713_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m3173176371((Enumerator_t2983962278 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2382073330, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001d:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyCurrent_m3278789713_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2983962278 * _thisAdjusted = reinterpret_cast<Enumerator_t2983962278 *>(__this + 1);
	Enumerator_VerifyCurrent_m3278789713(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m401572848_gshared (Enumerator_t2983962278 * __this, const MethodInfo* method)
{
	{
		__this->set_dictionary_0((Dictionary_2_t1663937576 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m401572848_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2983962278 * _thisAdjusted = reinterpret_cast<Enumerator_t2983962278 *>(__this + 1);
	Enumerator_Dispose_m401572848(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3742107451_gshared (Enumerator_t3601534125 * __this, Dictionary_2_t2281509423 * ___dictionary0, const MethodInfo* method)
{
	{
		Dictionary_2_t2281509423 * L_0 = ___dictionary0;
		__this->set_dictionary_0(L_0);
		Dictionary_2_t2281509423 * L_1 = ___dictionary0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_generation_14();
		__this->set_stamp_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3742107451_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t2281509423 * ___dictionary0, const MethodInfo* method)
{
	Enumerator_t3601534125 * _thisAdjusted = reinterpret_cast<Enumerator_t3601534125 *>(__this + 1);
	Enumerator__ctor_m3742107451(_thisAdjusted, ___dictionary0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m229223308_gshared (Enumerator_t3601534125 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m1702320752((Enumerator_t3601534125 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t38854645  L_0 = (KeyValuePair_2_t38854645 )__this->get_current_3();
		KeyValuePair_2_t38854645  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m229223308_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3601534125 * _thisAdjusted = reinterpret_cast<Enumerator_t3601534125 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m229223308(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3225937576_gshared (Enumerator_t3601534125 * __this, const MethodInfo* method)
{
	{
		Enumerator_Reset_m3129803197((Enumerator_t3601534125 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3225937576_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3601534125 * _thisAdjusted = reinterpret_cast<Enumerator_t3601534125 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m3225937576(_thisAdjusted, method);
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m221119093_gshared (Enumerator_t3601534125 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m1702320752((Enumerator_t3601534125 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t38854645 * L_0 = (KeyValuePair_2_t38854645 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Key_m2561166459((KeyValuePair_2_t38854645 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		KeyValuePair_2_t38854645 * L_2 = (KeyValuePair_2_t38854645 *)__this->get_address_of_current_3();
		Il2CppObject * L_3 = KeyValuePair_2_get_Value_m499643803((KeyValuePair_2_t38854645 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		DictionaryEntry_t3048875398  L_4;
		memset(&L_4, 0, sizeof(L_4));
		DictionaryEntry__ctor_m2901884110(&L_4, (Il2CppObject *)L_1, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m221119093_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3601534125 * _thisAdjusted = reinterpret_cast<Enumerator_t3601534125 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m221119093(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m467957770_gshared (Enumerator_t3601534125 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = Enumerator_get_CurrentKey_m3839846791((Enumerator_t3601534125 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		return L_0;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m467957770_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3601534125 * _thisAdjusted = reinterpret_cast<Enumerator_t3601534125 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m467957770(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2325383168_gshared (Enumerator_t3601534125 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = Enumerator_get_CurrentValue_m402763047((Enumerator_t3601534125 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return L_0;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2325383168_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3601534125 * _thisAdjusted = reinterpret_cast<Enumerator_t3601534125 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2325383168(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3349738440_gshared (Enumerator_t3601534125 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Enumerator_VerifyState_m262343092((Enumerator_t3601534125 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		goto IL_007b;
	}

IL_0019:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		int32_t L_2 = (int32_t)L_1;
		V_1 = (int32_t)L_2;
		__this->set_next_1(((int32_t)((int32_t)L_2+(int32_t)1)));
		int32_t L_3 = V_1;
		V_0 = (int32_t)L_3;
		Dictionary_2_t2281509423 * L_4 = (Dictionary_2_t2281509423 *)__this->get_dictionary_0();
		NullCheck(L_4);
		LinkU5BU5D_t62501539* L_5 = (LinkU5BU5D_t62501539*)L_4->get_linkSlots_5();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		int32_t L_7 = (int32_t)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_7&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_007b;
		}
	}
	{
		Dictionary_2_t2281509423 * L_8 = (Dictionary_2_t2281509423 *)__this->get_dictionary_0();
		NullCheck(L_8);
		ObjectU5BU5D_t3614634134* L_9 = (ObjectU5BU5D_t3614634134*)L_8->get_keySlots_6();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		Il2CppObject * L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		Dictionary_2_t2281509423 * L_13 = (Dictionary_2_t2281509423 *)__this->get_dictionary_0();
		NullCheck(L_13);
		ObjectU5BU5D_t3614634134* L_14 = (ObjectU5BU5D_t3614634134*)L_13->get_valueSlots_7();
		int32_t L_15 = V_0;
		NullCheck(L_14);
		int32_t L_16 = L_15;
		Il2CppObject * L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		KeyValuePair_2_t38854645  L_18;
		memset(&L_18, 0, sizeof(L_18));
		KeyValuePair_2__ctor_m1640124561(&L_18, (Il2CppObject *)L_12, (Il2CppObject *)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		__this->set_current_3(L_18);
		return (bool)1;
	}

IL_007b:
	{
		int32_t L_19 = (int32_t)__this->get_next_1();
		Dictionary_2_t2281509423 * L_20 = (Dictionary_2_t2281509423 *)__this->get_dictionary_0();
		NullCheck(L_20);
		int32_t L_21 = (int32_t)L_20->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_21)))
		{
			goto IL_0019;
		}
	}
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m3349738440_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3601534125 * _thisAdjusted = reinterpret_cast<Enumerator_t3601534125 *>(__this + 1);
	return Enumerator_MoveNext_m3349738440(_thisAdjusted, method);
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t38854645  Enumerator_get_Current_m25299632_gshared (Enumerator_t3601534125 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t38854645  L_0 = (KeyValuePair_2_t38854645 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  KeyValuePair_2_t38854645  Enumerator_get_Current_m25299632_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3601534125 * _thisAdjusted = reinterpret_cast<Enumerator_t3601534125 *>(__this + 1);
	return Enumerator_get_Current_m25299632(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::get_CurrentKey()
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m3839846791_gshared (Enumerator_t3601534125 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m1702320752((Enumerator_t3601534125 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t38854645 * L_0 = (KeyValuePair_2_t38854645 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Key_m2561166459((KeyValuePair_2_t38854645 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m3839846791_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3601534125 * _thisAdjusted = reinterpret_cast<Enumerator_t3601534125 *>(__this + 1);
	return Enumerator_get_CurrentKey_m3839846791(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::get_CurrentValue()
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m402763047_gshared (Enumerator_t3601534125 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m1702320752((Enumerator_t3601534125 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t38854645 * L_0 = (KeyValuePair_2_t38854645 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Value_m499643803((KeyValuePair_2_t38854645 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m402763047_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3601534125 * _thisAdjusted = reinterpret_cast<Enumerator_t3601534125 *>(__this + 1);
	return Enumerator_get_CurrentValue_m402763047(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::Reset()
extern "C"  void Enumerator_Reset_m3129803197_gshared (Enumerator_t3601534125 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m262343092((Enumerator_t3601534125 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_Reset_m3129803197_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3601534125 * _thisAdjusted = reinterpret_cast<Enumerator_t3601534125 *>(__this + 1);
	Enumerator_Reset_m3129803197(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral671556834;
extern const uint32_t Enumerator_VerifyState_m262343092_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m262343092_gshared (Enumerator_t3601534125 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m262343092_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t2281509423 * L_0 = (Dictionary_2_t2281509423 *)__this->get_dictionary_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		Dictionary_2_t2281509423 * L_2 = (Dictionary_2_t2281509423 *)__this->get_dictionary_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_generation_14();
		int32_t L_4 = (int32_t)__this->get_stamp_2();
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t721527559 * L_5 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_5, (String_t*)_stringLiteral671556834, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m262343092_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3601534125 * _thisAdjusted = reinterpret_cast<Enumerator_t3601534125 *>(__this + 1);
	Enumerator_VerifyState_m262343092(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::VerifyCurrent()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2382073330;
extern const uint32_t Enumerator_VerifyCurrent_m1702320752_MetadataUsageId;
extern "C"  void Enumerator_VerifyCurrent_m1702320752_gshared (Enumerator_t3601534125 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyCurrent_m1702320752_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m262343092((Enumerator_t3601534125 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2382073330, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001d:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyCurrent_m1702320752_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3601534125 * _thisAdjusted = reinterpret_cast<Enumerator_t3601534125 *>(__this + 1);
	Enumerator_VerifyCurrent_m1702320752(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m1905011127_gshared (Enumerator_t3601534125 * __this, const MethodInfo* method)
{
	{
		__this->set_dictionary_0((Dictionary_2_t2281509423 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m1905011127_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3601534125 * _thisAdjusted = reinterpret_cast<Enumerator_t3601534125 *>(__this + 1);
	Enumerator_Dispose_m1905011127(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1167548803_gshared (Enumerator_t2047162844 * __this, Dictionary_2_t727138142 * ___dictionary0, const MethodInfo* method)
{
	{
		Dictionary_2_t727138142 * L_0 = ___dictionary0;
		__this->set_dictionary_0(L_0);
		Dictionary_2_t727138142 * L_1 = ___dictionary0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_generation_14();
		__this->set_stamp_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1167548803_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t727138142 * ___dictionary0, const MethodInfo* method)
{
	Enumerator_t2047162844 * _thisAdjusted = reinterpret_cast<Enumerator_t2047162844 *>(__this + 1);
	Enumerator__ctor_m1167548803(_thisAdjusted, ___dictionary0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2209161860_gshared (Enumerator_t2047162844 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3852554688((Enumerator_t2047162844 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t2779450660  L_0 = (KeyValuePair_2_t2779450660 )__this->get_current_3();
		KeyValuePair_2_t2779450660  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2209161860_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2047162844 * _thisAdjusted = reinterpret_cast<Enumerator_t2047162844 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2209161860(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2326843624_gshared (Enumerator_t2047162844 * __this, const MethodInfo* method)
{
	{
		Enumerator_Reset_m2586713469((Enumerator_t2047162844 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2326843624_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2047162844 * _thisAdjusted = reinterpret_cast<Enumerator_t2047162844 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m2326843624(_thisAdjusted, method);
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2138280373_gshared (Enumerator_t2047162844 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3852554688((Enumerator_t2047162844 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t2779450660 * L_0 = (KeyValuePair_2_t2779450660 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Key_m1489805227((KeyValuePair_2_t2779450660 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		KeyValuePair_2_t2779450660 * L_2 = (KeyValuePair_2_t2779450660 *)__this->get_address_of_current_3();
		ArrayMetadata_t1135078014  L_3 = KeyValuePair_2_get_Value_m3869691915((KeyValuePair_2_t2779450660 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		ArrayMetadata_t1135078014  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_4);
		DictionaryEntry_t3048875398  L_6;
		memset(&L_6, 0, sizeof(L_6));
		DictionaryEntry__ctor_m2901884110(&L_6, (Il2CppObject *)L_1, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2138280373_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2047162844 * _thisAdjusted = reinterpret_cast<Enumerator_t2047162844 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2138280373(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1315557434_gshared (Enumerator_t2047162844 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = Enumerator_get_CurrentKey_m3038333311((Enumerator_t2047162844 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		return L_0;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1315557434_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2047162844 * _thisAdjusted = reinterpret_cast<Enumerator_t2047162844 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1315557434(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m4200658056_gshared (Enumerator_t2047162844 * __this, const MethodInfo* method)
{
	{
		ArrayMetadata_t1135078014  L_0 = Enumerator_get_CurrentValue_m2382161119((Enumerator_t2047162844 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		ArrayMetadata_t1135078014  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m4200658056_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2047162844 * _thisAdjusted = reinterpret_cast<Enumerator_t2047162844 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m4200658056(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2137572552_gshared (Enumerator_t2047162844 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Enumerator_VerifyState_m2203483644((Enumerator_t2047162844 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		goto IL_007b;
	}

IL_0019:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		int32_t L_2 = (int32_t)L_1;
		V_1 = (int32_t)L_2;
		__this->set_next_1(((int32_t)((int32_t)L_2+(int32_t)1)));
		int32_t L_3 = V_1;
		V_0 = (int32_t)L_3;
		Dictionary_2_t727138142 * L_4 = (Dictionary_2_t727138142 *)__this->get_dictionary_0();
		NullCheck(L_4);
		LinkU5BU5D_t62501539* L_5 = (LinkU5BU5D_t62501539*)L_4->get_linkSlots_5();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		int32_t L_7 = (int32_t)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_7&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_007b;
		}
	}
	{
		Dictionary_2_t727138142 * L_8 = (Dictionary_2_t727138142 *)__this->get_dictionary_0();
		NullCheck(L_8);
		ObjectU5BU5D_t3614634134* L_9 = (ObjectU5BU5D_t3614634134*)L_8->get_keySlots_6();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		Il2CppObject * L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		Dictionary_2_t727138142 * L_13 = (Dictionary_2_t727138142 *)__this->get_dictionary_0();
		NullCheck(L_13);
		ArrayMetadataU5BU5D_t3927266763* L_14 = (ArrayMetadataU5BU5D_t3927266763*)L_13->get_valueSlots_7();
		int32_t L_15 = V_0;
		NullCheck(L_14);
		int32_t L_16 = L_15;
		ArrayMetadata_t1135078014  L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		KeyValuePair_2_t2779450660  L_18;
		memset(&L_18, 0, sizeof(L_18));
		KeyValuePair_2__ctor_m3859112953(&L_18, (Il2CppObject *)L_12, (ArrayMetadata_t1135078014 )L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		__this->set_current_3(L_18);
		return (bool)1;
	}

IL_007b:
	{
		int32_t L_19 = (int32_t)__this->get_next_1();
		Dictionary_2_t727138142 * L_20 = (Dictionary_2_t727138142 *)__this->get_dictionary_0();
		NullCheck(L_20);
		int32_t L_21 = (int32_t)L_20->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_21)))
		{
			goto IL_0019;
		}
	}
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m2137572552_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2047162844 * _thisAdjusted = reinterpret_cast<Enumerator_t2047162844 *>(__this + 1);
	return Enumerator_MoveNext_m2137572552(_thisAdjusted, method);
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::get_Current()
extern "C"  KeyValuePair_2_t2779450660  Enumerator_get_Current_m923671264_gshared (Enumerator_t2047162844 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t2779450660  L_0 = (KeyValuePair_2_t2779450660 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  KeyValuePair_2_t2779450660  Enumerator_get_Current_m923671264_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2047162844 * _thisAdjusted = reinterpret_cast<Enumerator_t2047162844 *>(__this + 1);
	return Enumerator_get_Current_m923671264(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::get_CurrentKey()
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m3038333311_gshared (Enumerator_t2047162844 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3852554688((Enumerator_t2047162844 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t2779450660 * L_0 = (KeyValuePair_2_t2779450660 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Key_m1489805227((KeyValuePair_2_t2779450660 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m3038333311_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2047162844 * _thisAdjusted = reinterpret_cast<Enumerator_t2047162844 *>(__this + 1);
	return Enumerator_get_CurrentKey_m3038333311(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::get_CurrentValue()
extern "C"  ArrayMetadata_t1135078014  Enumerator_get_CurrentValue_m2382161119_gshared (Enumerator_t2047162844 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3852554688((Enumerator_t2047162844 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t2779450660 * L_0 = (KeyValuePair_2_t2779450660 *)__this->get_address_of_current_3();
		ArrayMetadata_t1135078014  L_1 = KeyValuePair_2_get_Value_m3869691915((KeyValuePair_2_t2779450660 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  ArrayMetadata_t1135078014  Enumerator_get_CurrentValue_m2382161119_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2047162844 * _thisAdjusted = reinterpret_cast<Enumerator_t2047162844 *>(__this + 1);
	return Enumerator_get_CurrentValue_m2382161119(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::Reset()
extern "C"  void Enumerator_Reset_m2586713469_gshared (Enumerator_t2047162844 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m2203483644((Enumerator_t2047162844 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_Reset_m2586713469_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2047162844 * _thisAdjusted = reinterpret_cast<Enumerator_t2047162844 *>(__this + 1);
	Enumerator_Reset_m2586713469(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral671556834;
extern const uint32_t Enumerator_VerifyState_m2203483644_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m2203483644_gshared (Enumerator_t2047162844 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m2203483644_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t727138142 * L_0 = (Dictionary_2_t727138142 *)__this->get_dictionary_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		Dictionary_2_t727138142 * L_2 = (Dictionary_2_t727138142 *)__this->get_dictionary_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_generation_14();
		int32_t L_4 = (int32_t)__this->get_stamp_2();
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t721527559 * L_5 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_5, (String_t*)_stringLiteral671556834, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m2203483644_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2047162844 * _thisAdjusted = reinterpret_cast<Enumerator_t2047162844 *>(__this + 1);
	Enumerator_VerifyState_m2203483644(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::VerifyCurrent()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2382073330;
extern const uint32_t Enumerator_VerifyCurrent_m3852554688_MetadataUsageId;
extern "C"  void Enumerator_VerifyCurrent_m3852554688_gshared (Enumerator_t2047162844 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyCurrent_m3852554688_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m2203483644((Enumerator_t2047162844 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2382073330, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001d:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyCurrent_m3852554688_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2047162844 * _thisAdjusted = reinterpret_cast<Enumerator_t2047162844 *>(__this + 1);
	Enumerator_VerifyCurrent_m3852554688(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::Dispose()
extern "C"  void Enumerator_Dispose_m4092052839_gshared (Enumerator_t2047162844 * __this, const MethodInfo* method)
{
	{
		__this->set_dictionary_0((Dictionary_2_t727138142 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m4092052839_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2047162844 * _thisAdjusted = reinterpret_cast<Enumerator_t2047162844 *>(__this + 1);
	Enumerator_Dispose_m4092052839(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3882269915_gshared (Enumerator_t675255274 * __this, Dictionary_2_t3650197868 * ___dictionary0, const MethodInfo* method)
{
	{
		Dictionary_2_t3650197868 * L_0 = ___dictionary0;
		__this->set_dictionary_0(L_0);
		Dictionary_2_t3650197868 * L_1 = ___dictionary0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_generation_14();
		__this->set_stamp_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3882269915_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t3650197868 * ___dictionary0, const MethodInfo* method)
{
	Enumerator_t675255274 * _thisAdjusted = reinterpret_cast<Enumerator_t675255274 *>(__this + 1);
	Enumerator__ctor_m3882269915(_thisAdjusted, ___dictionary0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3006262826_gshared (Enumerator_t675255274 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m424726820((Enumerator_t675255274 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1407543090  L_0 = (KeyValuePair_2_t1407543090 )__this->get_current_3();
		KeyValuePair_2_t1407543090  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3006262826_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t675255274 * _thisAdjusted = reinterpret_cast<Enumerator_t675255274 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m3006262826(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m786642932_gshared (Enumerator_t675255274 * __this, const MethodInfo* method)
{
	{
		Enumerator_Reset_m975923089((Enumerator_t675255274 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m786642932_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t675255274 * _thisAdjusted = reinterpret_cast<Enumerator_t675255274 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m786642932(_thisAdjusted, method);
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3805059129_gshared (Enumerator_t675255274 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m424726820((Enumerator_t675255274 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1407543090 * L_0 = (KeyValuePair_2_t1407543090 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Key_m2438555343((KeyValuePair_2_t1407543090 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		KeyValuePair_2_t1407543090 * L_2 = (KeyValuePair_2_t1407543090 *)__this->get_address_of_current_3();
		ObjectMetadata_t4058137740  L_3 = KeyValuePair_2_get_Value_m3741940767((KeyValuePair_2_t1407543090 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		ObjectMetadata_t4058137740  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_4);
		DictionaryEntry_t3048875398  L_6;
		memset(&L_6, 0, sizeof(L_6));
		DictionaryEntry__ctor_m2901884110(&L_6, (Il2CppObject *)L_1, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3805059129_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t675255274 * _thisAdjusted = reinterpret_cast<Enumerator_t675255274 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3805059129(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3135173292_gshared (Enumerator_t675255274 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = Enumerator_get_CurrentKey_m329261871((Enumerator_t675255274 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		return L_0;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3135173292_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t675255274 * _thisAdjusted = reinterpret_cast<Enumerator_t675255274 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3135173292(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3285338134_gshared (Enumerator_t675255274 * __this, const MethodInfo* method)
{
	{
		ObjectMetadata_t4058137740  L_0 = Enumerator_get_CurrentValue_m2906228503((Enumerator_t675255274 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		ObjectMetadata_t4058137740  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3285338134_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t675255274 * _thisAdjusted = reinterpret_cast<Enumerator_t675255274 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3285338134(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m513463628_gshared (Enumerator_t675255274 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Enumerator_VerifyState_m3950116612((Enumerator_t675255274 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		goto IL_007b;
	}

IL_0019:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		int32_t L_2 = (int32_t)L_1;
		V_1 = (int32_t)L_2;
		__this->set_next_1(((int32_t)((int32_t)L_2+(int32_t)1)));
		int32_t L_3 = V_1;
		V_0 = (int32_t)L_3;
		Dictionary_2_t3650197868 * L_4 = (Dictionary_2_t3650197868 *)__this->get_dictionary_0();
		NullCheck(L_4);
		LinkU5BU5D_t62501539* L_5 = (LinkU5BU5D_t62501539*)L_4->get_linkSlots_5();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		int32_t L_7 = (int32_t)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_7&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_007b;
		}
	}
	{
		Dictionary_2_t3650197868 * L_8 = (Dictionary_2_t3650197868 *)__this->get_dictionary_0();
		NullCheck(L_8);
		ObjectU5BU5D_t3614634134* L_9 = (ObjectU5BU5D_t3614634134*)L_8->get_keySlots_6();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		Il2CppObject * L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		Dictionary_2_t3650197868 * L_13 = (Dictionary_2_t3650197868 *)__this->get_dictionary_0();
		NullCheck(L_13);
		ObjectMetadataU5BU5D_t1554594757* L_14 = (ObjectMetadataU5BU5D_t1554594757*)L_13->get_valueSlots_7();
		int32_t L_15 = V_0;
		NullCheck(L_14);
		int32_t L_16 = L_15;
		ObjectMetadata_t4058137740  L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		KeyValuePair_2_t1407543090  L_18;
		memset(&L_18, 0, sizeof(L_18));
		KeyValuePair_2__ctor_m1145061153(&L_18, (Il2CppObject *)L_12, (ObjectMetadata_t4058137740 )L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		__this->set_current_3(L_18);
		return (bool)1;
	}

IL_007b:
	{
		int32_t L_19 = (int32_t)__this->get_next_1();
		Dictionary_2_t3650197868 * L_20 = (Dictionary_2_t3650197868 *)__this->get_dictionary_0();
		NullCheck(L_20);
		int32_t L_21 = (int32_t)L_20->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_21)))
		{
			goto IL_0019;
		}
	}
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m513463628_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t675255274 * _thisAdjusted = reinterpret_cast<Enumerator_t675255274 *>(__this + 1);
	return Enumerator_MoveNext_m513463628(_thisAdjusted, method);
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::get_Current()
extern "C"  KeyValuePair_2_t1407543090  Enumerator_get_Current_m2333579100_gshared (Enumerator_t675255274 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t1407543090  L_0 = (KeyValuePair_2_t1407543090 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  KeyValuePair_2_t1407543090  Enumerator_get_Current_m2333579100_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t675255274 * _thisAdjusted = reinterpret_cast<Enumerator_t675255274 *>(__this + 1);
	return Enumerator_get_Current_m2333579100(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::get_CurrentKey()
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m329261871_gshared (Enumerator_t675255274 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m424726820((Enumerator_t675255274 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1407543090 * L_0 = (KeyValuePair_2_t1407543090 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Key_m2438555343((KeyValuePair_2_t1407543090 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m329261871_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t675255274 * _thisAdjusted = reinterpret_cast<Enumerator_t675255274 *>(__this + 1);
	return Enumerator_get_CurrentKey_m329261871(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::get_CurrentValue()
extern "C"  ObjectMetadata_t4058137740  Enumerator_get_CurrentValue_m2906228503_gshared (Enumerator_t675255274 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m424726820((Enumerator_t675255274 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1407543090 * L_0 = (KeyValuePair_2_t1407543090 *)__this->get_address_of_current_3();
		ObjectMetadata_t4058137740  L_1 = KeyValuePair_2_get_Value_m3741940767((KeyValuePair_2_t1407543090 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  ObjectMetadata_t4058137740  Enumerator_get_CurrentValue_m2906228503_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t675255274 * _thisAdjusted = reinterpret_cast<Enumerator_t675255274 *>(__this + 1);
	return Enumerator_get_CurrentValue_m2906228503(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::Reset()
extern "C"  void Enumerator_Reset_m975923089_gshared (Enumerator_t675255274 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m3950116612((Enumerator_t675255274 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_Reset_m975923089_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t675255274 * _thisAdjusted = reinterpret_cast<Enumerator_t675255274 *>(__this + 1);
	Enumerator_Reset_m975923089(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral671556834;
extern const uint32_t Enumerator_VerifyState_m3950116612_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m3950116612_gshared (Enumerator_t675255274 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m3950116612_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t3650197868 * L_0 = (Dictionary_2_t3650197868 *)__this->get_dictionary_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		Dictionary_2_t3650197868 * L_2 = (Dictionary_2_t3650197868 *)__this->get_dictionary_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_generation_14();
		int32_t L_4 = (int32_t)__this->get_stamp_2();
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t721527559 * L_5 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_5, (String_t*)_stringLiteral671556834, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m3950116612_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t675255274 * _thisAdjusted = reinterpret_cast<Enumerator_t675255274 *>(__this + 1);
	Enumerator_VerifyState_m3950116612(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::VerifyCurrent()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2382073330;
extern const uint32_t Enumerator_VerifyCurrent_m424726820_MetadataUsageId;
extern "C"  void Enumerator_VerifyCurrent_m424726820_gshared (Enumerator_t675255274 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyCurrent_m424726820_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m3950116612((Enumerator_t675255274 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2382073330, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001d:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyCurrent_m424726820_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t675255274 * _thisAdjusted = reinterpret_cast<Enumerator_t675255274 *>(__this + 1);
	Enumerator_VerifyCurrent_m424726820(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,ThirdParty.Json.LitJson.ObjectMetadata>::Dispose()
extern "C"  void Enumerator_Dispose_m3127619583_gshared (Enumerator_t675255274 * __this, const MethodInfo* method)
{
	{
		__this->set_dictionary_0((Dictionary_2_t3650197868 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3127619583_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t675255274 * _thisAdjusted = reinterpret_cast<Enumerator_t675255274 *>(__this + 1);
	Enumerator_Dispose_m3127619583(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1874499463_gshared (Enumerator_t4050882528 * __this, Dictionary_2_t2730857826 * ___dictionary0, const MethodInfo* method)
{
	{
		Dictionary_2_t2730857826 * L_0 = ___dictionary0;
		__this->set_dictionary_0(L_0);
		Dictionary_2_t2730857826 * L_1 = ___dictionary0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_generation_14();
		__this->set_stamp_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1874499463_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t2730857826 * ___dictionary0, const MethodInfo* method)
{
	Enumerator_t4050882528 * _thisAdjusted = reinterpret_cast<Enumerator_t4050882528 *>(__this + 1);
	Enumerator__ctor_m1874499463(_thisAdjusted, ___dictionary0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2507884600_gshared (Enumerator_t4050882528 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m2900414404((Enumerator_t4050882528 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t488203048  L_0 = (KeyValuePair_2_t488203048 )__this->get_current_3();
		KeyValuePair_2_t488203048  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2507884600_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4050882528 * _thisAdjusted = reinterpret_cast<Enumerator_t4050882528 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2507884600(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4018492724_gshared (Enumerator_t4050882528 * __this, const MethodInfo* method)
{
	{
		Enumerator_Reset_m2489846157((Enumerator_t4050882528 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4018492724_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4050882528 * _thisAdjusted = reinterpret_cast<Enumerator_t4050882528 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m4018492724(_thisAdjusted, method);
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m744625813_gshared (Enumerator_t4050882528 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m2900414404((Enumerator_t4050882528 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t488203048 * L_0 = (KeyValuePair_2_t488203048 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Key_m573362703((KeyValuePair_2_t488203048 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		KeyValuePair_2_t488203048 * L_2 = (KeyValuePair_2_t488203048 *)__this->get_address_of_current_3();
		int32_t L_3 = KeyValuePair_2_get_Value_m1644876463((KeyValuePair_2_t488203048 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		int32_t L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_4);
		DictionaryEntry_t3048875398  L_6;
		memset(&L_6, 0, sizeof(L_6));
		DictionaryEntry__ctor_m2901884110(&L_6, (Il2CppObject *)L_1, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m744625813_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4050882528 * _thisAdjusted = reinterpret_cast<Enumerator_t4050882528 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m744625813(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2958681418_gshared (Enumerator_t4050882528 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = Enumerator_get_CurrentKey_m2295079915((Enumerator_t4050882528 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		return L_0;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2958681418_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4050882528 * _thisAdjusted = reinterpret_cast<Enumerator_t4050882528 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2958681418(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2914875968_gshared (Enumerator_t4050882528 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Enumerator_get_CurrentValue_m3498625419((Enumerator_t4050882528 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2914875968_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4050882528 * _thisAdjusted = reinterpret_cast<Enumerator_t4050882528 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2914875968(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2619008060_gshared (Enumerator_t4050882528 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Enumerator_VerifyState_m1671341316((Enumerator_t4050882528 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		goto IL_007b;
	}

IL_0019:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		int32_t L_2 = (int32_t)L_1;
		V_1 = (int32_t)L_2;
		__this->set_next_1(((int32_t)((int32_t)L_2+(int32_t)1)));
		int32_t L_3 = V_1;
		V_0 = (int32_t)L_3;
		Dictionary_2_t2730857826 * L_4 = (Dictionary_2_t2730857826 *)__this->get_dictionary_0();
		NullCheck(L_4);
		LinkU5BU5D_t62501539* L_5 = (LinkU5BU5D_t62501539*)L_4->get_linkSlots_5();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		int32_t L_7 = (int32_t)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_7&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_007b;
		}
	}
	{
		Dictionary_2_t2730857826 * L_8 = (Dictionary_2_t2730857826 *)__this->get_dictionary_0();
		NullCheck(L_8);
		ObjectU5BU5D_t3614634134* L_9 = (ObjectU5BU5D_t3614634134*)L_8->get_keySlots_6();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		Il2CppObject * L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		Dictionary_2_t2730857826 * L_13 = (Dictionary_2_t2730857826 *)__this->get_dictionary_0();
		NullCheck(L_13);
		TextEditOpU5BU5D_t1473213239* L_14 = (TextEditOpU5BU5D_t1473213239*)L_13->get_valueSlots_7();
		int32_t L_15 = V_0;
		NullCheck(L_14);
		int32_t L_16 = L_15;
		int32_t L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		KeyValuePair_2_t488203048  L_18;
		memset(&L_18, 0, sizeof(L_18));
		KeyValuePair_2__ctor_m3870834457(&L_18, (Il2CppObject *)L_12, (int32_t)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		__this->set_current_3(L_18);
		return (bool)1;
	}

IL_007b:
	{
		int32_t L_19 = (int32_t)__this->get_next_1();
		Dictionary_2_t2730857826 * L_20 = (Dictionary_2_t2730857826 *)__this->get_dictionary_0();
		NullCheck(L_20);
		int32_t L_21 = (int32_t)L_20->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_21)))
		{
			goto IL_0019;
		}
	}
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m2619008060_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4050882528 * _thisAdjusted = reinterpret_cast<Enumerator_t4050882528 *>(__this + 1);
	return Enumerator_MoveNext_m2619008060(_thisAdjusted, method);
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Current()
extern "C"  KeyValuePair_2_t488203048  Enumerator_get_Current_m492584036_gshared (Enumerator_t4050882528 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t488203048  L_0 = (KeyValuePair_2_t488203048 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  KeyValuePair_2_t488203048  Enumerator_get_Current_m492584036_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4050882528 * _thisAdjusted = reinterpret_cast<Enumerator_t4050882528 *>(__this + 1);
	return Enumerator_get_Current_m492584036(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_CurrentKey()
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m2295079915_gshared (Enumerator_t4050882528 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m2900414404((Enumerator_t4050882528 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t488203048 * L_0 = (KeyValuePair_2_t488203048 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Key_m573362703((KeyValuePair_2_t488203048 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m2295079915_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4050882528 * _thisAdjusted = reinterpret_cast<Enumerator_t4050882528 *>(__this + 1);
	return Enumerator_get_CurrentKey_m2295079915(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_CurrentValue()
extern "C"  int32_t Enumerator_get_CurrentValue_m3498625419_gshared (Enumerator_t4050882528 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m2900414404((Enumerator_t4050882528 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t488203048 * L_0 = (KeyValuePair_2_t488203048 *)__this->get_address_of_current_3();
		int32_t L_1 = KeyValuePair_2_get_Value_m1644876463((KeyValuePair_2_t488203048 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  int32_t Enumerator_get_CurrentValue_m3498625419_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4050882528 * _thisAdjusted = reinterpret_cast<Enumerator_t4050882528 *>(__this + 1);
	return Enumerator_get_CurrentValue_m3498625419(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::Reset()
extern "C"  void Enumerator_Reset_m2489846157_gshared (Enumerator_t4050882528 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m1671341316((Enumerator_t4050882528 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_Reset_m2489846157_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4050882528 * _thisAdjusted = reinterpret_cast<Enumerator_t4050882528 *>(__this + 1);
	Enumerator_Reset_m2489846157(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral671556834;
extern const uint32_t Enumerator_VerifyState_m1671341316_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m1671341316_gshared (Enumerator_t4050882528 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m1671341316_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t2730857826 * L_0 = (Dictionary_2_t2730857826 *)__this->get_dictionary_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		Dictionary_2_t2730857826 * L_2 = (Dictionary_2_t2730857826 *)__this->get_dictionary_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_generation_14();
		int32_t L_4 = (int32_t)__this->get_stamp_2();
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t721527559 * L_5 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_5, (String_t*)_stringLiteral671556834, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m1671341316_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4050882528 * _thisAdjusted = reinterpret_cast<Enumerator_t4050882528 *>(__this + 1);
	Enumerator_VerifyState_m1671341316(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::VerifyCurrent()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2382073330;
extern const uint32_t Enumerator_VerifyCurrent_m2900414404_MetadataUsageId;
extern "C"  void Enumerator_VerifyCurrent_m2900414404_gshared (Enumerator_t4050882528 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyCurrent_m2900414404_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m1671341316((Enumerator_t4050882528 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2382073330, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001d:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyCurrent_m2900414404_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4050882528 * _thisAdjusted = reinterpret_cast<Enumerator_t4050882528 *>(__this + 1);
	Enumerator_VerifyCurrent_m2900414404(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::Dispose()
extern "C"  void Enumerator_Dispose_m793495907_gshared (Enumerator_t4050882528 * __this, const MethodInfo* method)
{
	{
		__this->set_dictionary_0((Dictionary_2_t2730857826 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m793495907_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4050882528 * _thisAdjusted = reinterpret_cast<Enumerator_t4050882528 *>(__this + 1);
	Enumerator_Dispose_m793495907(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Amazon.Runtime.Metric,System.Int64>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1698121165_gshared (Enumerator_t2053692668 * __this, Dictionary_2_t3659156526 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t3659156526 * L_0 = ___host0;
		NullCheck((Dictionary_2_t3659156526 *)L_0);
		Enumerator_t684213932  L_1 = ((  Enumerator_t684213932  (*) (Dictionary_2_t3659156526 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t3659156526 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1698121165_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t3659156526 * ___host0, const MethodInfo* method)
{
	Enumerator_t2053692668 * _thisAdjusted = reinterpret_cast<Enumerator_t2053692668 *>(__this + 1);
	Enumerator__ctor_m1698121165(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Amazon.Runtime.Metric,System.Int64>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1956978320_gshared (Enumerator_t2053692668 * __this, const MethodInfo* method)
{
	{
		Enumerator_t684213932 * L_0 = (Enumerator_t684213932 *)__this->get_address_of_host_enumerator_0();
		int32_t L_1 = Enumerator_get_CurrentKey_m2231206827((Enumerator_t684213932 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_2);
		return L_3;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1956978320_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2053692668 * _thisAdjusted = reinterpret_cast<Enumerator_t2053692668 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1956978320(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Amazon.Runtime.Metric,System.Int64>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m175547014_gshared (Enumerator_t2053692668 * __this, const MethodInfo* method)
{
	{
		Enumerator_t684213932 * L_0 = (Enumerator_t684213932 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m1537962437((Enumerator_t684213932 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m175547014_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2053692668 * _thisAdjusted = reinterpret_cast<Enumerator_t2053692668 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m175547014(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Amazon.Runtime.Metric,System.Int64>::Dispose()
extern "C"  void Enumerator_Dispose_m3080558725_gshared (Enumerator_t2053692668 * __this, const MethodInfo* method)
{
	{
		Enumerator_t684213932 * L_0 = (Enumerator_t684213932 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m2961717971((Enumerator_t684213932 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3080558725_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2053692668 * _thisAdjusted = reinterpret_cast<Enumerator_t2053692668 *>(__this + 1);
	Enumerator_Dispose_m3080558725(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Amazon.Runtime.Metric,System.Int64>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3650830322_gshared (Enumerator_t2053692668 * __this, const MethodInfo* method)
{
	{
		Enumerator_t684213932 * L_0 = (Enumerator_t684213932 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m1520629361((Enumerator_t684213932 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m3650830322_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2053692668 * _thisAdjusted = reinterpret_cast<Enumerator_t2053692668 *>(__this + 1);
	return Enumerator_MoveNext_m3650830322(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Amazon.Runtime.Metric,System.Int64>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m4234579618_gshared (Enumerator_t2053692668 * __this, const MethodInfo* method)
{
	{
		Enumerator_t684213932 * L_0 = (Enumerator_t684213932 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t1416501748 * L_1 = (KeyValuePair_2_t1416501748 *)L_0->get_address_of_current_3();
		int32_t L_2 = KeyValuePair_2_get_Key_m404179876((KeyValuePair_2_t1416501748 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  int32_t Enumerator_get_Current_m4234579618_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2053692668 * _thisAdjusted = reinterpret_cast<Enumerator_t2053692668 *>(__this + 1);
	return Enumerator_get_Current_m4234579618(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Amazon.Runtime.Metric,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2774311225_gshared (Enumerator_t3834063926 * __this, Dictionary_2_t1144560488 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t1144560488 * L_0 = ___host0;
		NullCheck((Dictionary_2_t1144560488 *)L_0);
		Enumerator_t2464585190  L_1 = ((  Enumerator_t2464585190  (*) (Dictionary_2_t1144560488 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t1144560488 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m2774311225_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t1144560488 * ___host0, const MethodInfo* method)
{
	Enumerator_t3834063926 * _thisAdjusted = reinterpret_cast<Enumerator_t3834063926 *>(__this + 1);
	Enumerator__ctor_m2774311225(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Amazon.Runtime.Metric,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2082606486_gshared (Enumerator_t3834063926 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2464585190 * L_0 = (Enumerator_t2464585190 *)__this->get_address_of_host_enumerator_0();
		int32_t L_1 = Enumerator_get_CurrentKey_m2750410819((Enumerator_t2464585190 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_2);
		return L_3;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2082606486_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3834063926 * _thisAdjusted = reinterpret_cast<Enumerator_t3834063926 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2082606486(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Amazon.Runtime.Metric,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2940477526_gshared (Enumerator_t3834063926 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2464585190 * L_0 = (Enumerator_t2464585190 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m1867529997((Enumerator_t2464585190 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2940477526_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3834063926 * _thisAdjusted = reinterpret_cast<Enumerator_t3834063926 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m2940477526(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Amazon.Runtime.Metric,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m2168347345_gshared (Enumerator_t3834063926 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2464585190 * L_0 = (Enumerator_t2464585190 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m2167966803((Enumerator_t2464585190 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2168347345_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3834063926 * _thisAdjusted = reinterpret_cast<Enumerator_t3834063926 *>(__this + 1);
	Enumerator_Dispose_m2168347345(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Amazon.Runtime.Metric,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m403331010_gshared (Enumerator_t3834063926 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2464585190 * L_0 = (Enumerator_t2464585190 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m3844071554((Enumerator_t2464585190 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m403331010_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3834063926 * _thisAdjusted = reinterpret_cast<Enumerator_t3834063926 *>(__this + 1);
	return Enumerator_MoveNext_m403331010(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Amazon.Runtime.Metric,System.Object>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m2865873134_gshared (Enumerator_t3834063926 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2464585190 * L_0 = (Enumerator_t2464585190 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t3196873006 * L_1 = (KeyValuePair_2_t3196873006 *)L_0->get_address_of_current_3();
		int32_t L_2 = KeyValuePair_2_get_Key_m2312610863((KeyValuePair_2_t3196873006 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  int32_t Enumerator_get_Current_m2865873134_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3834063926 * _thisAdjusted = reinterpret_cast<Enumerator_t3834063926 *>(__this + 1);
	return Enumerator_get_Current_m2865873134(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m356032285_gshared (Enumerator_t3769206521 * __this, Dictionary_2_t1079703083 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t1079703083 * L_0 = ___host0;
		NullCheck((Dictionary_2_t1079703083 *)L_0);
		Enumerator_t2399727785  L_1 = ((  Enumerator_t2399727785  (*) (Dictionary_2_t1079703083 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t1079703083 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m356032285_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t1079703083 * ___host0, const MethodInfo* method)
{
	Enumerator_t3769206521 * _thisAdjusted = reinterpret_cast<Enumerator_t3769206521 *>(__this + 1);
	Enumerator__ctor_m356032285(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1856980258_gshared (Enumerator_t3769206521 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2399727785 * L_0 = (Enumerator_t2399727785 *)__this->get_address_of_host_enumerator_0();
		int32_t L_1 = Enumerator_get_CurrentKey_m3410494779((Enumerator_t2399727785 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_2);
		return L_3;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1856980258_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3769206521 * _thisAdjusted = reinterpret_cast<Enumerator_t3769206521 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1856980258(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m84942922_gshared (Enumerator_t3769206521 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2399727785 * L_0 = (Enumerator_t2399727785 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m2787013825((Enumerator_t2399727785 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m84942922_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3769206521 * _thisAdjusted = reinterpret_cast<Enumerator_t3769206521 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m84942922(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m2091286541_gshared (Enumerator_t3769206521 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2399727785 * L_0 = (Enumerator_t2399727785 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m1631869499((Enumerator_t2399727785 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2091286541_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3769206521 * _thisAdjusted = reinterpret_cast<Enumerator_t3769206521 *>(__this + 1);
	Enumerator_Dispose_m2091286541(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3192922974_gshared (Enumerator_t3769206521 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2399727785 * L_0 = (Enumerator_t2399727785 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m3809319098((Enumerator_t2399727785 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m3192922974_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3769206521 * _thisAdjusted = reinterpret_cast<Enumerator_t3769206521 *>(__this + 1);
	return Enumerator_MoveNext_m3192922974(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Int32>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m4198336486_gshared (Enumerator_t3769206521 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2399727785 * L_0 = (Enumerator_t2399727785 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t3132015601 * L_1 = (KeyValuePair_2_t3132015601 *)L_0->get_address_of_current_3();
		int32_t L_2 = KeyValuePair_2_get_Key_m1683812983((KeyValuePair_2_t3132015601 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  int32_t Enumerator_get_Current_m4198336486_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3769206521 * _thisAdjusted = reinterpret_cast<Enumerator_t3769206521 *>(__this + 1);
	return Enumerator_get_Current_m4198336486(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2975000652_gshared (Enumerator_t91811072 * __this, Dictionary_2_t1697274930 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t1697274930 * L_0 = ___host0;
		NullCheck((Dictionary_2_t1697274930 *)L_0);
		Enumerator_t3017299632  L_1 = ((  Enumerator_t3017299632  (*) (Dictionary_2_t1697274930 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t1697274930 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m2975000652_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t1697274930 * ___host0, const MethodInfo* method)
{
	Enumerator_t91811072 * _thisAdjusted = reinterpret_cast<Enumerator_t91811072 *>(__this + 1);
	Enumerator__ctor_m2975000652(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1197115851_gshared (Enumerator_t91811072 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3017299632 * L_0 = (Enumerator_t3017299632 *)__this->get_address_of_host_enumerator_0();
		int32_t L_1 = Enumerator_get_CurrentKey_m447338908((Enumerator_t3017299632 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_2);
		return L_3;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1197115851_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t91811072 * _thisAdjusted = reinterpret_cast<Enumerator_t91811072 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1197115851(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1291229611_gshared (Enumerator_t91811072 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3017299632 * L_0 = (Enumerator_t3017299632 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m761796566((Enumerator_t3017299632 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1291229611_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t91811072 * _thisAdjusted = reinterpret_cast<Enumerator_t91811072 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m1291229611(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3298895664_gshared (Enumerator_t91811072 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3017299632 * L_0 = (Enumerator_t3017299632 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m2243145188((Enumerator_t3017299632 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3298895664_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t91811072 * _thisAdjusted = reinterpret_cast<Enumerator_t91811072 *>(__this + 1);
	Enumerator_Dispose_m3298895664(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1388125215_gshared (Enumerator_t91811072 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3017299632 * L_0 = (Enumerator_t3017299632 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m2770956757((Enumerator_t3017299632 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m1388125215_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t91811072 * _thisAdjusted = reinterpret_cast<Enumerator_t91811072 *>(__this + 1);
	return Enumerator_MoveNext_m1388125215(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Object>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m862874469_gshared (Enumerator_t91811072 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3017299632 * L_0 = (Enumerator_t3017299632 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t3749587448 * L_1 = (KeyValuePair_2_t3749587448 *)L_0->get_address_of_current_3();
		int32_t L_2 = KeyValuePair_2_get_Key_m1435832840((KeyValuePair_2_t3749587448 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  int32_t Enumerator_get_Current_m862874469_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t91811072 * _thisAdjusted = reinterpret_cast<Enumerator_t91811072 *>(__this + 1);
	return Enumerator_get_Current_m862874469(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int64,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3400877137_gshared (Enumerator_t2073503839 * __this, Dictionary_2_t3678967697 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t3678967697 * L_0 = ___host0;
		NullCheck((Dictionary_2_t3678967697 *)L_0);
		Enumerator_t704025103  L_1 = ((  Enumerator_t704025103  (*) (Dictionary_2_t3678967697 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t3678967697 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3400877137_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t3678967697 * ___host0, const MethodInfo* method)
{
	Enumerator_t2073503839 * _thisAdjusted = reinterpret_cast<Enumerator_t2073503839 *>(__this + 1);
	Enumerator__ctor_m3400877137(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int64,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m247337542_gshared (Enumerator_t2073503839 * __this, const MethodInfo* method)
{
	{
		Enumerator_t704025103 * L_0 = (Enumerator_t704025103 *)__this->get_address_of_host_enumerator_0();
		int64_t L_1 = Enumerator_get_CurrentKey_m4167196999((Enumerator_t704025103 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		int64_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_2);
		return L_3;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m247337542_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2073503839 * _thisAdjusted = reinterpret_cast<Enumerator_t2073503839 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m247337542(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int64,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4166432760_gshared (Enumerator_t2073503839 * __this, const MethodInfo* method)
{
	{
		Enumerator_t704025103 * L_0 = (Enumerator_t704025103 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m598725905((Enumerator_t704025103 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4166432760_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2073503839 * _thisAdjusted = reinterpret_cast<Enumerator_t2073503839 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m4166432760(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int64,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3239242417_gshared (Enumerator_t2073503839 * __this, const MethodInfo* method)
{
	{
		Enumerator_t704025103 * L_0 = (Enumerator_t704025103 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m3446908287((Enumerator_t704025103 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3239242417_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2073503839 * _thisAdjusted = reinterpret_cast<Enumerator_t2073503839 *>(__this + 1);
	Enumerator_Dispose_m3239242417(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int64,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1062748132_gshared (Enumerator_t2073503839 * __this, const MethodInfo* method)
{
	{
		Enumerator_t704025103 * L_0 = (Enumerator_t704025103 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m3139459564((Enumerator_t704025103 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m1062748132_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2073503839 * _thisAdjusted = reinterpret_cast<Enumerator_t2073503839 *>(__this + 1);
	return Enumerator_MoveNext_m1062748132(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int64,System.Object>::get_Current()
extern "C"  int64_t Enumerator_get_Current_m3153023504_gshared (Enumerator_t2073503839 * __this, const MethodInfo* method)
{
	{
		Enumerator_t704025103 * L_0 = (Enumerator_t704025103 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t1436312919 * L_1 = (KeyValuePair_2_t1436312919 *)L_0->get_address_of_current_3();
		int64_t L_2 = KeyValuePair_2_get_Key_m3157806383((KeyValuePair_2_t1436312919 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  int64_t Enumerator_get_Current_m3153023504_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2073503839 * _thisAdjusted = reinterpret_cast<Enumerator_t2073503839 *>(__this + 1);
	return Enumerator_get_Current_m3153023504(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.IntPtr,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3235517721_gshared (Enumerator_t1526010755 * __this, Dictionary_2_t3131474613 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t3131474613 * L_0 = ___host0;
		NullCheck((Dictionary_2_t3131474613 *)L_0);
		Enumerator_t156532019  L_1 = ((  Enumerator_t156532019  (*) (Dictionary_2_t3131474613 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t3131474613 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3235517721_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t3131474613 * ___host0, const MethodInfo* method)
{
	Enumerator_t1526010755 * _thisAdjusted = reinterpret_cast<Enumerator_t1526010755 *>(__this + 1);
	Enumerator__ctor_m3235517721(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.IntPtr,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3071477686_gshared (Enumerator_t1526010755 * __this, const MethodInfo* method)
{
	{
		Enumerator_t156532019 * L_0 = (Enumerator_t156532019 *)__this->get_address_of_host_enumerator_0();
		IntPtr_t L_1 = Enumerator_get_CurrentKey_m2387752419((Enumerator_t156532019 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		IntPtr_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_2);
		return L_3;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3071477686_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1526010755 * _thisAdjusted = reinterpret_cast<Enumerator_t1526010755 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m3071477686(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.IntPtr,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1082414166_gshared (Enumerator_t1526010755 * __this, const MethodInfo* method)
{
	{
		Enumerator_t156532019 * L_0 = (Enumerator_t156532019 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m1147555085((Enumerator_t156532019 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1082414166_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1526010755 * _thisAdjusted = reinterpret_cast<Enumerator_t1526010755 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m1082414166(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.IntPtr,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m168635025_gshared (Enumerator_t1526010755 * __this, const MethodInfo* method)
{
	{
		Enumerator_t156532019 * L_0 = (Enumerator_t156532019 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m2117437651((Enumerator_t156532019 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m168635025_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1526010755 * _thisAdjusted = reinterpret_cast<Enumerator_t1526010755 *>(__this + 1);
	Enumerator_Dispose_m168635025(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.IntPtr,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3535822434_gshared (Enumerator_t1526010755 * __this, const MethodInfo* method)
{
	{
		Enumerator_t156532019 * L_0 = (Enumerator_t156532019 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m266619810((Enumerator_t156532019 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m3535822434_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1526010755 * _thisAdjusted = reinterpret_cast<Enumerator_t1526010755 *>(__this + 1);
	return Enumerator_MoveNext_m3535822434(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.IntPtr,System.Object>::get_Current()
extern "C"  IntPtr_t Enumerator_get_Current_m286596974_gshared (Enumerator_t1526010755 * __this, const MethodInfo* method)
{
	{
		Enumerator_t156532019 * L_0 = (Enumerator_t156532019 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t888819835 * L_1 = (KeyValuePair_2_t888819835 *)L_0->get_address_of_current_3();
		IntPtr_t L_2 = KeyValuePair_2_get_Key_m1574332879((KeyValuePair_2_t888819835 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  IntPtr_t Enumerator_get_Current_m286596974_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1526010755 * _thisAdjusted = reinterpret_cast<Enumerator_t1526010755 *>(__this + 1);
	return Enumerator_get_Current_m286596974(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m4088904673_gshared (Enumerator_t3914370944 * __this, Dictionary_2_t1224867506 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t1224867506 * L_0 = ___host0;
		NullCheck((Dictionary_2_t1224867506 *)L_0);
		Enumerator_t2544892208  L_1 = ((  Enumerator_t2544892208  (*) (Dictionary_2_t1224867506 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t1224867506 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m4088904673_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t1224867506 * ___host0, const MethodInfo* method)
{
	Enumerator_t3914370944 * _thisAdjusted = reinterpret_cast<Enumerator_t3914370944 *>(__this + 1);
	Enumerator__ctor_m4088904673(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1411351484_gshared (Enumerator_t3914370944 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2544892208 * L_0 = (Enumerator_t2544892208 *)__this->get_address_of_host_enumerator_0();
		Il2CppObject * L_1 = Enumerator_get_CurrentKey_m4133598171((Enumerator_t2544892208 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1411351484_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3914370944 * _thisAdjusted = reinterpret_cast<Enumerator_t3914370944 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1411351484(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1944504356_gshared (Enumerator_t3914370944 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2544892208 * L_0 = (Enumerator_t2544892208 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m1928144285((Enumerator_t2544892208 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1944504356_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3914370944 * _thisAdjusted = reinterpret_cast<Enumerator_t3914370944 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m1944504356(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::Dispose()
extern "C"  void Enumerator_Dispose_m3979808513_gshared (Enumerator_t3914370944 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2544892208 * L_0 = (Enumerator_t2544892208 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m1362422387((Enumerator_t2544892208 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3979808513_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3914370944 * _thisAdjusted = reinterpret_cast<Enumerator_t3914370944 *>(__this + 1);
	Enumerator_Dispose_m3979808513(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2015454696_gshared (Enumerator_t3914370944 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2544892208 * L_0 = (Enumerator_t2544892208 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m3850333932((Enumerator_t2544892208 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m2015454696_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3914370944 * _thisAdjusted = reinterpret_cast<Enumerator_t3914370944 *>(__this + 1);
	return Enumerator_MoveNext_m2015454696(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m3493547464_gshared (Enumerator_t3914370944 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2544892208 * L_0 = (Enumerator_t2544892208 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t3277180024 * L_1 = (KeyValuePair_2_t3277180024 *)L_0->get_address_of_current_3();
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m4122353311((KeyValuePair_2_t3277180024 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_get_Current_m3493547464_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3914370944 * _thisAdjusted = reinterpret_cast<Enumerator_t3914370944 *>(__this + 1);
	return Enumerator_get_Current_m3493547464(_thisAdjusted, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
