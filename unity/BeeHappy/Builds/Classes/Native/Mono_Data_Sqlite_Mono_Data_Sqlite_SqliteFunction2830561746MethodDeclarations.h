﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Data.Sqlite.SqliteFunction
struct SqliteFunction_t2830561746;
// System.Object
struct Il2CppObject;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.String
struct String_t;
// Mono.Data.Sqlite.SqliteFunction[]
struct SqliteFunctionU5BU5D_t949732007;
// Mono.Data.Sqlite.SQLiteBase
struct SQLiteBase_t2015643195;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SQLiteBase2015643195.h"

// System.Void Mono.Data.Sqlite.SqliteFunction::.cctor()
extern "C"  void SqliteFunction__cctor_m449480182 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Mono.Data.Sqlite.SqliteFunction::Invoke(System.Object[])
extern "C"  Il2CppObject * SqliteFunction_Invoke_m3931136062 (SqliteFunction_t2830561746 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteFunction::Step(System.Object[],System.Int32,System.Object&)
extern "C"  void SqliteFunction_Step_m4270568968 (SqliteFunction_t2830561746 * __this, ObjectU5BU5D_t3614634134* ___args0, int32_t ___stepNumber1, Il2CppObject ** ___contextData2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Mono.Data.Sqlite.SqliteFunction::Final(System.Object)
extern "C"  Il2CppObject * SqliteFunction_Final_m4222365480 (SqliteFunction_t2830561746 * __this, Il2CppObject * ___contextData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.SqliteFunction::Compare(System.String,System.String)
extern "C"  int32_t SqliteFunction_Compare_m2847016942 (SqliteFunction_t2830561746 * __this, String_t* ___param10, String_t* ___param21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] Mono.Data.Sqlite.SqliteFunction::ConvertParams(System.Int32,System.IntPtr)
extern "C"  ObjectU5BU5D_t3614634134* SqliteFunction_ConvertParams_m2146968012 (SqliteFunction_t2830561746 * __this, int32_t ___nArgs0, IntPtr_t ___argsptr1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteFunction::SetReturnValue(System.IntPtr,System.Object)
extern "C"  void SqliteFunction_SetReturnValue_m861686036 (SqliteFunction_t2830561746 * __this, IntPtr_t ___context0, Il2CppObject * ___returnValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteFunction::ScalarCallback(System.IntPtr,System.Int32,System.IntPtr)
extern "C"  void SqliteFunction_ScalarCallback_m2875354027 (SqliteFunction_t2830561746 * __this, IntPtr_t ___context0, int32_t ___nArgs1, IntPtr_t ___argsptr2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.SqliteFunction::CompareCallback(System.IntPtr,System.Int32,System.IntPtr,System.Int32,System.IntPtr)
extern "C"  int32_t SqliteFunction_CompareCallback_m239219207 (SqliteFunction_t2830561746 * __this, IntPtr_t ___ptr0, int32_t ___len11, IntPtr_t ___ptr12, int32_t ___len23, IntPtr_t ___ptr24, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.SqliteFunction::CompareCallback16(System.IntPtr,System.Int32,System.IntPtr,System.Int32,System.IntPtr)
extern "C"  int32_t SqliteFunction_CompareCallback16_m828130140 (SqliteFunction_t2830561746 * __this, IntPtr_t ___ptr0, int32_t ___len11, IntPtr_t ___ptr12, int32_t ___len23, IntPtr_t ___ptr24, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteFunction::StepCallback(System.IntPtr,System.Int32,System.IntPtr)
extern "C"  void SqliteFunction_StepCallback_m40504675 (SqliteFunction_t2830561746 * __this, IntPtr_t ___context0, int32_t ___nArgs1, IntPtr_t ___argsptr2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteFunction::FinalCallback(System.IntPtr)
extern "C"  void SqliteFunction_FinalCallback_m2133047866 (SqliteFunction_t2830561746 * __this, IntPtr_t ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteFunction::Dispose(System.Boolean)
extern "C"  void SqliteFunction_Dispose_m2778442657 (SqliteFunction_t2830561746 * __this, bool ___disposing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteFunction::Dispose()
extern "C"  void SqliteFunction_Dispose_m3126580814 (SqliteFunction_t2830561746 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Data.Sqlite.SqliteFunction[] Mono.Data.Sqlite.SqliteFunction::BindFunctions(Mono.Data.Sqlite.SQLiteBase)
extern "C"  SqliteFunctionU5BU5D_t949732007* SqliteFunction_BindFunctions_m1392425603 (Il2CppObject * __this /* static, unused */, SQLiteBase_t2015643195 * ___sqlbase0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
