﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BobRandomiser
struct BobRandomiser_t2069133119;

#include "codegen/il2cpp-codegen.h"

// System.Void BobRandomiser::.ctor()
extern "C"  void BobRandomiser__ctor_m2124599422 (BobRandomiser_t2069133119 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BobRandomiser::Start()
extern "C"  void BobRandomiser_Start_m2940967130 (BobRandomiser_t2069133119 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BobRandomiser::Update()
extern "C"  void BobRandomiser_Update_m1115337443 (BobRandomiser_t2069133119 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
