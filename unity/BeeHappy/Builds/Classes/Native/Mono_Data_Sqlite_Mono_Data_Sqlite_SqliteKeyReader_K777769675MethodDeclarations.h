﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Data.Sqlite.SqliteKeyReader/KeyInfo
struct KeyInfo_t777769675;
struct KeyInfo_t777769675_marshaled_pinvoke;
struct KeyInfo_t777769675_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct KeyInfo_t777769675;
struct KeyInfo_t777769675_marshaled_pinvoke;

extern "C" void KeyInfo_t777769675_marshal_pinvoke(const KeyInfo_t777769675& unmarshaled, KeyInfo_t777769675_marshaled_pinvoke& marshaled);
extern "C" void KeyInfo_t777769675_marshal_pinvoke_back(const KeyInfo_t777769675_marshaled_pinvoke& marshaled, KeyInfo_t777769675& unmarshaled);
extern "C" void KeyInfo_t777769675_marshal_pinvoke_cleanup(KeyInfo_t777769675_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct KeyInfo_t777769675;
struct KeyInfo_t777769675_marshaled_com;

extern "C" void KeyInfo_t777769675_marshal_com(const KeyInfo_t777769675& unmarshaled, KeyInfo_t777769675_marshaled_com& marshaled);
extern "C" void KeyInfo_t777769675_marshal_com_back(const KeyInfo_t777769675_marshaled_com& marshaled, KeyInfo_t777769675& unmarshaled);
extern "C" void KeyInfo_t777769675_marshal_com_cleanup(KeyInfo_t777769675_marshaled_com& marshaled);
