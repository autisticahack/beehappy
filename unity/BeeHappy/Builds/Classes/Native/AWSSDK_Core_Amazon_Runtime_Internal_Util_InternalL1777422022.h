﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// System.Type
struct Type_t;
// Amazon.Util.Internal.ITypeInfo
struct ITypeInfo_t3592676621;
// System.Reflection.MethodInfo
struct MethodInfo_t;

#include "AWSSDK_Core_Amazon_Runtime_Internal_Util_InternalL2972373343.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Util_InternalL3882482337.h"
#include "mscorlib_System_Nullable_1_gen2088641033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Util.InternalLog4netLogger
struct  InternalLog4netLogger_t1777422022  : public InternalLogger_t2972373343
{
public:
	// System.Object Amazon.Runtime.Internal.Util.InternalLog4netLogger::internalLogger
	Il2CppObject * ___internalLogger_18;
	// System.Nullable`1<System.Boolean> Amazon.Runtime.Internal.Util.InternalLog4netLogger::isErrorEnabled
	Nullable_1_t2088641033  ___isErrorEnabled_19;
	// System.Nullable`1<System.Boolean> Amazon.Runtime.Internal.Util.InternalLog4netLogger::isDebugEnabled
	Nullable_1_t2088641033  ___isDebugEnabled_20;
	// System.Nullable`1<System.Boolean> Amazon.Runtime.Internal.Util.InternalLog4netLogger::isInfoEnabled
	Nullable_1_t2088641033  ___isInfoEnabled_21;

public:
	inline static int32_t get_offset_of_internalLogger_18() { return static_cast<int32_t>(offsetof(InternalLog4netLogger_t1777422022, ___internalLogger_18)); }
	inline Il2CppObject * get_internalLogger_18() const { return ___internalLogger_18; }
	inline Il2CppObject ** get_address_of_internalLogger_18() { return &___internalLogger_18; }
	inline void set_internalLogger_18(Il2CppObject * value)
	{
		___internalLogger_18 = value;
		Il2CppCodeGenWriteBarrier(&___internalLogger_18, value);
	}

	inline static int32_t get_offset_of_isErrorEnabled_19() { return static_cast<int32_t>(offsetof(InternalLog4netLogger_t1777422022, ___isErrorEnabled_19)); }
	inline Nullable_1_t2088641033  get_isErrorEnabled_19() const { return ___isErrorEnabled_19; }
	inline Nullable_1_t2088641033 * get_address_of_isErrorEnabled_19() { return &___isErrorEnabled_19; }
	inline void set_isErrorEnabled_19(Nullable_1_t2088641033  value)
	{
		___isErrorEnabled_19 = value;
	}

	inline static int32_t get_offset_of_isDebugEnabled_20() { return static_cast<int32_t>(offsetof(InternalLog4netLogger_t1777422022, ___isDebugEnabled_20)); }
	inline Nullable_1_t2088641033  get_isDebugEnabled_20() const { return ___isDebugEnabled_20; }
	inline Nullable_1_t2088641033 * get_address_of_isDebugEnabled_20() { return &___isDebugEnabled_20; }
	inline void set_isDebugEnabled_20(Nullable_1_t2088641033  value)
	{
		___isDebugEnabled_20 = value;
	}

	inline static int32_t get_offset_of_isInfoEnabled_21() { return static_cast<int32_t>(offsetof(InternalLog4netLogger_t1777422022, ___isInfoEnabled_21)); }
	inline Nullable_1_t2088641033  get_isInfoEnabled_21() const { return ___isInfoEnabled_21; }
	inline Nullable_1_t2088641033 * get_address_of_isInfoEnabled_21() { return &___isInfoEnabled_21; }
	inline void set_isInfoEnabled_21(Nullable_1_t2088641033  value)
	{
		___isInfoEnabled_21 = value;
	}
};

struct InternalLog4netLogger_t1777422022_StaticFields
{
public:
	// Amazon.Runtime.Internal.Util.InternalLog4netLogger/LoadState Amazon.Runtime.Internal.Util.InternalLog4netLogger::loadState
	int32_t ___loadState_2;
	// System.Object Amazon.Runtime.Internal.Util.InternalLog4netLogger::LOCK
	Il2CppObject * ___LOCK_3;
	// System.Type Amazon.Runtime.Internal.Util.InternalLog4netLogger::logMangerType
	Type_t * ___logMangerType_4;
	// Amazon.Util.Internal.ITypeInfo Amazon.Runtime.Internal.Util.InternalLog4netLogger::logMangerTypeInfo
	Il2CppObject * ___logMangerTypeInfo_5;
	// System.Reflection.MethodInfo Amazon.Runtime.Internal.Util.InternalLog4netLogger::getLoggerWithTypeMethod
	MethodInfo_t * ___getLoggerWithTypeMethod_6;
	// System.Type Amazon.Runtime.Internal.Util.InternalLog4netLogger::logType
	Type_t * ___logType_7;
	// Amazon.Util.Internal.ITypeInfo Amazon.Runtime.Internal.Util.InternalLog4netLogger::logTypeInfo
	Il2CppObject * ___logTypeInfo_8;
	// System.Reflection.MethodInfo Amazon.Runtime.Internal.Util.InternalLog4netLogger::logMethod
	MethodInfo_t * ___logMethod_9;
	// System.Type Amazon.Runtime.Internal.Util.InternalLog4netLogger::levelType
	Type_t * ___levelType_10;
	// Amazon.Util.Internal.ITypeInfo Amazon.Runtime.Internal.Util.InternalLog4netLogger::levelTypeInfo
	Il2CppObject * ___levelTypeInfo_11;
	// System.Object Amazon.Runtime.Internal.Util.InternalLog4netLogger::debugLevelPropertyValue
	Il2CppObject * ___debugLevelPropertyValue_12;
	// System.Object Amazon.Runtime.Internal.Util.InternalLog4netLogger::infoLevelPropertyValue
	Il2CppObject * ___infoLevelPropertyValue_13;
	// System.Object Amazon.Runtime.Internal.Util.InternalLog4netLogger::errorLevelPropertyValue
	Il2CppObject * ___errorLevelPropertyValue_14;
	// System.Reflection.MethodInfo Amazon.Runtime.Internal.Util.InternalLog4netLogger::isEnabledForMethod
	MethodInfo_t * ___isEnabledForMethod_15;
	// System.Type Amazon.Runtime.Internal.Util.InternalLog4netLogger::systemStringFormatType
	Type_t * ___systemStringFormatType_16;
	// System.Type Amazon.Runtime.Internal.Util.InternalLog4netLogger::loggerType
	Type_t * ___loggerType_17;

public:
	inline static int32_t get_offset_of_loadState_2() { return static_cast<int32_t>(offsetof(InternalLog4netLogger_t1777422022_StaticFields, ___loadState_2)); }
	inline int32_t get_loadState_2() const { return ___loadState_2; }
	inline int32_t* get_address_of_loadState_2() { return &___loadState_2; }
	inline void set_loadState_2(int32_t value)
	{
		___loadState_2 = value;
	}

	inline static int32_t get_offset_of_LOCK_3() { return static_cast<int32_t>(offsetof(InternalLog4netLogger_t1777422022_StaticFields, ___LOCK_3)); }
	inline Il2CppObject * get_LOCK_3() const { return ___LOCK_3; }
	inline Il2CppObject ** get_address_of_LOCK_3() { return &___LOCK_3; }
	inline void set_LOCK_3(Il2CppObject * value)
	{
		___LOCK_3 = value;
		Il2CppCodeGenWriteBarrier(&___LOCK_3, value);
	}

	inline static int32_t get_offset_of_logMangerType_4() { return static_cast<int32_t>(offsetof(InternalLog4netLogger_t1777422022_StaticFields, ___logMangerType_4)); }
	inline Type_t * get_logMangerType_4() const { return ___logMangerType_4; }
	inline Type_t ** get_address_of_logMangerType_4() { return &___logMangerType_4; }
	inline void set_logMangerType_4(Type_t * value)
	{
		___logMangerType_4 = value;
		Il2CppCodeGenWriteBarrier(&___logMangerType_4, value);
	}

	inline static int32_t get_offset_of_logMangerTypeInfo_5() { return static_cast<int32_t>(offsetof(InternalLog4netLogger_t1777422022_StaticFields, ___logMangerTypeInfo_5)); }
	inline Il2CppObject * get_logMangerTypeInfo_5() const { return ___logMangerTypeInfo_5; }
	inline Il2CppObject ** get_address_of_logMangerTypeInfo_5() { return &___logMangerTypeInfo_5; }
	inline void set_logMangerTypeInfo_5(Il2CppObject * value)
	{
		___logMangerTypeInfo_5 = value;
		Il2CppCodeGenWriteBarrier(&___logMangerTypeInfo_5, value);
	}

	inline static int32_t get_offset_of_getLoggerWithTypeMethod_6() { return static_cast<int32_t>(offsetof(InternalLog4netLogger_t1777422022_StaticFields, ___getLoggerWithTypeMethod_6)); }
	inline MethodInfo_t * get_getLoggerWithTypeMethod_6() const { return ___getLoggerWithTypeMethod_6; }
	inline MethodInfo_t ** get_address_of_getLoggerWithTypeMethod_6() { return &___getLoggerWithTypeMethod_6; }
	inline void set_getLoggerWithTypeMethod_6(MethodInfo_t * value)
	{
		___getLoggerWithTypeMethod_6 = value;
		Il2CppCodeGenWriteBarrier(&___getLoggerWithTypeMethod_6, value);
	}

	inline static int32_t get_offset_of_logType_7() { return static_cast<int32_t>(offsetof(InternalLog4netLogger_t1777422022_StaticFields, ___logType_7)); }
	inline Type_t * get_logType_7() const { return ___logType_7; }
	inline Type_t ** get_address_of_logType_7() { return &___logType_7; }
	inline void set_logType_7(Type_t * value)
	{
		___logType_7 = value;
		Il2CppCodeGenWriteBarrier(&___logType_7, value);
	}

	inline static int32_t get_offset_of_logTypeInfo_8() { return static_cast<int32_t>(offsetof(InternalLog4netLogger_t1777422022_StaticFields, ___logTypeInfo_8)); }
	inline Il2CppObject * get_logTypeInfo_8() const { return ___logTypeInfo_8; }
	inline Il2CppObject ** get_address_of_logTypeInfo_8() { return &___logTypeInfo_8; }
	inline void set_logTypeInfo_8(Il2CppObject * value)
	{
		___logTypeInfo_8 = value;
		Il2CppCodeGenWriteBarrier(&___logTypeInfo_8, value);
	}

	inline static int32_t get_offset_of_logMethod_9() { return static_cast<int32_t>(offsetof(InternalLog4netLogger_t1777422022_StaticFields, ___logMethod_9)); }
	inline MethodInfo_t * get_logMethod_9() const { return ___logMethod_9; }
	inline MethodInfo_t ** get_address_of_logMethod_9() { return &___logMethod_9; }
	inline void set_logMethod_9(MethodInfo_t * value)
	{
		___logMethod_9 = value;
		Il2CppCodeGenWriteBarrier(&___logMethod_9, value);
	}

	inline static int32_t get_offset_of_levelType_10() { return static_cast<int32_t>(offsetof(InternalLog4netLogger_t1777422022_StaticFields, ___levelType_10)); }
	inline Type_t * get_levelType_10() const { return ___levelType_10; }
	inline Type_t ** get_address_of_levelType_10() { return &___levelType_10; }
	inline void set_levelType_10(Type_t * value)
	{
		___levelType_10 = value;
		Il2CppCodeGenWriteBarrier(&___levelType_10, value);
	}

	inline static int32_t get_offset_of_levelTypeInfo_11() { return static_cast<int32_t>(offsetof(InternalLog4netLogger_t1777422022_StaticFields, ___levelTypeInfo_11)); }
	inline Il2CppObject * get_levelTypeInfo_11() const { return ___levelTypeInfo_11; }
	inline Il2CppObject ** get_address_of_levelTypeInfo_11() { return &___levelTypeInfo_11; }
	inline void set_levelTypeInfo_11(Il2CppObject * value)
	{
		___levelTypeInfo_11 = value;
		Il2CppCodeGenWriteBarrier(&___levelTypeInfo_11, value);
	}

	inline static int32_t get_offset_of_debugLevelPropertyValue_12() { return static_cast<int32_t>(offsetof(InternalLog4netLogger_t1777422022_StaticFields, ___debugLevelPropertyValue_12)); }
	inline Il2CppObject * get_debugLevelPropertyValue_12() const { return ___debugLevelPropertyValue_12; }
	inline Il2CppObject ** get_address_of_debugLevelPropertyValue_12() { return &___debugLevelPropertyValue_12; }
	inline void set_debugLevelPropertyValue_12(Il2CppObject * value)
	{
		___debugLevelPropertyValue_12 = value;
		Il2CppCodeGenWriteBarrier(&___debugLevelPropertyValue_12, value);
	}

	inline static int32_t get_offset_of_infoLevelPropertyValue_13() { return static_cast<int32_t>(offsetof(InternalLog4netLogger_t1777422022_StaticFields, ___infoLevelPropertyValue_13)); }
	inline Il2CppObject * get_infoLevelPropertyValue_13() const { return ___infoLevelPropertyValue_13; }
	inline Il2CppObject ** get_address_of_infoLevelPropertyValue_13() { return &___infoLevelPropertyValue_13; }
	inline void set_infoLevelPropertyValue_13(Il2CppObject * value)
	{
		___infoLevelPropertyValue_13 = value;
		Il2CppCodeGenWriteBarrier(&___infoLevelPropertyValue_13, value);
	}

	inline static int32_t get_offset_of_errorLevelPropertyValue_14() { return static_cast<int32_t>(offsetof(InternalLog4netLogger_t1777422022_StaticFields, ___errorLevelPropertyValue_14)); }
	inline Il2CppObject * get_errorLevelPropertyValue_14() const { return ___errorLevelPropertyValue_14; }
	inline Il2CppObject ** get_address_of_errorLevelPropertyValue_14() { return &___errorLevelPropertyValue_14; }
	inline void set_errorLevelPropertyValue_14(Il2CppObject * value)
	{
		___errorLevelPropertyValue_14 = value;
		Il2CppCodeGenWriteBarrier(&___errorLevelPropertyValue_14, value);
	}

	inline static int32_t get_offset_of_isEnabledForMethod_15() { return static_cast<int32_t>(offsetof(InternalLog4netLogger_t1777422022_StaticFields, ___isEnabledForMethod_15)); }
	inline MethodInfo_t * get_isEnabledForMethod_15() const { return ___isEnabledForMethod_15; }
	inline MethodInfo_t ** get_address_of_isEnabledForMethod_15() { return &___isEnabledForMethod_15; }
	inline void set_isEnabledForMethod_15(MethodInfo_t * value)
	{
		___isEnabledForMethod_15 = value;
		Il2CppCodeGenWriteBarrier(&___isEnabledForMethod_15, value);
	}

	inline static int32_t get_offset_of_systemStringFormatType_16() { return static_cast<int32_t>(offsetof(InternalLog4netLogger_t1777422022_StaticFields, ___systemStringFormatType_16)); }
	inline Type_t * get_systemStringFormatType_16() const { return ___systemStringFormatType_16; }
	inline Type_t ** get_address_of_systemStringFormatType_16() { return &___systemStringFormatType_16; }
	inline void set_systemStringFormatType_16(Type_t * value)
	{
		___systemStringFormatType_16 = value;
		Il2CppCodeGenWriteBarrier(&___systemStringFormatType_16, value);
	}

	inline static int32_t get_offset_of_loggerType_17() { return static_cast<int32_t>(offsetof(InternalLog4netLogger_t1777422022_StaticFields, ___loggerType_17)); }
	inline Type_t * get_loggerType_17() const { return ___loggerType_17; }
	inline Type_t ** get_address_of_loggerType_17() { return &___loggerType_17; }
	inline void set_loggerType_17(Type_t * value)
	{
		___loggerType_17 = value;
		Il2CppCodeGenWriteBarrier(&___loggerType_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
