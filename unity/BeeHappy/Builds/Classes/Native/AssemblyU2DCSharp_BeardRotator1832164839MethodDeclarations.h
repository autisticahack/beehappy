﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BeardRotator
struct BeardRotator_t1832164839;

#include "codegen/il2cpp-codegen.h"

// System.Void BeardRotator::.ctor()
extern "C"  void BeardRotator__ctor_m3718583662 (BeardRotator_t1832164839 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BeardRotator::Start()
extern "C"  void BeardRotator_Start_m3465517906 (BeardRotator_t1832164839 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BeardRotator::Update()
extern "C"  void BeardRotator_Update_m3394339439 (BeardRotator_t1832164839 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BeardRotator::DropBeard()
extern "C"  void BeardRotator_DropBeard_m3373673719 (BeardRotator_t1832164839 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BeardRotator::NewScene()
extern "C"  void BeardRotator_NewScene_m908199254 (BeardRotator_t1832164839 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
