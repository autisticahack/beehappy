﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>
struct Dictionary_2_t727138142;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2047162844.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22779450660.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_ArrayMetadata1135078014.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1167548803_gshared (Enumerator_t2047162844 * __this, Dictionary_2_t727138142 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m1167548803(__this, ___dictionary0, method) ((  void (*) (Enumerator_t2047162844 *, Dictionary_2_t727138142 *, const MethodInfo*))Enumerator__ctor_m1167548803_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2209161860_gshared (Enumerator_t2047162844 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2209161860(__this, method) ((  Il2CppObject * (*) (Enumerator_t2047162844 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2209161860_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2326843624_gshared (Enumerator_t2047162844 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2326843624(__this, method) ((  void (*) (Enumerator_t2047162844 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2326843624_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2138280373_gshared (Enumerator_t2047162844 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2138280373(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t2047162844 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2138280373_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1315557434_gshared (Enumerator_t2047162844 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1315557434(__this, method) ((  Il2CppObject * (*) (Enumerator_t2047162844 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1315557434_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m4200658056_gshared (Enumerator_t2047162844 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m4200658056(__this, method) ((  Il2CppObject * (*) (Enumerator_t2047162844 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m4200658056_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2137572552_gshared (Enumerator_t2047162844 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2137572552(__this, method) ((  bool (*) (Enumerator_t2047162844 *, const MethodInfo*))Enumerator_MoveNext_m2137572552_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::get_Current()
extern "C"  KeyValuePair_2_t2779450660  Enumerator_get_Current_m923671264_gshared (Enumerator_t2047162844 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m923671264(__this, method) ((  KeyValuePair_2_t2779450660  (*) (Enumerator_t2047162844 *, const MethodInfo*))Enumerator_get_Current_m923671264_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::get_CurrentKey()
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m3038333311_gshared (Enumerator_t2047162844 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m3038333311(__this, method) ((  Il2CppObject * (*) (Enumerator_t2047162844 *, const MethodInfo*))Enumerator_get_CurrentKey_m3038333311_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::get_CurrentValue()
extern "C"  ArrayMetadata_t1135078014  Enumerator_get_CurrentValue_m2382161119_gshared (Enumerator_t2047162844 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m2382161119(__this, method) ((  ArrayMetadata_t1135078014  (*) (Enumerator_t2047162844 *, const MethodInfo*))Enumerator_get_CurrentValue_m2382161119_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::Reset()
extern "C"  void Enumerator_Reset_m2586713469_gshared (Enumerator_t2047162844 * __this, const MethodInfo* method);
#define Enumerator_Reset_m2586713469(__this, method) ((  void (*) (Enumerator_t2047162844 *, const MethodInfo*))Enumerator_Reset_m2586713469_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::VerifyState()
extern "C"  void Enumerator_VerifyState_m2203483644_gshared (Enumerator_t2047162844 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m2203483644(__this, method) ((  void (*) (Enumerator_t2047162844 *, const MethodInfo*))Enumerator_VerifyState_m2203483644_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m3852554688_gshared (Enumerator_t2047162844 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m3852554688(__this, method) ((  void (*) (Enumerator_t2047162844 *, const MethodInfo*))Enumerator_VerifyCurrent_m3852554688_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::Dispose()
extern "C"  void Enumerator_Dispose_m4092052839_gshared (Enumerator_t2047162844 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m4092052839(__this, method) ((  void (*) (Enumerator_t2047162844 *, const MethodInfo*))Enumerator_Dispose_m4092052839_gshared)(__this, method)
