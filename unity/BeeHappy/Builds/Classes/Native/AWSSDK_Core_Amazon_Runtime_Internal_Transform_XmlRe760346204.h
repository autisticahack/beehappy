﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_Resp3934041557.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Transform.XmlResponseUnmarshaller
struct  XmlResponseUnmarshaller_t760346204  : public ResponseUnmarshaller_t3934041557
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
