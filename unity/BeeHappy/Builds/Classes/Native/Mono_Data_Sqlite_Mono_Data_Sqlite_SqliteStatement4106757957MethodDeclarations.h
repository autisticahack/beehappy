﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Data.Sqlite.SqliteStatement
struct SqliteStatement_t4106757957;
// Mono.Data.Sqlite.SQLiteBase
struct SQLiteBase_t2015643195;
// Mono.Data.Sqlite.SqliteStatementHandle
struct SqliteStatementHandle_t3796671787;
// System.String
struct String_t;
// Mono.Data.Sqlite.SqliteParameter
struct SqliteParameter_t354437343;
// System.String[]
struct StringU5BU5D_t1642385972;

#include "codegen/il2cpp-codegen.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SQLiteBase2015643195.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteStatementH3796671787.h"
#include "mscorlib_System_String2029220233.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteStatement4106757957.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteParameter354437343.h"

// System.Void Mono.Data.Sqlite.SqliteStatement::.ctor(Mono.Data.Sqlite.SQLiteBase,Mono.Data.Sqlite.SqliteStatementHandle,System.String,Mono.Data.Sqlite.SqliteStatement)
extern "C"  void SqliteStatement__ctor_m3705907298 (SqliteStatement_t4106757957 * __this, SQLiteBase_t2015643195 * ___sqlbase0, SqliteStatementHandle_t3796671787 * ___stmt1, String_t* ___strCommand2, SqliteStatement_t4106757957 * ___previous3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Data.Sqlite.SqliteStatement::MapParameter(System.String,Mono.Data.Sqlite.SqliteParameter)
extern "C"  bool SqliteStatement_MapParameter_m3421976017 (SqliteStatement_t4106757957 * __this, String_t* ___s0, SqliteParameter_t354437343 * ___p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteStatement::Dispose()
extern "C"  void SqliteStatement_Dispose_m2914942309 (SqliteStatement_t4106757957 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteStatement::BindParameters()
extern "C"  void SqliteStatement_BindParameters_m3187122349 (SqliteStatement_t4106757957 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteStatement::BindParameter(System.Int32,Mono.Data.Sqlite.SqliteParameter)
extern "C"  void SqliteStatement_BindParameter_m1067317071 (SqliteStatement_t4106757957 * __this, int32_t ___index0, SqliteParameter_t354437343 * ___param1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] Mono.Data.Sqlite.SqliteStatement::get_TypeDefinitions()
extern "C"  StringU5BU5D_t1642385972* SqliteStatement_get_TypeDefinitions_m2691691414 (SqliteStatement_t4106757957 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteStatement::SetTypes(System.String)
extern "C"  void SqliteStatement_SetTypes_m2069379721 (SqliteStatement_t4106757957 * __this, String_t* ___typedefs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
