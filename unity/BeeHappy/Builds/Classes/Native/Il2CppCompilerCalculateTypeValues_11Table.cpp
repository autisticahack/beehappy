﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Specialized_HybridDiction290043810.h"
#include "System_System_Collections_Specialized_ListDictiona3458713452.h"
#include "System_System_Collections_Specialized_ListDictiona2725637098.h"
#include "System_System_Collections_Specialized_ListDictiona1923170152.h"
#include "System_System_Collections_Specialized_ListDictionar528898270.h"
#include "System_System_Collections_Specialized_ListDictiona2037848305.h"
#include "System_System_Collections_Specialized_NameObjectCo2034248631.h"
#include "System_System_Collections_Specialized_NameObjectCo3244489099.h"
#include "System_System_Collections_Specialized_NameObjectCo1718269396.h"
#include "System_System_Collections_Specialized_NameObjectCol633582367.h"
#include "System_System_Collections_Specialized_NameValueCol3047564564.h"
#include "System_System_Collections_Specialized_StringCollect352985975.h"
#include "System_System_Collections_Specialized_StringDictio1070889667.h"
#include "System_System_Collections_Specialized_StringEnumera441637433.h"
#include "System_System_ComponentModel_BrowsableAttribute2487167291.h"
#include "System_System_ComponentModel_CategoryAttribute540457070.h"
#include "System_System_ComponentModel_CollectionChangeActio3495844100.h"
#include "System_System_ComponentModel_CollectionChangeEvent1734749345.h"
#include "System_System_ComponentModel_Component2826673791.h"
#include "System_System_ComponentModel_ComponentConverter3121608223.h"
#include "System_System_ComponentModel_DefaultEventAttribute1079704873.h"
#include "System_System_ComponentModel_DefaultPropertyAttrib1962767338.h"
#include "System_System_ComponentModel_DefaultValueAttribute1302720498.h"
#include "System_System_ComponentModel_DescriptionAttribute3207779672.h"
#include "System_System_ComponentModel_DesignOnlyAttribute2394309572.h"
#include "System_System_ComponentModel_DesignTimeVisibleAttr2120749151.h"
#include "System_System_ComponentModel_DesignerAttribute2778719479.h"
#include "System_System_ComponentModel_DesignerCategoryAttri1270090451.h"
#include "System_System_ComponentModel_DesignerSerialization3751360903.h"
#include "System_System_ComponentModel_DesignerSerialization2980019899.h"
#include "System_System_ComponentModel_EditorAttribute3559776959.h"
#include "System_System_ComponentModel_EditorBrowsableAttrib1050682502.h"
#include "System_System_ComponentModel_EditorBrowsableState373498655.h"
#include "System_System_ComponentModel_ListEntry935815304.h"
#include "System_System_ComponentModel_EventHandlerList1298116880.h"
#include "System_System_ComponentModel_ExpandableObjectConve1139197709.h"
#include "System_System_ComponentModel_InvalidEnumArgumentEx3709744516.h"
#include "System_System_ComponentModel_ListBindableAttribute1092011273.h"
#include "System_System_ComponentModel_ListChangedEventArgs3132270315.h"
#include "System_System_ComponentModel_ListChangedType3463990274.h"
#include "System_System_ComponentModel_ListSortDirection4186912589.h"
#include "System_System_ComponentModel_MarshalByValueCompone3997823175.h"
#include "System_System_ComponentModel_MemberDescriptor3749827553.h"
#include "System_System_ComponentModel_MergablePropertyAttri1597820506.h"
#include "System_System_ComponentModel_PropertyChangedEventA1689446432.h"
#include "System_System_ComponentModel_PropertyDescriptor4250402154.h"
#include "System_System_ComponentModel_PropertyDescriptorCol3166009492.h"
#include "System_System_ComponentModel_ReadOnlyAttribute4102148880.h"
#include "System_System_ComponentModel_RecommendedAsConfigura420947846.h"
#include "System_System_ComponentModel_ReferenceConverter3131270729.h"
#include "System_System_ComponentModel_RefreshEventArgs2077477224.h"
#include "System_System_ComponentModel_RefreshProperties2240171922.h"
#include "System_System_ComponentModel_RefreshPropertiesAttr2234294918.h"
#include "System_System_ComponentModel_StringConverter3749524419.h"
#include "System_System_ComponentModel_ToolboxItemAttribute3063187714.h"
#include "System_System_ComponentModel_TypeConverter745995970.h"
#include "System_System_ComponentModel_TypeConverterAttribute252469870.h"
#include "System_System_ComponentModel_TypeDescriptionProvid2438624375.h"
#include "System_System_ComponentModel_TypeDescriptor3595688691.h"
#include "System_System_ComponentModel_WeakObjectWrapper2012978780.h"
#include "System_System_ComponentModel_WeakObjectWrapperComp3891611113.h"
#include "System_System_ComponentModel_Win32Exception1708275760.h"
#include "System_System_Diagnostics_Stopwatch1380178105.h"
#include "System_System_Diagnostics_TraceListener3414949279.h"
#include "System_System_IO_Compression_CompressionMode1471062003.h"
#include "System_System_IO_Compression_DeflateStream3198596725.h"
#include "System_System_IO_Compression_DeflateStream_Unmanag1990215745.h"
#include "System_System_IO_Compression_DeflateStream_ReadMet3362229488.h"
#include "System_System_IO_Compression_DeflateStream_WriteMe1894833619.h"
#include "System_System_IO_Compression_GZipStream2274754946.h"
#include "System_System_Net_Security_AuthenticatedStream1183414097.h"
#include "System_System_Net_Security_AuthenticationLevel2424130044.h"
#include "System_System_Net_Security_SslPolicyErrors1928581431.h"
#include "System_System_Net_Security_SslStream1853163792.h"
#include "System_System_Net_Security_SslStream_U3CBeginAuthe1358332250.h"
#include "System_System_Net_Sockets_AddressFamily303362630.h"
#include "System_System_Net_Sockets_LingerOption1165263720.h"
#include "System_System_Net_Sockets_MulticastOption2505469155.h"
#include "System_System_Net_Sockets_NetworkStream581172200.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1100 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1100[9] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1101 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1101[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1102 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1102[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1103 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1103[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1104 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1104[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1105 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1105[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1106 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1106[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1107 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1107[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1108 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1108[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1109 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1109[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1110 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1110[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1111 = { sizeof (HybridDictionary_t290043810), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1111[3] = 
{
	HybridDictionary_t290043810::get_offset_of_caseInsensitive_0(),
	HybridDictionary_t290043810::get_offset_of_hashtable_1(),
	HybridDictionary_t290043810::get_offset_of_list_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1112 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1113 = { sizeof (ListDictionary_t3458713452), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1113[4] = 
{
	ListDictionary_t3458713452::get_offset_of_count_0(),
	ListDictionary_t3458713452::get_offset_of_version_1(),
	ListDictionary_t3458713452::get_offset_of_head_2(),
	ListDictionary_t3458713452::get_offset_of_comparer_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1114 = { sizeof (DictionaryNode_t2725637098), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1114[3] = 
{
	DictionaryNode_t2725637098::get_offset_of_key_0(),
	DictionaryNode_t2725637098::get_offset_of_value_1(),
	DictionaryNode_t2725637098::get_offset_of_next_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1115 = { sizeof (DictionaryNodeEnumerator_t1923170152), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1115[4] = 
{
	DictionaryNodeEnumerator_t1923170152::get_offset_of_dict_0(),
	DictionaryNodeEnumerator_t1923170152::get_offset_of_isAtStart_1(),
	DictionaryNodeEnumerator_t1923170152::get_offset_of_current_2(),
	DictionaryNodeEnumerator_t1923170152::get_offset_of_version_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1116 = { sizeof (DictionaryNodeCollection_t528898270), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1116[2] = 
{
	DictionaryNodeCollection_t528898270::get_offset_of_dict_0(),
	DictionaryNodeCollection_t528898270::get_offset_of_isKeyList_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1117 = { sizeof (DictionaryNodeCollectionEnumerator_t2037848305), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1117[2] = 
{
	DictionaryNodeCollectionEnumerator_t2037848305::get_offset_of_inner_0(),
	DictionaryNodeCollectionEnumerator_t2037848305::get_offset_of_isKeyList_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1118 = { sizeof (NameObjectCollectionBase_t2034248631), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1118[10] = 
{
	NameObjectCollectionBase_t2034248631::get_offset_of_m_ItemsContainer_0(),
	NameObjectCollectionBase_t2034248631::get_offset_of_m_NullKeyItem_1(),
	NameObjectCollectionBase_t2034248631::get_offset_of_m_ItemsArray_2(),
	NameObjectCollectionBase_t2034248631::get_offset_of_m_hashprovider_3(),
	NameObjectCollectionBase_t2034248631::get_offset_of_m_comparer_4(),
	NameObjectCollectionBase_t2034248631::get_offset_of_m_defCapacity_5(),
	NameObjectCollectionBase_t2034248631::get_offset_of_m_readonly_6(),
	NameObjectCollectionBase_t2034248631::get_offset_of_infoCopy_7(),
	NameObjectCollectionBase_t2034248631::get_offset_of_keyscoll_8(),
	NameObjectCollectionBase_t2034248631::get_offset_of_equality_comparer_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1119 = { sizeof (_Item_t3244489099), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1119[2] = 
{
	_Item_t3244489099::get_offset_of_key_0(),
	_Item_t3244489099::get_offset_of_value_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1120 = { sizeof (_KeysEnumerator_t1718269396), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1120[2] = 
{
	_KeysEnumerator_t1718269396::get_offset_of_m_collection_0(),
	_KeysEnumerator_t1718269396::get_offset_of_m_position_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1121 = { sizeof (KeysCollection_t633582367), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1121[1] = 
{
	KeysCollection_t633582367::get_offset_of_m_collection_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1122 = { sizeof (NameValueCollection_t3047564564), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1122[2] = 
{
	NameValueCollection_t3047564564::get_offset_of_cachedAllKeys_10(),
	NameValueCollection_t3047564564::get_offset_of_cachedAll_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1123 = { sizeof (StringCollection_t352985975), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1123[1] = 
{
	StringCollection_t352985975::get_offset_of_data_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1124 = { sizeof (StringDictionary_t1070889667), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1124[1] = 
{
	StringDictionary_t1070889667::get_offset_of_contents_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1125 = { sizeof (StringEnumerator_t441637433), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1125[1] = 
{
	StringEnumerator_t441637433::get_offset_of_enumerable_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1126 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1127 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1128 = { sizeof (BrowsableAttribute_t2487167291), -1, sizeof(BrowsableAttribute_t2487167291_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1128[4] = 
{
	BrowsableAttribute_t2487167291::get_offset_of_browsable_0(),
	BrowsableAttribute_t2487167291_StaticFields::get_offset_of_Default_1(),
	BrowsableAttribute_t2487167291_StaticFields::get_offset_of_No_2(),
	BrowsableAttribute_t2487167291_StaticFields::get_offset_of_Yes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1129 = { sizeof (CategoryAttribute_t540457070), -1, sizeof(CategoryAttribute_t540457070_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1129[3] = 
{
	CategoryAttribute_t540457070::get_offset_of_category_0(),
	CategoryAttribute_t540457070::get_offset_of_IsLocalized_1(),
	CategoryAttribute_t540457070_StaticFields::get_offset_of_lockobj_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1130 = { sizeof (CollectionChangeAction_t3495844100)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1130[4] = 
{
	CollectionChangeAction_t3495844100::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1131 = { sizeof (CollectionChangeEventArgs_t1734749345), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1131[2] = 
{
	CollectionChangeEventArgs_t1734749345::get_offset_of_changeAction_1(),
	CollectionChangeEventArgs_t1734749345::get_offset_of_theElement_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1132 = { sizeof (Component_t2826673791), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1132[3] = 
{
	Component_t2826673791::get_offset_of_event_handlers_1(),
	Component_t2826673791::get_offset_of_mySite_2(),
	Component_t2826673791::get_offset_of_disposedEvent_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1133 = { sizeof (ComponentConverter_t3121608223), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1134 = { sizeof (DefaultEventAttribute_t1079704873), -1, sizeof(DefaultEventAttribute_t1079704873_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1134[2] = 
{
	DefaultEventAttribute_t1079704873::get_offset_of_eventName_0(),
	DefaultEventAttribute_t1079704873_StaticFields::get_offset_of_Default_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1135 = { sizeof (DefaultPropertyAttribute_t1962767338), -1, sizeof(DefaultPropertyAttribute_t1962767338_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1135[2] = 
{
	DefaultPropertyAttribute_t1962767338::get_offset_of_property_name_0(),
	DefaultPropertyAttribute_t1962767338_StaticFields::get_offset_of_Default_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1136 = { sizeof (DefaultValueAttribute_t1302720498), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1136[1] = 
{
	DefaultValueAttribute_t1302720498::get_offset_of_DefaultValue_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1137 = { sizeof (DescriptionAttribute_t3207779672), -1, sizeof(DescriptionAttribute_t3207779672_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1137[2] = 
{
	DescriptionAttribute_t3207779672::get_offset_of_desc_0(),
	DescriptionAttribute_t3207779672_StaticFields::get_offset_of_Default_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1138 = { sizeof (DesignOnlyAttribute_t2394309572), -1, sizeof(DesignOnlyAttribute_t2394309572_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1138[4] = 
{
	DesignOnlyAttribute_t2394309572::get_offset_of_design_only_0(),
	DesignOnlyAttribute_t2394309572_StaticFields::get_offset_of_Default_1(),
	DesignOnlyAttribute_t2394309572_StaticFields::get_offset_of_No_2(),
	DesignOnlyAttribute_t2394309572_StaticFields::get_offset_of_Yes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1139 = { sizeof (DesignTimeVisibleAttribute_t2120749151), -1, sizeof(DesignTimeVisibleAttribute_t2120749151_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1139[4] = 
{
	DesignTimeVisibleAttribute_t2120749151::get_offset_of_visible_0(),
	DesignTimeVisibleAttribute_t2120749151_StaticFields::get_offset_of_Default_1(),
	DesignTimeVisibleAttribute_t2120749151_StaticFields::get_offset_of_No_2(),
	DesignTimeVisibleAttribute_t2120749151_StaticFields::get_offset_of_Yes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1140 = { sizeof (DesignerAttribute_t2778719479), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1140[2] = 
{
	DesignerAttribute_t2778719479::get_offset_of_name_0(),
	DesignerAttribute_t2778719479::get_offset_of_basetypename_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1141 = { sizeof (DesignerCategoryAttribute_t1270090451), -1, sizeof(DesignerCategoryAttribute_t1270090451_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1141[5] = 
{
	DesignerCategoryAttribute_t1270090451::get_offset_of_category_0(),
	DesignerCategoryAttribute_t1270090451_StaticFields::get_offset_of_Component_1(),
	DesignerCategoryAttribute_t1270090451_StaticFields::get_offset_of_Form_2(),
	DesignerCategoryAttribute_t1270090451_StaticFields::get_offset_of_Generic_3(),
	DesignerCategoryAttribute_t1270090451_StaticFields::get_offset_of_Default_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1142 = { sizeof (DesignerSerializationVisibility_t3751360903)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1142[4] = 
{
	DesignerSerializationVisibility_t3751360903::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1143 = { sizeof (DesignerSerializationVisibilityAttribute_t2980019899), -1, sizeof(DesignerSerializationVisibilityAttribute_t2980019899_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1143[5] = 
{
	DesignerSerializationVisibilityAttribute_t2980019899::get_offset_of_visibility_0(),
	DesignerSerializationVisibilityAttribute_t2980019899_StaticFields::get_offset_of_Default_1(),
	DesignerSerializationVisibilityAttribute_t2980019899_StaticFields::get_offset_of_Content_2(),
	DesignerSerializationVisibilityAttribute_t2980019899_StaticFields::get_offset_of_Hidden_3(),
	DesignerSerializationVisibilityAttribute_t2980019899_StaticFields::get_offset_of_Visible_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1144 = { sizeof (EditorAttribute_t3559776959), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1144[2] = 
{
	EditorAttribute_t3559776959::get_offset_of_name_0(),
	EditorAttribute_t3559776959::get_offset_of_basename_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1145 = { sizeof (EditorBrowsableAttribute_t1050682502), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1145[1] = 
{
	EditorBrowsableAttribute_t1050682502::get_offset_of_state_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1146 = { sizeof (EditorBrowsableState_t373498655)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1146[4] = 
{
	EditorBrowsableState_t373498655::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1147 = { sizeof (ListEntry_t935815304), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1147[3] = 
{
	ListEntry_t935815304::get_offset_of_key_0(),
	ListEntry_t935815304::get_offset_of_value_1(),
	ListEntry_t935815304::get_offset_of_next_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1148 = { sizeof (EventHandlerList_t1298116880), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1148[2] = 
{
	EventHandlerList_t1298116880::get_offset_of_entries_0(),
	EventHandlerList_t1298116880::get_offset_of_null_entry_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1149 = { sizeof (ExpandableObjectConverter_t1139197709), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1150 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1151 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1152 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1153 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1154 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1155 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1156 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1157 = { sizeof (InvalidEnumArgumentException_t3709744516), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1158 = { sizeof (ListBindableAttribute_t1092011273), -1, sizeof(ListBindableAttribute_t1092011273_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1158[4] = 
{
	ListBindableAttribute_t1092011273_StaticFields::get_offset_of_Default_0(),
	ListBindableAttribute_t1092011273_StaticFields::get_offset_of_No_1(),
	ListBindableAttribute_t1092011273_StaticFields::get_offset_of_Yes_2(),
	ListBindableAttribute_t1092011273::get_offset_of_bindable_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1159 = { sizeof (ListChangedEventArgs_t3132270315), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1159[3] = 
{
	ListChangedEventArgs_t3132270315::get_offset_of_changedType_1(),
	ListChangedEventArgs_t3132270315::get_offset_of_oldIndex_2(),
	ListChangedEventArgs_t3132270315::get_offset_of_newIndex_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1160 = { sizeof (ListChangedType_t3463990274)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1160[9] = 
{
	ListChangedType_t3463990274::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1161 = { sizeof (ListSortDirection_t4186912589)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1161[3] = 
{
	ListSortDirection_t4186912589::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1162 = { sizeof (MarshalByValueComponent_t3997823175), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1162[2] = 
{
	MarshalByValueComponent_t3997823175::get_offset_of_mySite_0(),
	MarshalByValueComponent_t3997823175::get_offset_of_disposedEvent_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1163 = { sizeof (MemberDescriptor_t3749827553), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1163[1] = 
{
	MemberDescriptor_t3749827553::get_offset_of_name_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1164 = { sizeof (MergablePropertyAttribute_t1597820506), -1, sizeof(MergablePropertyAttribute_t1597820506_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1164[4] = 
{
	MergablePropertyAttribute_t1597820506::get_offset_of_mergable_0(),
	MergablePropertyAttribute_t1597820506_StaticFields::get_offset_of_Default_1(),
	MergablePropertyAttribute_t1597820506_StaticFields::get_offset_of_No_2(),
	MergablePropertyAttribute_t1597820506_StaticFields::get_offset_of_Yes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1165 = { sizeof (PropertyChangedEventArgs_t1689446432), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1165[1] = 
{
	PropertyChangedEventArgs_t1689446432::get_offset_of_propertyName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1166 = { sizeof (PropertyDescriptor_t4250402154), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1167 = { sizeof (PropertyDescriptorCollection_t3166009492), -1, sizeof(PropertyDescriptorCollection_t3166009492_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1167[3] = 
{
	PropertyDescriptorCollection_t3166009492_StaticFields::get_offset_of_Empty_0(),
	PropertyDescriptorCollection_t3166009492::get_offset_of_properties_1(),
	PropertyDescriptorCollection_t3166009492::get_offset_of_readOnly_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1168 = { sizeof (ReadOnlyAttribute_t4102148880), -1, sizeof(ReadOnlyAttribute_t4102148880_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1168[4] = 
{
	ReadOnlyAttribute_t4102148880::get_offset_of_read_only_0(),
	ReadOnlyAttribute_t4102148880_StaticFields::get_offset_of_No_1(),
	ReadOnlyAttribute_t4102148880_StaticFields::get_offset_of_Yes_2(),
	ReadOnlyAttribute_t4102148880_StaticFields::get_offset_of_Default_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1169 = { sizeof (RecommendedAsConfigurableAttribute_t420947846), -1, sizeof(RecommendedAsConfigurableAttribute_t420947846_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1169[4] = 
{
	RecommendedAsConfigurableAttribute_t420947846::get_offset_of_recommendedAsConfigurable_0(),
	RecommendedAsConfigurableAttribute_t420947846_StaticFields::get_offset_of_Default_1(),
	RecommendedAsConfigurableAttribute_t420947846_StaticFields::get_offset_of_No_2(),
	RecommendedAsConfigurableAttribute_t420947846_StaticFields::get_offset_of_Yes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1170 = { sizeof (ReferenceConverter_t3131270729), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1171 = { sizeof (RefreshEventArgs_t2077477224), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1171[2] = 
{
	RefreshEventArgs_t2077477224::get_offset_of_component_1(),
	RefreshEventArgs_t2077477224::get_offset_of_type_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1172 = { sizeof (RefreshProperties_t2240171922)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1172[4] = 
{
	RefreshProperties_t2240171922::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1173 = { sizeof (RefreshPropertiesAttribute_t2234294918), -1, sizeof(RefreshPropertiesAttribute_t2234294918_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1173[4] = 
{
	RefreshPropertiesAttribute_t2234294918::get_offset_of_refresh_0(),
	RefreshPropertiesAttribute_t2234294918_StaticFields::get_offset_of_All_1(),
	RefreshPropertiesAttribute_t2234294918_StaticFields::get_offset_of_Default_2(),
	RefreshPropertiesAttribute_t2234294918_StaticFields::get_offset_of_Repaint_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1174 = { sizeof (StringConverter_t3749524419), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1175 = { sizeof (ToolboxItemAttribute_t3063187714), -1, sizeof(ToolboxItemAttribute_t3063187714_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1175[4] = 
{
	ToolboxItemAttribute_t3063187714_StaticFields::get_offset_of_Default_0(),
	ToolboxItemAttribute_t3063187714_StaticFields::get_offset_of_None_1(),
	ToolboxItemAttribute_t3063187714::get_offset_of_itemType_2(),
	ToolboxItemAttribute_t3063187714::get_offset_of_itemTypeName_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1176 = { sizeof (TypeConverter_t745995970), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1177 = { sizeof (TypeConverterAttribute_t252469870), -1, sizeof(TypeConverterAttribute_t252469870_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1177[2] = 
{
	TypeConverterAttribute_t252469870_StaticFields::get_offset_of_Default_0(),
	TypeConverterAttribute_t252469870::get_offset_of_converter_type_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1178 = { sizeof (TypeDescriptionProvider_t2438624375), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1179 = { sizeof (TypeDescriptor_t3595688691), -1, sizeof(TypeDescriptor_t3595688691_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1179[8] = 
{
	TypeDescriptor_t3595688691_StaticFields::get_offset_of_creatingDefaultConverters_0(),
	TypeDescriptor_t3595688691_StaticFields::get_offset_of_componentTable_1(),
	TypeDescriptor_t3595688691_StaticFields::get_offset_of_typeTable_2(),
	TypeDescriptor_t3595688691_StaticFields::get_offset_of_typeDescriptionProvidersLock_3(),
	TypeDescriptor_t3595688691_StaticFields::get_offset_of_typeDescriptionProviders_4(),
	TypeDescriptor_t3595688691_StaticFields::get_offset_of_componentDescriptionProvidersLock_5(),
	TypeDescriptor_t3595688691_StaticFields::get_offset_of_componentDescriptionProviders_6(),
	TypeDescriptor_t3595688691_StaticFields::get_offset_of_Refreshed_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1180 = { sizeof (WeakObjectWrapper_t2012978780), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1180[2] = 
{
	WeakObjectWrapper_t2012978780::get_offset_of_U3CTargetHashCodeU3Ek__BackingField_0(),
	WeakObjectWrapper_t2012978780::get_offset_of_U3CWeakU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1181 = { sizeof (WeakObjectWrapperComparer_t3891611113), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1182 = { sizeof (Win32Exception_t1708275760), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1182[1] = 
{
	Win32Exception_t1708275760::get_offset_of_native_error_code_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1183 = { sizeof (Stopwatch_t1380178105), -1, sizeof(Stopwatch_t1380178105_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1183[5] = 
{
	Stopwatch_t1380178105_StaticFields::get_offset_of_Frequency_0(),
	Stopwatch_t1380178105_StaticFields::get_offset_of_IsHighResolution_1(),
	Stopwatch_t1380178105::get_offset_of_elapsed_2(),
	Stopwatch_t1380178105::get_offset_of_started_3(),
	Stopwatch_t1380178105::get_offset_of_is_running_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1184 = { sizeof (TraceListener_t3414949279), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1184[4] = 
{
	TraceListener_t3414949279::get_offset_of_indentSize_1(),
	TraceListener_t3414949279::get_offset_of_attributes_2(),
	TraceListener_t3414949279::get_offset_of_name_3(),
	TraceListener_t3414949279::get_offset_of_needIndent_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1185 = { sizeof (CompressionMode_t1471062003)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1185[3] = 
{
	CompressionMode_t1471062003::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1186 = { sizeof (DeflateStream_t3198596725), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1186[8] = 
{
	DeflateStream_t3198596725::get_offset_of_base_stream_1(),
	DeflateStream_t3198596725::get_offset_of_mode_2(),
	DeflateStream_t3198596725::get_offset_of_leaveOpen_3(),
	DeflateStream_t3198596725::get_offset_of_disposed_4(),
	DeflateStream_t3198596725::get_offset_of_feeder_5(),
	DeflateStream_t3198596725::get_offset_of_z_stream_6(),
	DeflateStream_t3198596725::get_offset_of_io_buffer_7(),
	DeflateStream_t3198596725::get_offset_of_data_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1187 = { sizeof (UnmanagedReadOrWrite_t1990215745), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1188 = { sizeof (ReadMethod_t3362229488), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1189 = { sizeof (WriteMethod_t1894833619), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1190 = { sizeof (GZipStream_t2274754946), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1190[1] = 
{
	GZipStream_t2274754946::get_offset_of_deflateStream_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1191 = { sizeof (AuthenticatedStream_t1183414097), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1191[2] = 
{
	AuthenticatedStream_t1183414097::get_offset_of_innerStream_1(),
	AuthenticatedStream_t1183414097::get_offset_of_leaveStreamOpen_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1192 = { sizeof (AuthenticationLevel_t2424130044)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1192[4] = 
{
	AuthenticationLevel_t2424130044::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1193 = { sizeof (SslPolicyErrors_t1928581431)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1193[5] = 
{
	SslPolicyErrors_t1928581431::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1194 = { sizeof (SslStream_t1853163792), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1194[3] = 
{
	SslStream_t1853163792::get_offset_of_ssl_stream_3(),
	SslStream_t1853163792::get_offset_of_validation_callback_4(),
	SslStream_t1853163792::get_offset_of_selection_callback_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1195 = { sizeof (U3CBeginAuthenticateAsClientU3Ec__AnonStorey7_t1358332250), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1195[2] = 
{
	U3CBeginAuthenticateAsClientU3Ec__AnonStorey7_t1358332250::get_offset_of_clientCertificates_0(),
	U3CBeginAuthenticateAsClientU3Ec__AnonStorey7_t1358332250::get_offset_of_U3CU3Ef__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1196 = { sizeof (AddressFamily_t303362630)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1196[32] = 
{
	AddressFamily_t303362630::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1197 = { sizeof (LingerOption_t1165263720), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1197[2] = 
{
	LingerOption_t1165263720::get_offset_of_enabled_0(),
	LingerOption_t1165263720::get_offset_of_seconds_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1198 = { sizeof (MulticastOption_t2505469155), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1199 = { sizeof (NetworkStream_t581172200), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1199[6] = 
{
	NetworkStream_t581172200::get_offset_of_access_1(),
	NetworkStream_t581172200::get_offset_of_socket_2(),
	NetworkStream_t581172200::get_offset_of_owns_socket_3(),
	NetworkStream_t581172200::get_offset_of_readable_4(),
	NetworkStream_t581172200::get_offset_of_writeable_5(),
	NetworkStream_t581172200::get_offset_of_disposed_6(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
