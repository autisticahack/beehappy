﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.Util.CryptoUtilFactory/CryptoUtil
struct CryptoUtil_t1025015063;
// System.Collections.Generic.HashSet`1<System.String>
struct HashSet_1_t362681087;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Util.CryptoUtilFactory
struct  CryptoUtilFactory_t2421970493  : public Il2CppObject
{
public:

public:
};

struct CryptoUtilFactory_t2421970493_StaticFields
{
public:
	// Amazon.Util.CryptoUtilFactory/CryptoUtil Amazon.Util.CryptoUtilFactory::util
	CryptoUtil_t1025015063 * ___util_0;
	// System.Collections.Generic.HashSet`1<System.String> Amazon.Util.CryptoUtilFactory::_initializedAlgorithmNames
	HashSet_1_t362681087 * ____initializedAlgorithmNames_1;
	// System.Object Amazon.Util.CryptoUtilFactory::_keyedHashAlgorithmCreationLock
	Il2CppObject * ____keyedHashAlgorithmCreationLock_2;

public:
	inline static int32_t get_offset_of_util_0() { return static_cast<int32_t>(offsetof(CryptoUtilFactory_t2421970493_StaticFields, ___util_0)); }
	inline CryptoUtil_t1025015063 * get_util_0() const { return ___util_0; }
	inline CryptoUtil_t1025015063 ** get_address_of_util_0() { return &___util_0; }
	inline void set_util_0(CryptoUtil_t1025015063 * value)
	{
		___util_0 = value;
		Il2CppCodeGenWriteBarrier(&___util_0, value);
	}

	inline static int32_t get_offset_of__initializedAlgorithmNames_1() { return static_cast<int32_t>(offsetof(CryptoUtilFactory_t2421970493_StaticFields, ____initializedAlgorithmNames_1)); }
	inline HashSet_1_t362681087 * get__initializedAlgorithmNames_1() const { return ____initializedAlgorithmNames_1; }
	inline HashSet_1_t362681087 ** get_address_of__initializedAlgorithmNames_1() { return &____initializedAlgorithmNames_1; }
	inline void set__initializedAlgorithmNames_1(HashSet_1_t362681087 * value)
	{
		____initializedAlgorithmNames_1 = value;
		Il2CppCodeGenWriteBarrier(&____initializedAlgorithmNames_1, value);
	}

	inline static int32_t get_offset_of__keyedHashAlgorithmCreationLock_2() { return static_cast<int32_t>(offsetof(CryptoUtilFactory_t2421970493_StaticFields, ____keyedHashAlgorithmCreationLock_2)); }
	inline Il2CppObject * get__keyedHashAlgorithmCreationLock_2() const { return ____keyedHashAlgorithmCreationLock_2; }
	inline Il2CppObject ** get_address_of__keyedHashAlgorithmCreationLock_2() { return &____keyedHashAlgorithmCreationLock_2; }
	inline void set__keyedHashAlgorithmCreationLock_2(Il2CppObject * value)
	{
		____keyedHashAlgorithmCreationLock_2 = value;
		Il2CppCodeGenWriteBarrier(&____keyedHashAlgorithmCreationLock_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
