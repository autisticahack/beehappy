﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.CognitoSync.SyncManager.DatasetUpdates
struct DatasetUpdates_t2321405236;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.Record>
struct List_1_t237920701;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Boolean Amazon.CognitoSync.SyncManager.DatasetUpdates::get_Deleted()
extern "C"  bool DatasetUpdates_get_Deleted_m1515177712 (DatasetUpdates_t2321405236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.CognitoSync.SyncManager.DatasetUpdates::get_Exists()
extern "C"  bool DatasetUpdates_get_Exists_m2124060329 (DatasetUpdates_t2321405236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> Amazon.CognitoSync.SyncManager.DatasetUpdates::get_MergedDatasetNameList()
extern "C"  List_1_t1398341365 * DatasetUpdates_get_MergedDatasetNameList_m637312269 (DatasetUpdates_t2321405236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.Record> Amazon.CognitoSync.SyncManager.DatasetUpdates::get_Records()
extern "C"  List_1_t237920701 * DatasetUpdates_get_Records_m1151559775 (DatasetUpdates_t2321405236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Amazon.CognitoSync.SyncManager.DatasetUpdates::get_SyncCount()
extern "C"  int64_t DatasetUpdates_get_SyncCount_m3080733778 (DatasetUpdates_t2321405236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.CognitoSync.SyncManager.DatasetUpdates::get_SyncSessionToken()
extern "C"  String_t* DatasetUpdates_get_SyncSessionToken_m761697608 (DatasetUpdates_t2321405236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.SyncManager.DatasetUpdates::.ctor(System.String,System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.Record>,System.Int64,System.String,System.Boolean,System.Boolean,System.Collections.Generic.List`1<System.String>)
extern "C"  void DatasetUpdates__ctor_m1311594993 (DatasetUpdates_t2321405236 * __this, String_t* ___datasetName0, List_1_t237920701 * ___records1, int64_t ___syncCount2, String_t* ___syncSessionToken3, bool ___exists4, bool ___deleted5, List_1_t1398341365 * ___mergedDatasetNameList6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
