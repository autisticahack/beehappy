﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1<System.IntPtr>
struct EqualityComparer_1_t1077695880;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.Generic.EqualityComparer`1<System.IntPtr>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m4140732359_gshared (EqualityComparer_1_t1077695880 * __this, const MethodInfo* method);
#define EqualityComparer_1__ctor_m4140732359(__this, method) ((  void (*) (EqualityComparer_1_t1077695880 *, const MethodInfo*))EqualityComparer_1__ctor_m4140732359_gshared)(__this, method)
// System.Void System.Collections.Generic.EqualityComparer`1<System.IntPtr>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m3033994022_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1__cctor_m3033994022(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1__cctor_m3033994022_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.IntPtr>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m487741360_gshared (EqualityComparer_1_t1077695880 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m487741360(__this, ___obj0, method) ((  int32_t (*) (EqualityComparer_1_t1077695880 *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m487741360_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.IntPtr>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m459863666_gshared (EqualityComparer_1_t1077695880 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m459863666(__this, ___x0, ___y1, method) ((  bool (*) (EqualityComparer_1_t1077695880 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m459863666_gshared)(__this, ___x0, ___y1, method)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.IntPtr>::get_Default()
extern "C"  EqualityComparer_1_t1077695880 * EqualityComparer_1_get_Default_m465946551_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1_get_Default_m465946551(__this /* static, unused */, method) ((  EqualityComparer_1_t1077695880 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1_get_Default_m465946551_gshared)(__this /* static, unused */, method)
