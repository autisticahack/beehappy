﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AWSSDK_Core_Amazon_Runtime_AWSRegion969138115.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.AppConfigAWSRegion
struct  AppConfigAWSRegion_t3930413220  : public AWSRegion_t969138115
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
