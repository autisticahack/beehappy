﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ThirdParty.iOS4Unity.Selector
struct Selector_t1939762325;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_IntPtr2504060609.h"

// System.Void ThirdParty.iOS4Unity.Selector::.ctor(System.String,System.Boolean)
extern "C"  void Selector__ctor_m2958736194 (Selector_t1939762325 * __this, String_t* ___name0, bool ___alloc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.iOS4Unity.Selector::.ctor(System.String)
extern "C"  void Selector__ctor_m3707829215 (Selector_t1939762325 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr ThirdParty.iOS4Unity.Selector::get_Handle()
extern "C"  IntPtr_t Selector_get_Handle_m351135527 (Selector_t1939762325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr ThirdParty.iOS4Unity.Selector::GetHandle(System.String)
extern "C"  IntPtr_t Selector_GetHandle_m3802952106 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.iOS4Unity.Selector::.cctor()
extern "C"  void Selector__cctor_m2969781974 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
