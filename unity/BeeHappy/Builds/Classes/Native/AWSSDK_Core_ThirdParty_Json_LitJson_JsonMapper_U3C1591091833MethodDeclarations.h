﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ThirdParty.Json.LitJson.JsonMapper/<>c
struct U3CU3Ec_t1591091833;
// System.Object
struct Il2CppObject;
// ThirdParty.Json.LitJson.JsonWriter
struct JsonWriter_t3014444111;
// ThirdParty.Json.LitJson.IJsonWrapper
struct IJsonWrapper_t3095378610;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_JsonWriter3014444111.h"

// System.Void ThirdParty.Json.LitJson.JsonMapper/<>c::.cctor()
extern "C"  void U3CU3Ec__cctor_m1749976183 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.JsonMapper/<>c::.ctor()
extern "C"  void U3CU3Ec__ctor_m48908184 (U3CU3Ec_t1591091833 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseExporters>b__24_0(System.Object,ThirdParty.Json.LitJson.JsonWriter)
extern "C"  void U3CU3Ec_U3CRegisterBaseExportersU3Eb__24_0_m3647892888 (U3CU3Ec_t1591091833 * __this, Il2CppObject * ___obj0, JsonWriter_t3014444111 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseExporters>b__24_1(System.Object,ThirdParty.Json.LitJson.JsonWriter)
extern "C"  void U3CU3Ec_U3CRegisterBaseExportersU3Eb__24_1_m403640019 (U3CU3Ec_t1591091833 * __this, Il2CppObject * ___obj0, JsonWriter_t3014444111 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseExporters>b__24_2(System.Object,ThirdParty.Json.LitJson.JsonWriter)
extern "C"  void U3CU3Ec_U3CRegisterBaseExportersU3Eb__24_2_m626278178 (U3CU3Ec_t1591091833 * __this, Il2CppObject * ___obj0, JsonWriter_t3014444111 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseExporters>b__24_3(System.Object,ThirdParty.Json.LitJson.JsonWriter)
extern "C"  void U3CU3Ec_U3CRegisterBaseExportersU3Eb__24_3_m564382813 (U3CU3Ec_t1591091833 * __this, Il2CppObject * ___obj0, JsonWriter_t3014444111 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseExporters>b__24_4(System.Object,ThirdParty.Json.LitJson.JsonWriter)
extern "C"  void U3CU3Ec_U3CRegisterBaseExportersU3Eb__24_4_m3305872516 (U3CU3Ec_t1591091833 * __this, Il2CppObject * ___obj0, JsonWriter_t3014444111 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseExporters>b__24_5(System.Object,ThirdParty.Json.LitJson.JsonWriter)
extern "C"  void U3CU3Ec_U3CRegisterBaseExportersU3Eb__24_5_m61619647 (U3CU3Ec_t1591091833 * __this, Il2CppObject * ___obj0, JsonWriter_t3014444111 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseExporters>b__24_6(System.Object,ThirdParty.Json.LitJson.JsonWriter)
extern "C"  void U3CU3Ec_U3CRegisterBaseExportersU3Eb__24_6_m284257806 (U3CU3Ec_t1591091833 * __this, Il2CppObject * ___obj0, JsonWriter_t3014444111 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseExporters>b__24_7(System.Object,ThirdParty.Json.LitJson.JsonWriter)
extern "C"  void U3CU3Ec_U3CRegisterBaseExportersU3Eb__24_7_m222362441 (U3CU3Ec_t1591091833 * __this, Il2CppObject * ___obj0, JsonWriter_t3014444111 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseExporters>b__24_8(System.Object,ThirdParty.Json.LitJson.JsonWriter)
extern "C"  void U3CU3Ec_U3CRegisterBaseExportersU3Eb__24_8_m931111872 (U3CU3Ec_t1591091833 * __this, Il2CppObject * ___obj0, JsonWriter_t3014444111 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseExporters>b__24_9(System.Object,ThirdParty.Json.LitJson.JsonWriter)
extern "C"  void U3CU3Ec_U3CRegisterBaseExportersU3Eb__24_9_m1981826299 (U3CU3Ec_t1591091833 * __this, Il2CppObject * ___obj0, JsonWriter_t3014444111 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseExporters>b__24_10(System.Object,ThirdParty.Json.LitJson.JsonWriter)
extern "C"  void U3CU3Ec_U3CRegisterBaseExportersU3Eb__24_10_m3919046323 (U3CU3Ec_t1591091833 * __this, Il2CppObject * ___obj0, JsonWriter_t3014444111 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseImporters>b__25_0(System.Object)
extern "C"  Il2CppObject * U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_0_m2312218722 (U3CU3Ec_t1591091833 * __this, Il2CppObject * ___input0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseImporters>b__25_1(System.Object)
extern "C"  Il2CppObject * U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_1_m542108775 (U3CU3Ec_t1591091833 * __this, Il2CppObject * ___input0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseImporters>b__25_2(System.Object)
extern "C"  Il2CppObject * U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_2_m2023383788 (U3CU3Ec_t1591091833 * __this, Il2CppObject * ___input0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseImporters>b__25_3(System.Object)
extern "C"  Il2CppObject * U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_3_m253273841 (U3CU3Ec_t1591091833 * __this, Il2CppObject * ___input0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseImporters>b__25_4(System.Object)
extern "C"  Il2CppObject * U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_4_m785232718 (U3CU3Ec_t1591091833 * __this, Il2CppObject * ___input0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseImporters>b__25_5(System.Object)
extern "C"  Il2CppObject * U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_5_m3310090067 (U3CU3Ec_t1591091833 * __this, Il2CppObject * ___input0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseImporters>b__25_6(System.Object)
extern "C"  Il2CppObject * U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_6_m496397784 (U3CU3Ec_t1591091833 * __this, Il2CppObject * ___input0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseImporters>b__25_7(System.Object)
extern "C"  Il2CppObject * U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_7_m3021255133 (U3CU3Ec_t1591091833 * __this, Il2CppObject * ___input0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseImporters>b__25_8(System.Object)
extern "C"  Il2CppObject * U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_8_m3565818426 (U3CU3Ec_t1591091833 * __this, Il2CppObject * ___input0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseImporters>b__25_9(System.Object)
extern "C"  Il2CppObject * U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_9_m1795708479 (U3CU3Ec_t1591091833 * __this, Il2CppObject * ___input0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseImporters>b__25_10(System.Object)
extern "C"  Il2CppObject * U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_10_m1231844419 (U3CU3Ec_t1591091833 * __this, Il2CppObject * ___input0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseImporters>b__25_11(System.Object)
extern "C"  Il2CppObject * U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_11_m1950251426 (U3CU3Ec_t1591091833 * __this, Il2CppObject * ___input0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseImporters>b__25_12(System.Object)
extern "C"  Il2CppObject * U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_12_m2105962245 (U3CU3Ec_t1591091833 * __this, Il2CppObject * ___input0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseImporters>b__25_13(System.Object)
extern "C"  Il2CppObject * U3CU3Ec_U3CRegisterBaseImportersU3Eb__25_13_m2617176164 (U3CU3Ec_t1591091833 * __this, Il2CppObject * ___input0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ThirdParty.Json.LitJson.IJsonWrapper ThirdParty.Json.LitJson.JsonMapper/<>c::<ToObject>b__31_0()
extern "C"  Il2CppObject * U3CU3Ec_U3CToObjectU3Eb__31_0_m3161780688 (U3CU3Ec_t1591091833 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ThirdParty.Json.LitJson.IJsonWrapper ThirdParty.Json.LitJson.JsonMapper/<>c::<ToObject>b__32_0()
extern "C"  Il2CppObject * U3CU3Ec_U3CToObjectU3Eb__32_0_m464330091 (U3CU3Ec_t1591091833 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
