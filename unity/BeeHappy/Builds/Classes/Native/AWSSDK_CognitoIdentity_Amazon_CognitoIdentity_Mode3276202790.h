﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Amazo158465174.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.CognitoIdentity.Model.TooManyRequestsException
struct  TooManyRequestsException_t3276202790  : public AmazonCognitoIdentityException_t158465174
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
