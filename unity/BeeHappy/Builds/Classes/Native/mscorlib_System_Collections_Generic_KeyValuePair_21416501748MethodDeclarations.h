﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21416501748.h"
#include "AWSSDK_Core_Amazon_Runtime_Metric3273440202.h"

// System.Void System.Collections.Generic.KeyValuePair`2<Amazon.Runtime.Metric,System.Int64>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m1467949693_gshared (KeyValuePair_2_t1416501748 * __this, int32_t ___key0, int64_t ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m1467949693(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t1416501748 *, int32_t, int64_t, const MethodInfo*))KeyValuePair_2__ctor_m1467949693_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<Amazon.Runtime.Metric,System.Int64>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m404179876_gshared (KeyValuePair_2_t1416501748 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m404179876(__this, method) ((  int32_t (*) (KeyValuePair_2_t1416501748 *, const MethodInfo*))KeyValuePair_2_get_Key_m404179876_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Amazon.Runtime.Metric,System.Int64>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m4153237236_gshared (KeyValuePair_2_t1416501748 * __this, int32_t ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m4153237236(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1416501748 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m4153237236_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<Amazon.Runtime.Metric,System.Int64>::get_Value()
extern "C"  int64_t KeyValuePair_2_get_Value_m3929255215_gshared (KeyValuePair_2_t1416501748 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m3929255215(__this, method) ((  int64_t (*) (KeyValuePair_2_t1416501748 *, const MethodInfo*))KeyValuePair_2_get_Value_m3929255215_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Amazon.Runtime.Metric,System.Int64>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1164567988_gshared (KeyValuePair_2_t1416501748 * __this, int64_t ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m1164567988(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1416501748 *, int64_t, const MethodInfo*))KeyValuePair_2_set_Value_m1164567988_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<Amazon.Runtime.Metric,System.Int64>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m2510578186_gshared (KeyValuePair_2_t1416501748 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m2510578186(__this, method) ((  String_t* (*) (KeyValuePair_2_t1416501748 *, const MethodInfo*))KeyValuePair_2_ToString_m2510578186_gshared)(__this, method)
