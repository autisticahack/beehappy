﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Exception
struct Exception_t1927440687;
// Amazon.CognitoSync.SyncManager.Dataset
struct Dataset_t3040902086;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.CognitoSync.SyncManager.Dataset/<>c__DisplayClass73_0
struct  U3CU3Ec__DisplayClass73_0_t2111962513  : public Il2CppObject
{
public:
	// System.Exception Amazon.CognitoSync.SyncManager.Dataset/<>c__DisplayClass73_0::exception
	Exception_t1927440687 * ___exception_0;
	// Amazon.CognitoSync.SyncManager.Dataset Amazon.CognitoSync.SyncManager.Dataset/<>c__DisplayClass73_0::<>4__this
	Dataset_t3040902086 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_exception_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass73_0_t2111962513, ___exception_0)); }
	inline Exception_t1927440687 * get_exception_0() const { return ___exception_0; }
	inline Exception_t1927440687 ** get_address_of_exception_0() { return &___exception_0; }
	inline void set_exception_0(Exception_t1927440687 * value)
	{
		___exception_0 = value;
		Il2CppCodeGenWriteBarrier(&___exception_0, value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass73_0_t2111962513, ___U3CU3E4__this_1)); }
	inline Dataset_t3040902086 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline Dataset_t3040902086 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(Dataset_t3040902086 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E4__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
