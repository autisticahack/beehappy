﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.Util.InternalLog4netLogger
struct InternalLog4netLogger_t1777422022;
// System.Type
struct Type_t;
// System.Exception
struct Exception_t1927440687;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t3614634134;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_Exception1927440687.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Amazon.Runtime.Internal.Util.InternalLog4netLogger::loadStatics()
extern "C"  void InternalLog4netLogger_loadStatics_m343529819 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.InternalLog4netLogger::.ctor(System.Type)
extern "C"  void InternalLog4netLogger__ctor_m1192563377 (InternalLog4netLogger_t1777422022 * __this, Type_t * ___declaringType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.Util.InternalLog4netLogger::get_IsErrorEnabled()
extern "C"  bool InternalLog4netLogger_get_IsErrorEnabled_m767541506 (InternalLog4netLogger_t1777422022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.InternalLog4netLogger::Error(System.Exception,System.String,System.Object[])
extern "C"  void InternalLog4netLogger_Error_m2070842266 (InternalLog4netLogger_t1777422022 * __this, Exception_t1927440687 * ___exception0, String_t* ___messageFormat1, ObjectU5BU5D_t3614634134* ___args2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.Util.InternalLog4netLogger::get_IsDebugEnabled()
extern "C"  bool InternalLog4netLogger_get_IsDebugEnabled_m2466321449 (InternalLog4netLogger_t1777422022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.InternalLog4netLogger::Debug(System.Exception,System.String,System.Object[])
extern "C"  void InternalLog4netLogger_Debug_m2729177445 (InternalLog4netLogger_t1777422022 * __this, Exception_t1927440687 * ___exception0, String_t* ___messageFormat1, ObjectU5BU5D_t3614634134* ___args2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.InternalLog4netLogger::DebugFormat(System.String,System.Object[])
extern "C"  void InternalLog4netLogger_DebugFormat_m107110440 (InternalLog4netLogger_t1777422022 * __this, String_t* ___message0, ObjectU5BU5D_t3614634134* ___arguments1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.Util.InternalLog4netLogger::get_IsInfoEnabled()
extern "C"  bool InternalLog4netLogger_get_IsInfoEnabled_m447138120 (InternalLog4netLogger_t1777422022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.InternalLog4netLogger::InfoFormat(System.String,System.Object[])
extern "C"  void InternalLog4netLogger_InfoFormat_m3657869445 (InternalLog4netLogger_t1777422022 * __this, String_t* ___message0, ObjectU5BU5D_t3614634134* ___arguments1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.InternalLog4netLogger::.cctor()
extern "C"  void InternalLog4netLogger__cctor_m951288085 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
