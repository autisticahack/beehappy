﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Data.Common.DbDataRecordImpl
struct DbDataRecordImpl_t393023775;
// System.Data.Common.SchemaInfo[]
struct SchemaInfoU5BU5D_t3984074354;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Data.Common.DbDataRecordImpl::.ctor(System.Data.Common.SchemaInfo[],System.Object[])
extern "C"  void DbDataRecordImpl__ctor_m3862213960 (DbDataRecordImpl_t393023775 * __this, SchemaInfoU5BU5D_t3984074354* ___schema0, ObjectU5BU5D_t3614634134* ___values1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Data.Common.DbDataRecordImpl::get_FieldCount()
extern "C"  int32_t DbDataRecordImpl_get_FieldCount_m1811398251 (DbDataRecordImpl_t393023775 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Data.Common.DbDataRecordImpl::GetDataTypeName(System.Int32)
extern "C"  String_t* DbDataRecordImpl_GetDataTypeName_m1204659322 (DbDataRecordImpl_t393023775 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Data.Common.DbDataRecordImpl::GetFieldType(System.Int32)
extern "C"  Type_t * DbDataRecordImpl_GetFieldType_m1271668414 (DbDataRecordImpl_t393023775 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Data.Common.DbDataRecordImpl::GetName(System.Int32)
extern "C"  String_t* DbDataRecordImpl_GetName_m833554930 (DbDataRecordImpl_t393023775 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Data.Common.DbDataRecordImpl::GetValue(System.Int32)
extern "C"  Il2CppObject * DbDataRecordImpl_GetValue_m33003152 (DbDataRecordImpl_t393023775 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Data.Common.DbDataRecordImpl::GetValues(System.Object[])
extern "C"  int32_t DbDataRecordImpl_GetValues_m4233779265 (DbDataRecordImpl_t393023775 * __this, ObjectU5BU5D_t3614634134* ___values0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Data.Common.DbDataRecordImpl::IsDBNull(System.Int32)
extern "C"  bool DbDataRecordImpl_IsDBNull_m529039411 (DbDataRecordImpl_t393023775 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
