﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.UnityInitializer
struct UnityInitializer_t2778189483;
// System.Object
struct Il2CppObject;
// System.Threading.Thread
struct Thread_t241561612;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.UnityInitializer
struct  UnityInitializer_t2778189483  : public MonoBehaviour_t1158329972
{
public:

public:
};

struct UnityInitializer_t2778189483_StaticFields
{
public:
	// Amazon.UnityInitializer Amazon.UnityInitializer::_instance
	UnityInitializer_t2778189483 * ____instance_2;
	// System.Object Amazon.UnityInitializer::_lock
	Il2CppObject * ____lock_3;
	// System.Threading.Thread Amazon.UnityInitializer::_mainThread
	Thread_t241561612 * ____mainThread_4;

public:
	inline static int32_t get_offset_of__instance_2() { return static_cast<int32_t>(offsetof(UnityInitializer_t2778189483_StaticFields, ____instance_2)); }
	inline UnityInitializer_t2778189483 * get__instance_2() const { return ____instance_2; }
	inline UnityInitializer_t2778189483 ** get_address_of__instance_2() { return &____instance_2; }
	inline void set__instance_2(UnityInitializer_t2778189483 * value)
	{
		____instance_2 = value;
		Il2CppCodeGenWriteBarrier(&____instance_2, value);
	}

	inline static int32_t get_offset_of__lock_3() { return static_cast<int32_t>(offsetof(UnityInitializer_t2778189483_StaticFields, ____lock_3)); }
	inline Il2CppObject * get__lock_3() const { return ____lock_3; }
	inline Il2CppObject ** get_address_of__lock_3() { return &____lock_3; }
	inline void set__lock_3(Il2CppObject * value)
	{
		____lock_3 = value;
		Il2CppCodeGenWriteBarrier(&____lock_3, value);
	}

	inline static int32_t get_offset_of__mainThread_4() { return static_cast<int32_t>(offsetof(UnityInitializer_t2778189483_StaticFields, ____mainThread_4)); }
	inline Thread_t241561612 * get__mainThread_4() const { return ____mainThread_4; }
	inline Thread_t241561612 ** get_address_of__mainThread_4() { return &____mainThread_4; }
	inline void set__mainThread_4(Thread_t241561612 * value)
	{
		____mainThread_4 = value;
		Il2CppCodeGenWriteBarrier(&____mainThread_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
