﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.CognitoSync.Model.Dataset
struct Dataset_t149289424;

#include "AWSSDK_Core_Amazon_Runtime_AmazonWebServiceResponse529043356.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.CognitoSync.Model.DeleteDatasetResponse
struct  DeleteDatasetResponse_t2927760190  : public AmazonWebServiceResponse_t529043356
{
public:
	// Amazon.CognitoSync.Model.Dataset Amazon.CognitoSync.Model.DeleteDatasetResponse::_dataset
	Dataset_t149289424 * ____dataset_3;

public:
	inline static int32_t get_offset_of__dataset_3() { return static_cast<int32_t>(offsetof(DeleteDatasetResponse_t2927760190, ____dataset_3)); }
	inline Dataset_t149289424 * get__dataset_3() const { return ____dataset_3; }
	inline Dataset_t149289424 ** get_address_of__dataset_3() { return &____dataset_3; }
	inline void set__dataset_3(Dataset_t149289424 * value)
	{
		____dataset_3 = value;
		Il2CppCodeGenWriteBarrier(&____dataset_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
