﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Int64>
struct Dictionary_2_t3659156526;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1050721994.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Amazon.Runtime.Metric,System.Int64>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m236173709_gshared (Enumerator_t1050721994 * __this, Dictionary_2_t3659156526 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m236173709(__this, ___host0, method) ((  void (*) (Enumerator_t1050721994 *, Dictionary_2_t3659156526 *, const MethodInfo*))Enumerator__ctor_m236173709_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Amazon.Runtime.Metric,System.Int64>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m162752738_gshared (Enumerator_t1050721994 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m162752738(__this, method) ((  Il2CppObject * (*) (Enumerator_t1050721994 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m162752738_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Amazon.Runtime.Metric,System.Int64>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1851689148_gshared (Enumerator_t1050721994 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1851689148(__this, method) ((  void (*) (Enumerator_t1050721994 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1851689148_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Amazon.Runtime.Metric,System.Int64>::Dispose()
extern "C"  void Enumerator_Dispose_m1587742133_gshared (Enumerator_t1050721994 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1587742133(__this, method) ((  void (*) (Enumerator_t1050721994 *, const MethodInfo*))Enumerator_Dispose_m1587742133_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Amazon.Runtime.Metric,System.Int64>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3485337056_gshared (Enumerator_t1050721994 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3485337056(__this, method) ((  bool (*) (Enumerator_t1050721994 *, const MethodInfo*))Enumerator_MoveNext_m3485337056_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Amazon.Runtime.Metric,System.Int64>::get_Current()
extern "C"  int64_t Enumerator_get_Current_m1859672514_gshared (Enumerator_t1050721994 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1859672514(__this, method) ((  int64_t (*) (Enumerator_t1050721994 *, const MethodInfo*))Enumerator_get_Current_m1859672514_gshared)(__this, method)
