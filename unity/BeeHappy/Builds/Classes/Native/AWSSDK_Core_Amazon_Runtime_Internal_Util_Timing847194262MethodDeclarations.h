﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.Util.Timing
struct Timing_t847194262;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_TimeSpan3430258949.h"

// System.Void Amazon.Runtime.Internal.Util.Timing::.ctor()
extern "C"  void Timing__ctor_m3782059410 (Timing_t847194262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.Timing::.ctor(System.Int64)
extern "C"  void Timing__ctor_m2620552042 (Timing_t847194262 * __this, int64_t ___currentTime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.Timing::Stop(System.Int64)
extern "C"  void Timing_Stop_m3998756636 (Timing_t847194262 * __this, int64_t ___currentTime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.Util.Timing::get_IsFinished()
extern "C"  bool Timing_get_IsFinished_m868700467 (Timing_t847194262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.Timing::set_IsFinished(System.Boolean)
extern "C"  void Timing_set_IsFinished_m1858684262 (Timing_t847194262 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Amazon.Runtime.Internal.Util.Timing::get_ElapsedTicks()
extern "C"  int64_t Timing_get_ElapsedTicks_m3418763726 (Timing_t847194262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.TimeSpan Amazon.Runtime.Internal.Util.Timing::get_ElapsedTime()
extern "C"  TimeSpan_t3430258949  Timing_get_ElapsedTime_m2524397493 (Timing_t847194262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
