﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Internal.RegionEndpointProviderV3
struct RegionEndpointProviderV3_t342927067;
// Amazon.Internal.IRegionEndpoint
struct IRegionEndpoint_t544433888;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Amazon.Internal.RegionEndpointProviderV3::.ctor()
extern "C"  void RegionEndpointProviderV3__ctor_m4234201321 (RegionEndpointProviderV3_t342927067 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Internal.IRegionEndpoint Amazon.Internal.RegionEndpointProviderV3::GetRegionEndpoint(System.String)
extern "C"  Il2CppObject * RegionEndpointProviderV3_GetRegionEndpoint_m1553823428 (RegionEndpointProviderV3_t342927067 * __this, String_t* ___regionName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Internal.IRegionEndpoint Amazon.Internal.RegionEndpointProviderV3::GetUnknownRegionEndpoint(System.String)
extern "C"  Il2CppObject * RegionEndpointProviderV3_GetUnknownRegionEndpoint_m60231838 (RegionEndpointProviderV3_t342927067 * __this, String_t* ___regionName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Internal.RegionEndpointProviderV3::.cctor()
extern "C"  void RegionEndpointProviderV3__cctor_m909048390 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
