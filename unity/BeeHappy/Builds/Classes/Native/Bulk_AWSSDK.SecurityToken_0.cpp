﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// Amazon.SecurityToken.AmazonSecurityTokenServiceClient
struct AmazonSecurityTokenServiceClient_t1241355389;
// Amazon.Runtime.AWSCredentials
struct AWSCredentials_t3583921007;
// Amazon.RegionEndpoint
struct RegionEndpoint_t661522805;
// Amazon.SecurityToken.AmazonSecurityTokenServiceConfig
struct AmazonSecurityTokenServiceConfig_t1261559370;
// Amazon.Runtime.Internal.Auth.AbstractAWSSigner
struct AbstractAWSSigner_t2114314031;
// Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest
struct AssumeRoleWithWebIdentityRequest_t193094215;
// Amazon.Runtime.AmazonServiceCallback`2<Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest,Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityResponse>
struct AmazonServiceCallback_2_t142122009;
// Amazon.Runtime.AsyncOptions
struct AsyncOptions_t558351272;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// Amazon.Runtime.Internal.Transform.IMarshaller`2<Amazon.Runtime.Internal.IRequest,Amazon.Runtime.AmazonWebServiceRequest>
struct IMarshaller_2_t3817076711;
// Amazon.Runtime.Internal.Transform.ResponseUnmarshaller
struct ResponseUnmarshaller_t3934041557;
// System.Action`4<Amazon.Runtime.AmazonWebServiceRequest,Amazon.Runtime.AmazonWebServiceResponse,System.Exception,Amazon.Runtime.AsyncOptions>
struct Action_4_t897279368;
// System.Object
struct Il2CppObject;
// Amazon.SecurityToken.AmazonSecurityTokenServiceClient/<>c__DisplayClass16_0
struct U3CU3Ec__DisplayClass16_0_t2700714712;
// Amazon.Runtime.AmazonWebServiceRequest
struct AmazonWebServiceRequest_t3384026212;
// Amazon.Runtime.AmazonWebServiceResponse
struct AmazonWebServiceResponse_t529043356;
// System.Exception
struct Exception_t1927440687;
// System.String
struct String_t;
// Amazon.SecurityToken.AmazonSecurityTokenServiceException
struct AmazonSecurityTokenServiceException_t3771342025;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// Amazon.SecurityToken.AmazonSecurityTokenServiceRequest
struct AmazonSecurityTokenServiceRequest_t265680841;
// Amazon.SecurityToken.Model.AssumedRoleUser
struct AssumedRoleUser_t150458319;
// Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityResponse
struct AssumeRoleWithWebIdentityResponse_t3931705881;
// Amazon.SecurityToken.Model.Credentials
struct Credentials_t3554032640;
// Amazon.Runtime.ImmutableCredentials
struct ImmutableCredentials_t282353664;
// Amazon.SecurityToken.Model.ExpiredTokenException
struct ExpiredTokenException_t1058704965;
// Amazon.SecurityToken.Model.IDPCommunicationErrorException
struct IDPCommunicationErrorException_t2234231496;
// Amazon.SecurityToken.Model.IDPRejectedClaimException
struct IDPRejectedClaimException_t367458406;
// Amazon.SecurityToken.Model.Internal.MarshallTransformations.AssumedRoleUserUnmarshaller
struct AssumedRoleUserUnmarshaller_t2473561683;
// Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext
struct XmlUnmarshallerContext_t1179575220;
// Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext
struct JsonUnmarshallerContext_t456235889;
// Amazon.SecurityToken.Model.Internal.MarshallTransformations.AssumeRoleWithWebIdentityRequestMarshaller
struct AssumeRoleWithWebIdentityRequestMarshaller_t1815493036;
// Amazon.Runtime.Internal.IRequest
struct IRequest_t2400804350;
// Amazon.SecurityToken.Model.Internal.MarshallTransformations.AssumeRoleWithWebIdentityResponseUnmarshaller
struct AssumeRoleWithWebIdentityResponseUnmarshaller_t3698885375;
// Amazon.Runtime.AmazonServiceException
struct AmazonServiceException_t3748559634;
// Amazon.SecurityToken.Model.Internal.MarshallTransformations.CredentialsUnmarshaller
struct CredentialsUnmarshaller_t3599710454;
// Amazon.SecurityToken.Model.InvalidIdentityTokenException
struct InvalidIdentityTokenException_t1321611201;
// Amazon.SecurityToken.Model.MalformedPolicyDocumentException
struct MalformedPolicyDocumentException_t157840309;
// Amazon.SecurityToken.Model.PackedPolicyTooLargeException
struct PackedPolicyTooLargeException_t4099730702;
// Amazon.SecurityToken.Model.RegionDisabledException
struct RegionDisabledException_t1422077785;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AWSSDK_SecurityToken_U3CModuleU3E3783534214.h"
#include "AWSSDK_SecurityToken_U3CModuleU3E3783534214MethodDeclarations.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_AmazonSe1241355389.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_AmazonSe1241355389MethodDeclarations.h"
#include "AWSSDK_Core_Amazon_Runtime_AWSCredentials3583921007.h"
#include "AWSSDK_Core_Amazon_RegionEndpoint661522805.h"
#include "mscorlib_System_Void1841601450.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_AmazonSe1261559370MethodDeclarations.h"
#include "AWSSDK_Core_Amazon_Runtime_ClientConfig3664713661MethodDeclarations.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_AmazonSe1261559370.h"
#include "AWSSDK_Core_Amazon_Runtime_AmazonServiceClient3583134838MethodDeclarations.h"
#include "AWSSDK_Core_Amazon_Runtime_ClientConfig3664713661.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Auth_AbstractA2114314031.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Auth_AWS4Signe1011449585MethodDeclarations.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Auth_AWS4Signe1011449585.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "AWSSDK_Core_Amazon_Runtime_AmazonServiceClient3583134838.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_Model_Ass193094215.h"
#include "AWSSDK_Core_Amazon_Runtime_AsyncOptions558351272.h"
#include "AWSSDK_Core_Amazon_Runtime_AmazonServiceCallback_2_142122009.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_AmazonSe2700714712MethodDeclarations.h"
#include "AWSSDK_Core_Amazon_Runtime_AsyncOptions558351272MethodDeclarations.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_Model_In1815493036MethodDeclarations.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_Model_In3698885375MethodDeclarations.h"
#include "System_Core_System_Action_4_gen897279368MethodDeclarations.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_AmazonSe2700714712.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_Model_In1815493036.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_Model_In3698885375.h"
#include "System_Core_System_Action_4_gen897279368.h"
#include "AWSSDK_Core_Amazon_Runtime_AmazonWebServiceRequest3384026212.h"
#include "AWSSDK_Core_Amazon_Runtime_AmazonWebServiceResponse529043356.h"
#include "mscorlib_System_Exception1927440687.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_Resp3934041557.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "AWSSDK_Core_Amazon_Runtime_AmazonServiceResult_2_g3957156559MethodDeclarations.h"
#include "AWSSDK_Core_Amazon_Runtime_AmazonServiceCallback_2_142122009MethodDeclarations.h"
#include "AWSSDK_Core_Amazon_Runtime_AmazonServiceResult_2_g3957156559.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_Model_As3931705881.h"
#include "AWSSDK_Core_Amazon_Runtime_FallbackRegionFactory860882262MethodDeclarations.h"
#include "mscorlib_System_String2029220233.h"
#include "AWSSDK_Core_Amazon_RegionEndpoint661522805MethodDeclarations.h"
#include "AWSSDK_Core_Amazon_Util_Internal_InternalSDKUtils3074264706MethodDeclarations.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_AmazonSe3771342025.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_AmazonSe3771342025MethodDeclarations.h"
#include "AWSSDK_Core_Amazon_Runtime_ErrorType1448377524.h"
#include "System_System_Net_HttpStatusCode1898409641.h"
#include "AWSSDK_Core_Amazon_Runtime_AmazonServiceException3748559634MethodDeclarations.h"
#include "mscorlib_System_Runtime_Serialization_Serialization228987430.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_AmazonSec265680841.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_AmazonSec265680841MethodDeclarations.h"
#include "AWSSDK_Core_Amazon_Runtime_AmazonWebServiceRequest3384026212MethodDeclarations.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_Model_Ass150458319.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_Model_Ass150458319MethodDeclarations.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_Model_Ass193094215MethodDeclarations.h"
#include "mscorlib_System_Int322071877448.h"
#include "mscorlib_System_Nullable_1_gen334943763.h"
#include "mscorlib_System_Nullable_1_gen334943763MethodDeclarations.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_Model_As3931705881MethodDeclarations.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_Model_Cr3554032640.h"
#include "AWSSDK_Core_Amazon_Runtime_AmazonWebServiceResponse529043356MethodDeclarations.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_Model_Cr3554032640MethodDeclarations.h"
#include "AWSSDK_Core_Amazon_Runtime_ImmutableCredentials282353664.h"
#include "AWSSDK_Core_Amazon_Runtime_ImmutableCredentials282353664MethodDeclarations.h"
#include "AWSSDK_Core_Amazon_Runtime_AWSCredentials3583921007MethodDeclarations.h"
#include "mscorlib_System_DateTime693205669.h"
#include "mscorlib_System_Nullable_1_gen3251239280.h"
#include "mscorlib_System_Nullable_1_gen3251239280MethodDeclarations.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_Model_Ex1058704965.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_Model_Ex1058704965MethodDeclarations.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_Model_ID2234231496.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_Model_ID2234231496MethodDeclarations.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_Model_IDP367458406.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_Model_IDP367458406MethodDeclarations.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_Model_In2473561683.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_Model_In2473561683MethodDeclarations.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_XmlU1179575220.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_XmlU1179575220MethodDeclarations.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_Unma1608322995MethodDeclarations.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_Stri3953260147MethodDeclarations.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_Stri3953260147.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_Unma1608322995.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_JsonU456235889.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_DefaultRequest3080757440MethodDeclarations.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Util_StringUti1089313136MethodDeclarations.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_DefaultRequest3080757440.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_Resp1207185784MethodDeclarations.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_Resp1207185784.h"
#include "AWSSDK_Core_Amazon_Runtime_ResponseMetadata527027456.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_Model_In3599710454MethodDeclarations.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_IntU2174001701MethodDeclarations.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_Model_In3599710454.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_IntU2174001701.h"
#include "AWSSDK_Core_Amazon_Runtime_AmazonServiceException3748559634.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_Erro3810099389MethodDeclarations.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_ErrorResponse3502566035MethodDeclarations.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_Model_In1321611201MethodDeclarations.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_Model_Mal157840309MethodDeclarations.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_Model_Pa4099730702MethodDeclarations.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_Model_Re1422077785MethodDeclarations.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_ErrorResponse3502566035.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_Erro3810099389.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_Model_In1321611201.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_Model_Mal157840309.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_Model_Pa4099730702.h"
#include "AWSSDK_SecurityToken_Amazon_SecurityToken_Model_Re1422077785.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_XmlRe760346204MethodDeclarations.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_Date1723598451MethodDeclarations.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_Date1723598451.h"

// System.IAsyncResult Amazon.Runtime.AmazonServiceClient::BeginInvoke<System.Object>(!!0,Amazon.Runtime.Internal.Transform.IMarshaller`2<Amazon.Runtime.Internal.IRequest,Amazon.Runtime.AmazonWebServiceRequest>,Amazon.Runtime.Internal.Transform.ResponseUnmarshaller,Amazon.Runtime.AsyncOptions,System.Action`4<Amazon.Runtime.AmazonWebServiceRequest,Amazon.Runtime.AmazonWebServiceResponse,System.Exception,Amazon.Runtime.AsyncOptions>)
extern "C"  Il2CppObject * AmazonServiceClient_BeginInvoke_TisIl2CppObject_m1051908351_gshared (AmazonServiceClient_t3583134838 * __this, Il2CppObject * p0, Il2CppObject* p1, ResponseUnmarshaller_t3934041557 * p2, AsyncOptions_t558351272 * p3, Action_4_t897279368 * p4, const MethodInfo* method);
#define AmazonServiceClient_BeginInvoke_TisIl2CppObject_m1051908351(__this, p0, p1, p2, p3, p4, method) ((  Il2CppObject * (*) (AmazonServiceClient_t3583134838 *, Il2CppObject *, Il2CppObject*, ResponseUnmarshaller_t3934041557 *, AsyncOptions_t558351272 *, Action_4_t897279368 *, const MethodInfo*))AmazonServiceClient_BeginInvoke_TisIl2CppObject_m1051908351_gshared)(__this, p0, p1, p2, p3, p4, method)
// System.IAsyncResult Amazon.Runtime.AmazonServiceClient::BeginInvoke<Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest>(!!0,Amazon.Runtime.Internal.Transform.IMarshaller`2<Amazon.Runtime.Internal.IRequest,Amazon.Runtime.AmazonWebServiceRequest>,Amazon.Runtime.Internal.Transform.ResponseUnmarshaller,Amazon.Runtime.AsyncOptions,System.Action`4<Amazon.Runtime.AmazonWebServiceRequest,Amazon.Runtime.AmazonWebServiceResponse,System.Exception,Amazon.Runtime.AsyncOptions>)
#define AmazonServiceClient_BeginInvoke_TisAssumeRoleWithWebIdentityRequest_t193094215_m629721998(__this, p0, p1, p2, p3, p4, method) ((  Il2CppObject * (*) (AmazonServiceClient_t3583134838 *, AssumeRoleWithWebIdentityRequest_t193094215 *, Il2CppObject*, ResponseUnmarshaller_t3934041557 *, AsyncOptions_t558351272 *, Action_4_t897279368 *, const MethodInfo*))AmazonServiceClient_BeginInvoke_TisIl2CppObject_m1051908351_gshared)(__this, p0, p1, p2, p3, p4, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Amazon.SecurityToken.AmazonSecurityTokenServiceClient::.ctor(Amazon.Runtime.AWSCredentials,Amazon.RegionEndpoint)
extern Il2CppClass* AmazonSecurityTokenServiceConfig_t1261559370_il2cpp_TypeInfo_var;
extern const uint32_t AmazonSecurityTokenServiceClient__ctor_m77079877_MetadataUsageId;
extern "C"  void AmazonSecurityTokenServiceClient__ctor_m77079877 (AmazonSecurityTokenServiceClient_t1241355389 * __this, AWSCredentials_t3583921007 * ___credentials0, RegionEndpoint_t661522805 * ___region1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AmazonSecurityTokenServiceClient__ctor_m77079877_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AWSCredentials_t3583921007 * L_0 = ___credentials0;
		AmazonSecurityTokenServiceConfig_t1261559370 * L_1 = (AmazonSecurityTokenServiceConfig_t1261559370 *)il2cpp_codegen_object_new(AmazonSecurityTokenServiceConfig_t1261559370_il2cpp_TypeInfo_var);
		AmazonSecurityTokenServiceConfig__ctor_m2978057874(L_1, /*hidden argument*/NULL);
		AmazonSecurityTokenServiceConfig_t1261559370 * L_2 = L_1;
		RegionEndpoint_t661522805 * L_3 = ___region1;
		NullCheck(L_2);
		ClientConfig_set_RegionEndpoint_m2421036023(L_2, L_3, /*hidden argument*/NULL);
		AmazonSecurityTokenServiceClient__ctor_m3543846829(__this, L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Amazon.SecurityToken.AmazonSecurityTokenServiceClient::.ctor(Amazon.Runtime.AWSCredentials,Amazon.SecurityToken.AmazonSecurityTokenServiceConfig)
extern "C"  void AmazonSecurityTokenServiceClient__ctor_m3543846829 (AmazonSecurityTokenServiceClient_t1241355389 * __this, AWSCredentials_t3583921007 * ___credentials0, AmazonSecurityTokenServiceConfig_t1261559370 * ___clientConfig1, const MethodInfo* method)
{
	{
		AWSCredentials_t3583921007 * L_0 = ___credentials0;
		AmazonSecurityTokenServiceConfig_t1261559370 * L_1 = ___clientConfig1;
		AmazonServiceClient__ctor_m2824112809(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// Amazon.Runtime.Internal.Auth.AbstractAWSSigner Amazon.SecurityToken.AmazonSecurityTokenServiceClient::CreateSigner()
extern Il2CppClass* AWS4Signer_t1011449585_il2cpp_TypeInfo_var;
extern const uint32_t AmazonSecurityTokenServiceClient_CreateSigner_m2338458870_MetadataUsageId;
extern "C"  AbstractAWSSigner_t2114314031 * AmazonSecurityTokenServiceClient_CreateSigner_m2338458870 (AmazonSecurityTokenServiceClient_t1241355389 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AmazonSecurityTokenServiceClient_CreateSigner_m2338458870_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AWS4Signer_t1011449585 * L_0 = (AWS4Signer_t1011449585 *)il2cpp_codegen_object_new(AWS4Signer_t1011449585_il2cpp_TypeInfo_var);
		AWS4Signer__ctor_m1541432815(L_0, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void Amazon.SecurityToken.AmazonSecurityTokenServiceClient::Dispose(System.Boolean)
extern "C"  void AmazonSecurityTokenServiceClient_Dispose_m824202305 (AmazonSecurityTokenServiceClient_t1241355389 * __this, bool ___disposing0, const MethodInfo* method)
{
	{
		bool L_0 = ___disposing0;
		AmazonServiceClient_Dispose_m3444168299(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Amazon.SecurityToken.AmazonSecurityTokenServiceClient::AssumeRoleWithWebIdentityAsync(Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest,Amazon.Runtime.AmazonServiceCallback`2<Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest,Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityResponse>,Amazon.Runtime.AsyncOptions)
extern Il2CppClass* U3CU3Ec__DisplayClass16_0_t2700714712_il2cpp_TypeInfo_var;
extern Il2CppClass* AsyncOptions_t558351272_il2cpp_TypeInfo_var;
extern Il2CppClass* AssumeRoleWithWebIdentityRequestMarshaller_t1815493036_il2cpp_TypeInfo_var;
extern Il2CppClass* AssumeRoleWithWebIdentityResponseUnmarshaller_t3698885375_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_4_t897279368_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass16_0_U3CAssumeRoleWithWebIdentityAsyncU3Eb__0_m832857059_MethodInfo_var;
extern const MethodInfo* Action_4__ctor_m1555803652_MethodInfo_var;
extern const MethodInfo* AmazonServiceClient_BeginInvoke_TisAssumeRoleWithWebIdentityRequest_t193094215_m629721998_MethodInfo_var;
extern const uint32_t AmazonSecurityTokenServiceClient_AssumeRoleWithWebIdentityAsync_m255795346_MetadataUsageId;
extern "C"  void AmazonSecurityTokenServiceClient_AssumeRoleWithWebIdentityAsync_m255795346 (AmazonSecurityTokenServiceClient_t1241355389 * __this, AssumeRoleWithWebIdentityRequest_t193094215 * ___request0, AmazonServiceCallback_2_t142122009 * ___callback1, AsyncOptions_t558351272 * ___options2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AmazonSecurityTokenServiceClient_AssumeRoleWithWebIdentityAsync_m255795346_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass16_0_t2700714712 * V_0 = NULL;
	AssumeRoleWithWebIdentityRequestMarshaller_t1815493036 * V_1 = NULL;
	AssumeRoleWithWebIdentityResponseUnmarshaller_t3698885375 * V_2 = NULL;
	Action_4_t897279368 * V_3 = NULL;
	AsyncOptions_t558351272 * G_B3_0 = NULL;
	{
		U3CU3Ec__DisplayClass16_0_t2700714712 * L_0 = (U3CU3Ec__DisplayClass16_0_t2700714712 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass16_0_t2700714712_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass16_0__ctor_m349328331(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass16_0_t2700714712 * L_1 = V_0;
		AmazonServiceCallback_2_t142122009 * L_2 = ___callback1;
		NullCheck(L_1);
		L_1->set_callback_0(L_2);
		AsyncOptions_t558351272 * L_3 = ___options2;
		if (!L_3)
		{
			goto IL_0013;
		}
	}
	{
		AsyncOptions_t558351272 * L_4 = ___options2;
		G_B3_0 = L_4;
		goto IL_0018;
	}

IL_0013:
	{
		AsyncOptions_t558351272 * L_5 = (AsyncOptions_t558351272 *)il2cpp_codegen_object_new(AsyncOptions_t558351272_il2cpp_TypeInfo_var);
		AsyncOptions__ctor_m2169674811(L_5, /*hidden argument*/NULL);
		G_B3_0 = L_5;
	}

IL_0018:
	{
		___options2 = G_B3_0;
		AssumeRoleWithWebIdentityRequestMarshaller_t1815493036 * L_6 = (AssumeRoleWithWebIdentityRequestMarshaller_t1815493036 *)il2cpp_codegen_object_new(AssumeRoleWithWebIdentityRequestMarshaller_t1815493036_il2cpp_TypeInfo_var);
		AssumeRoleWithWebIdentityRequestMarshaller__ctor_m3147474726(L_6, /*hidden argument*/NULL);
		V_1 = L_6;
		IL2CPP_RUNTIME_CLASS_INIT(AssumeRoleWithWebIdentityResponseUnmarshaller_t3698885375_il2cpp_TypeInfo_var);
		AssumeRoleWithWebIdentityResponseUnmarshaller_t3698885375 * L_7 = AssumeRoleWithWebIdentityResponseUnmarshaller_get_Instance_m3286108612(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = L_7;
		V_3 = (Action_4_t897279368 *)NULL;
		U3CU3Ec__DisplayClass16_0_t2700714712 * L_8 = V_0;
		NullCheck(L_8);
		AmazonServiceCallback_2_t142122009 * L_9 = L_8->get_callback_0();
		if (!L_9)
		{
			goto IL_003d;
		}
	}
	{
		U3CU3Ec__DisplayClass16_0_t2700714712 * L_10 = V_0;
		IntPtr_t L_11;
		L_11.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass16_0_U3CAssumeRoleWithWebIdentityAsyncU3Eb__0_m832857059_MethodInfo_var);
		Action_4_t897279368 * L_12 = (Action_4_t897279368 *)il2cpp_codegen_object_new(Action_4_t897279368_il2cpp_TypeInfo_var);
		Action_4__ctor_m1555803652(L_12, L_10, L_11, /*hidden argument*/Action_4__ctor_m1555803652_MethodInfo_var);
		V_3 = L_12;
	}

IL_003d:
	{
		AssumeRoleWithWebIdentityRequest_t193094215 * L_13 = ___request0;
		AssumeRoleWithWebIdentityRequestMarshaller_t1815493036 * L_14 = V_1;
		AssumeRoleWithWebIdentityResponseUnmarshaller_t3698885375 * L_15 = V_2;
		AsyncOptions_t558351272 * L_16 = ___options2;
		Action_4_t897279368 * L_17 = V_3;
		AmazonServiceClient_BeginInvoke_TisAssumeRoleWithWebIdentityRequest_t193094215_m629721998(__this, L_13, L_14, L_15, L_16, L_17, /*hidden argument*/AmazonServiceClient_BeginInvoke_TisAssumeRoleWithWebIdentityRequest_t193094215_m629721998_MethodInfo_var);
		return;
	}
}
// System.Void Amazon.SecurityToken.AmazonSecurityTokenServiceClient/<>c__DisplayClass16_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass16_0__ctor_m349328331 (U3CU3Ec__DisplayClass16_0_t2700714712 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Amazon.SecurityToken.AmazonSecurityTokenServiceClient/<>c__DisplayClass16_0::<AssumeRoleWithWebIdentityAsync>b__0(Amazon.Runtime.AmazonWebServiceRequest,Amazon.Runtime.AmazonWebServiceResponse,System.Exception,Amazon.Runtime.AsyncOptions)
extern Il2CppClass* AssumeRoleWithWebIdentityRequest_t193094215_il2cpp_TypeInfo_var;
extern Il2CppClass* AssumeRoleWithWebIdentityResponse_t3931705881_il2cpp_TypeInfo_var;
extern Il2CppClass* AmazonServiceResult_2_t3957156559_il2cpp_TypeInfo_var;
extern const MethodInfo* AmazonServiceResult_2__ctor_m1060740062_MethodInfo_var;
extern const MethodInfo* AmazonServiceCallback_2_Invoke_m1778014778_MethodInfo_var;
extern const uint32_t U3CU3Ec__DisplayClass16_0_U3CAssumeRoleWithWebIdentityAsyncU3Eb__0_m832857059_MetadataUsageId;
extern "C"  void U3CU3Ec__DisplayClass16_0_U3CAssumeRoleWithWebIdentityAsyncU3Eb__0_m832857059 (U3CU3Ec__DisplayClass16_0_t2700714712 * __this, AmazonWebServiceRequest_t3384026212 * ___req0, AmazonWebServiceResponse_t529043356 * ___res1, Exception_t1927440687 * ___ex2, AsyncOptions_t558351272 * ___ao3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__DisplayClass16_0_U3CAssumeRoleWithWebIdentityAsyncU3Eb__0_m832857059_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AmazonServiceResult_2_t3957156559 * V_0 = NULL;
	{
		AmazonWebServiceRequest_t3384026212 * L_0 = ___req0;
		AmazonWebServiceResponse_t529043356 * L_1 = ___res1;
		Exception_t1927440687 * L_2 = ___ex2;
		AsyncOptions_t558351272 * L_3 = ___ao3;
		NullCheck(L_3);
		Il2CppObject * L_4 = AsyncOptions_get_State_m534435792(L_3, /*hidden argument*/NULL);
		AmazonServiceResult_2_t3957156559 * L_5 = (AmazonServiceResult_2_t3957156559 *)il2cpp_codegen_object_new(AmazonServiceResult_2_t3957156559_il2cpp_TypeInfo_var);
		AmazonServiceResult_2__ctor_m1060740062(L_5, ((AssumeRoleWithWebIdentityRequest_t193094215 *)CastclassClass(L_0, AssumeRoleWithWebIdentityRequest_t193094215_il2cpp_TypeInfo_var)), ((AssumeRoleWithWebIdentityResponse_t3931705881 *)CastclassClass(L_1, AssumeRoleWithWebIdentityResponse_t3931705881_il2cpp_TypeInfo_var)), L_2, L_4, /*hidden argument*/AmazonServiceResult_2__ctor_m1060740062_MethodInfo_var);
		V_0 = L_5;
		AmazonServiceCallback_2_t142122009 * L_6 = __this->get_callback_0();
		AmazonServiceResult_2_t3957156559 * L_7 = V_0;
		NullCheck(L_6);
		AmazonServiceCallback_2_Invoke_m1778014778(L_6, L_7, /*hidden argument*/AmazonServiceCallback_2_Invoke_m1778014778_MethodInfo_var);
		return;
	}
}
// System.Void Amazon.SecurityToken.AmazonSecurityTokenServiceConfig::.ctor()
extern Il2CppClass* AmazonSecurityTokenServiceConfig_t1261559370_il2cpp_TypeInfo_var;
extern Il2CppClass* ClientConfig_t3664713661_il2cpp_TypeInfo_var;
extern Il2CppClass* FallbackRegionFactory_t860882262_il2cpp_TypeInfo_var;
extern Il2CppClass* RegionEndpoint_t661522805_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3875252058;
extern const uint32_t AmazonSecurityTokenServiceConfig__ctor_m2978057874_MetadataUsageId;
extern "C"  void AmazonSecurityTokenServiceConfig__ctor_m2978057874 (AmazonSecurityTokenServiceConfig_t1261559370 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AmazonSecurityTokenServiceConfig__ctor_m2978057874_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RegionEndpoint_t661522805 * V_0 = NULL;
	RegionEndpoint_t661522805 * G_B2_0 = NULL;
	AmazonSecurityTokenServiceConfig_t1261559370 * G_B2_1 = NULL;
	RegionEndpoint_t661522805 * G_B1_0 = NULL;
	AmazonSecurityTokenServiceConfig_t1261559370 * G_B1_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(AmazonSecurityTokenServiceConfig_t1261559370_il2cpp_TypeInfo_var);
		String_t* L_0 = ((AmazonSecurityTokenServiceConfig_t1261559370_StaticFields*)AmazonSecurityTokenServiceConfig_t1261559370_il2cpp_TypeInfo_var->static_fields)->get_UserAgentString_21();
		__this->set__userAgent_22(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(ClientConfig_t3664713661_il2cpp_TypeInfo_var);
		ClientConfig__ctor_m3613490344(__this, /*hidden argument*/NULL);
		ClientConfig_set_AuthenticationServiceName_m1205259675(__this, _stringLiteral3875252058, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FallbackRegionFactory_t860882262_il2cpp_TypeInfo_var);
		RegionEndpoint_t661522805 * L_1 = FallbackRegionFactory_GetRegionEndpoint_m3879892175(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		V_0 = L_1;
		RegionEndpoint_t661522805 * L_2 = V_0;
		RegionEndpoint_t661522805 * L_3 = L_2;
		G_B1_0 = L_3;
		G_B1_1 = __this;
		if (L_3)
		{
			G_B2_0 = L_3;
			G_B2_1 = __this;
			goto IL_002e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(RegionEndpoint_t661522805_il2cpp_TypeInfo_var);
		RegionEndpoint_t661522805 * L_4 = ((RegionEndpoint_t661522805_StaticFields*)RegionEndpoint_t661522805_il2cpp_TypeInfo_var->static_fields)->get_USEast1_1();
		G_B2_0 = L_4;
		G_B2_1 = G_B1_1;
	}

IL_002e:
	{
		NullCheck(G_B2_1);
		IL2CPP_RUNTIME_CLASS_INIT(ClientConfig_t3664713661_il2cpp_TypeInfo_var);
		ClientConfig_set_RegionEndpoint_m2421036023(G_B2_1, G_B2_0, /*hidden argument*/NULL);
		return;
	}
}
// System.String Amazon.SecurityToken.AmazonSecurityTokenServiceConfig::get_RegionEndpointServiceName()
extern Il2CppCodeGenString* _stringLiteral3875252058;
extern const uint32_t AmazonSecurityTokenServiceConfig_get_RegionEndpointServiceName_m2944299163_MetadataUsageId;
extern "C"  String_t* AmazonSecurityTokenServiceConfig_get_RegionEndpointServiceName_m2944299163 (AmazonSecurityTokenServiceConfig_t1261559370 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AmazonSecurityTokenServiceConfig_get_RegionEndpointServiceName_m2944299163_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		return _stringLiteral3875252058;
	}
}
// System.String Amazon.SecurityToken.AmazonSecurityTokenServiceConfig::get_UserAgent()
extern "C"  String_t* AmazonSecurityTokenServiceConfig_get_UserAgent_m3797224716 (AmazonSecurityTokenServiceConfig_t1261559370 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get__userAgent_22();
		return L_0;
	}
}
// System.Void Amazon.SecurityToken.AmazonSecurityTokenServiceConfig::.cctor()
extern Il2CppClass* InternalSDKUtils_t3074264706_il2cpp_TypeInfo_var;
extern Il2CppClass* AmazonSecurityTokenServiceConfig_t1261559370_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3983658366;
extern const uint32_t AmazonSecurityTokenServiceConfig__cctor_m1950723557_MetadataUsageId;
extern "C"  void AmazonSecurityTokenServiceConfig__cctor_m1950723557 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AmazonSecurityTokenServiceConfig__cctor_m1950723557_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InternalSDKUtils_t3074264706_il2cpp_TypeInfo_var);
		String_t* L_0 = InternalSDKUtils_BuildUserAgentString_m4237096378(NULL /*static, unused*/, _stringLiteral3983658366, /*hidden argument*/NULL);
		((AmazonSecurityTokenServiceConfig_t1261559370_StaticFields*)AmazonSecurityTokenServiceConfig_t1261559370_il2cpp_TypeInfo_var->static_fields)->set_UserAgentString_21(L_0);
		return;
	}
}
// System.Void Amazon.SecurityToken.AmazonSecurityTokenServiceException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
extern "C"  void AmazonSecurityTokenServiceException__ctor_m4274350954 (AmazonSecurityTokenServiceException_t3771342025 * __this, String_t* ___message0, Exception_t1927440687 * ___innerException1, int32_t ___errorType2, String_t* ___errorCode3, String_t* ___requestId4, int32_t ___statusCode5, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		Exception_t1927440687 * L_1 = ___innerException1;
		int32_t L_2 = ___errorType2;
		String_t* L_3 = ___errorCode3;
		String_t* L_4 = ___requestId4;
		int32_t L_5 = ___statusCode5;
		AmazonServiceException__ctor_m1032639064(__this, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Amazon.SecurityToken.AmazonSecurityTokenServiceException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void AmazonSecurityTokenServiceException__ctor_m2614436174 (AmazonSecurityTokenServiceException_t3771342025 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method)
{
	{
		SerializationInfo_t228987430 * L_0 = ___info0;
		StreamingContext_t1417235061  L_1 = ___context1;
		AmazonServiceException__ctor_m903377152(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Amazon.SecurityToken.AmazonSecurityTokenServiceRequest::.ctor()
extern "C"  void AmazonSecurityTokenServiceRequest__ctor_m2827735933 (AmazonSecurityTokenServiceRequest_t265680841 * __this, const MethodInfo* method)
{
	{
		AmazonWebServiceRequest__ctor_m2915505731(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Amazon.SecurityToken.Model.AssumedRoleUser::set_Arn(System.String)
extern "C"  void AssumedRoleUser_set_Arn_m1960284340 (AssumedRoleUser_t150458319 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set__arn_0(L_0);
		return;
	}
}
// System.Void Amazon.SecurityToken.Model.AssumedRoleUser::set_AssumedRoleId(System.String)
extern "C"  void AssumedRoleUser_set_AssumedRoleId_m2821507572 (AssumedRoleUser_t150458319 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set__assumedRoleId_1(L_0);
		return;
	}
}
// System.Void Amazon.SecurityToken.Model.AssumedRoleUser::.ctor()
extern "C"  void AssumedRoleUser__ctor_m2660329274 (AssumedRoleUser_t150458319 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest::get_DurationSeconds()
extern const MethodInfo* Nullable_1_GetValueOrDefault_m1304217444_MethodInfo_var;
extern const uint32_t AssumeRoleWithWebIdentityRequest_get_DurationSeconds_m179215680_MetadataUsageId;
extern "C"  int32_t AssumeRoleWithWebIdentityRequest_get_DurationSeconds_m179215680 (AssumeRoleWithWebIdentityRequest_t193094215 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AssumeRoleWithWebIdentityRequest_get_DurationSeconds_m179215680_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Nullable_1_t334943763 * L_0 = __this->get_address_of__durationSeconds_3();
		int32_t L_1 = Nullable_1_GetValueOrDefault_m1304217444(L_0, /*hidden argument*/Nullable_1_GetValueOrDefault_m1304217444_MethodInfo_var);
		return L_1;
	}
}
// System.Void Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest::set_DurationSeconds(System.Int32)
extern const MethodInfo* Nullable_1__ctor_m852490454_MethodInfo_var;
extern const uint32_t AssumeRoleWithWebIdentityRequest_set_DurationSeconds_m1975187011_MetadataUsageId;
extern "C"  void AssumeRoleWithWebIdentityRequest_set_DurationSeconds_m1975187011 (AssumeRoleWithWebIdentityRequest_t193094215 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AssumeRoleWithWebIdentityRequest_set_DurationSeconds_m1975187011_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___value0;
		Nullable_1_t334943763  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Nullable_1__ctor_m852490454(&L_1, L_0, /*hidden argument*/Nullable_1__ctor_m852490454_MethodInfo_var);
		__this->set__durationSeconds_3(L_1);
		return;
	}
}
// System.Boolean Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest::IsSetDurationSeconds()
extern const MethodInfo* Nullable_1_get_HasValue_m3631752075_MethodInfo_var;
extern const uint32_t AssumeRoleWithWebIdentityRequest_IsSetDurationSeconds_m4189349523_MetadataUsageId;
extern "C"  bool AssumeRoleWithWebIdentityRequest_IsSetDurationSeconds_m4189349523 (AssumeRoleWithWebIdentityRequest_t193094215 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AssumeRoleWithWebIdentityRequest_IsSetDurationSeconds_m4189349523_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Nullable_1_t334943763 * L_0 = __this->get_address_of__durationSeconds_3();
		bool L_1 = Nullable_1_get_HasValue_m3631752075(L_0, /*hidden argument*/Nullable_1_get_HasValue_m3631752075_MethodInfo_var);
		return L_1;
	}
}
// System.String Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest::get_Policy()
extern "C"  String_t* AssumeRoleWithWebIdentityRequest_get_Policy_m1592875998 (AssumeRoleWithWebIdentityRequest_t193094215 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get__policy_4();
		return L_0;
	}
}
// System.Boolean Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest::IsSetPolicy()
extern "C"  bool AssumeRoleWithWebIdentityRequest_IsSetPolicy_m2233601292 (AssumeRoleWithWebIdentityRequest_t193094215 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get__policy_4();
		return (bool)((!(((Il2CppObject*)(String_t*)L_0) <= ((Il2CppObject*)(Il2CppObject *)NULL)))? 1 : 0);
	}
}
// System.String Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest::get_ProviderId()
extern "C"  String_t* AssumeRoleWithWebIdentityRequest_get_ProviderId_m1708604136 (AssumeRoleWithWebIdentityRequest_t193094215 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get__providerId_5();
		return L_0;
	}
}
// System.Boolean Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest::IsSetProviderId()
extern "C"  bool AssumeRoleWithWebIdentityRequest_IsSetProviderId_m3772479994 (AssumeRoleWithWebIdentityRequest_t193094215 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get__providerId_5();
		return (bool)((!(((Il2CppObject*)(String_t*)L_0) <= ((Il2CppObject*)(Il2CppObject *)NULL)))? 1 : 0);
	}
}
// System.String Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest::get_RoleArn()
extern "C"  String_t* AssumeRoleWithWebIdentityRequest_get_RoleArn_m4048335823 (AssumeRoleWithWebIdentityRequest_t193094215 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get__roleArn_6();
		return L_0;
	}
}
// System.Void Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest::set_RoleArn(System.String)
extern "C"  void AssumeRoleWithWebIdentityRequest_set_RoleArn_m1456092898 (AssumeRoleWithWebIdentityRequest_t193094215 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set__roleArn_6(L_0);
		return;
	}
}
// System.Boolean Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest::IsSetRoleArn()
extern "C"  bool AssumeRoleWithWebIdentityRequest_IsSetRoleArn_m3216350363 (AssumeRoleWithWebIdentityRequest_t193094215 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get__roleArn_6();
		return (bool)((!(((Il2CppObject*)(String_t*)L_0) <= ((Il2CppObject*)(Il2CppObject *)NULL)))? 1 : 0);
	}
}
// System.String Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest::get_RoleSessionName()
extern "C"  String_t* AssumeRoleWithWebIdentityRequest_get_RoleSessionName_m3730323139 (AssumeRoleWithWebIdentityRequest_t193094215 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get__roleSessionName_7();
		return L_0;
	}
}
// System.Void Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest::set_RoleSessionName(System.String)
extern "C"  void AssumeRoleWithWebIdentityRequest_set_RoleSessionName_m3614869118 (AssumeRoleWithWebIdentityRequest_t193094215 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set__roleSessionName_7(L_0);
		return;
	}
}
// System.Boolean Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest::IsSetRoleSessionName()
extern "C"  bool AssumeRoleWithWebIdentityRequest_IsSetRoleSessionName_m653479599 (AssumeRoleWithWebIdentityRequest_t193094215 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get__roleSessionName_7();
		return (bool)((!(((Il2CppObject*)(String_t*)L_0) <= ((Il2CppObject*)(Il2CppObject *)NULL)))? 1 : 0);
	}
}
// System.String Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest::get_WebIdentityToken()
extern "C"  String_t* AssumeRoleWithWebIdentityRequest_get_WebIdentityToken_m405092039 (AssumeRoleWithWebIdentityRequest_t193094215 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get__webIdentityToken_8();
		return L_0;
	}
}
// System.Void Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest::set_WebIdentityToken(System.String)
extern "C"  void AssumeRoleWithWebIdentityRequest_set_WebIdentityToken_m951693196 (AssumeRoleWithWebIdentityRequest_t193094215 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set__webIdentityToken_8(L_0);
		return;
	}
}
// System.Boolean Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest::IsSetWebIdentityToken()
extern "C"  bool AssumeRoleWithWebIdentityRequest_IsSetWebIdentityToken_m4244755691 (AssumeRoleWithWebIdentityRequest_t193094215 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get__webIdentityToken_8();
		return (bool)((!(((Il2CppObject*)(String_t*)L_0) <= ((Il2CppObject*)(Il2CppObject *)NULL)))? 1 : 0);
	}
}
// System.Void Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest::.ctor()
extern "C"  void AssumeRoleWithWebIdentityRequest__ctor_m3449302728 (AssumeRoleWithWebIdentityRequest_t193094215 * __this, const MethodInfo* method)
{
	{
		AmazonSecurityTokenServiceRequest__ctor_m2827735933(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityResponse::set_AssumedRoleUser(Amazon.SecurityToken.Model.AssumedRoleUser)
extern "C"  void AssumeRoleWithWebIdentityResponse_set_AssumedRoleUser_m3816583747 (AssumeRoleWithWebIdentityResponse_t3931705881 * __this, AssumedRoleUser_t150458319 * ___value0, const MethodInfo* method)
{
	{
		AssumedRoleUser_t150458319 * L_0 = ___value0;
		__this->set__assumedRoleUser_3(L_0);
		return;
	}
}
// System.Void Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityResponse::set_Audience(System.String)
extern "C"  void AssumeRoleWithWebIdentityResponse_set_Audience_m3186117859 (AssumeRoleWithWebIdentityResponse_t3931705881 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set__audience_4(L_0);
		return;
	}
}
// Amazon.SecurityToken.Model.Credentials Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityResponse::get_Credentials()
extern "C"  Credentials_t3554032640 * AssumeRoleWithWebIdentityResponse_get_Credentials_m496982176 (AssumeRoleWithWebIdentityResponse_t3931705881 * __this, const MethodInfo* method)
{
	{
		Credentials_t3554032640 * L_0 = __this->get__credentials_5();
		return L_0;
	}
}
// System.Void Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityResponse::set_Credentials(Amazon.SecurityToken.Model.Credentials)
extern "C"  void AssumeRoleWithWebIdentityResponse_set_Credentials_m3033710271 (AssumeRoleWithWebIdentityResponse_t3931705881 * __this, Credentials_t3554032640 * ___value0, const MethodInfo* method)
{
	{
		Credentials_t3554032640 * L_0 = ___value0;
		__this->set__credentials_5(L_0);
		return;
	}
}
// System.Void Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityResponse::set_PackedPolicySize(System.Int32)
extern const MethodInfo* Nullable_1__ctor_m852490454_MethodInfo_var;
extern const uint32_t AssumeRoleWithWebIdentityResponse_set_PackedPolicySize_m1522389737_MetadataUsageId;
extern "C"  void AssumeRoleWithWebIdentityResponse_set_PackedPolicySize_m1522389737 (AssumeRoleWithWebIdentityResponse_t3931705881 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AssumeRoleWithWebIdentityResponse_set_PackedPolicySize_m1522389737_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___value0;
		Nullable_1_t334943763  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Nullable_1__ctor_m852490454(&L_1, L_0, /*hidden argument*/Nullable_1__ctor_m852490454_MethodInfo_var);
		__this->set__packedPolicySize_6(L_1);
		return;
	}
}
// System.Void Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityResponse::set_Provider(System.String)
extern "C"  void AssumeRoleWithWebIdentityResponse_set_Provider_m3771874134 (AssumeRoleWithWebIdentityResponse_t3931705881 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set__provider_7(L_0);
		return;
	}
}
// System.Void Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityResponse::set_SubjectFromWebIdentityToken(System.String)
extern "C"  void AssumeRoleWithWebIdentityResponse_set_SubjectFromWebIdentityToken_m2341987586 (AssumeRoleWithWebIdentityResponse_t3931705881 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set__subjectFromWebIdentityToken_8(L_0);
		return;
	}
}
// System.Void Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityResponse::.ctor()
extern "C"  void AssumeRoleWithWebIdentityResponse__ctor_m4120461074 (AssumeRoleWithWebIdentityResponse_t3931705881 * __this, const MethodInfo* method)
{
	{
		AmazonWebServiceResponse__ctor_m3474488249(__this, /*hidden argument*/NULL);
		return;
	}
}
// Amazon.Runtime.ImmutableCredentials Amazon.SecurityToken.Model.Credentials::GetCredentials()
extern Il2CppClass* ImmutableCredentials_t282353664_il2cpp_TypeInfo_var;
extern const uint32_t Credentials_GetCredentials_m116816754_MetadataUsageId;
extern "C"  ImmutableCredentials_t282353664 * Credentials_GetCredentials_m116816754 (Credentials_t3554032640 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Credentials_GetCredentials_m116816754_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ImmutableCredentials_t282353664 * L_0 = __this->get__credentials_0();
		if (L_0)
		{
			goto IL_0025;
		}
	}
	{
		String_t* L_1 = Credentials_get_AccessKeyId_m486408405(__this, /*hidden argument*/NULL);
		String_t* L_2 = Credentials_get_SecretAccessKey_m927403830(__this, /*hidden argument*/NULL);
		String_t* L_3 = Credentials_get_SessionToken_m2509135854(__this, /*hidden argument*/NULL);
		ImmutableCredentials_t282353664 * L_4 = (ImmutableCredentials_t282353664 *)il2cpp_codegen_object_new(ImmutableCredentials_t282353664_il2cpp_TypeInfo_var);
		ImmutableCredentials__ctor_m1993825745(L_4, L_1, L_2, L_3, /*hidden argument*/NULL);
		__this->set__credentials_0(L_4);
	}

IL_0025:
	{
		ImmutableCredentials_t282353664 * L_5 = __this->get__credentials_0();
		NullCheck(L_5);
		ImmutableCredentials_t282353664 * L_6 = ImmutableCredentials_Copy_m872003133(L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void Amazon.SecurityToken.Model.Credentials::.ctor()
extern "C"  void Credentials__ctor_m280882687 (Credentials_t3554032640 * __this, const MethodInfo* method)
{
	{
		AWSCredentials__ctor_m2037750724(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String Amazon.SecurityToken.Model.Credentials::get_AccessKeyId()
extern "C"  String_t* Credentials_get_AccessKeyId_m486408405 (Credentials_t3554032640 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get__accessKeyId_1();
		return L_0;
	}
}
// System.Void Amazon.SecurityToken.Model.Credentials::set_AccessKeyId(System.String)
extern "C"  void Credentials_set_AccessKeyId_m658001590 (Credentials_t3554032640 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set__accessKeyId_1(L_0);
		return;
	}
}
// System.DateTime Amazon.SecurityToken.Model.Credentials::get_Expiration()
extern const MethodInfo* Nullable_1_GetValueOrDefault_m113677951_MethodInfo_var;
extern const uint32_t Credentials_get_Expiration_m963783206_MetadataUsageId;
extern "C"  DateTime_t693205669  Credentials_get_Expiration_m963783206 (Credentials_t3554032640 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Credentials_get_Expiration_m963783206_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Nullable_1_t3251239280 * L_0 = __this->get_address_of__expiration_2();
		DateTime_t693205669  L_1 = Nullable_1_GetValueOrDefault_m113677951(L_0, /*hidden argument*/Nullable_1_GetValueOrDefault_m113677951_MethodInfo_var);
		return L_1;
	}
}
// System.Void Amazon.SecurityToken.Model.Credentials::set_Expiration(System.DateTime)
extern const MethodInfo* Nullable_1__ctor_m1750116951_MethodInfo_var;
extern const uint32_t Credentials_set_Expiration_m4276484463_MetadataUsageId;
extern "C"  void Credentials_set_Expiration_m4276484463 (Credentials_t3554032640 * __this, DateTime_t693205669  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Credentials_set_Expiration_m4276484463_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		DateTime_t693205669  L_0 = ___value0;
		Nullable_1_t3251239280  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Nullable_1__ctor_m1750116951(&L_1, L_0, /*hidden argument*/Nullable_1__ctor_m1750116951_MethodInfo_var);
		__this->set__expiration_2(L_1);
		return;
	}
}
// System.String Amazon.SecurityToken.Model.Credentials::get_SecretAccessKey()
extern "C"  String_t* Credentials_get_SecretAccessKey_m927403830 (Credentials_t3554032640 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get__secretAccessKey_3();
		return L_0;
	}
}
// System.Void Amazon.SecurityToken.Model.Credentials::set_SecretAccessKey(System.String)
extern "C"  void Credentials_set_SecretAccessKey_m1846861829 (Credentials_t3554032640 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set__secretAccessKey_3(L_0);
		return;
	}
}
// System.String Amazon.SecurityToken.Model.Credentials::get_SessionToken()
extern "C"  String_t* Credentials_get_SessionToken_m2509135854 (Credentials_t3554032640 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get__sessionToken_4();
		return L_0;
	}
}
// System.Void Amazon.SecurityToken.Model.Credentials::set_SessionToken(System.String)
extern "C"  void Credentials_set_SessionToken_m37439381 (Credentials_t3554032640 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set__sessionToken_4(L_0);
		return;
	}
}
// System.Void Amazon.SecurityToken.Model.ExpiredTokenException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
extern "C"  void ExpiredTokenException__ctor_m2499536043 (ExpiredTokenException_t1058704965 * __this, String_t* ___message0, Exception_t1927440687 * ___innerException1, int32_t ___errorType2, String_t* ___errorCode3, String_t* ___requestId4, int32_t ___statusCode5, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		Exception_t1927440687 * L_1 = ___innerException1;
		int32_t L_2 = ___errorType2;
		String_t* L_3 = ___errorCode3;
		String_t* L_4 = ___requestId4;
		int32_t L_5 = ___statusCode5;
		AmazonSecurityTokenServiceException__ctor_m4274350954(__this, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Amazon.SecurityToken.Model.ExpiredTokenException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void ExpiredTokenException__ctor_m2392106947 (ExpiredTokenException_t1058704965 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method)
{
	{
		SerializationInfo_t228987430 * L_0 = ___info0;
		StreamingContext_t1417235061  L_1 = ___context1;
		AmazonSecurityTokenServiceException__ctor_m2614436174(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Amazon.SecurityToken.Model.IDPCommunicationErrorException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
extern "C"  void IDPCommunicationErrorException__ctor_m1130552656 (IDPCommunicationErrorException_t2234231496 * __this, String_t* ___message0, Exception_t1927440687 * ___innerException1, int32_t ___errorType2, String_t* ___errorCode3, String_t* ___requestId4, int32_t ___statusCode5, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		Exception_t1927440687 * L_1 = ___innerException1;
		int32_t L_2 = ___errorType2;
		String_t* L_3 = ___errorCode3;
		String_t* L_4 = ___requestId4;
		int32_t L_5 = ___statusCode5;
		AmazonSecurityTokenServiceException__ctor_m4274350954(__this, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Amazon.SecurityToken.Model.IDPCommunicationErrorException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void IDPCommunicationErrorException__ctor_m580102248 (IDPCommunicationErrorException_t2234231496 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method)
{
	{
		SerializationInfo_t228987430 * L_0 = ___info0;
		StreamingContext_t1417235061  L_1 = ___context1;
		AmazonSecurityTokenServiceException__ctor_m2614436174(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Amazon.SecurityToken.Model.IDPRejectedClaimException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
extern "C"  void IDPRejectedClaimException__ctor_m1994288158 (IDPRejectedClaimException_t367458406 * __this, String_t* ___message0, Exception_t1927440687 * ___innerException1, int32_t ___errorType2, String_t* ___errorCode3, String_t* ___requestId4, int32_t ___statusCode5, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		Exception_t1927440687 * L_1 = ___innerException1;
		int32_t L_2 = ___errorType2;
		String_t* L_3 = ___errorCode3;
		String_t* L_4 = ___requestId4;
		int32_t L_5 = ___statusCode5;
		AmazonSecurityTokenServiceException__ctor_m4274350954(__this, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Amazon.SecurityToken.Model.IDPRejectedClaimException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void IDPRejectedClaimException__ctor_m825776726 (IDPRejectedClaimException_t367458406 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method)
{
	{
		SerializationInfo_t228987430 * L_0 = ___info0;
		StreamingContext_t1417235061  L_1 = ___context1;
		AmazonSecurityTokenServiceException__ctor_m2614436174(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// Amazon.SecurityToken.Model.AssumedRoleUser Amazon.SecurityToken.Model.Internal.MarshallTransformations.AssumedRoleUserUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext)
extern Il2CppClass* AssumedRoleUser_t150458319_il2cpp_TypeInfo_var;
extern Il2CppClass* StringUnmarshaller_t3953260147_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral386854109;
extern Il2CppCodeGenString* _stringLiteral2328816741;
extern const uint32_t AssumedRoleUserUnmarshaller_Unmarshall_m1449889951_MetadataUsageId;
extern "C"  AssumedRoleUser_t150458319 * AssumedRoleUserUnmarshaller_Unmarshall_m1449889951 (AssumedRoleUserUnmarshaller_t2473561683 * __this, XmlUnmarshallerContext_t1179575220 * ___context0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AssumedRoleUserUnmarshaller_Unmarshall_m1449889951_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AssumedRoleUser_t150458319 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	StringUnmarshaller_t3953260147 * V_3 = NULL;
	StringUnmarshaller_t3953260147 * V_4 = NULL;
	{
		AssumedRoleUser_t150458319 * L_0 = (AssumedRoleUser_t150458319 *)il2cpp_codegen_object_new(AssumedRoleUser_t150458319_il2cpp_TypeInfo_var);
		AssumedRoleUser__ctor_m2660329274(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		XmlUnmarshallerContext_t1179575220 * L_1 = ___context0;
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 Amazon.Runtime.Internal.Transform.UnmarshallerContext::get_CurrentDepth() */, L_1);
		V_1 = L_2;
		int32_t L_3 = V_1;
		V_2 = ((int32_t)((int32_t)L_3+(int32_t)1));
		XmlUnmarshallerContext_t1179575220 * L_4 = ___context0;
		NullCheck(L_4);
		bool L_5 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean Amazon.Runtime.Internal.Transform.UnmarshallerContext::get_IsStartOfDocument() */, L_4);
		if (!L_5)
		{
			goto IL_008a;
		}
	}
	{
		int32_t L_6 = V_2;
		V_2 = ((int32_t)((int32_t)L_6+(int32_t)2));
		goto IL_008a;
	}

IL_001f:
	{
		XmlUnmarshallerContext_t1179575220 * L_7 = ___context0;
		NullCheck(L_7);
		bool L_8 = VirtFuncInvoker0< bool >::Invoke(9 /* System.Boolean Amazon.Runtime.Internal.Transform.UnmarshallerContext::get_IsStartElement() */, L_7);
		if (L_8)
		{
			goto IL_002f;
		}
	}
	{
		XmlUnmarshallerContext_t1179575220 * L_9 = ___context0;
		NullCheck(L_9);
		bool L_10 = XmlUnmarshallerContext_get_IsAttribute_m1726264931(L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0077;
		}
	}

IL_002f:
	{
		XmlUnmarshallerContext_t1179575220 * L_11 = ___context0;
		int32_t L_12 = V_2;
		NullCheck(L_11);
		bool L_13 = UnmarshallerContext_TestExpression_m3665247446(L_11, _stringLiteral386854109, L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0052;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(StringUnmarshaller_t3953260147_il2cpp_TypeInfo_var);
		StringUnmarshaller_t3953260147 * L_14 = StringUnmarshaller_get_Instance_m107503370(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_3 = L_14;
		AssumedRoleUser_t150458319 * L_15 = V_0;
		StringUnmarshaller_t3953260147 * L_16 = V_3;
		XmlUnmarshallerContext_t1179575220 * L_17 = ___context0;
		NullCheck(L_16);
		String_t* L_18 = StringUnmarshaller_Unmarshall_m2571974896(L_16, L_17, /*hidden argument*/NULL);
		NullCheck(L_15);
		AssumedRoleUser_set_Arn_m1960284340(L_15, L_18, /*hidden argument*/NULL);
		goto IL_008a;
	}

IL_0052:
	{
		XmlUnmarshallerContext_t1179575220 * L_19 = ___context0;
		int32_t L_20 = V_2;
		NullCheck(L_19);
		bool L_21 = UnmarshallerContext_TestExpression_m3665247446(L_19, _stringLiteral2328816741, L_20, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_008a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(StringUnmarshaller_t3953260147_il2cpp_TypeInfo_var);
		StringUnmarshaller_t3953260147 * L_22 = StringUnmarshaller_get_Instance_m107503370(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_4 = L_22;
		AssumedRoleUser_t150458319 * L_23 = V_0;
		StringUnmarshaller_t3953260147 * L_24 = V_4;
		XmlUnmarshallerContext_t1179575220 * L_25 = ___context0;
		NullCheck(L_24);
		String_t* L_26 = StringUnmarshaller_Unmarshall_m2571974896(L_24, L_25, /*hidden argument*/NULL);
		NullCheck(L_23);
		AssumedRoleUser_set_AssumedRoleId_m2821507572(L_23, L_26, /*hidden argument*/NULL);
		goto IL_008a;
	}

IL_0077:
	{
		XmlUnmarshallerContext_t1179575220 * L_27 = ___context0;
		NullCheck(L_27);
		bool L_28 = VirtFuncInvoker0< bool >::Invoke(10 /* System.Boolean Amazon.Runtime.Internal.Transform.UnmarshallerContext::get_IsEndElement() */, L_27);
		if (!L_28)
		{
			goto IL_008a;
		}
	}
	{
		XmlUnmarshallerContext_t1179575220 * L_29 = ___context0;
		NullCheck(L_29);
		int32_t L_30 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 Amazon.Runtime.Internal.Transform.UnmarshallerContext::get_CurrentDepth() */, L_29);
		int32_t L_31 = V_1;
		if ((((int32_t)L_30) >= ((int32_t)L_31)))
		{
			goto IL_008a;
		}
	}
	{
		AssumedRoleUser_t150458319 * L_32 = V_0;
		return L_32;
	}

IL_008a:
	{
		XmlUnmarshallerContext_t1179575220 * L_33 = ___context0;
		int32_t L_34 = V_1;
		NullCheck(L_33);
		bool L_35 = UnmarshallerContext_ReadAtDepth_m2943457426(L_33, L_34, /*hidden argument*/NULL);
		if (L_35)
		{
			goto IL_001f;
		}
	}
	{
		AssumedRoleUser_t150458319 * L_36 = V_0;
		return L_36;
	}
}
// Amazon.SecurityToken.Model.AssumedRoleUser Amazon.SecurityToken.Model.Internal.MarshallTransformations.AssumedRoleUserUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern "C"  AssumedRoleUser_t150458319 * AssumedRoleUserUnmarshaller_Unmarshall_m3565771478 (AssumedRoleUserUnmarshaller_t2473561683 * __this, JsonUnmarshallerContext_t456235889 * ___context0, const MethodInfo* method)
{
	{
		return (AssumedRoleUser_t150458319 *)NULL;
	}
}
// Amazon.SecurityToken.Model.Internal.MarshallTransformations.AssumedRoleUserUnmarshaller Amazon.SecurityToken.Model.Internal.MarshallTransformations.AssumedRoleUserUnmarshaller::get_Instance()
extern Il2CppClass* AssumedRoleUserUnmarshaller_t2473561683_il2cpp_TypeInfo_var;
extern const uint32_t AssumedRoleUserUnmarshaller_get_Instance_m2346020868_MetadataUsageId;
extern "C"  AssumedRoleUserUnmarshaller_t2473561683 * AssumedRoleUserUnmarshaller_get_Instance_m2346020868 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AssumedRoleUserUnmarshaller_get_Instance_m2346020868_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(AssumedRoleUserUnmarshaller_t2473561683_il2cpp_TypeInfo_var);
		AssumedRoleUserUnmarshaller_t2473561683 * L_0 = ((AssumedRoleUserUnmarshaller_t2473561683_StaticFields*)AssumedRoleUserUnmarshaller_t2473561683_il2cpp_TypeInfo_var->static_fields)->get__instance_0();
		return L_0;
	}
}
// System.Void Amazon.SecurityToken.Model.Internal.MarshallTransformations.AssumedRoleUserUnmarshaller::.ctor()
extern "C"  void AssumedRoleUserUnmarshaller__ctor_m2457816639 (AssumedRoleUserUnmarshaller_t2473561683 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Amazon.SecurityToken.Model.Internal.MarshallTransformations.AssumedRoleUserUnmarshaller::.cctor()
extern Il2CppClass* AssumedRoleUserUnmarshaller_t2473561683_il2cpp_TypeInfo_var;
extern const uint32_t AssumedRoleUserUnmarshaller__cctor_m2822142530_MetadataUsageId;
extern "C"  void AssumedRoleUserUnmarshaller__cctor_m2822142530 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AssumedRoleUserUnmarshaller__cctor_m2822142530_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AssumedRoleUserUnmarshaller_t2473561683 * L_0 = (AssumedRoleUserUnmarshaller_t2473561683 *)il2cpp_codegen_object_new(AssumedRoleUserUnmarshaller_t2473561683_il2cpp_TypeInfo_var);
		AssumedRoleUserUnmarshaller__ctor_m2457816639(L_0, /*hidden argument*/NULL);
		((AssumedRoleUserUnmarshaller_t2473561683_StaticFields*)AssumedRoleUserUnmarshaller_t2473561683_il2cpp_TypeInfo_var->static_fields)->set__instance_0(L_0);
		return;
	}
}
// Amazon.Runtime.Internal.IRequest Amazon.SecurityToken.Model.Internal.MarshallTransformations.AssumeRoleWithWebIdentityRequestMarshaller::Marshall(Amazon.Runtime.AmazonWebServiceRequest)
extern Il2CppClass* AssumeRoleWithWebIdentityRequest_t193094215_il2cpp_TypeInfo_var;
extern const uint32_t AssumeRoleWithWebIdentityRequestMarshaller_Marshall_m404585616_MetadataUsageId;
extern "C"  Il2CppObject * AssumeRoleWithWebIdentityRequestMarshaller_Marshall_m404585616 (AssumeRoleWithWebIdentityRequestMarshaller_t1815493036 * __this, AmazonWebServiceRequest_t3384026212 * ___input0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AssumeRoleWithWebIdentityRequestMarshaller_Marshall_m404585616_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AmazonWebServiceRequest_t3384026212 * L_0 = ___input0;
		Il2CppObject * L_1 = AssumeRoleWithWebIdentityRequestMarshaller_Marshall_m1111591347(__this, ((AssumeRoleWithWebIdentityRequest_t193094215 *)CastclassClass(L_0, AssumeRoleWithWebIdentityRequest_t193094215_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return L_1;
	}
}
// Amazon.Runtime.Internal.IRequest Amazon.SecurityToken.Model.Internal.MarshallTransformations.AssumeRoleWithWebIdentityRequestMarshaller::Marshall(Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest)
extern Il2CppClass* DefaultRequest_t3080757440_il2cpp_TypeInfo_var;
extern Il2CppClass* IRequest_t2400804350_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_2_t1943082916_il2cpp_TypeInfo_var;
extern Il2CppClass* StringUtils_t1089313136_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1762942677;
extern Il2CppCodeGenString* _stringLiteral3806092036;
extern Il2CppCodeGenString* _stringLiteral1720637116;
extern Il2CppCodeGenString* _stringLiteral4699698;
extern Il2CppCodeGenString* _stringLiteral3629752616;
extern Il2CppCodeGenString* _stringLiteral3912255965;
extern Il2CppCodeGenString* _stringLiteral816661212;
extern Il2CppCodeGenString* _stringLiteral589561758;
extern Il2CppCodeGenString* _stringLiteral3196682245;
extern Il2CppCodeGenString* _stringLiteral3248881953;
extern Il2CppCodeGenString* _stringLiteral887622993;
extern const uint32_t AssumeRoleWithWebIdentityRequestMarshaller_Marshall_m1111591347_MetadataUsageId;
extern "C"  Il2CppObject * AssumeRoleWithWebIdentityRequestMarshaller_Marshall_m1111591347 (AssumeRoleWithWebIdentityRequestMarshaller_t1815493036 * __this, AssumeRoleWithWebIdentityRequest_t193094215 * ___publicRequest0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AssumeRoleWithWebIdentityRequestMarshaller_Marshall_m1111591347_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		AssumeRoleWithWebIdentityRequest_t193094215 * L_0 = ___publicRequest0;
		DefaultRequest_t3080757440 * L_1 = (DefaultRequest_t3080757440 *)il2cpp_codegen_object_new(DefaultRequest_t3080757440_il2cpp_TypeInfo_var);
		DefaultRequest__ctor_m3534427872(L_1, L_0, _stringLiteral1762942677, /*hidden argument*/NULL);
		V_0 = L_1;
		Il2CppObject * L_2 = V_0;
		NullCheck(L_2);
		Il2CppObject* L_3 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(4 /* System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.Runtime.Internal.IRequest::get_Parameters() */, IRequest_t2400804350_il2cpp_TypeInfo_var, L_2);
		NullCheck(L_3);
		InterfaceActionInvoker2< String_t*, String_t* >::Invoke(0 /* System.Void System.Collections.Generic.IDictionary`2<System.String,System.String>::Add(!0,!1) */, IDictionary_2_t1943082916_il2cpp_TypeInfo_var, L_3, _stringLiteral3806092036, _stringLiteral1720637116);
		Il2CppObject * L_4 = V_0;
		NullCheck(L_4);
		Il2CppObject* L_5 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(4 /* System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.Runtime.Internal.IRequest::get_Parameters() */, IRequest_t2400804350_il2cpp_TypeInfo_var, L_4);
		NullCheck(L_5);
		InterfaceActionInvoker2< String_t*, String_t* >::Invoke(0 /* System.Void System.Collections.Generic.IDictionary`2<System.String,System.String>::Add(!0,!1) */, IDictionary_2_t1943082916_il2cpp_TypeInfo_var, L_5, _stringLiteral4699698, _stringLiteral3629752616);
		AssumeRoleWithWebIdentityRequest_t193094215 * L_6 = ___publicRequest0;
		if (!L_6)
		{
			goto IL_010e;
		}
	}
	{
		AssumeRoleWithWebIdentityRequest_t193094215 * L_7 = ___publicRequest0;
		NullCheck(L_7);
		bool L_8 = AssumeRoleWithWebIdentityRequest_IsSetDurationSeconds_m4189349523(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_005f;
		}
	}
	{
		Il2CppObject * L_9 = V_0;
		NullCheck(L_9);
		Il2CppObject* L_10 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(4 /* System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.Runtime.Internal.IRequest::get_Parameters() */, IRequest_t2400804350_il2cpp_TypeInfo_var, L_9);
		AssumeRoleWithWebIdentityRequest_t193094215 * L_11 = ___publicRequest0;
		NullCheck(L_11);
		int32_t L_12 = AssumeRoleWithWebIdentityRequest_get_DurationSeconds_m179215680(L_11, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(StringUtils_t1089313136_il2cpp_TypeInfo_var);
		String_t* L_13 = StringUtils_FromInt_m146443913(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		NullCheck(L_10);
		InterfaceActionInvoker2< String_t*, String_t* >::Invoke(0 /* System.Void System.Collections.Generic.IDictionary`2<System.String,System.String>::Add(!0,!1) */, IDictionary_2_t1943082916_il2cpp_TypeInfo_var, L_10, _stringLiteral3912255965, L_13);
	}

IL_005f:
	{
		AssumeRoleWithWebIdentityRequest_t193094215 * L_14 = ___publicRequest0;
		NullCheck(L_14);
		bool L_15 = AssumeRoleWithWebIdentityRequest_IsSetPolicy_m2233601292(L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0082;
		}
	}
	{
		Il2CppObject * L_16 = V_0;
		NullCheck(L_16);
		Il2CppObject* L_17 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(4 /* System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.Runtime.Internal.IRequest::get_Parameters() */, IRequest_t2400804350_il2cpp_TypeInfo_var, L_16);
		AssumeRoleWithWebIdentityRequest_t193094215 * L_18 = ___publicRequest0;
		NullCheck(L_18);
		String_t* L_19 = AssumeRoleWithWebIdentityRequest_get_Policy_m1592875998(L_18, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(StringUtils_t1089313136_il2cpp_TypeInfo_var);
		String_t* L_20 = StringUtils_FromString_m1100270676(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		NullCheck(L_17);
		InterfaceActionInvoker2< String_t*, String_t* >::Invoke(0 /* System.Void System.Collections.Generic.IDictionary`2<System.String,System.String>::Add(!0,!1) */, IDictionary_2_t1943082916_il2cpp_TypeInfo_var, L_17, _stringLiteral816661212, L_20);
	}

IL_0082:
	{
		AssumeRoleWithWebIdentityRequest_t193094215 * L_21 = ___publicRequest0;
		NullCheck(L_21);
		bool L_22 = AssumeRoleWithWebIdentityRequest_IsSetProviderId_m3772479994(L_21, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_00a5;
		}
	}
	{
		Il2CppObject * L_23 = V_0;
		NullCheck(L_23);
		Il2CppObject* L_24 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(4 /* System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.Runtime.Internal.IRequest::get_Parameters() */, IRequest_t2400804350_il2cpp_TypeInfo_var, L_23);
		AssumeRoleWithWebIdentityRequest_t193094215 * L_25 = ___publicRequest0;
		NullCheck(L_25);
		String_t* L_26 = AssumeRoleWithWebIdentityRequest_get_ProviderId_m1708604136(L_25, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(StringUtils_t1089313136_il2cpp_TypeInfo_var);
		String_t* L_27 = StringUtils_FromString_m1100270676(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
		NullCheck(L_24);
		InterfaceActionInvoker2< String_t*, String_t* >::Invoke(0 /* System.Void System.Collections.Generic.IDictionary`2<System.String,System.String>::Add(!0,!1) */, IDictionary_2_t1943082916_il2cpp_TypeInfo_var, L_24, _stringLiteral589561758, L_27);
	}

IL_00a5:
	{
		AssumeRoleWithWebIdentityRequest_t193094215 * L_28 = ___publicRequest0;
		NullCheck(L_28);
		bool L_29 = AssumeRoleWithWebIdentityRequest_IsSetRoleArn_m3216350363(L_28, /*hidden argument*/NULL);
		if (!L_29)
		{
			goto IL_00c8;
		}
	}
	{
		Il2CppObject * L_30 = V_0;
		NullCheck(L_30);
		Il2CppObject* L_31 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(4 /* System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.Runtime.Internal.IRequest::get_Parameters() */, IRequest_t2400804350_il2cpp_TypeInfo_var, L_30);
		AssumeRoleWithWebIdentityRequest_t193094215 * L_32 = ___publicRequest0;
		NullCheck(L_32);
		String_t* L_33 = AssumeRoleWithWebIdentityRequest_get_RoleArn_m4048335823(L_32, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(StringUtils_t1089313136_il2cpp_TypeInfo_var);
		String_t* L_34 = StringUtils_FromString_m1100270676(NULL /*static, unused*/, L_33, /*hidden argument*/NULL);
		NullCheck(L_31);
		InterfaceActionInvoker2< String_t*, String_t* >::Invoke(0 /* System.Void System.Collections.Generic.IDictionary`2<System.String,System.String>::Add(!0,!1) */, IDictionary_2_t1943082916_il2cpp_TypeInfo_var, L_31, _stringLiteral3196682245, L_34);
	}

IL_00c8:
	{
		AssumeRoleWithWebIdentityRequest_t193094215 * L_35 = ___publicRequest0;
		NullCheck(L_35);
		bool L_36 = AssumeRoleWithWebIdentityRequest_IsSetRoleSessionName_m653479599(L_35, /*hidden argument*/NULL);
		if (!L_36)
		{
			goto IL_00eb;
		}
	}
	{
		Il2CppObject * L_37 = V_0;
		NullCheck(L_37);
		Il2CppObject* L_38 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(4 /* System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.Runtime.Internal.IRequest::get_Parameters() */, IRequest_t2400804350_il2cpp_TypeInfo_var, L_37);
		AssumeRoleWithWebIdentityRequest_t193094215 * L_39 = ___publicRequest0;
		NullCheck(L_39);
		String_t* L_40 = AssumeRoleWithWebIdentityRequest_get_RoleSessionName_m3730323139(L_39, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(StringUtils_t1089313136_il2cpp_TypeInfo_var);
		String_t* L_41 = StringUtils_FromString_m1100270676(NULL /*static, unused*/, L_40, /*hidden argument*/NULL);
		NullCheck(L_38);
		InterfaceActionInvoker2< String_t*, String_t* >::Invoke(0 /* System.Void System.Collections.Generic.IDictionary`2<System.String,System.String>::Add(!0,!1) */, IDictionary_2_t1943082916_il2cpp_TypeInfo_var, L_38, _stringLiteral3248881953, L_41);
	}

IL_00eb:
	{
		AssumeRoleWithWebIdentityRequest_t193094215 * L_42 = ___publicRequest0;
		NullCheck(L_42);
		bool L_43 = AssumeRoleWithWebIdentityRequest_IsSetWebIdentityToken_m4244755691(L_42, /*hidden argument*/NULL);
		if (!L_43)
		{
			goto IL_010e;
		}
	}
	{
		Il2CppObject * L_44 = V_0;
		NullCheck(L_44);
		Il2CppObject* L_45 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(4 /* System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.Runtime.Internal.IRequest::get_Parameters() */, IRequest_t2400804350_il2cpp_TypeInfo_var, L_44);
		AssumeRoleWithWebIdentityRequest_t193094215 * L_46 = ___publicRequest0;
		NullCheck(L_46);
		String_t* L_47 = AssumeRoleWithWebIdentityRequest_get_WebIdentityToken_m405092039(L_46, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(StringUtils_t1089313136_il2cpp_TypeInfo_var);
		String_t* L_48 = StringUtils_FromString_m1100270676(NULL /*static, unused*/, L_47, /*hidden argument*/NULL);
		NullCheck(L_45);
		InterfaceActionInvoker2< String_t*, String_t* >::Invoke(0 /* System.Void System.Collections.Generic.IDictionary`2<System.String,System.String>::Add(!0,!1) */, IDictionary_2_t1943082916_il2cpp_TypeInfo_var, L_45, _stringLiteral887622993, L_48);
	}

IL_010e:
	{
		Il2CppObject * L_49 = V_0;
		return L_49;
	}
}
// System.Void Amazon.SecurityToken.Model.Internal.MarshallTransformations.AssumeRoleWithWebIdentityRequestMarshaller::.ctor()
extern "C"  void AssumeRoleWithWebIdentityRequestMarshaller__ctor_m3147474726 (AssumeRoleWithWebIdentityRequestMarshaller_t1815493036 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// Amazon.Runtime.AmazonWebServiceResponse Amazon.SecurityToken.Model.Internal.MarshallTransformations.AssumeRoleWithWebIdentityResponseUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext)
extern Il2CppClass* AssumeRoleWithWebIdentityResponse_t3931705881_il2cpp_TypeInfo_var;
extern Il2CppClass* AssumeRoleWithWebIdentityResponseUnmarshaller_t3698885375_il2cpp_TypeInfo_var;
extern Il2CppClass* ResponseMetadataUnmarshaller_t1207185784_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2128488985;
extern Il2CppCodeGenString* _stringLiteral36321836;
extern const uint32_t AssumeRoleWithWebIdentityResponseUnmarshaller_Unmarshall_m3864513860_MetadataUsageId;
extern "C"  AmazonWebServiceResponse_t529043356 * AssumeRoleWithWebIdentityResponseUnmarshaller_Unmarshall_m3864513860 (AssumeRoleWithWebIdentityResponseUnmarshaller_t3698885375 * __this, XmlUnmarshallerContext_t1179575220 * ___context0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AssumeRoleWithWebIdentityResponseUnmarshaller_Unmarshall_m3864513860_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AssumeRoleWithWebIdentityResponse_t3931705881 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		AssumeRoleWithWebIdentityResponse_t3931705881 * L_0 = (AssumeRoleWithWebIdentityResponse_t3931705881 *)il2cpp_codegen_object_new(AssumeRoleWithWebIdentityResponse_t3931705881_il2cpp_TypeInfo_var);
		AssumeRoleWithWebIdentityResponse__ctor_m4120461074(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		XmlUnmarshallerContext_t1179575220 * L_1 = ___context0;
		NullCheck(L_1);
		VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean Amazon.Runtime.Internal.Transform.UnmarshallerContext::Read() */, L_1);
		XmlUnmarshallerContext_t1179575220 * L_2 = ___context0;
		NullCheck(L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 Amazon.Runtime.Internal.Transform.UnmarshallerContext::get_CurrentDepth() */, L_2);
		V_1 = L_3;
		goto IL_0054;
	}

IL_0016:
	{
		XmlUnmarshallerContext_t1179575220 * L_4 = ___context0;
		NullCheck(L_4);
		bool L_5 = VirtFuncInvoker0< bool >::Invoke(9 /* System.Boolean Amazon.Runtime.Internal.Transform.UnmarshallerContext::get_IsStartElement() */, L_4);
		if (!L_5)
		{
			goto IL_0054;
		}
	}
	{
		XmlUnmarshallerContext_t1179575220 * L_6 = ___context0;
		NullCheck(L_6);
		bool L_7 = UnmarshallerContext_TestExpression_m3665247446(L_6, _stringLiteral2128488985, 2, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0035;
		}
	}
	{
		XmlUnmarshallerContext_t1179575220 * L_8 = ___context0;
		AssumeRoleWithWebIdentityResponse_t3931705881 * L_9 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(AssumeRoleWithWebIdentityResponseUnmarshaller_t3698885375_il2cpp_TypeInfo_var);
		AssumeRoleWithWebIdentityResponseUnmarshaller_UnmarshallResult_m2565070237(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		goto IL_0054;
	}

IL_0035:
	{
		XmlUnmarshallerContext_t1179575220 * L_10 = ___context0;
		NullCheck(L_10);
		bool L_11 = UnmarshallerContext_TestExpression_m3665247446(L_10, _stringLiteral36321836, 2, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0054;
		}
	}
	{
		AssumeRoleWithWebIdentityResponse_t3931705881 * L_12 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(ResponseMetadataUnmarshaller_t1207185784_il2cpp_TypeInfo_var);
		ResponseMetadataUnmarshaller_t1207185784 * L_13 = ResponseMetadataUnmarshaller_get_Instance_m945488888(NULL /*static, unused*/, /*hidden argument*/NULL);
		XmlUnmarshallerContext_t1179575220 * L_14 = ___context0;
		NullCheck(L_13);
		ResponseMetadata_t527027456 * L_15 = ResponseMetadataUnmarshaller_Unmarshall_m1427520039(L_13, L_14, /*hidden argument*/NULL);
		NullCheck(L_12);
		AmazonWebServiceResponse_set_ResponseMetadata_m2931428714(L_12, L_15, /*hidden argument*/NULL);
	}

IL_0054:
	{
		XmlUnmarshallerContext_t1179575220 * L_16 = ___context0;
		int32_t L_17 = V_1;
		NullCheck(L_16);
		bool L_18 = UnmarshallerContext_ReadAtDepth_m2943457426(L_16, L_17, /*hidden argument*/NULL);
		if (L_18)
		{
			goto IL_0016;
		}
	}
	{
		AssumeRoleWithWebIdentityResponse_t3931705881 * L_19 = V_0;
		return L_19;
	}
}
// System.Void Amazon.SecurityToken.Model.Internal.MarshallTransformations.AssumeRoleWithWebIdentityResponseUnmarshaller::UnmarshallResult(Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext,Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityResponse)
extern Il2CppClass* AssumedRoleUserUnmarshaller_t2473561683_il2cpp_TypeInfo_var;
extern Il2CppClass* StringUnmarshaller_t3953260147_il2cpp_TypeInfo_var;
extern Il2CppClass* CredentialsUnmarshaller_t3599710454_il2cpp_TypeInfo_var;
extern Il2CppClass* IntUnmarshaller_t2174001701_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2606451677;
extern Il2CppCodeGenString* _stringLiteral3912029664;
extern Il2CppCodeGenString* _stringLiteral2015805848;
extern Il2CppCodeGenString* _stringLiteral3022960573;
extern Il2CppCodeGenString* _stringLiteral1682529065;
extern Il2CppCodeGenString* _stringLiteral856808875;
extern const uint32_t AssumeRoleWithWebIdentityResponseUnmarshaller_UnmarshallResult_m2565070237_MetadataUsageId;
extern "C"  void AssumeRoleWithWebIdentityResponseUnmarshaller_UnmarshallResult_m2565070237 (Il2CppObject * __this /* static, unused */, XmlUnmarshallerContext_t1179575220 * ___context0, AssumeRoleWithWebIdentityResponse_t3931705881 * ___response1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AssumeRoleWithWebIdentityResponseUnmarshaller_UnmarshallResult_m2565070237_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	AssumedRoleUserUnmarshaller_t2473561683 * V_2 = NULL;
	StringUnmarshaller_t3953260147 * V_3 = NULL;
	CredentialsUnmarshaller_t3599710454 * V_4 = NULL;
	IntUnmarshaller_t2174001701 * V_5 = NULL;
	StringUnmarshaller_t3953260147 * V_6 = NULL;
	StringUnmarshaller_t3953260147 * V_7 = NULL;
	{
		XmlUnmarshallerContext_t1179575220 * L_0 = ___context0;
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 Amazon.Runtime.Internal.Transform.UnmarshallerContext::get_CurrentDepth() */, L_0);
		V_0 = L_1;
		int32_t L_2 = V_0;
		V_1 = ((int32_t)((int32_t)L_2+(int32_t)1));
		XmlUnmarshallerContext_t1179575220 * L_3 = ___context0;
		NullCheck(L_3);
		bool L_4 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean Amazon.Runtime.Internal.Transform.UnmarshallerContext::get_IsStartOfDocument() */, L_3);
		if (!L_4)
		{
			goto IL_0110;
		}
	}
	{
		int32_t L_5 = V_1;
		V_1 = ((int32_t)((int32_t)L_5+(int32_t)2));
		goto IL_0110;
	}

IL_001f:
	{
		XmlUnmarshallerContext_t1179575220 * L_6 = ___context0;
		NullCheck(L_6);
		bool L_7 = VirtFuncInvoker0< bool >::Invoke(9 /* System.Boolean Amazon.Runtime.Internal.Transform.UnmarshallerContext::get_IsStartElement() */, L_6);
		if (L_7)
		{
			goto IL_0032;
		}
	}
	{
		XmlUnmarshallerContext_t1179575220 * L_8 = ___context0;
		NullCheck(L_8);
		bool L_9 = XmlUnmarshallerContext_get_IsAttribute_m1726264931(L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0110;
		}
	}

IL_0032:
	{
		XmlUnmarshallerContext_t1179575220 * L_10 = ___context0;
		int32_t L_11 = V_1;
		NullCheck(L_10);
		bool L_12 = UnmarshallerContext_TestExpression_m3665247446(L_10, _stringLiteral2606451677, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0058;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(AssumedRoleUserUnmarshaller_t2473561683_il2cpp_TypeInfo_var);
		AssumedRoleUserUnmarshaller_t2473561683 * L_13 = AssumedRoleUserUnmarshaller_get_Instance_m2346020868(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = L_13;
		AssumeRoleWithWebIdentityResponse_t3931705881 * L_14 = ___response1;
		AssumedRoleUserUnmarshaller_t2473561683 * L_15 = V_2;
		XmlUnmarshallerContext_t1179575220 * L_16 = ___context0;
		NullCheck(L_15);
		AssumedRoleUser_t150458319 * L_17 = AssumedRoleUserUnmarshaller_Unmarshall_m1449889951(L_15, L_16, /*hidden argument*/NULL);
		NullCheck(L_14);
		AssumeRoleWithWebIdentityResponse_set_AssumedRoleUser_m3816583747(L_14, L_17, /*hidden argument*/NULL);
		goto IL_0110;
	}

IL_0058:
	{
		XmlUnmarshallerContext_t1179575220 * L_18 = ___context0;
		int32_t L_19 = V_1;
		NullCheck(L_18);
		bool L_20 = UnmarshallerContext_TestExpression_m3665247446(L_18, _stringLiteral3912029664, L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_007e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(StringUnmarshaller_t3953260147_il2cpp_TypeInfo_var);
		StringUnmarshaller_t3953260147 * L_21 = StringUnmarshaller_get_Instance_m107503370(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_3 = L_21;
		AssumeRoleWithWebIdentityResponse_t3931705881 * L_22 = ___response1;
		StringUnmarshaller_t3953260147 * L_23 = V_3;
		XmlUnmarshallerContext_t1179575220 * L_24 = ___context0;
		NullCheck(L_23);
		String_t* L_25 = StringUnmarshaller_Unmarshall_m2571974896(L_23, L_24, /*hidden argument*/NULL);
		NullCheck(L_22);
		AssumeRoleWithWebIdentityResponse_set_Audience_m3186117859(L_22, L_25, /*hidden argument*/NULL);
		goto IL_0110;
	}

IL_007e:
	{
		XmlUnmarshallerContext_t1179575220 * L_26 = ___context0;
		int32_t L_27 = V_1;
		NullCheck(L_26);
		bool L_28 = UnmarshallerContext_TestExpression_m3665247446(L_26, _stringLiteral2015805848, L_27, /*hidden argument*/NULL);
		if (!L_28)
		{
			goto IL_00a3;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CredentialsUnmarshaller_t3599710454_il2cpp_TypeInfo_var);
		CredentialsUnmarshaller_t3599710454 * L_29 = CredentialsUnmarshaller_get_Instance_m3716340548(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_4 = L_29;
		AssumeRoleWithWebIdentityResponse_t3931705881 * L_30 = ___response1;
		CredentialsUnmarshaller_t3599710454 * L_31 = V_4;
		XmlUnmarshallerContext_t1179575220 * L_32 = ___context0;
		NullCheck(L_31);
		Credentials_t3554032640 * L_33 = CredentialsUnmarshaller_Unmarshall_m2423097439(L_31, L_32, /*hidden argument*/NULL);
		NullCheck(L_30);
		AssumeRoleWithWebIdentityResponse_set_Credentials_m3033710271(L_30, L_33, /*hidden argument*/NULL);
		goto IL_0110;
	}

IL_00a3:
	{
		XmlUnmarshallerContext_t1179575220 * L_34 = ___context0;
		int32_t L_35 = V_1;
		NullCheck(L_34);
		bool L_36 = UnmarshallerContext_TestExpression_m3665247446(L_34, _stringLiteral3022960573, L_35, /*hidden argument*/NULL);
		if (!L_36)
		{
			goto IL_00c8;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IntUnmarshaller_t2174001701_il2cpp_TypeInfo_var);
		IntUnmarshaller_t2174001701 * L_37 = IntUnmarshaller_get_Instance_m769761570(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_5 = L_37;
		AssumeRoleWithWebIdentityResponse_t3931705881 * L_38 = ___response1;
		IntUnmarshaller_t2174001701 * L_39 = V_5;
		XmlUnmarshallerContext_t1179575220 * L_40 = ___context0;
		NullCheck(L_39);
		int32_t L_41 = IntUnmarshaller_Unmarshall_m705606319(L_39, L_40, /*hidden argument*/NULL);
		NullCheck(L_38);
		AssumeRoleWithWebIdentityResponse_set_PackedPolicySize_m1522389737(L_38, L_41, /*hidden argument*/NULL);
		goto IL_0110;
	}

IL_00c8:
	{
		XmlUnmarshallerContext_t1179575220 * L_42 = ___context0;
		int32_t L_43 = V_1;
		NullCheck(L_42);
		bool L_44 = UnmarshallerContext_TestExpression_m3665247446(L_42, _stringLiteral1682529065, L_43, /*hidden argument*/NULL);
		if (!L_44)
		{
			goto IL_00ed;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(StringUnmarshaller_t3953260147_il2cpp_TypeInfo_var);
		StringUnmarshaller_t3953260147 * L_45 = StringUnmarshaller_get_Instance_m107503370(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_6 = L_45;
		AssumeRoleWithWebIdentityResponse_t3931705881 * L_46 = ___response1;
		StringUnmarshaller_t3953260147 * L_47 = V_6;
		XmlUnmarshallerContext_t1179575220 * L_48 = ___context0;
		NullCheck(L_47);
		String_t* L_49 = StringUnmarshaller_Unmarshall_m2571974896(L_47, L_48, /*hidden argument*/NULL);
		NullCheck(L_46);
		AssumeRoleWithWebIdentityResponse_set_Provider_m3771874134(L_46, L_49, /*hidden argument*/NULL);
		goto IL_0110;
	}

IL_00ed:
	{
		XmlUnmarshallerContext_t1179575220 * L_50 = ___context0;
		int32_t L_51 = V_1;
		NullCheck(L_50);
		bool L_52 = UnmarshallerContext_TestExpression_m3665247446(L_50, _stringLiteral856808875, L_51, /*hidden argument*/NULL);
		if (!L_52)
		{
			goto IL_0110;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(StringUnmarshaller_t3953260147_il2cpp_TypeInfo_var);
		StringUnmarshaller_t3953260147 * L_53 = StringUnmarshaller_get_Instance_m107503370(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_7 = L_53;
		AssumeRoleWithWebIdentityResponse_t3931705881 * L_54 = ___response1;
		StringUnmarshaller_t3953260147 * L_55 = V_7;
		XmlUnmarshallerContext_t1179575220 * L_56 = ___context0;
		NullCheck(L_55);
		String_t* L_57 = StringUnmarshaller_Unmarshall_m2571974896(L_55, L_56, /*hidden argument*/NULL);
		NullCheck(L_54);
		AssumeRoleWithWebIdentityResponse_set_SubjectFromWebIdentityToken_m2341987586(L_54, L_57, /*hidden argument*/NULL);
	}

IL_0110:
	{
		XmlUnmarshallerContext_t1179575220 * L_58 = ___context0;
		int32_t L_59 = V_0;
		NullCheck(L_58);
		bool L_60 = UnmarshallerContext_ReadAtDepth_m2943457426(L_58, L_59, /*hidden argument*/NULL);
		if (L_60)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}
}
// Amazon.Runtime.AmazonServiceException Amazon.SecurityToken.Model.Internal.MarshallTransformations.AssumeRoleWithWebIdentityResponseUnmarshaller::UnmarshallException(Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext,System.Exception,System.Net.HttpStatusCode)
extern Il2CppClass* ExpiredTokenException_t1058704965_il2cpp_TypeInfo_var;
extern Il2CppClass* IDPCommunicationErrorException_t2234231496_il2cpp_TypeInfo_var;
extern Il2CppClass* IDPRejectedClaimException_t367458406_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidIdentityTokenException_t1321611201_il2cpp_TypeInfo_var;
extern Il2CppClass* MalformedPolicyDocumentException_t157840309_il2cpp_TypeInfo_var;
extern Il2CppClass* PackedPolicyTooLargeException_t4099730702_il2cpp_TypeInfo_var;
extern Il2CppClass* RegionDisabledException_t1422077785_il2cpp_TypeInfo_var;
extern Il2CppClass* AmazonSecurityTokenServiceException_t3771342025_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3485245051;
extern Il2CppCodeGenString* _stringLiteral2637113941;
extern Il2CppCodeGenString* _stringLiteral1707893241;
extern Il2CppCodeGenString* _stringLiteral2760132984;
extern Il2CppCodeGenString* _stringLiteral994693084;
extern Il2CppCodeGenString* _stringLiteral2321210161;
extern Il2CppCodeGenString* _stringLiteral155974403;
extern const uint32_t AssumeRoleWithWebIdentityResponseUnmarshaller_UnmarshallException_m2512616822_MetadataUsageId;
extern "C"  AmazonServiceException_t3748559634 * AssumeRoleWithWebIdentityResponseUnmarshaller_UnmarshallException_m2512616822 (AssumeRoleWithWebIdentityResponseUnmarshaller_t3698885375 * __this, XmlUnmarshallerContext_t1179575220 * ___context0, Exception_t1927440687 * ___innerException1, int32_t ___statusCode2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AssumeRoleWithWebIdentityResponseUnmarshaller_UnmarshallException_m2512616822_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ErrorResponse_t3502566035 * V_0 = NULL;
	{
		ErrorResponseUnmarshaller_t3810099389 * L_0 = ErrorResponseUnmarshaller_GetInstance_m574966211(NULL /*static, unused*/, /*hidden argument*/NULL);
		XmlUnmarshallerContext_t1179575220 * L_1 = ___context0;
		NullCheck(L_0);
		ErrorResponse_t3502566035 * L_2 = ErrorResponseUnmarshaller_Unmarshall_m4167121930(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		ErrorResponse_t3502566035 * L_3 = V_0;
		NullCheck(L_3);
		String_t* L_4 = ErrorResponse_get_Code_m1007089050(L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0046;
		}
	}
	{
		ErrorResponse_t3502566035 * L_5 = V_0;
		NullCheck(L_5);
		String_t* L_6 = ErrorResponse_get_Code_m1007089050(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		bool L_7 = String_Equals_m2633592423(L_6, _stringLiteral3485245051, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0046;
		}
	}
	{
		ErrorResponse_t3502566035 * L_8 = V_0;
		NullCheck(L_8);
		String_t* L_9 = ErrorResponse_get_Message_m1145825032(L_8, /*hidden argument*/NULL);
		Exception_t1927440687 * L_10 = ___innerException1;
		ErrorResponse_t3502566035 * L_11 = V_0;
		NullCheck(L_11);
		int32_t L_12 = ErrorResponse_get_Type_m3388577699(L_11, /*hidden argument*/NULL);
		ErrorResponse_t3502566035 * L_13 = V_0;
		NullCheck(L_13);
		String_t* L_14 = ErrorResponse_get_Code_m1007089050(L_13, /*hidden argument*/NULL);
		ErrorResponse_t3502566035 * L_15 = V_0;
		NullCheck(L_15);
		String_t* L_16 = ErrorResponse_get_RequestId_m2536924555(L_15, /*hidden argument*/NULL);
		int32_t L_17 = ___statusCode2;
		ExpiredTokenException_t1058704965 * L_18 = (ExpiredTokenException_t1058704965 *)il2cpp_codegen_object_new(ExpiredTokenException_t1058704965_il2cpp_TypeInfo_var);
		ExpiredTokenException__ctor_m2499536043(L_18, L_9, L_10, L_12, L_14, L_16, L_17, /*hidden argument*/NULL);
		return L_18;
	}

IL_0046:
	{
		ErrorResponse_t3502566035 * L_19 = V_0;
		NullCheck(L_19);
		String_t* L_20 = ErrorResponse_get_Code_m1007089050(L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_0080;
		}
	}
	{
		ErrorResponse_t3502566035 * L_21 = V_0;
		NullCheck(L_21);
		String_t* L_22 = ErrorResponse_get_Code_m1007089050(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		bool L_23 = String_Equals_m2633592423(L_22, _stringLiteral2637113941, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_0080;
		}
	}
	{
		ErrorResponse_t3502566035 * L_24 = V_0;
		NullCheck(L_24);
		String_t* L_25 = ErrorResponse_get_Message_m1145825032(L_24, /*hidden argument*/NULL);
		Exception_t1927440687 * L_26 = ___innerException1;
		ErrorResponse_t3502566035 * L_27 = V_0;
		NullCheck(L_27);
		int32_t L_28 = ErrorResponse_get_Type_m3388577699(L_27, /*hidden argument*/NULL);
		ErrorResponse_t3502566035 * L_29 = V_0;
		NullCheck(L_29);
		String_t* L_30 = ErrorResponse_get_Code_m1007089050(L_29, /*hidden argument*/NULL);
		ErrorResponse_t3502566035 * L_31 = V_0;
		NullCheck(L_31);
		String_t* L_32 = ErrorResponse_get_RequestId_m2536924555(L_31, /*hidden argument*/NULL);
		int32_t L_33 = ___statusCode2;
		IDPCommunicationErrorException_t2234231496 * L_34 = (IDPCommunicationErrorException_t2234231496 *)il2cpp_codegen_object_new(IDPCommunicationErrorException_t2234231496_il2cpp_TypeInfo_var);
		IDPCommunicationErrorException__ctor_m1130552656(L_34, L_25, L_26, L_28, L_30, L_32, L_33, /*hidden argument*/NULL);
		return L_34;
	}

IL_0080:
	{
		ErrorResponse_t3502566035 * L_35 = V_0;
		NullCheck(L_35);
		String_t* L_36 = ErrorResponse_get_Code_m1007089050(L_35, /*hidden argument*/NULL);
		if (!L_36)
		{
			goto IL_00ba;
		}
	}
	{
		ErrorResponse_t3502566035 * L_37 = V_0;
		NullCheck(L_37);
		String_t* L_38 = ErrorResponse_get_Code_m1007089050(L_37, /*hidden argument*/NULL);
		NullCheck(L_38);
		bool L_39 = String_Equals_m2633592423(L_38, _stringLiteral1707893241, /*hidden argument*/NULL);
		if (!L_39)
		{
			goto IL_00ba;
		}
	}
	{
		ErrorResponse_t3502566035 * L_40 = V_0;
		NullCheck(L_40);
		String_t* L_41 = ErrorResponse_get_Message_m1145825032(L_40, /*hidden argument*/NULL);
		Exception_t1927440687 * L_42 = ___innerException1;
		ErrorResponse_t3502566035 * L_43 = V_0;
		NullCheck(L_43);
		int32_t L_44 = ErrorResponse_get_Type_m3388577699(L_43, /*hidden argument*/NULL);
		ErrorResponse_t3502566035 * L_45 = V_0;
		NullCheck(L_45);
		String_t* L_46 = ErrorResponse_get_Code_m1007089050(L_45, /*hidden argument*/NULL);
		ErrorResponse_t3502566035 * L_47 = V_0;
		NullCheck(L_47);
		String_t* L_48 = ErrorResponse_get_RequestId_m2536924555(L_47, /*hidden argument*/NULL);
		int32_t L_49 = ___statusCode2;
		IDPRejectedClaimException_t367458406 * L_50 = (IDPRejectedClaimException_t367458406 *)il2cpp_codegen_object_new(IDPRejectedClaimException_t367458406_il2cpp_TypeInfo_var);
		IDPRejectedClaimException__ctor_m1994288158(L_50, L_41, L_42, L_44, L_46, L_48, L_49, /*hidden argument*/NULL);
		return L_50;
	}

IL_00ba:
	{
		ErrorResponse_t3502566035 * L_51 = V_0;
		NullCheck(L_51);
		String_t* L_52 = ErrorResponse_get_Code_m1007089050(L_51, /*hidden argument*/NULL);
		if (!L_52)
		{
			goto IL_00f4;
		}
	}
	{
		ErrorResponse_t3502566035 * L_53 = V_0;
		NullCheck(L_53);
		String_t* L_54 = ErrorResponse_get_Code_m1007089050(L_53, /*hidden argument*/NULL);
		NullCheck(L_54);
		bool L_55 = String_Equals_m2633592423(L_54, _stringLiteral2760132984, /*hidden argument*/NULL);
		if (!L_55)
		{
			goto IL_00f4;
		}
	}
	{
		ErrorResponse_t3502566035 * L_56 = V_0;
		NullCheck(L_56);
		String_t* L_57 = ErrorResponse_get_Message_m1145825032(L_56, /*hidden argument*/NULL);
		Exception_t1927440687 * L_58 = ___innerException1;
		ErrorResponse_t3502566035 * L_59 = V_0;
		NullCheck(L_59);
		int32_t L_60 = ErrorResponse_get_Type_m3388577699(L_59, /*hidden argument*/NULL);
		ErrorResponse_t3502566035 * L_61 = V_0;
		NullCheck(L_61);
		String_t* L_62 = ErrorResponse_get_Code_m1007089050(L_61, /*hidden argument*/NULL);
		ErrorResponse_t3502566035 * L_63 = V_0;
		NullCheck(L_63);
		String_t* L_64 = ErrorResponse_get_RequestId_m2536924555(L_63, /*hidden argument*/NULL);
		int32_t L_65 = ___statusCode2;
		InvalidIdentityTokenException_t1321611201 * L_66 = (InvalidIdentityTokenException_t1321611201 *)il2cpp_codegen_object_new(InvalidIdentityTokenException_t1321611201_il2cpp_TypeInfo_var);
		InvalidIdentityTokenException__ctor_m2120287835(L_66, L_57, L_58, L_60, L_62, L_64, L_65, /*hidden argument*/NULL);
		return L_66;
	}

IL_00f4:
	{
		ErrorResponse_t3502566035 * L_67 = V_0;
		NullCheck(L_67);
		String_t* L_68 = ErrorResponse_get_Code_m1007089050(L_67, /*hidden argument*/NULL);
		if (!L_68)
		{
			goto IL_012e;
		}
	}
	{
		ErrorResponse_t3502566035 * L_69 = V_0;
		NullCheck(L_69);
		String_t* L_70 = ErrorResponse_get_Code_m1007089050(L_69, /*hidden argument*/NULL);
		NullCheck(L_70);
		bool L_71 = String_Equals_m2633592423(L_70, _stringLiteral994693084, /*hidden argument*/NULL);
		if (!L_71)
		{
			goto IL_012e;
		}
	}
	{
		ErrorResponse_t3502566035 * L_72 = V_0;
		NullCheck(L_72);
		String_t* L_73 = ErrorResponse_get_Message_m1145825032(L_72, /*hidden argument*/NULL);
		Exception_t1927440687 * L_74 = ___innerException1;
		ErrorResponse_t3502566035 * L_75 = V_0;
		NullCheck(L_75);
		int32_t L_76 = ErrorResponse_get_Type_m3388577699(L_75, /*hidden argument*/NULL);
		ErrorResponse_t3502566035 * L_77 = V_0;
		NullCheck(L_77);
		String_t* L_78 = ErrorResponse_get_Code_m1007089050(L_77, /*hidden argument*/NULL);
		ErrorResponse_t3502566035 * L_79 = V_0;
		NullCheck(L_79);
		String_t* L_80 = ErrorResponse_get_RequestId_m2536924555(L_79, /*hidden argument*/NULL);
		int32_t L_81 = ___statusCode2;
		MalformedPolicyDocumentException_t157840309 * L_82 = (MalformedPolicyDocumentException_t157840309 *)il2cpp_codegen_object_new(MalformedPolicyDocumentException_t157840309_il2cpp_TypeInfo_var);
		MalformedPolicyDocumentException__ctor_m3284050635(L_82, L_73, L_74, L_76, L_78, L_80, L_81, /*hidden argument*/NULL);
		return L_82;
	}

IL_012e:
	{
		ErrorResponse_t3502566035 * L_83 = V_0;
		NullCheck(L_83);
		String_t* L_84 = ErrorResponse_get_Code_m1007089050(L_83, /*hidden argument*/NULL);
		if (!L_84)
		{
			goto IL_0168;
		}
	}
	{
		ErrorResponse_t3502566035 * L_85 = V_0;
		NullCheck(L_85);
		String_t* L_86 = ErrorResponse_get_Code_m1007089050(L_85, /*hidden argument*/NULL);
		NullCheck(L_86);
		bool L_87 = String_Equals_m2633592423(L_86, _stringLiteral2321210161, /*hidden argument*/NULL);
		if (!L_87)
		{
			goto IL_0168;
		}
	}
	{
		ErrorResponse_t3502566035 * L_88 = V_0;
		NullCheck(L_88);
		String_t* L_89 = ErrorResponse_get_Message_m1145825032(L_88, /*hidden argument*/NULL);
		Exception_t1927440687 * L_90 = ___innerException1;
		ErrorResponse_t3502566035 * L_91 = V_0;
		NullCheck(L_91);
		int32_t L_92 = ErrorResponse_get_Type_m3388577699(L_91, /*hidden argument*/NULL);
		ErrorResponse_t3502566035 * L_93 = V_0;
		NullCheck(L_93);
		String_t* L_94 = ErrorResponse_get_Code_m1007089050(L_93, /*hidden argument*/NULL);
		ErrorResponse_t3502566035 * L_95 = V_0;
		NullCheck(L_95);
		String_t* L_96 = ErrorResponse_get_RequestId_m2536924555(L_95, /*hidden argument*/NULL);
		int32_t L_97 = ___statusCode2;
		PackedPolicyTooLargeException_t4099730702 * L_98 = (PackedPolicyTooLargeException_t4099730702 *)il2cpp_codegen_object_new(PackedPolicyTooLargeException_t4099730702_il2cpp_TypeInfo_var);
		PackedPolicyTooLargeException__ctor_m328983654(L_98, L_89, L_90, L_92, L_94, L_96, L_97, /*hidden argument*/NULL);
		return L_98;
	}

IL_0168:
	{
		ErrorResponse_t3502566035 * L_99 = V_0;
		NullCheck(L_99);
		String_t* L_100 = ErrorResponse_get_Code_m1007089050(L_99, /*hidden argument*/NULL);
		if (!L_100)
		{
			goto IL_01a2;
		}
	}
	{
		ErrorResponse_t3502566035 * L_101 = V_0;
		NullCheck(L_101);
		String_t* L_102 = ErrorResponse_get_Code_m1007089050(L_101, /*hidden argument*/NULL);
		NullCheck(L_102);
		bool L_103 = String_Equals_m2633592423(L_102, _stringLiteral155974403, /*hidden argument*/NULL);
		if (!L_103)
		{
			goto IL_01a2;
		}
	}
	{
		ErrorResponse_t3502566035 * L_104 = V_0;
		NullCheck(L_104);
		String_t* L_105 = ErrorResponse_get_Message_m1145825032(L_104, /*hidden argument*/NULL);
		Exception_t1927440687 * L_106 = ___innerException1;
		ErrorResponse_t3502566035 * L_107 = V_0;
		NullCheck(L_107);
		int32_t L_108 = ErrorResponse_get_Type_m3388577699(L_107, /*hidden argument*/NULL);
		ErrorResponse_t3502566035 * L_109 = V_0;
		NullCheck(L_109);
		String_t* L_110 = ErrorResponse_get_Code_m1007089050(L_109, /*hidden argument*/NULL);
		ErrorResponse_t3502566035 * L_111 = V_0;
		NullCheck(L_111);
		String_t* L_112 = ErrorResponse_get_RequestId_m2536924555(L_111, /*hidden argument*/NULL);
		int32_t L_113 = ___statusCode2;
		RegionDisabledException_t1422077785 * L_114 = (RegionDisabledException_t1422077785 *)il2cpp_codegen_object_new(RegionDisabledException_t1422077785_il2cpp_TypeInfo_var);
		RegionDisabledException__ctor_m2507891627(L_114, L_105, L_106, L_108, L_110, L_112, L_113, /*hidden argument*/NULL);
		return L_114;
	}

IL_01a2:
	{
		ErrorResponse_t3502566035 * L_115 = V_0;
		NullCheck(L_115);
		String_t* L_116 = ErrorResponse_get_Message_m1145825032(L_115, /*hidden argument*/NULL);
		Exception_t1927440687 * L_117 = ___innerException1;
		ErrorResponse_t3502566035 * L_118 = V_0;
		NullCheck(L_118);
		int32_t L_119 = ErrorResponse_get_Type_m3388577699(L_118, /*hidden argument*/NULL);
		ErrorResponse_t3502566035 * L_120 = V_0;
		NullCheck(L_120);
		String_t* L_121 = ErrorResponse_get_Code_m1007089050(L_120, /*hidden argument*/NULL);
		ErrorResponse_t3502566035 * L_122 = V_0;
		NullCheck(L_122);
		String_t* L_123 = ErrorResponse_get_RequestId_m2536924555(L_122, /*hidden argument*/NULL);
		int32_t L_124 = ___statusCode2;
		AmazonSecurityTokenServiceException_t3771342025 * L_125 = (AmazonSecurityTokenServiceException_t3771342025 *)il2cpp_codegen_object_new(AmazonSecurityTokenServiceException_t3771342025_il2cpp_TypeInfo_var);
		AmazonSecurityTokenServiceException__ctor_m4274350954(L_125, L_116, L_117, L_119, L_121, L_123, L_124, /*hidden argument*/NULL);
		return L_125;
	}
}
// Amazon.SecurityToken.Model.Internal.MarshallTransformations.AssumeRoleWithWebIdentityResponseUnmarshaller Amazon.SecurityToken.Model.Internal.MarshallTransformations.AssumeRoleWithWebIdentityResponseUnmarshaller::get_Instance()
extern Il2CppClass* AssumeRoleWithWebIdentityResponseUnmarshaller_t3698885375_il2cpp_TypeInfo_var;
extern const uint32_t AssumeRoleWithWebIdentityResponseUnmarshaller_get_Instance_m3286108612_MetadataUsageId;
extern "C"  AssumeRoleWithWebIdentityResponseUnmarshaller_t3698885375 * AssumeRoleWithWebIdentityResponseUnmarshaller_get_Instance_m3286108612 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AssumeRoleWithWebIdentityResponseUnmarshaller_get_Instance_m3286108612_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(AssumeRoleWithWebIdentityResponseUnmarshaller_t3698885375_il2cpp_TypeInfo_var);
		AssumeRoleWithWebIdentityResponseUnmarshaller_t3698885375 * L_0 = ((AssumeRoleWithWebIdentityResponseUnmarshaller_t3698885375_StaticFields*)AssumeRoleWithWebIdentityResponseUnmarshaller_t3698885375_il2cpp_TypeInfo_var->static_fields)->get__instance_0();
		return L_0;
	}
}
// System.Void Amazon.SecurityToken.Model.Internal.MarshallTransformations.AssumeRoleWithWebIdentityResponseUnmarshaller::.ctor()
extern "C"  void AssumeRoleWithWebIdentityResponseUnmarshaller__ctor_m3146897921 (AssumeRoleWithWebIdentityResponseUnmarshaller_t3698885375 * __this, const MethodInfo* method)
{
	{
		XmlResponseUnmarshaller__ctor_m4168779604(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Amazon.SecurityToken.Model.Internal.MarshallTransformations.AssumeRoleWithWebIdentityResponseUnmarshaller::.cctor()
extern Il2CppClass* AssumeRoleWithWebIdentityResponseUnmarshaller_t3698885375_il2cpp_TypeInfo_var;
extern const uint32_t AssumeRoleWithWebIdentityResponseUnmarshaller__cctor_m1770634210_MetadataUsageId;
extern "C"  void AssumeRoleWithWebIdentityResponseUnmarshaller__cctor_m1770634210 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AssumeRoleWithWebIdentityResponseUnmarshaller__cctor_m1770634210_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AssumeRoleWithWebIdentityResponseUnmarshaller_t3698885375 * L_0 = (AssumeRoleWithWebIdentityResponseUnmarshaller_t3698885375 *)il2cpp_codegen_object_new(AssumeRoleWithWebIdentityResponseUnmarshaller_t3698885375_il2cpp_TypeInfo_var);
		AssumeRoleWithWebIdentityResponseUnmarshaller__ctor_m3146897921(L_0, /*hidden argument*/NULL);
		((AssumeRoleWithWebIdentityResponseUnmarshaller_t3698885375_StaticFields*)AssumeRoleWithWebIdentityResponseUnmarshaller_t3698885375_il2cpp_TypeInfo_var->static_fields)->set__instance_0(L_0);
		return;
	}
}
// Amazon.SecurityToken.Model.Credentials Amazon.SecurityToken.Model.Internal.MarshallTransformations.CredentialsUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext)
extern Il2CppClass* Credentials_t3554032640_il2cpp_TypeInfo_var;
extern Il2CppClass* StringUnmarshaller_t3953260147_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTimeUnmarshaller_t1723598451_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3375931278;
extern Il2CppCodeGenString* _stringLiteral3212374675;
extern Il2CppCodeGenString* _stringLiteral3839334689;
extern Il2CppCodeGenString* _stringLiteral790267625;
extern const uint32_t CredentialsUnmarshaller_Unmarshall_m2423097439_MetadataUsageId;
extern "C"  Credentials_t3554032640 * CredentialsUnmarshaller_Unmarshall_m2423097439 (CredentialsUnmarshaller_t3599710454 * __this, XmlUnmarshallerContext_t1179575220 * ___context0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CredentialsUnmarshaller_Unmarshall_m2423097439_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Credentials_t3554032640 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	StringUnmarshaller_t3953260147 * V_3 = NULL;
	DateTimeUnmarshaller_t1723598451 * V_4 = NULL;
	StringUnmarshaller_t3953260147 * V_5 = NULL;
	StringUnmarshaller_t3953260147 * V_6 = NULL;
	{
		Credentials_t3554032640 * L_0 = (Credentials_t3554032640 *)il2cpp_codegen_object_new(Credentials_t3554032640_il2cpp_TypeInfo_var);
		Credentials__ctor_m280882687(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		XmlUnmarshallerContext_t1179575220 * L_1 = ___context0;
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 Amazon.Runtime.Internal.Transform.UnmarshallerContext::get_CurrentDepth() */, L_1);
		V_1 = L_2;
		int32_t L_3 = V_1;
		V_2 = ((int32_t)((int32_t)L_3+(int32_t)1));
		XmlUnmarshallerContext_t1179575220 * L_4 = ___context0;
		NullCheck(L_4);
		bool L_5 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean Amazon.Runtime.Internal.Transform.UnmarshallerContext::get_IsStartOfDocument() */, L_4);
		if (!L_5)
		{
			goto IL_00e0;
		}
	}
	{
		int32_t L_6 = V_2;
		V_2 = ((int32_t)((int32_t)L_6+(int32_t)2));
		goto IL_00e0;
	}

IL_0025:
	{
		XmlUnmarshallerContext_t1179575220 * L_7 = ___context0;
		NullCheck(L_7);
		bool L_8 = VirtFuncInvoker0< bool >::Invoke(9 /* System.Boolean Amazon.Runtime.Internal.Transform.UnmarshallerContext::get_IsStartElement() */, L_7);
		if (L_8)
		{
			goto IL_0038;
		}
	}
	{
		XmlUnmarshallerContext_t1179575220 * L_9 = ___context0;
		NullCheck(L_9);
		bool L_10 = XmlUnmarshallerContext_get_IsAttribute_m1726264931(L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_00cd;
		}
	}

IL_0038:
	{
		XmlUnmarshallerContext_t1179575220 * L_11 = ___context0;
		int32_t L_12 = V_2;
		NullCheck(L_11);
		bool L_13 = UnmarshallerContext_TestExpression_m3665247446(L_11, _stringLiteral3375931278, L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_005e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(StringUnmarshaller_t3953260147_il2cpp_TypeInfo_var);
		StringUnmarshaller_t3953260147 * L_14 = StringUnmarshaller_get_Instance_m107503370(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_3 = L_14;
		Credentials_t3554032640 * L_15 = V_0;
		StringUnmarshaller_t3953260147 * L_16 = V_3;
		XmlUnmarshallerContext_t1179575220 * L_17 = ___context0;
		NullCheck(L_16);
		String_t* L_18 = StringUnmarshaller_Unmarshall_m2571974896(L_16, L_17, /*hidden argument*/NULL);
		NullCheck(L_15);
		Credentials_set_AccessKeyId_m658001590(L_15, L_18, /*hidden argument*/NULL);
		goto IL_00e0;
	}

IL_005e:
	{
		XmlUnmarshallerContext_t1179575220 * L_19 = ___context0;
		int32_t L_20 = V_2;
		NullCheck(L_19);
		bool L_21 = UnmarshallerContext_TestExpression_m3665247446(L_19, _stringLiteral3212374675, L_20, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_0083;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DateTimeUnmarshaller_t1723598451_il2cpp_TypeInfo_var);
		DateTimeUnmarshaller_t1723598451 * L_22 = DateTimeUnmarshaller_get_Instance_m2045152802(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_4 = L_22;
		Credentials_t3554032640 * L_23 = V_0;
		DateTimeUnmarshaller_t1723598451 * L_24 = V_4;
		XmlUnmarshallerContext_t1179575220 * L_25 = ___context0;
		NullCheck(L_24);
		DateTime_t693205669  L_26 = DateTimeUnmarshaller_Unmarshall_m1665504504(L_24, L_25, /*hidden argument*/NULL);
		NullCheck(L_23);
		Credentials_set_Expiration_m4276484463(L_23, L_26, /*hidden argument*/NULL);
		goto IL_00e0;
	}

IL_0083:
	{
		XmlUnmarshallerContext_t1179575220 * L_27 = ___context0;
		int32_t L_28 = V_2;
		NullCheck(L_27);
		bool L_29 = UnmarshallerContext_TestExpression_m3665247446(L_27, _stringLiteral3839334689, L_28, /*hidden argument*/NULL);
		if (!L_29)
		{
			goto IL_00a8;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(StringUnmarshaller_t3953260147_il2cpp_TypeInfo_var);
		StringUnmarshaller_t3953260147 * L_30 = StringUnmarshaller_get_Instance_m107503370(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_5 = L_30;
		Credentials_t3554032640 * L_31 = V_0;
		StringUnmarshaller_t3953260147 * L_32 = V_5;
		XmlUnmarshallerContext_t1179575220 * L_33 = ___context0;
		NullCheck(L_32);
		String_t* L_34 = StringUnmarshaller_Unmarshall_m2571974896(L_32, L_33, /*hidden argument*/NULL);
		NullCheck(L_31);
		Credentials_set_SecretAccessKey_m1846861829(L_31, L_34, /*hidden argument*/NULL);
		goto IL_00e0;
	}

IL_00a8:
	{
		XmlUnmarshallerContext_t1179575220 * L_35 = ___context0;
		int32_t L_36 = V_2;
		NullCheck(L_35);
		bool L_37 = UnmarshallerContext_TestExpression_m3665247446(L_35, _stringLiteral790267625, L_36, /*hidden argument*/NULL);
		if (!L_37)
		{
			goto IL_00e0;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(StringUnmarshaller_t3953260147_il2cpp_TypeInfo_var);
		StringUnmarshaller_t3953260147 * L_38 = StringUnmarshaller_get_Instance_m107503370(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_6 = L_38;
		Credentials_t3554032640 * L_39 = V_0;
		StringUnmarshaller_t3953260147 * L_40 = V_6;
		XmlUnmarshallerContext_t1179575220 * L_41 = ___context0;
		NullCheck(L_40);
		String_t* L_42 = StringUnmarshaller_Unmarshall_m2571974896(L_40, L_41, /*hidden argument*/NULL);
		NullCheck(L_39);
		Credentials_set_SessionToken_m37439381(L_39, L_42, /*hidden argument*/NULL);
		goto IL_00e0;
	}

IL_00cd:
	{
		XmlUnmarshallerContext_t1179575220 * L_43 = ___context0;
		NullCheck(L_43);
		bool L_44 = VirtFuncInvoker0< bool >::Invoke(10 /* System.Boolean Amazon.Runtime.Internal.Transform.UnmarshallerContext::get_IsEndElement() */, L_43);
		if (!L_44)
		{
			goto IL_00e0;
		}
	}
	{
		XmlUnmarshallerContext_t1179575220 * L_45 = ___context0;
		NullCheck(L_45);
		int32_t L_46 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 Amazon.Runtime.Internal.Transform.UnmarshallerContext::get_CurrentDepth() */, L_45);
		int32_t L_47 = V_1;
		if ((((int32_t)L_46) >= ((int32_t)L_47)))
		{
			goto IL_00e0;
		}
	}
	{
		Credentials_t3554032640 * L_48 = V_0;
		return L_48;
	}

IL_00e0:
	{
		XmlUnmarshallerContext_t1179575220 * L_49 = ___context0;
		int32_t L_50 = V_1;
		NullCheck(L_49);
		bool L_51 = UnmarshallerContext_ReadAtDepth_m2943457426(L_49, L_50, /*hidden argument*/NULL);
		if (L_51)
		{
			goto IL_0025;
		}
	}
	{
		Credentials_t3554032640 * L_52 = V_0;
		return L_52;
	}
}
// Amazon.SecurityToken.Model.Credentials Amazon.SecurityToken.Model.Internal.MarshallTransformations.CredentialsUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern "C"  Credentials_t3554032640 * CredentialsUnmarshaller_Unmarshall_m4114969110 (CredentialsUnmarshaller_t3599710454 * __this, JsonUnmarshallerContext_t456235889 * ___context0, const MethodInfo* method)
{
	{
		return (Credentials_t3554032640 *)NULL;
	}
}
// Amazon.SecurityToken.Model.Internal.MarshallTransformations.CredentialsUnmarshaller Amazon.SecurityToken.Model.Internal.MarshallTransformations.CredentialsUnmarshaller::get_Instance()
extern Il2CppClass* CredentialsUnmarshaller_t3599710454_il2cpp_TypeInfo_var;
extern const uint32_t CredentialsUnmarshaller_get_Instance_m3716340548_MetadataUsageId;
extern "C"  CredentialsUnmarshaller_t3599710454 * CredentialsUnmarshaller_get_Instance_m3716340548 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CredentialsUnmarshaller_get_Instance_m3716340548_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CredentialsUnmarshaller_t3599710454_il2cpp_TypeInfo_var);
		CredentialsUnmarshaller_t3599710454 * L_0 = ((CredentialsUnmarshaller_t3599710454_StaticFields*)CredentialsUnmarshaller_t3599710454_il2cpp_TypeInfo_var->static_fields)->get__instance_0();
		return L_0;
	}
}
// System.Void Amazon.SecurityToken.Model.Internal.MarshallTransformations.CredentialsUnmarshaller::.ctor()
extern "C"  void CredentialsUnmarshaller__ctor_m2680197702 (CredentialsUnmarshaller_t3599710454 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Amazon.SecurityToken.Model.Internal.MarshallTransformations.CredentialsUnmarshaller::.cctor()
extern Il2CppClass* CredentialsUnmarshaller_t3599710454_il2cpp_TypeInfo_var;
extern const uint32_t CredentialsUnmarshaller__cctor_m3449932329_MetadataUsageId;
extern "C"  void CredentialsUnmarshaller__cctor_m3449932329 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CredentialsUnmarshaller__cctor_m3449932329_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		CredentialsUnmarshaller_t3599710454 * L_0 = (CredentialsUnmarshaller_t3599710454 *)il2cpp_codegen_object_new(CredentialsUnmarshaller_t3599710454_il2cpp_TypeInfo_var);
		CredentialsUnmarshaller__ctor_m2680197702(L_0, /*hidden argument*/NULL);
		((CredentialsUnmarshaller_t3599710454_StaticFields*)CredentialsUnmarshaller_t3599710454_il2cpp_TypeInfo_var->static_fields)->set__instance_0(L_0);
		return;
	}
}
// System.Void Amazon.SecurityToken.Model.InvalidIdentityTokenException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
extern "C"  void InvalidIdentityTokenException__ctor_m2120287835 (InvalidIdentityTokenException_t1321611201 * __this, String_t* ___message0, Exception_t1927440687 * ___innerException1, int32_t ___errorType2, String_t* ___errorCode3, String_t* ___requestId4, int32_t ___statusCode5, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		Exception_t1927440687 * L_1 = ___innerException1;
		int32_t L_2 = ___errorType2;
		String_t* L_3 = ___errorCode3;
		String_t* L_4 = ___requestId4;
		int32_t L_5 = ___statusCode5;
		AmazonSecurityTokenServiceException__ctor_m4274350954(__this, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Amazon.SecurityToken.Model.InvalidIdentityTokenException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void InvalidIdentityTokenException__ctor_m3901955363 (InvalidIdentityTokenException_t1321611201 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method)
{
	{
		SerializationInfo_t228987430 * L_0 = ___info0;
		StreamingContext_t1417235061  L_1 = ___context1;
		AmazonSecurityTokenServiceException__ctor_m2614436174(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Amazon.SecurityToken.Model.MalformedPolicyDocumentException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
extern "C"  void MalformedPolicyDocumentException__ctor_m3284050635 (MalformedPolicyDocumentException_t157840309 * __this, String_t* ___message0, Exception_t1927440687 * ___innerException1, int32_t ___errorType2, String_t* ___errorCode3, String_t* ___requestId4, int32_t ___statusCode5, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		Exception_t1927440687 * L_1 = ___innerException1;
		int32_t L_2 = ___errorType2;
		String_t* L_3 = ___errorCode3;
		String_t* L_4 = ___requestId4;
		int32_t L_5 = ___statusCode5;
		AmazonSecurityTokenServiceException__ctor_m4274350954(__this, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Amazon.SecurityToken.Model.MalformedPolicyDocumentException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void MalformedPolicyDocumentException__ctor_m1086358291 (MalformedPolicyDocumentException_t157840309 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method)
{
	{
		SerializationInfo_t228987430 * L_0 = ___info0;
		StreamingContext_t1417235061  L_1 = ___context1;
		AmazonSecurityTokenServiceException__ctor_m2614436174(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Amazon.SecurityToken.Model.PackedPolicyTooLargeException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
extern "C"  void PackedPolicyTooLargeException__ctor_m328983654 (PackedPolicyTooLargeException_t4099730702 * __this, String_t* ___message0, Exception_t1927440687 * ___innerException1, int32_t ___errorType2, String_t* ___errorCode3, String_t* ___requestId4, int32_t ___statusCode5, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		Exception_t1927440687 * L_1 = ___innerException1;
		int32_t L_2 = ___errorType2;
		String_t* L_3 = ___errorCode3;
		String_t* L_4 = ___requestId4;
		int32_t L_5 = ___statusCode5;
		AmazonSecurityTokenServiceException__ctor_m4274350954(__this, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Amazon.SecurityToken.Model.PackedPolicyTooLargeException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void PackedPolicyTooLargeException__ctor_m942336894 (PackedPolicyTooLargeException_t4099730702 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method)
{
	{
		SerializationInfo_t228987430 * L_0 = ___info0;
		StreamingContext_t1417235061  L_1 = ___context1;
		AmazonSecurityTokenServiceException__ctor_m2614436174(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Amazon.SecurityToken.Model.RegionDisabledException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
extern "C"  void RegionDisabledException__ctor_m2507891627 (RegionDisabledException_t1422077785 * __this, String_t* ___message0, Exception_t1927440687 * ___innerException1, int32_t ___errorType2, String_t* ___errorCode3, String_t* ___requestId4, int32_t ___statusCode5, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		Exception_t1927440687 * L_1 = ___innerException1;
		int32_t L_2 = ___errorType2;
		String_t* L_3 = ___errorCode3;
		String_t* L_4 = ___requestId4;
		int32_t L_5 = ___statusCode5;
		AmazonSecurityTokenServiceException__ctor_m4274350954(__this, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Amazon.SecurityToken.Model.RegionDisabledException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void RegionDisabledException__ctor_m1109066131 (RegionDisabledException_t1422077785 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method)
{
	{
		SerializationInfo_t228987430 * L_0 = ___info0;
		StreamingContext_t1417235061  L_1 = ___context1;
		AmazonSecurityTokenServiceException__ctor_m2614436174(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
