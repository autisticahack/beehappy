﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.Runtime.Internal.DefaultRequest/<>c
struct U3CU3Ec_t12014549;
// System.Func`2<System.IO.Stream,System.Boolean>
struct Func_2_t4109217329;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.DefaultRequest/<>c
struct  U3CU3Ec_t12014549  : public Il2CppObject
{
public:

public:
};

struct U3CU3Ec_t12014549_StaticFields
{
public:
	// Amazon.Runtime.Internal.DefaultRequest/<>c Amazon.Runtime.Internal.DefaultRequest/<>c::<>9
	U3CU3Ec_t12014549 * ___U3CU3E9_0;
	// System.Func`2<System.IO.Stream,System.Boolean> Amazon.Runtime.Internal.DefaultRequest/<>c::<>9__57_0
	Func_2_t4109217329 * ___U3CU3E9__57_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t12014549_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t12014549 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t12014549 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t12014549 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9_0, value);
	}

	inline static int32_t get_offset_of_U3CU3E9__57_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t12014549_StaticFields, ___U3CU3E9__57_0_1)); }
	inline Func_2_t4109217329 * get_U3CU3E9__57_0_1() const { return ___U3CU3E9__57_0_1; }
	inline Func_2_t4109217329 ** get_address_of_U3CU3E9__57_0_1() { return &___U3CU3E9__57_0_1; }
	inline void set_U3CU3E9__57_0_1(Func_2_t4109217329 * value)
	{
		___U3CU3E9__57_0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9__57_0_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
