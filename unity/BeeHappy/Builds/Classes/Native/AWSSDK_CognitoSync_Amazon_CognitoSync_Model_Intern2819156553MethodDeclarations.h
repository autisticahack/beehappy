﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.CognitoSync.Model.Internal.MarshallTransformations.ListRecordsResponseUnmarshaller
struct ListRecordsResponseUnmarshaller_t2819156553;
// Amazon.Runtime.AmazonWebServiceResponse
struct AmazonWebServiceResponse_t529043356;
// Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext
struct JsonUnmarshallerContext_t456235889;
// Amazon.Runtime.AmazonServiceException
struct AmazonServiceException_t3748559634;
// System.Exception
struct Exception_t1927440687;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_JsonU456235889.h"
#include "mscorlib_System_Exception1927440687.h"
#include "System_System_Net_HttpStatusCode1898409641.h"

// Amazon.Runtime.AmazonWebServiceResponse Amazon.CognitoSync.Model.Internal.MarshallTransformations.ListRecordsResponseUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern "C"  AmazonWebServiceResponse_t529043356 * ListRecordsResponseUnmarshaller_Unmarshall_m3856700437 (ListRecordsResponseUnmarshaller_t2819156553 * __this, JsonUnmarshallerContext_t456235889 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.AmazonServiceException Amazon.CognitoSync.Model.Internal.MarshallTransformations.ListRecordsResponseUnmarshaller::UnmarshallException(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,System.Exception,System.Net.HttpStatusCode)
extern "C"  AmazonServiceException_t3748559634 * ListRecordsResponseUnmarshaller_UnmarshallException_m2922459625 (ListRecordsResponseUnmarshaller_t2819156553 * __this, JsonUnmarshallerContext_t456235889 * ___context0, Exception_t1927440687 * ___innerException1, int32_t ___statusCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.CognitoSync.Model.Internal.MarshallTransformations.ListRecordsResponseUnmarshaller Amazon.CognitoSync.Model.Internal.MarshallTransformations.ListRecordsResponseUnmarshaller::get_Instance()
extern "C"  ListRecordsResponseUnmarshaller_t2819156553 * ListRecordsResponseUnmarshaller_get_Instance_m4217795707 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Model.Internal.MarshallTransformations.ListRecordsResponseUnmarshaller::.ctor()
extern "C"  void ListRecordsResponseUnmarshaller__ctor_m2594938889 (ListRecordsResponseUnmarshaller_t2819156553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Model.Internal.MarshallTransformations.ListRecordsResponseUnmarshaller::.cctor()
extern "C"  void ListRecordsResponseUnmarshaller__cctor_m3592642120 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
