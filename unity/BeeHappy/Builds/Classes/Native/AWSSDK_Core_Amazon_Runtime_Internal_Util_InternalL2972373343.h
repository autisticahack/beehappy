﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Type
struct Type_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Util.InternalLogger
struct  InternalLogger_t2972373343  : public Il2CppObject
{
public:
	// System.Type Amazon.Runtime.Internal.Util.InternalLogger::<DeclaringType>k__BackingField
	Type_t * ___U3CDeclaringTypeU3Ek__BackingField_0;
	// System.Boolean Amazon.Runtime.Internal.Util.InternalLogger::<IsEnabled>k__BackingField
	bool ___U3CIsEnabledU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CDeclaringTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(InternalLogger_t2972373343, ___U3CDeclaringTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CDeclaringTypeU3Ek__BackingField_0() const { return ___U3CDeclaringTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CDeclaringTypeU3Ek__BackingField_0() { return &___U3CDeclaringTypeU3Ek__BackingField_0; }
	inline void set_U3CDeclaringTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CDeclaringTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CDeclaringTypeU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CIsEnabledU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(InternalLogger_t2972373343, ___U3CIsEnabledU3Ek__BackingField_1)); }
	inline bool get_U3CIsEnabledU3Ek__BackingField_1() const { return ___U3CIsEnabledU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CIsEnabledU3Ek__BackingField_1() { return &___U3CIsEnabledU3Ek__BackingField_1; }
	inline void set_U3CIsEnabledU3Ek__BackingField_1(bool value)
	{
		___U3CIsEnabledU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
