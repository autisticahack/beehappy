﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Util.AWSSDKUtils/<>c__DisplayClass37_0`1<System.Object>
struct U3CU3Ec__DisplayClass37_0_1_t4001026854;

#include "codegen/il2cpp-codegen.h"

// System.Void Amazon.Util.AWSSDKUtils/<>c__DisplayClass37_0`1<System.Object>::.ctor()
extern "C"  void U3CU3Ec__DisplayClass37_0_1__ctor_m4223920067_gshared (U3CU3Ec__DisplayClass37_0_1_t4001026854 * __this, const MethodInfo* method);
#define U3CU3Ec__DisplayClass37_0_1__ctor_m4223920067(__this, method) ((  void (*) (U3CU3Ec__DisplayClass37_0_1_t4001026854 *, const MethodInfo*))U3CU3Ec__DisplayClass37_0_1__ctor_m4223920067_gshared)(__this, method)
// System.Void Amazon.Util.AWSSDKUtils/<>c__DisplayClass37_0`1<System.Object>::<InvokeInBackground>b__0()
extern "C"  void U3CU3Ec__DisplayClass37_0_1_U3CInvokeInBackgroundU3Eb__0_m1029580678_gshared (U3CU3Ec__DisplayClass37_0_1_t4001026854 * __this, const MethodInfo* method);
#define U3CU3Ec__DisplayClass37_0_1_U3CInvokeInBackgroundU3Eb__0_m1029580678(__this, method) ((  void (*) (U3CU3Ec__DisplayClass37_0_1_t4001026854 *, const MethodInfo*))U3CU3Ec__DisplayClass37_0_1_U3CInvokeInBackgroundU3Eb__0_m1029580678_gshared)(__this, method)
