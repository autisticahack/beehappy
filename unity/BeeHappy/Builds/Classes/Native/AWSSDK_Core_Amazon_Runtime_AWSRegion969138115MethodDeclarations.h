﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.AWSRegion
struct AWSRegion_t969138115;
// Amazon.RegionEndpoint
struct RegionEndpoint_t661522805;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_RegionEndpoint661522805.h"

// Amazon.RegionEndpoint Amazon.Runtime.AWSRegion::get_Region()
extern "C"  RegionEndpoint_t661522805 * AWSRegion_get_Region_m2666077117 (AWSRegion_t969138115 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.AWSRegion::set_Region(Amazon.RegionEndpoint)
extern "C"  void AWSRegion_set_Region_m1250267774 (AWSRegion_t969138115 * __this, RegionEndpoint_t661522805 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.AWSRegion::.ctor()
extern "C"  void AWSRegion__ctor_m4119372480 (AWSRegion_t969138115 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
