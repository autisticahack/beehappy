﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<Amazon.Runtime.IAsyncExecutionContext>
struct Action_1_t3594144368;
// System.Action`2<System.Exception,Amazon.Runtime.IAsyncExecutionContext>
struct Action_2_t1841170552;
// Amazon.Runtime.IAsyncExecutionContext
struct IAsyncExecutionContext_t3792344986;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Util.ThreadPoolThrottler`1/ThreadPoolOptions`1<Amazon.Runtime.IAsyncExecutionContext,Amazon.Runtime.IAsyncExecutionContext>
struct  ThreadPoolOptions_1_t3222238215  : public Il2CppObject
{
public:
	// System.Action`1<Q> Amazon.Runtime.Internal.Util.ThreadPoolThrottler`1/ThreadPoolOptions`1::<Callback>k__BackingField
	Action_1_t3594144368 * ___U3CCallbackU3Ek__BackingField_0;
	// System.Action`2<System.Exception,Q> Amazon.Runtime.Internal.Util.ThreadPoolThrottler`1/ThreadPoolOptions`1::<ErrorCallback>k__BackingField
	Action_2_t1841170552 * ___U3CErrorCallbackU3Ek__BackingField_1;
	// Q Amazon.Runtime.Internal.Util.ThreadPoolThrottler`1/ThreadPoolOptions`1::<State>k__BackingField
	Il2CppObject * ___U3CStateU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ThreadPoolOptions_1_t3222238215, ___U3CCallbackU3Ek__BackingField_0)); }
	inline Action_1_t3594144368 * get_U3CCallbackU3Ek__BackingField_0() const { return ___U3CCallbackU3Ek__BackingField_0; }
	inline Action_1_t3594144368 ** get_address_of_U3CCallbackU3Ek__BackingField_0() { return &___U3CCallbackU3Ek__BackingField_0; }
	inline void set_U3CCallbackU3Ek__BackingField_0(Action_1_t3594144368 * value)
	{
		___U3CCallbackU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCallbackU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CErrorCallbackU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ThreadPoolOptions_1_t3222238215, ___U3CErrorCallbackU3Ek__BackingField_1)); }
	inline Action_2_t1841170552 * get_U3CErrorCallbackU3Ek__BackingField_1() const { return ___U3CErrorCallbackU3Ek__BackingField_1; }
	inline Action_2_t1841170552 ** get_address_of_U3CErrorCallbackU3Ek__BackingField_1() { return &___U3CErrorCallbackU3Ek__BackingField_1; }
	inline void set_U3CErrorCallbackU3Ek__BackingField_1(Action_2_t1841170552 * value)
	{
		___U3CErrorCallbackU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CErrorCallbackU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CStateU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ThreadPoolOptions_1_t3222238215, ___U3CStateU3Ek__BackingField_2)); }
	inline Il2CppObject * get_U3CStateU3Ek__BackingField_2() const { return ___U3CStateU3Ek__BackingField_2; }
	inline Il2CppObject ** get_address_of_U3CStateU3Ek__BackingField_2() { return &___U3CStateU3Ek__BackingField_2; }
	inline void set_U3CStateU3Ek__BackingField_2(Il2CppObject * value)
	{
		___U3CStateU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CStateU3Ek__BackingField_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
