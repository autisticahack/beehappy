﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.ClientConfig
struct ClientConfig_t3664713661;
// System.String
struct String_t;
// Amazon.RegionEndpoint
struct RegionEndpoint_t661522805;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AWSSDK_Core_Amazon_RegionEndpoint661522805.h"

// System.Void Amazon.Runtime.ClientConfig::set_SignatureVersion(System.String)
extern "C"  void ClientConfig_set_SignatureVersion_m1168150583 (ClientConfig_t3664713661 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.RegionEndpoint Amazon.Runtime.ClientConfig::get_RegionEndpoint()
extern "C"  RegionEndpoint_t661522805 * ClientConfig_get_RegionEndpoint_m1043738138 (ClientConfig_t3664713661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.ClientConfig::set_RegionEndpoint(Amazon.RegionEndpoint)
extern "C"  void ClientConfig_set_RegionEndpoint_m2421036023 (ClientConfig_t3664713661 * __this, RegionEndpoint_t661522805 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Runtime.ClientConfig::get_ServiceURL()
extern "C"  String_t* ClientConfig_get_ServiceURL_m2492809620 (ClientConfig_t3664713661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.ClientConfig::get_UseHttp()
extern "C"  bool ClientConfig_get_UseHttp_m860344734 (ClientConfig_t3664713661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Runtime.ClientConfig::DetermineServiceURL()
extern "C"  String_t* ClientConfig_DetermineServiceURL_m2840321236 (ClientConfig_t3664713661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Runtime.ClientConfig::GetUrl(Amazon.RegionEndpoint,System.String,System.Boolean,System.Boolean)
extern "C"  String_t* ClientConfig_GetUrl_m1922078087 (Il2CppObject * __this /* static, unused */, RegionEndpoint_t661522805 * ___regionEndpoint0, String_t* ___regionEndpointServiceName1, bool ___useHttp2, bool ___useDualStack3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Runtime.ClientConfig::get_AuthenticationRegion()
extern "C"  String_t* ClientConfig_get_AuthenticationRegion_m2324899788 (ClientConfig_t3664713661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Runtime.ClientConfig::get_AuthenticationServiceName()
extern "C"  String_t* ClientConfig_get_AuthenticationServiceName_m773577826 (ClientConfig_t3664713661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.ClientConfig::set_AuthenticationServiceName(System.String)
extern "C"  void ClientConfig_set_AuthenticationServiceName_m1205259675 (ClientConfig_t3664713661 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Amazon.Runtime.ClientConfig::get_MaxErrorRetry()
extern "C"  int32_t ClientConfig_get_MaxErrorRetry_m2883982881 (ClientConfig_t3664713661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.ClientConfig::get_LogResponse()
extern "C"  bool ClientConfig_get_LogResponse_m1523160988 (ClientConfig_t3664713661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Amazon.Runtime.ClientConfig::get_BufferSize()
extern "C"  int32_t ClientConfig_get_BufferSize_m871909402 (ClientConfig_t3664713661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Amazon.Runtime.ClientConfig::get_ProgressUpdateInterval()
extern "C"  int64_t ClientConfig_get_ProgressUpdateInterval_m1499785281 (ClientConfig_t3664713661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.ClientConfig::get_ResignRetries()
extern "C"  bool ClientConfig_get_ResignRetries_m344097101 (ClientConfig_t3664713661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.ClientConfig::get_LogMetrics()
extern "C"  bool ClientConfig_get_LogMetrics_m359479978 (ClientConfig_t3664713661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.ClientConfig::get_DisableLogging()
extern "C"  bool ClientConfig_get_DisableLogging_m165970400 (ClientConfig_t3664713661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.ClientConfig::.ctor()
extern "C"  void ClientConfig__ctor_m3613490344 (ClientConfig_t3664713661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.ClientConfig::Initialize()
extern "C"  void ClientConfig_Initialize_m3566867580 (ClientConfig_t3664713661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.ClientConfig::get_UseDualstackEndpoint()
extern "C"  bool ClientConfig_get_UseDualstackEndpoint_m173973001 (ClientConfig_t3664713661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.ClientConfig::get_ThrottleRetries()
extern "C"  bool ClientConfig_get_ThrottleRetries_m881119113 (ClientConfig_t3664713661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.ClientConfig::Validate()
extern "C"  void ClientConfig_Validate_m4244294360 (ClientConfig_t3664713661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.ClientConfig::.cctor()
extern "C"  void ClientConfig__cctor_m3979502777 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
