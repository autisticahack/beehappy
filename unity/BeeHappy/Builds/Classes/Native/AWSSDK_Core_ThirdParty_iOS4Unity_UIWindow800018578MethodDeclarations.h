﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ThirdParty.iOS4Unity.UIWindow
struct UIWindow_t800018578;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr2504060609.h"

// System.Void ThirdParty.iOS4Unity.UIWindow::.cctor()
extern "C"  void UIWindow__cctor_m3975553979 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.iOS4Unity.UIWindow::.ctor(System.IntPtr)
extern "C"  void UIWindow__ctor_m3442098196 (UIWindow_t800018578 * __this, IntPtr_t ___handle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
