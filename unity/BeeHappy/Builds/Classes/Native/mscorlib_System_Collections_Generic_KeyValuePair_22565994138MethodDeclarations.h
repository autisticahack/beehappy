﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23196873006MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<Amazon.Runtime.Metric,System.Collections.Generic.List`1<System.Object>>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m3850323035(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2565994138 *, int32_t, List_1_t2058570427 *, const MethodInfo*))KeyValuePair_2__ctor_m595192313_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<Amazon.Runtime.Metric,System.Collections.Generic.List`1<System.Object>>::get_Key()
#define KeyValuePair_2_get_Key_m3085516210(__this, method) ((  int32_t (*) (KeyValuePair_2_t2565994138 *, const MethodInfo*))KeyValuePair_2_get_Key_m2312610863_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Amazon.Runtime.Metric,System.Collections.Generic.List`1<System.Object>>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m3092659174(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2565994138 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m547641270_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<Amazon.Runtime.Metric,System.Collections.Generic.List`1<System.Object>>::get_Value()
#define KeyValuePair_2_get_Value_m1353922381(__this, method) ((  List_1_t2058570427 * (*) (KeyValuePair_2_t2565994138 *, const MethodInfo*))KeyValuePair_2_get_Value_m3734313679_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Amazon.Runtime.Metric,System.Collections.Generic.List`1<System.Object>>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m3577441046(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2565994138 *, List_1_t2058570427 *, const MethodInfo*))KeyValuePair_2_set_Value_m1567047294_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<Amazon.Runtime.Metric,System.Collections.Generic.List`1<System.Object>>::ToString()
#define KeyValuePair_2_ToString_m2478902956(__this, method) ((  String_t* (*) (KeyValuePair_2_t2565994138 *, const MethodInfo*))KeyValuePair_2_ToString_m3523821758_gshared)(__this, method)
