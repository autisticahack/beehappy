﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Diagnostics.Stopwatch
struct Stopwatch_t1380178105;
// Amazon.Runtime.Internal.IRequest
struct IRequest_t2400804350;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Diagnostics_Stopwatch1380178105.h"

// System.Int64 Amazon.Runtime.Internal.Util.Extensions::GetElapsedDateTimeTicks(System.Diagnostics.Stopwatch)
extern "C"  int64_t Extensions_GetElapsedDateTimeTicks_m879006810 (Il2CppObject * __this /* static, unused */, Stopwatch_t1380178105 * ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.Util.Extensions::HasRequestData(Amazon.Runtime.Internal.IRequest)
extern "C"  bool Extensions_HasRequestData_m2584243408 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___request0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.Extensions::.cctor()
extern "C"  void Extensions__cctor_m4188126083 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
