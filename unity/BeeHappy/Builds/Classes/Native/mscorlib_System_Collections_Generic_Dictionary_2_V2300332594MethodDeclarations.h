﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V4142587627MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Amazon.Runtime.Metric,Amazon.Runtime.Internal.Util.Timing>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m1605752693(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t2300332594 *, Dictionary_2_t3597272751 *, const MethodInfo*))ValueCollection__ctor_m983902812_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Amazon.Runtime.Metric,Amazon.Runtime.Internal.Util.Timing>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3070231367(__this, ___item0, method) ((  void (*) (ValueCollection_t2300332594 *, Timing_t847194262 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3409785142_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Amazon.Runtime.Metric,Amazon.Runtime.Internal.Util.Timing>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m642673680(__this, method) ((  void (*) (ValueCollection_t2300332594 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2769354283_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Amazon.Runtime.Metric,Amazon.Runtime.Internal.Util.Timing>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1311745645(__this, ___item0, method) ((  bool (*) (ValueCollection_t2300332594 *, Timing_t847194262 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2796363082_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Amazon.Runtime.Metric,Amazon.Runtime.Internal.Util.Timing>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2733601914(__this, ___item0, method) ((  bool (*) (ValueCollection_t2300332594 *, Timing_t847194262 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2318512635_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<Amazon.Runtime.Metric,Amazon.Runtime.Internal.Util.Timing>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2788306600(__this, method) ((  Il2CppObject* (*) (ValueCollection_t2300332594 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2423026497_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Amazon.Runtime.Metric,Amazon.Runtime.Internal.Util.Timing>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m2206470368(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2300332594 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m690507645_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<Amazon.Runtime.Metric,Amazon.Runtime.Internal.Util.Timing>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3572362495(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2300332594 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2267909886_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Amazon.Runtime.Metric,Amazon.Runtime.Internal.Util.Timing>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2991208582(__this, method) ((  bool (*) (ValueCollection_t2300332594 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3813706855_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Amazon.Runtime.Metric,Amazon.Runtime.Internal.Util.Timing>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3913734604(__this, method) ((  bool (*) (ValueCollection_t2300332594 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m467693041_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<Amazon.Runtime.Metric,Amazon.Runtime.Internal.Util.Timing>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m137196048(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2300332594 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m3529335277_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Amazon.Runtime.Metric,Amazon.Runtime.Internal.Util.Timing>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m2319476188(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2300332594 *, TimingU5BU5D_t3035800659*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m2739320779_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<Amazon.Runtime.Metric,Amazon.Runtime.Internal.Util.Timing>::GetEnumerator()
#define ValueCollection_GetEnumerator_m2103290433(__this, method) ((  Enumerator_t988838219  (*) (ValueCollection_t2300332594 *, const MethodInfo*))ValueCollection_GetEnumerator_m807736870_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<Amazon.Runtime.Metric,Amazon.Runtime.Internal.Util.Timing>::get_Count()
#define ValueCollection_get_Count_m1474744716(__this, method) ((  int32_t (*) (ValueCollection_t2300332594 *, const MethodInfo*))ValueCollection_get_Count_m3120365865_gshared)(__this, method)
