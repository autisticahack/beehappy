﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Util.AWSSDKUtils/<>c__DisplayClass37_1`1<System.Object>
struct U3CU3Ec__DisplayClass37_1_1_t3163399583;

#include "codegen/il2cpp-codegen.h"

// System.Void Amazon.Util.AWSSDKUtils/<>c__DisplayClass37_1`1<System.Object>::.ctor()
extern "C"  void U3CU3Ec__DisplayClass37_1_1__ctor_m1903798626_gshared (U3CU3Ec__DisplayClass37_1_1_t3163399583 * __this, const MethodInfo* method);
#define U3CU3Ec__DisplayClass37_1_1__ctor_m1903798626(__this, method) ((  void (*) (U3CU3Ec__DisplayClass37_1_1_t3163399583 *, const MethodInfo*))U3CU3Ec__DisplayClass37_1_1__ctor_m1903798626_gshared)(__this, method)
