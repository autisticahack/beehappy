﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3628058259MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,Amazon.Runtime.Internal.Util.Timing>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m1639343261(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t1785803226 *, Dictionary_2_t3597272751 *, const MethodInfo*))KeyCollection__ctor_m2275592418_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,Amazon.Runtime.Internal.Util.Timing>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m745713751(__this, ___item0, method) ((  void (*) (KeyCollection_t1785803226 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m22610452_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,Amazon.Runtime.Internal.Util.Timing>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2728362832(__this, method) ((  void (*) (KeyCollection_t1785803226 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3969664523_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,Amazon.Runtime.Internal.Util.Timing>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1969857309(__this, ___item0, method) ((  bool (*) (KeyCollection_t1785803226 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2395394556_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,Amazon.Runtime.Internal.Util.Timing>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2698279152(__this, ___item0, method) ((  bool (*) (KeyCollection_t1785803226 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1815209971_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,Amazon.Runtime.Internal.Util.Timing>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2336322582(__this, method) ((  Il2CppObject* (*) (KeyCollection_t1785803226 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m246573329_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,Amazon.Runtime.Internal.Util.Timing>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m1402436126(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1785803226 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m3092510701_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,Amazon.Runtime.Internal.Util.Timing>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2092397327(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1785803226 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1668434068_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,Amazon.Runtime.Internal.Util.Timing>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1202559814(__this, method) ((  bool (*) (KeyCollection_t1785803226 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2896445255_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,Amazon.Runtime.Internal.Util.Timing>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m2896141874(__this, method) ((  bool (*) (KeyCollection_t1785803226 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m451785489_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,Amazon.Runtime.Internal.Util.Timing>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m2966115166(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1785803226 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m1284653485_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,Amazon.Runtime.Internal.Util.Timing>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m2171560284(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1785803226 *, MetricU5BU5D_t2477685199*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m3857679723_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,Amazon.Runtime.Internal.Util.Timing>::GetEnumerator()
#define KeyCollection_GetEnumerator_m2834044097(__this, method) ((  Enumerator_t1991808893  (*) (KeyCollection_t1785803226 *, const MethodInfo*))KeyCollection_GetEnumerator_m912416230_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,Amazon.Runtime.Internal.Util.Timing>::get_Count()
#define KeyCollection_get_Count_m512251562(__this, method) ((  int32_t (*) (KeyCollection_t1785803226 *, const MethodInfo*))KeyCollection_get_Count_m3852128017_gshared)(__this, method)
