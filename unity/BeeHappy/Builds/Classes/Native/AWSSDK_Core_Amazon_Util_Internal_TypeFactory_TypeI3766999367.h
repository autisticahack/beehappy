﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AWSSDK_Core_Amazon_Util_Internal_TypeFactory_Abstr3397773112.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Util.Internal.TypeFactory/TypeInfoWrapper
struct  TypeInfoWrapper_t3766999367  : public AbstractTypeInfo_t3397773112
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
