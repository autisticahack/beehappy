﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Data.Sqlite.SQLiteCommitCallback
struct SQLiteCommitCallback_t2277160688;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Mono.Data.Sqlite.SQLiteCommitCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void SQLiteCommitCallback__ctor_m391641155 (SQLiteCommitCallback_t2277160688 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.SQLiteCommitCallback::Invoke(System.IntPtr)
extern "C"  int32_t SQLiteCommitCallback_Invoke_m3183311751 (SQLiteCommitCallback_t2277160688 * __this, IntPtr_t ___puser0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Mono.Data.Sqlite.SQLiteCommitCallback::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * SQLiteCommitCallback_BeginInvoke_m3859749880 (SQLiteCommitCallback_t2277160688 * __this, IntPtr_t ___puser0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.SQLiteCommitCallback::EndInvoke(System.IAsyncResult)
extern "C"  int32_t SQLiteCommitCallback_EndInvoke_m1204990439 (SQLiteCommitCallback_t2277160688 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
