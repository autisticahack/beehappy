﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Linq.OrderedEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct OrderedEnumerable_1_t1563487624;
// System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerable_1_t330981690;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerator_1_t1809345768;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Linq.OrderedEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.IEnumerable`1<TElement>)
extern "C"  void OrderedEnumerable_1__ctor_m1559215142_gshared (OrderedEnumerable_1_t1563487624 * __this, Il2CppObject* ___source0, const MethodInfo* method);
#define OrderedEnumerable_1__ctor_m1559215142(__this, ___source0, method) ((  void (*) (OrderedEnumerable_1_t1563487624 *, Il2CppObject*, const MethodInfo*))OrderedEnumerable_1__ctor_m1559215142_gshared)(__this, ___source0, method)
// System.Collections.IEnumerator System.Linq.OrderedEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * OrderedEnumerable_1_System_Collections_IEnumerable_GetEnumerator_m1617986410_gshared (OrderedEnumerable_1_t1563487624 * __this, const MethodInfo* method);
#define OrderedEnumerable_1_System_Collections_IEnumerable_GetEnumerator_m1617986410(__this, method) ((  Il2CppObject * (*) (OrderedEnumerable_1_t1563487624 *, const MethodInfo*))OrderedEnumerable_1_System_Collections_IEnumerable_GetEnumerator_m1617986410_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<TElement> System.Linq.OrderedEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::GetEnumerator()
extern "C"  Il2CppObject* OrderedEnumerable_1_GetEnumerator_m2408194437_gshared (OrderedEnumerable_1_t1563487624 * __this, const MethodInfo* method);
#define OrderedEnumerable_1_GetEnumerator_m2408194437(__this, method) ((  Il2CppObject* (*) (OrderedEnumerable_1_t1563487624 *, const MethodInfo*))OrderedEnumerable_1_GetEnumerator_m2408194437_gshared)(__this, method)
