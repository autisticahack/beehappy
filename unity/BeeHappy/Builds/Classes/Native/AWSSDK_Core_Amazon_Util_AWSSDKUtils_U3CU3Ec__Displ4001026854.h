﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.EventHandler`1<System.Object>
struct EventHandler_1_t1280756467;
// Amazon.Util.AWSSDKUtils/<>c__DisplayClass37_1`1<System.Object>
struct U3CU3Ec__DisplayClass37_1_1_t3163399583;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Util.AWSSDKUtils/<>c__DisplayClass37_0`1<System.Object>
struct  U3CU3Ec__DisplayClass37_0_1_t4001026854  : public Il2CppObject
{
public:
	// System.EventHandler`1<T> Amazon.Util.AWSSDKUtils/<>c__DisplayClass37_0`1::eventHandler
	EventHandler_1_t1280756467 * ___eventHandler_0;
	// Amazon.Util.AWSSDKUtils/<>c__DisplayClass37_1`1<T> Amazon.Util.AWSSDKUtils/<>c__DisplayClass37_0`1::CS$<>8__locals1
	U3CU3Ec__DisplayClass37_1_1_t3163399583 * ___CSU24U3CU3E8__locals1_1;

public:
	inline static int32_t get_offset_of_eventHandler_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass37_0_1_t4001026854, ___eventHandler_0)); }
	inline EventHandler_1_t1280756467 * get_eventHandler_0() const { return ___eventHandler_0; }
	inline EventHandler_1_t1280756467 ** get_address_of_eventHandler_0() { return &___eventHandler_0; }
	inline void set_eventHandler_0(EventHandler_1_t1280756467 * value)
	{
		___eventHandler_0 = value;
		Il2CppCodeGenWriteBarrier(&___eventHandler_0, value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E8__locals1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass37_0_1_t4001026854, ___CSU24U3CU3E8__locals1_1)); }
	inline U3CU3Ec__DisplayClass37_1_1_t3163399583 * get_CSU24U3CU3E8__locals1_1() const { return ___CSU24U3CU3E8__locals1_1; }
	inline U3CU3Ec__DisplayClass37_1_1_t3163399583 ** get_address_of_CSU24U3CU3E8__locals1_1() { return &___CSU24U3CU3E8__locals1_1; }
	inline void set_CSU24U3CU3E8__locals1_1(U3CU3Ec__DisplayClass37_1_1_t3163399583 * value)
	{
		___CSU24U3CU3E8__locals1_1 = value;
		Il2CppCodeGenWriteBarrier(&___CSU24U3CU3E8__locals1_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
