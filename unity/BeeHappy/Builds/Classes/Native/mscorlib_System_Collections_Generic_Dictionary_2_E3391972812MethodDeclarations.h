﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En156532019MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Collections.Generic.Dictionary`2<System.IntPtr,ThirdParty.iOS4Unity.Callbacks/Methods>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m4013906164(__this, ___dictionary0, method) ((  void (*) (Enumerator_t3391972812 *, Dictionary_2_t2071948110 *, const MethodInfo*))Enumerator__ctor_m1897170543_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Collections.Generic.Dictionary`2<System.IntPtr,ThirdParty.iOS4Unity.Callbacks/Methods>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3280000355(__this, method) ((  Il2CppObject * (*) (Enumerator_t3391972812 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2389891262_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Collections.Generic.Dictionary`2<System.IntPtr,ThirdParty.iOS4Unity.Callbacks/Methods>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3999654311(__this, method) ((  void (*) (Enumerator_t3391972812 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3302454658_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Collections.Generic.Dictionary`2<System.IntPtr,ThirdParty.iOS4Unity.Callbacks/Methods>>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1182833430(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t3391972812 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m4077548725_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Collections.Generic.Dictionary`2<System.IntPtr,ThirdParty.iOS4Unity.Callbacks/Methods>>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2491277541(__this, method) ((  Il2CppObject * (*) (Enumerator_t3391972812 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2319419488_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Collections.Generic.Dictionary`2<System.IntPtr,ThirdParty.iOS4Unity.Callbacks/Methods>>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1451299109(__this, method) ((  Il2CppObject * (*) (Enumerator_t3391972812 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m120250090_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Collections.Generic.Dictionary`2<System.IntPtr,ThirdParty.iOS4Unity.Callbacks/Methods>>::MoveNext()
#define Enumerator_MoveNext_m2206803651(__this, method) ((  bool (*) (Enumerator_t3391972812 *, const MethodInfo*))Enumerator_MoveNext_m266619810_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Collections.Generic.Dictionary`2<System.IntPtr,ThirdParty.iOS4Unity.Callbacks/Methods>>::get_Current()
#define Enumerator_get_Current_m440115483(__this, method) ((  KeyValuePair_2_t4124260628  (*) (Enumerator_t3391972812 *, const MethodInfo*))Enumerator_get_Current_m3216113338_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Collections.Generic.Dictionary`2<System.IntPtr,ThirdParty.iOS4Unity.Callbacks/Methods>>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m3788864484(__this, method) ((  IntPtr_t (*) (Enumerator_t3391972812 *, const MethodInfo*))Enumerator_get_CurrentKey_m2387752419_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Collections.Generic.Dictionary`2<System.IntPtr,ThirdParty.iOS4Unity.Callbacks/Methods>>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m240000420(__this, method) ((  Dictionary_2_t1629922792 * (*) (Enumerator_t3391972812 *, const MethodInfo*))Enumerator_get_CurrentValue_m3147984131_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Collections.Generic.Dictionary`2<System.IntPtr,ThirdParty.iOS4Unity.Callbacks/Methods>>::Reset()
#define Enumerator_Reset_m895088882(__this, method) ((  void (*) (Enumerator_t3391972812 *, const MethodInfo*))Enumerator_Reset_m1147555085_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Collections.Generic.Dictionary`2<System.IntPtr,ThirdParty.iOS4Unity.Callbacks/Methods>>::VerifyState()
#define Enumerator_VerifyState_m1713684049(__this, method) ((  void (*) (Enumerator_t3391972812 *, const MethodInfo*))Enumerator_VerifyState_m1547102902_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Collections.Generic.Dictionary`2<System.IntPtr,ThirdParty.iOS4Unity.Callbacks/Methods>>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m3232399455(__this, method) ((  void (*) (Enumerator_t3391972812 *, const MethodInfo*))Enumerator_VerifyCurrent_m213906106_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Collections.Generic.Dictionary`2<System.IntPtr,ThirdParty.iOS4Unity.Callbacks/Methods>>::Dispose()
#define Enumerator_Dispose_m3992682840(__this, method) ((  void (*) (Enumerator_t3391972812 *, const MethodInfo*))Enumerator_Dispose_m2117437651_gshared)(__this, method)
