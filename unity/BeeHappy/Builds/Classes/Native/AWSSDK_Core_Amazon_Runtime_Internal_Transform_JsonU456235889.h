﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.IO.StreamReader
struct StreamReader_t2360341767;
// ThirdParty.Json.LitJson.JsonReader
struct JsonReader_t354941621;
// Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext/JsonPathStack
struct JsonPathStack_t805090354;
// System.String
struct String_t;

#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_Unma1608322995.h"
#include "mscorlib_System_Nullable_1_gen708647570.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext
struct  JsonUnmarshallerContext_t456235889  : public UnmarshallerContext_t1608322995
{
public:
	// System.IO.StreamReader Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext::streamReader
	StreamReader_t2360341767 * ___streamReader_6;
	// ThirdParty.Json.LitJson.JsonReader Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext::jsonReader
	JsonReader_t354941621 * ___jsonReader_7;
	// Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext/JsonPathStack Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext::stack
	JsonPathStack_t805090354 * ___stack_8;
	// System.String Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext::currentField
	String_t* ___currentField_9;
	// System.Nullable`1<ThirdParty.Json.LitJson.JsonToken> Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext::currentToken
	Nullable_1_t708647570  ___currentToken_10;
	// System.Boolean Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext::disposed
	bool ___disposed_11;
	// System.Boolean Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext::wasPeeked
	bool ___wasPeeked_12;

public:
	inline static int32_t get_offset_of_streamReader_6() { return static_cast<int32_t>(offsetof(JsonUnmarshallerContext_t456235889, ___streamReader_6)); }
	inline StreamReader_t2360341767 * get_streamReader_6() const { return ___streamReader_6; }
	inline StreamReader_t2360341767 ** get_address_of_streamReader_6() { return &___streamReader_6; }
	inline void set_streamReader_6(StreamReader_t2360341767 * value)
	{
		___streamReader_6 = value;
		Il2CppCodeGenWriteBarrier(&___streamReader_6, value);
	}

	inline static int32_t get_offset_of_jsonReader_7() { return static_cast<int32_t>(offsetof(JsonUnmarshallerContext_t456235889, ___jsonReader_7)); }
	inline JsonReader_t354941621 * get_jsonReader_7() const { return ___jsonReader_7; }
	inline JsonReader_t354941621 ** get_address_of_jsonReader_7() { return &___jsonReader_7; }
	inline void set_jsonReader_7(JsonReader_t354941621 * value)
	{
		___jsonReader_7 = value;
		Il2CppCodeGenWriteBarrier(&___jsonReader_7, value);
	}

	inline static int32_t get_offset_of_stack_8() { return static_cast<int32_t>(offsetof(JsonUnmarshallerContext_t456235889, ___stack_8)); }
	inline JsonPathStack_t805090354 * get_stack_8() const { return ___stack_8; }
	inline JsonPathStack_t805090354 ** get_address_of_stack_8() { return &___stack_8; }
	inline void set_stack_8(JsonPathStack_t805090354 * value)
	{
		___stack_8 = value;
		Il2CppCodeGenWriteBarrier(&___stack_8, value);
	}

	inline static int32_t get_offset_of_currentField_9() { return static_cast<int32_t>(offsetof(JsonUnmarshallerContext_t456235889, ___currentField_9)); }
	inline String_t* get_currentField_9() const { return ___currentField_9; }
	inline String_t** get_address_of_currentField_9() { return &___currentField_9; }
	inline void set_currentField_9(String_t* value)
	{
		___currentField_9 = value;
		Il2CppCodeGenWriteBarrier(&___currentField_9, value);
	}

	inline static int32_t get_offset_of_currentToken_10() { return static_cast<int32_t>(offsetof(JsonUnmarshallerContext_t456235889, ___currentToken_10)); }
	inline Nullable_1_t708647570  get_currentToken_10() const { return ___currentToken_10; }
	inline Nullable_1_t708647570 * get_address_of_currentToken_10() { return &___currentToken_10; }
	inline void set_currentToken_10(Nullable_1_t708647570  value)
	{
		___currentToken_10 = value;
	}

	inline static int32_t get_offset_of_disposed_11() { return static_cast<int32_t>(offsetof(JsonUnmarshallerContext_t456235889, ___disposed_11)); }
	inline bool get_disposed_11() const { return ___disposed_11; }
	inline bool* get_address_of_disposed_11() { return &___disposed_11; }
	inline void set_disposed_11(bool value)
	{
		___disposed_11 = value;
	}

	inline static int32_t get_offset_of_wasPeeked_12() { return static_cast<int32_t>(offsetof(JsonUnmarshallerContext_t456235889, ___wasPeeked_12)); }
	inline bool get_wasPeeked_12() const { return ___wasPeeked_12; }
	inline bool* get_address_of_wasPeeked_12() { return &___wasPeeked_12; }
	inline void set_wasPeeked_12(bool value)
	{
		___wasPeeked_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
