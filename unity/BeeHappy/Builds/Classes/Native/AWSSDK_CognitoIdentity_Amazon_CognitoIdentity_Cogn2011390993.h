﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.CognitoIdentity.CognitoAWSCredentials/IdentityState
struct  IdentityState_t2011390993  : public Il2CppObject
{
public:
	// System.String Amazon.CognitoIdentity.CognitoAWSCredentials/IdentityState::<IdentityId>k__BackingField
	String_t* ___U3CIdentityIdU3Ek__BackingField_0;
	// System.String Amazon.CognitoIdentity.CognitoAWSCredentials/IdentityState::<LoginProvider>k__BackingField
	String_t* ___U3CLoginProviderU3Ek__BackingField_1;
	// System.String Amazon.CognitoIdentity.CognitoAWSCredentials/IdentityState::<LoginToken>k__BackingField
	String_t* ___U3CLoginTokenU3Ek__BackingField_2;
	// System.Boolean Amazon.CognitoIdentity.CognitoAWSCredentials/IdentityState::<FromCache>k__BackingField
	bool ___U3CFromCacheU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CIdentityIdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(IdentityState_t2011390993, ___U3CIdentityIdU3Ek__BackingField_0)); }
	inline String_t* get_U3CIdentityIdU3Ek__BackingField_0() const { return ___U3CIdentityIdU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CIdentityIdU3Ek__BackingField_0() { return &___U3CIdentityIdU3Ek__BackingField_0; }
	inline void set_U3CIdentityIdU3Ek__BackingField_0(String_t* value)
	{
		___U3CIdentityIdU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CIdentityIdU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CLoginProviderU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(IdentityState_t2011390993, ___U3CLoginProviderU3Ek__BackingField_1)); }
	inline String_t* get_U3CLoginProviderU3Ek__BackingField_1() const { return ___U3CLoginProviderU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CLoginProviderU3Ek__BackingField_1() { return &___U3CLoginProviderU3Ek__BackingField_1; }
	inline void set_U3CLoginProviderU3Ek__BackingField_1(String_t* value)
	{
		___U3CLoginProviderU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CLoginProviderU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CLoginTokenU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(IdentityState_t2011390993, ___U3CLoginTokenU3Ek__BackingField_2)); }
	inline String_t* get_U3CLoginTokenU3Ek__BackingField_2() const { return ___U3CLoginTokenU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CLoginTokenU3Ek__BackingField_2() { return &___U3CLoginTokenU3Ek__BackingField_2; }
	inline void set_U3CLoginTokenU3Ek__BackingField_2(String_t* value)
	{
		___U3CLoginTokenU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CLoginTokenU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CFromCacheU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(IdentityState_t2011390993, ___U3CFromCacheU3Ek__BackingField_3)); }
	inline bool get_U3CFromCacheU3Ek__BackingField_3() const { return ___U3CFromCacheU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CFromCacheU3Ek__BackingField_3() { return &___U3CFromCacheU3Ek__BackingField_3; }
	inline void set_U3CFromCacheU3Ek__BackingField_3(bool value)
	{
		___U3CFromCacheU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
