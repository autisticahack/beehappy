﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1747572097.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_888819835.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m920047666_gshared (InternalEnumerator_1_t1747572097 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m920047666(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1747572097 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m920047666_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m138745466_gshared (InternalEnumerator_1_t1747572097 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m138745466(__this, method) ((  void (*) (InternalEnumerator_1_t1747572097 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m138745466_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1313308902_gshared (InternalEnumerator_1_t1747572097 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1313308902(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1747572097 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1313308902_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1628196379_gshared (InternalEnumerator_1_t1747572097 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1628196379(__this, method) ((  void (*) (InternalEnumerator_1_t1747572097 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1628196379_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m930150026_gshared (InternalEnumerator_1_t1747572097 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m930150026(__this, method) ((  bool (*) (InternalEnumerator_1_t1747572097 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m930150026_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>>::get_Current()
extern "C"  KeyValuePair_2_t888819835  InternalEnumerator_1_get_Current_m1526874475_gshared (InternalEnumerator_1_t1747572097 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1526874475(__this, method) ((  KeyValuePair_2_t888819835  (*) (InternalEnumerator_1_t1747572097 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1526874475_gshared)(__this, method)
