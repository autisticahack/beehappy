﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.UnityWebRequestWrapper
struct UnityWebRequestWrapper_t1542496577;
// System.String
struct String_t;
// Amazon.Runtime.Internal.DownloadHandlerBufferWrapper
struct DownloadHandlerBufferWrapper_t1800693841;
// Amazon.Runtime.Internal.UploadHandlerRawWrapper
struct UploadHandlerRawWrapper_t3528989734;
// UnityEngine.AsyncOperation
struct AsyncOperation_t3814632279;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_DownloadHandle1800693841.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_UploadHandlerR3528989734.h"
#include "mscorlib_System_Nullable_1_gen161475956.h"

// System.Void Amazon.Runtime.Internal.UnityWebRequestWrapper::.cctor()
extern "C"  void UnityWebRequestWrapper__cctor_m1560343078 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.UnityWebRequestWrapper::.ctor(System.String,System.String)
extern "C"  void UnityWebRequestWrapper__ctor_m802142705 (UnityWebRequestWrapper_t1542496577 * __this, String_t* ___url0, String_t* ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.UnityWebRequestWrapper::set_DownloadHandler(Amazon.Runtime.Internal.DownloadHandlerBufferWrapper)
extern "C"  void UnityWebRequestWrapper_set_DownloadHandler_m3181223940 (UnityWebRequestWrapper_t1542496577 * __this, DownloadHandlerBufferWrapper_t1800693841 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.Internal.DownloadHandlerBufferWrapper Amazon.Runtime.Internal.UnityWebRequestWrapper::get_DownloadHandler()
extern "C"  DownloadHandlerBufferWrapper_t1800693841 * UnityWebRequestWrapper_get_DownloadHandler_m552932307 (UnityWebRequestWrapper_t1542496577 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.UnityWebRequestWrapper::set_UploadHandler(Amazon.Runtime.Internal.UploadHandlerRawWrapper)
extern "C"  void UnityWebRequestWrapper_set_UploadHandler_m3151858284 (UnityWebRequestWrapper_t1542496577 * __this, UploadHandlerRawWrapper_t3528989734 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.UnityWebRequestWrapper::SetRequestHeader(System.String,System.String)
extern "C"  void UnityWebRequestWrapper_SetRequestHeader_m4111966989 (UnityWebRequestWrapper_t1542496577 * __this, String_t* ___key0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AsyncOperation Amazon.Runtime.Internal.UnityWebRequestWrapper::Send()
extern "C"  AsyncOperation_t3814632279 * UnityWebRequestWrapper_Send_m4070681172 (UnityWebRequestWrapper_t1542496577 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 Amazon.Runtime.Internal.UnityWebRequestWrapper::get_DownloadedBytes()
extern "C"  uint64_t UnityWebRequestWrapper_get_DownloadedBytes_m1074448700 (UnityWebRequestWrapper_t1542496577 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> Amazon.Runtime.Internal.UnityWebRequestWrapper::get_ResponseHeaders()
extern "C"  Dictionary_2_t3943999495 * UnityWebRequestWrapper_get_ResponseHeaders_m3554277863 (UnityWebRequestWrapper_t1542496577 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Net.HttpStatusCode> Amazon.Runtime.Internal.UnityWebRequestWrapper::get_StatusCode()
extern "C"  Nullable_1_t161475956  UnityWebRequestWrapper_get_StatusCode_m2681406310 (UnityWebRequestWrapper_t1542496577 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.UnityWebRequestWrapper::get_IsError()
extern "C"  bool UnityWebRequestWrapper_get_IsError_m4048652832 (UnityWebRequestWrapper_t1542496577 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Runtime.Internal.UnityWebRequestWrapper::get_Error()
extern "C"  String_t* UnityWebRequestWrapper_get_Error_m1100231637 (UnityWebRequestWrapper_t1542496577 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.UnityWebRequestWrapper::Dispose(System.Boolean)
extern "C"  void UnityWebRequestWrapper_Dispose_m1843808501 (UnityWebRequestWrapper_t1542496577 * __this, bool ___disposing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.UnityWebRequestWrapper::Dispose()
extern "C"  void UnityWebRequestWrapper_Dispose_m3181753374 (UnityWebRequestWrapper_t1542496577 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
