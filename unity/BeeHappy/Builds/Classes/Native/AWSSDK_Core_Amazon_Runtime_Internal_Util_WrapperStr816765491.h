﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.IO.Stream
struct Stream_t3255436806;

#include "mscorlib_System_IO_Stream3255436806.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Util.WrapperStream
struct  WrapperStream_t816765491  : public Stream_t3255436806
{
public:
	// System.IO.Stream Amazon.Runtime.Internal.Util.WrapperStream::<BaseStream>k__BackingField
	Stream_t3255436806 * ___U3CBaseStreamU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CBaseStreamU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(WrapperStream_t816765491, ___U3CBaseStreamU3Ek__BackingField_1)); }
	inline Stream_t3255436806 * get_U3CBaseStreamU3Ek__BackingField_1() const { return ___U3CBaseStreamU3Ek__BackingField_1; }
	inline Stream_t3255436806 ** get_address_of_U3CBaseStreamU3Ek__BackingField_1() { return &___U3CBaseStreamU3Ek__BackingField_1; }
	inline void set_U3CBaseStreamU3Ek__BackingField_1(Stream_t3255436806 * value)
	{
		___U3CBaseStreamU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CBaseStreamU3Ek__BackingField_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
