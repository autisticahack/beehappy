﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.Runtime.Internal.CapacityManager
struct CapacityManager_t2181902141;
// Amazon.Runtime.Internal.RetryCapacity
struct RetryCapacity_t2794668894;
// System.Collections.Generic.ICollection`1<System.Net.HttpStatusCode>
struct ICollection_1_t2850484946;
// System.Collections.Generic.ICollection`1<System.Net.WebExceptionStatus>
struct ICollection_1_t2121448836;
// System.Collections.Generic.HashSet`1<System.String>
struct HashSet_1_t362681087;
// System.Collections.Generic.ICollection`1<System.String>
struct ICollection_1_t2981295538;

#include "AWSSDK_Core_Amazon_Runtime_RetryPolicy1476739446.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.DefaultRetryPolicy
struct  DefaultRetryPolicy_t2207644155  : public RetryPolicy_t1476739446
{
public:
	// System.Int32 Amazon.Runtime.Internal.DefaultRetryPolicy::_maxBackoffInMilliseconds
	int32_t ____maxBackoffInMilliseconds_5;
	// Amazon.Runtime.Internal.RetryCapacity Amazon.Runtime.Internal.DefaultRetryPolicy::_retryCapacity
	RetryCapacity_t2794668894 * ____retryCapacity_6;
	// System.Collections.Generic.ICollection`1<System.Net.HttpStatusCode> Amazon.Runtime.Internal.DefaultRetryPolicy::_httpStatusCodesToRetryOn
	Il2CppObject* ____httpStatusCodesToRetryOn_7;
	// System.Collections.Generic.ICollection`1<System.Net.WebExceptionStatus> Amazon.Runtime.Internal.DefaultRetryPolicy::_webExceptionStatusesToRetryOn
	Il2CppObject* ____webExceptionStatusesToRetryOn_8;
	// System.Collections.Generic.ICollection`1<System.String> Amazon.Runtime.Internal.DefaultRetryPolicy::_errorCodesToRetryOn
	Il2CppObject* ____errorCodesToRetryOn_10;

public:
	inline static int32_t get_offset_of__maxBackoffInMilliseconds_5() { return static_cast<int32_t>(offsetof(DefaultRetryPolicy_t2207644155, ____maxBackoffInMilliseconds_5)); }
	inline int32_t get__maxBackoffInMilliseconds_5() const { return ____maxBackoffInMilliseconds_5; }
	inline int32_t* get_address_of__maxBackoffInMilliseconds_5() { return &____maxBackoffInMilliseconds_5; }
	inline void set__maxBackoffInMilliseconds_5(int32_t value)
	{
		____maxBackoffInMilliseconds_5 = value;
	}

	inline static int32_t get_offset_of__retryCapacity_6() { return static_cast<int32_t>(offsetof(DefaultRetryPolicy_t2207644155, ____retryCapacity_6)); }
	inline RetryCapacity_t2794668894 * get__retryCapacity_6() const { return ____retryCapacity_6; }
	inline RetryCapacity_t2794668894 ** get_address_of__retryCapacity_6() { return &____retryCapacity_6; }
	inline void set__retryCapacity_6(RetryCapacity_t2794668894 * value)
	{
		____retryCapacity_6 = value;
		Il2CppCodeGenWriteBarrier(&____retryCapacity_6, value);
	}

	inline static int32_t get_offset_of__httpStatusCodesToRetryOn_7() { return static_cast<int32_t>(offsetof(DefaultRetryPolicy_t2207644155, ____httpStatusCodesToRetryOn_7)); }
	inline Il2CppObject* get__httpStatusCodesToRetryOn_7() const { return ____httpStatusCodesToRetryOn_7; }
	inline Il2CppObject** get_address_of__httpStatusCodesToRetryOn_7() { return &____httpStatusCodesToRetryOn_7; }
	inline void set__httpStatusCodesToRetryOn_7(Il2CppObject* value)
	{
		____httpStatusCodesToRetryOn_7 = value;
		Il2CppCodeGenWriteBarrier(&____httpStatusCodesToRetryOn_7, value);
	}

	inline static int32_t get_offset_of__webExceptionStatusesToRetryOn_8() { return static_cast<int32_t>(offsetof(DefaultRetryPolicy_t2207644155, ____webExceptionStatusesToRetryOn_8)); }
	inline Il2CppObject* get__webExceptionStatusesToRetryOn_8() const { return ____webExceptionStatusesToRetryOn_8; }
	inline Il2CppObject** get_address_of__webExceptionStatusesToRetryOn_8() { return &____webExceptionStatusesToRetryOn_8; }
	inline void set__webExceptionStatusesToRetryOn_8(Il2CppObject* value)
	{
		____webExceptionStatusesToRetryOn_8 = value;
		Il2CppCodeGenWriteBarrier(&____webExceptionStatusesToRetryOn_8, value);
	}

	inline static int32_t get_offset_of__errorCodesToRetryOn_10() { return static_cast<int32_t>(offsetof(DefaultRetryPolicy_t2207644155, ____errorCodesToRetryOn_10)); }
	inline Il2CppObject* get__errorCodesToRetryOn_10() const { return ____errorCodesToRetryOn_10; }
	inline Il2CppObject** get_address_of__errorCodesToRetryOn_10() { return &____errorCodesToRetryOn_10; }
	inline void set__errorCodesToRetryOn_10(Il2CppObject* value)
	{
		____errorCodesToRetryOn_10 = value;
		Il2CppCodeGenWriteBarrier(&____errorCodesToRetryOn_10, value);
	}
};

struct DefaultRetryPolicy_t2207644155_StaticFields
{
public:
	// Amazon.Runtime.Internal.CapacityManager Amazon.Runtime.Internal.DefaultRetryPolicy::_capacityManagerInstance
	CapacityManager_t2181902141 * ____capacityManagerInstance_4;
	// System.Collections.Generic.HashSet`1<System.String> Amazon.Runtime.Internal.DefaultRetryPolicy::_coreCLRRetryErrorMessages
	HashSet_1_t362681087 * ____coreCLRRetryErrorMessages_9;

public:
	inline static int32_t get_offset_of__capacityManagerInstance_4() { return static_cast<int32_t>(offsetof(DefaultRetryPolicy_t2207644155_StaticFields, ____capacityManagerInstance_4)); }
	inline CapacityManager_t2181902141 * get__capacityManagerInstance_4() const { return ____capacityManagerInstance_4; }
	inline CapacityManager_t2181902141 ** get_address_of__capacityManagerInstance_4() { return &____capacityManagerInstance_4; }
	inline void set__capacityManagerInstance_4(CapacityManager_t2181902141 * value)
	{
		____capacityManagerInstance_4 = value;
		Il2CppCodeGenWriteBarrier(&____capacityManagerInstance_4, value);
	}

	inline static int32_t get_offset_of__coreCLRRetryErrorMessages_9() { return static_cast<int32_t>(offsetof(DefaultRetryPolicy_t2207644155_StaticFields, ____coreCLRRetryErrorMessages_9)); }
	inline HashSet_1_t362681087 * get__coreCLRRetryErrorMessages_9() const { return ____coreCLRRetryErrorMessages_9; }
	inline HashSet_1_t362681087 ** get_address_of__coreCLRRetryErrorMessages_9() { return &____coreCLRRetryErrorMessages_9; }
	inline void set__coreCLRRetryErrorMessages_9(HashSet_1_t362681087 * value)
	{
		____coreCLRRetryErrorMessages_9 = value;
		Il2CppCodeGenWriteBarrier(&____coreCLRRetryErrorMessages_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
