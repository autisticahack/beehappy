﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Queue_1_Enumerat3019169210MethodDeclarations.h"

// System.Void System.Collections.Generic.Queue`1/Enumerator<Amazon.Runtime.Internal.Util.ThreadPoolThrottler`1/ThreadPoolOptions`1<System.Object,System.Object>>::.ctor(System.Collections.Generic.Queue`1<T>)
#define Enumerator__ctor_m3167468371(__this, ___q0, method) ((  void (*) (Enumerator_t1913226750 *, Queue_1_t1403163670 *, const MethodInfo*))Enumerator__ctor_m677001007_gshared)(__this, ___q0, method)
// System.Void System.Collections.Generic.Queue`1/Enumerator<Amazon.Runtime.Internal.Util.ThreadPoolThrottler`1/ThreadPoolOptions`1<System.Object,System.Object>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2262714498(__this, method) ((  void (*) (Enumerator_t1913226750 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m373072478_gshared)(__this, method)
// System.Object System.Collections.Generic.Queue`1/Enumerator<Amazon.Runtime.Internal.Util.ThreadPoolThrottler`1/ThreadPoolOptions`1<System.Object,System.Object>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1616668508(__this, method) ((  Il2CppObject * (*) (Enumerator_t1913226750 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2167685344_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1/Enumerator<Amazon.Runtime.Internal.Util.ThreadPoolThrottler`1/ThreadPoolOptions`1<System.Object,System.Object>>::Dispose()
#define Enumerator_Dispose_m427269569(__this, method) ((  void (*) (Enumerator_t1913226750 *, const MethodInfo*))Enumerator_Dispose_m575349149_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Queue`1/Enumerator<Amazon.Runtime.Internal.Util.ThreadPoolThrottler`1/ThreadPoolOptions`1<System.Object,System.Object>>::MoveNext()
#define Enumerator_MoveNext_m563803146(__this, method) ((  bool (*) (Enumerator_t1913226750 *, const MethodInfo*))Enumerator_MoveNext_m742418190_gshared)(__this, method)
// T System.Collections.Generic.Queue`1/Enumerator<Amazon.Runtime.Internal.Util.ThreadPoolThrottler`1/ThreadPoolOptions`1<System.Object,System.Object>>::get_Current()
#define Enumerator_get_Current_m754961969(__this, method) ((  ThreadPoolOptions_1_t1583506835 * (*) (Enumerator_t1913226750 *, const MethodInfo*))Enumerator_get_Current_m1613610405_gshared)(__this, method)
