﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.HashSet`1<System.Net.WebExceptionStatus>
struct HashSet_1_t3797801681;
// System.Collections.Generic.IEqualityComparer`1<System.Net.WebExceptionStatus>
struct IEqualityComparer_1_t382006309;
// System.Collections.Generic.IEnumerable`1<System.Net.WebExceptionStatus>
struct IEnumerable_1_t1461500576;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// System.Collections.Generic.IEnumerator`1<System.Net.WebExceptionStatus>
struct IEnumerator_1_t2939864654;
// System.Net.WebExceptionStatus[]
struct WebExceptionStatusU5BU5D_t24880858;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serialization228987430.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"
#include "System_System_Net_WebExceptionStatus1169373531.h"
#include "mscorlib_System_Object2689449295.h"
#include "System_Core_System_Collections_Generic_HashSet_1_E2286117523.h"

// System.Void System.Collections.Generic.HashSet`1<System.Net.WebExceptionStatus>::.ctor()
extern "C"  void HashSet_1__ctor_m3366937246_gshared (HashSet_1_t3797801681 * __this, const MethodInfo* method);
#define HashSet_1__ctor_m3366937246(__this, method) ((  void (*) (HashSet_1_t3797801681 *, const MethodInfo*))HashSet_1__ctor_m3366937246_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<System.Net.WebExceptionStatus>::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
extern "C"  void HashSet_1__ctor_m3961040332_gshared (HashSet_1_t3797801681 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define HashSet_1__ctor_m3961040332(__this, ___comparer0, method) ((  void (*) (HashSet_1_t3797801681 *, Il2CppObject*, const MethodInfo*))HashSet_1__ctor_m3961040332_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.HashSet`1<System.Net.WebExceptionStatus>::.ctor(System.Collections.Generic.IEnumerable`1<T>,System.Collections.Generic.IEqualityComparer`1<T>)
extern "C"  void HashSet_1__ctor_m3935865569_gshared (HashSet_1_t3797801681 * __this, Il2CppObject* ___collection0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define HashSet_1__ctor_m3935865569(__this, ___collection0, ___comparer1, method) ((  void (*) (HashSet_1_t3797801681 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))HashSet_1__ctor_m3935865569_gshared)(__this, ___collection0, ___comparer1, method)
// System.Void System.Collections.Generic.HashSet`1<System.Net.WebExceptionStatus>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void HashSet_1__ctor_m4239270373_gshared (HashSet_1_t3797801681 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define HashSet_1__ctor_m4239270373(__this, ___info0, ___context1, method) ((  void (*) (HashSet_1_t3797801681 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))HashSet_1__ctor_m4239270373_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1<System.Net.WebExceptionStatus>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1684330102_gshared (HashSet_1_t3797801681 * __this, const MethodInfo* method);
#define HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1684330102(__this, method) ((  Il2CppObject* (*) (HashSet_1_t3797801681 *, const MethodInfo*))HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1684330102_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Net.WebExceptionStatus>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1658743907_gshared (HashSet_1_t3797801681 * __this, const MethodInfo* method);
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1658743907(__this, method) ((  bool (*) (HashSet_1_t3797801681 *, const MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1658743907_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<System.Net.WebExceptionStatus>::System.Collections.Generic.ICollection<T>.CopyTo(T[],System.Int32)
extern "C"  void HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m3620626275_gshared (HashSet_1_t3797801681 * __this, WebExceptionStatusU5BU5D_t24880858* ___array0, int32_t ___index1, const MethodInfo* method);
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m3620626275(__this, ___array0, ___index1, method) ((  void (*) (HashSet_1_t3797801681 *, WebExceptionStatusU5BU5D_t24880858*, int32_t, const MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m3620626275_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.HashSet`1<System.Net.WebExceptionStatus>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2606793727_gshared (HashSet_1_t3797801681 * __this, int32_t ___item0, const MethodInfo* method);
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2606793727(__this, ___item0, method) ((  void (*) (HashSet_1_t3797801681 *, int32_t, const MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2606793727_gshared)(__this, ___item0, method)
// System.Collections.IEnumerator System.Collections.Generic.HashSet`1<System.Net.WebExceptionStatus>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * HashSet_1_System_Collections_IEnumerable_GetEnumerator_m1854152479_gshared (HashSet_1_t3797801681 * __this, const MethodInfo* method);
#define HashSet_1_System_Collections_IEnumerable_GetEnumerator_m1854152479(__this, method) ((  Il2CppObject * (*) (HashSet_1_t3797801681 *, const MethodInfo*))HashSet_1_System_Collections_IEnumerable_GetEnumerator_m1854152479_gshared)(__this, method)
// System.Int32 System.Collections.Generic.HashSet`1<System.Net.WebExceptionStatus>::get_Count()
extern "C"  int32_t HashSet_1_get_Count_m2927349850_gshared (HashSet_1_t3797801681 * __this, const MethodInfo* method);
#define HashSet_1_get_Count_m2927349850(__this, method) ((  int32_t (*) (HashSet_1_t3797801681 *, const MethodInfo*))HashSet_1_get_Count_m2927349850_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<System.Net.WebExceptionStatus>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<T>)
extern "C"  void HashSet_1_Init_m4121070761_gshared (HashSet_1_t3797801681 * __this, int32_t ___capacity0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define HashSet_1_Init_m4121070761(__this, ___capacity0, ___comparer1, method) ((  void (*) (HashSet_1_t3797801681 *, int32_t, Il2CppObject*, const MethodInfo*))HashSet_1_Init_m4121070761_gshared)(__this, ___capacity0, ___comparer1, method)
// System.Void System.Collections.Generic.HashSet`1<System.Net.WebExceptionStatus>::InitArrays(System.Int32)
extern "C"  void HashSet_1_InitArrays_m1838231979_gshared (HashSet_1_t3797801681 * __this, int32_t ___size0, const MethodInfo* method);
#define HashSet_1_InitArrays_m1838231979(__this, ___size0, method) ((  void (*) (HashSet_1_t3797801681 *, int32_t, const MethodInfo*))HashSet_1_InitArrays_m1838231979_gshared)(__this, ___size0, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Net.WebExceptionStatus>::SlotsContainsAt(System.Int32,System.Int32,T)
extern "C"  bool HashSet_1_SlotsContainsAt_m3596432911_gshared (HashSet_1_t3797801681 * __this, int32_t ___index0, int32_t ___hash1, int32_t ___item2, const MethodInfo* method);
#define HashSet_1_SlotsContainsAt_m3596432911(__this, ___index0, ___hash1, ___item2, method) ((  bool (*) (HashSet_1_t3797801681 *, int32_t, int32_t, int32_t, const MethodInfo*))HashSet_1_SlotsContainsAt_m3596432911_gshared)(__this, ___index0, ___hash1, ___item2, method)
// System.Void System.Collections.Generic.HashSet`1<System.Net.WebExceptionStatus>::CopyTo(T[],System.Int32)
extern "C"  void HashSet_1_CopyTo_m3874129811_gshared (HashSet_1_t3797801681 * __this, WebExceptionStatusU5BU5D_t24880858* ___array0, int32_t ___index1, const MethodInfo* method);
#define HashSet_1_CopyTo_m3874129811(__this, ___array0, ___index1, method) ((  void (*) (HashSet_1_t3797801681 *, WebExceptionStatusU5BU5D_t24880858*, int32_t, const MethodInfo*))HashSet_1_CopyTo_m3874129811_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.HashSet`1<System.Net.WebExceptionStatus>::CopyTo(T[],System.Int32,System.Int32)
extern "C"  void HashSet_1_CopyTo_m2242815898_gshared (HashSet_1_t3797801681 * __this, WebExceptionStatusU5BU5D_t24880858* ___array0, int32_t ___index1, int32_t ___count2, const MethodInfo* method);
#define HashSet_1_CopyTo_m2242815898(__this, ___array0, ___index1, ___count2, method) ((  void (*) (HashSet_1_t3797801681 *, WebExceptionStatusU5BU5D_t24880858*, int32_t, int32_t, const MethodInfo*))HashSet_1_CopyTo_m2242815898_gshared)(__this, ___array0, ___index1, ___count2, method)
// System.Void System.Collections.Generic.HashSet`1<System.Net.WebExceptionStatus>::Resize()
extern "C"  void HashSet_1_Resize_m631769662_gshared (HashSet_1_t3797801681 * __this, const MethodInfo* method);
#define HashSet_1_Resize_m631769662(__this, method) ((  void (*) (HashSet_1_t3797801681 *, const MethodInfo*))HashSet_1_Resize_m631769662_gshared)(__this, method)
// System.Int32 System.Collections.Generic.HashSet`1<System.Net.WebExceptionStatus>::GetLinkHashCode(System.Int32)
extern "C"  int32_t HashSet_1_GetLinkHashCode_m285765110_gshared (HashSet_1_t3797801681 * __this, int32_t ___index0, const MethodInfo* method);
#define HashSet_1_GetLinkHashCode_m285765110(__this, ___index0, method) ((  int32_t (*) (HashSet_1_t3797801681 *, int32_t, const MethodInfo*))HashSet_1_GetLinkHashCode_m285765110_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.Generic.HashSet`1<System.Net.WebExceptionStatus>::GetItemHashCode(T)
extern "C"  int32_t HashSet_1_GetItemHashCode_m2633090414_gshared (HashSet_1_t3797801681 * __this, int32_t ___item0, const MethodInfo* method);
#define HashSet_1_GetItemHashCode_m2633090414(__this, ___item0, method) ((  int32_t (*) (HashSet_1_t3797801681 *, int32_t, const MethodInfo*))HashSet_1_GetItemHashCode_m2633090414_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Net.WebExceptionStatus>::Add(T)
extern "C"  bool HashSet_1_Add_m2875731328_gshared (HashSet_1_t3797801681 * __this, int32_t ___item0, const MethodInfo* method);
#define HashSet_1_Add_m2875731328(__this, ___item0, method) ((  bool (*) (HashSet_1_t3797801681 *, int32_t, const MethodInfo*))HashSet_1_Add_m2875731328_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.HashSet`1<System.Net.WebExceptionStatus>::Clear()
extern "C"  void HashSet_1_Clear_m1136913467_gshared (HashSet_1_t3797801681 * __this, const MethodInfo* method);
#define HashSet_1_Clear_m1136913467(__this, method) ((  void (*) (HashSet_1_t3797801681 *, const MethodInfo*))HashSet_1_Clear_m1136913467_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Net.WebExceptionStatus>::Contains(T)
extern "C"  bool HashSet_1_Contains_m1267968165_gshared (HashSet_1_t3797801681 * __this, int32_t ___item0, const MethodInfo* method);
#define HashSet_1_Contains_m1267968165(__this, ___item0, method) ((  bool (*) (HashSet_1_t3797801681 *, int32_t, const MethodInfo*))HashSet_1_Contains_m1267968165_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Net.WebExceptionStatus>::Remove(T)
extern "C"  bool HashSet_1_Remove_m2232758942_gshared (HashSet_1_t3797801681 * __this, int32_t ___item0, const MethodInfo* method);
#define HashSet_1_Remove_m2232758942(__this, ___item0, method) ((  bool (*) (HashSet_1_t3797801681 *, int32_t, const MethodInfo*))HashSet_1_Remove_m2232758942_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.HashSet`1<System.Net.WebExceptionStatus>::IntersectWith(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void HashSet_1_IntersectWith_m166661430_gshared (HashSet_1_t3797801681 * __this, Il2CppObject* ___other0, const MethodInfo* method);
#define HashSet_1_IntersectWith_m166661430(__this, ___other0, method) ((  void (*) (HashSet_1_t3797801681 *, Il2CppObject*, const MethodInfo*))HashSet_1_IntersectWith_m166661430_gshared)(__this, ___other0, method)
// System.Void System.Collections.Generic.HashSet`1<System.Net.WebExceptionStatus>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void HashSet_1_GetObjectData_m2366277184_gshared (HashSet_1_t3797801681 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define HashSet_1_GetObjectData_m2366277184(__this, ___info0, ___context1, method) ((  void (*) (HashSet_1_t3797801681 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))HashSet_1_GetObjectData_m2366277184_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.HashSet`1<System.Net.WebExceptionStatus>::OnDeserialization(System.Object)
extern "C"  void HashSet_1_OnDeserialization_m2423483230_gshared (HashSet_1_t3797801681 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define HashSet_1_OnDeserialization_m2423483230(__this, ___sender0, method) ((  void (*) (HashSet_1_t3797801681 *, Il2CppObject *, const MethodInfo*))HashSet_1_OnDeserialization_m2423483230_gshared)(__this, ___sender0, method)
// System.Collections.Generic.HashSet`1/Enumerator<T> System.Collections.Generic.HashSet`1<System.Net.WebExceptionStatus>::GetEnumerator()
extern "C"  Enumerator_t2286117523  HashSet_1_GetEnumerator_m2974532628_gshared (HashSet_1_t3797801681 * __this, const MethodInfo* method);
#define HashSet_1_GetEnumerator_m2974532628(__this, method) ((  Enumerator_t2286117523  (*) (HashSet_1_t3797801681 *, const MethodInfo*))HashSet_1_GetEnumerator_m2974532628_gshared)(__this, method)
