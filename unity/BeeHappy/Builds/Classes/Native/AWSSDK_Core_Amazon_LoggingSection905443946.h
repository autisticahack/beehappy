﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Type
struct Type_t;

#include "mscorlib_System_Object2689449295.h"
#include "AWSSDK_Core_Amazon_LoggingOptions2865640765.h"
#include "AWSSDK_Core_Amazon_ResponseLoggingOption3443611737.h"
#include "mscorlib_System_Nullable_1_gen334943763.h"
#include "mscorlib_System_Nullable_1_gen2088641033.h"
#include "AWSSDK_Core_Amazon_LogMetricsFormatOption97749509.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.LoggingSection
struct  LoggingSection_t905443946  : public Il2CppObject
{
public:
	// Amazon.LoggingOptions Amazon.LoggingSection::<LogTo>k__BackingField
	int32_t ___U3CLogToU3Ek__BackingField_0;
	// Amazon.ResponseLoggingOption Amazon.LoggingSection::<LogResponses>k__BackingField
	int32_t ___U3CLogResponsesU3Ek__BackingField_1;
	// System.Nullable`1<System.Int32> Amazon.LoggingSection::<LogResponsesSizeLimit>k__BackingField
	Nullable_1_t334943763  ___U3CLogResponsesSizeLimitU3Ek__BackingField_2;
	// System.Nullable`1<System.Boolean> Amazon.LoggingSection::<LogMetrics>k__BackingField
	Nullable_1_t2088641033  ___U3CLogMetricsU3Ek__BackingField_3;
	// Amazon.LogMetricsFormatOption Amazon.LoggingSection::<LogMetricsFormat>k__BackingField
	int32_t ___U3CLogMetricsFormatU3Ek__BackingField_4;
	// System.Type Amazon.LoggingSection::<LogMetricsCustomFormatter>k__BackingField
	Type_t * ___U3CLogMetricsCustomFormatterU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CLogToU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(LoggingSection_t905443946, ___U3CLogToU3Ek__BackingField_0)); }
	inline int32_t get_U3CLogToU3Ek__BackingField_0() const { return ___U3CLogToU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CLogToU3Ek__BackingField_0() { return &___U3CLogToU3Ek__BackingField_0; }
	inline void set_U3CLogToU3Ek__BackingField_0(int32_t value)
	{
		___U3CLogToU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CLogResponsesU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(LoggingSection_t905443946, ___U3CLogResponsesU3Ek__BackingField_1)); }
	inline int32_t get_U3CLogResponsesU3Ek__BackingField_1() const { return ___U3CLogResponsesU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CLogResponsesU3Ek__BackingField_1() { return &___U3CLogResponsesU3Ek__BackingField_1; }
	inline void set_U3CLogResponsesU3Ek__BackingField_1(int32_t value)
	{
		___U3CLogResponsesU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CLogResponsesSizeLimitU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(LoggingSection_t905443946, ___U3CLogResponsesSizeLimitU3Ek__BackingField_2)); }
	inline Nullable_1_t334943763  get_U3CLogResponsesSizeLimitU3Ek__BackingField_2() const { return ___U3CLogResponsesSizeLimitU3Ek__BackingField_2; }
	inline Nullable_1_t334943763 * get_address_of_U3CLogResponsesSizeLimitU3Ek__BackingField_2() { return &___U3CLogResponsesSizeLimitU3Ek__BackingField_2; }
	inline void set_U3CLogResponsesSizeLimitU3Ek__BackingField_2(Nullable_1_t334943763  value)
	{
		___U3CLogResponsesSizeLimitU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CLogMetricsU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(LoggingSection_t905443946, ___U3CLogMetricsU3Ek__BackingField_3)); }
	inline Nullable_1_t2088641033  get_U3CLogMetricsU3Ek__BackingField_3() const { return ___U3CLogMetricsU3Ek__BackingField_3; }
	inline Nullable_1_t2088641033 * get_address_of_U3CLogMetricsU3Ek__BackingField_3() { return &___U3CLogMetricsU3Ek__BackingField_3; }
	inline void set_U3CLogMetricsU3Ek__BackingField_3(Nullable_1_t2088641033  value)
	{
		___U3CLogMetricsU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CLogMetricsFormatU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(LoggingSection_t905443946, ___U3CLogMetricsFormatU3Ek__BackingField_4)); }
	inline int32_t get_U3CLogMetricsFormatU3Ek__BackingField_4() const { return ___U3CLogMetricsFormatU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CLogMetricsFormatU3Ek__BackingField_4() { return &___U3CLogMetricsFormatU3Ek__BackingField_4; }
	inline void set_U3CLogMetricsFormatU3Ek__BackingField_4(int32_t value)
	{
		___U3CLogMetricsFormatU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CLogMetricsCustomFormatterU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(LoggingSection_t905443946, ___U3CLogMetricsCustomFormatterU3Ek__BackingField_5)); }
	inline Type_t * get_U3CLogMetricsCustomFormatterU3Ek__BackingField_5() const { return ___U3CLogMetricsCustomFormatterU3Ek__BackingField_5; }
	inline Type_t ** get_address_of_U3CLogMetricsCustomFormatterU3Ek__BackingField_5() { return &___U3CLogMetricsCustomFormatterU3Ek__BackingField_5; }
	inline void set_U3CLogMetricsCustomFormatterU3Ek__BackingField_5(Type_t * value)
	{
		___U3CLogMetricsCustomFormatterU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CLogMetricsCustomFormatterU3Ek__BackingField_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
