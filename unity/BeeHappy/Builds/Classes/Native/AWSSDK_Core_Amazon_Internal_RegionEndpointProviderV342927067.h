﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ThirdParty.Json.LitJson.JsonData
struct JsonData_t4263252052;
// System.Collections.Generic.Dictionary`2<System.String,Amazon.Internal.IRegionEndpoint>
struct Dictionary_2_t2459213150;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Internal.RegionEndpointProviderV3
struct  RegionEndpointProviderV3_t342927067  : public Il2CppObject
{
public:
	// ThirdParty.Json.LitJson.JsonData Amazon.Internal.RegionEndpointProviderV3::_root
	JsonData_t4263252052 * ____root_0;
	// System.Collections.Generic.Dictionary`2<System.String,Amazon.Internal.IRegionEndpoint> Amazon.Internal.RegionEndpointProviderV3::_regionEndpointMap
	Dictionary_2_t2459213150 * ____regionEndpointMap_1;
	// System.Object Amazon.Internal.RegionEndpointProviderV3::_regionEndpointMapLock
	Il2CppObject * ____regionEndpointMapLock_2;
	// System.Object Amazon.Internal.RegionEndpointProviderV3::_allRegionEndpointsLock
	Il2CppObject * ____allRegionEndpointsLock_3;

public:
	inline static int32_t get_offset_of__root_0() { return static_cast<int32_t>(offsetof(RegionEndpointProviderV3_t342927067, ____root_0)); }
	inline JsonData_t4263252052 * get__root_0() const { return ____root_0; }
	inline JsonData_t4263252052 ** get_address_of__root_0() { return &____root_0; }
	inline void set__root_0(JsonData_t4263252052 * value)
	{
		____root_0 = value;
		Il2CppCodeGenWriteBarrier(&____root_0, value);
	}

	inline static int32_t get_offset_of__regionEndpointMap_1() { return static_cast<int32_t>(offsetof(RegionEndpointProviderV3_t342927067, ____regionEndpointMap_1)); }
	inline Dictionary_2_t2459213150 * get__regionEndpointMap_1() const { return ____regionEndpointMap_1; }
	inline Dictionary_2_t2459213150 ** get_address_of__regionEndpointMap_1() { return &____regionEndpointMap_1; }
	inline void set__regionEndpointMap_1(Dictionary_2_t2459213150 * value)
	{
		____regionEndpointMap_1 = value;
		Il2CppCodeGenWriteBarrier(&____regionEndpointMap_1, value);
	}

	inline static int32_t get_offset_of__regionEndpointMapLock_2() { return static_cast<int32_t>(offsetof(RegionEndpointProviderV3_t342927067, ____regionEndpointMapLock_2)); }
	inline Il2CppObject * get__regionEndpointMapLock_2() const { return ____regionEndpointMapLock_2; }
	inline Il2CppObject ** get_address_of__regionEndpointMapLock_2() { return &____regionEndpointMapLock_2; }
	inline void set__regionEndpointMapLock_2(Il2CppObject * value)
	{
		____regionEndpointMapLock_2 = value;
		Il2CppCodeGenWriteBarrier(&____regionEndpointMapLock_2, value);
	}

	inline static int32_t get_offset_of__allRegionEndpointsLock_3() { return static_cast<int32_t>(offsetof(RegionEndpointProviderV3_t342927067, ____allRegionEndpointsLock_3)); }
	inline Il2CppObject * get__allRegionEndpointsLock_3() const { return ____allRegionEndpointsLock_3; }
	inline Il2CppObject ** get_address_of__allRegionEndpointsLock_3() { return &____allRegionEndpointsLock_3; }
	inline void set__allRegionEndpointsLock_3(Il2CppObject * value)
	{
		____allRegionEndpointsLock_3 = value;
		Il2CppCodeGenWriteBarrier(&____allRegionEndpointsLock_3, value);
	}
};

struct RegionEndpointProviderV3_t342927067_StaticFields
{
public:
	// ThirdParty.Json.LitJson.JsonData Amazon.Internal.RegionEndpointProviderV3::_emptyDictionaryJsonData
	JsonData_t4263252052 * ____emptyDictionaryJsonData_4;

public:
	inline static int32_t get_offset_of__emptyDictionaryJsonData_4() { return static_cast<int32_t>(offsetof(RegionEndpointProviderV3_t342927067_StaticFields, ____emptyDictionaryJsonData_4)); }
	inline JsonData_t4263252052 * get__emptyDictionaryJsonData_4() const { return ____emptyDictionaryJsonData_4; }
	inline JsonData_t4263252052 ** get_address_of__emptyDictionaryJsonData_4() { return &____emptyDictionaryJsonData_4; }
	inline void set__emptyDictionaryJsonData_4(JsonData_t4263252052 * value)
	{
		____emptyDictionaryJsonData_4 = value;
		Il2CppCodeGenWriteBarrier(&____emptyDictionaryJsonData_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
