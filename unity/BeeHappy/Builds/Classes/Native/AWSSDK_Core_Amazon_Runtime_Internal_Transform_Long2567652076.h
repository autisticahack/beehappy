﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.Runtime.Internal.Transform.LongUnmarshaller
struct LongUnmarshaller_t2567652076;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Transform.LongUnmarshaller
struct  LongUnmarshaller_t2567652076  : public Il2CppObject
{
public:

public:
};

struct LongUnmarshaller_t2567652076_StaticFields
{
public:
	// Amazon.Runtime.Internal.Transform.LongUnmarshaller Amazon.Runtime.Internal.Transform.LongUnmarshaller::_instance
	LongUnmarshaller_t2567652076 * ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(LongUnmarshaller_t2567652076_StaticFields, ____instance_0)); }
	inline LongUnmarshaller_t2567652076 * get__instance_0() const { return ____instance_0; }
	inline LongUnmarshaller_t2567652076 ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(LongUnmarshaller_t2567652076 * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier(&____instance_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
