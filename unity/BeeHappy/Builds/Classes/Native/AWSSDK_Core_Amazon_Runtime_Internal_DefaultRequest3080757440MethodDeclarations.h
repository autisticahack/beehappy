﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.DefaultRequest
struct DefaultRequest_t3080757440;
// Amazon.Runtime.AmazonWebServiceRequest
struct AmazonWebServiceRequest_t3384026212;
// System.String
struct String_t;
// System.Collections.Generic.IDictionary`2<System.String,System.String>
struct IDictionary_2_t1943082916;
// System.Uri
struct Uri_t19570940;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.IO.Stream
struct Stream_t3255436806;
// Amazon.RegionEndpoint
struct RegionEndpoint_t661522805;
// Amazon.Runtime.Internal.Auth.AWS4SigningResult
struct AWS4SigningResult_t430803065;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_Runtime_AmazonWebServiceRequest3384026212.h"
#include "mscorlib_System_String2029220233.h"
#include "System_System_Uri19570940.h"
#include "mscorlib_System_IO_Stream3255436806.h"

// System.Void Amazon.Runtime.Internal.DefaultRequest::.ctor(Amazon.Runtime.AmazonWebServiceRequest,System.String)
extern "C"  void DefaultRequest__ctor_m3534427872 (DefaultRequest_t3080757440 * __this, AmazonWebServiceRequest_t3384026212 * ___request0, String_t* ___serviceName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Runtime.Internal.DefaultRequest::get_RequestName()
extern "C"  String_t* DefaultRequest_get_RequestName_m2176725708 (DefaultRequest_t3080757440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Runtime.Internal.DefaultRequest::get_HttpMethod()
extern "C"  String_t* DefaultRequest_get_HttpMethod_m1056297349 (DefaultRequest_t3080757440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.DefaultRequest::set_HttpMethod(System.String)
extern "C"  void DefaultRequest_set_HttpMethod_m2581561882 (DefaultRequest_t3080757440 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.DefaultRequest::get_UseQueryString()
extern "C"  bool DefaultRequest_get_UseQueryString_m3762516327 (DefaultRequest_t3080757440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.DefaultRequest::set_UseQueryString(System.Boolean)
extern "C"  void DefaultRequest_set_UseQueryString_m1647228068 (DefaultRequest_t3080757440 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.AmazonWebServiceRequest Amazon.Runtime.Internal.DefaultRequest::get_OriginalRequest()
extern "C"  AmazonWebServiceRequest_t3384026212 * DefaultRequest_get_OriginalRequest_m3208859366 (DefaultRequest_t3080757440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.Runtime.Internal.DefaultRequest::get_Headers()
extern "C"  Il2CppObject* DefaultRequest_get_Headers_m2869288332 (DefaultRequest_t3080757440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.Runtime.Internal.DefaultRequest::get_Parameters()
extern "C"  Il2CppObject* DefaultRequest_get_Parameters_m1555107000 (DefaultRequest_t3080757440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.Runtime.Internal.DefaultRequest::get_SubResources()
extern "C"  Il2CppObject* DefaultRequest_get_SubResources_m1038883135 (DefaultRequest_t3080757440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Uri Amazon.Runtime.Internal.DefaultRequest::get_Endpoint()
extern "C"  Uri_t19570940 * DefaultRequest_get_Endpoint_m1304705460 (DefaultRequest_t3080757440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.DefaultRequest::set_Endpoint(System.Uri)
extern "C"  void DefaultRequest_set_Endpoint_m43029849 (DefaultRequest_t3080757440 * __this, Uri_t19570940 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Runtime.Internal.DefaultRequest::get_ResourcePath()
extern "C"  String_t* DefaultRequest_get_ResourcePath_m2119989689 (DefaultRequest_t3080757440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.DefaultRequest::set_ResourcePath(System.String)
extern "C"  void DefaultRequest_set_ResourcePath_m391702752 (DefaultRequest_t3080757440 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Amazon.Runtime.Internal.DefaultRequest::get_Content()
extern "C"  ByteU5BU5D_t3397334013* DefaultRequest_get_Content_m2013537500 (DefaultRequest_t3080757440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.DefaultRequest::set_Content(System.Byte[])
extern "C"  void DefaultRequest_set_Content_m2786678813 (DefaultRequest_t3080757440 * __this, ByteU5BU5D_t3397334013* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.DefaultRequest::get_SetContentFromParameters()
extern "C"  bool DefaultRequest_get_SetContentFromParameters_m2255751478 (DefaultRequest_t3080757440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.DefaultRequest::set_SetContentFromParameters(System.Boolean)
extern "C"  void DefaultRequest_set_SetContentFromParameters_m396506127 (DefaultRequest_t3080757440 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream Amazon.Runtime.Internal.DefaultRequest::get_ContentStream()
extern "C"  Stream_t3255436806 * DefaultRequest_get_ContentStream_m964906984 (DefaultRequest_t3080757440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.DefaultRequest::set_ContentStream(System.IO.Stream)
extern "C"  void DefaultRequest_set_ContentStream_m1183100289 (DefaultRequest_t3080757440 * __this, Stream_t3255436806 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Amazon.Runtime.Internal.DefaultRequest::get_OriginalStreamPosition()
extern "C"  int64_t DefaultRequest_get_OriginalStreamPosition_m1767296630 (DefaultRequest_t3080757440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.DefaultRequest::set_OriginalStreamPosition(System.Int64)
extern "C"  void DefaultRequest_set_OriginalStreamPosition_m1705908705 (DefaultRequest_t3080757440 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Runtime.Internal.DefaultRequest::ComputeContentStreamHash()
extern "C"  String_t* DefaultRequest_ComputeContentStreamHash_m3127311037 (DefaultRequest_t3080757440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Runtime.Internal.DefaultRequest::get_ServiceName()
extern "C"  String_t* DefaultRequest_get_ServiceName_m3999003666 (DefaultRequest_t3080757440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.RegionEndpoint Amazon.Runtime.Internal.DefaultRequest::get_AlternateEndpoint()
extern "C"  RegionEndpoint_t661522805 * DefaultRequest_get_AlternateEndpoint_m1744791030 (DefaultRequest_t3080757440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.DefaultRequest::get_Suppress404Exceptions()
extern "C"  bool DefaultRequest_get_Suppress404Exceptions_m2719686480 (DefaultRequest_t3080757440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.Internal.Auth.AWS4SigningResult Amazon.Runtime.Internal.DefaultRequest::get_AWS4SignerResult()
extern "C"  AWS4SigningResult_t430803065 * DefaultRequest_get_AWS4SignerResult_m1692528296 (DefaultRequest_t3080757440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.DefaultRequest::get_UseChunkEncoding()
extern "C"  bool DefaultRequest_get_UseChunkEncoding_m2391851234 (DefaultRequest_t3080757440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.DefaultRequest::set_UseSigV4(System.Boolean)
extern "C"  void DefaultRequest_set_UseSigV4_m600563494 (DefaultRequest_t3080757440 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Runtime.Internal.DefaultRequest::get_AuthenticationRegion()
extern "C"  String_t* DefaultRequest_get_AuthenticationRegion_m1188928132 (DefaultRequest_t3080757440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.DefaultRequest::set_AuthenticationRegion(System.String)
extern "C"  void DefaultRequest_set_AuthenticationRegion_m325608953 (DefaultRequest_t3080757440 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.DefaultRequest::IsRequestStreamRewindable()
extern "C"  bool DefaultRequest_IsRequestStreamRewindable_m4075459126 (DefaultRequest_t3080757440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.DefaultRequest::MayContainRequestBody()
extern "C"  bool DefaultRequest_MayContainRequestBody_m3302463672 (DefaultRequest_t3080757440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.DefaultRequest::HasRequestBody()
extern "C"  bool DefaultRequest_HasRequestBody_m2804230993 (DefaultRequest_t3080757440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
