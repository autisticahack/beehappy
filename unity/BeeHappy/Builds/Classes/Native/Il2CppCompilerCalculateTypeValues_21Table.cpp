﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Events_UnityEventBase828812576.h"
#include "UnityEngine_UnityEngine_Events_UnityAction4025899511.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent408735097.h"
#include "UnityEngine_UnityEngine_UnityString276356480.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"
#include "UnityEngine_UnityEngine_WaitForSecondsRealtime2105307154.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Anim1808633952.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Fram1120735295.h"
#include "UnityEngine_UnityEngine_Internal_DefaultValueAttri1027170048.h"
#include "UnityEngine_UnityEngine_Internal_ExcludeFromDocsAtt665825653.h"
#include "UnityEngine_UnityEngine_Logger3328995178.h"
#include "UnityEngine_UnityEngine_Scripting_UsedByNativeCode3212052468.h"
#include "UnityEngine_UnityEngine_Scripting_RequiredByNative1913052472.h"
#include "UnityEngine_UnityEngine_Serialization_FormerlySeri3673080018.h"
#include "UnityEngine_UnityEngineInternal_TypeInferenceRules1810425448.h"
#include "UnityEngine_UnityEngineInternal_TypeInferenceRuleA1390152093.h"
#include "UnityEngine_UnityEngineInternal_GenericStack3718539591.h"
#include "UnityEngine_UnityEngineInternal_NetFxCoreExtension4275971970.h"
#include "AWSSDK_Core_U3CModuleU3E3783534214.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_ObjC468753822.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_ObjC_Libraries4218932343.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_Runtime1145037850.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_Runtime_U3CU3Ec88230275.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_Selector1939762325.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_Callbacks3084417356.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_Callbacks_Methods1187897474.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_NSBundle3599697655.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_NSData1166097363.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_NSDictionary3598984691.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_NSError1370871221.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_NSLocale2224424797.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_NSMutableDictionar171997941.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_NSNotification1437913478.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_NSNotificationCen3366908717.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_NSNumberFormatter3902210636.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_NSObject1518098886.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_NSTimeZone416177812.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_SKPayment4242751664.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_SKPaymentQueue538265951.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_SKPaymentTransact2918057744.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_SKProduct3309436227.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_SKProductsRequest3969930329.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_SKProductsRespons3540529521.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_AdBannerView1238242894.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_UIActionSheet2483183669.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_UIActivityViewCon2230362714.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_UIAlertView2175232939.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_UIApplication1857964820.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_UIDevice860038178.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_UIImage3580593017.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_UILocalNotificatio869364318.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_UIScreen4048045402.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_UIScreenMode609995313.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_UIUserNotificatio4227938467.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_UIView1452205135.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_UIViewController1891120779.h"
#include "AWSSDK_Core_ThirdParty_iOS4Unity_UIWindow800018578.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_JsonType808352724.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_JsonData4263252052.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_OrderedDiction1664192327.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_JsonException1457213491.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_PropertyMetada3287739986.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_ArrayMetadata1135078014.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_ObjectMetadata4058137740.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_ExporterFunc173265409.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_ImporterFunc850687278.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_WrapperFactory327905379.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_JsonMapper77474459.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_JsonMapper_U3C1591091833.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_JsonToken2445581255.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_JsonReader354941621.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_Condition2230619687.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_WriterContext1209007092.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_JsonWriter3014444111.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_FsmContext4275593467.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_Lexer954714164.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_Lexer_StateHan3489987002.h"
#include "AWSSDK_Core_ThirdParty_Ionic_Zlib_CRC32619824607.h"
#include "AWSSDK_Core_ThirdParty_Ionic_Zlib_CrcCalculatorStr2228597532.h"
#include "AWSSDK_Core_System_IO_InvalidDataException1057132120.h"
#include "AWSSDK_Core_Amazon_AWSConfigs909839638.h"
#include "AWSSDK_Core_Amazon_AWSConfigs_HttpClientOption4250830711.h"
#include "AWSSDK_Core_Amazon_AWSConfigs_U3CU3Ec__DisplayClas3765470194.h"
#include "AWSSDK_Core_Amazon_AWSConfigs_U3CU3Ec__DisplayClas3765470195.h"
#include "AWSSDK_Core_Amazon_AWSConfigs_U3CU3Ec__DisplayClas1056790079.h"
#include "AWSSDK_Core_Amazon_LoggingOptions2865640765.h"
#include "AWSSDK_Core_Amazon_ResponseLoggingOption3443611737.h"
#include "AWSSDK_Core_Amazon_LogMetricsFormatOption97749509.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2100 = { sizeof (UnityEventBase_t828812576), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2100[4] = 
{
	UnityEventBase_t828812576::get_offset_of_m_Calls_0(),
	UnityEventBase_t828812576::get_offset_of_m_PersistentCalls_1(),
	UnityEventBase_t828812576::get_offset_of_m_TypeName_2(),
	UnityEventBase_t828812576::get_offset_of_m_CallsDirty_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2101 = { sizeof (UnityAction_t4025899511), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2102 = { sizeof (UnityEvent_t408735097), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2102[1] = 
{
	UnityEvent_t408735097::get_offset_of_m_InvokeArray_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2103 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2104 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2104[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2105 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2106 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2106[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2107 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2108 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2108[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2109 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2110 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2110[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2111 = { sizeof (UnityString_t276356480), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2112 = { sizeof (Vector2_t2243707579)+ sizeof (Il2CppObject), sizeof(Vector2_t2243707579 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2112[3] = 
{
	Vector2_t2243707579::get_offset_of_x_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector2_t2243707579::get_offset_of_y_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2113 = { sizeof (Vector4_t2243707581)+ sizeof (Il2CppObject), sizeof(Vector4_t2243707581 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2113[5] = 
{
	0,
	Vector4_t2243707581::get_offset_of_x_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector4_t2243707581::get_offset_of_y_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector4_t2243707581::get_offset_of_z_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector4_t2243707581::get_offset_of_w_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2114 = { sizeof (WaitForSecondsRealtime_t2105307154), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2114[1] = 
{
	WaitForSecondsRealtime_t2105307154::get_offset_of_waitTime_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2115 = { sizeof (AnimationPlayableUtilities_t1808633952), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2116 = { sizeof (FrameData_t1120735295)+ sizeof (Il2CppObject), sizeof(FrameData_t1120735295 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2116[4] = 
{
	FrameData_t1120735295::get_offset_of_m_UpdateId_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameData_t1120735295::get_offset_of_m_Time_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameData_t1120735295::get_offset_of_m_LastTime_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameData_t1120735295::get_offset_of_m_TimeScale_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2117 = { sizeof (DefaultValueAttribute_t1027170048), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2117[1] = 
{
	DefaultValueAttribute_t1027170048::get_offset_of_DefaultValue_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2118 = { sizeof (ExcludeFromDocsAttribute_t665825653), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2119 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2120 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2121 = { sizeof (Logger_t3328995178), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2121[3] = 
{
	Logger_t3328995178::get_offset_of_U3ClogHandlerU3Ek__BackingField_0(),
	Logger_t3328995178::get_offset_of_U3ClogEnabledU3Ek__BackingField_1(),
	Logger_t3328995178::get_offset_of_U3CfilterLogTypeU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2122 = { sizeof (UsedByNativeCodeAttribute_t3212052468), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2123 = { sizeof (RequiredByNativeCodeAttribute_t1913052472), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2124 = { sizeof (FormerlySerializedAsAttribute_t3673080018), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2124[1] = 
{
	FormerlySerializedAsAttribute_t3673080018::get_offset_of_m_oldName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2125 = { sizeof (TypeInferenceRules_t1810425448)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2125[5] = 
{
	TypeInferenceRules_t1810425448::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2126 = { sizeof (TypeInferenceRuleAttribute_t1390152093), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2126[1] = 
{
	TypeInferenceRuleAttribute_t1390152093::get_offset_of__rule_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2127 = { sizeof (GenericStack_t3718539591), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2128 = { sizeof (NetFxCoreExtensions_t4275971970), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2129 = { sizeof (U3CModuleU3E_t3783534221), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2130 = { sizeof (ObjC_t468753822), -1, sizeof(ObjC_t468753822_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2130[12] = 
{
	ObjC_t468753822_StaticFields::get_offset_of_TimeIntervalSinceReferenceDateSelector_0(),
	ObjC_t468753822_StaticFields::get_offset_of_AbsoluteStringSelector_1(),
	ObjC_t468753822_StaticFields::get_offset_of_DoubleValueSelector_2(),
	ObjC_t468753822_StaticFields::get_offset_of_CountSelector_3(),
	ObjC_t468753822_StaticFields::get_offset_of_ObjectAtIndexSelector_4(),
	ObjC_t468753822_StaticFields::get_offset_of_AllObjectsSelector_5(),
	ObjC_t468753822_StaticFields::get_offset_of_ArrayWithObjects_CountSelector_6(),
	ObjC_t468753822_StaticFields::get_offset_of_SetWithArraySelector_7(),
	ObjC_t468753822_StaticFields::get_offset_of_InitWithCharacters_lengthSelector_8(),
	ObjC_t468753822_StaticFields::get_offset_of_URLWithStringSelector_9(),
	ObjC_t468753822_StaticFields::get_offset_of_DateWithTimeIntervalSinceReferenceDateSelector_10(),
	ObjC_t468753822_StaticFields::get_offset_of_InitWithDoubleSelector_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2131 = { sizeof (Libraries_t4218932343), -1, sizeof(Libraries_t4218932343_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2131[2] = 
{
	Libraries_t4218932343_StaticFields::get_offset_of_Foundation_0(),
	Libraries_t4218932343_StaticFields::get_offset_of_UIKit_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2132 = { sizeof (Runtime_t1145037850), -1, sizeof(Runtime_t1145037850_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2132[4] = 
{
	Runtime_t1145037850_StaticFields::get_offset_of__contructorLock_0(),
	Runtime_t1145037850_StaticFields::get_offset_of__objectLock_1(),
	Runtime_t1145037850_StaticFields::get_offset_of__constructors_2(),
	Runtime_t1145037850_StaticFields::get_offset_of__objects_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2133 = { sizeof (U3CU3Ec_t88230275), -1, sizeof(U3CU3Ec_t88230275_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2133[1] = 
{
	U3CU3Ec_t88230275_StaticFields::get_offset_of_U3CU3E9_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2134 = { sizeof (Selector_t1939762325), -1, sizeof(Selector_t1939762325_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2134[16] = 
{
	Selector_t1939762325_StaticFields::get_offset_of_Init_0(),
	Selector_t1939762325_StaticFields::get_offset_of_InitWithCoder_1(),
	Selector_t1939762325_StaticFields::get_offset_of_InitWithName_2(),
	Selector_t1939762325_StaticFields::get_offset_of_InitWithFrame_3(),
	Selector_t1939762325_StaticFields::get_offset_of_MethodSignatureForSelector_4(),
	Selector_t1939762325_StaticFields::get_offset_of_FrameLength_5(),
	Selector_t1939762325_StaticFields::get_offset_of_RetainCount_6(),
	Selector_t1939762325_StaticFields::get_offset_of_AllocHandle_7(),
	Selector_t1939762325_StaticFields::get_offset_of_ReleaseHandle_8(),
	Selector_t1939762325_StaticFields::get_offset_of_RetainHandle_9(),
	Selector_t1939762325_StaticFields::get_offset_of_AutoreleaseHandle_10(),
	Selector_t1939762325_StaticFields::get_offset_of_DoesNotRecognizeSelectorHandle_11(),
	Selector_t1939762325_StaticFields::get_offset_of_PerformSelectorOnMainThreadWithObjectWaitUntilDoneHandle_12(),
	Selector_t1939762325_StaticFields::get_offset_of_PerformSelectorWithObjectAfterDelayHandle_13(),
	Selector_t1939762325_StaticFields::get_offset_of_UTF8StringHandle_14(),
	Selector_t1939762325::get_offset_of_handle_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2135 = { sizeof (Callbacks_t3084417356), -1, sizeof(Callbacks_t3084417356_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2135[2] = 
{
	Callbacks_t3084417356_StaticFields::get_offset_of__delegates_0(),
	Callbacks_t3084417356_StaticFields::get_offset_of__callbacks_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2136 = { sizeof (Methods_t1187897474), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2137 = { sizeof (NSBundle_t3599697655), -1, sizeof(NSBundle_t3599697655_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2137[1] = 
{
	NSBundle_t3599697655_StaticFields::get_offset_of__classHandle_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2138 = { sizeof (NSData_t1166097363), -1, sizeof(NSData_t1166097363_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2138[1] = 
{
	NSData_t1166097363_StaticFields::get_offset_of__classHandle_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2139 = { sizeof (NSDictionary_t3598984691), -1, sizeof(NSDictionary_t3598984691_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2139[1] = 
{
	NSDictionary_t3598984691_StaticFields::get_offset_of__classHandle_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2140 = { sizeof (NSError_t1370871221), -1, sizeof(NSError_t1370871221_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2140[1] = 
{
	NSError_t1370871221_StaticFields::get_offset_of__classHandle_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2141 = { sizeof (NSLocale_t2224424797), -1, sizeof(NSLocale_t2224424797_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2141[1] = 
{
	NSLocale_t2224424797_StaticFields::get_offset_of__classHandle_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2142 = { sizeof (NSMutableDictionary_t171997941), -1, sizeof(NSMutableDictionary_t171997941_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2142[1] = 
{
	NSMutableDictionary_t171997941_StaticFields::get_offset_of__classHandle_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2143 = { sizeof (NSNotification_t1437913478), -1, sizeof(NSNotification_t1437913478_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2143[1] = 
{
	NSNotification_t1437913478_StaticFields::get_offset_of__classHandle_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2144 = { sizeof (NSNotificationCenter_t3366908717), -1, sizeof(NSNotificationCenter_t3366908717_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2144[1] = 
{
	NSNotificationCenter_t3366908717_StaticFields::get_offset_of__classHandle_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2145 = { sizeof (NSNumberFormatter_t3902210636), -1, sizeof(NSNumberFormatter_t3902210636_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2145[1] = 
{
	NSNumberFormatter_t3902210636_StaticFields::get_offset_of__classHandle_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2146 = { sizeof (NSObject_t1518098886), -1, sizeof(NSObject_t1518098886_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2146[3] = 
{
	NSObject_t1518098886_StaticFields::get_offset_of__classHandle_0(),
	NSObject_t1518098886::get_offset_of_Handle_1(),
	NSObject_t1518098886::get_offset_of__shouldRelease_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2147 = { sizeof (NSTimeZone_t416177812), -1, sizeof(NSTimeZone_t416177812_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2147[1] = 
{
	NSTimeZone_t416177812_StaticFields::get_offset_of__classHandle_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2148 = { sizeof (SKPayment_t4242751664), -1, sizeof(SKPayment_t4242751664_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2148[1] = 
{
	SKPayment_t4242751664_StaticFields::get_offset_of__classHandle_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2149 = { sizeof (SKPaymentQueue_t538265951), -1, sizeof(SKPaymentQueue_t538265951_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2149[1] = 
{
	SKPaymentQueue_t538265951_StaticFields::get_offset_of__classHandle_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2150 = { sizeof (SKPaymentTransaction_t2918057744), -1, sizeof(SKPaymentTransaction_t2918057744_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2150[1] = 
{
	SKPaymentTransaction_t2918057744_StaticFields::get_offset_of__classHandle_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2151 = { sizeof (SKProduct_t3309436227), -1, sizeof(SKProduct_t3309436227_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2151[1] = 
{
	SKProduct_t3309436227_StaticFields::get_offset_of__classHandle_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2152 = { sizeof (SKProductsRequest_t3969930329), -1, sizeof(SKProductsRequest_t3969930329_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2152[1] = 
{
	SKProductsRequest_t3969930329_StaticFields::get_offset_of__classHandle_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2153 = { sizeof (SKProductsResponse_t3540529521), -1, sizeof(SKProductsResponse_t3540529521_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2153[1] = 
{
	SKProductsResponse_t3540529521_StaticFields::get_offset_of__classHandle_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2154 = { sizeof (AdBannerView_t1238242894), -1, sizeof(AdBannerView_t1238242894_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2154[1] = 
{
	AdBannerView_t1238242894_StaticFields::get_offset_of__classHandle_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2155 = { sizeof (UIActionSheet_t2483183669), -1, sizeof(UIActionSheet_t2483183669_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2155[1] = 
{
	UIActionSheet_t2483183669_StaticFields::get_offset_of__classHandle_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2156 = { sizeof (UIActivityViewController_t2230362714), -1, sizeof(UIActivityViewController_t2230362714_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2156[1] = 
{
	UIActivityViewController_t2230362714_StaticFields::get_offset_of__classHandle_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2157 = { sizeof (UIAlertView_t2175232939), -1, sizeof(UIAlertView_t2175232939_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2157[1] = 
{
	UIAlertView_t2175232939_StaticFields::get_offset_of__classHandle_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2158 = { sizeof (UIApplication_t1857964820), -1, sizeof(UIApplication_t1857964820_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2158[1] = 
{
	UIApplication_t1857964820_StaticFields::get_offset_of__classHandle_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2159 = { sizeof (UIDevice_t860038178), -1, sizeof(UIDevice_t860038178_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2159[3] = 
{
	UIDevice_t860038178_StaticFields::get_offset_of__classHandle_3(),
	UIDevice_t860038178_StaticFields::get_offset_of__majorVersion_4(),
	UIDevice_t860038178_StaticFields::get_offset_of__minorVersion_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2160 = { sizeof (UIImage_t3580593017), -1, sizeof(UIImage_t3580593017_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2160[1] = 
{
	UIImage_t3580593017_StaticFields::get_offset_of__classHandle_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2161 = { sizeof (UILocalNotification_t869364318), -1, sizeof(UILocalNotification_t869364318_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2161[1] = 
{
	UILocalNotification_t869364318_StaticFields::get_offset_of__classHandle_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2162 = { sizeof (UIScreen_t4048045402), -1, sizeof(UIScreen_t4048045402_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2162[1] = 
{
	UIScreen_t4048045402_StaticFields::get_offset_of__classHandle_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2163 = { sizeof (UIScreenMode_t609995313), -1, sizeof(UIScreenMode_t609995313_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2163[1] = 
{
	UIScreenMode_t609995313_StaticFields::get_offset_of__classHandle_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2164 = { sizeof (UIUserNotificationSettings_t4227938467), -1, sizeof(UIUserNotificationSettings_t4227938467_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2164[1] = 
{
	UIUserNotificationSettings_t4227938467_StaticFields::get_offset_of__classHandle_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2165 = { sizeof (UIView_t1452205135), -1, sizeof(UIView_t1452205135_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2165[1] = 
{
	UIView_t1452205135_StaticFields::get_offset_of__classHandle_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2166 = { sizeof (UIViewController_t1891120779), -1, sizeof(UIViewController_t1891120779_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2166[1] = 
{
	UIViewController_t1891120779_StaticFields::get_offset_of__classHandle_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2167 = { sizeof (UIWindow_t800018578), -1, sizeof(UIWindow_t800018578_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2167[1] = 
{
	UIWindow_t800018578_StaticFields::get_offset_of__classHandle_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2168 = { sizeof (JsonType_t808352724)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2168[11] = 
{
	JsonType_t808352724::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2169 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2170 = { sizeof (JsonData_t4263252052), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2170[9] = 
{
	JsonData_t4263252052::get_offset_of_inst_array_0(),
	JsonData_t4263252052::get_offset_of_inst_boolean_1(),
	JsonData_t4263252052::get_offset_of_inst_double_2(),
	JsonData_t4263252052::get_offset_of_inst_number_3(),
	JsonData_t4263252052::get_offset_of_inst_object_4(),
	JsonData_t4263252052::get_offset_of_inst_string_5(),
	JsonData_t4263252052::get_offset_of_json_6(),
	JsonData_t4263252052::get_offset_of_type_7(),
	JsonData_t4263252052::get_offset_of_object_list_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2171 = { sizeof (OrderedDictionaryEnumerator_t1664192327), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2171[1] = 
{
	OrderedDictionaryEnumerator_t1664192327::get_offset_of_list_enumerator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2172 = { sizeof (JsonException_t1457213491), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2173 = { sizeof (PropertyMetadata_t3287739986)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2173[3] = 
{
	PropertyMetadata_t3287739986::get_offset_of_Info_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PropertyMetadata_t3287739986::get_offset_of_IsField_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PropertyMetadata_t3287739986::get_offset_of_Type_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2174 = { sizeof (ArrayMetadata_t1135078014)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2174[3] = 
{
	ArrayMetadata_t1135078014::get_offset_of_element_type_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ArrayMetadata_t1135078014::get_offset_of_is_array_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ArrayMetadata_t1135078014::get_offset_of_is_list_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2175 = { sizeof (ObjectMetadata_t4058137740)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2175[3] = 
{
	ObjectMetadata_t4058137740::get_offset_of_element_type_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ObjectMetadata_t4058137740::get_offset_of_is_dictionary_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ObjectMetadata_t4058137740::get_offset_of_properties_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2176 = { sizeof (ExporterFunc_t173265409), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2177 = { sizeof (ImporterFunc_t850687278), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2178 = { sizeof (WrapperFactory_t327905379), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2179 = { sizeof (JsonMapper_t77474459), -1, sizeof(JsonMapper_t77474459_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2179[17] = 
{
	JsonMapper_t77474459_StaticFields::get_offset_of_max_nesting_depth_0(),
	JsonMapper_t77474459_StaticFields::get_offset_of_datetime_format_1(),
	JsonMapper_t77474459_StaticFields::get_offset_of_base_exporters_table_2(),
	JsonMapper_t77474459_StaticFields::get_offset_of_custom_exporters_table_3(),
	JsonMapper_t77474459_StaticFields::get_offset_of_base_importers_table_4(),
	JsonMapper_t77474459_StaticFields::get_offset_of_custom_importers_table_5(),
	JsonMapper_t77474459_StaticFields::get_offset_of_array_metadata_6(),
	JsonMapper_t77474459_StaticFields::get_offset_of_array_metadata_lock_7(),
	JsonMapper_t77474459_StaticFields::get_offset_of_conv_ops_8(),
	JsonMapper_t77474459_StaticFields::get_offset_of_conv_ops_lock_9(),
	JsonMapper_t77474459_StaticFields::get_offset_of_object_metadata_10(),
	JsonMapper_t77474459_StaticFields::get_offset_of_object_metadata_lock_11(),
	JsonMapper_t77474459_StaticFields::get_offset_of_type_properties_12(),
	JsonMapper_t77474459_StaticFields::get_offset_of_type_properties_lock_13(),
	JsonMapper_t77474459_StaticFields::get_offset_of_static_writer_14(),
	JsonMapper_t77474459_StaticFields::get_offset_of_static_writer_lock_15(),
	JsonMapper_t77474459_StaticFields::get_offset_of_dictionary_properties_to_ignore_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2180 = { sizeof (U3CU3Ec_t1591091833), -1, sizeof(U3CU3Ec_t1591091833_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2180[28] = 
{
	U3CU3Ec_t1591091833_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t1591091833_StaticFields::get_offset_of_U3CU3E9__24_0_1(),
	U3CU3Ec_t1591091833_StaticFields::get_offset_of_U3CU3E9__24_1_2(),
	U3CU3Ec_t1591091833_StaticFields::get_offset_of_U3CU3E9__24_2_3(),
	U3CU3Ec_t1591091833_StaticFields::get_offset_of_U3CU3E9__24_3_4(),
	U3CU3Ec_t1591091833_StaticFields::get_offset_of_U3CU3E9__24_4_5(),
	U3CU3Ec_t1591091833_StaticFields::get_offset_of_U3CU3E9__24_5_6(),
	U3CU3Ec_t1591091833_StaticFields::get_offset_of_U3CU3E9__24_6_7(),
	U3CU3Ec_t1591091833_StaticFields::get_offset_of_U3CU3E9__24_7_8(),
	U3CU3Ec_t1591091833_StaticFields::get_offset_of_U3CU3E9__24_8_9(),
	U3CU3Ec_t1591091833_StaticFields::get_offset_of_U3CU3E9__24_9_10(),
	U3CU3Ec_t1591091833_StaticFields::get_offset_of_U3CU3E9__24_10_11(),
	U3CU3Ec_t1591091833_StaticFields::get_offset_of_U3CU3E9__25_0_12(),
	U3CU3Ec_t1591091833_StaticFields::get_offset_of_U3CU3E9__25_1_13(),
	U3CU3Ec_t1591091833_StaticFields::get_offset_of_U3CU3E9__25_2_14(),
	U3CU3Ec_t1591091833_StaticFields::get_offset_of_U3CU3E9__25_3_15(),
	U3CU3Ec_t1591091833_StaticFields::get_offset_of_U3CU3E9__25_4_16(),
	U3CU3Ec_t1591091833_StaticFields::get_offset_of_U3CU3E9__25_5_17(),
	U3CU3Ec_t1591091833_StaticFields::get_offset_of_U3CU3E9__25_6_18(),
	U3CU3Ec_t1591091833_StaticFields::get_offset_of_U3CU3E9__25_7_19(),
	U3CU3Ec_t1591091833_StaticFields::get_offset_of_U3CU3E9__25_8_20(),
	U3CU3Ec_t1591091833_StaticFields::get_offset_of_U3CU3E9__25_9_21(),
	U3CU3Ec_t1591091833_StaticFields::get_offset_of_U3CU3E9__25_10_22(),
	U3CU3Ec_t1591091833_StaticFields::get_offset_of_U3CU3E9__25_11_23(),
	U3CU3Ec_t1591091833_StaticFields::get_offset_of_U3CU3E9__25_12_24(),
	U3CU3Ec_t1591091833_StaticFields::get_offset_of_U3CU3E9__25_13_25(),
	U3CU3Ec_t1591091833_StaticFields::get_offset_of_U3CU3E9__31_0_26(),
	U3CU3Ec_t1591091833_StaticFields::get_offset_of_U3CU3E9__32_0_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2181 = { sizeof (JsonToken_t2445581255)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2181[15] = 
{
	JsonToken_t2445581255::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2182 = { sizeof (JsonReader_t354941621), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2182[13] = 
{
	JsonReader_t354941621::get_offset_of_depth_0(),
	JsonReader_t354941621::get_offset_of_current_input_1(),
	JsonReader_t354941621::get_offset_of_current_symbol_2(),
	JsonReader_t354941621::get_offset_of_end_of_json_3(),
	JsonReader_t354941621::get_offset_of_end_of_input_4(),
	JsonReader_t354941621::get_offset_of_lexer_5(),
	JsonReader_t354941621::get_offset_of_parser_in_string_6(),
	JsonReader_t354941621::get_offset_of_parser_return_7(),
	JsonReader_t354941621::get_offset_of_read_started_8(),
	JsonReader_t354941621::get_offset_of_reader_9(),
	JsonReader_t354941621::get_offset_of_reader_is_owned_10(),
	JsonReader_t354941621::get_offset_of_token_value_11(),
	JsonReader_t354941621::get_offset_of_token_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2183 = { sizeof (Condition_t2230619687)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2183[6] = 
{
	Condition_t2230619687::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2184 = { sizeof (WriterContext_t1209007092), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2184[5] = 
{
	WriterContext_t1209007092::get_offset_of_Count_0(),
	WriterContext_t1209007092::get_offset_of_InArray_1(),
	WriterContext_t1209007092::get_offset_of_InObject_2(),
	WriterContext_t1209007092::get_offset_of_ExpectingValue_3(),
	WriterContext_t1209007092::get_offset_of_Padding_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2185 = { sizeof (JsonWriter_t3014444111), -1, sizeof(JsonWriter_t3014444111_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2185[11] = 
{
	JsonWriter_t3014444111_StaticFields::get_offset_of_number_format_0(),
	JsonWriter_t3014444111::get_offset_of_context_1(),
	JsonWriter_t3014444111::get_offset_of_ctx_stack_2(),
	JsonWriter_t3014444111::get_offset_of_has_reached_end_3(),
	JsonWriter_t3014444111::get_offset_of_hex_seq_4(),
	JsonWriter_t3014444111::get_offset_of_indentation_5(),
	JsonWriter_t3014444111::get_offset_of_indent_value_6(),
	JsonWriter_t3014444111::get_offset_of_inst_string_builder_7(),
	JsonWriter_t3014444111::get_offset_of_pretty_print_8(),
	JsonWriter_t3014444111::get_offset_of_validate_9(),
	JsonWriter_t3014444111::get_offset_of_writer_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2186 = { sizeof (FsmContext_t4275593467), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2186[4] = 
{
	FsmContext_t4275593467::get_offset_of_Return_0(),
	FsmContext_t4275593467::get_offset_of_NextState_1(),
	FsmContext_t4275593467::get_offset_of_L_2(),
	FsmContext_t4275593467::get_offset_of_StateStack_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2187 = { sizeof (Lexer_t954714164), -1, sizeof(Lexer_t954714164_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2187[14] = 
{
	Lexer_t954714164_StaticFields::get_offset_of_fsm_return_table_0(),
	Lexer_t954714164_StaticFields::get_offset_of_fsm_handler_table_1(),
	Lexer_t954714164::get_offset_of_allow_comments_2(),
	Lexer_t954714164::get_offset_of_allow_single_quoted_strings_3(),
	Lexer_t954714164::get_offset_of_end_of_input_4(),
	Lexer_t954714164::get_offset_of_fsm_context_5(),
	Lexer_t954714164::get_offset_of_input_buffer_6(),
	Lexer_t954714164::get_offset_of_input_char_7(),
	Lexer_t954714164::get_offset_of_reader_8(),
	Lexer_t954714164::get_offset_of_state_9(),
	Lexer_t954714164::get_offset_of_string_buffer_10(),
	Lexer_t954714164::get_offset_of_string_value_11(),
	Lexer_t954714164::get_offset_of_token_12(),
	Lexer_t954714164::get_offset_of_unichar_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2188 = { sizeof (StateHandler_t3489987002), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2189 = { sizeof (CRC32_t619824607), -1, sizeof(CRC32_t619824607_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2189[3] = 
{
	CRC32_t619824607::get_offset_of__TotalBytesRead_0(),
	CRC32_t619824607_StaticFields::get_offset_of_crc32Table_1(),
	CRC32_t619824607::get_offset_of__RunningCrc32Result_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2190 = { sizeof (CrcCalculatorStream_t2228597532), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2190[3] = 
{
	CrcCalculatorStream_t2228597532::get_offset_of__InnerStream_1(),
	CrcCalculatorStream_t2228597532::get_offset_of__Crc32_2(),
	CrcCalculatorStream_t2228597532::get_offset_of__length_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2191 = { sizeof (InvalidDataException_t1057132120), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2192 = { sizeof (AWSConfigs_t909839638), -1, sizeof(AWSConfigs_t909839638_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2192[19] = 
{
	AWSConfigs_t909839638_StaticFields::get_offset_of_validSeparators_0(),
	AWSConfigs_t909839638_StaticFields::get_offset_of__awsRegion_1(),
	AWSConfigs_t909839638_StaticFields::get_offset_of__logging_2(),
	AWSConfigs_t909839638_StaticFields::get_offset_of__responseLogging_3(),
	AWSConfigs_t909839638_StaticFields::get_offset_of__logMetrics_4(),
	AWSConfigs_t909839638_StaticFields::get_offset_of__endpointDefinition_5(),
	AWSConfigs_t909839638_StaticFields::get_offset_of__awsProfileName_6(),
	AWSConfigs_t909839638_StaticFields::get_offset_of__awsAccountsLocation_7(),
	AWSConfigs_t909839638_StaticFields::get_offset_of__useSdkCache_8(),
	AWSConfigs_t909839638_StaticFields::get_offset_of__lock_9(),
	AWSConfigs_t909839638_StaticFields::get_offset_of_standardConfigs_10(),
	AWSConfigs_t909839638_StaticFields::get_offset_of_configPresent_11(),
	AWSConfigs_t909839638_StaticFields::get_offset_of__rootConfig_12(),
	AWSConfigs_t909839638_StaticFields::get_offset_of_U3CClockOffsetU3Ek__BackingField_13(),
	AWSConfigs_t909839638_StaticFields::get_offset_of_mPropertyChanged_14(),
	AWSConfigs_t909839638_StaticFields::get_offset_of_propertyChangedLock_15(),
	AWSConfigs_t909839638_StaticFields::get_offset_of__httpClient_16(),
	AWSConfigs_t909839638_StaticFields::get_offset_of__traceListeners_17(),
	AWSConfigs_t909839638_StaticFields::get_offset_of_xmlDoc_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2193 = { sizeof (HttpClientOption_t4250830711)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2193[3] = 
{
	HttpClientOption_t4250830711::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2194 = { sizeof (U3CU3Ec__DisplayClass89_0_t3765470194), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2194[3] = 
{
	U3CU3Ec__DisplayClass89_0_t3765470194::get_offset_of_awsConfig_0(),
	U3CU3Ec__DisplayClass89_0_t3765470194::get_offset_of_xDoc_1(),
	U3CU3Ec__DisplayClass89_0_t3765470194::get_offset_of_action_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2195 = { sizeof (U3CU3Ec__DisplayClass89_1_t3765470195), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2195[2] = 
{
	U3CU3Ec__DisplayClass89_1_t3765470195::get_offset_of_e_0(),
	U3CU3Ec__DisplayClass89_1_t3765470195::get_offset_of_CSU24U3CU3E8__locals1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2196 = { sizeof (U3CU3Ec__DisplayClass91_0_t1056790079), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2196[1] = 
{
	U3CU3Ec__DisplayClass91_0_t1056790079::get_offset_of_propertyInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2197 = { sizeof (LoggingOptions_t2865640765)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2197[6] = 
{
	LoggingOptions_t2865640765::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2198 = { sizeof (ResponseLoggingOption_t3443611737)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2198[4] = 
{
	ResponseLoggingOption_t3443611737::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2199 = { sizeof (LogMetricsFormatOption_t97749509)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2199[3] = 
{
	LogMetricsFormatOption_t97749509::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
