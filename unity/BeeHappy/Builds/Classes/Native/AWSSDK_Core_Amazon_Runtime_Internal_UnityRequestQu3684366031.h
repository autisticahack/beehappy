﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.Runtime.Internal.UnityRequestQueue
struct UnityRequestQueue_t3684366031;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.Queue`1<Amazon.Runtime.IUnityHttpRequest>
struct Queue_1_t1679560232;
// System.Collections.Generic.Queue`1<Amazon.Runtime.Internal.RuntimeAsyncResult>
struct Queue_1_t4099012848;
// System.Collections.Generic.Queue`1<System.Action>
struct Queue_1_t3046128587;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.UnityRequestQueue
struct  UnityRequestQueue_t3684366031  : public Il2CppObject
{
public:
	// System.Collections.Generic.Queue`1<Amazon.Runtime.IUnityHttpRequest> Amazon.Runtime.Internal.UnityRequestQueue::_requests
	Queue_1_t1679560232 * ____requests_4;
	// System.Collections.Generic.Queue`1<Amazon.Runtime.Internal.RuntimeAsyncResult> Amazon.Runtime.Internal.UnityRequestQueue::_callbacks
	Queue_1_t4099012848 * ____callbacks_5;
	// System.Collections.Generic.Queue`1<System.Action> Amazon.Runtime.Internal.UnityRequestQueue::_mainThreadCallbacks
	Queue_1_t3046128587 * ____mainThreadCallbacks_6;

public:
	inline static int32_t get_offset_of__requests_4() { return static_cast<int32_t>(offsetof(UnityRequestQueue_t3684366031, ____requests_4)); }
	inline Queue_1_t1679560232 * get__requests_4() const { return ____requests_4; }
	inline Queue_1_t1679560232 ** get_address_of__requests_4() { return &____requests_4; }
	inline void set__requests_4(Queue_1_t1679560232 * value)
	{
		____requests_4 = value;
		Il2CppCodeGenWriteBarrier(&____requests_4, value);
	}

	inline static int32_t get_offset_of__callbacks_5() { return static_cast<int32_t>(offsetof(UnityRequestQueue_t3684366031, ____callbacks_5)); }
	inline Queue_1_t4099012848 * get__callbacks_5() const { return ____callbacks_5; }
	inline Queue_1_t4099012848 ** get_address_of__callbacks_5() { return &____callbacks_5; }
	inline void set__callbacks_5(Queue_1_t4099012848 * value)
	{
		____callbacks_5 = value;
		Il2CppCodeGenWriteBarrier(&____callbacks_5, value);
	}

	inline static int32_t get_offset_of__mainThreadCallbacks_6() { return static_cast<int32_t>(offsetof(UnityRequestQueue_t3684366031, ____mainThreadCallbacks_6)); }
	inline Queue_1_t3046128587 * get__mainThreadCallbacks_6() const { return ____mainThreadCallbacks_6; }
	inline Queue_1_t3046128587 ** get_address_of__mainThreadCallbacks_6() { return &____mainThreadCallbacks_6; }
	inline void set__mainThreadCallbacks_6(Queue_1_t3046128587 * value)
	{
		____mainThreadCallbacks_6 = value;
		Il2CppCodeGenWriteBarrier(&____mainThreadCallbacks_6, value);
	}
};

struct UnityRequestQueue_t3684366031_StaticFields
{
public:
	// Amazon.Runtime.Internal.UnityRequestQueue Amazon.Runtime.Internal.UnityRequestQueue::_instance
	UnityRequestQueue_t3684366031 * ____instance_0;
	// System.Object Amazon.Runtime.Internal.UnityRequestQueue::_requestsLock
	Il2CppObject * ____requestsLock_1;
	// System.Object Amazon.Runtime.Internal.UnityRequestQueue::_callbacksLock
	Il2CppObject * ____callbacksLock_2;
	// System.Object Amazon.Runtime.Internal.UnityRequestQueue::_mainThreadCallbackLock
	Il2CppObject * ____mainThreadCallbackLock_3;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(UnityRequestQueue_t3684366031_StaticFields, ____instance_0)); }
	inline UnityRequestQueue_t3684366031 * get__instance_0() const { return ____instance_0; }
	inline UnityRequestQueue_t3684366031 ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(UnityRequestQueue_t3684366031 * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier(&____instance_0, value);
	}

	inline static int32_t get_offset_of__requestsLock_1() { return static_cast<int32_t>(offsetof(UnityRequestQueue_t3684366031_StaticFields, ____requestsLock_1)); }
	inline Il2CppObject * get__requestsLock_1() const { return ____requestsLock_1; }
	inline Il2CppObject ** get_address_of__requestsLock_1() { return &____requestsLock_1; }
	inline void set__requestsLock_1(Il2CppObject * value)
	{
		____requestsLock_1 = value;
		Il2CppCodeGenWriteBarrier(&____requestsLock_1, value);
	}

	inline static int32_t get_offset_of__callbacksLock_2() { return static_cast<int32_t>(offsetof(UnityRequestQueue_t3684366031_StaticFields, ____callbacksLock_2)); }
	inline Il2CppObject * get__callbacksLock_2() const { return ____callbacksLock_2; }
	inline Il2CppObject ** get_address_of__callbacksLock_2() { return &____callbacksLock_2; }
	inline void set__callbacksLock_2(Il2CppObject * value)
	{
		____callbacksLock_2 = value;
		Il2CppCodeGenWriteBarrier(&____callbacksLock_2, value);
	}

	inline static int32_t get_offset_of__mainThreadCallbackLock_3() { return static_cast<int32_t>(offsetof(UnityRequestQueue_t3684366031_StaticFields, ____mainThreadCallbackLock_3)); }
	inline Il2CppObject * get__mainThreadCallbackLock_3() const { return ____mainThreadCallbackLock_3; }
	inline Il2CppObject ** get_address_of__mainThreadCallbackLock_3() { return &____mainThreadCallbackLock_3; }
	inline void set__mainThreadCallbackLock_3(Il2CppObject * value)
	{
		____mainThreadCallbackLock_3 = value;
		Il2CppCodeGenWriteBarrier(&____mainThreadCallbackLock_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
