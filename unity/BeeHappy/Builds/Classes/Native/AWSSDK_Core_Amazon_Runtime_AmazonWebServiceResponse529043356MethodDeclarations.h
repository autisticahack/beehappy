﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.AmazonWebServiceResponse
struct AmazonWebServiceResponse_t529043356;
// Amazon.Runtime.ResponseMetadata
struct ResponseMetadata_t527027456;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_Runtime_ResponseMetadata527027456.h"
#include "System_System_Net_HttpStatusCode1898409641.h"

// Amazon.Runtime.ResponseMetadata Amazon.Runtime.AmazonWebServiceResponse::get_ResponseMetadata()
extern "C"  ResponseMetadata_t527027456 * AmazonWebServiceResponse_get_ResponseMetadata_m3858190505 (AmazonWebServiceResponse_t529043356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.AmazonWebServiceResponse::set_ResponseMetadata(Amazon.Runtime.ResponseMetadata)
extern "C"  void AmazonWebServiceResponse_set_ResponseMetadata_m2931428714 (AmazonWebServiceResponse_t529043356 * __this, ResponseMetadata_t527027456 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Amazon.Runtime.AmazonWebServiceResponse::get_ContentLength()
extern "C"  int64_t AmazonWebServiceResponse_get_ContentLength_m2370437132 (AmazonWebServiceResponse_t529043356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.AmazonWebServiceResponse::set_ContentLength(System.Int64)
extern "C"  void AmazonWebServiceResponse_set_ContentLength_m1031631455 (AmazonWebServiceResponse_t529043356 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.HttpStatusCode Amazon.Runtime.AmazonWebServiceResponse::get_HttpStatusCode()
extern "C"  int32_t AmazonWebServiceResponse_get_HttpStatusCode_m704837737 (AmazonWebServiceResponse_t529043356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.AmazonWebServiceResponse::set_HttpStatusCode(System.Net.HttpStatusCode)
extern "C"  void AmazonWebServiceResponse_set_HttpStatusCode_m3739246904 (AmazonWebServiceResponse_t529043356 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.AmazonWebServiceResponse::.ctor()
extern "C"  void AmazonWebServiceResponse__ctor_m3474488249 (AmazonWebServiceResponse_t529043356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
