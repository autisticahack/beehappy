﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Comparison_1_gen716359003MethodDeclarations.h"

// System.Void System.Comparison`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.String>>::.ctor(System.Object,System.IntPtr)
#define Comparison_1__ctor_m787177995(__this, ___object0, ___method1, method) ((  void (*) (Comparison_1_t56129941 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m943266423_gshared)(__this, ___object0, ___method1, method)
// System.Int32 System.Comparison`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.String>>::Invoke(T,T)
#define Comparison_1_Invoke_m2929342437(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparison_1_t56129941 *, KeyValuePair_2_t3089358386 , KeyValuePair_2_t3089358386 , const MethodInfo*))Comparison_1_Invoke_m1351529777_gshared)(__this, ___x0, ___y1, method)
// System.IAsyncResult System.Comparison`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.String>>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
#define Comparison_1_BeginInvoke_m1204857368(__this, ___x0, ___y1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Comparison_1_t56129941 *, KeyValuePair_2_t3089358386 , KeyValuePair_2_t3089358386 , AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Comparison_1_BeginInvoke_m1911012798_gshared)(__this, ___x0, ___y1, ___callback2, ___object3, method)
// System.Int32 System.Comparison`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.String>>::EndInvoke(System.IAsyncResult)
#define Comparison_1_EndInvoke_m4244068295(__this, ___result0, method) ((  int32_t (*) (Comparison_1_t56129941 *, Il2CppObject *, const MethodInfo*))Comparison_1_EndInvoke_m4270453955_gshared)(__this, ___result0, method)
