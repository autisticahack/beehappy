﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Linq.QuickSort`1/<Sort>c__Iterator21<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct U3CSortU3Ec__Iterator21_t4132977838;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerator_1_t1809345768;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645.h"

// System.Void System.Linq.QuickSort`1/<Sort>c__Iterator21<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor()
extern "C"  void U3CSortU3Ec__Iterator21__ctor_m1235766664_gshared (U3CSortU3Ec__Iterator21_t4132977838 * __this, const MethodInfo* method);
#define U3CSortU3Ec__Iterator21__ctor_m1235766664(__this, method) ((  void (*) (U3CSortU3Ec__Iterator21_t4132977838 *, const MethodInfo*))U3CSortU3Ec__Iterator21__ctor_m1235766664_gshared)(__this, method)
// TElement System.Linq.QuickSort`1/<Sort>c__Iterator21<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.IEnumerator<TElement>.get_Current()
extern "C"  KeyValuePair_2_t38854645  U3CSortU3Ec__Iterator21_System_Collections_Generic_IEnumeratorU3CTElementU3E_get_Current_m1879380469_gshared (U3CSortU3Ec__Iterator21_t4132977838 * __this, const MethodInfo* method);
#define U3CSortU3Ec__Iterator21_System_Collections_Generic_IEnumeratorU3CTElementU3E_get_Current_m1879380469(__this, method) ((  KeyValuePair_2_t38854645  (*) (U3CSortU3Ec__Iterator21_t4132977838 *, const MethodInfo*))U3CSortU3Ec__Iterator21_System_Collections_Generic_IEnumeratorU3CTElementU3E_get_Current_m1879380469_gshared)(__this, method)
// System.Object System.Linq.QuickSort`1/<Sort>c__Iterator21<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSortU3Ec__Iterator21_System_Collections_IEnumerator_get_Current_m751348090_gshared (U3CSortU3Ec__Iterator21_t4132977838 * __this, const MethodInfo* method);
#define U3CSortU3Ec__Iterator21_System_Collections_IEnumerator_get_Current_m751348090(__this, method) ((  Il2CppObject * (*) (U3CSortU3Ec__Iterator21_t4132977838 *, const MethodInfo*))U3CSortU3Ec__Iterator21_System_Collections_IEnumerator_get_Current_m751348090_gshared)(__this, method)
// System.Collections.IEnumerator System.Linq.QuickSort`1/<Sort>c__Iterator21<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CSortU3Ec__Iterator21_System_Collections_IEnumerable_GetEnumerator_m2250081433_gshared (U3CSortU3Ec__Iterator21_t4132977838 * __this, const MethodInfo* method);
#define U3CSortU3Ec__Iterator21_System_Collections_IEnumerable_GetEnumerator_m2250081433(__this, method) ((  Il2CppObject * (*) (U3CSortU3Ec__Iterator21_t4132977838 *, const MethodInfo*))U3CSortU3Ec__Iterator21_System_Collections_IEnumerable_GetEnumerator_m2250081433_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<TElement> System.Linq.QuickSort`1/<Sort>c__Iterator21<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.IEnumerable<TElement>.GetEnumerator()
extern "C"  Il2CppObject* U3CSortU3Ec__Iterator21_System_Collections_Generic_IEnumerableU3CTElementU3E_GetEnumerator_m4028416676_gshared (U3CSortU3Ec__Iterator21_t4132977838 * __this, const MethodInfo* method);
#define U3CSortU3Ec__Iterator21_System_Collections_Generic_IEnumerableU3CTElementU3E_GetEnumerator_m4028416676(__this, method) ((  Il2CppObject* (*) (U3CSortU3Ec__Iterator21_t4132977838 *, const MethodInfo*))U3CSortU3Ec__Iterator21_System_Collections_Generic_IEnumerableU3CTElementU3E_GetEnumerator_m4028416676_gshared)(__this, method)
// System.Boolean System.Linq.QuickSort`1/<Sort>c__Iterator21<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::MoveNext()
extern "C"  bool U3CSortU3Ec__Iterator21_MoveNext_m534311468_gshared (U3CSortU3Ec__Iterator21_t4132977838 * __this, const MethodInfo* method);
#define U3CSortU3Ec__Iterator21_MoveNext_m534311468(__this, method) ((  bool (*) (U3CSortU3Ec__Iterator21_t4132977838 *, const MethodInfo*))U3CSortU3Ec__Iterator21_MoveNext_m534311468_gshared)(__this, method)
// System.Void System.Linq.QuickSort`1/<Sort>c__Iterator21<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Dispose()
extern "C"  void U3CSortU3Ec__Iterator21_Dispose_m1501510827_gshared (U3CSortU3Ec__Iterator21_t4132977838 * __this, const MethodInfo* method);
#define U3CSortU3Ec__Iterator21_Dispose_m1501510827(__this, method) ((  void (*) (U3CSortU3Ec__Iterator21_t4132977838 *, const MethodInfo*))U3CSortU3Ec__Iterator21_Dispose_m1501510827_gshared)(__this, method)
// System.Void System.Linq.QuickSort`1/<Sort>c__Iterator21<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Reset()
extern "C"  void U3CSortU3Ec__Iterator21_Reset_m1823727181_gshared (U3CSortU3Ec__Iterator21_t4132977838 * __this, const MethodInfo* method);
#define U3CSortU3Ec__Iterator21_Reset_m1823727181(__this, method) ((  void (*) (U3CSortU3Ec__Iterator21_t4132977838 *, const MethodInfo*))U3CSortU3Ec__Iterator21_Reset_m1823727181_gshared)(__this, method)
