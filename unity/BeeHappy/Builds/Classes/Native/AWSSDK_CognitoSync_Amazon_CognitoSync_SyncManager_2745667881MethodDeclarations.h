﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.CognitoSync.SyncManager.Dataset/SyncConflictDelegate
struct SyncConflictDelegate_t2745667881;
// System.Object
struct Il2CppObject;
// Amazon.CognitoSync.SyncManager.Dataset
struct Dataset_t3040902086;
// System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.SyncConflict>
struct List_1_t2399431441;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_SyncManager_3040902086.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Amazon.CognitoSync.SyncManager.Dataset/SyncConflictDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void SyncConflictDelegate__ctor_m59982427 (SyncConflictDelegate_t2745667881 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.CognitoSync.SyncManager.Dataset/SyncConflictDelegate::Invoke(Amazon.CognitoSync.SyncManager.Dataset,System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.SyncConflict>)
extern "C"  bool SyncConflictDelegate_Invoke_m2312903032 (SyncConflictDelegate_t2745667881 * __this, Dataset_t3040902086 * ___dataset0, List_1_t2399431441 * ___conflicts1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Amazon.CognitoSync.SyncManager.Dataset/SyncConflictDelegate::BeginInvoke(Amazon.CognitoSync.SyncManager.Dataset,System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.SyncConflict>,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * SyncConflictDelegate_BeginInvoke_m1582753913 (SyncConflictDelegate_t2745667881 * __this, Dataset_t3040902086 * ___dataset0, List_1_t2399431441 * ___conflicts1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.CognitoSync.SyncManager.Dataset/SyncConflictDelegate::EndInvoke(System.IAsyncResult)
extern "C"  bool SyncConflictDelegate_EndInvoke_m3492268389 (SyncConflictDelegate_t2745667881 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
