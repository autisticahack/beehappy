﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1642385972;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.String Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage/DatasetColumns::BuildQuery(System.String)
extern "C"  String_t* DatasetColumns_BuildQuery_m2562712950 (Il2CppObject * __this /* static, unused */, String_t* ___conditions0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage/DatasetColumns::BuildInsert(System.String[])
extern "C"  String_t* DatasetColumns_BuildInsert_m355743771 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t1642385972* ___fieldList0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage/DatasetColumns::BuildUpdate(System.String[],System.String)
extern "C"  String_t* DatasetColumns_BuildUpdate_m2155059143 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t1642385972* ___fieldList0, String_t* ___conditions1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage/DatasetColumns::BuildDelete(System.String)
extern "C"  String_t* DatasetColumns_BuildDelete_m1505896119 (Il2CppObject * __this /* static, unused */, String_t* ___conditions0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage/DatasetColumns::.cctor()
extern "C"  void DatasetColumns__cctor_m2074620234 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
