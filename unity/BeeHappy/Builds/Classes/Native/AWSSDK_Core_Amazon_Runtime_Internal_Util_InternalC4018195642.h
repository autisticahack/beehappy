﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AWSSDK_Core_Amazon_Runtime_Internal_Util_InternalL2972373343.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Util.InternalConsoleLogger
struct  InternalConsoleLogger_t4018195642  : public InternalLogger_t2972373343
{
public:

public:
};

struct InternalConsoleLogger_t4018195642_StaticFields
{
public:
	// System.Int64 Amazon.Runtime.Internal.Util.InternalConsoleLogger::_sequanceId
	int64_t ____sequanceId_2;

public:
	inline static int32_t get_offset_of__sequanceId_2() { return static_cast<int32_t>(offsetof(InternalConsoleLogger_t4018195642_StaticFields, ____sequanceId_2)); }
	inline int64_t get__sequanceId_2() const { return ____sequanceId_2; }
	inline int64_t* get_address_of__sequanceId_2() { return &____sequanceId_2; }
	inline void set__sequanceId_2(int64_t value)
	{
		____sequanceId_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
