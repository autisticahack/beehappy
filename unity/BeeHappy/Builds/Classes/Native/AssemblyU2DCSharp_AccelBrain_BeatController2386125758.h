﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AccelBrain.BeatController
struct  BeatController_t2386125758  : public MonoBehaviour_t1158329972
{
public:
	// System.Double AccelBrain.BeatController::LeftFrequency
	double ___LeftFrequency_2;
	// System.Double AccelBrain.BeatController::RightFrequency
	double ___RightFrequency_3;
	// System.Double AccelBrain.BeatController::Gain
	double ___Gain_4;
	// System.Double AccelBrain.BeatController::SampleRate
	double ___SampleRate_5;

public:
	inline static int32_t get_offset_of_LeftFrequency_2() { return static_cast<int32_t>(offsetof(BeatController_t2386125758, ___LeftFrequency_2)); }
	inline double get_LeftFrequency_2() const { return ___LeftFrequency_2; }
	inline double* get_address_of_LeftFrequency_2() { return &___LeftFrequency_2; }
	inline void set_LeftFrequency_2(double value)
	{
		___LeftFrequency_2 = value;
	}

	inline static int32_t get_offset_of_RightFrequency_3() { return static_cast<int32_t>(offsetof(BeatController_t2386125758, ___RightFrequency_3)); }
	inline double get_RightFrequency_3() const { return ___RightFrequency_3; }
	inline double* get_address_of_RightFrequency_3() { return &___RightFrequency_3; }
	inline void set_RightFrequency_3(double value)
	{
		___RightFrequency_3 = value;
	}

	inline static int32_t get_offset_of_Gain_4() { return static_cast<int32_t>(offsetof(BeatController_t2386125758, ___Gain_4)); }
	inline double get_Gain_4() const { return ___Gain_4; }
	inline double* get_address_of_Gain_4() { return &___Gain_4; }
	inline void set_Gain_4(double value)
	{
		___Gain_4 = value;
	}

	inline static int32_t get_offset_of_SampleRate_5() { return static_cast<int32_t>(offsetof(BeatController_t2386125758, ___SampleRate_5)); }
	inline double get_SampleRate_5() const { return ___SampleRate_5; }
	inline double* get_address_of_SampleRate_5() { return &___SampleRate_5; }
	inline void set_SampleRate_5(double value)
	{
		___SampleRate_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
