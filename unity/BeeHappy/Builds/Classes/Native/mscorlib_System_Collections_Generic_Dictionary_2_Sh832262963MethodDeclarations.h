﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>
struct ShimEnumerator_t832262963;
// System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>
struct Dictionary_2_t727138142;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m774979914_gshared (ShimEnumerator_t832262963 * __this, Dictionary_2_t727138142 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m774979914(__this, ___host0, method) ((  void (*) (ShimEnumerator_t832262963 *, Dictionary_2_t727138142 *, const MethodInfo*))ShimEnumerator__ctor_m774979914_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m1198409173_gshared (ShimEnumerator_t832262963 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m1198409173(__this, method) ((  bool (*) (ShimEnumerator_t832262963 *, const MethodInfo*))ShimEnumerator_MoveNext_m1198409173_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::get_Entry()
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m3444299241_gshared (ShimEnumerator_t832262963 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m3444299241(__this, method) ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t832262963 *, const MethodInfo*))ShimEnumerator_get_Entry_m3444299241_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m2804359804_gshared (ShimEnumerator_t832262963 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m2804359804(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t832262963 *, const MethodInfo*))ShimEnumerator_get_Key_m2804359804_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m505292094_gshared (ShimEnumerator_t832262963 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m505292094(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t832262963 *, const MethodInfo*))ShimEnumerator_get_Value_m505292094_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m489028466_gshared (ShimEnumerator_t832262963 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m489028466(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t832262963 *, const MethodInfo*))ShimEnumerator_get_Current_m489028466_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::Reset()
extern "C"  void ShimEnumerator_Reset_m1108407716_gshared (ShimEnumerator_t832262963 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m1108407716(__this, method) ((  void (*) (ShimEnumerator_t832262963 *, const MethodInfo*))ShimEnumerator_Reset_m1108407716_gshared)(__this, method)
