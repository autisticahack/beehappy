﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


// System.Threading.ReaderWriterLockSlim/LockDetails
struct LockDetails_t2550015005;
// System.Func`2<System.IntPtr,System.Object>
struct Func_2_t3675469371;
// System.Action
struct Action_t3226471752;
// System.Collections.Generic.HashSet`1<UnityEngine.UI.Text>
struct HashSet_1_t2984649583;

#include "mscorlib_System_Array3829468939.h"
#include "System_Core_System_Collections_Generic_HashSet_1_Li865133271.h"
#include "System_Core_System_Threading_ReaderWriterLockSlim_2550015005.h"
#include "System_Core_System_Func_2_gen3675469371.h"
#include "System_Core_System_Collections_Generic_HashSet_1_Li204904209.h"
#include "System_Core_System_Action3226471752.h"
#include "System_Core_System_Collections_Generic_HashSet_1_Lin74093617.h"
#include "System_Core_System_Collections_Generic_HashSet_1_L3640024803.h"
#include "System_Core_System_Collections_Generic_HashSet_1_L3210155869.h"
#include "System_Core_System_Collections_Generic_HashSet_1_L2826872705.h"
#include "System_Core_System_Collections_Generic_HashSet_1_g2984649583.h"
#include "System_Core_System_Collections_Generic_HashSet_1_Li116960033.h"

#pragma once
// System.Collections.Generic.HashSet`1/Link<System.Object>[]
struct LinkU5BU5D_t3597933550  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Link_t865133271  m_Items[1];

public:
	inline Link_t865133271  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Link_t865133271 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Link_t865133271  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Link_t865133271  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Link_t865133271 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Link_t865133271  value)
	{
		m_Items[index] = value;
	}
};
// System.Threading.ReaderWriterLockSlim/LockDetails[]
struct LockDetailsU5BU5D_t1234517712  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) LockDetails_t2550015005 * m_Items[1];

public:
	inline LockDetails_t2550015005 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline LockDetails_t2550015005 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, LockDetails_t2550015005 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline LockDetails_t2550015005 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline LockDetails_t2550015005 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, LockDetails_t2550015005 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Func`2<System.IntPtr,System.Object>[]
struct Func_2U5BU5D_t4217289338  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Func_2_t3675469371 * m_Items[1];

public:
	inline Func_2_t3675469371 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Func_2_t3675469371 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Func_2_t3675469371 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Func_2_t3675469371 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Func_2_t3675469371 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Func_2_t3675469371 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.HashSet`1/Link<System.String>[]
struct LinkU5BU5D_t1625685388  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Link_t204904209  m_Items[1];

public:
	inline Link_t204904209  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Link_t204904209 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Link_t204904209  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Link_t204904209  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Link_t204904209 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Link_t204904209  value)
	{
		m_Items[index] = value;
	}
};
// System.Action[]
struct ActionU5BU5D_t87223449  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Action_t3226471752 * m_Items[1];

public:
	inline Action_t3226471752 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Action_t3226471752 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Action_t3226471752 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Action_t3226471752 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Action_t3226471752 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Action_t3226471752 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.HashSet`1/Link<System.Net.HttpStatusCode>[]
struct LinkU5BU5D_t1288554476  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Link_t74093617  m_Items[1];

public:
	inline Link_t74093617  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Link_t74093617 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Link_t74093617  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Link_t74093617  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Link_t74093617 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Link_t74093617  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.HashSet`1/Link<System.Net.WebExceptionStatus>[]
struct LinkU5BU5D_t8180274  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Link_t3640024803  m_Items[1];

public:
	inline Link_t3640024803  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Link_t3640024803 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Link_t3640024803  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Link_t3640024803  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Link_t3640024803 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Link_t3640024803  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.HashSet`1/Link<System.Xml.XmlNodeType>[]
struct LinkU5BU5D_t3374044816  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Link_t3210155869  m_Items[1];

public:
	inline Link_t3210155869  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Link_t3210155869 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Link_t3210155869  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Link_t3210155869  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Link_t3210155869 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Link_t3210155869  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.HashSet`1/Link<UnityEngine.UI.Text>[]
struct LinkU5BU5D_t4199738716  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Link_t2826872705  m_Items[1];

public:
	inline Link_t2826872705  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Link_t2826872705 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Link_t2826872705  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Link_t2826872705  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Link_t2826872705 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Link_t2826872705  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.HashSet`1<UnityEngine.UI.Text>[]
struct HashSet_1U5BU5D_t2427620854  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) HashSet_1_t2984649583 * m_Items[1];

public:
	inline HashSet_1_t2984649583 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline HashSet_1_t2984649583 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, HashSet_1_t2984649583 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline HashSet_1_t2984649583 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline HashSet_1_t2984649583 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, HashSet_1_t2984649583 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.HashSet`1/Link<UnityEngine.UI.IClippable>[]
struct LinkU5BU5D_t222250812  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Link_t116960033  m_Items[1];

public:
	inline Link_t116960033  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Link_t116960033 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Link_t116960033  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Link_t116960033  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Link_t116960033 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Link_t116960033  value)
	{
		m_Items[index] = value;
	}
};
