﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.HashSet`1<System.Xml.XmlNodeType>
struct HashSet_1_t3367932747;
// System.Collections.Generic.IEqualityComparer`1<System.Xml.XmlNodeType>
struct IEqualityComparer_1_t4247104671;
// System.Collections.Generic.IEnumerable`1<System.Xml.XmlNodeType>
struct IEnumerable_1_t1031631642;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// System.Collections.Generic.IEnumerator`1<System.Xml.XmlNodeType>
struct IEnumerator_1_t2509995720;
// System.Xml.XmlNodeType[]
struct XmlNodeTypeU5BU5D_t3390745400;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serialization228987430.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"
#include "System_Xml_System_Xml_XmlNodeType739504597.h"
#include "mscorlib_System_Object2689449295.h"
#include "System_Core_System_Collections_Generic_HashSet_1_E1856248589.h"

// System.Void System.Collections.Generic.HashSet`1<System.Xml.XmlNodeType>::.ctor()
extern "C"  void HashSet_1__ctor_m4246660448_gshared (HashSet_1_t3367932747 * __this, const MethodInfo* method);
#define HashSet_1__ctor_m4246660448(__this, method) ((  void (*) (HashSet_1_t3367932747 *, const MethodInfo*))HashSet_1__ctor_m4246660448_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<System.Xml.XmlNodeType>::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
extern "C"  void HashSet_1__ctor_m310525802_gshared (HashSet_1_t3367932747 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define HashSet_1__ctor_m310525802(__this, ___comparer0, method) ((  void (*) (HashSet_1_t3367932747 *, Il2CppObject*, const MethodInfo*))HashSet_1__ctor_m310525802_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.HashSet`1<System.Xml.XmlNodeType>::.ctor(System.Collections.Generic.IEnumerable`1<T>,System.Collections.Generic.IEqualityComparer`1<T>)
extern "C"  void HashSet_1__ctor_m4041776197_gshared (HashSet_1_t3367932747 * __this, Il2CppObject* ___collection0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define HashSet_1__ctor_m4041776197(__this, ___collection0, ___comparer1, method) ((  void (*) (HashSet_1_t3367932747 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))HashSet_1__ctor_m4041776197_gshared)(__this, ___collection0, ___comparer1, method)
// System.Void System.Collections.Generic.HashSet`1<System.Xml.XmlNodeType>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void HashSet_1__ctor_m153717185_gshared (HashSet_1_t3367932747 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define HashSet_1__ctor_m153717185(__this, ___info0, ___context1, method) ((  void (*) (HashSet_1_t3367932747 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))HashSet_1__ctor_m153717185_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1<System.Xml.XmlNodeType>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3565122648_gshared (HashSet_1_t3367932747 * __this, const MethodInfo* method);
#define HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3565122648(__this, method) ((  Il2CppObject* (*) (HashSet_1_t3367932747 *, const MethodInfo*))HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3565122648_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Xml.XmlNodeType>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2344981359_gshared (HashSet_1_t3367932747 * __this, const MethodInfo* method);
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2344981359(__this, method) ((  bool (*) (HashSet_1_t3367932747 *, const MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2344981359_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<System.Xml.XmlNodeType>::System.Collections.Generic.ICollection<T>.CopyTo(T[],System.Int32)
extern "C"  void HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m859683455_gshared (HashSet_1_t3367932747 * __this, XmlNodeTypeU5BU5D_t3390745400* ___array0, int32_t ___index1, const MethodInfo* method);
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m859683455(__this, ___array0, ___index1, method) ((  void (*) (HashSet_1_t3367932747 *, XmlNodeTypeU5BU5D_t3390745400*, int32_t, const MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m859683455_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.HashSet`1<System.Xml.XmlNodeType>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m421863211_gshared (HashSet_1_t3367932747 * __this, int32_t ___item0, const MethodInfo* method);
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m421863211(__this, ___item0, method) ((  void (*) (HashSet_1_t3367932747 *, int32_t, const MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m421863211_gshared)(__this, ___item0, method)
// System.Collections.IEnumerator System.Collections.Generic.HashSet`1<System.Xml.XmlNodeType>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * HashSet_1_System_Collections_IEnumerable_GetEnumerator_m3093453667_gshared (HashSet_1_t3367932747 * __this, const MethodInfo* method);
#define HashSet_1_System_Collections_IEnumerable_GetEnumerator_m3093453667(__this, method) ((  Il2CppObject * (*) (HashSet_1_t3367932747 *, const MethodInfo*))HashSet_1_System_Collections_IEnumerable_GetEnumerator_m3093453667_gshared)(__this, method)
// System.Int32 System.Collections.Generic.HashSet`1<System.Xml.XmlNodeType>::get_Count()
extern "C"  int32_t HashSet_1_get_Count_m28649588_gshared (HashSet_1_t3367932747 * __this, const MethodInfo* method);
#define HashSet_1_get_Count_m28649588(__this, method) ((  int32_t (*) (HashSet_1_t3367932747 *, const MethodInfo*))HashSet_1_get_Count_m28649588_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<System.Xml.XmlNodeType>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<T>)
extern "C"  void HashSet_1_Init_m1122283837_gshared (HashSet_1_t3367932747 * __this, int32_t ___capacity0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define HashSet_1_Init_m1122283837(__this, ___capacity0, ___comparer1, method) ((  void (*) (HashSet_1_t3367932747 *, int32_t, Il2CppObject*, const MethodInfo*))HashSet_1_Init_m1122283837_gshared)(__this, ___capacity0, ___comparer1, method)
// System.Void System.Collections.Generic.HashSet`1<System.Xml.XmlNodeType>::InitArrays(System.Int32)
extern "C"  void HashSet_1_InitArrays_m1815067055_gshared (HashSet_1_t3367932747 * __this, int32_t ___size0, const MethodInfo* method);
#define HashSet_1_InitArrays_m1815067055(__this, ___size0, method) ((  void (*) (HashSet_1_t3367932747 *, int32_t, const MethodInfo*))HashSet_1_InitArrays_m1815067055_gshared)(__this, ___size0, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Xml.XmlNodeType>::SlotsContainsAt(System.Int32,System.Int32,T)
extern "C"  bool HashSet_1_SlotsContainsAt_m2604953907_gshared (HashSet_1_t3367932747 * __this, int32_t ___index0, int32_t ___hash1, int32_t ___item2, const MethodInfo* method);
#define HashSet_1_SlotsContainsAt_m2604953907(__this, ___index0, ___hash1, ___item2, method) ((  bool (*) (HashSet_1_t3367932747 *, int32_t, int32_t, int32_t, const MethodInfo*))HashSet_1_SlotsContainsAt_m2604953907_gshared)(__this, ___index0, ___hash1, ___item2, method)
// System.Void System.Collections.Generic.HashSet`1<System.Xml.XmlNodeType>::CopyTo(T[],System.Int32)
extern "C"  void HashSet_1_CopyTo_m3816538215_gshared (HashSet_1_t3367932747 * __this, XmlNodeTypeU5BU5D_t3390745400* ___array0, int32_t ___index1, const MethodInfo* method);
#define HashSet_1_CopyTo_m3816538215(__this, ___array0, ___index1, method) ((  void (*) (HashSet_1_t3367932747 *, XmlNodeTypeU5BU5D_t3390745400*, int32_t, const MethodInfo*))HashSet_1_CopyTo_m3816538215_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.HashSet`1<System.Xml.XmlNodeType>::CopyTo(T[],System.Int32,System.Int32)
extern "C"  void HashSet_1_CopyTo_m2509258068_gshared (HashSet_1_t3367932747 * __this, XmlNodeTypeU5BU5D_t3390745400* ___array0, int32_t ___index1, int32_t ___count2, const MethodInfo* method);
#define HashSet_1_CopyTo_m2509258068(__this, ___array0, ___index1, ___count2, method) ((  void (*) (HashSet_1_t3367932747 *, XmlNodeTypeU5BU5D_t3390745400*, int32_t, int32_t, const MethodInfo*))HashSet_1_CopyTo_m2509258068_gshared)(__this, ___array0, ___index1, ___count2, method)
// System.Void System.Collections.Generic.HashSet`1<System.Xml.XmlNodeType>::Resize()
extern "C"  void HashSet_1_Resize_m2727485420_gshared (HashSet_1_t3367932747 * __this, const MethodInfo* method);
#define HashSet_1_Resize_m2727485420(__this, method) ((  void (*) (HashSet_1_t3367932747 *, const MethodInfo*))HashSet_1_Resize_m2727485420_gshared)(__this, method)
// System.Int32 System.Collections.Generic.HashSet`1<System.Xml.XmlNodeType>::GetLinkHashCode(System.Int32)
extern "C"  int32_t HashSet_1_GetLinkHashCode_m265856408_gshared (HashSet_1_t3367932747 * __this, int32_t ___index0, const MethodInfo* method);
#define HashSet_1_GetLinkHashCode_m265856408(__this, ___index0, method) ((  int32_t (*) (HashSet_1_t3367932747 *, int32_t, const MethodInfo*))HashSet_1_GetLinkHashCode_m265856408_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.Generic.HashSet`1<System.Xml.XmlNodeType>::GetItemHashCode(T)
extern "C"  int32_t HashSet_1_GetItemHashCode_m244621568_gshared (HashSet_1_t3367932747 * __this, int32_t ___item0, const MethodInfo* method);
#define HashSet_1_GetItemHashCode_m244621568(__this, ___item0, method) ((  int32_t (*) (HashSet_1_t3367932747 *, int32_t, const MethodInfo*))HashSet_1_GetItemHashCode_m244621568_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Xml.XmlNodeType>::Add(T)
extern "C"  bool HashSet_1_Add_m1428324854_gshared (HashSet_1_t3367932747 * __this, int32_t ___item0, const MethodInfo* method);
#define HashSet_1_Add_m1428324854(__this, ___item0, method) ((  bool (*) (HashSet_1_t3367932747 *, int32_t, const MethodInfo*))HashSet_1_Add_m1428324854_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.HashSet`1<System.Xml.XmlNodeType>::Clear()
extern "C"  void HashSet_1_Clear_m1715814495_gshared (HashSet_1_t3367932747 * __this, const MethodInfo* method);
#define HashSet_1_Clear_m1715814495(__this, method) ((  void (*) (HashSet_1_t3367932747 *, const MethodInfo*))HashSet_1_Clear_m1715814495_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Xml.XmlNodeType>::Contains(T)
extern "C"  bool HashSet_1_Contains_m3644054006_gshared (HashSet_1_t3367932747 * __this, int32_t ___item0, const MethodInfo* method);
#define HashSet_1_Contains_m3644054006(__this, ___item0, method) ((  bool (*) (HashSet_1_t3367932747 *, int32_t, const MethodInfo*))HashSet_1_Contains_m3644054006_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Xml.XmlNodeType>::Remove(T)
extern "C"  bool HashSet_1_Remove_m292533372_gshared (HashSet_1_t3367932747 * __this, int32_t ___item0, const MethodInfo* method);
#define HashSet_1_Remove_m292533372(__this, ___item0, method) ((  bool (*) (HashSet_1_t3367932747 *, int32_t, const MethodInfo*))HashSet_1_Remove_m292533372_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.HashSet`1<System.Xml.XmlNodeType>::IntersectWith(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void HashSet_1_IntersectWith_m15209704_gshared (HashSet_1_t3367932747 * __this, Il2CppObject* ___other0, const MethodInfo* method);
#define HashSet_1_IntersectWith_m15209704(__this, ___other0, method) ((  void (*) (HashSet_1_t3367932747 *, Il2CppObject*, const MethodInfo*))HashSet_1_IntersectWith_m15209704_gshared)(__this, ___other0, method)
// System.Void System.Collections.Generic.HashSet`1<System.Xml.XmlNodeType>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void HashSet_1_GetObjectData_m396045702_gshared (HashSet_1_t3367932747 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define HashSet_1_GetObjectData_m396045702(__this, ___info0, ___context1, method) ((  void (*) (HashSet_1_t3367932747 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))HashSet_1_GetObjectData_m396045702_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.HashSet`1<System.Xml.XmlNodeType>::OnDeserialization(System.Object)
extern "C"  void HashSet_1_OnDeserialization_m3541515416_gshared (HashSet_1_t3367932747 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define HashSet_1_OnDeserialization_m3541515416(__this, ___sender0, method) ((  void (*) (HashSet_1_t3367932747 *, Il2CppObject *, const MethodInfo*))HashSet_1_OnDeserialization_m3541515416_gshared)(__this, ___sender0, method)
// System.Collections.Generic.HashSet`1/Enumerator<T> System.Collections.Generic.HashSet`1<System.Xml.XmlNodeType>::GetEnumerator()
extern "C"  Enumerator_t1856248589  HashSet_1_GetEnumerator_m1673126_gshared (HashSet_1_t3367932747 * __this, const MethodInfo* method);
#define HashSet_1_GetEnumerator_m1673126(__this, method) ((  Enumerator_t1856248589  (*) (HashSet_1_t3367932747 *, const MethodInfo*))HashSet_1_GetEnumerator_m1673126_gshared)(__this, method)
