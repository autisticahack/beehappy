﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2016035531MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<Amazon.Runtime.Metric,System.Collections.Generic.List`1<Amazon.Runtime.IMetricsTiming>,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
#define Transform_1__ctor_m449668129(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t3963152382 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m1390018089_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Amazon.Runtime.Metric,System.Collections.Generic.List`1<Amazon.Runtime.IMetricsTiming>,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m4244011925(__this, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Transform_1_t3963152382 *, int32_t, List_1_t3837499352 *, const MethodInfo*))Transform_1_Invoke_m3629793757_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<Amazon.Runtime.Metric,System.Collections.Generic.List`1<Amazon.Runtime.IMetricsTiming>,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
#define Transform_1_BeginInvoke_m3926364918(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t3963152382 *, int32_t, List_1_t3837499352 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m2429879322_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Amazon.Runtime.Metric,System.Collections.Generic.List`1<Amazon.Runtime.IMetricsTiming>,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
#define Transform_1_EndInvoke_m1556689179(__this, ___result0, method) ((  DictionaryEntry_t3048875398  (*) (Transform_1_t3963152382 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m3230763475_gshared)(__this, ___result0, method)
