﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage
struct SQLiteLocalStorage_t3359635340;
// System.String
struct String_t;
// System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.DatasetMetadata>
struct List_1_t574851791;
// Amazon.CognitoSync.SyncManager.DatasetMetadata
struct DatasetMetadata_t1205730659;
// Amazon.CognitoSync.SyncManager.Record
struct Record_t868799569;
// System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.Record>
struct List_1_t237920701;
// System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage/Statement>
struct List_1_t3077645727;
// System.Collections.Generic.HashSet`1<System.String>
struct HashSet_1_t362681087;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// Mono.Data.Sqlite.SqliteDataReader
struct SqliteDataReader_t477797653;
// Mono.Data.Sqlite.SqliteCommand
struct SqliteCommand_t788407733;
// System.String[]
struct StringU5BU5D_t1642385972;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteDataReader477797653.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteCommand788407733.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_SyncManager_R868799569.h"

// System.Void Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage::Dispose()
extern "C"  void SQLiteLocalStorage_Dispose_m3051267384 (SQLiteLocalStorage_t3359635340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage::CreateDataset(System.String,System.String)
extern "C"  void SQLiteLocalStorage_CreateDataset_m2924842897 (SQLiteLocalStorage_t3359635340 * __this, String_t* ___identityId0, String_t* ___datasetName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage::GetValue(System.String,System.String,System.String)
extern "C"  String_t* SQLiteLocalStorage_GetValue_m1237569789 (SQLiteLocalStorage_t3359635340 * __this, String_t* ___identityId0, String_t* ___datasetName1, String_t* ___key2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage::PutValue(System.String,System.String,System.String,System.String)
extern "C"  void SQLiteLocalStorage_PutValue_m173939165 (SQLiteLocalStorage_t3359635340 * __this, String_t* ___identityId0, String_t* ___datasetName1, String_t* ___key2, String_t* ___value3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.DatasetMetadata> Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage::GetDatasetMetadata(System.String)
extern "C"  List_1_t574851791 * SQLiteLocalStorage_GetDatasetMetadata_m1766566662 (SQLiteLocalStorage_t3359635340 * __this, String_t* ___identityId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.CognitoSync.SyncManager.DatasetMetadata Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage::GetDatasetMetadata(System.String,System.String)
extern "C"  DatasetMetadata_t1205730659 * SQLiteLocalStorage_GetDatasetMetadata_m2296558160 (SQLiteLocalStorage_t3359635340 * __this, String_t* ___identityId0, String_t* ___datasetName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.CognitoSync.SyncManager.Record Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage::GetRecord(System.String,System.String,System.String)
extern "C"  Record_t868799569 * SQLiteLocalStorage_GetRecord_m3450828924 (SQLiteLocalStorage_t3359635340 * __this, String_t* ___identityId0, String_t* ___datasetName1, String_t* ___key2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage::PutRecords(System.String,System.String,System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.Record>)
extern "C"  void SQLiteLocalStorage_PutRecords_m2991239187 (SQLiteLocalStorage_t3359635340 * __this, String_t* ___identityId0, String_t* ___datasetName1, List_1_t237920701 * ___records2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage::ConditionallyPutRecords(System.String,System.String,System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.Record>,System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.Record>)
extern "C"  void SQLiteLocalStorage_ConditionallyPutRecords_m3339330035 (SQLiteLocalStorage_t3359635340 * __this, String_t* ___identityId0, String_t* ___datasetName1, List_1_t237920701 * ___records2, List_1_t237920701 * ___localRecords3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage::DeleteDataset(System.String,System.String)
extern "C"  void SQLiteLocalStorage_DeleteDataset_m140151122 (SQLiteLocalStorage_t3359635340 * __this, String_t* ___identityId0, String_t* ___datasetName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage::DeleteDataset(System.String,System.String,System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage/Statement>)
extern "C"  void SQLiteLocalStorage_DeleteDataset_m3866731267 (SQLiteLocalStorage_t3359635340 * __this, String_t* ___identityId0, String_t* ___datasetName1, List_1_t3077645727 * ___additionalStatements2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage::PurgeDataset(System.String,System.String)
extern "C"  void SQLiteLocalStorage_PurgeDataset_m3815636262 (SQLiteLocalStorage_t3359635340 * __this, String_t* ___identityId0, String_t* ___datasetName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage::GetLastSyncCount(System.String,System.String)
extern "C"  int64_t SQLiteLocalStorage_GetLastSyncCount_m3988588624 (SQLiteLocalStorage_t3359635340 * __this, String_t* ___identityId0, String_t* ___datasetName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.Record> Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage::GetModifiedRecords(System.String,System.String)
extern "C"  List_1_t237920701 * SQLiteLocalStorage_GetModifiedRecords_m3067161468 (SQLiteLocalStorage_t3359635340 * __this, String_t* ___identityId0, String_t* ___datasetName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage::UpdateLastSyncCount(System.String,System.String,System.Int64)
extern "C"  void SQLiteLocalStorage_UpdateLastSyncCount_m3515512772 (SQLiteLocalStorage_t3359635340 * __this, String_t* ___identityId0, String_t* ___datasetName1, int64_t ___lastSyncCount2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage::WipeData()
extern "C"  void SQLiteLocalStorage_WipeData_m3243656886 (SQLiteLocalStorage_t3359635340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage::ChangeIdentityId(System.String,System.String)
extern "C"  void SQLiteLocalStorage_ChangeIdentityId_m3531440760 (SQLiteLocalStorage_t3359635340 * __this, String_t* ___oldIdentityId0, String_t* ___newIdentityId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage::UpdateLastModifiedTimestamp(System.String,System.String)
extern "C"  void SQLiteLocalStorage_UpdateLastModifiedTimestamp_m3574100047 (SQLiteLocalStorage_t3359635340 * __this, String_t* ___identityId0, String_t* ___datasetName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage::PutValueHelper(System.String,System.String,System.String,System.String)
extern "C"  bool SQLiteLocalStorage_PutValueHelper_m968113175 (SQLiteLocalStorage_t3359635340 * __this, String_t* ___identityId0, String_t* ___datasetName1, String_t* ___key2, String_t* ___value3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.HashSet`1<System.String> Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage::GetCommonDatasetNames(System.String,System.String)
extern "C"  HashSet_1_t362681087 * SQLiteLocalStorage_GetCommonDatasetNames_m1848924775 (SQLiteLocalStorage_t3359635340 * __this, String_t* ___oldIdentityId0, String_t* ___newIdentityId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage::.ctor()
extern "C"  void SQLiteLocalStorage__ctor_m2526223739 (SQLiteLocalStorage_t3359635340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage::Dispose(System.Boolean)
extern "C"  void SQLiteLocalStorage_Dispose_m1093941887 (SQLiteLocalStorage_t3359635340 * __this, bool ___disposing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage::SetupDatabase()
extern "C"  void SQLiteLocalStorage_SetupDatabase_m605334193 (SQLiteLocalStorage_t3359635340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage::CreateDatasetHelper(System.String,System.Object[])
extern "C"  void SQLiteLocalStorage_CreateDatasetHelper_m2278307649 (SQLiteLocalStorage_t3359635340 * __this, String_t* ___query0, ObjectU5BU5D_t3614634134* ___parameters1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.CognitoSync.SyncManager.DatasetMetadata Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage::GetMetadataHelper(System.String,System.String)
extern "C"  DatasetMetadata_t1205730659 * SQLiteLocalStorage_GetMetadataHelper_m68325140 (SQLiteLocalStorage_t3359635340 * __this, String_t* ___identityId0, String_t* ___datasetName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.CognitoSync.SyncManager.DatasetMetadata Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage::DatasetMetadataFromReader(Mono.Data.Sqlite.SqliteDataReader)
extern "C"  DatasetMetadata_t1205730659 * SQLiteLocalStorage_DatasetMetadataFromReader_m2033155303 (Il2CppObject * __this /* static, unused */, SqliteDataReader_t477797653 * ___reader0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage::BindData(Mono.Data.Sqlite.SqliteCommand,System.Object[])
extern "C"  void SQLiteLocalStorage_BindData_m558438868 (Il2CppObject * __this /* static, unused */, SqliteCommand_t788407733 * ___command0, ObjectU5BU5D_t3614634134* ___parameters1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.DatasetMetadata> Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage::GetDatasetMetadataHelper(System.String,System.String[])
extern "C"  List_1_t574851791 * SQLiteLocalStorage_GetDatasetMetadataHelper_m3491798206 (SQLiteLocalStorage_t3359635340 * __this, String_t* ___query0, StringU5BU5D_t1642385972* ___parameters1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.CognitoSync.SyncManager.Record Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage::GetRecordHelper(System.String,System.String[])
extern "C"  Record_t868799569 * SQLiteLocalStorage_GetRecordHelper_m1020881496 (SQLiteLocalStorage_t3359635340 * __this, String_t* ___query0, StringU5BU5D_t1642385972* ___parameters1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage::GetLastSyncCountHelper(System.String,System.String[])
extern "C"  int64_t SQLiteLocalStorage_GetLastSyncCountHelper_m3756237928 (SQLiteLocalStorage_t3359635340 * __this, String_t* ___query0, StringU5BU5D_t1642385972* ___parameters1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.Record> Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage::GetModifiedRecordsHelper(System.String,System.Object[])
extern "C"  List_1_t237920701 * SQLiteLocalStorage_GetModifiedRecordsHelper_m856938638 (SQLiteLocalStorage_t3359635340 * __this, String_t* ___query0, ObjectU5BU5D_t3614634134* ___parameters1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage::UpdateOrInsertRecord(System.String,System.String,Amazon.CognitoSync.SyncManager.Record)
extern "C"  void SQLiteLocalStorage_UpdateOrInsertRecord_m2322033630 (SQLiteLocalStorage_t3359635340 * __this, String_t* ___identityId0, String_t* ___datasetName1, Record_t868799569 * ___record2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.CognitoSync.SyncManager.Record Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage::RecordFromReader(Mono.Data.Sqlite.SqliteDataReader)
extern "C"  Record_t868799569 * SQLiteLocalStorage_RecordFromReader_m1718894383 (Il2CppObject * __this /* static, unused */, SqliteDataReader_t477797653 * ___reader0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage::ExecuteMultipleHelper(System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage/Statement>)
extern "C"  void SQLiteLocalStorage_ExecuteMultipleHelper_m1277966065 (SQLiteLocalStorage_t3359635340 * __this, List_1_t3077645727 * ___statements0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage::UpdateLastSyncCountHelper(System.String,System.Object[])
extern "C"  void SQLiteLocalStorage_UpdateLastSyncCountHelper_m1533248776 (SQLiteLocalStorage_t3359635340 * __this, String_t* ___query0, ObjectU5BU5D_t3614634134* ___parameters1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage::UpdateLastModifiedTimestampHelper(System.String,System.Object[])
extern "C"  void SQLiteLocalStorage_UpdateLastModifiedTimestampHelper_m1350048275 (SQLiteLocalStorage_t3359635340 * __this, String_t* ___query0, ObjectU5BU5D_t3614634134* ___parameters1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.SyncManager.Internal.SQLiteLocalStorage::.cctor()
extern "C"  void SQLiteLocalStorage__cctor_m456438636 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
