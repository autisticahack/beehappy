﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.ComponentModel.RefreshEventArgs
struct RefreshEventArgs_t2077477224;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.ComponentModel.RefreshEventArgs::.ctor(System.Object)
extern "C"  void RefreshEventArgs__ctor_m3407787435 (RefreshEventArgs_t2077477224 * __this, Il2CppObject * ___componentChanged0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
