﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t3397334013;

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_DateTime693205669.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Auth.AWS4SigningResult
struct  AWS4SigningResult_t430803065  : public Il2CppObject
{
public:
	// System.String Amazon.Runtime.Internal.Auth.AWS4SigningResult::_awsAccessKeyId
	String_t* ____awsAccessKeyId_0;
	// System.DateTime Amazon.Runtime.Internal.Auth.AWS4SigningResult::_originalDateTime
	DateTime_t693205669  ____originalDateTime_1;
	// System.String Amazon.Runtime.Internal.Auth.AWS4SigningResult::_signedHeaders
	String_t* ____signedHeaders_2;
	// System.String Amazon.Runtime.Internal.Auth.AWS4SigningResult::_scope
	String_t* ____scope_3;
	// System.Byte[] Amazon.Runtime.Internal.Auth.AWS4SigningResult::_signingKey
	ByteU5BU5D_t3397334013* ____signingKey_4;
	// System.Byte[] Amazon.Runtime.Internal.Auth.AWS4SigningResult::_signature
	ByteU5BU5D_t3397334013* ____signature_5;

public:
	inline static int32_t get_offset_of__awsAccessKeyId_0() { return static_cast<int32_t>(offsetof(AWS4SigningResult_t430803065, ____awsAccessKeyId_0)); }
	inline String_t* get__awsAccessKeyId_0() const { return ____awsAccessKeyId_0; }
	inline String_t** get_address_of__awsAccessKeyId_0() { return &____awsAccessKeyId_0; }
	inline void set__awsAccessKeyId_0(String_t* value)
	{
		____awsAccessKeyId_0 = value;
		Il2CppCodeGenWriteBarrier(&____awsAccessKeyId_0, value);
	}

	inline static int32_t get_offset_of__originalDateTime_1() { return static_cast<int32_t>(offsetof(AWS4SigningResult_t430803065, ____originalDateTime_1)); }
	inline DateTime_t693205669  get__originalDateTime_1() const { return ____originalDateTime_1; }
	inline DateTime_t693205669 * get_address_of__originalDateTime_1() { return &____originalDateTime_1; }
	inline void set__originalDateTime_1(DateTime_t693205669  value)
	{
		____originalDateTime_1 = value;
	}

	inline static int32_t get_offset_of__signedHeaders_2() { return static_cast<int32_t>(offsetof(AWS4SigningResult_t430803065, ____signedHeaders_2)); }
	inline String_t* get__signedHeaders_2() const { return ____signedHeaders_2; }
	inline String_t** get_address_of__signedHeaders_2() { return &____signedHeaders_2; }
	inline void set__signedHeaders_2(String_t* value)
	{
		____signedHeaders_2 = value;
		Il2CppCodeGenWriteBarrier(&____signedHeaders_2, value);
	}

	inline static int32_t get_offset_of__scope_3() { return static_cast<int32_t>(offsetof(AWS4SigningResult_t430803065, ____scope_3)); }
	inline String_t* get__scope_3() const { return ____scope_3; }
	inline String_t** get_address_of__scope_3() { return &____scope_3; }
	inline void set__scope_3(String_t* value)
	{
		____scope_3 = value;
		Il2CppCodeGenWriteBarrier(&____scope_3, value);
	}

	inline static int32_t get_offset_of__signingKey_4() { return static_cast<int32_t>(offsetof(AWS4SigningResult_t430803065, ____signingKey_4)); }
	inline ByteU5BU5D_t3397334013* get__signingKey_4() const { return ____signingKey_4; }
	inline ByteU5BU5D_t3397334013** get_address_of__signingKey_4() { return &____signingKey_4; }
	inline void set__signingKey_4(ByteU5BU5D_t3397334013* value)
	{
		____signingKey_4 = value;
		Il2CppCodeGenWriteBarrier(&____signingKey_4, value);
	}

	inline static int32_t get_offset_of__signature_5() { return static_cast<int32_t>(offsetof(AWS4SigningResult_t430803065, ____signature_5)); }
	inline ByteU5BU5D_t3397334013* get__signature_5() const { return ____signature_5; }
	inline ByteU5BU5D_t3397334013** get_address_of__signature_5() { return &____signature_5; }
	inline void set__signature_5(ByteU5BU5D_t3397334013* value)
	{
		____signature_5 = value;
		Il2CppCodeGenWriteBarrier(&____signature_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
