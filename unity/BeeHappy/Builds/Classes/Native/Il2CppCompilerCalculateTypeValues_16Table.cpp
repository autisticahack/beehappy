﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleConten2303138587.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleConten3718357176.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleConten2728776481.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleType248156492.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleTypeCo1606103299.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleTypeLi2170323082.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleTypeRe1099506232.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleTypeUnio91327365.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaTotalDigitsF2939281405.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaType1795078578.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaUnique403169513.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaUse3553149267.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaUtil3576230726.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaValidationEx2387044366.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaValidationFla910489930.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaValidator1978690704.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaValidator_Tran34209471.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaValidity995929432.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaWhiteSpaceFa2007056012.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaXPath604820427.h"
#include "System_Xml_System_Xml_Schema_XmlSeverityType3547578624.h"
#include "System_Xml_System_Xml_Schema_XmlTypeCode58293802.h"
#include "System_Xml_System_Xml_Serialization_CodeIdentifier3245527920.h"
#include "System_Xml_System_Xml_Serialization_SchemaTypes3045759914.h"
#include "System_Xml_System_Xml_Serialization_TypeData3979356678.h"
#include "System_Xml_System_Xml_Serialization_TypeTranslator1077722680.h"
#include "System_Xml_System_Xml_Serialization_XmlAnyAttribute713947513.h"
#include "System_Xml_System_Xml_Serialization_XmlAnyElementA2502375235.h"
#include "System_Xml_System_Xml_Serialization_XmlAttributeAtt850813783.h"
#include "System_Xml_System_Xml_Serialization_XmlElementAttr2182839281.h"
#include "System_Xml_System_Xml_Serialization_XmlEnumAttribut919400678.h"
#include "System_Xml_System_Xml_Serialization_XmlIgnoreAttri2333915871.h"
#include "System_Xml_System_Xml_Serialization_XmlNamespaceDe2069522403.h"
#include "System_Xml_System_Xml_Serialization_XmlRootAttribu3527426713.h"
#include "System_Xml_System_Xml_Serialization_XmlSchemaProvi2486667559.h"
#include "System_Xml_System_Xml_Serialization_XmlSerializerN3063656491.h"
#include "System_Xml_System_Xml_Serialization_XmlTextAttribu3321178844.h"
#include "System_Xml_System_Xml_XPath_XPathItem3130801258.h"
#include "System_Xml_System_Xml_ConformanceLevel3761201363.h"
#include "System_Xml_Mono_Xml_DTDAutomataFactory3605390810.h"
#include "System_Xml_Mono_Xml_DTDAutomata545990600.h"
#include "System_Xml_Mono_Xml_DTDElementAutomata2864881036.h"
#include "System_Xml_Mono_Xml_DTDChoiceAutomata2810241733.h"
#include "System_Xml_Mono_Xml_DTDSequenceAutomata1228770437.h"
#include "System_Xml_Mono_Xml_DTDOneOrMoreAutomata1559764132.h"
#include "System_Xml_Mono_Xml_DTDEmptyAutomata411530619.h"
#include "System_Xml_Mono_Xml_DTDAnyAutomata146446906.h"
#include "System_Xml_Mono_Xml_DTDInvalidAutomata247674167.h"
#include "System_Xml_Mono_Xml_DTDObjectModel1113953282.h"
#include "System_Xml_Mono_Xml_DictionaryBase1005937181.h"
#include "System_Xml_Mono_Xml_DictionaryBase_U3CU3Ec__Iterat3518389200.h"
#include "System_Xml_Mono_Xml_DTDCollectionBase2621362935.h"
#include "System_Xml_Mono_Xml_DTDElementDeclarationCollectio2224069626.h"
#include "System_Xml_Mono_Xml_DTDAttListDeclarationCollection243645429.h"
#include "System_Xml_Mono_Xml_DTDEntityDeclarationCollection1212505713.h"
#include "System_Xml_Mono_Xml_DTDNotationDeclarationCollectio228085060.h"
#include "System_Xml_Mono_Xml_DTDContentModel445576364.h"
#include "System_Xml_Mono_Xml_DTDContentModelCollection3164170484.h"
#include "System_Xml_Mono_Xml_DTDNode1758286970.h"
#include "System_Xml_Mono_Xml_DTDElementDeclaration8748002.h"
#include "System_Xml_Mono_Xml_DTDAttributeDefinition3692870749.h"
#include "System_Xml_Mono_Xml_DTDAttListDeclaration2272374839.h"
#include "System_Xml_Mono_Xml_DTDEntityBase2353758560.h"
#include "System_Xml_Mono_Xml_DTDEntityDeclaration4283284771.h"
#include "System_Xml_Mono_Xml_DTDNotationDeclaration1758408116.h"
#include "System_Xml_Mono_Xml_DTDParameterEntityDeclarationC3496720022.h"
#include "System_Xml_Mono_Xml_DTDParameterEntityDeclaration252230634.h"
#include "System_Xml_Mono_Xml_DTDContentOrderType3150259539.h"
#include "System_Xml_Mono_Xml_DTDAttributeOccurenceType2819881069.h"
#include "System_Xml_Mono_Xml_DTDOccurence99371501.h"
#include "System_Xml_System_Xml_DTDReader2453137441.h"
#include "System_Xml_Mono_Xml_DTDValidatingReader4120969348.h"
#include "System_Xml_Mono_Xml_DTDValidatingReader_AttributeS1499247213.h"
#include "System_Xml_System_Xml_EntityHandling3960499440.h"
#include "System_Xml_Mono_Xml_EntityResolvingXmlReader2086920314.h"
#include "System_Xml_System_Xml_Formatting1126649075.h"
#include "System_Xml_System_Xml_NameTable594386929.h"
#include "System_Xml_System_Xml_NameTable_Entry2583369454.h"
#include "System_Xml_System_Xml_NamespaceHandling1452270444.h"
#include "System_Xml_System_Xml_NewLineHandling1737195169.h"
#include "System_Xml_System_Xml_ReadState3138905245.h"
#include "System_Xml_System_Xml_ValidationType1401987383.h"
#include "System_Xml_System_Xml_WhitespaceHandling3754063142.h"
#include "System_Xml_System_Xml_WriteState1534871862.h"
#include "System_Xml_System_Xml_XQueryConvert3510797773.h"
#include "System_Xml_System_Xml_XmlAttribute175731005.h"
#include "System_Xml_System_Xml_XmlAttributeCollection3359885287.h"
#include "System_Xml_System_Xml_XmlCDataSection1124775823.h"
#include "System_Xml_System_Xml_XmlChar1369421061.h"
#include "System_Xml_System_Xml_XmlCharacterData575748506.h"
#include "System_Xml_System_Xml_XmlComment3999331572.h"
#include "System_Xml_System_Xml_XmlConvert1936105738.h"
#include "System_Xml_System_Xml_XmlDateTimeSerializationMode137774893.h"
#include "System_Xml_System_Xml_XmlDeclaration1545359137.h"
#include "System_Xml_System_Xml_XmlDocument3649534162.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1600 = { sizeof (XmlSchemaSimpleContent_t2303138587), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1600[1] = 
{
	XmlSchemaSimpleContent_t2303138587::get_offset_of_content_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1601 = { sizeof (XmlSchemaSimpleContentExtension_t3718357176), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1601[3] = 
{
	XmlSchemaSimpleContentExtension_t3718357176::get_offset_of_any_17(),
	XmlSchemaSimpleContentExtension_t3718357176::get_offset_of_attributes_18(),
	XmlSchemaSimpleContentExtension_t3718357176::get_offset_of_baseTypeName_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1602 = { sizeof (XmlSchemaSimpleContentRestriction_t2728776481), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1602[5] = 
{
	XmlSchemaSimpleContentRestriction_t2728776481::get_offset_of_any_17(),
	XmlSchemaSimpleContentRestriction_t2728776481::get_offset_of_attributes_18(),
	XmlSchemaSimpleContentRestriction_t2728776481::get_offset_of_baseType_19(),
	XmlSchemaSimpleContentRestriction_t2728776481::get_offset_of_baseTypeName_20(),
	XmlSchemaSimpleContentRestriction_t2728776481::get_offset_of_facets_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1603 = { sizeof (XmlSchemaSimpleType_t248156492), -1, sizeof(XmlSchemaSimpleType_t248156492_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1603[54] = 
{
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_schemaLocationType_28(),
	XmlSchemaSimpleType_t248156492::get_offset_of_content_29(),
	XmlSchemaSimpleType_t248156492::get_offset_of_islocal_30(),
	XmlSchemaSimpleType_t248156492::get_offset_of_recursed_31(),
	XmlSchemaSimpleType_t248156492::get_offset_of_variety_32(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsAnySimpleType_33(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsString_34(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsBoolean_35(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsDecimal_36(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsFloat_37(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsDouble_38(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsDuration_39(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsDateTime_40(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsTime_41(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsDate_42(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsGYearMonth_43(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsGYear_44(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsGMonthDay_45(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsGDay_46(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsGMonth_47(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsHexBinary_48(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsBase64Binary_49(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsAnyUri_50(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsQName_51(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsNotation_52(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsNormalizedString_53(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsToken_54(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsLanguage_55(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsNMToken_56(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsNMTokens_57(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsName_58(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsNCName_59(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsID_60(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsIDRef_61(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsIDRefs_62(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsEntity_63(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsEntities_64(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsInteger_65(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsNonPositiveInteger_66(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsNegativeInteger_67(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsLong_68(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsInt_69(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsShort_70(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsByte_71(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsNonNegativeInteger_72(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsUnsignedLong_73(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsUnsignedInt_74(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsUnsignedShort_75(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsUnsignedByte_76(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsPositiveInteger_77(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XdtUntypedAtomic_78(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XdtAnyAtomicType_79(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XdtYearMonthDuration_80(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XdtDayTimeDuration_81(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1604 = { sizeof (XmlSchemaSimpleTypeContent_t1606103299), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1604[1] = 
{
	XmlSchemaSimpleTypeContent_t1606103299::get_offset_of_OwnerType_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1605 = { sizeof (XmlSchemaSimpleTypeList_t2170323082), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1605[4] = 
{
	XmlSchemaSimpleTypeList_t2170323082::get_offset_of_itemType_17(),
	XmlSchemaSimpleTypeList_t2170323082::get_offset_of_itemTypeName_18(),
	XmlSchemaSimpleTypeList_t2170323082::get_offset_of_validatedListItemType_19(),
	XmlSchemaSimpleTypeList_t2170323082::get_offset_of_validatedListItemSchemaType_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1606 = { sizeof (XmlSchemaSimpleTypeRestriction_t1099506232), -1, sizeof(XmlSchemaSimpleTypeRestriction_t1099506232_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1606[18] = 
{
	XmlSchemaSimpleTypeRestriction_t1099506232::get_offset_of_baseType_17(),
	XmlSchemaSimpleTypeRestriction_t1099506232::get_offset_of_baseTypeName_18(),
	XmlSchemaSimpleTypeRestriction_t1099506232::get_offset_of_facets_19(),
	XmlSchemaSimpleTypeRestriction_t1099506232::get_offset_of_enumarationFacetValues_20(),
	XmlSchemaSimpleTypeRestriction_t1099506232::get_offset_of_patternFacetValues_21(),
	XmlSchemaSimpleTypeRestriction_t1099506232::get_offset_of_rexPatterns_22(),
	XmlSchemaSimpleTypeRestriction_t1099506232::get_offset_of_lengthFacet_23(),
	XmlSchemaSimpleTypeRestriction_t1099506232::get_offset_of_maxLengthFacet_24(),
	XmlSchemaSimpleTypeRestriction_t1099506232::get_offset_of_minLengthFacet_25(),
	XmlSchemaSimpleTypeRestriction_t1099506232::get_offset_of_fractionDigitsFacet_26(),
	XmlSchemaSimpleTypeRestriction_t1099506232::get_offset_of_totalDigitsFacet_27(),
	XmlSchemaSimpleTypeRestriction_t1099506232::get_offset_of_maxInclusiveFacet_28(),
	XmlSchemaSimpleTypeRestriction_t1099506232::get_offset_of_maxExclusiveFacet_29(),
	XmlSchemaSimpleTypeRestriction_t1099506232::get_offset_of_minInclusiveFacet_30(),
	XmlSchemaSimpleTypeRestriction_t1099506232::get_offset_of_minExclusiveFacet_31(),
	XmlSchemaSimpleTypeRestriction_t1099506232::get_offset_of_fixedFacets_32(),
	XmlSchemaSimpleTypeRestriction_t1099506232_StaticFields::get_offset_of_lengthStyle_33(),
	XmlSchemaSimpleTypeRestriction_t1099506232_StaticFields::get_offset_of_listFacets_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1607 = { sizeof (XmlSchemaSimpleTypeUnion_t91327365), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1607[4] = 
{
	XmlSchemaSimpleTypeUnion_t91327365::get_offset_of_baseTypes_17(),
	XmlSchemaSimpleTypeUnion_t91327365::get_offset_of_memberTypes_18(),
	XmlSchemaSimpleTypeUnion_t91327365::get_offset_of_validatedTypes_19(),
	XmlSchemaSimpleTypeUnion_t91327365::get_offset_of_validatedSchemaTypes_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1608 = { sizeof (XmlSchemaTotalDigitsFacet_t2939281405), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1609 = { sizeof (XmlSchemaType_t1795078578), -1, sizeof(XmlSchemaType_t1795078578_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1609[12] = 
{
	XmlSchemaType_t1795078578::get_offset_of_final_16(),
	XmlSchemaType_t1795078578::get_offset_of_isMixed_17(),
	XmlSchemaType_t1795078578::get_offset_of_name_18(),
	XmlSchemaType_t1795078578::get_offset_of_recursed_19(),
	XmlSchemaType_t1795078578::get_offset_of_BaseSchemaTypeName_20(),
	XmlSchemaType_t1795078578::get_offset_of_BaseXmlSchemaTypeInternal_21(),
	XmlSchemaType_t1795078578::get_offset_of_DatatypeInternal_22(),
	XmlSchemaType_t1795078578::get_offset_of_resolvedDerivedBy_23(),
	XmlSchemaType_t1795078578::get_offset_of_finalResolved_24(),
	XmlSchemaType_t1795078578::get_offset_of_QNameInternal_25(),
	XmlSchemaType_t1795078578_StaticFields::get_offset_of_U3CU3Ef__switchU24map2E_26(),
	XmlSchemaType_t1795078578_StaticFields::get_offset_of_U3CU3Ef__switchU24map2F_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1610 = { sizeof (XmlSchemaUnique_t403169513), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1611 = { sizeof (XmlSchemaUse_t3553149267)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1611[5] = 
{
	XmlSchemaUse_t3553149267::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1612 = { sizeof (XmlSchemaUtil_t3576230726), -1, sizeof(XmlSchemaUtil_t3576230726_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1612[10] = 
{
	XmlSchemaUtil_t3576230726_StaticFields::get_offset_of_FinalAllowed_0(),
	XmlSchemaUtil_t3576230726_StaticFields::get_offset_of_ElementBlockAllowed_1(),
	XmlSchemaUtil_t3576230726_StaticFields::get_offset_of_ComplexTypeBlockAllowed_2(),
	XmlSchemaUtil_t3576230726_StaticFields::get_offset_of_StrictMsCompliant_3(),
	XmlSchemaUtil_t3576230726_StaticFields::get_offset_of_U3CU3Ef__switchU24map36_4(),
	XmlSchemaUtil_t3576230726_StaticFields::get_offset_of_U3CU3Ef__switchU24map37_5(),
	XmlSchemaUtil_t3576230726_StaticFields::get_offset_of_U3CU3Ef__switchU24map38_6(),
	XmlSchemaUtil_t3576230726_StaticFields::get_offset_of_U3CU3Ef__switchU24map39_7(),
	XmlSchemaUtil_t3576230726_StaticFields::get_offset_of_U3CU3Ef__switchU24map3A_8(),
	XmlSchemaUtil_t3576230726_StaticFields::get_offset_of_U3CU3Ef__switchU24map3B_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1613 = { sizeof (XmlSchemaValidationException_t2387044366), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1614 = { sizeof (XmlSchemaValidationFlags_t910489930)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1614[7] = 
{
	XmlSchemaValidationFlags_t910489930::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1615 = { sizeof (XmlSchemaValidator_t1978690704), -1, sizeof(XmlSchemaValidator_t1978690704_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1615[26] = 
{
	XmlSchemaValidator_t1978690704_StaticFields::get_offset_of_emptyAttributeArray_0(),
	XmlSchemaValidator_t1978690704::get_offset_of_nominalEventSender_1(),
	XmlSchemaValidator_t1978690704::get_offset_of_lineInfo_2(),
	XmlSchemaValidator_t1978690704::get_offset_of_nsResolver_3(),
	XmlSchemaValidator_t1978690704::get_offset_of_sourceUri_4(),
	XmlSchemaValidator_t1978690704::get_offset_of_nameTable_5(),
	XmlSchemaValidator_t1978690704::get_offset_of_schemas_6(),
	XmlSchemaValidator_t1978690704::get_offset_of_xmlResolver_7(),
	XmlSchemaValidator_t1978690704::get_offset_of_options_8(),
	XmlSchemaValidator_t1978690704::get_offset_of_transition_9(),
	XmlSchemaValidator_t1978690704::get_offset_of_state_10(),
	XmlSchemaValidator_t1978690704::get_offset_of_occuredAtts_11(),
	XmlSchemaValidator_t1978690704::get_offset_of_defaultAttributes_12(),
	XmlSchemaValidator_t1978690704::get_offset_of_defaultAttributesCache_13(),
	XmlSchemaValidator_t1978690704::get_offset_of_idManager_14(),
	XmlSchemaValidator_t1978690704::get_offset_of_keyTables_15(),
	XmlSchemaValidator_t1978690704::get_offset_of_currentKeyFieldConsumers_16(),
	XmlSchemaValidator_t1978690704::get_offset_of_tmpKeyrefPool_17(),
	XmlSchemaValidator_t1978690704::get_offset_of_elementQNameStack_18(),
	XmlSchemaValidator_t1978690704::get_offset_of_storedCharacters_19(),
	XmlSchemaValidator_t1978690704::get_offset_of_shouldValidateCharacters_20(),
	XmlSchemaValidator_t1978690704::get_offset_of_depth_21(),
	XmlSchemaValidator_t1978690704::get_offset_of_xsiNilDepth_22(),
	XmlSchemaValidator_t1978690704::get_offset_of_skipValidationDepth_23(),
	XmlSchemaValidator_t1978690704::get_offset_of_CurrentAttributeType_24(),
	XmlSchemaValidator_t1978690704::get_offset_of_ValidationEventHandler_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1616 = { sizeof (Transition_t34209471)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1616[5] = 
{
	Transition_t34209471::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1617 = { sizeof (XmlSchemaValidity_t995929432)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1617[4] = 
{
	XmlSchemaValidity_t995929432::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1618 = { sizeof (XmlSchemaWhiteSpaceFacet_t2007056012), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1619 = { sizeof (XmlSchemaXPath_t604820427), -1, sizeof(XmlSchemaXPath_t604820427_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1619[6] = 
{
	XmlSchemaXPath_t604820427::get_offset_of_xpath_16(),
	XmlSchemaXPath_t604820427::get_offset_of_nsmgr_17(),
	XmlSchemaXPath_t604820427::get_offset_of_isSelector_18(),
	XmlSchemaXPath_t604820427::get_offset_of_compiledExpression_19(),
	XmlSchemaXPath_t604820427::get_offset_of_currentPath_20(),
	XmlSchemaXPath_t604820427_StaticFields::get_offset_of_U3CU3Ef__switchU24map3C_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1620 = { sizeof (XmlSeverityType_t3547578624)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1620[3] = 
{
	XmlSeverityType_t3547578624::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1621 = { sizeof (XmlTypeCode_t58293802)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1621[56] = 
{
	XmlTypeCode_t58293802::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1622 = { sizeof (CodeIdentifier_t3245527920), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1623 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1624 = { sizeof (SchemaTypes_t3045759914)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1624[9] = 
{
	SchemaTypes_t3045759914::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1625 = { sizeof (TypeData_t3979356678), -1, sizeof(TypeData_t3979356678_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1625[12] = 
{
	TypeData_t3979356678::get_offset_of_type_0(),
	TypeData_t3979356678::get_offset_of_elementName_1(),
	TypeData_t3979356678::get_offset_of_sType_2(),
	TypeData_t3979356678::get_offset_of_listItemType_3(),
	TypeData_t3979356678::get_offset_of_typeName_4(),
	TypeData_t3979356678::get_offset_of_fullTypeName_5(),
	TypeData_t3979356678::get_offset_of_listItemTypeData_6(),
	TypeData_t3979356678::get_offset_of_mappedType_7(),
	TypeData_t3979356678::get_offset_of_facet_8(),
	TypeData_t3979356678::get_offset_of_hasPublicConstructor_9(),
	TypeData_t3979356678::get_offset_of_nullableOverride_10(),
	TypeData_t3979356678_StaticFields::get_offset_of_keywords_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1626 = { sizeof (TypeTranslator_t1077722680), -1, sizeof(TypeTranslator_t1077722680_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1626[4] = 
{
	TypeTranslator_t1077722680_StaticFields::get_offset_of_nameCache_0(),
	TypeTranslator_t1077722680_StaticFields::get_offset_of_primitiveTypes_1(),
	TypeTranslator_t1077722680_StaticFields::get_offset_of_primitiveArrayTypes_2(),
	TypeTranslator_t1077722680_StaticFields::get_offset_of_nullableTypes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1627 = { sizeof (XmlAnyAttributeAttribute_t713947513), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1628 = { sizeof (XmlAnyElementAttribute_t2502375235), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1628[1] = 
{
	XmlAnyElementAttribute_t2502375235::get_offset_of_order_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1629 = { sizeof (XmlAttributeAttribute_t850813783), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1629[2] = 
{
	XmlAttributeAttribute_t850813783::get_offset_of_attributeName_0(),
	XmlAttributeAttribute_t850813783::get_offset_of_dataType_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1630 = { sizeof (XmlElementAttribute_t2182839281), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1630[3] = 
{
	XmlElementAttribute_t2182839281::get_offset_of_elementName_0(),
	XmlElementAttribute_t2182839281::get_offset_of_type_1(),
	XmlElementAttribute_t2182839281::get_offset_of_order_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1631 = { sizeof (XmlEnumAttribute_t919400678), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1631[1] = 
{
	XmlEnumAttribute_t919400678::get_offset_of_name_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1632 = { sizeof (XmlIgnoreAttribute_t2333915871), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1633 = { sizeof (XmlNamespaceDeclarationsAttribute_t2069522403), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1634 = { sizeof (XmlRootAttribute_t3527426713), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1634[3] = 
{
	XmlRootAttribute_t3527426713::get_offset_of_elementName_0(),
	XmlRootAttribute_t3527426713::get_offset_of_isNullable_1(),
	XmlRootAttribute_t3527426713::get_offset_of_ns_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1635 = { sizeof (XmlSchemaProviderAttribute_t2486667559), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1635[2] = 
{
	XmlSchemaProviderAttribute_t2486667559::get_offset_of__methodName_0(),
	XmlSchemaProviderAttribute_t2486667559::get_offset_of__isAny_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1636 = { sizeof (XmlSerializerNamespaces_t3063656491), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1636[1] = 
{
	XmlSerializerNamespaces_t3063656491::get_offset_of_namespaces_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1637 = { sizeof (XmlTextAttribute_t3321178844), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1638 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1639 = { sizeof (XPathItem_t3130801258), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1640 = { sizeof (ConformanceLevel_t3761201363)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1640[4] = 
{
	ConformanceLevel_t3761201363::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1641 = { sizeof (DTDAutomataFactory_t3605390810), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1641[3] = 
{
	DTDAutomataFactory_t3605390810::get_offset_of_root_0(),
	DTDAutomataFactory_t3605390810::get_offset_of_choiceTable_1(),
	DTDAutomataFactory_t3605390810::get_offset_of_sequenceTable_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1642 = { sizeof (DTDAutomata_t545990600), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1642[1] = 
{
	DTDAutomata_t545990600::get_offset_of_root_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1643 = { sizeof (DTDElementAutomata_t2864881036), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1643[1] = 
{
	DTDElementAutomata_t2864881036::get_offset_of_name_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1644 = { sizeof (DTDChoiceAutomata_t2810241733), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1644[4] = 
{
	DTDChoiceAutomata_t2810241733::get_offset_of_left_1(),
	DTDChoiceAutomata_t2810241733::get_offset_of_right_2(),
	DTDChoiceAutomata_t2810241733::get_offset_of_hasComputedEmptiable_3(),
	DTDChoiceAutomata_t2810241733::get_offset_of_cachedEmptiable_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1645 = { sizeof (DTDSequenceAutomata_t1228770437), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1645[4] = 
{
	DTDSequenceAutomata_t1228770437::get_offset_of_left_1(),
	DTDSequenceAutomata_t1228770437::get_offset_of_right_2(),
	DTDSequenceAutomata_t1228770437::get_offset_of_hasComputedEmptiable_3(),
	DTDSequenceAutomata_t1228770437::get_offset_of_cachedEmptiable_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1646 = { sizeof (DTDOneOrMoreAutomata_t1559764132), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1646[1] = 
{
	DTDOneOrMoreAutomata_t1559764132::get_offset_of_children_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1647 = { sizeof (DTDEmptyAutomata_t411530619), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1648 = { sizeof (DTDAnyAutomata_t146446906), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1649 = { sizeof (DTDInvalidAutomata_t247674167), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1650 = { sizeof (DTDObjectModel_t1113953282), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1650[23] = 
{
	DTDObjectModel_t1113953282::get_offset_of_factory_0(),
	DTDObjectModel_t1113953282::get_offset_of_rootAutomata_1(),
	DTDObjectModel_t1113953282::get_offset_of_emptyAutomata_2(),
	DTDObjectModel_t1113953282::get_offset_of_anyAutomata_3(),
	DTDObjectModel_t1113953282::get_offset_of_invalidAutomata_4(),
	DTDObjectModel_t1113953282::get_offset_of_elementDecls_5(),
	DTDObjectModel_t1113953282::get_offset_of_attListDecls_6(),
	DTDObjectModel_t1113953282::get_offset_of_peDecls_7(),
	DTDObjectModel_t1113953282::get_offset_of_entityDecls_8(),
	DTDObjectModel_t1113953282::get_offset_of_notationDecls_9(),
	DTDObjectModel_t1113953282::get_offset_of_validationErrors_10(),
	DTDObjectModel_t1113953282::get_offset_of_resolver_11(),
	DTDObjectModel_t1113953282::get_offset_of_nameTable_12(),
	DTDObjectModel_t1113953282::get_offset_of_externalResources_13(),
	DTDObjectModel_t1113953282::get_offset_of_baseURI_14(),
	DTDObjectModel_t1113953282::get_offset_of_name_15(),
	DTDObjectModel_t1113953282::get_offset_of_publicId_16(),
	DTDObjectModel_t1113953282::get_offset_of_systemId_17(),
	DTDObjectModel_t1113953282::get_offset_of_intSubset_18(),
	DTDObjectModel_t1113953282::get_offset_of_intSubsetHasPERef_19(),
	DTDObjectModel_t1113953282::get_offset_of_isStandalone_20(),
	DTDObjectModel_t1113953282::get_offset_of_lineNumber_21(),
	DTDObjectModel_t1113953282::get_offset_of_linePosition_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1651 = { sizeof (DictionaryBase_t1005937181), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1652 = { sizeof (U3CU3Ec__Iterator3_t3518389200), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1652[5] = 
{
	U3CU3Ec__Iterator3_t3518389200::get_offset_of_U3CU24s_431U3E__0_0(),
	U3CU3Ec__Iterator3_t3518389200::get_offset_of_U3CpU3E__1_1(),
	U3CU3Ec__Iterator3_t3518389200::get_offset_of_U24PC_2(),
	U3CU3Ec__Iterator3_t3518389200::get_offset_of_U24current_3(),
	U3CU3Ec__Iterator3_t3518389200::get_offset_of_U3CU3Ef__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1653 = { sizeof (DTDCollectionBase_t2621362935), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1653[1] = 
{
	DTDCollectionBase_t2621362935::get_offset_of_root_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1654 = { sizeof (DTDElementDeclarationCollection_t2224069626), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1655 = { sizeof (DTDAttListDeclarationCollection_t243645429), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1656 = { sizeof (DTDEntityDeclarationCollection_t1212505713), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1657 = { sizeof (DTDNotationDeclarationCollection_t228085060), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1658 = { sizeof (DTDContentModel_t445576364), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1658[7] = 
{
	DTDContentModel_t445576364::get_offset_of_root_5(),
	DTDContentModel_t445576364::get_offset_of_compiledAutomata_6(),
	DTDContentModel_t445576364::get_offset_of_ownerElementName_7(),
	DTDContentModel_t445576364::get_offset_of_elementName_8(),
	DTDContentModel_t445576364::get_offset_of_orderType_9(),
	DTDContentModel_t445576364::get_offset_of_childModels_10(),
	DTDContentModel_t445576364::get_offset_of_occurence_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1659 = { sizeof (DTDContentModelCollection_t3164170484), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1659[1] = 
{
	DTDContentModelCollection_t3164170484::get_offset_of_contentModel_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1660 = { sizeof (DTDNode_t1758286970), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1660[5] = 
{
	DTDNode_t1758286970::get_offset_of_root_0(),
	DTDNode_t1758286970::get_offset_of_isInternalSubset_1(),
	DTDNode_t1758286970::get_offset_of_baseURI_2(),
	DTDNode_t1758286970::get_offset_of_lineNumber_3(),
	DTDNode_t1758286970::get_offset_of_linePosition_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1661 = { sizeof (DTDElementDeclaration_t8748002), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1661[6] = 
{
	DTDElementDeclaration_t8748002::get_offset_of_root_5(),
	DTDElementDeclaration_t8748002::get_offset_of_contentModel_6(),
	DTDElementDeclaration_t8748002::get_offset_of_name_7(),
	DTDElementDeclaration_t8748002::get_offset_of_isEmpty_8(),
	DTDElementDeclaration_t8748002::get_offset_of_isAny_9(),
	DTDElementDeclaration_t8748002::get_offset_of_isMixedContent_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1662 = { sizeof (DTDAttributeDefinition_t3692870749), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1662[7] = 
{
	DTDAttributeDefinition_t3692870749::get_offset_of_name_5(),
	DTDAttributeDefinition_t3692870749::get_offset_of_datatype_6(),
	DTDAttributeDefinition_t3692870749::get_offset_of_enumeratedLiterals_7(),
	DTDAttributeDefinition_t3692870749::get_offset_of_unresolvedDefault_8(),
	DTDAttributeDefinition_t3692870749::get_offset_of_enumeratedNotations_9(),
	DTDAttributeDefinition_t3692870749::get_offset_of_occurenceType_10(),
	DTDAttributeDefinition_t3692870749::get_offset_of_resolvedDefaultValue_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1663 = { sizeof (DTDAttListDeclaration_t2272374839), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1663[3] = 
{
	DTDAttListDeclaration_t2272374839::get_offset_of_name_5(),
	DTDAttListDeclaration_t2272374839::get_offset_of_attributeOrders_6(),
	DTDAttListDeclaration_t2272374839::get_offset_of_attributes_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1664 = { sizeof (DTDEntityBase_t2353758560), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1664[10] = 
{
	DTDEntityBase_t2353758560::get_offset_of_name_5(),
	DTDEntityBase_t2353758560::get_offset_of_publicId_6(),
	DTDEntityBase_t2353758560::get_offset_of_systemId_7(),
	DTDEntityBase_t2353758560::get_offset_of_literalValue_8(),
	DTDEntityBase_t2353758560::get_offset_of_replacementText_9(),
	DTDEntityBase_t2353758560::get_offset_of_uriString_10(),
	DTDEntityBase_t2353758560::get_offset_of_absUri_11(),
	DTDEntityBase_t2353758560::get_offset_of_isInvalid_12(),
	DTDEntityBase_t2353758560::get_offset_of_loadFailed_13(),
	DTDEntityBase_t2353758560::get_offset_of_resolver_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1665 = { sizeof (DTDEntityDeclaration_t4283284771), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1665[6] = 
{
	DTDEntityDeclaration_t4283284771::get_offset_of_entityValue_15(),
	DTDEntityDeclaration_t4283284771::get_offset_of_notationName_16(),
	DTDEntityDeclaration_t4283284771::get_offset_of_ReferencingEntities_17(),
	DTDEntityDeclaration_t4283284771::get_offset_of_scanned_18(),
	DTDEntityDeclaration_t4283284771::get_offset_of_recursed_19(),
	DTDEntityDeclaration_t4283284771::get_offset_of_hasExternalReference_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1666 = { sizeof (DTDNotationDeclaration_t1758408116), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1666[5] = 
{
	DTDNotationDeclaration_t1758408116::get_offset_of_name_5(),
	DTDNotationDeclaration_t1758408116::get_offset_of_localName_6(),
	DTDNotationDeclaration_t1758408116::get_offset_of_prefix_7(),
	DTDNotationDeclaration_t1758408116::get_offset_of_publicId_8(),
	DTDNotationDeclaration_t1758408116::get_offset_of_systemId_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1667 = { sizeof (DTDParameterEntityDeclarationCollection_t3496720022), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1667[2] = 
{
	DTDParameterEntityDeclarationCollection_t3496720022::get_offset_of_peDecls_0(),
	DTDParameterEntityDeclarationCollection_t3496720022::get_offset_of_root_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1668 = { sizeof (DTDParameterEntityDeclaration_t252230634), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1669 = { sizeof (DTDContentOrderType_t3150259539)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1669[4] = 
{
	DTDContentOrderType_t3150259539::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1670 = { sizeof (DTDAttributeOccurenceType_t2819881069)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1670[5] = 
{
	DTDAttributeOccurenceType_t2819881069::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1671 = { sizeof (DTDOccurence_t99371501)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1671[5] = 
{
	DTDOccurence_t99371501::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1672 = { sizeof (DTDReader_t2453137441), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1672[14] = 
{
	DTDReader_t2453137441::get_offset_of_currentInput_0(),
	DTDReader_t2453137441::get_offset_of_parserInputStack_1(),
	DTDReader_t2453137441::get_offset_of_nameBuffer_2(),
	DTDReader_t2453137441::get_offset_of_nameLength_3(),
	DTDReader_t2453137441::get_offset_of_nameCapacity_4(),
	DTDReader_t2453137441::get_offset_of_valueBuffer_5(),
	DTDReader_t2453137441::get_offset_of_currentLinkedNodeLineNumber_6(),
	DTDReader_t2453137441::get_offset_of_currentLinkedNodeLinePosition_7(),
	DTDReader_t2453137441::get_offset_of_dtdIncludeSect_8(),
	DTDReader_t2453137441::get_offset_of_normalization_9(),
	DTDReader_t2453137441::get_offset_of_processingInternalSubset_10(),
	DTDReader_t2453137441::get_offset_of_cachedPublicId_11(),
	DTDReader_t2453137441::get_offset_of_cachedSystemId_12(),
	DTDReader_t2453137441::get_offset_of_DTD_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1673 = { sizeof (DTDValidatingReader_t4120969348), -1, sizeof(DTDValidatingReader_t4120969348_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1673[29] = 
{
	DTDValidatingReader_t4120969348::get_offset_of_reader_3(),
	DTDValidatingReader_t4120969348::get_offset_of_sourceTextReader_4(),
	DTDValidatingReader_t4120969348::get_offset_of_validatingReader_5(),
	DTDValidatingReader_t4120969348::get_offset_of_dtd_6(),
	DTDValidatingReader_t4120969348::get_offset_of_resolver_7(),
	DTDValidatingReader_t4120969348::get_offset_of_currentElement_8(),
	DTDValidatingReader_t4120969348::get_offset_of_attributes_9(),
	DTDValidatingReader_t4120969348::get_offset_of_attributeCount_10(),
	DTDValidatingReader_t4120969348::get_offset_of_currentAttribute_11(),
	DTDValidatingReader_t4120969348::get_offset_of_consumedAttribute_12(),
	DTDValidatingReader_t4120969348::get_offset_of_elementStack_13(),
	DTDValidatingReader_t4120969348::get_offset_of_automataStack_14(),
	DTDValidatingReader_t4120969348::get_offset_of_popScope_15(),
	DTDValidatingReader_t4120969348::get_offset_of_isStandalone_16(),
	DTDValidatingReader_t4120969348::get_offset_of_currentAutomata_17(),
	DTDValidatingReader_t4120969348::get_offset_of_previousAutomata_18(),
	DTDValidatingReader_t4120969348::get_offset_of_idList_19(),
	DTDValidatingReader_t4120969348::get_offset_of_missingIDReferences_20(),
	DTDValidatingReader_t4120969348::get_offset_of_nsmgr_21(),
	DTDValidatingReader_t4120969348::get_offset_of_currentTextValue_22(),
	DTDValidatingReader_t4120969348::get_offset_of_constructingTextValue_23(),
	DTDValidatingReader_t4120969348::get_offset_of_shouldResetCurrentTextValue_24(),
	DTDValidatingReader_t4120969348::get_offset_of_isSignificantWhitespace_25(),
	DTDValidatingReader_t4120969348::get_offset_of_isWhitespace_26(),
	DTDValidatingReader_t4120969348::get_offset_of_isText_27(),
	DTDValidatingReader_t4120969348::get_offset_of_attributeValueEntityStack_28(),
	DTDValidatingReader_t4120969348::get_offset_of_valueBuilder_29(),
	DTDValidatingReader_t4120969348::get_offset_of_whitespaceChars_30(),
	DTDValidatingReader_t4120969348_StaticFields::get_offset_of_U3CU3Ef__switchU24map43_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1674 = { sizeof (AttributeSlot_t1499247213), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1674[6] = 
{
	AttributeSlot_t1499247213::get_offset_of_Name_0(),
	AttributeSlot_t1499247213::get_offset_of_LocalName_1(),
	AttributeSlot_t1499247213::get_offset_of_NS_2(),
	AttributeSlot_t1499247213::get_offset_of_Prefix_3(),
	AttributeSlot_t1499247213::get_offset_of_Value_4(),
	AttributeSlot_t1499247213::get_offset_of_IsDefault_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1675 = { sizeof (EntityHandling_t3960499440)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1675[3] = 
{
	EntityHandling_t3960499440::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1676 = { sizeof (EntityResolvingXmlReader_t2086920314), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1676[8] = 
{
	EntityResolvingXmlReader_t2086920314::get_offset_of_entity_3(),
	EntityResolvingXmlReader_t2086920314::get_offset_of_source_4(),
	EntityResolvingXmlReader_t2086920314::get_offset_of_context_5(),
	EntityResolvingXmlReader_t2086920314::get_offset_of_resolver_6(),
	EntityResolvingXmlReader_t2086920314::get_offset_of_entity_handling_7(),
	EntityResolvingXmlReader_t2086920314::get_offset_of_entity_inside_attr_8(),
	EntityResolvingXmlReader_t2086920314::get_offset_of_inside_attr_9(),
	EntityResolvingXmlReader_t2086920314::get_offset_of_do_resolve_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1677 = { sizeof (Formatting_t1126649075)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1677[3] = 
{
	Formatting_t1126649075::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1678 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1679 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1680 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1681 = { sizeof (NameTable_t594386929), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1681[3] = 
{
	NameTable_t594386929::get_offset_of_count_0(),
	NameTable_t594386929::get_offset_of_buckets_1(),
	NameTable_t594386929::get_offset_of_size_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1682 = { sizeof (Entry_t2583369454), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1682[4] = 
{
	Entry_t2583369454::get_offset_of_str_0(),
	Entry_t2583369454::get_offset_of_hash_1(),
	Entry_t2583369454::get_offset_of_len_2(),
	Entry_t2583369454::get_offset_of_next_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1683 = { sizeof (NamespaceHandling_t1452270444)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1683[3] = 
{
	NamespaceHandling_t1452270444::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1684 = { sizeof (NewLineHandling_t1737195169)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1684[4] = 
{
	NewLineHandling_t1737195169::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1685 = { sizeof (ReadState_t3138905245)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1685[6] = 
{
	ReadState_t3138905245::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1686 = { sizeof (ValidationType_t1401987383)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1686[6] = 
{
	ValidationType_t1401987383::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1687 = { sizeof (WhitespaceHandling_t3754063142)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1687[4] = 
{
	WhitespaceHandling_t3754063142::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1688 = { sizeof (WriteState_t1534871862)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1688[8] = 
{
	WriteState_t1534871862::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1689 = { sizeof (XQueryConvert_t3510797773), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1690 = { sizeof (XmlAttribute_t175731005), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1690[4] = 
{
	XmlAttribute_t175731005::get_offset_of_name_5(),
	XmlAttribute_t175731005::get_offset_of_isDefault_6(),
	XmlAttribute_t175731005::get_offset_of_lastLinkedChild_7(),
	XmlAttribute_t175731005::get_offset_of_schemaInfo_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1691 = { sizeof (XmlAttributeCollection_t3359885287), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1691[2] = 
{
	XmlAttributeCollection_t3359885287::get_offset_of_ownerElement_4(),
	XmlAttributeCollection_t3359885287::get_offset_of_ownerDocument_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1692 = { sizeof (XmlCDataSection_t1124775823), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1693 = { sizeof (XmlChar_t1369421061), -1, sizeof(XmlChar_t1369421061_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1693[5] = 
{
	XmlChar_t1369421061_StaticFields::get_offset_of_WhitespaceChars_0(),
	XmlChar_t1369421061_StaticFields::get_offset_of_firstNamePages_1(),
	XmlChar_t1369421061_StaticFields::get_offset_of_namePages_2(),
	XmlChar_t1369421061_StaticFields::get_offset_of_nameBitmap_3(),
	XmlChar_t1369421061_StaticFields::get_offset_of_U3CU3Ef__switchU24map47_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1694 = { sizeof (XmlCharacterData_t575748506), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1694[1] = 
{
	XmlCharacterData_t575748506::get_offset_of_data_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1695 = { sizeof (XmlComment_t3999331572), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1696 = { sizeof (XmlConvert_t1936105738), -1, sizeof(XmlConvert_t1936105738_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1696[8] = 
{
	XmlConvert_t1936105738_StaticFields::get_offset_of_datetimeFormats_0(),
	XmlConvert_t1936105738_StaticFields::get_offset_of_defaultDateTimeFormats_1(),
	XmlConvert_t1936105738_StaticFields::get_offset_of_roundtripDateTimeFormats_2(),
	XmlConvert_t1936105738_StaticFields::get_offset_of_localDateTimeFormats_3(),
	XmlConvert_t1936105738_StaticFields::get_offset_of_utcDateTimeFormats_4(),
	XmlConvert_t1936105738_StaticFields::get_offset_of_unspecifiedDateTimeFormats_5(),
	XmlConvert_t1936105738_StaticFields::get_offset_of__defaultStyle_6(),
	XmlConvert_t1936105738_StaticFields::get_offset_of_U3CU3Ef__switchU24map49_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1697 = { sizeof (XmlDateTimeSerializationMode_t137774893)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1697[5] = 
{
	XmlDateTimeSerializationMode_t137774893::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1698 = { sizeof (XmlDeclaration_t1545359137), -1, sizeof(XmlDeclaration_t1545359137_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1698[4] = 
{
	XmlDeclaration_t1545359137::get_offset_of_encoding_6(),
	XmlDeclaration_t1545359137::get_offset_of_standalone_7(),
	XmlDeclaration_t1545359137::get_offset_of_version_8(),
	XmlDeclaration_t1545359137_StaticFields::get_offset_of_U3CU3Ef__switchU24map4A_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1699 = { sizeof (XmlDocument_t3649534162), -1, sizeof(XmlDocument_t3649534162_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1699[19] = 
{
	XmlDocument_t3649534162_StaticFields::get_offset_of_optimal_create_types_5(),
	XmlDocument_t3649534162::get_offset_of_optimal_create_element_6(),
	XmlDocument_t3649534162::get_offset_of_optimal_create_attribute_7(),
	XmlDocument_t3649534162::get_offset_of_nameTable_8(),
	XmlDocument_t3649534162::get_offset_of_baseURI_9(),
	XmlDocument_t3649534162::get_offset_of_implementation_10(),
	XmlDocument_t3649534162::get_offset_of_preserveWhitespace_11(),
	XmlDocument_t3649534162::get_offset_of_resolver_12(),
	XmlDocument_t3649534162::get_offset_of_idTable_13(),
	XmlDocument_t3649534162::get_offset_of_nameCache_14(),
	XmlDocument_t3649534162::get_offset_of_lastLinkedChild_15(),
	XmlDocument_t3649534162::get_offset_of_schemaInfo_16(),
	XmlDocument_t3649534162::get_offset_of_loadMode_17(),
	XmlDocument_t3649534162::get_offset_of_NodeChanged_18(),
	XmlDocument_t3649534162::get_offset_of_NodeChanging_19(),
	XmlDocument_t3649534162::get_offset_of_NodeInserted_20(),
	XmlDocument_t3649534162::get_offset_of_NodeInserting_21(),
	XmlDocument_t3649534162::get_offset_of_NodeRemoved_22(),
	XmlDocument_t3649534162::get_offset_of_NodeRemoving_23(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
