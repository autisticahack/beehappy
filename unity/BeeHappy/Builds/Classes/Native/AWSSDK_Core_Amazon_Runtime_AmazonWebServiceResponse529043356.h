﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.Runtime.ResponseMetadata
struct ResponseMetadata_t527027456;

#include "mscorlib_System_Object2689449295.h"
#include "System_System_Net_HttpStatusCode1898409641.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.AmazonWebServiceResponse
struct  AmazonWebServiceResponse_t529043356  : public Il2CppObject
{
public:
	// Amazon.Runtime.ResponseMetadata Amazon.Runtime.AmazonWebServiceResponse::responseMetadataField
	ResponseMetadata_t527027456 * ___responseMetadataField_0;
	// System.Int64 Amazon.Runtime.AmazonWebServiceResponse::contentLength
	int64_t ___contentLength_1;
	// System.Net.HttpStatusCode Amazon.Runtime.AmazonWebServiceResponse::httpStatusCode
	int32_t ___httpStatusCode_2;

public:
	inline static int32_t get_offset_of_responseMetadataField_0() { return static_cast<int32_t>(offsetof(AmazonWebServiceResponse_t529043356, ___responseMetadataField_0)); }
	inline ResponseMetadata_t527027456 * get_responseMetadataField_0() const { return ___responseMetadataField_0; }
	inline ResponseMetadata_t527027456 ** get_address_of_responseMetadataField_0() { return &___responseMetadataField_0; }
	inline void set_responseMetadataField_0(ResponseMetadata_t527027456 * value)
	{
		___responseMetadataField_0 = value;
		Il2CppCodeGenWriteBarrier(&___responseMetadataField_0, value);
	}

	inline static int32_t get_offset_of_contentLength_1() { return static_cast<int32_t>(offsetof(AmazonWebServiceResponse_t529043356, ___contentLength_1)); }
	inline int64_t get_contentLength_1() const { return ___contentLength_1; }
	inline int64_t* get_address_of_contentLength_1() { return &___contentLength_1; }
	inline void set_contentLength_1(int64_t value)
	{
		___contentLength_1 = value;
	}

	inline static int32_t get_offset_of_httpStatusCode_2() { return static_cast<int32_t>(offsetof(AmazonWebServiceResponse_t529043356, ___httpStatusCode_2)); }
	inline int32_t get_httpStatusCode_2() const { return ___httpStatusCode_2; }
	inline int32_t* get_address_of_httpStatusCode_2() { return &___httpStatusCode_2; }
	inline void set_httpStatusCode_2(int32_t value)
	{
		___httpStatusCode_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
