﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BHSceneManager
struct BHSceneManager_t812796361;

#include "codegen/il2cpp-codegen.h"

// System.Void BHSceneManager::.ctor()
extern "C"  void BHSceneManager__ctor_m699644820 (BHSceneManager_t812796361 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BHSceneManager::Start()
extern "C"  void BHSceneManager_Start_m811563700 (BHSceneManager_t812796361 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BHSceneManager::Update()
extern "C"  void BHSceneManager_Update_m620495637 (BHSceneManager_t812796361 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BHSceneManager::LoadScene()
extern "C"  void BHSceneManager_LoadScene_m2017181036 (BHSceneManager_t812796361 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
