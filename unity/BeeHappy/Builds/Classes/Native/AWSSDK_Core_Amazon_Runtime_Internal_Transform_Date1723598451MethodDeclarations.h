﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.Transform.DateTimeUnmarshaller
struct DateTimeUnmarshaller_t1723598451;
// Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext
struct XmlUnmarshallerContext_t1179575220;
// Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext
struct JsonUnmarshallerContext_t456235889;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime693205669.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_XmlU1179575220.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_JsonU456235889.h"

// System.Void Amazon.Runtime.Internal.Transform.DateTimeUnmarshaller::.ctor()
extern "C"  void DateTimeUnmarshaller__ctor_m3879531063 (DateTimeUnmarshaller_t1723598451 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.Internal.Transform.DateTimeUnmarshaller Amazon.Runtime.Internal.Transform.DateTimeUnmarshaller::get_Instance()
extern "C"  DateTimeUnmarshaller_t1723598451 * DateTimeUnmarshaller_get_Instance_m2045152802 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime Amazon.Runtime.Internal.Transform.DateTimeUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext)
extern "C"  DateTime_t693205669  DateTimeUnmarshaller_Unmarshall_m1665504504 (DateTimeUnmarshaller_t1723598451 * __this, XmlUnmarshallerContext_t1179575220 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime Amazon.Runtime.Internal.Transform.DateTimeUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern "C"  DateTime_t693205669  DateTimeUnmarshaller_Unmarshall_m1465130531 (DateTimeUnmarshaller_t1723598451 * __this, JsonUnmarshallerContext_t456235889 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Transform.DateTimeUnmarshaller::.cctor()
extern "C"  void DateTimeUnmarshaller__cctor_m2072425786 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
