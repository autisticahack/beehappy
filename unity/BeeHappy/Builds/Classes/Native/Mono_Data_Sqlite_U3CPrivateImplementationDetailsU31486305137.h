﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object2689449295.h"
#include "Mono_Data_Sqlite_U3CPrivateImplementationDetailsU31278837972.h"
#include "Mono_Data_Sqlite_U3CPrivateImplementationDetailsU31957337327.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t1486305145  : public Il2CppObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType$104 <PrivateImplementationDetails>::$$field-0
	U24ArrayTypeU24104_t1278837972  ___U24U24fieldU2D0_0;
	// <PrivateImplementationDetails>/$ArrayType$8 <PrivateImplementationDetails>::$$field-1
	U24ArrayTypeU248_t1957337329  ___U24U24fieldU2D1_1;

public:
	inline static int32_t get_offset_of_U24U24fieldU2D0_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields, ___U24U24fieldU2D0_0)); }
	inline U24ArrayTypeU24104_t1278837972  get_U24U24fieldU2D0_0() const { return ___U24U24fieldU2D0_0; }
	inline U24ArrayTypeU24104_t1278837972 * get_address_of_U24U24fieldU2D0_0() { return &___U24U24fieldU2D0_0; }
	inline void set_U24U24fieldU2D0_0(U24ArrayTypeU24104_t1278837972  value)
	{
		___U24U24fieldU2D0_0 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D1_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields, ___U24U24fieldU2D1_1)); }
	inline U24ArrayTypeU248_t1957337329  get_U24U24fieldU2D1_1() const { return ___U24U24fieldU2D1_1; }
	inline U24ArrayTypeU248_t1957337329 * get_address_of_U24U24fieldU2D1_1() { return &___U24U24fieldU2D1_1; }
	inline void set_U24U24fieldU2D1_1(U24ArrayTypeU248_t1957337329  value)
	{
		___U24U24fieldU2D1_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
