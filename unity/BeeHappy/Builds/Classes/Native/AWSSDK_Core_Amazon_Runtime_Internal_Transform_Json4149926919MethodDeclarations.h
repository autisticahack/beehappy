﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.Transform.JsonErrorResponseUnmarshaller
struct JsonErrorResponseUnmarshaller_t4149926919;
// Amazon.Runtime.Internal.ErrorResponse
struct ErrorResponse_t3502566035;
// Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext
struct JsonUnmarshallerContext_t456235889;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_JsonU456235889.h"

// Amazon.Runtime.Internal.ErrorResponse Amazon.Runtime.Internal.Transform.JsonErrorResponseUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern "C"  ErrorResponse_t3502566035 * JsonErrorResponseUnmarshaller_Unmarshall_m3547220927 (JsonErrorResponseUnmarshaller_t4149926919 * __this, JsonUnmarshallerContext_t456235889 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.Internal.Transform.JsonErrorResponseUnmarshaller Amazon.Runtime.Internal.Transform.JsonErrorResponseUnmarshaller::GetInstance()
extern "C"  JsonErrorResponseUnmarshaller_t4149926919 * JsonErrorResponseUnmarshaller_GetInstance_m1721802307 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Transform.JsonErrorResponseUnmarshaller::.ctor()
extern "C"  void JsonErrorResponseUnmarshaller__ctor_m1731640889 (JsonErrorResponseUnmarshaller_t4149926919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
