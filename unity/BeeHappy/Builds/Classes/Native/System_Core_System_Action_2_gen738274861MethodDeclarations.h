﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action_2_gen2572051853MethodDeclarations.h"

// System.Void System.Action`2<System.Exception,System.Object>::.ctor(System.Object,System.IntPtr)
#define Action_2__ctor_m4154418848(__this, ___object0, ___method1, method) ((  void (*) (Action_2_t738274861 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_2__ctor_m3362391082_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`2<System.Exception,System.Object>::Invoke(T1,T2)
#define Action_2_Invoke_m2612079195(__this, ___arg10, ___arg21, method) ((  void (*) (Action_2_t738274861 *, Exception_t1927440687 *, Il2CppObject *, const MethodInfo*))Action_2_Invoke_m1501152969_gshared)(__this, ___arg10, ___arg21, method)
// System.IAsyncResult System.Action`2<System.Exception,System.Object>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
#define Action_2_BeginInvoke_m2003031554(__this, ___arg10, ___arg21, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Action_2_t738274861 *, Exception_t1927440687 *, Il2CppObject *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Action_2_BeginInvoke_m1914861552_gshared)(__this, ___arg10, ___arg21, ___callback2, ___object3, method)
// System.Void System.Action`2<System.Exception,System.Object>::EndInvoke(System.IAsyncResult)
#define Action_2_EndInvoke_m3905005866(__this, ___result0, method) ((  void (*) (Action_2_t738274861 *, Il2CppObject *, const MethodInfo*))Action_2_EndInvoke_m3956733788_gshared)(__this, ___result0, method)
