﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AWSSDK_Core_Amazon_Runtime_AmazonServiceException3748559634.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.SecurityToken.AmazonSecurityTokenServiceException
struct  AmazonSecurityTokenServiceException_t3771342025  : public AmazonServiceException_t3748559634
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
