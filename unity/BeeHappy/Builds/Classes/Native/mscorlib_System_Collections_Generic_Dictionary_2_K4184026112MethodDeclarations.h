﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1838728343MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,ThirdParty.Json.LitJson.ObjectMetadata>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m1628231087(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t4184026112 *, Dictionary_2_t1700528341 *, const MethodInfo*))KeyCollection__ctor_m1439175414_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2320421133(__this, ___item0, method) ((  void (*) (KeyCollection_t4184026112 *, Type_t *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m741303632_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3352614580(__this, method) ((  void (*) (KeyCollection_t4184026112 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m849613147_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3904615459(__this, ___item0, method) ((  bool (*) (KeyCollection_t4184026112 *, Type_t *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3874625808_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1970202368(__this, ___item0, method) ((  bool (*) (KeyCollection_t4184026112 *, Type_t *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1301535531_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2877496380(__this, method) ((  Il2CppObject* (*) (KeyCollection_t4184026112 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3508981317_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m2462655826(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t4184026112 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m2690697909_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3727400365(__this, method) ((  Il2CppObject * (*) (KeyCollection_t4184026112 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1851700276_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3990308426(__this, method) ((  bool (*) (KeyCollection_t4184026112 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3045537847_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1254061410(__this, method) ((  bool (*) (KeyCollection_t4184026112 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1756756033_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,ThirdParty.Json.LitJson.ObjectMetadata>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m3517537116(__this, method) ((  Il2CppObject * (*) (KeyCollection_t4184026112 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m659350425_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,ThirdParty.Json.LitJson.ObjectMetadata>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m1885657748(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t4184026112 *, TypeU5BU5D_t1664964607*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m847362259_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,ThirdParty.Json.LitJson.ObjectMetadata>::GetEnumerator()
#define KeyCollection_GetEnumerator_m4271366495(__this, method) ((  Enumerator_t95064483  (*) (KeyCollection_t4184026112 *, const MethodInfo*))KeyCollection_GetEnumerator_m3668909536_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,ThirdParty.Json.LitJson.ObjectMetadata>::get_Count()
#define KeyCollection_get_Count_m937272318(__this, method) ((  int32_t (*) (KeyCollection_t4184026112 *, const MethodInfo*))KeyCollection_get_Count_m3604538821_gshared)(__this, method)
