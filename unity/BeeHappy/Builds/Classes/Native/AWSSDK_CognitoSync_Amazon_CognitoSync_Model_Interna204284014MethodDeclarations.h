﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.CognitoSync.Model.Internal.MarshallTransformations.DeleteDatasetResponseUnmarshaller
struct DeleteDatasetResponseUnmarshaller_t204284014;
// Amazon.Runtime.AmazonWebServiceResponse
struct AmazonWebServiceResponse_t529043356;
// Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext
struct JsonUnmarshallerContext_t456235889;
// Amazon.Runtime.AmazonServiceException
struct AmazonServiceException_t3748559634;
// System.Exception
struct Exception_t1927440687;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_JsonU456235889.h"
#include "mscorlib_System_Exception1927440687.h"
#include "System_System_Net_HttpStatusCode1898409641.h"

// Amazon.Runtime.AmazonWebServiceResponse Amazon.CognitoSync.Model.Internal.MarshallTransformations.DeleteDatasetResponseUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern "C"  AmazonWebServiceResponse_t529043356 * DeleteDatasetResponseUnmarshaller_Unmarshall_m2478393198 (DeleteDatasetResponseUnmarshaller_t204284014 * __this, JsonUnmarshallerContext_t456235889 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.AmazonServiceException Amazon.CognitoSync.Model.Internal.MarshallTransformations.DeleteDatasetResponseUnmarshaller::UnmarshallException(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,System.Exception,System.Net.HttpStatusCode)
extern "C"  AmazonServiceException_t3748559634 * DeleteDatasetResponseUnmarshaller_UnmarshallException_m943529136 (DeleteDatasetResponseUnmarshaller_t204284014 * __this, JsonUnmarshallerContext_t456235889 * ___context0, Exception_t1927440687 * ___innerException1, int32_t ___statusCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.CognitoSync.Model.Internal.MarshallTransformations.DeleteDatasetResponseUnmarshaller Amazon.CognitoSync.Model.Internal.MarshallTransformations.DeleteDatasetResponseUnmarshaller::get_Instance()
extern "C"  DeleteDatasetResponseUnmarshaller_t204284014 * DeleteDatasetResponseUnmarshaller_get_Instance_m2285353115 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Model.Internal.MarshallTransformations.DeleteDatasetResponseUnmarshaller::.ctor()
extern "C"  void DeleteDatasetResponseUnmarshaller__ctor_m3249054380 (DeleteDatasetResponseUnmarshaller_t204284014 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.Model.Internal.MarshallTransformations.DeleteDatasetResponseUnmarshaller::.cctor()
extern "C"  void DeleteDatasetResponseUnmarshaller__cctor_m1067624941 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
