﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.Runtime.AmazonWebServiceResponse
struct AmazonWebServiceResponse_t529043356;
// Amazon.Runtime.Internal.Transform.IWebResponseData
struct IWebResponseData_t3815728244;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.ResponseContext
struct  ResponseContext_t3850805926  : public Il2CppObject
{
public:
	// Amazon.Runtime.AmazonWebServiceResponse Amazon.Runtime.Internal.ResponseContext::<Response>k__BackingField
	AmazonWebServiceResponse_t529043356 * ___U3CResponseU3Ek__BackingField_0;
	// Amazon.Runtime.Internal.Transform.IWebResponseData Amazon.Runtime.Internal.ResponseContext::<HttpResponse>k__BackingField
	Il2CppObject * ___U3CHttpResponseU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CResponseU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ResponseContext_t3850805926, ___U3CResponseU3Ek__BackingField_0)); }
	inline AmazonWebServiceResponse_t529043356 * get_U3CResponseU3Ek__BackingField_0() const { return ___U3CResponseU3Ek__BackingField_0; }
	inline AmazonWebServiceResponse_t529043356 ** get_address_of_U3CResponseU3Ek__BackingField_0() { return &___U3CResponseU3Ek__BackingField_0; }
	inline void set_U3CResponseU3Ek__BackingField_0(AmazonWebServiceResponse_t529043356 * value)
	{
		___U3CResponseU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CResponseU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CHttpResponseU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ResponseContext_t3850805926, ___U3CHttpResponseU3Ek__BackingField_1)); }
	inline Il2CppObject * get_U3CHttpResponseU3Ek__BackingField_1() const { return ___U3CHttpResponseU3Ek__BackingField_1; }
	inline Il2CppObject ** get_address_of_U3CHttpResponseU3Ek__BackingField_1() { return &___U3CHttpResponseU3Ek__BackingField_1; }
	inline void set_U3CHttpResponseU3Ek__BackingField_1(Il2CppObject * value)
	{
		___U3CHttpResponseU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CHttpResponseU3Ek__BackingField_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
