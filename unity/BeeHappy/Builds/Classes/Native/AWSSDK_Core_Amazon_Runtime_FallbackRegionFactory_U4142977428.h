﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.Runtime.FallbackRegionFactory/<>c
struct U3CU3Ec_t4142977428;
// Amazon.Runtime.FallbackRegionFactory/RegionGenerator
struct RegionGenerator_t3123742622;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.FallbackRegionFactory/<>c
struct  U3CU3Ec_t4142977428  : public Il2CppObject
{
public:

public:
};

struct U3CU3Ec_t4142977428_StaticFields
{
public:
	// Amazon.Runtime.FallbackRegionFactory/<>c Amazon.Runtime.FallbackRegionFactory/<>c::<>9
	U3CU3Ec_t4142977428 * ___U3CU3E9_0;
	// Amazon.Runtime.FallbackRegionFactory/RegionGenerator Amazon.Runtime.FallbackRegionFactory/<>c::<>9__11_0
	RegionGenerator_t3123742622 * ___U3CU3E9__11_0_1;
	// Amazon.Runtime.FallbackRegionFactory/RegionGenerator Amazon.Runtime.FallbackRegionFactory/<>c::<>9__11_1
	RegionGenerator_t3123742622 * ___U3CU3E9__11_1_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4142977428_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t4142977428 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t4142977428 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t4142977428 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9_0, value);
	}

	inline static int32_t get_offset_of_U3CU3E9__11_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4142977428_StaticFields, ___U3CU3E9__11_0_1)); }
	inline RegionGenerator_t3123742622 * get_U3CU3E9__11_0_1() const { return ___U3CU3E9__11_0_1; }
	inline RegionGenerator_t3123742622 ** get_address_of_U3CU3E9__11_0_1() { return &___U3CU3E9__11_0_1; }
	inline void set_U3CU3E9__11_0_1(RegionGenerator_t3123742622 * value)
	{
		___U3CU3E9__11_0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9__11_0_1, value);
	}

	inline static int32_t get_offset_of_U3CU3E9__11_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4142977428_StaticFields, ___U3CU3E9__11_1_2)); }
	inline RegionGenerator_t3123742622 * get_U3CU3E9__11_1_2() const { return ___U3CU3E9__11_1_2; }
	inline RegionGenerator_t3123742622 ** get_address_of_U3CU3E9__11_1_2() { return &___U3CU3E9__11_1_2; }
	inline void set_U3CU3E9__11_1_2(RegionGenerator_t3123742622 * value)
	{
		___U3CU3E9__11_1_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9__11_1_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
