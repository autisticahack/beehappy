﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>
struct Dictionary_2_t727138142;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t1902082073;
// System.Collections.Generic.IDictionary`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>
struct IDictionary_2_t3021188859;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t3641524600;
// System.Collections.ICollection
struct ICollection_t91669223;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>[]
struct KeyValuePair_2U5BU5D_t3167552973;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>>
struct IEnumerator_1_t254974487;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t259680273;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>
struct KeyCollection_t3210635913;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>
struct ValueCollection_t3725165281;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serialization228987430.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22779450660.h"
#include "mscorlib_System_Array3829468939.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_ArrayMetadata1135078014.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2047162844.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::.ctor()
extern "C"  void Dictionary_2__ctor_m2742391335_gshared (Dictionary_2_t727138142 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m2742391335(__this, method) ((  void (*) (Dictionary_2_t727138142 *, const MethodInfo*))Dictionary_2__ctor_m2742391335_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m1511666200_gshared (Dictionary_2_t727138142 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m1511666200(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t727138142 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m1511666200_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C"  void Dictionary_2__ctor_m3409036229_gshared (Dictionary_2_t727138142 * __this, Il2CppObject* ___dictionary0, const MethodInfo* method);
#define Dictionary_2__ctor_m3409036229(__this, ___dictionary0, method) ((  void (*) (Dictionary_2_t727138142 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m3409036229_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m589735040_gshared (Dictionary_2_t727138142 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m589735040(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t727138142 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m589735040_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m38600112_gshared (Dictionary_2_t727138142 * __this, Il2CppObject* ___dictionary0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m38600112(__this, ___dictionary0, ___comparer1, method) ((  void (*) (Dictionary_2_t727138142 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m38600112_gshared)(__this, ___dictionary0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m2180725942_gshared (Dictionary_2_t727138142 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m2180725942(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t727138142 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2__ctor_m2180725942_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m2556602131_gshared (Dictionary_2_t727138142 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m2556602131(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t727138142 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m2556602131_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Keys_m2004706093_gshared (Dictionary_2_t727138142 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m2004706093(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t727138142 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m2004706093_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.IDictionary.get_Values()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Values_m3536653797_gshared (Dictionary_2_t727138142 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Values_m3536653797(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t727138142 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m3536653797_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m2064518759_gshared (Dictionary_2_t727138142 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m2064518759(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t727138142 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m2064518759_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m2787436234_gshared (Dictionary_2_t727138142 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m2787436234(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t727138142 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m2787436234_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m282049645_gshared (Dictionary_2_t727138142 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m282049645(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t727138142 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m282049645_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool Dictionary_2_System_Collections_IDictionary_Contains_m1058071785_gshared (Dictionary_2_t727138142 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m1058071785(__this, ___key0, method) ((  bool (*) (Dictionary_2_t727138142 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m1058071785_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m149069734_gshared (Dictionary_2_t727138142 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m149069734(__this, ___key0, method) ((  void (*) (Dictionary_2_t727138142 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m149069734_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1494339827_gshared (Dictionary_2_t727138142 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1494339827(__this, method) ((  bool (*) (Dictionary_2_t727138142 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1494339827_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3581562211_gshared (Dictionary_2_t727138142 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3581562211(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t727138142 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3581562211_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m4149917501_gshared (Dictionary_2_t727138142 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m4149917501(__this, method) ((  bool (*) (Dictionary_2_t727138142 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m4149917501_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3890204740_gshared (Dictionary_2_t727138142 * __this, KeyValuePair_2_t2779450660  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3890204740(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t727138142 *, KeyValuePair_2_t2779450660 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3890204740_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1055748000_gshared (Dictionary_2_t727138142 * __this, KeyValuePair_2_t2779450660  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1055748000(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t727138142 *, KeyValuePair_2_t2779450660 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1055748000_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1809915784_gshared (Dictionary_2_t727138142 * __this, KeyValuePair_2U5BU5D_t3167552973* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1809915784(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t727138142 *, KeyValuePair_2U5BU5D_t3167552973*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1809915784_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3779055887_gshared (Dictionary_2_t727138142 * __this, KeyValuePair_2_t2779450660  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3779055887(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t727138142 *, KeyValuePair_2_t2779450660 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3779055887_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m1548942883_gshared (Dictionary_2_t727138142 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m1548942883(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t727138142 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m1548942883_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m4083331662_gshared (Dictionary_2_t727138142 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m4083331662(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t727138142 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m4083331662_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1282525557_gshared (Dictionary_2_t727138142 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1282525557(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t727138142 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1282525557_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m879367080_gshared (Dictionary_2_t727138142 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m879367080(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t727138142 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m879367080_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m127825803_gshared (Dictionary_2_t727138142 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m127825803(__this, method) ((  int32_t (*) (Dictionary_2_t727138142 *, const MethodInfo*))Dictionary_2_get_Count_m127825803_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::get_Item(TKey)
extern "C"  ArrayMetadata_t1135078014  Dictionary_2_get_Item_m3578929368_gshared (Dictionary_2_t727138142 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m3578929368(__this, ___key0, method) ((  ArrayMetadata_t1135078014  (*) (Dictionary_2_t727138142 *, Il2CppObject *, const MethodInfo*))Dictionary_2_get_Item_m3578929368_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m649570103_gshared (Dictionary_2_t727138142 * __this, Il2CppObject * ___key0, ArrayMetadata_t1135078014  ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m649570103(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t727138142 *, Il2CppObject *, ArrayMetadata_t1135078014 , const MethodInfo*))Dictionary_2_set_Item_m649570103_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m2701250503_gshared (Dictionary_2_t727138142 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m2701250503(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t727138142 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m2701250503_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m55273468_gshared (Dictionary_2_t727138142 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m55273468(__this, ___size0, method) ((  void (*) (Dictionary_2_t727138142 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m55273468_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m1180648718_gshared (Dictionary_2_t727138142 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m1180648718(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t727138142 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m1180648718_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t2779450660  Dictionary_2_make_pair_m2867258804_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___key0, ArrayMetadata_t1135078014  ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m2867258804(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t2779450660  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, ArrayMetadata_t1135078014 , const MethodInfo*))Dictionary_2_make_pair_m2867258804_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::pick_key(TKey,TValue)
extern "C"  Il2CppObject * Dictionary_2_pick_key_m1314516338_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___key0, ArrayMetadata_t1135078014  ___value1, const MethodInfo* method);
#define Dictionary_2_pick_key_m1314516338(__this /* static, unused */, ___key0, ___value1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, ArrayMetadata_t1135078014 , const MethodInfo*))Dictionary_2_pick_key_m1314516338_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::pick_value(TKey,TValue)
extern "C"  ArrayMetadata_t1135078014  Dictionary_2_pick_value_m417748954_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___key0, ArrayMetadata_t1135078014  ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m417748954(__this /* static, unused */, ___key0, ___value1, method) ((  ArrayMetadata_t1135078014  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, ArrayMetadata_t1135078014 , const MethodInfo*))Dictionary_2_pick_value_m417748954_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m958248227_gshared (Dictionary_2_t727138142 * __this, KeyValuePair_2U5BU5D_t3167552973* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m958248227(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t727138142 *, KeyValuePair_2U5BU5D_t3167552973*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m958248227_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::Resize()
extern "C"  void Dictionary_2_Resize_m2338437693_gshared (Dictionary_2_t727138142 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m2338437693(__this, method) ((  void (*) (Dictionary_2_t727138142 *, const MethodInfo*))Dictionary_2_Resize_m2338437693_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m776268556_gshared (Dictionary_2_t727138142 * __this, Il2CppObject * ___key0, ArrayMetadata_t1135078014  ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m776268556(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t727138142 *, Il2CppObject *, ArrayMetadata_t1135078014 , const MethodInfo*))Dictionary_2_Add_m776268556_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::Clear()
extern "C"  void Dictionary_2_Clear_m92263852_gshared (Dictionary_2_t727138142 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m92263852(__this, method) ((  void (*) (Dictionary_2_t727138142 *, const MethodInfo*))Dictionary_2_Clear_m92263852_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m2323438664_gshared (Dictionary_2_t727138142 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m2323438664(__this, ___key0, method) ((  bool (*) (Dictionary_2_t727138142 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ContainsKey_m2323438664_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m1310121632_gshared (Dictionary_2_t727138142 * __this, ArrayMetadata_t1135078014  ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m1310121632(__this, ___value0, method) ((  bool (*) (Dictionary_2_t727138142 *, ArrayMetadata_t1135078014 , const MethodInfo*))Dictionary_2_ContainsValue_m1310121632_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m2903662831_gshared (Dictionary_2_t727138142 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m2903662831(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t727138142 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2_GetObjectData_m2903662831_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m1165608543_gshared (Dictionary_2_t727138142 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m1165608543(__this, ___sender0, method) ((  void (*) (Dictionary_2_t727138142 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m1165608543_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m969038240_gshared (Dictionary_2_t727138142 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m969038240(__this, ___key0, method) ((  bool (*) (Dictionary_2_t727138142 *, Il2CppObject *, const MethodInfo*))Dictionary_2_Remove_m969038240_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m439825007_gshared (Dictionary_2_t727138142 * __this, Il2CppObject * ___key0, ArrayMetadata_t1135078014 * ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m439825007(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t727138142 *, Il2CppObject *, ArrayMetadata_t1135078014 *, const MethodInfo*))Dictionary_2_TryGetValue_m439825007_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::get_Keys()
extern "C"  KeyCollection_t3210635913 * Dictionary_2_get_Keys_m1694478532_gshared (Dictionary_2_t727138142 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m1694478532(__this, method) ((  KeyCollection_t3210635913 * (*) (Dictionary_2_t727138142 *, const MethodInfo*))Dictionary_2_get_Keys_m1694478532_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::get_Values()
extern "C"  ValueCollection_t3725165281 * Dictionary_2_get_Values_m1671455652_gshared (Dictionary_2_t727138142 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m1671455652(__this, method) ((  ValueCollection_t3725165281 * (*) (Dictionary_2_t727138142 *, const MethodInfo*))Dictionary_2_get_Values_m1671455652_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::ToTKey(System.Object)
extern "C"  Il2CppObject * Dictionary_2_ToTKey_m1495293765_gshared (Dictionary_2_t727138142 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m1495293765(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t727138142 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m1495293765_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::ToTValue(System.Object)
extern "C"  ArrayMetadata_t1135078014  Dictionary_2_ToTValue_m157136837_gshared (Dictionary_2_t727138142 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m157136837(__this, ___value0, method) ((  ArrayMetadata_t1135078014  (*) (Dictionary_2_t727138142 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m157136837_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m3711460239_gshared (Dictionary_2_t727138142 * __this, KeyValuePair_2_t2779450660  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m3711460239(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t727138142 *, KeyValuePair_2_t2779450660 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m3711460239_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::GetEnumerator()
extern "C"  Enumerator_t2047162844  Dictionary_2_GetEnumerator_m223960780_gshared (Dictionary_2_t727138142 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m223960780(__this, method) ((  Enumerator_t2047162844  (*) (Dictionary_2_t727138142 *, const MethodInfo*))Dictionary_2_GetEnumerator_m223960780_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::<CopyTo>m__0(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Dictionary_2_U3CCopyToU3Em__0_m997169325_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___key0, ArrayMetadata_t1135078014  ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m997169325(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, ArrayMetadata_t1135078014 , const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m997169325_gshared)(__this /* static, unused */, ___key0, ___value1, method)
