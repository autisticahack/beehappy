﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.WebServiceExceptionEventArgs
struct WebServiceExceptionEventArgs_t3226266439;
// System.Collections.Generic.IDictionary`2<System.String,System.String>
struct IDictionary_2_t1943082916;
// System.String
struct String_t;
// System.Uri
struct Uri_t19570940;
// Amazon.Runtime.AmazonWebServiceRequest
struct AmazonWebServiceRequest_t3384026212;
// System.Exception
struct Exception_t1927440687;
// Amazon.Runtime.Internal.IRequest
struct IRequest_t2400804350;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "System_System_Uri19570940.h"
#include "AWSSDK_Core_Amazon_Runtime_AmazonWebServiceRequest3384026212.h"
#include "mscorlib_System_Exception1927440687.h"

// System.Void Amazon.Runtime.WebServiceExceptionEventArgs::.ctor()
extern "C"  void WebServiceExceptionEventArgs__ctor_m2569602846 (WebServiceExceptionEventArgs_t3226266439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.WebServiceExceptionEventArgs::set_Headers(System.Collections.Generic.IDictionary`2<System.String,System.String>)
extern "C"  void WebServiceExceptionEventArgs_set_Headers_m3200151843 (WebServiceExceptionEventArgs_t3226266439 * __this, Il2CppObject* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.WebServiceExceptionEventArgs::set_Parameters(System.Collections.Generic.IDictionary`2<System.String,System.String>)
extern "C"  void WebServiceExceptionEventArgs_set_Parameters_m359412705 (WebServiceExceptionEventArgs_t3226266439 * __this, Il2CppObject* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.WebServiceExceptionEventArgs::set_ServiceName(System.String)
extern "C"  void WebServiceExceptionEventArgs_set_ServiceName_m884662357 (WebServiceExceptionEventArgs_t3226266439 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.WebServiceExceptionEventArgs::set_Endpoint(System.Uri)
extern "C"  void WebServiceExceptionEventArgs_set_Endpoint_m3944724499 (WebServiceExceptionEventArgs_t3226266439 * __this, Uri_t19570940 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.WebServiceExceptionEventArgs::set_Request(Amazon.Runtime.AmazonWebServiceRequest)
extern "C"  void WebServiceExceptionEventArgs_set_Request_m3922608158 (WebServiceExceptionEventArgs_t3226266439 * __this, AmazonWebServiceRequest_t3384026212 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.WebServiceExceptionEventArgs::set_Exception(System.Exception)
extern "C"  void WebServiceExceptionEventArgs_set_Exception_m1747414258 (WebServiceExceptionEventArgs_t3226266439 * __this, Exception_t1927440687 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.WebServiceExceptionEventArgs Amazon.Runtime.WebServiceExceptionEventArgs::Create(System.Exception,Amazon.Runtime.Internal.IRequest)
extern "C"  WebServiceExceptionEventArgs_t3226266439 * WebServiceExceptionEventArgs_Create_m1980678015 (Il2CppObject * __this /* static, unused */, Exception_t1927440687 * ___exception0, Il2CppObject * ___request1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
