﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2058570427MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.SyncConflict>::.ctor()
#define List_1__ctor_m3787585419(__this, method) ((  void (*) (List_1_t2399431441 *, const MethodInfo*))List_1__ctor_m3568471827_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.SyncConflict>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m1531915494(__this, ___collection0, method) ((  void (*) (List_1_t2399431441 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m454375187_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.SyncConflict>::.ctor(System.Int32)
#define List_1__ctor_m1997649696(__this, ___capacity0, method) ((  void (*) (List_1_t2399431441 *, int32_t, const MethodInfo*))List_1__ctor_m136460305_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.SyncConflict>::.cctor()
#define List_1__cctor_m4132366220(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m138621019_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.SyncConflict>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m51361839(__this, method) ((  Il2CppObject* (*) (List_1_t2399431441 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m154161632_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.SyncConflict>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m852629123(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t2399431441 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m2020941110_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.SyncConflict>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m3880366830(__this, method) ((  Il2CppObject * (*) (List_1_t2399431441 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m3552870393_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.SyncConflict>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m2772727563(__this, ___item0, method) ((  int32_t (*) (List_1_t2399431441 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m1765626550_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.SyncConflict>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m22330283(__this, ___item0, method) ((  bool (*) (List_1_t2399431441 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m149594880_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.SyncConflict>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m1022003493(__this, ___item0, method) ((  int32_t (*) (List_1_t2399431441 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m406088260_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.SyncConflict>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m4093527804(__this, ___index0, ___item1, method) ((  void (*) (List_1_t2399431441 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m3961795241_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.SyncConflict>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m3506314066(__this, ___item0, method) ((  void (*) (List_1_t2399431441 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m3415450529_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.SyncConflict>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2447567954(__this, method) ((  bool (*) (List_1_t2399431441 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2131934397_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.SyncConflict>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m2198826323(__this, method) ((  bool (*) (List_1_t2399431441 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m418560222_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.SyncConflict>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m3131462371(__this, method) ((  Il2CppObject * (*) (List_1_t2399431441 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1594235606_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.SyncConflict>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m1260547224(__this, method) ((  bool (*) (List_1_t2399431441 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m2120144013_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.SyncConflict>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m3428500471(__this, method) ((  bool (*) (List_1_t2399431441 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m257950146_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.SyncConflict>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m3482366332(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t2399431441 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m936612973_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.SyncConflict>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m498510125(__this, ___index0, ___value1, method) ((  void (*) (List_1_t2399431441 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m162109184_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.SyncConflict>::Add(T)
#define List_1_Add_m1831385519(__this, ___item0, method) ((  void (*) (List_1_t2399431441 *, SyncConflict_t3030310309 *, const MethodInfo*))List_1_Add_m3051125391_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.SyncConflict>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m2834556879(__this, ___newCount0, method) ((  void (*) (List_1_t2399431441 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m185971996_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.SyncConflict>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m1069926047(__this, ___collection0, method) ((  void (*) (List_1_t2399431441 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m1580067148_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.SyncConflict>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m2539676063(__this, ___enumerable0, method) ((  void (*) (List_1_t2399431441 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m2489692396_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.SyncConflict>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m3098858310(__this, ___collection0, method) ((  void (*) (List_1_t2399431441 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m3614127065_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.SyncConflict>::AsReadOnly()
#define List_1_AsReadOnly_m3670425855(__this, method) ((  ReadOnlyCollection_1_t3216096001 * (*) (List_1_t2399431441 *, const MethodInfo*))List_1_AsReadOnly_m2563000362_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.SyncConflict>::Clear()
#define List_1_Clear_m2093989452(__this, method) ((  void (*) (List_1_t2399431441 *, const MethodInfo*))List_1_Clear_m4254626809_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.SyncConflict>::Contains(T)
#define List_1_Contains_m1047784474(__this, ___item0, method) ((  bool (*) (List_1_t2399431441 *, SyncConflict_t3030310309 *, const MethodInfo*))List_1_Contains_m2577748987_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.SyncConflict>::CopyTo(T[])
#define List_1_CopyTo_m101375447(__this, ___array0, method) ((  void (*) (List_1_t2399431441 *, SyncConflictU5BU5D_t2337015336*, const MethodInfo*))List_1_CopyTo_m626830950_gshared)(__this, ___array0, method)
// System.Void System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.SyncConflict>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m626758948(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t2399431441 *, SyncConflictU5BU5D_t2337015336*, int32_t, const MethodInfo*))List_1_CopyTo_m1758262197_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.SyncConflict>::Find(System.Predicate`1<T>)
#define List_1_Find_m1510297604(__this, ___match0, method) ((  SyncConflict_t3030310309 * (*) (List_1_t2399431441 *, Predicate_1_t1473280424 *, const MethodInfo*))List_1_Find_m1725159095_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.SyncConflict>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m3366285547(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t1473280424 *, const MethodInfo*))List_1_CheckMatch_m1196994270_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.SyncConflict>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m989762708(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t2399431441 *, int32_t, int32_t, Predicate_1_t1473280424 *, const MethodInfo*))List_1_GetIndex_m3409004147_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.SyncConflict>::GetEnumerator()
#define List_1_GetEnumerator_m3311424046(__this, method) ((  Enumerator_t1934161115  (*) (List_1_t2399431441 *, const MethodInfo*))List_1_GetEnumerator_m1147178262_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.SyncConflict>::IndexOf(T)
#define List_1_IndexOf_m3370508418(__this, ___item0, method) ((  int32_t (*) (List_1_t2399431441 *, SyncConflict_t3030310309 *, const MethodInfo*))List_1_IndexOf_m2070479489_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.SyncConflict>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m1038817015(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t2399431441 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3137156970_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.SyncConflict>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m1763006148(__this, ___index0, method) ((  void (*) (List_1_t2399431441 *, int32_t, const MethodInfo*))List_1_CheckIndex_m524615377_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.SyncConflict>::Insert(System.Int32,T)
#define List_1_Insert_m2368531713(__this, ___index0, ___item1, method) ((  void (*) (List_1_t2399431441 *, int32_t, SyncConflict_t3030310309 *, const MethodInfo*))List_1_Insert_m11735664_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.SyncConflict>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m3541140582(__this, ___collection0, method) ((  void (*) (List_1_t2399431441 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m3968030679_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.SyncConflict>::Remove(T)
#define List_1_Remove_m1783114625(__this, ___item0, method) ((  bool (*) (List_1_t2399431441 *, SyncConflict_t3030310309 *, const MethodInfo*))List_1_Remove_m1271859478_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.SyncConflict>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m984010875(__this, ___match0, method) ((  int32_t (*) (List_1_t2399431441 *, Predicate_1_t1473280424 *, const MethodInfo*))List_1_RemoveAll_m2972055270_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.SyncConflict>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m2207558725(__this, ___index0, method) ((  void (*) (List_1_t2399431441 *, int32_t, const MethodInfo*))List_1_RemoveAt_m3615096820_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.SyncConflict>::Reverse()
#define List_1_Reverse_m2528825927(__this, method) ((  void (*) (List_1_t2399431441 *, const MethodInfo*))List_1_Reverse_m4038478200_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.SyncConflict>::Sort()
#define List_1_Sort_m2790466937(__this, method) ((  void (*) (List_1_t2399431441 *, const MethodInfo*))List_1_Sort_m554162636_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.SyncConflict>::Sort(System.Collections.Generic.IComparer`1<T>)
#define List_1_Sort_m296240765(__this, ___comparer0, method) ((  void (*) (List_1_t2399431441 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m1776685136_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.SyncConflict>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m2328910628(__this, ___comparison0, method) ((  void (*) (List_1_t2399431441 *, Comparison_1_t4292049160 *, const MethodInfo*))List_1_Sort_m785723827_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.SyncConflict>::ToArray()
#define List_1_ToArray_m21527260(__this, method) ((  SyncConflictU5BU5D_t2337015336* (*) (List_1_t2399431441 *, const MethodInfo*))List_1_ToArray_m1887490109_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.SyncConflict>::TrimExcess()
#define List_1_TrimExcess_m3986477030(__this, method) ((  void (*) (List_1_t2399431441 *, const MethodInfo*))List_1_TrimExcess_m1944241237_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.SyncConflict>::get_Capacity()
#define List_1_get_Capacity_m1197144096(__this, method) ((  int32_t (*) (List_1_t2399431441 *, const MethodInfo*))List_1_get_Capacity_m3133733835_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.SyncConflict>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m3111300703(__this, ___value0, method) ((  void (*) (List_1_t2399431441 *, int32_t, const MethodInfo*))List_1_set_Capacity_m491101164_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.SyncConflict>::get_Count()
#define List_1_get_Count_m4060914183(__this, method) ((  int32_t (*) (List_1_t2399431441 *, const MethodInfo*))List_1_get_Count_m2899908835_gshared)(__this, method)
// T System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.SyncConflict>::get_Item(System.Int32)
#define List_1_get_Item_m3310774417(__this, ___index0, method) ((  SyncConflict_t3030310309 * (*) (List_1_t2399431441 *, int32_t, const MethodInfo*))List_1_get_Item_m1354830498_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.SyncConflict>::set_Item(System.Int32,T)
#define List_1_set_Item_m1403552518(__this, ___index0, ___value1, method) ((  void (*) (List_1_t2399431441 *, int32_t, SyncConflict_t3030310309 *, const MethodInfo*))List_1_set_Item_m4128108021_gshared)(__this, ___index0, ___value1, method)
