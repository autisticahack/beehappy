﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.SimpleAsyncResult
struct SimpleAsyncResult_t4203640901;
// System.Object
struct Il2CppObject;
// System.Threading.WaitHandle
struct WaitHandle_t677569169;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void Amazon.Runtime.Internal.SimpleAsyncResult::.ctor(System.Object)
extern "C"  void SimpleAsyncResult__ctor_m1515357591 (SimpleAsyncResult_t4203640901 * __this, Il2CppObject * ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Amazon.Runtime.Internal.SimpleAsyncResult::get_AsyncState()
extern "C"  Il2CppObject * SimpleAsyncResult_get_AsyncState_m1007818622 (SimpleAsyncResult_t4203640901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.SimpleAsyncResult::set_AsyncState(System.Object)
extern "C"  void SimpleAsyncResult_set_AsyncState_m2659237211 (SimpleAsyncResult_t4203640901 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.WaitHandle Amazon.Runtime.Internal.SimpleAsyncResult::get_AsyncWaitHandle()
extern "C"  WaitHandle_t677569169 * SimpleAsyncResult_get_AsyncWaitHandle_m275040478 (SimpleAsyncResult_t4203640901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.SimpleAsyncResult::get_CompletedSynchronously()
extern "C"  bool SimpleAsyncResult_get_CompletedSynchronously_m208301125 (SimpleAsyncResult_t4203640901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.SimpleAsyncResult::get_IsCompleted()
extern "C"  bool SimpleAsyncResult_get_IsCompleted_m3378841847 (SimpleAsyncResult_t4203640901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
