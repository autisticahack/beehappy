﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.AWSCredentials
struct AWSCredentials_t3583921007;

#include "codegen/il2cpp-codegen.h"

// System.Void Amazon.Runtime.AWSCredentials::.ctor()
extern "C"  void AWSCredentials__ctor_m2037750724 (AWSCredentials_t3583921007 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
