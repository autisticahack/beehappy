﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22779450660MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,ThirdParty.Json.LitJson.ArrayMetadata>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m611643748(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t829781133 *, Type_t *, ArrayMetadata_t1135078014 , const MethodInfo*))KeyValuePair_2__ctor_m3859112953_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Type,ThirdParty.Json.LitJson.ArrayMetadata>::get_Key()
#define KeyValuePair_2_get_Key_m3549125990(__this, method) ((  Type_t * (*) (KeyValuePair_2_t829781133 *, const MethodInfo*))KeyValuePair_2_get_Key_m1489805227_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,ThirdParty.Json.LitJson.ArrayMetadata>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m4070438169(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t829781133 *, Type_t *, const MethodInfo*))KeyValuePair_2_set_Key_m1755966420_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Type,ThirdParty.Json.LitJson.ArrayMetadata>::get_Value()
#define KeyValuePair_2_get_Value_m3624254694(__this, method) ((  ArrayMetadata_t1135078014  (*) (KeyValuePair_2_t829781133 *, const MethodInfo*))KeyValuePair_2_get_Value_m3869691915_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,ThirdParty.Json.LitJson.ArrayMetadata>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m2964548633(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t829781133 *, ArrayMetadata_t1135078014 , const MethodInfo*))KeyValuePair_2_set_Value_m3110329900_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Type,ThirdParty.Json.LitJson.ArrayMetadata>::ToString()
#define KeyValuePair_2_ToString_m4280692315(__this, method) ((  String_t* (*) (KeyValuePair_2_t829781133 *, const MethodInfo*))KeyValuePair_2_ToString_m2776738692_gshared)(__this, method)
