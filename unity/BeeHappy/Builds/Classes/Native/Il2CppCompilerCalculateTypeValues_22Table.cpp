﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_AWSSection3096528870.h"
#include "AWSSDK_Core_Amazon_ProxySection4280332351.h"
#include "AWSSDK_Core_Amazon_LoggingSection905443946.h"
#include "AWSSDK_Core_Amazon_RegionEndpoint661522805.h"
#include "AWSSDK_Core_Amazon_RegionEndpoint_Endpoint3348692641.h"
#include "AWSSDK_Core_Amazon_UnityInitializer2778189483.h"
#include "AWSSDK_Core_Amazon_Internal_RegionEndpointProvider1909011008.h"
#include "AWSSDK_Core_Amazon_Internal_RegionEndpointProvider1885241449.h"
#include "AWSSDK_Core_Amazon_Internal_RegionEndpointV32255552612.h"
#include "AWSSDK_Core_Amazon_Internal_RegionEndpointV3_Servi3203095671.h"
#include "AWSSDK_Core_Amazon_Internal_RegionEndpointProviderV342927067.h"
#include "AWSSDK_Core_Amazon_Util_ProxyConfig2693849256.h"
#include "AWSSDK_Core_Amazon_Util_LoggingConfig4162907495.h"
#include "AWSSDK_Core_Amazon_Util_AWSSDKUtils2036360342.h"
#include "AWSSDK_Core_Amazon_Util_AWSSDKUtils_U3CU3Ec3667845516.h"
#include "AWSSDK_Core_Amazon_Util_CryptoUtilFactory2421970493.h"
#include "AWSSDK_Core_Amazon_Util_CryptoUtilFactory_CryptoUt1025015063.h"
#include "AWSSDK_Core_Amazon_Util_Storage_KVStore2216900448.h"
#include "AWSSDK_Core_Amazon_Util_Storage_Internal_NetworkIn3319294148.h"
#include "AWSSDK_Core_Amazon_Util_Storage_Internal_NetworkIn2945196168.h"
#include "AWSSDK_Core_Amazon_Util_Internal_InternalSDKUtils3074264706.h"
#include "AWSSDK_Core_Amazon_Util_Internal_InternalSDKUtils_4069291374.h"
#include "AWSSDK_Core_Amazon_Util_Internal_RootConfig4046630774.h"
#include "AWSSDK_Core_Amazon_Util_Internal_TypeFactory695630734.h"
#include "AWSSDK_Core_Amazon_Util_Internal_TypeFactory_Abstr3397773112.h"
#include "AWSSDK_Core_Amazon_Util_Internal_TypeFactory_TypeI3766999367.h"
#include "AWSSDK_Core_Amazon_Util_Internal_AndroidInterop1929019630.h"
#include "AWSSDK_Core_Amazon_Util_Internal_AndroidInterop_U3CU22341567.h"
#include "AWSSDK_Core_Amazon_Util_Internal_AmazonHookedPlatf2168027349.h"
#include "AWSSDK_Core_Amazon_Util_Internal_PlatformServices_A788768262.h"
#include "AWSSDK_Core_Amazon_Util_Internal_PlatformServices_N879095062.h"
#include "AWSSDK_Core_Amazon_Util_Internal_PlatformServices_1607167653.h"
#include "AWSSDK_Core_Amazon_Util_Internal_PlatformServices_S589839913.h"
#include "AWSSDK_Core_Amazon_Util_Internal_PlatformServices_1632807378.h"
#include "AWSSDK_Core_Amazon_Util_Internal_PlatformServices_3929346268.h"
#include "AWSSDK_Core_Amazon_Util_Internal_PlatformServices_3429773411.h"
#include "AWSSDK_Core_Amazon_Util_Internal_PlatformServices_4123618435.h"
#include "AWSSDK_Core_Amazon_Util_Internal_PlatformServices_3059923765.h"
#include "AWSSDK_Core_Amazon_Runtime_AmazonClientException332426366.h"
#include "AWSSDK_Core_Amazon_Runtime_AmazonServiceClient3583134838.h"
#include "AWSSDK_Core_Amazon_Runtime_AmazonServiceException3748559634.h"
#include "AWSSDK_Core_Amazon_Runtime_AmazonUnmarshallingExce1083472470.h"
#include "AWSSDK_Core_Amazon_Runtime_AmazonWebServiceRequest3384026212.h"
#include "AWSSDK_Core_Amazon_Runtime_AmazonWebServiceResponse529043356.h"
#include "AWSSDK_Core_Amazon_Runtime_ImmutableCredentials282353664.h"
#include "AWSSDK_Core_Amazon_Runtime_AWSCredentials3583921007.h"
#include "AWSSDK_Core_Amazon_Runtime_RefreshingAWSCredential1767066958.h"
#include "AWSSDK_Core_Amazon_Runtime_RefreshingAWSCredential3294867821.h"
#include "AWSSDK_Core_Amazon_Runtime_AnonymousAWSCredentials3877662854.h"
#include "AWSSDK_Core_Amazon_Runtime_AWSRegion969138115.h"
#include "AWSSDK_Core_Amazon_Runtime_AppConfigAWSRegion3930413220.h"
#include "AWSSDK_Core_Amazon_Runtime_FallbackRegionFactory860882262.h"
#include "AWSSDK_Core_Amazon_Runtime_FallbackRegionFactory_R3123742622.h"
#include "AWSSDK_Core_Amazon_Runtime_FallbackRegionFactory_U4142977428.h"
#include "AWSSDK_Core_Amazon_Runtime_ClientConfig3664713661.h"
#include "AWSSDK_Core_Amazon_Runtime_ConstantClass4000559886.h"
#include "AWSSDK_Core_Amazon_Runtime_SigningAlgorithm3740229458.h"
#include "AWSSDK_Core_Amazon_Runtime_ErrorType1448377524.h"
#include "AWSSDK_Core_Amazon_Runtime_ExceptionEventHandler3236465969.h"
#include "AWSSDK_Core_Amazon_Runtime_ExceptionEventArgs154100464.h"
#include "AWSSDK_Core_Amazon_Runtime_WebServiceExceptionEven3226266439.h"
#include "AWSSDK_Core_Amazon_Runtime_PreRequestEventArgs776850383.h"
#include "AWSSDK_Core_Amazon_Runtime_PreRequestEventHandler345425304.h"
#include "AWSSDK_Core_Amazon_Runtime_RequestEventArgs434225820.h"
#include "AWSSDK_Core_Amazon_Runtime_WebServiceRequestEventA1089733597.h"
#include "AWSSDK_Core_Amazon_Runtime_RequestEventHandler2213783891.h"
#include "AWSSDK_Core_Amazon_Runtime_Metric3273440202.h"
#include "AWSSDK_Core_Amazon_Runtime_ResponseEventHandler3870676125.h"
#include "AWSSDK_Core_Amazon_Runtime_ResponseEventArgs4056063878.h"
#include "AWSSDK_Core_Amazon_Runtime_WebServiceResponseEvent1104338495.h"
#include "AWSSDK_Core_Amazon_Runtime_ResponseMetadata527027456.h"
#include "AWSSDK_Core_Amazon_Runtime_StreamTransferProgressA2639638063.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2200 = { sizeof (AWSSection_t3096528870), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2200[10] = 
{
	AWSSection_t3096528870::get_offset_of_U3CLoggingU3Ek__BackingField_0(),
	AWSSection_t3096528870::get_offset_of_U3CEndpointDefinitionU3Ek__BackingField_1(),
	AWSSection_t3096528870::get_offset_of_U3CRegionU3Ek__BackingField_2(),
	AWSSection_t3096528870::get_offset_of_U3CUseSdkCacheU3Ek__BackingField_3(),
	AWSSection_t3096528870::get_offset_of_U3CCorrectForClockSkewU3Ek__BackingField_4(),
	AWSSection_t3096528870::get_offset_of_U3CProxyU3Ek__BackingField_5(),
	AWSSection_t3096528870::get_offset_of_U3CProfileNameU3Ek__BackingField_6(),
	AWSSection_t3096528870::get_offset_of_U3CProfilesLocationU3Ek__BackingField_7(),
	AWSSection_t3096528870::get_offset_of_U3CApplicationNameU3Ek__BackingField_8(),
	AWSSection_t3096528870::get_offset_of__serviceSections_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2201 = { sizeof (ProxySection_t4280332351), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2202 = { sizeof (LoggingSection_t905443946), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2202[6] = 
{
	LoggingSection_t905443946::get_offset_of_U3CLogToU3Ek__BackingField_0(),
	LoggingSection_t905443946::get_offset_of_U3CLogResponsesU3Ek__BackingField_1(),
	LoggingSection_t905443946::get_offset_of_U3CLogResponsesSizeLimitU3Ek__BackingField_2(),
	LoggingSection_t905443946::get_offset_of_U3CLogMetricsU3Ek__BackingField_3(),
	LoggingSection_t905443946::get_offset_of_U3CLogMetricsFormatU3Ek__BackingField_4(),
	LoggingSection_t905443946::get_offset_of_U3CLogMetricsCustomFormatterU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2203 = { sizeof (RegionEndpoint_t661522805), -1, sizeof(RegionEndpoint_t661522805_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2203[18] = 
{
	RegionEndpoint_t661522805_StaticFields::get_offset_of__hashBySystemName_0(),
	RegionEndpoint_t661522805_StaticFields::get_offset_of_USEast1_1(),
	RegionEndpoint_t661522805_StaticFields::get_offset_of_USEast2_2(),
	RegionEndpoint_t661522805_StaticFields::get_offset_of_USWest1_3(),
	RegionEndpoint_t661522805_StaticFields::get_offset_of_USWest2_4(),
	RegionEndpoint_t661522805_StaticFields::get_offset_of_EUWest1_5(),
	RegionEndpoint_t661522805_StaticFields::get_offset_of_EUCentral1_6(),
	RegionEndpoint_t661522805_StaticFields::get_offset_of_APNortheast1_7(),
	RegionEndpoint_t661522805_StaticFields::get_offset_of_APNortheast2_8(),
	RegionEndpoint_t661522805_StaticFields::get_offset_of_APSouth1_9(),
	RegionEndpoint_t661522805_StaticFields::get_offset_of_APSoutheast1_10(),
	RegionEndpoint_t661522805_StaticFields::get_offset_of_APSoutheast2_11(),
	RegionEndpoint_t661522805_StaticFields::get_offset_of_SAEast1_12(),
	RegionEndpoint_t661522805_StaticFields::get_offset_of_USGovCloudWest1_13(),
	RegionEndpoint_t661522805_StaticFields::get_offset_of_CNNorth1_14(),
	RegionEndpoint_t661522805_StaticFields::get_offset_of__regionEndpointProvider_15(),
	RegionEndpoint_t661522805::get_offset_of_U3CSystemNameU3Ek__BackingField_16(),
	RegionEndpoint_t661522805::get_offset_of_U3CDisplayNameU3Ek__BackingField_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2204 = { sizeof (Endpoint_t3348692641), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2204[3] = 
{
	Endpoint_t3348692641::get_offset_of_U3CHostnameU3Ek__BackingField_0(),
	Endpoint_t3348692641::get_offset_of_U3CAuthRegionU3Ek__BackingField_1(),
	Endpoint_t3348692641::get_offset_of_U3CSignatureVersionOverrideU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2205 = { sizeof (UnityInitializer_t2778189483), -1, sizeof(UnityInitializer_t2778189483_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2205[3] = 
{
	UnityInitializer_t2778189483_StaticFields::get_offset_of__instance_2(),
	UnityInitializer_t2778189483_StaticFields::get_offset_of__lock_3(),
	UnityInitializer_t2778189483_StaticFields::get_offset_of__mainThread_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2206 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2207 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2208 = { sizeof (RegionEndpointProviderV2_t1909011008), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2209 = { sizeof (RegionEndpoint_t1885241449), -1, sizeof(RegionEndpoint_t1885241449_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2209[6] = 
{
	RegionEndpoint_t1885241449_StaticFields::get_offset_of__documentEndpoints_0(),
	RegionEndpoint_t1885241449_StaticFields::get_offset_of_loaded_1(),
	RegionEndpoint_t1885241449_StaticFields::get_offset_of_LOCK_OBJECT_2(),
	RegionEndpoint_t1885241449_StaticFields::get_offset_of_hashBySystemName_3(),
	RegionEndpoint_t1885241449::get_offset_of_U3CSystemNameU3Ek__BackingField_4(),
	RegionEndpoint_t1885241449::get_offset_of_U3CDisplayNameU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2210 = { sizeof (RegionEndpointV3_t2255552612), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2210[6] = 
{
	RegionEndpointV3_t2255552612::get_offset_of__serviceMap_0(),
	RegionEndpointV3_t2255552612::get_offset_of_U3CRegionNameU3Ek__BackingField_1(),
	RegionEndpointV3_t2255552612::get_offset_of_U3CDisplayNameU3Ek__BackingField_2(),
	RegionEndpointV3_t2255552612::get_offset_of__partitionJsonData_3(),
	RegionEndpointV3_t2255552612::get_offset_of__servicesJsonData_4(),
	RegionEndpointV3_t2255552612::get_offset_of__servicesLoaded_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2211 = { sizeof (ServiceMap_t3203095671), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2211[2] = 
{
	ServiceMap_t3203095671::get_offset_of__serviceMap_0(),
	ServiceMap_t3203095671::get_offset_of__dualServiceMap_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2212 = { sizeof (RegionEndpointProviderV3_t342927067), -1, sizeof(RegionEndpointProviderV3_t342927067_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2212[5] = 
{
	RegionEndpointProviderV3_t342927067::get_offset_of__root_0(),
	RegionEndpointProviderV3_t342927067::get_offset_of__regionEndpointMap_1(),
	RegionEndpointProviderV3_t342927067::get_offset_of__regionEndpointMapLock_2(),
	RegionEndpointProviderV3_t342927067::get_offset_of__allRegionEndpointsLock_3(),
	RegionEndpointProviderV3_t342927067_StaticFields::get_offset_of__emptyDictionaryJsonData_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2213 = { sizeof (ProxyConfig_t2693849256), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2214 = { sizeof (LoggingConfig_t4162907495), -1, sizeof(LoggingConfig_t4162907495_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2214[7] = 
{
	LoggingConfig_t4162907495_StaticFields::get_offset_of_DefaultLogResponsesSizeLimit_0(),
	LoggingConfig_t4162907495::get_offset_of__logTo_1(),
	LoggingConfig_t4162907495::get_offset_of_U3CLogResponsesU3Ek__BackingField_2(),
	LoggingConfig_t4162907495::get_offset_of_U3CLogResponsesSizeLimitU3Ek__BackingField_3(),
	LoggingConfig_t4162907495::get_offset_of_U3CLogMetricsU3Ek__BackingField_4(),
	LoggingConfig_t4162907495::get_offset_of_U3CLogMetricsFormatU3Ek__BackingField_5(),
	LoggingConfig_t4162907495::get_offset_of_U3CLogMetricsCustomFormatterU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2215 = { sizeof (AWSSDKUtils_t2036360342), -1, sizeof(AWSSDKUtils_t2036360342_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2215[6] = 
{
	AWSSDKUtils_t2036360342_StaticFields::get_offset_of_EPOCH_START_0(),
	AWSSDKUtils_t2036360342_StaticFields::get_offset_of_RFCEncodingSchemes_1(),
	AWSSDKUtils_t2036360342_StaticFields::get_offset_of_ValidPathCharacters_2(),
	AWSSDKUtils_t2036360342_StaticFields::get_offset_of__dispatcher_3(),
	AWSSDKUtils_t2036360342_StaticFields::get_offset_of__preserveStackTraceLookupLock_4(),
	AWSSDKUtils_t2036360342_StaticFields::get_offset_of__preserveStackTraceLookup_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2216 = { sizeof (U3CU3Ec_t3667845516), -1, sizeof(U3CU3Ec_t3667845516_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2216[2] = 
{
	U3CU3Ec_t3667845516_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t3667845516_StaticFields::get_offset_of_U3CU3E9__28_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2217 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2217[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2218 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2218[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2219 = { sizeof (CryptoUtilFactory_t2421970493), -1, sizeof(CryptoUtilFactory_t2421970493_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2219[3] = 
{
	CryptoUtilFactory_t2421970493_StaticFields::get_offset_of_util_0(),
	CryptoUtilFactory_t2421970493_StaticFields::get_offset_of__initializedAlgorithmNames_1(),
	CryptoUtilFactory_t2421970493_StaticFields::get_offset_of__keyedHashAlgorithmCreationLock_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2220 = { sizeof (CryptoUtil_t1025015063), -1, 0, sizeof(CryptoUtil_t1025015063_ThreadStaticFields) };
extern const int32_t g_FieldOffsetTable2220[1] = 
{
	THREAD_STATIC_FIELD_OFFSET,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2221 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2222 = { sizeof (KVStore_t2216900448), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2223 = { sizeof (NetworkInfo_t3319294148), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2224 = { sizeof (U3CU3Ec__DisplayClass1_0_t2945196168), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2224[2] = 
{
	U3CU3Ec__DisplayClass1_0_t2945196168::get_offset_of__networkReachability_0(),
	U3CU3Ec__DisplayClass1_0_t2945196168::get_offset_of_asyncEvent_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2225 = { sizeof (InternalSDKUtils_t3074264706), -1, sizeof(InternalSDKUtils_t3074264706_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2225[4] = 
{
	InternalSDKUtils_t3074264706_StaticFields::get_offset_of__customSdkUserAgent_0(),
	InternalSDKUtils_t3074264706_StaticFields::get_offset_of__customData_1(),
	InternalSDKUtils_t3074264706_StaticFields::get_offset_of__userAgentBaseName_2(),
	InternalSDKUtils_t3074264706_StaticFields::get_offset_of__logger_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2226 = { sizeof (U3CU3Ec__DisplayClass31_0_t4069291374), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2226[1] = 
{
	U3CU3Ec__DisplayClass31_0_t4069291374::get_offset_of_action_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2227 = { sizeof (RootConfig_t4046630774), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2227[10] = 
{
	RootConfig_t4046630774::get_offset_of_U3CLoggingU3Ek__BackingField_0(),
	RootConfig_t4046630774::get_offset_of_U3CProxyU3Ek__BackingField_1(),
	RootConfig_t4046630774::get_offset_of_U3CEndpointDefinitionU3Ek__BackingField_2(),
	RootConfig_t4046630774::get_offset_of_U3CRegionU3Ek__BackingField_3(),
	RootConfig_t4046630774::get_offset_of_U3CProfileNameU3Ek__BackingField_4(),
	RootConfig_t4046630774::get_offset_of_U3CProfilesLocationU3Ek__BackingField_5(),
	RootConfig_t4046630774::get_offset_of_U3CUseSdkCacheU3Ek__BackingField_6(),
	RootConfig_t4046630774::get_offset_of_U3CCorrectForClockSkewU3Ek__BackingField_7(),
	RootConfig_t4046630774::get_offset_of_U3CApplicationNameU3Ek__BackingField_8(),
	RootConfig_t4046630774::get_offset_of_U3CServiceSectionsU3Ek__BackingField_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2228 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2229 = { sizeof (TypeFactory_t695630734), -1, sizeof(TypeFactory_t695630734_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2229[1] = 
{
	TypeFactory_t695630734_StaticFields::get_offset_of_EmptyTypes_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2230 = { sizeof (AbstractTypeInfo_t3397773112), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2230[1] = 
{
	AbstractTypeInfo_t3397773112::get_offset_of__type_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2231 = { sizeof (TypeInfoWrapper_t3766999367), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2232 = { sizeof (AndroidInterop_t1929019630), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2233 = { sizeof (U3CU3Ec_t22341567), -1, sizeof(U3CU3Ec_t22341567_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2233[5] = 
{
	U3CU3Ec_t22341567_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t22341567_StaticFields::get_offset_of_U3CU3E9__1_0_1(),
	U3CU3Ec_t22341567_StaticFields::get_offset_of_U3CU3E9__1_1_2(),
	U3CU3Ec_t22341567_StaticFields::get_offset_of_U3CU3E9__3_0_3(),
	U3CU3Ec_t22341567_StaticFields::get_offset_of_U3CU3E9__3_1_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2234 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2234[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2235 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2235[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2236 = { sizeof (AmazonHookedPlatformInfo_t2168027349), -1, sizeof(AmazonHookedPlatformInfo_t2168027349_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2236[13] = 
{
	AmazonHookedPlatformInfo_t2168027349_StaticFields::get_offset_of__logger_0(),
	AmazonHookedPlatformInfo_t2168027349_StaticFields::get_offset_of_instance_1(),
	AmazonHookedPlatformInfo_t2168027349::get_offset_of_device_platform_2(),
	AmazonHookedPlatformInfo_t2168027349::get_offset_of_device_model_3(),
	AmazonHookedPlatformInfo_t2168027349::get_offset_of_device_make_4(),
	AmazonHookedPlatformInfo_t2168027349::get_offset_of_device_platformVersion_5(),
	AmazonHookedPlatformInfo_t2168027349::get_offset_of_device_locale_6(),
	AmazonHookedPlatformInfo_t2168027349::get_offset_of_app_version_name_7(),
	AmazonHookedPlatformInfo_t2168027349::get_offset_of_app_version_code_8(),
	AmazonHookedPlatformInfo_t2168027349::get_offset_of_app_package_name_9(),
	AmazonHookedPlatformInfo_t2168027349::get_offset_of_app_title_10(),
	AmazonHookedPlatformInfo_t2168027349::get_offset_of_U3CPersistentDataPathU3Ek__BackingField_11(),
	AmazonHookedPlatformInfo_t2168027349::get_offset_of_U3CUnityVersionU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2237 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2238 = { sizeof (ApplicationSettingsMode_t788768262)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2238[4] = 
{
	ApplicationSettingsMode_t788768262::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2239 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2240 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2241 = { sizeof (NetworkStatus_t879095062)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2241[4] = 
{
	NetworkStatus_t879095062::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2242 = { sizeof (NetworkStatusEventArgs_t1607167653), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2242[1] = 
{
	NetworkStatusEventArgs_t1607167653::get_offset_of_U3CStatusU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2243 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2244 = { sizeof (ServiceFactory_t589839913), -1, sizeof(ServiceFactory_t589839913_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2244[6] = 
{
	ServiceFactory_t589839913_StaticFields::get_offset_of__lock_0(),
	ServiceFactory_t589839913_StaticFields::get_offset_of__factoryInitialized_1(),
	ServiceFactory_t589839913_StaticFields::get_offset_of__mappings_2(),
	ServiceFactory_t589839913::get_offset_of__instantationMappings_3(),
	ServiceFactory_t589839913::get_offset_of__singletonServices_4(),
	ServiceFactory_t589839913_StaticFields::get_offset_of_Instance_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2245 = { sizeof (InstantiationModel_t1632807378)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2245[3] = 
{
	InstantiationModel_t1632807378::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2246 = { sizeof (ApplicationInfo_t3929346268), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2247 = { sizeof (ApplicationSettings_t3429773411), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2247[1] = 
{
	ApplicationSettings_t3429773411::get_offset_of_kvStore_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2248 = { sizeof (EnvironmentInfo_t4123618435), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2248[2] = 
{
	EnvironmentInfo_t4123618435::get_offset_of_U3CFrameworkUserAgentU3Ek__BackingField_0(),
	EnvironmentInfo_t4123618435::get_offset_of_U3CPlatformUserAgentU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2249 = { sizeof (NetworkReachability_t3059923765), -1, sizeof(NetworkReachability_t3059923765_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2249[2] = 
{
	NetworkReachability_t3059923765::get_offset_of_mNetworkReachabilityChanged_0(),
	NetworkReachability_t3059923765_StaticFields::get_offset_of_reachabilityChangedLock_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2250 = { sizeof (AmazonClientException_t332426366), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2251 = { sizeof (AmazonServiceClient_t3583134838), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2251[10] = 
{
	AmazonServiceClient_t3583134838::get_offset_of__disposed_0(),
	AmazonServiceClient_t3583134838::get_offset_of__logger_1(),
	AmazonServiceClient_t3583134838::get_offset_of_U3CRuntimePipelineU3Ek__BackingField_2(),
	AmazonServiceClient_t3583134838::get_offset_of_U3CCredentialsU3Ek__BackingField_3(),
	AmazonServiceClient_t3583134838::get_offset_of_U3CConfigU3Ek__BackingField_4(),
	AmazonServiceClient_t3583134838::get_offset_of_mBeforeMarshallingEvent_5(),
	AmazonServiceClient_t3583134838::get_offset_of_mBeforeRequestEvent_6(),
	AmazonServiceClient_t3583134838::get_offset_of_mAfterResponseEvent_7(),
	AmazonServiceClient_t3583134838::get_offset_of_mExceptionEvent_8(),
	AmazonServiceClient_t3583134838::get_offset_of_U3CSignerU3Ek__BackingField_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2252 = { sizeof (AmazonServiceException_t3748559634), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2252[4] = 
{
	AmazonServiceException_t3748559634::get_offset_of_errorType_11(),
	AmazonServiceException_t3748559634::get_offset_of_errorCode_12(),
	AmazonServiceException_t3748559634::get_offset_of_requestId_13(),
	AmazonServiceException_t3748559634::get_offset_of_statusCode_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2253 = { sizeof (AmazonUnmarshallingException_t1083472470), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2253[2] = 
{
	AmazonUnmarshallingException_t1083472470::get_offset_of_U3CLastKnownLocationU3Ek__BackingField_15(),
	AmazonUnmarshallingException_t1083472470::get_offset_of_U3CResponseBodyU3Ek__BackingField_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2254 = { sizeof (AmazonWebServiceRequest_t3384026212), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2254[3] = 
{
	AmazonWebServiceRequest_t3384026212::get_offset_of_mBeforeRequestEvent_0(),
	AmazonWebServiceRequest_t3384026212::get_offset_of_U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_StreamUploadProgressCallbackU3Ek__BackingField_1(),
	AmazonWebServiceRequest_t3384026212::get_offset_of_U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_UseSigV4U3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2255 = { sizeof (AmazonWebServiceResponse_t529043356), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2255[3] = 
{
	AmazonWebServiceResponse_t529043356::get_offset_of_responseMetadataField_0(),
	AmazonWebServiceResponse_t529043356::get_offset_of_contentLength_1(),
	AmazonWebServiceResponse_t529043356::get_offset_of_httpStatusCode_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2256 = { sizeof (ImmutableCredentials_t282353664), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2256[3] = 
{
	ImmutableCredentials_t282353664::get_offset_of_U3CAccessKeyU3Ek__BackingField_0(),
	ImmutableCredentials_t282353664::get_offset_of_U3CSecretKeyU3Ek__BackingField_1(),
	ImmutableCredentials_t282353664::get_offset_of_U3CTokenU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2257 = { sizeof (AWSCredentials_t3583921007), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2258 = { sizeof (RefreshingAWSCredentials_t1767066958), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2258[3] = 
{
	RefreshingAWSCredentials_t1767066958::get_offset_of_currentState_0(),
	RefreshingAWSCredentials_t1767066958::get_offset_of__refreshLock_1(),
	RefreshingAWSCredentials_t1767066958::get_offset_of__preemptExpiryTime_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2259 = { sizeof (CredentialsRefreshState_t3294867821), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2259[2] = 
{
	CredentialsRefreshState_t3294867821::get_offset_of_U3CCredentialsU3Ek__BackingField_0(),
	CredentialsRefreshState_t3294867821::get_offset_of_U3CExpirationU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2260 = { sizeof (AnonymousAWSCredentials_t3877662854), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2261 = { sizeof (AWSRegion_t969138115), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2261[1] = 
{
	AWSRegion_t969138115::get_offset_of_U3CRegionU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2262 = { sizeof (AppConfigAWSRegion_t3930413220), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2263 = { sizeof (FallbackRegionFactory_t860882262), -1, sizeof(FallbackRegionFactory_t860882262_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2263[4] = 
{
	FallbackRegionFactory_t860882262_StaticFields::get_offset_of__lock_0(),
	FallbackRegionFactory_t860882262_StaticFields::get_offset_of_U3CAllGeneratorsU3Ek__BackingField_1(),
	FallbackRegionFactory_t860882262_StaticFields::get_offset_of_U3CNonMetadataGeneratorsU3Ek__BackingField_2(),
	FallbackRegionFactory_t860882262_StaticFields::get_offset_of_cachedRegion_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2264 = { sizeof (RegionGenerator_t3123742622), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2265 = { sizeof (U3CU3Ec_t4142977428), -1, sizeof(U3CU3Ec_t4142977428_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2265[3] = 
{
	U3CU3Ec_t4142977428_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t4142977428_StaticFields::get_offset_of_U3CU3E9__11_0_1(),
	U3CU3Ec_t4142977428_StaticFields::get_offset_of_U3CU3E9__11_1_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2266 = { sizeof (ClientConfig_t3664713661), -1, sizeof(ClientConfig_t3664713661_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2266[21] = 
{
	ClientConfig_t3664713661_StaticFields::get_offset_of_InfiniteTimeout_0(),
	ClientConfig_t3664713661_StaticFields::get_offset_of_MaxTimeout_1(),
	ClientConfig_t3664713661::get_offset_of_regionEndpoint_2(),
	ClientConfig_t3664713661::get_offset_of_probeForRegionEndpoint_3(),
	ClientConfig_t3664713661::get_offset_of_throttleRetries_4(),
	ClientConfig_t3664713661::get_offset_of_useHttp_5(),
	ClientConfig_t3664713661::get_offset_of_serviceURL_6(),
	ClientConfig_t3664713661::get_offset_of_authRegion_7(),
	ClientConfig_t3664713661::get_offset_of_authServiceName_8(),
	ClientConfig_t3664713661::get_offset_of_signatureVersion_9(),
	ClientConfig_t3664713661::get_offset_of_signatureMethod_10(),
	ClientConfig_t3664713661::get_offset_of_maxErrorRetry_11(),
	ClientConfig_t3664713661::get_offset_of_logResponse_12(),
	ClientConfig_t3664713661::get_offset_of_bufferSize_13(),
	ClientConfig_t3664713661::get_offset_of_progressUpdateInterval_14(),
	ClientConfig_t3664713661::get_offset_of_resignRetries_15(),
	ClientConfig_t3664713661::get_offset_of_logMetrics_16(),
	ClientConfig_t3664713661::get_offset_of_disableLogging_17(),
	ClientConfig_t3664713661::get_offset_of_allowAutoRedirect_18(),
	ClientConfig_t3664713661::get_offset_of_useDualstackEndpoint_19(),
	ClientConfig_t3664713661::get_offset_of_proxyPort_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2267 = { sizeof (ConstantClass_t4000559886), -1, sizeof(ConstantClass_t4000559886_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2267[3] = 
{
	ConstantClass_t4000559886_StaticFields::get_offset_of_staticFieldsLock_0(),
	ConstantClass_t4000559886_StaticFields::get_offset_of_staticFields_1(),
	ConstantClass_t4000559886::get_offset_of_U3CValueU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2268 = { sizeof (SigningAlgorithm_t3740229458)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2268[3] = 
{
	SigningAlgorithm_t3740229458::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2269 = { sizeof (ErrorType_t1448377524)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2269[4] = 
{
	ErrorType_t1448377524::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2270 = { sizeof (ExceptionEventHandler_t3236465969), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2271 = { sizeof (ExceptionEventArgs_t154100464), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2272 = { sizeof (WebServiceExceptionEventArgs_t3226266439), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2272[6] = 
{
	WebServiceExceptionEventArgs_t3226266439::get_offset_of_U3CHeadersU3Ek__BackingField_1(),
	WebServiceExceptionEventArgs_t3226266439::get_offset_of_U3CParametersU3Ek__BackingField_2(),
	WebServiceExceptionEventArgs_t3226266439::get_offset_of_U3CServiceNameU3Ek__BackingField_3(),
	WebServiceExceptionEventArgs_t3226266439::get_offset_of_U3CEndpointU3Ek__BackingField_4(),
	WebServiceExceptionEventArgs_t3226266439::get_offset_of_U3CRequestU3Ek__BackingField_5(),
	WebServiceExceptionEventArgs_t3226266439::get_offset_of_U3CExceptionU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2273 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2274 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2275 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2276 = { sizeof (PreRequestEventArgs_t776850383), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2276[1] = 
{
	PreRequestEventArgs_t776850383::get_offset_of_U3CRequestU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2277 = { sizeof (PreRequestEventHandler_t345425304), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2278 = { sizeof (RequestEventArgs_t434225820), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2279 = { sizeof (WebServiceRequestEventArgs_t1089733597), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2279[5] = 
{
	WebServiceRequestEventArgs_t1089733597::get_offset_of_U3CHeadersU3Ek__BackingField_1(),
	WebServiceRequestEventArgs_t1089733597::get_offset_of_U3CParametersU3Ek__BackingField_2(),
	WebServiceRequestEventArgs_t1089733597::get_offset_of_U3CServiceNameU3Ek__BackingField_3(),
	WebServiceRequestEventArgs_t1089733597::get_offset_of_U3CEndpointU3Ek__BackingField_4(),
	WebServiceRequestEventArgs_t1089733597::get_offset_of_U3CRequestU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2280 = { sizeof (RequestEventHandler_t2213783891), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2281 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2282 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2283 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2284 = { sizeof (Metric_t3273440202)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2284[27] = 
{
	Metric_t3273440202::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2285 = { sizeof (ResponseEventHandler_t3870676125), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2286 = { sizeof (ResponseEventArgs_t4056063878), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2287 = { sizeof (WebServiceResponseEventArgs_t1104338495), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2287[7] = 
{
	WebServiceResponseEventArgs_t1104338495::get_offset_of_U3CRequestHeadersU3Ek__BackingField_1(),
	WebServiceResponseEventArgs_t1104338495::get_offset_of_U3CResponseHeadersU3Ek__BackingField_2(),
	WebServiceResponseEventArgs_t1104338495::get_offset_of_U3CParametersU3Ek__BackingField_3(),
	WebServiceResponseEventArgs_t1104338495::get_offset_of_U3CServiceNameU3Ek__BackingField_4(),
	WebServiceResponseEventArgs_t1104338495::get_offset_of_U3CEndpointU3Ek__BackingField_5(),
	WebServiceResponseEventArgs_t1104338495::get_offset_of_U3CRequestU3Ek__BackingField_6(),
	WebServiceResponseEventArgs_t1104338495::get_offset_of_U3CResponseU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2288 = { sizeof (ResponseMetadata_t527027456), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2288[2] = 
{
	ResponseMetadata_t527027456::get_offset_of_requestIdField_0(),
	ResponseMetadata_t527027456::get_offset_of__metadata_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2289 = { sizeof (StreamTransferProgressArgs_t2639638063), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2289[3] = 
{
	StreamTransferProgressArgs_t2639638063::get_offset_of__incrementTransferred_1(),
	StreamTransferProgressArgs_t2639638063::get_offset_of__total_2(),
	StreamTransferProgressArgs_t2639638063::get_offset_of__transferred_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2290 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2291 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2292 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2293 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2294 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2295 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2296 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2297 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2298 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2299 = { 0, 0, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
