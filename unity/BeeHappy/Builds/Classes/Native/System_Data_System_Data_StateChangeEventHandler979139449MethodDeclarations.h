﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Data.StateChangeEventHandler
struct StateChangeEventHandler_t979139449;
// System.Object
struct Il2CppObject;
// System.Data.StateChangeEventArgs
struct StateChangeEventArgs_t215369544;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "System_Data_System_Data_StateChangeEventArgs215369544.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void System.Data.StateChangeEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void StateChangeEventHandler__ctor_m3466766454 (StateChangeEventHandler_t979139449 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.StateChangeEventHandler::Invoke(System.Object,System.Data.StateChangeEventArgs)
extern "C"  void StateChangeEventHandler_Invoke_m803603641 (StateChangeEventHandler_t979139449 * __this, Il2CppObject * ___sender0, StateChangeEventArgs_t215369544 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult System.Data.StateChangeEventHandler::BeginInvoke(System.Object,System.Data.StateChangeEventArgs,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * StateChangeEventHandler_BeginInvoke_m1649635066 (StateChangeEventHandler_t979139449 * __this, Il2CppObject * ___sender0, StateChangeEventArgs_t215369544 * ___e1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.StateChangeEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void StateChangeEventHandler_EndInvoke_m3472464448 (StateChangeEventHandler_t979139449 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
