﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Data.Sqlite.SQLiteFinalCallback
struct SQLiteFinalCallback_t3453741687;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Mono.Data.Sqlite.SQLiteFinalCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void SQLiteFinalCallback__ctor_m2949827950 (SQLiteFinalCallback_t3453741687 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SQLiteFinalCallback::Invoke(System.IntPtr)
extern "C"  void SQLiteFinalCallback_Invoke_m2306189754 (SQLiteFinalCallback_t3453741687 * __this, IntPtr_t ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Mono.Data.Sqlite.SQLiteFinalCallback::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * SQLiteFinalCallback_BeginInvoke_m427804167 (SQLiteFinalCallback_t3453741687 * __this, IntPtr_t ___context0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SQLiteFinalCallback::EndInvoke(System.IAsyncResult)
extern "C"  void SQLiteFinalCallback_EndInvoke_m2070424096 (SQLiteFinalCallback_t3453741687 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
