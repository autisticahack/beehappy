﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Data.Common.DbConnection
struct DbConnection_t1449646780;
// System.Data.Common.DbCommand
struct DbCommand_t673053565;
// System.Transactions.Transaction
struct Transaction_t869361102;
// System.Data.DataTable
struct DataTable_t3267612424;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Data.Common.DbProviderFactory
struct DbProviderFactory_t661609867;

#include "codegen/il2cpp-codegen.h"
#include "System_Transactions_System_Transactions_Transaction869361102.h"
#include "mscorlib_System_String2029220233.h"
#include "System_Data_System_Data_Common_DbCommand673053565.h"
#include "System_Data_System_Data_DbType3924915636.h"

// System.Void System.Data.Common.DbConnection::.ctor()
extern "C"  void DbConnection__ctor_m172397736 (DbConnection_t1449646780 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DbCommand System.Data.Common.DbConnection::CreateCommand()
extern "C"  DbCommand_t673053565 * DbConnection_CreateCommand_m1562783513 (DbConnection_t1449646780 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.DbConnection::EnlistTransaction(System.Transactions.Transaction)
extern "C"  void DbConnection_EnlistTransaction_m1210261181 (DbConnection_t1449646780 * __this, Transaction_t869361102 * ___transaction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.DataTable System.Data.Common.DbConnection::GetSchema(System.String)
extern "C"  DataTable_t3267612424 * DbConnection_GetSchema_m1658951375 (DbConnection_t1449646780 * __this, String_t* ___collectionName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.DbConnection::AddParameter(System.Data.Common.DbCommand,System.String,System.Data.DbType,System.Int32)
extern "C"  void DbConnection_AddParameter_m2383548141 (DbConnection_t1449646780 * __this, DbCommand_t673053565 * ___command0, String_t* ___parameterName1, int32_t ___parameterType2, int32_t ___parameterSize3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.DataTable System.Data.Common.DbConnection::GetSchema(System.String,System.String[])
extern "C"  DataTable_t3267612424 * DbConnection_GetSchema_m4142908883 (DbConnection_t1449646780 * __this, String_t* ___collectionName0, StringU5BU5D_t1642385972* ___restrictionValues1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DbProviderFactory System.Data.Common.DbConnection::get_DbProviderFactory()
extern "C"  DbProviderFactory_t661609867 * DbConnection_get_DbProviderFactory_m1130898014 (DbConnection_t1449646780 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
