﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ThirdParty.Json.LitJson.JsonWriter
struct JsonWriter_t3014444111;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;
// System.IO.TextWriter
struct TextWriter_t4027217640;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Text_StringBuilder1221177846.h"
#include "mscorlib_System_IO_TextWriter4027217640.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_Condition2230619687.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Decimal724701077.h"
#include "mscorlib_System_DateTime693205669.h"

// System.Void ThirdParty.Json.LitJson.JsonWriter::.cctor()
extern "C"  void JsonWriter__cctor_m1823466769 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.JsonWriter::.ctor()
extern "C"  void JsonWriter__ctor_m1529769822 (JsonWriter_t3014444111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.JsonWriter::.ctor(System.Text.StringBuilder)
extern "C"  void JsonWriter__ctor_m83397816 (JsonWriter_t3014444111 * __this, StringBuilder_t1221177846 * ___sb0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.JsonWriter::.ctor(System.IO.TextWriter)
extern "C"  void JsonWriter__ctor_m2344288487 (JsonWriter_t3014444111 * __this, TextWriter_t4027217640 * ___writer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.JsonWriter::DoValidation(ThirdParty.Json.LitJson.Condition)
extern "C"  void JsonWriter_DoValidation_m2832088507 (JsonWriter_t3014444111 * __this, int32_t ___cond0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.JsonWriter::Init()
extern "C"  void JsonWriter_Init_m302664798 (JsonWriter_t3014444111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.JsonWriter::IntToHex(System.Int32,System.Char[])
extern "C"  void JsonWriter_IntToHex_m4158006151 (Il2CppObject * __this /* static, unused */, int32_t ___n0, CharU5BU5D_t1328083999* ___hex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.JsonWriter::Indent()
extern "C"  void JsonWriter_Indent_m2307597358 (JsonWriter_t3014444111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.JsonWriter::Put(System.String)
extern "C"  void JsonWriter_Put_m564997329 (JsonWriter_t3014444111 * __this, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.JsonWriter::PutNewline()
extern "C"  void JsonWriter_PutNewline_m1108742881 (JsonWriter_t3014444111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.JsonWriter::PutNewline(System.Boolean)
extern "C"  void JsonWriter_PutNewline_m347485110 (JsonWriter_t3014444111 * __this, bool ___add_comma0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.JsonWriter::PutString(System.String)
extern "C"  void JsonWriter_PutString_m1961797724 (JsonWriter_t3014444111 * __this, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.JsonWriter::Unindent()
extern "C"  void JsonWriter_Unindent_m339292545 (JsonWriter_t3014444111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ThirdParty.Json.LitJson.JsonWriter::ToString()
extern "C"  String_t* JsonWriter_ToString_m3989428233 (JsonWriter_t3014444111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.JsonWriter::Write(System.Decimal)
extern "C"  void JsonWriter_Write_m580542055 (JsonWriter_t3014444111 * __this, Decimal_t724701077  ___number0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.JsonWriter::Write(System.Double)
extern "C"  void JsonWriter_Write_m1257361297 (JsonWriter_t3014444111 * __this, double ___number0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.JsonWriter::Write(System.Int32)
extern "C"  void JsonWriter_Write_m2327906996 (JsonWriter_t3014444111 * __this, int32_t ___number0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.JsonWriter::Write(System.Int64)
extern "C"  void JsonWriter_Write_m1165107417 (JsonWriter_t3014444111 * __this, int64_t ___number0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.JsonWriter::Write(System.String)
extern "C"  void JsonWriter_Write_m3974344637 (JsonWriter_t3014444111 * __this, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.JsonWriter::Write(System.UInt64)
extern "C"  void JsonWriter_Write_m281441472 (JsonWriter_t3014444111 * __this, uint64_t ___number0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.JsonWriter::Write(System.DateTime)
extern "C"  void JsonWriter_Write_m3398380085 (JsonWriter_t3014444111 * __this, DateTime_t693205669  ___date0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.JsonWriter::WriteArrayEnd()
extern "C"  void JsonWriter_WriteArrayEnd_m361558409 (JsonWriter_t3014444111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.JsonWriter::WriteArrayStart()
extern "C"  void JsonWriter_WriteArrayStart_m2755874534 (JsonWriter_t3014444111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.JsonWriter::WriteObjectEnd()
extern "C"  void JsonWriter_WriteObjectEnd_m4253837441 (JsonWriter_t3014444111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.JsonWriter::WriteObjectStart()
extern "C"  void JsonWriter_WriteObjectStart_m3468068136 (JsonWriter_t3014444111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.JsonWriter::WritePropertyName(System.String)
extern "C"  void JsonWriter_WritePropertyName_m1171400361 (JsonWriter_t3014444111 * __this, String_t* ___property_name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
