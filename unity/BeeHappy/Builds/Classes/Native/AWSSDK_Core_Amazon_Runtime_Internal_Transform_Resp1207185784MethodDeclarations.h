﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.Transform.ResponseMetadataUnmarshaller
struct ResponseMetadataUnmarshaller_t1207185784;
// Amazon.Runtime.ResponseMetadata
struct ResponseMetadata_t527027456;
// Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext
struct XmlUnmarshallerContext_t1179575220;
// Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext
struct JsonUnmarshallerContext_t456235889;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_XmlU1179575220.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_JsonU456235889.h"

// System.Void Amazon.Runtime.Internal.Transform.ResponseMetadataUnmarshaller::.ctor()
extern "C"  void ResponseMetadataUnmarshaller__ctor_m1045807434 (ResponseMetadataUnmarshaller_t1207185784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.Internal.Transform.ResponseMetadataUnmarshaller Amazon.Runtime.Internal.Transform.ResponseMetadataUnmarshaller::get_Instance()
extern "C"  ResponseMetadataUnmarshaller_t1207185784 * ResponseMetadataUnmarshaller_get_Instance_m945488888 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.ResponseMetadata Amazon.Runtime.Internal.Transform.ResponseMetadataUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext)
extern "C"  ResponseMetadata_t527027456 * ResponseMetadataUnmarshaller_Unmarshall_m1427520039 (ResponseMetadataUnmarshaller_t1207185784 * __this, XmlUnmarshallerContext_t1179575220 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.ResponseMetadata Amazon.Runtime.Internal.Transform.ResponseMetadataUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern "C"  ResponseMetadata_t527027456 * ResponseMetadataUnmarshaller_Unmarshall_m4037172498 (ResponseMetadataUnmarshaller_t1207185784 * __this, JsonUnmarshallerContext_t456235889 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Transform.ResponseMetadataUnmarshaller::.cctor()
extern "C"  void ResponseMetadataUnmarshaller__cctor_m3159824923 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
