﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ThirdParty.Ionic.Zlib.CRC32
struct CRC32_t619824607;
// System.Byte[]
struct ByteU5BU5D_t3397334013;

#include "codegen/il2cpp-codegen.h"

// System.Int64 ThirdParty.Ionic.Zlib.CRC32::get_TotalBytesRead()
extern "C"  int64_t CRC32_get_TotalBytesRead_m3362320857 (CRC32_t619824607 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ThirdParty.Ionic.Zlib.CRC32::get_Crc32Result()
extern "C"  int32_t CRC32_get_Crc32Result_m2738951533 (CRC32_t619824607 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Ionic.Zlib.CRC32::SlurpBlock(System.Byte[],System.Int32,System.Int32)
extern "C"  void CRC32_SlurpBlock_m286301878 (CRC32_t619824607 * __this, ByteU5BU5D_t3397334013* ___block0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Ionic.Zlib.CRC32::.cctor()
extern "C"  void CRC32__cctor_m3152074895 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Ionic.Zlib.CRC32::.ctor()
extern "C"  void CRC32__ctor_m3498155070 (CRC32_t619824607 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
