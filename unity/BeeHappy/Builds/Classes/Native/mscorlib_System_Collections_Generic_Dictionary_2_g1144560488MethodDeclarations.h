﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Object>
struct Dictionary_2_t1144560488;
// System.Collections.Generic.IEqualityComparer`1<Amazon.Runtime.Metric>
struct IEqualityComparer_1_t2486072980;
// System.Collections.Generic.IDictionary`2<Amazon.Runtime.Metric,System.Object>
struct IDictionary_2_t3438611205;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// System.Collections.Generic.ICollection`1<Amazon.Runtime.Metric>
struct ICollection_1_t4225515507;
// System.Collections.ICollection
struct ICollection_t91669223;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<Amazon.Runtime.Metric,System.Object>[]
struct KeyValuePair_2U5BU5D_t2610501211;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<Amazon.Runtime.Metric,System.Object>>
struct IEnumerator_1_t672396833;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t259680273;
// System.Collections.Generic.Dictionary`2/KeyCollection<Amazon.Runtime.Metric,System.Object>
struct KeyCollection_t3628058259;
// System.Collections.Generic.Dictionary`2/ValueCollection<Amazon.Runtime.Metric,System.Object>
struct ValueCollection_t4142587627;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serialization228987430.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23196873006.h"
#include "mscorlib_System_Array3829468939.h"
#include "AWSSDK_Core_Amazon_Runtime_Metric3273440202.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2464585190.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m1375108483_gshared (Dictionary_2_t1144560488 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m1375108483(__this, method) ((  void (*) (Dictionary_2_t1144560488 *, const MethodInfo*))Dictionary_2__ctor_m1375108483_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m2533510210_gshared (Dictionary_2_t1144560488 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m2533510210(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t1144560488 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2533510210_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C"  void Dictionary_2__ctor_m3512592877_gshared (Dictionary_2_t1144560488 * __this, Il2CppObject* ___dictionary0, const MethodInfo* method);
#define Dictionary_2__ctor_m3512592877(__this, ___dictionary0, method) ((  void (*) (Dictionary_2_t1144560488 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m3512592877_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Object>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m33526746_gshared (Dictionary_2_t1144560488 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m33526746(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t1144560488 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m33526746_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m2914770042_gshared (Dictionary_2_t1144560488 * __this, Il2CppObject* ___dictionary0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m2914770042(__this, ___dictionary0, ___comparer1, method) ((  void (*) (Dictionary_2_t1144560488 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2914770042_gshared)(__this, ___dictionary0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m772529612_gshared (Dictionary_2_t1144560488 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m772529612(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t1144560488 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2__ctor_m772529612_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m447356911_gshared (Dictionary_2_t1144560488 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m447356911(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t1144560488 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m447356911_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Object>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Keys_m2281580533_gshared (Dictionary_2_t1144560488 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m2281580533(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1144560488 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m2281580533_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Object>::System.Collections.IDictionary.get_Values()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Values_m1459430581_gshared (Dictionary_2_t1144560488 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Values_m1459430581(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1144560488 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m1459430581_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m3786615843_gshared (Dictionary_2_t1144560488 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m3786615843(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t1144560488 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m3786615843_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m713062216_gshared (Dictionary_2_t1144560488 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m713062216(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1144560488 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m713062216_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m3720746037_gshared (Dictionary_2_t1144560488 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m3720746037(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1144560488 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m3720746037_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Object>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool Dictionary_2_System_Collections_IDictionary_Contains_m327295033_gshared (Dictionary_2_t1144560488 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m327295033(__this, ___key0, method) ((  bool (*) (Dictionary_2_t1144560488 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m327295033_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m2775049012_gshared (Dictionary_2_t1144560488 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m2775049012(__this, ___key0, method) ((  void (*) (Dictionary_2_t1144560488 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m2775049012_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m2033590967_gshared (Dictionary_2_t1144560488 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m2033590967(__this, method) ((  bool (*) (Dictionary_2_t1144560488 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m2033590967_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1288383167_gshared (Dictionary_2_t1144560488 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1288383167(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1144560488 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1288383167_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m802680901_gshared (Dictionary_2_t1144560488 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m802680901(__this, method) ((  bool (*) (Dictionary_2_t1144560488 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m802680901_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m923982838_gshared (Dictionary_2_t1144560488 * __this, KeyValuePair_2_t3196873006  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m923982838(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t1144560488 *, KeyValuePair_2_t3196873006 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m923982838_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2814678698_gshared (Dictionary_2_t1144560488 * __this, KeyValuePair_2_t3196873006  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2814678698(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t1144560488 *, KeyValuePair_2_t3196873006 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2814678698_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2281891210_gshared (Dictionary_2_t1144560488 * __this, KeyValuePair_2U5BU5D_t2610501211* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2281891210(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1144560488 *, KeyValuePair_2U5BU5D_t2610501211*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2281891210_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1474880515_gshared (Dictionary_2_t1144560488 * __this, KeyValuePair_2_t3196873006  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1474880515(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t1144560488 *, KeyValuePair_2_t3196873006 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1474880515_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m661006031_gshared (Dictionary_2_t1144560488 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m661006031(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1144560488 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m661006031_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3717789540_gshared (Dictionary_2_t1144560488 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3717789540(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1144560488 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3717789540_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3329258525_gshared (Dictionary_2_t1144560488 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3329258525(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t1144560488 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3329258525_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m659556578_gshared (Dictionary_2_t1144560488 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m659556578(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1144560488 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m659556578_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Object>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m2014614799_gshared (Dictionary_2_t1144560488 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m2014614799(__this, method) ((  int32_t (*) (Dictionary_2_t1144560488 *, const MethodInfo*))Dictionary_2_get_Count_m2014614799_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Object>::get_Item(TKey)
extern "C"  Il2CppObject * Dictionary_2_get_Item_m3500828354_gshared (Dictionary_2_t1144560488 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m3500828354(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t1144560488 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m3500828354_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Object>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m706647835_gshared (Dictionary_2_t1144560488 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m706647835(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1144560488 *, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_set_Item_m706647835_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m3847574931_gshared (Dictionary_2_t1144560488 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m3847574931(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t1144560488 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m3847574931_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Object>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m910935862_gshared (Dictionary_2_t1144560488 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m910935862(__this, ___size0, method) ((  void (*) (Dictionary_2_t1144560488 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m910935862_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Object>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m1720282588_gshared (Dictionary_2_t1144560488 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m1720282588(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1144560488 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m1720282588_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Object>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t3196873006  Dictionary_2_make_pair_m1425010974_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m1425010974(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t3196873006  (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_make_pair_m1425010974_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Object>::pick_key(TKey,TValue)
extern "C"  int32_t Dictionary_2_pick_key_m3091792648_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_key_m3091792648(__this /* static, unused */, ___key0, ___value1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_pick_key_m3091792648_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Object>::pick_value(TKey,TValue)
extern "C"  Il2CppObject * Dictionary_2_pick_value_m2542204640_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m2542204640(__this /* static, unused */, ___key0, ___value1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_pick_value_m2542204640_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m133672535_gshared (Dictionary_2_t1144560488 * __this, KeyValuePair_2U5BU5D_t2610501211* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m133672535(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1144560488 *, KeyValuePair_2U5BU5D_t2610501211*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m133672535_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Object>::Resize()
extern "C"  void Dictionary_2_Resize_m625399077_gshared (Dictionary_2_t1144560488 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m625399077(__this, method) ((  void (*) (Dictionary_2_t1144560488 *, const MethodInfo*))Dictionary_2_Resize_m625399077_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Object>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m3460488822_gshared (Dictionary_2_t1144560488 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m3460488822(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1144560488 *, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_Add_m3460488822_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Object>::Clear()
extern "C"  void Dictionary_2_Clear_m3695438406_gshared (Dictionary_2_t1144560488 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m3695438406(__this, method) ((  void (*) (Dictionary_2_t1144560488 *, const MethodInfo*))Dictionary_2_Clear_m3695438406_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Object>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m2240585314_gshared (Dictionary_2_t1144560488 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m2240585314(__this, ___key0, method) ((  bool (*) (Dictionary_2_t1144560488 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m2240585314_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Object>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m930553450_gshared (Dictionary_2_t1144560488 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m930553450(__this, ___value0, method) ((  bool (*) (Dictionary_2_t1144560488 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ContainsValue_m930553450_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m1242670075_gshared (Dictionary_2_t1144560488 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m1242670075(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t1144560488 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2_GetObjectData_m1242670075_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Object>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m563914291_gshared (Dictionary_2_t1144560488 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m563914291(__this, ___sender0, method) ((  void (*) (Dictionary_2_t1144560488 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m563914291_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Object>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m2378912762_gshared (Dictionary_2_t1144560488 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m2378912762(__this, ___key0, method) ((  bool (*) (Dictionary_2_t1144560488 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m2378912762_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Object>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m3030247339_gshared (Dictionary_2_t1144560488 * __this, int32_t ___key0, Il2CppObject ** ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m3030247339(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t1144560488 *, int32_t, Il2CppObject **, const MethodInfo*))Dictionary_2_TryGetValue_m3030247339_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Object>::get_Keys()
extern "C"  KeyCollection_t3628058259 * Dictionary_2_get_Keys_m3891333038_gshared (Dictionary_2_t1144560488 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m3891333038(__this, method) ((  KeyCollection_t3628058259 * (*) (Dictionary_2_t1144560488 *, const MethodInfo*))Dictionary_2_get_Keys_m3891333038_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Object>::get_Values()
extern "C"  ValueCollection_t4142587627 * Dictionary_2_get_Values_m1929542622_gshared (Dictionary_2_t1144560488 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m1929542622(__this, method) ((  ValueCollection_t4142587627 * (*) (Dictionary_2_t1144560488 *, const MethodInfo*))Dictionary_2_get_Values_m1929542622_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Object>::ToTKey(System.Object)
extern "C"  int32_t Dictionary_2_ToTKey_m979324989_gshared (Dictionary_2_t1144560488 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m979324989(__this, ___key0, method) ((  int32_t (*) (Dictionary_2_t1144560488 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m979324989_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Object>::ToTValue(System.Object)
extern "C"  Il2CppObject * Dictionary_2_ToTValue_m3947482477_gshared (Dictionary_2_t1144560488 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m3947482477(__this, ___value0, method) ((  Il2CppObject * (*) (Dictionary_2_t1144560488 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m3947482477_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Object>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m3754870187_gshared (Dictionary_2_t1144560488 * __this, KeyValuePair_2_t3196873006  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m3754870187(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t1144560488 *, KeyValuePair_2_t3196873006 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m3754870187_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Object>::GetEnumerator()
extern "C"  Enumerator_t2464585190  Dictionary_2_GetEnumerator_m3376110438_gshared (Dictionary_2_t1144560488 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m3376110438(__this, method) ((  Enumerator_t2464585190  (*) (Dictionary_2_t1144560488 *, const MethodInfo*))Dictionary_2_GetEnumerator_m3376110438_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Object>::<CopyTo>m__0(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Dictionary_2_U3CCopyToU3Em__0_m2886133605_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m2886133605(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m2886133605_gshared)(__this /* static, unused */, ___key0, ___value1, method)
