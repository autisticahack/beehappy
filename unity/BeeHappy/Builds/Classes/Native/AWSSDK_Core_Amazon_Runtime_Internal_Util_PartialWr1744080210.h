﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AWSSDK_Core_Amazon_Runtime_Internal_Util_WrapperStr816765491.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Util.PartialWrapperStream
struct  PartialWrapperStream_t1744080210  : public WrapperStream_t816765491
{
public:
	// System.Int64 Amazon.Runtime.Internal.Util.PartialWrapperStream::initialPosition
	int64_t ___initialPosition_2;
	// System.Int64 Amazon.Runtime.Internal.Util.PartialWrapperStream::partSize
	int64_t ___partSize_3;

public:
	inline static int32_t get_offset_of_initialPosition_2() { return static_cast<int32_t>(offsetof(PartialWrapperStream_t1744080210, ___initialPosition_2)); }
	inline int64_t get_initialPosition_2() const { return ___initialPosition_2; }
	inline int64_t* get_address_of_initialPosition_2() { return &___initialPosition_2; }
	inline void set_initialPosition_2(int64_t value)
	{
		___initialPosition_2 = value;
	}

	inline static int32_t get_offset_of_partSize_3() { return static_cast<int32_t>(offsetof(PartialWrapperStream_t1744080210, ___partSize_3)); }
	inline int64_t get_partSize_3() const { return ___partSize_3; }
	inline int64_t* get_address_of_partSize_3() { return &___partSize_3; }
	inline void set_partSize_3(int64_t value)
	{
		___partSize_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
