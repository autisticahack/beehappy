﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Data.Common.DbDataRecord
struct DbDataRecord_t1591831971;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Data.Common.DbDataRecord::.ctor()
extern "C"  void DbDataRecord__ctor_m1905381629 (DbDataRecord_t1591831971 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
