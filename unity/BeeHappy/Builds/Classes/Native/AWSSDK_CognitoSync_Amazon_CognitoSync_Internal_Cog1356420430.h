﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.Type,Amazon.CognitoSync.Internal.CognitoCredentialsRetriever/CSRequestCache/CSRequest>
struct Dictionary_2_t1557426931;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.CognitoSync.Internal.CognitoCredentialsRetriever/CSRequestCache
struct  CSRequestCache_t1356420430  : public Il2CppObject
{
public:

public:
};

struct CSRequestCache_t1356420430_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Type,Amazon.CognitoSync.Internal.CognitoCredentialsRetriever/CSRequestCache/CSRequest> Amazon.CognitoSync.Internal.CognitoCredentialsRetriever/CSRequestCache::requestCache
	Dictionary_2_t1557426931 * ___requestCache_0;

public:
	inline static int32_t get_offset_of_requestCache_0() { return static_cast<int32_t>(offsetof(CSRequestCache_t1356420430_StaticFields, ___requestCache_0)); }
	inline Dictionary_2_t1557426931 * get_requestCache_0() const { return ___requestCache_0; }
	inline Dictionary_2_t1557426931 ** get_address_of_requestCache_0() { return &___requestCache_0; }
	inline void set_requestCache_0(Dictionary_2_t1557426931 * value)
	{
		___requestCache_0 = value;
		Il2CppCodeGenWriteBarrier(&___requestCache_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
