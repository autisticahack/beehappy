﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen161475956.h"
#include "System_System_Net_HttpStatusCode1898409641.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Nullable`1<System.Net.HttpStatusCode>::.ctor(T)
extern "C"  void Nullable_1__ctor_m1772187560_gshared (Nullable_1_t161475956 * __this, int32_t ___value0, const MethodInfo* method);
#define Nullable_1__ctor_m1772187560(__this, ___value0, method) ((  void (*) (Nullable_1_t161475956 *, int32_t, const MethodInfo*))Nullable_1__ctor_m1772187560_gshared)(__this, ___value0, method)
// System.Boolean System.Nullable`1<System.Net.HttpStatusCode>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m1604088093_gshared (Nullable_1_t161475956 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m1604088093(__this, method) ((  bool (*) (Nullable_1_t161475956 *, const MethodInfo*))Nullable_1_get_HasValue_m1604088093_gshared)(__this, method)
// T System.Nullable`1<System.Net.HttpStatusCode>::get_Value()
extern "C"  int32_t Nullable_1_get_Value_m3874086585_gshared (Nullable_1_t161475956 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m3874086585(__this, method) ((  int32_t (*) (Nullable_1_t161475956 *, const MethodInfo*))Nullable_1_get_Value_m3874086585_gshared)(__this, method)
// System.Boolean System.Nullable`1<System.Net.HttpStatusCode>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m1866955167_gshared (Nullable_1_t161475956 * __this, Il2CppObject * ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m1866955167(__this, ___other0, method) ((  bool (*) (Nullable_1_t161475956 *, Il2CppObject *, const MethodInfo*))Nullable_1_Equals_m1866955167_gshared)(__this, ___other0, method)
// System.Boolean System.Nullable`1<System.Net.HttpStatusCode>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m3668973186_gshared (Nullable_1_t161475956 * __this, Nullable_1_t161475956  ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m3668973186(__this, ___other0, method) ((  bool (*) (Nullable_1_t161475956 *, Nullable_1_t161475956 , const MethodInfo*))Nullable_1_Equals_m3668973186_gshared)(__this, ___other0, method)
// System.Int32 System.Nullable`1<System.Net.HttpStatusCode>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m2611149221_gshared (Nullable_1_t161475956 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m2611149221(__this, method) ((  int32_t (*) (Nullable_1_t161475956 *, const MethodInfo*))Nullable_1_GetHashCode_m2611149221_gshared)(__this, method)
// T System.Nullable`1<System.Net.HttpStatusCode>::GetValueOrDefault()
extern "C"  int32_t Nullable_1_GetValueOrDefault_m1815728518_gshared (Nullable_1_t161475956 * __this, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m1815728518(__this, method) ((  int32_t (*) (Nullable_1_t161475956 *, const MethodInfo*))Nullable_1_GetValueOrDefault_m1815728518_gshared)(__this, method)
// T System.Nullable`1<System.Net.HttpStatusCode>::GetValueOrDefault(T)
extern "C"  int32_t Nullable_1_GetValueOrDefault_m3156809264_gshared (Nullable_1_t161475956 * __this, int32_t ___defaultValue0, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m3156809264(__this, ___defaultValue0, method) ((  int32_t (*) (Nullable_1_t161475956 *, int32_t, const MethodInfo*))Nullable_1_GetValueOrDefault_m3156809264_gshared)(__this, ___defaultValue0, method)
// System.String System.Nullable`1<System.Net.HttpStatusCode>::ToString()
extern "C"  String_t* Nullable_1_ToString_m2273074853_gshared (Nullable_1_t161475956 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m2273074853(__this, method) ((  String_t* (*) (Nullable_1_t161475956 *, const MethodInfo*))Nullable_1_ToString_m2273074853_gshared)(__this, method)
