﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest
struct AssumeRoleWithWebIdentityRequest_t193094215;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Int32 Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest::get_DurationSeconds()
extern "C"  int32_t AssumeRoleWithWebIdentityRequest_get_DurationSeconds_m179215680 (AssumeRoleWithWebIdentityRequest_t193094215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest::set_DurationSeconds(System.Int32)
extern "C"  void AssumeRoleWithWebIdentityRequest_set_DurationSeconds_m1975187011 (AssumeRoleWithWebIdentityRequest_t193094215 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest::IsSetDurationSeconds()
extern "C"  bool AssumeRoleWithWebIdentityRequest_IsSetDurationSeconds_m4189349523 (AssumeRoleWithWebIdentityRequest_t193094215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest::get_Policy()
extern "C"  String_t* AssumeRoleWithWebIdentityRequest_get_Policy_m1592875998 (AssumeRoleWithWebIdentityRequest_t193094215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest::IsSetPolicy()
extern "C"  bool AssumeRoleWithWebIdentityRequest_IsSetPolicy_m2233601292 (AssumeRoleWithWebIdentityRequest_t193094215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest::get_ProviderId()
extern "C"  String_t* AssumeRoleWithWebIdentityRequest_get_ProviderId_m1708604136 (AssumeRoleWithWebIdentityRequest_t193094215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest::IsSetProviderId()
extern "C"  bool AssumeRoleWithWebIdentityRequest_IsSetProviderId_m3772479994 (AssumeRoleWithWebIdentityRequest_t193094215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest::get_RoleArn()
extern "C"  String_t* AssumeRoleWithWebIdentityRequest_get_RoleArn_m4048335823 (AssumeRoleWithWebIdentityRequest_t193094215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest::set_RoleArn(System.String)
extern "C"  void AssumeRoleWithWebIdentityRequest_set_RoleArn_m1456092898 (AssumeRoleWithWebIdentityRequest_t193094215 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest::IsSetRoleArn()
extern "C"  bool AssumeRoleWithWebIdentityRequest_IsSetRoleArn_m3216350363 (AssumeRoleWithWebIdentityRequest_t193094215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest::get_RoleSessionName()
extern "C"  String_t* AssumeRoleWithWebIdentityRequest_get_RoleSessionName_m3730323139 (AssumeRoleWithWebIdentityRequest_t193094215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest::set_RoleSessionName(System.String)
extern "C"  void AssumeRoleWithWebIdentityRequest_set_RoleSessionName_m3614869118 (AssumeRoleWithWebIdentityRequest_t193094215 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest::IsSetRoleSessionName()
extern "C"  bool AssumeRoleWithWebIdentityRequest_IsSetRoleSessionName_m653479599 (AssumeRoleWithWebIdentityRequest_t193094215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest::get_WebIdentityToken()
extern "C"  String_t* AssumeRoleWithWebIdentityRequest_get_WebIdentityToken_m405092039 (AssumeRoleWithWebIdentityRequest_t193094215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest::set_WebIdentityToken(System.String)
extern "C"  void AssumeRoleWithWebIdentityRequest_set_WebIdentityToken_m951693196 (AssumeRoleWithWebIdentityRequest_t193094215 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest::IsSetWebIdentityToken()
extern "C"  bool AssumeRoleWithWebIdentityRequest_IsSetWebIdentityToken_m4244755691 (AssumeRoleWithWebIdentityRequest_t193094215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest::.ctor()
extern "C"  void AssumeRoleWithWebIdentityRequest__ctor_m3449302728 (AssumeRoleWithWebIdentityRequest_t193094215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
