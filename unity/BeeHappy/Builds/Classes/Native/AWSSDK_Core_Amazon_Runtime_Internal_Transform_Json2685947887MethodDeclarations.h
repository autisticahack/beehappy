﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.Transform.JsonResponseUnmarshaller
struct JsonResponseUnmarshaller_t2685947887;
// Amazon.Runtime.AmazonWebServiceResponse
struct AmazonWebServiceResponse_t529043356;
// Amazon.Runtime.Internal.Transform.UnmarshallerContext
struct UnmarshallerContext_t1608322995;
// Amazon.Runtime.AmazonServiceException
struct AmazonServiceException_t3748559634;
// System.Exception
struct Exception_t1927440687;
// System.IO.Stream
struct Stream_t3255436806;
// Amazon.Runtime.Internal.Transform.IWebResponseData
struct IWebResponseData_t3815728244;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_Unma1608322995.h"
#include "mscorlib_System_Exception1927440687.h"
#include "System_System_Net_HttpStatusCode1898409641.h"
#include "mscorlib_System_IO_Stream3255436806.h"

// Amazon.Runtime.AmazonWebServiceResponse Amazon.Runtime.Internal.Transform.JsonResponseUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.UnmarshallerContext)
extern "C"  AmazonWebServiceResponse_t529043356 * JsonResponseUnmarshaller_Unmarshall_m3623253101 (JsonResponseUnmarshaller_t2685947887 * __this, UnmarshallerContext_t1608322995 * ___input0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.AmazonServiceException Amazon.Runtime.Internal.Transform.JsonResponseUnmarshaller::UnmarshallException(Amazon.Runtime.Internal.Transform.UnmarshallerContext,System.Exception,System.Net.HttpStatusCode)
extern "C"  AmazonServiceException_t3748559634 * JsonResponseUnmarshaller_UnmarshallException_m1495062289 (JsonResponseUnmarshaller_t2685947887 * __this, UnmarshallerContext_t1608322995 * ___input0, Exception_t1927440687 * ___innerException1, int32_t ___statusCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.Internal.Transform.UnmarshallerContext Amazon.Runtime.Internal.Transform.JsonResponseUnmarshaller::ConstructUnmarshallerContext(System.IO.Stream,System.Boolean,Amazon.Runtime.Internal.Transform.IWebResponseData)
extern "C"  UnmarshallerContext_t1608322995 * JsonResponseUnmarshaller_ConstructUnmarshallerContext_m2386264049 (JsonResponseUnmarshaller_t2685947887 * __this, Stream_t3255436806 * ___responseStream0, bool ___maintainResponseBody1, Il2CppObject * ___response2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.Transform.JsonResponseUnmarshaller::ShouldReadEntireResponse(Amazon.Runtime.Internal.Transform.IWebResponseData,System.Boolean)
extern "C"  bool JsonResponseUnmarshaller_ShouldReadEntireResponse_m1920720720 (JsonResponseUnmarshaller_t2685947887 * __this, Il2CppObject * ___response0, bool ___readEntireResponse1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Transform.JsonResponseUnmarshaller::.ctor()
extern "C"  void JsonResponseUnmarshaller__ctor_m2636444097 (JsonResponseUnmarshaller_t2685947887 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
