﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.Signer
struct Signer_t1810841758;
// Amazon.Runtime.IExecutionContext
struct IExecutionContext_t2477130752;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// Amazon.Runtime.IAsyncExecutionContext
struct IAsyncExecutionContext_t3792344986;
// Amazon.Runtime.IRequestContext
struct IRequestContext_t1620878341;

#include "codegen/il2cpp-codegen.h"

// System.Void Amazon.Runtime.Internal.Signer::InvokeSync(Amazon.Runtime.IExecutionContext)
extern "C"  void Signer_InvokeSync_m2802613413 (Signer_t1810841758 * __this, Il2CppObject * ___executionContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Amazon.Runtime.Internal.Signer::InvokeAsync(Amazon.Runtime.IAsyncExecutionContext)
extern "C"  Il2CppObject * Signer_InvokeAsync_m1733963106 (Signer_t1810841758 * __this, Il2CppObject * ___executionContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Signer::PreInvoke(Amazon.Runtime.IExecutionContext)
extern "C"  void Signer_PreInvoke_m3875914805 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___executionContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.Runtime.Internal.Signer::ShouldSign(Amazon.Runtime.IRequestContext)
extern "C"  bool Signer_ShouldSign_m1230172047 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___requestContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Signer::SignRequest(Amazon.Runtime.IRequestContext)
extern "C"  void Signer_SignRequest_m3644327839 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___requestContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Signer::.ctor()
extern "C"  void Signer__ctor_m3388759206 (Signer_t1810841758 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
