﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.Runtime.Internal.Util.Logger
struct Logger_t2262497814;
// Amazon.Runtime.Internal.RuntimePipeline
struct RuntimePipeline_t3355992556;
// Amazon.Runtime.AWSCredentials
struct AWSCredentials_t3583921007;
// Amazon.Runtime.IClientConfig
struct IClientConfig_t4078933728;
// Amazon.Runtime.PreRequestEventHandler
struct PreRequestEventHandler_t345425304;
// Amazon.Runtime.RequestEventHandler
struct RequestEventHandler_t2213783891;
// Amazon.Runtime.ResponseEventHandler
struct ResponseEventHandler_t3870676125;
// Amazon.Runtime.ExceptionEventHandler
struct ExceptionEventHandler_t3236465969;
// Amazon.Runtime.Internal.Auth.AbstractAWSSigner
struct AbstractAWSSigner_t2114314031;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.AmazonServiceClient
struct  AmazonServiceClient_t3583134838  : public Il2CppObject
{
public:
	// System.Boolean Amazon.Runtime.AmazonServiceClient::_disposed
	bool ____disposed_0;
	// Amazon.Runtime.Internal.Util.Logger Amazon.Runtime.AmazonServiceClient::_logger
	Logger_t2262497814 * ____logger_1;
	// Amazon.Runtime.Internal.RuntimePipeline Amazon.Runtime.AmazonServiceClient::<RuntimePipeline>k__BackingField
	RuntimePipeline_t3355992556 * ___U3CRuntimePipelineU3Ek__BackingField_2;
	// Amazon.Runtime.AWSCredentials Amazon.Runtime.AmazonServiceClient::<Credentials>k__BackingField
	AWSCredentials_t3583921007 * ___U3CCredentialsU3Ek__BackingField_3;
	// Amazon.Runtime.IClientConfig Amazon.Runtime.AmazonServiceClient::<Config>k__BackingField
	Il2CppObject * ___U3CConfigU3Ek__BackingField_4;
	// Amazon.Runtime.PreRequestEventHandler Amazon.Runtime.AmazonServiceClient::mBeforeMarshallingEvent
	PreRequestEventHandler_t345425304 * ___mBeforeMarshallingEvent_5;
	// Amazon.Runtime.RequestEventHandler Amazon.Runtime.AmazonServiceClient::mBeforeRequestEvent
	RequestEventHandler_t2213783891 * ___mBeforeRequestEvent_6;
	// Amazon.Runtime.ResponseEventHandler Amazon.Runtime.AmazonServiceClient::mAfterResponseEvent
	ResponseEventHandler_t3870676125 * ___mAfterResponseEvent_7;
	// Amazon.Runtime.ExceptionEventHandler Amazon.Runtime.AmazonServiceClient::mExceptionEvent
	ExceptionEventHandler_t3236465969 * ___mExceptionEvent_8;
	// Amazon.Runtime.Internal.Auth.AbstractAWSSigner Amazon.Runtime.AmazonServiceClient::<Signer>k__BackingField
	AbstractAWSSigner_t2114314031 * ___U3CSignerU3Ek__BackingField_9;

public:
	inline static int32_t get_offset_of__disposed_0() { return static_cast<int32_t>(offsetof(AmazonServiceClient_t3583134838, ____disposed_0)); }
	inline bool get__disposed_0() const { return ____disposed_0; }
	inline bool* get_address_of__disposed_0() { return &____disposed_0; }
	inline void set__disposed_0(bool value)
	{
		____disposed_0 = value;
	}

	inline static int32_t get_offset_of__logger_1() { return static_cast<int32_t>(offsetof(AmazonServiceClient_t3583134838, ____logger_1)); }
	inline Logger_t2262497814 * get__logger_1() const { return ____logger_1; }
	inline Logger_t2262497814 ** get_address_of__logger_1() { return &____logger_1; }
	inline void set__logger_1(Logger_t2262497814 * value)
	{
		____logger_1 = value;
		Il2CppCodeGenWriteBarrier(&____logger_1, value);
	}

	inline static int32_t get_offset_of_U3CRuntimePipelineU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AmazonServiceClient_t3583134838, ___U3CRuntimePipelineU3Ek__BackingField_2)); }
	inline RuntimePipeline_t3355992556 * get_U3CRuntimePipelineU3Ek__BackingField_2() const { return ___U3CRuntimePipelineU3Ek__BackingField_2; }
	inline RuntimePipeline_t3355992556 ** get_address_of_U3CRuntimePipelineU3Ek__BackingField_2() { return &___U3CRuntimePipelineU3Ek__BackingField_2; }
	inline void set_U3CRuntimePipelineU3Ek__BackingField_2(RuntimePipeline_t3355992556 * value)
	{
		___U3CRuntimePipelineU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CRuntimePipelineU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CCredentialsU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AmazonServiceClient_t3583134838, ___U3CCredentialsU3Ek__BackingField_3)); }
	inline AWSCredentials_t3583921007 * get_U3CCredentialsU3Ek__BackingField_3() const { return ___U3CCredentialsU3Ek__BackingField_3; }
	inline AWSCredentials_t3583921007 ** get_address_of_U3CCredentialsU3Ek__BackingField_3() { return &___U3CCredentialsU3Ek__BackingField_3; }
	inline void set_U3CCredentialsU3Ek__BackingField_3(AWSCredentials_t3583921007 * value)
	{
		___U3CCredentialsU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCredentialsU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3CConfigU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(AmazonServiceClient_t3583134838, ___U3CConfigU3Ek__BackingField_4)); }
	inline Il2CppObject * get_U3CConfigU3Ek__BackingField_4() const { return ___U3CConfigU3Ek__BackingField_4; }
	inline Il2CppObject ** get_address_of_U3CConfigU3Ek__BackingField_4() { return &___U3CConfigU3Ek__BackingField_4; }
	inline void set_U3CConfigU3Ek__BackingField_4(Il2CppObject * value)
	{
		___U3CConfigU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CConfigU3Ek__BackingField_4, value);
	}

	inline static int32_t get_offset_of_mBeforeMarshallingEvent_5() { return static_cast<int32_t>(offsetof(AmazonServiceClient_t3583134838, ___mBeforeMarshallingEvent_5)); }
	inline PreRequestEventHandler_t345425304 * get_mBeforeMarshallingEvent_5() const { return ___mBeforeMarshallingEvent_5; }
	inline PreRequestEventHandler_t345425304 ** get_address_of_mBeforeMarshallingEvent_5() { return &___mBeforeMarshallingEvent_5; }
	inline void set_mBeforeMarshallingEvent_5(PreRequestEventHandler_t345425304 * value)
	{
		___mBeforeMarshallingEvent_5 = value;
		Il2CppCodeGenWriteBarrier(&___mBeforeMarshallingEvent_5, value);
	}

	inline static int32_t get_offset_of_mBeforeRequestEvent_6() { return static_cast<int32_t>(offsetof(AmazonServiceClient_t3583134838, ___mBeforeRequestEvent_6)); }
	inline RequestEventHandler_t2213783891 * get_mBeforeRequestEvent_6() const { return ___mBeforeRequestEvent_6; }
	inline RequestEventHandler_t2213783891 ** get_address_of_mBeforeRequestEvent_6() { return &___mBeforeRequestEvent_6; }
	inline void set_mBeforeRequestEvent_6(RequestEventHandler_t2213783891 * value)
	{
		___mBeforeRequestEvent_6 = value;
		Il2CppCodeGenWriteBarrier(&___mBeforeRequestEvent_6, value);
	}

	inline static int32_t get_offset_of_mAfterResponseEvent_7() { return static_cast<int32_t>(offsetof(AmazonServiceClient_t3583134838, ___mAfterResponseEvent_7)); }
	inline ResponseEventHandler_t3870676125 * get_mAfterResponseEvent_7() const { return ___mAfterResponseEvent_7; }
	inline ResponseEventHandler_t3870676125 ** get_address_of_mAfterResponseEvent_7() { return &___mAfterResponseEvent_7; }
	inline void set_mAfterResponseEvent_7(ResponseEventHandler_t3870676125 * value)
	{
		___mAfterResponseEvent_7 = value;
		Il2CppCodeGenWriteBarrier(&___mAfterResponseEvent_7, value);
	}

	inline static int32_t get_offset_of_mExceptionEvent_8() { return static_cast<int32_t>(offsetof(AmazonServiceClient_t3583134838, ___mExceptionEvent_8)); }
	inline ExceptionEventHandler_t3236465969 * get_mExceptionEvent_8() const { return ___mExceptionEvent_8; }
	inline ExceptionEventHandler_t3236465969 ** get_address_of_mExceptionEvent_8() { return &___mExceptionEvent_8; }
	inline void set_mExceptionEvent_8(ExceptionEventHandler_t3236465969 * value)
	{
		___mExceptionEvent_8 = value;
		Il2CppCodeGenWriteBarrier(&___mExceptionEvent_8, value);
	}

	inline static int32_t get_offset_of_U3CSignerU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(AmazonServiceClient_t3583134838, ___U3CSignerU3Ek__BackingField_9)); }
	inline AbstractAWSSigner_t2114314031 * get_U3CSignerU3Ek__BackingField_9() const { return ___U3CSignerU3Ek__BackingField_9; }
	inline AbstractAWSSigner_t2114314031 ** get_address_of_U3CSignerU3Ek__BackingField_9() { return &___U3CSignerU3Ek__BackingField_9; }
	inline void set_U3CSignerU3Ek__BackingField_9(AbstractAWSSigner_t2114314031 * value)
	{
		___U3CSignerU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CSignerU3Ek__BackingField_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
