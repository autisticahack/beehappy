﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ThirdParty.Json.LitJson.IJsonWrapper
struct IJsonWrapper_t3095378610;
// ThirdParty.Json.LitJson.WrapperFactory
struct WrapperFactory_t327905379;
// ThirdParty.Json.LitJson.JsonReader
struct JsonReader_t354941621;
// System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,ThirdParty.Json.LitJson.ImporterFunc>>
struct IDictionary_2_t723569914;
// System.Type
struct Type_t;
// ThirdParty.Json.LitJson.ImporterFunc
struct ImporterFunc_t850687278;
// ThirdParty.Json.LitJson.JsonData
struct JsonData_t4263252052;
// System.IO.TextReader
struct TextReader_t1561828458;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_WrapperFactory327905379.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_JsonReader354941621.h"
#include "mscorlib_System_Type1303803226.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_ImporterFunc850687278.h"
#include "mscorlib_System_IO_TextReader1561828458.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ThirdParty.Json.LitJson.JsonMapper::.cctor()
extern "C"  void JsonMapper__cctor_m1834007045 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ThirdParty.Json.LitJson.IJsonWrapper ThirdParty.Json.LitJson.JsonMapper::ReadValue(ThirdParty.Json.LitJson.WrapperFactory,ThirdParty.Json.LitJson.JsonReader)
extern "C"  Il2CppObject * JsonMapper_ReadValue_m2812052028 (Il2CppObject * __this /* static, unused */, WrapperFactory_t327905379 * ___factory0, JsonReader_t354941621 * ___reader1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.JsonMapper::RegisterBaseExporters()
extern "C"  void JsonMapper_RegisterBaseExporters_m2987482258 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.JsonMapper::RegisterBaseImporters()
extern "C"  void JsonMapper_RegisterBaseImporters_m2686124695 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.Json.LitJson.JsonMapper::RegisterImporter(System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,ThirdParty.Json.LitJson.ImporterFunc>>,System.Type,System.Type,ThirdParty.Json.LitJson.ImporterFunc)
extern "C"  void JsonMapper_RegisterImporter_m1039024633 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___table0, Type_t * ___json_type1, Type_t * ___value_type2, ImporterFunc_t850687278 * ___importer3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ThirdParty.Json.LitJson.JsonData ThirdParty.Json.LitJson.JsonMapper::ToObject(System.IO.TextReader)
extern "C"  JsonData_t4263252052 * JsonMapper_ToObject_m233897228 (Il2CppObject * __this /* static, unused */, TextReader_t1561828458 * ___reader0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ThirdParty.Json.LitJson.JsonData ThirdParty.Json.LitJson.JsonMapper::ToObject(System.String)
extern "C"  JsonData_t4263252052 * JsonMapper_ToObject_m515800907 (Il2CppObject * __this /* static, unused */, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ThirdParty.Json.LitJson.IJsonWrapper ThirdParty.Json.LitJson.JsonMapper::ToWrapper(ThirdParty.Json.LitJson.WrapperFactory,ThirdParty.Json.LitJson.JsonReader)
extern "C"  Il2CppObject * JsonMapper_ToWrapper_m3543010123 (Il2CppObject * __this /* static, unused */, WrapperFactory_t327905379 * ___factory0, JsonReader_t354941621 * ___reader1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ThirdParty.Json.LitJson.IJsonWrapper ThirdParty.Json.LitJson.JsonMapper::ToWrapper(ThirdParty.Json.LitJson.WrapperFactory,System.String)
extern "C"  Il2CppObject * JsonMapper_ToWrapper_m1983722396 (Il2CppObject * __this /* static, unused */, WrapperFactory_t327905379 * ___factory0, String_t* ___json1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
