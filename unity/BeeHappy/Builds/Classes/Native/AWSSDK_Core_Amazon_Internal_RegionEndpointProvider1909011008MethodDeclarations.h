﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Internal.RegionEndpointProviderV2
struct RegionEndpointProviderV2_t1909011008;
// Amazon.Internal.IRegionEndpoint
struct IRegionEndpoint_t544433888;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// Amazon.Internal.IRegionEndpoint Amazon.Internal.RegionEndpointProviderV2::GetRegionEndpoint(System.String)
extern "C"  Il2CppObject * RegionEndpointProviderV2_GetRegionEndpoint_m1954348735 (RegionEndpointProviderV2_t1909011008 * __this, String_t* ___regionName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Internal.RegionEndpointProviderV2::.ctor()
extern "C"  void RegionEndpointProviderV2__ctor_m3526217742 (RegionEndpointProviderV2_t1909011008 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
