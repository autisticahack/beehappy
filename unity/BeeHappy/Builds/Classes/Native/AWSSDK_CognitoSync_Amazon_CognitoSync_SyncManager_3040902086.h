﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// Amazon.CognitoSync.SyncManager.ILocalStorage
struct ILocalStorage_t1053993977;
// Amazon.CognitoSync.SyncManager.Internal.CognitoSyncStorage
struct CognitoSyncStorage_t101590051;
// Amazon.CognitoIdentity.CognitoAWSCredentials
struct CognitoAWSCredentials_t2370264792;
// Amazon.Runtime.Internal.Util.Logger
struct Logger_t2262497814;
// System.EventHandler`1<Amazon.CognitoSync.SyncManager.SyncSuccessEventArgs>
struct EventHandler_1_t2868688519;
// System.EventHandler`1<Amazon.CognitoSync.SyncManager.SyncFailureEventArgs>
struct EventHandler_1_t339652670;
// Amazon.CognitoSync.SyncManager.Dataset/SyncConflictDelegate
struct SyncConflictDelegate_t2745667881;
// Amazon.CognitoSync.SyncManager.Dataset/DatasetDeletedDelegate
struct DatasetDeletedDelegate_t2502617919;
// Amazon.CognitoSync.SyncManager.Dataset/DatasetMergedDelegate
struct DatasetMergedDelegate_t713795432;
// Amazon.Util.Internal.PlatformServices.INetworkReachability
struct INetworkReachability_t2670889314;
// Amazon.Runtime.AsyncOptions
struct AsyncOptions_t558351272;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.CognitoSync.SyncManager.Dataset
struct  Dataset_t3040902086  : public Il2CppObject
{
public:
	// System.String Amazon.CognitoSync.SyncManager.Dataset::_datasetName
	String_t* ____datasetName_1;
	// Amazon.CognitoSync.SyncManager.ILocalStorage Amazon.CognitoSync.SyncManager.Dataset::_local
	Il2CppObject * ____local_2;
	// Amazon.CognitoSync.SyncManager.Internal.CognitoSyncStorage Amazon.CognitoSync.SyncManager.Dataset::_remote
	CognitoSyncStorage_t101590051 * ____remote_3;
	// Amazon.CognitoIdentity.CognitoAWSCredentials Amazon.CognitoSync.SyncManager.Dataset::_cognitoCredentials
	CognitoAWSCredentials_t2370264792 * ____cognitoCredentials_4;
	// System.Boolean Amazon.CognitoSync.SyncManager.Dataset::waitingForConnectivity
	bool ___waitingForConnectivity_5;
	// System.Boolean Amazon.CognitoSync.SyncManager.Dataset::_disposed
	bool ____disposed_6;
	// Amazon.Runtime.Internal.Util.Logger Amazon.CognitoSync.SyncManager.Dataset::_logger
	Logger_t2262497814 * ____logger_7;
	// System.Boolean Amazon.CognitoSync.SyncManager.Dataset::locked
	bool ___locked_8;
	// System.Boolean Amazon.CognitoSync.SyncManager.Dataset::queuedSync
	bool ___queuedSync_9;
	// System.EventHandler`1<Amazon.CognitoSync.SyncManager.SyncSuccessEventArgs> Amazon.CognitoSync.SyncManager.Dataset::mOnSyncSuccess
	EventHandler_1_t2868688519 * ___mOnSyncSuccess_10;
	// System.EventHandler`1<Amazon.CognitoSync.SyncManager.SyncFailureEventArgs> Amazon.CognitoSync.SyncManager.Dataset::mOnSyncFailure
	EventHandler_1_t339652670 * ___mOnSyncFailure_11;
	// Amazon.CognitoSync.SyncManager.Dataset/SyncConflictDelegate Amazon.CognitoSync.SyncManager.Dataset::OnSyncConflict
	SyncConflictDelegate_t2745667881 * ___OnSyncConflict_12;
	// Amazon.CognitoSync.SyncManager.Dataset/DatasetDeletedDelegate Amazon.CognitoSync.SyncManager.Dataset::OnDatasetDeleted
	DatasetDeletedDelegate_t2502617919 * ___OnDatasetDeleted_13;
	// Amazon.CognitoSync.SyncManager.Dataset/DatasetMergedDelegate Amazon.CognitoSync.SyncManager.Dataset::OnDatasetMerged
	DatasetMergedDelegate_t713795432 * ___OnDatasetMerged_14;
	// Amazon.Util.Internal.PlatformServices.INetworkReachability Amazon.CognitoSync.SyncManager.Dataset::_netReachability
	Il2CppObject * ____netReachability_15;
	// Amazon.Runtime.AsyncOptions Amazon.CognitoSync.SyncManager.Dataset::OnConnectivityOptions
	AsyncOptions_t558351272 * ___OnConnectivityOptions_16;

public:
	inline static int32_t get_offset_of__datasetName_1() { return static_cast<int32_t>(offsetof(Dataset_t3040902086, ____datasetName_1)); }
	inline String_t* get__datasetName_1() const { return ____datasetName_1; }
	inline String_t** get_address_of__datasetName_1() { return &____datasetName_1; }
	inline void set__datasetName_1(String_t* value)
	{
		____datasetName_1 = value;
		Il2CppCodeGenWriteBarrier(&____datasetName_1, value);
	}

	inline static int32_t get_offset_of__local_2() { return static_cast<int32_t>(offsetof(Dataset_t3040902086, ____local_2)); }
	inline Il2CppObject * get__local_2() const { return ____local_2; }
	inline Il2CppObject ** get_address_of__local_2() { return &____local_2; }
	inline void set__local_2(Il2CppObject * value)
	{
		____local_2 = value;
		Il2CppCodeGenWriteBarrier(&____local_2, value);
	}

	inline static int32_t get_offset_of__remote_3() { return static_cast<int32_t>(offsetof(Dataset_t3040902086, ____remote_3)); }
	inline CognitoSyncStorage_t101590051 * get__remote_3() const { return ____remote_3; }
	inline CognitoSyncStorage_t101590051 ** get_address_of__remote_3() { return &____remote_3; }
	inline void set__remote_3(CognitoSyncStorage_t101590051 * value)
	{
		____remote_3 = value;
		Il2CppCodeGenWriteBarrier(&____remote_3, value);
	}

	inline static int32_t get_offset_of__cognitoCredentials_4() { return static_cast<int32_t>(offsetof(Dataset_t3040902086, ____cognitoCredentials_4)); }
	inline CognitoAWSCredentials_t2370264792 * get__cognitoCredentials_4() const { return ____cognitoCredentials_4; }
	inline CognitoAWSCredentials_t2370264792 ** get_address_of__cognitoCredentials_4() { return &____cognitoCredentials_4; }
	inline void set__cognitoCredentials_4(CognitoAWSCredentials_t2370264792 * value)
	{
		____cognitoCredentials_4 = value;
		Il2CppCodeGenWriteBarrier(&____cognitoCredentials_4, value);
	}

	inline static int32_t get_offset_of_waitingForConnectivity_5() { return static_cast<int32_t>(offsetof(Dataset_t3040902086, ___waitingForConnectivity_5)); }
	inline bool get_waitingForConnectivity_5() const { return ___waitingForConnectivity_5; }
	inline bool* get_address_of_waitingForConnectivity_5() { return &___waitingForConnectivity_5; }
	inline void set_waitingForConnectivity_5(bool value)
	{
		___waitingForConnectivity_5 = value;
	}

	inline static int32_t get_offset_of__disposed_6() { return static_cast<int32_t>(offsetof(Dataset_t3040902086, ____disposed_6)); }
	inline bool get__disposed_6() const { return ____disposed_6; }
	inline bool* get_address_of__disposed_6() { return &____disposed_6; }
	inline void set__disposed_6(bool value)
	{
		____disposed_6 = value;
	}

	inline static int32_t get_offset_of__logger_7() { return static_cast<int32_t>(offsetof(Dataset_t3040902086, ____logger_7)); }
	inline Logger_t2262497814 * get__logger_7() const { return ____logger_7; }
	inline Logger_t2262497814 ** get_address_of__logger_7() { return &____logger_7; }
	inline void set__logger_7(Logger_t2262497814 * value)
	{
		____logger_7 = value;
		Il2CppCodeGenWriteBarrier(&____logger_7, value);
	}

	inline static int32_t get_offset_of_locked_8() { return static_cast<int32_t>(offsetof(Dataset_t3040902086, ___locked_8)); }
	inline bool get_locked_8() const { return ___locked_8; }
	inline bool* get_address_of_locked_8() { return &___locked_8; }
	inline void set_locked_8(bool value)
	{
		___locked_8 = value;
	}

	inline static int32_t get_offset_of_queuedSync_9() { return static_cast<int32_t>(offsetof(Dataset_t3040902086, ___queuedSync_9)); }
	inline bool get_queuedSync_9() const { return ___queuedSync_9; }
	inline bool* get_address_of_queuedSync_9() { return &___queuedSync_9; }
	inline void set_queuedSync_9(bool value)
	{
		___queuedSync_9 = value;
	}

	inline static int32_t get_offset_of_mOnSyncSuccess_10() { return static_cast<int32_t>(offsetof(Dataset_t3040902086, ___mOnSyncSuccess_10)); }
	inline EventHandler_1_t2868688519 * get_mOnSyncSuccess_10() const { return ___mOnSyncSuccess_10; }
	inline EventHandler_1_t2868688519 ** get_address_of_mOnSyncSuccess_10() { return &___mOnSyncSuccess_10; }
	inline void set_mOnSyncSuccess_10(EventHandler_1_t2868688519 * value)
	{
		___mOnSyncSuccess_10 = value;
		Il2CppCodeGenWriteBarrier(&___mOnSyncSuccess_10, value);
	}

	inline static int32_t get_offset_of_mOnSyncFailure_11() { return static_cast<int32_t>(offsetof(Dataset_t3040902086, ___mOnSyncFailure_11)); }
	inline EventHandler_1_t339652670 * get_mOnSyncFailure_11() const { return ___mOnSyncFailure_11; }
	inline EventHandler_1_t339652670 ** get_address_of_mOnSyncFailure_11() { return &___mOnSyncFailure_11; }
	inline void set_mOnSyncFailure_11(EventHandler_1_t339652670 * value)
	{
		___mOnSyncFailure_11 = value;
		Il2CppCodeGenWriteBarrier(&___mOnSyncFailure_11, value);
	}

	inline static int32_t get_offset_of_OnSyncConflict_12() { return static_cast<int32_t>(offsetof(Dataset_t3040902086, ___OnSyncConflict_12)); }
	inline SyncConflictDelegate_t2745667881 * get_OnSyncConflict_12() const { return ___OnSyncConflict_12; }
	inline SyncConflictDelegate_t2745667881 ** get_address_of_OnSyncConflict_12() { return &___OnSyncConflict_12; }
	inline void set_OnSyncConflict_12(SyncConflictDelegate_t2745667881 * value)
	{
		___OnSyncConflict_12 = value;
		Il2CppCodeGenWriteBarrier(&___OnSyncConflict_12, value);
	}

	inline static int32_t get_offset_of_OnDatasetDeleted_13() { return static_cast<int32_t>(offsetof(Dataset_t3040902086, ___OnDatasetDeleted_13)); }
	inline DatasetDeletedDelegate_t2502617919 * get_OnDatasetDeleted_13() const { return ___OnDatasetDeleted_13; }
	inline DatasetDeletedDelegate_t2502617919 ** get_address_of_OnDatasetDeleted_13() { return &___OnDatasetDeleted_13; }
	inline void set_OnDatasetDeleted_13(DatasetDeletedDelegate_t2502617919 * value)
	{
		___OnDatasetDeleted_13 = value;
		Il2CppCodeGenWriteBarrier(&___OnDatasetDeleted_13, value);
	}

	inline static int32_t get_offset_of_OnDatasetMerged_14() { return static_cast<int32_t>(offsetof(Dataset_t3040902086, ___OnDatasetMerged_14)); }
	inline DatasetMergedDelegate_t713795432 * get_OnDatasetMerged_14() const { return ___OnDatasetMerged_14; }
	inline DatasetMergedDelegate_t713795432 ** get_address_of_OnDatasetMerged_14() { return &___OnDatasetMerged_14; }
	inline void set_OnDatasetMerged_14(DatasetMergedDelegate_t713795432 * value)
	{
		___OnDatasetMerged_14 = value;
		Il2CppCodeGenWriteBarrier(&___OnDatasetMerged_14, value);
	}

	inline static int32_t get_offset_of__netReachability_15() { return static_cast<int32_t>(offsetof(Dataset_t3040902086, ____netReachability_15)); }
	inline Il2CppObject * get__netReachability_15() const { return ____netReachability_15; }
	inline Il2CppObject ** get_address_of__netReachability_15() { return &____netReachability_15; }
	inline void set__netReachability_15(Il2CppObject * value)
	{
		____netReachability_15 = value;
		Il2CppCodeGenWriteBarrier(&____netReachability_15, value);
	}

	inline static int32_t get_offset_of_OnConnectivityOptions_16() { return static_cast<int32_t>(offsetof(Dataset_t3040902086, ___OnConnectivityOptions_16)); }
	inline AsyncOptions_t558351272 * get_OnConnectivityOptions_16() const { return ___OnConnectivityOptions_16; }
	inline AsyncOptions_t558351272 ** get_address_of_OnConnectivityOptions_16() { return &___OnConnectivityOptions_16; }
	inline void set_OnConnectivityOptions_16(AsyncOptions_t558351272 * value)
	{
		___OnConnectivityOptions_16 = value;
		Il2CppCodeGenWriteBarrier(&___OnConnectivityOptions_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
