﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.AnonymousAWSCredentials
struct AnonymousAWSCredentials_t3877662854;
// Amazon.Runtime.ImmutableCredentials
struct ImmutableCredentials_t282353664;

#include "codegen/il2cpp-codegen.h"

// Amazon.Runtime.ImmutableCredentials Amazon.Runtime.AnonymousAWSCredentials::GetCredentials()
extern "C"  ImmutableCredentials_t282353664 * AnonymousAWSCredentials_GetCredentials_m1532160418 (AnonymousAWSCredentials_t3877662854 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.AnonymousAWSCredentials::.ctor()
extern "C"  void AnonymousAWSCredentials__ctor_m211307267 (AnonymousAWSCredentials_t3877662854 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
