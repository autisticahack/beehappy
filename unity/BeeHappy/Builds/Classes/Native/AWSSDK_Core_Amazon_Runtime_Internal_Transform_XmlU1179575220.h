﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Xml.XmlReaderSettings
struct XmlReaderSettings_t1578612233;
// System.Collections.Generic.HashSet`1<System.Xml.XmlNodeType>
struct HashSet_1_t3367932747;
// System.IO.StreamReader
struct StreamReader_t2360341767;
// System.Xml.XmlReader
struct XmlReader_t3675626668;
// System.Collections.Generic.Stack`1<System.String>
struct Stack_1_t3116948387;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// System.Collections.Generic.IEnumerator`1<System.String>
struct IEnumerator_1_t3799711356;

#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_Unma1608322995.h"
#include "System_Xml_System_Xml_XmlNodeType739504597.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext
struct  XmlUnmarshallerContext_t1179575220  : public UnmarshallerContext_t1608322995
{
public:
	// System.IO.StreamReader Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::streamReader
	StreamReader_t2360341767 * ___streamReader_8;
	// System.Xml.XmlReader Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::_xmlReader
	XmlReader_t3675626668 * ____xmlReader_9;
	// System.Collections.Generic.Stack`1<System.String> Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::stack
	Stack_1_t3116948387 * ___stack_10;
	// System.String Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::stackString
	String_t* ___stackString_11;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::attributeValues
	Dictionary_2_t3943999495 * ___attributeValues_12;
	// System.Collections.Generic.List`1<System.String> Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::attributeNames
	List_1_t1398341365 * ___attributeNames_13;
	// System.Collections.Generic.IEnumerator`1<System.String> Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::attributeEnumerator
	Il2CppObject* ___attributeEnumerator_14;
	// System.Xml.XmlNodeType Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::nodeType
	int32_t ___nodeType_15;
	// System.String Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::nodeContent
	String_t* ___nodeContent_16;
	// System.Boolean Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::disposed
	bool ___disposed_17;

public:
	inline static int32_t get_offset_of_streamReader_8() { return static_cast<int32_t>(offsetof(XmlUnmarshallerContext_t1179575220, ___streamReader_8)); }
	inline StreamReader_t2360341767 * get_streamReader_8() const { return ___streamReader_8; }
	inline StreamReader_t2360341767 ** get_address_of_streamReader_8() { return &___streamReader_8; }
	inline void set_streamReader_8(StreamReader_t2360341767 * value)
	{
		___streamReader_8 = value;
		Il2CppCodeGenWriteBarrier(&___streamReader_8, value);
	}

	inline static int32_t get_offset_of__xmlReader_9() { return static_cast<int32_t>(offsetof(XmlUnmarshallerContext_t1179575220, ____xmlReader_9)); }
	inline XmlReader_t3675626668 * get__xmlReader_9() const { return ____xmlReader_9; }
	inline XmlReader_t3675626668 ** get_address_of__xmlReader_9() { return &____xmlReader_9; }
	inline void set__xmlReader_9(XmlReader_t3675626668 * value)
	{
		____xmlReader_9 = value;
		Il2CppCodeGenWriteBarrier(&____xmlReader_9, value);
	}

	inline static int32_t get_offset_of_stack_10() { return static_cast<int32_t>(offsetof(XmlUnmarshallerContext_t1179575220, ___stack_10)); }
	inline Stack_1_t3116948387 * get_stack_10() const { return ___stack_10; }
	inline Stack_1_t3116948387 ** get_address_of_stack_10() { return &___stack_10; }
	inline void set_stack_10(Stack_1_t3116948387 * value)
	{
		___stack_10 = value;
		Il2CppCodeGenWriteBarrier(&___stack_10, value);
	}

	inline static int32_t get_offset_of_stackString_11() { return static_cast<int32_t>(offsetof(XmlUnmarshallerContext_t1179575220, ___stackString_11)); }
	inline String_t* get_stackString_11() const { return ___stackString_11; }
	inline String_t** get_address_of_stackString_11() { return &___stackString_11; }
	inline void set_stackString_11(String_t* value)
	{
		___stackString_11 = value;
		Il2CppCodeGenWriteBarrier(&___stackString_11, value);
	}

	inline static int32_t get_offset_of_attributeValues_12() { return static_cast<int32_t>(offsetof(XmlUnmarshallerContext_t1179575220, ___attributeValues_12)); }
	inline Dictionary_2_t3943999495 * get_attributeValues_12() const { return ___attributeValues_12; }
	inline Dictionary_2_t3943999495 ** get_address_of_attributeValues_12() { return &___attributeValues_12; }
	inline void set_attributeValues_12(Dictionary_2_t3943999495 * value)
	{
		___attributeValues_12 = value;
		Il2CppCodeGenWriteBarrier(&___attributeValues_12, value);
	}

	inline static int32_t get_offset_of_attributeNames_13() { return static_cast<int32_t>(offsetof(XmlUnmarshallerContext_t1179575220, ___attributeNames_13)); }
	inline List_1_t1398341365 * get_attributeNames_13() const { return ___attributeNames_13; }
	inline List_1_t1398341365 ** get_address_of_attributeNames_13() { return &___attributeNames_13; }
	inline void set_attributeNames_13(List_1_t1398341365 * value)
	{
		___attributeNames_13 = value;
		Il2CppCodeGenWriteBarrier(&___attributeNames_13, value);
	}

	inline static int32_t get_offset_of_attributeEnumerator_14() { return static_cast<int32_t>(offsetof(XmlUnmarshallerContext_t1179575220, ___attributeEnumerator_14)); }
	inline Il2CppObject* get_attributeEnumerator_14() const { return ___attributeEnumerator_14; }
	inline Il2CppObject** get_address_of_attributeEnumerator_14() { return &___attributeEnumerator_14; }
	inline void set_attributeEnumerator_14(Il2CppObject* value)
	{
		___attributeEnumerator_14 = value;
		Il2CppCodeGenWriteBarrier(&___attributeEnumerator_14, value);
	}

	inline static int32_t get_offset_of_nodeType_15() { return static_cast<int32_t>(offsetof(XmlUnmarshallerContext_t1179575220, ___nodeType_15)); }
	inline int32_t get_nodeType_15() const { return ___nodeType_15; }
	inline int32_t* get_address_of_nodeType_15() { return &___nodeType_15; }
	inline void set_nodeType_15(int32_t value)
	{
		___nodeType_15 = value;
	}

	inline static int32_t get_offset_of_nodeContent_16() { return static_cast<int32_t>(offsetof(XmlUnmarshallerContext_t1179575220, ___nodeContent_16)); }
	inline String_t* get_nodeContent_16() const { return ___nodeContent_16; }
	inline String_t** get_address_of_nodeContent_16() { return &___nodeContent_16; }
	inline void set_nodeContent_16(String_t* value)
	{
		___nodeContent_16 = value;
		Il2CppCodeGenWriteBarrier(&___nodeContent_16, value);
	}

	inline static int32_t get_offset_of_disposed_17() { return static_cast<int32_t>(offsetof(XmlUnmarshallerContext_t1179575220, ___disposed_17)); }
	inline bool get_disposed_17() const { return ___disposed_17; }
	inline bool* get_address_of_disposed_17() { return &___disposed_17; }
	inline void set_disposed_17(bool value)
	{
		___disposed_17 = value;
	}
};

struct XmlUnmarshallerContext_t1179575220_StaticFields
{
public:
	// System.Xml.XmlReaderSettings Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::READER_SETTINGS
	XmlReaderSettings_t1578612233 * ___READER_SETTINGS_6;
	// System.Collections.Generic.HashSet`1<System.Xml.XmlNodeType> Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::nodesToSkip
	HashSet_1_t3367932747 * ___nodesToSkip_7;

public:
	inline static int32_t get_offset_of_READER_SETTINGS_6() { return static_cast<int32_t>(offsetof(XmlUnmarshallerContext_t1179575220_StaticFields, ___READER_SETTINGS_6)); }
	inline XmlReaderSettings_t1578612233 * get_READER_SETTINGS_6() const { return ___READER_SETTINGS_6; }
	inline XmlReaderSettings_t1578612233 ** get_address_of_READER_SETTINGS_6() { return &___READER_SETTINGS_6; }
	inline void set_READER_SETTINGS_6(XmlReaderSettings_t1578612233 * value)
	{
		___READER_SETTINGS_6 = value;
		Il2CppCodeGenWriteBarrier(&___READER_SETTINGS_6, value);
	}

	inline static int32_t get_offset_of_nodesToSkip_7() { return static_cast<int32_t>(offsetof(XmlUnmarshallerContext_t1179575220_StaticFields, ___nodesToSkip_7)); }
	inline HashSet_1_t3367932747 * get_nodesToSkip_7() const { return ___nodesToSkip_7; }
	inline HashSet_1_t3367932747 ** get_address_of_nodesToSkip_7() { return &___nodesToSkip_7; }
	inline void set_nodesToSkip_7(HashSet_1_t3367932747 * value)
	{
		___nodesToSkip_7 = value;
		Il2CppCodeGenWriteBarrier(&___nodesToSkip_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
