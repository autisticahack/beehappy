﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AWSSDK_Core_Amazon_Runtime_AmazonWebServiceRequest3384026212.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.CognitoIdentity.AmazonCognitoIdentityRequest
struct  AmazonCognitoIdentityRequest_t492659996  : public AmazonWebServiceRequest_t3384026212
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
