﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.Linq.XContainer/<Elements>c__Iterator1F
struct U3CElementsU3Ec__Iterator1F_t3355856435;
// System.Xml.Linq.XElement
struct XElement_t553821050;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Xml.Linq.XElement>
struct IEnumerator_1_t2324312173;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Xml.Linq.XContainer/<Elements>c__Iterator1F::.ctor()
extern "C"  void U3CElementsU3Ec__Iterator1F__ctor_m3175455302 (U3CElementsU3Ec__Iterator1F_t3355856435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Linq.XElement System.Xml.Linq.XContainer/<Elements>c__Iterator1F::System.Collections.Generic.IEnumerator<System.Xml.Linq.XElement>.get_Current()
extern "C"  XElement_t553821050 * U3CElementsU3Ec__Iterator1F_System_Collections_Generic_IEnumeratorU3CSystem_Xml_Linq_XElementU3E_get_Current_m2433454917 (U3CElementsU3Ec__Iterator1F_t3355856435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Xml.Linq.XContainer/<Elements>c__Iterator1F::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CElementsU3Ec__Iterator1F_System_Collections_IEnumerator_get_Current_m2723591646 (U3CElementsU3Ec__Iterator1F_t3355856435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Xml.Linq.XContainer/<Elements>c__Iterator1F::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CElementsU3Ec__Iterator1F_System_Collections_IEnumerable_GetEnumerator_m1459653753 (U3CElementsU3Ec__Iterator1F_t3355856435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.Xml.Linq.XElement> System.Xml.Linq.XContainer/<Elements>c__Iterator1F::System.Collections.Generic.IEnumerable<System.Xml.Linq.XElement>.GetEnumerator()
extern "C"  Il2CppObject* U3CElementsU3Ec__Iterator1F_System_Collections_Generic_IEnumerableU3CSystem_Xml_Linq_XElementU3E_GetEnumerator_m1986683062 (U3CElementsU3Ec__Iterator1F_t3355856435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.Linq.XContainer/<Elements>c__Iterator1F::MoveNext()
extern "C"  bool U3CElementsU3Ec__Iterator1F_MoveNext_m2769662402 (U3CElementsU3Ec__Iterator1F_t3355856435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Linq.XContainer/<Elements>c__Iterator1F::Dispose()
extern "C"  void U3CElementsU3Ec__Iterator1F_Dispose_m3766188423 (U3CElementsU3Ec__Iterator1F_t3355856435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Linq.XContainer/<Elements>c__Iterator1F::Reset()
extern "C"  void U3CElementsU3Ec__Iterator1F_Reset_m4008773469 (U3CElementsU3Ec__Iterator1F_t3355856435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
