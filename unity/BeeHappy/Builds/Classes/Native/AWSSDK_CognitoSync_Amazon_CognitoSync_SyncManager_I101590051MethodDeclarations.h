﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.CognitoSync.SyncManager.Internal.CognitoSyncStorage
struct CognitoSyncStorage_t101590051;
// Amazon.CognitoIdentity.CognitoAWSCredentials
struct CognitoAWSCredentials_t2370264792;
// Amazon.CognitoSync.AmazonCognitoSyncConfig
struct AmazonCognitoSyncConfig_t1479139240;
// System.String
struct String_t;
// Amazon.CognitoSync.Model.RecordPatch
struct RecordPatch_t97905615;
// Amazon.CognitoSync.SyncManager.Record
struct Record_t868799569;
// Amazon.CognitoSync.Model.Record
struct Record_t441586865;
// Amazon.CognitoSync.SyncManager.SyncManagerException
struct SyncManagerException_t4165705311;
// System.Exception
struct Exception_t1927440687;
// Amazon.Runtime.AmazonServiceException
struct AmazonServiceException_t3748559634;
// Amazon.CognitoSync.SyncManager.DatasetUpdates
struct DatasetUpdates_t2321405236;
// System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.Record>
struct List_1_t237920701;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_CognitoIdentity_Amazon_CognitoIdentity_Cogn2370264792.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_AmazonCognit1479139240.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_SyncManager_R868799569.h"
#include "AWSSDK_CognitoSync_Amazon_CognitoSync_Model_Record441586865.h"
#include "mscorlib_System_Exception1927440687.h"
#include "mscorlib_System_String2029220233.h"
#include "AWSSDK_Core_Amazon_Runtime_AmazonServiceException3748559634.h"

// System.Void Amazon.CognitoSync.SyncManager.Internal.CognitoSyncStorage::Dispose()
extern "C"  void CognitoSyncStorage_Dispose_m1972168175 (CognitoSyncStorage_t101590051 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.SyncManager.Internal.CognitoSyncStorage::Dispose(System.Boolean)
extern "C"  void CognitoSyncStorage_Dispose_m921351194 (CognitoSyncStorage_t101590051 * __this, bool ___disposing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.SyncManager.Internal.CognitoSyncStorage::.ctor(Amazon.CognitoIdentity.CognitoAWSCredentials,Amazon.CognitoSync.AmazonCognitoSyncConfig)
extern "C"  void CognitoSyncStorage__ctor_m3416472071 (CognitoSyncStorage_t101590051 * __this, CognitoAWSCredentials_t2370264792 * ___cognitoCredentials0, AmazonCognitoSyncConfig_t1479139240 * ___config1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.CognitoSync.SyncManager.Internal.CognitoSyncStorage::GetCurrentIdentityId()
extern "C"  String_t* CognitoSyncStorage_GetCurrentIdentityId_m4195956693 (CognitoSyncStorage_t101590051 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.CognitoSync.Model.RecordPatch Amazon.CognitoSync.SyncManager.Internal.CognitoSyncStorage::RecordToPatch(Amazon.CognitoSync.SyncManager.Record)
extern "C"  RecordPatch_t97905615 * CognitoSyncStorage_RecordToPatch_m1873300722 (Il2CppObject * __this /* static, unused */, Record_t868799569 * ___record0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.CognitoSync.SyncManager.Record Amazon.CognitoSync.SyncManager.Internal.CognitoSyncStorage::ModelToRecord(Amazon.CognitoSync.Model.Record)
extern "C"  Record_t868799569 * CognitoSyncStorage_ModelToRecord_m2657689069 (Il2CppObject * __this /* static, unused */, Record_t441586865 * ___model0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.CognitoSync.SyncManager.SyncManagerException Amazon.CognitoSync.SyncManager.Internal.CognitoSyncStorage::HandleException(System.Exception,System.String)
extern "C"  SyncManagerException_t4165705311 * CognitoSyncStorage_HandleException_m971634219 (Il2CppObject * __this /* static, unused */, Exception_t1927440687 * ___e0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Amazon.CognitoSync.SyncManager.Internal.CognitoSyncStorage::IsNetworkException(Amazon.Runtime.AmazonServiceException)
extern "C"  bool CognitoSyncStorage_IsNetworkException_m4091219969 (Il2CppObject * __this /* static, unused */, AmazonServiceException_t3748559634 * ___ase0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.CognitoSync.SyncManager.DatasetUpdates Amazon.CognitoSync.SyncManager.Internal.CognitoSyncStorage::ListUpdates(System.String,System.Int64)
extern "C"  DatasetUpdates_t2321405236 * CognitoSyncStorage_ListUpdates_m2801668975 (CognitoSyncStorage_t101590051 * __this, String_t* ___datasetName0, int64_t ___lastSyncCount1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.CognitoSync.SyncManager.DatasetUpdates Amazon.CognitoSync.SyncManager.Internal.CognitoSyncStorage::PopulateListUpdates(System.String,System.Int64,System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.Record>,System.String)
extern "C"  DatasetUpdates_t2321405236 * CognitoSyncStorage_PopulateListUpdates_m1458461406 (CognitoSyncStorage_t101590051 * __this, String_t* ___datasetName0, int64_t ___lastSyncCount1, List_1_t237920701 * ___records2, String_t* ___nextToken3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.Record> Amazon.CognitoSync.SyncManager.Internal.CognitoSyncStorage::PutRecords(System.String,System.Collections.Generic.List`1<Amazon.CognitoSync.SyncManager.Record>,System.String)
extern "C"  List_1_t237920701 * CognitoSyncStorage_PutRecords_m1294439544 (CognitoSyncStorage_t101590051 * __this, String_t* ___datasetName0, List_1_t237920701 * ___records1, String_t* ___syncSessionToken2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.CognitoSync.SyncManager.Internal.CognitoSyncStorage::DeleteDataset(System.String)
extern "C"  void CognitoSyncStorage_DeleteDataset_m4118568549 (CognitoSyncStorage_t101590051 * __this, String_t* ___datasetName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
