﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.Runtime.Internal.Util.ILogger
struct ILogger_t676853571;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.ExceptionHandler`1<System.Object>
struct  ExceptionHandler_1_t1695515460  : public Il2CppObject
{
public:
	// Amazon.Runtime.Internal.Util.ILogger Amazon.Runtime.Internal.ExceptionHandler`1::_logger
	Il2CppObject * ____logger_0;

public:
	inline static int32_t get_offset_of__logger_0() { return static_cast<int32_t>(offsetof(ExceptionHandler_1_t1695515460, ____logger_0)); }
	inline Il2CppObject * get__logger_0() const { return ____logger_0; }
	inline Il2CppObject ** get_address_of__logger_0() { return &____logger_0; }
	inline void set__logger_0(Il2CppObject * value)
	{
		____logger_0 = value;
		Il2CppCodeGenWriteBarrier(&____logger_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
