﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AWSSDK_SecurityToken_Amazon_SecurityToken_AmazonSe3771342025.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.SecurityToken.Model.InvalidIdentityTokenException
struct  InvalidIdentityTokenException_t1321611201  : public AmazonSecurityTokenServiceException_t3771342025
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
