﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ThirdParty.iOS4Unity.Runtime/<>c
struct U3CU3Ec_t88230275;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr2504060609.h"

// System.Void ThirdParty.iOS4Unity.Runtime/<>c::.cctor()
extern "C"  void U3CU3Ec__cctor_m3273718101 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.iOS4Unity.Runtime/<>c::.ctor()
extern "C"  void U3CU3Ec__ctor_m2285424738 (U3CU3Ec_t88230275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ThirdParty.iOS4Unity.Runtime/<>c::<.cctor>b__9_0(System.IntPtr)
extern "C"  Il2CppObject * U3CU3Ec_U3C_cctorU3Eb__9_0_m3568427932 (U3CU3Ec_t88230275 * __this, IntPtr_t ___h0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ThirdParty.iOS4Unity.Runtime/<>c::<.cctor>b__9_1(System.IntPtr)
extern "C"  Il2CppObject * U3CU3Ec_U3C_cctorU3Eb__9_1_m4216144417 (U3CU3Ec_t88230275 * __this, IntPtr_t ___h0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ThirdParty.iOS4Unity.Runtime/<>c::<.cctor>b__9_2(System.IntPtr)
extern "C"  Il2CppObject * U3CU3Ec_U3C_cctorU3Eb__9_2_m2795754898 (U3CU3Ec_t88230275 * __this, IntPtr_t ___h0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ThirdParty.iOS4Unity.Runtime/<>c::<.cctor>b__9_3(System.IntPtr)
extern "C"  Il2CppObject * U3CU3Ec_U3C_cctorU3Eb__9_3_m1957941015 (U3CU3Ec_t88230275 * __this, IntPtr_t ___h0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ThirdParty.iOS4Unity.Runtime/<>c::<.cctor>b__9_4(System.IntPtr)
extern "C"  Il2CppObject * U3CU3Ec_U3C_cctorU3Eb__9_4_m3673782408 (U3CU3Ec_t88230275 * __this, IntPtr_t ___h0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ThirdParty.iOS4Unity.Runtime/<>c::<.cctor>b__9_5(System.IntPtr)
extern "C"  Il2CppObject * U3CU3Ec_U3C_cctorU3Eb__9_5_m26531597 (U3CU3Ec_t88230275 * __this, IntPtr_t ___h0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ThirdParty.iOS4Unity.Runtime/<>c::<.cctor>b__9_6(System.IntPtr)
extern "C"  Il2CppObject * U3CU3Ec_U3C_cctorU3Eb__9_6_m2901109374 (U3CU3Ec_t88230275 * __this, IntPtr_t ___h0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ThirdParty.iOS4Unity.Runtime/<>c::<.cctor>b__9_7(System.IntPtr)
extern "C"  Il2CppObject * U3CU3Ec_U3C_cctorU3Eb__9_7_m2063295491 (U3CU3Ec_t88230275 * __this, IntPtr_t ___h0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ThirdParty.iOS4Unity.Runtime/<>c::<.cctor>b__9_8(System.IntPtr)
extern "C"  Il2CppObject * U3CU3Ec_U3C_cctorU3Eb__9_8_m2046416836 (U3CU3Ec_t88230275 * __this, IntPtr_t ___h0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ThirdParty.iOS4Unity.Runtime/<>c::<.cctor>b__9_9(System.IntPtr)
extern "C"  Il2CppObject * U3CU3Ec_U3C_cctorU3Eb__9_9_m2694133321 (U3CU3Ec_t88230275 * __this, IntPtr_t ___h0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ThirdParty.iOS4Unity.Runtime/<>c::<.cctor>b__9_10(System.IntPtr)
extern "C"  Il2CppObject * U3CU3Ec_U3C_cctorU3Eb__9_10_m3153786313 (U3CU3Ec_t88230275 * __this, IntPtr_t ___h0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ThirdParty.iOS4Unity.Runtime/<>c::<.cctor>b__9_11(System.IntPtr)
extern "C"  Il2CppObject * U3CU3Ec_U3C_cctorU3Eb__9_11_m2435370920 (U3CU3Ec_t88230275 * __this, IntPtr_t ___h0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ThirdParty.iOS4Unity.Runtime/<>c::<.cctor>b__9_12(System.IntPtr)
extern "C"  Il2CppObject * U3CU3Ec_U3C_cctorU3Eb__9_12_m2279668747 (U3CU3Ec_t88230275 * __this, IntPtr_t ___h0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ThirdParty.iOS4Unity.Runtime/<>c::<.cctor>b__9_13(System.IntPtr)
extern "C"  Il2CppObject * U3CU3Ec_U3C_cctorU3Eb__9_13_m1768446186 (U3CU3Ec_t88230275 * __this, IntPtr_t ___h0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ThirdParty.iOS4Unity.Runtime/<>c::<.cctor>b__9_14(System.IntPtr)
extern "C"  Il2CppObject * U3CU3Ec_U3C_cctorU3Eb__9_14_m192947533 (U3CU3Ec_t88230275 * __this, IntPtr_t ___h0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ThirdParty.iOS4Unity.Runtime/<>c::<.cctor>b__9_15(System.IntPtr)
extern "C"  Il2CppObject * U3CU3Ec_U3C_cctorU3Eb__9_15_m3769499436 (U3CU3Ec_t88230275 * __this, IntPtr_t ___h0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ThirdParty.iOS4Unity.Runtime/<>c::<.cctor>b__9_16(System.IntPtr)
extern "C"  Il2CppObject * U3CU3Ec_U3C_cctorU3Eb__9_16_m3613797263 (U3CU3Ec_t88230275 * __this, IntPtr_t ___h0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ThirdParty.iOS4Unity.Runtime/<>c::<.cctor>b__9_17(System.IntPtr)
extern "C"  Il2CppObject * U3CU3Ec_U3C_cctorU3Eb__9_17_m3102574702 (U3CU3Ec_t88230275 * __this, IntPtr_t ___h0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ThirdParty.iOS4Unity.Runtime/<>c::<.cctor>b__9_18(System.IntPtr)
extern "C"  Il2CppObject * U3CU3Ec_U3C_cctorU3Eb__9_18_m503388881 (U3CU3Ec_t88230275 * __this, IntPtr_t ___h0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ThirdParty.iOS4Unity.Runtime/<>c::<.cctor>b__9_19(System.IntPtr)
extern "C"  Il2CppObject * U3CU3Ec_U3C_cctorU3Eb__9_19_m4079940784 (U3CU3Ec_t88230275 * __this, IntPtr_t ___h0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ThirdParty.iOS4Unity.Runtime/<>c::<.cctor>b__9_20(System.IntPtr)
extern "C"  Il2CppObject * U3CU3Ec_U3C_cctorU3Eb__9_20_m2617430392 (U3CU3Ec_t88230275 * __this, IntPtr_t ___h0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ThirdParty.iOS4Unity.Runtime/<>c::<.cctor>b__9_21(System.IntPtr)
extern "C"  Il2CppObject * U3CU3Ec_U3C_cctorU3Eb__9_21_m1899014999 (U3CU3Ec_t88230275 * __this, IntPtr_t ___h0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ThirdParty.iOS4Unity.Runtime/<>c::<.cctor>b__9_22(System.IntPtr)
extern "C"  Il2CppObject * U3CU3Ec_U3C_cctorU3Eb__9_22_m1743312826 (U3CU3Ec_t88230275 * __this, IntPtr_t ___h0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ThirdParty.iOS4Unity.Runtime/<>c::<.cctor>b__9_23(System.IntPtr)
extern "C"  Il2CppObject * U3CU3Ec_U3C_cctorU3Eb__9_23_m1232090265 (U3CU3Ec_t88230275 * __this, IntPtr_t ___h0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ThirdParty.iOS4Unity.Runtime/<>c::<.cctor>b__9_24(System.IntPtr)
extern "C"  Il2CppObject * U3CU3Ec_U3C_cctorU3Eb__9_24_m3951558908 (U3CU3Ec_t88230275 * __this, IntPtr_t ___h0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ThirdParty.iOS4Unity.Runtime/<>c::<.cctor>b__9_25(System.IntPtr)
extern "C"  Il2CppObject * U3CU3Ec_U3C_cctorU3Eb__9_25_m3233143515 (U3CU3Ec_t88230275 * __this, IntPtr_t ___h0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ThirdParty.iOS4Unity.Runtime/<>c::<.cctor>b__9_26(System.IntPtr)
extern "C"  Il2CppObject * U3CU3Ec_U3C_cctorU3Eb__9_26_m3077441342 (U3CU3Ec_t88230275 * __this, IntPtr_t ___h0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ThirdParty.iOS4Unity.Runtime/<>c::<.cctor>b__9_27(System.IntPtr)
extern "C"  Il2CppObject * U3CU3Ec_U3C_cctorU3Eb__9_27_m2566218781 (U3CU3Ec_t88230275 * __this, IntPtr_t ___h0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ThirdParty.iOS4Unity.Runtime/<>c::<.cctor>b__9_28(System.IntPtr)
extern "C"  Il2CppObject * U3CU3Ec_U3C_cctorU3Eb__9_28_m4262000256 (U3CU3Ec_t88230275 * __this, IntPtr_t ___h0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ThirdParty.iOS4Unity.Runtime/<>c::<.cctor>b__9_29(System.IntPtr)
extern "C"  Il2CppObject * U3CU3Ec_U3C_cctorU3Eb__9_29_m3543584863 (U3CU3Ec_t88230275 * __this, IntPtr_t ___h0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ThirdParty.iOS4Unity.Runtime/<>c::<.cctor>b__9_30(System.IntPtr)
extern "C"  Il2CppObject * U3CU3Ec_U3C_cctorU3Eb__9_30_m1288642067 (U3CU3Ec_t88230275 * __this, IntPtr_t ___h0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
