﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.IEnumerator`1<System.Xml.Linq.XElement>
struct IEnumerator_1_t2324312173;
// System.Xml.Linq.XElement
struct XElement_t553821050;
// System.Xml.Linq.XName
struct XName_t785190363;
// System.Xml.Linq.XContainer
struct XContainer_t1445911831;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Linq.XContainer/<Elements>c__Iterator1F
struct  U3CElementsU3Ec__Iterator1F_t3355856435  : public Il2CppObject
{
public:
	// System.Collections.Generic.IEnumerator`1<System.Xml.Linq.XElement> System.Xml.Linq.XContainer/<Elements>c__Iterator1F::<$s_50>__0
	Il2CppObject* ___U3CU24s_50U3E__0_0;
	// System.Xml.Linq.XElement System.Xml.Linq.XContainer/<Elements>c__Iterator1F::<el>__1
	XElement_t553821050 * ___U3CelU3E__1_1;
	// System.Xml.Linq.XName System.Xml.Linq.XContainer/<Elements>c__Iterator1F::name
	XName_t785190363 * ___name_2;
	// System.Int32 System.Xml.Linq.XContainer/<Elements>c__Iterator1F::$PC
	int32_t ___U24PC_3;
	// System.Xml.Linq.XElement System.Xml.Linq.XContainer/<Elements>c__Iterator1F::$current
	XElement_t553821050 * ___U24current_4;
	// System.Xml.Linq.XName System.Xml.Linq.XContainer/<Elements>c__Iterator1F::<$>name
	XName_t785190363 * ___U3CU24U3Ename_5;
	// System.Xml.Linq.XContainer System.Xml.Linq.XContainer/<Elements>c__Iterator1F::<>f__this
	XContainer_t1445911831 * ___U3CU3Ef__this_6;

public:
	inline static int32_t get_offset_of_U3CU24s_50U3E__0_0() { return static_cast<int32_t>(offsetof(U3CElementsU3Ec__Iterator1F_t3355856435, ___U3CU24s_50U3E__0_0)); }
	inline Il2CppObject* get_U3CU24s_50U3E__0_0() const { return ___U3CU24s_50U3E__0_0; }
	inline Il2CppObject** get_address_of_U3CU24s_50U3E__0_0() { return &___U3CU24s_50U3E__0_0; }
	inline void set_U3CU24s_50U3E__0_0(Il2CppObject* value)
	{
		___U3CU24s_50U3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_50U3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CelU3E__1_1() { return static_cast<int32_t>(offsetof(U3CElementsU3Ec__Iterator1F_t3355856435, ___U3CelU3E__1_1)); }
	inline XElement_t553821050 * get_U3CelU3E__1_1() const { return ___U3CelU3E__1_1; }
	inline XElement_t553821050 ** get_address_of_U3CelU3E__1_1() { return &___U3CelU3E__1_1; }
	inline void set_U3CelU3E__1_1(XElement_t553821050 * value)
	{
		___U3CelU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CelU3E__1_1, value);
	}

	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(U3CElementsU3Ec__Iterator1F_t3355856435, ___name_2)); }
	inline XName_t785190363 * get_name_2() const { return ___name_2; }
	inline XName_t785190363 ** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(XName_t785190363 * value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier(&___name_2, value);
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CElementsU3Ec__Iterator1F_t3355856435, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CElementsU3Ec__Iterator1F_t3355856435, ___U24current_4)); }
	inline XElement_t553821050 * get_U24current_4() const { return ___U24current_4; }
	inline XElement_t553821050 ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(XElement_t553821050 * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Ename_5() { return static_cast<int32_t>(offsetof(U3CElementsU3Ec__Iterator1F_t3355856435, ___U3CU24U3Ename_5)); }
	inline XName_t785190363 * get_U3CU24U3Ename_5() const { return ___U3CU24U3Ename_5; }
	inline XName_t785190363 ** get_address_of_U3CU24U3Ename_5() { return &___U3CU24U3Ename_5; }
	inline void set_U3CU24U3Ename_5(XName_t785190363 * value)
	{
		___U3CU24U3Ename_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Ename_5, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_6() { return static_cast<int32_t>(offsetof(U3CElementsU3Ec__Iterator1F_t3355856435, ___U3CU3Ef__this_6)); }
	inline XContainer_t1445911831 * get_U3CU3Ef__this_6() const { return ___U3CU3Ef__this_6; }
	inline XContainer_t1445911831 ** get_address_of_U3CU3Ef__this_6() { return &___U3CU3Ef__this_6; }
	inline void set_U3CU3Ef__this_6(XContainer_t1445911831 * value)
	{
		___U3CU3Ef__this_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
