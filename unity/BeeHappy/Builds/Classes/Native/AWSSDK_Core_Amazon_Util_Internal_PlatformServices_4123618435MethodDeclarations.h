﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Util.Internal.PlatformServices.EnvironmentInfo
struct EnvironmentInfo_t4123618435;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.String Amazon.Util.Internal.PlatformServices.EnvironmentInfo::get_FrameworkUserAgent()
extern "C"  String_t* EnvironmentInfo_get_FrameworkUserAgent_m2529155456 (EnvironmentInfo_t4123618435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Util.Internal.PlatformServices.EnvironmentInfo::get_PlatformUserAgent()
extern "C"  String_t* EnvironmentInfo_get_PlatformUserAgent_m2112782567 (EnvironmentInfo_t4123618435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
