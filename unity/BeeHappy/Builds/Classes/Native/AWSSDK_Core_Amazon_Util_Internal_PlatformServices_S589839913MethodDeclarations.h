﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Util.Internal.PlatformServices.ServiceFactory
struct ServiceFactory_t589839913;

#include "codegen/il2cpp-codegen.h"

// System.Void Amazon.Util.Internal.PlatformServices.ServiceFactory::.ctor()
extern "C"  void ServiceFactory__ctor_m1177188142 (ServiceFactory_t589839913 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Util.Internal.PlatformServices.ServiceFactory::.cctor()
extern "C"  void ServiceFactory__cctor_m2878186765 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
