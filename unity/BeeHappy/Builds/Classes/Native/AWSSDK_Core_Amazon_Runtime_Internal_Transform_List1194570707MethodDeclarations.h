﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_List1903008017MethodDeclarations.h"

// System.Void Amazon.Runtime.Internal.Transform.ListUnmarshaller`2<System.String,Amazon.Runtime.Internal.Transform.StringUnmarshaller>::.ctor(IUnmarshaller)
#define ListUnmarshaller_2__ctor_m2687319997(__this, ___iUnmarshaller0, method) ((  void (*) (ListUnmarshaller_2_t1194570707 *, StringUnmarshaller_t3953260147 *, const MethodInfo*))ListUnmarshaller_2__ctor_m1344739393_gshared)(__this, ___iUnmarshaller0, method)
// System.Collections.Generic.List`1<I> Amazon.Runtime.Internal.Transform.ListUnmarshaller`2<System.String,Amazon.Runtime.Internal.Transform.StringUnmarshaller>::Unmarshall(Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext)
#define ListUnmarshaller_2_Unmarshall_m3927432904(__this, ___context0, method) ((  List_1_t1398341365 * (*) (ListUnmarshaller_2_t1194570707 *, XmlUnmarshallerContext_t1179575220 *, const MethodInfo*))ListUnmarshaller_2_Unmarshall_m4244807702_gshared)(__this, ___context0, method)
// System.Collections.Generic.List`1<I> Amazon.Runtime.Internal.Transform.ListUnmarshaller`2<System.String,Amazon.Runtime.Internal.Transform.StringUnmarshaller>::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
#define ListUnmarshaller_2_Unmarshall_m2438903080(__this, ___context0, method) ((  List_1_t1398341365 * (*) (ListUnmarshaller_2_t1194570707 *, JsonUnmarshallerContext_t456235889 *, const MethodInfo*))ListUnmarshaller_2_Unmarshall_m3347864357_gshared)(__this, ___context0, method)
