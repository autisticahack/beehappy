﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_Simp4064287855MethodDeclarations.h"

// T Amazon.Runtime.Internal.Transform.SimpleTypeUnmarshaller`1<System.String>::Unmarshall(Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext)
#define SimpleTypeUnmarshaller_1_Unmarshall_m2036205052(__this /* static, unused */, ___context0, method) ((  String_t* (*) (Il2CppObject * /* static, unused */, XmlUnmarshallerContext_t1179575220 *, const MethodInfo*))SimpleTypeUnmarshaller_1_Unmarshall_m2397289302_gshared)(__this /* static, unused */, ___context0, method)
// T Amazon.Runtime.Internal.Transform.SimpleTypeUnmarshaller`1<System.String>::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
#define SimpleTypeUnmarshaller_1_Unmarshall_m1482674883(__this /* static, unused */, ___context0, method) ((  String_t* (*) (Il2CppObject * /* static, unused */, JsonUnmarshallerContext_t456235889 *, const MethodInfo*))SimpleTypeUnmarshaller_1_Unmarshall_m3239430399_gshared)(__this /* static, unused */, ___context0, method)
