﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,System.Type>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m544997585(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t998506345 *, Type_t *, Type_t *, const MethodInfo*))KeyValuePair_2__ctor_m1640124561_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Type,System.Type>::get_Key()
#define KeyValuePair_2_get_Key_m1503053371(__this, method) ((  Type_t * (*) (KeyValuePair_2_t998506345 *, const MethodInfo*))KeyValuePair_2_get_Key_m2561166459_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,System.Type>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m3431695540(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t998506345 *, Type_t *, const MethodInfo*))KeyValuePair_2_set_Key_m744486900_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Type,System.Type>::get_Value()
#define KeyValuePair_2_get_Value_m1903397979(__this, method) ((  Type_t * (*) (KeyValuePair_2_t998506345 *, const MethodInfo*))KeyValuePair_2_get_Value_m499643803_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,System.Type>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m2755007564(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t998506345 *, Type_t *, const MethodInfo*))KeyValuePair_2_set_Value_m1416408204_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Type,System.Type>::ToString()
#define KeyValuePair_2_ToString_m2658387020(__this, method) ((  String_t* (*) (KeyValuePair_2_t998506345 *, const MethodInfo*))KeyValuePair_2_ToString_m2613351884_gshared)(__this, method)
