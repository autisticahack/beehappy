﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.Transform.JsonMarshallerContext
struct JsonMarshallerContext_t4063314926;
// ThirdParty.Json.LitJson.JsonWriter
struct JsonWriter_t3014444111;
// Amazon.Runtime.Internal.IRequest
struct IRequest_t2400804350;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_JsonWriter3014444111.h"

// ThirdParty.Json.LitJson.JsonWriter Amazon.Runtime.Internal.Transform.JsonMarshallerContext::get_Writer()
extern "C"  JsonWriter_t3014444111 * JsonMarshallerContext_get_Writer_m2343372188 (JsonMarshallerContext_t4063314926 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Transform.JsonMarshallerContext::set_Writer(ThirdParty.Json.LitJson.JsonWriter)
extern "C"  void JsonMarshallerContext_set_Writer_m1701775233 (JsonMarshallerContext_t4063314926 * __this, JsonWriter_t3014444111 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Transform.JsonMarshallerContext::.ctor(Amazon.Runtime.Internal.IRequest,ThirdParty.Json.LitJson.JsonWriter)
extern "C"  void JsonMarshallerContext__ctor_m1418069536 (JsonMarshallerContext_t4063314926 * __this, Il2CppObject * ___request0, JsonWriter_t3014444111 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
