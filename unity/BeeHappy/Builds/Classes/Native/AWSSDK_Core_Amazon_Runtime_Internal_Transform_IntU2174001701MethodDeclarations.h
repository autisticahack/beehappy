﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.Transform.IntUnmarshaller
struct IntUnmarshaller_t2174001701;
// Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext
struct XmlUnmarshallerContext_t1179575220;
// Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext
struct JsonUnmarshallerContext_t456235889;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_XmlU1179575220.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_JsonU456235889.h"

// System.Void Amazon.Runtime.Internal.Transform.IntUnmarshaller::.ctor()
extern "C"  void IntUnmarshaller__ctor_m164694297 (IntUnmarshaller_t2174001701 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Runtime.Internal.Transform.IntUnmarshaller Amazon.Runtime.Internal.Transform.IntUnmarshaller::get_Instance()
extern "C"  IntUnmarshaller_t2174001701 * IntUnmarshaller_get_Instance_m769761570 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Amazon.Runtime.Internal.Transform.IntUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext)
extern "C"  int32_t IntUnmarshaller_Unmarshall_m705606319 (IntUnmarshaller_t2174001701 * __this, XmlUnmarshallerContext_t1179575220 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Amazon.Runtime.Internal.Transform.IntUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern "C"  int32_t IntUnmarshaller_Unmarshall_m1534374026 (IntUnmarshaller_t2174001701 * __this, JsonUnmarshallerContext_t456235889 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Transform.IntUnmarshaller::.cctor()
extern "C"  void IntUnmarshaller__cctor_m3056354376 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
