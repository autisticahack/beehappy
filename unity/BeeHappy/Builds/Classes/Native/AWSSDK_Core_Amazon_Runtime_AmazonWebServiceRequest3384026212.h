﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Amazon.Runtime.RequestEventHandler
struct RequestEventHandler_t2213783891;
// System.EventHandler`1<Amazon.Runtime.StreamTransferProgressArgs>
struct EventHandler_1_t1230945235;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.AmazonWebServiceRequest
struct  AmazonWebServiceRequest_t3384026212  : public Il2CppObject
{
public:
	// Amazon.Runtime.RequestEventHandler Amazon.Runtime.AmazonWebServiceRequest::mBeforeRequestEvent
	RequestEventHandler_t2213783891 * ___mBeforeRequestEvent_0;
	// System.EventHandler`1<Amazon.Runtime.StreamTransferProgressArgs> Amazon.Runtime.AmazonWebServiceRequest::<Amazon.Runtime.Internal.IAmazonWebServiceRequest.StreamUploadProgressCallback>k__BackingField
	EventHandler_1_t1230945235 * ___U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_StreamUploadProgressCallbackU3Ek__BackingField_1;
	// System.Boolean Amazon.Runtime.AmazonWebServiceRequest::<Amazon.Runtime.Internal.IAmazonWebServiceRequest.UseSigV4>k__BackingField
	bool ___U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_UseSigV4U3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_mBeforeRequestEvent_0() { return static_cast<int32_t>(offsetof(AmazonWebServiceRequest_t3384026212, ___mBeforeRequestEvent_0)); }
	inline RequestEventHandler_t2213783891 * get_mBeforeRequestEvent_0() const { return ___mBeforeRequestEvent_0; }
	inline RequestEventHandler_t2213783891 ** get_address_of_mBeforeRequestEvent_0() { return &___mBeforeRequestEvent_0; }
	inline void set_mBeforeRequestEvent_0(RequestEventHandler_t2213783891 * value)
	{
		___mBeforeRequestEvent_0 = value;
		Il2CppCodeGenWriteBarrier(&___mBeforeRequestEvent_0, value);
	}

	inline static int32_t get_offset_of_U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_StreamUploadProgressCallbackU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AmazonWebServiceRequest_t3384026212, ___U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_StreamUploadProgressCallbackU3Ek__BackingField_1)); }
	inline EventHandler_1_t1230945235 * get_U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_StreamUploadProgressCallbackU3Ek__BackingField_1() const { return ___U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_StreamUploadProgressCallbackU3Ek__BackingField_1; }
	inline EventHandler_1_t1230945235 ** get_address_of_U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_StreamUploadProgressCallbackU3Ek__BackingField_1() { return &___U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_StreamUploadProgressCallbackU3Ek__BackingField_1; }
	inline void set_U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_StreamUploadProgressCallbackU3Ek__BackingField_1(EventHandler_1_t1230945235 * value)
	{
		___U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_StreamUploadProgressCallbackU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_StreamUploadProgressCallbackU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_UseSigV4U3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AmazonWebServiceRequest_t3384026212, ___U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_UseSigV4U3Ek__BackingField_2)); }
	inline bool get_U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_UseSigV4U3Ek__BackingField_2() const { return ___U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_UseSigV4U3Ek__BackingField_2; }
	inline bool* get_address_of_U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_UseSigV4U3Ek__BackingField_2() { return &___U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_UseSigV4U3Ek__BackingField_2; }
	inline void set_U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_UseSigV4U3Ek__BackingField_2(bool value)
	{
		___U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_UseSigV4U3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
