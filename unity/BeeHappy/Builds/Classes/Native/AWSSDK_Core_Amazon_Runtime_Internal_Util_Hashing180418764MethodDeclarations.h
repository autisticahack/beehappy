﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Int32[]
struct Int32U5BU5D_t3030399641;

#include "codegen/il2cpp-codegen.h"

// System.Int32 Amazon.Runtime.Internal.Util.Hashing::Hash(System.Object[])
extern "C"  int32_t Hashing_Hash_m4160720114 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t3614634134* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Amazon.Runtime.Internal.Util.Hashing::CombineHashes(System.Int32[])
extern "C"  int32_t Hashing_CombineHashes_m2413807072 (Il2CppObject * __this /* static, unused */, Int32U5BU5D_t3030399641* ___hashes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Amazon.Runtime.Internal.Util.Hashing::CombineHashesInternal(System.Int32,System.Int32)
extern "C"  int32_t Hashing_CombineHashesInternal_m1584888036 (Il2CppObject * __this /* static, unused */, int32_t ___a0, int32_t ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
