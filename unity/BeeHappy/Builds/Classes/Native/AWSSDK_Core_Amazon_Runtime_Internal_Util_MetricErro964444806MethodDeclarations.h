﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.Runtime.Internal.Util.MetricError
struct MetricError_t964444806;
// System.String
struct String_t;
// System.Exception
struct Exception_t1927440687;
// System.Object[]
struct ObjectU5BU5D_t3614634134;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_Runtime_Metric3273440202.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Exception1927440687.h"
#include "mscorlib_System_DateTime693205669.h"

// Amazon.Runtime.Metric Amazon.Runtime.Internal.Util.MetricError::get_Metric()
extern "C"  int32_t MetricError_get_Metric_m1422654246 (MetricError_t964444806 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.MetricError::set_Metric(Amazon.Runtime.Metric)
extern "C"  void MetricError_set_Metric_m1786925873 (MetricError_t964444806 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.Runtime.Internal.Util.MetricError::get_Message()
extern "C"  String_t* MetricError_get_Message_m1858092105 (MetricError_t964444806 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.MetricError::set_Message(System.String)
extern "C"  void MetricError_set_Message_m1419742594 (MetricError_t964444806 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception Amazon.Runtime.Internal.Util.MetricError::get_Exception()
extern "C"  Exception_t1927440687 * MetricError_get_Exception_m1194766923 (MetricError_t964444806 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.MetricError::set_Exception(System.Exception)
extern "C"  void MetricError_set_Exception_m4245808816 (MetricError_t964444806 * __this, Exception_t1927440687 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime Amazon.Runtime.Internal.Util.MetricError::get_Time()
extern "C"  DateTime_t693205669  MetricError_get_Time_m2914243259 (MetricError_t964444806 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.MetricError::set_Time(System.DateTime)
extern "C"  void MetricError_set_Time_m2275927036 (MetricError_t964444806 * __this, DateTime_t693205669  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.MetricError::.ctor(Amazon.Runtime.Metric,System.String,System.Object[])
extern "C"  void MetricError__ctor_m2584600004 (MetricError_t964444806 * __this, int32_t ___metric0, String_t* ___messageFormat1, ObjectU5BU5D_t3614634134* ___args2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.Runtime.Internal.Util.MetricError::.ctor(Amazon.Runtime.Metric,System.Exception,System.String,System.Object[])
extern "C"  void MetricError__ctor_m3554866044 (MetricError_t964444806 * __this, int32_t ___metric0, Exception_t1927440687 * ___exception1, String_t* ___messageFormat2, ObjectU5BU5D_t3614634134* ___args3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
