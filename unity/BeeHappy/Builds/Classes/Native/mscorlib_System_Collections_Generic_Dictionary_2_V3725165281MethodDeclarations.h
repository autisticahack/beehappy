﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>
struct ValueCollection_t3725165281;
// System.Collections.Generic.Dictionary`2<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>
struct Dictionary_2_t727138142;
// System.Collections.Generic.IEnumerator`1<ThirdParty.Json.LitJson.ArrayMetadata>
struct IEnumerator_1_t2905569137;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// ThirdParty.Json.LitJson.ArrayMetadata[]
struct ArrayMetadataU5BU5D_t3927266763;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_ThirdParty_Json_LitJson_ArrayMetadata1135078014.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2413670906.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m1108018630_gshared (ValueCollection_t3725165281 * __this, Dictionary_2_t727138142 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m1108018630(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t3725165281 *, Dictionary_2_t727138142 *, const MethodInfo*))ValueCollection__ctor_m1108018630_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3345332980_gshared (ValueCollection_t3725165281 * __this, ArrayMetadata_t1135078014  ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3345332980(__this, ___item0, method) ((  void (*) (ValueCollection_t3725165281 *, ArrayMetadata_t1135078014 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3345332980_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1917629527_gshared (ValueCollection_t3725165281 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1917629527(__this, method) ((  void (*) (ValueCollection_t3725165281 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1917629527_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1785773216_gshared (ValueCollection_t3725165281 * __this, ArrayMetadata_t1135078014  ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1785773216(__this, ___item0, method) ((  bool (*) (ValueCollection_t3725165281 *, ArrayMetadata_t1135078014 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1785773216_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m721874727_gshared (ValueCollection_t3725165281 * __this, ArrayMetadata_t1135078014  ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m721874727(__this, ___item0, method) ((  bool (*) (ValueCollection_t3725165281 *, ArrayMetadata_t1135078014 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m721874727_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2748591009_gshared (ValueCollection_t3725165281 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2748591009(__this, method) ((  Il2CppObject* (*) (ValueCollection_t3725165281 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2748591009_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m280898373_gshared (ValueCollection_t3725165281 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m280898373(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t3725165281 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m280898373_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m211044868_gshared (ValueCollection_t3725165281 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m211044868(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3725165281 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m211044868_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3396541731_gshared (ValueCollection_t3725165281 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3396541731(__this, method) ((  bool (*) (ValueCollection_t3725165281 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3396541731_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3022974697_gshared (ValueCollection_t3725165281 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3022974697(__this, method) ((  bool (*) (ValueCollection_t3725165281 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3022974697_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m2568042789_gshared (ValueCollection_t3725165281 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m2568042789(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3725165281 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m2568042789_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m752359783_gshared (ValueCollection_t3725165281 * __this, ArrayMetadataU5BU5D_t3927266763* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m752359783(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t3725165281 *, ArrayMetadataU5BU5D_t3927266763*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m752359783_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::GetEnumerator()
extern "C"  Enumerator_t2413670906  ValueCollection_GetEnumerator_m2382992780_gshared (ValueCollection_t3725165281 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m2382992780(__this, method) ((  Enumerator_t2413670906  (*) (ValueCollection_t3725165281 *, const MethodInfo*))ValueCollection_GetEnumerator_m2382992780_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,ThirdParty.Json.LitJson.ArrayMetadata>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m1001996513_gshared (ValueCollection_t3725165281 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m1001996513(__this, method) ((  int32_t (*) (ValueCollection_t3725165281 *, const MethodInfo*))ValueCollection_get_Count_m1001996513_gshared)(__this, method)
