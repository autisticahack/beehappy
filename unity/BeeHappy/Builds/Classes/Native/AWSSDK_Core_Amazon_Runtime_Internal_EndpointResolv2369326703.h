﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AWSSDK_Core_Amazon_Runtime_Internal_PipelineHandle1486422324.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.EndpointResolver
struct  EndpointResolver_t2369326703  : public PipelineHandler_t1486422324
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
