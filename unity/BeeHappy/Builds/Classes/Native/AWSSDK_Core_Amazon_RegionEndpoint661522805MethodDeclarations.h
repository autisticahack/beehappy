﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Amazon.RegionEndpoint
struct RegionEndpoint_t661522805;
// System.String
struct String_t;
// Amazon.Internal.IRegionEndpointProvider
struct IRegionEndpointProvider_t2524767261;
// Amazon.Internal.IRegionEndpoint
struct IRegionEndpoint_t544433888;
// Amazon.RegionEndpoint/Endpoint
struct Endpoint_t3348692641;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// Amazon.RegionEndpoint Amazon.RegionEndpoint::GetBySystemName(System.String)
extern "C"  RegionEndpoint_t661522805 * RegionEndpoint_GetBySystemName_m1648583385 (Il2CppObject * __this /* static, unused */, String_t* ___systemName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.RegionEndpoint Amazon.RegionEndpoint::NewEndpoint(System.String,System.String)
extern "C"  RegionEndpoint_t661522805 * RegionEndpoint_NewEndpoint_m736293387 (Il2CppObject * __this /* static, unused */, String_t* ___systemName0, String_t* ___displayName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Internal.IRegionEndpointProvider Amazon.RegionEndpoint::get_RegionEndpointProvider()
extern "C"  Il2CppObject * RegionEndpoint_get_RegionEndpointProvider_m281647112 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.RegionEndpoint::.ctor(System.String,System.String)
extern "C"  void RegionEndpoint__ctor_m3028203532 (RegionEndpoint_t661522805 * __this, String_t* ___systemName0, String_t* ___displayName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.RegionEndpoint::get_SystemName()
extern "C"  String_t* RegionEndpoint_get_SystemName_m3691577380 (RegionEndpoint_t661522805 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.RegionEndpoint::set_SystemName(System.String)
extern "C"  void RegionEndpoint_set_SystemName_m2199217877 (RegionEndpoint_t661522805 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.RegionEndpoint::get_DisplayName()
extern "C"  String_t* RegionEndpoint_get_DisplayName_m2488337113 (RegionEndpoint_t661522805 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.RegionEndpoint::set_DisplayName(System.String)
extern "C"  void RegionEndpoint_set_DisplayName_m917299226 (RegionEndpoint_t661522805 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.Internal.IRegionEndpoint Amazon.RegionEndpoint::get_InternedRegionEndpoint()
extern "C"  Il2CppObject * RegionEndpoint_get_InternedRegionEndpoint_m301203529 (RegionEndpoint_t661522805 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Amazon.RegionEndpoint/Endpoint Amazon.RegionEndpoint::GetEndpointForService(System.String,System.Boolean)
extern "C"  Endpoint_t3348692641 * RegionEndpoint_GetEndpointForService_m3177011392 (RegionEndpoint_t661522805 * __this, String_t* ___serviceName0, bool ___dualStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Amazon.RegionEndpoint::ToString()
extern "C"  String_t* RegionEndpoint_ToString_m74067991 (RegionEndpoint_t661522805 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Amazon.RegionEndpoint::.cctor()
extern "C"  void RegionEndpoint__cctor_m3603279771 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
