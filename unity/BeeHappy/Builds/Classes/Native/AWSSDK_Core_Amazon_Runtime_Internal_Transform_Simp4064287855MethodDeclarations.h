﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext
struct XmlUnmarshallerContext_t1179575220;
// Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext
struct JsonUnmarshallerContext_t456235889;

#include "codegen/il2cpp-codegen.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_XmlU1179575220.h"
#include "AWSSDK_Core_Amazon_Runtime_Internal_Transform_JsonU456235889.h"

// T Amazon.Runtime.Internal.Transform.SimpleTypeUnmarshaller`1<System.Object>::Unmarshall(Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext)
extern "C"  Il2CppObject * SimpleTypeUnmarshaller_1_Unmarshall_m2397289302_gshared (Il2CppObject * __this /* static, unused */, XmlUnmarshallerContext_t1179575220 * ___context0, const MethodInfo* method);
#define SimpleTypeUnmarshaller_1_Unmarshall_m2397289302(__this /* static, unused */, ___context0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, XmlUnmarshallerContext_t1179575220 *, const MethodInfo*))SimpleTypeUnmarshaller_1_Unmarshall_m2397289302_gshared)(__this /* static, unused */, ___context0, method)
// T Amazon.Runtime.Internal.Transform.SimpleTypeUnmarshaller`1<System.Object>::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern "C"  Il2CppObject * SimpleTypeUnmarshaller_1_Unmarshall_m3239430399_gshared (Il2CppObject * __this /* static, unused */, JsonUnmarshallerContext_t456235889 * ___context0, const MethodInfo* method);
#define SimpleTypeUnmarshaller_1_Unmarshall_m3239430399(__this /* static, unused */, ___context0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, JsonUnmarshallerContext_t456235889 *, const MethodInfo*))SimpleTypeUnmarshaller_1_Unmarshall_m3239430399_gshared)(__this /* static, unused */, ___context0, method)
