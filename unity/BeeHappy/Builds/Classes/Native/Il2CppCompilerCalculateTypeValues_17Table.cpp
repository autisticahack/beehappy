﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XmlDocumentFragment3083262362.h"
#include "System_Xml_System_Xml_XmlDocumentType824160610.h"
#include "System_Xml_System_Xml_XmlElement2877111883.h"
#include "System_Xml_System_Xml_XmlEntity4027255380.h"
#include "System_Xml_System_Xml_XmlEntityReference3053868353.h"
#include "System_Xml_System_Xml_XmlException4188277960.h"
#include "System_Xml_System_Xml_XmlImplementation1664517635.h"
#include "System_Xml_System_Xml_XmlStreamReader2725532304.h"
#include "System_Xml_System_Xml_NonBlockingStreamReader3963211903.h"
#include "System_Xml_System_Xml_XmlInputStream2650744719.h"
#include "System_Xml_System_Xml_XmlLinkedNode1287616130.h"
#include "System_Xml_System_Xml_XmlNameEntry3745551716.h"
#include "System_Xml_System_Xml_XmlNameEntryCache3855584002.h"
#include "System_Xml_System_Xml_XmlNameTable1345805268.h"
#include "System_Xml_System_Xml_XmlNamedNodeMap145210370.h"
#include "System_Xml_System_Xml_XmlNamespaceManager486731501.h"
#include "System_Xml_System_Xml_XmlNamespaceManager_NsDecl3210081295.h"
#include "System_Xml_System_Xml_XmlNamespaceManager_NsScope2513625351.h"
#include "System_Xml_System_Xml_XmlNode616554813.h"
#include "System_Xml_System_Xml_XmlNode_EmptyNodeList1718403287.h"
#include "System_Xml_System_Xml_XmlNodeChangedAction1188489541.h"
#include "System_Xml_System_Xml_XmlNodeChangedEventArgs4036174778.h"
#include "System_Xml_System_Xml_XmlNodeList497326455.h"
#include "System_Xml_System_Xml_XmlNodeListChildren2811458520.h"
#include "System_Xml_System_Xml_XmlNodeListChildren_Enumerato569056069.h"
#include "System_Xml_System_Xml_XmlNodeReader1022603664.h"
#include "System_Xml_System_Xml_XmlNodeReaderImpl2982135230.h"
#include "System_Xml_System_Xml_XmlNodeType739504597.h"
#include "System_Xml_System_Xml_XmlNotation206561061.h"
#include "System_Xml_System_Xml_XmlOutputMethod2267235953.h"
#include "System_Xml_System_Xml_XmlParserContext2728039553.h"
#include "System_Xml_System_Xml_XmlParserContext_ContextItem1262420678.h"
#include "System_Xml_System_Xml_XmlParserInput2366782760.h"
#include "System_Xml_System_Xml_XmlParserInput_XmlParserInputS25800784.h"
#include "System_Xml_System_Xml_XmlProcessingInstruction431557540.h"
#include "System_Xml_System_Xml_XmlQualifiedName1944712516.h"
#include "System_Xml_System_Xml_XmlReader3675626668.h"
#include "System_Xml_System_Xml_XmlReaderBinarySupport1548133672.h"
#include "System_Xml_System_Xml_XmlReaderBinarySupport_Comma1644897369.h"
#include "System_Xml_System_Xml_XmlReaderBinarySupport_CharG1955031820.h"
#include "System_Xml_System_Xml_XmlReaderSettings1578612233.h"
#include "System_Xml_System_Xml_XmlResolver2024571559.h"
#include "System_Xml_System_Xml_XmlSignificantWhitespace1224054391.h"
#include "System_Xml_System_Xml_XmlSpace2880376877.h"
#include "System_Xml_System_Xml_XmlText4111601336.h"
#include "System_Xml_Mono_Xml2_XmlTextReader511376973.h"
#include "System_Xml_Mono_Xml2_XmlTextReader_XmlTokenInfo254587324.h"
#include "System_Xml_Mono_Xml2_XmlTextReader_XmlAttributeTok3353594030.h"
#include "System_Xml_Mono_Xml2_XmlTextReader_TagName2340974457.h"
#include "System_Xml_Mono_Xml2_XmlTextReader_DtdInputState3313602765.h"
#include "System_Xml_Mono_Xml2_XmlTextReader_DtdInputStateSt3023928423.h"
#include "System_Xml_System_Xml_XmlTextReader3514170725.h"
#include "System_Xml_System_Xml_XmlTextWriter2527250655.h"
#include "System_Xml_System_Xml_XmlTextWriter_XmlNodeInfo3709371029.h"
#include "System_Xml_System_Xml_XmlTextWriter_StringUtil2068578019.h"
#include "System_Xml_System_Xml_XmlTextWriter_XmlDeclState3530111136.h"
#include "System_Xml_System_Xml_XmlTokenizedType1619571710.h"
#include "System_Xml_System_Xml_XmlUrlResolver896669594.h"
#include "System_Xml_System_Xml_XmlValidatingReader3416770767.h"
#include "System_Xml_System_Xml_XmlWhitespace2557770518.h"
#include "System_Xml_System_Xml_XmlWriter1048088568.h"
#include "System_Xml_System_Xml_XmlWriterSettings924210539.h"
#include "System_Xml_System_Xml_Schema_ValidationEventHandle1580700381.h"
#include "System_Xml_System_Xml_Schema_XmlValueGetter1685472371.h"
#include "System_Xml_System_Xml_XmlNodeChangedEventHandler2964483403.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A3672778802.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A1957337327.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A2038352954.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24Ar628910058.h"
#include "System_Xml_Linq_U3CModuleU3E3783534214.h"
#include "System_Xml_Linq_System_Xml_Linq_LoadOptions53198144.h"
#include "System_Xml_Linq_System_Xml_Linq_SaveOptions2961005337.h"
#include "System_Xml_Linq_System_Xml_Linq_XAttribute3858477518.h"
#include "System_Xml_Linq_System_Xml_Linq_XCData1306050895.h"
#include "System_Xml_Linq_System_Xml_Linq_XComment1618005895.h"
#include "System_Xml_Linq_System_Xml_Linq_XContainer1445911831.h"
#include "System_Xml_Linq_System_Xml_Linq_XContainer_U3CNode1473861320.h"
#include "System_Xml_Linq_System_Xml_Linq_XContainer_U3CElem2952571908.h"
#include "System_Xml_Linq_System_Xml_Linq_XContainer_U3CElem3355856435.h"
#include "System_Xml_Linq_System_Xml_Linq_XDeclaration3367285402.h"
#include "System_Xml_Linq_System_Xml_Linq_XDocument2733326047.h"
#include "System_Xml_Linq_System_Xml_Linq_XDocumentType738990919.h"
#include "System_Xml_Linq_System_Xml_Linq_XElement553821050.h"
#include "System_Xml_Linq_System_Xml_Linq_XElement_U3CAttrib3474816789.h"
#include "System_Xml_Linq_System_Xml_Linq_XName785190363.h"
#include "System_Xml_Linq_System_Xml_Linq_XNamespace1613015075.h"
#include "System_Xml_Linq_System_Xml_Linq_XNode2707504214.h"
#include "System_Xml_Linq_System_Xml_Linq_XNodeDocumentOrder1208684534.h"
#include "System_Xml_Linq_System_Xml_Linq_XNodeDocumentOrder4155354831.h"
#include "System_Xml_Linq_System_Xml_Linq_XNodeEqualityCompa3116038621.h"
#include "System_Xml_Linq_System_Xml_Linq_XObject3550811009.h"
#include "System_Xml_Linq_System_Xml_Linq_XProcessingInstruc2854903359.h"
#include "System_Xml_Linq_System_Xml_Linq_XText1860529549.h"
#include "System_Xml_Linq_System_Xml_Linq_XUtil3957506230.h"
#include "System_Xml_Linq_System_Xml_Linq_XUtil_U3CExpandArra507968046.h"
#include "System_Xml_Linq_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "System_Xml_Linq_U3CPrivateImplementationDetailsU3E1703410334.h"
#include "UnityEngine_U3CModuleU3E3783534214.h"
#include "UnityEngine_UnityEngine_NetworkReachability1092747145.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1700 = { sizeof (XmlDocumentFragment_t3083262362), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1700[1] = 
{
	XmlDocumentFragment_t3083262362::get_offset_of_lastLinkedChild_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1701 = { sizeof (XmlDocumentType_t824160610), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1701[3] = 
{
	XmlDocumentType_t824160610::get_offset_of_entities_6(),
	XmlDocumentType_t824160610::get_offset_of_notations_7(),
	XmlDocumentType_t824160610::get_offset_of_dtd_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1702 = { sizeof (XmlElement_t2877111883), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1702[5] = 
{
	XmlElement_t2877111883::get_offset_of_attributes_6(),
	XmlElement_t2877111883::get_offset_of_name_7(),
	XmlElement_t2877111883::get_offset_of_lastLinkedChild_8(),
	XmlElement_t2877111883::get_offset_of_isNotEmpty_9(),
	XmlElement_t2877111883::get_offset_of_schemaInfo_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1703 = { sizeof (XmlEntity_t4027255380), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1703[7] = 
{
	XmlEntity_t4027255380::get_offset_of_name_5(),
	XmlEntity_t4027255380::get_offset_of_NDATA_6(),
	XmlEntity_t4027255380::get_offset_of_publicId_7(),
	XmlEntity_t4027255380::get_offset_of_systemId_8(),
	XmlEntity_t4027255380::get_offset_of_baseUri_9(),
	XmlEntity_t4027255380::get_offset_of_lastLinkedChild_10(),
	XmlEntity_t4027255380::get_offset_of_contentAlreadySet_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1704 = { sizeof (XmlEntityReference_t3053868353), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1704[2] = 
{
	XmlEntityReference_t3053868353::get_offset_of_entityName_6(),
	XmlEntityReference_t3053868353::get_offset_of_lastLinkedChild_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1705 = { sizeof (XmlException_t4188277960), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1705[5] = 
{
	XmlException_t4188277960::get_offset_of_lineNumber_11(),
	XmlException_t4188277960::get_offset_of_linePosition_12(),
	XmlException_t4188277960::get_offset_of_sourceUri_13(),
	XmlException_t4188277960::get_offset_of_res_14(),
	XmlException_t4188277960::get_offset_of_messages_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1706 = { sizeof (XmlImplementation_t1664517635), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1706[1] = 
{
	XmlImplementation_t1664517635::get_offset_of_InternalNameTable_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1707 = { sizeof (XmlStreamReader_t2725532304), -1, sizeof(XmlStreamReader_t2725532304_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1707[2] = 
{
	XmlStreamReader_t2725532304::get_offset_of_input_12(),
	XmlStreamReader_t2725532304_StaticFields::get_offset_of_invalidDataException_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1708 = { sizeof (NonBlockingStreamReader_t3963211903), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1708[11] = 
{
	NonBlockingStreamReader_t3963211903::get_offset_of_input_buffer_1(),
	NonBlockingStreamReader_t3963211903::get_offset_of_decoded_buffer_2(),
	NonBlockingStreamReader_t3963211903::get_offset_of_decoded_count_3(),
	NonBlockingStreamReader_t3963211903::get_offset_of_pos_4(),
	NonBlockingStreamReader_t3963211903::get_offset_of_buffer_size_5(),
	NonBlockingStreamReader_t3963211903::get_offset_of_encoding_6(),
	NonBlockingStreamReader_t3963211903::get_offset_of_decoder_7(),
	NonBlockingStreamReader_t3963211903::get_offset_of_base_stream_8(),
	NonBlockingStreamReader_t3963211903::get_offset_of_mayBlock_9(),
	NonBlockingStreamReader_t3963211903::get_offset_of_line_builder_10(),
	NonBlockingStreamReader_t3963211903::get_offset_of_foundCR_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1709 = { sizeof (XmlInputStream_t2650744719), -1, sizeof(XmlInputStream_t2650744719_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1709[7] = 
{
	XmlInputStream_t2650744719_StaticFields::get_offset_of_StrictUTF8_1(),
	XmlInputStream_t2650744719::get_offset_of_enc_2(),
	XmlInputStream_t2650744719::get_offset_of_stream_3(),
	XmlInputStream_t2650744719::get_offset_of_buffer_4(),
	XmlInputStream_t2650744719::get_offset_of_bufLength_5(),
	XmlInputStream_t2650744719::get_offset_of_bufPos_6(),
	XmlInputStream_t2650744719_StaticFields::get_offset_of_encodingException_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1710 = { sizeof (XmlLinkedNode_t1287616130), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1710[1] = 
{
	XmlLinkedNode_t1287616130::get_offset_of_nextSibling_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1711 = { sizeof (XmlNameEntry_t3745551716), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1711[5] = 
{
	XmlNameEntry_t3745551716::get_offset_of_Prefix_0(),
	XmlNameEntry_t3745551716::get_offset_of_LocalName_1(),
	XmlNameEntry_t3745551716::get_offset_of_NS_2(),
	XmlNameEntry_t3745551716::get_offset_of_Hash_3(),
	XmlNameEntry_t3745551716::get_offset_of_prefixed_name_cache_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1712 = { sizeof (XmlNameEntryCache_t3855584002), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1712[4] = 
{
	XmlNameEntryCache_t3855584002::get_offset_of_table_0(),
	XmlNameEntryCache_t3855584002::get_offset_of_nameTable_1(),
	XmlNameEntryCache_t3855584002::get_offset_of_dummy_2(),
	XmlNameEntryCache_t3855584002::get_offset_of_cacheBuffer_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1713 = { sizeof (XmlNameTable_t1345805268), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1714 = { sizeof (XmlNamedNodeMap_t145210370), -1, sizeof(XmlNamedNodeMap_t145210370_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1714[4] = 
{
	XmlNamedNodeMap_t145210370_StaticFields::get_offset_of_emptyEnumerator_0(),
	XmlNamedNodeMap_t145210370::get_offset_of_parent_1(),
	XmlNamedNodeMap_t145210370::get_offset_of_nodeList_2(),
	XmlNamedNodeMap_t145210370::get_offset_of_readOnly_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1715 = { sizeof (XmlNamespaceManager_t486731501), -1, sizeof(XmlNamespaceManager_t486731501_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1715[9] = 
{
	XmlNamespaceManager_t486731501::get_offset_of_decls_0(),
	XmlNamespaceManager_t486731501::get_offset_of_declPos_1(),
	XmlNamespaceManager_t486731501::get_offset_of_scopes_2(),
	XmlNamespaceManager_t486731501::get_offset_of_scopePos_3(),
	XmlNamespaceManager_t486731501::get_offset_of_defaultNamespace_4(),
	XmlNamespaceManager_t486731501::get_offset_of_count_5(),
	XmlNamespaceManager_t486731501::get_offset_of_nameTable_6(),
	XmlNamespaceManager_t486731501::get_offset_of_internalAtomizedNames_7(),
	XmlNamespaceManager_t486731501_StaticFields::get_offset_of_U3CU3Ef__switchU24map28_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1716 = { sizeof (NsDecl_t3210081295)+ sizeof (Il2CppObject), sizeof(NsDecl_t3210081295_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1716[2] = 
{
	NsDecl_t3210081295::get_offset_of_Prefix_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	NsDecl_t3210081295::get_offset_of_Uri_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1717 = { sizeof (NsScope_t2513625351)+ sizeof (Il2CppObject), sizeof(NsScope_t2513625351_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1717[2] = 
{
	NsScope_t2513625351::get_offset_of_DeclCount_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	NsScope_t2513625351::get_offset_of_DefaultNamespace_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1718 = { sizeof (XmlNode_t616554813), -1, sizeof(XmlNode_t616554813_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1718[5] = 
{
	XmlNode_t616554813_StaticFields::get_offset_of_emptyList_0(),
	XmlNode_t616554813::get_offset_of_ownerDocument_1(),
	XmlNode_t616554813::get_offset_of_parentNode_2(),
	XmlNode_t616554813::get_offset_of_childNodes_3(),
	XmlNode_t616554813_StaticFields::get_offset_of_U3CU3Ef__switchU24map44_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1719 = { sizeof (EmptyNodeList_t1718403287), -1, sizeof(EmptyNodeList_t1718403287_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1719[1] = 
{
	EmptyNodeList_t1718403287_StaticFields::get_offset_of_emptyEnumerator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1720 = { sizeof (XmlNodeChangedAction_t1188489541)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1720[4] = 
{
	XmlNodeChangedAction_t1188489541::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1721 = { sizeof (XmlNodeChangedEventArgs_t4036174778), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1721[6] = 
{
	XmlNodeChangedEventArgs_t4036174778::get_offset_of__oldParent_1(),
	XmlNodeChangedEventArgs_t4036174778::get_offset_of__newParent_2(),
	XmlNodeChangedEventArgs_t4036174778::get_offset_of__action_3(),
	XmlNodeChangedEventArgs_t4036174778::get_offset_of__node_4(),
	XmlNodeChangedEventArgs_t4036174778::get_offset_of__oldValue_5(),
	XmlNodeChangedEventArgs_t4036174778::get_offset_of__newValue_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1722 = { sizeof (XmlNodeList_t497326455), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1723 = { sizeof (XmlNodeListChildren_t2811458520), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1723[1] = 
{
	XmlNodeListChildren_t2811458520::get_offset_of_parent_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1724 = { sizeof (Enumerator_t569056069), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1724[3] = 
{
	Enumerator_t569056069::get_offset_of_parent_0(),
	Enumerator_t569056069::get_offset_of_currentChild_1(),
	Enumerator_t569056069::get_offset_of_passedLastNode_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1725 = { sizeof (XmlNodeReader_t1022603664), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1725[4] = 
{
	XmlNodeReader_t1022603664::get_offset_of_entity_3(),
	XmlNodeReader_t1022603664::get_offset_of_source_4(),
	XmlNodeReader_t1022603664::get_offset_of_entityInsideAttribute_5(),
	XmlNodeReader_t1022603664::get_offset_of_insideAttribute_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1726 = { sizeof (XmlNodeReaderImpl_t2982135230), -1, sizeof(XmlNodeReaderImpl_t2982135230_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1726[12] = 
{
	XmlNodeReaderImpl_t2982135230::get_offset_of_document_3(),
	XmlNodeReaderImpl_t2982135230::get_offset_of_startNode_4(),
	XmlNodeReaderImpl_t2982135230::get_offset_of_current_5(),
	XmlNodeReaderImpl_t2982135230::get_offset_of_ownerLinkedNode_6(),
	XmlNodeReaderImpl_t2982135230::get_offset_of_state_7(),
	XmlNodeReaderImpl_t2982135230::get_offset_of_depth_8(),
	XmlNodeReaderImpl_t2982135230::get_offset_of_isEndElement_9(),
	XmlNodeReaderImpl_t2982135230::get_offset_of_ignoreStartNode_10(),
	XmlNodeReaderImpl_t2982135230_StaticFields::get_offset_of_U3CU3Ef__switchU24map4D_11(),
	XmlNodeReaderImpl_t2982135230_StaticFields::get_offset_of_U3CU3Ef__switchU24map4E_12(),
	XmlNodeReaderImpl_t2982135230_StaticFields::get_offset_of_U3CU3Ef__switchU24map4F_13(),
	XmlNodeReaderImpl_t2982135230_StaticFields::get_offset_of_U3CU3Ef__switchU24map50_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1727 = { sizeof (XmlNodeType_t739504597)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1727[19] = 
{
	XmlNodeType_t739504597::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1728 = { sizeof (XmlNotation_t206561061), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1728[4] = 
{
	XmlNotation_t206561061::get_offset_of_localName_5(),
	XmlNotation_t206561061::get_offset_of_publicId_6(),
	XmlNotation_t206561061::get_offset_of_systemId_7(),
	XmlNotation_t206561061::get_offset_of_prefix_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1729 = { sizeof (XmlOutputMethod_t2267235953)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1729[5] = 
{
	XmlOutputMethod_t2267235953::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1730 = { sizeof (XmlParserContext_t2728039553), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1730[13] = 
{
	XmlParserContext_t2728039553::get_offset_of_baseURI_0(),
	XmlParserContext_t2728039553::get_offset_of_docTypeName_1(),
	XmlParserContext_t2728039553::get_offset_of_encoding_2(),
	XmlParserContext_t2728039553::get_offset_of_internalSubset_3(),
	XmlParserContext_t2728039553::get_offset_of_namespaceManager_4(),
	XmlParserContext_t2728039553::get_offset_of_nameTable_5(),
	XmlParserContext_t2728039553::get_offset_of_publicID_6(),
	XmlParserContext_t2728039553::get_offset_of_systemID_7(),
	XmlParserContext_t2728039553::get_offset_of_xmlLang_8(),
	XmlParserContext_t2728039553::get_offset_of_xmlSpace_9(),
	XmlParserContext_t2728039553::get_offset_of_contextItems_10(),
	XmlParserContext_t2728039553::get_offset_of_contextItemCount_11(),
	XmlParserContext_t2728039553::get_offset_of_dtd_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1731 = { sizeof (ContextItem_t1262420678), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1731[3] = 
{
	ContextItem_t1262420678::get_offset_of_BaseURI_0(),
	ContextItem_t1262420678::get_offset_of_XmlLang_1(),
	ContextItem_t1262420678::get_offset_of_XmlSpace_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1732 = { sizeof (XmlParserInput_t2366782760), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1732[5] = 
{
	XmlParserInput_t2366782760::get_offset_of_sourceStack_0(),
	XmlParserInput_t2366782760::get_offset_of_source_1(),
	XmlParserInput_t2366782760::get_offset_of_has_peek_2(),
	XmlParserInput_t2366782760::get_offset_of_peek_char_3(),
	XmlParserInput_t2366782760::get_offset_of_allowTextDecl_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1733 = { sizeof (XmlParserInputSource_t25800784), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1733[6] = 
{
	XmlParserInputSource_t25800784::get_offset_of_BaseURI_0(),
	XmlParserInputSource_t25800784::get_offset_of_reader_1(),
	XmlParserInputSource_t25800784::get_offset_of_state_2(),
	XmlParserInputSource_t25800784::get_offset_of_isPE_3(),
	XmlParserInputSource_t25800784::get_offset_of_line_4(),
	XmlParserInputSource_t25800784::get_offset_of_column_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1734 = { sizeof (XmlProcessingInstruction_t431557540), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1734[2] = 
{
	XmlProcessingInstruction_t431557540::get_offset_of_target_6(),
	XmlProcessingInstruction_t431557540::get_offset_of_data_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1735 = { sizeof (XmlQualifiedName_t1944712516), -1, sizeof(XmlQualifiedName_t1944712516_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1735[4] = 
{
	XmlQualifiedName_t1944712516_StaticFields::get_offset_of_Empty_0(),
	XmlQualifiedName_t1944712516::get_offset_of_name_1(),
	XmlQualifiedName_t1944712516::get_offset_of_ns_2(),
	XmlQualifiedName_t1944712516::get_offset_of_hash_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1736 = { sizeof (XmlReader_t3675626668), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1736[3] = 
{
	XmlReader_t3675626668::get_offset_of_readStringBuffer_0(),
	XmlReader_t3675626668::get_offset_of_binary_1(),
	XmlReader_t3675626668::get_offset_of_settings_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1737 = { sizeof (XmlReaderBinarySupport_t1548133672), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1737[5] = 
{
	XmlReaderBinarySupport_t1548133672::get_offset_of_reader_0(),
	XmlReaderBinarySupport_t1548133672::get_offset_of_base64CacheStartsAt_1(),
	XmlReaderBinarySupport_t1548133672::get_offset_of_state_2(),
	XmlReaderBinarySupport_t1548133672::get_offset_of_hasCache_3(),
	XmlReaderBinarySupport_t1548133672::get_offset_of_dontReset_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1738 = { sizeof (CommandState_t1644897369)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1738[6] = 
{
	CommandState_t1644897369::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1739 = { sizeof (CharGetter_t1955031820), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1740 = { sizeof (XmlReaderSettings_t1578612233), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1740[16] = 
{
	XmlReaderSettings_t1578612233::get_offset_of_checkCharacters_0(),
	XmlReaderSettings_t1578612233::get_offset_of_closeInput_1(),
	XmlReaderSettings_t1578612233::get_offset_of_conformance_2(),
	XmlReaderSettings_t1578612233::get_offset_of_ignoreComments_3(),
	XmlReaderSettings_t1578612233::get_offset_of_ignoreProcessingInstructions_4(),
	XmlReaderSettings_t1578612233::get_offset_of_ignoreWhitespace_5(),
	XmlReaderSettings_t1578612233::get_offset_of_lineNumberOffset_6(),
	XmlReaderSettings_t1578612233::get_offset_of_linePositionOffset_7(),
	XmlReaderSettings_t1578612233::get_offset_of_prohibitDtd_8(),
	XmlReaderSettings_t1578612233::get_offset_of_nameTable_9(),
	XmlReaderSettings_t1578612233::get_offset_of_schemas_10(),
	XmlReaderSettings_t1578612233::get_offset_of_schemasNeedsInitialization_11(),
	XmlReaderSettings_t1578612233::get_offset_of_validationFlags_12(),
	XmlReaderSettings_t1578612233::get_offset_of_validationType_13(),
	XmlReaderSettings_t1578612233::get_offset_of_xmlResolver_14(),
	XmlReaderSettings_t1578612233::get_offset_of_ValidationEventHandler_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1741 = { sizeof (XmlResolver_t2024571559), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1742 = { sizeof (XmlSignificantWhitespace_t1224054391), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1743 = { sizeof (XmlSpace_t2880376877)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1743[4] = 
{
	XmlSpace_t2880376877::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1744 = { sizeof (XmlText_t4111601336), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1745 = { sizeof (XmlTextReader_t511376973), -1, sizeof(XmlTextReader_t511376973_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1745[54] = 
{
	XmlTextReader_t511376973::get_offset_of_cursorToken_3(),
	XmlTextReader_t511376973::get_offset_of_currentToken_4(),
	XmlTextReader_t511376973::get_offset_of_currentAttributeToken_5(),
	XmlTextReader_t511376973::get_offset_of_currentAttributeValueToken_6(),
	XmlTextReader_t511376973::get_offset_of_attributeTokens_7(),
	XmlTextReader_t511376973::get_offset_of_attributeValueTokens_8(),
	XmlTextReader_t511376973::get_offset_of_currentAttribute_9(),
	XmlTextReader_t511376973::get_offset_of_currentAttributeValue_10(),
	XmlTextReader_t511376973::get_offset_of_attributeCount_11(),
	XmlTextReader_t511376973::get_offset_of_parserContext_12(),
	XmlTextReader_t511376973::get_offset_of_nameTable_13(),
	XmlTextReader_t511376973::get_offset_of_nsmgr_14(),
	XmlTextReader_t511376973::get_offset_of_readState_15(),
	XmlTextReader_t511376973::get_offset_of_disallowReset_16(),
	XmlTextReader_t511376973::get_offset_of_depth_17(),
	XmlTextReader_t511376973::get_offset_of_elementDepth_18(),
	XmlTextReader_t511376973::get_offset_of_depthUp_19(),
	XmlTextReader_t511376973::get_offset_of_popScope_20(),
	XmlTextReader_t511376973::get_offset_of_elementNames_21(),
	XmlTextReader_t511376973::get_offset_of_elementNameStackPos_22(),
	XmlTextReader_t511376973::get_offset_of_allowMultipleRoot_23(),
	XmlTextReader_t511376973::get_offset_of_isStandalone_24(),
	XmlTextReader_t511376973::get_offset_of_returnEntityReference_25(),
	XmlTextReader_t511376973::get_offset_of_entityReferenceName_26(),
	XmlTextReader_t511376973::get_offset_of_valueBuffer_27(),
	XmlTextReader_t511376973::get_offset_of_reader_28(),
	XmlTextReader_t511376973::get_offset_of_peekChars_29(),
	XmlTextReader_t511376973::get_offset_of_peekCharsIndex_30(),
	XmlTextReader_t511376973::get_offset_of_peekCharsLength_31(),
	XmlTextReader_t511376973::get_offset_of_curNodePeekIndex_32(),
	XmlTextReader_t511376973::get_offset_of_preserveCurrentTag_33(),
	XmlTextReader_t511376973::get_offset_of_line_34(),
	XmlTextReader_t511376973::get_offset_of_column_35(),
	XmlTextReader_t511376973::get_offset_of_currentLinkedNodeLineNumber_36(),
	XmlTextReader_t511376973::get_offset_of_currentLinkedNodeLinePosition_37(),
	XmlTextReader_t511376973::get_offset_of_useProceedingLineInfo_38(),
	XmlTextReader_t511376973::get_offset_of_startNodeType_39(),
	XmlTextReader_t511376973::get_offset_of_currentState_40(),
	XmlTextReader_t511376973::get_offset_of_nestLevel_41(),
	XmlTextReader_t511376973::get_offset_of_readCharsInProgress_42(),
	XmlTextReader_t511376973::get_offset_of_binaryCharGetter_43(),
	XmlTextReader_t511376973::get_offset_of_namespaces_44(),
	XmlTextReader_t511376973::get_offset_of_whitespaceHandling_45(),
	XmlTextReader_t511376973::get_offset_of_resolver_46(),
	XmlTextReader_t511376973::get_offset_of_normalization_47(),
	XmlTextReader_t511376973::get_offset_of_checkCharacters_48(),
	XmlTextReader_t511376973::get_offset_of_prohibitDtd_49(),
	XmlTextReader_t511376973::get_offset_of_closeInput_50(),
	XmlTextReader_t511376973::get_offset_of_entityHandling_51(),
	XmlTextReader_t511376973::get_offset_of_whitespacePool_52(),
	XmlTextReader_t511376973::get_offset_of_whitespaceCache_53(),
	XmlTextReader_t511376973::get_offset_of_stateStack_54(),
	XmlTextReader_t511376973_StaticFields::get_offset_of_U3CU3Ef__switchU24map51_55(),
	XmlTextReader_t511376973_StaticFields::get_offset_of_U3CU3Ef__switchU24map52_56(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1746 = { sizeof (XmlTokenInfo_t254587324), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1746[13] = 
{
	XmlTokenInfo_t254587324::get_offset_of_valueCache_0(),
	XmlTokenInfo_t254587324::get_offset_of_Reader_1(),
	XmlTokenInfo_t254587324::get_offset_of_Name_2(),
	XmlTokenInfo_t254587324::get_offset_of_LocalName_3(),
	XmlTokenInfo_t254587324::get_offset_of_Prefix_4(),
	XmlTokenInfo_t254587324::get_offset_of_NamespaceURI_5(),
	XmlTokenInfo_t254587324::get_offset_of_IsEmptyElement_6(),
	XmlTokenInfo_t254587324::get_offset_of_QuoteChar_7(),
	XmlTokenInfo_t254587324::get_offset_of_LineNumber_8(),
	XmlTokenInfo_t254587324::get_offset_of_LinePosition_9(),
	XmlTokenInfo_t254587324::get_offset_of_ValueBufferStart_10(),
	XmlTokenInfo_t254587324::get_offset_of_ValueBufferEnd_11(),
	XmlTokenInfo_t254587324::get_offset_of_NodeType_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1747 = { sizeof (XmlAttributeTokenInfo_t3353594030), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1747[4] = 
{
	XmlAttributeTokenInfo_t3353594030::get_offset_of_ValueTokenStartIndex_13(),
	XmlAttributeTokenInfo_t3353594030::get_offset_of_ValueTokenEndIndex_14(),
	XmlAttributeTokenInfo_t3353594030::get_offset_of_valueCache_15(),
	XmlAttributeTokenInfo_t3353594030::get_offset_of_tmpBuilder_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1748 = { sizeof (TagName_t2340974457)+ sizeof (Il2CppObject), sizeof(TagName_t2340974457_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1748[3] = 
{
	TagName_t2340974457::get_offset_of_Name_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TagName_t2340974457::get_offset_of_LocalName_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TagName_t2340974457::get_offset_of_Prefix_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1749 = { sizeof (DtdInputState_t3313602765)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1749[10] = 
{
	DtdInputState_t3313602765::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1750 = { sizeof (DtdInputStateStack_t3023928423), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1750[1] = 
{
	DtdInputStateStack_t3023928423::get_offset_of_intern_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1751 = { sizeof (XmlTextReader_t3514170725), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1751[5] = 
{
	XmlTextReader_t3514170725::get_offset_of_entity_3(),
	XmlTextReader_t3514170725::get_offset_of_source_4(),
	XmlTextReader_t3514170725::get_offset_of_entityInsideAttribute_5(),
	XmlTextReader_t3514170725::get_offset_of_insideAttribute_6(),
	XmlTextReader_t3514170725::get_offset_of_entityNameStack_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1752 = { sizeof (XmlTextWriter_t2527250655), -1, sizeof(XmlTextWriter_t2527250655_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1752[35] = 
{
	XmlTextWriter_t2527250655_StaticFields::get_offset_of_unmarked_utf8encoding_1(),
	XmlTextWriter_t2527250655_StaticFields::get_offset_of_escaped_text_chars_2(),
	XmlTextWriter_t2527250655_StaticFields::get_offset_of_escaped_attr_chars_3(),
	XmlTextWriter_t2527250655::get_offset_of_base_stream_4(),
	XmlTextWriter_t2527250655::get_offset_of_source_5(),
	XmlTextWriter_t2527250655::get_offset_of_writer_6(),
	XmlTextWriter_t2527250655::get_offset_of_preserver_7(),
	XmlTextWriter_t2527250655::get_offset_of_preserved_name_8(),
	XmlTextWriter_t2527250655::get_offset_of_is_preserved_xmlns_9(),
	XmlTextWriter_t2527250655::get_offset_of_allow_doc_fragment_10(),
	XmlTextWriter_t2527250655::get_offset_of_close_output_stream_11(),
	XmlTextWriter_t2527250655::get_offset_of_ignore_encoding_12(),
	XmlTextWriter_t2527250655::get_offset_of_namespaces_13(),
	XmlTextWriter_t2527250655::get_offset_of_xmldecl_state_14(),
	XmlTextWriter_t2527250655::get_offset_of_check_character_validity_15(),
	XmlTextWriter_t2527250655::get_offset_of_newline_handling_16(),
	XmlTextWriter_t2527250655::get_offset_of_is_document_entity_17(),
	XmlTextWriter_t2527250655::get_offset_of_state_18(),
	XmlTextWriter_t2527250655::get_offset_of_node_state_19(),
	XmlTextWriter_t2527250655::get_offset_of_nsmanager_20(),
	XmlTextWriter_t2527250655::get_offset_of_open_count_21(),
	XmlTextWriter_t2527250655::get_offset_of_elements_22(),
	XmlTextWriter_t2527250655::get_offset_of_new_local_namespaces_23(),
	XmlTextWriter_t2527250655::get_offset_of_explicit_nsdecls_24(),
	XmlTextWriter_t2527250655::get_offset_of_namespace_handling_25(),
	XmlTextWriter_t2527250655::get_offset_of_indent_26(),
	XmlTextWriter_t2527250655::get_offset_of_indent_count_27(),
	XmlTextWriter_t2527250655::get_offset_of_indent_char_28(),
	XmlTextWriter_t2527250655::get_offset_of_indent_string_29(),
	XmlTextWriter_t2527250655::get_offset_of_newline_30(),
	XmlTextWriter_t2527250655::get_offset_of_indent_attributes_31(),
	XmlTextWriter_t2527250655::get_offset_of_quote_char_32(),
	XmlTextWriter_t2527250655::get_offset_of_v2_33(),
	XmlTextWriter_t2527250655_StaticFields::get_offset_of_U3CU3Ef__switchU24map53_34(),
	XmlTextWriter_t2527250655_StaticFields::get_offset_of_U3CU3Ef__switchU24map54_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1753 = { sizeof (XmlNodeInfo_t3709371029), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1753[7] = 
{
	XmlNodeInfo_t3709371029::get_offset_of_Prefix_0(),
	XmlNodeInfo_t3709371029::get_offset_of_LocalName_1(),
	XmlNodeInfo_t3709371029::get_offset_of_NS_2(),
	XmlNodeInfo_t3709371029::get_offset_of_HasSimple_3(),
	XmlNodeInfo_t3709371029::get_offset_of_HasElements_4(),
	XmlNodeInfo_t3709371029::get_offset_of_XmlLang_5(),
	XmlNodeInfo_t3709371029::get_offset_of_XmlSpace_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1754 = { sizeof (StringUtil_t2068578019), -1, sizeof(StringUtil_t2068578019_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1754[2] = 
{
	StringUtil_t2068578019_StaticFields::get_offset_of_cul_0(),
	StringUtil_t2068578019_StaticFields::get_offset_of_cmp_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1755 = { sizeof (XmlDeclState_t3530111136)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1755[5] = 
{
	XmlDeclState_t3530111136::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1756 = { sizeof (XmlTokenizedType_t1619571710)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1756[14] = 
{
	XmlTokenizedType_t1619571710::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1757 = { sizeof (XmlUrlResolver_t896669594), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1757[1] = 
{
	XmlUrlResolver_t896669594::get_offset_of_credential_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1758 = { sizeof (XmlValidatingReader_t3416770767), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1758[12] = 
{
	XmlValidatingReader_t3416770767::get_offset_of_entityHandling_3(),
	XmlValidatingReader_t3416770767::get_offset_of_sourceReader_4(),
	XmlValidatingReader_t3416770767::get_offset_of_xmlTextReader_5(),
	XmlValidatingReader_t3416770767::get_offset_of_validatingReader_6(),
	XmlValidatingReader_t3416770767::get_offset_of_resolver_7(),
	XmlValidatingReader_t3416770767::get_offset_of_resolverSpecified_8(),
	XmlValidatingReader_t3416770767::get_offset_of_validationType_9(),
	XmlValidatingReader_t3416770767::get_offset_of_schemas_10(),
	XmlValidatingReader_t3416770767::get_offset_of_dtdReader_11(),
	XmlValidatingReader_t3416770767::get_offset_of_schemaInfo_12(),
	XmlValidatingReader_t3416770767::get_offset_of_storedCharacters_13(),
	XmlValidatingReader_t3416770767::get_offset_of_ValidationEventHandler_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1759 = { sizeof (XmlWhitespace_t2557770518), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1760 = { sizeof (XmlWriter_t1048088568), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1760[1] = 
{
	XmlWriter_t1048088568::get_offset_of_settings_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1761 = { sizeof (XmlWriterSettings_t924210539), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1761[12] = 
{
	XmlWriterSettings_t924210539::get_offset_of_checkCharacters_0(),
	XmlWriterSettings_t924210539::get_offset_of_closeOutput_1(),
	XmlWriterSettings_t924210539::get_offset_of_conformance_2(),
	XmlWriterSettings_t924210539::get_offset_of_encoding_3(),
	XmlWriterSettings_t924210539::get_offset_of_indent_4(),
	XmlWriterSettings_t924210539::get_offset_of_indentChars_5(),
	XmlWriterSettings_t924210539::get_offset_of_newLineChars_6(),
	XmlWriterSettings_t924210539::get_offset_of_newLineOnAttributes_7(),
	XmlWriterSettings_t924210539::get_offset_of_newLineHandling_8(),
	XmlWriterSettings_t924210539::get_offset_of_omitXmlDeclaration_9(),
	XmlWriterSettings_t924210539::get_offset_of_outputMethod_10(),
	XmlWriterSettings_t924210539::get_offset_of_U3CNamespaceHandlingU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1762 = { sizeof (ValidationEventHandler_t1580700381), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1763 = { sizeof (XmlValueGetter_t1685472371), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1764 = { sizeof (XmlNodeChangedEventHandler_t2964483403), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1765 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305141), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1765[8] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields::get_offset_of_U24U24fieldU2D23_0(),
	U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields::get_offset_of_U24U24fieldU2D24_1(),
	U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields::get_offset_of_U24U24fieldU2D26_2(),
	U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields::get_offset_of_U24U24fieldU2D27_3(),
	U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields::get_offset_of_U24U24fieldU2D28_4(),
	U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields::get_offset_of_U24U24fieldU2D29_5(),
	U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields::get_offset_of_U24U24fieldU2D43_6(),
	U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields::get_offset_of_U24U24fieldU2D44_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1766 = { sizeof (U24ArrayTypeU2412_t3672778807)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2412_t3672778807 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1767 = { sizeof (U24ArrayTypeU248_t1957337328)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU248_t1957337328 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1768 = { sizeof (U24ArrayTypeU24256_t2038352957)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24256_t2038352957 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1769 = { sizeof (U24ArrayTypeU241280_t628910058)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU241280_t628910058 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1770 = { sizeof (U3CModuleU3E_t3783534219), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1771 = { sizeof (LoadOptions_t53198144)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1771[5] = 
{
	LoadOptions_t53198144::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1772 = { sizeof (SaveOptions_t2961005337)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1772[3] = 
{
	SaveOptions_t2961005337::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1773 = { sizeof (XAttribute_t3858477518), -1, sizeof(XAttribute_t3858477518_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1773[6] = 
{
	XAttribute_t3858477518_StaticFields::get_offset_of_empty_array_4(),
	XAttribute_t3858477518::get_offset_of_name_5(),
	XAttribute_t3858477518::get_offset_of_value_6(),
	XAttribute_t3858477518::get_offset_of_next_7(),
	XAttribute_t3858477518::get_offset_of_previous_8(),
	XAttribute_t3858477518_StaticFields::get_offset_of_escapeChars_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1774 = { sizeof (XCData_t1306050895), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1775 = { sizeof (XComment_t1618005895), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1775[1] = 
{
	XComment_t1618005895::get_offset_of_value_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1776 = { sizeof (XContainer_t1445911831), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1776[2] = 
{
	XContainer_t1445911831::get_offset_of_first_8(),
	XContainer_t1445911831::get_offset_of_last_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1777 = { sizeof (U3CNodesU3Ec__Iterator1A_t1473861320), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1777[5] = 
{
	U3CNodesU3Ec__Iterator1A_t1473861320::get_offset_of_U3CnU3E__0_0(),
	U3CNodesU3Ec__Iterator1A_t1473861320::get_offset_of_U3CnextU3E__1_1(),
	U3CNodesU3Ec__Iterator1A_t1473861320::get_offset_of_U24PC_2(),
	U3CNodesU3Ec__Iterator1A_t1473861320::get_offset_of_U24current_3(),
	U3CNodesU3Ec__Iterator1A_t1473861320::get_offset_of_U3CU3Ef__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1778 = { sizeof (U3CElementsU3Ec__Iterator1E_t2952571908), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1778[6] = 
{
	U3CElementsU3Ec__Iterator1E_t2952571908::get_offset_of_U3CU24s_49U3E__0_0(),
	U3CElementsU3Ec__Iterator1E_t2952571908::get_offset_of_U3CnU3E__1_1(),
	U3CElementsU3Ec__Iterator1E_t2952571908::get_offset_of_U3CelU3E__2_2(),
	U3CElementsU3Ec__Iterator1E_t2952571908::get_offset_of_U24PC_3(),
	U3CElementsU3Ec__Iterator1E_t2952571908::get_offset_of_U24current_4(),
	U3CElementsU3Ec__Iterator1E_t2952571908::get_offset_of_U3CU3Ef__this_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1779 = { sizeof (U3CElementsU3Ec__Iterator1F_t3355856435), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1779[7] = 
{
	U3CElementsU3Ec__Iterator1F_t3355856435::get_offset_of_U3CU24s_50U3E__0_0(),
	U3CElementsU3Ec__Iterator1F_t3355856435::get_offset_of_U3CelU3E__1_1(),
	U3CElementsU3Ec__Iterator1F_t3355856435::get_offset_of_name_2(),
	U3CElementsU3Ec__Iterator1F_t3355856435::get_offset_of_U24PC_3(),
	U3CElementsU3Ec__Iterator1F_t3355856435::get_offset_of_U24current_4(),
	U3CElementsU3Ec__Iterator1F_t3355856435::get_offset_of_U3CU24U3Ename_5(),
	U3CElementsU3Ec__Iterator1F_t3355856435::get_offset_of_U3CU3Ef__this_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1780 = { sizeof (XDeclaration_t3367285402), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1780[3] = 
{
	XDeclaration_t3367285402::get_offset_of_encoding_0(),
	XDeclaration_t3367285402::get_offset_of_standalone_1(),
	XDeclaration_t3367285402::get_offset_of_version_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1781 = { sizeof (XDocument_t2733326047), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1781[1] = 
{
	XDocument_t2733326047::get_offset_of_xmldecl_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1782 = { sizeof (XDocumentType_t738990919), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1782[4] = 
{
	XDocumentType_t738990919::get_offset_of_name_8(),
	XDocumentType_t738990919::get_offset_of_pubid_9(),
	XDocumentType_t738990919::get_offset_of_sysid_10(),
	XDocumentType_t738990919::get_offset_of_intSubset_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1783 = { sizeof (XElement_t553821050), -1, sizeof(XElement_t553821050_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1783[5] = 
{
	XElement_t553821050_StaticFields::get_offset_of_emptySequence_10(),
	XElement_t553821050::get_offset_of_name_11(),
	XElement_t553821050::get_offset_of_attr_first_12(),
	XElement_t553821050::get_offset_of_attr_last_13(),
	XElement_t553821050::get_offset_of_explicit_is_empty_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1784 = { sizeof (U3CAttributesU3Ec__Iterator20_t3474816789), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1784[5] = 
{
	U3CAttributesU3Ec__Iterator20_t3474816789::get_offset_of_U3CaU3E__0_0(),
	U3CAttributesU3Ec__Iterator20_t3474816789::get_offset_of_U3CnextU3E__1_1(),
	U3CAttributesU3Ec__Iterator20_t3474816789::get_offset_of_U24PC_2(),
	U3CAttributesU3Ec__Iterator20_t3474816789::get_offset_of_U24current_3(),
	U3CAttributesU3Ec__Iterator20_t3474816789::get_offset_of_U3CU3Ef__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1785 = { sizeof (XName_t785190363), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1785[2] = 
{
	XName_t785190363::get_offset_of_local_0(),
	XName_t785190363::get_offset_of_ns_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1786 = { sizeof (XNamespace_t1613015075), -1, sizeof(XNamespace_t1613015075_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1786[6] = 
{
	XNamespace_t1613015075_StaticFields::get_offset_of_blank_0(),
	XNamespace_t1613015075_StaticFields::get_offset_of_xml_1(),
	XNamespace_t1613015075_StaticFields::get_offset_of_xmlns_2(),
	XNamespace_t1613015075_StaticFields::get_offset_of_nstable_3(),
	XNamespace_t1613015075::get_offset_of_uri_4(),
	XNamespace_t1613015075::get_offset_of_table_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1787 = { sizeof (XNode_t2707504214), -1, sizeof(XNode_t2707504214_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1787[4] = 
{
	XNode_t2707504214_StaticFields::get_offset_of_eq_comparer_4(),
	XNode_t2707504214_StaticFields::get_offset_of_order_comparer_5(),
	XNode_t2707504214::get_offset_of_previous_6(),
	XNode_t2707504214::get_offset_of_next_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1788 = { sizeof (XNodeDocumentOrderComparer_t1208684534), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1789 = { sizeof (CompareResult_t4155354831)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1789[9] = 
{
	CompareResult_t4155354831::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1790 = { sizeof (XNodeEqualityComparer_t3116038621), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1791 = { sizeof (XObject_t3550811009), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1791[4] = 
{
	XObject_t3550811009::get_offset_of_owner_0(),
	XObject_t3550811009::get_offset_of_baseuri_1(),
	XObject_t3550811009::get_offset_of_line_2(),
	XObject_t3550811009::get_offset_of_column_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1792 = { sizeof (XProcessingInstruction_t2854903359), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1792[2] = 
{
	XProcessingInstruction_t2854903359::get_offset_of_name_8(),
	XProcessingInstruction_t2854903359::get_offset_of_data_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1793 = { sizeof (XText_t1860529549), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1793[1] = 
{
	XText_t1860529549::get_offset_of_value_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1794 = { sizeof (XUtil_t3957506230), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1795 = { sizeof (U3CExpandArrayU3Ec__Iterator25_t507968046), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1795[9] = 
{
	U3CExpandArrayU3Ec__Iterator25_t507968046::get_offset_of_o_0(),
	U3CExpandArrayU3Ec__Iterator25_t507968046::get_offset_of_U3CnU3E__0_1(),
	U3CExpandArrayU3Ec__Iterator25_t507968046::get_offset_of_U3CU24s_86U3E__1_2(),
	U3CExpandArrayU3Ec__Iterator25_t507968046::get_offset_of_U3CobjU3E__2_3(),
	U3CExpandArrayU3Ec__Iterator25_t507968046::get_offset_of_U3CU24s_87U3E__3_4(),
	U3CExpandArrayU3Ec__Iterator25_t507968046::get_offset_of_U3CooU3E__4_5(),
	U3CExpandArrayU3Ec__Iterator25_t507968046::get_offset_of_U24PC_6(),
	U3CExpandArrayU3Ec__Iterator25_t507968046::get_offset_of_U24current_7(),
	U3CExpandArrayU3Ec__Iterator25_t507968046::get_offset_of_U3CU24U3Eo_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1796 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305142), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1796[1] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields::get_offset_of_U24U24fieldU2D0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1797 = { sizeof (U24ArrayTypeU2416_t1703410337)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2416_t1703410337 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1798 = { sizeof (U3CModuleU3E_t3783534220), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1799 = { sizeof (NetworkReachability_t1092747145)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1799[4] = 
{
	NetworkReachability_t1092747145::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
