﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_String2029220233.h"

// System.IntPtr ThirdParty.iOS4Unity.ObjC::GetClass(System.String)
extern "C"  IntPtr_t ObjC_GetClass_m4189432679 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.iOS4Unity.ObjC::MessageSend(System.IntPtr,System.IntPtr)
extern "C"  void ObjC_MessageSend_m2734814359 (Il2CppObject * __this /* static, unused */, IntPtr_t ___receiver0, IntPtr_t ___selector1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.iOS4Unity.ObjC::MessageSend(System.IntPtr,System.IntPtr,System.IntPtr)
extern "C"  void ObjC_MessageSend_m4176128033 (Il2CppObject * __this /* static, unused */, IntPtr_t ___receiver0, IntPtr_t ___selector1, IntPtr_t ___arg12, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr ThirdParty.iOS4Unity.ObjC::MessageSendIntPtr(System.IntPtr,System.IntPtr)
extern "C"  IntPtr_t ObjC_MessageSendIntPtr_m4279986307 (Il2CppObject * __this /* static, unused */, IntPtr_t ___receiver0, IntPtr_t ___selector1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr ThirdParty.iOS4Unity.ObjC::MessageSendIntPtr(System.IntPtr,System.IntPtr,System.IntPtr)
extern "C"  IntPtr_t ObjC_MessageSendIntPtr_m2832329625 (Il2CppObject * __this /* static, unused */, IntPtr_t ___receiver0, IntPtr_t ___selector1, IntPtr_t ___arg12, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr ThirdParty.iOS4Unity.ObjC::MessageSendIntPtr(System.IntPtr,System.IntPtr,System.IntPtr,System.Int32)
extern "C"  IntPtr_t ObjC_MessageSendIntPtr_m1847167438 (Il2CppObject * __this /* static, unused */, IntPtr_t ___receiver0, IntPtr_t ___selector1, IntPtr_t ___arg12, int32_t ___arg23, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ThirdParty.iOS4Unity.ObjC::MessageSendString(System.IntPtr,System.IntPtr)
extern "C"  String_t* ObjC_MessageSendString_m3341885059 (Il2CppObject * __this /* static, unused */, IntPtr_t ___receiver0, IntPtr_t ___selector1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr ThirdParty.iOS4Unity.ObjC::dlsym(System.IntPtr,System.String)
extern "C"  IntPtr_t ObjC_dlsym_m3959325786 (Il2CppObject * __this /* static, unused */, IntPtr_t ___handle0, String_t* ___symbol1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr ThirdParty.iOS4Unity.ObjC::dlopen(System.String,System.Int32)
extern "C"  IntPtr_t ObjC_dlopen_m3299971014 (Il2CppObject * __this /* static, unused */, String_t* ___path0, int32_t ___mode1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ThirdParty.iOS4Unity.ObjC::FromNSString(System.IntPtr)
extern "C"  String_t* ObjC_FromNSString_m3897991665 (Il2CppObject * __this /* static, unused */, IntPtr_t ___handle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ThirdParty.iOS4Unity.ObjC::GetStringConstant(System.IntPtr,System.String)
extern "C"  String_t* ObjC_GetStringConstant_m3908450372 (Il2CppObject * __this /* static, unused */, IntPtr_t ___handle0, String_t* ___symbol1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr ThirdParty.iOS4Unity.ObjC::ToNSString(System.String)
extern "C"  IntPtr_t ObjC_ToNSString_m4064545762 (Il2CppObject * __this /* static, unused */, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdParty.iOS4Unity.ObjC::.cctor()
extern "C"  void ObjC__cctor_m371117919 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
