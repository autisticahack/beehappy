﻿using UnityEngine;
using System.Collections;

public class PlayerControl : MonoBehaviour {

	public float speed = 0.0f;
	private Rigidbody rb;

	void Start ()
	{
		rb = GetComponent<Rigidbody> ();
	}

	void FixedUpdate()
	{
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moverVertial = Input.GetAxis ("Vertical");

		Vector3 movement = new Vector3 (moveHorizontal,0.0f, moverVertial);
		rb.AddForce (movement * speed);
	}

	void OnTriggerEnter(Collider other) {

		if (other.gameObject.CompareTag ("PickUp"))
			other.gameObject.SetActive (false);
	}
		
}
