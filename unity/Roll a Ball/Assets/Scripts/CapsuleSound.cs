﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapsuleSound : MonoBehaviour {
	public AudioClip impact;
	public float volume = 1.0F;
	AudioSource audio;

	// Use this for initialization
	void Start () {
		audio = GetComponent<AudioSource> ();
		
	}

	void OnCollisionEnter () {
		audio.PlayOneShot (impact, volume);
		
	}
}
