from flask import Flask
from flask import render_template
import datetime
import boto3


# EB looks for an 'application' callable by default.
application = Flask(__name__)

def datetimeformat(value='Just now', format='%H:%M / %d-%m-%Y'):
    #    return datetime.datetime.strptime(value, '%Y-%m-%dT%H:%M:%S%z').strftime(format)
    try:
        d = datetime.datetime.strptime(value, '%Y-%m-%dT%H:%M:%S%z').strftime(format)
    except (ValueError,  TypeError):
        # data format mismatch
        d = "Just now"
    return d

application.jinja_env.filters['datetimeformat'] = datetimeformat

# this is just a prototype for hte hackathon - out of time - need to parse the data and not use dummy floats.
def gpsformat(value):
    lat = "%.8f" % (51.516212)
    lon = "%.8f" % (-0.0847068)
    return """<a href="http://maps.google.com/maps?&z=17&q="""+ lat + "+" + lon + "&ll=" + lat + "+" + lon + """ "><img src="/static/pin.png" width="20" height="20"/>"""

application.jinja_env.filters['gpsformat'] = gpsformat


def list_events():

    # change to name of database in dynamo
    dynamodb = boto3.resource('dynamodb', region_name='eu-central-1')

    # change to name of table
    table = dynamodb.Table('Event')

    # here you can search the events...
    result = table.scan()

    eventitems = result['Items']

    return render_template('events.html', title='Events', events=eventitems)

def list_remedies():

    # change to name of database in dynamo
    dynamodb = boto3.resource('dynamodb', region_name='eu-central-1')

    # change to name of table
    table = dynamodb.Table('Remedy')

    # here you can search the events...
    result = table.scan()

    remedyitems = result['Items']

    return render_template('remedy.html', title='Remedies', remedies=remedyitems)

def list_events_get():

    # change to name of database in dynamo
    dynamodb = boto3.resource('dynamodb', region_name='eu-central-1')

    # change to name of table
    table = dynamodb.Table('Event')

    # here you can search the events...
    result = table.scan()

    eventitems = result['Items']

    return eventitems

def list_remedies_get():

    # change to name of database in dynamo
    dynamodb = boto3.resource('dynamodb', region_name='eu-central-1')

    # change to name of table
    table = dynamodb.Table('Remedy')

    # here you can search the events...
    result = table.scan()

    remedyitems = result['Items']

    return remedyitems

# add a rule for the index page.
# application.add_url_rule('/', 'index', (lambda: header_text +
#     say_hello() + instructions + footer_text))
application.add_url_rule('/', 'index', lambda: render_template('index.html'))

application.add_url_rule('/events/', 'events', lambda: list_events())

application.add_url_rule('/remedies/', 'remedies', lambda: list_remedies())

application.add_url_rule('/events/get/', 'getevents', lambda: list_events_get())

application.add_url_rule('/remedies/get/', 'getremedies', lambda: list_remedies_get())

# run the app.
if __name__ == "__main__":
    # Setting debug to True enables debug output. This line should be
    # removed before deploying a production app.
    # application.debug = True
    application.run()
